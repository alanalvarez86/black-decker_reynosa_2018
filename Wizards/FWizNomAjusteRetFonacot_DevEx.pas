unit FWizNomAjusteRetFonacot_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  {$ifdef TRESS_DELPHIXE5_UP}Data.DB,{$endif}
  Dialogs, FWizNomBasico_DevEx, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, ZetaDBTextBox, ZetaKeyCombo, Mask,
  ZetaNumero, ZetaCommonLists, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons,
  cxRadioGroup, cxTextEdit, cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses,
  cxImage, dxCustomWizardControl, dxWizardControl, TressMorado2013, cxCheckBox;

type
  TWizNomAjusteRetFonacot_DevEx = class(TWizNomBasico_DevEx)
    GBTipoPrestamo: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    NoTipoPrestamo: TZetaTextBox;
    TipoPrestamo: TZetaTextBox;
    Label6: TLabel;
    MesDefault: TZetaKeyCombo;
    RGFiltro: TRadioGroup;
    YearDefaultAct: TZetaNumero;
    RangosDNominas: TdxWizardControlPage;
    GBRangosNominas: TGroupBox;
    Instrucciones: TdxWizardControlPage;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Prestamo: TdxWizardControlPage;
    Definicion: TdxWizardControlPage;
    GroupBox2: TGroupBox;
    GBSaldosFavor: TGroupBox;
    lSaldarpresta: TCheckBox;
    lSaldarBajas: TCheckBox;
    IncluirIncapa: TCheckBox;
    lbTolerancia: TLabel;
    Tolerancia: TZetaNumero;
    GroupBox3: TGroupBox;
    TipoPrestamoAjus: TZetaKeyLookup_DevEx;
    Label4: TLabel;
    RGAnterior: TRadioGroup;
    RGAjuste: TRadioGroup;
    Label7: TLabel;
    Label9: TLabel;
    ckProvision: TcxCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure RGFiltroClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure RGAjusteClick(Sender: TObject); //DevEx(by am): Agregado para enfocar correctamente los controles
  private
    { Private declarations }
    FPanelesVisible, FMesAnterior : Integer;
    FRangosNomina: Variant;
    FPanelesCreados: Boolean;
    procedure HabilitarFiltrosNom;
    function GetFiltraPorNom: Boolean;
    procedure SetFiltroPeriodo;
    procedure SetControlesRNominas;
  protected
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaParametros;override;
    procedure CargaListaVerificacion; override;
    procedure AjustarLabelLeft(lblLabel: TLabel; iPeriodo: Integer); //acl
  public
    { Public declarations }
    Panels : array of TPanel;
  end;

const
     K_REEMPLAZAR = 0;
     K_ANTERIOR   = 1;
     K_RANGO_NOM = 1;
     K_FILTRO_MES = 0;
     K_FILTRO_NOMINA = 0;
     K_SIN_VALOR = 0;
     K_TOLERANCIA_DEFAULT = 1.00;
     K_MAX_NOMINAS = High ( eTipoPeriodo );
     K_FINAL = -1;

var
  WizNomAjusteRetFonacot_DevEx: TWizNomAjusteRetFonacot_DevEx;

implementation

uses DCliente,
     DProcesos,
     DTablas,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaTipoEntidad,
     ZGlobalTress,
     DBaseGlobal,
     ZBaseSelectGrid_DevEx,
     DGlobal,
     FNomAjusteFonacotGridSelect_DevEx,
     DCatalogos,
     DNomina,
     ZetaDialogo;

{$R *.dfm}



procedure TWizNomAjusteRetFonacot_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     dmTablas.cdsTPresta.Conectar;
     SetFiltroPeriodo;
     TipoPrestamoAjus.Tag:= K_GLOBAL_DEF_NOM_TIPOPRESTAAJUSTE;
     ParametrosControl := YearDefaultAct;
     HelpContext:= H_WIZ_AJUSTE_RET_FONACOT;
     RGFiltro.ItemIndex:= K_FILTRO_MES;
     //RGFiltroNomina.ItemIndex := K_FILTRO_NOMINA;
     Tolerancia.Valor:= K_TOLERANCIA_DEFAULT;

     {***Se define la seceunta de las paginas***}
     Ejecucion.PageIndex := WizardControl.PageCount-1;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount-2;
     Definicion.PageIndex := WizardControl.PageCount-3;
     Prestamo.PageIndex := WizardControl.PageCount-4;
     RangosDNominas.PageIndex := WizardControl.PageCount-5;
     Parametros.PageIndex := WizardControl.PageCount-6;
     Instrucciones.PageIndex := WizardControl.PageCount-7;
     {***Se define el tipo de salto a utilizar en los wizards***}
     Wizard.SaltoEspecial := TRUE;

//     Parametros.header.InstanceSize.Size.
end;

{acl 7/10/08 Para ajustar dinamicamente la posicion de los labels} 
procedure TWizNomAjusteRetFonacot_DevEx.AjustarLabelLeft(lblLabel: TLabel; iPeriodo: Integer);
const
     K_WIDTH_MAX = 120;
     K_WIDTH_2PUNTOS = 3;
var
   iWidthO, iDif: integer;
begin
     iWidthO:=lblLabel.Width;
     
     lblLabel.Caption:= ObtieneElemento( lfTipoPeriodo, iPeriodo ) + ':';
     if ( lblLabel.Width > K_WIDTH_MAX ) then
     begin
          lblLabel.Width:= K_WIDTH_MAX;
     end;     

     if( lblLabel.Width > ( iWidthO - K_WIDTH_2PUNTOS ) ) then
     begin
          iDif:= lblLabel.Width - iWidthO;
          lblLabel.Left:=lblLabel.Left - iDif;
     end;
end;

procedure TWizNomAjusteRetFonacot_DevEx.FormShow(Sender: TObject);
{var
   iPeriodoIni, iPeriodoFinal: Integer; }
begin
     inherited;
     NoTipoPrestamo.Caption:= Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO);
     TipoPrestamo.Caption:= dmTablas.cdsTPresta.GetDescripcion(NoTipoPrestamo.Caption);
     with dmCliente do
     begin
          YearDefaultAct.Valor := GetDatosPeriodoActivo.Year;
          MesDefault.Valor:= ImssMes;
     end;
     //dmCatalogos.GetPeriodoPorMes( iPeriodoIni, iPeriodoFinal);
     FMesAnterior:= -1;
     HabilitarFiltrosNom;
     Advertencia.Caption := 'Al ejecutar este proceso se generar� un cargo o un abono a los empleados que apliquen los ajustes de Fonacot.';
end;

procedure TWizNomAjusteRetFonacot_DevEx.SetFiltroPeriodo;
var
   iYear : Integer;
begin
     iYear := YearDefaultAct.ValorEntero;
     with dmCatalogos do
     begin
          with cdsPeriodoOtro do
          begin
               if ( IsEmpty ) or
                  ( iYear <> FieldByName( 'PE_YEAR' ).AsInteger ) then
               begin
                    GetDatosPeriodoOtro( iYear );
               end;
          end;
     end;
end;

procedure TWizNomAjusteRetFonacot_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const
     K_MENSAJE_VALOR_NOM = 'Debe especificarse un valor v�lido de rango de n�minas';
var
   iYear, iMes: Integer;
   dFecha, dFechaIni, dFechaFin: TDate;
   eStatus: eStatusPeriodo;
   oLookup: TZetaKeyLookup_DevEx;

   function CargoRangoNominas( const lFiltroRangoNominas: Boolean): Boolean;
   var
      i,c,iPanel,iRangoIni,iRangoFin: Integer;
      oPanel: TPanel;
      eTipo: Integer;

        procedure IniciaBusquedaPeriodo;
      begin
           iRangoIni:= 0;
           iRangoFin:= 0;
           iPanel:= 0;
           dFecha:= 0;
           dFechaIni:= 0;
           dFechaFin:= 0;
      end;

   begin
        Result:= TRUE;
        oLookup:= NIL;
        IniciaBusquedaPeriodo;

        if lFiltroRangoNominas then
        begin
            FRangosNomina:= VarArrayCreate( [ 0, FPanelesVisible ], varVariant );
             with GBRangosNominas do
             begin
                  //Barre los controles del Grupo: Son los paneles

                  for i:= 0 to ControlCount - 1 do
                  begin
                        if ( Result ) and ( Controls[i] is TPanel ) then
                        begin
                             oPanel:= TPanel( Controls[i]);
                             //Si es visible el Panel:
                             if ( oPanel.Visible ) then
                             begin
                                  //Barre los controles del Panel
                                  for c:= 0 to oPanel.ControlCount - 1 do
                                  begin
                                       if ( Result ) and ( oPanel.Controls[c] is TZetaKeyLookup_DevEx ) then
                                       begin
                                            oLookup:= TZetaKeyLookup_DevEx( oPanel.Controls[c] );
                                            if ( oLookup.Tag = K_FINAL ) then
                                            begin
                                                 iRangoFin:= oLookup.Valor;
                                                 Result:= dmCatalogos.BuscaPeriodo( iYear, iRangoFin, eTipoPeriodo(oPanel.Tag), eStatus, dFecha, dFechaFin );
                                            end
                                            else
                                            begin
                                                 iRangoIni:= oLookup.Valor;
                                                 Result:= dmCatalogos.BuscaPeriodo( iYear, iRangoIni, eTipoPeriodo(oPanel.Tag), eStatus, dFechaIni, dFecha );
                                            end;
                                       end;
                                  end;
                                  //Ya que barri� los controles asigna fechas y rangos
                                  if ( Result ) then
                                  begin
                                       Result:= ( iRangoIni<= iRangoFin );
                                       if ( Result ) then
                                       begin
                                            FRangosNomina[iPanel]:= VarArrayOf( [ oPanel.Tag, iRangoIni, iRangoFin, dFechaIni, dFechaFin]);
                                            Inc( iPanel );
                                       end
                                  end;
                             end;
                        end;
                  end;
             end;
        end
        else
        begin
             with dmCatalogos do
             begin
                  if FMesAnterior <> MesDefault.Valor then
                  begin
                       //FRangosNomina:= VarArrayCreate( [ 0, Ord(High(eTipoPeriodo)) ], varVariant ); //OLD
                       FRangosNomina:= VarArrayCreate( [ 0, FArregloPeriodo.Count ], varVariant );
                       for eTipo:= 0 to FArregloPeriodo.count-1 do
                       begin
                            IniciaBusquedaPeriodo;
                            GetPeriodoPorMes( eTipo, MesDefault.Valor, iRangoIni, iRangoFin );
                            if ( iRangoIni > 0 ) then
                            begin
                                 Result:= (iRangoIni <= iRangoFin) and
                                           BuscaPeriodo( iYear, iRangoIni, eTipo, eStatus, dFechaIni, dFecha ) and
                                           BuscaPeriodo( iYear, iRangoFin, eTipo, eStatus, dFecha, dFechaFin );
                            end;
                            FRangosNomina[Ord(eTipo)]:= VarArrayOf( [ eTipo, iRangoIni, iRangoFin, dFechaIni, dFechaFin]);
                       end;
                       FMesAnterior:= MesDefault.Valor;
                  end;
             end;
        end;
   end;


begin
     ParametrosControl := nil;
     HabilitarFiltrosNom;
     iYear:= YearDefaultAct.ValorEntero;
     iMes:= MesDefault.Valor;
     {***Validar si se puede Mover hacia adelante***}
     if CanMove then
     begin
          if Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
          begin
               SetFiltroPeriodo;
               if ( dmNomina.GetStatusFonacot( iMes, iYear ) = slAfectadaTotal ) then
                   begin
                        CanMove:= Error( 'El c�lculo de Fonacot se encuentra cerrado', MesDefault );
                   end
               else
                 if ( dmCatalogos.cdsPeriodoOtro.IsEmpty ) then
                 begin
                       CanMove:= Error( Format( 'No existen periodos definidos para el a�o %d', [ iYear ] ), YearDefaultAct );
                 end
                 else
                  if ( GetFiltraporNom ) then
                  begin
                       SetControlesRNominas;
                  end
                  else
                    if not CargoRangoNominas(GetFiltraporNom) then
                    begin
                      CanMove:= Error( 'Error al revisar periodos que acumulan al mes', RGFiltro );
                    end;
          end
          else if Wizard.Adelante and Wizard.EsPaginaActual( Prestamo ) then
          begin

              if ( NoTipoPrestamo.Caption = TipoPrestamoAjus.Llave ) then
                  begin
                        CanMove:= Error( 'El Tipo de Pr�stamo para Ajuste no puede ser igual al Tipo de Pr�stamo de Fonacot', TipoPrestamoAjus );
                  end
              else
                  if ( TipoPrestamoAjus.Llave = VACIO ) then
                      begin
                          CanMove:= Error( 'Debe Especificarse un Tipo de Pr�stamo para Ajuste', TipoPrestamoAjus );
                      end

          end
          else if Wizard.Adelante and Wizard.EsPaginaActual( RangosDNominas ) then
          begin
               if not CargoRangoNominas(GetFiltraporNom)  then
               begin
                    CanMove:= Error( K_MENSAJE_VALOR_NOM, oLookup );
               end;
          end;
     end;



     {***Validar hacia que pagina queremos que se mueva***}
     if Wizard.Adelante then
     begin
          if ( WizardControl.ActivePage = FiltrosCondiciones ) then
          begin
               SeleccionarClick( Sender );
               CanMove := Verificacion;
          end;
          if CanMove then
          begin
                if Wizard.EsPaginaActual (Instrucciones ) Then
                    iNewPAge := Parametros.PageIndex
                else if  Wizard.EsPaginaActual( Parametros) then
                   if  ( RGFiltro.ItemIndex = K_RANGO_NOM ) then
                       iNewPage := RangosDNominas.PageIndex
                   else
                       iNewPage := Prestamo.PageIndex
                else if Wizard.EsPaginaActual( RangosDNominas )then
                    iNewPage := Prestamo.PageIndex
                else if  Wizard.EsPaginaActual( Prestamo) then
                    iNewPage := Definicion.PageIndex
                else if Wizard.EsPaginaActual( Definicion )then
                    iNewPage := FiltrosCondiciones.PageIndex
               else if Wizard.EsPaginaActual( FiltrosCondiciones ) then
                    iNewPage := Ejecucion.PageIndex;
          end;
     end
     else
     begin
         if  Wizard.EsPaginaActual( Ejecucion ) then
         begin
             iNewPage := FiltrosCondiciones.PageIndex;
             Verificacion := False;
         end
         else if Wizard.EsPaginaActual (FiltrosCondiciones ) then
              iNewPage := Definicion.PageIndex
         else if Wizard.EsPaginaActual ( Definicion ) then
              iNewPage := Prestamo.PageIndex
         else if Wizard.EsPaginaActual( Prestamo ) then
              if  ( RGFiltro.ItemIndex = K_RANGO_NOM ) then
                 iNewPage := RangosDNominas.PageIndex
             else
                 iNewPage := Parametros.PageIndex
         else if Wizard.EsPaginaActual( RangosDNominas )then
              iNewPage := Parametros.PageIndex
         else if Wizard.EsPaginaActual (Parametros)  then
              iNewPage := Instrucciones.PageIndex;
     end;
     inherited;
end;

procedure TWizNomAjusteRetFonacot_DevEx.HabilitarFiltrosNom;
var
   lHabilita: Boolean;
begin
     lHabilita:= ( RGFiltro.ItemIndex = K_RANGO_NOM );
     RangosDNominas.Enabled:= lHabilita;
     FMesAnterior:= -1;
end;

procedure TWizNomAjusteRetFonacot_DevEx.RGAjusteClick(Sender: TObject);
var lHabilita : Boolean;
begin
  inherited;
  if RGAjuste.ItemIndex = 1 then
  begin
        lHabilita := True;
        RGAnterior.Top := 196
  end
  else
  begin
      lHabilita := False;
       RGAnterior.Top := 163;
  end;


  Label7.Visible := lHabilita;
  ckProvision.Visible := lHabilita;
//  rgAjusteTodos.Visible := lHabilita;

end;

procedure TWizNomAjusteRetFonacot_DevEx.RGFiltroClick(Sender: TObject);
begin
     HabilitarFiltrosNom;
end;

function TWizNomAjusteRetFonacot_DevEx.Verificar: Boolean;
begin
     //Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TNomAjusteFonacotGridSelect_DevEx );
     NomAjusteFonacotGridSelect_DevEx := TNomAjusteFonacotGridSelect_DevEx.Create( Self );
     try
          //Self.Hide;
          with NomAjusteFonacotGridSelect_DevEx do
             begin
                  Dataset := dmProcesos.cdsDataSet;

                  if Dataset.RecordCount = 0 then
                      OK_DevEx.Enabled := False;

                  ShowModal;

                  Result := ( Modalidad = mrOk );
                  if Result = False  then
                       Verificacion := False

             end;
          finally
             FreeAndNil( NomAjusteFonacotGridSelect_DevEx );
     end;
end;

procedure TWizNomAjusteRetFonacot_DevEx.CargaListaVerificacion;
begin
     inherited;
     dmProcesos.AjusteRetFonacotGetLista( ParameterList );
end;

function TWizNomAjusteRetFonacot_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AjusteRetFonacot(ParameterList, Verificacion);
end;

procedure TWizNomAjusteRetFonacot_DevEx.CargaParametros;
const
     K_SIN_VALOR = -1;
var
   lReemplazar: Boolean;
   //FRangosNomina: Variant;

   {procedure PreparaArreglo;
   var
      i, Count, iRangoIni, iRangoFin: Integer;
   begin
        for i:= 0 to FPanelesVisible do
        begin
             with GBRangosNominas do
             begin
                  for i:= 0 to ControlCount - 1 do
                  begin
                        if ( Controls[i].ClassType = TPanel ) then
                        begin
                             oPanel:= TPanel( Controls[i] );
                             if ( oPanel.Visible ) then
                             begin
                                  for c:= 0 to oPanel.ControlCount - 1 do
                                  begin
                                       if ( oPanel.Controls[c].ClassType = TZetaKeyLookup ) then
                                       begin
                                            oLookup:= TZetaKeyLookup( oPanel.Controls[c] );
                                            if ( oLookup.Tag  = K_FINAL ) then
                                            begin
                                                 iRangoFin:= oLookup.Valor;
                                            end
                                            else
                                            begin
                                                 iRangoIni:= oLookup.Valor;
                                            end;
                                       end;
                                  end;
                                  FRangosNomina[i]:= VarArrayOf( [ iRangoIni, iRangoFin, dFechaIni, dFechaFin]);
                             end;
                        end;
                  end;
             end;
        end;
        //Count:= FRangosNomina.Count;
        //FRangosNomina[i]:= VarArrayOf( [i, i, i]);
        FRangosNomina[0] := VarArrayOf( [1, 26, 29,39625,39651]);
        FRangosNomina[1] := VarArrayOf( [3, 12, 14,39625,39651]);
        FRangosNomina[2] := VarArrayOf( [6, 26, 29,39615,39643]);
   end;   }

begin
     inherited;
     lReemplazar:= ( RGAnterior.ItemIndex = K_REEMPLAZAR );

     with Descripciones do
     begin
          AddInteger('A�o', YearDefaultAct.ValorEntero );
          AddString('Mes', ObtieneElemento( lfMeses, MesDefault.Valor  - GetOffSet( lfMeses ) ) );
          AddString('Tipo de Pr�stamo Fonacot', NoTipoPrestamo.Caption );
          AddString('Tipo de Pr�stamo de Ajuste', TipoPrestamoAjus.Llave );
          AddBoolean('Dejar Ajuste Anterior', lReemplazar );
          AddBoolean('Incluir Incapacitados', IncluirIncapa.Checked  );
          AddBoolean('Filtro rango de n�minas', ( RGFiltro.ItemIndex = K_RANGO_NOM ) );
          AddBoolean('Entregar saldo ajuste a favor', lSaldarPresta.Checked ); //Se cambio titulo US: 12876 (Saldar pr�stamos a favor)
          AddBoolean('Saldar en bajas', lSaldarBajas.Checked );
          AddFloat('Tolerancia', Tolerancia.Valor );
          if RGAjuste.ItemIndex = 0 then
              AddString('Ajuste de cr�dito', 'Todos' )
          else if RGAjuste.ItemIndex = 1 then
          begin
            AddString('Ajuste de cr�dito', 'Saldo a favor' );

            if ckProvision.Checked then
                AddString('Provisi�n de vacaciones', 'Si' )
            else
                AddString('Provisi�n de vacaciones', 'Todos' )
          end
          else
            AddString('Ajuste de cr�dito', 'Saldo en contra' )
     end;
    // PreparaArreglo;

     with ParameterList do
     begin
          AddInteger('Year', YearDefaultAct.ValorEntero );
          AddInteger('Mes', MesDefault.Valor );
          AddString('TPrestamo', NoTipoPrestamo.Caption );
          AddString('TPrestamoAjus', TipoPrestamoAjus.Llave );
          AddBoolean('AjusAnt', lReemplazar );
          AddBoolean('Incapacitados', IncluirIncapa.Checked  );
          AddBoolean('FiltraTipNom',( RGFiltro.ItemIndex = K_RANGO_NOM ) );
          AddBoolean('SaldarPresta', lSaldarPresta.Checked );
          AddBoolean('SaldarBajas', lSaldarBajas.Checked );
          // AP(11/10/2007): Se Agreg� �ste par�metro para ahorrarnos el trabajo de ir al servidor e investigar el dato
          AddInteger('NoConcepto', dmTablas.GetConcepto( NoTipoPrestamo.Caption,TRUE,TRUE ) );
          AddFloat('Tolerancia', Tolerancia.Valor );
          AddVariant('RangosNomina', FRangosNomina );

          if GetFiltraporNom then
             AddInteger('MaxTiposNom', FPanelesVisible )
          else
              AddInteger('MaxTiposNom', FArregloPeriodo.Count  );

          if RGAjuste.ItemIndex = 1 then
            if ckProvision.Checked then
                AddInteger('Provision', 1 ) //Saldo a Favor: Provision
            else
                AddInteger('Provision', 2 ) //Saldo a Favor: Todos
          else if RGAjuste.ItemIndex = 2 then
              AddInteger('Provision', 3 ) //Saldo en contra
          else
              AddInteger('Provision', 0 ) //Todos

     end;
end;


function TWizNomAjusteRetFonacot_DevEx.GetFiltraPorNom: Boolean;
begin
     Result := ( RGFiltro.ItemIndex = K_RANGO_NOM );
end;

procedure TWizNomAjusteRetFonacot_DevEx.SetControlesRNominas;
    const
         K_DOS_PUNTOS = ':';
         K_WIDTH_MAX = 120;
         K_PANEL_TOP = 1234567;
    function DinamicoPanelPeriodos( iNumeroPanel: Integer; componentOwner: TComponent; componentParent: TWinControl ):TPanel;
    var
       pnlTiposPeriodos : TPanel;
    begin
         try
           pnlTiposPeriodos := TPanel.Create( componentOwner );
           pnlTiposPeriodos.Parent := componentParent;
           pnlTiposPeriodos.Visible := False;
           pnlTiposPeriodos.Top := K_PANEL_TOP;
           pnlTiposPeriodos.Align := alTop;
           pnlTiposPeriodos.Left := 2;
           pnlTiposPeriodos.Width := 496;
           pnlTiposPeriodos.Height := 30;
           pnlTiposPeriodos.Tag := iNumeroPanel;
           pnlTiposPeriodos.TabOrder := iNumeroPanel;
           Result := pnlTiposPeriodos;
           except on e:exception do
           begin
                Result := TPanel.Create( Nil );
           end;
         end;
    end;

    function DinamicoLabelTextoPeriodo( iNumeroPanel: Integer; sTextoTipoPeriodo: String; lblMaxWidth: Integer; componentOwner: TComponent; componentParent: TWinControl; iLeftLookUpIni: Integer ): TLabel;
    var
       oLabelIni : TLabel;
    begin
         try
           oLabelIni := TLabel.Create( componentOwner );
           oLabelIni.Parent := componentParent;
           oLabelIni.Left := iLeftLookUpIni - Canvas.TextWidth( sTextoTipoPeriodo + K_DOS_PUNTOS ) - 7;
           oLabelIni.Top := 8;
           oLabelIni.Width := lblMaxWidth;
           oLabelIni.Height := 13;
           oLabelIni.Caption := sTextoTipoPeriodo + K_DOS_PUNTOS;
           oLabelIni.Transparent := True;
           Result := oLabelIni;
           except on e:exception do
           begin
                Result := TLabel.Create( Nil );
           end;
         end;
    end;

    procedure DinamicoLabelTextoFin( iNumeroPanel: Integer; sTexto: String; componentOwner: TComponent; componentParent: TWinControl );
    var
       oLabelFin : TLabel;
    begin
         try
           oLabelFin := TLabel.Create( componentOwner );
           oLabelFin.Parent := componentParent;
           oLabelFin.Left := 292;
           oLabelFin.Top := 8;
           oLabelFin.Width := 6;
           oLabelFin.Height := 13;
           oLabelFin.Caption := sTexto;
           oLabelFin.Transparent := True;
           except on e:exception do
           begin
                ZetaDialogo.ZError( Caption,  e.Message, 0 );
           end;
         end;
    end;

    function DinamicoLookUpIni( iNumeroPanel: Integer; componentOwner: TComponent; componentParent: TWinControl ): TZetaKeyLookup_DevEx;
    var
       PeriodoIni : TZetaKeyLookup_DevEx;
    begin
         try
           PeriodoIni := TZetaKeyLookup_DevEx.Create( componentOwner );
           PeriodoIni.Parent := componentParent;
           PeriodoIni.Tag := 0;
           PeriodoIni.Left := 196;
           PeriodoIni.Top := 4;
           PeriodoIni.Width := 85;
           PeriodoIni.Height := 21;
           PeriodoIni.LookupDataset := dmCatalogos.cdsPeriodoOtro;
           PeriodoIni.EditarSoloActivos := False;
           PeriodoIni.IgnorarConfidencialidad := False;
           PeriodoIni.TabOrder := 0;
           PeriodoIni.TabStop := True;
           PeriodoIni.WidthLlave := 60;
           Result := PeriodoIni;
           except on e:exception do
           begin
                Result := TZetaKeyLookup_DevEx.Create( Nil );
           end;
         end;
    end;

    procedure DinamicoLookUpFin( iNumeroPanel: Integer; componentOwner: TComponent; componentParent: TWinControl );
    var
       PeriodoFin : TZetaKeyLookup_DevEx;
    begin
         try
           PeriodoFin := TZetaKeyLookup_DevEx.Create( componentOwner );
           PeriodoFin.Parent := componentParent;
           PeriodoFin.Tag := -1;
           PeriodoFin.Left := 308;
           PeriodoFin.Top := 4;
           PeriodoFin.Width := 85;
           PeriodoFin.Height := 21;
           PeriodoFin.LookupDataset := dmCatalogos.cdsPeriodoOtro;
           PeriodoFin.EditarSoloActivos := False;
           PeriodoFin.IgnorarConfidencialidad := False;
           PeriodoFin.TabOrder := 1;
           PeriodoFin.TabStop := True;
           PeriodoFin.WidthLlave := 60;
           except on e:exception do
           begin
                ZetaDialogo.ZError( Caption,  e.Message, 0 );
           end;
         end;
    end;

    function MaxWidthTextoTipoPeriodo: Integer;
    var
       lblMaxWidth : Integer;
       I : Integer;
    begin
         lblMaxWidth := 0;
         for I := 0 to FArregloPeriodo.Count-1 do
         begin
              if (Canvas.TextWidth(ObtieneElemento( lfTipoPeriodo, I ))>lblMaxWidth) then
                 lblMaxWidth :=  Canvas.TextWidth(ObtieneElemento( lfTipoPeriodo, I ));
         end;
         if lblMaxWidth > K_WIDTH_MAX then
            lblMaxWidth := K_WIDTH_MAX;
        Result := lblMaxWidth;
    end;

    procedure AsignaLookupsRNominas;
    var
       i, c, iPeriodoIni, iPeriodoFinal: Integer;
       oPanel: TPanel;
       oLookup: TZetaKeyLookup_DevEx;
       oLookupIni: TZetaKeyLookup_DevEx;
       oLabelIni : TLabel;
    const
         K_FILTRO = '( PE_TIPO = %d )';
    begin
         with GBRangosNominas do
         begin
              if FPanelesCreados then
              begin
                  for i:= 0 to FArregloPeriodo.Count-1 do
                  begin
                       if Panels[i] is TPanel then
                          FreeAndNil( Panels[i] );
                  end;
                  FPanelesCreados := False;
                  SetLength(Panels, 0);
              end;
              if not FPanelesCreados then
              begin
                   //(@am): Crear paneles necesarios
                   SetLength(Panels, FArregloPeriodo.Count);
                   for i := 0 to FArregloPeriodo.Count-1 do
                   begin
                        if dmCatalogos.cdsPeriodoOtro.Locate( 'PE_TIPO', i, [] )  then
                        begin
                             Panels[i] := DinamicoPanelPeriodos( i, Self, GBRangosNominas );
                             if not ( Panels[i].Visible )  then
                             begin
                                  oLookupIni := DinamicoLookUpIni( i, Self, Panels[i] );
                                  DinamicoLabelTextoFin( i, 'a', Self, Panels[i] );
                                  DinamicoLookUpFin( i, Self, Panels[i] );
                                  oLabelIni := DinamicoLabelTextoPeriodo( i, ObtieneElemento( lfTipoPeriodo, i ), MaxWidthTextoTipoPeriodo, Self, Panels[i], oLookupIni.left );
                                  AjustarLabelLeft(oLabelIni, i);
                             end;
                        end
                        else
                            Panels[i] := Nil;
                   end;
                   FPanelesCreados := TRUE;
              end;
         end;
         //(@m): Asociar informacion a controles de cada panel
         with GBRangosNominas do
         begin
              for i:= 0 to ControlCount - 1 do
              begin
                    if ( Controls[i] is TPanel ) then
                    begin
                         oPanel:= TPanel( Controls[i] );
                         oPanel.Align:= alTop;
                         oPanel.Visible:= ( dmCatalogos.cdsPeriodoOtro.Locate( 'PE_TIPO', oPanel.Tag, [] )  );
                         if ( oPanel.Visible ) then
                         begin
                              Inc( FPanelesVisible );
                              for c:= 0 to oPanel.ControlCount - 1 do
                              begin
                                   if ( oPanel.Controls[c] is TZetaKeyLookup_DevEx ) then
                                   begin
                                        oLookup:= TZetaKeyLookup_DevEx( oPanel.Controls[c] );
                                        TZetaKeyLookup_DevEx( TPanel(Controls[i]).Controls[c] ).Filtro:= Format( K_FILTRO, [ oPanel.Tag ] );
                                        dmCatalogos.GetPeriodoPorMes( eTipoPeriodo( oPanel.Tag ), MesDefault.Valor, iPeriodoIni, iPeriodoFinal );
                                        if  ( oLookup.Tag = K_FINAL ) then
                                        begin
                                             TZetaKeyLookup_DevEx( TPanel(Controls[i]).Controls[c]).Valor:= iPeriodoFinal;
                                        end
                                        else
                                        begin
                                              TZetaKeyLookup_DevEx( TPanel(Controls[i]).Controls[c] ).Valor:= iPeriodoIni;
                                        end;
                                   end;
                              end;
                         end;
                    end;
              end;
         end;
    end;
begin
     inherited;
     FPanelesVisible:= 0;
     SetFiltroPeriodo;
     AsignaLookupsRNominas;
     FMesAnterior:= MesDefault.Valor;
end;

procedure TWizNomAjusteRetFonacot_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control dependiendo del tipo de proceso
     if Wizard.EsPaginaActual( Parametros ) then
        ParametrosControl := YearDefaultAct
     else
         ParametrosControl := nil;
     inherited;
end;

end.
