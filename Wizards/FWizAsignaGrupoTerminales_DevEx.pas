unit FWizAsignaGrupoTerminales_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, FWizAsisBaseFiltro, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo,
  ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl, FWizSistBaseFiltro,
  dxSkinsCore;

type
  TWizAsignaGrupoTerminales_DevEx = class(TWizsistBaseFiltro)
    Label1: TLabel;
    luGrupo: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;    
  end;

var
  WizAsignaGrupoTerminales_DevEx: TWizAsignaGrupoTerminales_DevEx;

implementation

uses dSistema,
     dProcesos,
     FEmpBiometricoGridSelect_devEx,
     ZBaseSelectGrid_DevEx,
     dCliente,
     ZetaCommonClasses,
     ZetaCommonTools;

{$R *.dfm}

procedure TWizAsignaGrupoTerminales_DevEx.CargaListaVerificacion;
begin
     inherited;
     dmProcesos.EmpleadosConBiometricosGetLista( ParameterList );
end;

procedure TWizAsignaGrupoTerminales_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddString( 'Grupo', luGrupo.Llave );
          AddString( 'Empresa', dmCliente.Compania );
     end;
     with Descripciones do
     begin
          AddString( 'Grupo de terminales', luGrupo.Llave );
     end;
end;

function TWizAsignaGrupoTerminales_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AsignaGrupoTerminales( ParameterList, Verificacion );
end;

procedure TWizAsignaGrupoTerminales_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_PROCESOS_ASIGGPOTERMINALES;
     with dmSistema do
     begin
          cdsListaGrupos.Conectar;
          luGrupo.LookUpDataSet := cdsListaGrupos;
     end;
end;

function TWizAsignaGrupoTerminales_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpBiometricoGridSelect_devEx );
end;

procedure TWizAsignaGrupoTerminales_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.Adelante then
     begin
          if( WizardControl.ActivePage = Parametros )then
          begin
               if StrVacio( luGrupo.Llave ) then
                 CanMove := Error( '� Error ! Es necesario especificar un grupo de terminales', luGrupo )
          end;
     end;
end;

procedure TWizAsignaGrupoTerminales_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetFocusedControl( self.luGrupo )
end;

end.
