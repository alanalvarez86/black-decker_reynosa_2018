unit FWizAsistIntercambioFestivo;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseWizardFiltro, ComCtrls, ZetaKeyLookup, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, Mask, ZetaFecha;

type
  TWizAsistIntercambioFestivo = class(TBaseWizardFiltro)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    FechaNueva: TZetaFecha;
    FechaOriginal: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
  private
    { Private declarations }
  protected
    Procedure CargaParametros; override;
    procedure CargaListaVerificacion; override;
    function Verificar: Boolean;  override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizAsistIntercambioFestivo: TWizAsistIntercambioFestivo;

implementation

{$R *.dfm}

Uses DCliente,
     DProcesos,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     ZBaseSelectGrid,
     FEmpleadoGridSelect;

procedure TWizAsistIntercambioFestivo.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := FechaOriginal;
     with dmCliente do
     begin
          FechaOriginal.Valor := FechaAsistencia;
          FechaNueva.Valor := FechaAsistencia;
     end;
     HelpContext:= H_ASIS_WIZ_INTERCAMBIO_FESTIVO; //act. HC
end;

Procedure TWizAsistIntercambioFestivo.CargaParametros;
begin
     with Descripciones do
     begin
          Clear;
          AddDate('Fecha Original', FechaOriginal.Valor);
          AddDate('Fecha Nueva', FechaNueva.Valor);
     end;
     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate('FechaOriginal', FechaOriginal.Valor);
          AddDate('FechaNueva', FechaNueva.Valor);
          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
     end;
end;

procedure TWizAsistIntercambioFestivo.CargaListaVerificacion;
begin
     dmProcesos.IntercambioFestivoGetLista( ParameterList );
end;

function TWizAsistIntercambioFestivo.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect );
end;

function TWizAsistIntercambioFestivo.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.IntercambioFestivo( ParameterList, Verificacion );
end;

procedure TWizAsistIntercambioFestivo.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
        if ( FechaOriginal.Valor = FechaNueva.Valor ) then
           CanMove := Error('Las fechas original y nueva deben ser diferentes', FechaNueva);
        if CanMove then
           CanMove := dmCliente.PuedeCambiarTarjetaDlg(FechaNueva.Valor,FechaNueva.Valor);
     end;
end;

end.
