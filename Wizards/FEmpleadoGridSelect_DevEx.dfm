inherited EmpleadoGridSelect_DevEx: TEmpleadoGridSelect_DevEx
  Left = 810
  Top = 105
  Width = 506
  HorzScrollBar.Range = 490
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Empleados Deseados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 490
    inherited OK_DevEx: TcxButton
      Left = 327
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 405
    end
  end
  inherited PanelSuperior: TPanel
    Width = 490
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 490
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 65
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 65
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
