unit FWizNomPagarRepartoAhorro_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Mask,
     Dialogs, Db, ComCtrls, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons,
     ZetaDBGrid,
     ZetaEdit,
     ZetaWizard,
     ZetaNumero,
     ZetaDBTextBox,
     FWizNomBase_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomPagarRepartoAhorro_DevEx = class(TWizNomBase_DevEx)
    PanelParametros: TPanel;
    YearLBL: TLabel;
    Year: TZetaNumero;
    ConceptoLBL: TLabel;
    Concepto: TZetaKeyLookup_DevEx;
    TipoAhorro: TZetaKeyLookup_DevEx;
    TipoAhorroLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomPagarRepartoAhorro_DevEx: TWizNomPagarRepartoAhorro_DevEx;

implementation

uses ZetaDialogo,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZGlobalTress,
     ZBaseSelectGrid_DevEx,
     FEmpleadoGridSelect_DevEx,
     DCliente,
     DProcesos,
     DTablas,
     DCatalogos;

{$R *.DFM}

procedure TWizNomPagarRepartoAhorro_DevEx.FormCreate(Sender: TObject);
begin
     TipoAhorro.Tag := K_GLOBAL_DEF_REPARTIR_AHORROS_TIPO;
     inherited;
     with dmCliente do
     begin
          Self.Year.Valor := YearDefault;
          {
          iNumeroNomina.Caption := IntToStr( PeriodoNumero );
          iTipoNomina.Caption := ObtieneElemento(lfTipoPeriodo, Ord( PeriodoTipo ) );
          sDescripcion.Caption := GetPeriodoDescripcion;
          sStatusNomina.Caption := ObtieneElemento( lfStatusPeriodo, Ord(GetDatosPeriodoActivo.Status));
          iMesNomina.Caption := ObtieneElemento( lfMeses, GetDatosPeriodoActivo.Mes - 1);
          FechaInicial.Caption := DateToStr(GetDatosPeriodoActivo.Inicio);
          FechaFinal.Caption := DateToStr(GetDatosPeriodoActivo.Fin);
          }
     end;
     HelpContext := H30357_Pagar_ahorros;
     ParametrosControl := Year;
     Concepto.LookupDataset := dmCatalogos.cdsConceptos;
     TipoAhorro.LookupDataset := dmTablas.cdsTAhorro;
end;

procedure TWizNomPagarRepartoAhorro_DevEx.FormShow(Sender: TObject);
begin
     dmTablas.cdsTAhorro.Conectar;
     dmCatalogos.cdsConceptos.Conectar;
     inherited;
end;

procedure TWizNomPagarRepartoAhorro_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
ParametrosControl := nil;
     inherited;
     if CanMove then
     begin
          with Wizard do
          begin
               if EsPaginaActual( Parametros ) and Adelante then
               begin
                    if ( Concepto.Valor = 0 ) then
                    begin
                         ZetaDialogo.zError( Caption, '� Concepto De N�mina No Fu� Especificado !', 0 );
                         ActiveControl := Concepto;
                         CanMove := False;
                    end
               end;
          end;
     end;
end;

procedure TWizNomPagarRepartoAhorro_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'YearAhorro', Year.ValorEntero );
          AddString( 'TipoAhorro', TipoAhorro.Llave );
          AddInteger( 'Concepto', Concepto.Valor );
     end;
     with Descripciones do
     begin
          AddInteger( 'A�o', Year.ValorEntero );
          AddString( 'Tipo de ahorro', GetDescripLlave( TipoAhorro ) );
          AddString( 'Concepto', GetDescripLlave( Concepto) );
     end;
end;

procedure TWizNomPagarRepartoAhorro_DevEx.CargaListaVerificacion;
begin
     dmProcesos.PagarRepartoAhorroGetLista( ParameterList );
end;

function TWizNomPagarRepartoAhorro_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

function TWizNomPagarRepartoAhorro_DevEx.EjecutarWizard: boolean;
begin
     Result := dmProcesos.PagarRepartoAhorro( ParameterList, Verificacion );
end;

procedure TWizNomPagarRepartoAhorro_DevEx.WizardAfterMove(Sender: TObject);
begin
  inherited;
   if Wizard.EsPaginaActual(Parametros) then
      ParametrosControl := Year;
end;

end.



