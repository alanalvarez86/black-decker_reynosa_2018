inherited ImportarTablaCSVGridShow_DevEx: TImportarTablaCSVGridShow_DevEx
  Left = 313
  Top = 147
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Lista de Errores Detectados'
  ClientHeight = 213
  ClientWidth = 1138
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 177
    Width = 1138
    inherited OK_DevEx: TcxButton
      Left = 964
      Width = 85
      Hint = 'Continuar'
      Caption = 'C&ontinuar'
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 1053
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 1138
    Height = 177
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 525168
  end
  inherited DataSource: TDataSource
    Left = 920
    Top = 0
  end
end
