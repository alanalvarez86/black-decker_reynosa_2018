unit FWizEmpImportarImagenes_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  FileCtrl, jpeg, Grids, DBGrids, ZetaClientDataSet, DB, ZetaCommonClasses,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, Menus, cxButtons, cxClasses,
  cxShellBrowserDialog;

type
  TWizEmpImportarImagenes_DevEx = class(TcxBaseWizard)
    DataSource: TDataSource;
    Observaciones: TEdit;
    lblObservaciones: TLabel;
    TipoImagen: TEdit;
    lblTipoImagen: TLabel;
    Filtro: TFilterComboBox;
    lblTipoArchivos: TLabel;
    Directorio: TEdit;
    DirectorioLBL: TLabel;
    ImportFileSeek: TcxButton;
    cxShellBrowserDialog1: TcxShellBrowserDialog;
    procedure ImportFileSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FCtr: Integer;
    function GetImportDirectory: String;
    function GetImageFilter: String;
    function LeerDirectorio: Boolean;    
  Protected
    { Protected declarations }
    procedure CargaParametros; override;
    function Verificar: Boolean;
    procedure CargaListaVerificacion;
    function EjecutarWizard: Boolean; override;    
  public
    { Public declarations }
    procedure ShowError( const sMensaje: String; Error: Exception );
  end;

var
  WizEmpImportarImagenes_DevEx: TWizEmpImportarImagenes_DevEx;
  ParametrosGrid: TzetaParams;  

implementation

uses //ZetaMigrar,
     ZetaDialogo,
     ZetaCommonTools,
     DRecursos,
     ZBaseSelectGrid_DevEx,
     FImportarImagenGridSelect_DevEx,
     DProcesos;

{$R *.dfm}

function TWizEmpImportarImagenes_DevEx.Verificar: Boolean;
begin
     dmProcesos.cdsDataset.First;
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TImportarImagenGridSelect_DevEx, FALSE );
end;

procedure TWizEmpImportarImagenes_DevEx.CargaListaVerificacion;
begin
     with dmRecursos do
     begin
           CargaGridImagenesImportar(ParameterList, GetImageFilter, TRUE );
           dmProcesos.cdsDataset := cdsImagenes;
           dmProcesos.cdsDataSet.MergeChangeLog;
     end;
end;

function TWizEmpImportarImagenes_DevEx.LeerDirectorio: Boolean;
begin
     Application.ProcessMessages;
     CargaParametros;
     CargaListaVerificacion;
     Result := Verificar;
end;

procedure TWizEmpImportarImagenes_DevEx.CargaParametros;
begin
     with ParameterList do
     begin
          AddString( 'Directorio', Directorio.Text);
          AddString( 'Filtro', Filtro.Text);
          AddString( 'Tipo', TipoImagen.Text);
          AddMemo( 'Observaciones', Observaciones.Text );
     end;
     with Descripciones do
     begin
          AddString( 'Directorio', Directorio.Text);
          AddString( 'Filtro', Filtro.Text);
          AddString( 'Tipo', TipoImagen.Text);
          AddMemo( 'Observaciones', Observaciones.Text );
     end;
end;

procedure TWizEmpImportarImagenes_DevEx.ShowError( const sMensaje: String; Error: Exception );
begin
     zExcepcion( Caption, sMensaje, Error, 0 );
end;

function TWizEmpImportarImagenes_DevEx.GetImportDirectory: String;
begin
     Result := Directorio.Text;
end;

function TWizEmpImportarImagenes_DevEx.GetImageFilter: String;
begin
     Result := ZetaCommonTools.SetFileNamePath( GetImportDirectory, Filtro.Mask );
end;

procedure TWizEmpImportarImagenes_DevEx.ImportFileSeekClick(Sender: TObject);
var
   sDirectory: String;
begin
     inherited;
     with Directorio do
     begin
          sDirectory := Text;
          cxShellBrowserDialog1.execute;
          //if SelectDirectory( sDirectory, [], 0 ) then
             Text := cxShellBrowserDialog1.Path;
     end;
end;

procedure TWizEmpImportarImagenes_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;

function HayArchivos( const sFileSpec: String ): Boolean;
var
   Datos: TSearchRec;
begin
     FCtr := 0;
     if ( FindFirst( sFileSpec, faAnyFile, Datos ) = 0 ) then
     begin
          Inc( FCtr );
          while ( FindNext( Datos ) = 0 ) do
          begin
               Inc( FCtr );
          end;
          FindClose( Datos );
     end;
     Result := ( FCtr > 0 );
end;

begin
     inherited;
     if CanMove then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if Wizard.Adelante then
             begin
                  if ( WizardControl.ActivePage = Parametros ) then
                  begin
                       if not DirectoryExists( GetImportDirectory ) then
                       begin
                            ZetaDialogo.zWarning( Caption, 'Directorio de im�genes no existe', 0, mbOK );
                            ActiveControl := Directorio;
                            CanMove := False;
                       end
                       else
                       begin
                            if not HayArchivos( GetImageFilter ) then
                            begin
                                 ZetaDialogo.zWarning( Caption, 'No se encontraron im�genes en el folder especificado', 0, mbOK );
                                 ActiveControl := Directorio;
                                 CanMove := False;
                            end
                            else
                            begin
                                 CanMove := LeerDirectorio;
                            end;
                       end;
                  end
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TWizEmpImportarImagenes_DevEx.FormCreate(Sender: TObject);
begin
     FCtr := 0;
     HelpContext:= H_EMP_IMPORTAR_IMAGENES;
     inherited;
end;

function TWizEmpImportarImagenes_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarImagenes( ParameterList );
end;

end.
