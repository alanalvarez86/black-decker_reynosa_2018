inherited WizNomPagarAguinaldo_DevEx: TWizNomPagarAguinaldo_DevEx
  Left = 328
  Top = 185
  Caption = 'Pagar Aguinaldos'
  ClientHeight = 423
  ClientWidth = 436
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 436
    Height = 423
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso aplica los montos del c'#225'lculo de aguinaldos en una ' +
        'n'#243'mina especial.'
      Header.Title = 'Pagar Aguinaldos'
      object YearLBL: TLabel [0]
        Left = 40
        Top = 33
        Width = 22
        Height = 13
        Caption = '&A'#241'o:'
        FocusControl = Year
      end
      inherited GroupBox1: TGroupBox
        Top = 66
        Width = 407
      end
      object Year: TZetaNumero
        Left = 65
        Top = 29
        Width = 57
        Height = 21
        Mascara = mnDias
        TabOrder = 1
        Text = '0'
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 188
        Width = 414
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 414
        inherited Advertencia: TcxLabel
          Caption = 
            'Al Aplicar este proceso se mostrar'#225'n la lista de excepciones de ' +
            'd'#237'as que se exportar'#225'n a la n'#243'mina especial.'
          Style.IsFontAssigned = True
          Width = 346
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 8
        Top = 135
      end
      inherited sFiltroLBL: TLabel
        Left = 33
        Top = 164
      end
      inherited Seleccionar: TcxButton
        Left = 136
        Top = 252
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 60
        Top = 132
      end
      inherited sFiltro: TcxMemo
        Left = 60
        Top = 163
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 60
        Top = 2
      end
      inherited bAjusteISR: TcxButton
        Left = 375
        Top = 164
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
