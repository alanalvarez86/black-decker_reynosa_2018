inherited WizEmpRenumeraEmpleados_DevEx: TWizEmpRenumeraEmpleados_DevEx
  Left = 256
  Top = 294
  Caption = 'Renumeraci'#243'n de Empleados'
  ClientHeight = 454
  ClientWidth = 470
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel [0]
    Left = 49
    Top = 13
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Arc&hivo:'
  end
  object Label6: TLabel [1]
    Left = 17
    Top = 39
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = '&Formato ASCII:'
  end
  object Label7: TLabel [2]
    Left = 14
    Top = 65
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = '&Observaciones:'
  end
  object ArchivoSeek1: TcxButton [3]
    Left = 384
    Top = 7
    Width = 25
    Height = 25
    Hint = 'Buscar Archivo de Importaci'#243'n'
    TabOrder = 1
    OptionsImage.Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333333333333333333FFF333333333333000333333333
      3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
      3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
      0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
      BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
      33337777773FF733333333333300033333333333337773333333333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    OptionsImage.NumGlyphs = 2
  end
  inherited WizardControl: TdxWizardControl
    Width = 470
    Height = 454
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Mediante este proceso puede renumerar a los empleados, afectando' +
        ' a todos los registros relacionados con el empleado.'
      Header.Title = 'Renumerar empleados'
      object gbArchivo: TcxGroupBox
        Left = 0
        Top = 8
        Align = alCustom
        Caption = '                                   '
        TabOrder = 5
        Height = 105
        Width = 448
        object Label1: TLabel
          Left = 43
          Top = 34
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'Archi&vo:'
          FocusControl = edArchivo
          Transparent = True
        end
        object Label2: TLabel
          Left = 11
          Top = 60
          Width = 71
          Height = 13
          Alignment = taRightJustify
          Caption = '&Formato ASCII:'
          FocusControl = Formato
          Transparent = True
        end
        object ArchivoSeek: TcxButton
          Left = 376
          Top = 30
          Width = 21
          Height = 21
          Hint = 'Buscar Archivo de Importaci'#243'n'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = ArchivoSeekClick
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000050D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF51D294FF53D395FF53D394FF51D2
            94FF55D396FF55D396FF55D396FF51D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FFFBFEFDFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFFFFFEFF
            FFFFFEFFFFFFFEFFFFFFFEFFFFFFFEFFFEFFFEFFFEFFFFFFFFFFFDFFFEFF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FFFCFEFDFFFEFFFEFFFFFF
            FFFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFFFFFEFFFEFFFDFF
            FEFFFFFFFFFFFEFEFEFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FFFBFDFDFFFFFFFFFF5ED69CFF56D497FF5AD499FF59D599FF5BD599FF59D4
            99FF59D499FF57D498FF58D498FFFBFCFBFFFBFDFDFF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFFEFFFEFFFFFFFFFF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFF5FCF9FFFAFE
            FCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFA4E8C7FFA9E9
            CAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFA5E8C7FF99E5C0FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF58D597FFFCFE
            FDFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF58D597FFFCFEFDFF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FFFEFFFEFF5CD69BFF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF58D498FFC7F1DDFF62D7
            9EFFFFFFFFFF5DD59DFFBBEDD5FF52D294FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFDBF6E9FFFEFEFEFFFDFDFDFFFAFEFCFFF1FBF7FF53D395FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF60D69DFFD8F5E7FFFFFFFFFFDDF6
            EAFF53D395FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFB7ECD2FF58D498FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
          OptionsImage.Margin = 1
        end
        object edArchivo: TEdit
          Left = 84
          Top = 30
          Width = 289
          Height = 21
          TabOrder = 0
        end
        object Formato: TZetaKeyCombo
          Left = 84
          Top = 56
          Width = 185
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfFormatoASCII
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
      end
      object gbDelta: TcxGroupBox
        Left = 0
        Top = 128
        Align = alCustom
        Caption = '                                                      '
        TabOrder = 3
        Height = 113
        Width = 448
        object lblIncremento: TLabel
          Left = 35
          Top = 28
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Can&tidad:'
          FocusControl = edIncremento
          Transparent = True
        end
        object lblOperacion: TLabel
          Left = 28
          Top = 53
          Width = 52
          Height = 13
          Alignment = taRightJustify
          Caption = '&Operaci'#243'n:'
          FocusControl = cbOperacion
          Transparent = True
        end
        object edIncremento: TZetaNumero
          Left = 83
          Top = 24
          Width = 60
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 0
        end
        object ckBajasIncremento: TcxCheckBox
          Left = 18
          Top = 76
          Caption = 'Inclu'#237'r &Bajas:'
          Properties.Alignment = taRightJustify
          Properties.ValueChecked = 'S'#237
          Properties.ValueUnchecked = 'No'
          TabOrder = 2
          Transparent = True
          Width = 81
        end
        object cbOperacion: TZetaKeyCombo
          Left = 83
          Top = 50
          Width = 62
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfOperacionIncremento
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
      end
      object rbDelta: TcxRadioButton
        Left = 12
        Top = 128
        Width = 165
        Height = 17
        Caption = 'Incremento al C'#243'&digo Actual'
        Color = clBtnFace
        ParentColor = False
        TabOrder = 2
        OnClick = radioClick
        Transparent = True
      end
      object rbTexto: TcxRadioButton
        Left = 13
        Top = 10
        Width = 100
        Height = 17
        Caption = 'Arc&hivo ASCII'
        ParentColor = False
        TabOrder = 0
        OnClick = radioClick
        Transparent = True
      end
      object gbKardex: TcxGroupBox
        Left = 0
        Top = 256
        Align = alCustom
        Caption = '                                                          '
        TabOrder = 4
        Height = 57
        Width = 448
        object LblGenerales: TLabel
          Left = 16
          Top = 23
          Width = 121
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Registro &General:'
          FocusControl = RegGenerales
          Transparent = True
        end
        object RegGenerales: TZetaKeyLookup_DevEx
          Left = 140
          Top = 19
          Width = 285
          Height = 21
          Filtro = 'TB_SISTEMA<>'#39'S'#39
          LookupDataset = dmTablas.cdsMovKardex
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 75
        end
      end
      object ckbKardex: TcxCheckBox
        Left = 13
        Top = 255
        Caption = 'Inclu'#237'r movimiento de &kardex'
        ParentColor = False
        Properties.ValueChecked = 'S'#237
        Properties.ValueUnchecked = 'No'
        Style.Color = clBtnFace
        TabOrder = 1
        Transparent = True
        OnClick = ckbKardexClick
        Width = 172
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 219
        Width = 448
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 448
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 380
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 14
        Top = 145
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        Top = 168
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Top = 262
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        Top = 142
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Top = 168
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
        Top = 12
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 336
    Top = 66
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'dat'
    Filter = 
      'Archivos de Datos (*.dat)|*.dat|Archivos de Texto (*.txt)|*.txt|' +
      'Todos (*.*)|*.*'
    Options = [ofHideReadOnly]
    Title = 'Seleccione el Archivo a Importar'
    Left = 374
    Top = 66
  end
end
