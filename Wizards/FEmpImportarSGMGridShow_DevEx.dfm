inherited EmpImportarSGMGridShow_DevEx: TEmpImportarSGMGridShow_DevEx
  Left = 366
  Top = 352
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Lista de Errores Detectados'
  ClientWidth = 1013
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 1013
    inherited OK_DevEx: TcxButton
      Left = 845
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 924
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 1013
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object RENGLON: TcxGridDBColumn
        Caption = 'Rengl'#243'n'
        DataBinding.FieldName = 'RENGLON'
        MinWidth = 60
        Width = 80
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        MinWidth = 80
        Width = 80
      end
      object OPERACION: TcxGridDBColumn
        Caption = 'Operaci'#243'n'
        DataBinding.FieldName = 'OPERACION'
        MinWidth = 80
        Width = 80
      end
      object FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'FECHA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        MinWidth = 70
        Width = 75
      end
      object CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CODIGO'
        MinWidth = 70
        Width = 70
      end
      object ADICIONALES: TcxGridDBColumn
        Caption = 'Adicionales'
        DataBinding.FieldName = 'ADICIONALES'
        MinWidth = 80
        Width = 100
      end
      object COMENTARIO: TcxGridDBColumn
        Caption = 'Comentarios'
        DataBinding.FieldName = 'COMENTARIO'
        MinWidth = 85
        Width = 85
      end
      object NUM_ERROR: TcxGridDBColumn
        Caption = 'Errores'
        DataBinding.FieldName = 'NUM_ERROR'
        MinWidth = 70
        Width = 70
      end
      object DESC_ERROR: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Errores'
        DataBinding.FieldName = 'DESC_ERROR'
        MinWidth = 120
        Width = 450
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Top = 40
  end
end
