unit FEmpRenumeraGridShow_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridShow_DevEx, Db, StdCtrls, ExtCtrls,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons;

type
  TEmpRenumeraGridShow_DevEx = class(TBaseGridShow_DevEx)
    RENGLON: TcxGridDBColumn;
    CB_CODIGO_OLD: TcxGridDBColumn;
    CB_CODIGO_NEW: TcxGridDBColumn;
    NUM_ERROR: TcxGridDBColumn;
    DESC_ERROR: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpRenumeraGridShow_DevEx: TEmpRenumeraGridShow_DevEx;

implementation

{$R *.DFM}

end.
