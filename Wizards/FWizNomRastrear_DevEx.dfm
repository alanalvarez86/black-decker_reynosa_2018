inherited WizNomRastrear_DevEx: TWizNomRastrear_DevEx
  Left = 206
  Top = 173
  Caption = 'Rastrear C'#225'lculo'
  ClientHeight = 412
  ClientWidth = 487
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 487
    Height = 412
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que genera un archivo de texto con el detalle de los pas' +
        'os utilizados por Sistema TRESS en el c'#225'lculo de un concepto de ' +
        'n'#243'mina.'
      Header.Title = 'Rastrear c'#225'lculo'
      inherited GroupBox1: TGroupBox
        Left = 30
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 177
        Width = 465
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 465
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 397
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      PageVisible = False
      inherited sFiltro: TcxMemo
        Style.IsFontAssigned = True
      end
    end
    object Parametros2: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Complete los campos para indicar los par'#225'metros de rastreo.'
      Header.Title = 'Par'#225'metros para rastreo'
      object ArchivoLBL: TLabel
        Left = 117
        Top = 109
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Arc&hivo:'
        FocusControl = Archivo
        Transparent = True
      end
      object ConceptoLBL: TLabel
        Left = 55
        Top = 83
        Width = 101
        Height = 13
        Alignment = taRightJustify
        Caption = 'Concepto a &Rastrear:'
        FocusControl = Concepto
        Transparent = True
      end
      object EmpleadoLbl: TLabel
        Left = 106
        Top = 56
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'E&mpleado:'
        FocusControl = Empleado
        Transparent = True
      end
      object Empleado: TZetaKeyLookup_DevEx
        Left = 160
        Top = 52
        Width = 302
        Height = 21
        LookupDataset = dmCliente.cdsEmpleadoLookUp
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
      object Concepto: TZetaKeyLookup_DevEx
        Left = 160
        Top = 79
        Width = 302
        Height = 21
        Filtro = 'CO_NUMERO <= 999'
        LookupDataset = dmCatalogos.cdsConceptos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
      object Archivo: TEdit
        Left = 160
        Top = 105
        Width = 278
        Height = 21
        TabOrder = 2
      end
      object lNomParam: TCheckBox
        Left = 6
        Top = 130
        Width = 167
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Mostrar &Par'#225'metros de N'#243'mina:'
        TabOrder = 4
      end
      object ArchivoSeek: TcxButton
        Left = 439
        Top = 105
        Width = 21
        Height = 21
        Hint = 'Buscar Nombre de Archivo'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = ArchivoSeekClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A40500000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF8AE1B7FFB8EDD3FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF6EDAA6FFF7FDFAFFFFFF
          FFFF95E4BDFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFB8EDD3FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE6F9F0FF71DA
          A7FF7CDDAEFFD6F4E6FFF1FBF7FF8FE2BAFF53D395FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFECFAF3FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFBDEED6FFE9F9F1FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFE9F9
          F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFBDEE
          D6FFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FFCEF2E1FFD9F5E7FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FFE9F9F1FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF8CE2B8FFD0F3E2FFFFFFFFFF95E4BDFF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF6BD9A4FF92E3
          BCFF6BD9A4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFE9F9F1FF50D293FF50D293FF50D293FF50D2
          93FF60D69DFF79DDACFF9AE5C1FFD3F4E4FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFE9F9F1FF50D2
          93FF50D293FF50D293FF76DCABFFFCFEFDFFFFFFFFFFFAFEFCFF79DDACFF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFE9F9F1FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFFFCFE
          FDFF8AE1B7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFECFAF3FF50D293FF50D293FF50D2
          93FF92E3BCFFFFFFFFFF95E4BDFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFB8ED
          D3FFFCFEFDFFFFFFFFFFFFFFFFFFFFFFFFFF9DE6C2FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
        PaintStyle = bpsGlyph
      end
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'txt'
    Filter = 'Archivos Texto (*.txt)|*.txt|Todos (*.*)|*.*'
    Title = 'Escoja el Archivo de Resultados del Rastreo'
    Left = 203
    Top = 290
  end
end
