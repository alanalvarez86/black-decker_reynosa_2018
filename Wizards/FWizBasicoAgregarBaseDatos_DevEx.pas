unit FWizBasicoAgregarBaseDatos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,  ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  ZetaEdit, ZetaDialogo, DB, ADODB, Mask, DBClient, ZetaClientDataSet,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, cxRadioGroup, cxTextEdit, cxMemo;

type
  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
    end;

  TWizBasicoAgregarBaseDatos_DevEx = class(TcxBaseWizard)
    NuevaBD2: TdxWizardControlPage;
    Estructura: TdxWizardControlPage;
    txtDescripcion: TZetaEdit;
    txtCodigo: TZetaEdit;
    lblDescripcion: TLabel;
    lblCodigo: TLabel;
    lblUbicacion: TLabel;
    cbBasesDatos: TComboBox;
    txtBaseDatos: TEdit;
    lblSeleccionarBD: TLabel;
    lbNombreBD: TLabel;
    GBUsuariosMSSQL: TcxGroupBox;
    txtUsuario: TEdit;
    txtClave: TMaskEdit;
    lblClave: TLabel;
    lblUsuario: TLabel;
    rbSameUser: TcxRadioButton;
    rbOtroUser: TcxRadioButton;
    rbBDNueva: TcxRadioButton;
    rbBDExistente: TcxRadioButton;
    GrupoAvance: TcxGroupBox;
    cxMemoStatus: TcxMemo;
    cxMemoPasos: TcxMemo;
    lblDatoUbicacion: TcxLabel;
    procedure WizardAfterMove(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rbBDNuevaClick(Sender: TObject);
    procedure rbOtroUserClick(Sender: TObject);
    procedure rbSameUserClick(Sender: TObject);
    procedure rbBDExistenteClick(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    SC : TSQLConnection;
    function GetConnStr: widestring;
    property ConnStr : widestring read GetConnStr;
  public
    { Public declarations }
  protected
   { Protected declarations }
    procedure CargaParametros; override;
    procedure HabilitaControlesUsuario(lHabilita: Boolean);
    procedure LimpiaControlesUsuario; 
    function Warning(const sError: String; oControl: TWinControl): Boolean;
    function ProbarConexion(out sMensaje: WideString): Boolean;
    procedure SetStatus (lTerminado: Boolean);
    function ExisteBD: boolean;
  end;

Const
     K_TERMINADO='  Terminado';
     K_ERROR='  Error';
var
  WizBasicoAgregarBaseDatos_DevEx: TWizBasicoAgregarBaseDatos_DevEx;

implementation

uses DProcesos,
    DSistema,
    DBaseSistema,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaServerTools;

{$R *.dfm}


procedure TWizBasicoAgregarBaseDatos_DevEx.SetStatus (lTerminado: Boolean);
begin
     if lTerminado then
          cxMemoStatus.Lines.Append( K_TERMINADO)
     else
          cxMemoStatus.Lines.Append( K_ERROR);
end;

function TWizBasicoAgregarBaseDatos_DevEx.GetConnStr: widestring;
begin
  SC.ServerName := dmSistema.GetServer;
  SC.UserName := txtUsuario.Text;
  SC.Password := txtClave.Text;

  Result := 'Provider=SQLOLEDB.1;';
  Result := Result + 'Data Source=' + SC.ServerName + ';';
  if SC.DatabaseName <> '' then
    Result := Result + 'Initial Catalog=' + SC.DatabaseName + ';';

    Result := Result + 'uid=' + SC.UserName + ';';
    Result := Result + 'pwd=' + SC.Password + ';';

end;


procedure TWizBasicoAgregarBaseDatos_DevEx.CargaParametros;
begin
     inherited;
     {***DevEx(by am): Agregar las Desscripciones en la Implementacion***}
     {***DevEx(by am): Agregar los Parametros en la Implementacion***}
end;

procedure TWizBasicoAgregarBaseDatos_DevEx.WizardAfterMove(Sender: TObject);
var
   sUbicacion:String;
begin
  inherited;
     if Wizard.EsPaginaActual( NuevaBD2 ) then
     begin
          sUbicacion := format(' %s.%s ', [dmSistema.GetServer, txtBaseDatos.Text ]);
          lblDatoUbicacion.Caption :=  sUbicacion;
          //txtTamanoBDOrigen.Text :=  dmSistema.GetDBSize(ConnStr, cbEmpresas.Text);
          //txtArchivoLog.Text :=  dmSistema.GetDataLogSize(ConnStr, cbEmpresas.Text);
     end;
end;

procedure TWizBasicoAgregarBaseDatos_DevEx.FormCreate(Sender: TObject);
begin
          {***DevEx(by am): Hacer la Definicion de la secuencia de las paginas en la implementacion***}
          inherited;

          HelpContext := H_SIST_PROC_AGREGAR_BASE_DATOS; //DevEx(by am): Todos los Wizards que agregan BD comparten el mismo HelpContext
          with dmSistema do
          begin
               cdsEmpresasLookUp.Conectar;
          end;
end;

procedure TWizBasicoAgregarBaseDatos_DevEx.FormShow(Sender: TObject);
begin
  inherited;
     rbSameUser.Checked := TRUE;
     HabilitaControlesUsuario(FALSE);
     dmSistema.GetBasesDatos(dmSistema.GetServer, VACIO, VACIO);
end;

procedure TWizBasicoAgregarBaseDatos_DevEx.rbBDNuevaClick(Sender: TObject);
begin
  inherited;
     txtBaseDatos.Enabled := true;
     txtBaseDatos.SetFocus;
     cbBasesDatos.Enabled := false;
end;

procedure TWizBasicoAgregarBaseDatos_DevEx.rbSameUserClick(Sender: TObject);
begin
  inherited;
    LimpiaControlesUsuario;
    HabilitaControlesUsuario(FALSE);
end;

procedure TWizBasicoAgregarBaseDatos_DevEx.rbOtroUserClick(Sender: TObject);
begin
  inherited;
    HabilitaControlesUsuario(TRUE);
end;

procedure TWizBasicoAgregarBaseDatos_DevEx.HabilitaControlesUsuario(lHabilita: Boolean);
begin
    txtUsuario.Enabled := lHabilita;
    txtClave.Enabled := lHabilita;
    if  txtUsuario.Enabled then
         txtUsuario.SetFocus;
end;

procedure TWizBasicoAgregarBaseDatos_DevEx.LimpiaControlesUsuario;
begin
    txtUsuario.Text := VACIO;
    txtClave.Text := VACIO;
end;

procedure TWizBasicoAgregarBaseDatos_DevEx.WizardAlEjecutar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     if lOk then
         dmSistema.cdsSistBaseDatos.Refrescar;
end;

//Utilizado por WIZARDs: EMP, PRES, SEL, VIS, PRUEBAS
function TWizBasicoAgregarBaseDatos_DevEx.Warning( const sError: String; oControl: TWinControl ): Boolean;
begin
     ZetaDialogo.ZWarning( Caption, sError, 0, mbOK );
     oControl.SetFocus;
     Result := False;
end;


function TWizBasicoAgregarBaseDatos_DevEx.ProbarConexion(out sMensaje: WideString): Boolean;
var
   lOk : Boolean;
begin
     lOk:=FALSE;
     if rbOtroUser.Checked then
     begin
          if not( txtUsuario.Text = VACIO) then
          begin
               if not ( txtClave.Text = VACIO ) then
               begin
                    lOk := dmSistema.ConectarseServer( ConnStr, sMensaje)
               end
               else
               begin
                    lOk:=FALSE;
                    sMensaje := 'El campo Clave no puede quedar vac�o.';
               end;
          end
          else
          begin
               lOk:=FALSE;
               sMensaje := 'El campo Usuario no puede quedar vac�o.';
          end;
     end;

  Result := lOk;
end;


procedure TWizBasicoAgregarBaseDatos_DevEx.rbBDExistenteClick(
  Sender: TObject);
begin
     txtBaseDatos.Enabled := false;
     cbBasesDatos.Enabled := true;
     cbBasesDatos.setFocus;
end;


//Utilizado por WIZARDs: EMP, PRES, SEL, VIS, PRUEBAS no lo usa
function TWizBasicoAgregarBaseDatos_DevEx.ExisteBD: boolean;
var i: integer;
begin
     Result := FALSE;

     for i:=0 to cbBasesDatos.Items.Count do
     begin
          if UpperCase (txtBaseDatos.Text) = UpperCase (cbBasesDatos.Items.Strings[i]) then
             Result := TRUE;
     end
end;

end.
