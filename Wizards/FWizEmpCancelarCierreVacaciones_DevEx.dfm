inherited WizEmpCancelarCierreVacaciones_DevEx: TWizEmpCancelarCierreVacaciones_DevEx
  Left = 485
  Top = 280
  Caption = 'Cancelar cierre de vacaciones'
  ClientHeight = 441
  ClientWidth = 439
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 439
    Height = 441
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso cancela los movimientos de tipo Cierre que se reali' +
        'zaron en el per'#237'odo que se indique.'
      Header.Title = 'Cancelar cierre de vacaciones'
      object lblCanModifica: TLabel
        Left = 88
        Top = 177
        Width = 107
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cancelar modificaci'#243'n:'
        Transparent = True
      end
      object rbFechaCierre: TRadioGroup
        Left = 69
        Top = 53
        Width = 284
        Height = 105
        Caption = ' Fecha de cierre: '
        ItemIndex = 0
        Items.Strings = (
          '&Ultimo cierre de cada empleado'
          '&A esta fecha:')
        TabOrder = 0
        OnClick = rbFechaCierreClick
      end
      object zFechaCierre: TZetaFecha
        Left = 191
        Top = 120
        Width = 106
        Height = 22
        Cursor = crArrow
        Enabled = False
        TabOrder = 2
        Text = '29/abr/05'
        Valor = 38471.000000000000000000
      end
      object zcbModificacion: TZetaKeyCombo
        Left = 200
        Top = 173
        Width = 137
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 1
        ListaFija = lfCancelaMod
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 206
        Width = 417
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 417
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 349
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 14
      end
      inherited sFiltroLBL: TLabel
        Left = 39
      end
      inherited Seleccionar: TcxButton
        Left = 150
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
      end
      inherited bAjusteISR: TcxButton
        Left = 380
      end
    end
  end
end
