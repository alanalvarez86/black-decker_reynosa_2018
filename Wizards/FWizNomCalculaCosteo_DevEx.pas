unit FWizNomCalculaCosteo_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
     ZetaEdit, ZetaDBTextBox, ZetaKeyCombo, ZetaNumero,
     Mask, ZetaFecha, FWizNomBase_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
     cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl;

type
  TWizNomCalculaCosteo_DevEx = class(TWizNomBase_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizNomCalculaCosteo_DevEx: TWizNomCalculaCosteo_DevEx;

implementation

uses DProcesos,
     ZetaCommonClasses;

{$R *.DFM}

procedure TWizNomCalculaCosteo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_Calcula_Costeo_Transferencias;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

function TWizNomCalculaCosteo_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CalculaCosteo(ParameterList);
end;

procedure TWizNomCalculaCosteo_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Al aplicar el proceso se calcula el costeo de n�mina en proporci�n a las transferencias registradas en el periodo de n�mina.';
end;

end.
