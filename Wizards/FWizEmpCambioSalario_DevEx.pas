unit FWizEmpCambioSalario_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, ComCtrls, ExtCtrls,
     ZetaNumero, ZetaKeyCombo, ZetaFecha, ZetaEdit,
     FWizEmpBaseFiltro, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
     ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit,
     cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage,
     dxCustomWizardControl, dxWizardControl, cxCheckBox;

type
  TWizEmpCambioSalario_DevEx = class(TWizEmpBaseFiltro)
    dReferenciaLBL: TLabel;
    dReferencia: TZetaFecha;
    nTipoLBL: TLabel;
    iTipo: TZetaKeyCombo;
    rTasaLBL: TLabel;
    rTasa: TZetaNumero;
    rCantidadLBL: TLabel;
    rCantidad: TZetaNumero;
    sDescripcionLBL: TLabel;
    sDescripcion: TEdit;
    sObservaLBL: TLabel;
    sObserva: TcxMemo;
    lIncapacitados: TcxCheckBox;
    dIMSS: TZetaFecha;
    Label1: TLabel;

    procedure FormCreate(Sender: TObject);
    procedure iTipoChange(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure dReferenciaValidDate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function GetPorPorcentaje: Boolean;
    procedure SetControls;
  protected
    { Protected declarations }
    property PorPorcentaje: Boolean read GetPorPorcentaje;
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpCambioSalario_DevEx: TWizEmpCambioSalario_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     ZBaseSelectGrid_DevEx,
     FEmpCambioSalarioGridSelect_DevEx, ZcxWizardBasico, ZcxBaseWizard;

{$R *.DFM}

procedure TWizEmpCambioSalario_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := dReferencia;
     dReferencia.Valor := dmCliente.FechaDefault;
     iTipo.ItemIndex := Ord( toPorcentaje );
     sDescripcion.Text := 'Modificaci�n Global de Salario';
     SetControls;
     HelpContext:= H10163_Cambio_salario_global;
end;

procedure TWizEmpCambioSalario_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'Fecha', dReferencia.Valor );
          AddFloat( 'Tasa', rTasa.Valor );
          AddFloat( 'Cantidad', rCantidad.Valor );
          AddString( 'Descripcion', sDescripcion.Text );
          AddMemo( 'Observaciones', sObserva.Text );
          if PorPorcentaje then
             AddInteger( 'TipoAumento', Ord( toPorcentaje ) )
          else
             AddInteger( 'TipoAumento', Ord( toCantidad ) );
          AddBoolean( 'NoIncluirIncapacitados', lIncapacitados.Checked );
          AddDate( 'Fecha2', dIMSS.Valor );
     end;

     with Descripciones do
     begin
          AddDate( 'Fecha de cambio', dReferencia.Valor );
          AddDate( 'Fecha de aviso a IMSS', dImss.Valor );
          if PorPorcentaje then
          begin
               AddString( 'Tipo de aumento', ObtieneElemento( lfTipoOtrasPer, Ord( toPorcentaje ) ) );
               AddFloat( 'Tasa', rTasa.Valor );
          end
          else
          begin
               AddString( 'Tipo de aumento', ObtieneElemento( lfTipoOtrasPer,Ord( toCantidad ) ) );
               AddFloat( 'Cantidad', rCantidad.Valor );
          end;
          AddString( 'Descripci�n', sDescripcion.Text );
          AddMemo( 'Observaciones', sObserva.Text );
          AddBoolean( 'No inclu�r incapacitados?', lIncapacitados.Checked );
     end;


end;

procedure TWizEmpCambioSalario_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CambioSalarioGetLista( ParameterList );
end;

function TWizEmpCambioSalario_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpCambioSalarioGridSelect_DevEx );
end;

procedure TWizEmpCambioSalario_DevEx.iTipoChange(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TWizEmpCambioSalario_DevEx.SetControls;
begin
     rTasa.Enabled := PorPorcentaje;
     rTasaLBL.Enabled := rTasa.Enabled;
     rCantidad.Enabled := not rTasa.Enabled;
     rCantidadLBL.Enabled := rCantidad.Enabled;
end;

function TWizEmpCambioSalario_DevEx.GetPorPorcentaje: Boolean;
begin
     Result := ( iTipo.ItemIndex = Ord( toPorcentaje ) );
end;

procedure TWizEmpCambioSalario_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               if PorPorcentaje then
               begin
                    if ( rTasa.Valor = 0 ) then
                       CanMove := Error( 'El valor del porcentaje debe ser diferente de cero.', rTasa )
               end
               else
                   if rCantidad.Valor = 0 then
                      CanMove := Error( 'El valor de la cantidad debe ser diferente de cero.', rCantidad );
          end;
     end;
end;

function TWizEmpCambioSalario_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CambioSalario( ParameterList, Verificacion );
end;

procedure TWizEmpCambioSalario_DevEx.dReferenciaValidDate(Sender: TObject);
begin
     inherited;
     dImss.Valor := dReferencia.Valor;
end;

procedure TWizEmpCambioSalario_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'El salario de los empleados seleccionados ser� actualizado, gener�ndose un movimiento de kardex por cada actualizaci�n.';
end;

procedure TWizEmpCambioSalario_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := dReferencia
     else
         ParametrosControl := nil;
     inherited;
end;


end.
