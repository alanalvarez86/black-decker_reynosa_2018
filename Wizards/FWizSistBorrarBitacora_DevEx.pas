unit FWizSistBorrarBitacora_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls, Mask,
     ZetaCommonLists, ZetaFecha, CheckLst, ZcxBaseWizard,
     cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
     dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters,
     cxContainer, cxEdit, ZetaCXWizard,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
     dxCustomWizardControl, dxWizardControl, Menus, cxButtons, cxRadioGroup,
  cxCheckListBox;

type
  TWizSistBorrarBitacora_DevEx = class(TcxBaseWizard)
    Lista: TdxWizardControlPage;
    gbProcesos: TGroupBox;
    chProcesos: TcxCheckListBox;
    PrendeBtn: TcxButton;
    GroupBox1: TcxGroupBox;
    Fecha: TZetaFecha;
    lblFecha: TLabel;
    RB_Empresa: TcxRadioButton;
    RB_Sistema: TcxRadioButton;
    RB_Kiosco: TcxRadioButton;
    RB_Cafeteria: TcxRadioButton;
    GBBitacora: TcxGroupBox;
    CB_Procesos: TCheckBox;
    CB_Individuales: TCheckBox;
    RB_Procesos: TcxRadioButton;
    RB_Individuales: TcxRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure PrendeBtnClick(Sender: TObject);
    procedure RB_Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    lLista: boolean;
    function GetLista: String;
    procedure CargaLista( lfLista: ListasFijas );
    procedure InicializaProcesos(const lState: Boolean);
    procedure InicializaRadioButtons;{OP: 06/06/08}
    procedure ActualizaRadioButtons;{OP: 06/06/08}    
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

const
     DIAS_BITACORA = 90;
var
  WizSistBorrarBitacora_DevEx: TWizSistBorrarBitacora_DevEx;

implementation


uses DCliente,
     DProcesos,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo;

{$R *.DFM}

procedure TWizSistBorrarBitacora_DevEx.FormCreate(Sender: TObject);
begin
     llista := True;
     Fecha.Valor := dmCliente.FechaDefault - DIAS_BITACORA;
     RB_Procesos.Checked := TRUE;
     {OP: 06/06/08}
     {$ifdef DOS_CAPAS}
     Height := 301;
     lblFecha.Top := 201;
     Fecha.Top    := 197;
     GB_Kiosco.Visible := false;
     GB_Cafetaria.Visible := false;
     RB_Kiosco.Visible := false;
     RB_Cafeteria.Visible := false;
     chProcesos.Height := 170;
     Advertencia.Height := 145;
     {$endif}
     InicializaRadioButtons;{OP:06/06/08}
     inherited;
     HelpContext := H80837_Borrar_bitacora;

     //DevEx
     Parametros.PageIndex := 0;
     Lista.PageIndex := 1;
     Ejecucion.PageIndex := 2;
     {***Se define el tipo de salto a utilizar en los wizards***}
     Wizard.SaltoEspecial := TRUE;
end;

procedure TWizSistBorrarBitacora_DevEx.CargaParametros;
begin
     with ParameterList do
     begin
          AddBoolean( 'Procesos', RB_Procesos.Checked );
          AddBoolean( 'Individuales', RB_Individuales.Checked );
          AddBoolean( 'Empresa', RB_Empresa.Checked );{OP: 06/06/08}
          AddBoolean( 'Sistema', RB_Sistema.Checked );{OP: 06/06/08}
          AddBoolean( 'Kiosco', RB_Kiosco.Checked );{OP: 06/06/08}
          AddBoolean( 'Cafeteria', RB_Cafeteria.Checked );{OP: 06/06/08}
          AddDate( 'Fecha', Fecha.Valor );
          AddString( 'Lista', GetLista );
     end;

     //DevEx
     Descripciones.Clear;
     with Descripciones do
     begin
          AddBoolean( 'Bit�cora de la Empresa', RB_Empresa.Checked );
          if ( RB_Empresa.Checked ) then
          begin
               AddBoolean( 'Procesos', RB_Procesos.Checked );
               AddBoolean( 'Individuales', RB_Individuales.Checked );
          end
          else
          begin
               AddBoolean( 'Procesos', False );
               AddBoolean( 'Individuales', False );
          end;
          AddBoolean( 'Bit�cora y Procesos Sistema', RB_Sistema.Checked );
          AddBoolean( 'Bit�cora de Kiosco', RB_Kiosco.Checked );
          AddBoolean( 'Bit�cora de Cafeter�a', RB_Cafeteria.Checked );
          AddDate( 'Fecha', Fecha.Valor );
          if ( RB_Empresa.Checked or RB_Sistema.Checked ) then
             AddString( 'Lista', GetLista );
     end
end;

procedure TWizSistBorrarBitacora_DevEx.CargaLista( lfLista: ListasFijas );
var
   ListaProcesos: TStrings;
   i: integer;
begin
     if lLista then
     begin
          ListaProcesos := TStringList.Create;
          try
             try
                ZetaCommonLists.LlenaLista( lfLista, ListaProcesos );
                with chProcesos do
                begin
                     Clear;
                     with Items do
                     begin
                          BeginUpdate;
                          try
                             for i := 0 to ( ListaProcesos.Count - 1 ) do
                             begin
                                  //Add( ListaProcesos[ i ] );
                                  AddItem( ListaProcesos[ i ] );  //DevEx
                                  //Checked[ i ] := FALSE;
                                  Items[ i ].Checked;      //DevEx
                                  lLista := False;
                             end;
                          finally
                                 EndUpdate;
                          end;
                     end;
                end;
             except
                   on error: exception do
                      ZetaDialogo.zError( Caption, 'Error al preparar la Lista', 0 );
             end;
          finally
                 FreeAndNil( ListaProcesos );
          end;
     end;
end;

function TWizSistBorrarBitacora_DevEx.GetLista: String;
var
   i : Integer;
begin
     Result := VACIO;
     with chProcesos do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               //if Checked[ i ] then
               if Items[ i ].Checked then   //DevEx
                  Result := ZetaCommonTools.ConcatString( Result, intToStr( i ), ',' );
          end;
     end;
end;

procedure TWizSistBorrarBitacora_DevEx.InicializaProcesos( const lState: Boolean );
var
   i: Integer;
begin
     with chProcesos do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      //Checked[ i ] := lState;
                      Items[ i ].Checked := lState;   //DevEx
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

procedure TWizSistBorrarBitacora_DevEx.PrendeBtnClick(Sender: TObject);
begin
     InicializaProcesos( TRUE );
end;

procedure TWizSistBorrarBitacora_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   lMover: boolean;
begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then   //si parte de la pagina inicial
          begin
               if ( RB_Empresa.Checked ) and ( not RB_Procesos.Checked ) and ( not RB_Individuales.Checked ) then
               begin
                    ZetaDialogo.zError( Caption, '� Debe especificarse al menos un tipo de Bit�cora !', 0 );
                    ActiveControl := GBBitacora;
                    CanMove := False;
               end
               else
                   if ( Fecha.Valor > dmCliente.FechaDefault ) then
                   begin
                        ZetaDialogo.zError( Caption, '� Fecha debe ser menor o igual a la fecha del Sistema !', 0 );
                        ActiveControl := Fecha;
                        CanMove := False;
               end;
               {OP: 06/06/08}
               if( RB_Kiosco.Checked or RB_Cafeteria.Checked ) then
                   iNewPage := Ejecucion.PageIndex
               else
                   iNewPage := Lista.PageIndex;    //DevEx

               lLista := TRUE;
          end;
          if CanMove and Adelante and EsPaginaActual( Lista ) then    //si parte de segunda pagina
          begin
               lMover := TRUE;
               if not strVacio( GetLista ) then
                  lMover := FALSE;

               if lMover then
               begin
                    ZetaDialogo.zError( Caption, '� Debe escoger al menos un elemento de la Lista !', 0 );
                    ActiveControl := gbProcesos;
                    CanMove := False;
               end;

               iNewPage := Ejecucion.PageIndex; //DevEx
          end;

           //Hacia atras, partiendo de segunda pagina
          if CanMove and ( Not Adelante ) and EsPaginaActual( Lista ) then
             iNewPage := Parametros.PageIndex;

          {OP: 06/06/08}
          if CanMove and ( Not Adelante ) and EsPaginaActual( Ejecucion ) then
             iNewPage := Parametros.PageIndex;


     end;
end;

procedure TWizSistBorrarBitacora_DevEx.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if Wizard.EsPaginaActual( Lista ) then
       if RB_Empresa.Checked then {OP: 06/06/08}
       begin
            if ( RB_Procesos.Checked ) then
            begin
                 gbProcesos.Caption := ' Procesos a Borrar ( Empresa Activa ) ';
                 CargaLista( lfProcesos );
            end
            else if ( RB_Individuales.Checked ) then
            begin
                 gbProcesos.Caption := ' Individuales a Borrar ( Empresa Activa ) ';
                 CargaLista( lfClaseBitacora );
            end;
        end
        else if RB_Sistema.Checked then {OP: 06/06/08}
        begin
             gbProcesos.Caption := ' Bit�coras de Sistema ';
             Cargalista( lfClaseSistBitacora );
        end;

end;

function TWizSistBorrarBitacora_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.BorrarBitacora( ParameterList );
end;

{OP: 06/06/08}
procedure TWizSistBorrarBitacora_DevEx.InicializaRadioButtons;
begin
     RB_Empresa.Checked  := true;
     RB_Procesos.Checked := true;
end;

{OP: 06/06/08}
procedure TWizSistBorrarBitacora_DevEx.ActualizaRadioButtons;
begin
     RB_Procesos.Enabled  := RB_Empresa.Checked;
     RB_Individuales.Enabled := RB_Empresa.Checked;
     GBBitacora.Enabled := RB_Empresa.Checked;
end;

{OP: 06/06/08}
procedure TWizSistBorrarBitacora_DevEx.RB_Click(Sender: TObject);
begin
     inherited;
     ActualizaRadioButtons
end;

procedure TWizSistBorrarBitacora_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption :=
     'Al aplicar el proceso se borrar�n el tipo de registros indicados en los par�metros.';
end;



end.
