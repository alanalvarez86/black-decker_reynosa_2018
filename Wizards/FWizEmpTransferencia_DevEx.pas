unit FWizEmpTransferencia_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ComCtrls, Db, ExtCtrls, Mask,
     ZetaNumero, ZetaCommonClasses, ZcxBaseWizard, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
      TressMorado2013, dxSkinsDefaultPainters,
     cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel,
     cxGroupBox, dxCustomWizardControl, dxWizardControl, cxCheckBox,
     ZetaKeyLookup_DevEx;

type
  TWizEmpTransferencia_DevEx = class(TcxBaseWizard)
    GroupBox1: TcxGroupBox;
    Label2: TLabel;
    EmpleadoSource: TZetaKeyLookup_DevEx;
    GroupBox2: TcxGroupBox;
    Label1: TLabel;
    EmpleadoTarget: TZetaNumero;
    EmpresaTarget: TZetaKeyLookup_DevEx;
    Label3: TLabel;
    GroupBox3: TcxGroupBox;
    lblTipoKardex: TLabel;
    TipoKardex: TZetaKeyLookup_DevEx;
    chkTipoKardex: TcxCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EmpleadoSourceValidLookup(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure chkTipoKardexClick(Sender: TObject);{OP: 27/05/08}
    procedure FormClose(Sender: TObject; var Action: TCloseAction);{OP: 27/05/08}
    procedure EmpresaTargetExit(Sender: TObject);{OP: 26/05/08}
    procedure EmpresaTargetValidKey(Sender: TObject);
    procedure FormDestroy(Sender: TObject);{OP: 27/05/08}
  private
    { Private declarations }
    FEmpresaDestino: Variant;
    procedure ActivaTipoKardex( lActivo: Boolean ); {OP: 26/05/08}
    function ValidaVacioTipoKardex: Boolean; {OP: 26/05/08}
    procedure EmpresaValidaTipoKardex; {OP: 26/05/08}
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpTransferencia_DevEx: TWizEmpTransferencia_DevEx;

implementation

uses DCliente,
     DSistema,
     DProcesos,
     ZetaDialogo,
     ZetaCommonTools, dTablas, ZcxWizardBasico;

{$R *.DFM}

{ TWizEmpTransferencia }

procedure TWizEmpTransferencia_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     dmCliente.SetTipoLookUpEmpleado( eLookEmpComidas );{OP: 28/05/08}
     EmpleadoSource.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     EmpresaTarget.LookupDataset := dmSistema.cdsEmpresasLookUp;
     ParametrosControl := EmpleadoSource;
     with dmCliente do
     begin
          EmpleadoSource.Valor := Empleado;
          {
          EmpresaTarget.Filtro := 'CM_CODIGO <> ''' + Compania + '''';
          EmpresaTarget.Filtro := Format( 'CM_CODIGO <> ''%s'' and CM_ALIAS <> ''%s''', [ Compania, cdsCompany.FieldByName( 'CM_ALIAS' ).AsString ] );
          }
          EmpresaTarget.Filtro := Format( '( CM_CODIGO <> ''%s'' ) and ( CM_DATOS <> ''%s'' )', [ Compania, cdsCompany.FieldByName( 'CM_DATOS' ).AsString ] );
     end;
     HelpContext:= H11610_Transferencia_de_empleado;
end;

procedure TWizEmpTransferencia_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmSistema.cdsEmpresasLookup.Conectar;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     dmTablas.cdsMovKardex.Conectar;{OP: 26/05/08}

     Advertencia.Caption := 'Al ejecutar el proceso se transferir�n los registros de los empleados a la empresa indicada.';
end;

procedure TWizEmpTransferencia_DevEx.CargaParametros;
begin
     Descripciones.Clear;
     inherited CargaParametros;
     with ParameterList do
     begin
          AddInteger( 'EmpleadoSource', EmpleadoSource.Valor );
          AddInteger( 'EmpleadoTarget', EmpleadoTarget.ValorEntero );
          AddString( 'EmpresaCodigo', dmCliente.GetDatosEmpresaActiva.Codigo );{OP: 28/05/08}
          AddString( 'EmpresaDescrip', dmCliente.GetDatosEmpresaActiva.Nombre );{OP: 28/05/08}
          AddString( 'KardexTipo', TipoKardex.Llave );{OP: 27/05/08}
          AddBoolean( 'EmpleadoActivo', zStrToBool( dmCliente.cdsEmpleadoLookUp.FieldByName( 'CB_ACTIVO' ).AsString ) );{OP: 16/06/08}
          AddDate( 'EmpleadoFecBaj', dmCliente.cdsEmpleadoLookUp.FieldByName( 'CB_FEC_BAJ' ).AsDateTime );{OP: 27/05/08}
          with dmTablas.cdsMovKardex do
          begin
               AddString( 'KardexDescrip', FieldByName( 'TB_ELEMENT' ).AsString );
               AddInteger( 'KardexNivel', FieldByName( 'TB_NIVEL' ).AsInteger );
          end;
          {$ifndef DOS_CAPAS}
          AddInteger( 'NumeroBiometrico', dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger );
          {$endif}
     end;

     //DevEx
     with Descripciones do
     begin
          AddInteger( 'Empleado a transferir', EmpleadoSource.Valor );
          AddString( 'Transferir a empresa', EmpresaTarget.Llave + ': ' + EmpresaTarget.Descripcion );
          AddInteger( 'No. de empleado nuevo', EmpleadoTarget.ValorEntero );
          if ( chkTipoKardex.Checked ) then
          begin
             AddString( 'Agrega Kardex de origen', chkTipoKardex.Properties.ValueChecked );
             AddString( 'Tipo de Kardex', TipoKardex.Llave + ': ' + TipoKardex.Descripcion );
          end;
     end;

     with dmSistema do
     begin
          if cdsEmpresasLookup.locate( 'CM_CODIGO', EmpresaTarget.Llave, [] ) then
             FEmpresaDestino:= dmCliente.BuildEmpresa(cdsEmpresasLookup);
          // Los Datos de FEmpresaDestino se envian en la llamada a dmProcesos
     end;
end;

procedure TWizEmpTransferencia_DevEx.EmpleadoSourceValidLookup(Sender: TObject);
begin
     EmpleadoTarget.Valor:= EmpleadoSource.Valor;
end;

procedure TWizEmpTransferencia_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;
     if CanMove and Wizard.Adelante then
     begin
          //if PageControl.ActivePage = Parametros then
          if WizardControl.ActivePage = Parametros then
          begin
               if ( EmpleadoSource.Valor = 0 ) then
                  CanMove := Error( 'No se escogi� un empleado a transferir', EmpleadoSource )
               else
                   if ZetaCommonTools.StrVacio( EmpresaTarget.Llave ) then
                      CanMove := Error( 'No se ha especificado empresa a la cual transferir empleado', EmpresaTarget )
                   else
                       if ZetaCommonTools.StrVacio( EmpleadoTarget.Text ) then
                          CanMove := Error( 'No se especific� el nuevo n�mero del empleado', EmpleadoTarget )
                       else
                           if ValidaVacioTipoKardex then
                              CanMove := Error( 'No se ha especificado el tipo de Kardex', TipoKardex ); {OP: 26/05/08}
          end;
     end;
end;

procedure TWizEmpTransferencia_DevEx.WizardAfterMove(Sender: TObject);
begin

     if Wizard.EsPaginaActual(Parametros) then
             ParametrosControl := EmpleadoSource
     else
         ParametrosControl := nil;
     inherited;
     if Wizard.EsPaginaActual( Ejecucion ) then
     begin
       {
          //with Advertencia.Lines do
          begin
               try
                  BeginUpdate;
                  Clear;
                  Add( 'El Empleado ' + EmpleadoSource.Llave + ' Ser� Transferido' );
                  Add( 'De La Compa��a ' + dmCliente.GetDatosEmpresaActiva.Nombre );
                  Add( 'A La Compa��a ' + EmpresaTarget.Descripcion );
                  Add( 'Con El Nuevo N�mero De Empleado ' + EmpleadoTarget.Text );
                  Add( '' );
                  Add( 'Todos Sus Datos Ser�n Borrados De La Empresa' );
                  Add( 'Activa Excepto Los Correspondientes A N�minas' );
                  Add( 'y Liquidaciones De IMSS' );
               finally
                      EndUpdate;
               end;
          end;
        }
         //DevEx
        Advertencia.Caption :='Al ejecutar el proceso se transferir�n los registros de los empleados a la empresa indicada.';


     end;
end;

function TWizEmpTransferencia_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.Transferencia( ParameterList, FEmpresaDestino );
end;

{OP: 26/05/08}
procedure TWizEmpTransferencia_DevEx.ActivaTipoKardex( lActivo: Boolean );
begin
     lblTipoKardex.Enabled := lActivo;
     TipoKardex.Enabled := lActivo;
     if Not lActivo then
        TipoKardex.Llave := VACIO;
end;

{OP: 26/05/08}
function TWizEmpTransferencia_DevEx.ValidaVacioTipoKardex: Boolean;
begin
     Result := ( chkTipoKardex.Checked ) and ( StrVacio( TipoKardex.Llave ) );
end;

{OP: 26/05/08}
procedure TWizEmpTransferencia_DevEx.chkTipoKardexClick(Sender: TObject);
begin
     inherited;
     ActivaTipoKardex( chkTipoKardex.Checked );
end;

{OP: 27/05/08}
procedure TWizEmpTransferencia_DevEx.EmpresaValidaTipoKardex;
begin
     dmTablas.cdsMovKardexCambiaEmpresa( EmpresaTarget.Llave );
end;

{OP: 26/05/08}
procedure TWizEmpTransferencia_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     dmTablas.cdsMovKardex.SetDataChange;
end;

{OP: 26/05/08}
procedure TWizEmpTransferencia_DevEx.EmpresaTargetExit(Sender: TObject);
var
   lEmpresaValida : Boolean;
begin
     inherited;
     lEmpresaValida := StrLleno( EmpresaTarget.Llave );
     if ( Not lEmpresaValida ) then
     begin
          chkTipoKardex.Enabled := lEmpresaValida;
          chkTipoKardex.Checked := lEmpresaValida;
          ActivaTipoKardex( lEmpresaValida );
     end;
end;

{OP: 26/05/08}
procedure TWizEmpTransferencia_DevEx.EmpresaTargetValidKey(Sender: TObject);
var
   lEmpresaValida : Boolean;
begin
     inherited;
     lEmpresaValida := StrLleno( EmpresaTarget.Llave );
     if lEmpresaValida then
     begin
          EmpresaValidaTipoKardex;
          chkTipoKardex.Enabled := lEmpresaValida;
          TipoKardex.Llave := VACIO;
     end;
end;

{OP: 28/05/08}
procedure TWizEmpTransferencia_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     dmCliente.ResetLookupEmpleado;{OP: 28/05/08}
end;

end.



