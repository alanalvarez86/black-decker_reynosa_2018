unit FWizNomCalcularPagoFonacot_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  {$ifdef TRESS_DELPHIXE5_UP}Data.DB,{$endif}
  Dialogs, ZcxBaseWizard, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, ZetaKeyCombo, ZetaDBTextBox, Mask,
  ZetaNumero, ZetaCommonLists, ZCXBaseWizardFiltro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl, ZetaKeyLookup_DevEx,
  ZetaCXWizard, Menus, cxRadioGroup, cxTextEdit, cxMemo, cxButtons;

type
  TWizNomCalcularPagoFonacot_DevEx = class(TBaseCXWizardFiltro)
    RangosDNominas: TdxWizardControlPage;
    GBTipoPrestamo: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    NoTipoPrestamo: TZetaTextBox;
    TipoPrestamo: TZetaTextBox;
    MesDefault: TZetaKeyCombo;
    RGFiltro: TRadioGroup;
    YearDefaultAct: TZetaNumero;
    GBRangosNominas: TGroupBox;
    Ajuste: TdxWizardControlPage;
    Label4: TLabel;
    TipoPrestamoAjus: TZetaKeyLookup_DevEx;
    ReportarIncapa: TCheckBox;
    RGReportar: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure ReportarIncapaClick(Sender: TObject);
    procedure RGFiltroClick(Sender: TObject);
    //procedure SemanalIniValidKey(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Agregado para enfocar correctamente los controles
  private
    { Private declarations }
    FPanelesVisible,FMesAnterior: Integer;
    FRangosNomina: Variant;
    FPanelesCreados: Boolean;
    function GetFiltraPorNom: Boolean;
    procedure HabilitarFiltrosNom;
    procedure SetFiltroPeriodo;
    procedure SetControlesRNominas;
  protected
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros;override;
    procedure AjustarLabelLeft(lblLabel: TLabel; tpOrdinal: eTipoPeriodo); //acl
  public
    { Public declarations }
     Panels : array of TPanel;
  end;

const
     K_REPPRIMERINCAPA = 0;
     K_REPMAYORINCAPA   = 1;
     K_REEMPLAZAR = 0;
     K_ANTERIOR   = 1;
     K_RANGO_NOM = 1;
     K_FILTRO_MES = 0;
     K_MAX_NOMINAS = High( eTipoPeriodo );
     K_FINAL = -1;

var
  WizNomCalcularPagoFonacot_DevEx: TWizNomCalcularPagoFonacot_DevEx;

implementation

uses DCliente,
     DProcesos,
     DTablas,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaTipoEntidad,
     ZGlobalTress,
     DBaseGlobal,
     DGlobal,
     dCatalogos,
     DNomina,
     ZetaDialogo;

{$R *.dfm}


procedure TWizNomCalcularPagoFonacot_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     dmTablas.cdsTPresta.Conectar;
     TipoPrestamoAjus.Tag:= K_GLOBAL_DEF_NOM_TIPOPRESTAJUSTE_CALCULO;
     SetFiltroPeriodo;
     with dmCliente do
     begin
          YearDefaultAct.Valor := GetDatosPeriodoActivo.Year;
          MesDefault.Valor:= ImssMes;
     end;
     HelpContext:= H_WIZ_CALCULAR_PAGO_FONACOT;
     RGFiltro.ItemIndex:= K_FILTRO_MES;
     ParametrosControl := YearDefaultAct;

     {***Se define la seceunta de las paginas***}
     Ejecucion.PageIndex := WizardControl.PageCount-1;
     Ajuste.PageIndex := WizardControl.PageCount-2;
     RangosDNominas.PageIndex := WizardControl.PageCount-3;
     Parametros.PageIndex := WizardControl.PageCount-4;
     {***Se define el tipo de salto a utilizar en los wizards***}
     Wizard.SaltoEspecial := TRUE;

     //(@am):Bandera para la creacion dinamica de paneles en la opcion 'Rangos de nomina'
     FPanelesCreados := FALSE;
end;

procedure TWizNomCalcularPagoFonacot_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     NoTipoPrestamo.Caption:= Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO);
     TipoPrestamo.Caption:= dmTablas.cdsTPresta.GetDescripcion(NoTipoPrestamo.Caption);
     HabilitarFiltrosNom;
     FMesAnterior:= -1;
     Advertencia.Caption := 'Al ejecutar el proceso se calcular� el pago de retenciones de Fonacot correspondientes al mes y a�o indicados.';
end;

{acl 7/10/08 Para ajustar dinamicamente la posicion de los labels}
procedure TWizNomCalcularPagoFonacot_DevEx.AjustarLabelLeft(lblLabel: TLabel; tpOrdinal: eTipoPeriodo);
const
     K_WIDTH_MAX = 120;
     K_WIDTH_2PUNTOS = 3;
var
   iWidthO, iDif: integer;
begin
     iWidthO:=lblLabel.Width;

     lblLabel.Caption:= ObtieneElemento( lfTipoPeriodo, ord( tpOrdinal ) ) + ':';
     if ( lblLabel.Width > K_WIDTH_MAX ) then
     begin
          lblLabel.Width:= K_WIDTH_MAX;
     end;

     if( lblLabel.Width > ( iWidthO - K_WIDTH_2PUNTOS ) ) then
     begin
          iDif:= lblLabel.Width - iWidthO;
          lblLabel.Left:=lblLabel.Left - iDif;
     end;
end;


procedure TWizNomCalcularPagoFonacot_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const
     K_MENSAJE_VALOR_NOM = 'Debe especificarse un valor v�lido de rango de n�minas';
var
   iYear, iMes: Integer;
   dFecha, dFechaIni, dFechaFin: TDate;
   eStatus: eStatusPeriodo;
   oLookup: TZetaKeyLookup_DevEx;

   function CargoRangoNominas( const lFiltroRangoNominas: Boolean): Boolean;
   var
      i,c,iPanel,iRangoIni,iRangoFin: Integer;
      oPanel: TPanel;
      //eTipo: eTipoPeriodo;  //OLD
      eTipo: Integer;

      procedure IniciaBusquedaPeriodo;
      begin
           iRangoIni:= 0;
           iRangoFin:= 0;
           iPanel:= 0;
           dFecha:= 0;
           dFechaIni:= 0;
           dFechaFin:= 0;
      end;
   begin
        Result:= TRUE;
        oLookup:= NIL;
        IniciaBusquedaPeriodo;
        if lFiltroRangoNominas then
        begin
             FRangosNomina:= VarArrayCreate( [ 0, FPanelesVisible ], varVariant );
             with GBRangosNominas do
             begin
                  //Barre los controles del Grupo: Son los paneles

                  for i:= 0 to ControlCount - 1 do
                  begin
                        if ( Result ) and ( Controls[i] is TPanel ) then
                        begin
                             oPanel:= TPanel( Controls[i]);
                             //Si es visible el Panel:
                             if ( oPanel.Visible ) then
                             begin
                                  //Barre los controles del Panel
                                  for c:= 0 to oPanel.ControlCount - 1 do
                                  begin
                                       if ( Result ) and ( oPanel.Controls[c] is TZetaKeyLookup_DevEx ) then
                                       begin
                                            oLookup:= TZetaKeyLookup_DevEx( oPanel.Controls[c] );
                                            if ( oLookup.Tag = K_FINAL ) then
                                            begin
                                                 iRangoFin:= oLookup.Valor;
                                                 Result:= dmCatalogos.BuscaPeriodo( iYear, iRangoFin, eTipoPeriodo(oPanel.Tag), eStatus, dFecha, dFechaFin );
                                            end
                                            else
                                            begin
                                                 iRangoIni:= oLookup.Valor;
                                                 Result:= dmCatalogos.BuscaPeriodo( iYear, iRangoIni, eTipoPeriodo(oPanel.Tag), eStatus, dFechaIni, dFecha );
                                            end;
                                       end;
                                  end;
                                  //Ya que barri� los controles asigna fechas y rangos
                                  if ( Result ) then
                                  begin
                                       Result:= ( iRangoIni<= iRangoFin );
                                       if ( Result ) then
                                       begin
                                            FRangosNomina[iPanel]:= VarArrayOf( [ oPanel.Tag, iRangoIni, iRangoFin, dFechaIni, dFechaFin]);
                                            Inc( iPanel );
                                       end
                                  end;
                             end;
                        end;
                  end;
             end;
        end
        else  { Se mandan las fechas de las nominas donde acumula ese mes, se necesita para incluir a empleados dados de baja en esas nominas}
        begin
             with dmCatalogos do
             begin
                  if FMesAnterior <> MesDefault.Valor then
                  begin
                       //FRangosNomina:= VarArrayCreate( [ 0, Ord(High(eTipoPeriodo)) ], varVariant ); //OLD
                       FRangosNomina:= VarArrayCreate( [ 0, FArregloPeriodo.Count ], varVariant );
                       for eTipo:= 0 to FArregloPeriodo.count-1 do
                       begin
                            IniciaBusquedaPeriodo;
                            GetPeriodoPorMes( eTipo, MesDefault.Valor, iRangoIni, iRangoFin );
                            if ( iRangoIni > 0 ) then
                            begin
                                 Result:= (iRangoIni <= iRangoFin) and
                                           BuscaPeriodo( iYear, iRangoIni, eTipo, eStatus, dFechaIni, dFecha ) and
                                           BuscaPeriodo( iYear, iRangoFin, eTipo, eStatus, dFecha, dFechaFin );
                            end;
                            FRangosNomina[Ord(eTipo)]:= VarArrayOf( [ eTipo, iRangoIni, iRangoFin, dFechaIni, dFechaFin]);
                       end;
                       FMesAnterior:= MesDefault.Valor;
                  end;
             end;
        end;
   end;

begin
     ParametrosControl := nil;
     iYear:= YearDefaultAct.ValorEntero;
     iMes:= MesDefault.Valor;
     if CanMove then
     begin
          if Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
          begin
               SetFiltroPeriodo;
               
               if ( dmNomina.GetStatusFonacot( iMes, iYear ) = slAfectadaTotal ) then
               begin
                    CanMove:= Error( 'El c�lculo de Fonacot se encuentra cerrado', MesDefault );
               end
               else
                 if ( TipoPrestamoAjus.Llave <> VACIO ) and ( NoTipoPrestamo.Caption = TipoPrestamoAjus.Llave ) then
                 begin
                      CanMove:= Error( 'El Tipo de Pr�stamo para Ajuste no puede ser igual al Tipo de Pr�stamo de Fonacot', TipoPrestamoAjus );
                 end
               else if ( dmCatalogos.cdsPeriodoOtro.IsEmpty ) then
                 begin
                      CanMove:= Error( Format( 'No existen periodos definidos para el a�o %d', [ iYear ] ), YearDefaultAct );
                 end
               else if ( GetFiltraporNom ) then
                 begin
                      SetControlesRNominas;
                 end
               else if not CargoRangoNominas(GetFiltraporNom) then
                 begin
                      CanMove:= Error( 'Error al revisar periodos que acumulan al mes', RGFiltro );
                 end;
          end
          else if Wizard.Adelante and Wizard.EsPaginaActual( Ajuste ) then
          begin
               if ( TipoPrestamoAjus.Llave <> VACIO ) and ( NoTipoPrestamo.Caption = TipoPrestamoAjus.Llave ) then
               begin
                    CanMove:= Error( 'El Tipo de Pr�stamo para Ajuste no puede ser igual al Tipo de Pr�stamo de Fonacot', TipoPrestamoAjus );
               end
          end
          else if Wizard.Adelante and Wizard.EsPaginaActual( RangosDNominas ) then
          begin
               if not CargoRangoNominas(GetFiltraporNom) then
               begin
                    CanMove:= Error( K_MENSAJE_VALOR_NOM, oLookup );
               end;
          end;
     end;
     {***Validar hacia que pagina queremos que se mueva***}
     if Wizard.Adelante then
     begin
          if CanMove then
          begin
               if  Wizard.EsPaginaActual( Parametros) then
                   if  ( RGFiltro.ItemIndex = K_RANGO_NOM ) then
                       iNewPage := RangosDNominas.PageIndex
                   else
                       iNewPage := Ajuste.PageIndex
               else if Wizard.EsPaginaActual( RangosDNominas )then
                   iNewPage := Ajuste.PageIndex
               else if Wizard.EsPaginaActual ( Ajuste )  then
                   iNewPage := Ejecucion.PageIndex
          end;
     end
     else
     begin
         if  Wizard.EsPaginaActual( Ejecucion ) then
              //if  ( RGFiltro.ItemIndex = K_RANGO_NOM ) then
                 iNewPage := Ajuste.PageIndex
             //else
             //    iNewPage := Parametros.PageIndex
         else if Wizard.EsPaginaActual( RangosDNominas )then
              iNewPage := Parametros.PageIndex
         else if Wizard.EsPaginaActual ( Ajuste ) then
              if  ( RGFiltro.ItemIndex = K_RANGO_NOM ) then
                  iNewPage := RangosDNominas.PageIndex
              else
                  iNewPage := Parametros.PageIndex
     end;
     inherited;
end;

procedure TWizNomCalcularPagoFonacot_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control dependiendo del tipo de proceso
     if Wizard.EsPaginaActual( Parametros ) then
        ParametrosControl := YearDefaultAct
     else
         ParametrosControl := nil;
     inherited;
end;

procedure TWizNomCalcularPagoFonacot_DevEx.RGFiltroClick(Sender: TObject);
begin
     HabilitarFiltrosNom;
end;

procedure TWizNomCalcularPagoFonacot_DevEx.HabilitarFiltrosNom;
var
   lHabilita: Boolean;
begin
     lHabilita:= ( RGFiltro.ItemIndex = K_RANGO_NOM );
     RangosDNominas.Enabled:= lHabilita;
     FMesAnterior:= -1;
end;


function TWizNomCalcularPagoFonacot_DevEx.EjecutarWizard: Boolean;
var
   lRepPrimeraIncapa: Boolean;
begin
     lRepPrimeraIncapa:= ( RGReportar.ItemIndex = K_REPPRIMERINCAPA );
     with ParameterList do
     begin
          AddBoolean('NuevoMecanismo', Global.GetGlobalBooleano (K_GLOBAL_FONACOT_PRESTAMO_CONCILIA) );
          AddInteger('Year', YearDefaultAct.ValorEntero );
          AddInteger('Mes', MesDefault.Valor );
          AddString('TPrestamo', NoTipoPrestamo.Caption );
          AddString('TPrestamoAjus', TipoPrestamoAjus.Llave );
          AddBoolean('RepPrimeraIncapa', lRepPrimeraIncapa );
          AddBoolean('ReportarIncapa', ReportarIncapa.Checked  );
          AddBoolean('FiltraTipNom', GetFiltraporNom );
          // AP(11/10/2007): Se Agreg� �ste par�metro para ahorrarnos el trabajo de ir al servidor e investigar el dato
          AddInteger('NoConcepto', dmTablas.GetConcepto( NoTipoPrestamo.Caption,TRUE,TRUE ) );
          AddVariant('RangosNomina', FRangosNomina );
          if GetFiltraporNom then
             AddInteger('MaxTiposNom', FPanelesVisible )
          else
              AddInteger('MaxTiposNom', FArregloPeriodo.Count  );
     end;
     Result := dmProcesos.CalcularPagoFonacot(ParameterList );
end;

procedure TWizNomCalcularPagoFonacot_DevEx.CargaParametros;
{var
   lRepPrimeraIncapa: Boolean; }
begin
    // lRepPrimeraIncapa:= ( RGReportar.ItemIndex = K_REPPRIMERINCAPA );
     inherited;

     //inherited CargaParametros;
     with Descripciones do
     begin
          AddString( 'A�o', Inttostr( YearDefaultAct.ValorEntero ));
          AddString( 'Mes', MesDefault.Text );
          AddString( 'Tipo de Pr�stamo Fonacot', NoTipoPrestamo.Caption );
          AddString( 'Tipo de Pr�stamo Ajuste', TipoPrestamoAjus.Llave );
          AddString( 'Ajuste Anterior', 'SI' );
          if ReportarIncapa.Checked then
             AddString( 'Incluir Incapacitados', 'SI' )
          else
             AddString( 'Incluir Incapacitados', 'NO' );

          AddString( 'Rango', 'Todos' );

     end;
      //Descripciones.Clear; //DevEx(by am): Limpiamos la lista de descripciones para que aparezca el mensaje por defecto indicando que el proces no requiere parametros.

end;

procedure TWizNomCalcularPagoFonacot_DevEx.ReportarIncapaClick(Sender: TObject);
begin
     inherited;
     RGReportar.Enabled:= ReportarIncapa.Checked;
end;

function TWizNomCalcularPagoFonacot_DevEx.GetFiltraPorNom: Boolean;
begin
     Result := ( RGFiltro.ItemIndex = K_RANGO_NOM );
end;

procedure TWizNomCalcularPagoFonacot_DevEx.SetControlesRNominas;

const
         K_DOS_PUNTOS = ':';
         K_WIDTH_MAX = 120;
         K_PANEL_TOP = 1234567;
    function DinamicoPanelPeriodos( iNumeroPanel: Integer; componentOwner: TComponent; componentParent: TWinControl ):TPanel;
    var
       pnlTiposPeriodos : TPanel;
    begin
         try
           pnlTiposPeriodos := TPanel.Create( componentOwner );
           pnlTiposPeriodos.Parent := componentParent;
           pnlTiposPeriodos.Visible := False;
           pnlTiposPeriodos.Top := K_PANEL_TOP;
           pnlTiposPeriodos.Align := alTop;
           pnlTiposPeriodos.Left := 2;
           pnlTiposPeriodos.Width := 496;
           pnlTiposPeriodos.Height := 30;
           pnlTiposPeriodos.Tag := iNumeroPanel;
           pnlTiposPeriodos.TabOrder := iNumeroPanel;
           Result := pnlTiposPeriodos;
           except on e:exception do
           begin
                Result := TPanel.Create( Nil );
           end;
         end;
    end;

    function DinamicoLabelTextoPeriodo( iNumeroPanel: Integer; sTextoTipoPeriodo: String; lblMaxWidth: Integer; componentOwner: TComponent; componentParent: TWinControl; iLeftLookUpIni: Integer ): TLabel;
    var
       oLabelIni : TLabel;
    begin
         try
           oLabelIni := TLabel.Create( componentOwner );
           oLabelIni.Parent := componentParent;
           oLabelIni.Left := iLeftLookUpIni - Canvas.TextWidth( sTextoTipoPeriodo + K_DOS_PUNTOS ) - 7;
           oLabelIni.Top := 8;
           oLabelIni.Width := lblMaxWidth;
           oLabelIni.Height := 13;
           oLabelIni.Caption := sTextoTipoPeriodo + K_DOS_PUNTOS;
           oLabelIni.Transparent := True;
           Result := oLabelIni;
           except on e:exception do
           begin
                Result := TLabel.Create( Nil );
           end;
         end;
    end;

    procedure DinamicoLabelTextoFin( iNumeroPanel: Integer; sTexto: String; componentOwner: TComponent; componentParent: TWinControl );
    var
       oLabelFin : TLabel;
    begin
         try
           oLabelFin := TLabel.Create( componentOwner );
           oLabelFin.Parent := componentParent;
           oLabelFin.Left := 232;
           oLabelFin.Top := 8;
           oLabelFin.Width := 6;
           oLabelFin.Height := 13;
           oLabelFin.Caption := sTexto;
           oLabelFin.Transparent := True;
           except on e:exception do
           begin
                ZetaDialogo.ZError( Caption,  e.Message, 0 );
           end;
         end;
    end;

    function DinamicoLookUpIni( iNumeroPanel: Integer; componentOwner: TComponent; componentParent: TWinControl ): TZetaKeyLookup_DevEx;
    var
       PeriodoIni : TZetaKeyLookup_DevEx;
    begin
         try
           PeriodoIni := TZetaKeyLookup_DevEx.Create( componentOwner );
           PeriodoIni.Parent := componentParent;
           PeriodoIni.Tag := 0;
           PeriodoIni.Left := 136;
           PeriodoIni.Top := 4;
           PeriodoIni.Width := 85;
           PeriodoIni.Height := 21;
           PeriodoIni.LookupDataset := dmCatalogos.cdsPeriodoOtro;
           PeriodoIni.EditarSoloActivos := False;
           PeriodoIni.IgnorarConfidencialidad := False;
           PeriodoIni.TabOrder := 0;
           PeriodoIni.TabStop := True;
           PeriodoIni.WidthLlave := 60;
           Result := PeriodoIni;
           except on e:exception do
           begin
                Result := TZetaKeyLookup_DevEx.Create( Nil );
           end;
         end;
    end;

    procedure DinamicoLookUpFin( iNumeroPanel: Integer; componentOwner: TComponent; componentParent: TWinControl );
    var
       PeriodoFin : TZetaKeyLookup_DevEx;
    begin
         try
           PeriodoFin := TZetaKeyLookup_DevEx.Create( componentOwner );
           PeriodoFin.Parent := componentParent;
           PeriodoFin.Tag := -1;
           PeriodoFin.Left := 250;
           PeriodoFin.Top := 4;
           PeriodoFin.Width := 85;
           PeriodoFin.Height := 21;
           PeriodoFin.LookupDataset := dmCatalogos.cdsPeriodoOtro;
           PeriodoFin.EditarSoloActivos := False;
           PeriodoFin.IgnorarConfidencialidad := False;
           PeriodoFin.TabOrder := 1;
           PeriodoFin.TabStop := True;
           PeriodoFin.WidthLlave := 60;
           except on e:exception do
           begin
                ZetaDialogo.ZError( Caption,  e.Message, 0 );
           end;
         end;
    end;

    function MaxWidthTextoTipoPeriodo: Integer;
    var
       lblMaxWidth : Integer;
       I : Integer;
    begin
         lblMaxWidth := 0;
         for I := 0 to FArregloPeriodo.Count-1 do
         begin
              if (Canvas.TextWidth(ObtieneElemento( lfTipoPeriodo, I ))>lblMaxWidth) then
                 lblMaxWidth :=  Canvas.TextWidth(ObtieneElemento( lfTipoPeriodo, I ));
         end;
         if lblMaxWidth > K_WIDTH_MAX then
            lblMaxWidth := K_WIDTH_MAX;
        Result := lblMaxWidth;
    end;

    procedure AsignaLookupsRNominas;
    var
       i, c, iPeriodoIni, iPeriodoFinal: Integer;
       oPanel: TPanel;
       oLookup: TZetaKeyLookup_DevEx;
       oLookupIni: TZetaKeyLookup_DevEx;
       oLabelIni : TLabel;
    const
         K_FILTRO = '( PE_TIPO = %d )';
    begin
         with GBRangosNominas do
         begin
              if FPanelesCreados then
              begin
                  for i:= 0 to FArregloPeriodo.Count-1 do
                  begin
                       if Panels[i] is TPanel then
                          FreeAndNil( Panels[i] );
                  end;
                  FPanelesCreados := False;
                  SetLength(Panels, 0);
              end;
              if not FPanelesCreados then
              begin
                   //(@am): Crear paneles necesarios
                   SetLength(Panels, FArregloPeriodo.Count );
                   for i := 0 to FArregloPeriodo.Count-1 do
                   begin
                        if dmCatalogos.cdsPeriodoOtro.Locate( 'PE_TIPO', i, [] )  then
                        begin
                             Panels[i] := DinamicoPanelPeriodos( i, Self, GBRangosNominas );
                             if not ( Panels[i].Visible )  then
                             begin
                                  oLookupIni := DinamicoLookUpIni( i, Self, Panels[i] );
                                  DinamicoLabelTextoFin( i, 'a', Self, Panels[i] );
                                  DinamicoLookUpFin( i, Self, Panels[i] );
                                  oLabelIni := DinamicoLabelTextoPeriodo( i, ObtieneElemento( lfTipoPeriodo, i ), MaxWidthTextoTipoPeriodo, Self, Panels[i], oLookupIni.left );
                                  AjustarLabelLeft(oLabelIni, i);
                             end;
                        end
                        {else
                            Panels[i] := Nil;}
                   end;
                   FPanelesCreados := TRUE;
              end;
         end;
         //(@am): Si los paneles no estan creados, los crea (siempre los debe crear porque la creacion cambia en base a la pagina de parametros.)
         //(@m): Asociar informacion a controles de cada panel
         with GBRangosNominas do
         begin
              for i:= 0 to GBRangosNominas.ControlCount - 1 do
              begin
                    if ( Controls[i] is TPanel ) then
                    begin
                         oPanel:= TPanel( Controls[i] );
                         oPanel.Align:= alTop;
                         oPanel.Visible:= ( dmCatalogos.cdsPeriodoOtro.Locate( 'PE_TIPO', oPanel.Tag, [] )  );
                         if ( oPanel.Visible ) then
                         begin
                              Inc( FPanelesVisible );
                              for c:= 0 to oPanel.ControlCount - 1 do
                              begin
                                   if ( oPanel.Controls[c] is TZetaKeyLookup_DevEx ) then
                                   begin
                                        oLookup:= TZetaKeyLookup_DevEx( oPanel.Controls[c] );
                                        TZetaKeyLookup_DevEx( TPanel(Controls[i]).Controls[c] ).Filtro:= Format( K_FILTRO, [ oPanel.Tag ] );
                                        //(@am): Antes se optimizaba haciendo el query y asignacion una sola vez.
                                        //Como ahora al dar regresar borramos componentes, esto no es posible, la asignacion debe hacerse siempre.
                                        //if ( FMesAnterior <> MesDefault.Valor ) then
                                        //begin
                                        dmCatalogos.GetPeriodoPorMes( eTipoPeriodo( oPanel.Tag ), MesDefault.Valor, iPeriodoIni, iPeriodoFinal );
                                        if  ( oLookup.Tag = K_FINAL ) then
                                        begin
                                             TZetaKeyLookup_DevEx( TPanel(Controls[i]).Controls[c]).Valor:= iPeriodoFinal;
                                        end
                                        else
                                        begin
                                             TZetaKeyLookup_DevEx( TPanel(Controls[i]).Controls[c] ).Valor:= iPeriodoIni;
                                        end;
                                        //end;
                                        oLookup.LookupDataset := dmCatalogos.cdsPeriodoOtro; //Asigna evento
                                   end;
                              end;
                         end;
                    end;
              end;
         end;
    end;
begin
     FPanelesVisible:= 0;
     SetFiltroPeriodo;
     AsignaLookupsRNominas;
     FMesAnterior:= MesDefault.Valor;
end;

procedure TWizNomCalcularPagoFonacot_DevEx.SetFiltroPeriodo;
var
   iYear : Integer;
begin
     iYear := YearDefaultAct.ValorEntero;
     with dmCatalogos do
     begin
          with cdsPeriodoOtro do
          begin
               if ( IsEmpty ) or
                  ( iYear <> FieldByName( 'PE_YEAR' ).AsInteger ) then
               begin
                    GetDatosPeriodoOtro( iYear );
               end;
          end;
     end;
end;

//(@am): Esta validacion no estaba terminada solo se aplicaba a nominas semanales, no consideraba Semana A o B
{procedure TWizNomCalcularPagoFonacot_DevEx.SemanalIniValidKey(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsPeriodoOtro.Locate('PE_NUMERO', SemanalIni.Llave, [] );
end;}

end.
