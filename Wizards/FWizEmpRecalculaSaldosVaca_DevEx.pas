unit FWizEmpRecalculaSaldosVaca_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, FWizEmpBaseFiltro, Mask, ZetaFecha, ComCtrls, 
  ZetaEdit, StdCtrls, Buttons, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, ExtCtrls, ZetaKeyLookup_DevEx,
  cxButtons, cxRadioGroup, cxTextEdit, cxMemo, cxGroupBox, cxLabel,
  dxGDIPlusClasses, cxImage, dxCustomWizardControl, dxWizardControl;

type
  TWizEmpRecalculaSaldosVaca_DevEx = class(TWizEmpBaseFiltro)
    Label1: TLabel;
    FechaRef: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure FormShow(Sender: TObject); //DevEx(by am): Se agrega para validar el enfoque de controles.
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
    procedure CargaListaVerificacion; override;
    function Verificar: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizEmpRecalculaSaldosVaca_DevEx: TWizEmpRecalculaSaldosVaca_DevEx;

const
     //Constantes Paginas
     K_PAGE_PARAMETROS =0;

implementation

{$R *.DFM}

uses DCliente, Dprocesos, FEmpRecalculaSaldosVacaGridSelect_DevEx, ZBaseSelectGrid_DevEx, ZetaCommonClasses;

procedure TWizEmpRecalculaSaldosVaca_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FechaRef.Valor := dmCliente.FechaDefault;
     HelpContext:= H_EMP_RECALCULA_SALDOS_VACA ;
     ParametrosControl := FechaRef;
     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := K_PAGE_PARAMETROS;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizEmpRecalculaSaldosVaca_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with Descripciones do
     begin
          AddDate( 'Fecha de Referencia', FechaRef.Valor );
     end;
     with ParameterList do
     begin
          AddDate( 'Fecha', FechaRef.Valor );
     end;
end;

procedure TWizEmpRecalculaSaldosVaca_DevEx.CargaListaVerificacion;
begin
     dmProcesos.RecalculaSaldosVacaGetLista( ParameterList );
end;

function TWizEmpRecalculaSaldosVaca_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.RecalculaSaldosVaca( ParameterList, Verificacion );
end;

function TWizEmpRecalculaSaldosVaca_DevEx.Verificar: Boolean;
begin
     with dmProcesos do
     begin
          Result := ZBaseSelectGrid_DevEx.GridSelect( cdsDataset, TEmpRecalculaSaldosVacaGridSelect_DevEx );
     end;
end;

procedure TWizEmpRecalculaSaldosVaca_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual( Parametros )then
         ParametrosControl := FechaRef
     else
         ParametrosControl := nil;// Si la pagina es distinta no se necesita enfocar nada.
     inherited;

end;

procedure TWizEmpRecalculaSaldosVaca_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Se recalcularán los saldos de vacaciones.';
end;

end.
