inherited WizNomPagoRecibos_DevEx: TWizNomPagoRecibos_DevEx
  Left = 422
  Top = 207
  Caption = 'Importar Pago de Recibos'
  ClientHeight = 450
  ClientWidth = 461
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 461
    Height = 450
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que modifica el estatus de pagado a no pagado de los rec' +
        'ibos que corresponden a un per'#237'odo de n'#243'mina.'
      Header.Title = 'Importar pago de recibos'
      inherited GroupBox1: TGroupBox
        Top = 9
        Width = 427
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 215
        Width = 439
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 439
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 371
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      PageVisible = False
      inherited sCondicionLBl: TLabel
        Left = 30
      end
      inherited sFiltroLBL: TLabel
        Left = 55
      end
      inherited Seleccionar: TcxButton
        Left = 158
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 82
      end
      inherited sFiltro: TcxMemo
        Left = 82
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 82
      end
      inherited bAjusteISR: TcxButton
        Left = 396
      end
    end
    object Parametros2: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        ' Complete los par'#225'metros para indicar la informaci'#243'n del proceso' +
        '.'
      Header.Title = 'Par'#225'metros'
      object PanelAnimation: TPanel
        Left = 0
        Top = 185
        Width = 439
        Height = 125
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Animate: TAnimate
          Left = 0
          Top = 0
          Width = 439
          Height = 108
          Align = alClient
          Active = True
          AutoSize = False
          CommonAVI = aviCopyFiles
          StopFrame = 1
        end
        object PanelMensaje: TPanel
          Left = 0
          Top = 108
          Width = 439
          Height = 17
          Align = alBottom
          BevelOuter = bvNone
          Caption = 'Verificando Archivo ...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 439
        Height = 185
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Label5: TLabel
          Left = 51
          Top = 25
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'A&rchivo:'
          FocusControl = Archivo
          Transparent = True
        end
        object Label8: TLabel
          Left = 10
          Top = 51
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = '&Tipo de Formato:'
          FocusControl = iFormato
          Transparent = True
        end
        object Label6: TLabel
          Left = 14
          Top = 147
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = '&Fecha de Pago:'
          FocusControl = fPagados
          Transparent = True
        end
        object Archivo: TEdit
          Left = 94
          Top = 21
          Width = 300
          Height = 21
          TabOrder = 0
          Text = 'RECIBOS.DAT'
        end
        object nPagados: TRadioGroup
          Left = 94
          Top = 72
          Width = 300
          Height = 65
          Caption = '&Marcar Como'
          ItemIndex = 0
          Items.Strings = (
            'Pagados'
            'No Pagados')
          TabOrder = 3
          OnClick = nPagadosClick
        end
        object iFormato: TZetaKeyCombo
          Left = 94
          Top = 47
          Width = 159
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 2
          ListaFija = lfFormatoASCII
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object fPagados: TZetaFecha
          Left = 94
          Top = 144
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 4
          Text = '22/feb/99'
          Valor = 36213.000000000000000000
          OnValidDate = ParametrosChange
        end
        object BGuardaArchivo: TcxButton
          Left = 396
          Top = 21
          Width = 21
          Height = 21
          Hint = 'Buscar Archivo de Importaci'#243'n'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BGuardaArchivoClick
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
            FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
            FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
            F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
            F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
            F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
            F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
            FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          PaintStyle = bpsGlyph
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 291
  end
  object OpenDialog: TOpenDialog
    Filter = 
      'Archivos de Datos (*.dat)|*.dat|Archivos de Texto (*.txt)|*.txt|' +
      'Todos (*.*)|*.*'
    Options = [ofHideReadOnly]
    Title = 'Seleccione el Archivo a Importar'
    Left = 428
    Top = 78
  end
end
