inherited WizNomISPTAnualPago_DevEx: TWizNomISPTAnualPago_DevEx
  Caption = 'Diferencias ISR a N'#243'mina'
  ClientHeight = 486
  ClientWidth = 486
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 486
    Height = 486
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Complete los par'#225'metros para concluir la informaci'#243'n del proceso' +
        '.'
      Header.Title = 'Par'#225'metros'
      object CreditoSalarioLBL: TLabel [0]
        Left = 22
        Top = 109
        Width = 128
        Height = 13
        Alignment = taRightJustify
        Caption = 'Concep&to cr'#233'dito al salario:'
        FocusControl = CreditoSalario
        Transparent = True
      end
      object ISPTNetoLBL: TLabel [1]
        Left = 54
        Top = 84
        Width = 96
        Height = 13
        Alignment = taRightJustify
        Caption = 'Conce&pto ISR Neto:'
        FocusControl = ISPTNeto
        Transparent = True
      end
      object ACargoLBL: TLabel [2]
        Left = 62
        Top = 59
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Caption = 'Co&ncepto a cargo:'
        FocusControl = ACargo
        Transparent = True
      end
      object AFavorLBL: TLabel [3]
        Left = 65
        Top = 34
        Width = 85
        Height = 13
        Alignment = taRightJustify
        Caption = 'C&oncepto a favor:'
        FocusControl = AFavor
        Transparent = True
      end
      inherited GroupBox1: TGroupBox
        Left = 16
        Top = 137
        Width = 435
        Align = alCustom
        TabOrder = 4
      end
      object CreditoSalario: TZetaKeyLookup_DevEx
        Left = 153
        Top = 105
        Width = 300
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        LookupDataset = dmCatalogos.cdsConceptos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
      end
      object ISPTNeto: TZetaKeyLookup_DevEx
        Left = 153
        Top = 80
        Width = 300
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        LookupDataset = dmCatalogos.cdsConceptos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
      end
      object ACargo: TZetaKeyLookup_DevEx
        Left = 153
        Top = 55
        Width = 300
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        LookupDataset = dmCatalogos.cdsConceptos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
      object AFavor: TZetaKeyLookup_DevEx
        Left = 153
        Top = 30
        Width = 300
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        LookupDataset = dmCatalogos.cdsConceptos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 251
        Width = 464
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 464
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 396
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 38
        Top = 161
      end
      inherited sFiltroLBL: TLabel
        Left = 63
        Top = 190
      end
      inherited Seleccionar: TcxButton
        Left = 166
        Top = 278
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 90
        Top = 158
      end
      inherited sFiltro: TcxMemo
        Left = 90
        Top = 189
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 90
        Top = 28
      end
      inherited bAjusteISR: TcxButton
        Left = 404
        Top = 190
      end
    end
    object Inicio: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        'Este proceso obtiene las diferencias de ISR de cada empleado y l' +
        'as refleja en una n'#243'mina o en un pr'#233'stamo.'
      Header.Title = 'Diferencias ISR a n'#243'mina'
      object Label5: TLabel
        Left = 192
        Top = 64
        Width = 22
        Height = 13
        Alignment = taRightJustify
        Caption = '&A'#241'o:'
        FocusControl = Year
        Transparent = True
      end
      object Metodo: TRadioGroup
        Left = 176
        Top = 88
        Width = 121
        Height = 60
        Caption = ' Enviar diferencias a'
        ItemIndex = 0
        Items.Strings = (
          'N'#243'mina'
          'Pr'#233'stamos')
        TabOrder = 1
        OnClick = MetodoClick
      end
      object Year: TZetaNumero
        Left = 218
        Top = 60
        Width = 56
        Height = 21
        Mascara = mnDias
        TabOrder = 0
        Text = '0'
        OnChange = YearChange
      end
    end
    object DatosPrestamo: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        'Complete los par'#225'metros para concluir la informaci'#243'n del proceso' +
        '.'
      Header.Title = 'Par'#225'metros'
      object Label12: TLabel
        Left = 6
        Top = 216
        Width = 127
        Height = 13
        Alignment = taRightJustify
        Caption = 'F'#243'r&mula de compensaci'#243'n:'
        Transparent = True
      end
      object Label11: TLabel
        Left = 79
        Top = 192
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = '&Referencia:'
        FocusControl = PReferencia
        Transparent = True
      end
      object Label10: TLabel
        Left = 44
        Top = 168
        Width = 88
        Height = 13
        Caption = 'Compensar &desde:'
        FocusControl = PInicio
        Transparent = True
      end
      object Label9: TLabel
        Left = 8
        Top = 143
        Width = 126
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pr'#233'stamo cr'#233'dito al &salario:'
        FocusControl = PCreditoSalario
        Transparent = True
      end
      object Label8: TLabel
        Left = 40
        Top = 119
        Width = 94
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pr'#233'stamo &ISR Neto:'
        FocusControl = PISPTNeto
        Transparent = True
      end
      object Label7: TLabel
        Left = 48
        Top = 95
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pr'#233'stamo a &cargo:'
        FocusControl = PACargo
        Transparent = True
      end
      object Label6: TLabel
        Left = 51
        Top = 70
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pr'#233'stamo a &favor:'
        FocusControl = PAFavor
        Transparent = True
      end
      object PCreditoSalario: TZetaKeyLookup_DevEx
        Left = 137
        Top = 140
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsTPresta
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
      end
      object PISPTNeto: TZetaKeyLookup_DevEx
        Left = 137
        Top = 115
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsTPresta
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
      end
      object PACargo: TZetaKeyLookup_DevEx
        Left = 137
        Top = 91
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsTPresta
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
      object PAFavor: TZetaKeyLookup_DevEx
        Left = 137
        Top = 67
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsTPresta
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
      object PReferencia: TZetaEdit
        Left = 138
        Top = 188
        Width = 121
        Height = 21
        TabOrder = 5
      end
      object PInicio: TZetaFecha
        Left = 138
        Top = 164
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 4
        Text = '06/dic/99'
        Valor = 36500.000000000000000000
      end
      object PFormulaBtn: TcxButton
        Left = 414
        Top = 213
        Width = 21
        Height = 21
        Hint = 'Abrir constructor de f'#243'rmulas'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = PFormulaBtnClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFECD5BFFFF0DE
          CDFFEFDCCBFFDCB28BFFD8AA7FFFE4C5A7FFF2E3D5FFDAAE85FFD8AA7FFFEEDA
          C7FFEAD2BBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFECD5BFFFE1BD9BFFDCB38DFFF8EFE7FFD9AB81FFDAAE85FFDAAE
          85FFE7CBB1FFDAAE85FFFDFBF9FFDDB58FFFDEB793FFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF6EB
          E1FFE6C9ADFFD8AA7FFFD8AA7FFFDAAF87FFEEDAC7FFF6EBE1FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFECD5BFFFF4E7DBFFD8AA7FFFD8AA7FFFD8AA7FFFEDD7
          C3FFEDD7C3FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE4C3A5FFFEFCFBFFD9AD
          83FFD8AA7FFFD8AA7FFFF5EADFFFF0DFCFFFD9AB81FFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFDAAF87FFFFFFFFFFE2BF9FFFDAAE85FFE1BE9DFFFDFBF9FFDCB38DFFECD5
          BFFFEDD7C3FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF9F3EDFFECD5BFFFD8AA7FFFDEB6
          91FFDFBA97FFD8AA7FFFDAAE85FFDFBA97FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF1E0
          D1FFF4E7DBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFE9CFB7FFFDFBF9FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE0BB99FFFFFFFFFFDFBA
          97FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAF
          87FFDEB793FFFFFFFFFFECD5BFFFDDB58FFFDAAF87FFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFE2BF9FFFECD5BFFFF9F2EBFFF9F2EBFFECD5BFFFE2BF
          9FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE9CF
          B7FFF9F2EBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFDCB28BFFFEFCFBFFDCB38DFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFEAD2BBFFECD6
          C1FFD8AA7FFFF5E8DDFFE2BF9FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFEBD3BDFFE9CFB7FFF3E4D7FFDBB189FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      end
      object PFormula: TcxMemo
        Left = 138
        Top = 212
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Courier New'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 7
        Height = 61
        Width = 273
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
