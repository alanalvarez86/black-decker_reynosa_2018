unit FWizEmpRenovacionSGM_DevEx;

interface 

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, ZetaDBTextBox, ZetaKeyCombo, Mask,
  ZetaNumero, ZetaFecha, FWizEmpBaseFiltro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizEmpRenovacionSGM_DevEx = class(TWizEmpBaseFiltro)
    PolizaActualGroup: TGroupBox;
    PanelSuperior: TPanel;
    CodigoPolizaLookup: TZetaKeyLookup_DevEx;
    LblCodigo: TLabel;
    LblPoliza: TLabel;
    LblReferencia: TLabel;
    LblVigencia: TLabel;
    LblCondiciones: TLabel;
    Poliza: TZetaTextBox;
    ReferenciaDropDown: TZetaKeyCombo;
    LblReferenciaActual: TLabel;
    LblRenovarDependientes: TLabel;
    LblObservaciones: TLabel;
    ReferenciaActualDropDown: TZetaKeyCombo;
    LblVigenciaA: TLabel;
    Observaciones: TcxMemo;
    VigenciaInicio: TZetaTextBox;
    VigenciaFin: TZetaTextBox;
    Memo1: TcxMemo;
    chkAplicaTabla: TCheckBox;
    Label1: TLabel;
    lookupTablasAmortizacion: TZetaKeyLookup_DevEx;
    lblTablaAmort: TLabel;
    grpPorcentaje: TGroupBox;
    txtPorcTitulares: TZetaNumero;
    lblTitulares: TLabel;
    lblDependientes: TLabel;
    txtPorcDependiente: TZetaNumero;
    lstRenovar: TZetaKeyCombo;
    gbTopes: TGroupBox;
    lblTopeEmpresa: TLabel;
    lblTopeEmpleado: TLabel;
    TopeEmpresa: TZetaNumero;
    TopeEmpleado: TZetaNumero;
    lblLstDiferencia: TLabel;
    lstDiferencia: TZetaKeyCombo;
    FechaRef: TZetaFecha;
    lblFechaRef: TLabel;
    procedure CodigoPolizaLookupExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure CodigoPolizaLookupValidKey(Sender: TObject);
    procedure ReferenciaDropDownChange(Sender: TObject);
    procedure chkAplicaTablaClick(Sender: TObject);
    procedure TopeEmpresaExit(Sender: TObject);
    procedure TopeEmpleadoExit(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    procedure LimpiaComponentes;
    { Private declarations }
  protected
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpRenovacionSGM_DevEx: TWizEmpRenovacionSGM_DevEx;

implementation

uses
    ZetaCommonClasses,
    ZetaCommonTools,
    dCatalogos, 
    DBClient,
    DProcesos,
    ZBaseSelectGrid_DevEx,
    FEmpSGMGridSelect_DevEx, ZetaClientDataSet, DateUtils;

{$R *.dfm}
procedure TWizEmpRenovacionSGM_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_RENOVAR_SGM;
     ParametrosControl := CodigoPolizaLookup;
     WizardControl.ActivePage := Parametros;
end;

procedure TWizEmpRenovacionSGM_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  with dmCatalogos do
     begin
          cdsSegGastosMed.Conectar;
          cdsVigenciasSGM.Conectar;
          cdsTablasAmortizacion.Conectar;
          CodigoPolizaLookup.LookupDataset := cdsSegGastosMed;
          lookupTablasAmortizacion.LookupDataset := cdsTablasAmortizacion;
          chkAplicaTabla.Checked := False; 
     end;
end;

procedure TWizEmpRenovacionSGM_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   sMensaje : String;
begin
    ParametrosControl := nil;
     inherited;
     if Wizard.Adelante then
     begin
          //PARAMETROS
          if WizardControl.ActivePage = Parametros then
               if StrVacio( Poliza.Caption ) then
                    CanMove := Error(' � No hay P�liza Seleccionada ! ', CodigoPolizaLookup )
               else
                   if StrVacio(VigenciaInicio.Caption) or StrVacio(VigenciaFin.Caption) then
                      CanMove := Error(' � No hay Referencia Seleccionada ! ', ReferenciaDropDown )
               else
                   if ( chkAplicaTabla.Checked and StrVacio(lookupTablasAmortizacion.Llave ) ) then
                      CanMove := Error(' � No hay Tabla de Cotizaci�n Seleccionada! ', lookupTablasAmortizacion );
          if WizardControl.ActivePage = FiltrosCondiciones then
          begin
               sMensaje :='Al aplicar este proceso se renovar�n las polizas para asegurados indicados %s' ;
               if lstRenovar.ItemIndex > 1 then
                    sMensaje := Format( sMensaje, [' y sus parientes.' ] )
               else
                    sMensaje := Format( sMensaje, [ '.' ] );
               Advertencia.Caption := sMensaje
          end;     
     end;
end;

procedure TWizEmpRenovacionSGM_DevEx.LimpiaComponentes;
begin
     with ReferenciaDropDown do
     begin
          Items.Clear;
          Enabled := False;
     end;
     with ReferenciaActualDropDown do
     begin
          Items.Clear;
          Enabled := False;
     end;
     VigenciaInicio.Caption := VACIO;
     VigenciaFin.Caption := VACIO;
     Memo1.Lines.Clear;
end;


procedure TWizEmpRenovacionSGM_DevEx.CodigoPolizaLookupValidKey(Sender: TObject);
     procedure LlenaCombo;
     begin
          LimpiaComponentes;
          with dmCatalogos.cdsVigenciasSGM do
          begin
               Filter := Format( 'PM_CODIGO = %s', [ EntreComillas( dmCatalogos.cdsSegGastosMed.FieldByName('PM_CODIGO').AsString ) ] );
               Filtered := True;
               ReferenciaDropDown.Enabled := True;
               ReferenciaActualDropDown.Enabled := True;
               ReferenciaDropDown.Items.Clear;
               First;
               while ( not eof ) do
               begin
                    ReferenciaDropDown.Items.Insert(0, FieldByName('PV_REFEREN').AsString);
                    ReferenciaActualDropDown.Items.Insert(0, FieldByName('PV_REFEREN').AsString);

                    if (FieldByName('PV_FEC_FIN').AsDateTime <= Today) then
                         ReferenciaActualDropDown.ItemIndex := 0;

                    if (FieldByName('PV_FEC_FIN').AsDateTime >= Today) and (FieldByName('PV_FEC_INI').AsDateTime <= Today) then
                    begin
                         ReferenciaDropDown.ItemIndex := 0;
                         ReferenciaDropDownChange(nil);
                    end;
                    Next;
               end;
          end;
     end;
begin
     inherited;
     with dmCatalogos do
     begin
          if StrLleno( CodigoPolizaLookup.Llave ) then
          begin
               Poliza.Caption := cdsSegGastosMed.FieldByName('PM_NUMERO').AsString;
               with cdsVigenciasSGM do
               begin
                    Filter := Format( 'PM_CODIGO = %s', [ EntreComillas( cdsSegGastosMed.FieldByName('PM_CODIGO').AsString ) ] );
                    Filtered := True;
                    if ( Recordcount >= 2 ) then
                         LlenaCombo()
                    else
                    begin
                         Error( '� No Hay Referencias en la P�liza Seleccionada. Deber� tener como M�nimo 2 !', CodigoPolizaLookup );
                         LimpiaComponentes;
                         FocusControl(CodigoPolizaLookup);
                    end;
                    Filtered := False;
               end;
          end;
     end;
end;

procedure TWizEmpRenovacionSGM_DevEx.ReferenciaDropDownChange(Sender: TObject);
var
   sReferenciaSeleccionada : String;
begin
     inherited;
     sReferenciaSeleccionada := ReferenciaDropDown.Items[ReferenciaDropDown.ItemIndex];
     with dmCatalogos.cdsVigenciasSGM, ReferenciaDropDown do
          if ( (ItemIndex >= 0) and
             (dmCatalogos.cdsVigenciasSGM.Locate( 'PV_REFEREN',  sReferenciaSeleccionada, [] ) )) then
          begin
               VigenciaInicio.Caption := FieldByName( 'PV_FEC_INI' ).AsString;
               VigenciaFin.Caption := FieldByName( 'PV_FEC_FIN' ).AsString;
               Memo1.Text := FieldByName( 'PV_CONDIC' ).AsString;
               FechaRef.Valor := FieldByName( 'PV_FEC_INI' ).AsDateTime;
          end;
end;

procedure TWizEmpRenovacionSGM_DevEx.CodigoPolizaLookupExit(Sender: TObject);
begin
     inherited;
     If ( CodigoPolizaLookup.Llave = VACIO ) then
          LimpiaComponentes;
end;

procedure TWizEmpRenovacionSGM_DevEx.CargaParametros;
var
   dFechaInicio, dFechaFin : TDate;
begin
     inherited;
     with ParameterList, dmCatalogos do
     begin
          AddString( 'PM_CODIGO', CodigoPolizaLookup.Llave );
          AddString( 'PM_DESCRIP', CodigoPolizaLookup.Descripcion );
          AddString( 'Numero', Poliza.Caption );

          AddString( 'PV_REFEREN', ReferenciaActualDropDown.Text );
          AddInteger( 'TipoAsegura', lstRenovar.ItemIndex );
          AddString( 'Observaciones', Observaciones.Text );

          cdsVigenciasSGM.Locate( 'PV_REFEREN',  ReferenciaActualDropDown.Text, [] );
          AddString( 'ReferenciaActual', ReferenciaActualDropDown.Text ); //Referencia Actual
          dFechaInicio := cdsVigenciasSGM.FieldByName( 'PV_FEC_INI' ).AsDateTime;
          dFechaFin := cdsVigenciasSGM.FieldByName( 'PV_FEC_FIN' ).AsDateTime;
          AddDate( 'FechaInicio', dFechaInicio );
          AddDate( 'FechaFin', dFechaFin );

          cdsVigenciasSGM.Locate( 'PV_REFEREN',  ReferenciaDropDown.Text, [] );
          AddString( 'ReferenciaNueva', ReferenciaDropDown.Text ); //Referencia Nueva
          AddDate( 'NuevaFechaInicio', cdsVigenciasSGM.FieldByName( 'PV_FEC_INI' ).AsDateTime );
          AddDate( 'NuevaFechaFin', cdsVigenciasSGM.FieldByName( 'PV_FEC_FIN' ).AsDateTime );
          AddInteger('Asegurado',lstRenovar.ItemIndex);

          AddBoolean('AplicaTabla',chkAplicaTabla.Checked );
          AddString('TablaAmortizacion',lookupTablasAmortizacion.Llave );
          AddString('TablaDescrip',lookupTablasAmortizacion.Descripcion );
          AddFloat('AumentoTitular',txtPorcTitulares.Valor);
          AddFloat('AumentoDependiente',txtPorcDependiente.Valor);
          AddDate('FechaRef',FechaRef.Valor );
          AddFloat('TopeEmpresa',TopeEmpresa.Valor);
          AddFloat('TopeEmpleado',TopeEmpleado.Valor);
          AddInteger('Diferencia',lstDiferencia.ItemIndex);
     end;
      with Descripciones do
     begin
          AddString( 'C�digo', CodigoPolizaLookup.Llave );
          AddString( 'P�liza', Poliza.Caption  );

          AddString( 'Referencia Actual', ReferenciaActualDropDown.Text );
          AddString( 'Tipo de Asegurado', lstRenovar.Text );

          AddBoolean('Aplicar Tabla',chkAplicaTabla.Checked );
          if( chkAplicaTabla.Checked  ) then
          begin
            AddFloat('Aumento Titular',txtPorcTitulares.Valor);
            AddFloat('Aumento Dependiente',txtPorcDependiente.Valor);
            AddDate('Fecha de Referencia',FechaRef.Valor );
            AddFloat('Tope de Costo de Empresa',TopeEmpresa.Valor);
            AddFloat('Tope de Costo de Empleado',TopeEmpleado.Valor);
            AddString('Diferencia de Costo',lstDiferencia.Text);
          end;
           AddString( 'Observaciones', Observaciones.Text );
     end;
end;


function TWizEmpRenovacionSGM_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpSGMGridSelect_DevEx );
end;

procedure TWizEmpRenovacionSGM_DevEx.CargaListaVerificacion;
begin
     inherited;
     dmProcesos.RenovacionSGMGetLista( ParameterList );
end;

function TWizEmpRenovacionSGM_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.RenovacionSGM( ParameterList, Verificacion );
end;

procedure TWizEmpRenovacionSGM_DevEx.chkAplicaTablaClick(Sender: TObject);
begin
     inherited;
     lblTablaAmort.Enabled :=  chkAplicaTabla.Checked;
     grpPorcentaje.Enabled :=  chkAplicaTabla.Checked;
     lookupTablasAmortizacion.Enabled :=  chkAplicaTabla.Checked;
     txtPorcTitulares.Enabled :=  chkAplicaTabla.Checked;
     txtPorcDependiente.Enabled :=  chkAplicaTabla.Checked;
     lblDependientes.Enabled :=  chkAplicaTabla.Checked;
     lblTitulares.Enabled :=  chkAplicaTabla.Checked;
     FechaRef.Enabled := chkAplicaTabla.Checked;
     lblFechaRef.Enabled:= chkAplicaTabla.Checked;
     gbTopes.Enabled := chkAplicaTabla.Checked;
     lblTopeEmpresa.Enabled := chkAplicaTabla.Checked;
     TopeEmpresa.Enabled := chkAplicaTabla.Checked;
     lblTopeEmpleado.Enabled := chkAplicaTabla.Checked;
     TopeEmpleado.Enabled := chkAplicaTabla.Checked;
     lblLstDiferencia.Enabled := chkAplicaTabla.Checked;
     lstDiferencia.Enabled := chkAplicaTabla.Checked;
end;


procedure TWizEmpRenovacionSGM_DevEx.TopeEmpresaExit(Sender: TObject);
begin
     inherited;
     if TopeEmpresa.Valor > 0 then
     begin
          lstDiferencia.Enabled := False;
          lblLstDiferencia.Enabled := False;
          TopeEmpleado.Valor := 0
     end
     else if TopeEmpleado.Valor = 0 then
     begin
          lstDiferencia.Enabled := True;
          lblLstDiferencia.Enabled := True;
     end;
end;

procedure TWizEmpRenovacionSGM_DevEx.TopeEmpleadoExit(Sender: TObject);
begin
     inherited;
      if TopeEmpleado.Valor > 0 then
     begin
          lstDiferencia.Enabled := False;
          lblLstDiferencia.Enabled := False;
          TopeEmpresa.Valor := 0
     end
     else if TopeEmpresa.Valor = 0 then
     begin
          lstDiferencia.Enabled := True;
          lblLstDiferencia.Enabled := True;
     end;
end;

procedure TWizEmpRenovacionSGM_DevEx.WizardAfterMove(Sender: TObject);
begin
    inherited;
  if Wizard.EsPaginaActual(Parametros) then
  ParametrosControl := CodigoPolizaLookup;
end;

end.
