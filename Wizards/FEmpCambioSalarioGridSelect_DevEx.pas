unit FEmpCambioSalarioGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Db, ZBaseSelectGrid_DevEx, cxGraphics,
     cxLookAndFeels, cxLookAndFeelPainters, Menus,
     dxSkinsCore,  TressMorado2013,
     dxSkinsDefaultPainters, cxControls, cxStyles,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxNavigator, cxDBData, cxTextEdit, cxGridCustomTableView,
     cxGridTableView, cxGridDBTableView, ImgList, cxGridLevel, cxClasses,
     cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEmpCambioSalarioGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_SALARIO: TcxGridDBColumn;
    NUEVO_SAL: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpCambioSalarioGridSelect_DevEx: TEmpCambioSalarioGridSelect_DevEx;

implementation

uses ZBasicoSelectGrid_DevEx;

{$R *.DFM}

procedure TEmpCambioSalarioGridSelect_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with DataSet do
     begin
          MaskPesos( 'CB_SALARIO' );
          MaskPesos( 'NUEVO_SAL' );
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
