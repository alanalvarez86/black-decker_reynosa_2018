unit FImportarImagenGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Grids, DBGrids, ZetaDBGrid,
  Buttons, ExtCtrls, ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons;

type
  TImportarImagenGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ImportarImagenGridSelect_DevEx: TImportarImagenGridSelect_DevEx;

implementation

{$R *.dfm}

procedure TImportarImagenGridSelect_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  {***(@am): Se remueve el gridmode de esta forma ya que el dataset se modifica externamente al modificar los archivos del folder donde estan las imagenes***}
  ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= False;
end;

end.
