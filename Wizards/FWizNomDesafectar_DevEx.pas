unit FWizNomDesafectar_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ComCtrls, StdCtrls, ExtCtrls, Buttons,
     FWizNomBase_DevEx,
     ZetaWizard,
     ZetaDBTextBox,
     ZetaEdit, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomDesafectar_DevEx = class(TWizNomBase_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean;override;
  public
    { Public declarations }
  end;

var
  WizNomDesafectar_DevEx: TWizNomDesafectar_DevEx;

implementation

uses ZetaCommonClasses,
     DProcesos,
     FTressShell;

{$R *.DFM}

procedure TWizNomDesafectar_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H30349_Desafectar_nomina;
     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

function TWizNomDesafectar_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.DesafectarNomina(ParameterList);
end;

procedure TWizNomDesafectar_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'El proceso resta de los acumulados, los montos del periodo previamente afectado. Al desafectar una n�mina, su estatus pasa a ''Calculada total'' lo que permite modificarla y recalcularla.'; 
end;

end.
