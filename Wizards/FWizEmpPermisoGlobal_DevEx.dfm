inherited WizEmpPermisoGlobal_DevEx: TWizEmpPermisoGlobal_DevEx
  Left = 314
  Top = 200
  Caption = 'Permisos Globales'
  ClientHeight = 445
  ClientWidth = 468
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 468
    Height = 445
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que registra permisos a un grupo de empleados en un rang' +
        'o de fechas.'
      Header.Title = 'Permisos Globales'
      object LIN_FEC_INI: TLabel
        Left = 89
        Top = 19
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha Inicio:'
        FocusControl = FechaIni
        Transparent = True
      end
      object LIN_DIAS: TLabel
        Left = 124
        Top = 49
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = '&D'#237'as:'
        FocusControl = Dias
        Transparent = True
      end
      object LIN_FEC_FIN: TLabel
        Left = 107
        Top = 78
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Regresa:'
        Transparent = True
      end
      object FechaFin: TZetaTextBox
        Left = 155
        Top = 76
        Width = 80
        Height = 17
        AutoSize = False
        Caption = 'FechaFin'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label3: TLabel
        Left = 121
        Top = 108
        Width = 29
        Height = 13
        Caption = 'C&lase:'
        FocusControl = Clase
        Transparent = True
      end
      object Label13: TLabel
        Left = 126
        Top = 138
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = '&Tipo:'
        FocusControl = TipoPermiso
        Transparent = True
      end
      object Label11: TLabel
        Left = 95
        Top = 170
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Referenc&ia:'
        FocusControl = Referencia
        Transparent = True
      end
      object Label5: TLabel
        Left = 76
        Top = 199
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = '&Observaciones:'
        FocusControl = Observaciones
        Transparent = True
      end
      object FechaIni: TZetaFecha
        Left = 155
        Top = 14
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '17/dic/97'
        Valor = 35781.000000000000000000
        OnValidDate = FechaIniValidDate
      end
      object Dias: TZetaNumero
        Left = 155
        Top = 45
        Width = 65
        Height = 21
        Mascara = mnDias
        TabOrder = 1
        Text = '0'
        OnExit = DiasExit
      end
      object Clase: TZetaKeyCombo
        Left = 155
        Top = 103
        Width = 185
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 3
        OnChange = ClaseChange
        OnExit = ClaseExit
        ListaFija = lfTipoPermiso
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object TipoPermiso: TZetaKeyLookup_DevEx
        Left = 155
        Top = 134
        Width = 275
        Height = 21
        LookupDataset = dmTablas.cdsIncidencias
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
      end
      object Referencia: TEdit
        Left = 155
        Top = 165
        Width = 130
        Height = 21
        MaxLength = 8
        TabOrder = 5
      end
      object Observaciones: TEdit
        Left = 155
        Top = 196
        Width = 275
        Height = 21
        MaxLength = 40
        TabOrder = 6
      end
      object lAjustarFechas: TcxCheckBox
        Left = 8
        Top = 224
        Caption = 'Ajustar Fechas En &Empalmes:'
        Properties.Alignment = taRightJustify
        State = cbsChecked
        TabOrder = 7
        Transparent = True
        Width = 163
      end
      object gbDiasFechaReg: TcxGroupBox
        Left = 176
        Top = 227
        TabOrder = 2
        Height = 60
        Width = 273
        object LIN_DIAS_HAB: TLabel
          Left = 58
          Top = 10
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = '&D'#237'as:'
          FocusControl = DiasHab
          Transparent = True
        end
        object LIN_FEC_REG: TLabel
          Left = 41
          Top = 39
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Regresa:'
          Transparent = True
        end
        object rbDias: TcxRadioButton
          Left = 16
          Top = 8
          Width = 17
          Height = 17
          TabOrder = 0
          OnClick = rbDiasClick
          Transparent = True
        end
        object rbFechaFin: TcxRadioButton
          Left = 16
          Top = 35
          Width = 17
          Height = 17
          TabOrder = 2
          OnClick = rbFechaFinClick
          Transparent = True
        end
        object FechaReg: TZetaFecha
          Left = 89
          Top = 34
          Width = 115
          Height = 22
          Cursor = crArrow
          Enabled = False
          TabOrder = 3
          Text = '20/dic/12'
          Valor = 41263.000000000000000000
          OnChange = FechaRegChange
        end
        object DiasHab: TZetaNumero
          Left = 89
          Top = 6
          Width = 65
          Height = 21
          Enabled = False
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
          OnExit = DiasExit
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 210
        Width = 446
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 446
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 378
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 22
      end
      inherited sFiltroLBL: TLabel
        Left = 47
      end
      inherited Seleccionar: TcxButton
        Left = 150
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 74
      end
      inherited sFiltro: TcxMemo
        Left = 74
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 74
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 448
    Top = 26
  end
end
