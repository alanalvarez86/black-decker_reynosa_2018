unit FWizAsistRecalcular_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, Mask,
     ZetaEdit,
     ZetaWizard,
     ZetaFecha, ZCXBaseWizardFiltro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters;

type
  TWizAsistRecalcular_DevEx = class(TBaseCXWizardFiltro)
    Cancelaciones: TdxWizardControlPage;
    gbFechasAsistencia: TcxGroupBox;
    dInicialLBL: TLabel;
    dFinalLBL: TLabel;
    FechaInicial: TZetaFecha;
    FechaFinal: TZetaFecha;
    gbClasificacionTarjeta: TcxGroupBox;
    lRevisa: TCheckBox;
    Reclasificar: TCheckBox;
    grReprocesarTarjetas: TcxGroupBox;
    lReprocesarTarjetas: TCheckBox;
    gbCancelaciones: TcxGroupBox;
    GroupBox1: TGroupBox;
    lAutExtras: TCheckBox;
    lAutDescanso: TCheckBox;
    lAutConGoce: TCheckBox;
    lAutSinGoce: TCheckBox;
    lAutHorasPrep: TCheckBox;
    lTemporales: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure lRevisaClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    procedure PermisosSeparados;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizAsistRecalcular_DevEx: TWizAsistRecalcular_DevEx;

implementation

uses ZetaCommonClasses,
     DAsistencia,
     DProcesos,
     DCliente,
     DSistema,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonTools;

{$R *.DFM}

procedure TWizAsistRecalcular_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := FechaInicial;
     with dmCliente do
     begin
          FechaInicial.Valor := FechaDefault;
          FechaFinal.Valor := FechaDefault;
     end;
     HelpContext := H20237_Recalcular_tarjetas;
     PermisosSeparados;
end;

procedure TWizAsistRecalcular_DevEx.PermisosSeparados;
var
     bPermiso : Boolean;
begin
     // (JB) Se asignan los nuevos derechos de D_ASIS_PROC_RECALC_TARJETA
     //      Derechos por separado para Revisar Clasificacion de Tarjeta
     //      Se revisa con el primer bit K_DERECHO_CONSULTA
     bPermiso := ZAccesosMgr.CheckDerecho( D_ASIS_PROC_RECALC_TARJETA,K_DERECHO_CONSULTA );
     lRevisa.Enabled := bPermiso;
     Reclasificar.Enabled := bPermiso;
     if not bPermiso then
     begin
          lRevisa.Checked := False;
          Reclasificar.Checked := False;
     end;
     // (JB) Por Bug 631-2610 y comentario de CMoreno se realiza modificacion
     lTemporales.Enabled := bPermiso and lRevisa.Checked;
     if ( not lRevisa.Checked ) then
     begin
          lTemporales.Enabled := false;
          lTemporales.Checked := false;
     end
     // Derechos por separado para Cancelar horarios temporales.
     // Se revisa con el tercer bit K_DERECHO_CAMBIO
     else
     begin
          bPermiso := ZAccesosMgr.CheckDerecho( D_ASIS_PROC_RECALC_TARJETA,K_DERECHO_CAMBIO );
          lTemporales.Enabled := bPermiso;
          if not bPermiso then
             lTemporales.Checked := False;
     end;

     //      Derechos por separado para Efectuar Cancelaciones
     //      Se revisa con el segundo bit K_DERECHO_ALTA
     bPermiso := ZAccesosMgr.CheckDerecho( D_ASIS_PROC_RECALC_TARJETA,K_DERECHO_ALTA );
     lAutExtras.Enabled := bPermiso;
     lAutDescanso.Enabled := bPermiso;
     lAutConGoce.Enabled := bPermiso;
     lAutSinGoce.Enabled := bPermiso;
     lAutHorasPrep.Enabled := bPermiso;
     if not bPermiso then
     begin
          lAutExtras.Checked := False;
          lAutDescanso.Checked := False;
          lAutConGoce.Checked := False;
          lAutSinGoce.Checked := False;
          lAutHorasPrep.Checked := False;
     end;
     //      Derechos por separado para Reprocesar Tarjetas
     //      Se revisa con el tercer bit K_DERECHO_BAJA
     bPermiso := ZAccesosMgr.CheckDerecho( D_ASIS_PROC_RECALC_TARJETA,K_DERECHO_BAJA );
     lReprocesarTarjetas.Enabled := bPermiso;
     if not bPermiso then
        lReprocesarTarjetas.Checked := False;

     Cancelaciones.PageVisible := TRUE;
     // Revisar si no es necesaria la p�gina de Cancelaciones.
     if (not ZAccesosMgr.CheckDerecho( D_ASIS_PROC_RECALC_TARJETA,K_DERECHO_ALTA )) and
        (not ZAccesosMgr.CheckDerecho( D_ASIS_PROC_RECALC_TARJETA,K_DERECHO_CAMBIO )) then
     begin
          Cancelaciones.PageVisible := FALSE;
     end;

end;

procedure TWizAsistRecalcular_DevEx.lRevisaClick(Sender: TObject);
begin
     inherited;
     PermisosSeparados;
end;

procedure TWizAsistRecalcular_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddDate( 'FechaInicial', FechaInicial.Valor );
          AddDate( 'FechaFinal', FechaFinal.Valor );
          AddBoolean( 'RevisarHorarios', lRevisa.Checked );
          AddBoolean( 'CancelarTemporales', lTemporales.Checked );
          AddBoolean( 'CancelarExtras', lAutExtras.Checked );
          AddBoolean( 'CancelarDescansos', lAutDescanso.Checked );
          AddBoolean( 'CancelarPermisosConGoce', lAutConGoce.Checked );
          AddBoolean( 'CancelarPermisosSinGoce', lAutSinGoce.Checked );
          AddBoolean( 'CancelarHorasPrepagadas', lAutHorasPrep.Checked );
          AddBoolean( 'Reclasificar', Reclasificar.Checked );
          //Derecho de Aprobar Autorizaciones
          AddBoolean( 'AprobarAutorizaciones', dmAsistencia.TieneDerechoAprobar );
          { Parametros del Reproceso de Tarjetas }
          AddBoolean( 'ReprocesarTarjetas', lReprocesarTarjetas.Checked );
          //Este parametro lo necesita el proceso de Tarjetas.
          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );

     end;
      with Descripciones do
     begin
          AddDate( 'Fecha Inicial', FechaInicial.Valor );
          AddDate( 'Fecha Final', FechaFinal.Valor );
          AddBoolean( 'Revisar Horarios', lRevisa.Checked );
          AddBoolean( 'Cancelar Temporales', lTemporales.Checked );
          AddBoolean( 'Cancelar Extras', lAutExtras.Checked );
          AddBoolean( 'Cancelar Descansos', lAutDescanso.Checked );
          AddBoolean( 'Cancelar Permisos C. Goce', lAutConGoce.Checked );
          AddBoolean( 'Cancelar Permisos S. Goce', lAutSinGoce.Checked );
          AddBoolean( 'Cancelar H. Prepagadas', lAutHorasPrep.Checked );
          AddBoolean( 'Reclasificar', Reclasificar.Checked );
          //Derecho de Aprobar Autorizaciones
          AddBoolean( 'Aprobar Autorizaciones', dmAsistencia.TieneDerechoAprobar );
          { Parametros del Reproceso de Tarjetas }
          AddBoolean( 'Reprocesar Tarjetas', lReprocesarTarjetas.Checked );
          //Este parametro lo necesita el proceso de Tarjetas.
          AddBoolean( 'Puede Cambiar Tarjeta', dmCliente.PuedeModificarTarjetasAnteriores );

     end;

end;

function TWizAsistRecalcular_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.RecalcularTarjetas( ParameterList );
end;

procedure TWizAsistRecalcular_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   sMensaje: String;
begin
     ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if ( lReprocesarTarjetas.Checked ) and ( StrVacio( dmSistema.GetDigitoEmpresa ) ) then
               begin
                    CanMove := Error( 'No se puede Reprocesar Tarjetas sino se tiene un  # D�gito asignado a la Empresa', lReprocesarTarjetas  );
               end
               else
               begin
                    if ( lReprocesarTarjetas.Checked ) then
                    begin
                         CanMove := dmCliente.PuedeCambiarTarjeta( FechaInicial.Valor - 1, FechaFinal.Valor + 1, 0, sMensaje );
                         if ( not CanMove ) then
                         begin
                              sMensaje := sMensaje + CR_LF + 'Al Reprocesar tarjetas se requiere revisar un d�a antes' + CR_LF + 'y un d�a despu�s del rango indicado';
                              ZetaDialogo.ZWarning( 'Mensaje', sMensaje, 0, mbYes );
                         end;
                    end
                    else
                        CanMove := dmCliente.PuedeCambiarTarjetaDlg( FechaInicial.Valor, FechaFinal.Valor );
               end
          end;
     end;
end;

procedure TWizAsistRecalcular_DevEx.WizardAfterMove(Sender: TObject);
const
//596
     K_H_1 = 475;
     K_H_2 = 605;
begin
     inherited;
     if Wizard.EsPaginaActual(Parametros) then
       ParametrosControl := FechaInicial;

//     if Height = K_H_2 then
//     begin
//          Top := Round (Top + ((K_H_2-K_H_1)/2));
//          Height := K_H_1;
//     end;
//     if Wizard.EsPaginaActual(Ejecucion) then
//     begin
//          Height := K_H_2;
//          Top := Round (Top - ((K_H_2-K_H_1)/2));
//     end;
end;

end.
