inherited WizSistCopiarConfiguracionCafeteria_DevEx: TWizSistCopiarConfiguracionCafeteria_DevEx
  Left = 384
  Top = 199
  Caption = 'Copiar Configuraci'#243'n'
  ClientHeight = 454
  ClientWidth = 451
  ExplicitWidth = 457
  ExplicitHeight = 483
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 451
    Height = 454
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
      7249000003CF494441545847B5963F68137114C76F70C8D0A1430707073B0876
      7013914E824B06070727117470D04D5CB248AEA2B56A6D93264DD2FB97CBE572
      97FBD3186A5B8B58A8884B2705B577AD60411011840E0A452AC6F7E25DFCE5EE
      97988BF5C187DF71F77BDFF7EEDDFBFD7EC7A0B19A3BC4569DC3FB86F5E66053
      B817634D672C69BA7BACE536F693A4E54E7A21BA5BD272765B4E9008386E9342
      FD825A5E88EED672C0446A5B87F05ED2DC9C20C5FAA519E06FE64F4E9ACE8A77
      8B618D8D93A450BF7872DDCD9FDCEC03D38D6343266DB74A0AF58B17A2BB41E0
      4F34E77F057477BC10DD8DB59C4B507E079B6FDF30DD77B0BAAE7821FECD1A8D
      C67F8561E5ED186BB867B11291B19DE35E9EFD5BD2DAACD3BE61377E37AC33C6
      AEAD1D78707392F11172A5F5ECFDDC77B86EA4C6D33FE7A6B92F1541BF4ECE09
      D25A05BDE2053F17145AB49731994610295B5C87B16D2E49E40460A95EA00969
      821E87319400546457961E0EC175C8078994007478019DCAC5DA113E2D7C5079
      3D6194EC13425E7D0AF743C17DD213991FF029129A581D56B9CA1DA55056E17E
      B4047069DD365E0E88423DC6A7B8CF64807EA870EA0D18B109DDD7B480210CF7
      1A3A94382D4D0AF5CBD4ADA9862E19C30C2E252C2DBCA1DC0DB6BE3D98139FC6
      66BD2EA7313755D8A9F05AC290AA71E8898B50EA85A95BD3D4B948794EE5BCC5
      D89BF1F2F2199A1022CF16D7E1FBC6E0BAADC974B13A9A9998F9817382E4EECF
      EE32F866B86D36D775276CE71426004E8CCA6B77A6A17CA450F65E76CF281A1D
      3BBD2A55AFC2D8EE7337B307F7E37018392BD46F4EE2EDEB2886C879E51929A6
      E44B0BFE331A589960D255C9C06AF6B80A60ED9309701969CB174274414BF8CF
      3A917F50F80663CBC792CDD378BFB7048C8DCB640262B6F8C21742A0998AFE33
      1AB3E26A2CD88CA66C454800FAC04F40158D44B09C1CEC0B8BE662DBB9405216
      CDF330B6F9881969C3D41E0DF69400FC2FAA9800272F86847C3A5521AFAE1E84
      A5BB0BD7219FB914FF11FF885ED282928C996FBFCE48CF63BAF678303D9EFA49
      1343E0ADB654C93AA3E92B838A5C3F221417AEA6EF66F7687311A5A0180C3BEF
      1E4DDA9BA9E0C643326EBC9245F5C9517062A0E36B41A17E9957EC11AC6CCF06
      4EB0A68D21DC4082625181B3A079204532744024B116CF4EF33B65AE928684CE
      E316EC0BD3C0A62DE5959A2EEAD7E127E5A33023BE5FB6979A4D1BC9D081C612
      FC8C940B0A366128787E32F7CD56E647E0BA351FCE8BD6961DC948912070001D
      833194809C6BF60CD507896434019F3A9414F6F7D0A1A3F195E6B9DF89484613
      08A24AF6107CEB51AB648DD8256B8036E70F93CC2FADD62E283A1FEEC1000000
      0049454E44AE426082}
    ExplicitWidth = 451
    ExplicitHeight = 454
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Especificar la cafeter'#237'a de la cual se van a tomar los datos de ' +
        'configuraci'#243'n, as'#237' como las cafeter'#237'as a las cuales se desea cop' +
        'iar la configuraci'#243'n.'
      Header.Title = 'Cafeter'#237'as a configurar'
      ExplicitWidth = 429
      ExplicitHeight = 314
      object gpConfiguracionDe: TGroupBox
        Left = 68
        Top = 3
        Width = 294
        Height = 73
        Caption = ' Copiar configuraci'#243'n de: '
        TabOrder = 0
        object Label1: TLabel
          Left = 24
          Top = 32
          Width = 105
          Height = 13
          Caption = 'Estaci'#243'n de cafeter'#237'a:'
        end
        object Estacion: TComboBox
          Left = 135
          Top = 30
          Width = 135
          Height = 21
          BevelOuter = bvNone
          Style = csDropDownList
          TabOrder = 0
        end
      end
      object gpConfiguracionA: TGroupBox
        Left = 68
        Top = 95
        Width = 294
        Height = 212
        Caption = ' Copiar configuraci'#243'n hacia: '
        TabOrder = 1
        object rdTodas: TRadioButton
          Left = 32
          Top = 24
          Width = 225
          Height = 17
          Caption = 'Todas las cafeter'#237'as registradas'
          TabOrder = 0
          OnClick = rdTodasClick
        end
        object rdNuevas: TRadioButton
          Left = 32
          Top = 47
          Width = 225
          Height = 17
          Caption = 'Cafeter'#237'as registradas sin configuraci'#243'n'
          Checked = True
          TabOrder = 1
          TabStop = True
          OnClick = rdNuevasClick
        end
        object rdLista: TRadioButton
          Left = 32
          Top = 70
          Width = 225
          Height = 17
          Caption = 'Lista de cafeter'#237'as '
          TabOrder = 2
          OnClick = rdListaClick
        end
        object listaEstaciones: TcxCheckListBox
          Left = 51
          Top = 93
          Width = 220
          Height = 97
          Enabled = False
          Items = <>
          ParentFont = False
          StyleDisabled.Color = clBtnFace
          StyleFocused.Color = clHighlightText
          StyleHot.Color = clHighlightText
          StyleHot.TextStyle = []
          TabOrder = 3
        end
      end
    end
    object Informacion: TdxWizardControlPage [1]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Indicar las pesta'#241'as de las cuales se tomar'#225'n los datos. En caso' +
        ' de no indicar una, se utilizar'#225' informaci'#243'n default.'
      Header.Title = 'Configuraci'#243'n a copiar'
      ParentFont = False
      object gpInformacionCopiar: TGroupBox
        Left = 21
        Top = 54
        Width = 389
        Height = 166
        Caption = ' Seleccionar informaci'#243'n a copiar: '
        TabOrder = 0
        object rdTodos: TRadioButton
          Left = 32
          Top = 24
          Width = 225
          Height = 17
          Caption = 'Copiar todos los datos de configuraci'#243'n'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = rdTodosClick
        end
        object rdCategoria: TRadioButton
          Left = 32
          Top = 47
          Width = 313
          Height = 17
          Caption = 'Seleccionar los datos de configuraci'#243'n a copiar por categor'#237'a'
          TabOrder = 1
          OnClick = rdCategoriaClick
        end
        object Categorias: TcxCheckListBox
          Left = 49
          Top = 70
          Width = 296
          Height = 75
          Enabled = False
          Items = <
            item
              Text = 'General'
            end
            item
              Text = 'Calendario'
            end
            item
              Text = 'Tipos de comida'
            end
            item
              Text = 'Seguridad'
            end>
          ParentFont = False
          TabOrder = 2
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.Title = 'Presione aplicar para iniciar el proceso'
      ExplicitWidth = 429
      ExplicitHeight = 314
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 429
        ExplicitHeight = 219
        Height = 219
        Width = 429
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 429
        Width = 429
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar el proceso se copiar'#225'n los datos de configuraci'#243'n sel' +
            'eccionados a las cafeter'#237'as indicadas. Algunos datos de configur' +
            'aci'#243'n deber'#225'n especificarse en la estaci'#243'n de cada cafeter'#237'a.'
          Style.IsFontAssigned = True
          ExplicitWidth = 361
          Width = 361
          AnchorY = 57
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 136
    Top = 426
  end
end
