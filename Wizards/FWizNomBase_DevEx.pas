unit FWizNomBase_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ComCtrls, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons,
     ZetaDBTextBox,
     ZetaDBGrid,
     ZetacxWizard,
     ZcxBaseWizardFiltro,
     ZetaEdit, ZetaWizard, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit, cxMemo,
  dxCustomWizardControl, dxWizardControl, cxGroupBox, cxLabel,
  dxGDIPlusClasses, cxImage, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters;

type
  TWizNomBase_DevEx = class(TBaseCXWizardFiltro)
    GroupBox1: TGroupBox;
    PeriodoTipoLbl: TLabel;
    iTipoNomina: TZetaTextBox;
    iNumeroNomina: TZetaTextBox;
    iMesNomina: TZetaTextBox;
    sDescripcion: TZetaTextBox;
    FechaInicial: TZetaTextBox;
    FechaFinal: TZetaTextBox;
    sStatusNomina: TZetaTextBox;
    Label4: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    PeriodoNumeroLBL: TLabel;
    Label1: TLabel;

    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);

  private
    { Private declarations }
  protected
    { Private declarations }
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomBase_DevEx: TWizNomBase_DevEx;

implementation

uses DCliente,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

procedure TWizNomBase_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          with dmCliente do
               AddString( 'Periodo', ShowNomina( YearDefault, Ord( PeriodoTipo ), PeriodoNumero ) );
     end;
end;

procedure TWizNomBase_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          with GetDatosPeriodoActivo do
          begin
               iNumeroNomina.Caption := IntToStr( Numero );
               iTipoNomina.Caption := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
               iMesNomina.Caption := ObtieneElemento( lfMeses, ( Mes - 1  ) );
               sStatusNomina.Caption := ZetaCommonTools.GetDescripcionStatusPeriodo( Status, StatusTimbrado );
               FechaInicial.Caption := FormatDateTime( {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}LongDateFormat, Fin );
          end;
     end;
end;

procedure TWizNomBase_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if dmCliente.GetDatosPeriodoActivo.Tipo = K_PERIODO_VACIO then
     begin
          zError( Caption, 'El tipo de n�mina es invalido.', 0 );
          CanMove := False;
     end;

end;

end.
