unit FWizEmpCerrarVacaciones_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ComCtrls, ExtCtrls, Mask,
     FWizEmpBaseFiltro,
     ZetaEdit,
     ZetaFecha, ZetaNumero, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons,
  cxRadioGroup, cxTextEdit, cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses,
  cxImage, dxCustomWizardControl, dxWizardControl;

type
  TWizEmpCerrarVacaciones_DevEx = class(TWizEmpBaseFiltro)
    PagarGB: TGroupBox;
    PagarSaldo: TRadioButton;
    PagarFijo: TRadioButton;
    PagarDias: TZetaNumero;
    PrimaGB: TGroupBox;
    PrimaSaldo: TRadioButton;
    PrimaFijo: TRadioButton;
    PrimaDias: TZetaNumero;
    GozarGB: TGroupBox;
    GozarSaldo: TRadioButton;
    GozarFijo: TRadioButton;
    GozarDias: TZetaNumero;
    FechaGB: TGroupBox;
    PorAniversario: TRadioButton;
    PorFecha: TRadioButton;
    Fecha: TZetaFecha;
    ObservacionesLBL: TLabel;
    Observaciones: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SetControls(Sender: TObject);
     procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Se agrega para validar el enfoque de controles.
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpCerrarVacaciones_DevEx: TWizEmpCerrarVacaciones_DevEx;

//Constantes Paginas
const
     K_PAGE_PARAMETROS =0;

implementation

uses DCliente,
     DProcesos,
     ZetaCommonClasses,
     ZBaseSelectGrid_DevEx,
     FEmpCerrarVacacionesGridSelect_DevEx;

{$R *.DFM}

{ TWizEmpCancelarVacaciones }

procedure TWizEmpCerrarVacaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := PorAniversario;
     Fecha.Valor := dmCliente.FechaDefault;
     Observaciones.Properties.MaxLength := K_ANCHO_TITULO;
     HelpContext := H11511_Cerrar_vacaciones;

     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := K_PAGE_PARAMETROS;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizEmpCerrarVacaciones_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetControls( Self );
     Advertencia.Caption :='Este proceso generar� un movimiento de tipo Cierre en el historial de vacaciones.';
end;

procedure TWizEmpCerrarVacaciones_DevEx.SetControls(Sender: TObject);
begin
     Fecha.Enabled := PorFecha.Checked;
     GozarDias.Enabled := GozarFijo.Checked;
     PagarDias.Enabled := PagarFijo.Checked;
     PrimaDias.Enabled := PrimaFijo.Checked;
end;

procedure TWizEmpCerrarVacaciones_DevEx.CargaParametros;
const K_STRING_SALDO : string = 'Saldo a la fecha';
begin
     inherited CargaParametros;
     with Descripciones do
     begin
          Clear;
          if PorAniversario.Checked then
             AddString( 'Fecha cierre', 'Aniversario del Empleado' )
          else
              AddDate( 'Fecha cierre', Fecha.Valor );
          if GozarSaldo.Checked then
             AddString( 'Cierre d�as por gozar', K_STRING_SALDO )
          else
              AddFloat( 'Cierre d�as por gozar', GozarDias.Valor );
          if PagarSaldo.Checked then
             AddString( 'Cierre d�as por pagar', K_STRING_SALDO )
          else
              AddFloat( 'Cierre d�as por pagar', PagarDias.Valor );
          if PrimaSaldo.Checked then
             AddString( 'Cierre d�as P.V por pagar', K_STRING_SALDO )
          else
              AddFloat( 'Cierre d�as P.V por pagar', PrimaDias.Valor );
          AddString( 'Observaciones', Observaciones.Text );
     end;
     ParamInicial := 0; //DevEx(by am): Se requiere inicializar el parametro de nuevo pues esta forma limpia las descripciones en la implementacion.

     with ParameterList do
     begin
          AddDate( 'Fecha', Fecha.Valor );
          AddBoolean( 'PorAniversario', PorAniversario.Checked );
          AddBoolean( 'GozarSaldo', GozarSaldo.Checked );
          AddBoolean( 'PagarSaldo', PagarSaldo.Checked );
          AddBoolean( 'PrimaSaldo', PrimaSaldo.Checked );
          AddFloat( 'GozarDias', GozarDias.Valor );
          AddFloat( 'PagarDias', PagarDias.Valor );
          AddFloat( 'PrimaDias', PrimaDias.Valor );
          AddString( 'Observaciones', Observaciones.Text );
     end;
end;

procedure TWizEmpCerrarVacaciones_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CierreGlobalVacacionesGetLista( ParameterList );
end;

function TWizEmpCerrarVacaciones_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpCierreVacacionesGridSelect_DevEx );
end;

function TWizEmpCerrarVacaciones_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CierreGlobalVacaciones( ParameterList, Verificacion );
end;

procedure TWizEmpCerrarVacaciones_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual( Parametros )then
         ParametrosControl := PorAniversario
     else
         ParametrosControl := nil;// Si la pagina es distinta no se necesita enfocar nada.
     inherited;

end;
end.
