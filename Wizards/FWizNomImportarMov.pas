unit FWizNomImportarMov;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls,
     ZBaseWizard,
     ZetaDBTextBox,
     ZetaWizard,
     ZetaCommonLists,
     ZetaKeyCombo, Mask, ZetaNumero;

type
  TWizNomImportarMov = class(TBaseWizard)
    GroupBox1: TGroupBox;
    PeriodoTipoLbl: TLabel;
    iTipoNomina: TZetaTextBox;
    iNumeroNomina: TZetaTextBox;
    iMesNomina: TZetaTextBox;
    sDescripcion: TZetaTextBox;
    FechaInicial: TZetaTextBox;
    FechaFinal: TZetaTextBox;
    sStatusNomina: TZetaTextBox;
    Label4: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    PeriodoNumeroLBL: TLabel;
    Label1: TLabel;
    Archivo: TTabSheet;
    Panel1: TPanel;
    Label5: TLabel;
    Label7: TLabel;
    BGuardaArchivo: TSpeedButton;
    Label8: TLabel;
    sArchivo: TEdit;
    iOperacion: TZetaKeyCombo;
    iFormato: TZetaKeyCombo;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    iMesActivo: TZetaTextBox;
    PanelAnimation: TPanel;
    Animate: TAnimate;
    PanelMensaje: TPanel;
    GBImportar: TGroupBox;
    RBExcepciones: TRadioButton;
    RBAcumulados: TRadioButton;
    GBAcumulados: TGroupBox;
    LblYear: TLabel;
    YearAcum: TZetaNumero;
    MesAcum: TZetaKeyCombo;
    LblMes: TLabel;
    GroupBox3: TGroupBox;
    CBNoIncapacitados: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure BGuardaArchivoClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure RBImportarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    ArchivoVerif : String;
    function GetArchivo: String;
    function LeerArchivo: Boolean;
    function GetTipoImportacion: eImportacion;
    procedure SetControlesAcumula(const lEnabled: Boolean);
    procedure ActualizaNoIncapacitados;{OP: 12/06/08}
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomImportarMov: TWizNomImportarMov;

implementation

uses DCliente,
     DProcesos,
     ZGlobalTress,
     ZBaseGridShow,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZAsciiTools,
     ZetaDialogo,
     FNomImportarMovGridShow,
     FTressShell,
     ZAccesosMgr,
     ZAccesosTress;

{$R *.DFM}

procedure TWizNomImportarMov.FormCreate(Sender: TObject);
begin
     PanelAnimation.Visible := FALSE;
     with sArchivo do
     begin
          Tag := K_GLOBAL_DEF_IMP_MOVS;
     end;
     inherited;
     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          with GetDatosPeriodoActivo do
          begin
               iNumeroNomina.Caption := IntToStr( Numero );
               iTipoNomina.Caption := ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
               iMesNomina.Caption := ZetaCommonLists.ObtieneElemento( lfMeses, ( Mes - 1  ) );
               sStatusNomina.Caption := ZetaCommonTools.GetDescripcionStatusPeriodo( Status, StatusTimbrado );
               FechaInicial.Caption := FormatDateTime( LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( LongDateFormat, Fin );
               iMesActivo.Caption := ZetaCommonLists.ObtieneElemento( lfMeses, Mes - 1 );
               // Parametros
               YearAcum.Valor := Year;
               MesAcum.Valor := Mes - 1;
               RBExcepciones.Caption := RBExcepciones.Caption + ' ' + GetPeriodoDescripcion;
               SetControlesAcumula( RBAcumulados.Checked );
          end;
     end;
     iOperacion.ItemIndex := Ord( omSustituir );
     iFormato.ItemIndex := Ord( faASCIIFijo );
     HelpContext := H33411_Importar_movimientos;
end;

procedure TWizNomImportarMov.FormShow(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          with RBExcepciones do
          begin
               Enabled := ( not NominaYaAfectada ) and CheckDerecho( D_NOM_PROC_IMP_MOVIMIEN, K_DERECHO_CONSULTA );
               Checked := Enabled;
          End;
          With RBAcumulados do
          begin
               Enabled := CheckDerecho( D_NOM_PROC_IMP_MOVIMIEN, K_DERECHO_ALTA );
               Checked := ( Not RBExcepciones.Enabled ) and Enabled;
          end;
     end;
     ActualizaNoIncapacitados;{OP: 12/06/08}
end;


function TWizNomImportarMov.GetArchivo: String;
begin
     Result := sArchivo.Text;
end;

procedure TWizNomImportarMov.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddString( 'Archivo', GetArchivo );
          AddInteger( 'Operacion', iOperacion.Valor );
          AddInteger( 'Importacion', Ord( GetTipoImportacion ) );
          AddInteger( 'Formato', iFormato.Valor );
          AddInteger( 'YearAcum', YearAcum.ValorEntero );
          AddInteger( 'MesAcum', MesAcum.Valor + 1 );
          AddBoolean( 'NoIncapacitados', CBNoIncapacitados.Checked );{OP: 12/06/08}
          AddBoolean( 'ExcederLimites', Revisa( D_NOM_EXCEPCIONES_LIM_MONTO) );

     end;
end;

function TWizNomImportarMov.LeerArchivo: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmProcesos do
        begin
             PanelAnimation.Visible := TRUE;
             Application.ProcessMessages;
             CargaParametros;
             ImportarMovGetListaASCII( ParameterList );
             PanelAnimation.Visible := FALSE;
             if ( ErrorCount > 0 ) then                 // Hubo Errores
             begin
                  ZAsciiTools.FiltraASCII( cdsDataSet, True );
                  Result := ZBaseGridShow.GridShow( cdsDataset, TNomImportarMovGridShow );
                  ZAsciiTools.FiltraASCII( cdsDataSet, False );
             end
             else
                  Result := True;
        end;
        if Result then
           ArchivoVerif := GetArchivo
        else
           ArchivoVerif := VACIO;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TWizNomImportarMov.BGuardaArchivoClick(Sender: TObject);
begin
     inherited;
     sArchivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, sArchivo.Text, 'dat' );
end;

procedure TWizNomImportarMov.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Archivo ) then
               begin
                    if ( ( not( RBExcepciones.Checked ) ) and ( not( RBAcumulados.Checked ) ) )then
                    begin
                       CanMove := Error( 'Debe Especificarse Hacia Donde se Importa',Archivo);
                    end
                    else
                        if ZetaCommonTools.StrVacio( GetArchivo ) then
                           CanMove := Error( 'Debe Especificarse un Archivo a Importar', Archivo )
                        else
                           if not FileExists( GetArchivo ) then
                              CanMove := Error( 'El Archivo ' + GetArchivo + ' No Existe', Archivo )
                           else
                           begin
                                if ( ArchivoVerif <> GetArchivo ) or
                                   ( ZetaDialogo.ZConfirm( Caption, 'El Archivo ' + GetArchivo + ' Ya Fu� Verificado !' + CR_LF +
                                                           'Volver a Verificar ?', 0, mbYes ) ) then
                                   CanMove := LeerArchivo
                                else
                                   CanMove := TRUE;
                           end;
               end;
          end;
     end;
end;

function TWizNomImportarMov.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarMov( ParameterList );
end;

procedure TWizNomImportarMov.SetControlesAcumula( const lEnabled: Boolean );
begin
     LblYear.Enabled := lEnabled;
     YearAcum.Enabled := lEnabled;
     LblMes.Enabled := lEnabled;
     MesAcum.Enabled := lEnabled;
end;

function TWizNomImportarMov.GetTipoImportacion: eImportacion;
begin
     if ( RBExcepciones.Checked ) then
        Result := eiExcepciones
     else
        Result := eiAcumulados;
end;

procedure TWizNomImportarMov.RBImportarClick(Sender: TObject);
begin
     inherited;
     SetControlesAcumula( RBAcumulados.Checked );
     ArchivoVerif := VACIO;
     ActualizaNoIncapacitados;{OP: 12/06/08}
end;

{OP: 12/06/08}
procedure TWizNomImportarMov.ActualizaNoIncapacitados;
begin
     CBNoIncapacitados.Enabled := RBExcepciones.Checked;
     if( Not RBExcepciones.Checked ) then
         CBNoIncapacitados.Checked := RBExcepciones.Checked;
end;

end.
