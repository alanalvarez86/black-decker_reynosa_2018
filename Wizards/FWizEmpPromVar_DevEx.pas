unit FWizEmpPromVar_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls, Mask, Spin,
     ZetaEdit, ZetaNumero, ZetaFecha,
     ZetaKeyCombo, FWizEmpBaseFiltro, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters,
     dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters,
     cxContainer, cxEdit, Menus, ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons,
     cxRadioGroup, cxTextEdit, cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses,
     cxImage, dxCustomWizardControl, dxWizardControl, cxCheckBox;

type
  TWizEmpPromVar_DevEx = class(TWizEmpBaseFiltro)

    nYearLBL: TLabel;
    nMonthLBL: TLabel;
    iMonth: TZetaKeyCombo;
    Label1: TLabel;
    sFormula: TMemo;
    FormulaBuild: TcxButton;
    dReferencia: TZetaFecha;
    dReferenciaLBL: TLabel;
    sDescripcionLBL: TLabel;
    sDescripcion: TEdit;
    sObservaLBL: TLabel;
    sObserva: TcxMemo;
    iYear: TSpinEdit;
    lbBimestre: TLabel;
    lIncapacitados: TcxCheckBox;
    dIMSS: TZetaFecha;
    Label2: TLabel;

    procedure FormCreate(Sender: TObject);
    procedure FormulaBuildClick(Sender: TObject);
    //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure iYearChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dReferenciaValidDate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
    procedure ActualizaCaptionBimestre;
  public
    { Public declarations }
  end;

var
  WizEmpPromVar_DevEx: TWizEmpPromVar_DevEx;

implementation

uses DProcesos,
     DCliente,
     ZetaDialogo,
     ZBaseSelectGrid_DevEx,
     ZConstruyeFormula,
     ZGlobalTress,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     FEmpPromVarGridSelect_DevEx, ZcxWizardBasico;

{$R *.DFM}

procedure TWizEmpPromVar_DevEx.FormCreate(Sender: TObject);
var
   Year, Month, Day: Word;
   dFecha: TDate;
begin
     ParametrosControl := iYear;
     sFormula.Tag := K_GLOBAL_DEF_PROM_DIAS;
     inherited;
     EmpleadoCodigo := 'COLABORA.CB_CODIGO';
     dFecha := dmCliente.FechaDefault;
     DecodeDate( ZetaCommonTools.LastMonth( dFecha ), Year, Month, Day );
     dReferencia.Valor := FirstDayOfMonth( dFecha );
     iYear.Value := Year;
     iMonth.Valor := NumeroBimestre( dmCliente.IMSSMes );
     HelpContext := H10162_Promediar_percepciones;
end;

procedure TWizEmpPromVar_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  ActualizaCaptionBimestre;
  Advertencia.Caption := 'Se modificar�n los salarios integrados para los empleados que en el mes hayan devengado importes gravados por el IMSS.' ;

end;

procedure TWizEmpPromVar_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'Year', iYear.Value );
          AddInteger( 'Mes', iMonth.Valor );
          AddString( 'Formula', sFormula.Lines.Text );
          AddString( 'Descripcion', sDescripcion.Text );
          AddDate( 'Fecha', dReferencia.Valor );
          AddMemo( 'Observaciones', sObserva.Text );
          AddBoolean( 'NoIncluirIncapacitados', not lIncapacitados.Checked );
          AddDate( 'Fecha2', dIMSS.Valor );
     end;

     with Descripciones do
     begin
          AddInteger( 'A�o de acumulados', iYear.Value );
          AddString( 'Bimestre', ObtieneElemento( lfBimestres, iMonth.Valor -1 ) );
          AddString( 'Percepciones promedio', sFormula.Lines.Text );
          AddDate( 'Fecha de cambio', dReferencia.Valor );
          AddDate( 'Fecha de aviso a IMSS', dImss.Valor );
          AddString( 'Descripci�n', sDescripcion.Text );
          AddMemo( 'Observaciones', sObserva.Text );
          AddBoolean( 'Inclu�r incapacitados', lIncapacitados.Checked );
     end;

end;

procedure TWizEmpPromVar_DevEx.FormulaBuildClick(Sender: TObject);
begin
     inherited;
     with sFormula do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( enNomina, Text, SelStart, evNomina );
     end;
end;

procedure TWizEmpPromVar_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   wYear, wMonth, wDay: Word;
begin
     inherited;
     ParametrosControl := nil;

     if CanMove then
     begin
          if Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
          begin
               DecodeDate( dReferencia.Valor, wYear, wMonth, wDay );
               if ( wYear < iYear.Value ) then
               begin
                    zError( Caption, 'A�o de acumulados debe ser menor � igual que la fecha de registro.', 0 );
                    ActiveControl := iYear;
                    CanMove := False;
               end
               else if ( wYear = iYear.Value ) and ( NumeroBimestre( wMonth ) < iMonth.Valor ) then
               begin
                    zError( Caption, 'Bimestre de acumulados debe ser menor � igual que la fecha de registro.', 0 );
                    ActiveControl := iMonth;
                    CanMove := False;
               end
               else
               if ( sFormula.Lines.Count <= 0 ) then
               begin
                    zError( Caption, 'La f�rmula de percepciones no fu� especificada.', 0 );
                    ActiveControl := sFormula;
                    CanMove := False;
               end;
          end;
     end;
end;

procedure TWizEmpPromVar_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := iYear
     else
         ParametrosControl := nil;
     inherited;
end;

procedure TWizEmpPromVar_DevEx.CargaListaVerificacion;
begin
     dmProcesos.PromediarVariablesGetLista( ParameterList );
end;

function TWizEmpPromVar_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpPromVarGridSelect_DevEx );
end;

function TWizEmpPromVar_DevEx.EjecutarWizard;
begin
     Result := dmProcesos.PromediarVariables( ParameterList, Verificacion );
end;

procedure TWizEmpPromVar_DevEx.iYearChange(Sender: TObject);
begin
     inherited;
     ActualizaCaptionBimestre;
end;

procedure TWizEmpPromVar_DevEx.ActualizaCaptionBimestre;
begin
     if iYear.Value < K_YEAR_REFORMA then
          lbBimestre.Caption := ''
     else
     begin
          lbBimestre.Caption := 'Bimestre # ' + IntToStr( iMonth.Valor );
     end;
end;

procedure TWizEmpPromVar_DevEx.dReferenciaValidDate(Sender: TObject);
begin
     inherited;
     dImss.Valor := dReferencia.Valor;
end;

end.
