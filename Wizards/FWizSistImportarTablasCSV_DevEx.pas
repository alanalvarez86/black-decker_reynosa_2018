unit FWizSistImportarTablasCSV_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, ZetaCXWizard,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, StdCtrls, ComCtrls, ZetaKeyCombo, Buttons, ZetaEdit,
  ExtCtrls, ZetaSmartLists, Menus, cxButtons, cxListBox,
  ZetaSmartLists_DevEx;

type
  TWizSistImportarTablasCSV_DevEx = class(TcxBaseWizard)
   OpenDialog: TOpenDialog;
    Panel1: TPanel;
    PanelAnimation: TPanel;
    Animate: TAnimate;
    PanelMensaje: TPanel;
    gbBaseDatos: TcxGroupBox;
    lblDescripcion: TLabel;
    lblCodigo: TLabel;
    lblTipo: TLabel;
    DB_CODIGO: TZetaEdit;
    DB_DESCRIP: TZetaEdit;
    DB_TIPO: TZetaEdit;
    gbTablaDestino: TcxGroupBox;
    TablaDestino: TcxListBox;
    gbArchivo: TcxGroupBox;
    Archivo: TEdit;
    ArchivoSeek: TcxButton;
    Label4: TLabel;
    cbFFecha: TZetaKeyCombo;
    Campos: TdxWizardControlPage;
    GroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DB_CODIGO2: TZetaEdit;
    DB_DESCRIP2: TZetaEdit;
    DB_TIPO2: TZetaEdit;
    ZetaSmartListsButton1: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton2: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton3: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton4: TZetaSmartListsButton_DevEx;
    gbCamposTabla: TcxGroupBox;
    gbCamposImportar: TcxGroupBox;
    CamposDisponibles: TZetaSmartListBox_DevEx;
    CamposSeleccion: TZetaSmartListBox_DevEx;
    ZetaSmartLists: TZetaSmartLists_DevEx;
    procedure ArchivoSeekClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure TablaDestinoClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure ZetaSmartListsAlRechazar(Sender: TObject;
      var Objeto: TObject; Texto: String);
    procedure ZetaSmartListsAlSeleccionar(Sender: TObject;
      var Objeto: TObject; Texto: String);
  private
    { Private declarations }
    FVerificaASCII: Boolean;
    FVerificacion: Boolean;
    ArchivoVerif : String;
    PuedeRechazarCampo: Boolean;
    function LeerArchivo: Boolean;
    procedure SetVerificacion( const Value: Boolean );
    procedure CargaListaVerificaASCII;
    function GetArchivo: String;
    procedure CargarListBoxes;
    function EstanLlavesAImportar: boolean;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizSistImportarTablasCSV_DevEx: TWizSistImportarTablasCSV_DevEx;

implementation

uses DProcesos,
     ZetaDialogo,
     ZetaMessages,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZAsciiTools,
     ZBaseSelectGrid_DevEx,
     ZBaseGridShow_DevEx,
     FImportarTablaCSVGridShow_DevEx,
     FImportarTablaCSVGridSelect_DevEx, dSistema,  DConsultas, ZcxWizardBasico;

{$R *.DFM}

procedure TWizSistImportarTablasCSV_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PanelAnimation.Visible := FALSE;
     ParametrosControl := Archivo;
     PuedeRechazarCampo := FALSE;
     Archivo.Text := ZetaMessages.SetFileNameDefaultPath( 'Archivo.csv' );
     {V2013}
     // Formato.ItemIndex := Ord( faASCIIDel );

     cbFFecha.ItemIndex := Ord( ifDDMMYYYYs );
     ArchivoVerif:= VACIO;
     HelpContext := H_SIST_BASE_DATOS;

     //DevEx
     Parametros.PageIndex := 0;
     Campos.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizSistImportarTablasCSV_DevEx.CargaParametros;
begin

     inherited CargaParametros;

     with ParameterList do
     begin
          AddString( 'Archivo', Archivo.Text );  
          AddString ( 'Tabla', TablaDestino.Items[TablaDestino.ItemIndex] );
          AddInteger( 'FormatoImpFecha', cbFFecha.Valor );
          AddString ('BaseDatosUbicacion', dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString);
          AddString ('Usuario', dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString);
          AddString ('Password', dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString);
          AddString ('Campos', CamposSeleccion.Items.CommaText);
     end;

     with Descripciones do
     begin
          AddString( 'Tabla destino', TablaDestino.Items[TablaDestino.ItemIndex] );
          AddString( 'Archivo a importar', Archivo.Text );
          AddString( 'Formato fechas', ObtieneElemento( lfFormatoImpFecha, cbFFecha.Valor ) );
          AddString ('Campos', CamposSeleccion.Items.CommaText);
     end;


end;

procedure TWizSistImportarTablasCSV_DevEx.CargaListaVerificaASCII;
begin
     with dmProcesos do
     begin
          ImportarTablaCSVGetListaASCII( ParameterList );
          if ( ErrorCount > 0 ) then                 // Hubo Errores
          begin
               with dmProcesos do
               begin
                    ZAsciiTools.FiltraASCII( cdsDataSet, True );
                    FVerificaASCII := ZBaseGridShow_DevEx.GridShow( cdsDataset, TImportarTablaCSVGridShow_DevEx );
                    ZAsciiTools.FiltraASCII( cdsDataSet, False );
               end;
          end
          else
              FVerificaASCII := True;
     end;
end;

{procedure TWizSistImportarTablasCSV.CargaListaVerificacion;
begin
     with dmProcesos do
     begin
          ImportarKardexGetLista( ParameterList );
     end;
end;}

function TWizSistImportarTablasCSV_DevEx.Verificar: Boolean;
begin
     dmProcesos.cdsDataSet.First;
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TImportarTablaCSVGridSelect_DevEx, FALSE );
end;

procedure TWizSistImportarTablasCSV_DevEx.SetVerificacion(const Value: Boolean);
begin
     FVerificacion := Value;
end;

function TWizSistImportarTablasCSV_DevEx.GetArchivo: String;
begin
     Result := Archivo.Text;
end;

function TWizSistImportarTablasCSV_DevEx.LeerArchivo: Boolean;
begin
     Result := FALSE;
     PanelAnimation.Visible := TRUE;
     Application.ProcessMessages;
     CargaParametros;
     CargaListaVerificaASCII;
     PanelAnimation.Visible := FALSE;
     if FVerificaASCII then
     begin
          SetVerificacion( Verificar );
          Result := FVerificacion;
     end;
     if Result then
        ArchivoVerif := GetArchivo
     else
        ArchivoVerif := VACIO;
end;

procedure TWizSistImportarTablasCSV_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var sListaCamposLlave: String;
begin
     ParametrosControl := nil;  //DevEx
     inherited;
     sListaCamposLlave := VACIO;
     if Wizard.Adelante then
     begin
          //if ( PageControl.ActivePage = Parametros ) then
          if ( WizardControl.ActivePage = Parametros ) then  //DevEx
          begin
               if TablaDestino.ItemIndex < 0 then
                  CanMove := Error( 'Debe seleccionar la tabla destino', TablaDestino )
               else if ZetaCommonTools.StrVacio( GetArchivo ) then
                  CanMove := Error( 'Debe especificarse un archivo a importar', Archivo )
               else if not FileExists( GetArchivo ) then
                     CanMove := Error( 'El archivo ' + GetArchivo + ' no existe', Archivo )
               else begin
                   CanMove := TRUE;
                   if CanMove then
                      CargarListBoxes;
               end;
          end
          //else if PageControl.ActivePage = Campos then
          else if WizardControl.ActivePage = Campos then   //DevEx
          begin
               if CamposSeleccion.Items.Count = 0 then
                  CanMove := Error ('No ha seleccionado campos a importar', CamposSeleccion)
               else if not EstanLlavesAImportar then
               begin
                    dmSistema.cdsCamposLlaveTabla.First;
                    while not dmSistema.cdsCamposLlaveTabla.Eof do
                    begin
                         if sListaCamposLlave <> VACIO then
                            sListaCamposLlave := sListaCamposLlave + ', ';
                         sListaCamposLlave := sListaCamposLlave + dmSistema.cdsCamposLlaveTabla.FieldByName ('COLUMN_NAME').AsString;
                         dmSistema.cdsCamposLlaveTabla.Next;
                    end;
                    CanMove := Error (Format ('Los campos llave: %s deben incluirse en la importaci�n', [sListaCamposLlave]), CamposSeleccion)
               end
               else if ( ArchivoVerif <> GetArchivo ) or
                          ( ZetaDialogo.ZConfirm( Caption, 'El archivo ' + GetArchivo + ' ya fu� verificado !' + CR_LF +
                                                  'Volver a verificar ?', 0, mbYes ) ) then
                     CanMove := LeerArchivo
          end;
     end;
end;

procedure TWizSistImportarTablasCSV_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := Archivo
     else
         ParametrosControl := nil;

     inherited;
     if Wizard.Adelante then
     begin
          //if PageControl.ActivePage = Campos then
          if WizardControl.ActivePage = Campos then   //DevEx
             gbCamposTabla.Caption := 'Campos tabla ' + UpperCase(TablaDestino.Items[TablaDestino.ItemIndex])
          //else if PageControl.ActivePage = Ejecucion then
          else if WizardControl.ActivePage = Ejecucion then   //DevEx
          begin
               {
               Advertencia.Lines.Clear;
               Advertencia.Lines.Append('');
               Advertencia.Lines.Append('Se importar�n los registros seleccionados del archivo ' + ExtractFileName(Archivo.Text) + '.');
               Advertencia.Lines.Append('');
               Advertencia.Lines.Append('Presione el bot�n Ejecutar para iniciar el proceso.');
               }
               Advertencia.Caption :=
                 'Al aplicar el proceso se a�adir� la informaci�n contenida en el archivo ' +
                 ExtractFileName(Archivo.Text) + ' en la base de datos seleccionada.'

          end;
     end;
end;

function TWizSistImportarTablasCSV_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarTablaCSV( ParameterList );
end;

procedure TWizSistImportarTablasCSV_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     Archivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, Archivo.Text, 'csv' );
end;

procedure TWizSistImportarTablasCSV_DevEx.FormShow(Sender: TObject);
begin
  inherited;

     //PageControl.ActivePage := Parametros;
     WizardControl.ActivePage := Parametros;   //DevEx
     // Localizar Base de Datos con la que se est� trabajando (empresa actual)
     // y mostrar en campos
     DB_CODIGO.Text := dmSistema.cdsSistBaseDatos.FieldByName ('DB_CODIGO').AsString;
     DB_DESCRIP.Text := dmSistema.cdsSistBaseDatos.FieldByName ('DB_DESCRIP').AsString;
     DB_TIPO.Text := ObtieneElemento (lfTipoCompanyName, dmSistema.cdsSistBaseDatos.FieldByName ('DB_TIPO').AsInteger);
     DB_CODIGO2.Text := dmSistema.cdsSistBaseDatos.FieldByName ('DB_CODIGO').AsString;
     DB_DESCRIP2.Text := dmSistema.cdsSistBaseDatos.FieldByName ('DB_DESCRIP').AsString;
     DB_TIPO2.Text := ObtieneElemento (lfTipoCompanyName, dmSistema.cdsSistBaseDatos.FieldByName ('DB_TIPO').AsInteger);

     // Llenar lista de tablas destino
     dmSistema.GetTablasBD(dmSistema.cdsSistBaseDatos.FieldByName ('DB_DATOS').AsString,
             dmSistema.cdsSistBaseDatos.FieldByName ('DB_USRNAME').AsString,
             dmSistema.cdsSistBaseDatos.FieldByName ('DB_PASSWRD').AsString);
     dmSistema.cdsTablasBD.First;
     while not dmSistema.cdsTablasBD.Eof do
     begin 
           TablaDestino.Items.Add(dmSistema.cdsTablasBD.FieldByName ('TABLE_NAME').AsString);
           dmSistema.cdsTablasBD.Next;
     end;
     // ----- -----

     // Mostrar archivo csv default
     Archivo.Text := ExtractFilePath( Application.ExeName ) + 'Archivo.csv';
     // ----- -----

     TablaDestino.SetFocus;   

     // Asignar BaseDatosImportacionXMLCSV.	
     dmConsultas.BaseDatosImportacionXMLCSV := dmSistema.GetBaseDatosImportacion;
end;

procedure TWizSistImportarTablasCSV_DevEx.TablaDestinoClick(Sender: TObject);
begin
     Archivo.Text := ExtractFilePath(Archivo.Text) + TablaDestino.Items[TablaDestino.ItemIndex] + '.csv';
end;

procedure TWizSistImportarTablasCSV_DevEx.CargarListBoxes;
begin

     dmSistema.GetCamposTabla(dmSistema.cdsSistBaseDatos.FieldByName ('DB_DATOS').AsString,
                              dmSistema.cdsSistBaseDatos.FieldByName ('DB_USRNAME').AsString,
                              dmSistema.cdsSistBaseDatos.FieldByName ('DB_PASSWRD').AsString,
                              TablaDestino.Items[TablaDestino.ItemIndex]);

     dmSistema.GetCamposLlaveTabla(dmSistema.cdsSistBaseDatos.FieldByName ('DB_DATOS').AsString,
                              dmSistema.cdsSistBaseDatos.FieldByName ('DB_USRNAME').AsString,
                              dmSistema.cdsSistBaseDatos.FieldByName ('DB_PASSWRD').AsString,
                              TablaDestino.Items[TablaDestino.ItemIndex]);

     //limpiar smartlistbox
     CamposDisponibles.Items.Clear;
     CamposSeleccion.Items.Clear;

     // agregar campos disponibles a smartlistbox CamposDisponibles
     dmSistema.cdsCamposTabla.First;
     while not dmSistema.cdsCamposTabla.Eof do
     begin
          CamposDisponibles.Items.Add (dmSistema.cdsCamposTabla.FieldByName('NAME').AsString);
          dmSistema.cdsCamposTabla.Next;
     end;

     // agregar campos llave a smartlistbox CamposSeleccion
     dmSistema.cdsCamposLlaveTabla.First;
     while not dmSistema.cdsCamposLlaveTabla.Eof do
     begin
          CamposDisponibles.ItemIndex := CamposDisponibles.Items.IndexOf(dmSistema.cdsCamposLlaveTabla.FieldByName('COLUMN_NAME').AsString);
          //CamposDisponibles.MoveSelection(CamposSeleccion);    //DevEx
          ZetaSmartListsButton1.SmartLists.Escoger;    //Elegir el campo si es llave
          dmSistema.cdsCamposLlaveTabla.Next;
     end;
end;

procedure TWizSistImportarTablasCSV_DevEx.ZetaSmartListsAlRechazar(
  Sender: TObject; var Objeto: TObject; Texto: String);
begin
     {if dmSistema.cdsCamposLlaveTabla.Locate('COLUMN_NAME', CamposSeleccion.Items[CamposSeleccion.ItemIndex], []) then
     begin
        ZError (Caption, 'No es posible mover un campo llave', 0);
        // ZetaSmartListsButton2.Enabled := FALSE;
        // ZetaSmartLists.Refrescar;
        // CamposDisponibles.ItemIndex := CamposDisponibles.Items.Count-1;
        // CamposDisponibles.MoveSelection(CamposSeleccion);
        ZetaSmartLists.Escoger;
     end
     else
     begin
         // ZetaSmartListsButton2.Enabled := TRUE;
         inherited;
     end;
     ZetaSmartLists.Refrescar;}
end;

procedure TWizSistImportarTablasCSV_DevEx.ZetaSmartListsAlSeleccionar(
  Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     {if dmSistema.cdsCamposLlaveTabla.Locate('COLUMN_NAME', CamposSeleccion.Items[CamposSeleccion.ItemIndex], []) then
        PuedeRechazarCampo := FALSE
        // ZError (Caption, 'No es posible mover un campo llave', 0)
     else
         PuedeRechazarCampo := TRUE;}
end;

function TWizSistImportarTablasCSV_DevEx.EstanLlavesAImportar: boolean;
var i: Integer;
    bEncontrado: boolean;
begin
     Result := TRUE;
     dmSistema.cdsCamposLlaveTabla.First;
     while not dmSistema.cdsCamposLlaveTabla.Eof do
     begin
          bEncontrado := FALSE;
          // recorrer CamposSeleccion
          for i:=0 to CamposSeleccion.Items.Count-1 do
          begin
               if dmSistema.cdsCamposLlaveTabla.FieldByName('COLUMN_NAME').AsString = CamposSeleccion.Items[i] then
               begin
                    bEncontrado := TRUE; 
                    break;
               end;
          end;
          if not bEncontrado then
          begin
               Result:= FALSE;
               break;
          end;
          dmSistema.cdsCamposLlaveTabla.Next;
     end;
end;

end.
