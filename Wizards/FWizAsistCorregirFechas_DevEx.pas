unit FWizAsistCorregirFechas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     ZetaWizard,
     ZetaFecha,
     ZetaEdit, ZCXBaseWizardFiltro, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters;

type
  TWizAsistCorregirFechas_DevEx = class(TBaseCXWizardFiltro)
    iCambiar: TcxRadioGroup;
    lRevisar: TCheckBox;
    GroupBox1: TGroupBox;
    FechaActual: TZetaFecha;
    Label1: TLabel;
    Label2: TLabel;
    FechaNueva: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizAsistCorregirFechas_DevEx: TWizAsistCorregirFechas_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaDialogo,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TWizAsistCorregirFechas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := FechaActual;
     with dmCliente do
     begin
          FechaActual.Valor := FechaDefault;
          FechaNueva.Valor := FechaDefault;
     end;
     HelpContext := H20236_Corregir_fechas_globales;
end;

procedure TWizAsistCorregirFechas_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if ( FechaActual.Valor = FechaNueva.Valor ) then
               begin
                    CanMove := Error( 'La Fecha Actual y Fecha Nueva Deben Ser Diferentes', FechaActual );
               end;

               if CanMove then
               begin
                    with dmCliente do
                         CanMove := PuedeCambiarTarjetaDlg( FechaActual.Valor, 0 ) AND
                                    PuedeCambiarTarjetaDlg( FechaNueva.Valor, 0 );
               end;
          end;

     end;
end;

procedure TWizAsistCorregirFechas_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddDate( 'FechaActual', FechaActual.Valor );
          AddDate( 'FechaNueva', FechaNueva.Valor );
          AddInteger( 'Cambio', iCambiar.ItemIndex );
          AddBoolean( 'RevisarHorario', lRevisar.Checked );
          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
     end;
     with Descripciones do
     begin
          AddDate( 'Fecha Actual', FechaActual.Valor );
          AddDate( 'Fecha Nueva', FechaNueva.Valor );
          AddBoolean( 'R. Hor. en Fecha Nueva', lRevisar.Checked );
     end;
end;

function TWizAsistCorregirFechas_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CorregirFechas( ParameterList );
end;

procedure TWizAsistCorregirFechas_DevEx.WizardAfterMove(Sender: TObject);
begin
   if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := FechaActual;
     inherited
end;

end.
