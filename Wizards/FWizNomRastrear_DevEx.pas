unit FWizNomRastrear_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, ExtCtrls, Buttons,
     FWizNomBase_DevEx,
     ZetaWizard,
     ZetaEdit,
     ZetaDBTextBox, 
     cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomRastrear_DevEx = class(TWizNomBase_DevEx)
    Parametros2: TdxWizardControlPage;
    Empleado: TZetaKeyLookup_DevEx;
    Concepto: TZetaKeyLookup_DevEx;
    Archivo: TEdit;
    lNomParam: TCheckBox;
    ArchivoLBL: TLabel;
    ConceptoLBL: TLabel;
    EmpleadoLbl: TLabel;
    ArchivoSeek: TcxButton;
    OpenDialog: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizNomRastrear_DevEx: TWizNomRastrear_DevEx;

implementation

uses ZetaDialogo,
     ZetaMessages,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZGlobalTress,
     DProcesos,
     DCliente,
     DCatalogos;

{$R *.DFM}

{ ************* TWinNomRastrear *************** }

{ PENDIENTE: la verificacion de ver si la nomina existe, se hace en el servidor }

procedure TWizNomRastrear_DevEx.FormCreate(Sender: TObject);
const
     K_TRACE_FILE = 'RASTREO.TXT';
begin
     Concepto.Tag := K_GLOBAL_DEF_CONCEPTO_X_RASTREAR;
     inherited;
     Archivo.Text := ZetaMessages.SetFileNameDefaultPath( K_TRACE_FILE );
     HelpContext := H33417_Rastrear_calculo;
     lNomParam.Checked := True;
     Concepto.LookupDataset := dmCatalogos.cdsConceptos;
     Empleado.LookupDataset := dmCliente.cdsEmpleadoLookUp;

      {***Se define la seceunta de las paginas***}
     Ejecucion.PageIndex := WizardControl.PageCount-1;
     Parametros2.PageIndex := WizardControl.PageCount-2;
     Parametros.PageIndex := WizardControl.PageCount-3;
end;

procedure TWizNomRastrear_DevEx.FormShow(Sender: TObject);
begin
     dmCatalogos.cdsConceptos.Conectar;
     inherited;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     Empleado.Valor := dmCliente.Empleado;
     with Concepto do
     begin
          if ( Valor = 0 ) then
             Valor := 51;
     end;
     Advertencia.Caption := ' Se realizar� el rastreo del concepto y empleado indicados en la n�mina.';
end;

procedure TWizNomRastrear_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     inherited;
     Archivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, Archivo.Text, 'txt' );
end;

procedure TWizNomRastrear_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          AddString( 'Empleado' , Empleado.Llave);
          AddString( 'C�digo concepto' , Concepto.Llave);
     end;
     with ParameterList do
     begin
          with Concepto do
          begin
               AddInteger( 'NumeroConcepto', Valor );
               AddString( 'DesConcepto' , Descripcion );
               AddString( 'Formula' , LookUpDataSet.FieldByName( 'CO_FORMULA' ).AsString );
          end;
          AddBoolean( 'MostrarNomParam' ,lNomParam.Checked );
          AddBoolean('MuestraScript', FALSE );

          AddString( 'Archivo', Archivo.Text );
          with Empleado do
          begin
               AddInteger( 'Empleado' , Valor );
               AddString( 'Nombre', Descripcion );
          end;
     end;
end;

function TWizNomRastrear_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.Rastrear(ParameterList);
end;

end.

