inherited WizNomImportarMov_DevEx: TWizNomImportarMov_DevEx
  Left = 583
  Top = 192
  Caption = 'Importar Movimientos de N'#243'mina'
  ClientHeight = 408
  ClientWidth = 551
  ExplicitWidth = 557
  ExplicitHeight = 437
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 551
    Height = 408
    ExplicitWidth = 551
    ExplicitHeight = 337
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        ' Proceso que importa los movimientos de n'#243'mina a un periodo, de ' +
        'los conceptos que se indiquen.'
      Header.Title = 'Importar movimientos de n'#243'mina'
      ExplicitWidth = 529
      ExplicitHeight = 268
      inherited GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 529
        Height = 268
        Align = alClient
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 529
        ExplicitHeight = 211
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 529
      ExplicitHeight = 268
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 529
        ExplicitHeight = 191
        Height = 173
        Width = 529
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 529
        Width = 529
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitWidth = 461
          Width = 461
          AnchorY = 57
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      PageVisible = False
      ExplicitWidth = 529
      ExplicitHeight = 268
      inherited sCondicionLBl: TLabel
        Left = 6
        Top = 137
        ExplicitLeft = 6
        ExplicitTop = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 31
        Top = 166
        ExplicitLeft = 31
        ExplicitTop = 166
      end
      inherited Seleccionar: TcxButton
        Left = 134
        Top = 254
        ExplicitLeft = 134
        ExplicitTop = 254
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
        Top = 134
        ExplicitLeft = 58
        ExplicitTop = 134
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Top = 165
        Style.IsFontAssigned = True
        ExplicitLeft = 58
        ExplicitTop = 165
      end
      inherited GBRango: TGroupBox
        Left = 58
        Top = 4
        ExplicitLeft = 58
        ExplicitTop = 4
      end
      inherited bAjusteISR: TcxButton
        Left = 380
        Top = 166
        ExplicitLeft = 380
        ExplicitTop = 166
      end
    end
    object Archivo: TdxWizardControlPage
      Header.Description = 'Seleccione los par'#225'metros de carga de archivo.'
      Header.Title = 'Archivo'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 529
        Height = 111
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Label5: TLabel
          Left = 153
          Top = 30
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'A&rchivo:'
          FocusControl = sArchivo
        end
        object Label7: TLabel
          Left = 81
          Top = 55
          Width = 111
          Height = 13
          Alignment = taRightJustify
          Caption = 'Operaci'#243'n con &Montos:'
          FocusControl = iOperacion
        end
        object Label8: TLabel
          Left = 110
          Top = 78
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de &Formato:'
          FocusControl = iFormato
        end
        object BGuardaArchivo: TcxButton
          Left = 503
          Top = 26
          Width = 21
          Height = 21
          Hint = 'Buscar archivo a importar'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
            FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
            FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
            F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
            F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
            F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
            F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
            FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BGuardaArchivoClick
        end
        object sArchivo: TEdit
          Left = 196
          Top = 26
          Width = 300
          Height = 21
          TabOrder = 1
        end
        object iOperacion: TZetaKeyCombo
          Left = 196
          Top = 51
          Width = 159
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
          ListaFija = lfOperacionMontos
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object iFormato: TZetaKeyCombo
          Left = 196
          Top = 76
          Width = 159
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 4
          ListaFija = lfFormatoASCII
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object CBNoIncapacitados: TCheckBox
          Left = 12
          Top = 7
          Width = 198
          Height = 17
          Alignment = taLeftJustify
          Caption = 'No &importar empleados incapacitados:'
          TabOrder = 0
        end
      end
      object PanelAnimation: TPanel
        Left = 0
        Top = 111
        Width = 529
        Height = 157
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitHeight = 175
        object Animate: TAnimate
          Left = 0
          Top = 115
          Width = 529
          Height = 25
          Align = alBottom
          Active = True
          AutoSize = False
          CommonAVI = aviCopyFiles
          StopFrame = 34
          ExplicitTop = 133
        end
        object PanelMensaje: TPanel
          Left = 0
          Top = 140
          Width = 529
          Height = 17
          Align = alBottom
          BevelOuter = bvNone
          Caption = 'Verificando Archivo ...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          ExplicitTop = 158
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 504
    Top = 4
  end
  object OpenDialog: TOpenDialog
    Filter = 'ASCII Movimientos (*.dat)|*.dat|Todos (*.*)|*.*'
    Options = [ofHideReadOnly, ofAllowMultiSelect]
    Left = 348
    Top = 65534
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'txt'
    FileName = 'ImpError'
    Filter = 
      'Archivos Texto ( *.txt )|*.txt|Archivos Datos ( *.dat )|*.dat|Bi' +
      't'#225'coras (*.log)|*.log|Todos |*.*'
    FilterIndex = 0
    Title = 'Grabar Errores En'
    Left = 377
    Top = 65534
  end
end
