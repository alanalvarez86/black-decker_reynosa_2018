unit FWizEmpCursoTomado_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, Mask,
     FWizEmpBaseFiltro,
     ZetaFecha,
     ZetaEdit,
     ZetaHora, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, ZetaNumero;

type
  TWizEmpCursoTomado_DevEx = class(TWizEmpBaseFiltro)
    KC_EST: TZetaKeyLookup_DevEx;
    SE_COSTO3: TZetaNumero;
    SE_COSTO2: TZetaNumero;
    SE_COSTO1: TZetaNumero;
    SE_LUGAR: TEdit;
    SE_COMENTA: TEdit;
    SE_HOR_FIN: TZetaHora;
    SE_HOR_INI: TZetaHora;
    SE_CUPO: TZetaNumero;
    KC_REVISIO: TEdit;
    dFinal: TZetaFecha;
    sMaestro: TZetaKeyLookup_DevEx;
    sCurso: TZetaKeyLookup_DevEx;
    rHoras: TZetaNumero;
    dInicio: TZetaFecha;
    rEvaluacion: TZetaNumero;
    lblEstablec: TLabel;
    SE_COSTO3Lbl: TLabel;
    SE_COSTO2Lbl: TLabel;
    SE_COSTO1Lbl: TLabel;
    lblComenta: TLabel;
    lblLugar: TLabel;
    lblHorFin: TLabel;
    lblHorIni: TLabel;
    lblCupo: TLabel;
    lblRevision: TLabel;
    KC_FEC_FINLbl: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure sCursoValidKey(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);//DevEx(by am): Agregado para enfocar los controles correctamente
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    FCursoActual : String;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpCursoTomado_DevEx: TWizEmpCursoTomado_DevEx;

implementation

uses DCliente,
     DCatalogos,
     DProcesos,
     DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     ZBaseSelectGrid_DevEx,
     FEmpCursoTomadoGridSelect_DevEx;

{$R *.DFM}

{ TWizEmpCursoTomado }

procedure TWizEmpCursoTomado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := sCurso;
     dInicio.Valor := dmCliente.FechaDefault;
     dFinal.Valor := dInicio.Valor;
     HelpContext := H10170_Curso_tomado_global;
     sCurso.LookupDataset := dmCatalogos.cdsCursos;
     sMaestro.LookupDataset := dmCatalogos.cdsMaestros;
     KC_EST.LookupDataset := dmCatalogos.cdsEstablecimientos;
     FCursoActual := ZetaCommonClasses.VACIO;

     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TWizEmpCursoTomado_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          cdsMaestros.Conectar;
          cdsEstablecimientos.Conectar;
     end;
     //Texto para la ejecucion final
     Advertencia.Caption := 'Al aplicar el proceso se registrar� en el historial de cursos de cada empleado , el nombre del curso y la fecha en que se tom�.';
end;

procedure TWizEmpCursoTomado_DevEx.CargaParametros;

 function GetCosto( const rValor: TPesos ): string;
 begin
      Result := VACIO;
      if ( rValor <> 0 ) then
         Result := FloatToStr( rValor );
 end;

begin
     inherited CargaParametros;
     with Descripciones do
     begin
          AddString( 'Curso', GetDescripLlave( sCurso ) );
          AddString( 'Revisi�n', KC_REVISIO.Text );
          AddString( 'Del', FechaCorta( dInicio.Valor ) + ' al ' + FechaCorta( dFinal.Valor ) );
          AddString( 'Horario', MaskHora( SE_HOR_INI.Valor ) + ' a ' + MaskHora( SE_HOR_FIN.Valor ) );
          AddFloat( 'Duraci�n', rHoras.Valor );
          AddFloat( 'Evaluaci�n', rEvaluacion.Valor );
          AddInteger( 'Cupo', SE_CUPO.ValorEntero );
          AddString( 'Maestro', GetDescripLlave( sMaestro ) );
          AddString( 'Costos', ConcatString( GetCosto( SE_COSTO1.Valor ),
                               ConcatString( GetCosto( SE_COSTO2.Valor ),
                               GetCosto( SE_COSTO3.Valor ), ','), ',' ) );
          AddString( 'Lugar', SE_LUGAR.Text );
          AddString( 'Establecimiento',GetDescripLlave( KC_EST ) );
          AddString( 'Comentarios', SE_COMENTA.Text );
     end;

     with ParameterList do
     begin
          AddString( 'Curso', sCurso.Llave );
          AddString( 'Revision', KC_REVISIO.Text );
          AddString( 'Maestro', SMaestro.Llave );
          AddDate( 'Inicio', dInicio.Valor );
          AddDate( 'Final', dFinal.Valor );
          AddFloat( 'Evaluacion', rEvaluacion.Valor );
          AddFloat( 'Horas', rHoras.Valor );
          AddString( 'HoraInicio', SE_HOR_INI.Valor );
          AddString( 'HoraFin', SE_HOR_FIN.Valor );
          AddInteger( 'Cupo', SE_CUPO.ValorEntero );
          AddFloat( 'Costo1', SE_COSTO1.Valor );
          AddFloat( 'Costo2', SE_COSTO2.Valor );
          AddFloat( 'Costo3', SE_COSTO3.Valor );
          AddString( 'Establec', KC_EST.Llave );
          AddString( 'Lugar', SE_LUGAR.Text );
          AddString( 'Comentarios', SE_COMENTA.Text );

     end;
end;

procedure TWizEmpCursoTomado_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CursoTomadoGetLista( ParameterList );
end;

function TWizEmpCursoTomado_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEX.GridSelect( dmProcesos.cdsDataset, TEmpCursoTomadoGridSelect_DevEx );
end;

procedure TWizEmpCursoTomado_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
Const
     K_CODIGO_INVALIDO = 'C�digo Inactivo!!!';
begin

     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros ) then
          begin
               if StrVacio( sCurso.Llave ) then
                  CanMove := Error( '� No se ha especificado un curso !', sCurso )
               else
               if ( dFinal.Valor < dInicio.Valor ) then
                  CanMove := Error( '� Fecha final del curso NO puede ser menor que la inicial !', dFinal )
               else
               if ( rHoras.Valor <= 0 ) then
                  CanMove := Error( '� Duraci�n del curso debe ser mayor a cero !', rHoras )
               else
               if ( rEvaluacion.Valor < 0 ) then
                  CanMove := Error( '� Evaluaci�n debe ser mayor o igual a cero !', rEvaluacion );
               //Validaci�n de catalogos inactivos   @DACP
               if  Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS ) then
               begin
                    if sCurso.Descripcion = K_CODIGO_INVALIDO then
                       CanMove := FALSE;
                    if sMaestro.Descripcion = K_CODIGO_INVALIDO then
                       CanMove := FALSE;
                    if KC_EST.Descripcion = K_CODIGO_INVALIDO then
                       CanMove := FALSE;
               end;
          end;
     end;
     inherited;
end;

procedure TWizEmpCursoTomado_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control de la pagina de parametros
     if (Wizard.EsPaginaActual( Parametros ))then
        ParametrosControl := sCurso;
     inherited;
end;

procedure TWizEmpCursoTomado_DevEx.sCursoValidKey(Sender: TObject);
begin
     inherited;
     if StrLleno( sCurso.Llave ) and ( sCurso.Llave <> FCursoActual ) then  // Para que no vuelva a asignar los valores cuando se utilize el [Anterior]
     begin
          with dmCatalogos.cdsCursos do
          begin
               sMaestro.Llave := FieldByName( 'MA_CODIGO' ).AsString;
               rHoras.Valor := FieldByName( 'CU_HORAS' ).AsFloat;
               KC_REVISIO.Text := FieldByName( 'CU_REVISIO' ).AsString;
               SE_COSTO1.Valor := FieldByName( 'CU_COSTO1' ).AsFloat;
               SE_COSTO2.Valor := FieldByName( 'CU_COSTO2' ).AsFloat;
               SE_COSTO3.Valor := FieldByName( 'CU_COSTO3' ).AsFloat;
          end;
          FCursoActual := sCurso.Llave;
     end;
end;

function TWizEmpCursoTomado_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CursoTomado( ParameterList, Verificacion );
end;

procedure TWizEmpCursoTomado_DevEx.SetEditarSoloActivos;
begin
     sCurso.EditarSoloActivos := TRUE;
     sMaestro.EditarSoloActivos := TRUE;
     KC_EST.EditarSoloActivos := TRUE;
end;

end.



