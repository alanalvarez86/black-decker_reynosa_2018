unit FWizNomTransferenciasCosteo_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
     ZCXBaseWizardFiltro, ZetaEdit, ZetaDBTextBox,
     ZetaCommonLists, ZetaKeyCombo, ZetaNumero, Mask, ZetaFecha,
     cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
     cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl, cxCheckBox, FWizNomBase_DevEx;

type
  TWizNomTransferenciasCosteo_DevEx = class(TBaseCXWizardFiltro)
    lbOriginal: TLabel;
    CC_Original: TZetaKeyLookup_DevEx;
    GroupBox2: TcxGroupBox;
    Label5: TLabel;
    HorasLbl: TLabel;
    lbDestino: TLabel;
    MotivoLbl: TLabel;
    Label6: TLabel;
    TipoAutLbl: TLabel;
    AU_FECHA: TZetaFecha;
    TR_HORAS: TZetaNumero;
    CC_DESTINO: TZetaKeyLookup_DevEx;
    TR_MOTIVO: TZetaKeyLookup_DevEx;
    TR_TEXTO: TZetaEdit;
    TR_TIPO: TZetaKeyCombo;
    cbDuplicados: TcxCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FNivel: string;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
    procedure CargaListaVerificacion;override;
    function Verificar: Boolean;override;
  public
    { Public declarations }
  end;

var
  WizNomTransferenciasCosteo_DevEx: TWizNomTransferenciasCosteo_DevEx;

implementation

uses DProcesos,
     DCliente,
     DGlobal,
     dTablas,
     ZGlobalTress,
     ZAccesosMgr,
     ZAccesosTress,
     ZBaseSelectGrid_DevEx,
     ZetaCommonClasses,
     ZetaCommonTools,
     FTressShell,
     FNomTransferenciasCosteoGridSelect_DevEx, ZcxWizardBasico;

{$R *.DFM}

procedure TWizNomTransferenciasCosteo_DevEx.FormCreate(Sender: TObject);

begin
     inherited;
     ParametrosControl := AU_FECHA;
     HelpContext:= H_Agregar_Transferencias;
     with Global do
     begin
          FNivel := Global.NombreCosteo;
          Caption := 'Transferencias de Empleados por ' + FNivel;
          lbDestino.Caption := 'Transferir hacia:';
          lbOriginal.Caption := FNivel + ' Original:';
          cbDuplicados.Caption := 'Validar que no exista transferencia anterior a mismo(a) ' + FNivel;
     end;

     AU_FECHA.Valor := dmCliente.GetDatosPeriodoActivo.Inicio;
     TR_TIPO.ItemIndex := ord( tcOrdinarias );
     TR_TEXTO.MaxLength := K_ANCHO_DESCRIPCION;

     TR_MOTIVO.LookupDataset := dmTablas.cdsMotivoTransfer;
     CC_DESTINO.LookupDataset := dmTablas.GetDataSetTransferencia;
     CC_ORIGINAL.LookupDataset := dmTablas.GetDataSetTransferencia(FALSE);

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizNomTransferenciasCosteo_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddDate( 'FechaInicio', AU_FECHA.Valor );
          AddFloat( 'Horas', TR_HORAS.Valor );
          AddString( 'Motivo', TR_MOTIVO.Llave );
          AddString( 'Observa', TR_TEXTO.Text );
          AddString( 'CCDestino', CC_DESTINO.LLave );
          AddString( 'CCOriginal', CC_ORIGINAL.LLave );
          AddInteger ('Tipo', TR_TIPO.ItemIndex );
          AddBoolean( 'SoloUnaTransferencia', cbDuplicados.Checked );
          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
     end;

     with Descripciones do
     begin
          //Clear;   //DevEx
          AddString ( 'Fecha Inicial' , FechaCorta( AU_FECHA.Valor ) );
          AddString ( 'Duraci�n', Format( ' %s horas', [ FloattoStr( TR_HORAS.Valor )] ) );
          AddString ( Format( '%s Destino',  [ FNivel] ),  CC_DESTINO.LLave + ': ' + CC_DESTINO.Descripcion ) ;
          if ( Trim(CC_ORIGINAL.Descripcion) = ''  ) then
             AddString ( Format( '%s Original ',  [ FNivel] ), CC_ORIGINAL.LLave)
          else
              AddString ( Format( '%s Original ',  [ FNivel] ), CC_ORIGINAL.LLave  + ': ' + CC_ORIGINAL.Descripcion ) ;

          if ( Trim(TR_MOTIVO.Descripcion) = ''  ) then
             AddString ( 'Motivo' ,  TR_MOTIVO.Llave )
          else
              AddString ( 'Motivo' ,  TR_MOTIVO.Llave  + ': ' + TR_MOTIVO.Descripcion);
          AddString ( 'Observaciones' , TR_TEXTO.Text );
          AddString ( 'Tipo de Transferencia' , ObtieneElemento( lfTipoHoraTransferenciaCosteo, TR_TIPO.ItemIndex ) );
          AddString ( 'Validar sin Transf. Anterior', zBooltostr( cbDuplicados.Checked )  );
     end;

end;

procedure TWizNomTransferenciasCosteo_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if Adelante and EsPaginaActual( Parametros ) then
          begin
               if CC_DESTINO.LookupDataset = nil then
                  CanMove := Error( 'El Nivel de Organigrama para Costeo no ha sido definido.' + CR_LF +
                                    'No se podr�n agregar o modificar las Transferencias.', TWinControl( Salir ) )  //DevEx
               else if ( AU_FECHA.Valor = NullDateTime ) then
                  CanMove := Error( 'La fecha no puede quedar vac�a', AU_FECHA )
               else if ( TR_HORAS.Valor = 0 ) then
                  CanMove := Error( 'El valor de horas no puede quedar en cero', TR_HORAS )
               else if StrVacio( CC_DESTINO.LLave ) then
                  with Global do
                       CanMove := Error( Format( '%s destino no puede quedar vac�o(a)', [Global.NombreCosteo] ), CC_DESTINO );
          end;
     end;//with
end;

function TWizNomTransferenciasCosteo_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.TransferenciasCosteo(ParameterList, Verificacion );
end;

procedure TWizNomTransferenciasCosteo_DevEx.CargaListaVerificacion;
begin
     dmProcesos.TransferenciasCosteoGetLista( ParameterList );
end;

function TWizNomTransferenciasCosteo_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TNomTransferenciasCosteoGridSelect_DevEx );
end;


procedure TWizNomTransferenciasCosteo_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmTablas do
     begin
          cdsMotivoTransfer.Conectar;
          if CC_ORIGINAL.LookupDataset <> NIL then
             CC_ORIGINAL.LookupDataset.Conectar;
     end;

     //DevEx
     Advertencia.Caption :=
        'Al ejecutar el proceso quedan registradas las transferencias de los empleados seleccionados al nivel de costeo destino.';
end;

//DevEx
procedure TWizNomTransferenciasCosteo_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := AU_FECHA
     else
         ParametrosControl := nil;
     inherited;
end;


end.
