inherited WizNomAfectar_DevEx: TWizNomAfectar_DevEx
  Left = 188
  Top = 123
  Caption = 'Afectar N'#243'mina'
  ClientHeight = 423
  ClientWidth = 439
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 439
    Height = 423
    ExplicitWidth = 439
    ExplicitHeight = 423
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que cierra una n'#243'mina a cualquier modificaci'#243'n y suma su' +
        's montos a los acumulados correspondientes.'
      Header.Title = 'Afectar n'#243'mina'
      ExplicitWidth = 417
      ExplicitHeight = 283
      inherited GroupBox1: TGroupBox
        Top = 41
        ExplicitTop = 41
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 417
      ExplicitHeight = 283
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 417
        ExplicitHeight = 188
        Height = 188
        Width = 417
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 417
        Width = 417
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitWidth = 349
          Width = 349
          AnchorY = 57
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      ExplicitWidth = 417
      ExplicitHeight = 283
      inherited sCondicionLBl: TLabel
        Left = 14
        ExplicitLeft = 14
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        ExplicitLeft = 39
      end
      inherited Seleccionar: TcxButton
        Left = 142
        ExplicitLeft = 142
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        ExplicitLeft = 66
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Style.IsFontAssigned = True
        ExplicitLeft = 66
      end
      inherited GBRango: TGroupBox
        Left = 66
        ExplicitLeft = 66
      end
      inherited bAjusteISR: TcxButton
        Left = 380
        ExplicitLeft = 380
      end
    end
  end
end
