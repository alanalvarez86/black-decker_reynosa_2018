unit FWizEmpImportarImagenes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseWizard, ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  FileCtrl, jpeg, Grids, DBGrids, ZetaClientDataSet, DB, ZetaCommonClasses;

type
  TWizEmpImportarImagenes = class(TBaseWizard)
    PnlInicial: TPanel;
    DirectorioLBL: TLabel;
    Directorio: TEdit;
    ImportFileSeek: TSpeedButton;
    Filtro: TFilterComboBox;
    lblTipoArchivos: TLabel;
    lblTipoImagen: TLabel;
    TipoImagen: TEdit;
    lblObservaciones: TLabel;
    Observaciones: TEdit;
    DataSource: TDataSource;
    procedure ImportFileSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FCtr: Integer;
    function GetImportDirectory: String;
    function GetImageFilter: String;
    function LeerDirectorio: Boolean;    
  Protected
    { Protected declarations }
    procedure CargaParametros; override;
    function Verificar: Boolean;
    procedure CargaListaVerificacion;
    function EjecutarWizard: Boolean; override;    
  public
    { Public declarations }
    procedure ShowError( const sMensaje: String; Error: Exception );    
  end;

var
  WizEmpImportarImagenes: TWizEmpImportarImagenes;
  ParametrosGrid: TzetaParams;  

implementation

uses ZetaMigrar,
     ZetaDialogo,
     ZetaCommonTools,
     DRecursos,
     FBaseReportes,
     ZBaseSelectGrid,
     FImportarImagenGridSelect,
     DProcesos;

{$R *.dfm}

function TWizEmpImportarImagenes.Verificar: Boolean;
begin
     dmProcesos.cdsDataset.First;
     Result := ZBaseSelectGrid.GridSelect( dmProcesos.cdsDataset, TImportarImagenGridSelect, FALSE );
end;

procedure TWizEmpImportarImagenes.CargaListaVerificacion;
begin
     with dmRecursos do
     begin
           CargaGridImagenesImportar(ParameterList, GetImageFilter, TRUE );
           dmProcesos.cdsDataset := cdsImagenes;
           dmProcesos.cdsDataSet.MergeChangeLog;
     end;
end;

function TWizEmpImportarImagenes.LeerDirectorio: Boolean;
begin
     Application.ProcessMessages;
     CargaParametros;
     CargaListaVerificacion;
     Result := Verificar;
end;

procedure TWizEmpImportarImagenes.CargaParametros;
begin
     with ParameterList do
     begin
          AddString( 'Directorio', Directorio.Text);
          AddString( 'Filtro', Filtro.Text);
          AddString( 'Tipo', TipoImagen.Text);
          AddMemo( 'Observaciones', Observaciones.Text );
     end;
end;

procedure TWizEmpImportarImagenes.ShowError( const sMensaje: String; Error: Exception );
begin
     zExcepcion( Caption, sMensaje, Error, 0 );
end;

function TWizEmpImportarImagenes.GetImportDirectory: String;
begin
     Result := Directorio.Text;
end;

function TWizEmpImportarImagenes.GetImageFilter: String;
begin
     Result := ZetaCommonTools.SetFileNamePath( GetImportDirectory, Filtro.Mask );
end;

procedure TWizEmpImportarImagenes.ImportFileSeekClick(Sender: TObject);
var
   sDirectory: String;
begin
     inherited;
     with Directorio do
     begin
          sDirectory := Text;
          if SelectDirectory( sDirectory, [], 0 ) then
             Text := sDirectory;
     end;
end;

procedure TWizEmpImportarImagenes.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;

function HayArchivos( const sFileSpec: String ): Boolean;
var
   Datos: TSearchRec;
begin
     FCtr := 0;
     if ( FindFirst( sFileSpec, faAnyFile, Datos ) = 0 ) then
     begin
          Inc( FCtr );
          while ( FindNext( Datos ) = 0 ) do
          begin
               Inc( FCtr );
          end;
          FindClose( Datos );
     end;
     Result := ( FCtr > 0 );
end;

begin
     inherited;
     if CanMove then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if Wizard.Adelante then
             begin
                  if ( PageControl.ActivePage = Parametros ) then
                  begin
                       if not DirectoryExists( GetImportDirectory ) then
                       begin
                            ZetaDialogo.zWarning( Caption, 'Directorio de im�genes no existe', 0, mbOK );
                            ActiveControl := Directorio;
                            CanMove := False;
                       end
                       else
                       begin
                            if not HayArchivos( GetImageFilter ) then
                            begin
                                 ZetaDialogo.zWarning( Caption, 'No se encontraron im�genes en el folder especificado', 0, mbOK );
                                 ActiveControl := Directorio;
                                 CanMove := False;
                            end
                            else
                            begin
                                 CanMove := LeerDirectorio;
                            end;
                       end;
                  end
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TWizEmpImportarImagenes.FormCreate(Sender: TObject);
begin
     FCtr := 0;                                 
     HelpContext:= H_EMP_IMPORTAR_IMAGENES;
     inherited;
end;

function TWizEmpImportarImagenes.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarImagenes( ParameterList );
end;


end.
