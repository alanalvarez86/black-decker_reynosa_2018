inherited WizEmpFoliarCapacitacionesSTPS_DevEx: TWizEmpFoliarCapacitacionesSTPS_DevEx
  Left = 666
  Top = 206
  Caption = 'Foliar Capacitaciones de STPS'
  ClientHeight = 424
  ClientWidth = 425
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 425
    Height = 424
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que permite asignar una clave de control a las capacitac' +
        'iones que se tomaron durante el periodo indicado.'
      Header.Title = 'Foliar capacitaciones de STPS'
      object FolioInicialLbl: TLabel
        Left = 153
        Top = 173
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'F&olio inicial:'
        FocusControl = FolioInicial
        Transparent = True
      end
      object GroupBox1: TGroupBox
        Left = 104
        Top = 57
        Width = 217
        Height = 97
        Caption = 'Periodo de capacitaciones'
        TabOrder = 0
        object InicioLbl: TLabel
          Left = 32
          Top = 32
          Width = 28
          Height = 13
          Caption = '&Inicio:'
          FocusControl = fechaInicio
        end
        object FinLbl: TLabel
          Left = 40
          Top = 56
          Width = 17
          Height = 13
          Caption = '&Fin:'
          FocusControl = fechaFin
        end
        object fechaInicio: TZetaFecha
          Left = 64
          Top = 30
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '24/sep/12'
          Valor = 41176.000000000000000000
          OnChange = fechaInicioChange
        end
        object fechaFin: TZetaFecha
          Left = 64
          Top = 54
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '24/sep/12'
          Valor = 41176.000000000000000000
          OnChange = fechaFinChange
        end
      end
      object FolioInicial: TZetaNumero
        Left = 210
        Top = 169
        Width = 50
        Height = 21
        Mascara = mnDias
        TabOrder = 1
        Text = '0'
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 189
        Width = 403
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 403
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 335
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 6
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 31
        Top = 166
      end
      inherited Seleccionar: TcxButton
        Left = 134
        Top = 254
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Top = 165
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 58
        Top = 4
      end
      inherited bAjusteISR: TcxButton
        Left = 372
        Top = 166
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
