unit FWizNomCalcularAguinaldo_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     ZetaEdit,
     ZetaWizard,
     ZetaFecha,
     ZetaNumero,
     ZetaDBTextBox, ZCXBaseWizardFiltro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomCalcularAguinaldo_DevEx = class(TBaseCXWizardFiltro)
    Faltas: TcxMemo;
    FaltasLBL: TLabel;
    Anticipos: TcxMemo;
    Incapacidades: TcxMemo;
    Year: TZetaNumero;
    YearLBL: TLabel;
    FechaInicialLBL: TLabel;
    FechaInicial: TZetaFecha;
    FechaFinalLBL: TLabel;
    FechaFinal: TZetaFecha;
    Proporcional: TRadioGroup;
    IncapacidadesLBL: TLabel;
    AnticiposLBL: TLabel;
    BitBtn1: TcxButton;
    BitBtn2: TcxButton;
    BitBtn3: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure YearExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    procedure InitFechas;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizNomCalcularAguinaldo_DevEx: TWizNomCalcularAguinaldo_DevEx;

implementation

uses DCliente,
     DGlobal,
     DProcesos,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZGlobalTress,
     ZConstruyeFormula;

{$R *.DFM}

procedure TWizNomCalcularAguinaldo_DevEx.FormCreate(Sender: TObject);
begin
     Year.Valor := dmCliente.YearDefault;
     Anticipos.Tag := K_GLOBAL_DEF_AGUINALDO_ANTICIPOS;
     inherited;
     ParametrosControl := Year;
     HelpContext := H30352_Calcular_aguinaldos;
     InitFechas;
end;

procedure TWizNomCalcularAguinaldo_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Faltas.Text := Global.GetGlobalString( K_GLOBAL_DEF_AGUINALDO_FALTAS );
     Incapacidades.Text := Global.GetGlobalString( K_GLOBAL_DEF_AGUINALDO_INCAPACIDADES );
end;

procedure TWizNomCalcularAguinaldo_DevEx.InitFechas;
begin
     with Year do
     begin
          FechaInicial.Valor := FirstDayOfYear( ValorEntero );
          FechaFinal.Valor := FirstDayOfYear( ValorEntero + 1 ) - 1;
     end;
end;

procedure TWizNomCalcularAguinaldo_DevEx.YearExit(Sender: TObject);
begin
     inherited;
     InitFechas;
end;

procedure TWizNomCalcularAguinaldo_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddInteger( 'Year', Year.ValorEntero );
          AddInteger( 'Proporcional', iMax( 0, Proporcional.ItemIndex ) );
          AddDate( 'dInicial', FechaInicial.Valor );
          AddDate( 'dFinal', FechaFinal.Valor );
          AddString( 'Faltas', Faltas.Text );
          AddString( 'Incapacidades', Incapacidades.Text );
          AddString( 'Anticipos', Anticipos.Text );
     end;
          with Descripciones do
     begin
          AddInteger( 'A�o', Year.ValorEntero );
          AddDate( 'Fecha Inicial', FechaInicial.Valor );
          AddDate( 'Fecha Final', FechaFinal.Valor );
          AddString( 'Descontar Faltas', Faltas.Text );
          AddString( 'Descontar Incapacidades', Incapacidades.Text );
          AddString( 'Descontar Anticipos', Anticipos.Text );
          AddString( 'Proporcional a', Proporcional.items[Proporcional.ItemIndex]  );
     end;
end;

function TWizNomCalcularAguinaldo_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CalcularAguinaldo( ParameterList );
end;

procedure TWizNomCalcularAguinaldo_DevEx.BitBtn1Click(Sender: TObject);
begin
     Faltas.Text:= GetFormulaConst( enEmpleado , Faltas.Text, Faltas.SelStart, evAguinaldo );
end;

procedure TWizNomCalcularAguinaldo_DevEx.BitBtn2Click(Sender: TObject);
begin
     Incapacidades.Text:= GetFormulaConst( enEmpleado , Incapacidades.Text, Incapacidades.SelStart, evAguinaldo );
end;

procedure TWizNomCalcularAguinaldo_DevEx.BitBtn3Click(Sender: TObject);
begin
     Anticipos.Text:= GetFormulaConst( enEmpleado , Anticipos.Text, Anticipos.SelStart, evAguinaldo );
end;

procedure TWizNomCalcularAguinaldo_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
  inherited;
  ParametrosControl :=nil;
end;

procedure TWizNomCalcularAguinaldo_DevEx.WizardAfterMove(Sender: TObject);
begin
  inherited;
  if Wizard.EsPaginaActual(Parametros) then
  ParametrosControl := Year;
end;

end.
