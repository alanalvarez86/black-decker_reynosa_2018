unit FWizEmpFoliarCapacitacionesSTPS_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FWizEmpBaseFiltro, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, Mask, ZetaNumero, ZetaFecha,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizEmpFoliarCapacitacionesSTPS_DevEx = class(TWizEmpBaseFiltro)
    GroupBox1: TGroupBox;
    InicioLbl: TLabel;
    FinLbl: TLabel;
    fechaInicio: TZetaFecha;
    fechaFin: TZetaFecha;
    FolioInicial: TZetaNumero;
    FolioInicialLbl: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure fechaInicioChange(Sender: TObject);
    procedure fechaFinChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Se agrega para validar el enfoque de controles.
  private
    { Private declarations }  
  protected
    { Private declarations }
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;    
  public
    { Public declarations }
  end;

var
  WizEmpFoliarCapacitacionesSTPS_DevEx: TWizEmpFoliarCapacitacionesSTPS_DevEx;

implementation

uses
    DCliente,
    DProcesos,
    ZetaCommonClasses,
    ZBaseSelectGrid_DevEx,
    FEmpFoliarCapacitacionesSTPSGridSelect_DevEx,
    ZetaCommonTools;

{$R *.dfm}

procedure TWizEmpFoliarCapacitacionesSTPS_DevEx.FormCreate(Sender: TObject);
begin  
     inherited;
     fechaInicio.Valor := FirstDayOfYear (dmCliente.YearDefault - 1);
     fechaFin.Valor := LastDayOfYear (dmCliente.YearDefault - 1);
     FolioInicial.Valor := 1;
     ParametrosControl := fechaInicio;
     HelpContext := H_Foliar_Capacitaciones;
     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizEmpFoliarCapacitacionesSTPS_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
           if Wizard.EsPaginaActual( Parametros )then
          begin
               // if (fechaInicio.Valor > fechaFin.Valor) then
                  // CanMove := Error ('La fecha final es anterior a la fecha inicial', fechaInicio);

               if FolioInicial.Valor <= 0 then
                 CanMove := Error( 'El folio inicial debe de tener un valor igual o mayor a 1', FolioInicial );
          end;
     end;
     inherited;
end;

function TWizEmpFoliarCapacitacionesSTPS_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.FoliarCapacitacionesSTPS( ParameterList, Verificacion );
end;

procedure TWizEmpFoliarCapacitacionesSTPS_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with Descripciones do
     begin
          AddDate( 'Fecha Inicio', fechaInicio.Valor ); 
          AddDate( 'Fecha Fin', fechaFin.Valor );
          AddInteger( 'Folio Inicial', FolioInicial.ValorEntero );
          AddDate( 'Fecha del sistema', dmCliente.FechaDefault );
          AddString('Reinicio de Folio', BoolAsSiNo(false));
     end;

     with ParameterList do
     begin
          AddDate( 'FechaInicio', fechaInicio.Valor );
          AddDate( 'FechaFin', fechaFin.Valor );
          AddInteger( 'FolioInicial', FolioInicial.ValorEntero );
          AddDate('Fecha', dmCliente.FechaDefault);
          AddBoolean('ReinicioFolio', false);
     end;
end;

function TWizEmpFoliarCapacitacionesSTPS_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpFoliarCapacitacionesSTPSGridSelect_DevEx );
end;

procedure TWizEmpFoliarCapacitacionesSTPS_DevEx.CargaListaVerificacion;
begin
     dmProcesos.FoliarCapacitacionesSTPSGetLista (ParameterList );
end;

procedure TWizEmpFoliarCapacitacionesSTPS_DevEx.fechaInicioChange(
  Sender: TObject);
begin
  inherited;
  if (fechaInicio.Valor > fechaFin.Valor) then
     fechaFin.Valor := fechaInicio.Valor; 
end;

procedure TWizEmpFoliarCapacitacionesSTPS_DevEx.fechaFinChange(Sender: TObject);
begin
  inherited;
  if (fechaFin.Valor < fechaInicio.Valor) then
     fechaFin.valor := fechaInicio.valor;
end;

procedure TWizEmpFoliarCapacitacionesSTPS_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption :='Al Aplicar el proceso se foliaran las capacitaciones del periodo indicado.'; 
end;

procedure TWizEmpFoliarCapacitacionesSTPS_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual( Parametros )then
         ParametrosControl := fechaInicio;
     inherited;

end;

end.
