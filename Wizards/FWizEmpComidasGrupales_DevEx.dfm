inherited WizEmpComidasGrupales_DevEx: TWizEmpComidasGrupales_DevEx
  Left = 353
  Top = 196
  Caption = 'Registro Grupal de Comidas'
  ClientHeight = 427
  ClientWidth = 455
  ExplicitWidth = 461
  ExplicitHeight = 456
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 455
    Height = 427
    ExplicitWidth = 455
    ExplicitHeight = 427
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso para realizar el registro de varios consumos a un grupo ' +
        'de empleados o invitadores.'
      Header.Title = 'Registro grupal de comidas'
      ExplicitWidth = 433
      ExplicitHeight = 287
      object GroupBox1: TcxGroupBox
        Left = 0
        Top = 170
        Align = alTop
        Caption = '                                    '
        TabOrder = 1
        Height = 80
        Width = 433
        object lblNomArchivo: TLabel
          Left = 11
          Top = 34
          Width = 105
          Height = 13
          Alignment = taRightJustify
          Caption = 'Archivo de Checadas:'
          Enabled = False
          Transparent = True
        end
        object ArchivoSeek: TcxButton
          Left = 393
          Top = 30
          Width = 21
          Height = 21
          Hint = 'Buscar Archivo'
          Enabled = False
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
            FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
            FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
            F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
            F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
            F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
            F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
            FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = ArchivoSeekClick
        end
        object edArchivo: TEdit
          Left = 120
          Top = 30
          Width = 270
          Height = 21
          Enabled = False
          MaxLength = 79
          TabOrder = 1
        end
        object rbImpArchivo: TcxRadioButton
          Left = 13
          Top = -1
          Width = 101
          Height = 17
          Caption = 'Importar Archivo'
          TabOrder = 0
          OnClick = rbImpArchivoClick
          Transparent = True
        end
      end
      object GroupBox2: TcxGroupBox
        Left = 0
        Top = 0
        Align = alTop
        Caption = '                                       '
        TabOrder = 0
        Height = 170
        Width = 433
        object lblFechaReg: TLabel
          Left = 23
          Top = 28
          Width = 92
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha del Registro:'
          Transparent = True
        end
        object lblCantidad: TLabel
          Left = 70
          Top = 100
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cantidad:'
          Transparent = True
        end
        object lblTipoComida: TLabel
          Left = 38
          Top = 76
          Width = 77
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Comida:'
          Transparent = True
        end
        object lblHora: TLabel
          Left = 30
          Top = 51
          Width = 85
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora del Registro:'
          Transparent = True
        end
        object Label1: TLabel
          Left = 71
          Top = 123
          Width = 44
          Height = 13
          Alignment = taRightJustify
          Caption = 'Estaci'#243'n:'
          Transparent = True
        end
        object zFecReg: TZetaFecha
          Left = 119
          Top = 23
          Width = 110
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '02/Jan/04'
          Valor = 37988.000000000000000000
        end
        object zHoraReg: TZetaHora
          Left = 119
          Top = 47
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 2
          Text = ''
          Tope = 24
        end
        object cbTipoComida: TComboBox
          Left = 119
          Top = 72
          Width = 80
          Height = 21
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
          Items.Strings = (
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
        object znCantidad: TZetaNumero
          Left = 119
          Top = 96
          Width = 80
          Height = 21
          Mascara = mnDias
          TabOrder = 4
          Text = '0'
        end
        object rbGeneraChecada: TcxRadioButton
          Left = 12
          Top = -1
          Width = 113
          Height = 17
          Caption = 'Generar Checadas'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = rbGeneraChecadaClick
          Transparent = True
        end
        object cbEstacion: TComboBox
          Left = 119
          Top = 119
          Width = 80
          Height = 21
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 5
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 433
      ExplicitHeight = 287
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 433
        ExplicitHeight = 192
        Height = 192
        Width = 433
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 433
        Width = 433
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 365
          ExplicitHeight = 74
          Width = 365
          AnchorY = 57
        end
        inherited cxImage1: TcxImage
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitHeight = 74
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      ExplicitWidth = 433
      ExplicitHeight = 287
      inherited sCondicionLBl: TLabel
        Left = 14
        Top = 137
        ExplicitLeft = 14
        ExplicitTop = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        Top = 160
        ExplicitLeft = 39
        ExplicitTop = 160
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Top = 254
        Visible = True
        ExplicitLeft = 142
        ExplicitTop = 254
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        Top = 134
        ExplicitLeft = 66
        ExplicitTop = 134
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Top = 160
        Style.IsFontAssigned = True
        ExplicitLeft = 66
        ExplicitTop = 160
      end
      inherited GBRango: TGroupBox
        Left = 66
        Top = 4
        ExplicitLeft = 66
        ExplicitTop = 4
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
  object OpenDialog: TOpenDialog
    Filter = 'Datos ( *.dat )|*.dat|Textos ( *.txt )|*.txt|Todos ( *.* )|*.*'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofAllowMultiSelect]
    Left = 532
    Top = 32
  end
end
