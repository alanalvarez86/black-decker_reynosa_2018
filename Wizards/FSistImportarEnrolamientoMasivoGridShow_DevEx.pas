unit FSistImportarEnrolamientoMasivoGridShow_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, ZBaseGridShow_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, cxControls, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons;

type
  TSistImportarEnrolamientoMasivoGridShow_DevEx = class(TBaseGridShow_DevEx)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistImportarEnrolamientoMasivoGridShow_DevEx: TSistImportarEnrolamientoMasivoGridShow_DevEx;

implementation

{$R *.DFM}

end.
