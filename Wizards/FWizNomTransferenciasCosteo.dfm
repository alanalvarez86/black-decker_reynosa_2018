inherited WizNomTransferenciasCosteo: TWizNomTransferenciasCosteo
  Left = 458
  Top = 191
  Caption = 'Transferencias de Empleados por'
  ClientHeight = 354
  ClientWidth = 512
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl: TPageControl [0]
    Width = 512
    Height = 318
    ActivePage = Parametros
    inherited Parametros: TTabSheet
      object GroupBox2: TGroupBox
        Left = 9
        Top = 52
        Width = 486
        Height = 203
        Caption = ' Datos de la Transferencia '
        TabOrder = 0
        object Label5: TLabel
          Left = 49
          Top = 29
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha Inicial:'
        end
        object HorasLbl: TLabel
          Left = 81
          Top = 53
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'Horas:'
          FocusControl = TR_HORAS
        end
        object lbDestino: TLabel
          Left = 39
          Top = 78
          Width = 73
          Height = 13
          Alignment = taRightJustify
          Caption = 'CC a Transferir:'
          FocusControl = CC_DESTINO
        end
        object MotivoLbl: TLabel
          Left = 77
          Top = 103
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = 'Motivo:'
          FocusControl = TR_MOTIVO
        end
        object Label6: TLabel
          Left = 38
          Top = 128
          Width = 74
          Height = 13
          Alignment = taRightJustify
          Caption = 'Observaciones:'
        end
        object TipoAutLbl: TLabel
          Left = 88
          Top = 153
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
        end
        object AU_FECHA: TZetaFecha
          Left = 114
          Top = 24
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '04/Mar/11'
          Valor = 40606.000000000000000000
        end
        object TR_HORAS: TZetaNumero
          Left = 114
          Top = 49
          Width = 41
          Height = 21
          Mascara = mnHoras
          TabOrder = 1
          Text = '0.00'
        end
        object CC_DESTINO: TZetaKeyLookup
          Left = 114
          Top = 74
          Width = 300
          Height = 21
          LookupDataset = dmTablas.cdsNivel1
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
        end
        object TR_MOTIVO: TZetaKeyLookup
          Left = 114
          Top = 99
          Width = 300
          Height = 21
          LookupDataset = dmTablas.cdsMotivoTransfer
          TabOrder = 3
          TabStop = True
          WidthLlave = 60
        end
        object TR_TEXTO: TZetaEdit
          Left = 114
          Top = 124
          Width = 300
          Height = 21
          TabOrder = 4
        end
        object TR_TIPO: TZetaKeyCombo
          Left = 114
          Top = 149
          Width = 145
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 5
          ListaFija = lfTipoHoraTransferenciaCosteo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object cbDuplicados: TCheckBox
          Left = 114
          Top = 176
          Width = 359
          Height = 17
          Caption = 
            'Validar que no exista transferencia anterior a mismo(a) Centro d' +
            'e Costo'
          Checked = True
          State = cbChecked
          TabOrder = 6
        end
      end
    end
    inherited FiltrosCondiciones: TTabSheet
      inherited FiltrosGB: TGroupBox
        Left = 7
        Top = 1
        Width = 489
        Height = 306
        inherited sFiltroLBL: TLabel
          Left = 105
        end
        inherited sCondicionLBl: TLabel
          Left = 80
        end
        object lbOriginal: TLabel [2]
          Left = 57
          Top = 254
          Width = 73
          Height = 13
          Alignment = taRightJustify
          Caption = 'CC a Transferir:'
          FocusControl = CC_Original
        end
        inherited sFiltro: TMemo
          Left = 133
        end
        inherited GBRango: TGroupBox
          Left = 133
        end
        inherited ECondicion: TZetaKeyLookup
          Left = 133
        end
        inherited BAgregaCampo: TBitBtn
          Left = 448
        end
        inherited Seleccionar: TBitBtn
          Left = 160
          Top = 276
          TabOrder = 5
          Visible = True
        end
        object CC_Original: TZetaKeyLookup
          Left = 133
          Top = 250
          Width = 338
          Height = 21
          LookupDataset = dmTablas.cdsNivel1
          TabOrder = 4
          TabStop = True
          WidthLlave = 82
        end
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 504
        Lines.Strings = (
          ''
          'Se registrar'#225'n las transferencias'
          'para los empleados seleccionados.'
          ''
          'Presione el bot'#243'n '#39'Ejecutar'#39' para iniciar el proceso.')
      end
      inherited ProgressPanel: TPanel
        Top = 241
        Width = 504
        inherited ProgressBar: TProgressBar
          Left = 73
        end
      end
    end
  end
  inherited Wizard: TZetaWizard [1]
    Top = 318
    Width = 512
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Enabled = False
    end
    inherited Salir: TZetaWizardButton
      Left = 426
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 343
    end
  end
end
