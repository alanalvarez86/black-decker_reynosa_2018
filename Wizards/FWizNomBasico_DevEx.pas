unit FWizNomBasico_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ComCtrls, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons,
     ZetaDBTextBox,
     ZetaDBGrid,
     ZetacxWizard,
     ZcxBaseWizardFiltro,
     ZetaEdit, ZetaWizard, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit, cxMemo,
  dxCustomWizardControl, dxWizardControl, cxGroupBox, cxLabel,
  dxGDIPlusClasses, cxImage, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters;

type
  TWizNomBasico_DevEx = class(TBaseCXWizardFiltro)

    procedure FormCreate(Sender: TObject);

  private
    { Private declarations }
  protected
    { Private declarations }
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomBasico_DevEx: TWizNomBasico_DevEx;

implementation

uses DCliente,
     ZetaCommonTools,
     ZetaCommonLists;

{$R *.DFM}

procedure TWizNomBasico_DevEx.CargaParametros;
begin
     with Descripciones do
     begin
          with dmCliente do
               AddString( 'Per�odo', ShowNomina( YearDefault, Ord( PeriodoTipo ), PeriodoNumero ) );
     end;

     inherited;
end;

procedure TWizNomBasico_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {***Se define la seceunta de las paginas***}
     FiltrosCondiciones.PageIndex := WizardControl.PageCount-1;
     Ejecucion.PageIndex := WizardControl.PageCount-2;
     Parametros.PageIndex := WizardControl.PageCount-3;
end;
end.
