unit FWizNomSimLiquidacionGlobal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, ZetaDBTextBox, Mask, ZetaFecha,
  FWizEmpBaseFiltro, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit,
  cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage,
  dxCustomWizardControl, dxWizardControl, cxCheckBox;

type
  TLiquidacionGlobal_DevEx = class(TWizEmpBaseFiltro)
    GroupBox1: TcxGroupBox;
    iNumeroNomina: TZetaTextBox;
    iMesNomina: TZetaTextBox;
    sDescripcion: TZetaTextBox;
    FechaInicial: TZetaTextBox;
    FechaFinal: TZetaTextBox;
    sStatusNomina: TZetaTextBox;
    Label4: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    PeriodoNumeroLBL: TLabel;
    Label5: TLabel;
    GRTipo: TcxRadioGroup;
    Label1: TLabel;
    Observaciones: TZetaEdit;
    GrSimAprobadas: TcxRadioGroup;
    GrSimAplicadas: TcxRadioGroup;
    Panel1: TPanel;
    Label6: TLabel;
    FechaLiquidacion: TZetaFecha;
    IncluirBajas: TcxCheckBox;
    FechaBaja: TZetaFecha;
    SimulacionesPrevias: TdxWizardControlPage;

    procedure FormShow(Sender: TObject);
    procedure IncluirBajasClick(Sender: TObject);
    procedure GRTipoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

    { Private declarations }
  public
    { Public declarations }
  protected
    procedure CargaListaVerificacion;override;
    procedure CargaParametros;override;
    function EjecutarWizard: Boolean;override;
    function Verificar: Boolean;override;
  end;

var
  LiquidacionGlobal_DevEx: TLiquidacionGlobal_DevEx;

implementation

uses DProcesos,
     DCliente,
     ZetaCommonLists,
     FEmpleadoGridSelect_DevEx,
     ZetaCommonClasses,
     ZetaCommonTools,
     DGlobal,
     ZBaseSelectGrid_DevEx,
     ZGlobalTress, ZcxWizardBasico, ZCXBaseWizardFiltro,
  ZcxBaseWizard;

{$R *.dfm}

procedure TLiquidacionGlobal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= HHHHH_Liquidacion_global_Finiquitos;
     GrTipo.ItemIndex := 0;
     Observaciones.MaxLength := ZetaCommonClasses.K_ANCHO_DESCRIPCION;

     Observaciones.Text := GRTipo.Properties.Items.Items[GRTipo.ItemIndex].Caption;
     //Observaciones.Text := GRTipo.Properties.Items.Strings[GRTipo.ItemIndex];
end;

procedure TLiquidacionGlobal_DevEx.CargaListaVerificacion;
begin
     dmProcesos.LiquidacionGlobalLista( ParameterList );
end;

function TLiquidacionGlobal_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

function TLiquidacionGlobal_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.LiquidacionGlobalSimulacion(ParameterList, Verificacion);
end;

procedure TLiquidacionGlobal_DevEx.CargaParametros;
var
   Fecha:TDate;
begin
     Fecha := NullDateTime;
     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'TipoLiquidacion', GRTipo.ItemIndex + 1 );
          AddString( 'Observaciones', Observaciones.Text );
          AddString( 'Simulacion', K_GLOBAL_SI );
          AddString('EncimarAprobada',zBoolToStr(GrSimAprobadas.ItemIndex = 1));
          AddString('EncimarAplicada',zBoolToStr(GrSimAplicadas.ItemIndex = 1));
          AddDate('FechaSimulacion',FechaLiquidacion.Valor );
          if IncluirBajas.Checked then
             Fecha := FechaBaja.Valor;
          AddDate ('FechaBaja',Fecha);
     end;

     with Descripciones do
     begin
          //AddString( 'Tipo Liquidaci�n', GRTipo.Items.Strings[GRTipo.ItemIndex] );
          AddString( 'Tipo liquidaci�n', GRTipo.Properties.Items.Items[GRTipo.ItemIndex].Caption);   //DevEx
          AddString( 'Observaciones', Observaciones.Text );
          AddString( 'Encimar aprobadas', GrSimAprobadas.Properties.Items.Items[GrSimAprobadas.ItemIndex].Value); //DevEx
          AddString( 'Encimar aplicadas', GrSimAprobadas.Properties.Items.Items[GrSimAprobadas.ItemIndex].Value ); //DevEx

          AddDate('Fecha de liquidaci�n',FechaLiquidacion.Valor );
          if IncluirBajas.Checked then
          begin
             Fecha := FechaBaja.Valor;
             AddDate ('Inclu�r bajas desde',Fecha);
          end;
     end;
end;

procedure TLiquidacionGlobal_DevEx.FormShow(Sender: TObject);


const
     {K_MENSAJE = 'Advertencia'+CR_LF+
                 'Se va registrar la Liquidaci�n Global para los empleados indicados en el Periodo: %d para el A�o: %d'+CR_LF+
                 'Presione ''Ejecutar'' para iniciar el proceso';
                 }
     //DevEx
     K_MENSAJE = 'Se efectuar� un balance de los conceptos pendientes de pago del empleado para saldarlos.';   //DevEx

begin
     inherited;
     FechaLiquidacion.Valor := dmCliente.FechaDefault;
     FechaBaja.Valor := dmCliente.FechaDefault;
     GrSimAprobadas.Visible := Global.GetGlobalBooleano ( K_GLOBAL_SIM_FINIQ_APROBACION );
     GrSimAprobadas.Caption := 'Si ya existen simulaciones aprobadas en el '+ IntToStr(dmCliente.YearDefault);
     GrSimAplicadas.Caption := 'Si ya existen simulaciones aplicadas en el '+ IntToStr(dmCliente.YearDefault);

     //Advertencia.Lines.Clear;
     Advertencia.Caption := '';;     //DevEx
     //Advertencia.Lines.Add(Format(K_MENSAJE,[Global.GetGlobalInteger(K_GLOBAL_SIMULACION_FINIQUITOS ),dmCliente.YearDefault] ));
     Advertencia.Caption := (Format(K_MENSAJE,[Global.GetGlobalInteger(K_GLOBAL_SIMULACION_FINIQUITOS ),dmCliente.YearDefault] ));   //DevEx

     {if not GrSimAprobadas.Visible then
     begin
          grExisteSimu.Height := GrSimAprobadas.Height  + 20 ;
          grExisteSimu.Top := 50;
     end;}

     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          //SetPeriodoNumero(Global.GetGlobalInteger(K_GLOBAL_SIMULACION_FINIQUITOS ) );
          //RefrescaPeriodo;
          with GetDatosPeriodoActivo do
          begin
               iNumeroNomina.Caption := IntToStr( Numero );
               iMesNomina.Caption := ObtieneElemento( lfMeses, ( Mes - 1  ) );
               sStatusNomina.Caption := ZetaCommonTools.GetDescripcionStatusPeriodo( Status, StatusTimbrado );
               FechaInicial.Caption := FormatDateTime( {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}LongDateFormat, Fin );
          end;
          //Observaciones.Text := GRTipo.Items.Strings[GRTipo.ItemIndex];
          Observaciones.Text := GRTipo.Properties.Items.Items[GRTipo.ItemIndex].Caption;  //DevEx
     end;

     //DevEx
     SimulacionesPrevias.PageIndex := 1;
     FiltrosCondiciones.PageIndex := 2;
     Ejecucion.PageIndex := 3;
end;

procedure TLiquidacionGlobal_DevEx.IncluirBajasClick(Sender: TObject);
begin
     inherited;
      FechaBaja.Enabled := IncluirBajas.Checked;
end;

procedure TLiquidacionGlobal_DevEx.GRTipoClick(Sender: TObject);
begin
     inherited;
     //Observaciones.Text := GRTipo.Items.Strings[GRTipo.ItemIndex];
     Observaciones.Text := GRTipo.Properties.Items.Items[GRTipo.ItemIndex].Caption;    //DevEx
end;


end.
