inherited EmpImportarOrganigramaGridShow_DevEx: TEmpImportarOrganigramaGridShow_DevEx
  Left = 211
  Top = 208
  Width = 1079
  Height = 282
  Caption = 'Lista de Errores Detectados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 208
    Width = 1063
    inherited OK_DevEx: TcxButton
      Left = 899
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 978
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 1063
    Height = 208
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object RENGLON: TcxGridDBColumn
        Caption = 'Rengl'#243'n'
        DataBinding.FieldName = 'RENGLON'
        Width = 64
      end
      object CB_JEFE: TcxGridDBColumn
        Caption = 'Empleado Jefe '
        DataBinding.FieldName = 'CB_JEFE'
        Width = 64
      end
      object PLAZA_JEF: TcxGridDBColumn
        Caption = 'Plaza Jefe'
        DataBinding.FieldName = 'PLAZA_JEF'
        Width = 64
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 64
      end
      object ZetaDBGridDBTableViewAU_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'AU_FECHA'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_PUESTO: TcxGridDBColumn
        Caption = 'Puesto'
        DataBinding.FieldName = 'CB_PUESTO'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_CLASIFI: TcxGridDBColumn
        Caption = 'Clasificaci'#243'n'
        DataBinding.FieldName = 'CB_CLASIFI'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_TURNO: TcxGridDBColumn
        Caption = 'Turno'
        DataBinding.FieldName = 'CB_TURNO'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_NIVEL1: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL1'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_NIVEL2: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL2'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_NIVEL3: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL3'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_NIVEL4: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL4'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_NIVEL5: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL5'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_NIVEL6: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL6'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_NIVEL7: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL7'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_NIVEL8: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL8'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_NIVEL9: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL9'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_NIVEL10: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL10'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_NIVEL11: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL11'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCB_NIVEL12: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL12'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewOPERACION: TcxGridDBColumn
        Caption = 'Operaci'#243'n'
        DataBinding.FieldName = 'OPERACION'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewFECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'FECHA'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CODIGO'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewADICIONALES: TcxGridDBColumn
        Caption = 'Adicionales'
        DataBinding.FieldName = 'ADICIONALES'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewCOMENTARIO: TcxGridDBColumn
        Caption = 'Comentarios'
        DataBinding.FieldName = 'COMENTARIO'
        Visible = False
        Width = 20
      end
      object PL_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre Plaza'
        DataBinding.FieldName = 'PL_NOMBRE'
        Width = 64
      end
      object ZetaDBGridDBTableViewPL_CODIGO: TcxGridDBColumn
        Caption = 'Folio'
        DataBinding.FieldName = 'PL_CODIGO'
        Visible = False
        Width = 20
      end
      object PL_TIREP: TcxGridDBColumn
        Caption = 'Relaci'#243'n'
        DataBinding.FieldName = 'PL_TIREP'
      end
      object PL_FEC_INI: TcxGridDBColumn
        Caption = 'Inicia'
        DataBinding.FieldName = 'PL_FEC_INI'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object PL_FEC_FIN: TcxGridDBColumn
        Caption = 'Termina'
        DataBinding.FieldName = 'PL_FEC_FIN'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object NUM_ERROR: TcxGridDBColumn
        Caption = 'Errores'
        DataBinding.FieldName = 'NUM_ERROR'
        Width = 64
      end
      object DESC_ERROR: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Errores'
        DataBinding.FieldName = 'DESC_ERROR'
        Width = 64
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 80
    Top = 48
  end
end
