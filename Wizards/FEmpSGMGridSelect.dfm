inherited EmpSGMGridSelect: TEmpSGMGridSelect
  Left = 92
  Top = 269
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Aplicar Eventos'
  ClientHeight = 255
  ClientWidth = 1296
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 219
    Width = 1296
    inherited OK: TBitBtn
      Left = 1137
      Anchors = [akRight, akBottom]
    end
    inherited Cancelar: TBitBtn
      Left = 1215
      Anchors = [akRight, akBottom]
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 1296
    Height = 184
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        Title.Caption = 'Nombre'
        Width = 213
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_TIPO_DESC'
        Title.Caption = 'Tipo'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DEPENDIENT'
        Title.Caption = 'Pariente'
        Width = 207
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PA_DESC_REL'
        Title.Caption = 'Parentesco'
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_CERTIFI'
        Title.Caption = 'Certificado'
        Width = 73
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RN_FEC_INI'
        Title.Caption = 'Inicio'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RN_FEC_FIN'
        Title.Caption = 'Final'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_CFIJO'
        Title.Caption = 'Costo Fijo'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_CPOLIZA'
        Title.Caption = 'Costo P'#243'liza'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_CTOTAL'
        Title.Caption = 'Total'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_CTITULA'
        Title.Caption = 'Costo Empleado'
        Width = 83
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_CEMPRES'
        Title.Caption = 'Costo Empresa'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_OBSERVA'
        Title.Caption = 'Observaciones'
        Width = 123
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PA_APE_MAT'
        Title.Caption = 'Apellido Materno'
        Width = -1
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'PA_NOMBRES'
        Title.Caption = 'Nombres'
        Width = -1
        Visible = False
      end>
  end
  inherited PanelSuperior: TPanel
    Width = 1296
  end
end
