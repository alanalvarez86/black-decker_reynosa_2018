inherited WizEmpCursoTomado_DevEx: TWizEmpCursoTomado_DevEx
  Left = 813
  Top = 210
  Caption = 'Curso Tomado Global'
  ClientHeight = 533
  ClientWidth = 440
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 440
    Height = 533
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        ' Proceso que permite registrar la asistencia de un grupo de empl' +
        'eados a un curso.'
      Header.Title = 'Curso tomado global'
      object lblEstablec: TLabel
        Left = 11
        Top = 316
        Width = 77
        Height = 13
        Caption = 'Es&tablecimiento:'
        FocusControl = KC_EST
        Transparent = True
      end
      object SE_COSTO3Lbl: TLabel
        Left = 49
        Top = 293
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Costo &3:'
        FocusControl = SE_COSTO3
        Transparent = True
      end
      object SE_COSTO2Lbl: TLabel
        Left = 49
        Top = 269
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Costo &2:'
        FocusControl = SE_COSTO2
        Transparent = True
      end
      object SE_COSTO1Lbl: TLabel
        Left = 49
        Top = 247
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Costo &1:'
        FocusControl = SE_COSTO1
        Transparent = True
      end
      object lblComenta: TLabel
        Left = 27
        Top = 362
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = 'C&omentarios:'
        FocusControl = SE_COMENTA
        Transparent = True
      end
      object lblLugar: TLabel
        Left = 58
        Top = 339
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = '&Lugar:'
        FocusControl = SE_LUGAR
        Transparent = True
      end
      object lblHorFin: TLabel
        Left = 30
        Top = 129
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Hora de Fi&n:'
        Transparent = True
      end
      object lblHorIni: TLabel
        Left = 19
        Top = 106
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = '&Hora de Inicio:'
        Transparent = True
      end
      object lblCupo: TLabel
        Left = 60
        Top = 201
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cu&po:'
        FocusControl = SE_CUPO
        Transparent = True
      end
      object lblRevision: TLabel
        Left = 44
        Top = 32
        Width = 44
        Height = 13
        Caption = '&Revisi'#243'n:'
        FocusControl = KC_REVISIO
        Transparent = True
      end
      object KC_FEC_FINLbl: TLabel
        Left = 63
        Top = 82
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = '&Final:'
        FocusControl = dFinal
        Transparent = True
      end
      object Label3: TLabel
        Left = 47
        Top = 224
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = '&Maestro:'
        FocusControl = sMaestro
        Transparent = True
      end
      object Label5: TLabel
        Left = 32
        Top = 178
        Width = 56
        Height = 13
        Alignment = taRightJustify
        Caption = '&Evaluaci'#243'n:'
        FocusControl = rEvaluacion
        Transparent = True
      end
      object Label4: TLabel
        Left = 42
        Top = 153
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = '&Duraci'#243'n:'
        FocusControl = rHoras
        Transparent = True
      end
      object Label2: TLabel
        Left = 60
        Top = 57
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = '&Inicio:'
        FocusControl = dInicio
        Transparent = True
      end
      object Label1: TLabel
        Left = 58
        Top = 7
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'C&urso:'
        FocusControl = sCurso
        Transparent = True
      end
      object KC_EST: TZetaKeyLookup_DevEx
        Left = 90
        Top = 312
        Width = 313
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 13
        TabStop = True
        WidthLlave = 75
      end
      object SE_COSTO3: TZetaNumero
        Left = 90
        Top = 289
        Width = 93
        Height = 21
        Mascara = mnPesos
        TabOrder = 12
        Text = '0.00'
      end
      object SE_COSTO2: TZetaNumero
        Left = 90
        Top = 265
        Width = 93
        Height = 21
        Mascara = mnPesos
        TabOrder = 11
        Text = '0.00'
      end
      object SE_COSTO1: TZetaNumero
        Left = 90
        Top = 243
        Width = 93
        Height = 21
        Mascara = mnPesos
        TabOrder = 10
        Text = '0.00'
      end
      object SE_LUGAR: TEdit
        Left = 90
        Top = 335
        Width = 313
        Height = 21
        MaxLength = 50
        TabOrder = 14
      end
      object SE_COMENTA: TEdit
        Left = 90
        Top = 358
        Width = 313
        Height = 21
        MaxLength = 50
        TabOrder = 15
      end
      object SE_HOR_FIN: TZetaHora
        Left = 90
        Top = 125
        Width = 39
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 5
        Tope = 24
      end
      object SE_HOR_INI: TZetaHora
        Left = 90
        Top = 102
        Width = 40
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 4
        Tope = 24
      end
      object SE_CUPO: TZetaNumero
        Left = 90
        Top = 197
        Width = 75
        Height = 21
        Mascara = mnDias
        TabOrder = 8
        Text = '0'
      end
      object KC_REVISIO: TEdit
        Left = 90
        Top = 28
        Width = 75
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 1
      end
      object dFinal: TZetaFecha
        Left = 90
        Top = 77
        Width = 104
        Height = 22
        Cursor = crArrow
        TabOrder = 3
        Text = '26/ene/00'
        Valor = 36551.000000000000000000
      end
      object sMaestro: TZetaKeyLookup_DevEx
        Left = 90
        Top = 220
        Width = 315
        Height = 21
        LookupDataset = dmCatalogos.cdsMaestros
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        WidthLlave = 75
      end
      object sCurso: TZetaKeyLookup_DevEx
        Left = 90
        Top = 3
        Width = 315
        Height = 21
        LookupDataset = dmCatalogos.cdsCursos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 75
        OnValidKey = sCursoValidKey
      end
      object rHoras: TZetaNumero
        Left = 90
        Top = 149
        Width = 75
        Height = 21
        Mascara = mnHoras
        TabOrder = 6
        Text = '0.00'
      end
      object dInicio: TZetaFecha
        Left = 90
        Top = 52
        Width = 104
        Height = 22
        Cursor = crArrow
        TabOrder = 2
        Text = '14/ene/98'
        Valor = 35809.000000000000000000
      end
      object rEvaluacion: TZetaNumero
        Left = 90
        Top = 174
        Width = 75
        Height = 21
        Mascara = mnHoras
        TabOrder = 7
        Text = '0.00'
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 298
        Width = 418
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 418
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 350
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 14
        Top = 185
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        Top = 208
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Top = 302
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        Top = 182
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Top = 213
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
        Top = 52
      end
      inherited bAjusteISR: TcxButton
        Left = 380
        Top = 214
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 402
  end
end
