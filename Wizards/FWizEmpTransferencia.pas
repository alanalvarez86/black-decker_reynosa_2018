unit FWizEmpTransferencia;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ComCtrls, Buttons, Grids, DBGrids, Db, ExtCtrls, Mask,
     ZBaseWizard,
     ZetaKeyLookup,
     ZetaCommonLists,
     ZetaWizard,
     ZetaNumero,
     ZetaCommonClasses;

type
  TWizEmpTransferencia = class(TBaseWizard)
    GroupBox1: TGroupBox;
    Label2: TLabel;
    EmpleadoSource: TZetaKeyLookup;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    EmpleadoTarget: TZetaNumero;
    EmpresaTarget: TZetaKeyLookup;
    Label3: TLabel;
    GroupBox3: TGroupBox;
    lblTipoKardex: TLabel;
    TipoKardex: TZetaKeyLookup;
    chkTipoKardex: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EmpleadoSourceValidLookup(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure chkTipoKardexClick(Sender: TObject);{OP: 27/05/08}
    procedure FormClose(Sender: TObject; var Action: TCloseAction);{OP: 27/05/08}
    procedure EmpresaTargetExit(Sender: TObject);{OP: 26/05/08}
    procedure EmpresaTargetValidKey(Sender: TObject);
    procedure FormDestroy(Sender: TObject);{OP: 27/05/08}
  private
    { Private declarations }
    FEmpresaDestino: Variant;
    procedure ActivaTipoKardex( lActivo: Boolean ); {OP: 26/05/08}
    function ValidaVacioTipoKardex: Boolean; {OP: 26/05/08}
    procedure EmpresaValidaTipoKardex; {OP: 26/05/08}
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpTransferencia: TWizEmpTransferencia;

implementation

uses DCliente,
     DSistema,
     DProcesos,
     ZetaDialogo,
     ZetaCommonTools, dTablas;

{$R *.DFM}

{ TWizEmpTransferencia }

procedure TWizEmpTransferencia.FormCreate(Sender: TObject);
begin
     inherited;
     dmCliente.SetTipoLookUpEmpleado( eLookEmpComidas );{OP: 28/05/08}
     EmpleadoSource.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     EmpresaTarget.LookupDataset := dmSistema.cdsEmpresasLookUp;
     ParametrosControl := EmpleadoSource;
     with dmCliente do
     begin
          EmpleadoSource.Valor := Empleado;
          {
          EmpresaTarget.Filtro := 'CM_CODIGO <> ''' + Compania + '''';
          EmpresaTarget.Filtro := Format( 'CM_CODIGO <> ''%s'' and CM_ALIAS <> ''%s''', [ Compania, cdsCompany.FieldByName( 'CM_ALIAS' ).AsString ] );
          }
          EmpresaTarget.Filtro := Format( '( CM_CODIGO <> ''%s'' ) and ( CM_DATOS <> ''%s'' ) and ( CM_CTRL_RL <> ''%s'' ) ', [ Compania, cdsCompany.FieldByName( 'CM_DATOS' ).AsString, ObtieneElemento( lfTipoCompany, ord(tc3Prueba) )  ] );
     end;
     HelpContext:= H11610_Transferencia_de_empleado;
end;

procedure TWizEmpTransferencia.FormShow(Sender: TObject);
begin
     inherited;
     dmSistema.cdsEmpresasLookup.Conectar;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     dmTablas.cdsMovKardex.Conectar;{OP: 26/05/08}
end;

procedure TWizEmpTransferencia.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddInteger( 'EmpleadoSource', EmpleadoSource.Valor );
          AddInteger( 'EmpleadoTarget', EmpleadoTarget.ValorEntero );
          AddString( 'EmpresaCodigo', dmCliente.GetDatosEmpresaActiva.Codigo );{OP: 28/05/08}
          AddString( 'EmpresaDescrip', dmCliente.GetDatosEmpresaActiva.Nombre );{OP: 28/05/08}
          AddString( 'KardexTipo', TipoKardex.Llave );{OP: 27/05/08}
          AddBoolean( 'EmpleadoActivo', zStrToBool( dmCliente.cdsEmpleadoLookUp.FieldByName( 'CB_ACTIVO' ).AsString ) );{OP: 16/06/08}
          AddDate( 'EmpleadoFecBaj', dmCliente.cdsEmpleadoLookUp.FieldByName( 'CB_FEC_BAJ' ).AsDateTime );{OP: 27/05/08}
          with dmTablas.cdsMovKardex do
          begin
               AddString( 'KardexDescrip', FieldByName( 'TB_ELEMENT' ).AsString );
               AddInteger( 'KardexNivel', FieldByName( 'TB_NIVEL' ).AsInteger );
          end;
          {$ifndef DOS_CAPAS}
          AddInteger( 'NumeroBiometrico', dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger );
          {$endif}
     end;
     with dmSistema do
     begin
          if cdsEmpresasLookup.locate( 'CM_CODIGO', EmpresaTarget.Llave, [] ) then
             FEmpresaDestino:= dmCliente.BuildEmpresa(cdsEmpresasLookup);
          // Los Datos de FEmpresaDestino se envian en la llamada a dmProcesos
     end;
end;

procedure TWizEmpTransferencia.EmpleadoSourceValidLookup(Sender: TObject);
begin
     EmpleadoTarget.Valor:= EmpleadoSource.Valor;
end;

procedure TWizEmpTransferencia.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if CanMove and Wizard.Adelante then
     begin
          if PageControl.ActivePage = Parametros then
          begin
               if ( EmpleadoSource.Valor = 0 ) then
                  CanMove := Error( 'No Se Escogi� Un Empleado A Transferir', EmpleadoSource )
               else
                   if ZetaCommonTools.StrVacio( EmpresaTarget.Llave ) then
                      CanMove := Error( 'No se ha Especificado Empresa A la Cual Transferir Empleado', EmpresaTarget )
                   else
                       if ZetaCommonTools.StrVacio( EmpleadoTarget.Text ) then
                          CanMove := Error( 'No Se Especific� El Nuevo N�mero Del Empleado', EmpleadoTarget )
                       else
                           if ValidaVacioTipoKardex then
                              CanMove := Error( 'No se ha especificado el tipo de Kardex', TipoKardex ); {OP: 26/05/08}
          end;
     end;
end;

procedure TWizEmpTransferencia.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if Wizard.EsPaginaActual( Ejecucion ) then
     begin
          with Advertencia.Lines do
          begin
               try
                  BeginUpdate;
                  Clear;
                  Add( 'El Empleado ' + EmpleadoSource.Llave + ' Ser� Transferido' );
                  Add( 'De La Compa��a ' + dmCliente.GetDatosEmpresaActiva.Nombre );
                  Add( 'A La Compa��a ' + EmpresaTarget.Descripcion );
                  Add( 'Con El Nuevo N�mero De Empleado ' + EmpleadoTarget.Text );
                  Add( '' );
                  Add( 'Todos Sus Datos Ser�n Borrados De La Empresa' );
                  Add( 'Activa Excepto Los Correspondientes A N�minas' );
                  Add( 'y Liquidaciones De IMSS' );
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

function TWizEmpTransferencia.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.Transferencia( ParameterList, FEmpresaDestino );
end;

{OP: 26/05/08}
procedure TWizEmpTransferencia.ActivaTipoKardex( lActivo: Boolean );
begin
     lblTipoKardex.Enabled := lActivo;
     TipoKardex.Enabled := lActivo;
     if Not lActivo then
        TipoKardex.Llave := VACIO;
end;

{OP: 26/05/08}
function TWizEmpTransferencia.ValidaVacioTipoKardex: Boolean;
begin
     Result := ( chkTipoKardex.Checked ) and ( StrVacio( TipoKardex.Llave ) );
end;

{OP: 26/05/08}
procedure TWizEmpTransferencia.chkTipoKardexClick(Sender: TObject);
begin
     inherited;
     ActivaTipoKardex( chkTipoKardex.Checked );
end;

{OP: 27/05/08}
procedure TWizEmpTransferencia.EmpresaValidaTipoKardex;
begin
     dmTablas.cdsMovKardexCambiaEmpresa( EmpresaTarget.Llave );
end;

{OP: 26/05/08}
procedure TWizEmpTransferencia.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     dmTablas.cdsMovKardex.SetDataChange;
end;

{OP: 26/05/08}
procedure TWizEmpTransferencia.EmpresaTargetExit(Sender: TObject);
var
   lEmpresaValida : Boolean;
begin
     inherited;
     lEmpresaValida := StrLleno( EmpresaTarget.Llave );
     if ( Not lEmpresaValida ) then
     begin
          chkTipoKardex.Enabled := lEmpresaValida;
          chkTipoKardex.Checked := lEmpresaValida;
          ActivaTipoKardex( lEmpresaValida );
     end;
end;

{OP: 26/05/08}
procedure TWizEmpTransferencia.EmpresaTargetValidKey(Sender: TObject);
var
   lEmpresaValida : Boolean;
begin
     inherited;
     lEmpresaValida := StrLleno( EmpresaTarget.Llave );
     if lEmpresaValida then
     begin
          EmpresaValidaTipoKardex;
          chkTipoKardex.Enabled := lEmpresaValida;
          TipoKardex.Llave := VACIO;
     end;
end;

{OP: 28/05/08}
procedure TWizEmpTransferencia.FormDestroy(Sender: TObject);
begin
     inherited;
     dmCliente.ResetLookupEmpleado;{OP: 28/05/08}
end;

end.



