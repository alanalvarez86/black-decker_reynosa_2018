unit FWizEmpCorregirFechasCafe_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ZetaEdit, StdCtrls, ExtCtrls, Mask, ZetaFecha,
  FWizEmpBaseFiltro, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons,
  cxRadioGroup, cxTextEdit, cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses,
  cxImage, dxCustomWizardControl, dxWizardControl;

type
  TWizEmpCorregirFechasCafe_DevEx = class(TWizEmpBaseFiltro)
    rbCorregir: TcxRadioGroup;
    gbFechas: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    FechaActual: TZetaFecha;
    FechaNueva: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    //DevEx     //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rbCorregirClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetControl( const lEnabled: boolean );
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpCorregirFechasCafe_DevEx: TWizEmpCorregirFechasCafe_DevEx;

implementation

{$R *.DFM}

uses
    ZetaCommonLists,
    ZetaCommonClasses,
    ZetaTipoEntidad,
    ZConstruyeFormula,
    DProcesos,
    DCliente, ZcxWizardBasico, ZcxBaseWizard;

{ TWizEmpCorregirFechasCafe_DevEx }

procedure TWizEmpCorregirFechasCafe_DevEx.FormCreate(Sender: TObject);

    function getStringList( radioGroup : TcxRadioGroup ): TStrings;
     var
        lista : TStringList;
        i : Integer;
     begin
          lista := TStringList.Create();
          with radioGroup.Properties.Items do
          begin
               for i := 0 to ( Count -1 ) do
               begin
                    lista.Add( items[i].Caption);
               end;
          end;
          Result := lista;
    end;

begin
     inherited;
     ParametrosControl := FechaActual;
     with dmCliente do
     begin
          FechaActual.Valor := FechaAsistencia;
          FechaNueva.Valor := FechaAsistencia;
     end;
     //ZetaCommonLists.LlenaLista( lfTipoCorregirFechas, rbCorregir.Items );
     ZetaCommonLists.LlenaLista( lfTipoCorregirFechas, getStringList( rbCorregir ) );
     HelpContext := H11624_Corregir_fechas_globales_de_Cafeteria;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;

end;

procedure TWizEmpCorregirFechasCafe_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     setControl( rbCorregir.ItemIndex <> Ord( tcfInvitaciones ) );
     Advertencia.Caption := 'Al ejecutar el proceso se corregirán las fechas de consumo indicadas.';
end;

procedure TWizEmpCorregirFechasCafe_DevEx.rbCorregirClick(Sender: TObject);
begin
     inherited;
     SetControl( rbCorregir.ItemIndex <> Ord( tcfInvitaciones ) );
end;

procedure TWizEmpCorregirFechasCafe_DevEx.BAgregaCampoClick(Sender: TObject);
begin
     with sFiltro do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( enComida, Text, SelStart, evBase );
     end;
end;

procedure TWizEmpCorregirFechasCafe_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;
     with Wizard do
     begin
          if ( Adelante and EsPaginaActual( Parametros ) and ( FechaActual.Valor = FechaNueva.Valor ) ) then
          begin
               CanMove := Error( 'ˇ La fecha actual y la nueva deben ser diferentes !', FechaActual )
          end
     end;
end;

procedure TWizEmpCorregirFechasCafe_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddDate( 'FechaActual', FechaActual.Valor );
          AddDate( 'FechaNueva', FechaNueva.Valor );
          AddInteger( 'Corregir', rbCorregir.ItemIndex );
     end;

     with Descripciones do
     begin
          AddDate( 'Fecha actual', FechaActual.Valor );
          AddDate( 'Fecha nueva', FechaNueva.Valor );
          AddString( 'Corregir', rbCorregir.Properties.Items[rbCorregir.ItemIndex].Caption );
     end;
end;

function TWizEmpCorregirFechasCafe_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CorregirFechasGlobalesCafe( ParameterList );
end;

procedure TWizEmpCorregirFechasCafe_DevEx.SetControl( const lEnabled: boolean );
begin
     FiltrosCondiciones.Enabled := lEnabled;
end;


//DevEx     //Agregado para enfocar un control
procedure TWizEmpCorregirFechasCafe_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := FechaActual
     else
         ParametrosControl := nil;
     inherited;
end;

end.
