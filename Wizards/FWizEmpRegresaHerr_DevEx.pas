unit FWizEmpRegresaHerr_DevEx;
                                                               
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaEdit, StdCtrls, ComCtrls, ExtCtrls, Mask, ZetaFecha,
  DBCtrls, FWizEmpBaseFiltro,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit, cxMemo,
  cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage, dxCustomWizardControl,
  dxWizardControl, cxCheckBox;

type
  TWizEmpRegresaHerr_DevEx = class(TWizEmpBaseFiltro)
    GBDevolucion: TcxGroupBox;
    KT_FEC_FIN: TZetaFecha;
    LblFecDev: TLabel;
    KT_MOT_FIN: TZetaKeyLookup_DevEx;
    LblMotivo: TLabel;
    GBFiltros: TcxGroupBox;
    CBTool: TcxCheckBox;
    TO_CODIGO: TZetaKeyLookup_DevEx;
    KT_TALLA: TZetaKeyLookup_DevEx;
    CBTalla: TcxCheckBox;
    CBReferencia: TcxCheckBox;
    KT_REFEREN: TEdit;
    CBFecha: TcxCheckBox;
    FechaIni: TZetaFecha;
    Label2: TLabel;
    FechaFin: TZetaFecha;
    RGTipo: TcxRadioGroup;
    RGRenovacion: TcxRadioGroup;
    RefReemplazo: TEdit;
    LblRenova: TLabel;
    procedure CheckBoxClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure RGRenovacionClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetControls;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

const
     K_DEVOLUCION = 0;
     K_RENOVACION = 1;
var
  WizEmpRegresaHerr_DevEx: TWizEmpRegresaHerr_DevEx;

implementation

uses dCliente, dCatalogos, dTablas, dProcesos, FEmpRegresarHerrGridSelect_DevEx,
     ZetaCommonTools, ZetaCommonClasses, ZetaDialogo, ZBaseSelectGrid_DevEx,
  ZcxBaseWizard;

{$R *.DFM}

{ TWizEmpRegresaHerr_DevEx }

procedure TWizEmpRegresaHerr_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := KT_FEC_FIN;

     with dmCliente do
     begin
          KT_FEC_FIN.Valor := FechaDefault;
          FechaIni.Valor := StrAsFecha( '01/01/1990' );
          FechaFin.Valor := FechaDefault;
     end;
     SetControls;
     HelpContext := H11615_Regresar_Herramienta;

     KT_MOT_FIN.LookupDataset := dmTablas.cdsMotTool;
     TO_CODIGO.LookupDataset := dmCatalogos.cdsTools;
     KT_TALLA.LookupDataset := dmTablas.cdsTallas;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;

end;

procedure TWizEmpRegresaHerr_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmTablas do
     begin
          cdsTallas.Conectar;
          with cdsMotTool do
          begin
               Conectar;
               KT_MOT_FIN.Llave := FieldByName( 'TB_CODIGO' ).AsString;
          end;
     end;
     dmCatalogos.cdsTools.Conectar;
     RGTipo.ItemIndex := 0;
     RGTipoClick( self );

     Advertencia.Caption := 'Se registrará el regreso de herramienta seleccionada.';
end;

procedure TWizEmpRegresaHerr_DevEx.CheckBoxClick(Sender: TObject);
begin
     SetControls;
end;

procedure TWizEmpRegresaHerr_DevEx.RGTipoClick(Sender: TObject);
var
   lEnabled : Boolean;
begin
     lEnabled := ( RGTipo.ItemIndex = K_RENOVACION );
     LblRenova.Enabled := lEnabled;
     RGRenovacion.Enabled := lEnabled;
end;

procedure TWizEmpRegresaHerr_DevEx.RGRenovacionClick(Sender: TObject);
begin
     RefReemplazo.Enabled := RGRenovacion.Enabled and ( RGRenovacion.ItemIndex = K_RENOVACION );
     if RefReemplazo.Enabled then
        RefReemplazo.SetFocus;
end;

procedure TWizEmpRegresaHerr_DevEx.SetControls;
begin
     TO_CODIGO.Enabled := CBTool.Checked;
     KT_REFEREN.Enabled := CBReferencia.Checked;
     KT_TALLA.Enabled := CBTalla.Checked;
     FechaIni.Enabled := CBFecha.Checked;
     FechaFin.Enabled := CBFecha.Checked;
end;

procedure TWizEmpRegresaHerr_DevEx.CargaParametros;
var
   lRenovacion: boolean;
begin
     lRenovacion := ( RGTipo.ItemIndex = K_RENOVACION );

     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'FechaDev', KT_FEC_FIN.Valor );
          AddString( 'Motivo', KT_MOT_FIN.Llave );

          AddBoolean( 'Reemplazar', lRenovacion );
          AddBoolean( 'ReemplazaRef', ( RGRenovacion.ItemIndex = K_RENOVACION ) );
          AddString( 'RefReemplazo', RefReemplazo.Text );

          AddBoolean( 'FiltrarTool', CBTool.Checked );
          AddString( 'Tool', TO_CODIGO.Llave );
          AddBoolean( 'FiltrarRef', CBReferencia.Checked );
          AddString( 'Referencia', KT_REFEREN.Text );
          AddBoolean( 'FiltrarTalla', CBTalla.Checked );
          AddString( 'Talla', KT_TALLA.Llave );

          AddBoolean( 'FiltrarFecha', CBFecha.Checked );
          AddDate( 'FechaIni', FechaIni.Valor );
          AddDate( 'FechaFin', FechaFin.Valor );
     end;

     with Descripciones do
     begin
         // Clear;    //DevEx
          if lRenovacion then
             AddString( 'Tipo', 'Devolución' )
          else
              AddString( 'Tipo', 'Renovación' );
          AddDate( 'Fecha', KT_FEC_FIN.Valor );
          AddString( 'Motivo', GetDescripLlave( KT_MOT_FIN ) );
          if lRenovacion  then
          begin
               if ( RGRenovacion.ItemIndex = K_RENOVACION ) then
                  AddString( 'Cambiar referencia a', RefReemplazo.Text )
               else
                   //AddString( 'Conservar referencia', VACIO );
                   AddString( 'Conservar referencia', RGRenovacion.Properties.Items.Items [ RGRenovacion.ItemIndex].Value );
          end;
          if CBTool.Checked then
             AddString( 'Herramienta', GetDescripLlave( TO_CODIGO ) );
          if CBReferencia.Checked then
             AddString( 'Referencia', KT_REFEREN.Text );
          if CBTalla.Checked then
             AddString( 'Talla', GetDescripLlave( KT_TALLA  ) );
          if CBFecha.Checked then
          begin
               AddDate( 'Inicio', FechaIni.Valor );
               AddDate( 'Fin', FechaFin.Valor );
          end;
     end;

end;

procedure TWizEmpRegresaHerr_DevEx.CargaListaVerificacion;
begin
     dmProcesos.RegresarHerramientaGetLista( ParameterList );
end;

function TWizEmpRegresaHerr_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpRegresarHerrGridSelect_DevEx );
end;

function TWizEmpRegresaHerr_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.RegresarHerramienta( ParameterList, Verificacion );
end;

procedure TWizEmpRegresaHerr_DevEx.WizardBeforeMove(Sender: TObject;var iNewPage: Integer;
          var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;  //DevEx
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if strVacio( KT_MOT_FIN.Llave ) then
               begin
                    zError( Caption, 'Debe Especificarse el Motivo de Devolución', 0 );
                    ActiveControl := KT_MOT_FIN;
                    CanMove := False;
               end
               else if ( CBTool.Checked ) and strVacio( TO_CODIGO.Llave ) then
               begin
                    ZetaDialogo.zError( Caption, 'ˇ Debe Especificarse la Herramienta !', 0 );
                    ActiveControl := TO_CODIGO;
                    CanMove := False;
               end
               else if ( CBReferencia.Checked ) and strVacio( KT_REFEREN.Text ) then
               begin
                    ZetaDialogo.zError( Caption, 'ˇ Debe Especificarse la Referencia !', 0 );
                    ActiveControl := KT_REFEREN;
                    CanMove := False;
               end
               else if ( CBTalla.Checked ) and strVacio( KT_TALLA.Llave ) then
               begin
                    ZetaDialogo.zError( Caption, 'ˇ Debe Especificarse la Talla !', 0 );
                    ActiveControl := KT_TALLA;
                    CanMove := False;
               end
               else if ( CBFecha.Checked ) and ( FechaIni.Valor > FechaFin.Valor ) then
               begin
                    ZetaDialogo.zError( Caption, 'ˇ La Fecha Final debe ser Mayor o Igual que la Inicial !', 0 );
                    ActiveControl := FechaFin;
                    CanMove := False;
               end;
          end;
     end;
end;

procedure TWizEmpRegresaHerr_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := KT_FEC_FIN
     else
         ParametrosControl := nil;
     inherited;
end;

end.



