inherited WizEmpImportarSGM_DevEx: TWizEmpImportarSGM_DevEx
  Left = 405
  Top = 225
  Caption = 'Importar Seguros de Gastos M'#233'dicos'
  ClientHeight = 465
  ClientWidth = 480
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 480
    Height = 465
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC200000EC2011528
      4A80000003A5494441545847C5975D48536118C767ADB00F08428824F4C6BE56
      04690569174D4C8D6A5B107611815144415110147E6CAF67735FE73DDB740B22
      22FA40D12DA2ECEBA2920CC36AE2DCE6C711BCF0B24B832EBAF0623DCF7ACF3C
      9BAFA6C9E6033F8E7B9F87F7FF3FCFFBC1519348245614EE602EE10EE612EE60
      2EF9EFB84BEE6E1405D7716A73551242F2D970F603C4B4D426BA2481CE000984
      0AE22F6A755A5849764312C42E4538136AA54E56969DA0841EE509AB98F1104F
      092B5F7AD43B5EE6598263E596D0C40373B7FC820465FD79E79B3C96D64856C9
      C6114D034C5E63E5C9F00D198BDAA3061A881B5F0662A67AD7B75A2D4BCD0D10
      2624349150630ECA3E7826F360E01E4F3485550403EEE66431445BE45445206E
      4AA8F1C74CF15B3DFBD7B292D920DDE39B32C5159A9F0E6FC51A98FC0A5758C1
      0A10B7096BAFDE3BAC05B1E94C03081D3C5E87356961E91EDBC113472CDDA315
      582311A90076FC34571C80DCA47224EF3C3FA2E38923BEC8C936AC490B73C748
      214F1C697A1CDEC9CAD0C491794CFC80DC2E56A6B1F41CDBCC1347A470ED6556
      361BE4D194C61294DF668AC3BE1868F2BD4EAD19DE5E2EE2DA86470E8EE42710
      FE4005DA82DD51F24AC01EB8C131F0FBF6ABD24256921EA44BD682099B4ABCBD
      C1D7939C180345708DF1347804FA48010C3C10897813BB8317152BD708BD95AB
      0231639DB217E0F9B1E97DB98EA5D3838446F3C833790309C92570FC742438BE
      AFB123B6B7C571BF18F3207C096FBC8CB6F3185696C2DE57951F889E2EF20D1A
      7607A2C683701CB70B9FF55B1ADE95AFC67C2A1A3B6305F0B65FD4ADFFDB0179
      DADEEA2FA644347384E682A7009E49A360A2BDEFDCC18CF62B4C7A23278A92E2
      0D8FC3EB5028531C69EE1839230A9E52983475EF2F165896AFB01C6BA0EDBD1C
      0349C4707581C61C926FF2C411ABF7D91E782B1F4F20057B6B1E54B4EAFC51C3
      599E38024BF20476FE442F4F1C696B7D980FEDECE74DBE183C5ECB75EFA0F100
      4F1C81EEFC84CDC717873DD18F4B04134D654EBC683C56D1FBDDA0E3892B2C68
      009ECB33005F2B6D43869D3C6185EC1A70DB9DEEBE93653C6185EC1A7039ECCB
      EAC0B2F7001A881877F084153470E3E9CD5DA335E6AEB1EAA6CEB8093F4280A3
      E6CEB1434B3600DF0369BFC1407BD8B0DE1F35EAE138EA7D43276AFCC3864AFC
      DB35505587BFB1030B0213FDDB807217A80DE098C3D98A2FB160F044D5C027D6
      198988F573B0392F4A2DEE0B4904F64454796AB7973199F943FD4FC24AC01DCC
      25DCC15CC21DCC1D09CD1FBF1FA3CB22C0F0220000000049454E44AE426082}
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso para registrar las p'#243'lizas de seguro de gastos m'#233'dicos a' +
        ' un grupo de empleados por medio de un archivo de texto.'
      Header.Title = 'Importaci'#243'n de seguro de gastos m'#233'dicos'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 458
        Height = 273
        Align = alTop
        TabOrder = 0
        object Panel2: TPanel
          Left = 1
          Top = 166
          Width = 456
          Height = 106
          Align = alBottom
          TabOrder = 0
          object Label4: TLabel
            Left = 9
            Top = 71
            Width = 79
            Height = 13
            Alignment = taRightJustify
            Caption = 'Formato F&echas:'
            FocusControl = cbFFecha
          end
          object Label2: TLabel
            Left = 17
            Top = 46
            Width = 71
            Height = 13
            Alignment = taRightJustify
            Caption = '&Formato ASCII:'
            FocusControl = Formato
          end
          object Label1: TLabel
            Left = 49
            Top = 21
            Width = 39
            Height = 13
            Alignment = taRightJustify
            Caption = 'Arc&hivo:'
            FocusControl = Archivo
          end
          object ArchivoSeek: TcxButton
            Left = 385
            Top = 17
            Width = 21
            Height = 21
            Hint = 'Buscar archivo de importaci'#243'n'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = ArchivoSeekClick
            OptionsImage.Glyph.Data = {
              DA050000424DDA05000000000000360000002800000013000000130000000100
              200000000000A40500000000000000000000000000000000000000B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
              FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
              FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
              F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
              F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
              F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
              F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
              FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          end
          object cbFFecha: TZetaKeyCombo
            Left = 92
            Top = 67
            Width = 185
            Height = 21
            AutoComplete = False
            BevelKind = bkFlat
            Style = csDropDownList
            Ctl3D = False
            ItemHeight = 13
            ParentCtl3D = False
            TabOrder = 2
            ListaFija = lfFormatoImpFecha
            ListaVariable = lvPuesto
            Offset = 0
            Opcional = False
            EsconderVacios = False
          end
          object Formato: TZetaKeyCombo
            Left = 92
            Top = 42
            Width = 185
            Height = 21
            AutoComplete = False
            BevelKind = bkFlat
            Style = csDropDownList
            Ctl3D = False
            ItemHeight = 13
            ParentCtl3D = False
            TabOrder = 1
            ListaFija = lfFormatoASCII
            ListaVariable = lvPuesto
            Offset = 0
            Opcional = False
            EsconderVacios = False
          end
          object Archivo: TEdit
            Left = 92
            Top = 17
            Width = 289
            Height = 21
            TabOrder = 0
          end
        end
        object Panel3: TPanel
          Left = 1
          Top = 1
          Width = 456
          Height = 165
          Align = alClient
          TabOrder = 1
          object Label3: TLabel
            Left = 56
            Top = 12
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'C'#243'digo:'
          end
          object txtPM_NUMERO: TZetaTextBox
            Left = 96
            Top = 32
            Width = 137
            Height = 17
            AutoSize = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object Label5: TLabel
            Left = 61
            Top = 33
            Width = 31
            Height = 13
            Alignment = taRightJustify
            Caption = 'P'#243'liza:'
          end
          object Label6: TLabel
            Left = 37
            Top = 56
            Width = 55
            Height = 13
            Alignment = taRightJustify
            Caption = 'Referencia:'
          end
          object txtPV_FEC_INI: TZetaTextBox
            Left = 96
            Top = 76
            Width = 136
            Height = 17
            AutoSize = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object Label7: TLabel
            Left = 47
            Top = 77
            Width = 44
            Height = 13
            Alignment = taRightJustify
            Caption = 'Vigencia:'
          end
          object Label8: TLabel
            Left = 237
            Top = 79
            Width = 9
            Height = 13
            Alignment = taRightJustify
            Caption = 'a:'
          end
          object txtPV_FEC_FIN: TZetaTextBox
            Left = 248
            Top = 76
            Width = 121
            Height = 17
            AutoSize = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object txtPV_CONDIC: TZetaTextBox
            Left = 96
            Top = 98
            Width = 353
            Height = 59
            AutoSize = False
            ShowAccelChar = False
            WordWrap = True
            Brush.Color = clBtnFace
            Border = True
          end
          object Label9: TLabel
            Left = 31
            Top = 97
            Width = 61
            Height = 13
            Alignment = taRightJustify
            Caption = 'Condiciones:'
          end
          object PV_REFEREN: TZetaKeyCombo
            Left = 96
            Top = 52
            Width = 137
            Height = 21
            AutoComplete = False
            BevelKind = bkFlat
            Style = csDropDownList
            Ctl3D = False
            ItemHeight = 13
            ParentCtl3D = False
            TabOrder = 1
            OnChange = PV_REFERENChange
            ListaFija = lfNinguna
            ListaVariable = lvPuesto
            Offset = 0
            Opcional = False
            EsconderVacios = False
          end
          object PM_CODIGO: TZetaKeyLookup_DevEx
            Left = 96
            Top = 8
            Width = 300
            Height = 21
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
            OnExit = PM_CODIGOExit
          end
        end
      end
      object PanelAnimation: TPanel
        Left = 0
        Top = 273
        Width = 458
        Height = 52
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Animate: TAnimate
          Left = 0
          Top = 0
          Width = 458
          Height = 35
          Align = alClient
          Active = True
          AutoSize = False
          CommonAVI = aviCopyFiles
          StopFrame = 1
        end
        object PanelMensaje: TPanel
          Left = 0
          Top = 35
          Width = 458
          Height = 17
          Align = alBottom
          BevelOuter = bvNone
          Caption = 'Verificando Archivo ...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 230
        Width = 458
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 458
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar este proceso se importar'#225'n las p'#243'lizas de los asegura' +
            'dos contenidos en el archivo.'
          Style.IsFontAssigned = True
          Width = 390
          AnchorY = 51
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 393
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'dat'
    Filter = 
      'Archivos de Datos (*.dat)|*.dat|Archivos de Texto (*.txt)|*.txt|' +
      'Todos (*.*)|*.*'
    Options = [ofHideReadOnly]
    Title = 'Seleccione el Archivo a Importar'
    Left = 390
    Top = 182
  end
end
