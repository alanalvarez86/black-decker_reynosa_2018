unit FWizIMSSCalcular_DevEx;

{$define SUA_14}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, ExtCtrls, 
     FWizImssBaseFiltro, ZetaDBTextBox, ZetaEdit, Mask, ZetaNumero,
     cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, 
     cxRadioGroup, cxTextEdit, cxMemo, cxButtons, dxGDIPlusClasses, cxImage,
     cxLabel, cxGroupBox, ZetaKeyLookup_DevEx, ZetaKeyCombo,
     dxCustomWizardControl, dxWizardControl, ZetaCXWizard, cxCheckBox;

type
  TWizIMSSCalcular_DevEx = class(TWizIMSSBaseFiltro)
    GroupBox1: TcxGroupBox;
    lCuotaFija: TcxCheckBox;
    SeguroVivienda: TZetaNumero;
    lAusentismos: TcxCheckBox;
    LblSeguroVivienda: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizIMSSCalcular_DevEx: TWizIMSSCalcular_DevEx;

implementation

{$R *.DFM}

uses DProcesos, DCliente, ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools;

procedure TWizIMSSCalcular_DevEx.FormCreate(Sender: TObject);
begin
     // (JB) A partir del �ltimo bimestre del 2010 el seguro de da�os de
     //      vivienda cambia de 13 a 15 pesos. CR 1900
     SeguroVivienda.Tag := K_GLOBAL_DEF_IMSS_PAGO_SUGERIDODANOS;
     inherited;
     HelpContext := H40441_Calcular_pago;
{$ifdef SUA_14}
     lAusentismos.Checked := TRUE;
     lCuotaFija.Visible := FALSE;
{$endif}


     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizIMSSCalcular_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ZetaClientTools.SetControlEnabled( SeguroVivienda, LblSeguroVivienda, EsBimestre( dmCliente.IMSSMes ) );
     Advertencia.Caption :=
        'Al ejecutar el proceso se generan las cuotas IMSS, Retiro e INFONAVIT para cada empleado del mes correspondiente.';
end;

procedure TWizIMSSCalcular_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddBoolean( 'Ausentismos', lAusentismos.Checked );
          AddBoolean( 'EnviarCuotaFija', lCuotaFija.Checked );
          AddFloat( 'SeguroVivienda', SeguroVivienda.Valor );
     end;

     //DevEx
     with Descripciones do
     begin
         AddBoolean( 'Ausentismos', lAusentismos.Checked );
         if ( lCuotaFija.Visible ) then
            AddBoolean( 'Enviar cuota fija', lCuotaFija.Checked );
         AddFloat( 'Seguro vivienda', SeguroVivienda.Valor );
     end;
end;

function TWizIMSSCalcular_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CalcularPagosIMSS( ParameterList );
end;



end.
