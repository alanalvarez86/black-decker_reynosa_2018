unit FWizEmpImportarKardex_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
     ZetaKeyCombo, ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit,
     ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
     dxCustomWizardControl, dxWizardControl, Menus, cxButtons;

type
  TWizEmpImportarKardex_DevEx = class(TcxBaseWizard)
    OpenDialog: TOpenDialog;
    Panel1: TPanel;
    Label1: TLabel;
    Archivo: TEdit;
    Label2: TLabel;
    Formato: TZetaKeyCombo;
    Label3: TLabel;
    Observaciones: TEdit;
    ArchivoSeek: TcxButton;
    PanelAnimation: TPanel;
    Animate: TAnimate;
    PanelMensaje: TPanel;
    Label4: TLabel;
    cbFFecha: TZetaKeyCombo;
    procedure ArchivoSeekClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx     //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FVerificaASCII: Boolean;
    FVerificacion: Boolean;
    ArchivoVerif : String;
    function LeerArchivo: Boolean;
    procedure SetVerificacion( const Value: Boolean );
    procedure CargaListaVerificaASCII;
    function GetArchivo: String;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean;
    procedure CargaListaVerificacion;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpImportarKardex_DevEx: TWizEmpImportarKardex_DevEx;

implementation

uses DProcesos,
     ZetaDialogo,
     ZetaMessages,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZAsciiTools,
     ZBaseSelectGrid_DevEx,
     ZBaseGridShow_DevEx,
     FEmpImportarKardexGridShow_DevEx,
     FEmpEventosGridSelect_DevEx, ZcxWizardBasico;

{$R *.DFM}

procedure TWizEmpImportarKardex_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PanelAnimation.Visible := FALSE;
     ParametrosControl := Archivo;
     Archivo.Text := ZetaMessages.SetFileNameDefaultPath( 'KARDEX.DAT' );
     {V2013}
     Formato.ItemIndex := Ord( faASCIIDel );

     cbFFecha.ItemIndex := Ord( ifDDMMYYYYs );
     ArchivoVerif:= VACIO;
     HelpContext:= H10167_Importar_kardex;
end;

procedure TWizEmpImportarKardex_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'Formato', Formato.Valor );
          AddInteger( 'FormatoImpFecha', cbFFecha.Valor );
          AddString( 'Archivo', Archivo.Text );
          AddMemo( 'Observaciones', Observaciones.Text );
     end;

     with Descripciones do
     begin
          AddString( 'Archivo', Archivo.Text );
          AddString( 'Formato ASCII', ObtieneElemento( lfFormatoASCII, Formato.Valor ) );
          AddString( 'Formato fechas', ObtieneElemento( lfFormatoImpFecha, cbFFecha.Valor ) );
          AddMemo( 'Observaciones', Observaciones.Text );
     end;
end;

procedure TWizEmpImportarKardex_DevEx.CargaListaVerificaASCII;
begin
     with dmProcesos do
     begin
          ImportarKardexGetListaASCII( ParameterList );
          if ( ErrorCount > 0 ) then                 // Hubo Errores
          begin
               with dmProcesos do
               begin
                    ZAsciiTools.FiltraASCII( cdsDataSet, True );
                    FVerificaASCII := ZBaseGridShow_DevEx.GridShow( cdsDataset, TEmpImportarKardexGridShow_DevEx );
                    ZAsciiTools.FiltraASCII( cdsDataSet, False );
               end;
          end
          else
              FVerificaASCII := True;
     end;
end;

procedure TWizEmpImportarKardex_DevEx.CargaListaVerificacion;
begin
     with dmProcesos do
     begin
          ImportarKardexGetLista( ParameterList );
     end;
end;

function TWizEmpImportarKardex_DevEx.Verificar: Boolean;
begin
     dmProcesos.cdsDataSet.First;
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpEventosGridSelect_DevEx, FALSE );
end;

procedure TWizEmpImportarKardex_DevEx.SetVerificacion(const Value: Boolean);
begin
     FVerificacion := Value;
end;

function TWizEmpImportarKardex_DevEx.GetArchivo: String;
begin
     Result := Archivo.Text;
end;

function TWizEmpImportarKardex_DevEx.LeerArchivo: Boolean;
begin
     Result := FALSE;
     PanelAnimation.Visible := TRUE;
     Application.ProcessMessages;
        CargaParametros;
        CargaListaVerificaASCII;
     PanelAnimation.Visible := FALSE;
     if FVerificaASCII then
     begin
          CargaListaVerificacion;
          SetVerificacion( Verificar );
          Result := FVerificacion;
     end;
     if Result then
        ArchivoVerif := GetArchivo
     else
        ArchivoVerif := VACIO;
end;

procedure TWizEmpImportarKardex_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;  //DevEx
     if Wizard.Adelante then
     begin
          //if ( PageControl.ActivePage = Parametros ) then
          if ( WizardControl.ActivePage = Parametros ) then    //DevEx
          begin
               if ZetaCommonTools.StrVacio( GetArchivo ) then
                  CanMove := Error( 'Debe Especificarse un Archivo a Importar', Archivo )
               else
                  if not FileExists( GetArchivo ) then
                     CanMove := Error( 'El Archivo ' + GetArchivo + ' No Existe', Archivo )
                  else
                  begin
                       if ( ArchivoVerif <> GetArchivo ) or
                          ( ZetaDialogo.ZConfirm( Caption, 'El Archivo ' + GetArchivo + ' Ya Fu� Verificado !' + CR_LF +
                                                  'Volver a Verificar ?', 0, mbYes ) ) then
                          CanMove := LeerArchivo
                       else
                          CanMove := TRUE;
                  end;
          end;
     end;
end;

function TWizEmpImportarKardex_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarKardex( ParameterList );
end;

procedure TWizEmpImportarKardex_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     Archivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, Archivo.Text, 'dat' );
{     with OpenDialog do
     begin
          FileName := ExtractFileName( Archivo.Text );
          InitialDir := ExtractFilePath( Archivo.Text );
          if Execute then
             Archivo.Text := FileName;
     end; }
end;

procedure TWizEmpImportarKardex_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Al ejecutar el proceso generar� un movimiento de kardex.';
end;


//DevEx     //Agregado para enfocar un control
procedure TWizEmpImportarKardex_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
         ParametrosControl := Archivo
     else
         ParametrosControl := nil;
     inherited;
end;

end.
