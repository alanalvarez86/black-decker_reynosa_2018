unit FWizNomSimAplicarFinGlobal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZetaDBTextBox, StdCtrls, ExtCtrls, ComCtrls,
  FWizNomBase_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit, cxMemo,
  cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage, dxCustomWizardControl,
  dxWizardControl, ZetaEdit, cxCheckBox;

type
  TWizNomSimAplicarFinGlobal_DevEx = class(TWizNomBase_DevEx)
    chReEnviar: TcxCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure CargaListaVerificacion;override;
    procedure CargaParametros;override;
    function EjecutarWizard: Boolean;override;
    function Verificar: Boolean;override;
  end;

var
  WizNomSimAplicarFinGlobal_DevEx: TWizNomSimAplicarFinGlobal_DevEx;

implementation

uses
    DCliente,
    ZetaCommonLists,
    ZetaCommonTools,
    DGlobal,
    ZGlobalTress,
    DProcesos,
    FEmpleadoGridSelectAplicaFiniquito_DevEx,
    ZetaCommonClasses,
    DNomina,
    ZBaseSelectGrid_DevEx, ZConfirmaVerificacion_DevEx, ZcxWizardBasico,
    ZCXBaseWizardFiltro;

{$R *.dfm}

procedure TWizNomSimAplicarFinGlobal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= HHHHH_Aplicar_Finiquitos_Global;
end;


procedure TWizNomSimAplicarFinGlobal_DevEx.CargaListaVerificacion;
begin
    dmProcesos.AplicarFiniquitosGlobalGetLista( ParameterList );
end;

procedure TWizNomSimAplicarFinGlobal_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddBoolean( 'ReEnviarAplicadas', chReEnviar.Checked );
     end;

     with Descripciones do
     begin
          if( chReEnviar.Checked ) then
              AddString( 'Reenviar aplicadas', 'S�' )
          else
              AddString( 'Reenviar aplicadas', 'No' );
     end;   
end;

function TWizNomSimAplicarFinGlobal_DevEx.EjecutarWizard: Boolean;
begin
      Result := dmProcesos.AplicarFiniquitosGlobales(ParameterList,Verificacion);
      dmNomina.cdsSimulaGlobales.Refrescar;
end;

function TWizNomSimAplicarFinGlobal_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelectAplicaFiniquito_DevEx, False );
end;

procedure TWizNomSimAplicarFinGlobal_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     //DevEx
     //Parametros.Enabled := False;
     Advertencia.Caption := 'Se copiar�n las liquidaciones globales hacia el periodo de baja del empleado y las simulaciones globales quedar�n con estatus aplicada.';
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizNomSimAplicarFinGlobal_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     //if ( PageControl.ActivePage <> Ejecucion )then
     if ( WizardControl.ActivePage <> Ejecucion )then   //DevEx
     begin
          //if iNewPage = 1 then
          if iNewPage = ( Ejecucion.PageIndex ) then     //DevEx
          begin
               SeleccionarClick(Self);
               CanMove :=  Verificacion;
          end;
     end
     else
         CanMove := True; 
end;

end.
