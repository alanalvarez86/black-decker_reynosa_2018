inherited SistEnrolamientoMasivoGridSelect_DevEx: TSistEnrolamientoMasivoGridSelect_DevEx
  Left = 196
  Top = 317
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Empleados Deseados'
  ClientHeight = 216
  ClientWidth = 912
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 180
    Width = 912
    inherited OK_DevEx: TcxButton
      Left = 748
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 826
    end
  end
  inherited PanelSuperior: TPanel
    Width = 912
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 912
    Height = 145
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 65
      end
      object US_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'US_NOMBRE'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 227
      end
      object US_CORTO: TcxGridDBColumn
        Caption = 'Usuario'
        DataBinding.FieldName = 'US_CORTO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 117
      end
      object US_PASSWRD: TcxGridDBColumn
        Caption = 'Clave'
        DataBinding.FieldName = 'US_PASSWRD'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 91
      end
      object US_NOMBRE2: TcxGridDBColumn
        Caption = 'Nombre de Usuario'
        DataBinding.FieldName = 'US_NOMBRE'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Width = 20
      end
      object US_EMAIL: TcxGridDBColumn
        Caption = 'Correo Electr'#243'nico'
        DataBinding.FieldName = 'US_EMAIL'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 115
      end
      object US_DOMAIN: TcxGridDBColumn
        Caption = 'Usuario del dominio'
        DataBinding.FieldName = 'US_DOMAIN'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 110
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
