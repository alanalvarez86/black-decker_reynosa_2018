inherited WizSistBorrarHerramientas_DevEx: TWizSistBorrarHerramientas_DevEx
  Left = 244
  Top = 176
  Caption = 'Borrar Herramientas'
  ClientHeight = 416
  ClientWidth = 430
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 430
    Height = 416
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que borra los registros de las herramientas asignadas a ' +
        'los empleados en ciertas fechas.'
      Header.Title = 'Borrar herramientas'
      object GBFechas: TcxGroupBox
        Left = 8
        Top = 79
        Caption = ' Rango de Fechas: '
        TabOrder = 1
        Height = 57
        Width = 393
        object Label1: TLabel
          Left = 17
          Top = 23
          Width = 30
          Height = 13
          Caption = 'Inicial:'
          Transparent = True
        end
        object Label2: TLabel
          Left = 201
          Top = 23
          Width = 25
          Height = 13
          Caption = 'Final:'
          Transparent = True
        end
        object FechaIni: TZetaFecha
          Left = 52
          Top = 19
          Width = 135
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '27/dic/00'
          Valor = 36887.000000000000000000
        end
        object FechaFin: TZetaFecha
          Left = 231
          Top = 19
          Width = 135
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '27/dic/00'
          Valor = 36887.000000000000000000
        end
      end
      object GBFiltros: TcxGroupBox
        Left = 8
        Top = 146
        Caption = ' Filtrar por: '
        TabOrder = 2
        Height = 97
        Width = 393
        object CBTool: TCheckBox
          Left = 16
          Top = 16
          Width = 110
          Height = 17
          Alignment = taLeftJustify
          Caption = '           Herramienta:'
          TabOrder = 0
          OnClick = CheckBoxClick
        end
        object TO_CODIGO: TZetaKeyLookup_DevEx
          Left = 136
          Top = 14
          Width = 250
          Height = 21
          LookupDataset = dmCatalogos.cdsTools
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 75
        end
        object KT_TALLA: TZetaKeyLookup_DevEx
          Left = 136
          Top = 39
          Width = 250
          Height = 21
          LookupDataset = dmTablas.cdsTallas
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 3
          TabStop = True
          WidthLlave = 75
        end
        object KT_MOT_FIN: TZetaKeyLookup_DevEx
          Left = 136
          Top = 64
          Width = 250
          Height = 21
          LookupDataset = dmTablas.cdsMotTool
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 5
          TabStop = True
          WidthLlave = 75
        end
        object CBTalla: TCheckBox
          Left = 16
          Top = 41
          Width = 110
          Height = 17
          Alignment = taLeftJustify
          Caption = '                      Talla:'
          TabOrder = 2
          OnClick = CheckBoxClick
        end
        object CBMotivo: TCheckBox
          Left = 16
          Top = 66
          Width = 110
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Motivo Devoluci'#243'n:'
          TabOrder = 4
          OnClick = CheckBoxClick
        end
      end
      object RGActivos: TcxRadioGroup
        Left = 8
        Top = 17
        Caption = ' Empleados'
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = 'Activos'
            Value = 'Activos'
          end
          item
            Caption = 'Inactivos'
            Value = 'Inactivos'
          end>
        ItemIndex = 1
        TabOrder = 0
        OnClick = CheckBoxClick
        Height = 49
        Width = 393
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 181
        Width = 408
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 408
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 340
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 9
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 34
        Top = 166
      end
      inherited Seleccionar: TcxButton
        Left = 137
        Top = 254
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 61
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Left = 61
        Top = 165
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 61
        Top = 4
      end
      inherited bAjusteISR: TcxButton
        Left = 380
        Top = 166
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 280
    Top = 2
  end
end
