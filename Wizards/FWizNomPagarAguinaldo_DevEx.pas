unit FWizNomPagarAguinaldo_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Mask, StdCtrls, Buttons, ComCtrls, ExtCtrls,
     ZetaNumero,
     ZetaEdit,
     ZetaDBTextBox,
     ZetaWizard, FWizNomBase_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl;

type
  TWizNomPagarAguinaldo_DevEx = class(TWizNomBase_DevEx)
    YearLBL: TLabel;
    Year: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomPagarAguinaldo_DevEx: TWizNomPagarAguinaldo_DevEx;

implementation

uses ZetaCommonClasses,
     ZBaseSelectGrid_DevEx,
     FEmpleadoGridSelect_Devex,
     DProcesos,
     DCliente;

{$R *.DFM}

procedure TWizNomPagarAguinaldo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Year.Valor := dmCliente.YearDefault;
     ParametrosControl := Year;
     HelpContext := H30354_Pagar_aguinaldos;
end;

procedure TWizNomPagarAguinaldo_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'YearAguinaldo', Year.ValorEntero );
     end;
     with Descripciones do
     begin
          AddInteger( 'A�o', Year.ValorEntero );
     end;
end;

procedure TWizNomPagarAguinaldo_DevEx.CargaListaVerificacion;
begin
     dmProcesos.PagarAguinaldoGetLista( ParameterList );
end;

function TWizNomPagarAguinaldo_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

function TWizNomPagarAguinaldo_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.PagarAguinaldo( ParameterList, Verificacion );
end;

procedure TWizNomPagarAguinaldo_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
  inherited;
ParametrosControl := nil;
end;

procedure TWizNomPagarAguinaldo_DevEx.WizardAfterMove(Sender: TObject);
begin
  inherited;
  if Wizard.EsPaginaActual(Parametros) then
     ParametrosControl := Year;
end;

end.
