unit FWizEmpBorrarCursoTomado_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FWizEmpBaseFiltro, ComCtrls, ZetaEdit, StdCtrls, Buttons,
  ZetaWizard, ExtCtrls, Mask, ZetaFecha, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit,
  cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizEmpBorrarCursoTomado_DevEx = class(TWizEmpBaseFiltro)
    dFinal: TZetaFecha;
    dInicio: TZetaFecha;
    sMaestro: TZetaKeyLookup_DevEx;
    sCurso: TZetaKeyLookup_DevEx;
    KC_FEC_FINLbl: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);  //DevEx(by am): Agregado para enfocar correctamente los controles de las paginas del wz.
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpBorrarCursoTomado_DevEx: TWizEmpBorrarCursoTomado_DevEx;

implementation

{$R *.DFM}

uses
    DCatalogos,
    DProcesos,
    DCliente,
    DGlobal,
    ZGlobalTress,
    ZetaCommonClasses,
    ZetaCommonTools,
    ZetaDialogo,
    ZBaseSelectGrid_DevEx,
    FEmpBorraCursoTomadoGridSelect_DevEx;

procedure TWizEmpBorrarCursoTomado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := sCurso;
     dInicio.Valor := dmCliente.FechaDefault;
     dFinal.Valor := dInicio.Valor;
     HelpContext := H10176_Cancelar_curso_tomado_global;
     with dmCatalogos do
     begin
          sCurso.LookupDataset := cdsCursos;
          sMaestro.LookupDataset := cdsMaestros;
     end;
     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TWizEmpBorrarCursoTomado_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          cdsMaestros.Conectar;
     end;
     //Texto para la ejecucion final
     Advertencia.Caption := ' Al aplicar el proceso se eliminar�n los registros que realizaron de manera global en las fechas indicadas.'; 
end;

procedure TWizEmpBorrarCursoTomado_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with Descripciones do
     begin
          AddString( 'Curso', GetDescripLlave( sCurso ) );
          AddString( 'Maestro', GetDescripLlave( sMaestro ) );
          AddDate( 'Inicio', dInicio.Valor );
          AddDate( 'Final', dFinal.Valor );
     end;

     with ParameterList do
     begin
          AddString( 'Curso', sCurso.Llave );
          AddString( 'Maestro', sMaestro.Llave );
          AddDate( 'Inicio', dInicio.Valor );
          AddDate( 'Final', dFinal.Valor );
     end;
end;

procedure TWizEmpBorrarCursoTomado_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const
K_CODIGO_INVALIDO = 'C�digo Inactivo!!!';
begin
     inherited;
     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros ) then
          begin
               if StrVacio( sCurso.Llave ) then
                  CanMove := Error( '� No se ha especificado un curso !', sCurso )
               else if ( dFinal.Valor < dInicio.Valor ) then
                  CanMove := Error( '� La Fecha final del curso no puede ser menor que la inicial !', dFinal );
               //Validaci�n de catalogos inactivos   @DACP
               if  Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS ) then
               begin
                    if sCurso.Descripcion = K_CODIGO_INVALIDO then
                       CanMove := FALSE;
                    if sMaestro.Descripcion = K_CODIGO_INVALIDO then
                       CanMove := FALSE;
               end;
          end;
     end;
end;

procedure TWizEmpBorrarCursoTomado_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control de la pagina de parametros
     if (Wizard.EsPaginaActual( Parametros ))then
        ParametrosControl := sCurso;
     inherited;
end;

procedure TWizEmpBorrarCursoTomado_DevEx.CargaListaVerificacion;
begin
     dmProcesos.BorrarCursoTomadoGetLista( ParameterList );
end;

function TWizEmpBorrarCursoTomado_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpBorraCursoTomadoGridSelect_DevEx );
end;

function TWizEmpBorrarCursoTomado_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.BorrarCursoTomado( ParameterList, Verificacion );
end;

procedure TWizEmpBorrarCursoTomado_DevEx.SetEditarSoloActivos;
begin
     sCurso.EditarSoloActivos := TRUE;
     sMaestro.EditarSoloActivos := TRUE;
end;

end.
