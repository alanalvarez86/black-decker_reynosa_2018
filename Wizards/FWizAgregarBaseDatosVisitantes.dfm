inherited WizAgregarBaseDatosVisitantes: TWizAgregarBaseDatosVisitantes
  Left = 365
  Top = 417
  Caption = 'Agregar BD de tipo Visitantes'
  ClientHeight = 289
  ClientWidth = 433
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 253
    Width = 433
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Enabled = True
    end
    inherited Siguiente: TZetaWizardButton
      Enabled = False
    end
    inherited Ejecutar: TZetaWizardButton
      Enabled = True
    end
  end
  inherited PageControl: TPageControl
    Width = 433
    Height = 253
    ActivePage = Ejecucion
    inherited Parametros: TTabSheet
      object lbNombreBD: TLabel
        Left = 58
        Top = 113
        Width = 136
        Height = 13
        Caption = 'Nombre de la base de datos:'
      end
      object lblSeleccionarBD: TLabel
        Left = 65
        Top = 185
        Width = 129
        Height = 13
        Caption = 'Seleccionar base de datos:'
      end
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 425
        Height = 65
        Align = alTop
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Lines.Strings = (
          'Seleccione la opci'#243'n correspondiente para agregar una  base de '
          'datos de tipo Visitantes.')
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object rbBDNueva: TRadioButton
        Left = 32
        Top = 85
        Width = 233
        Height = 17
        Caption = 'C&rear base de datos nueva'
        Checked = True
        TabOrder = 1
        TabStop = True
        OnClick = rbBDNuevaClick
      end
      object txtBaseDatos: TEdit
        Left = 200
        Top = 109
        Width = 177
        Height = 21
        TabOrder = 2
      end
      object rbBDExistente: TRadioButton
        Left = 32
        Top = 161
        Width = 153
        Height = 17
        Caption = '&Base de datos existente:'
        TabOrder = 3
        OnClick = rbBDExistenteClick
      end
      object cbBasesDatos: TComboBox
        Left = 200
        Top = 181
        Width = 177
        Height = 21
        Style = csDropDownList
        Enabled = False
        ItemHeight = 13
        TabOrder = 4
      end
    end
    object NuevaBD1: TTabSheet [1]
      Caption = 'NuevaBD1'
      ImageIndex = 2
      object lblCantidadEmpleados: TLabel
        Left = 68
        Top = 106
        Width = 48
        Height = 13
        Caption = 'Visitantes:'
      end
      object lblCantEmpSeleccion: TLabel
        Left = 335
        Top = 103
        Width = 100
        Height = 13
        Caption = 'lblCantEmpSeleccion'
      end
      object lblNotaEmpleados: TLabel
        Left = 10
        Top = 140
        Width = 270
        Height = 13
        Caption = 'Considere el crecimiento estimado en los pr'#243'ximos 3 a'#241'os'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblDocumentos: TLabel
        Left = 19
        Top = 184
        Width = 104
        Height = 13
        Caption = 'Documentos digitales:'
      end
      object lblDocSeleccion: TLabel
        Left = 335
        Top = 181
        Width = 77
        Height = 13
        Caption = 'lblDocSeleccion'
      end
      object lblNotaDocumentos: TLabel
        Left = 10
        Top = 214
        Width = 296
        Height = 13
        Caption = 'Cantidad de documentos digitales que se subir'#225'n por visitantes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object memoEmpleadosDatos: TMemo
        Left = 0
        Top = 0
        Width = 425
        Height = 89
        Align = alTop
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object tbEmpleados: TcxTrackBar
        Left = 123
        Top = 90
        ParentFont = False
        Position = 300
        Properties.Frequency = 1200
        Properties.Max = 5000
        Properties.Min = 300
        Properties.TickType = tbttTicksAndNumbers
        Properties.OnChange = tbEmpleadosPropertiesChange
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.TextStyle = []
        Style.IsFontAssigned = True
        TabOrder = 1
        Height = 49
        Width = 214
      end
      object tbDocumentos: TcxTrackBar
        Left = 123
        Top = 168
        Position = 1
        Properties.Frequency = 3
        Properties.Max = 12
        Properties.Min = 1
        Properties.TickType = tbttTicksAndNumbers
        Properties.OnChange = tbDocumentosPropertiesChange
        TabOrder = 2
        Height = 49
        Width = 214
      end
    end
    object NuevaBD2: TTabSheet [2]
      Caption = 'NuevaBD2'
      ImageIndex = 3
      object lblUbicacion: TLabel
        Left = 33
        Top = 10
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ubicaci'#243'n:'
      end
      object lblTamanoBDOrigen: TLabel
        Left = 146
        Top = 32
        Width = 139
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tama'#241'o de archivo de datos:'
      end
      object lblMB1: TLabel
        Left = 356
        Top = 32
        Width = 16
        Height = 13
        Caption = 'MB'
      end
      object lblArchivoLog: TLabel
        Left = 225
        Top = 54
        Width = 60
        Height = 13
        Caption = 'Archivo Log:'
      end
      object lblMB2: TLabel
        Left = 357
        Top = 54
        Width = 16
        Height = 13
        Caption = 'MB'
      end
      object txtUbicacion: TZetaEdit
        Left = 87
        Top = 6
        Width = 266
        Height = 21
        MaxLength = 10
        ReadOnly = True
        TabOrder = 0
      end
      object txtTamanyo: TZetaNumero
        Left = 288
        Top = 28
        Width = 65
        Height = 21
        Mascara = mnMillares
        ReadOnly = True
        TabOrder = 1
        Text = '0.000'
      end
      object txtLog: TZetaNumero
        Left = 288
        Top = 50
        Width = 65
        Height = 21
        Mascara = mnDiasFraccion
        ReadOnly = True
        TabOrder = 2
        Text = '0.00'
      end
      object GBUsuariosMSSQL: TGroupBox
        Left = 32
        Top = 80
        Width = 353
        Height = 129
        TabOrder = 3
        object lblUsuarioMSSQL: TLabel
          Left = 16
          Top = 10
          Width = 233
          Height = 13
          Caption = 'Usuario de MSSQL para crear y acceder a la BD:'
        end
        object lblUsuario: TLabel
          Left = 94
          Top = 71
          Width = 39
          Height = 13
          Caption = 'Usuario:'
        end
        object lblClave: TLabel
          Left = 103
          Top = 93
          Width = 30
          Height = 13
          Caption = 'Clave:'
        end
        object rbSameUser: TRadioButton
          Left = 40
          Top = 27
          Width = 265
          Height = 17
          Caption = 'Usuario y clave default (Configuraci'#243'n del Sistema)'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = rbSameUserClick
        end
        object rbOtroUser: TRadioButton
          Left = 40
          Top = 47
          Width = 209
          Height = 17
          Caption = 'Utilizar otro usuario y clave de acceso:'
          TabOrder = 1
          OnClick = rbOtroUserClick
        end
        object txtUsuario: TEdit
          Left = 136
          Top = 67
          Width = 121
          Height = 21
          Enabled = False
          TabOrder = 2
        end
        object txtClave: TMaskEdit
          Left = 136
          Top = 89
          Width = 121
          Height = 21
          Enabled = False
          PasswordChar = '*'
          TabOrder = 3
        end
      end
    end
    object Estructura: TTabSheet [3]
      Caption = 'Estructura'
      ImageIndex = 4
      object lblCodigo: TLabel
        Left = 81
        Top = 92
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
      end
      object lblDescripcion: TLabel
        Left = 58
        Top = 132
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
      end
      object memConfirmacion: TMemo
        Left = 0
        Top = 0
        Width = 425
        Height = 65
        Align = alTop
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Lines.Strings = (
          'Ingrese los campos C'#243'digo y Descripci'#243'n para la base de datos '
          'que se va a agregar.')
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object txtCodigo: TZetaEdit
        Left = 120
        Top = 88
        Width = 121
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 1
        OnExit = txtCodigoExit
      end
      object txtDescripcion: TZetaEdit
        Left = 120
        Top = 128
        Width = 257
        Height = 21
        TabOrder = 2
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 425
        Alignment = taLeftJustify
      end
      inherited ProgressPanel: TPanel
        Top = 175
        Width = 425
        object lblProceso: TLabel [0]
          Left = 24
          Top = 25
          Width = 27
          Height = 13
          Caption = 'Paso:'
        end
      end
    end
  end
end
