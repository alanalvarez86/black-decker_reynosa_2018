inherited WizReportesBaseFiltro: TWizReportesBaseFiltro
  Left = 301
  Top = 130
  Caption = 'WizReportesBaseFiltro'
  ClientHeight = 540
  ClientWidth = 602
  ExplicitWidth = 608
  ExplicitHeight = 569
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 602
    Height = 540
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
      7249000003CF494441545847B5963F68137114C76F70C8D0A1430707073B0876
      7013914E824B06070727117470D04D5CB248AEA2B56A6D93264DD2FB97CBE572
      97FBD3186A5B8B58A8884B2705B577AD60411011840E0A452AC6F7E25DFCE5EE
      97988BF5C187DF71F77BDFF7EEDDFBFD7EC7A0B19A3BC4569DC3FB86F5E66053
      B817634D672C69BA7BACE536F693A4E54E7A21BA5BD272765B4E9008386E9342
      FD825A5E88EED672C0446A5B87F05ED2DC9C20C5FAA519E06FE64F4E9ACE8A77
      8B618D8D93A450BF7872DDCD9FDCEC03D38D6343266DB74A0AF58B17A2BB41E0
      4F34E77F057477BC10DD8DB59C4B507E079B6FDF30DD77B0BAAE7821FECD1A8D
      C67F8561E5ED186BB867B11291B19DE35E9EFD5BD2DAACD3BE61377E37AC33C6
      AEAD1D78707392F11172A5F5ECFDDC77B86EA4C6D33FE7A6B92F1541BF4ECE09
      D25A05BDE2053F17145AB49731994610295B5C87B16D2E49E40460A95EA00969
      821E87319400546457961E0EC175C8078994007478019DCAC5DA113E2D7C5079
      3D6194EC13425E7D0AF743C17DD213991FF029129A581D56B9CA1DA55056E17E
      B4047069DD365E0E88423DC6A7B8CF64807EA870EA0D18B109DDD7B480210CF7
      1A3A94382D4D0AF5CBD4ADA9862E19C30C2E252C2DBCA1DC0DB6BE3D98139FC6
      66BD2EA7313755D8A9F05AC290AA71E8898B50EA85A95BD3D4B948794EE5BCC5
      D89BF1F2F2199A1022CF16D7E1FBC6E0BAADC974B13A9A9998F9817382E4EECF
      EE32F866B86D36D775276CE71426004E8CCA6B77A6A17CA450F65E76CF281A1D
      3BBD2A55AFC2D8EE7337B307F7E37018392BD46F4EE2EDEB2886C879E51929A6
      E44B0BFE331A589960D255C9C06AF6B80A60ED9309701969CB174274414BF8CF
      3A917F50F80663CBC792CDD378BFB7048C8DCB640262B6F8C21742A0998AFE33
      1AB3E26A2CD88CA66C454800FAC04F40158D44B09C1CEC0B8BE662DBB9405216
      CDF330B6F9881969C3D41E0DF69400FC2FAA9800272F86847C3A5521AFAE1E84
      A5BB0BD7219FB914FF11FF885ED282928C996FBFCE48CF63BAF678303D9EFA49
      1343E0ADB654C93AA3E92B838A5C3F221417AEA6EF66F7687311A5A0180C3BEF
      1E4DDA9BA9E0C643326EBC9245F5C9517062A0E36B41A17E9957EC11AC6CCF06
      4EB0A68D21DC4082625181B3A079204532744024B116CF4EF33B65AE928684CE
      E316EC0BD3C0A62DE5959A2EEAD7E127E5A33023BE5FB6979A4D1BC9D081C612
      FC8C940B0A366128787E32F7CD56E647E0BA351FCE8BD6961DC948912070001D
      833194809C6BF60CD507896434019F3A9414F6F7D0A1A3F195E6B9DF89484613
      08A24AF6107CEB51AB648DD8256B8036E70F93CC2FADD62E283A1FEEC1000000
      0049454E44AE426082}
    ExplicitWidth = 602
    ExplicitHeight = 540
    inherited Parametros: TdxWizardControlPage
      ExplicitWidth = 580
      ExplicitHeight = 400
      object Label5: TLabel
        Left = 183
        Top = 224
        Width = 56
        Height = 13
        Caption = '&Frecuencia:'
        FocusControl = cbFrecuencia
      end
      object cbFrecuencia: TZetaDBKeyCombo
        Left = 245
        Top = 222
        Width = 145
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        OnChange = cbFrecuenciaChange
        ListaFija = lfEmailFrecuencia
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        LlaveNumerica = True
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 580
      ExplicitHeight = 400
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 580
        ExplicitHeight = 305
        Height = 305
        Width = 580
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 580
        Width = 580
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 512
          ExplicitHeight = 74
          Width = 512
          AnchorY = 57
        end
        inherited cxImage1: TcxImage
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitHeight = 74
        end
      end
    end
    object pDiario: TdxWizardControlPage
      Header.AssignedValues = [wchvGlyph]
      Header.Glyph.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
        6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
        7249000003CF494441545847B5963F68137114C76F70C8D0A1430707073B0876
        7013914E824B06070727117470D04D5CB248AEA2B56A6D93264DD2FB97CBE572
        97FBD3186A5B8B58A8884B2705B577AD60411011840E0A452AC6F7E25DFCE5EE
        97988BF5C187DF71F77BDFF7EEDDFBFD7EC7A0B19A3BC4569DC3FB86F5E66053
        B817634D672C69BA7BACE536F693A4E54E7A21BA5BD272765B4E9008386E9342
        FD825A5E88EED672C0446A5B87F05ED2DC9C20C5FAA519E06FE64F4E9ACE8A77
        8B618D8D93A450BF7872DDCD9FDCEC03D38D6343266DB74A0AF58B17A2BB41E0
        4F34E77F057477BC10DD8DB59C4B507E079B6FDF30DD77B0BAAE7821FECD1A8D
        C67F8561E5ED186BB867B11291B19DE35E9EFD5BD2DAACD3BE61377E37AC33C6
        AEAD1D78707392F11172A5F5ECFDDC77B86EA4C6D33FE7A6B92F1541BF4ECE09
        D25A05BDE2053F17145AB49731994610295B5C87B16D2E49E40460A95EA00969
        821E87319400546457961E0EC175C8078994007478019DCAC5DA113E2D7C5079
        3D6194EC13425E7D0AF743C17DD213991FF029129A581D56B9CA1DA55056E17E
        B4047069DD365E0E88423DC6A7B8CF64807EA870EA0D18B109DDD7B480210CF7
        1A3A94382D4D0AF5CBD4ADA9862E19C30C2E252C2DBCA1DC0DB6BE3D98139FC6
        66BD2EA7313755D8A9F05AC290AA71E8898B50EA85A95BD3D4B948794EE5BCC5
        D89BF1F2F2199A1022CF16D7E1FBC6E0BAADC974B13A9A9998F9817382E4EECF
        EE32F866B86D36D775276CE71426004E8CCA6B77A6A17CA450F65E76CF281A1D
        3BBD2A55AFC2D8EE7337B307F7E37018392BD46F4EE2EDEB2886C879E51929A6
        E44B0BFE331A589960D255C9C06AF6B80A60ED9309701969CB174274414BF8CF
        3A917F50F80663CBC792CDD378BFB7048C8DCB640262B6F8C21742A0998AFE33
        1AB3E26A2CD88CA66C454800FAC04F40158D44B09C1CEC0B8BE662DBB9405216
        CDF330B6F9881969C3D41E0DF69400FC2FAA9800272F86847C3A5521AFAE1E84
        A5BB0BD7219FB914FF11FF885ED282928C996FBFCE48CF63BAF678303D9EFA49
        1343E0ADB654C93AA3E92B838A5C3F221417AEA6EF66F7687311A5A0180C3BEF
        1E4DDA9BA9E0C643326EBC9245F5C9517062A0E36B41A17E9957EC11AC6CCF06
        4EB0A68D21DC4082625181B3A079204532744024B116CF4EF33B65AE928684CE
        E316EC0BD3C0A62DE5959A2EEAD7E127E5A33023BE5FB6979A4D1BC9D081C612
        FC8C940B0A366128787E32F7CD56E647E0BA351FCE8BD6961DC948912070001D
        833194809C6BF60CD507896434019F3A9414F6F7D0A1A3F195E6B9DF89484613
        08A24AF6107CEB51AB648DD8256B8036E70F93CC2FADD62E283A1FEEC1000000
        0049454E44AE426082}
      Header.Description = 
        'Indicar los par'#225'metros de ejecuci'#243'n del env'#237'o con frecuencia dia' +
        'ria.'
      Header.Title = 'Tarea con frecuencia diaria'
      object Label13: TLabel
        Left = 160
        Top = 159
        Width = 128
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha de inicio de la tarea:'
        FocusControl = fdFechaInicio
      end
      object lfdHora: TLabel
        Left = 222
        Top = 189
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = '&Hora servidor:'
        FocusControl = fdHora
      end
      object Label15: TLabel
        Left = 213
        Top = 218
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = '&Repetirse cada:'
        FocusControl = fdRepeticion
      end
      object Label16: TLabel
        Left = 340
        Top = 219
        Width = 27
        Height = 13
        Caption = 'd'#237'a(s)'
      end
      object Label22: TLabel
        Left = 339
        Top = 189
        Width = 97
        Height = 13
        Alignment = taRightJustify
        Caption = 'Formato de 24 horas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object fdFechaInicio: TZetaDBFecha
        Left = 296
        Top = 156
        Width = 112
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '07/Dec/16'
        Valor = 42711.000000000000000000
        DataField = 'CA_FECHA'
      end
      object fdHora: TZetaDBHora
        Left = 296
        Top = 186
        Width = 37
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 1
        Text = '    '
        Tope = 24
        Valor = '    '
        DataField = 'CA_HORA'
      end
      object fdRepeticion: TZetaDBNumero
        Left = 296
        Top = 215
        Width = 38
        Height = 21
        Mascara = mnDias
        MaxLength = 2
        TabOrder = 2
        Text = '0'
        DataField = 'CA_RECUR'
      end
    end
    object pMensual: TdxWizardControlPage
      Header.AssignedValues = [wchvGlyph]
      Header.Glyph.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
        6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
        7249000003CF494441545847B5963F68137114C76F70C8D0A1430707073B0876
        7013914E824B06070727117470D04D5CB248AEA2B56A6D93264DD2FB97CBE572
        97FBD3186A5B8B58A8884B2705B577AD60411011840E0A452AC6F7E25DFCE5EE
        97988BF5C187DF71F77BDFF7EEDDFBFD7EC7A0B19A3BC4569DC3FB86F5E66053
        B817634D672C69BA7BACE536F693A4E54E7A21BA5BD272765B4E9008386E9342
        FD825A5E88EED672C0446A5B87F05ED2DC9C20C5FAA519E06FE64F4E9ACE8A77
        8B618D8D93A450BF7872DDCD9FDCEC03D38D6343266DB74A0AF58B17A2BB41E0
        4F34E77F057477BC10DD8DB59C4B507E079B6FDF30DD77B0BAAE7821FECD1A8D
        C67F8561E5ED186BB867B11291B19DE35E9EFD5BD2DAACD3BE61377E37AC33C6
        AEAD1D78707392F11172A5F5ECFDDC77B86EA4C6D33FE7A6B92F1541BF4ECE09
        D25A05BDE2053F17145AB49731994610295B5C87B16D2E49E40460A95EA00969
        821E87319400546457961E0EC175C8078994007478019DCAC5DA113E2D7C5079
        3D6194EC13425E7D0AF743C17DD213991FF029129A581D56B9CA1DA55056E17E
        B4047069DD365E0E88423DC6A7B8CF64807EA870EA0D18B109DDD7B480210CF7
        1A3A94382D4D0AF5CBD4ADA9862E19C30C2E252C2DBCA1DC0DB6BE3D98139FC6
        66BD2EA7313755D8A9F05AC290AA71E8898B50EA85A95BD3D4B948794EE5BCC5
        D89BF1F2F2199A1022CF16D7E1FBC6E0BAADC974B13A9A9998F9817382E4EECF
        EE32F866B86D36D775276CE71426004E8CCA6B77A6A17CA450F65E76CF281A1D
        3BBD2A55AFC2D8EE7337B307F7E37018392BD46F4EE2EDEB2886C879E51929A6
        E44B0BFE331A589960D255C9C06AF6B80A60ED9309701969CB174274414BF8CF
        3A917F50F80663CBC792CDD378BFB7048C8DCB640262B6F8C21742A0998AFE33
        1AB3E26A2CD88CA66C454800FAC04F40158D44B09C1CEC0B8BE662DBB9405216
        CDF330B6F9881969C3D41E0DF69400FC2FAA9800272F86847C3A5521AFAE1E84
        A5BB0BD7219FB914FF11FF885ED282928C996FBFCE48CF63BAF678303D9EFA49
        1343E0ADB654C93AA3E92B838A5C3F221417AEA6EF66F7687311A5A0180C3BEF
        1E4DDA9BA9E0C643326EBC9245F5C9517062A0E36B41A17E9957EC11AC6CCF06
        4EB0A68D21DC4082625181B3A079204532744024B116CF4EF33B65AE928684CE
        E316EC0BD3C0A62DE5959A2EEAD7E127E5A33023BE5FB6979A4D1BC9D081C612
        FC8C940B0A366128787E32F7CD56E647E0BA351FCE8BD6961DC948912070001D
        833194809C6BF60CD507896434019F3A9414F6F7D0A1A3F195E6B9DF89484613
        08A24AF6107CEB51AB648DD8256B8036E70F93CC2FADD62E283A1FEEC1000000
        0049454E44AE426082}
      Header.Description = 
        'Indicar los par'#225'metros de ejecuci'#243'n del env'#237'o con frecuencia men' +
        'sual.'
      Header.Title = 'Tarea con frecuencia mensual'
      object Label10: TLabel
        Left = 28
        Top = 22
        Width = 123
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha de inicio del env'#237'o:'
        FocusControl = fmFechaInicio
      end
      object Label11: TLabel
        Left = 85
        Top = 52
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = '&Hora servidor:'
        FocusControl = fmHora
      end
      object Label12: TLabel
        Left = 33
        Top = 82
        Width = 118
        Height = 13
        Alignment = taRightJustify
        Caption = 'Aplicar en los sig. &meses:'
        FocusControl = chkMMeses
      end
      object Label6: TLabel
        Left = 200
        Top = 52
        Width = 97
        Height = 13
        Alignment = taRightJustify
        Caption = 'Formato de 24 horas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object fmFechaInicio: TZetaDBFecha
        Left = 157
        Top = 19
        Width = 112
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '07/Dec/16'
        Valor = 42711.000000000000000000
        DataField = 'CA_FECHA'
      end
      object fmHora: TZetaDBHora
        Left = 157
        Top = 49
        Width = 37
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 1
        Text = '    '
        Tope = 24
        Valor = '    '
        DataField = 'CA_HORA'
      end
      object chkMMeses: TcxCheckGroup
        Left = 157
        Top = 82
        Alignment = alCenterCenter
        Properties.Columns = 3
        Properties.Items = <
          item
            Caption = 'Enero'
            Tag = 1
          end
          item
            Caption = 'Febrero'
            Tag = 2
          end
          item
            Caption = 'Marzo'
            Tag = 3
          end
          item
            Caption = 'Abril'
            Tag = 4
          end
          item
            Caption = 'Mayo'
            Tag = 5
          end
          item
            Caption = 'Junio'
            Tag = 6
          end
          item
            Caption = 'Julio'
            Tag = 7
          end
          item
            Caption = 'Agosto'
            Tag = 8
          end
          item
            Caption = 'Septiembre'
            Tag = 9
          end
          item
            Caption = 'Octubre'
            Tag = 10
          end
          item
            Caption = 'Noviembre'
            Tag = 11
          end
          item
            Caption = 'Diciembre'
            Tag = 12
          end>
        Properties.ReadOnly = False
        Style.Edges = [bLeft, bTop, bRight, bBottom]
        TabOrder = 2
        Height = 96
        Width = 412
      end
      object chkMDiasMes: TcxCheckGroup
        Left = 157
        Top = 184
        Alignment = alCenterCenter
        Properties.Columns = 7
        Properties.Items = <
          item
            Caption = '1'
            Tag = 1
          end
          item
            Caption = '2'
            Tag = 2
          end
          item
            Caption = '3'
            Tag = 3
          end
          item
            Caption = '4'
            Tag = 4
          end
          item
            Caption = '5'
            Tag = 5
          end
          item
            Caption = '6'
            Tag = 6
          end
          item
            Caption = '7'
            Tag = 7
          end
          item
            Caption = '8'
            Tag = 8
          end
          item
            Caption = '9'
            Tag = 9
          end
          item
            Caption = '10'
            Tag = 10
          end
          item
            Caption = '11'
            Tag = 11
          end
          item
            Caption = '12'
            Tag = 12
          end
          item
            Caption = '13'
            Tag = 13
          end
          item
            Caption = '14'
            Tag = 14
          end
          item
            Caption = '15'
            Tag = 15
          end
          item
            Caption = '16'
            Tag = 16
          end
          item
            Caption = '17'
            Tag = 17
          end
          item
            Caption = '18'
            Tag = 18
          end
          item
            Caption = '19'
            Tag = 19
          end
          item
            Caption = '20'
            Tag = 20
          end
          item
            Caption = '21'
            Tag = 21
          end
          item
            Caption = '22'
            Tag = 22
          end
          item
            Caption = '23'
            Tag = 23
          end
          item
            Caption = '24'
            Tag = 24
          end
          item
            Caption = '25'
            Tag = 25
          end
          item
            Caption = '26'
            Tag = 26
          end
          item
            Caption = '27'
            Tag = 27
          end
          item
            Caption = '28'
            Tag = 28
          end
          item
            Caption = '29'
            Tag = 29
          end
          item
            Caption = '30'
            Tag = 30
          end
          item
            Caption = '31'
            Tag = 31
          end
          item
            Caption = #218'ltimo'
          end>
        Properties.ReadOnly = False
        Style.Edges = [bLeft, bTop, bRight, bBottom]
        TabOrder = 4
        Height = 96
        Width = 412
      end
      object chkMNumero: TcxCheckGroup
        Left = 157
        Top = 293
        Alignment = alCenterCenter
        Enabled = False
        ParentBackground = False
        Properties.Items = <
          item
            Caption = 'Primer'
          end
          item
            Caption = 'Segundo'
            Tag = 1
          end
          item
            Caption = 'Tercer'
            Tag = 2
          end
          item
            Caption = 'Cuarto'
            Tag = 3
          end
          item
            Caption = #218'ltimo'
            Tag = 4
          end>
        Properties.ReadOnly = False
        Style.Edges = [bLeft, bTop, bRight, bBottom]
        TabOrder = 6
        Height = 96
        Width = 112
      end
      object chkMDias: TcxCheckGroup
        Left = 316
        Top = 293
        Alignment = alCenterCenter
        Enabled = False
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = 'Domingo'
            Tag = 1
          end
          item
            Caption = 'Lunes'
            Tag = 2
          end
          item
            Caption = 'Martes'
            Tag = 3
          end
          item
            Caption = 'Mi'#233'rcoles'
            Tag = 4
          end
          item
            Caption = 'Jueves'
            Tag = 5
          end
          item
            Caption = 'Viernes'
            Tag = 6
          end
          item
            Caption = 'S'#225'bado'
            Tag = 7
          end>
        Properties.ReadOnly = False
        Style.Edges = [bLeft, bTop, bRight, bBottom]
        TabOrder = 7
        Height = 96
        Width = 253
      end
      object rDiasMes: TcxRadioButton
        Left = 70
        Top = 192
        Width = 81
        Height = 17
        Caption = 'D'#237'as del mes:'
        Checked = True
        TabOrder = 3
        TabStop = True
        OnClick = rDiasMesClick
        LookAndFeel.NativeStyle = True
        Transparent = True
      end
      object rDiasSemana: TcxRadioButton
        Left = 43
        Top = 293
        Width = 108
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'D'#237'as por semana:'
        Color = clBtnFace
        ParentBiDiMode = False
        ParentColor = False
        TabOrder = 5
        OnClick = rDiasSemanaClick
        ParentBackground = False
        Transparent = True
      end
    end
    object pSemanal: TdxWizardControlPage
      Header.AssignedValues = [wchvGlyph]
      Header.Glyph.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
        6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
        7249000003CF494441545847B5963F68137114C76F70C8D0A1430707073B0876
        7013914E824B06070727117470D04D5CB248AEA2B56A6D93264DD2FB97CBE572
        97FBD3186A5B8B58A8884B2705B577AD60411011840E0A452AC6F7E25DFCE5EE
        97988BF5C187DF71F77BDFF7EEDDFBFD7EC7A0B19A3BC4569DC3FB86F5E66053
        B817634D672C69BA7BACE536F693A4E54E7A21BA5BD272765B4E9008386E9342
        FD825A5E88EED672C0446A5B87F05ED2DC9C20C5FAA519E06FE64F4E9ACE8A77
        8B618D8D93A450BF7872DDCD9FDCEC03D38D6343266DB74A0AF58B17A2BB41E0
        4F34E77F057477BC10DD8DB59C4B507E079B6FDF30DD77B0BAAE7821FECD1A8D
        C67F8561E5ED186BB867B11291B19DE35E9EFD5BD2DAACD3BE61377E37AC33C6
        AEAD1D78707392F11172A5F5ECFDDC77B86EA4C6D33FE7A6B92F1541BF4ECE09
        D25A05BDE2053F17145AB49731994610295B5C87B16D2E49E40460A95EA00969
        821E87319400546457961E0EC175C8078994007478019DCAC5DA113E2D7C5079
        3D6194EC13425E7D0AF743C17DD213991FF029129A581D56B9CA1DA55056E17E
        B4047069DD365E0E88423DC6A7B8CF64807EA870EA0D18B109DDD7B480210CF7
        1A3A94382D4D0AF5CBD4ADA9862E19C30C2E252C2DBCA1DC0DB6BE3D98139FC6
        66BD2EA7313755D8A9F05AC290AA71E8898B50EA85A95BD3D4B948794EE5BCC5
        D89BF1F2F2199A1022CF16D7E1FBC6E0BAADC974B13A9A9998F9817382E4EECF
        EE32F866B86D36D775276CE71426004E8CCA6B77A6A17CA450F65E76CF281A1D
        3BBD2A55AFC2D8EE7337B307F7E37018392BD46F4EE2EDEB2886C879E51929A6
        E44B0BFE331A589960D255C9C06AF6B80A60ED9309701969CB174274414BF8CF
        3A917F50F80663CBC792CDD378BFB7048C8DCB640262B6F8C21742A0998AFE33
        1AB3E26A2CD88CA66C454800FAC04F40158D44B09C1CEC0B8BE662DBB9405216
        CDF330B6F9881969C3D41E0DF69400FC2FAA9800272F86847C3A5521AFAE1E84
        A5BB0BD7219FB914FF11FF885ED282928C996FBFCE48CF63BAF678303D9EFA49
        1343E0ADB654C93AA3E92B838A5C3F221417AEA6EF66F7687311A5A0180C3BEF
        1E4DDA9BA9E0C643326EBC9245F5C9517062A0E36B41A17E9957EC11AC6CCF06
        4EB0A68D21DC4082625181B3A079204532744024B116CF4EF33B65AE928684CE
        E316EC0BD3C0A62DE5959A2EEAD7E127E5A33023BE5FB6979A4D1BC9D081C612
        FC8C940B0A366128787E32F7CD56E647E0BA351FCE8BD6961DC948912070001D
        833194809C6BF60CD507896434019F3A9414F6F7D0A1A3F195E6B9DF89484613
        08A24AF6107CEB51AB648DD8256B8036E70F93CC2FADD62E283A1FEEC1000000
        0049454E44AE426082}
      Header.Description = 
        'Indicar los par'#225'metros de ejecuci'#243'n del env'#237'o con frecuencia sem' +
        'anal.'
      Header.Title = 'Tarea con frecuencia semanal'
      object lfsFechaInicio: TLabel
        Left = 99
        Top = 103
        Width = 123
        Height = 13
        Caption = '&Fecha de inicio del env'#237'o:'
        FocusControl = fsFechaInicio
      end
      object lfsHora: TLabel
        Left = 156
        Top = 133
        Width = 66
        Height = 13
        Caption = '&Hora servidor:'
        FocusControl = fsHora
      end
      object lfsRepetirse: TLabel
        Left = 147
        Top = 162
        Width = 75
        Height = 13
        Caption = '&Repetirse cada:'
        FocusControl = fsRepeticion
      end
      object Label7: TLabel
        Left = 90
        Top = 188
        Width = 134
        Height = 13
        Caption = 'Aplicar a los siguientes &d'#237'as:'
        FocusControl = chksFrecuenciaDias
      end
      object Label9: TLabel
        Left = 274
        Top = 162
        Width = 53
        Height = 13
        Caption = 'semanas(s)'
      end
      object Label41: TLabel
        Left = 273
        Top = 133
        Width = 97
        Height = 13
        Alignment = taRightJustify
        Caption = 'Formato de 24 horas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object fsFechaInicio: TZetaDBFecha
        Left = 228
        Top = 100
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '07/Dec/16'
        Valor = 42711.000000000000000000
        DataField = 'CA_FECHA'
      end
      object fsHora: TZetaDBHora
        Left = 228
        Top = 130
        Width = 39
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 1
        Text = '    '
        Tope = 24
        Valor = '    '
        DataField = 'CA_HORA'
      end
      object fsRepeticion: TZetaDBNumero
        Left = 228
        Top = 159
        Width = 40
        Height = 21
        Mascara = mnDias
        MaxLength = 2
        TabOrder = 2
        Text = '0'
        DataField = 'CA_RECUR'
      end
      object chksFrecuenciaDias: TcxCheckGroup
        Left = 228
        Top = 189
        Alignment = alCenterCenter
        Properties.Columns = 3
        Properties.Items = <
          item
            Caption = 'Domingo'
            Tag = 1
          end
          item
            Caption = 'Lunes'
            Tag = 2
          end
          item
            Caption = 'Martes'
            Tag = 3
          end
          item
            Caption = 'Mi'#233'rcoles'
            Tag = 4
          end
          item
            Caption = 'Jueves'
            Tag = 5
          end
          item
            Caption = 'Viernes'
            Tag = 6
          end
          item
            Caption = 'S'#225'bado'
            Tag = 7
          end>
        Properties.ReadOnly = False
        Style.Edges = [bLeft, bTop, bRight, bBottom]
        TabOrder = 3
        Height = 96
        Width = 291
      end
    end
    object pHora: TdxWizardControlPage
      Header.AssignedValues = [wchvGlyph]
      Header.Glyph.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
        6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
        7249000003CF494441545847B5963F68137114C76F70C8D0A1430707073B0876
        7013914E824B06070727117470D04D5CB248AEA2B56A6D93264DD2FB97CBE572
        97FBD3186A5B8B58A8884B2705B577AD60411011840E0A452AC6F7E25DFCE5EE
        97988BF5C187DF71F77BDFF7EEDDFBFD7EC7A0B19A3BC4569DC3FB86F5E66053
        B817634D672C69BA7BACE536F693A4E54E7A21BA5BD272765B4E9008386E9342
        FD825A5E88EED672C0446A5B87F05ED2DC9C20C5FAA519E06FE64F4E9ACE8A77
        8B618D8D93A450BF7872DDCD9FDCEC03D38D6343266DB74A0AF58B17A2BB41E0
        4F34E77F057477BC10DD8DB59C4B507E079B6FDF30DD77B0BAAE7821FECD1A8D
        C67F8561E5ED186BB867B11291B19DE35E9EFD5BD2DAACD3BE61377E37AC33C6
        AEAD1D78707392F11172A5F5ECFDDC77B86EA4C6D33FE7A6B92F1541BF4ECE09
        D25A05BDE2053F17145AB49731994610295B5C87B16D2E49E40460A95EA00969
        821E87319400546457961E0EC175C8078994007478019DCAC5DA113E2D7C5079
        3D6194EC13425E7D0AF743C17DD213991FF029129A581D56B9CA1DA55056E17E
        B4047069DD365E0E88423DC6A7B8CF64807EA870EA0D18B109DDD7B480210CF7
        1A3A94382D4D0AF5CBD4ADA9862E19C30C2E252C2DBCA1DC0DB6BE3D98139FC6
        66BD2EA7313755D8A9F05AC290AA71E8898B50EA85A95BD3D4B948794EE5BCC5
        D89BF1F2F2199A1022CF16D7E1FBC6E0BAADC974B13A9A9998F9817382E4EECF
        EE32F866B86D36D775276CE71426004E8CCA6B77A6A17CA450F65E76CF281A1D
        3BBD2A55AFC2D8EE7337B307F7E37018392BD46F4EE2EDEB2886C879E51929A6
        E44B0BFE331A589960D255C9C06AF6B80A60ED9309701969CB174274414BF8CF
        3A917F50F80663CBC792CDD378BFB7048C8DCB640262B6F8C21742A0998AFE33
        1AB3E26A2CD88CA66C454800FAC04F40158D44B09C1CEC0B8BE662DBB9405216
        CDF330B6F9881969C3D41E0DF69400FC2FAA9800272F86847C3A5521AFAE1E84
        A5BB0BD7219FB914FF11FF885ED282928C996FBFCE48CF63BAF678303D9EFA49
        1343E0ADB654C93AA3E92B838A5C3F221417AEA6EF66F7687311A5A0180C3BEF
        1E4DDA9BA9E0C643326EBC9245F5C9517062A0E36B41A17E9957EC11AC6CCF06
        4EB0A68D21DC4082625181B3A079204532744024B116CF4EF33B65AE928684CE
        E316EC0BD3C0A62DE5959A2EEAD7E127E5A33023BE5FB6979A4D1BC9D081C612
        FC8C940B0A366128787E32F7CD56E647E0BA351FCE8BD6961DC948912070001D
        833194809C6BF60CD507896434019F3A9414F6F7D0A1A3F195E6B9DF89484613
        08A24AF6107CEB51AB648DD8256B8036E70F93CC2FADD62E283A1FEEC1000000
        0049454E44AE426082}
      Header.Description = 
        'Indicar los par'#225'metros de ejecuci'#243'n del env'#237'o con frecuencia por' +
        ' hora.'
      Header.Title = 'Tarea con frecuencia por hora'
      object Label14: TLabel
        Left = 145
        Top = 165
        Width = 123
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha de inicio del env'#237'o:'
        FocusControl = fhFechaInicio
      end
      object Label17: TLabel
        Left = 202
        Top = 192
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = '&Hora servidor:'
        FocusControl = fhHora
      end
      object Label18: TLabel
        Left = 193
        Top = 222
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = '&Repetirse cada:'
        FocusControl = fhRepeticion
      end
      object Label19: TLabel
        Left = 318
        Top = 222
        Width = 32
        Height = 13
        Caption = 'hora(s)'
      end
      object Label23: TLabel
        Left = 317
        Top = 192
        Width = 97
        Height = 13
        Alignment = taRightJustify
        Caption = 'Formato de 24 horas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object fhFechaInicio: TZetaDBFecha
        Left = 274
        Top = 160
        Width = 111
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '07/Dec/16'
        Valor = 42711.000000000000000000
        DataField = 'CA_FECHA'
      end
      object fhHora: TZetaDBHora
        Left = 274
        Top = 190
        Width = 37
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 1
        Text = '    '
        Tope = 24
        Valor = '    '
        DataField = 'CA_HORA'
      end
      object fhRepeticion: TZetaDBNumero
        Left = 274
        Top = 219
        Width = 38
        Height = 21
        Mascara = mnDias
        MaxLength = 1
        TabOrder = 2
        Text = '0'
        DataField = 'CA_RECUR'
      end
    end
    object pEspecial: TdxWizardControlPage
      Header.AssignedValues = [wchvGlyph]
      Header.Glyph.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
        6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
        7249000003CF494441545847B5963F68137114C76F70C8D0A1430707073B0876
        7013914E824B06070727117470D04D5CB248AEA2B56A6D93264DD2FB97CBE572
        97FBD3186A5B8B58A8884B2705B577AD60411011840E0A452AC6F7E25DFCE5EE
        97988BF5C187DF71F77BDFF7EEDDFBFD7EC7A0B19A3BC4569DC3FB86F5E66053
        B817634D672C69BA7BACE536F693A4E54E7A21BA5BD272765B4E9008386E9342
        FD825A5E88EED672C0446A5B87F05ED2DC9C20C5FAA519E06FE64F4E9ACE8A77
        8B618D8D93A450BF7872DDCD9FDCEC03D38D6343266DB74A0AF58B17A2BB41E0
        4F34E77F057477BC10DD8DB59C4B507E079B6FDF30DD77B0BAAE7821FECD1A8D
        C67F8561E5ED186BB867B11291B19DE35E9EFD5BD2DAACD3BE61377E37AC33C6
        AEAD1D78707392F11172A5F5ECFDDC77B86EA4C6D33FE7A6B92F1541BF4ECE09
        D25A05BDE2053F17145AB49731994610295B5C87B16D2E49E40460A95EA00969
        821E87319400546457961E0EC175C8078994007478019DCAC5DA113E2D7C5079
        3D6194EC13425E7D0AF743C17DD213991FF029129A581D56B9CA1DA55056E17E
        B4047069DD365E0E88423DC6A7B8CF64807EA870EA0D18B109DDD7B480210CF7
        1A3A94382D4D0AF5CBD4ADA9862E19C30C2E252C2DBCA1DC0DB6BE3D98139FC6
        66BD2EA7313755D8A9F05AC290AA71E8898B50EA85A95BD3D4B948794EE5BCC5
        D89BF1F2F2199A1022CF16D7E1FBC6E0BAADC974B13A9A9998F9817382E4EECF
        EE32F866B86D36D775276CE71426004E8CCA6B77A6A17CA450F65E76CF281A1D
        3BBD2A55AFC2D8EE7337B307F7E37018392BD46F4EE2EDEB2886C879E51929A6
        E44B0BFE331A589960D255C9C06AF6B80A60ED9309701969CB174274414BF8CF
        3A917F50F80663CBC792CDD378BFB7048C8DCB640262B6F8C21742A0998AFE33
        1AB3E26A2CD88CA66C454800FAC04F40158D44B09C1CEC0B8BE662DBB9405216
        CDF330B6F9881969C3D41E0DF69400FC2FAA9800272F86847C3A5521AFAE1E84
        A5BB0BD7219FB914FF11FF885ED282928C996FBFCE48CF63BAF678303D9EFA49
        1343E0ADB654C93AA3E92B838A5C3F221417AEA6EF66F7687311A5A0180C3BEF
        1E4DDA9BA9E0C643326EBC9245F5C9517062A0E36B41A17E9957EC11AC6CCF06
        4EB0A68D21DC4082625181B3A079204532744024B116CF4EF33B65AE928684CE
        E316EC0BD3C0A62DE5959A2EEAD7E127E5A33023BE5FB6979A4D1BC9D081C612
        FC8C940B0A366128787E32F7CD56E647E0BA351FCE8BD6961DC948912070001D
        833194809C6BF60CD507896434019F3A9414F6F7D0A1A3F195E6B9DF89484613
        08A24AF6107CEB51AB648DD8256B8036E70F93CC2FADD62E283A1FEEC1000000
        0049454E44AE426082}
      Header.Description = 
        'Indicar los par'#225'metros de ejecuci'#243'n del env'#237'o con frecuencia esp' +
        'ecial (se aplica una sola vez).'
      Header.Title = 'Tarea con frecuencia especial'
      object Label20: TLabel
        Left = 148
        Top = 161
        Width = 123
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha de inicio del env'#237'o:'
        FocusControl = feFechaInicio
      end
      object Label21: TLabel
        Left = 205
        Top = 191
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = '&Hora servidor:'
        FocusControl = feHora
      end
      object Label24: TLabel
        Left = 317
        Top = 191
        Width = 97
        Height = 13
        Alignment = taRightJustify
        Caption = 'Formato de 24 horas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object feFechaInicio: TZetaDBFecha
        Left = 274
        Top = 158
        Width = 110
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '07/Dec/16'
        Valor = 42711.000000000000000000
        DataField = 'CA_FECHA'
      end
      object feHora: TZetaDBHora
        Left = 274
        Top = 188
        Width = 37
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 1
        Text = '    '
        Tope = 24
        Valor = '    '
        DataField = 'CA_HORA'
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 472
    Top = 90
  end
end
