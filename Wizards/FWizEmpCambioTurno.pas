unit FWizEmpCambioTurno;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ZetaEdit, StdCtrls, Buttons,
  ExtCtrls, Mask, ZetaFecha,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, cxContainer, cxEdit,
  ImgList, cxImage, cxButtons, ZetaKeyLookup_DevEx,
  cxTextEdit, cxMemo, ZCXBaseWizardFiltro, ZetaCXWizard, cxRadioGroup,
  dxGDIPlusClasses, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl;

type
  TWizEmpCambioTurno = class(TBaseCXWizardFiltro)
    GBCambio: TGroupBox;
    LblFecTur: TLabel;
    FechaTurno: TZetaFecha;
    TurnoAnterior: TZetaKeyLookup_DevEx;
    GBNuevoTurno: TGroupBox;
    LblFechaMov: TLabel;
    LblNuevoTurno: TLabel;
    FechaMov: TZetaFecha;
    TurnoNuevo: TZetaKeyLookup_DevEx;
    sDescripcionLBL: TLabel;
    Descripcion: TEdit;
    sObservaLBL: TLabel;
    Observaciones: TcxMemo;
    rbEmpTodos: TRadioButton;
    rbEmpTurno: TRadioButton;
    bbShowCalAnterior: TcxButton;
    bbShowCalNuevo: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure bbShowCalendarioClick(Sender: TObject);
    procedure TurnoValidKey(Sender: TObject);
    procedure rbEmpTurnoClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    function UsaTurnoEspecifico: Boolean;
    {$IFDEF SUPERVISORES}
    procedure SetAnimacion( const lEnabled: Boolean );
    {$ENDIF}
    procedure HabilitaBtnCalendario;
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
    procedure TerminarWizard; override;
  public
    { Public declarations }
  end;

var
  WizEmpCambioTurno: TWizEmpCambioTurno;

implementation

uses  {$ifdef SUPERVISORES} dSuper, {$ELSE} dRecursos, {$ENDIF} dCliente, dCatalogos,
     FTressShell, ZBaseSelectGrid_DevEx, FEmpleadoGridSelect_DevEx,
     ZetaCommonTools, ZetaCommonClasses, ZetaTipoEntidad,DGlobal,ZGlobalTress;

{$R *.DFM}

const
     K_TODOS_EMPLEADOS  = 0;
     K_TURNO_ESPECIFICO = 1;
     K_SHOW_TURNO_NUEVO = 1;

procedure TWizEmpCambioTurno.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := TurnoNuevo;

     with dmCatalogos do
     begin
          TurnoAnterior.LookupDataset := cdsTurnos;
          TurnoNuevo.LookupDataset := cdsTurnos;
     end;
     Descripcion.MaxLength := K_ANCHO_OBSERVACIONES;

     HelpContext := H_Cambio_Masivo_Turnos;

     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;

end;

procedure TWizEmpCambioTurno.FormShow(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          FechaMov.Valor := FechaDefault;
          FechaTurno.Valor := FechaDefault;
     end;

     dmCatalogos.cdsTurnos.Conectar;

     HabilitaBtnCalendario;

     rbEmpTodos.Checked := TRUE;
     rbEmpTurnoClick( self );
     {$IFDEF SUPERVISORES}
     		    RbLista.Checked := TRUE;
     {$ENDIF}
end;

procedure TWizEmpCambioTurno.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddBoolean( 'TurnoEspecifico', UsaTurnoEspecifico );
          AddString( 'TurnoAnterior', TurnoAnterior.Llave );
          AddDate( 'FechaTurno', FechaTurno.Valor );
          AddString( 'TurnoNuevo', TurnoNuevo.Llave );
          AddDate( 'FechaMov', FechaMov.Valor );
          AddString( 'Descripcion', Descripcion.Text );
          AddMemo( 'Observaciones', Observaciones.Text );
          {$IFDEF SUPERVISORES}
              AddBoolean( 'EsSupervisores', True );
          {$ELSE}
              AddBoolean( 'EsSupervisores', False );
          {$ENDIF}
     end;

     with Descripciones do
     begin
          if UsaTurnoEspecifico then
          begin
               AddString( 'Turno anterior', GetDescripLlave( TurnoAnterior ) );
               AddDate( 'Vigente al', FechaTurno.Valor );
          end
          else
          begin
               AddString( 'Turno anterior', 'Todos' );
          end;
          AddString( 'Turno nuevo', GetDescripLlave( TurnoNuevo ) );
          AddDate( 'Vigente a partir del', FechaMov.Valor );
          AddString( 'Descripcion', Descripcion.Text );
          AddMemo( 'Observaciones', Observaciones.Text );
     end;
end;

procedure TWizEmpCambioTurno.CargaListaVerificacion;
begin
    {$IFDEF SUPERVISORES}
      dmSuper.CambioMasivoTurnoGetLista( ParameterList );
     {$ELSE}
      dmRecursos.CambioMasivoTurnoGetLista( ParameterList );
      {$ENDIF}
end;

function TWizEmpCambioTurno.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( {$ifdef SUPERVISORES}dmSuper.cdsDataset,{$ELSE}dmRecursos.cdsDataset, {$ENDIF} TEmpleadoGridSelect_DevEx );
end;

function TWizEmpCambioTurno.EjecutarWizard: Boolean;
begin
     {$ifdef SUPERVISORES}
     Result := dmSuper.CambioMasivoTurno( ParameterList, Verificacion, SetAnimacion );
     {$ELSE}
     Result := dmRecursos.CambioMasivoTurno( ParameterList, Verificacion );
     {$ENDIF}

     if Result then
        TressShell.SetDataChange( [ enKardex ] );
end;

procedure TWizEmpCambioTurno.TerminarWizard;
begin
     inherited;
     Close;
end;

function TWizEmpCambioTurno.UsaTurnoEspecifico: Boolean;
begin
     Result := rbEmpTurno.Checked;
end;

{$IFDEF SUPERVISORES}
procedure TWizEmpCambioTurno.SetAnimacion(const lEnabled: Boolean);
begin
     Application.ProcessMessages;
end;
{$ENDIF}

procedure TWizEmpCambioTurno.HabilitaBtnCalendario;
begin
     bbShowCalAnterior.Enabled := UsaTurnoEspecifico and strLleno( TurnoAnterior.Llave );
     bbShowCalNuevo.Enabled := strLleno( TurnoNuevo.Llave );
end;

procedure TWizEmpCambioTurno.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const
K_CODIGO_INVALIDO = 'C�digo Inactivo!!!';
begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if CanMove then
               begin
                    if strVacio( TurnoNuevo.Llave ) then
                       CanMove := self.Error( 'Falta Especificar El Turno Nuevo', TurnoNuevo )
                    else if UsaTurnoEspecifico then
                    begin
                         if strVacio( TurnoAnterior.Llave ) then
                            CanMove := self.Error( 'Falta Especificar El Turno Espec�fico a Modificar', TurnoAnterior )
                         else if ( TurnoAnterior.Llave = TurnoNuevo.Llave ) then
                            CanMove := self.Error( 'Turno Nuevo Debe Ser Diferente Al Anterior', TurnoNuevo );
                    end;
                    //Validaci�n de catalogos inactivos   @DACP
                    if  Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS ) then
                    begin
                         if TurnoNuevo.Descripcion = K_CODIGO_INVALIDO then
                            CanMove := FALSE;
                         if TurnoAnterior.Descripcion = K_CODIGO_INVALIDO then
                            CanMove := FALSE;
                    end;
               end;
          end;
     end;
end;

procedure TWizEmpCambioTurno.bbShowCalendarioClick(Sender: TObject);
var
   sTurno: String;
begin
     inherited;
     if ( TControl( Sender ).Tag = K_SHOW_TURNO_NUEVO ) then
        sTurno := TurnoNuevo.Llave
     else
         sTurno := TurnoAnterior.Llave;
     if strLleno( sTurno ) then
        dmCatalogos.ShowCalendarioRitmo( sTurno );
end;

procedure TWizEmpCambioTurno.TurnoValidKey(Sender: TObject);
begin
     inherited;
     HabilitaBtnCalendario;
end;

procedure TWizEmpCambioTurno.rbEmpTurnoClick(Sender: TObject);
var
   lEnabled : Boolean;
begin
     inherited;
     lEnabled := UsaTurnoEspecifico;
     TurnoAnterior.Enabled := lEnabled;
     LblFecTur.Enabled := lEnabled;
     FechaTurno.Enabled := lEnabled;
     HabilitaBtnCalendario;
end;

procedure TWizEmpCambioTurno.SetEditarSoloActivos;
begin
     TurnoNuevo.EditarSoloActivos := TRUE;
     TurnoAnterior.EditarSoloActivos := TRUE;
end;

end.
