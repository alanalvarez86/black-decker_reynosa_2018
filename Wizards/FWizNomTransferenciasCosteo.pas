unit FWizNomTransferenciasCosteo;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls, Buttons,
     ZBaseWizardFiltro,
     ZetaKeyLookup,
     ZetaEdit,
     ZetaDBTextBox,
     ZetaWizard,
     ZetaCommonLists, ZetaKeyCombo, ZetaNumero, Mask, ZetaFecha;

type
  TWizNomTransferenciasCosteo = class(TBaseWizardFiltro)
    lbOriginal: TLabel;
    CC_Original: TZetaKeyLookup;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    HorasLbl: TLabel;
    lbDestino: TLabel;
    MotivoLbl: TLabel;
    Label6: TLabel;
    TipoAutLbl: TLabel;
    AU_FECHA: TZetaFecha;
    TR_HORAS: TZetaNumero;
    CC_DESTINO: TZetaKeyLookup;
    TR_MOTIVO: TZetaKeyLookup;
    TR_TEXTO: TZetaEdit;
    TR_TIPO: TZetaKeyCombo;
    cbDuplicados: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FNivel: string;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
    procedure CargaListaVerificacion;override;
    function Verificar: Boolean;override;
  public
    { Public declarations }
  end;

var
  WizNomTransferenciasCosteo: TWizNomTransferenciasCosteo;

implementation

uses DProcesos,
     DCliente,
     DGlobal,
     dTablas,
     ZGlobalTress,
     ZAccesosMgr,
     ZAccesosTress,
     ZBaseSelectGrid,
     ZetaCommonClasses,
     ZetaCommonTools,
     FTressShell,
     FNomTransferenciasCosteoGridSelect ;

{$R *.DFM}

procedure TWizNomTransferenciasCosteo.FormCreate(Sender: TObject);

begin
     inherited;
     ParametrosControl := AU_FECHA;
     HelpContext:= H_Agregar_Transferencias;
     with Global do
     begin
          FNivel := Global.NombreCosteo;
          Caption := 'Transferencias de Empleados por ' + FNivel;
          lbDestino.Caption := 'Transferir hacia:';
          lbOriginal.Caption := FNivel + ' Original:';
          cbDuplicados.Caption := 'Validar que no exista transferencia anterior a mismo(a) ' + FNivel;
     end;

     AU_FECHA.Valor := dmCliente.GetDatosPeriodoActivo.Inicio;
     TR_TIPO.ItemIndex := ord( tcOrdinarias );
     TR_TEXTO.MaxLength := K_ANCHO_DESCRIPCION;

     TR_MOTIVO.LookupDataset := dmTablas.cdsMotivoTransfer;
     CC_DESTINO.LookupDataset := dmTablas.GetDataSetTransferencia;
     CC_ORIGINAL.LookupDataset := dmTablas.GetDataSetTransferencia(FALSE);
end;

procedure TWizNomTransferenciasCosteo.CargaParametros;
begin

     with Descripciones do
     begin
          Clear;
          AddString ( 'Fecha Inicial' , FechaCorta( AU_FECHA.Valor ) );
          AddString ( 'Duraci�n', Format( ' %s horas', [ FloattoStr( TR_HORAS.Valor )] ) );
          AddString ( Format( '%s Destino',  [ FNivel] ),  CC_DESTINO.LLave ) ;
          AddString ( Format( '%s Original ',  [ FNivel] ), CC_ORIGINAL.LLave ) ;
          AddString ( 'Motivo' ,  TR_MOTIVO.Llave );
          AddString ( 'Observaciones' , TR_TEXTO.Text );
          AddString ( 'Tipo de Transferencia' , ObtieneElemento( lfTipoHoraTransferenciaCosteo, TR_TIPO.ItemIndex ) );
          AddString ( 'Validar que no exista transferencia anterior a mismo(a) ' + FNivel , zBooltostr( cbDuplicados.Checked )  );
     end;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'FechaInicio', AU_FECHA.Valor );
          AddFloat( 'Horas', TR_HORAS.Valor );
          AddString( 'Motivo', TR_MOTIVO.Llave );
          AddString( 'Observa', TR_TEXTO.Text );
          AddString( 'CCDestino', CC_DESTINO.LLave );
          AddString( 'CCOriginal', CC_ORIGINAL.LLave );
          AddInteger ('Tipo', TR_TIPO.ItemIndex );
          AddBoolean( 'SoloUnaTransferencia', cbDuplicados.Checked );
          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
     end;
end;

procedure TWizNomTransferenciasCosteo.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if Adelante and EsPaginaActual( Parametros ) then
          begin
               if CC_DESTINO.LookupDataset = nil then
                  CanMove := Error( 'El Nivel de Organigrama para Costeo no ha sido definido.' + CR_LF +
                                    'No se podr� Agregar o Modificar las Transferencias.', Salir )
               else if ( AU_FECHA.Valor = NullDateTime ) then
                  CanMove := Error( 'La fecha no puede quedar vac�a', AU_FECHA )
               else if ( TR_HORAS.Valor = 0 ) then
                  CanMove := Error( 'El valor de horas no puede quedar en cero', TR_HORAS )
               else if StrVacio( CC_DESTINO.LLave ) then
                  with Global do
                       CanMove := Error( Format( '%s destino no puede quedar vac�o(a)', [Global.NombreCosteo] ), CC_DESTINO );
          end;
     end;//with
end;

function TWizNomTransferenciasCosteo.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.TransferenciasCosteo(ParameterList, Verificacion );
end;

procedure TWizNomTransferenciasCosteo.CargaListaVerificacion;
begin
     dmProcesos.TransferenciasCosteoGetLista( ParameterList );
end;

function TWizNomTransferenciasCosteo.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid.GridSelect( dmProcesos.cdsDataset, TNomTransferenciasCosteoGridSelect );
end;


procedure TWizNomTransferenciasCosteo.FormShow(Sender: TObject);
begin
     inherited;
     with dmTablas do
     begin
          cdsMotivoTransfer.Conectar;
          if CC_ORIGINAL.LookupDataset <> NIL then
             CC_ORIGINAL.LookupDataset.Conectar;
     end;

end;

end.
