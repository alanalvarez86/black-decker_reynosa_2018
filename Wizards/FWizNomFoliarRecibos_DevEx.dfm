inherited WizNomFoliarRecibos_DevEx: TWizNomFoliarRecibos_DevEx
  Left = 296
  Top = 132
  Caption = 'Foliar Recibos'
  ClientHeight = 432
  ClientWidth = 438
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 438
    Height = 432
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        ' Proceso que enumera los recibos o cheques de un grupo de emplea' +
        'dos.'
      Header.Title = 'Foliar recibos'
      inherited GroupBox1: TGroupBox
        Top = 81
        TabOrder = 1
        inherited PeriodoTipoLbl: TLabel
          Transparent = True
        end
        inherited Label4: TLabel
          Transparent = True
        end
        inherited Label2: TLabel
          Transparent = True
        end
        inherited Label3: TLabel
          Transparent = True
        end
        inherited PeriodoNumeroLBL: TLabel
          Transparent = True
        end
        inherited Label1: TLabel
          Transparent = True
        end
      end
      object Panel2: TPanel
        Left = 6
        Top = 36
        Width = 409
        Height = 41
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Label5: TLabel
          Left = 33
          Top = 14
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = '&Folio:'
          FocusControl = nFolio
          Transparent = True
        end
        object nFolio: TZetaKeyLookup_DevEx
          Left = 73
          Top = 10
          Width = 329
          Height = 21
          LookupDataset = dmCatalogos.cdsFolios
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 197
        Width = 416
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 416
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 348
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 14
        Top = 145
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        Top = 174
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Top = 262
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        Top = 142
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Top = 173
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
        Top = 12
      end
      inherited bAjusteISR: TcxButton
        Left = 380
        Top = 174
      end
    end
    object Parametros2: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        ' Complete los par'#225'metros para indicar la informaci'#243'n del proceso' +
        '.'
      Header.Title = ' Par'#225'metros'
      object Panel1: TPanel
        Left = 22
        Top = 37
        Width = 377
        Height = 222
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Label6: TLabel
          Left = 84
          Top = 17
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = 'Folio:'
          Transparent = True
        end
        object Label7: TLabel
          Left = 24
          Top = 38
          Width = 85
          Height = 13
          Alignment = taRightJustify
          Caption = 'F'#243'mula de Monto:'
          Transparent = True
        end
        object Label8: TLabel
          Left = 41
          Top = 115
          Width = 68
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ultima Corrida:'
          Transparent = True
        end
        object Label9: TLabel
          Left = 21
          Top = 94
          Width = 88
          Height = 13
          Alignment = taRightJustify
          Caption = 'Moneda a Repetir:'
          Transparent = True
        end
        object sFolio: TZetaTextBox
          Left = 111
          Top = 15
          Width = 250
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object sCorrida: TZetaTextBox
          Left = 111
          Top = 113
          Width = 250
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object rMoneda: TZetaTextBox
          Left = 111
          Top = 92
          Width = 121
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object sFormula: TZetaTextBox
          Left = 111
          Top = 36
          Width = 250
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label10: TLabel
          Left = 42
          Top = 191
          Width = 65
          Height = 13
          Alignment = taRightJustify
          Caption = '# &Folio Inicial:'
          FocusControl = nFolioInicial
          Transparent = True
        end
        object sOrden: TZetaTextBox
          Left = 111
          Top = 134
          Width = 250
          Height = 49
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label11: TLabel
          Left = 40
          Top = 136
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ordenamiento:'
          Transparent = True
        end
        object lCeros: TCheckBox
          Left = 46
          Top = 55
          Width = 78
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Incluir Ceros:'
          Enabled = False
          TabOrder = 0
        end
        object lRepite: TCheckBox
          Left = 13
          Top = 73
          Width = 111
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Recibos Repetidos:'
          Enabled = False
          TabOrder = 1
        end
        object nFolioInicial: TZetaNumero
          Left = 111
          Top = 187
          Width = 121
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 2
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
