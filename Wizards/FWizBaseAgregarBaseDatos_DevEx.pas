unit FWizBaseAgregarBaseDatos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, ZetaDialogo,
  FWizBasicoAgregarBaseDatos_DevEx, cxLookAndFeelPainters, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, cxTrackBar,
  ZetaNumero, ZetaCXWizard, ZetaEdit, Mask, cxTextEdit, cxMemo,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, StdCtrls, cxRadioGroup,
  dxCustomWizardControl, dxWizardControl;

type
  TWizBaseAgregarBaseDatos_DevEx = class(TWizBasicoAgregarBaseDatos_DevEx)

    NuevaBD1: TdxWizardControlPage;
    lblCantEmpSeleccion: TLabel;
    lblDocSeleccion: TLabel;
    tbEmpleados: TcxTrackBar;
    tbDocumentos: TcxTrackBar;
    lblBaseDatos: TLabel;
    lblTamanoSugerido: TLabel;
    lblLogTransacciones: TLabel;
    lblBaseDatosValor: TLabel;
    lblTamanoSugeridoValor: TLabel;
    lblLogTransaccionesValor: TLabel;
    procedure WizardAfterMove(Sender: TObject);
    procedure tbEmpleadosPropertiesChange(Sender: TObject);
    procedure tbDocumentosPropertiesChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    ParamTamanioDB,ParamLog:Double;
    tamanio,log:String;
    procedure CalcularTamanyos;
    procedure CreaParametroTamanio;
  end;

var
  WizBaseAgregarBaseDatos_DevEx: TWizBaseAgregarBaseDatos_DevEx;

implementation

uses ZcxWizardBasico, dSistema, ZetaCommonClasses;

{$R *.dfm}

procedure TWizBaseAgregarBaseDatos_DevEx.CalcularTamanyos;
Const K_2_MB = 2048;
      K_1024 = 1024;
      K_KB_DOC = 100;
begin
      ParamTamanioDB := (((tbEmpleados.Position*K_2_MB)/K_1024) + ((tbEmpleados.Position*tbDocumentos.Position*K_KB_DOC)/K_1024));
      ParamLog:= ParamTamanioDB * 0.25;
      {***DevEx(by am):Este codigo ya no es necesario, se paso al metodo CreaParametroTamanio}
      {
        if ParamTamanioDB > K_1024 then
      begin
           txtTamanyo.Valor := txtTamanyo.Valor/K_1024;
           lblMB1.Caption := 'GB';
      end;

      if ParamLog > K_1024 then
      begin
           txtLog.Valor := txtLog.Valor/K_1024;
           lblMB2.Caption := 'GB';
      end;
      }
      lblCantEmpSeleccion.Caption := IntToStr(tbEmpleados.Position);
      lblDocSeleccion.Caption := IntToStr(tbDocumentos.Position);
      {lblBaseDatos.Caption := 'Base de datos: ' + Trim (txtBaseDatos.Text);
      CreaParametroTamanio; //DevEx(by am):Crear la cadena
      lblTamanoSugerido.Caption := 'Tama�o sugerido: ' + tamanio;
      lblLogTransacciones.Caption := 'Log de transacciones: ' + log;}
      lblBaseDatosValor.Caption := Trim (txtBaseDatos.Text);
      CreaParametroTamanio; //DevEx(by am):Crear la cadena
      lblTamanoSugeridoValor.Caption := tamanio;
      lblLogTransaccionesValor.Caption := log;
end;

procedure TWizBaseAgregarBaseDatos_DevEx.WizardAfterMove(Sender: TObject);
begin
  inherited;
  {***DevEx(by am): Conviene mas hacerlo en las implementaciones pues no siempre se ejecuta solo esta linea***}
  {if WizardControl.ActivePage = NuevaBD1 then
     begin
          CalcularTamanyos;
     end; }
end;

procedure TWizBaseAgregarBaseDatos_DevEx.tbEmpleadosPropertiesChange(Sender: TObject);
begin
  inherited;
     lblCantEmpSeleccion.Caption := IntToStr(tbEmpleados.Position);
     CalcularTamanyos;
end;

procedure TWizBaseAgregarBaseDatos_DevEx.tbDocumentosPropertiesChange(Sender: TObject);
begin
  inherited;
     lblDocSeleccion.Caption := IntToStr(tbDocumentos.Position);
     CalcularTamanyos;
end;

{***DevEx(by am): Agregada para utilizar el dato de los tama�os de las bd de forma generica en las implementaciones***}
procedure TWizBaseAgregarBaseDatos_DevEx.CreaParametroTamanio;
Const K_2_MB = 2048;
      K_1024 = 1024;
      K_KB_DOC = 100;
      K_GB='GB';
      K_MB='MB';
      K_ESPACIO=' ';
begin
      if ParamTamanioDB > K_1024 then
      begin
          tamanio:= FormatFloat ('0.00', (ParamTamanioDB/K_1024));
          tamanio:= tamanio+K_ESPACIO+K_GB;
      end
      else
      begin
        tamanio:= FormatFloat ('0.00',ParamTamanioDB);
        tamanio:= tamanio+K_ESPACIO+K_MB;
      end;

      if ParamLog > K_1024 then
      begin
           log := FormatFloat('0.00',(ParamLog/K_1024));
           log := log+K_ESPACIO+ K_GB;
      end
      else
      begin
          log := FormatFloat('0.00',ParamLog);
          log := log+K_ESPACIO+ K_MB;
      end;
end;

procedure TWizBaseAgregarBaseDatos_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  {***DevEx(by am):Inicializacion de variables globales***}
  ParamTamanioDB:=0;
  ParamLog:=0;
  tamanio:='';
  log:='';
  {***}
end;

end.
