inherited EmpSalarioIntegradoGridSelect_DevEx: TEmpSalarioIntegradoGridSelect_DevEx
  Left = 660
  Top = 167
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Empleados Deseados'
  ClientWidth = 592
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 592
    inherited OK_DevEx: TcxButton
      Left = 428
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 507
    end
  end
  inherited PanelSuperior: TPanel
    Width = 592
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 592
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsCustomize.ColumnHorzSizing = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object CB_SAL_INT: TcxGridDBColumn
        Caption = 'Base Actual'
        DataBinding.FieldName = 'CB_SAL_INT'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object NUEVO_INT: TcxGridDBColumn
        Caption = 'Base Nueva'
        DataBinding.FieldName = 'NUEVO_INT'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object DIFERENCIA: TcxGridDBColumn
        Caption = 'Diferencia'
        DataBinding.FieldName = 'DIFERENCIA'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PORCENTAJE: TcxGridDBColumn
        Caption = 'Porcentaje'
        DataBinding.FieldName = 'PORCENTAJE'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
