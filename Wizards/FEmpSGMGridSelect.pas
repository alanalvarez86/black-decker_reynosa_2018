unit FEmpSGMGridSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid, Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, dbClient;

type

  TEmpSGMGridSelect = class(TBaseGridSelect)
    procedure FormShow(Sender: TObject);

  private
    procedure MontosGetText(Sender: TField; var Text: String; DisplayText: Boolean);
  public
  end;

var
  EmpSGMGridSelect: TEmpSGMGridSelect;

implementation

uses dGlobal,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     FBaseReportes,
     ZBasicoSelectGrid,
     ZGlobalTress;

{$R *.DFM}

procedure TEmpSGMGridSelect.FormShow(Sender: TObject);
begin
     //AgregaColumnasGrid;// No Habr� problema con siempre Agregar Columnas por que se esta haciendo un free de la forma al terminar
     DataSet.FieldByName( 'EP_CFIJO' ).onGetText := MontosGetText;
     DataSet.FieldByName( 'EP_CTITULA' ).onGetText := MontosGetText;
     DataSet.FieldByName( 'EP_CPOLIZA' ).onGetText := MontosGetText;
     DataSet.FieldByName( 'EP_CEMPRES' ).onGetText := MontosGetText;
     DataSet.FieldByName( 'EP_CTOTAL' ).onGetText := MontosGetText;

     ZetaDBGrid.Columns[13].Visible := not (DataSet.FieldByName( 'EP_OBSERVA' ).AsString = 'BoRrAnDo'); 
     inherited;
end;

procedure TEmpSGMGridSelect.MontosGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     //MostrarCampoTexto( Sender, Text, DisplayText );
     if DisplayText then
        Text := FormatFloat( '#,0.00', Sender.AsFloat );
end;

end.
