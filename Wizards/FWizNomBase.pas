unit FWizNomBase;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ComCtrls, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons,
     ZetaDBTextBox,
     ZetaDBGrid,
     ZetaKeyLookup,
     ZetaWizard,
     ZBaseWizardFiltro,
     ZetaEdit;

type
  TWizNomBase = class(TBaseWizardFiltro)
    GroupBox1: TGroupBox;
    PeriodoTipoLbl: TLabel;
    iTipoNomina: TZetaTextBox;
    iNumeroNomina: TZetaTextBox;
    iMesNomina: TZetaTextBox;
    sDescripcion: TZetaTextBox;
    FechaInicial: TZetaTextBox;
    FechaFinal: TZetaTextBox;
    sStatusNomina: TZetaTextBox;
    Label4: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    PeriodoNumeroLBL: TLabel;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);

  private
    { Private declarations }
  protected
    { Private declarations }
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomBase: TWizNomBase;

implementation

uses DCliente,
     ZetaCommonTools,
     ZetaCommonLists;

{$R *.DFM}

procedure TWizNomBase.CargaParametros;
begin
     with Descripciones do
     begin
          with dmCliente do
               AddString( 'Per�odo', ShowNomina( YearDefault, Ord( PeriodoTipo ), PeriodoNumero ) );
     end;

     inherited;
end;

procedure TWizNomBase.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          with GetDatosPeriodoActivo do
          begin
               iNumeroNomina.Caption := IntToStr( Numero );
               iTipoNomina.Caption := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
               iMesNomina.Caption := ObtieneElemento( lfMeses, ( Mes - 1  ) );
               sStatusNomina.Caption := ZetaCommonTools.GetDescripcionStatusPeriodo( Status, StatusTimbrado );
               FechaInicial.Caption := FormatDateTime( LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( LongDateFormat, Fin );
          end;
     end;
end;

end.
