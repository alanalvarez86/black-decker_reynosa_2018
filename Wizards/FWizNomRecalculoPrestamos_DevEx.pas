unit FWizNomRecalculoPrestamos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, ComCtrls, Buttons, ExtCtrls,
     FWizEmpBaseFiltro,
     ZetaWizard,
     ZetaKeyCombo,
     ZetaNumero,
     ZetaEdit, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomRecalculoPrestamos_DevEx = class(TWizEmpBaseFiltro)
    RecalcularPrestamos: TGroupBox;
    TodosPrestamos: TRadioButton;
    SoloUnPrestamo: TRadioButton;
    PR_TIPO: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure TodosPrestamosClick(Sender: TObject);
    procedure SoloUnPrestamoClick(Sender: TObject);
    procedure ControlesVerificar;
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizNomRecalculoPrestamos_DevEx: TWizNomRecalculoPrestamos_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     DCliente,
     DProcesos,
     dTablas,
     dRecursos,
     DGlobal,
     ZGlobalTress,
     ZetaCommonTools;

{$R *.DFM}

procedure TWizNomRecalculoPrestamos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H394005_Recalcular_prestamos;
     ParametrosControl := TodosPrestamos;

     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;

     //Conectar LookUp
     dmTablas.cdsTPresta.Conectar;

     PR_TIPO.LookupDataset := dmTablas.cdsTPresta;

     ControlesVerificar;
end;

procedure TWizNomRecalculoPrestamos_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          //AddBoolean( 'Recalcula pr�stamos', True );
          if TodosPrestamos.Checked then
             AddBoolean( 'Todos los pr�stamos', TodosPrestamos.Checked );
          if SoloUnPrestamo.Checked then
             AddString('S�lo un tipo de pr�stamo', PR_TIPO.Descripcion );
     end;
     with ParameterList do
     begin
          AddInteger( 'Mes', 0 );
          AddInteger( 'Year', 0 );
          AddBoolean( 'Acumulados', False );
          AddBoolean( 'Ahorros', False );
          AddBoolean( 'Prestamos', True );
          if TodosPrestamos.Checked then
             AddString( 'TipoPrestamo', VACIO );
          if SoloUnPrestamo.Checked then
          begin
               AddString('TipoPrestamoDescripcion', Trim( PR_TIPO.Descripcion ) );
               AddString('TipoPrestamo', PR_TIPO.Llave );
          end;
     end;
end;

procedure TWizNomRecalculoPrestamos_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin

     ParametrosControl := nil;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          //Validar Prestamos
          if CanMove and SoloUnPrestamo.Checked then
          begin
               if StrVacio( PR_TIPO.Llave ) then
               begin
                    ZetaDialogo.ZError( Caption, 'Debe seleccionar un tipo de pr�stamo', 0 );
                    CanMove := FALSE;
               end;

               if ( dmRecursos.EsPrestamoFonacot( PR_TIPO.Llave ) ) and ( Global.GetGlobalBooleano( K_GLOBAL_FONACOT_PRESTAMO_CONCILIA ) ) then
               begin
                    ZetaDialogo.ZInformation ( Caption, 'No es posible usar el tipo de pr�stamo Fonacot si est� activo el mecanismo' + CR_LF +
                                                 'para Administrar pr�stamos sin conciliaci�n en Globales de N�mina.', 0 );
                    CanMove := FALSE;
               end;
          end;
     end;
     inherited;
end;

function TWizNomRecalculoPrestamos_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.RecalculoPrestamos(ParameterList);
end;

procedure TWizNomRecalculoPrestamos_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'El proceso recalcula los pr�stamos considerando las n�minas afectadas para el grupo de empleados seleccionados.';
end;

procedure TWizNomRecalculoPrestamos_DevEx.SoloUnPrestamoClick(Sender: TObject);
begin
     inherited;
     TodosPrestamos.Checked := not SoloUnPrestamo.Checked;
     PR_TIPO.Enabled := SoloUnPrestamo.Checked;
end;

procedure TWizNomRecalculoPrestamos_DevEx.TodosPrestamosClick(Sender: TObject);
begin
     inherited;
     SoloUnPrestamo.Checked := not TodosPrestamos.Checked;
     PR_TIPO.Enabled := not TodosPrestamos.Checked;
end;

procedure TWizNomRecalculoPrestamos_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.Adelante then
          if Wizard.EsPaginaActual( Parametros ) then
             ParametrosControl := TodosPrestamos;
     inherited;
end;

procedure TWizNomRecalculoPrestamos_DevEx.WizardAlEjecutar( Sender: TObject; var lOk: Boolean );
begin
     if Wizard.UltimoPaso and Wizard.EsPaginaActual( Ejecucion ) then
     begin
          if ZetaDialogo.zWarningYesNoConfirm( Caption, 'Este proceso realiza ajustes a los pr�stamos de los empleados seleccionados' + CR_LF + ' y no existe un proceso masivo para la recuperaci�n de los saldos anteriores.' + CR_LF + '�Desea continuar?', 0, mbNo ) then
             inherited
          else
          begin
               lOk := False;
               WizardControl.Buttons.Finish.Enabled := True;
          end;
     end;
end;

procedure TWizNomRecalculoPrestamos_DevEx.ControlesVerificar;
begin
     PR_TIPO.Enabled := SoloUnPrestamo.Checked;
end;

end.
