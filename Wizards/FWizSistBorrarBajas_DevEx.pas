unit FWizSistBorrarBajas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, ComCtrls, StdCtrls, Mask,
     ZetaEdit, ZetaFecha, FWizSistBaseFiltro, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
     cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl;

type
  TWizSISTBorrarBajas_DevEx = class(TWizSistBaseFiltro)
    dFecha: TZetaFecha;
    FechaLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);

  public
    { Public declarations }
  end;

var
  WizSISTBorrarBajas_DevEx: TWizSISTBorrarBajas_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaCommonTools,
     ZetaCommonClasses, ZcxWizardBasico;

{$R *.DFM}

procedure TWizSISTBorrarBajas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := dFecha;
     HelpContext := H80832_Borrar_bajas;
     with dmCliente do
     begin
          dFecha.Valor := ZetaCommonTools.FirstDayOfYear( YearDefault - 1 ) - 1;
     end;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizSISTBorrarBajas_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddDate( 'Fecha', dFecha.Valor );
     end;

     with Descripciones do
     begin
          AddDate( 'Borrar Bajas anteriores a', dFecha.Valor );
     end;
end;

function TWizSISTBorrarBajas_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.BorrarBajas( ParameterList );
end;


procedure TWizSISTBorrarBajas_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := dFecha
     else
         ParametrosControl := nil;
     inherited;
end;

procedure TWizSISTBorrarBajas_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Al aplicar este proceso se borrar�n todos los empleados en estatus baja en el periodo indicado.';
end;


end.
