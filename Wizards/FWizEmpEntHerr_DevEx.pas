unit FWizEmpEntHerr_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, Mask, ZetaFecha, ZetaEdit, StdCtrls, ComCtrls,
  ExtCtrls, FWizEmpBaseFiltro,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit, cxMemo,
  cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage, dxCustomWizardControl,
  dxWizardControl;

type
  TWizEmpEntHerr_DevEx = class(TWizEmpBaseFiltro)
    HerramientaLbl: TLabel;
    TO_CODIGO: TZetaKeyLookup_DevEx;
    FechaLbl: TLabel;
    KT_FEC_INI: TZetaFecha;
    LblReferencia: TLabel;
    KT_REFEREN: TEdit;
    LblTalla: TLabel;
    KT_TALLA: TZetaKeyLookup_DevEx;
    LblObservaciones: TLabel;
    KT_COMENTA: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpEntHerr_DevEx: TWizEmpEntHerr_DevEx;

implementation

uses dCliente, dCatalogos, dTablas, dProcesos, ZBaseSelectGrid_DevEx, FEmpEntHerrGridSelect_DevEx,
     ZetaCommonTools, ZetaDialogo, ZetaCommonClasses, ZcxWizardBasico,
     ZCXBaseWizardFiltro;

{$R *.DFM}

procedure TWizEmpEntHerr_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := TO_CODIGO;
     HelpContext := H11617_Entregar_Herramienta;
     TO_CODIGO.LookupDataset := dmCatalogos.cdsTools;
     KT_TALLA.LookupDataset := dmTablas.cdsTallas;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;

end;

procedure TWizEmpEntHerr_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmTablas.cdsTallas.Conectar;
     with dmCatalogos.cdsTools do
     begin
          Conectar;
          TO_CODIGO.Llave := FieldByName( 'TO_CODIGO' ).AsString;
     end;
     KT_FEC_INI.Valor := dmCliente.FechaDefault;

     Advertencia.Caption := 'Se registrará la entrega de herramienta a los empleados seleccionados.';
end;

procedure TWizEmpEntHerr_DevEx.CargaParametros;
begin

     inherited CargaParametros;

     with ParameterList do
     begin
          AddString( 'Herramienta', TO_CODIGO.Llave );
          AddDate( 'Fecha', KT_FEC_INI.Valor );
          AddString( 'Referencia', KT_REFEREN.Text );
          AddString( 'Talla', KT_TALLA.Llave );
          AddString( 'Observaciones', KT_COMENTA.Text );
     end;

     with Descripciones do
     begin
          AddString( 'Herramienta', GetDescripLlave( TO_CODIGO ) );
          AddDate( 'Fecha', KT_FEC_INI.Valor );
          AddString( 'Referencia', KT_REFEREN.Text );
          AddString( 'Talla', KT_TALLA.Llave );
          AddString( 'Observaciones', KT_COMENTA.Text );
     end;

end;

procedure TWizEmpEntHerr_DevEx.CargaListaVerificacion;
begin
     dmProcesos.EntregarHerramientaGetLista( ParameterList );
end;

function TWizEmpEntHerr_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpEntHerrGridSelect_DevEx );
end;

function TWizEmpEntHerr_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.EntregarHerramienta( ParameterList, Verificacion );
end;

procedure TWizEmpEntHerr_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
          var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;  //DevEx
     if CanMove then
     begin
          if Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
          begin
               if strVacio( TO_CODIGO.Llave ) then
               begin
                    zError( Caption, 'Debe especificarse la herramienta', 0 );
                    ActiveControl := TO_CODIGO;
                    CanMove := False;
               end;
          end;
     end;
end;

//DevEx     //Agregado para enfocar un control
procedure TWizEmpEntHerr_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := TO_CODIGO
     else
         ParametrosControl := nil;
     inherited;
end;


end.


