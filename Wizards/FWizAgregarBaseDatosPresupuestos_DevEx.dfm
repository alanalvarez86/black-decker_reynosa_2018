inherited WizAgregarBaseDatosPresupuestos_DevEx: TWizAgregarBaseDatosPresupuestos_DevEx
  Left = 1312
  Top = 203
  Caption = 'Agregar Base de Datos de tipo Presupuestos'
  ClientHeight = 529
  ClientWidth = 540
  ExplicitWidth = 546
  ExplicitHeight = 557
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 540
    Height = 529
    ExplicitWidth = 540
    ExplicitHeight = 529
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que permite agregar una base de datos de tipo Presupuest' +
        'os con la opci'#243'n para crearla o utilizar una existente.'
      Header.Title = 'Agregar base de datos tipo Presupuestos'
      ExplicitWidth = 518
      ExplicitHeight = 389
      inherited lblSeleccionarBD: TLabel
        Left = 121
        Top = 180
        ExplicitLeft = 121
        ExplicitTop = 180
      end
      inherited lbNombreBD: TLabel
        Left = 114
        Top = 124
        ExplicitLeft = 114
        ExplicitTop = 124
      end
      inherited cbBasesDatos: TComboBox
        Left = 256
        Top = 176
        ExplicitLeft = 256
        ExplicitTop = 176
      end
      inherited txtBaseDatos: TEdit
        Left = 256
        Top = 120
        ExplicitLeft = 256
        ExplicitTop = 120
      end
      inherited rbBDNueva: TcxRadioButton
        Left = 88
        Top = 93
        ExplicitLeft = 88
        ExplicitTop = 93
      end
      inherited rbBDExistente: TcxRadioButton
        Left = 88
        Top = 157
        ExplicitLeft = 88
        ExplicitTop = 157
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 518
      ExplicitHeight = 389
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 518
        ExplicitHeight = 214
        Height = 214
        Width = 518
      end
      inherited cxGroupBox1: TcxGroupBox
        TabOrder = 2
        ExplicitWidth = 518
        Width = 518
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 450
          Width = 450
          AnchorY = 42
        end
        inherited cxImage1: TcxImage
          TabStop = False
          TabOrder = 0
          ExplicitLeft = 1
          ExplicitTop = 20
        end
      end
      inherited GrupoAvance: TcxGroupBox
        Top = 279
        TabOrder = 1
        ExplicitTop = 279
        ExplicitWidth = 518
        ExplicitHeight = 110
        Height = 110
        Width = 518
        inherited cxMemoStatus: TcxMemo
          Left = 241
          TabStop = False
          TabOrder = 1
          ExplicitLeft = 241
          ExplicitWidth = 274
          ExplicitHeight = 85
          Height = 85
          Width = 274
        end
        inherited cxMemoPasos: TcxMemo
          TabStop = False
          TabOrder = 0
          ExplicitWidth = 238
          ExplicitHeight = 85
          Height = 85
          Width = 238
        end
      end
    end
    inherited NuevaBD2: TdxWizardControlPage
      ExplicitWidth = 518
      ExplicitHeight = 389
      inherited lblUbicacion: TLabel
        Left = 81
        Top = 90
        ExplicitLeft = 81
        ExplicitTop = 90
      end
      inherited GBUsuariosMSSQL: TcxGroupBox
        Left = 80
        Top = 112
        TabStop = True
        ExplicitLeft = 80
        ExplicitTop = 112
        inherited lblClave: TLabel
          Left = 104
          ExplicitLeft = 104
        end
        inherited lblUsuario: TLabel
          Left = 96
          ExplicitLeft = 96
        end
        inherited txtUsuario: TEdit
          Left = 144
          TabOrder = 2
          ExplicitLeft = 144
        end
        inherited txtClave: TMaskEdit
          Left = 144
          TabOrder = 3
          ExplicitLeft = 144
        end
        inherited rbSameUser: TcxRadioButton
          Left = 56
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 56
        end
        inherited rbOtroUser: TcxRadioButton
          Left = 56
          TabOrder = 1
          TabStop = True
          ExplicitLeft = 56
        end
      end
      inherited lblDatoUbicacion: TcxLabel
        Left = 136
        Top = 88
        ExplicitLeft = 136
        ExplicitTop = 88
      end
    end
    inherited Estructura: TdxWizardControlPage
      ExplicitWidth = 518
      ExplicitHeight = 389
      inherited lblDescripcion: TLabel
        Left = 114
        Top = 164
        ExplicitLeft = 114
        ExplicitTop = 164
      end
      inherited lblCodigo: TLabel
        Left = 137
        Top = 140
        ExplicitLeft = 137
        ExplicitTop = 140
      end
      inherited txtDescripcion: TZetaEdit
        Left = 176
        Top = 160
        ExplicitLeft = 176
        ExplicitTop = 160
      end
      inherited txtCodigo: TZetaEdit
        Left = 176
        Top = 136
        OnExit = txtCodigoExit
        ExplicitLeft = 176
        ExplicitTop = 136
      end
    end
    object BaseAnio: TdxWizardControlPage
      Hint = ''
      Header.Description = 
        'Indique la base de datos existente de tipo Tress que se usar'#225' pa' +
        'ra crear la nueva base de datos.'
      Header.Title = 'Base y A'#241'o'
      object Label3: TLabel
        Left = 104
        Top = 126
        Width = 71
        Height = 13
        Caption = 'Base de datos:'
        Transparent = True
      end
      object lblAnyo: TLabel
        Left = 80
        Top = 149
        Width = 95
        Height = 13
        Caption = 'A'#241'o a presupuestar:'
        Transparent = True
      end
      object txtAnio: TZetaNumero
        Left = 181
        Top = 145
        Width = 121
        Height = 21
        Mascara = mnDias
        TabOrder = 1
        Text = '0'
      end
      object cbBasesDatosBase: TZetaKeyCombo
        Left = 181
        Top = 121
        Width = 257
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object chbEspeciales: TcxCheckBox
        Left = 179
        Top = 169
        Hint = ''
        Caption = 'Aplicar script de programaci'#243'n especial'
        TabOrder = 2
        Transparent = True
        Width = 218
      end
    end
    object FechaAlta: TdxWizardControlPage
      Hint = ''
      Header.Description = 
        'Los empleados a transferir son aquellos que est'#233'n activos con fe' +
        'cha de ingreso menor o igual a la fecha  que se indique en '#233'ste ' +
        'par'#225'metro. '
      Header.Title = 'Transferir empleados activos'
      object lblFechaAlta: TLabel
        Left = 126
        Top = 126
        Width = 162
        Height = 13
        Caption = 'Incluir contrataciones anteriores a:'
        Transparent = True
      end
      object fecAlta: TZetaFecha
        Left = 294
        Top = 121
        Width = 97
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '05/mar/14'
        Valor = 41703.000000000000000000
      end
    end
    object FechaBaja: TdxWizardControlPage
      Hint = ''
      Header.Description = 
        'Los empleados a transferir son aquellos que su baja sea mayor a ' +
        'la fecha que se especifique en '#233'ste par'#225'metro, y se requieren cu' +
        'ando se desea calcular el Reparto de Utilidades.'
      Header.Title = 'Transferir empleados inactivos'
      object lblFechaBaja: TLabel
        Left = 114
        Top = 126
        Width = 187
        Height = 13
        Caption = 'Incluir empleados dados de baja desde:'
        Transparent = True
      end
      object fecBaja: TZetaFecha
        Left = 307
        Top = 121
        Width = 97
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '05/mar/14'
        Valor = 41703.000000000000000000
      end
    end
    object NominaMensual: TdxWizardControlPage
      Hint = ''
      Header.Description = 
        'Esta opci'#243'n realiza el cambio de tipo de periodo de n'#243'mina mensu' +
        'al para todos los empleados. Este par'#225'metro favorece el tiempo d' +
        'e c'#225'lculo del presupuesto.'
      Header.Title = 'Cambio de tipo de periodo mensual'
      object chbNominaMensual: TcxCheckBox
        Left = 156
        Top = 121
        Hint = ''
        Caption = 'Cambiar empleados a n'#243'mina mensual:'
        Properties.Alignment = taRightJustify
        State = cbsChecked
        TabOrder = 0
        Transparent = True
        Width = 206
      end
    end
    object ConfNomina: TdxWizardControlPage
      Hint = ''
      Header.Description = 
        'Esta opci'#243'n no borra ni modifica los par'#225'metros y conceptos de n' +
        #243'mina previamente configurados, solo agrega los que no est'#225'n en ' +
        'la base de datos destino.'
      Header.Title = 'Conservar configuraci'#243'n de n'#243'mina'
      object chbConfNomina: TcxCheckBox
        Left = 162
        Top = 121
        Hint = ''
        Caption = 'Conservar configuraci'#243'n de n'#243'mina:'
        Properties.Alignment = taRightJustify
        TabOrder = 0
        Transparent = True
        Width = 193
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 72
    Top = 323
  end
  object OpenDialog: TOpenDialog
    Options = [ofHideReadOnly]
    Title = 'Seleccione la carpeta con los archivos para carga inicial'
    Left = 14
    Top = 330
  end
end
