unit FNomDeclaracionAnualGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid_DevEx, Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons;

type
  TNomDeclaracionAnualGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NomDeclaracionAnualGridSelect_DevEx: TNomDeclaracionAnualGridSelect_DevEx;

implementation

{$R *.DFM}

//DevEx(by am): No se utiliza el FormShow de la herencia porque especifica los titulos de las columnas. Para esta implementacion los titulos cambian.
procedure TNomDeclaracionAnualGridSelect_DevEx.FormShow(Sender: TObject);
var I:integer;
begin
     SetControls;
     ZetaDbGridDBTableView.OptionsCustomize.ColumnHorzSizing := True;
     for I:=0 to ZetadbGridDbtableview.ColumnCount -1 do
        begin
        if(zetadbgriddbtableview.Columns[i].DataBinding.FieldName='CB_CODIGO')
        then
        begin
          zetadbgriddbtableview.Columns[i].Options.HorzSizing := false;
        end
        else
        if(zetadbgriddbtableview.Columns[i].DataBinding.FieldName='PrettyName') or (zetadbgriddbtableview.Columns[i].DataBinding.FieldName='PRETTYNAME') then
        begin
            zetadbgriddbtableview.Columns[i].Options.HorzSizing := true;
            zetadbgriddbtableview.Columns[i].MinWidth := 150;
        end
        else
        begin
           zetadbgriddbtableview.Columns[i].Options.HorzSizing := false;
        end;
     end;
     {***(am):Trabaja CON GridMode***}
     if ZetaDBGridDBTableView.DataController.DataModeController.GridMode then
        ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
     {***}
     ApplyMinWidth ;
end;

end.
