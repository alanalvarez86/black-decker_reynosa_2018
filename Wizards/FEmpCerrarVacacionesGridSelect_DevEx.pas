unit FEmpCerrarVacacionesGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, Db,
     ZBaseSelectGrid_DevEx,
     ZetaDBGrid, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpCierreVacacionesGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    FECHA_CIERRE: TcxGridDBColumn;
    D_GOZO: TcxGridDBColumn;
    D_PAGO: TcxGridDBColumn;
    D_PRIMA: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpCierreVacacionesGridSelect_DevEx: TEmpCierreVacacionesGridSelect_DevEx;

implementation

{$R *.DFM}

procedure TEmpCierreVacacionesGridSelect_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with DataSet do
     begin
          MaskPesos( 'D_GOZO' );
          MaskPesos( 'D_PAGO' );
          MaskPesos( 'D_PRIMA');
     end;
end;

end.
