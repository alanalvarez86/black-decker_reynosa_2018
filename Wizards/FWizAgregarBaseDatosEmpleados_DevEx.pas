unit FWizAgregarBaseDatosEmpleados_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, ZetaDialogo,
  FWizBaseAgregarBaseDatos_DevEx, cxLookAndFeelPainters, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, cxTrackBar,
  ZetaNumero, ZetaCXWizard, ZetaEdit, Mask, cxTextEdit, cxMemo,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, StdCtrls, cxRadioGroup,
  dxCustomWizardControl, dxWizardControl, 
  FWizBasicoAgregarBaseDatos_DevEx, cxCheckBox, ZetaKeyCombo;

type
  TWizAgregarBaseDatosEmpleados_DevEx = class(TWizBaseAgregarBaseDatos_DevEx)
    DefinirEstructura: TdxWizardControlPage;
    cbBasesDatosBase: TZetaKeyCombo;
    rbValoresDefault: TcxRadioButton;
    rbOtraBaseDatos: TcxRadioButton;
    chbEspeciales: TcxCheckBox;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure rbOtraBaseDatosClick(Sender: TObject);
    procedure rbValoresDefaultClick(Sender: TObject);
    procedure txtCodigoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  end;

var
  WizAgregarBaseDatosEmpleados_DevEx: TWizAgregarBaseDatosEmpleados_DevEx;

implementation

uses
    ZcxWizardBasico,
    DSistema,
    DBaseSistema,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaClientTools,
    ZetaServerTools;

{$R *.dfm}

{***DevEx(by am): Codigo de la implementacion***}

procedure TWizAgregarBaseDatosEmpleados_DevEx.CargaParametros;
Const K_2_MB = 2048;
      K_1024 = 1024;
      K_KB_DOC = 100;
begin
     inherited;
     ParamInicial:=0;
     Descripciones.Clear;
     with Descripciones do
     begin
          if (rbBDNueva.Checked) then
          begin
               AddString('Base de datos', txtBaseDatos.Text );
               AddString('Ubicaci�n' , dmSistema.GetServer + '.' + txtBaseDatos.Text);
          end
          else
          begin
               AddString('Base de datos existente', cbBasesDatos.Text );
               AddString('Ubicaci�n' ,dmSistema.GetServer + '.' + cbBasesDatos.Text);
          end;
          if NuevaBD1.PageVisible then
          begin
               CreaParametroTamanio;
               AddString('Tama�o',Format ('BD origen %s, log %s',[tamanio, log]));
          end;
          if DefinirEstructura.PageVisible then
          begin
              if rbValoresDefault.Checked then
                 AddString('Datos iniciales','Valores default')
              else
                 AddString('Datos iniciales' , cbBasesDatosBase.Text );
          end
          else
               AddString('Estructura','Tipo Tress');
     end;
     with ParameterList do
     begin
          AddInteger('Tipo', Ord (tc3Datos));
          AddString( 'Codigo', txtCodigo.Text );
          AddString( 'Descripcion', txtDescripcion.Text );
          AddBoolean('DefinirEstructura', DefinirEstructura.Enabled);
          AddBoolean('ValoresDefault', rbValoresDefault.Checked);
          AddBoolean('NuevaBD', rbBDNueva.Checked);
          AddBoolean('Especiales', chbEspeciales.Checked);

          if rbOtraBaseDatos.Checked then
          begin
               AddString( 'BaseDatosBase', cbBasesDatosBase.Llaves[cbBasesDatosBase.ItemIndex]);
          end;
          if rbBDNueva.Checked then
          begin  
               AddString( 'Usuario', Trim(txtUsuario.Text) );
               AddString( 'Clave', ZetaServerTools.Encrypt(txtClave.Text));
               AddString( 'NombreBDSQL', Trim(txtBaseDatos.Text));
               AddString( 'BaseDatos', Trim(txtBaseDatos.Text) );
               AddInteger ('MaxSize', Round (((tbEmpleados.Position*K_2_MB)/K_1024) + ((tbEmpleados.Position*tbDocumentos.Position*K_KB_DOC)/K_1024)));
          end   
          else
          begin 
               AddString( 'Usuario', VACIO );
               AddString( 'Clave', VACIO);
               AddString( 'BaseDatos', cbBasesDatos.Text );
          end;
     end;
end;


function TWizAgregarBaseDatosEmpleados_DevEx.EjecutarWizard: Boolean;
var resultado: Boolean;
    sAdvertenciaValidarBDEmpleados: WideString; 
    sBaseDatos: String;
begin
     // Si se definir� estructura de Base de Datos de Empleados,
     // ejecutar directamente sobre el dll a trav�s de DSistema.
     resultado := FALSE;

     if NuevaBD1.PageVisible then
        sBaseDatos := txtBaseDatos.Text
     else 
        sBaseDatos := cbBasesDatos.Text;
        
     if (DefinirEstructura.PageVisible) or (NuevaBD1.PageVisible = TRUE) then
     begin
         try
              // Validaci�n de base de datos de empleados.
              sAdvertenciaValidarBDEmpleados := dmSistema.ValidarBDEmpleados(txtCodigo.Text, dmSistema.GetServer + '.' + sBaseDatos, TRUE);
              if sAdvertenciaValidarBDEmpleados <> VACIO then
                   ZWarning(Caption, sAdvertenciaValidarBDEmpleados, 0, mbOK);
              // ----- -----

              {***DevEx(by am):Mostar Grupo de Avance y Limpiar memos de avance***}
              GrupoAvance.Visible := TRUE;
              cxMemoPasos.Lines.Clear;
              cxMemoStatus.Lines.Clear;
              {***DevEx(by am): Paso1***}
              if  NuevaBD1.PageVisible = TRUE then
              begin
                   cxMemoPasos.Lines.Append('Creando base de datos... ');
                   resultado := dmSistema.CrearBDEmpleados(ParameterList);
                   SetStatus(resultado); //Paso1
              end
              else
                  resultado := TRUE;

              {***DevEx(by am): Paso2***}
              if resultado then
              begin
                   cxMemoPasos.Lines.Append('Definiendo estructura... ');
                   resultado := dmSistema.CrearEstructuraBD (ParameterList);
                   SetStatus(resultado); //Paso2
              end;

              {***DevEx(by am): Paso3***}
              if (resultado) and (chbEspeciales.Checked) then
              begin
                   cxMemoPasos.Lines.Append('Scripts especiales... ');
                   resultado := dmSistema.CreaEspeciales(ParameterList);
                   SetStatus(resultado); //Paso3
              end;

              {***DevEx(by am): Paso4***}
              if resultado then
              begin
                  cxMemoPasos.Lines.Append('Valores iniciales... ');
                  resultado := dmSistema.CreaInicialesTabla(ParameterList);
                  SetStatus(resultado); //Paso4
              end;

              {***DevEx(by am): Paso5***}
              if resultado then
              begin
                   cxMemoPasos.Lines.Append('Valores sugeridos... ');
                   resultado := dmSistema.CreaSugeridosTabla(ParameterList);
                   SetStatus(resultado); //Paso5
              end;

              {***DevEx(by am): Paso6***}
              if resultado then
              begin
                   cxMemoPasos.Lines.Append('Diccionario de datos... ');
                   resultado := dmSistema.CreaDiccionarioTabla(ParameterList);
                   SetStatus(resultado); //Paso6
              end;

              {***DevEx(by am): Paso7***}
              if resultado then
              begin
                   cxMemoPasos.Lines.Append('Registrando base de datos en sistema... ');
                   resultado := dmSistema.EmpGrabarDBSistema(ParameterList);
                   SetStatus(resultado); //Paso7
              end;

              {***DevEx(by am): Paso8***}
              if resultado then
                   ZInformation(Caption, 'La base de datos fue agregada correctamente.', 0)
              else
                   ZError(Caption, 'Error al agregar base de datos.', 0);
              Result := TRUE;

         except
               on Error: Exception do
               begin
                   SetStatus(resultado); //Por si Se da alguno de los siguientes errores.
                   if (  Pos( 'PRIMARY KEY', UpperCase( Error.Message )) > 0) then
                   begin
                      ZError (Caption, Format('Error al agregar base de datos:' + CR_LF + 'El c�digo de base de datos: %s ya se encuentra dado de alta',[ txtCodigo.Text]), 0);
                   end                            
                   else if (  Pos( 'ALREADY EXISTS', UpperCase( Error.Message )) > 0) then
                   begin
                      ZError (Caption, Format('Error al agregar base de datos:' + CR_LF + 'Base de datos: %s ya existe. Proporcione otro nombre.',[ Trim (txtBaseDatos.Text)]), 0);
                   end
                   else
                   begin
                       ZError(Caption, 'Error al agregar base de datos:' + CR_LF + Error.Message, 0);
                   end;
                   Result := TRUE;
               end;

         end;
     end
     else
     begin
         try
             // Validaci�n de base de datos de empleados.
             sAdvertenciaValidarBDEmpleados := dmSistema.ValidarBDEmpleados(txtCodigo.Text, dmSistema.GetServer + '.' + sBaseDatos, TRUE);
             if sAdvertenciaValidarBDEmpleados <> VACIO then
                ZWarning(Caption, sAdvertenciaValidarBDEmpleados, 0, mbOK);

             Result := dmSistema.EmpGrabarDBSistema(ParameterList);
             if Result then
                ZInformation(Caption, 'La base de datos fue agregada correctamente.', 0)
             else
             begin
                 ZError(Caption, 'Error al agregar base de datos.', 0);
                 Result := TRUE;
             end;
         except
               on Error: Exception do
               begin
                   if (  Pos( 'PRIMARY KEY', UpperCase( Error.Message )) > 0) then
                   begin
                      ZError (Caption, Format('Error al agregar base de datos:' + CR_LF + 'El c�digo de base de datos: %s ya se encuentra dado de alta',[ txtCodigo.Text]), 0);
                      {***DevEx( @by am): Se comento para que el flujo de los wizards sea uniforme. Despues de presionar el boton Aplicar siempre se cerraran se ejecute o no la accion***}
                      {Wizard.Anterior;
                      Result := FALSE;}
                   end
                   else
                       ZError(Caption, 'Error al agregar base de datos:' + CR_LF + Error.Message, 0);

                   Result := TRUE;//DevEx(by am): Ante cualquier error el wizard siempre se cerrara. Ese es el flujo normal de los wizards.
               end;
         end
     end;
end;

procedure TWizAgregarBaseDatosEmpleados_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_SIST_PROC_AGREGAR_BASE_DATOS;
     with dmSistema do
     begin
          cdsEmpresasLookUp.Conectar;
     end;
     {***DevEx(by am): Definicion de la secuencia de las paginas***}
          Parametros.PageIndex := 0;
          NuevaBD1.PageIndex := 1;
          NuevaBD2.PageIndex := 2;
          DefinirEstructura.PageIndex := 3;
          Estructura.PageIndex := 4;
          Ejecucion.PageIndex := 5;
     {***}
end;

procedure TWizAgregarBaseDatosEmpleados_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmSistema.GetBasesDatos(dmSistema.GetServer, VACIO, VACIO);
     txtBaseDatos.SetFocus;

     if (cbBasesDatos.Items.Count = 0) then
     begin
         while not dmSistema.cdsBasesDatos.Eof do
         begin
              cbBasesDatos.AddItem(dmSistema.cdsBasesDatos.FieldByName ('NAME').AsString, nil);
              dmSistema.cdsBasesDatos.Next;
         end;
     end;

     chbEspeciales.Enabled := dmSistema.ExistenEspeciales;
     {***DevEx(by am): Se define el mensaje de Advertencia para la pantalla de ejecucion***}
     Advertencia.Caption := 'Se proceder� a preparar la base de datos de tipo Tress. Presione Aplicar para iniciar el proceso.'
end;

procedure TWizAgregarBaseDatosEmpleados_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const K_ESPACIO = ' ';
var mensaje : WideString;
begin
     ParametrosControl:= nil;
     if Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros ) then
          begin
               {***DevEx(by am): Es aqui en Parametros donde se decide el flujo que tomara el wizard***}
               NuevaBD1.PageVisible := (rbBDNueva.Checked);
               NuevaBD2.PageVisible := (rbBDNueva.Checked);
              if rbBDNueva.Checked then
              begin
                   if Trim (txtBaseDatos.Text) = VACIO then
                      CanMove := Warning ('El campo nombre de base de datos no puede quedar vac�o.', txtBaseDatos)
                   else if ExisteBD then
                      CanMove := Warning (Format('La base de datos: %s ya existe. Proporcione otro nombre.',[ Trim (txtBaseDatos.Text)]), txtBaseDatos)
                   else if Pos (K_ESPACIO, txtBaseDatos.Text) > 0 then
                        CanMove := Warning ('El nombre de la base de datos no puede contener espacios', txtBaseDatos)
                   else
                        {***CODIGO NO NECESARIO***}
                        //NuevaBD1.Enabled := TRUE;
                       //NuevaBD2.Enabled := TRUE;
                        CanMove := TRUE;

              end;
              if rbBDExistente.Checked then
              begin
                   {***CODIGO NO NECESARIO***}
                   //NuevaBD1.Enabled := FALSE;
                   //NuevaBD2.Enabled := FALSE;
                   if (cbBasesDatos.ItemIndex >= 0) then
                   begin
                        // Es Base deDatos ya agregada a COMPANY?
                        if (dmSistema.EsBaseDatosAgregada(dmSistema.GetServer + '.' + cbBasesDatos.Text)) then
                        begin
                             CanMove := Warning (Format ('La base de datos ''%s'' ya se encuentra agregada al sistema.', [cbBasesDatos.Text]), cbBasesDatos)
                        end
                        else
                        begin
                             // Averiguar si la Base de Datos seleccionada no es de Selecci�n o Visitantes
                             if (dmSistema.NoEsBDSeleccionVisitantes(cbBasesDatos.Text)) then
                             begin
                                 // Averiguar si la Base de Datos seleccionada tiene la estructura de Empleados (Tabla COLABORA)
                                 {***CODIGO OBSOLETO***}
                                 {if (dmSistema.EsBaseDatosEmpleados(cbBasesDatos.Text)) then
                                 begin
                                    DefinirEstructura1.Enabled := FALSE;
                                    CanMove := TRUE;
                                 end
                                 else
                                 begin
                                    DefinirEstructura1.Enabled := TRUE;
                                    CanMove := TRUE;
                                 end; }
                                 DefinirEstructura.PageVisible := not (dmSistema.EsBaseDatosEmpleados(cbBasesDatos.Text));
                                 CanMove := TRUE;
                             end
                             else
                                  CanMove := Warning ('La base de datos seleccionada no puede configurarse.'
                                  + CR_LF + 'Tiene definida una estructura diferente a Tress.', cbBasesDatos);
                        end;
                   end
                   else
                       CanMove := Warning ('Seleccionar una base de datos para continuar.', cbBasesDatos);
              end;
          end
          else if Wizard.EsPaginaActual( Estructura) then
          begin
               if (trim (txtCodigo.Text) = VACIO) then
               begin
                    CanMove := Warning ('El campo C�digo no puede quedar vac�o.', txtCodigo);
               end
               else if Pos (K_ESPACIO, txtCodigo.Text) > 0 then
               begin
                    CanMove := Warning ('El campo c�digo no puede contener espacios', txtCodigo);
               end 
               else if (dmSistema.ExisteBD_DBINFO(trim(txtCodigo.Text))) then
               begin
                    CanMove := Warning (Format ('El c�digo  ''%s'' ya se encuentra agregado al sistema.', [txtCodigo.Text]), txtCodigo)
               end
               else if (trim (txtDescripcion.Text) = VACIO) then
               begin
                    CanMove := Warning ('El campo Descripci�n no puede quedar vac�o.', txtDescripcion);
               end
               else
                   CanMove := TRUE;
          end
          else if Wizard.EsPaginaActual( DefinirEstructura ) then
          begin
               if (rbOtraBaseDatos.Checked) and (cbBasesDatosBase.ItemIndex < 0) then
                    CanMove := Warning ('Seleccionar una base de datos para continuar.', cbBasesDatosBase)
               else
                   CanMove := TRUE;
          end
          else if Wizard.EsPaginaActual( NuevaBD2) then
          begin
               if rbOtroUser.Checked then
               begin
                    if not ProbarConexion(mensaje) then
                    begin
                         CanMove := Warning (mensaje, txtUsuario);
                    end
                end;
           end
     end;
     //DevEx(by am): Codigo agregado para que se ejecute la herencia despues del codigo de la implementacion
     inherited;
end;

procedure TWizAgregarBaseDatosEmpleados_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual( DefinirEstructura ) then
     begin
         if (cbBasesDatosBase.Items.Count = 0) then
         begin
             try
                 cbBasesDatosBase.Lista.BeginUpdate;
                 // Obtener Bases de Datos de Tipo Tress.
                 with dmSistema.cdsSistBaseDatos do
                 begin
                     Conectar;
                     First;
                     while not Eof do
                     begin
                          if (FieldByName ('DB_TIPO').AsInteger = Ord( tc3Datos )) or (FieldByName ('DB_TIPO').AsInteger = Ord( tc3Prueba )) then
                             cbBasesDatosBase.Lista.Add(FieldByName ('DB_CODIGO').AsString + '=' + FieldByName ('DB_DESCRIP').AsString);
                          Next;
                     end;
                 end;
             finally
                    cbBasesDatosBase.Lista.EndUpdate;
             end;
         end;
     end
     else if Wizard.EsPaginaActual( Estructura ) then
     begin
          ParametrosControl := txtCodigo;
     end
     else if Wizard.EsPaginaActual( NuevaBD1 ) then
     begin
          CalcularTamanyos;
          lblCantEmpSeleccion.Caption := IntToStr(tbEmpleados.Position);
          lblDocSeleccion.Caption := IntToStr(tbDocumentos.Position);
     end;
     //DevEx(by am): No se necesita pues se hace en la base.
     {else if  Wizard.EsPaginaActual( NuevaBD2 ) then
     begin
          lblDatoUbicacion.Caption := dmSistema.GetServer + '.' + Trim (txtBaseDatos.Text);
     end; }
     inherited;
end;

procedure TWizAgregarBaseDatosEmpleados_DevEx.rbOtraBaseDatosClick(
  Sender: TObject);
begin
     cbBasesDatosBase.Enabled := TRUE;
     cbBasesDatosBase.SetFocus;
end;

procedure TWizAgregarBaseDatosEmpleados_DevEx.rbValoresDefaultClick(
  Sender: TObject);
begin
     cbBasesDatosBase.Enabled := FALSE;
end;

procedure TWizAgregarBaseDatosEmpleados_DevEx.txtCodigoExit(
  Sender: TObject);
begin
      txtCodigo.Text :=  Copy(txtCodigo.Text, 0, 10);
end;

end.

