inherited EmpVacacionesGridSelect_DevEx: TEmpVacacionesGridSelect_DevEx
  Left = 176
  Top = 209
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione los Registros de Vacaciones a Agregar'
  ClientHeight = 301
  ClientWidth = 559
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 265
    Width = 559
    inherited OK_DevEx: TcxButton
      Left = 390
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 468
    end
  end
  inherited PanelSuperior: TPanel
    Width = 559
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 559
    Height = 230
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
      end
      object CB_TOT_GOZ: TcxGridDBColumn
        Caption = 'Gozados'
        DataBinding.FieldName = 'CB_TOT_GOZ'
      end
      object CB_TOT_PAG: TcxGridDBColumn
        Caption = 'A Pagar'
        DataBinding.FieldName = 'CB_TOT_PAG'
      end
      object CB_TOT_PV: TcxGridDBColumn
        Caption = 'D'#237'as prima vac.'
        DataBinding.FieldName = 'CB_TOT_PV'
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
