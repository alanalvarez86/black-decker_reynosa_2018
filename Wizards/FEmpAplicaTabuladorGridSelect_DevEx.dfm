inherited EmpAplicaTabuladorGridSelect_DevEx: TEmpAplicaTabuladorGridSelect_DevEx
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Empleados Deseados'
  ClientWidth = 490
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 490
    inherited OK_DevEx: TcxButton
      Left = 326
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 405
      Cancel = True
    end
  end
  inherited PanelSuperior: TPanel
    Width = 490
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 490
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsCustomize.ColumnHorzSizing = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object CB_SALARIO: TcxGridDBColumn
        Caption = 'Salario Actual'
        DataBinding.FieldName = 'CB_SALARIO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 77
      end
      object SAL_CLASIF: TcxGridDBColumn
        Caption = 'Salario Nuevo'
        DataBinding.FieldName = 'SAL_CLASIF'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 78
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
