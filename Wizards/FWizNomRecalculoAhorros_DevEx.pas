unit FWizNomRecalculoAhorros_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, ComCtrls, Buttons, ExtCtrls,
     FWizEmpBaseFiltro,
     ZetaWizard,
     ZetaKeyCombo,
     ZetaNumero,
     ZetaEdit, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomRecalculoAhorros_DevEx = class(TWizEmpBaseFiltro)
    RecalcularAhorros: TGroupBox;
    TodosAhorros: TRadioButton;
    SoloUnAhorro: TRadioButton;
    AH_TIPO: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure SoloUnAhorroClick(Sender: TObject);
    procedure TodosAhorrosClick(Sender: TObject);
    procedure ControlesVerificar;
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizNomRecalculoAhorros_DevEx: TWizNomRecalculoAhorros_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     DCliente,
     DProcesos,
     dTablas,
     ZetaCommonTools;

{$R *.DFM}

procedure TWizNomRecalculoAhorros_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H33425_Recalcular_ahorros;
     ParametrosControl := TodosAhorros;

     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;

     //Conectar LookUp
     dmTablas.cdsTAhorro.Conectar;

     AH_TIPO.LookupDataset := dmTablas.cdsTAhorro;

     ControlesVerificar;
end;

procedure TWizNomRecalculoAhorros_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          //AddBoolean( 'Recalcula ahorros', True );
          if TodosAhorros.Checked then
             AddBoolean( 'Todos los ahorros', TodosAhorros.Checked );
          if SoloUnAhorro.Checked then
             AddString('S�lo un tipo de ahorro', AH_TIPO.Descripcion );
     end;
     with ParameterList do
     begin
          AddInteger( 'Mes', 0 );
          AddInteger( 'Year', 0 );
          AddBoolean( 'Acumulados', False );
          AddBoolean( 'Ahorros', True );
          AddBoolean( 'Prestamos', False );
          if TodosAhorros.Checked then
             AddString( 'TipoAhorro', VACIO );
          if SoloUnAhorro.Checked then
          begin
               AddString('TipoAhorroDescripcion', AH_TIPO.Descripcion );
               AddString('TipoAhorro', AH_TIPO.Llave );
          end;
     end;
end;

procedure TWizNomRecalculoAhorros_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin

     ParametrosControl := nil;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          //Validar Ahorros
          if CanMove and SoloUnAhorro.Checked then
          begin
               if StrVacio( AH_TIPO.Llave ) then
               begin
                    ZetaDialogo.ZError( Caption, 'Debe seleccionar un tipo de ahorro', 0 );
                    CanMove := FALSE;
               end;
          end;
     end;

     inherited;
end;

function TWizNomRecalculoAhorros_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.RecalculoAhorros(ParameterList);
end;

procedure TWizNomRecalculoAhorros_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'El proceso recalcula los saldos de ahorros considerando las n�minas afectadas para el grupo de empleados seleccionados.';
end;

procedure TWizNomRecalculoAhorros_DevEx.SoloUnAhorroClick( Sender: TObject );
begin
     inherited;
     TodosAhorros.Checked := False;
     AH_TIPO.Enabled := SoloUnAhorro.Checked;
end;

procedure TWizNomRecalculoAhorros_DevEx.TodosAhorrosClick( Sender: TObject );
begin
     inherited;
     AH_TIPO.Enabled := not TodosAhorros.Checked;
     SoloUnAhorro.Checked := not TodosAhorros.Checked;
end;

procedure TWizNomRecalculoAhorros_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.Adelante then
          if Wizard.EsPaginaActual( Parametros ) then
             ParametrosControl := TodosAhorros;
     inherited;
end;

procedure TWizNomRecalculoAhorros_DevEx.WizardAlEjecutar( Sender: TObject; var lOk: Boolean );
begin
     if Wizard.UltimoPaso and Wizard.EsPaginaActual( Ejecucion ) then
     begin
          if ZetaDialogo.zWarningYesNoConfirm( Caption, 'Este proceso realiza ajustes a los ahorros de los empleados seleccionados' + CR_LF + ' y no existe un proceso masivo para la recuperaci�n de los saldos anteriores.' + CR_LF + '�Desea continuar?', 0, mbNo ) then
             inherited
          else
          begin
               lOk := False;
               WizardControl.Buttons.Finish.Enabled := True;
          end;
     end;
end;

procedure TWizNomRecalculoAhorros_DevEx.ControlesVerificar;
begin
     AH_TIPO.Enabled := SoloUnAhorro.Checked;
end;

end.
