unit FNomTransferenciasCosteoGridSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid, Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls;

type
  TNomTransferenciasCosteoGridSelect = class(TBaseGridSelect)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NomTransferenciasCosteoGridSelect: TNomTransferenciasCosteoGridSelect;

implementation

uses
    ZGlobalTress,
    DGlobal;

{$R *.DFM}

procedure TNomTransferenciasCosteoGridSelect.FormCreate(Sender: TObject);
begin
     inherited;
     with Global do
          ZetaDBGrid.Columns[2].Title.Caption := Global.NombreCosteo + ' Origen';
end;

end.
