unit FWizAsistCancelarExcepcionesFestivo_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, Mask, ZetaFecha, ZCXBaseWizardFiltro,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit,
  cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl, dxSkinsCore,
  TressMorado2013;

type
  TWizAsistCancelarExcepcionesFestivo_DevEx = class(TBaseCXWizardFiltro)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    FechaFinal: TZetaFecha;
    FechaInicial: TZetaFecha;
    ckFestivoTrabajado: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
  protected
    Procedure CargaParametros; override;
    procedure CargaListaVerificacion; override;
    function  Verificar: Boolean; override;
    function EjecutarWizard:Boolean; override;
  public
    { Public declarations }
  end;

var
  WizAsistCancelarExcepcionesFestivo_DevEx: TWizAsistCancelarExcepcionesFestivo_DevEx;
implementation

{$R *.dfm}
Uses DCliente,
     DProcesos,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     ZBaseSelectGrid_DevEx,
     FEmpleadoGridSelect_DevEx,
     ZAccesosMgr,
     ZAccesosTress;

procedure TWizAsistCancelarExcepcionesFestivo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := FechaInicial;
     with dmCliente do
     begin
          FechaInicial.Valor := FechaAsistencia;
          FechaFinal.Valor := FechaAsistencia;
     end;
     HelpContext:= H_Cancelar_Excepciones_Festivos;
end;

Procedure TWizAsistCancelarExcepcionesFestivo_DevEx.CargaParametros;
begin

     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate('FechaInicial', FechaInicial.Valor);
          AddDate('FechaFinal', FechaFinal.Valor);
          AddBoolean('PuedeCambiarTarjetas',ZAccesosMgr.CheckDerecho( D_ASIS_BLOQUEO_CAMBIOS, K_DERECHO_MODIFICAR_TARJETAS_ANTERIORES ) );
          AddBoolean ( 'FestivoTrabajado', ckFestivoTrabajado.Checked );
     end;
          with Descripciones do
         begin
          AddDate('Fecha inicial', FechaInicial.Valor);
          AddDate('Fecha final', FechaFinal.Valor);
          if ckFestivoTrabajado.Checked then
            AddString( 'Horario de Festivo trabajado', 'S' )
          else
            AddString( 'Horario de Festivo trabajado', 'N' )
     end;
end;

function TWizAsistCancelarExcepcionesFestivo_DevEx.EjecutarWizard:Boolean;
begin
     Result := dmProcesos.CancelarExcepcionesFestivos( ParameterList, Verificacion );
end;

procedure TWizAsistCancelarExcepcionesFestivo_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CancelarExcepcionesFestivosGetLista( ParameterList );
end;

function TWizAsistCancelarExcepcionesFestivo_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

procedure TWizAsistCancelarExcepcionesFestivo_DevEx.WizardBeforeMove(
  Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
   ParametrosControl := nil;
     inherited;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          if ( FechaInicial.Valor > FechaFinal.Valor ) then
             CanMove := Error('La fecha final debe ser mayor o igual a la fecha inicial', FechaFinal);
          if CanMove then
             CanMove := dmCliente.PuedeCambiarTarjetaDlg(FechaInicial.Valor,FechaFinal.Valor);
     end;
end;

procedure TWizAsistCancelarExcepcionesFestivo_DevEx.WizardAfterMove(
  Sender: TObject);
begin
  inherited;
  if Wizard.EsPaginaActual(Parametros) then
    ParametrosControl := FechaInicial;
end;

end.
