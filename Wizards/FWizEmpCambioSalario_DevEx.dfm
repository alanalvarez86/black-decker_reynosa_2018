inherited WizEmpCambioSalario_DevEx: TWizEmpCambioSalario_DevEx
  Left = 508
  Top = 100
  Caption = 'Cambio de Salario Global'
  ClientHeight = 445
  ClientWidth = 450
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 450
    Height = 445
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso genera un cambio de salario aplicando un % de aumen' +
        'to o asignando una cantidad fija a la lista de empleados.'
      Header.Title = 'Cambio de Salario Global'
      object dReferenciaLBL: TLabel
        Left = 36
        Top = 20
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha de Cambio:'
        FocusControl = dReferencia
        Transparent = True
      end
      object nTipoLBL: TLabel
        Left = 38
        Top = 67
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = '&Tipo de Aumento:'
        FocusControl = iTipo
        Transparent = True
      end
      object rTasaLBL: TLabel
        Left = 68
        Top = 91
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = '&Porcentaje:'
        FocusControl = rTasa
        Transparent = True
      end
      object rCantidadLBL: TLabel
        Left = 77
        Top = 114
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ca&ntidad:'
        FocusControl = rCantidad
        Transparent = True
      end
      object sDescripcionLBL: TLabel
        Left = 63
        Top = 136
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = '&Descripci'#243'n:'
        FocusControl = sDescripcion
        Transparent = True
      end
      object sObservaLBL: TLabel
        Left = 48
        Top = 160
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = '&Observaciones:'
        FocusControl = sObserva
        Transparent = True
      end
      object Label1: TLabel
        Left = 8
        Top = 43
        Width = 114
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de aviso a &IMSS:'
        FocusControl = dIMSS
        Transparent = True
      end
      object dReferencia: TZetaFecha
        Left = 126
        Top = 15
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '02/ene/98'
        Valor = 35797.000000000000000000
        OnValidDate = dReferenciaValidDate
      end
      object iTipo: TZetaKeyCombo
        Left = 126
        Top = 63
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 2
        OnChange = iTipoChange
        ListaFija = lfTipoOtrasPer
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object rTasa: TZetaNumero
        Left = 126
        Top = 87
        Width = 80
        Height = 21
        Mascara = mnTasa
        TabOrder = 3
        Text = '0.0 %'
      end
      object rCantidad: TZetaNumero
        Left = 126
        Top = 110
        Width = 80
        Height = 21
        Mascara = mnPesosDiario
        TabOrder = 4
        Text = '0.00'
      end
      object sDescripcion: TEdit
        Left = 126
        Top = 133
        Width = 290
        Height = 21
        TabOrder = 5
        Text = 'Modificaci'#243'n Global de Salario'
      end
      object sObserva: TcxMemo
        Left = 126
        Top = 156
        TabOrder = 6
        Height = 113
        Width = 290
      end
      object lIncapacitados: TcxCheckBox
        Left = 4
        Top = 270
        Caption = 'No Incluir Incapacitados:'
        Properties.Alignment = taRightJustify
        State = cbsChecked
        TabOrder = 7
        Transparent = True
        Width = 139
      end
      object dIMSS: TZetaFecha
        Left = 126
        Top = 38
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '02/ene/98'
        Valor = 35797.000000000000000000
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 210
        Width = 428
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 428
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 360
          AnchorX = 247
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 14
      end
      inherited sFiltroLBL: TLabel
        Left = 39
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 329
  end
end
