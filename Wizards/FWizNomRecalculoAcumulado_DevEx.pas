unit FWizNomRecalculoAcumulado_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, ComCtrls, Buttons, ExtCtrls,
     FWizEmpBaseFiltro,
     ZetaWizard,
     ZetaKeyCombo,
     ZetaNumero,
     ZetaCommonLists,
     ZetaEdit, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, cxCheckBox, ZetaEdit_DevEx;

type
  TWizNomRecalculoAcumulado_DevEx = class(TWizEmpBaseFiltro)
    RecalcularAcumulados: TGroupBox;
    iYearLBL: TLabel;
    iMesLBL: TLabel;
    iYear: TZetaNumero;
    iMes: TZetaKeyCombo;
    UpDownYear: TUpDown;
    lblMesFinal: TLabel;
    MesFinal: TZetaKeyCombo;
    GroupBox1: TGroupBox;
    lbInicialConcepto: TLabel;
    lbFinalConcepto: TLabel;
    RBTodosConcepto: TRadioButton;
    RBRangoConcepto: TRadioButton;
    RBListaConcepto: TRadioButton;
    EInicialConcepto: TZetaEdit_DevEx;
    EFinalConcepto: TZetaEdit_DevEx;
    EListaConcepto: TZetaEdit_DevEx;
    BInicialConcepto: TcxButton;
    BFinalConcepto: TcxButton;
    BListaConcepto: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure lAcumuladosClick(lHabilitar: Boolean);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);//DevEx(by am): Agregado para enfocar correctamente los controles.
    procedure btnListaConceptoClick(Sender: TObject);
    procedure BuscaConceptos( const lConcatena: Boolean; oEdit: TZetaEdit_DevEx );
    procedure BInicialConceptoClick(Sender: TObject);
    procedure BFinalConceptoClick(Sender: TObject);
    procedure BListaConceptoClick(Sender: TObject);
    procedure RBTodosConceptoClick(Sender: TObject);
    procedure RBRangoConceptoClick(Sender: TObject);
    procedure RBListaConceptoClick(Sender: TObject);
  private
    { Private declarations }
    FRangoConcepto: eTipoRangoActivo;
    procedure EnabledBotonesConcepto( eTipo: eTipoRangoActivo );
    function GetConceptos : string;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizNomRecalculoAcumulado_DevEx: TWizNomRecalculoAcumulado_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaDialogo,
     DCliente,
     DProcesos,
     dCatalogos,
     ZetaCommonTools, ZetaClientTools;

{$R *.DFM}

procedure TWizNomRecalculoAcumulado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          cdsConceptos.Conectar;
     end;
     with dmCliente.GetDatosPeriodoActivo do
     begin
          iMes.Valor := ( Mes - 1 );
          MesFinal.Valor := ( Mes - 1 );
          UpDownYear.Position := Year;
          iYear.Valor := Year;
     end;
     HelpContext := H33418_Recalcular_acumulados;
     ParametrosControl := iYear;
     lAcumuladosClick(True);
     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
     EnabledBotonesConcepto( raTodos );
end;

procedure TWizNomRecalculoAcumulado_DevEx.lAcumuladosClick(lHabilitar: Boolean);
begin
     inherited;
     iMes.Enabled := lHabilitar;
     iMesLBL.Enabled := lHabilitar;
     iYear.Enabled := lHabilitar;
     iYearLBL.Enabled := lHabilitar;
     UpDownYear.Enabled := lHabilitar;
     lblMesFinal.Enabled := lHabilitar;
     MesFinal.Enabled := lHabilitar;
end;

procedure TWizNomRecalculoAcumulado_DevEx.RBListaConceptoClick(Sender: TObject);
begin
     inherited;
     EnabledBotonesConcepto( raLista );
end;

procedure TWizNomRecalculoAcumulado_DevEx.RBRangoConceptoClick(Sender: TObject);
begin
     inherited;
     EnabledBotonesConcepto( raRango );
end;

procedure TWizNomRecalculoAcumulado_DevEx.RBTodosConceptoClick(Sender: TObject);
begin
     inherited;
     EnabledBotonesConcepto( raTodos );
end;

procedure TWizNomRecalculoAcumulado_DevEx.BFinalConceptoClick(Sender: TObject);
begin
     inherited;
     BuscaConceptos( FALSE, EFinalConcepto );
end;

procedure TWizNomRecalculoAcumulado_DevEx.BInicialConceptoClick(Sender: TObject);
begin
     inherited;
     BuscaConceptos( FALSE, EInicialConcepto );
end;

procedure TWizNomRecalculoAcumulado_DevEx.BListaConceptoClick(Sender: TObject);
begin
     inherited;
     BuscaConceptos( TRUE, EListaConcepto );
end;

procedure TWizNomRecalculoAcumulado_DevEx.btnListaConceptoClick(Sender: TObject);
begin
     inherited;
     {with eListaConcepto do
     begin
          Text := BuscaConceptos( True, Text );
     end;}
end;

procedure TWizNomRecalculoAcumulado_DevEx.BuscaConceptos( const lConcatena: Boolean; oEdit: TZetaEdit_DevEx );
var
   sLlave, sKey, sDescripcion: String;
begin
     sLlave := oEdit.Text;
     with dmCatalogos.cdsConceptos do
     begin
          Search_DevEx( 'CO_NUMERO < 1000', sKey, sDescripcion );
          if lConcatena and ZetaCommonTools.StrLleno( sLlave ) then
          begin
               if strLleno( sKey ) then
                   oEdit.Text := sLlave + ',' + sKey
               else
                   oEdit.Text := sLlave
          end
          else if strLleno( sKey ) then
               oEdit.Text := sKey;
     end;
end;

procedure TWizNomRecalculoAcumulado_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
           AddInteger( 'A�o', iYear.ValorEntero );
           AddString( 'Mes inicial' , ObtieneElemento(lfMeses, iMes.Valor));
           AddString( 'Mes final' , ObtieneElemento(lfMeses, MesFinal.Valor));
     end;
     with ParameterList do
     begin
          AddInteger( 'Mes', iMes.Valor + 1 );
          AddInteger( 'MesFinal', MesFinal.Valor + 1 );
          AddInteger( 'Year', iYear.ValorEntero );
          AddBoolean( 'Acumulados', True );
          AddBoolean( 'Ahorros', False );
          AddBoolean( 'Prestamos', False );
          AddString ( 'Conceptos', GetConceptos );
          case FRangoConcepto of
               raRango: AddInteger( 'RangoConceptos', ord( raRango ) );
               raLista: AddInteger( 'RangoConceptos', ord( raLista ) );
               raTodos: AddInteger( 'RangoConceptos', ord( raTodos ) );
          end;
     end;
end;

procedure TWizNomRecalculoAcumulado_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
  var
     iConceptoIni, iConceptoFin: Integer;
  procedure VerificarConceptos( oEdit: TZetaEdit_DevEx; const sMensaje, sLimite: String; var CanMove: Boolean );
  begin
       if CanMove and StrVacio( oEdit.Text ) then
       begin
            ZetaDialogo.ZError( Caption, sMensaje, 0 );
            CanMove := FALSE;
       end;
       if CanMove and ( Length( oEdit.Text ) > ZetaCommonClasses.K_MAX_VARCHAR ) then
       begin
            ZetaDialogo.ZError( Caption, sLimite, 0 );
            CanMove := FALSE;
       end;
  end;
  procedure ValidarNumeroConceptos( oEdit: TZetaEdit_DevEx; const sMensaje: String; var CanMove: Boolean );
  begin
       if CanMove and not ValidaListaNumeros( oEdit.Valor ) then
       begin
            ZError( Caption, sMensaje, 0 );
            CanMove := False;
       end
  end;
begin
     iConceptoIni := 0;
     iConceptoFin := 0;
     ParametrosControl := nil;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          CanMove := ( iMes.Valor <= MesFinal.Valor );
          if not CanMove then
          begin
               ZError( Caption, 'El Mes Inicial Debe Ser Menor o Igual al Mes Final', 0 );
               ActiveControl := iMes;
          end;
          case FRangoConcepto of
               raRango:
               begin
                    VerificarConceptos( EInicialConcepto, 'Debe seleccionar un concepto inicial', 'El concepto inicial excede los 255 caracteres', CanMove );
                    VerificarConceptos( EFinalConcepto,   'Debe seleccionar un concepto final', 'El concepto final excede los 255 caracteres', CanMove );
                    ValidarNumeroConceptos( EInicialConcepto, 'El Concepto Inicial no es un n�mero', CanMove );
                    ValidarNumeroConceptos( EFinalConcepto, 'El Concepto Final no es un n�mero', CanMove );
                    if CanMove then
                    begin
                         try
                            iConceptoIni := StrToInt( EInicialConcepto.Valor );
                         except
                               CanMove := FALSE;
                         end;
                         try
                            iConceptoFin := StrToInt( EFinalConcepto.Valor );
                         except
                               CanMove := FALSE;
                         end;
                         if CanMove and not ( iConceptoIni <= iConceptoFin ) then
                         begin
                              ZError( Caption, 'El Concepto Inicial Debe Ser Menor o Igual al Concepto Final', 0 );
                              ActiveControl := EInicialConcepto;
                              CanMove := FALSE;
                         end;
                    end;
               end;
               raLista:
               begin
                    VerificarConceptos( EListaConcepto, 'Debe seleccionar al menos un concepto', 'La lista de conceptos excede los 255 caracteres', CanMove );
                    ValidarNumeroConceptos( EListaConcepto, 'La lista de conceptos no es v�lida', CanMove );
               end;
          end;
     end;
     inherited;
end;

function TWizNomRecalculoAcumulado_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.RecalculoAcumulado(ParameterList);
end;

procedure TWizNomRecalculoAcumulado_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'El proceso recalcula los acumulados considerando las n�minas afectadas para el rango de meses y grupo de empleados seleccionados.';
end;

procedure TWizNomRecalculoAcumulado_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.Adelante then
          if Wizard.EsPaginaActual( Parametros ) then
             ParametrosControl := iYear;
     inherited;
end;

procedure TWizNomRecalculoAcumulado_DevEx.EnabledBotonesConcepto( eTipo : eTipoRangoActivo );
var
   lLista, lRango: Boolean;
begin
     FRangoConcepto := eTipo;
     lRango := ( eTipo = raRango );
     lLista := ( eTipo = raLista );
     lbInicialConcepto.Enabled := lRango;
     lbFinalConcepto.Enabled := lRango;
     EInicialConcepto.Enabled := lRango;
     EFinalConcepto.Enabled := lRango;
     bInicialConcepto.Enabled := lRango;
     bFinalConcepto.Enabled := lRango;
     EListaConcepto.Enabled := lLista;
     bListaConcepto.Enabled := lLista;
end;

function TWizNomRecalculoAcumulado_DevEx.GetConceptos : string;
begin
     Result := VACIO;
     case FRangoConcepto of
          raRango: Result := ZetaClientTools.GetFiltroRango( 'CO_NUMERO', Trim( EInicialConcepto.Text ), Trim( EFinalConcepto.Text ) );
          raLista: Result := ZetaClientTools.GetFiltroLista( 'CO_NUMERO', Trim( EListaConcepto.Text ) );
     end;
end;

end.
