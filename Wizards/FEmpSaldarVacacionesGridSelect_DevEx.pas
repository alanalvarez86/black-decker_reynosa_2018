unit FEmpSaldarVacacionesGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FEmpVacacionesGridSelect_DevEx, Db, Grids, DBGrids, ZetaDBGrid, StdCtrls,
  Buttons, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, ImgList, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpSaldarVacacionesGridSelect_DevEx = class(TEmpVacacionesGridSelect_DevEx)
    ANIV_VENCE: TcxGridDBColumn;
    FEC_LIMITE: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpSaldarVacacionesGridSelect_DevEx: TEmpSaldarVacacionesGridSelect_DevEx;

implementation

{$R *.DFM}

procedure TEmpSaldarVacacionesGridSelect_DevEx.FormShow(Sender: TObject);
begin
  inherited;
   with DataSet do
     begin
          MaskFecha( 'ANIV_VENCE' ); //PENDIENTE: NO SIRVE SE DEBE HACER EN EL GRID.
          MaskFecha( 'FEC_LIMITE' );
     end;
end;

end.
