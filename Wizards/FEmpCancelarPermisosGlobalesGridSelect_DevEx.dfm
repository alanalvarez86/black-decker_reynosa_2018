inherited EmpCancelarPermisosGlobalesGridSelect_DevEx: TEmpCancelarPermisosGlobalesGridSelect_DevEx
  Left = 274
  Top = 422
  Width = 750
  Caption = 'Seleccione los empleados deseados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 734
    inherited OK_DevEx: TcxButton
      Left = 570
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 649
      Cancel = True
    end
  end
  inherited PanelSuperior: TPanel
    Width = 734
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 734
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 64
      end
      object PM_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha inicio'
        DataBinding.FieldName = 'PM_FEC_INI'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 64
      end
      object PM_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as'
        DataBinding.FieldName = 'PM_DIAS'
        Width = 64
      end
      object PM_CLASIFI: TcxGridDBColumn
        Caption = 'Clase'
        DataBinding.FieldName = 'PM_CLASIFI'
        Width = 64
      end
      object PM_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'PM_TIPO'
        Width = 64
      end
      object PM_NUMERO: TcxGridDBColumn
        Caption = 'Referencia'
        DataBinding.FieldName = 'PM_NUMERO'
        Width = 64
      end
      object PM_CAPTURA: TcxGridDBColumn
        Caption = 'Fecha captura'
        DataBinding.FieldName = 'PM_CAPTURA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 84
      end
      object US_CODIGO: TcxGridDBColumn
        Caption = 'Usuario'
        DataBinding.FieldName = 'US_CODIGO'
        Width = 64
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 680
    Top = 8
  end
end
