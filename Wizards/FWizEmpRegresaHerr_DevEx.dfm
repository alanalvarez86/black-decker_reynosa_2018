inherited WizEmpRegresaHerr_DevEx: TWizEmpRegresaHerr_DevEx
  Left = 387
  Top = 302
  Caption = 'Regresar Herramienta'
  ClientHeight = 486
  ClientWidth = 479
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 479
    Height = 486
    inherited Parametros: TdxWizardControlPage
      Header.Description = 'Proceso para regresar herramientas a un grupo de empleados.'
      Header.Title = 'Regresar Herramienta'
      object GBDevolucion: TcxGroupBox
        Left = 0
        Top = 0
        Align = alTop
        TabOrder = 0
        Height = 193
        Width = 457
        object LblFecDev: TLabel
          Left = 40
          Top = 72
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fech&a:'
          FocusControl = KT_FEC_FIN
          Transparent = True
        end
        object LblMotivo: TLabel
          Left = 40
          Top = 96
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = '&Motivo:'
          FocusControl = KT_MOT_FIN
          Transparent = True
        end
        object LblRenova: TLabel
          Left = 16
          Top = 120
          Width = 61
          Height = 13
          Alignment = taRightJustify
          Caption = 'Renovaci'#243'n:'
          FocusControl = RGRenovacion
          Transparent = True
        end
        object KT_FEC_FIN: TZetaFecha
          Left = 80
          Top = 64
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '13/jun/00'
          Valor = 36690.000000000000000000
        end
        object KT_MOT_FIN: TZetaKeyLookup_DevEx
          Left = 80
          Top = 88
          Width = 310
          Height = 21
          LookupDataset = dmTablas.cdsMotTool
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
        end
        object RGRenovacion: TcxRadioGroup
          Left = 80
          Top = 112
          Properties.Items = <
            item
              Caption = 'Conservar Referencia'
              Value = 'S'#237
            end
            item
              Caption = 'Cambiar Referencia a:'
              Value = 'No'
            end>
          ItemIndex = 0
          TabOrder = 3
          OnClick = RGRenovacionClick
          Height = 69
          Width = 308
        end
        object RefReemplazo: TEdit
          Left = 217
          Top = 155
          Width = 169
          Height = 21
          TabOrder = 4
        end
        object RGTipo: TcxRadioGroup
          Left = 0
          Top = 0
          Align = alCustom
          Properties.Columns = 2
          Properties.Items = <
            item
              Caption = '&Devoluci'#243'n'
              Value = '0'
            end
            item
              Caption = '&Renovaci'#243'n'
              Value = '1'
            end>
          ItemIndex = 0
          TabOrder = 0
          OnClick = RGTipoClick
          Height = 40
          Width = 457
        end
      end
      object GBFiltros: TcxGroupBox
        Left = 0
        Top = 195
        Align = alBottom
        Caption = ' Filtrar por: '
        TabOrder = 1
        Height = 151
        Width = 457
        object Label2: TLabel
          Left = 264
          Top = 102
          Width = 6
          Height = 13
          Caption = 'a'
          Transparent = True
        end
        object CBTool: TcxCheckBox
          Left = 16
          Top = 25
          Caption = '           Herramienta:'
          Properties.Alignment = taRightJustify
          TabOrder = 0
          Transparent = True
          OnClick = CheckBoxClick
          Width = 115
        end
        object TO_CODIGO: TZetaKeyLookup_DevEx
          Left = 136
          Top = 24
          Width = 310
          Height = 21
          LookupDataset = dmCatalogos.cdsTools
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 75
        end
        object KT_TALLA: TZetaKeyLookup_DevEx
          Left = 136
          Top = 72
          Width = 310
          Height = 21
          LookupDataset = dmTablas.cdsTallas
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 5
          TabStop = True
          WidthLlave = 75
        end
        object CBTalla: TcxCheckBox
          Left = 16
          Top = 73
          Caption = '                      Talla:'
          Properties.Alignment = taRightJustify
          TabOrder = 4
          Transparent = True
          OnClick = CheckBoxClick
          Width = 115
        end
        object CBReferencia: TcxCheckBox
          Left = 16
          Top = 49
          Caption = '            Referencia:'
          Properties.Alignment = taRightJustify
          TabOrder = 2
          Transparent = True
          OnClick = CheckBoxClick
          Width = 115
        end
        object KT_REFEREN: TEdit
          Left = 136
          Top = 48
          Width = 169
          Height = 21
          TabOrder = 3
        end
        object CBFecha: TcxCheckBox
          Left = 16
          Top = 97
          Caption = ' Fecha de Entrega:'
          Properties.Alignment = taRightJustify
          TabOrder = 6
          Transparent = True
          OnClick = CheckBoxClick
          Width = 115
        end
        object FechaIni: TZetaFecha
          Left = 136
          Top = 96
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 7
          Text = '27/dic/00'
          Valor = 36887.000000000000000000
        end
        object FechaFin: TZetaFecha
          Left = 280
          Top = 96
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 8
          Text = '27/dic/00'
          Valor = 36887.000000000000000000
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 251
        Width = 457
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 457
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 389
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 30
      end
      inherited sFiltroLBL: TLabel
        Left = 55
      end
      inherited Seleccionar: TcxButton
        Left = 158
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 82
      end
      inherited sFiltro: TcxMemo
        Left = 82
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 82
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 528
    Top = 13
  end
end
