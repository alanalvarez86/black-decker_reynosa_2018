inherited EmpImportarPrestamosGridShow_DevEx: TEmpImportarPrestamosGridShow_DevEx
  Left = 232
  Top = 542
  Width = 643
  Caption = 'Lista de Errores Detectados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 627
    inherited OK_DevEx: TcxButton
      Left = 463
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 542
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 627
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object RENGLON: TcxGridDBColumn
        Caption = 'Rengl'#243'n'
        DataBinding.FieldName = 'RENGLON'
        Width = 64
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 64
      end
      object PR_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'PR_TIPO'
        Width = 64
      end
      object PR_REFEREN: TcxGridDBColumn
        Caption = 'Referencia'
        DataBinding.FieldName = 'PR_REFEREN'
      end
      object PR_FECHA: TcxGridDBColumn
        Caption = 'Fecha Inicio'
        DataBinding.FieldName = 'PR_FECHA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object PR_MONTO: TcxGridDBColumn
        Caption = 'Monto'
        DataBinding.FieldName = 'PR_MONTO'
      end
      object NUM_ERROR: TcxGridDBColumn
        Caption = 'Errores'
        DataBinding.FieldName = 'NUM_ERROR'
        Width = 64
      end
      object DESC_ERROR: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Errores'
        DataBinding.FieldName = 'DESC_ERROR'
        Width = 64
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
