unit FWizEmpBaseFiltro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZCXBaseWizardFiltro, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  ExtCtrls, ZetaKeyLookup_DevEx, StdCtrls, cxButtons, ZetaEdit,
  cxRadioGroup, cxTextEdit, cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses,
  cxImage, dxCustomWizardControl, dxWizardControl, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters;

type
  TWizEmpBaseFiltro = class(TBaseCXWizardFiltro)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WizEmpBaseFiltro: TWizEmpBaseFiltro;

implementation

{$R *.dfm}

end.
