unit FWizSistImportarEnrolamientoMasivo_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, ZetaKeyCombo,
  ExtCtrls, ZetaWizard, FWizSistBaseFiltro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, ZetaEdit, cxRadioGroup, cxTextEdit, cxMemo,
  ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizSistImportarEnrolamientoMasivo_DevEx = class(TWizSistBaseFiltro)
    Panel1: TPanel;
    Archivo: TEdit;
    Formato: TZetaKeyCombo;
    ArchivoSeek: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    OpenDialog: TOpenDialog;
    PanelAnimation: TPanel;
    Animate: TAnimate;
    PanelMensaje: TPanel;
    zkcAccion: TZetaKeyCombo;
    Label3: TLabel;
    procedure ArchivoSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    FVerificaASCII: Boolean;
    ArchivoVerif : String;
    function LeerArchivo: Boolean;
    procedure CargaListaVerificaASCII;
    function GetArchivo: String;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }

  end;

var
  WizSistImportarEnrolamientoMasivo_DevEx: TWizSistImportarEnrolamientoMasivo_DevEx;

implementation

{$R *.DFM}

uses DProcesos,
     ZetaDialogo,
     ZetaMessages,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZAsciiTools,
     DGlobal,
     ZGlobalTress,
     ZBaseSelectGrid_DevEx,
     ZBaseGridShow_DevEx,
     FSistImportarEnrolamientoMasivoGridShow_DevEx;

procedure TWizSistImportarEnrolamientoMasivo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PanelAnimation.Visible := FALSE;
     ParametrosControl := Archivo;
     zkcAccion.ItemIndex := Ord(arIgnorar); 
     Formato.ItemIndex := Ord( faASCIIDel );
     ArchivoVerif:= VACIO;
     HelpContext := H_Importar_Enrolamiento_Masivo;
end;

procedure TWizSistImportarEnrolamientoMasivo_DevEx.CargaParametros;
begin


     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'Formato', Formato.Valor );
          AddString( 'Archivo', Archivo.Text );
          AddInteger( 'Accion', zkcAccion.Valor );
     end;

    with Descripciones do
     begin
          AddString( 'Archivo', Archivo.Text );
          AddString( 'Formato ASCII', ObtieneElemento( lfFormatoASCII, Formato.Valor ) );
          AddString( 'A Usuarios Existentes',  zkcAccion.Text );

     end;
end;
procedure TWizSistImportarEnrolamientoMasivo_DevEx.CargaListaVerificaASCII;
begin
     with dmProcesos do
     begin
          ImportarEnrolamientoMasivoGetListaASCII( ParameterList );
          if ( ErrorCount > 0 ) then                 // Hubo Errores
          begin
               with dmProcesos do
               begin
                    ZAsciiTools.FiltraASCII( cdsDataSet, True );
                    FVerificaASCII := ZBaseGridShow_DevEx.GridShow( cdsDataset, TSistImportarEnrolamientoMasivoGridShow_DevEx );
                    ZAsciiTools.FiltraASCII( cdsDataSet, False );
               end;
          end
          else
              FVerificaASCII := True;
     end;
end;

function TWizSistImportarEnrolamientoMasivo_DevEx.GetArchivo: String;
begin
     Result := Archivo.Text;
end;

function TWizSistImportarEnrolamientoMasivo_DevEx.LeerArchivo: Boolean;
begin
     PanelAnimation.Visible := TRUE;
     Application.ProcessMessages;
     CargaParametros;
     CargaListaVerificaASCII;
     Result := FVerificaASCII;
     PanelAnimation.Visible := FALSE;
     if Result then
        ArchivoVerif := GetArchivo
     else
        ArchivoVerif := VACIO;
end;
procedure TWizSistImportarEnrolamientoMasivo_DevEx.ArchivoSeekClick(
  Sender: TObject);
begin
     inherited;
     Archivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, Archivo.Text, 'dat' );
end;

procedure TWizSistImportarEnrolamientoMasivo_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Parametros ) then
               begin
                    if ZetaCommonTools.StrVacio( GetArchivo ) then
                       CanMove := Error( 'Debe Especificarse un Archivo a Importar', Parametros )
                    else
                       if not FileExists( GetArchivo ) then
                          CanMove := Error( 'El Archivo ' + GetArchivo + ' No Existe', Parametros )
                       else
                       begin
                            if ( ArchivoVerif <> GetArchivo ) or
                               ( ZetaDialogo.ZConfirm( Caption, 'El Archivo ' + GetArchivo + ' Ya Fu� Verificado !' + CR_LF +
                                                       'Volver a Verificar ?', 0, mbYes ) ) then
                               CanMove := LeerArchivo
                            else
                               CanMove := TRUE;
                       end;
               end;
          end;
     end;
end;

function TWizSistImportarEnrolamientoMasivo_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarEnrolamientoMasivo( ParameterList );
end;

procedure TWizSistImportarEnrolamientoMasivo_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     zkcAccion.SelStart := 1; 
end;



procedure TWizSistImportarEnrolamientoMasivo_DevEx.WizardAfterMove(
  Sender: TObject);
begin
  inherited;
   if Wizard.EsPaginaActual(Parametros) then
     ParametrosControl := Archivo;
end;

end.
