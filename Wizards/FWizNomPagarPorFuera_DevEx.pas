unit FWizNomPagarPorFuera_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls,
     ZetaEdit, ZetaDBTextBox, FWizNomBase_DevEx, ZetaKeyLookup_DevEx, 
     cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
     cxRadioGroup, cxTextEdit, cxMemo, cxButtons,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl;

type
  TWizNomPagarPorFuera_DevEx = class(TWizNomBase_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean;override;
    function Verificar: Boolean;override;
    procedure CargaListaVerificacion;override;
  public
    { Public declarations }
  end;

var
  WizNomPagarPorFuera_DevEx: TWizNomPagarPorFuera_DevEx;

implementation

uses ZetaCommonClasses,
     ZBaseSelectGrid_DevEx,
     FEmpleadoGridSelect_DevEx,
     DProcesos;

{$R *.DFM}

procedure TWizNomPagarPorFuera_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H33413_Pagar_por_fuera;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;

end;

procedure TWizNomPagarPorFuera_DevEx.CargaListaVerificacion;
begin
     dmProcesos.PagarPorFueraGetLista( ParameterList );
end;

function TWizNomPagarPorFuera_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

function TWizNomPagarPorFuera_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.PagarPorFuera(ParameterList, Verificacion);
end;

procedure TWizNomPagarPorFuera_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  //DevEx
  Advertencia.Caption :=
     'Al aplicar el proceso se marcar�n las n�minas de los empleados indicados, ' +
     'y no podr�n ser modificadas, ni se les podr� agregar nuevas excepciones de montos';

end;

end.

