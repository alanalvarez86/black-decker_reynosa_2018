unit FEmpImportarOrganigramaGridShow_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, ExtCtrls, ZBaseGridShow_DevEx,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpImportarOrganigramaGridShow_DevEx = class(TBaseGridShow_DevEx)
    RENGLON: TcxGridDBColumn;
    CB_JEFE: TcxGridDBColumn;
    PLAZA_JEF: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    PL_NOMBRE: TcxGridDBColumn;
    PL_TIREP: TcxGridDBColumn;
    PL_FEC_INI: TcxGridDBColumn;
    PL_FEC_FIN: TcxGridDBColumn;
    NUM_ERROR: TcxGridDBColumn;
    DESC_ERROR: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    procedure Connect;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpImportarOrganigramaGridShow_DevEx: TEmpImportarOrganigramaGridShow_DevEx;

implementation

{$R *.dfm}

uses
  DGlobal,ZetaCommonLists, ZetaCommonTools, ZetaCommonClasses,
  ZBaseDlgModal_DevEx;

procedure TEmpImportarOrganigramaGridShow_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Connect;
end;

procedure TEmpImportarOrganigramaGridShow_DevEx.Connect;
begin
     inherited;
end;

end.
