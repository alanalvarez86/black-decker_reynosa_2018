unit FWizEmpReiniciarCapacitacionesSTPS_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FWizEmpFoliarCapacitacionesSTPS_DevEx, ComCtrls, 
  ZetaEdit, StdCtrls, Buttons, ZetaFecha, Mask, ZetaNumero, ZetaWizard,
  ExtCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit,
  cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizEmpReiniciarCapacitacionesSTPS_DevEx = class(TWizEmpFoliarCapacitacionesSTPS_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizEmpReiniciarCapacitacionesSTPS_DevEx: TWizEmpReiniciarCapacitacionesSTPS_DevEx;

implementation

uses 
    DCliente,
    DProcesos,
    ZetaCommonClasses,
    ZetaCommonTools;

{$R *.dfm}

procedure TWizEmpReiniciarCapacitacionesSTPS_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H487000_Reiniciar_Capacitaciones_STPS;
end;


procedure TWizEmpReiniciarCapacitacionesSTPS_DevEx.CargaParametros;
begin
     inherited CargaParametros; // ??
     {with Descripciones do
     begin
          AddDate( 'Fecha Inicio', fechaInicio.Valor ); 
          AddDate( 'Fecha Fin', fechaFin.Valor );
          AddInteger( 'Folio Inicial', FolioInicial.ValorEntero );
          AddDate( 'Fecha', dmCliente.FechaDefault );
          AddString('Reinicio de Folio', BoolAsSiNo(true));
     end; }
     with ParameterList do
     begin
          AddDate( 'FechaInicio', fechaInicio.Valor );
          AddDate( 'FechaFin', fechaFin.Valor );
          AddInteger( 'FolioInicial', FolioInicial.ValorEntero );
          AddDate('Fecha', dmCliente.FechaDefault);
          AddBoolean('ReinicioFolio', true);
     end;
end;

procedure TWizEmpReiniciarCapacitacionesSTPS_DevEx.FormShow(
  Sender: TObject);
begin
  inherited;
  Advertencia.Caption :=' Al aplicar el proceso se reiniciará el folio a las capacitaciones de la Secretaría del Trabajo y Previsión Social.';
end;

end.
