unit FWizEmpCancelarPermisosGlobales_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  ExtCtrls, DBCtrls, ZetaKeyCombo, Mask, ZetaFecha,
  FWizEmpBaseFiltro, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit,
  cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage,
  dxCustomWizardControl, dxWizardControl, cxCheckBox;

type
  TWizEmpCancelarPermisosGlobales_DevEx = class(TWizEmpBaseFiltro)
    Fecha: TZetaFecha;
    FechaLBL: TLabel;
    TipoFechaLBL: TLabel;
    TipoFecha: TZetaKeyCombo;
    TipoMovimiento: TZetaKeyCombo;
    TipoMovimientoLBL: TLabel;
    GroupBox1: TcxGroupBox;
    PM_CLASIFI: TZetaKeyCombo;
    PM_TIPO: TZetaKeyLookup_DevEx;
    PorTipo: TcxCheckBox;
    PorReferencia: TcxCheckBox;
    PorClase: TcxCheckBox;
    PM_NUMERO: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx     //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure CheckBoxClick(Sender: TObject);
    
  private
    { Private declarations }
    procedure SetControls;
  protected
    { Protected declarations}
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpCancelarPermisosGlobales_DevEx: TWizEmpCancelarPermisosGlobales_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZBaseSelectGrid_DevEx,
     ZetaCommonTools,
     dTablas,
     FToolsRH,
     FEmpCancelarPermisosGlobalesGridSelect_DevEx, 
     ZcxWizardBasico;

{$R *.DFM}

{ TWizCancelarEmpPermisosGlobales }

procedure TWizEmpCancelarPermisosGlobales_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := Fecha;
     Fecha.Valor := dmCliente.FechaDefault;
     TipoFecha.ItemIndex := Ord(tkKardex);
     TipoMovimiento.ItemIndex := Ord(cmGlobales);
     HelpContext:= H_CANCELAR_PERMISOS_GLOB;

     with PM_TIPO do
     begin
          LookupDataSet := dmTablas.cdsIncidencias;
          Filtro := Format( K_INCIDENCIAS_FILTRO, [ Ord( eiPermiso ) ] );
     end;
     SetControls;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizEmpCancelarPermisosGlobales_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmTablas.cdsIncidencias.Conectar;
     Advertencia.Caption := 'Al ejecutar el proceso se eliminará del expediente de los empleados seleccionados los permisos especificados.';
end;

procedure TWizEmpCancelarPermisosGlobales_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CancelarPermisosGlobalesGetLista( ParameterList );
end;

procedure TWizEmpCancelarPermisosGlobales_DevEx.CargaParametros;
begin

     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'Fecha', Fecha.Valor );
          AddInteger( 'TipoFecha', TipoFecha.ItemIndex );
          AddInteger( 'TipoMovimiento', TipoMovimiento.ItemIndex );

          AddBoolean( 'FiltrarClase', PorClase.Checked );
          AddInteger( 'Clase', PM_CLASIFI.ItemIndex );

          AddBoolean( 'FiltrarTipo', PorTipo.Checked );
          AddString( 'Tipo', PM_TIPO.Llave );

          AddBoolean( 'FiltrarRef', PorReferencia.Checked );
          AddString( 'Referencia', PM_NUMERO.Text );
     end;

     with Descripciones do
     begin
          AddDate( 'Fecha', Fecha.Valor );
          AddString( 'Tipo de fecha', ObtieneElemento( lfTipoKarFecha, TipoFecha.ItemIndex ) );
          AddString( 'Cancelar modificación', ObtieneElemento( lfCancelaMod, TipoMovimiento.ItemIndex ) );
          if PorClase.Checked then
             AddString( 'Clase', ObtieneElemento( lfTipoPermiso,PM_CLASIFI.ItemIndex ) );
          if PorTipo.Checked then
             AddString( 'Tipo', PM_TIPO.Llave + ': ' + PM_TIPO.Descripcion );
          if PorReferencia.Checked then
             AddString( 'Referencia', PM_NUMERO.Text );
     end;

end;

function TWizEmpCancelarPermisosGlobales_DevEx.EjecutarWizard: Boolean;
begin
      Result := dmProcesos.CancelarPermisosGlobales( ParameterList, Verificacion );
end;

function TWizEmpCancelarPermisosGlobales_DevEx.Verificar: Boolean;
begin
      Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpCancelarPermisosGlobalesGridSelect_DevEx );
end;

procedure TWizEmpCancelarPermisosGlobales_DevEx.SetControls;
begin
     PM_CLASIFI.Enabled := PorClase.Checked;
     PM_TIPO.Enabled    := PorTipo.Checked;
     PM_NUMERO.Enabled  := PorReferencia.Checked;
end;

procedure TWizEmpCancelarPermisosGlobales_DevEx.CheckBoxClick(Sender: TObject);
begin
     SetControls;
end;

procedure TWizEmpCancelarPermisosGlobales_DevEx.WizardBeforeMove(Sender: TObject;
          var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if ( PorClase.Checked ) and ( PM_CLASIFI.ItemIndex = -1 ) then
               begin
                    CanMove := Error('Debe especificarse la clase de permiso', PM_CLASIFI );
               end
               else if ( PorTipo.Checked ) and StrVacio( PM_TIPO.Llave ) then
               begin
                    CanMove := Error( 'Debe especificarse el tipo de permiso', PM_TIPO);
               end
               else if ( PorReferencia.Checked ) and StrVacio( PM_NUMERO.Text ) then
               begin
                    CanMove := Error( 'Debe especificarse la referencia', PM_NUMERO );
               end;
          end;
     end;
end;


//DevEx     //Agregado para enfocar un control
procedure TWizEmpCancelarPermisosGlobales_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
         ParametrosControl := Fecha
     else
         ParametrosControl := nil;
     inherited;
end;


end.
