unit FWizNomPTUPago_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     FWizNomBase_DevEx,
     ZetaEdit,
     ZetaDBTextBox,
     ZetaWizard,
     ZetaNumero, 
     cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomPTUPago_DevEx = class(TWizNomBase_DevEx)
    YearLBL: TLabel;
    Year: TZetaNumero;
    ConceptoLBL: TLabel;
    Concepto: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Agregado para enfocar correctamente
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomPTUPago_DevEx: TWizNomPTUPago_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaDialogo,
     ZBaseSelectGrid_DevEx,
     FEmpleadoGridSelect_DevEx,
     DCliente,
     DProcesos,
     DCatalogos;

{$R *.DFM}

procedure TWizNomPTUPago_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Year.Valor := ( dmCliente.YearDefault - 1 );
     ParametrosControl := Year;
     HelpContext := H33510_Pagar_PTU;
     Concepto.LookupDataset := dmCatalogos.cdsConceptos;

     //DevEx(by am): Es necesario especificar las paginas del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizNomPTUPago_DevEx.FormShow(Sender: TObject);
begin
     dmCatalogos.cdsConceptos.Conectar;
     inherited;
     Advertencia.Caption := 'Al aplicar el proceso se har� el reparto de utilidades a los empleados indicados en la n�mina activa.'; 
end;

procedure TWizNomPTUPago_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if ( Year.ValorEntero <= 0 ) then
               begin
                    zError( Caption, '� A�o no fu� especificado !', 0 );
                    CanMove := False;
                    ActiveControl := Year;
               end
               else
               if ( Concepto.Valor = 0 ) then
               begin
                    zError( Caption, '� No se especific� el Concepto de N�mina !', 0 );
                    CanMove := False;
                    ActiveControl := Concepto;
               end;
          end;
     end;
     inherited;
end;

procedure TWizNomPTUPago_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control dependiendo del tipo de proceso
     with Wizard do
     begin
     if Adelante then
        if EsPaginaActual( Parametros ) then
           ParametrosControl := Year;
     end;
     inherited;
end;

procedure TWizNomPTUPago_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with Descripciones do
     begin
          AddInteger( 'A�o', Year.ValorEntero );
          AddString( 'Concepto', GetDescripLlave( Concepto ) );
     end;

     //inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'YearPTU', Year.ValorEntero );
          AddInteger( 'Concepto', Concepto.Valor );
     end;
end;

procedure TWizNomPTUPago_DevEx.CargaListaVerificacion;
begin
     dmProcesos.PTUPagoGetLista( ParameterList );
end;

function TWizNomPTUPago_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

function TWizNomPTUPago_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.PTUPago( ParameterList, Verificacion );
end;

end.



