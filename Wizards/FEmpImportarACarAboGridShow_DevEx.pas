unit FEmpImportarACarAboGridShow_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls, ZBaseGridShow_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpImportarACarAboGridShow_DevEx = class(TBaseGridShow_DevEx)
    RENGLON: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    AH_TIPO: TcxGridDBColumn;
    CR_FECHA: TcxGridDBColumn;
    TIPO_MOV: TcxGridDBColumn;
    MONTO: TcxGridDBColumn;
    NUM_ERROR: TcxGridDBColumn;
    DESC_ERROR: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpImportarACarAboGridShow_DevEx: TEmpImportarACarAboGridShow_DevEx;

implementation

{$R *.DFM}

procedure TEmpImportarACarAboGridShow_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     DataSet.MaskPesos( 'MONTO' );
end;

end.
