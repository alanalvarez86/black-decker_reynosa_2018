unit FWizAsignaNumBiometricos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, FWizAsisBaseFiltro, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo,
  ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl, FWizSistBaseFiltro;

type
  TWizAsignaNumBiometricos_DevEx = class(TWizsistBaseFiltro)
    luGrupo: TZetaKeyLookup_DevEx;
    Label1: TLabel;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
  end;

var
  WizAsignaNumBiometricos_DevEx: TWizAsignaNumBiometricos_DevEx;

implementation

uses dSistema,
     dProcesos,
     dCliente,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZBaseSelectGrid_DevEx,
     FEmpBiometricoGridSelect_DevEx;

{$R *.dfm}

procedure TWizAsignaNumBiometricos_DevEx.CargaParametros;
begin
     inherited ;
     with ParameterList do
     begin
          AddString( 'Grupo', luGrupo.Llave );
          AddString( 'Empresa', dmCliente.Compania );
     end;
     with Descripciones do
     begin
          AddString( 'Grupo de terminales', luGrupo.Llave );
     end;
end;

function TWizAsignaNumBiometricos_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AsignaNumBiometricos( ParameterList, Verificacion );
end;

procedure TWizAsignaNumBiometricos_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.Adelante then
     begin
          if( WizardControl.ActivePage = Parametros )then
          begin
               if StrVacio( luGrupo.Llave ) then
                 CanMove := Error( '� Error ! Es necesario especificar un grupo de terminales', luGrupo )
          end;
     end;
end;

procedure TWizAsignaNumBiometricos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_PROCESOS_ASIGNUMBIOMETRICO;
     with dmSistema do
     begin
          cdsListaGrupos.Conectar;
          luGrupo.LookUpDataSet := cdsListaGrupos;
     end;
end;

procedure TWizAsignaNumBiometricos_DevEx.CargaListaVerificacion;
begin
     inherited;
     dmProcesos.EmpleadosSinBiometricosGetLista( ParameterList );
end;

function TWizAsignaNumBiometricos_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpBiometricoGridSelect_DevEx );
end;

procedure TWizAsignaNumBiometricos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetFocusedControl( self.luGrupo )
end;

end.
