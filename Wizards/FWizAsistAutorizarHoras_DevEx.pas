unit FWizAsistAutorizarHoras_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ComCtrls, Mask, ExtCtrls,
     ZetaEdit,
     ZetaWizard,
     ZetaNumero,
     ZetaKeyCombo,
     ZetaFecha, ZCXBaseWizardFiltro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit,
  cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage,
  dxCustomWizardControl, dxWizardControl;

type
  TWizAsistAutorizarHoras_DevEx = class(TBaseCXWizardFiltro)
    dInicialLBL: TLabel;
    FechaInicial: TZetaFecha;
    dFinalLBL: TLabel;
    FechaFinal: TZetaFecha;
    Cancelar: TGroupBox;
    lTemporales: TCheckBox;
    GroupBox1: TGroupBox;
    lAutExtras: TCheckBox;
    lAutDescanso: TCheckBox;
    lAutConGoce: TCheckBox;
    lAutSinGoce: TCheckBox;
    Cambiar: TGroupBox;
    lHorario: TCheckBox;
    lTipoDia: TCheckBox;
    iTipoDia: TZetaKeyCombo;
    Autorizar: TGroupBox;
    lExtras: TCheckBox;
    lDescanso: TCheckBox;
    lConGoce: TCheckBox;
    lSinGoce: TCheckBox;
    lConGoceE: TCheckBox;
    lSinGoceE: TCheckBox;
    rExtras: TZetaNumero;
    rDescanso: TZetaNumero;
    rConGoce: TZetaNumero;
    rSinGoce: TZetaNumero;
    rConGoceE: TZetaNumero;
    rSinGoceE: TZetaNumero;
    D_ANTERIOR: TcxRadioGroup;
    Horario: TZetaKeyLookup_DevEx;
    mExtras: TZetaKeyLookup_DevEx;
    mDescanso: TZetaKeyLookup_DevEx;
    mConGoce: TZetaKeyLookup_DevEx;
    mSinGoce: TZetaKeyLookup_DevEx;
    mConGoceE: TZetaKeyLookup_DevEx;
    mSinGoceE: TZetaKeyLookup_DevEx;
    rPreFueraJor: TZetaNumero;
    mPreFueraJor: TZetaKeyLookup_DevEx;
    rPreDentroJor: TZetaNumero;
    mPreDentroJor: TZetaKeyLookup_DevEx;
    lPreFueraJor: TCheckBox;
    lPreDentroJor: TCheckBox;
    lAutHorasPrep: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure CheckBoxClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    sFiltroMotivos :string;
    procedure SetControls;
    procedure SetFiltros;

  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizAsistAutorizarHoras_DevEx: TWizAsistAutorizarHoras_DevEx;

implementation

uses DCliente,
     DCatalogos,
     DTablas,
     DProcesos,
     DAsistencia,
     DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaDialogo,
     ZBaseSelectGrid_DevEx,
     FEmpleadoGridSelect_DevEx,
     FToolsAsistencia;

{$R *.DFM}

const
     K_TAG_HORARIO = 1;
     K_TAG_EXTRAS = 2;
     K_TAG_DESCANSO = 3;
     K_TAG_CON_GOCE = 4;
     K_TAG_SIN_GOCE = 5;
     K_TAG_CON_GOCE_E = 6;
     K_TAG_SIN_GOCE_E = 7;
     K_TAG_PRE_FUERA_JOR = 8;
     K_TAG_PRE_DENTRO_JOR = 9;

procedure TWizAsistAutorizarHoras_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := FechaInicial;
     with dmCliente do
     begin
          FechaInicial.Valor := FechaAsistencia;
          FechaFinal.Valor := FechaAsistencia;
     end;
     iTipoDia.ItemIndex := 0;
     SetControls;
     HelpContext := H20234_Extras_permisos_globales;
     lHorario.Tag := K_TAG_HORARIO;
     lExtras.Tag := K_TAG_EXTRAS;
     lDescanso.Tag := K_TAG_DESCANSO;
     lConGoce.Tag := K_TAG_CON_GOCE;
     lSinGoce.Tag := K_TAG_SIN_GOCE;
     lConGoceE.Tag := K_TAG_CON_GOCE_E;
     lSinGoceE.Tag := K_TAG_SIN_GOCE_E;
     lPreFueraJor.Tag :=  K_TAG_PRE_FUERA_JOR;
     lPreDentroJor.Tag := K_TAG_PRE_DENTRO_JOR;

     Horario.LookupDataset := dmCatalogos.cdsHorarios;
     mExtras.LookupDataset := dmTablas.cdsMotAuto;
     mDescanso.LookupDataset := dmTablas.cdsMotAuto;
     mConGoce.LookupDataset := dmTablas.cdsMotAuto;
     mSinGoce.LookupDataset := dmTablas.cdsMotAuto;
     mConGoceE.LookupDataset := dmTablas.cdsMotAuto;
     mSinGoceE.LookupDataset := dmTablas.cdsMotAuto;
     mPreFueraJor.LookupDataSet:= dmTablas.cdsMotAuto;
     mPreDentroJor.LookupDataSet:= dmTablas.cdsMotAuto;

     SetFiltros;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TWizAsistAutorizarHoras_DevEx.SetFiltros;
const
     K_FLT_TIPO_MOTIVO = 'TB_TIPO = %d OR TB_TIPO = %d';

function SetFilter (eMotivo:eAutorizaChecadas ):string;
begin
     Result := Format (K_FLT_TIPO_MOTIVO ,[K_MOTIVO_AUTORIZ_OFFSET,Ord(eMotivo)]);
end;
begin
     mExtras.Filtro := SetFilter(acExtras);
     mDescanso.Filtro := SetFilter(acDescanso );
     mConGoce.Filtro := SetFilter(acConGoce );
     mSinGoce.Filtro := SetFilter(acSinGoce );
     mConGoceE.Filtro := SetFilter(acConGoceEntrada );
     mSinGoceE.Filtro := SetFilter(acSinGoceEntrada );
     mPreFueraJor.Filtro := SetFilter(acPrepagFueraJornada  );
     mPreDentroJor.Filtro := SetFilter(acPrepagDentroJornada );
end;

procedure TWizAsistAutorizarHoras_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     parametros.PageIndex := 0;
     filtrosCondiciones.PageIndex :=1;
     Ejecucion.pageINdex := 2;
     with dmCatalogos do
     begin
          cdsHorarios.Conectar;
     end;
     with dmTablas do
     begin
          sFiltroMotivos := VACIO;
          cdsMotAuto.Conectar;
     end;
end;

procedure TWizAsistAutorizarHoras_DevEx.CargaParametros;

 function GetTexto( const sTitulo: string;
                    lControl : TCheckBox;
                    oHoras: TZetaNumero;
                    oMotivo: TZetaKeyLookup_DevEx ): string;
 begin
      Result := VACIO;
      if lControl.Checked then
         if ( oHoras.Valor <> 0 ) then
            Result := sTitulo + ':' + FloatToStr(oHoras.Valor) + ' M:' +  oMotivo.Llave;
 end;

 var
    sCancelacion, sTexto : string;
begin

     inherited CargaParametros;
       with Descripciones do
     begin

          AddString( 'Del', FechaCorta( FechaInicial.Valor ) +  ' al ' + FechaCorta( FechaFinal.Valor ) );

          if lHorario.Checked then
             AddString( 'Cambiar horario', GetDescripLlave( Horario ) );

          if lTipoDia.Checked then
             AddString( 'Cambiar tipo de d�a', ObtieneElemento( lfStatusAusencia, iTipoDia.Valor ) );

          sTexto := VACIO;
          sTexto := ConcatString( sTexto, GetTexto( 'Ext.', lExtras, rExtras, mExtras ),  ',' );
          sTexto := ConcatString( sTexto, GetTexto( 'D.Trab', lDescanso, rDescanso, mDescanso ), ',' );
          sTexto := ConcatString( sTexto, GetTexto( 'P.C\G', lConGoce, rConGoce, mConGoce ), ',' );
          sTexto := ConcatString( sTexto, GetTexto( 'P.S\G', lSinGoce, rSinGoce, mSinGoce ), ',' );
          sTexto := ConcatString( sTexto, GetTexto( 'P.C\G E', lConGoceE, rConGoceE, mConGoceE ), ',' );
          sTexto := ConcatString( sTexto, GetTexto( 'P.S\G E', lSinGoceE, rSinGoceE, mSinGoceE ), ',' );
          sTexto := ConcatString( sTexto, GetTexto( 'Prep. F/J', lPreFueraJor, rPreFueraJor, mPreFueraJor ), ',' );
          sTexto := ConcatString( sTexto, GetTexto( 'Prep. D/J', lPreDentroJor, rPreDentroJor, mPreDentroJor ), ',' );

         // if( trim(Stexto) <> VACIO) then
         // AddString( 'Autorizar', sTexto );
          AddString( 'Si existe aut.', ObtieneElemento( lfOperacionConflicto, D_ANTERIOR.ItemIndex + 1 ));

          if lTemporales.Checked then
             sCancelacion := ConcatString( sCancelacion, 'H.Temp' , ',' );
          if lAutExtras.Checked then
             sCancelacion := ConcatString( sCancelacion, 'Extras' , ',' );
          if lAutDescanso.Checked then
             sCancelacion := ConcatString( sCancelacion, 'Descanso' , ',' );
          if lAutConGoce.Checked then
             sCancelacion := ConcatString( sCancelacion, 'Per.C\G' , ',' );
          if lAutSinGoce.Checked then
             sCancelacion := ConcatString( sCancelacion, 'Per.S\G' , ',' );
          if lAutHorasPrep.Checked then
             sCancelacion := ConcatString( sCancelacion, 'H.Prep.' , ',' );
         // if StrLleno( sCancelacion ) then
         //    AddString( 'Cancelar', sCancelacion );
     end;

     with ParameterList do
     begin
          AddDate( 'FechaInicial', FechaInicial.Valor );
          AddDate( 'FechaFinal', FechaFinal.Valor );

          AddBoolean( 'CambiarHorario', lHorario.Checked );
          AddString( 'Horario', Horario.Llave );

          AddBoolean( 'CambiarTipoDia', lTipoDia.Checked );
          AddInteger( 'TipoDia', iTipoDia.Valor );

          AddBoolean( 'AutorizarExtras', lExtras.Checked );
          AddFloat( 'Extras', rExtras.Valor );
          AddString( 'MotivoExtras', mExtras.Llave );

          AddBoolean( 'AutorizarDescanso', lDescanso.Checked );
          AddFloat( 'Descanso', rDescanso.Valor );
          AddString( 'MotivoDescanso', mDescanso.Llave );

          AddBoolean( 'AutorizarConGoce', lConGoce.Checked );
          AddFloat( 'ConGoce', rConGoce.Valor );
          AddString( 'MotivoConGoce', mConGoce.Llave );

          AddBoolean( 'AutorizarSinGoce', lSinGoce.Checked );
          AddFloat( 'SinGoce', rSinGoce.Valor );
          AddString( 'MotivoSinGoce', mSinGoce.Llave );

          AddBoolean( 'AutorizarConGoceE', lConGoceE.Checked );
          AddFloat( 'ConGoceE', rConGoceE.Valor );
          AddString( 'MotivoConGoceE', mConGoceE.Llave );

          AddBoolean( 'AutorizarSinGoceE', lSinGoceE.Checked );
          AddFloat( 'SinGoceE', rSinGoceE.Valor );
          AddString( 'MotivoSinGoceE', mSinGoceE.Llave );

          AddBoolean( 'AutorizarPrepFueraJor', lPreFueraJor.Checked );
          AddFloat( 'PrepFueraJor', rPreFueraJor.Valor );
          AddString( 'MotivoPrepFueraJor', mPreFueraJor.Llave );

          AddBoolean( 'AutorizarPrepDentroJor', lPreDentroJor.Checked );
          AddFloat( 'PrepDentroJor', rPreDentroJor.Valor );
          AddString( 'MotivoPrepDentroJor', mPreDentroJor.Llave );

          AddInteger( 'ExisteAnterior', D_ANTERIOR.ItemIndex );

          AddBoolean( 'CancelarHorarioTemporal', lTemporales.Checked );
          AddBoolean( 'CancelarExtras', lAutExtras.Checked );
          AddBoolean( 'CancelarDescansos', lAutDescanso.Checked );
          AddBoolean( 'CancelarPermisosConGoce', lAutConGoce.Checked );
          AddBoolean( 'CancelarPermisosSinGoce', lAutSinGoce.Checked );
          AddBoolean( 'CancelarHorasPrepagadas', lAutHorasPrep.Checked );
          //Derecho de Aprobar Autorizaciones
          AddBoolean( 'AprobarAutorizaciones', dmAsistencia.TieneDerechoAprobar );
          //Derecho para Ignorar Bloqueo por Status
          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
     end;
    
end;

procedure TWizAsistAutorizarHoras_DevEx.CargaListaVerificacion;
begin
     dmProcesos.AutorizarHorasGetLista( ParameterList );
end;

function TWizAsistAutorizarHoras_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

procedure TWizAsistAutorizarHoras_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const
K_CODIGO_INVALIDO = 'C�digo Inactivo!!!';
var
   i: Integer;
   lChecado: Boolean;

   function ValidaAutorizacion( const TipoAuto: eAutorizaChecadas; oControlHoras: TZetaNumero; oControlMotivo: TZetaKeyLookup_DevEx ):Boolean;
   begin
        if ( oControlHoras.Valor > 0 ) then
          Result := ValidaAutoWizard( Self.Caption, TipoAuto, oControlHoras.Valor, oControlMotivo )
        else
          Result := Error( Format( 'El N�mero de %s Tiene que ser Mayor a Cero', [ ZetaCommonLists.ObtieneElemento( lfAutorizaChecadas, Ord( TipoAuto ) ) ] ), oControlHoras );
   end;

begin
  ParametrosControl := nil;
     inherited;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          if ( FechaInicial.Valor > FechaFinal.Valor ) then
             CanMove := Error( 'Fecha Inicial Mayor A Fecha Final', FechaInicial )
          else
          begin
               lChecado := False;
               for i := 0 to ( ComponentCount - 1 ) do
               begin
                    if ( Components[ i ] is TCheckBox ) and ( TCheckBox( Components[ i ] ).Checked ) then
                    begin
                         lChecado := True;
                         case Components[ i ].Tag of
                              K_TAG_HORARIO: if StrVacio( Horario.Llave ) then
                                                CanMove := Error( 'El Horario no Puede Quedar Vac�o', Horario );
                              K_TAG_EXTRAS: CanMove := ValidaAutorizacion( acExtras, rExtras, mExtras );
                              K_TAG_DESCANSO: CanMove := ValidaAutorizacion( acDescanso, rDescanso, mDescanso );
                              K_TAG_CON_GOCE: CanMove := ValidaAutorizacion( acConGoce, rConGoce, mConGoce );
                              K_TAG_SIN_GOCE: CanMove := ValidaAutorizacion( acSinGoce, rSinGoce, mSinGoce );
                              K_TAG_CON_GOCE_E: CanMove := ValidaAutorizacion( acConGoceEntrada, rConGoceE, mConGoceE );
                              K_TAG_SIN_GOCE_E: CanMove := ValidaAutorizacion( acSinGoceEntrada, rSinGoceE, mSinGoceE );
                              K_TAG_PRE_FUERA_JOR: CanMove :=  ValidaAutorizacion( acPrepagFueraJornada, rPreFueraJor, mPreFueraJor );
                              K_TAG_PRE_DENTRO_JOR: CanMove :=  ValidaAutorizacion( acPrepagDentroJornada, rPreDentroJor, mPreDentroJor );
                         end;
                         if not CanMove then
                            Break;
                    end;
               end;
               if not lChecado then
                  CanMove := Error( 'Se Tiene que Seleccionar por lo Menos UNA Opci�n de' + CR_LF + 'Cambio, Autorizaci�n o Cancelaci�n', lHorario );
          end;
          if CanMove then
          begin
               CanMove := dmCliente.PuedeCambiarTarjetaDlg( FechaInicial.Valor, FechaFinal.Valor );
          end;

           //Validaci�n de catalogos inactivos   @DACP
           if  Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS ) then
           begin
                if HORARIO.Descripcion = K_CODIGO_INVALIDO then
                   CanMove := FALSE;
           end;

     end;
end;

procedure TWizAsistAutorizarHoras_DevEx.SetControls;
procedure ActualizaFiltro (const iTipo:Integer);
begin
     sFiltroMotivos := ConcatString(sFiltroMotivos,Format('TB_TIPO = %d',[iTipo]),' OR ');
end;

begin
     rExtras.Enabled := lExtras.Checked;
     rDescanso.Enabled := lDescanso.Checked;
     rConGoce.Enabled := lConGoce.Checked;
     rSinGoce.Enabled := lSinGoce.Checked;
     rConGoceE.Enabled := lConGoceE.Checked;
     rSinGoceE.Enabled := lSinGoceE.Checked;
     rPreFueraJor.Enabled:= lPreFueraJor.Checked;
     rPreDentroJor.Enabled:= lPreDentroJor.Checked;
     Horario.Enabled := lHorario.Checked;
     iTipoDia.Enabled := lTipoDia.Checked;

     mExtras.Enabled := lExtras.Checked;
     mDescanso.Enabled := lDescanso.Checked;
     mConGoce.Enabled := lConGoce.Checked;
     mSinGoce.Enabled := lSinGoce.Checked;
     mConGoceE.Enabled := lConGoceE.Checked;
     mSinGoceE.Enabled := lSinGoceE.Checked;
     mPreFueraJor.Enabled:= lPreFueraJor.Checked;
     mPreDentroJor.Enabled:= lPreDentroJor.Checked;

end;

procedure TWizAsistAutorizarHoras_DevEx.CheckBoxClick(Sender: TObject);
begin
     SetControls;
end;

function TWizAsistAutorizarHoras_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AutorizarHoras( ParameterList, Verificacion );
end;




procedure TWizAsistAutorizarHoras_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := FechaInicial;

     inherited;

end;

procedure TWizAsistAutorizarHoras_DevEx.SetEditarSoloActivos;
begin
     HORARIO.EditarSoloActivos := TRUE;
end;

end.



