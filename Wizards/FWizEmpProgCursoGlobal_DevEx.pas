unit FWizEmpProgCursoGlobal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FWizEmpBaseFiltro, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, Mask, ZetaFecha, 
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl;

type
  TWizEmpProgCursoGlobal_DevEx = class(TWizEmpBaseFiltro)
    dFecha: TZetaFecha;
    chbOpcional: TCheckBox;
    CodigoCurso: TZetaKeyLookup_DevEx;
    Label2: TLabel;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Agregado para enfocar correctamente los controles.
     procedure SetEditarSoloActivos;
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { Protected declarations}
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  end;

var
  WizEmpProgCursoGlobal_DevEx: TWizEmpProgCursoGlobal_DevEx;

implementation

uses ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     dProcesos,
     dCatalogos,
     dCliente,
     dGlobal,
     ZBaseSelectGrid_DevEx,
     ZGlobalTress,
     FEmpleadoGridSelect_DevEx;
{$R *.dfm}

procedure TWizEmpProgCursoGlobal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := CodigoCurso;
     HelpContext := H_EMP_CURSOS_PROG_GLOBAL;
     dFecha.Valor := dmCliente.FechaDefault;
     CodigoCurso.LookupDataSet := dmCatalogos.cdsCursos;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TWizEmpProgCursoGlobal_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsCursos.Conectar;
     Advertencia.Caption :='Al aplicar el proceso se registrar�n los cursos en el historial de cursos programados de los empleados indicados.'; 
end;

procedure TWizEmpProgCursoGlobal_DevEx.CargaListaVerificacion;
begin
     inherited;
     dmProcesos.CursoProgGlobalGetLista( ParameterList );
end;

procedure TWizEmpProgCursoGlobal_DevEx.CargaParametros;
begin
     inherited CargaParametros; 
     with Descripciones do
     begin
          AddString( 'C�digo Curso', GetDescripLlave( CodigoCurso ) );
          AddDate( 'Inicio', dFecha.Valor );          
          AddString( 'Opcional', zBoolToStr( chbOpcional.Checked ) );
     end;
     with ParameterList do
     begin
          AddString( 'CodigoCurso', CodigoCurso.Llave );
          AddDate( 'dFecha', dFecha.Valor );
          AddString( 'chbOpcional', zBoolToStr(  chbOpcional.Checked ) );
     end;
end;

function TWizEmpProgCursoGlobal_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CursoProgGlobal( ParameterList, Verificacion );
end;

function TWizEmpProgCursoGlobal_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

procedure TWizEmpProgCursoGlobal_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
Const
     K_CODIGO_INVALIDO = 'C�digo Inactivo!!!';
begin
     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
          if ( wizard.EsPaginaActual( Parametros )) then
          begin
               if StrVacio( CodigoCurso.Llave ) then
                 CanMove := Error( '� No se ha Especificado Curso !', CodigoCurso );

               //Validaci�n de catalogos inactivos   @DACP
               if  Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS ) then
               begin
                    if CodigoCurso.Descripcion = K_CODIGO_INVALIDO then
                       CanMove := FALSE;
               end;
          end;
     end;
     inherited;
end;

procedure TWizEmpProgCursoGlobal_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control dependiendo del tipo de proceso
     if Wizard.EsPaginaActual( Parametros ) then
        ParametrosControl := CodigoCurso;
     inherited;
end;

procedure TWizEmpProgCursoGlobal_DevEx.SetEditarSoloActivos;
begin
     CodigoCurso.EditarSoloActivos := TRUE;
end;

end.
