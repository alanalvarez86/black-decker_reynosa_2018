inherited WizNomAjusteRetFonacot_DevEx: TWizNomAjusteRetFonacot_DevEx
  Left = 745
  Top = 198
  Caption = 'Ajuste de Retenci'#243'n Fonacot'
  ClientHeight = 529
  ClientWidth = 522
  ExplicitWidth = 528
  ExplicitHeight = 558
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 522
    Height = 529
    ParentFont = False
    ExplicitWidth = 522
    ExplicitHeight = 529
    object Instrucciones: TdxWizardControlPage [0]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Proceso que compara el descuento de n'#243'mina contra la retenci'#243'n m' +
        'ensual.'
      Header.Title = 'Ajuste de Retenci'#243'n Fonacot'
      ParentFont = False
      object GroupBox1: TGroupBox
        Left = 0
        Top = 131
        Width = 497
        Height = 118
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label3: TLabel
          Left = 34
          Top = 25
          Width = 443
          Height = 75
          AutoSize = False
          Caption = 
            'Este proceso hace una comparaci'#243'n del descuento mensual contra l' +
            'a retenci'#243'n mensual requerida por Fonacot y crea un cargo/abono ' +
            'dependiendo si tiene saldo a favor o a cargo. Este saldo se le l' +
            'lama Ajuste que podr'#225' aplicar en otro mes.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
      end
    end
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'En este paso deber'#225' seleccionar las n'#243'minas que comprende el pag' +
        'o de Fonacot del mes seleccionado.'
      Header.Title = 'N'#243'minas a calcular'
      ParentFont = False
      ExplicitWidth = 500
      ExplicitHeight = 389
      object GBTipoPrestamo: TGroupBox
        Left = 0
        Top = 111
        Width = 497
        Height = 154
        Align = alCustom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label2: TLabel
          Left = 152
          Top = 48
          Width = 22
          Height = 13
          Caption = 'A'#241'o:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label6: TLabel
          Left = 151
          Top = 73
          Width = 23
          Height = 13
          Caption = 'Mes:'
          Transparent = True
        end
        object Label1: TLabel
          Left = 27
          Top = 23
          Width = 147
          Height = 13
          Caption = 'Tipo de pr'#233'stamos de Fonacot:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object NoTipoPrestamo: TZetaTextBox
          Left = 178
          Top = 21
          Width = 50
          Height = 17
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object TipoPrestamo: TZetaTextBox
          Left = 234
          Top = 21
          Width = 241
          Height = 17
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object MesDefault: TZetaKeyCombo
          Left = 178
          Top = 69
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          ListaFija = lfMeses
          ListaVariable = lvPuesto
          Offset = 1
          Opcional = False
          EsconderVacios = False
        end
        object RGFiltro: TRadioGroup
          Left = 178
          Top = 96
          Width = 297
          Height = 39
          Caption = 'Filtrar por:'
          Columns = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Mes'
            'Rango de n'#243'minas')
          ParentFont = False
          TabOrder = 2
          OnClick = RGFiltroClick
        end
        object YearDefaultAct: TZetaNumero
          Left = 178
          Top = 44
          Width = 50
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Mascara = mnDias
          ParentFont = False
          TabOrder = 0
          Text = '0'
        end
      end
    end
    object Prestamo: TdxWizardControlPage [2]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Deber'#225' indicar el tipo de pr'#233'stamo para depositar las diferencia' +
        's a cargo o a favor de cada cr'#233'dito, ademas indicar si desea ree' +
        'mplazar el monto si corre el proceso mas de una vez.'
      Header.Title = 'Pr'#233'stamo de Ajuste'
      ParentFont = False
      object GroupBox3: TGroupBox
        Left = 0
        Top = 60
        Width = 497
        Height = 301
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label4: TLabel
          Left = 26
          Top = 48
          Width = 140
          Height = 13
          Caption = 'Tipo de pr'#233'stamo para &ajuste:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label7: TLabel
          Left = 40
          Top = 171
          Width = 127
          Height = 13
          Caption = 'Solo provisi'#243'n vacaciones:'
          Visible = False
        end
        object Label9: TLabel
          Left = 92
          Top = 74
          Width = 75
          Height = 13
          Caption = 'Aplicar ajuste a:'
        end
        object TipoPrestamoAjus: TZetaKeyLookup_DevEx
          Left = 173
          Top = 44
          Width = 304
          Height = 21
          LookupDataset = dmTablas.cdsTPresta
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 50
        end
        object RGAjuste: TRadioGroup
          Left = 173
          Top = 74
          Width = 302
          Height = 78
          Align = alCustom
          BiDiMode = bdLeftToRight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemIndex = 0
          Items.Strings = (
            'Todos los cr'#233'ditos'
            'Cr'#233'ditos con saldo a favor'
            'Cr'#233'ditos con saldo en contra')
          ParentBiDiMode = False
          ParentFont = False
          TabOrder = 2
          OnClick = RGAjusteClick
        end
        object RGAnterior: TRadioGroup
          Left = 173
          Top = 163
          Width = 302
          Height = 61
          Align = alCustom
          BiDiMode = bdLeftToRight
          Caption = 'En caso de existir ajuste a&nterior'
          Columns = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemIndex = 0
          Items.Strings = (
            'Reemplazar'
            'Dejar Anterior')
          ParentBiDiMode = False
          ParentFont = False
          TabOrder = 1
        end
        object ckProvision: TcxCheckBox
          Left = 170
          Top = 170
          Caption = 'Si'
          TabOrder = 3
          Transparent = True
          Visible = False
          Width = 39
        end
      end
    end
    object RangosDNominas: TdxWizardControlPage [3]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 'Complete los campos para indicar los par'#225'metros del ajuste. '
      Header.Title = 'Par'#225'metros para ajuste'
      ParentFont = False
      object GBRangosNominas: TGroupBox
        Left = 0
        Top = 0
        Width = 500
        Height = 389
        Align = alClient
        Caption = 'Rangos de N'#243'minas:'
        TabOrder = 0
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage [4]
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 500
      ExplicitHeight = 389
      inherited sCondicionLBl: TLabel
        Left = 49
        Top = 209
        ExplicitLeft = 49
        ExplicitTop = 209
      end
      inherited sFiltroLBL: TLabel
        Left = 74
        Top = 238
        ExplicitLeft = 74
        ExplicitTop = 238
      end
      inherited Seleccionar: TcxButton
        Left = 178
        Top = 326
        ExplicitLeft = 178
        ExplicitTop = 326
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 101
        Top = 206
        ExplicitLeft = 101
        ExplicitTop = 206
      end
      inherited sFiltro: TcxMemo
        Left = 101
        Top = 237
        Style.IsFontAssigned = True
        ExplicitLeft = 101
        ExplicitTop = 237
      end
      inherited GBRango: TGroupBox
        Left = 101
        Top = 76
        ExplicitLeft = 101
        ExplicitTop = 76
      end
      inherited bAjusteISR: TcxButton
        Left = 415
        Top = 238
        ExplicitLeft = 415
        ExplicitTop = 238
      end
    end
    inherited Ejecucion: TdxWizardControlPage [5]
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 500
      ExplicitHeight = 389
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 500
        ExplicitHeight = 294
        Height = 294
        Width = 500
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 500
        Width = 500
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitWidth = 432
          Width = 432
          AnchorY = 57
        end
      end
    end
    object Definicion: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Especifique si desea incluir empleados incapacitados, indique el' +
        ' rango de tolerancia de diferencias y si desea entregar el saldo' +
        ' de ajuste a favor que tenga el empleado.'
      Header.Title = 'Definici'#243'n de par'#225'metros'
      ParentFont = False
      object GroupBox2: TGroupBox
        Left = 0
        Top = 72
        Width = 497
        Height = 215
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object lbTolerancia: TLabel
          Left = 227
          Top = 68
          Width = 53
          Height = 13
          Caption = 'Tolerancia:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object GBSaldosFavor: TGroupBox
          Left = 119
          Top = 106
          Width = 250
          Height = 65
          Caption = 'Entregar saldo de ajuste a favor:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object lSaldarpresta: TCheckBox
            Left = 12
            Top = 24
            Width = 113
            Height = 17
            Hint = 'Saldar pr'#233'stamos a favor para empleados activos'
            Alignment = taLeftJustify
            Caption = 'Empleados Activos:'
            TabOrder = 0
          end
          object lSaldarBajas: TCheckBox
            Left = 138
            Top = 24
            Width = 100
            Height = 17
            Hint = 'Saldar pr'#233'stamos a favor para empleados dados de baja'
            Alignment = taLeftJustify
            Caption = 'Empleados Baja:'
            TabOrder = 1
          end
        end
        object IncluirIncapa: TCheckBox
          Left = 180
          Top = 33
          Width = 118
          Height = 17
          Hint = 'Incluir Incapacidades en empleados con status activo'
          Alignment = taLeftJustify
          Caption = 'Incluir &incapacitados:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object Tolerancia: TZetaNumero
          Left = 285
          Top = 63
          Width = 50
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Mascara = mnHoras
          ParentFont = False
          TabOrder = 2
          Text = '0.00'
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 40
    Top = 469
  end
end
