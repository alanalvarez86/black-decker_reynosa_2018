inherited WizAgregarBaseDatosEmpleados: TWizAgregarBaseDatosEmpleados
  Left = 417
  Top = 148
  Caption = 'Agregar Base de Datos de tipo Tress'
  ClientHeight = 295
  ClientWidth = 434
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 259
    Width = 434
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Enabled = True
    end
    inherited Salir: TZetaWizardButton
      Left = 344
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 261
    end
  end
  inherited PageControl: TPageControl
    Width = 434
    Height = 259
    ActivePage = NuevaBD1
    inherited Parametros: TTabSheet
      object lbNombreBD: TLabel
        Left = 58
        Top = 124
        Width = 136
        Height = 13
        Caption = 'Nombre de la base de datos:'
        FocusControl = txtBaseDatos
      end
      object lblSeleccionarBD: TLabel
        Left = 65
        Top = 196
        Width = 129
        Height = 13
        Caption = 'Seleccionar base de datos:'
        FocusControl = cbBasesDatos
      end
      object rbBDNueva: TRadioButton
        Left = 32
        Top = 96
        Width = 233
        Height = 17
        Caption = 'C&rear base de datos nueva'
        Checked = True
        TabOrder = 1
        TabStop = True
        OnClick = rbBDNuevaClick
      end
      object rbBDExistente: TRadioButton
        Left = 32
        Top = 172
        Width = 153
        Height = 17
        Caption = '&Base de datos existente:'
        TabOrder = 3
        OnClick = rbBDExistenteClick
      end
      object cbBasesDatos: TComboBox
        Left = 200
        Top = 192
        Width = 177
        Height = 21
        Style = csDropDownList
        Enabled = False
        ItemHeight = 13
        TabOrder = 4
      end
      object txtBaseDatos: TEdit
        Left = 200
        Top = 120
        Width = 177
        Height = 21
        TabOrder = 2
      end
      object memoParametros: TMemo
        Left = 0
        Top = 0
        Width = 426
        Height = 65
        Align = alTop
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Lines.Strings = (
          'Seleccione la opci'#243'n correspondiente para agregar una base de '
          'datos de tipo Tress.')
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
    end
    object NuevaBD1: TTabSheet [1]
      Caption = 'NuevaBD1'
      Enabled = False
      ImageIndex = 4
      TabVisible = False
      object lblCantidadEmpleados: TLabel
        Left = 68
        Top = 106
        Width = 55
        Height = 13
        Caption = 'Empleados:'
      end
      object lblDocumentos: TLabel
        Left = 19
        Top = 184
        Width = 104
        Height = 13
        Caption = 'Documentos digitales:'
      end
      object lblCantEmpSeleccion: TLabel
        Left = 338
        Top = 106
        Width = 100
        Height = 13
        Caption = 'lblCantEmpSeleccion'
      end
      object lblDocSeleccion: TLabel
        Left = 338
        Top = 184
        Width = 77
        Height = 13
        Caption = 'lblDocSeleccion'
      end
      object lblNotaEmpleados: TLabel
        Left = 10
        Top = 140
        Width = 234
        Height = 13
        Caption = 'Considere el crecimiento estimado en los pr'#243'ximos 3 a'#241'os'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblNotaDocumentos: TLabel
        Left = 10
        Top = 218
        Width = 254
        Height = 13
        Caption = 'Cantidad de documentos digitales que se subir'#225'n por empleado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Serif'
        Font.Style = []
        ParentFont = False
      end
      object tbEmpleados: TcxTrackBar
        Left = 123
        Top = 90
        ParentFont = False
        Position = 300
        Properties.Frequency = 1200
        Properties.Max = 5000
        Properties.Min = 300
        Properties.TickType = tbttTicksAndNumbers
        Properties.OnChange = tbEmpleadosPropertiesChange
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.TextStyle = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Height = 49
        Width = 214
      end
      object tbDocumentos: TcxTrackBar
        Left = 123
        Top = 168
        Position = 1
        Properties.Frequency = 3
        Properties.Max = 12
        Properties.Min = 1
        Properties.TickType = tbttTicksAndNumbers
        Properties.OnChange = tbDocumentosPropertiesChange
        TabOrder = 1
        Height = 49
        Width = 214
      end
      object memoEmpleadosDatos: TMemo
        Left = 0
        Top = 0
        Width = 426
        Height = 89
        Align = alTop
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
      end
    end
    object NuevaBD2: TTabSheet [2]
      Caption = 'NuevaBD2'
      Enabled = False
      ImageIndex = 5
      TabVisible = False
      object lblUbicacion: TLabel
        Left = 33
        Top = 10
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ubicaci'#243'n:'
      end
      object lblTamanoBDOrigen: TLabel
        Left = 60
        Top = 32
        Width = 225
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tama'#241'o del archivo de la base de datos origen:'
      end
      object lblArchivoLog: TLabel
        Left = 225
        Top = 54
        Width = 60
        Height = 13
        Caption = 'Archivo Log:'
      end
      object lblMB1: TLabel
        Left = 356
        Top = 32
        Width = 16
        Height = 13
        Caption = 'MB'
      end
      object lblMB2: TLabel
        Left = 357
        Top = 54
        Width = 16
        Height = 13
        Caption = 'MB'
      end
      object txtUbicacion: TZetaEdit
        Left = 87
        Top = 6
        Width = 266
        Height = 21
        MaxLength = 10
        ReadOnly = True
        TabOrder = 0
      end
      object GBUsuariosMSSQL: TGroupBox
        Left = 32
        Top = 80
        Width = 353
        Height = 129
        TabOrder = 3
        object lblUsuarioMSSQL: TLabel
          Left = 16
          Top = 10
          Width = 233
          Height = 13
          Caption = 'Usuario de MSSQL para crear y acceder a la BD:'
        end
        object lblUsuario: TLabel
          Left = 94
          Top = 71
          Width = 39
          Height = 13
          Caption = 'Usuario:'
        end
        object lblClave: TLabel
          Left = 103
          Top = 93
          Width = 30
          Height = 13
          Caption = 'Clave:'
        end
        object rbSameUser: TRadioButton
          Left = 40
          Top = 27
          Width = 265
          Height = 17
          Caption = 'Usuario y clave default (Configuraci'#243'n del Sistema)'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = rbSameUserClick
        end
        object rbOtroUser: TRadioButton
          Left = 40
          Top = 47
          Width = 209
          Height = 17
          Caption = 'Utilizar otro usuario y clave de acceso:'
          TabOrder = 1
          OnClick = rbOtroUserClick
        end
        object txtUsuario: TEdit
          Left = 136
          Top = 67
          Width = 121
          Height = 21
          Enabled = False
          TabOrder = 2
        end
        object txtClave: TMaskEdit
          Left = 136
          Top = 89
          Width = 121
          Height = 21
          Enabled = False
          PasswordChar = '*'
          TabOrder = 3
        end
      end
      object txtTamanyo: TZetaNumero
        Left = 288
        Top = 28
        Width = 65
        Height = 21
        Mascara = mnMillares
        ReadOnly = True
        TabOrder = 1
        Text = '0.000'
      end
      object txtLog: TZetaNumero
        Left = 288
        Top = 50
        Width = 65
        Height = 21
        Mascara = mnDiasFraccion
        ReadOnly = True
        TabOrder = 2
        Text = '0.00'
      end
    end
    object DefinirEstructura1: TTabSheet [3]
      Caption = 'DefinirEstructura1'
      ImageIndex = 3
      TabVisible = False
      object memDefinirEstructura: TMemo
        Left = 0
        Top = 0
        Width = 426
        Height = 65
        Align = alTop
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Lines.Strings = (
          'Indique de donde provendr'#225'n los datos iniciales '
          'con los que se agregar'#225' la base de datos.')
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object rbValoresDefault: TRadioButton
        Left = 32
        Top = 112
        Width = 201
        Height = 17
        Caption = 'Valores default de Sistema TRESS'
        Checked = True
        TabOrder = 1
        TabStop = True
        OnClick = rbValoresDefaultClick
      end
      object rbOtraBaseDatos: TRadioButton
        Left = 32
        Top = 136
        Width = 361
        Height = 17
        Caption = 
          'Iniciar con informaci'#243'n de una base de datos (de tipo Tress) exi' +
          'stente'
        TabOrder = 2
        OnClick = rbOtraBaseDatosClick
      end
      object cbBasesDatosBase: TZetaKeyCombo
        Left = 51
        Top = 160
        Width = 257
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        Enabled = False
        ItemHeight = 0
        ParentCtl3D = False
        TabOrder = 3
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object chbEspeciales: TCheckBox
        Left = 32
        Top = 192
        Width = 209
        Height = 17
        Caption = 'Aplicar script de programaci'#243'n especial'
        TabOrder = 4
      end
    end
    object EstructuraEmpleados: TTabSheet [4]
      Caption = 'Estructura Empleados'
      ImageIndex = 2
      TabVisible = False
      object Label1: TLabel
        Left = 81
        Top = 92
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 58
        Top = 132
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
      end
      object edDescripcion: TZetaEdit
        Left = 120
        Top = 128
        Width = 257
        Height = 21
        TabOrder = 2
      end
      object edCodigo: TZetaEdit
        Left = 120
        Top = 88
        Width = 121
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 1
        OnExit = edCodigoExit
      end
      object memoEstEmpleados: TMemo
        Left = 0
        Top = 0
        Width = 426
        Height = 65
        Align = alTop
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Lines.Strings = (
          'Ingrese los campos C'#243'digo y Descripci'#243'n para la base de datos '
          'que se va a agregar.')
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 426
        Height = 177
        Lines.Strings = (
          'Advertencia'
          ''
          'Iniciar'#225' el proceso de ...'
          ' '
          'Presione el bot'#243'n '#39'Ejecutar'#39' para iniciar el proceso.')
      end
      inherited ProgressPanel: TPanel
        Top = 182
        Width = 426
        Visible = True
        object lblProceso: TLabel [0]
          Left = 36
          Top = 32
          Width = 27
          Height = 13
          Caption = 'Paso:'
        end
        inherited ProgressBar: TProgressBar
          Left = 36
          Top = 11
          Visible = True
        end
      end
    end
  end
  object OpenDialog: TOpenDialog
    Options = [ofHideReadOnly]
    Title = 'Seleccione la carpeta con los archivos para carga inicial'
    Left = 14
    Top = 330
  end
end
