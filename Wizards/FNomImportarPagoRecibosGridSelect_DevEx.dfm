inherited NomImportarPagoRecibosGridSelect_DevEx: TNomImportarPagoRecibosGridSelect_DevEx
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Empleados Deseados'
  ClientHeight = 216
  ClientWidth = 492
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 180
    Width = 492
    inherited OK_DevEx: TcxButton
      Left = 326
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 404
    end
  end
  inherited PanelSuperior: TPanel
    Width = 492
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 492
    Height = 145
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
      end
      object NO_FEC_PAG: TcxGridDBColumn
        Caption = 'Fecha Pago'
        DataBinding.FieldName = 'NO_FEC_PAG'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
