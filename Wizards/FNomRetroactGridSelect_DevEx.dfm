inherited GridCalculaRetoactivos_DevEx: TGridCalculaRetoactivos_DevEx
  Width = 531
  Caption = 'Seleccione Los Empleados Deseados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 515
    inherited OK_DevEx: TcxButton
      Left = 351
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 430
      Cancel = True
    end
  end
  inherited PanelSuperior: TPanel
    Width = 515
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 515
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 65
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 247
      end
      object CB_SALARIO: TcxGridDBColumn
        Caption = 'Salario Diario'
        DataBinding.FieldName = 'CB_SALARIO'
        Width = 85
      end
      object CB_SAL_INT: TcxGridDBColumn
        Caption = 'Salario Integrado'
        DataBinding.FieldName = 'CB_SAL_INT'
        Width = 85
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
