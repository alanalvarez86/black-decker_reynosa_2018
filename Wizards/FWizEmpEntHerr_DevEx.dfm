inherited WizEmpEntHerr_DevEx: TWizEmpEntHerr_DevEx
  Left = 342
  Top = 135
  Caption = 'Entregar Herramienta'
  ClientHeight = 421
  ClientWidth = 425
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 425
    Height = 421
    inherited Parametros: TdxWizardControlPage
      Header.Description = 'Proceso para entregar herramientas a un grupo de empleados.'
      Header.Title = 'Entregar Herramienta'
      object HerramientaLbl: TLabel
        Left = 16
        Top = 38
        Width = 60
        Height = 13
        Alignment = taRightJustify
        Caption = '&Herramienta:'
        Transparent = True
      end
      object FechaLbl: TLabel
        Left = 43
        Top = 70
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha:'
        FocusControl = KT_FEC_INI
        Transparent = True
      end
      object LblReferencia: TLabel
        Left = 21
        Top = 104
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = '&Referencia:'
        FocusControl = KT_REFEREN
        Transparent = True
      end
      object LblTalla: TLabel
        Left = 50
        Top = 138
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = '&Talla:'
        FocusControl = KT_TALLA
        Transparent = True
      end
      object LblObservaciones: TLabel
        Left = 2
        Top = 170
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Obser&vaciones:'
        FocusControl = KT_COMENTA
        Transparent = True
      end
      object TO_CODIGO: TZetaKeyLookup_DevEx
        Left = 80
        Top = 34
        Width = 325
        Height = 21
        LookupDataset = dmCatalogos.cdsTools
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
      object KT_FEC_INI: TZetaFecha
        Left = 80
        Top = 67
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '19/dic/97'
        Valor = 35783.000000000000000000
      end
      object KT_REFEREN: TEdit
        Left = 80
        Top = 101
        Width = 130
        Height = 21
        TabOrder = 2
      end
      object KT_TALLA: TZetaKeyLookup_DevEx
        Left = 80
        Top = 134
        Width = 325
        Height = 21
        LookupDataset = dmTablas.cdsTallas
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
      end
      object KT_COMENTA: TEdit
        Left = 80
        Top = 167
        Width = 300
        Height = 21
        MaxLength = 40
        TabOrder = 4
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 186
        Width = 403
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 403
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 335
          AnchorX = 235
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 6
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 31
        Top = 160
      end
      inherited Seleccionar: TcxButton
        Left = 134
        Top = 254
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Top = 160
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 58
        Top = 4
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 480
    Top = 34
  end
end
