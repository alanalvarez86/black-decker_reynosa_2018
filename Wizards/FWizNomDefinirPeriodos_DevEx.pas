unit FWizNomDefinirPeriodos_Devex;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Mask, StdCtrls, Db, Grids, DBGrids, ComCtrls, ExtCtrls, Buttons,
     ZetaNumero,
     ZetaFecha,
     ZetaKeyCombo,
     ZetaWizard,
     ZetaDBTextBox,
     ZetaDBGrid, FWizNomBase_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  ZetaEdit, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, dxSkinsCore, TressMorado2013;

type
  TWizNomDefinirPeriodos_DevEx = class(TWizNomBase_DevEx)
    Panel1: TPanel;
    iTipoPeriodoLBL: TLabel;
    iTipoPeriodo: TZetaKeyCombo;
    dReferencia: TZetaFecha;
    dReferenciaLBL: TLabel;
    iDiasLBL: TLabel;
    iDias: TZetaNumero;
    iPerIniLBL: TLabel;
    iPerFinLBL: TLabel;
    iPerIni: TZetaNumero;
    iPerFin: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizNomDefinirPeriodos_DevEx: TWizNomDefinirPeriodos_DevEx;

implementation

uses ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     DCliente,
     DProcesos;

{$R *.DFM}

procedure TWizNomDefinirPeriodos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          with iTipoPeriodo do
          begin
               with Lista do
               begin
                    BeginUpdate;
                    Clear;
                    with cdsTPeriodos do
                    begin
                         Refrescar;
                         First;
                         while ( not EOF ) do
                         begin
                              if strLleno( FieldByName( 'TP_DESCRIP' ).AsString ) and ( FieldByName( 'TP_CLAS' ).AsInteger <> Ord( tpDiario ) ) then
                              begin
                                   if ListaIntersectaConfidencialidad(dmCliente.Confidencialidad,  FieldByName( 'TP_NIVEL0' ).AsString) then
                                     Add( FieldByName('TP_TIPO').AsString + '=' + ObtieneElemento( lfTipoPeriodoConfidencial, FieldByName('TP_TIPO').AsInteger  ) );
                              end;

                              Next;
                         end;
                    end;
                    EndUpdate;
               end;

               Valor:= 0;
               if ( PeriodoClasificacion <> tpDiario) then
               begin
                    LlaveEntero:= Ord( PeriodoTipo );
               end;
          end;
          dReferencia.Valor := EncodeDate( YearDefault, 1, 1 );
     end;
     iPerIni.Valor := 1;
     iDias.Valor := 5;
     ParametrosControl := iTipoPeriodo;
     HelpContext := H30351_Definir_periodos;
end;

procedure TWizNomDefinirPeriodos_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
ParametrosControl := nil;
     inherited;
     if CanMove and ( Wizard.Adelante ) and ( Wizard.EsPaginaActual( Parametros ) ) then
     begin
          if iTipoPeriodo.ItemIndex > K_PERIODO_VACIO then
          begin
               if ( iPerIni.ValorEntero < 1 ) then
               begin
                    CanMove := False;
                    ZetaDialogo.ZError( Caption, 'El Periodo Inicial Tiene Que Ser Mayor o Igual a Uno', 0 );
                    ActiveControl := iPerIni;
               end
               else
                   if ( iPerFin.ValorEntero > 0 ) and ( iPerIni.ValorEntero > iPerFin.ValorEntero ) then
                   begin
                        CanMove := False;
                        ZetaDialogo.ZError( Caption, 'El Periodo Final Tiene Que Ser Mayor o Igual Al Per�odo Inicial', 0 );
                        ActiveControl := iPerFin;
                   end
                   else
                   begin
                        dmCliente.cdsTPeriodos.Locate('TP_TIPO', iTipoPeriodo.Llave, []);
                        if not ListaIntersectaConfidencialidad(
                              dmCliente.cdsTPeriodos.FieldByName (dmCliente.cdsTPeriodos.LookupConfidenField).AsString,
                              dmCliente.Confidencialidad) then
                                  DataBaseError( Format ('No es posible definir periodos para el tipo de n�mina: ''%s'' ',
                                  [ ObtieneElemento(lfTipoPeriodo, iTipoPeriodo.LlaveEntero) ]));
               end;
          end
          else
          begin
               zError( Caption, 'El tipo de n�mina es invalido.', 0 );
               CanMove := False;
          end;
     end;
end;

procedure TWizNomDefinirPeriodos_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddInteger( 'Year', dmCliente.YearDefault );
          AddInteger( 'Tipo', iTipoPeriodo.LlaveEntero );
          AddDate( 'Fecha', dReferencia.Valor );
          AddInteger( 'Dias', iDias.ValorEntero );
          AddInteger( 'PeriodoInicial', iPerIni.ValorEntero );
          AddInteger( 'PeriodoFinal', iPerFin.ValorEntero );
          AddInteger( 'Clasifi', Ord(GetClasificacionPeriodo(FListaTiposPeriodoConfidencialidad, iTipoPeriodo.LlaveEntero)));
     end;
      with Descripciones do
     begin
          ParamInicial := 4;
          AddString( 'Tipo', iTipoPeriodo.Text );
          AddDate( 'Fecha Inicial', dReferencia.Valor );
          AddInteger( 'Dias de Fondo', iDias.ValorEntero );
          AddInteger( 'Periodo Inicial', iPerIni.ValorEntero );
          AddInteger( 'Periodo Final', iPerFin.ValorEntero );
     end;
end;

function TWizNomDefinirPeriodos_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.DefinirPeriodos( ParameterList );
end;

procedure TWizNomDefinirPeriodos_DevEx.WizardAfterMove(Sender: TObject);
begin
  inherited;
  if Wizard.EsPaginaActual(Parametros) then
   ParametrosControl := iTipoPeriodo;
end;

end.
