unit FNomImportarMovGridShow_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, StdCtrls, ExtCtrls,
     ZBaseGridShow_DevEx,
     ZetaDBGrid, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxControls, cxStyles, dxSkinscxPCPainter,
     cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
     cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
     ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
     cxButtons;

type
  TNomImportarMovGridShow_DevEx = class(TBaseGridShow_DevEx)
    RENGLON: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    CO_NUMERO: TcxGridDBColumn;
    MO_MONTO: TcxGridDBColumn;
    MO_REFEREN: TcxGridDBColumn;
    NUM_ERROR: TcxGridDBColumn;
    DESC_ERROR: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NomImportarMovGridShow_DevEx: TNomImportarMovGridShow_DevEx;

implementation

{$R *.DFM}

end.
