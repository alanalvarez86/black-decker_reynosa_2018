inherited WizNomCalcularPagoFonacot: TWizNomCalcularPagoFonacot
  Left = 409
  Top = 213
  Caption = 'Calcular pago de Fonacot'
  ClientHeight = 433
  ClientWidth = 454
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 397
    Width = 454
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Enabled = False
    end
    inherited Salir: TZetaWizardButton
      Left = 365
      Anchors = [akRight, akBottom]
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 281
      Anchors = [akRight, akBottom]
    end
  end
  inherited PageControl: TPageControl
    Width = 454
    Height = 397
    ActivePage = Parametros
    inherited Parametros: TTabSheet
      object Label4: TLabel
        Left = 16
        Top = 164
        Width = 141
        Height = 13
        Caption = 'Tipo de pr'#233'stamo para &Ajuste:'
      end
      object GBTipoPrestamo: TGroupBox
        Left = 0
        Top = 0
        Width = 446
        Height = 145
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Left = 9
          Top = 25
          Width = 147
          Height = 13
          Caption = 'Tipo de pr'#233'stamos de Fonacot:'
        end
        object Label2: TLabel
          Left = 134
          Top = 47
          Width = 22
          Height = 13
          Caption = 'A'#241'o:'
        end
        object Label3: TLabel
          Left = 134
          Top = 71
          Width = 23
          Height = 13
          Caption = 'Mes:'
        end
        object NoTipoPrestamo: TZetaTextBox
          Left = 160
          Top = 24
          Width = 50
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object TipoPrestamo: TZetaTextBox
          Left = 216
          Top = 24
          Width = 212
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object MesDefault: TZetaKeyCombo
          Left = 160
          Top = 67
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfMeses
          ListaVariable = lvPuesto
          Offset = 1
          Opcional = False
          EsconderVacios = False
        end
        object RGFiltro: TRadioGroup
          Left = 160
          Top = 90
          Width = 265
          Height = 39
          Caption = 'Filtrar por:'
          Columns = 2
          Items.Strings = (
            'Mes'
            'Rango de n'#243'minas')
          TabOrder = 2
          OnClick = RGFiltroClick
        end
        object YearDefaultAct: TZetaNumero
          Left = 160
          Top = 43
          Width = 50
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
        end
      end
      object TipoPrestamoAjus: TZetaKeyLookup
        Left = 160
        Top = 160
        Width = 270
        Height = 21
        LookupDataset = dmTablas.cdsTPresta
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 50
      end
      object RGReportar: TRadioGroup
        Left = 160
        Top = 204
        Width = 268
        Height = 61
        Align = alCustom
        BiDiMode = bdLeftToRight
        Caption = 'En caso de existir varias incapacidades'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Reportar la primera'
          'Reportar la mayor')
        ParentBiDiMode = False
        TabOrder = 3
      end
      object ReportarIncapa: TCheckBox
        Left = 39
        Top = 184
        Width = 134
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Reportar incapacidades:'
        Checked = True
        State = cbChecked
        TabOrder = 2
        OnClick = ReportarIncapaClick
      end
    end
    object RangosDNominas: TTabSheet [1]
      Caption = 'RangosDNominas'
      ImageIndex = 3
      TabVisible = False
      object GBRangosNominas: TGroupBox
        Left = 0
        Top = 0
        Width = 446
        Height = 387
        Align = alClient
        Caption = 'Rangos de Nominas:'
        TabOrder = 0
        object PDiario: TPanel
          Left = 2
          Top = 15
          Width = 442
          Height = 30
          Align = alTop
          TabOrder = 0
          object lblDiario: TLabel
            Left = 100
            Top = 8
            Width = 30
            Height = 13
            Caption = 'Diario:'
          end
          object Label28: TLabel
            Left = 232
            Top = 8
            Width = 6
            Height = 13
            Caption = 'a'
          end
          object DiarioIni: TZetaKeyLookup
            Left = 136
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
          end
          object DiarioFin: TZetaKeyLookup
            Tag = -1
            Left = 248
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
          end
        end
        object PSemanal: TPanel
          Tag = 1
          Left = 2
          Top = 45
          Width = 442
          Height = 30
          Align = alTop
          TabOrder = 1
          object lblSemanal: TLabel
            Left = 86
            Top = 8
            Width = 44
            Height = 13
            Caption = 'Semanal:'
          end
          object Label26: TLabel
            Left = 232
            Top = 8
            Width = 6
            Height = 13
            Caption = 'a'
          end
          object SemanalFin: TZetaKeyLookup
            Tag = -1
            Left = 248
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
          end
          object SemanalIni: TZetaKeyLookup
            Left = 136
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
            OnValidKey = SemanalIniValidKey
          end
        end
        object PCatorcenal: TPanel
          Tag = 2
          Left = 2
          Top = 75
          Width = 442
          Height = 30
          Align = alTop
          TabOrder = 2
          object lblCatorcenal: TLabel
            Left = 76
            Top = 8
            Width = 54
            Height = 13
            Caption = 'Catorcenal:'
          end
          object Label24: TLabel
            Left = 232
            Top = 8
            Width = 6
            Height = 13
            Caption = 'a'
          end
          object CatorcenalIni: TZetaKeyLookup
            Left = 136
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
          end
          object CatorcenalFin: TZetaKeyLookup
            Tag = -1
            Left = 248
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
          end
        end
        object PQuincenal: TPanel
          Tag = 3
          Left = 2
          Top = 105
          Width = 442
          Height = 30
          Align = alTop
          TabOrder = 3
          object lblQuincenal: TLabel
            Left = 80
            Top = 8
            Width = 51
            Height = 13
            Caption = 'Quincenal:'
          end
          object Label22: TLabel
            Left = 232
            Top = 8
            Width = 6
            Height = 13
            Caption = 'a'
          end
          object QuincenalIni: TZetaKeyLookup
            Left = 136
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
          end
          object QuincenalFin: TZetaKeyLookup
            Tag = -1
            Left = 248
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
          end
        end
        object PMensual: TPanel
          Tag = 4
          Left = 2
          Top = 135
          Width = 442
          Height = 30
          Align = alTop
          TabOrder = 4
          object lblMensual: TLabel
            Left = 87
            Top = 8
            Width = 43
            Height = 13
            Caption = 'Mensual:'
          end
          object Label20: TLabel
            Left = 232
            Top = 8
            Width = 6
            Height = 13
            Caption = 'a'
          end
          object MensualIni: TZetaKeyLookup
            Left = 136
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
          end
          object MensualFin: TZetaKeyLookup
            Tag = -1
            Left = 248
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
          end
        end
        object PDecenal: TPanel
          Tag = 5
          Left = 2
          Top = 165
          Width = 442
          Height = 30
          Align = alTop
          TabOrder = 5
          object lblDecenal: TLabel
            Left = 87
            Top = 8
            Width = 43
            Height = 13
            Caption = 'Decenal:'
          end
          object Label18: TLabel
            Left = 232
            Top = 8
            Width = 6
            Height = 13
            Caption = 'a'
          end
          object DecenalIni: TZetaKeyLookup
            Left = 136
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
          end
          object DecenalFin: TZetaKeyLookup
            Tag = -1
            Left = 248
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
          end
        end
        object PSemanalA: TPanel
          Tag = 6
          Left = 2
          Top = 195
          Width = 442
          Height = 30
          Align = alTop
          TabOrder = 6
          object lblSemanalA: TLabel
            Left = 76
            Top = 8
            Width = 54
            Height = 13
            Caption = 'Semanal A:'
          end
          object Label16: TLabel
            Left = 232
            Top = 8
            Width = 6
            Height = 13
            Caption = 'a'
          end
          object SemanalAIni: TZetaKeyLookup
            Left = 136
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
          end
          object SemanalAFin: TZetaKeyLookup
            Tag = -1
            Left = 240
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
          end
        end
        object PSemanalB: TPanel
          Tag = 7
          Left = 2
          Top = 225
          Width = 442
          Height = 30
          Align = alTop
          TabOrder = 7
          object lblSemanalB: TLabel
            Left = 76
            Top = 8
            Width = 54
            Height = 13
            Caption = 'Semanal B:'
          end
          object Label14: TLabel
            Left = 232
            Top = 8
            Width = 6
            Height = 13
            Caption = 'a'
          end
          object SemanalBIni: TZetaKeyLookup
            Left = 136
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
          end
          object SemanalBFin: TZetaKeyLookup
            Tag = -1
            Left = 248
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
          end
        end
        object PQuincenalA: TPanel
          Tag = 8
          Left = 2
          Top = 255
          Width = 442
          Height = 30
          Align = alTop
          TabOrder = 8
          object lblQuincenalA: TLabel
            Left = 69
            Top = 8
            Width = 61
            Height = 13
            Caption = 'Quincenal A:'
          end
          object Label12: TLabel
            Left = 232
            Top = 8
            Width = 6
            Height = 13
            Caption = 'a'
          end
          object QuincenalAIni: TZetaKeyLookup
            Left = 136
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
          end
          object QuincenalAFin: TZetaKeyLookup
            Tag = -1
            Left = 248
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
          end
        end
        object PQuincenalB: TPanel
          Tag = 9
          Left = 2
          Top = 285
          Width = 442
          Height = 30
          Align = alTop
          TabOrder = 9
          object lblQuincenalB: TLabel
            Left = 69
            Top = 8
            Width = 61
            Height = 13
            Caption = 'Quincenal B:'
          end
          object Label10: TLabel
            Left = 232
            Top = 8
            Width = 6
            Height = 13
            Caption = 'a'
          end
          object QuincenalBIni: TZetaKeyLookup
            Left = 136
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
          end
          object QuincenalBFin: TZetaKeyLookup
            Tag = -1
            Left = 248
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
          end
        end
        object PCatorcenalA: TPanel
          Tag = 10
          Left = 2
          Top = 315
          Width = 442
          Height = 30
          Align = alTop
          TabOrder = 10
          object lblCatorcenalA: TLabel
            Left = 66
            Top = 8
            Width = 64
            Height = 13
            Caption = 'Catorcenal A:'
          end
          object Label8: TLabel
            Left = 232
            Top = 8
            Width = 6
            Height = 13
            Caption = 'a'
          end
          object CatorcenalAIni: TZetaKeyLookup
            Left = 136
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
          end
          object CatorcenalAFin: TZetaKeyLookup
            Tag = -1
            Left = 248
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
          end
        end
        object PCatorcenalB: TPanel
          Tag = 11
          Left = 2
          Top = 345
          Width = 442
          Height = 30
          Align = alTop
          TabOrder = 11
          object lblCatorcenalB: TLabel
            Left = 66
            Top = 8
            Width = 64
            Height = 13
            Caption = 'Catorcenal B:'
          end
          object Label6: TLabel
            Left = 232
            Top = 8
            Width = 6
            Height = 13
            Caption = 'a'
          end
          object CatorcenalBIni: TZetaKeyLookup
            Left = 136
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
          end
          object CatorcenalBFin: TZetaKeyLookup
            Tag = -1
            Left = 248
            Top = 4
            Width = 85
            Height = 21
            LookupDataset = dmCatalogos.cdsPeriodoOtro
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
          end
        end
      end
    end
    inherited FiltrosCondiciones: TTabSheet
      Enabled = False
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 446
        Lines.Strings = (
          'Advertencia'
          'Se Calcular'#225' el pago de retenciones de Fonacot'
          'correspondientes al mes y a'#241'o indicados en el proceso'
          ''
          
            'Si el proceso es exitoso el estatus del pago quedar'#225' como '#39'Calcu' +
            'lado '
          'Total'#39','
          'en caso de quedar inconcluso quedar'#225' como '#39'Calculado Parcial'#39
          ''
          'Presione el bot'#243'n '#39'Ejecutar'#39' para iniciar el proceso')
      end
      inherited ProgressPanel: TPanel
        Top = 320
        Width = 446
      end
    end
  end
end
