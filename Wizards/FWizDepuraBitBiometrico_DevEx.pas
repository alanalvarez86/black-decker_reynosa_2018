unit FWizDepuraBitBiometrico_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Mask, ZetaFecha, StdCtrls, ComCtrls, Buttons,
  ZetaWizard, ExtCtrls, ZcxBaseWizard, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, ZetaKeyLookup_DevEx;

type
  TWizDepuraBitBiometrico_DevEx = class(TcxBaseWizard)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    dInicio: TZetaFecha;
    dFin: TZetaFecha;
    lblEmpresa: TLabel;
    luEmpresa: TZetaKeyLookup_DevEx;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  end;

var
  WizDepuraBitBiometrico_DevEx: TWizDepuraBitBiometrico_DevEx;

implementation

uses
    DProcesos,
    DSistema,
    DBaseSistema,
    ZetaCommonClasses;

{$R *.dfm}

procedure TWizDepuraBitBiometrico_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddDate( 'FechaInicio', dInicio.Valor );
          AddDate( 'FechaFin', dFin.Valor );
          AddString( 'Empresa', luEmpresa.Llave );
     end;
      inherited;
     with Descripciones do
     begin
          AddString( 'Empresa', luEmpresa.Llave );
          AddDate( 'Fecha Inicial', dInicio.Valor );
          AddDate( 'Fecha Final', dFin.Valor );
     end;
end;

function TWizDepuraBitBiometrico_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.DepuraBitacoraBio( ParameterList );
end;

procedure TWizDepuraBitBiometrico_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.Adelante then
     begin
          if( WizardControl.ActivePage = Parametros )then
          begin
               if dInicio.Valor > dFin.Valor then
                 CanMove := Error( '�Error en rango de fechas! La fecha final debe ser mayor o igual a la fecha inicial', dInicio )
          end;
     end;
end;

procedure TWizDepuraBitBiometrico_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_PROCESOS_BORRARBITTERMINALESGTI;
     dInicio.Valor := Now;
     dFin.Valor := Now;

     with dmSistema do
     begin
          cdsEmpresasLookUp.Conectar;
          luEmpresa.LookupDataset := cdsEmpresasLookUp;
     end;
end;

procedure TWizDepuraBitBiometrico_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetFocusedControl( Self.luEmpresa );
end;

end.
