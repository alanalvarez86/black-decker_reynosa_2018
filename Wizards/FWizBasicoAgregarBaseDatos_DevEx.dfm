inherited WizBasicoAgregarBaseDatos_DevEx: TWizBasicoAgregarBaseDatos_DevEx
  Left = 776
  Top = 232
  Caption = 'WizBasicoAgregarBaseDatos_DevEx'
  ClientHeight = 491
  ClientWidth = 580
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 580
    Height = 491
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
      7249000003CF494441545847B5963F68137114C76F70C8D0A1430707073B0876
      7013914E824B06070727117470D04D5CB248AEA2B56A6D93264DD2FB97CBE572
      97FBD3186A5B8B58A8884B2705B577AD60411011840E0A452AC6F7E25DFCE5EE
      97988BF5C187DF71F77BDFF7EEDDFBFD7EC7A0B19A3BC4569DC3FB86F5E66053
      B817634D672C69BA7BACE536F693A4E54E7A21BA5BD272765B4E9008386E9342
      FD825A5E88EED672C0446A5B87F05ED2DC9C20C5FAA519E06FE64F4E9ACE8A77
      8B618D8D93A450BF7872DDCD9FDCEC03D38D6343266DB74A0AF58B17A2BB41E0
      4F34E77F057477BC10DD8DB59C4B507E079B6FDF30DD77B0BAAE7821FECD1A8D
      C67F8561E5ED186BB867B11291B19DE35E9EFD5BD2DAACD3BE61377E37AC33C6
      AEAD1D78707392F11172A5F5ECFDDC77B86EA4C6D33FE7A6B92F1541BF4ECE09
      D25A05BDE2053F17145AB49731994610295B5C87B16D2E49E40460A95EA00969
      821E87319400546457961E0EC175C8078994007478019DCAC5DA113E2D7C5079
      3D6194EC13425E7D0AF743C17DD213991FF029129A581D56B9CA1DA55056E17E
      B4047069DD365E0E88423DC6A7B8CF64807EA870EA0D18B109DDD7B480210CF7
      1A3A94382D4D0AF5CBD4ADA9862E19C30C2E252C2DBCA1DC0DB6BE3D98139FC6
      66BD2EA7313755D8A9F05AC290AA71E8898B50EA85A95BD3D4B948794EE5BCC5
      D89BF1F2F2199A1022CF16D7E1FBC6E0BAADC974B13A9A9998F9817382E4EECF
      EE32F866B86D36D775276CE71426004E8CCA6B77A6A17CA450F65E76CF281A1D
      3BBD2A55AFC2D8EE7337B307F7E37018392BD46F4EE2EDEB2886C879E51929A6
      E44B0BFE331A589960D255C9C06AF6B80A60ED9309701969CB174274414BF8CF
      3A917F50F80663CBC792CDD378BFB7048C8DCB640262B6F8C21742A0998AFE33
      1AB3E26A2CD88CA66C454800FAC04F40158D44B09C1CEC0B8BE662DBB9405216
      CDF330B6F9881969C3D41E0DF69400FC2FAA9800272F86847C3A5521AFAE1E84
      A5BB0BD7219FB914FF11FF885ED282928C996FBFCE48CF63BAF678303D9EFA49
      1343E0ADB654C93AA3E92B838A5C3F221417AEA6EF66F7687311A5A0180C3BEF
      1E4DDA9BA9E0C643326EBC9245F5C9517062A0E36B41A17E9957EC11AC6CCF06
      4EB0A68D21DC4082625181B3A079204532744024B116CF4EF33B65AE928684CE
      E316EC0BD3C0A62DE5959A2EEAD7E127E5A33023BE5FB6979A4D1BC9D081C612
      FC8C940B0A366128787E32F7CD56E647E0BA351FCE8BD6961DC948912070001D
      833194809C6BF60CD507896434019F3A9414F6F7D0A1A3F195E6B9DF89484613
      08A24AF6107CEB51AB648DD8256B8036E70F93CC2FADD62E283A1FEEC1000000
      0049454E44AE426082}
    inherited Parametros: TdxWizardControlPage
      Header.Description = 'Descripcion <Modificar>'
      object lblSeleccionarBD: TLabel
        Left = 153
        Top = 164
        Width = 129
        Height = 13
        Caption = 'Seleccionar base de datos:'
        Transparent = True
      end
      object lbNombreBD: TLabel
        Left = 146
        Top = 108
        Width = 136
        Height = 13
        Caption = 'Nombre de la base de datos:'
        Transparent = True
      end
      object cbBasesDatos: TComboBox
        Left = 288
        Top = 160
        Width = 177
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        Enabled = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 3
      end
      object txtBaseDatos: TEdit
        Left = 288
        Top = 104
        Width = 177
        Height = 21
        TabOrder = 1
      end
      object rbBDNueva: TcxRadioButton
        Left = 120
        Top = 77
        Width = 161
        Height = 17
        Caption = 'C&rear base de datos nueva'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = rbBDNuevaClick
        Transparent = True
      end
      object rbBDExistente: TcxRadioButton
        Left = 120
        Top = 141
        Width = 161
        Height = 17
        Caption = '&Base de datos existente:'
        TabOrder = 2
        OnClick = rbBDExistenteClick
        Transparent = True
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.Description = 
        'Al dar clic en Aplicar se iniciar'#225' el proceso mostrando el avanc' +
        'e en el  recuadro inferior.'
      Header.Title = 'Aplicar proceso'
      inherited GrupoParametros: TcxGroupBox
        Top = 65
        Height = 103
        Width = 558
      end
      inherited cxGroupBox1: TcxGroupBox
        ParentColor = False
        Height = 65
        Width = 558
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Height = 57
          Width = 490
          AnchorY = 36
        end
      end
      object GrupoAvance: TcxGroupBox
        Left = 0
        Top = 168
        Align = alBottom
        Caption = 'Avance'
        TabOrder = 2
        Visible = False
        Height = 183
        Width = 558
        object cxMemoStatus: TcxMemo
          Left = 188
          Top = 15
          Align = alClient
          Lines.Strings = (
            'cxMemoStatus')
          Properties.ReadOnly = True
          Style.Edges = []
          TabOrder = 0
          Height = 158
          Width = 367
        end
        object cxMemoPasos: TcxMemo
          Left = 3
          Top = 15
          Align = alLeft
          Lines.Strings = (
            'cxMemoPasos')
          Properties.Alignment = taRightJustify
          Properties.ReadOnly = True
          Style.Edges = []
          TabOrder = 1
          Height = 158
          Width = 185
        end
      end
    end
    object NuevaBD2: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Capturar el usuario y clave para el acceso a la base de datos.'
      Header.Title = ' Ubicaci'#243'n y acceso'
      object lblUbicacion: TLabel
        Left = 113
        Top = 82
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ubicaci'#243'n:'
        Transparent = True
      end
      object GBUsuariosMSSQL: TcxGroupBox
        Left = 104
        Top = 104
        Caption = 'Usuario de MSSQL para crear y acceder a la BD:'
        TabOrder = 0
        Height = 145
        Width = 369
        object lblClave: TLabel
          Left = 72
          Top = 101
          Width = 30
          Height = 13
          Caption = 'Clave:'
          Transparent = True
        end
        object lblUsuario: TLabel
          Left = 64
          Top = 77
          Width = 39
          Height = 13
          Caption = 'Usuario:'
          Transparent = True
        end
        object txtUsuario: TEdit
          Left = 112
          Top = 75
          Width = 121
          Height = 21
          Ctl3D = True
          ParentCtl3D = False
          TabOrder = 0
        end
        object txtClave: TMaskEdit
          Left = 112
          Top = 99
          Width = 121
          Height = 21
          PasswordChar = '*'
          TabOrder = 1
        end
        object rbSameUser: TcxRadioButton
          Left = 16
          Top = 29
          Width = 271
          Height = 17
          Caption = 'Usuario y clave default (Configuraci'#243'n del Sistema)'
          TabOrder = 2
          OnClick = rbSameUserClick
          Transparent = True
        end
        object rbOtroUser: TcxRadioButton
          Left = 16
          Top = 50
          Width = 209
          Height = 17
          Caption = 'Utilizar otro usuario y clave de acceso:'
          TabOrder = 3
          OnClick = rbOtroUserClick
          Transparent = True
        end
      end
      object lblDatoUbicacion: TcxLabel
        Left = 168
        Top = 80
        AutoSize = False
        Style.BorderStyle = ebsFlat
        Height = 17
        Width = 306
      end
    end
    object Estructura: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        'Ingrese los campos con los cuales se va a identificar a la base ' +
        'de datos.'
      Header.Title = 'Registro de Base de Datos'
      object lblDescripcion: TLabel
        Left = 138
        Top = 132
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
        Transparent = True
      end
      object lblCodigo: TLabel
        Left = 161
        Top = 108
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        Transparent = True
      end
      object txtDescripcion: TZetaEdit
        Left = 200
        Top = 128
        Width = 257
        Height = 21
        TabOrder = 1
      end
      object txtCodigo: TZetaEdit
        Left = 200
        Top = 104
        Width = 121
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 0
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Top = 259
  end
end
