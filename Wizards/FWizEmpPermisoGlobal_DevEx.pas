unit FWizEmpPermisoGlobal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls, ExtCtrls, DBCtrls,
  ZetaKeyCombo, ZetaDBTextBox, ZetaNumero,
  Mask, ZetaFecha, FWizEmpBaseFiltro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons,
  cxRadioGroup, cxTextEdit, cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses,
  cxImage, dxCustomWizardControl, dxWizardControl, cxCheckBox;

type
  TWizEmpPermisoGlobal_DevEx = class(TWizEmpBaseFiltro)
    LIN_FEC_INI: TLabel;
    FechaIni: TZetaFecha;
    LIN_DIAS: TLabel;
    Dias: TZetaNumero;
    LIN_FEC_FIN: TLabel;
    FechaFin: TZetaTextBox;
    Label3: TLabel;
    Clase: TZetaKeyCombo;
    Label13: TLabel;
    TipoPermiso: TZetaKeyLookup_DevEx;
    Label11: TLabel;
    Referencia: TEdit;
    Label5: TLabel;
    Observaciones: TEdit;
    lAjustarFechas: TcxCheckBox;
    rbDias: TcxRadioButton;
    rbFechaFin: TcxRadioButton;
    gbDiasFechaReg: TcxGroupBox;
    FechaReg: TZetaFecha;
    DiasHab: TZetaNumero;
    LIN_DIAS_HAB: TLabel;
    LIN_FEC_REG: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FechaIniValidDate(Sender: TObject);
    procedure DiasExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx     //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure ClaseChange(Sender: TObject);
    procedure ClaseExit(Sender: TObject);
    procedure rbDiasClick(Sender: TObject);
    procedure rbFechaFinClick(Sender: TObject);
    procedure FechaRegChange(Sender: TObject);
  private
    PermisoFiltro : string;
    procedure SetFechaFin;
    procedure SetFiltros;
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpPermisoGlobal_DevEx: TWizEmpPermisoGlobal_DevEx;

implementation

uses dCliente,
     dTablas,
     dProcesos,
     FToolsRH,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZBaseSelectGrid_DevEx,
     DRecursos,
     FEmpleadoGridSelectPermisoGlobal_DevEx,
     ZGlobalTress,
     DGlobal, ZcxWizardBasico;

{$R *.DFM}

procedure TWizEmpPermisoGlobal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PermisoFiltro := Format( K_INCIDENCIAS_FILTRO, [ Ord( eiPermiso ) ] );
     ParametrosControl := FechaIni;
     Dias.Valor := 1;
     FechaIni.Valor := dmCliente.FechaDefault;
     Clase.ItemIndex := Ord( tpConGoce );
     lAjustarFechas.Checked := TRUE;
     with TipoPermiso do
     begin
          LookupDataSet := dmTablas.cdsIncidencias;
          Filtro := PermisoFiltro;
     end;
     
    // gbDiasFechaReg.Left := 72;
    // gbDiasFechaReg.Top := 42;
     gbDiasFechaReg.Left := 66;
     gbDiasFechaReg.Top := 40;
     rbDias.Checked := TRUE;
     DiasHab.Valor := 1;
     FechaReg.Valor := FechaIni.Valor + 1;
     HelpContext := H10175_Permisos_globales;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;

end;

procedure TWizEmpPermisoGlobal_DevEx.SetFiltros;
begin
     TipoPermiso.Filtro := ConcatFiltros( PermisoFiltro,Format('TB_PERMISO = %d OR TB_PERMISO = %d',[ K_MOTIVO_PERMISOS_OFFSET, Clase.Valor ] ) );
end;

procedure TWizEmpPermisoGlobal_DevEx.FormShow(Sender: TObject);
var bGlobalPermisosDiasHabiles : Boolean;
begin
     inherited;
     dmTablas.cdsIncidencias.Conectar;
     SetFiltros;
     bGlobalPermisosDiasHabiles :=
          Global.GetGlobalBooleano( K_GLOBAL_PERMISOS_DIAS_HABILES );
     Dias.Visible := not bGlobalPermisosDiasHabiles;
     FechaFin.Visible := not bGlobalPermisosDiasHabiles;
     LIN_DIAS.Visible := not bGlobalPermisosDiasHabiles;
     LIN_FEC_FIN.Visible := not bGlobalPermisosDiasHabiles;
     gbDiasFechaReg.Visible := bGlobalPermisosDiasHabiles;

     Advertencia.Caption := 'Al ejecutar el proceso se crear�n los registros a los empleados indicados.';
end;

procedure TWizEmpPermisoGlobal_DevEx.CargaParametros;
var bGlobalPermisosDiasHabiles : Boolean;
begin
     bGlobalPermisosDiasHabiles := Global.GetGlobalBooleano( K_GLOBAL_PERMISOS_DIAS_HABILES );
     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'FechaIni', FechaIni.Valor );
          if bGlobalPermisosDiasHabiles then
             AddInteger( 'Dias', DiasHab.ValorEntero )
          else
              AddInteger( 'Dias', Dias.ValorEntero );
          if (FechaReg.Enabled) then
             AddDate( 'FechaFin', FechaReg.Valor );
          AddInteger( 'Clase', Clase.ItemIndex );
          AddString( 'TipoPermiso', TipoPermiso.Llave );
          AddString( 'Referencia', Referencia.Text );
          AddString( 'Observaciones', Observaciones.Text );
          AddBoolean( 'AjustarFechas', lAjustarFechas.Checked );
          AddBoolean( 'PermisoDiasHabiles',  bGlobalPermisosDiasHabiles);
          AddBoolean( 'EnviaFechaRegreso',  rbFechaFin.Checked);
     end;

     with Descripciones do
     begin
          AddDate( 'Inicio', FechaIni.Valor );
          if bGlobalPermisosDiasHabiles then
             AddInteger( 'Dias', DiasHab.ValorEntero )
          else
              AddInteger( 'Dias', Dias.ValorEntero );
          if (FechaReg.Enabled) then
             AddDate( 'Fin', FechaReg.Valor );
          AddString( 'Clase', ObtieneElemento( lfTipoPermiso, Clase.ItemIndex ) );
          AddString( 'Tipo', GetDescripLlave( TipoPermiso ) );
          AddString( 'Referencia', Referencia.Text );
          AddString( 'Observaciones', Observaciones.Text );
          AddBoolean( '�Ajustar Fechas?', lAjustarFechas.Checked );
     end;
end;

procedure TWizEmpPermisoGlobal_DevEx.CargaListaVerificacion;
begin
     dmProcesos.PermisoGlobalGetLista( ParameterList );
end;

function TWizEmpPermisoGlobal_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelectPermisoGlobal_DevEx );
end;

function TWizEmpPermisoGlobal_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.PermisoGlobal( ParameterList, Verificacion );
end;

procedure TWizEmpPermisoGlobal_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var bGlobalPermisosDiasHabiles : Boolean;
begin
     inherited;
     ParametrosControl := nil;
     bGlobalPermisosDiasHabiles := Global.GetGlobalBooleano( K_GLOBAL_PERMISOS_DIAS_HABILES );
     if Wizard.Adelante then
     begin
          //if ( PageControl.ActivePage = Parametros ) then
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               if bGlobalPermisosDiasHabiles then
               begin
                     if ( (rbDias.Checked) AND (DiasHab.Valor <= 0) ) then
                        CanMove := Error( '� D�as de Permiso debe ser mayor que cero !', DiasHab )
               end
               else
               begin
                     if ( Dias.Valor <= 0 ) then
                        CanMove := Error( '� D�as de Permiso debe ser mayor que cero !', Dias )
               end;
               if StrVacio( TipoPermiso.Llave ) then
                  CanMove := Error( '� No se ha especificado un tipo de Permiso !', TipoPermiso );
          end;
     end;
end;

procedure TWizEmpPermisoGlobal_DevEx.FechaIniValidDate(Sender: TObject);
begin
     inherited;
     SetFechaFin;
end;

procedure TWizEmpPermisoGlobal_DevEx.DiasExit(Sender: TObject);
begin
     inherited;
     SetFechaFin;
end;

procedure TWizEmpPermisoGlobal_DevEx.SetFechaFin;
begin
     if not Global.GetGlobalBooleano( K_GLOBAL_PERMISOS_DIAS_HABILES ) then
        FechaFin.Caption := ZetaCommonTools.FechaCorta( FechaIni.Valor + Dias.ValorEntero )
     else
     begin  
            if (FechaReg.Valor <= FechaIni.Valor) and (rbFechaFin.Checked)then
               FechaReg.Valor := FechaIni.Valor + 1;
     end;
end;

procedure TWizEmpPermisoGlobal_DevEx.ClaseChange(Sender: TObject);
begin
     inherited;
     SetFiltros;
end;

procedure TWizEmpPermisoGlobal_DevEx.ClaseExit(Sender: TObject);
begin
     inherited;
     with dmRecursos do
     begin
          if not dmRecursos.ValidarMotivoPermisos( TipoPermiso.Llave ,Clase.Valor ) then
          begin
               TipoPermiso.SetLlaveDescripcion(VACIO,VACIO);  
          end;
     end;
end;

procedure TWizEmpPermisoGlobal_DevEx.rbDiasClick(Sender: TObject);
begin
     inherited;
     DiasHab.Enabled := rbDias.Checked;
     FechaReg.Enabled := not rbDias.Checked;
end;

procedure TWizEmpPermisoGlobal_DevEx.rbFechaFinClick(Sender: TObject);
begin
     inherited;
     DiasHab.Enabled := rbDias.Checked;
     FechaReg.Enabled := not rbDias.Checked;
     FechaReg.Valor := FechaIni.Valor + 1;
end;

procedure TWizEmpPermisoGlobal_DevEx.FechaRegChange(Sender: TObject);
begin
  inherited;
  if (FechaReg.Valor <= FechaIni.Valor) then
     FechaReg.Valor := FechaIni.Valor + 1;
end;


//DevEx     //Agregado para enfocar un control
procedure TWizEmpPermisoGlobal_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
         ParametrosControl := FechaIni
     else
         ParametrosControl := nil;
     inherited;
end;
           
end.
