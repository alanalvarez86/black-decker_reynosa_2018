inherited EmpRegresarHerrGridSelect_DevEx: TEmpRegresarHerrGridSelect_DevEx
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Las Herramientas Deseadas'
  ClientWidth = 523
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 523
    inherited OK_DevEx: TcxButton
      Left = 359
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 438
    end
  end
  inherited PanelSuperior: TPanel
    Width = 523
    inherited BtnFiltrar: TcxButton
      Left = 438
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 523
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 55
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre Completo'
        DataBinding.FieldName = 'PRETTYNAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 220
      end
      object TO_CODIGO: TcxGridDBColumn
        Caption = 'Herramienta'
        DataBinding.FieldName = 'TO_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 70
      end
      object KT_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'KT_FEC_INI'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 75
      end
      object KT_REFEREN: TcxGridDBColumn
        Caption = 'Referencia'
        DataBinding.FieldName = 'KT_REFEREN'
        Width = 110
      end
      object KT_TALLA: TcxGridDBColumn
        Caption = 'Talla'
        DataBinding.FieldName = 'KT_TALLA'
        Width = 70
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
