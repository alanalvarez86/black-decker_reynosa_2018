unit FWizNomCalcular_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls, ZetaEdit,
     ZetaDBTextBox, ZetaCommonLists, FWizNomBase_DevEx,
     cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
      TressMorado2013, dxSkinsDefaultPainters,
     cxContainer, cxEdit, Menus, ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons,
     cxRadioGroup, cxTextEdit, cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses,
     cxImage, dxCustomWizardControl, dxWizardControl, cxCheckBox;

type
  TWizNomCalcular_DevEx = class(TWizNomBase_DevEx)
    GroupBox: TcxGroupBox;
    rbTipoNueva: TcxRadioButton;
    rbTipoTodos: TcxRadioButton;
    rbTipoRango: TcxRadioButton;
    rbTipoPendientes: TcxRadioButton;
    CBMuestraTiempos: TcxCheckBox;
    CBScript: TcxCheckBox;
    TipoCalculo: TdxWizardControlPage;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure rbTipoNuevaClick(Sender: TObject);
    procedure rbTipoTodosClick(Sender: TObject);
    procedure rbTipoRangoClick(Sender: TObject);
    procedure rbTipoPendientesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure SetControls( const eTipo: eTipoCalculo );
    function GetControlTipo(const eTipo: eTipoCalculo): TWinControl;
    function GetPosDerecho(const eTipo: eTipoCalculo): Integer;
    function GetTipoCalculo: eTipoCalculo;
    function ValidaTipoCalculo: Boolean;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomCalcular_DevEx: TWizNomCalcular_DevEx;

implementation

uses DProcesos,
     DCliente,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonClasses,
     FTressShell, ZCXBaseWizardFiltro, ZcxWizardBasico, ZcxBaseWizard;

{$R *.DFM}

procedure TWizNomCalcular_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H30341_Calcular_nomina;
     rbTipoNueva.Caption := ObtieneElemento( lfTipoCalculo, Ord( tcNueva ) );
     rbTipoTodos.Caption := ObtieneElemento( lfTipoCalculo, Ord( tcTodos ) );
     rbTipoRango.Caption := ObtieneElemento( lfTipoCalculo, Ord( tcRango ) );
     rbTipoPendientes.Caption := ObtieneElemento( lfTipoCalculo, Ord( tcPendiente ) );

     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 4;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 3;
     TipoCalculo.PageIndex := WizardControl.PageCount - 2;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizNomCalcular_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddBoolean( 'MuestraTiempos', CBMuestraTiempos.Checked );
          AddBoolean( 'MuestraScript', CBScript.Checked );
          AddInteger( 'TipoCalculo', Ord( GetTipoCalculo ) );
     end;

     with Descripciones do
     begin
          AddBoolean( 'Muestra tiempos', CBMuestraTiempos.Checked );
          AddBoolean( 'Muestra script SQL', CBScript.Checked );
          AddString ( 'Tipo de c�lculo', ObtieneElemento( lfTipoCalculo, Ord( GetTipoCalculo ) ) );  
     end;
end;

procedure TWizNomCalcular_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   eStatus: eStatusPeriodo;
begin
     inherited;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( FiltrosCondiciones ) then
               begin
                    eStatus := dmCliente.GetDatosPeriodoActivo.Status;
                    //PENDIENTE : antes estaba en DEntidades
                    //CBMuestraTiempos.Checked := dmCliente.MuestraTiempos;
                    if ( dmCliente.GetDatosPeriodoActivo.Uso = upEspecial ) then   // debe ser soloex
                    begin
                         rbTipoNueva.Enabled := TRUE;
                         rbTipoTodos.Enabled := TRUE;
                         rbTipoRango.Enabled := ( TipoRango in [ raRango, raLista ] ) ;  // depende de raRango
                         rbTipoPendientes.Enabled := ( rbTipoRango.Enabled  or ( eStatus = spCalculadaParcial ) );   // depende de status
                         if ( eStatus = spNueva ) then
                            SetControls( tcNueva )
                         else
                             if rbTipoRango.Enabled then
                                SetControls( tcRango )
                             else
                                 if rbTipoPendientes.Enabled then
                                    SetControls( tcPendiente )
                                 else
                                     SetControls( tcTodos );
                    end
                    else if ( eStatus = spNueva ) then
                    begin
                         SetControls( tcNueva );
                         rbTipoNueva.Enabled := TRUE;
                         rbTipoTodos.Enabled := FALSE;
                         rbTipoRango.Enabled := FALSE;
                         rbTipoPendientes.Enabled := FALSE;
                    end
                    else if ( TipoRango in [ raRango, raLista ] ) then
                    begin
                         SetControls( tcRango );
                         rbTipoTodos.Enabled := FALSE;
                         rbTipoNueva.Enabled := FALSE;
                         rbTipoRango.Enabled := TRUE;
                         rbTipoPendientes.Enabled := TRUE;
                    end
                    else if ( eStatus = spCalculadaParcial ) then
                    begin
                         SetControls( tcPendiente );
                         rbTipoNueva.Enabled := TRUE;
                         rbTipoTodos.Enabled := TRUE;
                         rbTipoRango.Enabled := FALSE;
                         rbTipoPendientes.Enabled := TRUE;
                    end
                    else if ( eStatus =  spCalculadaTotal ) then
                    begin
                         SetControls( tcTodos );
                         rbTipoNueva.Enabled := TRUE;
                         rbTipoTodos.Enabled := TRUE;
                         rbTipoRango.Enabled := FALSE;
                         rbTipoPendientes.Enabled := FALSE;
                    end
                    else if ( eStatus =  spAfectadaParcial ) then
                    begin
                         SetControls( tcPendiente );
                         rbTipoNueva.Enabled := FALSE;
                         rbTipoTodos.Enabled := TRUE;
                         rbTipoRango.Enabled := TRUE;
                         rbTipoPendientes.Enabled := TRUE;
                    end
                    else
                    begin
                         SetControls( tcTodos );
                         rbTipoNueva.Enabled := TRUE;
                         rbTipoTodos.Enabled := TRUE;
                         rbTipoRango.Enabled := FALSE;
                         rbTipoPendientes.Enabled := FALSE;
                    end;
               end
               else if EsPaginaActual( TipoCalculo ) then
               begin
                    CanMove := ValidaTipoCalculo;
               end;
          end;//Adelante
     end;//with
end;

procedure TWizNomCalcular_DevEx.SetControls( const eTipo: eTipoCalculo );
begin
     rbTipoNueva.Checked := ( eTipo = tcNueva );
     rbTipoTodos.Checked := ( eTipo = tcTodos );
     rbTipoRango.Checked := ( eTipo = tcRango );
     rbTipoPendientes.Checked := ( eTipo = tcPendiente );
end;

procedure TWizNomCalcular_DevEx.rbTipoNuevaClick(Sender: TObject);
begin
     inherited;
     SetControls( tcNueva );
end;

procedure TWizNomCalcular_DevEx.rbTipoTodosClick(Sender: TObject);
begin
     inherited;
     SetControls( tcTodos );
end;

procedure TWizNomCalcular_DevEx.rbTipoRangoClick(Sender: TObject);
begin
     inherited;
     SetControls( tcRango );
end;

procedure TWizNomCalcular_DevEx.rbTipoPendientesClick(Sender: TObject);
begin
     inherited;
     SetControls( tcPendiente );
end;

function TWizNomCalcular_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CalcularNomina(ParameterList);
end;

function TWizNomCalcular_DevEx.ValidaTipoCalculo: Boolean;
var
   eTipo: eTipoCalculo;
begin
     eTipo := GetTipoCalculo;
     Result := ZAccesosMgr.CheckDerecho( D_NOM_PROC_CALCULAR, GetPosDerecho( eTipo ) );
     if not Result then
     begin
          Error( Format( 'No Tiene Derecho para Calcular N�mina como %s', [
                 ZetaCommonLists.ObtieneElemento( lfTipoCalculo, Ord( eTipo ) ) ] ),
                 GetControlTipo( eTipo ) );
     end;
end;

function TWizNomCalcular_DevEx.GetTipoCalculo: eTipoCalculo;
begin
     if rbTipoNueva.Checked then
        Result := tcNueva
     else if rbTipoTodos.Checked then
        Result := tcTodos
     else if rbTipoRango.Checked then
        Result := tcRango
     else {if rbTipoPendientes.Checked then}
        Result := tcPendiente;
end;

function TWizNomCalcular_DevEx.GetPosDerecho( const eTipo: eTipoCalculo ): Integer;
begin
     if ( eTipo = tcNueva ) then
        Result := K_DERECHO_CONSULTA
     else if ( eTipo = tcTodos ) then
        Result := K_DERECHO_ALTA
     else if ( eTipo = tcRango ) then
        Result := K_DERECHO_BAJA
     else { if ( eTipo = tcPendiente ) then }
        Result := K_DERECHO_CAMBIO;
end;

function TWizNomCalcular_DevEx.GetControlTipo( const eTipo: eTipoCalculo ): TWinControl;
begin
     if ( eTipo = tcNueva ) then
        Result := rbTipoNueva
     else if ( eTipo = tcTodos ) then
        Result := rbTipoTodos
     else if ( eTipo = tcRango ) then
        Result := rbTipoRango
     else { if ( eTipo = tcPendiente ) then }
        Result := rbTipoPendientes;
end;


procedure TWizNomCalcular_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Al aplicar este proceso se calcular�n los montos a pagar para los empleados indicados.';

end;


end.
