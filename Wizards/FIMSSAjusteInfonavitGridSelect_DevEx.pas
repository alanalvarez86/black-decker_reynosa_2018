unit FIMSSAjusteInfonavitGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, ExtCtrls, ZBaseSelectGrid_DevEx,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  cxButtons;

type
  TIMSSAjusteInfonavitGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    K_PRETTYNAME: TcxGridDBColumn;
    CB_INFCRED: TcxGridDBColumn;
    K_AMORTIZA: TcxGridDBColumn;
    K_ACUMULA: TcxGridDBColumn;
    K_OTROS_AJUSTES: TcxGridDBColumn;
    K_MONTO_AJUSTE: TcxGridDBColumn;
    K_SALDO: TcxGridDBColumn;
    LE_DIAS_BM: TcxGridDBColumn;
    LE_PROV: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  IMSSAjusteInfonavitGridSelect_DevEx: TIMSSAjusteInfonavitGridSelect_DevEx;

implementation

{$R *.dfm}

procedure TIMSSAjusteInfonavitGridSelect_DevEx.FormShow(Sender: TObject);
begin 
     with DataSet do
     begin
          MaskPesos( 'K_AMORTIZA' );
          MaskPesos( 'K_ACUMULA' );
          MaskPesos( 'K_OTROS_AJUSTES' );
          MaskPesos( 'K_MONTO_AJUSTE' );
          MaskPesos( 'K_SALDO');
          MaskPesos( 'LE_PROVISION');
     end;
     inherited;
     (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).Caption := 'Empleado';
     (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).MinWidth := 75;
end;

end.
