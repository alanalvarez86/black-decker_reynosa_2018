unit FWizAsistPollRelojes_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Buttons, StdCtrls, ComCtrls, ExtCtrls,
     ZetaWizard, ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, ZetaCXWizard, cxGroupBox,
  cxLabel, dxGDIPlusClasses, cxImage, dxCustomWizardControl,
  dxWizardControl, Menus, cxButtons, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters;

type
  TWizAsistPollRelojes_DevEx = class(TcxBaseWizard)
    ProgramaLBL: TLabel;
    ComandosLBL: TLabel;
    ArchivoLBL: TLabel;
    ProcesarLBL: TLabel;
    Programa: TEdit;
    Comandos: TEdit;
    Archivo: TEdit;
    ProcesarTarjeta: TCheckBox;
    ProgramaSeek: TcxButton;
    ArchivoSeek: TcxButton;
    OpenDialog: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure ProgramaSeekClick(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    //function EjecutaPrograma: Boolean;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizAsistPollRelojes_DevEx: TWizAsistPollRelojes_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaMessages,
     ZGlobalTress,
     ZetaDialogo,
     DCliente,
     DProcesos;

{$R *.DFM}

procedure TWizAsistPollRelojes_DevEx.FormCreate(Sender: TObject);
begin
     ParametrosControl := Programa;
     Programa.Tag := K_GLOBAL_DEF_PROGRAMA_POLL;
     Comandos.Tag := K_GLOBAL_DEF_PARAMETROS_POLL;
     Archivo.Tag := K_GLOBAL_DEF_ARCHIVO_POLL;
     inherited;
     HelpContext := H20231_POLL_relojes;
end;

procedure TWizAsistPollRelojes_DevEx.CargaParametros;
begin

     inherited CargaParametros;
     with ParameterList do
     begin
          AddString( 'Archivo', Archivo.Text );
          AddString( 'Programa', Programa.Text );
          AddString( 'Comandos', Comandos.Text );
          AddBoolean( 'ProcesarTarjetas', ProcesarTarjeta.Checked );
          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
     end;

     with Descripciones do
     begin
          AddString( 'Programa', Programa.Text );
          AddString( 'Parametros', Comandos.Text );
          AddString( 'Archivo POll', Archivo.Text );
          AddBoolean( 'Procesar Tarjetas', ProcesarTarjeta.Checked );
     end;
end;

function TWizAsistPollRelojes_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.Poll( ParameterList );
end;

procedure TWizAsistPollRelojes_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   sArchivo, sMensaje : String;
begin
ParametrosControl := nil;
     inherited;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          sArchivo := Archivo.Text;
          if ZetaCommonTools.StrVacio( sArchivo ) and ( not ProcesarTarjeta.Checked ) then
             CanMove := Error( 'No Existen Par�metros para Ejecutar Poll de Relojes', self.Archivo )
          else if ( not EjecutaProgramaArchivo( Programa.Text, Comandos.Text, sArchivo, sMensaje ) ) then
             CanMove := Error( sMensaje, self.Programa );
     end;
{
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          if ZetaCommonTools.StrLleno( Programa.Text ) and not FileExists( Programa.Text ) then
          begin
               ZetaDialogo.zError( Caption, 'Programa ' + Programa.Text + ' No Existe', 0 );
               ActiveControl := Programa;
               CanMove := False;
          end
          else
              if ZetaCommonTools.StrVacio( Archivo.Text ) and not ProcesarTarjeta.Checked then
              begin
                   ZetaDialogo.zError( Caption, 'No Existen Par�metros para Ejecutar Poll de Relojes', 0 );
                   ActiveControl := Archivo;
                   CanMove := False;
              end
              else
                  if not EjecutaPrograma then
                  begin
                       ActiveControl := Programa;
                       CanMove := False;
                  end;
     end;
}
end;

procedure TWizAsistPollRelojes_DevEx.ProgramaSeekClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          Filter := 'Batch ( *.bat )|*.bat|Ejecutables ( *.exe )|*.exe|Todos ( *.* )|*.*';
          FilterIndex := 1;
          Title := 'Seleccione El Programa Deseado';
     end;
     Programa.Text := ZetaClientTools.AbreDialogo( OpenDialog, Programa.Text, 'BAT' );
{
     with OpenDialog do
     begin
          InitialDir := ExtractFilePath( Programa.Text );
          FileName := Programa.Text;
          DefaultExt := 'BAT';
          Filter := 'Batch ( *.bat )|*.bat|Ejecutables ( *.exe )|*.exe|Todos ( *.* )|*.*';
          FilterIndex := 1;
          Title := 'Seleccione El Programa Deseado';
          if Execute then
             Programa.Text := FileName;
     end;
}
end;

procedure TWizAsistPollRelojes_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          Filter := 'Archivos DAT ( *.dat )|*.dat|Archivos TXT ( *.txt )|*.txt|Todos ( *.* )|*.*';
          FilterIndex := 1;
          Title := 'Seleccione El Archivo Deseado';
     end;
     Archivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, Archivo.Text, 'DAT' );
{
     with OpenDialog do
     begin
          InitialDir := ExtractFilePath( Archivo.Text );
          FileName := Archivo.Text;
          DefaultExt := 'DAT';
          Filter := 'Archivos DAT ( *.dat )|*.dat|Archivos TXT ( *.txt )|*.txt|Todos ( *.* )|*.*';
          FilterIndex := 1;
          Title := 'Seleccione El Archivo Deseado';
          if Execute then
             Archivo.Text := FileName;
     end;
}
end;

{
function TWizAsistPollRelojes.EjecutaPrograma: Boolean;
var
   sPrograma: String[ 79 ];
   oCursor: TCursor;
begin
     sPrograma := Programa.Text;
     if ZetaCommonTools.StrLleno( sPrograma ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             case ZetaMessages.WinExecAndWait32( sPrograma, Comandos.Text, SW_SHOWNORMAL ) of
                -1: ZetaDialogo.ZError( Caption, 'Error al Ejecutar el Programa: ' + CR_LF + sPrograma , 0 );
                 1: ZetaDialogo.ZError( Caption, 'El Programa: ' + CR_LF + sPrograma + CR_LF + ' No Fu� Encontrado', 0 );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
          if FileExists( Archivo.Text) then
             Result := True
          else
          begin
               ZetaDialogo.zError( Caption, 'Archivo ' + Archivo.Text + ' No Fu� Creado', 0 );
               Result := False;
          end;
     end
     else
         Result := True;
end;
}
procedure TWizAsistPollRelojes_DevEx.FormShow(Sender: TObject);
begin
  inherited;
Advertencia.Caption := 'Si eligi� la opci�n Procesar Tarjetas, se registrar�n'
                         + ' las checadas en la tarjeta diaria de cada empleado,'
                         +   ' borr�ndolas de la tabla POLL.';
end;


procedure TWizAsistPollRelojes_DevEx.WizardAfterMove(Sender: TObject);
begin
  inherited;
   if Wizard.EsPaginaActual( parametros ) then
  ParametrosControl := Programa;
end;

end.



