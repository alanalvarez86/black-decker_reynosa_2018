unit FWizAgregarBaseDatosEmpleados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseWizard, Mask, ZetaFecha, StdCtrls, ComCtrls, Buttons,
  ZetaWizard, ExtCtrls, ZetaKeyLookup, 
  FWizEmpValidarDC4, ZetaDialogo, ZetaEdit, FileCtrl, ZetaNumero,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, ZetaKeyCombo, cxTrackBar, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters;
  
type
  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
    end;

  TWizAgregarBaseDatosEmpleados = class(TBaseWizard)
    rbBDNueva: TRadioButton;
    rbBDExistente: TRadioButton;
    cbBasesDatos: TComboBox;
    lbNombreBD: TLabel;
    txtBaseDatos: TEdit;
    lblSeleccionarBD: TLabel;
    EstructuraEmpleados: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    edDescripcion: TZetaEdit;
    DefinirEstructura1: TTabSheet;
    memDefinirEstructura: TMemo;
    rbValoresDefault: TRadioButton;
    rbOtraBaseDatos: TRadioButton;
    OpenDialog: TOpenDialog;
    edCodigo: TZetaEdit;
    lblProceso: TLabel;
    cbBasesDatosBase: TZetaKeyCombo;
    memoParametros: TMemo;
    memoEstEmpleados: TMemo;
    NuevaBD1: TTabSheet;
    NuevaBD2: TTabSheet;
    tbEmpleados: TcxTrackBar;
    tbDocumentos: TcxTrackBar;
    lblCantidadEmpleados: TLabel;
    lblDocumentos: TLabel;
    memoEmpleadosDatos: TMemo;
    lblUbicacion: TLabel;
    lblTamanoBDOrigen: TLabel;
    lblArchivoLog: TLabel;
    txtUbicacion: TZetaEdit;
    GBUsuariosMSSQL: TGroupBox;
    lblUsuarioMSSQL: TLabel;
    lblUsuario: TLabel;
    lblClave: TLabel;
    rbSameUser: TRadioButton;
    rbOtroUser: TRadioButton;
    txtUsuario: TEdit;
    txtClave: TMaskEdit;
    txtTamanyo: TZetaNumero;
    txtLog: TZetaNumero;
    lblMB1: TLabel;
    lblMB2: TLabel;
    lblCantEmpSeleccion: TLabel;
    lblDocSeleccion: TLabel;
    chbEspeciales: TCheckBox;
    lblNotaEmpleados: TLabel;
    lblNotaDocumentos: TLabel;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rbBDExistenteClick(Sender: TObject);
    procedure rbBDNuevaClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure rbOtraBaseDatosClick(Sender: TObject);
    procedure rbValoresDefaultClick(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure tbEmpleadosPropertiesChange(Sender: TObject);
    procedure tbDocumentosPropertiesChange(Sender: TObject);
    procedure rbSameUserClick(Sender: TObject);
    procedure rbOtroUserClick(Sender: TObject);
    procedure edCodigoExit(Sender: TObject);
  private
    { Private declarations }    
    SC : TSQLConnection;
    function GetConnStr: widestring;
    property ConnStr : widestring read GetConnStr; 
    function ExisteBD: boolean; 
  public
    { Public declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;  
    function Warning(const sError: String; oControl: TWinControl): Boolean;  
    function ProbarConexion(out sMensaje: WideString): Boolean;
    procedure CalcularTamanyos;
  end;

var
  WizAgregarBaseDatosEmpleados: TWizAgregarBaseDatosEmpleados;

implementation

uses
    DProcesos,
    DSistema,
    DBaseSistema,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaClientTools,
    ZetaServerTools;

{$R *.dfm}

procedure TWizAgregarBaseDatosEmpleados.CargaParametros;  
Const K_2_MB = 2048;
      K_1024 = 1024;
      K_KB_DOC = 100;
begin
     inherited;
     with ParameterList do
     begin                        
          AddInteger('Tipo', Ord (tc3Datos));
          AddString( 'Codigo', edCodigo.Text );
          AddString( 'Descripcion', edDescripcion.Text );
          AddBoolean('DefinirEstructura', DefinirEstructura1.Enabled);
          AddBoolean('ValoresDefault', rbValoresDefault.Checked);
          AddBoolean('NuevaBD', rbBDNueva.Checked);
          AddBoolean('Especiales', chbEspeciales.Checked);

          if rbOtraBaseDatos.Checked then
          begin
               AddString( 'BaseDatosBase', cbBasesDatosBase.Llaves[cbBasesDatosBase.ItemIndex]);
          end; 
          if rbBDNueva.Checked then
          begin
               AddString( 'NombreBDSQL', Trim(txtBaseDatos.Text));
               AddString( 'BaseDatos', Trim(txtBaseDatos.Text) );
               AddString( 'Usuario', Trim(txtUsuario.Text) );
               AddString( 'Clave', ZetaServerTools.Encrypt(txtClave.Text));
               AddInteger ('MaxSize', Round (((tbEmpleados.Position*K_2_MB)/K_1024) + ((tbEmpleados.Position*tbDocumentos.Position*K_KB_DOC)/K_1024)));
          end  
          else
          begin 
               AddString( 'Usuario', VACIO );
               AddString( 'Clave', VACIO);
               AddString( 'BaseDatos', cbBasesDatos.Text );
          end;
     end;
end;

function TWizAgregarBaseDatosEmpleados.EjecutarWizard: Boolean;
var resultado: Boolean;
    sAdvertenciaValidarBDEmpleados: WideString;
    sBaseDatos: String;
begin
     // Si se definir� estructura de Base de Datos de Empleados,
     // ejecutar directamente sobre el dll a trav�s de DSistema.

     if NuevaBD1.Enabled then
        sBaseDatos := txtBaseDatos.Text
     else 
        sBaseDatos := cbBasesDatos.Text;

     if (DefinirEstructura1.Enabled) or (NuevaBD1.Enabled = TRUE) then
     begin
         try
              Salir.Enabled := FALSE;
              ProgressPanel.Visible := TRUE;
              ProgressBar.Enabled := TRUE;
              ProgressBar.Visible := TRUE;

              lblProceso.Caption := 'Ejecutando...';
              Advertencia.Lines.Delete(Advertencia.Lines.Count-1);
              
              //
              // Validaci�n de base de datos de empleados.
              sAdvertenciaValidarBDEmpleados := dmSistema.ValidarBDEmpleados(edCodigo.Text, dmSistema.GetServer + '.' + sBaseDatos, TRUE);
              if sAdvertenciaValidarBDEmpleados <> VACIO then
                   ZWarning(Caption, sAdvertenciaValidarBDEmpleados, 0, mbOK);
              // ----- -----

              if  NuevaBD1.Enabled = TRUE then
              begin
                   Advertencia.Lines.Append('Creando base de datos... ');
                   ProgressBar.Position := 15;
                   resultado := dmSistema.CrearBDEmpleados(ParameterList);
              end
              else
                  resultado := TRUE;

              if resultado then
              begin
                   if  NuevaBD1.Enabled = TRUE then
                       Advertencia.Lines[Advertencia.Lines.Count-1] := Advertencia.Lines [Advertencia.Lines.Count-1] + 'Terminado';
                   Advertencia.Lines.Append('Definiendo estructura... ');
                   ProgressBar.Position := 25;
                   resultado := dmSistema.CrearEstructuraBD (ParameterList);
              end;
              if (resultado) and (chbEspeciales.Checked) then
              begin           
                   Advertencia.Lines[Advertencia.Lines.Count-1] := Advertencia.Lines [Advertencia.Lines.Count-1] + 'Terminado';
                   Advertencia.Lines.Append('Scripts especiales... ');
                   ProgressBar.Position := 40;
                   resultado := dmSistema.CreaEspeciales(ParameterList);
              end;
              if resultado then
              begin
                  Advertencia.Lines[Advertencia.Lines.Count-1] := Advertencia.Lines [Advertencia.Lines.Count-1] + 'Terminado';
                  Advertencia.Lines.Append('Valores iniciales... ');
                  ProgressBar.Position := 50;
                  resultado := dmSistema.CreaInicialesTabla(ParameterList);
              end;
              if resultado then
              begin
                   Advertencia.Lines[Advertencia.Lines.Count-1] := Advertencia.Lines [Advertencia.Lines.Count-1] + 'Terminado';
                   Advertencia.Lines.Append('Valores sugeridos... ');
                   ProgressBar.Position := 70;
                   resultado := dmSistema.CreaSugeridosTabla(ParameterList);
              end;
              
              if resultado then
              begin        
                   Advertencia.Lines[Advertencia.Lines.Count-1] := Advertencia.Lines [Advertencia.Lines.Count-1] + 'Terminado';
                   Advertencia.Lines.Append('Diccionario de datos... ');
                   ProgressBar.Position := 85;
                   resultado := dmSistema.CreaDiccionarioTabla(ParameterList)
              end;

              if resultado then
              begin                              
                   Advertencia.Lines[Advertencia.Lines.Count-1] := Advertencia.Lines [Advertencia.Lines.Count-1] + 'Terminado';
                   Advertencia.Lines.Append('Registrando base de datos en sistema... ');
                   ProgressBar.Position := 100;
                   resultado := dmSistema.EmpGrabarDBSistema(ParameterList);
              end;


              if resultado then
              begin                                             
                   Advertencia.Lines[Advertencia.Lines.Count-1] := Advertencia.Lines [Advertencia.Lines.Count-1] + 'Terminado';
                   lblProceso.Caption := 'Proceso terminado.';
                   ProgressBar.Position := 100;
                   ZInformation(Caption, 'La base de datos fue agregada correctamente.', 0);
                   Result := TRUE;
              end
              else
              begin                                            
                   Advertencia.Lines[Advertencia.Lines.Count-1] := Advertencia.Lines [Advertencia.Lines.Count-1] + 'Error';
                   lblProceso.Caption := 'Proceso con errores.';
                   ProgressBar.Enabled := FALSE;

                   // Wizard.Enabled := FALSE;
                   Anterior.Enabled := FALSE;
                   Ejecutar.Enabled := FALSE;
                   Salir.Enabled := TRUE;
                   Siguiente.Enabled := FALSE;
                   ZError(Caption, 'Error al agregar base de datos.', 0);
                   Result := TRUE;
              end;

         except
               on Error: Exception do
               begin
                   Advertencia.Lines[Advertencia.Lines.Count-1] := Advertencia.Lines [Advertencia.Lines.Count-1] + ' Error';
                   lblProceso.Caption := 'Proceso con errores.';
                   // ProgressBar.Enabled := FALSE;
                   
                   if (  Pos( 'PRIMARY KEY', UpperCase( Error.Message )) > 0) then
                   begin
                      ZError (Caption, Format('Error al agregar base de datos:' + CR_LF + 'El c�digo de base de datos: %s ya se encuentra dado de alta',[ edCodigo.Text]), 0);
                   end                            
                   else if (  Pos( 'ALREADY EXISTS', UpperCase( Error.Message )) > 0) then
                   begin
                      ZError (Caption, Format('Error al agregar base de datos:' + CR_LF + 'Base de datos: %s ya existe. Proporcione otro nombre.',[ Trim (txtBaseDatos.Text)]), 0);
                   end
                   else
                   begin
                       ZError(Caption, 'Error al agregar base de datos:' + CR_LF + Error.Message, 0);
                   end;
                   Result := TRUE;
               end;

         end;
     end
     else
     begin
         try
             ProgressBar.Visible := FALSE;
             // Validaci�n de base de datos de empleados.
             sAdvertenciaValidarBDEmpleados := dmSistema.ValidarBDEmpleados(edCodigo.Text, dmSistema.GetServer + '.' + sBaseDatos, TRUE);
             if sAdvertenciaValidarBDEmpleados <> VACIO then
                ZWarning(Caption, sAdvertenciaValidarBDEmpleados, 0, mbOK);

             Result := dmSistema.EmpGrabarDBSistema(ParameterList);
             if Result then
                ZInformation(Caption, 'La base de datos fue agregada correctamente.', 0)
             else
             begin
                 lblProceso.Caption := 'Proceso con errores.';                                                  
                 Advertencia.Lines[Advertencia.Lines.Count-1] := Advertencia.Lines [Advertencia.Lines.Count-1] + ' Error';
                 ZError(Caption, 'Error al agregar base de datos.', 0);
                 Result := TRUE;
             end;
         except
               on Error: Exception do
               begin   
                   lblProceso.Caption := 'Proceso con errores.';                                            
                   Advertencia.Lines[Advertencia.Lines.Count-1] := Advertencia.Lines [Advertencia.Lines.Count-1] + ' Error';                                 
                   if (  Pos( 'PRIMARY KEY', UpperCase( Error.Message )) > 0) then
                   begin
                      ZError (Caption, Format('Error al agregar base de datos:' + CR_LF + 'El c�digo de base de datos: %s ya se encuentra dado de alta',[ edCodigo.Text]), 0);
                     { Wizard.Anterior;
                      Result := FALSE;  } {DevEx(by am): Se removio para que quedara igual que en la Nueva Imagen***}
                   end
                   else
                   begin
                       ZError(Caption, 'Error al agregar base de datos:' + CR_LF + Error.Message, 0);
                       //Result := TRUE;
                   end;
                   Result := TRUE;
               end;
         end
     end;
end;

procedure TWizAgregarBaseDatosEmpleados.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_SIST_PROC_AGREGAR_BASE_DATOS;
     with dmSistema do
     begin
          cdsEmpresasLookUp.Conectar;
     end;
     PageControl.ActivePage := Parametros;
end;

procedure TWizAgregarBaseDatosEmpleados.FormShow(Sender: TObject);
begin
     inherited;
     dmSistema.GetBasesDatos(dmSistema.GetServer, VACIO, VACIO);
     txtBaseDatos.SetFocus;

     if (cbBasesDatos.Items.Count = 0) then
     begin
         while not dmSistema.cdsBasesDatos.Eof do
         begin
              cbBasesDatos.AddItem(dmSistema.cdsBasesDatos.FieldByName ('NAME').AsString, nil);
              dmSistema.cdsBasesDatos.Next;
         end;
     end;

     chbEspeciales.Enabled := dmSistema.ExistenEspeciales;
     
end;

procedure TWizAgregarBaseDatosEmpleados.rbBDExistenteClick(
  Sender: TObject);
begin
     txtBaseDatos.Enabled := false;
     cbBasesDatos.Enabled := true;
     cbBasesDatos.SetFocus;
end;

procedure TWizAgregarBaseDatosEmpleados.rbBDNuevaClick(Sender: TObject);
begin
     txtBaseDatos.Enabled := true;
     txtBaseDatos.SetFocus;
     cbBasesDatos.Enabled := false;
end;

procedure TWizAgregarBaseDatosEmpleados.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const K_ESPACIO = ' ';  
var mensaje : WideString;
begin
     if Wizard.Adelante then
     begin
          if PageControl.ActivePage = Parametros then
          begin
              if rbBDNueva.Checked then
              begin
                   if Trim (txtBaseDatos.Text) = VACIO then
                      CanMove := Warning ('El campo nombre de base de datos no puede quedar vac�o.', txtBaseDatos)
                   else if ExisteBD then
                      CanMove := Warning (Format('La base de datos: %s ya existe. Proporcione otro nombre.',[ Trim (txtBaseDatos.Text)]), txtBaseDatos)
                   else if Pos (K_ESPACIO, txtBaseDatos.Text) > 0 then
                        CanMove := Warning ('El nombre de la base de datos no puede contener espacios', txtBaseDatos)
                   else
                   begin
                        NuevaBD1.Enabled := TRUE;
                        NuevaBD2.Enabled := TRUE;
                        CanMove := TRUE;
                   end;
              end
              else if rbBDExistente.Checked then
              begin           
                   NuevaBD1.Enabled := FALSE;
                   NuevaBD2.Enabled := FALSE;
                   if (cbBasesDatos.ItemIndex >= 0) then
                   begin
                        // Es Base deDatos ya agregada a COMPANY?
                        if (dmSistema.EsBaseDatosAgregada(dmSistema.GetServer + '.' + cbBasesDatos.Text)) then
                        begin
                             CanMove := Warning (Format ('La base de datos ''%s'' ya se encuentra agregada al sistema.', [cbBasesDatos.Text]), cbBasesDatos)
                        end
                        else
                        begin
                             // Averiguar si la Base de Datos seleccionada no es de Selecci�n o Visitantes
                             if (dmSistema.NoEsBDSeleccionVisitantes(cbBasesDatos.Text)) then
                             begin
                                 // Averiguar si la Base de Datos seleccionada tiene la estructura de Empleados (Tabla COLABORA)
                                 if (dmSistema.EsBaseDatosEmpleados(cbBasesDatos.Text)) then
                                 begin
                                    DefinirEstructura1.Enabled := FALSE;
                                    CanMove := TRUE;
                                 end
                                 else
                                 begin
                                    DefinirEstructura1.Enabled := TRUE;
                                    CanMove := TRUE;
                                 end;
                             end
                             else 
                                  CanMove := Warning ('La base de datos seleccionada no puede configurarse.'
                                  + CR_LF + 'Tiene definida una estructura diferente a Tress.', cbBasesDatos);
                        end;
                   end
                   else
                       CanMove := Warning ('Seleccionar una base de datos para continuar.', cbBasesDatos);
              end
              else
                  inherited;
          end

          else if (PageControl.ActivePage = EstructuraEmpleados) then
          begin
               if (trim (edCodigo.Text) = VACIO) then
               begin
                    CanMove := Warning ('El campo C�digo no puede quedar vac�o.', edCodigo);
               end         
               else if Pos (K_ESPACIO, edCodigo.Text) > 0 then
               begin
                    CanMove := Warning ('El campo c�digo no puede contener espacios', edCodigo);
               end 
               else if (dmSistema.ExisteBD_DBINFO(trim(edCodigo.Text))) then
               begin
                    CanMove := Warning (Format ('El c�digo  ''%s'' ya se encuentra agregado al sistema.', [edCodigo.Text]), edCodigo)
               end
               else if (trim (edDescripcion.Text) = VACIO) then 
               begin
                    CanMove := Warning ('El campo Descripci�n no puede quedar vac�o.', edDescripcion);
               end
               else
                   CanMove := TRUE;
          end

          else if (PageControl.ActivePage = DefinirEstructura1) then
          begin
               if (rbOtraBaseDatos.Checked) and (cbBasesDatosBase.ItemIndex < 0) then
               begin
                    CanMove := Warning ('Seleccionar una base de datos para continuar.', cbBasesDatosBase);
               end
               else
               begin
                   CanMove := TRUE;
               end;
          end

          
          else if (PageControl.ActivePage = NuevaBD2) then
          begin
               // Estructura.Enabled := FALSE;
               if rbOtroUser.Checked then
               begin
                    if not ProbarConexion(mensaje) then
                    begin
                         CanMove := Warning (mensaje, txtUsuario);
                    end
                end;
           end
     end;
end;

procedure TWizAgregarBaseDatosEmpleados.WizardAfterMove(Sender: TObject);
begin
     inherited;

     if PageControl.ActivePage = DefinirEstructura1 then
     begin
         if (cbBasesDatosBase.Items.Count = 0) then
         begin
             try
                 cbBasesDatosBase.Lista.BeginUpdate;
                 // Obtener Bases de Datos de Tipo Tress.
                 with dmSistema.cdsSistBaseDatos do
                 begin
                     Conectar;
                     First;
                     while not Eof do
                     begin
                          if (FieldByName ('DB_TIPO').AsInteger = Ord( tc3Datos )) or (FieldByName ('DB_TIPO').AsInteger = Ord( tc3Prueba )) then
                             cbBasesDatosBase.Lista.Add(FieldByName ('DB_CODIGO').AsString + '=' + FieldByName ('DB_DESCRIP').AsString);
                          Next;
                     end;
                 end;
             finally
                    cbBasesDatosBase.Lista.EndUpdate;
             end;
         end;
     end

     else if PageControl.ActivePage = Ejecucion then
     begin
          Advertencia.Lines.Clear;
          Advertencia.Alignment := taLeftJustify;

          if (DefinirEstructura1.Enabled) then
          begin
              Advertencia.Lines.Add ('Se proceder� a preparar la base de datos de tipo Tress: ');
              if (rbBDNueva.Checked) then
                 Advertencia.Lines.Add ('Ubicaci�n: ' + dmSistema.GetServer + '.' + txtBaseDatos.Text)
              else
                  Advertencia.Lines.Add ('Ubicaci�n: ' + dmSistema.GetServer + '.' + cbBasesDatos.Text);
              if rbValoresDefault.Checked then
                 Advertencia.Lines.Add('Datos iniciales: Valores default.')
              else
                 Advertencia.Lines.Add('Datos iniciales: ' + cbBasesDatosBase.Text );
          end
          else
          begin 
             Advertencia.Lines.Add(format('Se agregar� la base de datos ''%s'' al sistema.', [cbBasesDatos.Text]));
             Advertencia.Lines.Add ('La base de datos ya cuenta con la estructura de tipo Tress.');
          end;

          Advertencia.Lines.Add(' ');
          Advertencia.Lines.Add('Presione el bot�n Ejecutar para iniciar el proceso.');
          lblProceso.Caption := '';
     end

     else if PageControl.ActivePage = EstructuraEmpleados then
     begin
          edCodigo.SetFocus;
     end

     else if PageControl.ActivePage = NuevaBD1 then
     begin
          CalcularTamanyos;
          lblCantEmpSeleccion.Caption := IntToStr(tbEmpleados.Position);
          lblDocSeleccion.Caption := IntToStr(tbDocumentos.Position);
     end

     else if PageControl.ActivePage = NuevaBD2 then
     begin
          txtUbicacion.Text := dmSistema.GetServer + '.' + Trim (txtBaseDatos.Text);
     end;
end;

procedure TWizAgregarBaseDatosEmpleados.rbOtraBaseDatosClick(
  Sender: TObject);
begin
     cbBasesDatosBase.Enabled := TRUE;
     cbBasesDatosBase.SetFocus;
end;

procedure TWizAgregarBaseDatosEmpleados.rbValoresDefaultClick(
  Sender: TObject);
begin
     cbBasesDatosBase.Enabled := FALSE;
end;
                                                             
function TWizAgregarBaseDatosEmpleados.Warning( const sError: String; oControl: TWinControl ): Boolean;
begin
     ZetaDialogo.ZWarning( Caption, sError, 0, mbOK );
     oControl.SetFocus;
     Result := False;
end;

procedure TWizAgregarBaseDatosEmpleados.WizardAlEjecutar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;

     if lOk then
         dmSistema.cdsSistBaseDatos.Refrescar;
end;

procedure TWizAgregarBaseDatosEmpleados.tbEmpleadosPropertiesChange(
  Sender: TObject);
begin
     inherited;
     lblCantEmpSeleccion.Caption := IntToStr(tbEmpleados.Position);
     CalcularTamanyos;
end;

procedure TWizAgregarBaseDatosEmpleados.tbDocumentosPropertiesChange(
  Sender: TObject);
begin
     inherited;
     lblDocSeleccion.Caption := IntToStr(tbDocumentos.Position);
     CalcularTamanyos;
end;

procedure TWizAgregarBaseDatosEmpleados.CalcularTamanyos;
Const K_2_MB = 2048;
      K_1024 = 1024;
      K_KB_DOC = 100;
begin
      lblMB1.Caption := 'MB';
      lblMB2.Caption := 'MB';
      // Calcular tama�o de la base de datos
      // considerando la cantidad de empleados y la cantidad de documentos.
      // txtTamanyo.Valor := (((tbEmpleados.Position*K_2_MB)/K_1024) + ((tbDocumentos.Position*K_KB_DOC)/K_1024));
      txtTamanyo.Valor := (((tbEmpleados.Position*K_2_MB)/K_1024) + ((tbEmpleados.Position*tbDocumentos.Position*K_KB_DOC)/K_1024));
      txtLog.Valor := txtTamanyo.Valor * 0.25;

      if txtTamanyo.Valor > K_1024 then
      begin
           txtTamanyo.Valor := txtTamanyo.Valor/K_1024;
           lblMB1.Caption := 'GB';
      end;

      if txtLog.Valor > K_1024 then
      begin
           txtLog.Valor := txtLog.Valor/K_1024;
           lblMB2.Caption := 'GB';
      end;

      memoEmpleadosDatos.Lines.Clear;
      memoEmpleadosDatos.Lines.Append('Base de datos: ' + Trim (txtBaseDatos.Text));
      // memoEmpleadosDatos.Lines.Append(Format ('Tama�o sugerido (%s): %s', [lblMB1.Caption, FloatToStr(Round (txtTamanyo.Valor))]));
      memoEmpleadosDatos.Lines.Append(Format ('Tama�o sugerido (%s): %s', [lblMB1.Caption, FormatFloat ('0.00', txtTamanyo.Valor)]));
      memoEmpleadosDatos.Lines.Append(Format ('Log de transacciones (%S): %s', [lblMB2.Caption, FormatFloat('0.00', txtLog.Valor)]));
      lblCantEmpSeleccion.Caption := IntToStr(tbEmpleados.Position);
      lblDocSeleccion.Caption := IntToStr(tbDocumentos.Position);
end;

function TWizAgregarBaseDatosEmpleados.ProbarConexion(out sMensaje: WideString): Boolean;
var
   lOk : Boolean;
begin
     lOk:=FALSE;
     if rbOtroUser.Checked then
     begin
          if not( txtUsuario.Text = VACIO) then
          begin
               if not ( txtClave.Text = VACIO ) then
               begin
                    lOk := dmSistema.ConectarseServer( ConnStr, sMensaje)
               end
               else
               begin
                    lOk:=FALSE;
                    sMensaje := 'El campo Clave no puede quedar vac�o.';
               end;
          end
          else
          begin
               lOk:=FALSE;
               sMensaje := 'El campo Usuario no puede quedar vac�o.';
          end;
     end;

  Result := lOk;
end;

function TWizAgregarBaseDatosEmpleados.GetConnStr: widestring;
begin
  SC.ServerName := dmSistema.GetServer;
  SC.UserName := txtUsuario.Text;
  SC.Password := txtClave.Text;

  Result := 'Provider=SQLOLEDB.1;';
  Result := Result + 'Data Source=' + SC.ServerName + ';';
  if SC.DatabaseName <> '' then
    Result := Result + 'Initial Catalog=' + SC.DatabaseName + ';';

    Result := Result + 'uid=' + SC.UserName + ';';
    Result := Result + 'pwd=' + SC.Password + ';';

end;

procedure TWizAgregarBaseDatosEmpleados.rbSameUserClick(Sender: TObject);
begin
     txtUsuario.Enabled := FALSE;
     txtClave.Enabled := FALSE;
end;

procedure TWizAgregarBaseDatosEmpleados.rbOtroUserClick(Sender: TObject);
begin
     txtUsuario.Enabled := TRUE;
     txtClave.Enabled := TRUE;
end;

function TWizAgregarBaseDatosEmpleados.ExisteBD: boolean;
var i: integer;
begin
     Result := FALSE;

     for i:=0 to cbBasesDatos.Items.Count do
     begin
          if UpperCase (txtBaseDatos.Text) = UpperCase (cbBasesDatos.Items.Strings[i]) then
             Result := TRUE;
     end
end;

procedure TWizAgregarBaseDatosEmpleados.edCodigoExit(Sender: TObject);
begin
      edCodigo.Text :=  Copy(edCodigo.Text, 0, 10);
end;

end.
