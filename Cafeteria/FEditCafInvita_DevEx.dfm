inherited EditCafInvita_DevEx: TEditCafInvita_DevEx
  Left = 166
  Top = 254
  Caption = 'Invitaciones'
  ClientHeight = 238
  ClientWidth = 394
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 202
    Width = 394
    TabOrder = 2
    inherited OK_DevEx: TcxButton
      Left = 228
      Top = 4
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 307
      Top = 4
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 394
    TabOrder = 5
    inherited ValorActivo2: TPanel
      Width = 68
      inherited textoValorActivo2: TLabel
        Width = 62
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 394
    Height = 38
    Align = alTop
    TabOrder = 0
    object InvitaLbl: TLabel
      Left = 26
      Top = 12
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = 'Invitador:'
    end
    object ZInvitador: TZetaTextBox
      Left = 83
      Top = 10
      Width = 297
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  object GroupBox2: TGroupBox [4]
    Left = 0
    Top = 88
    Width = 190
    Height = 114
    Align = alLeft
    TabOrder = 1
    object HoraLbl: TLabel
      Left = 44
      Top = 21
      Width = 26
      Height = 13
      Alignment = taRightJustify
      Caption = 'Hora:'
    end
    object TipoLbl: TLabel
      Left = 46
      Top = 45
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object ComidaLbl: TLabel
      Left = 27
      Top = 69
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Comidas:'
    end
    object Extraslbl: TLabel
      Left = 38
      Top = 93
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = 'Extras:'
    end
    object CF_TIPO: TDBComboBox
      Left = 83
      Top = 41
      Width = 80
      Height = 21
      DataField = 'CF_TIPO'
      DataSource = DataSource
      ItemHeight = 13
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9')
      TabOrder = 1
    end
    object CF_COMIDAS: TZetaDBNumero
      Left = 83
      Top = 65
      Width = 80
      Height = 21
      Mascara = mnDias
      TabOrder = 2
      Text = '0'
      DataField = 'CF_COMIDAS'
      DataSource = DataSource
    end
    object CF_EXTRAS: TZetaDBNumero
      Left = 83
      Top = 89
      Width = 80
      Height = 21
      Mascara = mnDias
      TabOrder = 3
      Text = '0'
      DataField = 'CF_EXTRAS'
      DataSource = DataSource
    end
    object CF_HORA: TZetaDBHora
      Left = 83
      Top = 16
      Width = 40
      Height = 21
      EditMask = '99:99;0'
      TabOrder = 0
      Text = '    '
      Tope = 24
      Valor = '    '
      DataField = 'CF_HORA'
      DataSource = DataSource
    end
  end
  object GroupBox1: TGroupBox [5]
    Left = 190
    Top = 88
    Width = 204
    Height = 114
    Align = alClient
    TabOrder = 3
    object RelojLbl: TLabel
      Left = 51
      Top = 18
      Width = 27
      Height = 13
      Alignment = taRightJustify
      Caption = 'Reloj:'
    end
    object ModificoLbl: TLabel
      Left = 35
      Top = 42
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Modific'#243':'
    end
    object ReglaLbl: TLabel
      Left = 47
      Top = 66
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'Regla:'
    end
    object CF_RELOJ: TZetaDBTextBox
      Left = 92
      Top = 17
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'CF_RELOJ'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CF_RELOJ'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object US_CODIGO: TZetaDBTextBox
      Left = 92
      Top = 41
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'US_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CL_CODIGO: TZetaDBTextBox
      Left = 92
      Top = 65
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'CL_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CL_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ExtraLbl: TLabel
      Left = 51
      Top = 90
      Width = 27
      Height = 13
      Caption = 'Extra:'
    end
    object CF_REG_EXT: TDBCheckBox
      Left = 92
      Top = 88
      Width = 17
      Height = 17
      DataField = 'CF_REG_EXT'
      DataSource = DataSource
      Enabled = False
      TabOrder = 0
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 209
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
