unit FCafInvita_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, StdCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid,
     ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013,
   dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, ZetaKeyLookup_DevEx, Menus,
  ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid, Mask, ZetaFecha;

type
  TCafInvita_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    LookUpInvita: TZetaKeyLookup_DevEx;
    Panel2: TPanel;
    AsistenciaFechaZF: TZetaFecha;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure LookUpInvitaValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AsistenciaFechaZFValidDate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Connect; override;
    procedure Disconnect;override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  CafInvita_DevEx: TCafInvita_DevEx;

implementation

uses DCafeteria,
     DCatalogos,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools, FTressShell, DCliente;

{$R *.DFM}

{ TCafInvita }

procedure TCafInvita_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := ZetaCommonClasses.H10173_Invitaciones;
end;

procedure TCafInvita_DevEx.FormShow(Sender: TObject);
begin
   CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
     inherited;
     AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
        ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  //Esconde la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
  //Para que nunca muestre el filterbox inferior
end;

procedure TCafInvita_DevEx.Connect;
begin
     LookUpInvita.LookupDataSet := dmCatalogos.cdsInvitadores;
     with dmCatalogos.cdsInvitadores do
     begin
          Conectar;
          with Self.LookUpInvita do
          begin
               First;
               Llave := FieldByName( 'IV_CODIGO' ).AsString;
          end;
     end;
     with dmCafeteria do
     begin
          Invitador := LookupInvita.Valor;
          cdsCafInvita.Conectar;
          DataSource.DataSet:= cdsCafInvita;
     end;
     Refresh;
     //CreaColumaSumatoria('CF_EXTRAS',skCount);
     //CreaColumaSumatoria('CF_COMIDAS',skSum);

end;

procedure TCafInvita_DevEx.Disconnect;
begin
     inherited;
     LookUpInvita.LookupDataSet := nil;
end;

procedure TCafInvita_DevEx.Refresh;
begin

     AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
     with dmCafeteria do
     begin
          Invitador := LookupInvita.Valor;
          cdsCafInvita.Refrescar;
     end;
end;

procedure TCafInvita_DevEx.LookUpInvitaValidKey(Sender: TObject);
begin
     inherited;
     Refresh;
end;

procedure TCafInvita_DevEx.Agregar;
begin
     dmCafeteria.cdsCafInvita.Agregar;
end;

procedure TCafInvita_DevEx.Modificar;
begin
     dmCafeteria.cdsCafInvita.Modificar;
end;

procedure TCafInvita_DevEx.Borrar;
begin
     dmCafeteria.cdsCafInvita.Borrar;
end;

procedure TCafInvita_DevEx.AsistenciaFechaZFValidDate(Sender: TObject);
begin
  inherited;
   TressShell.AsistenciaFechavalid(AsistenciaFechaZF.Valor);
end;

procedure TCafInvita_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
 AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
end;

end.
