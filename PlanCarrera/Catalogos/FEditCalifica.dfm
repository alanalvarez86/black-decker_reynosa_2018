inherited EditCalifica: TEditCalifica
  Left = 349
  Top = 267
  Caption = 'Tabla de Calificaciones'
  ClientHeight = 235
  ClientWidth = 386
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 47
    Top = 41
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label2: TLabel [1]
    Left = 24
    Top = 63
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object Label3: TLabel [2]
    Left = 52
    Top = 85
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object Label5: TLabel [3]
    Left = 43
    Top = 107
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label6: TLabel [4]
    Left = 53
    Top = 129
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label7: TLabel [5]
    Left = 48
    Top = 151
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'Escala:'
  end
  object Label8: TLabel [6]
    Left = 51
    Top = 173
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Orden:'
  end
  inherited PanelBotones: TPanel
    Top = 199
    Width = 386
    inherited OK: TBitBtn
      Left = 218
    end
    inherited Cancelar: TBitBtn
      Left = 303
    end
  end
  inherited PanelSuperior: TPanel
    Width = 386
  end
  inherited PanelIdentifica: TPanel
    Width = 386
    inherited ValorActivo2: TPanel
      Width = 60
    end
  end
  object CA_CODIGO: TZetaDBEdit [10]
    Left = 88
    Top = 37
    Width = 90
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 3
    ConfirmEdit = True
    DataField = 'CA_CODIGO'
    DataSource = DataSource
  end
  object CA_DESCRIP: TDBEdit [11]
    Left = 88
    Top = 59
    Width = 230
    Height = 21
    DataField = 'CA_DESCRIP'
    DataSource = DataSource
    TabOrder = 4
  end
  object CA_INGLES: TDBEdit [12]
    Left = 88
    Top = 81
    Width = 230
    Height = 21
    DataField = 'CA_INGLES'
    DataSource = DataSource
    TabOrder = 5
  end
  object CA_NUMERO: TZetaDBNumero [13]
    Left = 88
    Top = 103
    Width = 110
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 6
    Text = '0.00'
    UseEnterKey = True
    DataField = 'CA_NUMERO'
    DataSource = DataSource
  end
  object CA_TEXTO: TDBEdit [14]
    Left = 88
    Top = 125
    Width = 230
    Height = 21
    DataField = 'CA_TEXTO'
    DataSource = DataSource
    TabOrder = 7
  end
  object CA_GRUPO: TDBEdit [15]
    Left = 88
    Top = 147
    Width = 75
    Height = 21
    DataField = 'CA_GRUPO'
    DataSource = DataSource
    MaxLength = 8
    TabOrder = 8
  end
  object CA_ORDEN: TZetaDBNumero [16]
    Left = 88
    Top = 169
    Width = 110
    Height = 21
    Mascara = mnEmpleado
    TabOrder = 9
    UseEnterKey = True
    DataField = 'CA_ORDEN'
    DataSource = DataSource
  end
end
