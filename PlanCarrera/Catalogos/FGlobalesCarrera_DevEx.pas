unit FGlobalesCarrera_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, ImgList, ComCtrls, ToolWin, ZetaDBTextBox,
  ZBaseGlobal_DevEx, StdCtrls, cxClasses, dxBar, cxGraphics;

type
  TGlobalesCarrera_DevEx = class(TBaseConsulta)
    PanelTitulos: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RazonLbl: TZetaDBTextBox;
    RfcLbl: TZetaDBTextBox;
    InfonavitLbl: TZetaDBTextBox;
    ImageList: TImageList;
    dxBarManager1: TdxBarManager;
    cxImageList1: TcxImageList;
    dxBarManager1Bar1: TdxBar;
    dxBarDockControl1: TdxBarDockControl;
    FechasEvaluacion_DevEx: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure EvaluacionBtnClick(Sender: TObject);
  private
    { Private declarations }
    function AbrirGlobal( GlobalClass: TBaseGlobalClass_DevEx ): Integer;
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  GlobalesCarrera_DevEx: TGlobalesCarrera_DevEx;

implementation

uses DGlobal,
     //DMedico,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonClasses,
     FGlobalEvaluacion,
     FGlobalEvaluacion_DevEx,
     //FAyudaContexto,
     ZGlobalTress;

{$R *.DFM}

procedure TGlobalesCarrera_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     EsConsulta := False;
     HelpContext := H_Globales_Carrera;
end;

procedure TGlobalesCarrera_DevEx.Connect;
begin
     with Global do
     begin
          RFCLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RFC_EMPRESA );
          InfonavitLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_INFONAVIT_EMPRESA );
          RazonLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RAZON_EMPRESA );
     end;
end;

procedure TGlobalesCarrera_DevEx.EvaluacionBtnClick(Sender: TObject);
begin
     inherited;
     if ZAccesosMgr.CheckDerecho(  D_CARRERA_GLOBALES, K_DERECHO_CAMBIO ) then
         AbrirGlobal( TGlobalEvaluacion_DevEx )
     else
         ZetaDialogo.ZInformation('Operaci�n No V�lida', 'No Se Tiene Permiso Para Modificar Globales', 0 );
end;

function TGlobalesCarrera_DevEx.AbrirGlobal( GlobalClass: TBaseGlobalClass_DevEx ): Integer;
var
   Forma: TBaseGlobal_DevEx;
begin
     Forma := GlobalClass.Create( Application );
     with Forma do
     begin
          try
             ShowModal;
             Result := LastAction;
          finally
                 Free;
          end;
     end;
end;

end.
