unit FCalifica_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, Vcl.StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCalifica_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGrid1: TZetaDBGrid;
    CA_CODIGO: TcxGridDBColumn;
    CA_DESCRIP: TcxGridDBColumn;
    CA_GRUPO: TcxGridDBColumn;
    CA_INGLES: TcxGridDBColumn;
    CA_NUMERO: TcxGridDBColumn;
    CA_TEXTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;       
  end;

var
  Califica_DevEx: TCalifica_DevEx;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador_DevEx,
    ZetaCommonClasses;

{ TCalifica }
procedure TCalifica_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_Tabla_Calificaciones;
end;

procedure TCalifica_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  ApplyMinWidth;
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCalifica_DevEx.Agregar;
begin
     dmPlanCarrera.cdsCalificaciones.Agregar;
end;

procedure TCalifica_DevEx.Borrar;
begin
     dmPlanCarrera.cdsCalificaciones.Borrar;
end;

procedure TCalifica_DevEx.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsCalificaciones.Conectar;
          DataSource.DataSet := cdsCalificaciones;
     end;
end;

procedure TCalifica_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Calificaciones', 'CALIFICA', 'CA_CODIGO', dmPlanCarrera.cdsCalificaciones );
end;

procedure TCalifica_DevEx.ImprimirForma;
begin
  inherited;

end;

procedure TCalifica_DevEx.Modificar;
begin
     dmPlanCarrera.cdsCalificaciones.Modificar;
end;

function TCalifica_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := true;
end;

function TCalifica_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := true;
end;

procedure TCalifica_DevEx.Refresh;
begin
     dmPlanCarrera.cdsCalificaciones.Refrescar;
end;

end.
