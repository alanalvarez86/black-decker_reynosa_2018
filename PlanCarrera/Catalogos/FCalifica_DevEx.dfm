inherited Califica_DevEx: TCalifica_DevEx
  Left = 233
  Top = 253
  Caption = 'Tabla de Calificaciones'
  ClientHeight = 197
  ClientWidth = 499
  ExplicitWidth = 499
  ExplicitHeight = 197
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 499
    ExplicitWidth = 499
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 240
      ExplicitWidth = 240
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 234
        ExplicitLeft = 3
      end
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 499
    Height = 178
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CA_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_GRUPO'
        Title.Caption = 'Escala'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_INGLES'
        Title.Caption = 'Ingl'#233's'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_NUMERO'
        Title.Caption = 'N'#250'mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_TEXTO'
        Title.Caption = 'Texto'
        Width = 200
        Visible = True
      end>
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 499
    Height = 178
    ExplicitWidth = 499
    ExplicitHeight = 178
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CA_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CA_CODIGO'
      end
      object CA_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'CA_DESCRIP'
      end
      object CA_GRUPO: TcxGridDBColumn
        Caption = 'Escala'
        DataBinding.FieldName = 'CA_GRUPO'
      end
      object CA_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'CA_INGLES'
      end
      object CA_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CA_NUMERO'
      end
      object CA_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'CA_TEXTO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
