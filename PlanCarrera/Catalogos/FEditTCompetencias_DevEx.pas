unit FEditTCompetencias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaKeyCombo,
  ZetaNumero, Mask, ZetaEdit, ZetaSmartLists, ZBaseEdicion, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditTCompetencias_DevEx = class(TBaseEdicion_DevEx)
    TC_CODIGO: TZetaDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    TC_DESCRIP: TDBEdit;
    TC_INGLES: TDBEdit;
    Label3: TLabel;
    Label5: TLabel;
    TC_NUMERO: TZetaDBNumero;
    TC_TEXTO: TDBEdit;
    Label6: TLabel;
    TC_TIPO: TZetaDBKeyCombo;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditTCompetencias_DevEx: TEditTCompetencias_DevEx;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses,
    ZAccesosTress;

{ TEditTCompetencias }

procedure TEditTCompetencias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_TCOMPETEN;
     FirstControl := TC_CODIGO;
     HelpContext:= H_Tipos_Competencia;
end;

procedure TEditTCompetencias_DevEx.Connect;
begin
     DataSource.DataSet := dmPlanCarrera.cdsTCompetencia;
end;

procedure TEditTCompetencias_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Competencia', 'TCOMPETE', 'TC_CODIGO', dmPlanCarrera.cdsTCompetencia );
end;

end.
