unit FSistEditUsuarios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditUsuarios, Db, ZetaDBTextBox, StdCtrls, ZetaKeyCombo,
  ZetaKeyLookup, DBCtrls, Mask, ZetaNumero, ComCtrls, ExtCtrls, Buttons;

type
  TSistEditUsuarios_DevEx = class(TSistBaseEditUsuarios)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditUsuarios_DevEx: TSistEditUsuarios_DevEx;

implementation
{$R *.DFM}

end.
