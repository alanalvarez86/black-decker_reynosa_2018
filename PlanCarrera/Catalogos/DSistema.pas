unit DSistema;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBaseSistema, Db, DBClient, ZetaClientDataSet, ZetaServerDataSet;

type
  TdmSistema = class(TdmBaseSistema)
    cdsAdicionalesAccesos: TZetaClientDataSet;
    cdsGruposAdic: TZetaLookupDataSet;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmSistema: TdmSistema;

implementation

{$R *.DFM}

end.
