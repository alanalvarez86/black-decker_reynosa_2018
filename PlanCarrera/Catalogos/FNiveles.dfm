inherited Niveles: TNiveles
  Left = 220
  Top = 163
  Caption = 'Niveles'
  ClientHeight = 271
  ClientWidth = 647
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 647
    inherited ValorActivo2: TPanel
      Width = 388
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 647
    Height = 252
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'NP_CODIGO'
        Title.Caption = 'C�digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NP_DESCRIP'
        Title.Caption = 'Descripci�n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NP_ACTITUD'
        Title.Caption = 'Actitud'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NP_INGLES'
        Title.Caption = 'Ingl�s'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NP_NUMERO'
        Title.Caption = 'N�mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NP_TEXTO'
        Title.Caption = 'Texto'
        Width = 200
        Visible = True
      end>
  end
end
