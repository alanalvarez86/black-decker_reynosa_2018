inherited SelAccionesCompetencia: TSelAccionesCompetencia
  Left = 240
  Top = 198
  Caption = 'Seleccionar Acciones'
  ClientHeight = 373
  ClientWidth = 442
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 337
    Width = 442
    inherited OK: TBitBtn
      Left = 274
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 359
    end
  end
  object Lista: TCheckListBox
    Left = 0
    Top = 0
    Width = 442
    Height = 337
    Align = alClient
    ItemHeight = 13
    TabOrder = 1
  end
end
