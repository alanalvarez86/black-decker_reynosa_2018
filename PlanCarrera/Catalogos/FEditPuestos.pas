unit FEditPuestos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, ComCtrls, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask,
     ZBaseEdicionRenglon,
     ZetaDBGrid,
     ZetaEdit,
     ZetaKeyLookup,
     ZetaMessages,
     ZetaSmartLists,
     ZetaDBTextBox;

type
  TEditPuestos = class(TBaseEdicionRenglon)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    PU_DETALLE: TDBMemo;
    Dimensiones: TTabSheet;
    Panel3: TPanel;
    bbAgregarDimensiones: TBitBtn;
    bbBorrarDimensiones: TBitBtn;
    bbModificarDimensiones: TBitBtn;
    GridDimensiones: TZetaDBGrid;
    dsDimensiones: TDataSource;
    FP_CODIGO: TZetaDBKeyLookup;
    NP_CODIGO: TZetaDBKeyLookup;
    GroupBox1: TGroupBox;
    PD_DESCRIP: TDBMemo;
    SplitterDimension: TSplitter;
    PU_CODIGO: TZetaDBTextBox;
    PU_DESCRIP: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure bbAgregarDimensionesClick(Sender: TObject);
    procedure bbBorrarDimensionesClick(Sender: TObject);
    procedure bbModificarDimensionesClick(Sender: TObject);
    procedure BuscarBtnClick(Sender: TObject);
    procedure GridRenglonesEnter(Sender: TObject);
    procedure GridRenglonesColEnter(Sender: TObject);
    procedure GridRenglonesColExit(Sender: TObject);
    procedure dsRenglonDataChange(Sender: TObject; Field: TField);
    procedure dsDimensionesDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    function RequisitoEnfocado: Boolean;
    procedure BuscaCalificacion;
    procedure ActivaBotonBusqueda;
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure DoLookup; override;
    procedure HabilitaControles; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure DoCancelChanges; override;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  EditPuestos: TEditPuestos;

implementation

{$R *.DFM}

uses
    FSelCompetenciasPuesto,
    FSelDimensionesPuesto,
    DPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaDialogo,
    ZBaseDlgModal,
    ZetaCommonTools,
    ZAccesosTress;

const
     K_COLUMNA_COMPETENCIA_CODIGO = 0;
     K_COLUMNA_COMPETENCIA_DESCRIPCION = 1;
     K_COLUMNA_COMPETENCIA_REQUISITO = 2;
     K_COLUMNA_DIMENSION_CODIGO = 0;
     K_COLUMNA_DIMENSION_NOMBRE = 1;

procedure TEditPuestos.FormCreate(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_PUESTO;
     FirstControl := FP_CODIGO;
     //HelpContext:= H60617_Tipo_contrato;
     FP_CODIGO.LookupDataset := dmPlanCarrera.cdsFamilias;
     NP_CODIGO.LookupDataset := dmPlanCarrera.cdsNiveles;
     with GridRenglones do
     begin
          for i := K_COLUMNA_COMPETENCIA_CODIGO to K_COLUMNA_COMPETENCIA_DESCRIPCION do
          begin
               with Columns[ i ] do
               begin
                    Color := clInfoBk;
                    Font.Color := clInfoText;
                    ReadOnly := True;
               end;
          end;
     end;
     with GridDimensiones do
     begin
          Options := GridRenglones.Options;
          for i := K_COLUMNA_DIMENSION_CODIGO to K_COLUMNA_DIMENSION_NOMBRE do
          begin
               with Columns[ i ] do
               begin
                    Color := clInfoBk;
                    Font.Color := clInfoText;
                    ReadOnly := True;
               end;
          end;
     end;
end;

procedure TEditPuestos.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsCalificaciones.Conectar;
          DataSource.DataSet := cdsEditPuesto;
          dsRenglon.DataSet := cdsCompetenciaPuesto;
          dsDimensiones.DataSet := cdsPuestoDimension;
     end;
end;

procedure TEditPuestos.DoLookup;
begin
     inherited DoLookup;
end;

procedure TEditPuestos.HabilitaControles;
begin
     inherited HabilitaControles;
end;

procedure TEditPuestos.Agregar;
begin
     inherited Agregar;
end;

function TEditPuestos.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se permite agregar Puestos';
     Result := False;
end;

procedure TEditPuestos.Borrar;
begin
     inherited Borrar;
end;

function TEditPuestos.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se permite borrar Puestos';
     Result := False;
end;

procedure TEditPuestos.Modificar;
begin
     inherited Modificar;
end;

procedure TEditPuestos.DoCancelChanges;
begin
     with dmPlanCarrera do
     begin
          cdsCompetenciaPuesto.CancelUpdates;
          cdsPuestoDimension.CancelUpdates;
     end;
     inherited DoCancelChanges;
end;

procedure TEditPuestos.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if GridEnfocado and RequisitoEnfocado then
     begin
          if ( ssCtrl in Shift ) then
          begin
               case Key of
                    66:  { Letra F = Buscar }
                    begin
                         Key := 0;
                         DoLookup;
                    end;
               end;
          end;
          if ( Key = VK_RETURN ) then
          begin
               Key := 0;
          end;
     end;
     inherited KeyDown( Key, Shift );
end;

{ Eventos del GridRenglones - Competencias }

function TEditPuestos.RequisitoEnfocado: Boolean;
begin
     Result := False;
     if ( ActiveControl = GridRenglones ) then
     begin
          with GridRenglones do
          begin
               if Assigned( SelectedField ) then
                  Result := ( SelectedField.FieldName = 'CA_CODIGO' );
          end;
     end;
end;

procedure TEditPuestos.dsRenglonDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dsRenglon.Dataset do
     begin
          GridRenglones.Columns[ K_COLUMNA_COMPETENCIA_REQUISITO ].ReadOnly := IsEmpty;
          Self.BBBorrar.Enabled := not IsEmpty;
          Self.BBModificar.Enabled := not IsEmpty;
     end;
end;

procedure TEditPuestos.GridRenglonesEnter(Sender: TObject);
begin
     inherited;
     ActivaBotonBusqueda;
end;

procedure TEditPuestos.GridRenglonesColEnter(Sender: TObject);
begin
     inherited;
     ActivaBotonBusqueda;
end;

procedure TEditPuestos.GridRenglonesColExit(Sender: TObject);
begin
     inherited;
     ActivaBotonBusqueda;
end;

procedure TEditPuestos.BBAgregarClick(Sender: TObject);
begin
     inherited;
     ShowDlgModal( SelCompetenciasPuesto, TSelCompetenciasPuesto );
end;

procedure TEditPuestos.BBBorrarClick(Sender: TObject);
begin
     if ZetaDialogo.ZWarningConfirm( '� Atenci�n !', '� Desea borrar la Competencia ?', 0, mbCancel ) then
        inherited;
end;

procedure TEditPuestos.BBModificarClick(Sender: TObject);
begin
     inherited;
     ActiveControl := GridRenglones;
     with GridRenglones do
     begin
          SelectedField := Columns[ K_COLUMNA_COMPETENCIA_REQUISITO ].Field;
     end;
end;

procedure TEditPuestos.ActivaBotonBusqueda;
begin
     BuscarBtn.Visible := RequisitoEnfocado;
     BuscarBtn.Enabled := BuscarBtn.Visible;
end;

procedure TEditPuestos.BuscarBtnClick(Sender: TObject);
begin
     inherited;
     if RequisitoEnfocado then
     begin
          BuscaCalificacion;
     end;
end;

procedure TEditPuestos.BuscaCalificacion;
var
   sCalif, sCalifDesc: String;
begin
     with dmPlanCarrera.cdsCompetenciaPuesto do
     begin
          sCalif := FieldByName( 'CA_CODIGO' ).AsString;
          if dmPlanCarrera.cdsCalificaciones.Search( '', sCalif, sCalifDesc ) then
          begin
               if ( sCalif <> FieldByName( 'CA_CODIGO' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( 'CA_CODIGO' ).AsString := sCalif;
               end;
          end;
     end;
end;

{ Eventos del GridDimensiones - Dimensiones }

procedure TEditPuestos.dsDimensionesDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dsDimensiones.Dataset do
     begin
          PD_DESCRIP.Enabled := not IsEmpty;
          Self.bbBorrarDimensiones.Enabled := not IsEmpty;
          Self.bbModificarDimensiones.Enabled := not IsEmpty;
     end;
end;

procedure TEditPuestos.bbAgregarDimensionesClick(Sender: TObject);
begin
     inherited;
     ShowDlgModal( SelDimensionesPuesto, TSelDimensionesPuesto );
end;

procedure TEditPuestos.bbBorrarDimensionesClick(Sender: TObject);
begin
     with dmPlanCarrera.cdsPuestoDimension do
     begin
          if not IsEmpty then
          begin
               if ZetaDialogo.ZWarningConfirm( '� Atenci�n !', '� Desea borrar la Dimensi�n ?', 0, mbCancel ) then
               begin
                    Delete;
               end;
          end;
     end;
end;

procedure TEditPuestos.bbModificarDimensionesClick(Sender: TObject);
begin
     inherited;
     with dmPlanCarrera.cdsPuestoDimension do
     begin
          if not IsEmpty then
             Edit;
     end;
     ActiveControl := PD_DESCRIP;
end;

end.

