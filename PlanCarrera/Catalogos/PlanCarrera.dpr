program PlanCarrera;

uses
  MidasLib,
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in 'FTressShell.pas' {TressShell},
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  dBaseGlobal in '..\..\DataModules\dBaseGlobal.pas',
  DBaseDiccionario in '..\..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBaseSistema in '..\..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  FSistBaseEditEmpresas in '..\..\Sistema\FSistBaseEditEmpresas.pas' {SistBaseEditEmpresas},
  ZBaseEditAccesos in '..\..\Tools\ZBaseEditAccesos.pas' {ZetaEditAccesos},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseEdicion in '..\..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  ZBaseConsulta in '..\..\tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseEdicionRenglon in '..\..\Tools\ZBaseEdicionRenglon.pas' {BaseEdicionRenglon},
  ZBaseGlobal in '..\..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  FGlobalEvaluacion_DevEx in 'FGlobalEvaluacion_DevEx.pas' {GlobalEvaluacion_DevEx},
  ZBasicoNavBarShell in '..\..\Tools\ZBasicoNavBarShell.pas' {BasicoNavBarShell},
  ZBaseGlobal_DevEx in '..\..\Tools\ZBaseGlobal_DevEx.pas' {BaseGlobal_DevEx},
  ZBaseGridLectura_DevEx in '..\..\Tools\ZBaseGridLectura_DevEx.pas' {BaseGridLectura_DevEx},
  ZBaseEdicion_DevEx in '..\..\Tools\ZBaseEdicion_DevEx.pas',
  ZBaseEdicionRenglon_DevEx in '..\..\Tools\ZBaseEdicionRenglon_DevEx.pas' {BaseEdicionRenglon_DevEx};


{$R *.RES}
{$R WindowsXP.res}
{$R ..\Traducciones\Spanish.RES}

{procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  {SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;}
  Application.Title := 'Administrador Plan de Carrera';
  Application.HelpFile := 'PlanCarrera.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            //CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            //CierraSplash;
            Free;
       end;
  end;
end.

