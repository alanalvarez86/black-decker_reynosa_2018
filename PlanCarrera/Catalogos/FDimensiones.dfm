inherited Dimensiones: TDimensiones
  Left = 246
  Top = 276
  Caption = 'Dimensiones'
  ClientHeight = 212
  ClientWidth = 626
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 626
    inherited ValorActivo2: TPanel
      Width = 367
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 626
    Height = 193
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'DM_CODIGO'
        Title.Caption = 'C�digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DM_DESCRIP'
        Title.Caption = 'Descripci�n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DM_INGLES'
        Title.Caption = 'Ingl�s'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DM_NUMERO'
        Title.Caption = 'N�mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DM_TEXTO'
        Title.Caption = 'Texto'
        Width = 200
        Visible = True
      end>
  end
end
