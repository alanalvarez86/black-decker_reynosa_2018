unit FSelCompetenciasAccion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, CheckLst, Buttons, ExtCtrls, DB, ZBaseDlgModal,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, Vcl.ImgList,
  cxButtons;

type
  TSelCompetenciaAccion_DevEx = class(TZetaDlgModal_DevEx)
    Lista: TCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure Inicializa;
    procedure GrabaSeleccionados;
    function  YaExistia( const sCodigo : String ) : Boolean;
  public
    { Public declarations }
  end;

var
  SelCompetenciaAccion_DevEx: TSelCompetenciaAccion_DevEx;

implementation

{$R *.DFM}

uses dPlanCarrera;

procedure TSelCompetenciaAccion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Inicializa;
end;

procedure TSelCompetenciaAccion_DevEx.Inicializa;
var
   iPosicion : integer;
   sCodigo: string;
begin
     iPosicion := 0;
     with dmPlanCarrera.cdsCompetencia do
     begin
          Conectar;
          First;
          Lista.Clear;
          while not Eof do
          begin
               sCodigo := FieldByName( 'CM_CODIGO' ).AsString;
               Lista.Items.Add( sCodigo + ' = ' + FieldByName( 'CM_DESCRIP' ).AsString );
               if ( YaExistia( sCodigo ) ) then
                  Lista.Checked[ iPosicion ] := TRUE;
               iPosicion := iPosicion + 1;
               Next;
          end;
     end;

end;

procedure TSelCompetenciaAccion_DevEx.GrabaSeleccionados;
var
   i: Integer;
   sCodigo, sDescripcion: String;
begin
     with dmPlanCarrera.cdsCompetenciaAccion do
     begin
          DisableControls;
          try
             with Lista do
             begin
                  for i := 0 to Items.Count - 1 do
                  begin
                       sCodigo := Items.Names[ i ];
                       sDescripcion := Trim( Items.Values[ sCodigo ] );
                       sCodigo := Trim( sCodigo );
                       if Checked[ i ] then
                       begin
                            if not YaExistia( sCodigo ) then
                            begin
                                 Append;
                                 FieldByName( 'CM_CODIGO' ).AsString := sCodigo;
                                 FieldByName( 'CM_DESCRIP' ).AsString := sDescripcion;
                                 Post;
                            end;
                       end
                       else
                           if YaExistia( sCodigo ) then
                              Delete;
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TSelCompetenciaAccion_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
  GrabaSeleccionados;
end;

function TSelCompetenciaAccion_DevEx.YaExistia(const sCodigo: String): Boolean;
begin
     with dmPlanCarrera do
     begin
          Result := YaExistia( cdsCompetenciaAccion, 'CM_CODIGO', sCodigo );
     end;
end;

end.
