unit FCompetencias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, Vcl.StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ZetaCXGrid;

type
  TCompetencias_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGrid1: TZetaDBGrid;
    CM_CODIGO: TcxGridDBColumn;
    CM_DESCRIP: TcxGridDBColumn;
    TC_DESCRIP: TcxGridDBColumn;
    CM_INGLES: TcxGridDBColumn;
    CM_NUMERO: TcxGridDBColumn;
    CM_TEXTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Competencias_DevEx: TCompetencias_DevEx;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador_DevEx,
    ZetaCommonClasses;

{ TCompetencias }
procedure TCompetencias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_Catalogo_Competencias;
end;

procedure TCompetencias_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  ApplyMinWidth;
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCompetencias_DevEx.Agregar;
begin
     dmPlanCarrera.cdsCompetencia.Agregar;
     DoBestFit;
end;

procedure TCompetencias_DevEx.Borrar;
begin
     dmPlanCarrera.cdsCompetencia.Borrar;
     DoBestFit;
end;

procedure TCompetencias_DevEx.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsTCompetencia.Conectar;
          cdsCompetencia.Conectar;
          DataSource.DataSet := cdsCompetencia;
     end;
end;

procedure TCompetencias_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Competencia', 'COMPETEN', 'CM_CODIGO', dmPlanCarrera.cdsCompetencia );
end;

procedure TCompetencias_DevEx.ImprimirForma;
begin
  inherited;
end;

procedure TCompetencias_DevEx.Modificar;
begin
     dmPlanCarrera.cdsCompetencia.Modificar;
     DoBestFit;
end;

function TCompetencias_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

function TCompetencias_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

procedure TCompetencias_DevEx.Refresh;
begin
     dmPlanCarrera.cdsCompetencia.Refrescar;
end;

end.
