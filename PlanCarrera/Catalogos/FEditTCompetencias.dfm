inherited EditTCompetencias: TEditTCompetencias
  Left = 334
  Top = 265
  Caption = 'Tipos de Competencia'
  ClientHeight = 219
  ClientWidth = 392
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 47
    Top = 41
    Width = 36
    Height = 13
    Caption = 'C'#243'digo:'
  end
  object Label2: TLabel [1]
    Left = 24
    Top = 63
    Width = 59
    Height = 13
    Caption = 'Descripci'#243'n:'
  end
  object Label3: TLabel [2]
    Left = 52
    Top = 107
    Width = 31
    Height = 13
    Caption = 'Ingl'#233's:'
  end
  object Label5: TLabel [3]
    Left = 43
    Top = 129
    Width = 40
    Height = 13
    Caption = 'N'#250'mero:'
  end
  object Label6: TLabel [4]
    Left = 53
    Top = 151
    Width = 30
    Height = 13
    Caption = 'Texto:'
  end
  object Label4: TLabel [5]
    Left = 54
    Top = 85
    Width = 29
    Height = 13
    Caption = 'Clase:'
  end
  inherited PanelBotones: TPanel
    Top = 183
    Width = 392
    inherited OK: TBitBtn
      Left = 224
    end
    inherited Cancelar: TBitBtn
      Left = 309
    end
  end
  inherited PanelSuperior: TPanel
    Width = 392
  end
  inherited PanelIdentifica: TPanel
    Width = 392
    inherited ValorActivo2: TPanel
      Width = 66
    end
  end
  object TC_CODIGO: TZetaDBEdit [9]
    Left = 88
    Top = 37
    Width = 90
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 3
    ConfirmEdit = True
    DataField = 'TC_CODIGO'
    DataSource = DataSource
  end
  object TC_DESCRIP: TDBEdit [10]
    Left = 88
    Top = 59
    Width = 230
    Height = 21
    DataField = 'TC_DESCRIP'
    DataSource = DataSource
    TabOrder = 4
  end
  object TC_INGLES: TDBEdit [11]
    Left = 88
    Top = 103
    Width = 230
    Height = 21
    DataField = 'TC_INGLES'
    DataSource = DataSource
    TabOrder = 6
  end
  object TC_NUMERO: TZetaDBNumero [12]
    Left = 88
    Top = 125
    Width = 110
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 7
    Text = '0.00'
    UseEnterKey = True
    DataField = 'TC_NUMERO'
    DataSource = DataSource
  end
  object TC_TEXTO: TDBEdit [13]
    Left = 88
    Top = 147
    Width = 230
    Height = 21
    DataField = 'TC_TEXTO'
    DataSource = DataSource
    TabOrder = 8
  end
  object TC_TIPO: TZetaDBKeyCombo [14]
    Left = 88
    Top = 81
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
    ListaFija = lfTipoCompete
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'TC_TIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 161
  end
end
