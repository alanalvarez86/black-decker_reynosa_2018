unit FSelDimensionesPuesto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Db, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, CheckLst, Buttons, ExtCtrls, ZBaseDlgModal,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, Vcl.ImgList,
  cxButtons;

type
  TSelDimensionesPuesto_DevEx = class(TZetaDlgModal_DevEx)
    Lista: TCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure Inicializa;
    procedure GrabaSeleccionados;
    function  YaExistia( const sCodigo : String ) : Boolean;
  public
    { Public declarations }
  end;

var
  SelDimensionesPuesto_DevEx: TSelDimensionesPuesto_DevEx;

implementation

{$R *.DFM}

uses
    dPlanCarrera;

{ TSelDimensionesPuesto }

procedure TSelDimensionesPuesto_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Inicializa;
end;

procedure TSelDimensionesPuesto_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     GrabaSeleccionados;
end;

procedure TSelDimensionesPuesto_DevEx.GrabaSeleccionados;
var
   i: Integer;
   sCodigo, sDescripcion: String;
begin
     with dmPlanCarrera.cdsPuestoDimension do
     begin
          DisableControls;
          try
             with Lista do
             begin
                  for i := 0 to Items.Count - 1 do
                  begin
                       sCodigo := Items.Names[ i ];
                       sDescripcion := Trim( Items.Values[ sCodigo ] );
                       sCodigo := Trim( sCodigo );
                       if Checked[ i ] then
                       begin
                            if not YaExistia( sCodigo ) then
                            begin
                                 Append;
                                 FieldByName( 'DM_CODIGO' ).AsString := sCodigo;
                                 FieldByName( 'DM_DESCRIP' ).AsString := sDescripcion;
                                 Post;
                            end;
                       end
                       else
                           if YaExistia( sCodigo ) then
                              Delete;
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TSelDimensionesPuesto_DevEx.Inicializa;
var
   iPosicion : integer;
   sCodigo: string;
begin
     iPosicion := 0;
     with dmPlanCarrera.cdsDimensiones do
     begin
          Conectar;
          First;
          Lista.Clear;
          while not Eof do
          begin
               sCodigo := FieldByName( 'DM_CODIGO' ).AsString;
               Lista.Items.Add( sCodigo + ' = ' + FieldByName( 'DM_DESCRIP' ).AsString );
               if ( YaExistia( sCodigo ) ) then
                  Lista.Checked[ iPosicion ] := TRUE;
               iPosicion := iPosicion + 1;
               Next;
          end;
     end;end;

function TSelDimensionesPuesto_DevEx.YaExistia(const sCodigo: String): Boolean;
begin
     with dmPlanCarrera do
     begin
          Result := YaExistia( cdsPuestoDimension, 'DM_CODIGO', sCodigo );
     end;
end;

end.
