unit FDimensiones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls,
  ZBaseConsulta, Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TDimensiones_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGrid1: TZetaDBGrid;
    DM_CODIGO: TcxGridDBColumn;
    DM_DESCRIP: TcxGridDBColumn;
    DM_INGLES: TcxGridDBColumn;
    DM_NUMERO: TcxGridDBColumn;
    DM_TEXTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Dimensiones_DevEx: TDimensiones_DevEx;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador_DevEx,
    ZetaCommonClasses;

{ TDimensiones }
procedure TDimensiones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_Catalogo_Dimensiones;
end;

procedure TDimensiones_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  ApplyMinWidth;
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TDimensiones_DevEx.Agregar;
begin
     dmPlanCarrera.cdsDimensiones.Agregar;
end;

procedure TDimensiones_DevEx.Borrar;
begin
     dmPlanCarrera.cdsDimensiones.Borrar;
end;

procedure TDimensiones_DevEx.Connect;
begin
     with dmPlanCarrera do
     begin
          //cdsTCompetencia.Conectar;
          cdsDimensiones.conectar;
          DataSource.DataSet := cdsDimensiones;
     end;
end;

procedure TDimensiones_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Dimensiones', 'DIMENSIO', 'DM_CODIGO', dmPlanCarrera.cdsDimensiones );
end;

procedure TDimensiones_DevEx.ImprimirForma;
begin
  inherited;

end;

procedure TDimensiones_DevEx.Modificar;
begin
     dmPlanCarrera.cdsDimensiones.Modificar;
end;

function TDimensiones_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

function TDimensiones_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

procedure TDimensiones_DevEx.Refresh;
begin
     dmPlanCarrera.cdsDimensiones.Refrescar;
end;

end.
