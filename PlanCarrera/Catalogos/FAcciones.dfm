inherited Acciones: TAcciones
  Left = 193
  Top = 232
  Caption = 'Acciones'
  ClientHeight = 220
  ClientWidth = 765
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 765
    inherited ValorActivo2: TPanel
      Width = 506
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 765
    Height = 201
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'AN_CODIGO'
        Title.Caption = 'C�digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AN_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AN_CLASE'
        Title.Caption = 'Tipo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AN_TIP_MAT'
        Title.Caption = 'Material Did�ctico'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AN_DIAS'
        Title.Caption = 'Duraci�n en D�as'
        Width = 100
        Visible = True
      end>
  end
end
