unit FEditFamilias;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, ComCtrls, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask,
     ZBaseEdicionRenglon,
     ZetaDBGrid,
     ZetaEdit,
     ZetaNumero,
     ZetaSmartLists;

type
  TEditFamilias = class(TBaseEdicionRenglon)
    Label1: TLabel;
    Label2: TLabel;
    FP_DESCRIP: TDBEdit;
    FP_CODIGO: TZetaDBEdit;
    Label3: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    FP_TEXTO: TDBEdit;
    FP_NUMERO: TZetaDBNumero;
    FP_INGLES: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure dsRenglonDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure DoLookup; override;
    procedure HabilitaControles; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure DoCancelChanges; override;
  public
    { Public declarations }
  end;

var
  EditFamilias: TEditFamilias;

implementation

{$R *.DFM}

uses FSelCompetenciasFamilia,
     DPlanCarrera,
     ZetaBuscador,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     ZBaseDlgModal,
     ZAccesosTress;

{ TEditFamilias }

const
     K_COLUMNA_COMPETENCIA_CODIGO = 0;
     K_COLUMNA_COMPETENCIA_DESCRIPCION = 1;
     K_COLUMNA_COMPETENCIA_OBSERVACIONES = 2;

procedure TEditFamilias.FormCreate(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_FAMILIA;
     FirstControl := FP_CODIGO;
     with GridRenglones do
     begin
          for i := K_COLUMNA_COMPETENCIA_CODIGO to K_COLUMNA_COMPETENCIA_DESCRIPCION do
          begin
               with Columns[ i ] do
               begin
                    Color := clInfoBk;
                    Font.Color := clInfoText;
                    ReadOnly := True;
               end;
          end;
     end;
     //HelpContext:= H60617_Tipo_contrato;
end;

procedure TEditFamilias.Connect;
begin
     with dmPlanCarrera do
     begin
          {
          cdsEditFamilia.Refrescar;
          }
          DataSource.DataSet := cdsEditFamilia;
          dsRenglon.DataSet := cdsCompetenciaFamilia;
     end;
end;

procedure TEditFamilias.dsRenglonDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dsRenglon.Dataset do
     begin
          Self.GridRenglones.Columns[ K_COLUMNA_COMPETENCIA_OBSERVACIONES ].ReadOnly := IsEmpty;
          Self.BBBorrar.Enabled := not IsEmpty;
          Self.BBModificar.Enabled := not IsEmpty;
     end;
end;

procedure TEditFamilias.BBAgregarClick(Sender: TObject);
begin
     inherited;
     ShowDlgModal( SelCompetenciasFamilia, TSelCompetenciasFamilia );
end;

procedure TEditFamilias.BBBorrarClick(Sender: TObject);
begin
     if not dmPlanCarrera.cdsCompetenciaFamilia.IsEmpty then
     begin
          if ZetaDialogo.ZWarningConfirm( '� Atenci�n !', '� Desea borrar la Competencia ?', 0, mbCancel ) then
             inherited;
     end;
end;

procedure TEditFamilias.BBModificarClick(Sender: TObject);
begin
     inherited;
     ActiveControl := GridRenglones;
     with GridRenglones do
     begin
          SelectedField := Columns[ K_COLUMNA_COMPETENCIA_OBSERVACIONES ].Field;
     end;
end;

procedure TEditFamilias.DoLookup;
begin
     inherited DoLookup;
end;

procedure TEditFamilias.HabilitaControles;
begin
     inherited HabilitaControles;
end;

procedure TEditFamilias.Agregar;
begin
     inherited Agregar;
end;

procedure TEditFamilias.Borrar;
begin
     inherited Borrar;
end;

procedure TEditFamilias.Modificar;
begin
     inherited Modificar;
end;

procedure TEditFamilias.DoCancelChanges;
begin
     dmPlanCarrera.cdsCompetenciaFamilia.CancelUpdates;
     inherited DoCancelChanges;
end;

end.
