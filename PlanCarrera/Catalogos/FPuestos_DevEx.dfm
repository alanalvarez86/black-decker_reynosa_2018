inherited Puestos_DevEx: TPuestos_DevEx
  Caption = 'Puestos'
  ClientHeight = 276
  ClientWidth = 608
  ExplicitWidth = 608
  ExplicitHeight = 276
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 608
    ExplicitWidth = 608
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 349
      ExplicitWidth = 349
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 343
        ExplicitLeft = 3
      end
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 608
    Height = 257
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'PU_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PU_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FP_DESCRIP'
        Title.Caption = 'Familia'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NP_DESCRIP'
        Title.Caption = 'Nivel'
        Width = 82
        Visible = True
      end>
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 608
    Height = 257
    ExplicitWidth = 608
    ExplicitHeight = 257
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object PU_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'PU_CODIGO'
      end
      object PU_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'PU_DESCRIP'
      end
      object FP_DESCRIP: TcxGridDBColumn
        Caption = 'Familia'
        DataBinding.FieldName = 'FP_DESCRIP'
      end
      object NP_DESCRIP: TcxGridDBColumn
        Caption = 'Nivel'
        DataBinding.FieldName = 'NP_DESCRIP'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
