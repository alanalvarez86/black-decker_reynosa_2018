inherited EditAcciones: TEditAcciones
  Left = 368
  Top = 185
  Caption = 'Acciones'
  ClientHeight = 473
  ClientWidth = 592
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 437
    Width = 592
    inherited OK: TBitBtn
      Left = 434
    end
    inherited Cancelar: TBitBtn
      Left = 513
    end
  end
  inherited PanelSuperior: TPanel
    Width = 592
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 592
    inherited ValorActivo2: TPanel
      Width = 266
    end
  end
  inherited Panel1: TPanel
    Width = 592
    object Label1: TLabel
      Left = 110
      Top = 6
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 106
      Top = 30
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre:'
    end
    object AN_CODIGO: TZetaDBEdit
      Left = 150
      Top = 2
      Width = 90
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'AN_CODIGO'
      DataSource = DataSource
    end
    object AN_NOMBRE: TDBEdit
      Left = 150
      Top = 26
      Width = 403
      Height = 21
      DataField = 'AN_NOMBRE'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  inherited PageControl: TPageControl
    Width = 592
    Height = 336
    inherited Datos: TTabSheet
      object Label3: TLabel
        Left = 109
        Top = 6
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ingl'#233's:'
      end
      object Label5: TLabel
        Left = 100
        Top = 29
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object Label6: TLabel
        Left = 110
        Top = 51
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Texto:'
      end
      object Label4: TLabel
        Left = 116
        Top = 74
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object lblCurso: TLabel
        Left = 110
        Top = 97
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Curso:'
      end
      object lblMat: TLabel
        Left = 52
        Top = 119
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Caption = 'Material Did'#225'ctico:'
      end
      object Label10: TLabel
        Left = 94
        Top = 142
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Duraci'#243'n:'
      end
      object Label11: TLabel
        Left = 5
        Top = 164
        Width = 135
        Height = 13
        Alignment = taRightJustify
        Caption = 'Directorio de Archivo / URL:'
      end
      object Label8: TLabel
        Left = 81
        Top = 186
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
      end
      object ArchivoSeek: TSpeedButton
        Left = 555
        Top = 160
        Width = 23
        Height = 23
        Hint = 'Escoger el Directorio del Archivo'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333FFF333333333333000333333333
          3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
          3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
          0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
          BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
          33337777773FF733333333333300033333333333337773333333333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
        Visible = False
      end
      object AN_INGLES: TDBEdit
        Left = 145
        Top = 2
        Width = 230
        Height = 21
        DataField = 'AN_INGLES'
        DataSource = DataSource
        TabOrder = 0
      end
      object AN_NUMERO: TZetaDBNumero
        Left = 145
        Top = 25
        Width = 90
        Height = 21
        Mascara = mnNumeroGlobal
        TabOrder = 1
        Text = '0.00'
        UseEnterKey = True
        DataField = 'AN_NUMERO'
        DataSource = DataSource
      end
      object AN_TEXTO: TDBEdit
        Left = 145
        Top = 47
        Width = 230
        Height = 21
        DataField = 'AN_TEXTO'
        DataSource = DataSource
        TabOrder = 2
      end
      object AN_CLASE: TZetaDBKeyCombo
        Left = 145
        Top = 70
        Width = 232
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 3
        OnChange = AN_CLASEChange
        Items.Strings = (
          '')
        ListaFija = lfClaseAcciones
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'AN_CLASE'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object CU_CODIGO: TZetaDBKeyLookup
        Left = 145
        Top = 93
        Width = 340
        Height = 21
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CU_CODIGO'
        DataSource = DataSource
      end
      object AN_DIAS: TZetaDBNumero
        Left = 145
        Top = 138
        Width = 90
        Height = 21
        Mascara = mnNumeroGlobal
        TabOrder = 6
        Text = '0.00'
        UseEnterKey = True
        DataField = 'AN_DIAS'
        DataSource = DataSource
      end
      object AN_URL: TDBEdit
        Left = 145
        Top = 160
        Width = 405
        Height = 21
        DataField = 'AN_URL'
        DataSource = DataSource
        MaxLength = 150
        TabOrder = 7
      end
      object AN_DETALLE: TDBMemo
        Left = 145
        Top = 183
        Width = 406
        Height = 125
        DataField = 'AN_DETALLE'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 8
      end
      object AN_TIP_MAT: TDBComboBox
        Left = 145
        Top = 115
        Width = 232
        Height = 21
        DataField = 'AN_TIP_MAT'
        DataSource = DataSource
        ItemHeight = 13
        Items.Strings = (
          'Libro'
          'Manual'
          'Docto. Interno'
          'Docto. Electr'#243'nico')
        TabOrder = 5
      end
    end
    inherited Tabla: TTabSheet
      Caption = 'Competencias'
      inherited GridRenglones: TZetaDBGrid
        Width = 584
        Height = 279
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect]
        Columns = <
          item
            Expanded = False
            FieldName = 'CM_CODIGO'
            Title.Caption = 'Competencia'
            Width = 69
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CM_DESCRIP'
            Title.Caption = 'Descripci'#243'n'
            Width = 182
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CM_OBSERVA'
            Title.Caption = 'Observaciones'
            Width = 294
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 584
        inherited BBModificar: TBitBtn
          Visible = False
        end
      end
    end
  end
  inherited dsRenglon: TDataSource
    OnDataChange = dsRenglonDataChange
  end
end
