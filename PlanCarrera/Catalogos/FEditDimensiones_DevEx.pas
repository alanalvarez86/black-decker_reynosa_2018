unit FEditDimensiones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, ZetaNumero, Mask, DBCtrls, StdCtrls, ZetaEdit, Db,
  ExtCtrls, Buttons, ZetaSmartLists, ZBaseEdicion, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarExtItems, dxBar, cxClasses,
  Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditDimensiones_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    DM_CODIGO: TZetaDBEdit;
    Label2: TLabel;
    DM_DESCRIP: TDBEdit;
    Label3: TLabel;
    DM_INGLES: TDBEdit;
    Label5: TLabel;
    DM_NUMERO: TZetaDBNumero;
    Label6: TLabel;
    DM_TEXTO: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditDimensiones_DevEx: TEditDimensiones_DevEx;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador_DevEx,
    ZetaCommonClasses,
    ZAccesosTress;

{ TEditDimensiones }

procedure TEditDimensiones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_DIMENSIO;
     FirstControl := DM_CODIGO;
     HelpContext:= H_Catalogo_Dimensiones;
end;

procedure TEditDimensiones_DevEx.Connect;
begin
     DataSource.DataSet := dmPlanCarrera.cdsDimensiones;
end;

procedure TEditDimensiones_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Dimensiones', 'DIMENSIO', 'DM_CODIGO', dmPlanCarrera.cdsDimensiones );
end;

end.
