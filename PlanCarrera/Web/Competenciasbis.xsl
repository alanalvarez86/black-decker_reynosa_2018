<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>

<!-- TEMPLATE RAIZ -->

<xsl:template match="/">
<html> 
  <head>
    <link rel="stylesheet" href="PlanCarrera.css" type="text/css"/>
        <title> <xsl:apply-templates select="RAIZ/DESCRIPCIONES/MODULO"/> - 
	        <xsl:apply-templates select="RAIZ/DESCRIPCIONES/PANTALLA"/> 
	</title>
  </head>
 <body>
      <font class="TITULO">   
          <b>
            <TITULO><xsl:value-of select="RAIZ/DESCRIPCIONES/PANTALLA"/></TITULO>
          </b> 
      </font>
        <xsl:apply-templates/>
  </body>
</html>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO RAIZ -->


<xsl:template match="RAIZ">
     <xsl:apply-templates/>
</xsl:template> 


<!-- TEMPLATE DE ELEMENTO DESCRIPCIONES -->

<xsl:template match="DESCRIPCIONES">
     <xsl:apply-templates select="modulo"/>
</xsl:template> 


<!-- TEMPLATE DESPLIEGUE MODULO -->

<xsl:template match="MODULO">
    <xsl:value-of select="."/>
</xsl:template>



<!-- TEMPLATE DESCRIPCION ASP
<xsl:template match="ASP">
     <xsl:apply-templates/>
</xsl:template>
-->

<!-- TEMPLATE DESPLIEGUE PANTALLA -->

<xsl:template match="PANTALLA">
     <xsl:value-of select="."/>
</xsl:template> 

<!-- TEMPLATE PARA DESPLIEGUE MASTER (MENU-TITULOS) -->

<xsl:template match="MASTER">
     <hr width="95%"/>               
    <table align="center">
       <tr>
       <xsl:apply-templates/>
       </tr>
    </table>
     <hr width="95%"/>                
</xsl:template>


<!-- TEMPLATE PARA DESCRIPCIONES MASTER (TABLA-IZQUIERDA) -->
<xsl:template match="DESCMASTER">
       <td>
       <table border="0">
          <xsl:apply-templates/>
       </table>
       </td>
</xsl:template>
  


<!-- TEMPLATE PARA CONTENIDO MASTER (TABLA-DERECHA) -->
<xsl:template match="CONTMASTER">
       <td>
       <table border="0"> 
          <xsl:apply-templates/>
       </table>
       </td>
</xsl:template>


<!-- TEMPLATES PARA COLUMNAS DE TABLAS CONTENIDO MASTER -->
 <xsl:template match="CONTENIDO"> 
    <tr><td><xsl:apply-templates/></td></tr>
 </xsl:template>

<!-- TEMPLATE PARA MENU DE DESPLIEGUE MASTER -->

<xsl:template match="MENU">
    <select onchange=""> 
      <xsl:apply-templates select="OPCION|OPCIONSELECTED"/>
     </select> 
</xsl:template> 


<!-- TEMPLATE PARA OPCION DE MENU --> 
<xsl:template match="OPCION">
     <option value="@value"> <xsl:value-of select="@descripcion"/> </option>
</xsl:template>


<!-- TEMPLATE PARA OPCION DE MENU SELECTED--> 
<xsl:template match="OPCIONSELECTED">
      <option value="@value" selected="yes"> <xsl:value-of select="@descripcion"/> </option> 
</xsl:template>

<!-- TEMPLATE PARA LIGA HTML -->  

<xsl:template match="LIGA">
  <a href="{@href}"><xsl:value-of select="."/></a>
</xsl:template> 

<!-- TEMPLATE PARA LISTA HTML -->
<xsl:template match="LISTA">
<ul>  
  <xsl:apply-templates/>
</ul>
</xsl:template>

<!-- TEMPLATE PARA INICISO -->

<xsl:template match="INCISO">
  <li><xsl:apply-templates/></li>
</xsl:template> 


<!-- TEMPLATE DE FOOTER(FECHAS/MENUS RETORNOS)  -->

<xsl:template match="FOOTER">
   <hr width="95%"/>
   <table width="95%" border="0" cellspacing="0" cellpadding="0">
   <tr>
   <td align="left">
      <a href="javascript:window.history.back()">
          <img src="back.gif" border="0" alt="Pagina Anterior"/>
      </a>
   </td>
   <td align="right">
      <a href="PlanCarrera.asp">
          <img src="inicio.gif" border="0" alt="Incio"/>
      </a>
   </td>
   </tr>
   </table>
   <table width="95%">
   <tr align="center">
   <td align="center">
       <a href="PlanCarrera.asp">
           <img src="tress_small.gif" border="0"/>
       </a>
    <br/>
	Plan de Carrera
    </td>
    </tr>
    </table>
    <p align="center"/>
    <font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif">
      Copyright � 1998-2002 
    <a href="http://www.tress.com.mx">Grupo Tress</a> 
      Todos los derechos reservados 
    </font>
<p/>

</xsl:template>
</xsl:stylesheet>







               



