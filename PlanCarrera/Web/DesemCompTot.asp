<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false

	'Declaramos Variables
	dim PlanCarreraXML
	dim strDesmpCompTot
	dim docXSL
	dim docXML

	'Declaramos Variables para Iniciar el Componente y recibir el XML generado
	set PlanCarreraXML = Server.CreateObject("PlanCarrera.dmServerPlanCarrera")
	strDesmpCompTot= PlanCarreraXML.GetDesempenoTotales(GetXML("<TIPO>"+Request("TIPO")+"</TIPO><COMPETENCIA>"+Request("COMPETENCIA")+"</COMPETENCIA>"))
        'Response.write(strDesmpCompTot)

	set docXML = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXML.async = false
        docXML.loadXML(strDesmpCompTot)

	set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSL.async = false
	docXSL.load(Server.MapPath("PlanCarrera.xsl"))

        Response.Write(docXML.transformNode(docXSL))
%>

