<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false
	
	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	dim AccionXML	
	dim strAccion
	dim xmlAccion
	dim docXSL
        dim docXML

	'Declaramos Variables para Iniciar el Componente y recibir el XML generado
	set AccionXML = Server.CreateObject("PlanCarrera.dmServerPlanCarrera")
	strAccion = AccionXML.GetAccion(GetXML("<ACCION>"+Request("ACCION")+"</ACCION>"))

        set docXML = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXML.async = false
        docXML.loadXML( strAccion )

	set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSL.async = false
	docXSL.load(Server.MapPath("PlanCarrera.xsl"))

        Response.Write(docXML.transformNode(docXSL))
%>
