<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
	<xsl:template match="/">
	<xsl:element name="link">
		<xsl:attribute name="rel">stylesheet</xsl:attribute>
		<xsl:attribute name="href">../Portal.css</xsl:attribute>
		<xsl:attribute name="type">text/css</xsl:attribute>
	</xsl:element>
	<xsl:element name="body">
		<xsl:attribute name="vLink">#000000</xsl:attribute>
		<xsl:attribute name="aLink">#000000</xsl:attribute>
		<xsl:attribute name="link">#000000</xsl:attribute>
		<xsl:attribute name="bgColor">#FFFFFF</xsl:attribute>						
		<center>
			<xsl:element name="font">
				<xsl:attribute name="Color">#FF0000</xsl:attribute>			
				<xsl:attribute name="face">Verdana</xsl:attribute>				
				<xsl:element name="h1">��� Se encontraron errores !!!</xsl:element>
			</xsl:element>
			<xsl:element name="hr">
				<xsl:attribute name="width">95%</xsl:attribute>
			</xsl:element>			
			<xsl:element name="br"/>
			<h2><font face="Verdana">Mensajes de error</font></h2>
			<table width="95%" bgcolor="#E6E4E4" border="1" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff">
				<xsl:for-each select="RAIZ/ERRORES">
					<tr align="center">
						 <td><font class="subtitulo">				
								<xsl:value-of select="ERROR"/>
						 </font></td></tr>						
				</xsl:for-each>
			</table>
			<xsl:element name="br"/>
			<xsl:element name="hr">
				<xsl:attribute name="width">95%</xsl:attribute>
			</xsl:element>			
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr><td align="left"><a href="javascript:window.history.back()"><img src="back.gif" border="0" alt="P�gina Anterior" width="79" height="30"/></a></td>
   		  			<td align="Right"><a href="Competencias.asp"><img src="inicio.gif" border="0" alt="Inicio" width="56" height="21"/></a></td>
	     		</tr>
			</table>
			<center>
	 			<table border="0" cellpadding="2" width="100%">
    				<tr><td width="100%" align="center" valign="bottom"><img border="0" src="tress_small.gif"/><xsl:element name="br"/><font face="Arial" size="1">Plan de Carrera<xsl:element name="br"/>Grupo Tress Internacional<xsl:element name="br"/><a href="http://www.tress.com.mx" target="_new">http://www.tress.com.mx</a></font></td></tr>
  				</table>
  			</center>						
			<p align="center">
				<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
					<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
				</font>
			</p>  			
  		</center>
	</xsl:element>
</xsl:template>
</xsl:stylesheet>

