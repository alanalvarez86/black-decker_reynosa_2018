<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false

	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	dim CompxEmpXML
	dim strCompxEmp
	dim xmlCompxEmp
	dim docXSL
    dim docXML
    dim strSupervisor
    dim strCodigo
    
    strCodigo=Request("EMPLEADO")

	if strCodigo<>Session("cb_codigo") then
		strSupervisor = Session("Supervisor")
	else
		strSupervisor = "N"	
	end if


	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	set CompxEmpXML = Server.CreateObject("PlanCarrera.dmServerPlanCarrera")
	strCompxEmp = CompxEmpXML.CalificaCompetencias(GetXML("<EMPLEADO>"+Request("EMPLEADO")+"</EMPLEADO><COMPETENCIA>"+Request("COMPETENCIA")+"</COMPETENCIA><DESCRIPCION>"+Request("DESCRIPCION")+"</DESCRIPCION><CALIFICACION>"+Request("CALIFICACION")+"</CALIFICACION><OBSERVACIONES>"+Request("OBSERVACIONES")+"</OBSERVACIONES><SUPERVISOR>"+strSupervisor+"</SUPERVISOR><PAGINA>"+Request("PAGINA")+"</PAGINA><TIPO>"+Request("TIPO")+"</TIPO>"))

        'Response.write(strCompxEmp)

	set docXML = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXML.async = false
        docXML.loadXML(strCompxEmp)

	set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSL.async = false
	docXSL.load(Server.MapPath("PlanCarrera.xsl"))

        Response.Write(docXML.transformNode(docXSL))
%>
