<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Cat&#225;logo de Acciones</xsl:element>

<script language="javascript">
	function MuestraAcciones( oCombo )
	{
	var sNew
		if ( oCombo.value != T1.value )
		{
			sNew = oCombo.value;
			oCombo.value = T1.value;			
 	 		location.href="Acciones.asp?ItemNum="+sNew;	 	 		
		}	
	}
</script>

<center>
<font class="TITULO">
	<b><TITULO>Cat&#225;logo de Acciones</TITULO></b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<xsl:element name="input">
	<xsl:attribute name="Type">hidden</xsl:attribute>
	<xsl:attribute name="name">T1</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="RAIZ/CLASES/COD_DEFAULT"/></xsl:attribute>
</xsl:element>
<table>
	<tr>
		<td align="right">	
				<font class="LETREROS">
					<b><xsl:value-of select="//CLASES/LABELS/@codigo"/>:</b>			
				</font>	
		</td>	
		<td>
			<xsl:element name="select">
				<xsl:attribute name="name">cboAcciones</xsl:attribute>
				<xsl:attribute name="ID">cboAcciones</xsl:attribute>
				<xsl:attribute name="OnChange">MuestraAcciones(this)</xsl:attribute>
				<xsl:variable name="valorDef"><xsl:value-of select="RAIZ/CLASES/COD_DEFAULT"/></xsl:variable>
					<xsl:for-each select="RAIZ/CLASES/ROWS/ROW">		   
					<xsl:element name="option">
						<xsl:attribute name="value">							
								<xsl:value-of select="@codigo"/>						
						</xsl:attribute>					
						<xsl:if test="@codigo=$valorDef">
							<xsl:attribute name="selected">yes</xsl:attribute>
						</xsl:if>					
						<xsl:value-of select="@descripcion"/>
				</xsl:element>
				</xsl:for-each>				
			</xsl:element>
		</td>	
	</tr>
</table>
<xsl:element name="hr">			
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>			
	<xsl:variable name="CodDef2"><xsl:value-of select="RAIZ/CLASES/COD_DEFAULT"/></xsl:variable>
	<xsl:variable name="Cuantos"><xsl:value-of select="//ACCION/ROWS/@cuantos"/></xsl:variable>				
	<xsl:choose>
		<xsl:when test="$CodDef2=0">
			<xsl:choose>
				<xsl:when test="$Cuantos=0">
					<center>
						<font class="REG_ZERO">
							<b>* No se tienen cursos registrados *</b>
						</font>
					</center>
				</xsl:when>						
				<xsl:otherwise>
					<table class="TH_TEAL" width="95%"><!--cellSpacing="1" cellPadding="10" width="95%" border="0" align="center">-->
					<tr>
						<th class="TH_TEAL"><xsl:value-of select="//ACCION/LABELS/@AN_CODIGO"/></th>
						<th class="TH_TEAL"><xsl:value-of select="//ACCION/LABELS/@AN_NOMBRE"/></th>
						<th class="TH_TEAL"><xsl:value-of select="//ACCION/LABELS/@CU_CODIGO"/></th>							
					</tr>
					<xsl:for-each select="//ACCION/ROWS/ROW">
					<tr>
						<xsl:variable name="Codigo"><xsl:value-of select="@CU_CODIGO"/></xsl:variable>
						<xsl:variable name="LenCodigo"><xsl:value-of select="string-length($Codigo)"/></xsl:variable>								
						<td class="TD_COLOR"><font class="xsmallblack">
							<xsl:element name="a">
								<xsl:attribute name="href">Accion.asp?ItemNum=<xsl:value-of select="@AN_CODIGO"/></xsl:attribute>
								<xsl:value-of select="@AN_CODIGO"/>
							</xsl:element></font></td>
						<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@AN_NOMBRE"/></font></td>
						<xsl:choose>
							<xsl:when test="$LenCodigo!=0">
								<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CU_CODIGO"/><xsl:text> = </xsl:text><xsl:value-of select="@CU_NOMBRE"/></font></td>
							</xsl:when>
							<xsl:otherwise>
								<td class="TD_COLOR">										
									* No existe *
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</tr>
					</xsl:for-each>											
					</table>
				</xsl:otherwise>
			</xsl:choose>						
		</xsl:when>
		<xsl:when test="$CodDef2=1">
			<xsl:choose>					
				<xsl:when test="$Cuantos=0">
					<center>
						<font class="REG_ZERO">
							<b>* No se tiene material didáctico registrado *</b>
						</font>
					</center>
				</xsl:when>
				<xsl:otherwise>					
					<table class="TH_TEAL" width="95%"><!-- cellSpacing="1" cellPadding="10" width="95%" border="0" align="center">-->
					<tr>
						<th class="TH_TEAL"><xsl:value-of select="//ACCION/LABELS/@AN_CODIGO"/></th>
						<th class="TH_TEAL"><xsl:value-of select="//ACCION/LABELS/@AN_NOMBRE"/></th>
						<th class="TH_TEAL"><xsl:value-of select="//ACCION/LABELS/@AN_TIP_MAT"/></th>							
					</tr>
					<xsl:for-each select="//ACCION/ROWS/ROW">
					<tr>
						<td class="TD_COLOR"><font class="xsmallblack">
							<xsl:element name="a">
								<xsl:attribute name="href">Accion.asp?ItemNum=<xsl:value-of select="@AN_CODIGO"/></xsl:attribute>
								<xsl:value-of select="@AN_CODIGO"/>
							</xsl:element></font></td>
						<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@AN_NOMBRE"/></font></td>
						<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@AN_TIP_MAT"/></font></td>							
					</tr>						
					</xsl:for-each>								
					</table>
				</xsl:otherwise>
			</xsl:choose>						
		</xsl:when>
		<xsl:when test="$CodDef2=2">
			<xsl:choose>
				<xsl:when test="$Cuantos=0">
					<center>
						<font class="REG_ZERO">
							<b>* No se tienen actividades registradas *</b>
						</font>
					</center>
				</xsl:when>
				<xsl:otherwise>					
					<table class="TH_TEAL" width="95%"><!-- cellSpacing="1" cellPadding="10" width="95%" border="0" align="center">-->
					<tr>
						<th class="TH_TEAL"><xsl:value-of select="//ACCION/LABELS/@AN_CODIGO"/></th>
						<th class="TH_TEAL"><xsl:value-of select="//ACCION/LABELS/@AN_NOMBRE"/></th>
					</tr>
					<xsl:for-each select="//ACCION/ROWS/ROW">
					<tr>
						<td class="TD_COLOR"><font class="xsmallblack">
							<xsl:element name="a">
								<xsl:attribute name="href">Accion.asp?ItemNum=<xsl:value-of select="@AN_CODIGO"/></xsl:attribute>
								<xsl:value-of select="@AN_CODIGO"/>
							</xsl:element></font></td>
						<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@AN_NOMBRE"/></font></td>
					</tr>
					</xsl:for-each>						
					</table>
				</xsl:otherwise>
			</xsl:choose>					
		</xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="$Cuantos=0">
						<center>
						<font class="REG_ZERO">
							<b>* No se tienen otras acciones registradas *</b>
						</font>
					</center>
				</xsl:when>
				<xsl:otherwise>					
					<table class="TH_TEAL" cellSpacing="1" cellPadding="10" width="95%" border="0" align="center">     		
					<tr>
						<th class="TH_TEAL"><xsl:value-of select="//ACCION/LABELS/@AN_CODIGO"/></th>
						<th class="TH_TEAL"><xsl:value-of select="//ACCION/LABELS/@AN_NOMBRE"/></th>
					</tr>
					<xsl:for-each select="//ACCION/ROWS/ROW">
					<tr>
						<td class="TD_COLOR"><font class="xsmallblack">
							<xsl:element name="a">
								<xsl:attribute name="href">Accion.asp?ItemNum=<xsl:value-of select="@AN_CODIGO"/></xsl:attribute>
								<xsl:value-of select="@AN_CODIGO"/>
							</xsl:element></font></td>
						<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@AN_NOMBRE"/></font></td>
					</tr>
					</xsl:for-each>						
					</table>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>

<xsl:element name="br"/>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>

<table width="95%">
	<tr align="center">
		<td align="center">
   			<xsl:element name="a">
				<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
			<xsl:element name="img">
				<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
					<xsl:attribute name="border">0</xsl:attribute>
				</xsl:element> 					
			</xsl:element>	
			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>	
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright © 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>	

</xsl:template>
</xsl:stylesheet>