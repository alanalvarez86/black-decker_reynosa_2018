function MuestraCombo( sURL, oControl, oCombo )
{
  var sNew
  sNew = oCombo.value;
  oCombo.value = oControl.value;
  location.href=sURL+"?ItemNum="+sNew;
}
function ComboSubmit( oCombo, oControl, oForma )
{
  var sOld;
  sOld = oControl.value;
  if ( sOld != oCombo.value )
  {
    oControl.value = oCombo.value;
    oCombo.value = sOld;
    oForma.submit();
  }
}
function ComboSubmitPagina( oCombo, oControl, oForma )
{
  var sOld;
  sOld = oControl.value;
  if ( sOld != oCombo.value )
  {
  	oForma.PAGINA.value = "1";
    oControl.value = oCombo.value;
    oCombo.value = sOld;
    oForma.submit();
  }
}
function MenuTemp( oCombo, oControl )
{
  oControl.value = oCombo.value;
}

function Regresa( oForma )
{
 location.href='javascript:window.history.back()';
}

function Cambia( oForma )
{
  oForma.BTNACEPTAR.value = "Aceptar";
}
function Acepta( oForma )
{
  if ( oForma.BTNACEPTAR.value != "Aceptar" )
  {
 	location.href='javascript:window.history.back()';  
  }
  else
  {
    oForma.submit();
  }  
}
