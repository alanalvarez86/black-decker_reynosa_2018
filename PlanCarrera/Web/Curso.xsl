<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Curso</xsl:element>

<center>
<font class="TITULO">
	<b>Curso</b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<table>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//CURSO/LABELS/@CU_CODIGO"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//CURSO/ROWS/ROW/@CU_CODIGO"/>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//CURSO/LABELS/@CU_NOMBRE"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//CURSO/ROWS/ROW/@CU_NOMBRE"/>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//CURSO/LABELS/@TB_ELEMENT"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:variable name="strTipo"><xsl:value-of select="//CURSO/ROWS/ROW/@TB_ELEMENT"/></xsl:variable>
			<xsl:if test="string-length($strTipo)!=0">
				<xsl:element name="a">
					<xsl:attribute name="href">Cursos.asp?ItemNum=<xsl:value-of select="//CURSO/ROWS/ROW/@CU_CLASIFI"/></xsl:attribute>
					<xsl:value-of select="$strTipo"/>
				</xsl:element>	
			</xsl:if>
			<xsl:if test="string-length($strTipo)=0">
				* No se tiene registrado *
			</xsl:if>	
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//CURSO/LABELS/@CU_ACTIVO"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//CURSO/ROWS/ROW/@CU_ACTIVO"/>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//CURSO/LABELS/@CU_HORAS"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//CURSO/ROWS/ROW/@CU_HORAS"/>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//CURSO/LABELS/@MA_NOMBRE"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:variable name="strCodigo"><xsl:value-of select="//CURSO/ROWS/ROW/@MA_CODIGO"/></xsl:variable>
			<xsl:if test="string-length($strCodigo)!=0">
				<xsl:element name="a">
					<xsl:attribute name="href">Maestro.asp?ItemNum=<xsl:value-of select="//CURSO/ROWS/ROW/@MA_CODIGO"/></xsl:attribute>
					<xsl:value-of select="$strCodigo"/><xsl:text> = </xsl:text><xsl:value-of select="//CURSO/ROWS/ROW/@MA_NOMBRE"/>
				</xsl:element>
			</xsl:if>
			<xsl:if test="string-length($strCodigo)=0">
				* No se tiene registrado *
			</xsl:if>
		</td>
	</tr>
</table>

<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<xsl:variable name="NumReg"><xsl:value-of select="//CALCURSO/ROWS/@cuantos"/></xsl:variable>
<table width="95%" >
<tr>
	<font class="SUBTITULO">
		<b>Calendario</b>
	</font>
</tr>
</table>

<xsl:if test="$NumReg!=0">	
	<table class="TH_TEAL" width="95%">     		
		<tr>
			<th class="TH_TEAL"><xsl:value-of select="//CALCURSO/LABELS/@CC_FECHA"/></th>
		</tr>
		<xsl:for-each select="//CALCURSO/ROWS/ROW">
		<tr>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CC_FECHA"/></font></td>
		</tr>
		</xsl:for-each>
	</table>
</xsl:if>

<xsl:if test="$NumReg=0">	
	<xsl:element name="br"/>
	<font class="REG_ZERO">
		<b>* No hay datos *</b>
	</font>	
</xsl:if>

<xsl:element name="br"/>

<table width="95%" >
<tr>
	<font class="xsmallblack">	
	<xsl:element name="a">
		<xsl:attribute name="href">MatrizxCurso.asp?ItemNum=<xsl:value-of select="//CURSO/ROWS/ROW/@CU_CODIGO"/></xsl:attribute>
		Matriz de Entrenamiento
	</xsl:element></font>
</tr>
<tr>
	<font class="xsmallblack">	
	<xsl:element name="a">
		<xsl:attribute name="href">CursosProgTot.asp?TIPO=<xsl:value-of select="//CURSO/ROWS/ROW/@CU_CLASIFI"/><xsl:text>&#38;</xsl:text>CURSO=<xsl:value-of select="//CURSO/ROWS/ROW/@CU_CODIGO"/></xsl:attribute>
		Cursos Programados
	</xsl:element></font>
</tr>
</table>

<xsl:element name="br"/>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td> 			
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>

<table width="95%">
	<tr align="center">
		<td align="center">
   			<xsl:element name="a">
				<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
			<xsl:element name="img">
				<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
					<xsl:attribute name="border">0</xsl:attribute>
				</xsl:element> 					
			</xsl:element>	
			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>	
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>	

</xsl:template>
</xsl:stylesheet>