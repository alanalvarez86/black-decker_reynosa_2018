<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Supervisor</xsl:element>

<center>
<font class="TITULO">
	<b>Supervisor</b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<table>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//SUPERVISOR/LABELS/@CB_CODIGO"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">Empleado.asp?ItemNum=<xsl:value-of select="//SUPERVISOR/ROWS/ROW/@CB_CODIGO"/></xsl:attribute>
				<xsl:value-of select="//SUPERVISOR/ROWS/ROW/@CB_CODIGO"/>
			</xsl:element>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//SUPERVISOR/LABELS/@PRETTYNAME"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//SUPERVISOR/ROWS/ROW/@PRETTYNAME"/>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LBLCOLABORA"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:variable name="cuantos"><xsl:value-of select="//COLABORA/ROWS/@cuantos"/></xsl:variable>
			<xsl:choose>
				<xsl:when test="$cuantos!=0">
					<xsl:value-of select="//COLABORA/ROWS/@cuantos"/>			
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>	
		</td>
	</tr>
</table>

<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<xsl:variable name="NumReg"><xsl:value-of select="//CALCURSO/ROWS/@cuantos"/></xsl:variable>

<table width="95%" >
<tr>
	<font class="SUBTITULO">
		<b>Colaboradores</b>
	</font>
</tr>
</table>

<xsl:choose>
	<xsl:when test="$NumReg!=0">	
		<table class="TH_TEAL" width="95%">     		
			<tr>
				<th class="TH_TEAL"><xsl:value-of select="//COLABORA/LABELS/@CB_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//COLABORA/LABELS/@PRETTYNAME"/></th>				
				<th class="TH_TEAL"><xsl:value-of select="//COLABORA/LABELS/@CB_PUESTO"/></th>
			</tr>
			<xsl:for-each select="//COLABORA/ROWS/ROW">
			<tr>
				<td class="TD_COLOR"><font class="xsmallblack">			
				<xsl:element name="a">
					<xsl:attribute name="href">Empleado.asp?ItemNum=<xsl:value-of select="@CB_CODIGO"/></xsl:attribute>
					<xsl:value-of select="@CB_CODIGO"/>
				</xsl:element></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@PRETTYNAME"/></font></td>				
				<td class="TD_COLOR"><font class="xsmallblack">					
				<xsl:element name="a">
					<xsl:attribute name="href">Puesto.asp?ItemNum=<xsl:value-of select="@CB_PUESTO"/></xsl:attribute>
					<xsl:value-of select="@PU_DESCRIP"/>
				</xsl:element></font></td>	
			</tr>
			</xsl:for-each>
		</table>
	</xsl:when>
	<xsl:otherwise>
		<xsl:element name="br"/>
		<font class="REG_ZERO">
			<b>* No hay datos *</b>
		</font>
	</xsl:otherwise>	
</xsl:choose>

<xsl:element name="br"/>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td> 			
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>

<table width="95%">
	<tr align="center">
		<td align="center">
   			<xsl:element name="a">
				<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
			<xsl:element name="img">
				<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
					<xsl:attribute name="border">0</xsl:attribute>
				</xsl:element> 					
			</xsl:element>	
			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>	
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>	

</xsl:template>
</xsl:stylesheet>