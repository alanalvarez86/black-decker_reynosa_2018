unit FCafetera;
{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi XE3                                 ::
  :: Unidad:      FCafetera.pas                              ::
  :: Descripci�n: Programa principal de Cafetera.exe         ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses
  TLHelp32, PSAPI, FServerBase, ComCtrls, Messages, System.Classes, Data.DB, Datasnap.DBClient, ExtCtrls,
  //
  ZetaMessages;

const

  K_LONGITUD = 50;

type
  TCafeServer = class;

  TDateTrigger = class(TThread)
  private
    { Private declarations }
    FForma: TCafeServer;
  protected
    { Protected declarations }
    procedure Execute; override;
  public
    { Public declarations }
    constructor Create(Forma: TCafeServer);
  end;

  TCafeServer = class(TServerBase)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FTrigger        : TDateTrigger;
    FListaParametros: TStrings;
    FAutoReset      : Boolean;
    FProgressBar    : TProgressBar;
    FImagen         : TImage;
    FPrimera        : Boolean;
    procedure CargaIcono(const oIcon: PChar);
    procedure DescargaIcono;
    procedure GaugeAdvance(const sMsg: String; const iValue: Integer);
    procedure GaugeFinish(const sMsg: String; const iValue: Integer);
    procedure GaugeSetSize(const sMsg: String; const iValue: Integer);
    procedure GetListaParametros;
  public
    { Public declarations }
    procedure Start;
    procedure ReiniciarReglas;
    procedure Stop;
    property Imagen: TImage read FImagen write FImagen;
    property ProgressBar: TProgressBar read FProgressBar write FProgressBar;
  end;

var
  CafeServer: TCafeServer;

implementation

{$R *.DFM}

uses
  SysUtils, Windows, CafeteraConsts, Forms,  CafeteraUtils, ZetaCommonTools, ZetaCommonClasses, ZetaHelpCafe, DateUtils, DCafetera,
  DZetaServerProvider;

{ ******* TDateTrigger ******** }

constructor TDateTrigger.Create(Forma: TCafeServer);
begin
  inherited Create(True);
  FForma := Forma;
  FreeOnTerminate := True;
end;

procedure TDateTrigger.Execute;
const
  K_SEGUNDOS   = 1;    // N�mero de segundos a esperar antes de verificar de nuevo puertos, hora de reinicio.
var
  FStart, dAhora, dLeido: TDateTime;
  iPuerto               : Integer;
  iSegundero            : Integer;
begin
  iSegundero := K_SEGUNDOS + 1;
  // Poner la bandera de ultima lectura un segundo despues de la real
  FStart := FForma.GetGlobal(K_IDENT_HORA_REINICIO, K_CAFETERA_HORA_REINICIO);
  dLeido := IncSecond(FStart, 1);

  while not Terminated do begin
    // Leer el global por si hay cambios desde el Configurador
    if iSegundero >= K_SEGUNDOS then begin
      iSegundero := 0;
      // Vigiliar cambios de hora de reinicio
      FStart := FForma.GetGlobal(K_IDENT_HORA_REINICIO, K_CAFETERA_HORA_REINICIO);

      // Vigilar cambios de puerto
      if IsDesktopMode then
        iPuerto := FForma.GetGlobal(K_IDENT_PUERTO, K_CAFETERA_PUERTO)
      else
        iPuerto := FForma.GetGlobal(K_IDENT_PUERTO, 0);
      if (iPuerto <> FForma.PortNumber) or (not Assigned(FForma.SOAPServer)) then begin
        // Cambia el puerto desde el Configurador, se actualiza de inmediato
        FForma.PortNumber := iPuerto;
        FForma.Connected  := True;
      end;
    end;
    Sleep(K_SEGUNDOS * 1000);
    if Terminated then
      Break;
    Inc(iSegundero);

    // Se ha llegado al minuto "D"?
    dAhora := Time;
    dAhora := EncodeTime(HourOf(dAhora), MinuteOf(dAhora), 0, 0);
    if (dAhora = FStart) then begin
      dLeido := dAhora;
      Synchronize(FForma.Start);
       Sleep( 60 * 1000 ) ;
    end;
  end;
end;

{ ******** TServerComparte ********* }

procedure TCafeServer.DataModuleCreate(Sender: TObject);
begin
  inherited;
  FPrimera       := True;
  FProgressBar   := nil;
  dmCafetera     := TdmCafetera.Create(Self);
  MainDataModule := dmCafetera;
  dmCafetera.Connect;
  if IsDesktopMode then
    PortNumber := GetGlobal(K_IDENT_PUERTO, K_CAFETERA_PUERTO);

  // CV: Para quitar el restart, eliminar esta linea
  FTrigger := TDateTrigger.Create(Self);

  FListaParametros := TStringList.Create;
  GetListaParametros;
  FAutoReset := False;
  if FListaParametros.Count > 0 then
    FAutoReset := UpperCase(FListaParametros.Values[P_AUTORESET]) = K_GLOBAL_SI;
end;

procedure TCafeServer.DataModuleDestroy(Sender: TObject);
begin
  LogMessage('Apagando Servidor...');
  FTrigger.Terminate;
  FreeAndNil(dmCafetera);
  FreeAndNil(FListaParametros);
  LogMessage('Servidor Apagado');
  inherited;
end;

procedure TCafeServer.GetListaParametros;
var
  i: Integer;
begin
  with FListaParametros do begin
    Clear;
    for i := 1 to ParamCount do begin
      // se modifico uppercase los parametros respetan minusculas
      Add(ParamStr(i));
    end;
  end;
end;

procedure TCafeServer.GaugeSetSize(const sMsg: String; const iValue: Integer);
begin
  LogIt(sMsg);

  if not Assigned(FProgressBar) then
    Exit;
  with FProgressBar do begin
    Visible  := True;
    Position := 0;
    Min      := 0;
    Max      := iValue;
    Refresh;
  end;
end;

procedure TCafeServer.GaugeAdvance(const sMsg: String; const iValue: Integer);
begin
  LogIt(sMsg);

  if not Assigned(FProgressBar) then
    Exit;
  FProgressBar.StepBy(iValue);
end;

procedure TCafeServer.GaugeFinish(const sMsg: String; const iValue: Integer);
begin
  LogIt(sMsg);

  if not Assigned(FProgressBar) then
    Exit;
  with FProgressBar do begin
    Position := Max;
    Visible  := False;
  end;
end;

procedure TCafeServer.CargaIcono(const oIcon: PChar);
begin
  if not Assigned(FImagen) then
    Exit;

  with FImagen do begin
    with Picture.Icon do begin
      ReleaseHandle;
      Handle := LoadIcon(0, oIcon);
    end;
    Visible := True;
  end;
end;

procedure TCafeServer.DescargaIcono;
begin
  if not Assigned(FImagen) then
    Exit;

  with FImagen do begin
    with Picture.Icon do begin
      ReleaseHandle;
    end;
    Visible := False;
  end;
end;

procedure TCafeServer.Start;
begin
  LogMessage(StringOfChar('_', K_LONGITUD));
  FPrimera := True;
  ServerConnect(True);
  // CV:Restart, para eliminarlo, quitar esta linea
  FTrigger.Resume;
  ReiniciarReglas;
end;

procedure TCafeServer.ReiniciarReglas;
const
    K_INTENTOS_MAX = 10;
    K_ESPERA_REINTENTO = 30;
var
   iIntento, iEspera : integer;
   lReintentar : boolean;
   lResult: Boolean;
begin
  dmCafetera.IniciarZonaCritica;

  if not FPrimera then
    LogMessage(StringOfChar('_', K_LONGITUD));
  FPrimera := False;

  LogIt(Format('Puerto TCP: %d, Hora de reinicio: %s', [GetGlobal(K_IDENT_PUERTO, K_CAFETERA_PUERTO),
        FormatDateTime('hh:nn', GetGlobal(K_IDENT_HORA_REINICIO, K_CAFETERA_HORA_REINICIO))]));
  lResult := False;
  lReintentar := TRUE;


  try
        with dmCafetera do
        begin
             for iIntento:=0 to K_INTENTOS_MAX do
             begin

                try
                   Close;
                   if GetListaEmpresas then
                   begin
                        GaugeSetSize( 'Reinicializando Compa��as', 4 * Count );
                        GaugeSetSize( 'Reinicializando Dispositivos', 4 * Count );
                        try
                           lResult := Init( GaugeAdvance );
                           lReintentar := FALSE;
                        except
                              on Error: Exception do
                              begin
                                   LogExcepcion( 'Error Al Reinicializar Compa��as', Error );
                              end;
                        end;
                   end
                   else
                       LogMessage( 'No Hay Compa��as Habilitadas' );
                except
                      on Error: Exception do
                      begin
                           LogExcepcion( 'Error Al Releer Compa��as', Error );
                      end;
                end;

                if lReintentar then
                begin
                     LogMessage( Format( 'Esperando %d segundos para reintentar carga de empresas', [K_ESPERA_REINTENTO] )  );
                     for iEspera := 1 to K_ESPERA_REINTENTO do
                     begin
                         sleep( 1000);
                         Application.ProcessMessages;
                     end;
                end
                else
                   break;

             end;
        end;
  finally
        if lResult then begin
          if (dmCafetera.AutoCafeteria or dmCafetera.AutoAccesos) then begin
            if (dmCafetera.AutoCafeteria and dmCafetera.AutoAccesos) then begin
              GaugeFinish('Compa��as Inicializadas', 0);
              GaugeFinish('Dispositivos Inicializados', 0);
            end else begin
              if dmCafetera.AutoCafeteria then
                GaugeFinish('Compa��as Inicializadas - Cafeter�a OK / Caseta DEMO', 0)
              else if dmCafetera.AutoAccesos then
                GaugeFinish('Compa��as Inicializadas - Cafeter�a DEMO / Caseta OK', 0);
            end;
            DescargaIcono;
          end else begin
            GaugeFinish('Trabajando en Modo DEMO', 0);
            CargaIcono(IDI_EXCLAMATION);
          end;
          dmCafetera.Connect;
        end else begin
          GaugeFinish('Problemas Al Inicializar Compa��as', 0);
          CargaIcono(IDI_HAND);
          dmCafetera.Disconnect;
          if FAutoReset then begin
            // Close; // ToDo: Reiniciar servicio cuando la app est� corriendo en modo servicio
          end;
        end;
  end;
  dmCafetera.TerminarZonaCritica;
end;

procedure TCafeServer.Stop;
begin
  Connected := False;
end;

end.
