unit FListarChecadas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls,
     DCafetera,
     FCoffeShop,
     ZetaHora,
     ZetaFecha,
     ZetaNumero,
     ZetaDBTextBox, CheckLst, Grids, DBGrids, ZetaDBGrid, Db;

type
  TListarChecadas = class(TForm)
    PanelInferior: TPanel;
    Salir: TBitBtn;
    PageControl: TPageControl;
    Parametros: TTabSheet;
    Totales: TTabSheet;
    Comidas: TTabSheet;
    FechaLBL: TLabel;
    Fecha: TZetaFecha;
    ComidasLBL: TLabel;
    ListaTipos: TCheckListBox;
    Estacion: TEdit;
    EstacionLBL: TLabel;
    SoloTotales: TCheckBox;
    Generar: TBitBtn;
    dsTotales: TDataSource;
    dsDetalle: TDataSource;
    GridTotales: TZetaDBGrid;
    GridDetalle: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SoloTotalesClick(Sender: TObject);
    procedure GenerarClick(Sender: TObject);
  private
    { Private declarations }
    FCafetera: TdmCafetera;
    procedure SetControls;
  public
    { Public declarations }
    property Cafetera: TdmCafetera read FCafetera write FCafetera;
  end;

var
  ListarChecadas: TListarChecadas;

implementation

uses ZetaCommonTools,
     ZetaDialogo;

{$R *.DFM}

procedure TListarChecadas.FormCreate(Sender: TObject);
begin
     Fecha.Valor := Now;
     ListaTipos.Checked[ 0 ] := True;
end;

procedure TListarChecadas.FormShow(Sender: TObject);
begin
     SetControls;
end;

procedure TListarChecadas.SetControls;
begin
     Comidas.TabVisible := not SoloTotales.Checked;
end;

procedure TListarChecadas.SoloTotalesClick(Sender: TObject);
begin
     SetControls;
end;

procedure TListarChecadas.GenerarClick(Sender: TObject);
var
   oCursor: TCursor;
   sTipos, sDetalle: String;
   i: Integer;
   dValue: TDate;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        sTipos := '';
        with ListaTipos do
        begin
             for i := 0 to ( Items.Count - 1 ) do
             begin
                  if Checked[ i ] then
                     sTipos := sTipos + Format( '"%d", ', [ i + 1 ] );
             end;
        end;
        sTipos := ZetaCommonTools.CortaUltimo( Trim( sTipos ) );
        dValue := Fecha.Valor;
        with dmCafetera do
        begin
             LeeChecadas( dValue, dValue, sTipos, Estacion.Text, SoloTotales.Checked, sDetalle );
             dsTotales.Dataset := adsTotales;
             dsDetalle.Dataset := adsDetalle;
        end;
        PageControl.ActivePage := Totales;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
