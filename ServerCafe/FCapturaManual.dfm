object CapturaManual: TCapturaManual
  Left = 448
  Top = 161
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Captura Manual de Comidas'
  ClientHeight = 244
  ClientWidth = 368
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelInferior: TPanel
    Left = 0
    Top = 209
    Width = 368
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object OK: TBitBtn
      Left = 203
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Grabar Comida'
      Caption = '&OK'
      Default = True
      Enabled = False
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = OKClick
    end
    object Salir: TBitBtn
      Left = 289
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Abandonar Captura'
      Cancel = True
      Caption = '&Salir'
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      ModalResult = 1
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object RespuestaGB: TGroupBox
    Left = 209
    Top = 35
    Width = 159
    Height = 174
    Align = alClient
    Caption = ' Respuesta '
    TabOrder = 3
    object Respuesta: TLabel
      Left = 2
      Top = 15
      Width = 155
      Height = 144
      Align = alClient
      AutoSize = False
      WordWrap = True
    end
    object Tiempo: TLabel
      Left = 2
      Top = 159
      Width = 155
      Height = 13
      Align = alBottom
      Alignment = taCenter
    end
  end
  object PanelControles: TPanel
    Left = 0
    Top = 35
    Width = 209
    Height = 174
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    object EmpleadoLBL: TLabel
      Left = 30
      Top = 24
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = '&Empleado:'
      FocusControl = Empleado
    end
    object FechaLBL: TLabel
      Left = 47
      Top = 71
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = '&Fecha:'
      FocusControl = Fecha
    end
    object HoraLBL: TLabel
      Left = 54
      Top = 94
      Width = 26
      Height = 13
      Alignment = taRightJustify
      Caption = '&Hora:'
      FocusControl = Hora
    end
    object ComidasLBL: TLabel
      Left = 37
      Top = 118
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = '&Comidas:'
      FocusControl = Comidas
    end
    object TipoComidaLBL: TLabel
      Left = 3
      Top = 140
      Width = 77
      Height = 13
      Alignment = taRightJustify
      Caption = '&Tipo de Comida:'
      FocusControl = TipoComida
    end
    object InvitadorLBL: TLabel
      Left = 36
      Top = 48
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = '&Invitador:'
    end
    object Empleado: TZetaNumero
      Left = 83
      Top = 20
      Width = 46
      Height = 21
      Mascara = mnDias
      TabOrder = 1
      Text = '0'
      OnChange = EmpleadoChange
    end
    object Fecha: TZetaFecha
      Left = 83
      Top = 67
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 3
      Text = '10/Jul/98'
      Valor = 35986.000000000000000000
      OnChange = EmpleadoChange
    end
    object Hora: TZetaHora
      Left = 83
      Top = 91
      Width = 40
      Height = 21
      EditMask = '99:99;0'
      TabOrder = 4
      Text = ''
      Tope = 24
      OnChange = EmpleadoChange
    end
    object Comidas: TZetaNumero
      Left = 83
      Top = 114
      Width = 39
      Height = 21
      Mascara = mnDias
      TabOrder = 5
      Text = '0'
      OnChange = EmpleadoChange
    end
    object TipoComida: TComboBox
      Left = 83
      Top = 137
      Width = 115
      Height = 21
      Style = csDropDownList
      TabOrder = 6
      OnChange = EmpleadoChange
      Items.Strings = (
        'Tipo 1'
        'Tipo 2'
        'Tipo 3'
        'Tipo 4'
        'Tipo 5'
        'Tipo 6'
        'Tipo 7'
        'Tipo 8'
        'Tipo 9')
    end
    object EsInvitacion: TCheckBox
      Left = 13
      Top = 2
      Width = 83
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Es In&vitaci'#243'n:'
      TabOrder = 0
      OnClick = EsInvitacionClick
    end
    object Invitador: TZetaNumero
      Left = 83
      Top = 44
      Width = 46
      Height = 21
      Mascara = mnMinutos
      TabOrder = 2
      Text = '0'
    end
  end
  object PanelSuperior: TPanel
    Left = 0
    Top = 0
    Width = 368
    Height = 35
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object EmpresaLBL: TLabel
      Left = 34
      Top = 8
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = 'E&mpresa:'
      FocusControl = Empresa
    end
    object Empresa: TComboBox
      Left = 82
      Top = 4
      Width = 281
      Height = 21
      Style = csDropDownList
      TabOrder = 0
      OnChange = EmpleadoChange
      OnClick = EmpleadoChange
    end
  end
end
