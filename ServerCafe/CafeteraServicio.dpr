program CafeteraServicio;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  SysUtils,
  Windows,
  SvcMgr,
  Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  DServerBase in '..\Servidor\Asta\DServerBase.pas' {dmServerBase: TDataModule},
  FServerBase in '..\Servidor\Asta\FServerBase.pas' {ServerBase},
  FCafetera in 'FCafetera.pas' {CafeServer},
  ServidorSOAPImpl in 'ServidorSOAPImpl.pas',
  ServidorSOAPIntf in 'ServidorSOAPIntf.pas',
  ModuloSOAP in 'ModuloSOAP.pas' {WebModule1: TWebModule},
  DCafetera in 'DCafetera.pas' {dmCafetera: TDataModule},
  DServicio in 'DServicio.pas' {Cafetera_Servicio: TService},
  CafeteraConsts in 'Cliente\CafeteraConsts.pas',
  CafeteraUtils in 'CafeteraUtils.pas',
  FCafeteraServicio in 'FCafeteraServicio.pas' {FormaCafeteraServicio},
  FServidor in 'Cliente\FServidor.pas' {Servidor};

{$R *.RES}

procedure RunAsService;
begin
  // Windows 2003 Server requires StartServiceCtrlDispatcher to be
  // called before CoRegisterClassObject, which can be called indirectly
  // by Application.Initialize. TServiceApplication.DelayInitialize allows
  // Application.Initialize to be called from TService.Main (after
  // StartServiceCtrlDispatcher has been called).
  //
  // Delayed initialization of the Application object may affect
  // events which then occur prior to initialization, such as
  // TService.OnCreate. It is only recommended if the ServiceApplication
  // registers a class object with OLE and is intended for use with
  // Windows 2003 Server.
  //
  // SvcMgr.Application.DelayInitialize := True;
  //
  if not SvcMgr.Application.DelayInitialize or SvcMgr.Application.Installing then
    begin
      SvcMgr.Application.Initialize;
    end;
  CafeServer := nil;
  SvcMgr.Application.CreateForm(TCafetera_Servicio, Cafetera_Servicio);
  SvcMgr.Application.Run;
end;

procedure RunAsApplication;
begin
  Forms.Application.Initialize;
  Forms.Application.Title    := SERVICE_DISPLAYNAME;
  Forms.Application.HelpFile := 'TressCafeteria.hlp>CONTEXT';
  Forms.Application.CreateForm(TFormaCafeteraServicio, FormaCafeteraServicio);
  FormaCafeteraServicio.Show;
  FormaCafeteraServicio.Init;
  Forms.Application.Run;
end;

begin
  GetInstanceName();
  if not TressInstalado then begin
    if IsDesktopMode then
      Windows.MessageBox(GetDesktopWindow, K_NO_TRESS, 'Aviso', 0)
    else
      WriteEventLog(K_NO_TRESS, '', ServiceName);
    Exit
  end;

  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;

  if IsDesktopMode then
    RunAsApplication
  else
    RunAsService;
end.
