unit AstaServiceMgr;
{$D+,L+}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Execfile, AstaSMLauncherUtils, ScktComp, AstaServer,AstaParamList;

type
  TfrmASM = class(TForm)
    dlgOpen: TOpenDialog;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Label1: TLabel;
    AstaServerSocket1: TAstaServerSocket;
    Timer1: TTimer;
    lbxServices: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure sktOneSessionAvailable(Sender: TObject; Socket: Integer);
    procedure AstaServerSocket1CodedParamList(Sender: TObject;
      ClientSocket: TCustomWinSocket; MsgID: Integer;
      Params: TAstaParamList);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    bFirstLoad: boolean;
    ProcessManager: TProcessManager;
    procedure AddFileToList;
    procedure ExeEdit;
    function ExeName:String;
    procedure ExeDelete;
    procedure StartExe;
    procedure StopExe;
  public
    { Public declarations }
    procedure RefreshList;
    procedure RefreshIfChange;
  end;

var
  frmASM: TfrmASM;

implementation

{$R *.DFM}

uses AstaSMEdit;

procedure TfrmASM.FormCreate(Sender: TObject);
begin
  bFirstLoad := true;
  AstaServerSocket1.Active:=True;
end;

procedure TfrmASM.FormActivate(Sender: TObject);
begin
  if bFirstLoad then begin
    ProcessManager := TProcessManager.Create(Self);
    ProcessManager := ProcessManager;
    ProcessManager.UpdateForm := frmASM;
    RefreshList;
    ProcessManager.InitialProcessStart;
    bFirstLoad := false;
  end;
end;

procedure TfrmASM.RefreshList;
begin
  GetExeListFromRegistry(lbxServices.Items,TRUE);
  Refresh;
end;

procedure TfrmASM.RefreshIfChange;
begin
  if ProcessManager.UpdateExitStatus then
    RefreshList;
end;

procedure TfrmASM.Button4Click(Sender: TObject);
begin
   AddFileToList;
end;

procedure TfrmASM.AddFileToList;
begin
  dlgOpen.FileName := '*.exe';
  if dlgOpen.Execute then begin
    frmService.ebxName.Text := filenameFromPath(dlgOpen.FileName);
    frmService.ebxFullPath.Text := dlgOpen.FileName;
    frmService.ebxArguments.Text := '';
    frmService.cbxAutoStart.Checked := false;
    frmService.cbxReStart.Checked := false;
    frmService.SaveLast(dlgOpen.FileName,'',FALSE,FALSE);
    frmService.Show;
//    PutExeIntoRegistry(dlgOpen.FileName,'','',FALSE,TRUE);
  end;
end;

procedure TfrmASM.Button5Click(Sender: TObject);
begin
  ExeEdit;
end;

procedure TfrmASM.ExeEdit;
var
  name : string;
  FullPath : string;
  Arguments : string;
  AutoStart : boolean;
  ReStart : boolean;
begin
  name := ExeName;
  if length(trim(name))>0 then begin
    name := getFld(name,1,Tab);
    getExeFromRegistry(name,FullPath,Arguments,AutoStart,ReStart);
    frmService.ebxName.Text := Name;
    frmService.ebxFullPath.Text := FullPath;
    frmService.ebxArguments.Text := Arguments;
    frmService.cbxAutoStart.Checked := AutoStart;
    frmService.cbxReStart.Checked := ReStart;
    frmService.SaveLast(FullPath,Arguments,AutoStart,ReStart);
    frmService.Show;
  end;
end;

function TfrmASM.ExeName:String;
var
  i : integer;
begin
  result := '';
  if lbxServices.Items.Count > 0 then begin
    i := 0;
    while i<lbxServices.Items.Count do begin
      if lbxServices.Selected[i] then begin
        result := lbxServices.Items.Strings[i];
        i := lbxServices.Items.Count;
      end;
      i := i + 1;
    end;
  end;
  if length(trim(result))>0 then
    result := getFld(result,1,Tab);
end;

procedure TfrmASM.ExeDelete;
var
  name : string;
begin
  name := ExeName;
  if length(trim(name))>0 then begin
    deleteExeFromRegistry(name);
    RefreshList;
  end;
end;

procedure TfrmASM.Button3Click(Sender: TObject);
begin
  ExeDelete;
end;

procedure TfrmASM.Button1Click(Sender: TObject);
begin
  StartExe;
  RefreshList;
end;

procedure TfrmASM.StartExe;
var
  Name,FullPath,Arguments: string;
  AutoStart,ReStart: boolean;
begin
  Name := ExeName;
  if length(trim(Name))>0 then begin
    GetExeFromRegistry(Name,FullPath,Arguments,AutoStart,ReStart);
    ProcessManager.StartProcess(Name,FullPath,Arguments,AutoStart,ReStart);
  end;
end;

procedure TfrmASM.StopExe;
var
  Name: string;
begin
  Name := ExeName;
  if length(trim(Name))>0 then begin
    ProcessManager.StopProcess(Name);
  end;
end;

procedure TfrmASM.Button6Click(Sender: TObject);
begin
  RefreshList;
end;

procedure TfrmASM.Button2Click(Sender: TObject);
begin
  StopExe;
  RefreshList;
end;

procedure TfrmASM.sktOneSessionAvailable(Sender: TObject; Socket: Integer);
begin
  SessionAvailable(Sender,SOcket);
end;

procedure TfrmASM.AstaServerSocket1CodedParamList(Sender: TObject;
  ClientSocket: TCustomWinSocket; MsgID: Integer; Params: TAstaParamList);
begin
case Msgid of
 1000:begin
       ProcessManager.StopProcess(getFld(LbXServices.Items[0],1,tab));
       Params.Clear;
       Params.FastAdd(getFld(LbXServices.Items[0],1,tab)+' restarted');
       timer1.enabled:=true;
      end;
end;
end;

procedure TfrmASM.Timer1Timer(Sender: TObject);
begin
       timer1.enabled:=False;
       LbxServices.ItemIndex:=0;
       StartExe;
       RefreshList;

end;

end.
