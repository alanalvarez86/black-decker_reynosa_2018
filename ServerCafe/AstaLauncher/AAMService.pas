unit AAMService;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
     ZetaRegistryServer,
     AstaSMUtils;

type
  TCafeRegistryServer = class( TZetaRegistryServer )
  private
    { Private declarations }
    function GetCafeServerDependency: string;
    procedure SetCafeServerDependency(const Value: string);
  public
    { Public declarations }
    property CafeServerDependency: string read GetCafeServerDependency write SetCafeServerDependency;
  end;
  TAstaAppManager = class(TService)
    procedure AAMService12Create(Sender: TObject);
    procedure AAMService9Start(Sender: TService; var Started: Boolean);
    procedure AAMService9Stop(Sender: TService; var Stopped: Boolean);
    procedure AAMService9Pause(Sender: TService; var Paused: Boolean);
    procedure AAMService9Continue(Sender: TService; var Continued: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetServiceController: TServiceController; override;
  end;

var
  AstaAppManager: TAstaAppManager;

implementation

uses ZetaCommonTools,
     ZetaWinAPITools;

{$R *.DFM}

const
     CAFE_SERVER_DEPENDENCY = 'CafeServerDependency';

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
     AstaAppManager.Controller(CtrlCode);
end;

{ ******** TCafeRegistryServer ********* }

function TCafeRegistryServer.GetCafeServerDependency: string;
const
     DEFAULT_DEPENDENCY = 'MSSQLSERVER';
begin
     Result := Registry.ReadString( '', CAFE_SERVER_DEPENDENCY, DEFAULT_DEPENDENCY );
end;

procedure TCafeRegistryServer.SetCafeServerDependency(const Value: string);
begin
     Registry.WriteString( '', CAFE_SERVER_DEPENDENCY, Value );
end;

{ ******** TAstaAppManager ********* }

function TAstaAppManager.GetServiceController: TServiceController;
begin
     Result := ServiceController;
end;

procedure TAstaAppManager.AAMService12Create(Sender: TObject);
{$ifdef MSSQL}
var
   FRegistry: TCafeRegistryServer;
   sDependecy: String;
{$endif}
begin
     AstaSMUtils.SVCTHREAD := ServiceThread;
     AstaSMUtils.ProcessManager := AstaSMUtils.TProcessManager.Create(Self);
     {
     AstaSMUtils.ProcessManager.InitialProcessStart;
     if ZetaWinAPITools.GetInterbaseIsInstalled then
     }
     {$ifdef MSSQL}
     FRegistry := TCafeRegistryServer.Create( False );
     try
        sDependecy := FRegistry.CafeServerDependency;
     finally
            FreeAndNil( FRegistry );
     end;
     if ZetaCommonTools.StrLleno( sDependecy ) then
     begin
          with TDependency.Create( Dependencies ) do
          begin
               IsGroup := False;
               Name := sDependecy;
          end;
     end;
     {$endif}
     {$ifdef INTERBASE}
     {$ifdef ANTES}
     with TDependency.Create( Dependencies ) do
     begin
          IsGroup := False;
          Name := 'InterBaseGuardian';
     end;
     with TDependency.Create( Dependencies ) do
     begin
          IsGroup := False;
          Name := 'InterBaseServer';
     end;
     {$ENDIF}
     if ZetaWinAPITools.GetInterbaseIsInstalled then
     begin
          with TDependency.Create( Dependencies ) do
          begin
               IsGroup := False;
               Name := 'InterBaseGuardian';
          end;
          with TDependency.Create( Dependencies ) do
          begin
               IsGroup := False;
               Name := 'InterBaseServer';
          end;
     end
     else
         if ZetaWinAPITools.GetFirebirdIsInstalled then
         begin
              with TDependency.Create( Dependencies ) do
              begin
                   IsGroup := False;
                   Name := 'FirebirdGuardianDefaultInstance';
              end;
         end;
     {$endif}
end;

procedure TAstaAppManager.AAMService9Start(Sender: TService; var Started: Boolean);
begin
     AstaSMUtils.ProcessMonitor := AstaSMUtils.TProcessMonitor.Create( False );
     Started := True;
end;

procedure TAstaAppManager.AAMService9Stop(Sender: TService; var Stopped: Boolean);
var
  ThisProcess, DeadProcess: TProcessNode;
begin
     ThisProcess := AstaSMUtils.ProcessManager.ProcessList.FirstProcess;
     while ( ThisProcess <> nil ) do
     begin
          AstaSMUtils.ProcessManager.StopProcess(ThisProcess.ProcessName);
          DeadProcess := ThisProcess;
          ThisProcess := ThisProcess.NextProcess;
          DeadProcess.Free;
     end;
     AstaSMUtils.ProcessMonitor.Terminate;
     Stopped := True;
end;

procedure TAstaAppManager.AAMService9Pause(Sender: TService; var Paused: Boolean);
begin
     AstaSMUtils.ProcessMonitor.Suspend;
     Paused := True;
end;

procedure TAstaAppManager.AAMService9Continue(Sender: TService; var Continued: Boolean);
begin
     AstaSMUtils.ProcessMonitor.Resume;
     Continued := True;
end;

end.


