ASTA Services as NT Service
Many ASTA users have implemented their ASTA Servers as NT Services. Since ASTA now suports
16 3rd party Delphi Components, extending that support to NT Service version could be a bit
challenging.

So we at ASTA decided to create an Application (AstaServerLauncher) that could launch and
bounce back ASTA Servers if they go down AND to deliver an NT Service Version of
the same executable that would use the registry settings created by running it one time
as an normal EXE.

By supporting the One App as a service, we have enabled ALL AstaServers to run as an NT
Service.

=====================================================================================
AstaServerLauncher:
 designed to launch ASTA servers and bounce them back if they go down.
 Configure it to run one or more ASTA servers and any settings used will
 be used by the AstaServiceLauncherNTS which it reads from the registry.

AstaServerLauncher NTS- NT Service Version of AstaServerLauncher

This does not have a user interface, it is run as a service and reads
the registry to tell which programs to start.  Use the
AstaServiceLauncher to setup the Registry configuration.

To install it as a service run the program with the command line
parameter of /Install.  (You can create a shortcut to use for this by
editing the shortcuts "Target" property and adding /Install to it)
(/Uninstall does the reverse)

Then in control panel, open up Services.  Look for AstaServiceLauncherNTS
Double click it.  Set it to manual or automatic start.    Click on Allow
Service to Interact with desktop so that it is checked.   (You can also
set it to run in its own session by selecting a particular account with
This Account radion button).  Press OK.

You can start the service manually.  Once started it will read the
registry and startup any programs that need AutoStarted.   Then it will
keep those programs alive if they are set to AutoReStart.


