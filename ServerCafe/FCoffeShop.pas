unit FCoffeShop;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Db, Forms, Dialogs,
  Variants,
  DQueries, DZetaServerProvider, ZCreator, ZetaSQLBroker, ZEvaluador, ZetaQRExpr, ZetaCommonClasses,
  ZetaCommonLists;

{$DEFINE QUINCENALES}
{$DEFINE MULT_TIPO_COMIDA} // Permitir multiples registros de cafeteria de diferente tipo en un mismo minuto
{ .$undefine QUINCENALES }

type
  TCallBack = procedure(const sMsg: String; const iValue: Integer) of object;
  TImportResult = (irOk, irError, irRejected);
  TCodigo = String[10]; // C�digos de Tablas y Cat�logos
  TCodigoRegla = Integer;
  TReloj = String[4];
  TDigito = String[1];
  TInvitador = TNumEmp;

  TZetaEvaluadorCafe = class(TObject);
  TCafeteria = class;
  {$IFNDEF INTERBASE}
  THuella = class; // BIOMETRICO
  {$ENDIF}

  TChecada = class(TObject)
  private
    { Private declarations }
    FProvider  : TdmZetaServerProvider;
    FProviderComparte  : TdmZetaServerProvider;
    FCredencial: TDigito;
    FEmpresa   : TDigito;
    FEmpleado  : TNumEmp;
    FEsManual  : Boolean;
    FFecha     : TDate;
    FHora      : String;
    FRespuesta : String;
    FTipoGafete: eTipoGafete;
    FQueryEmpID: TZetaCursor;
    {$IFNDEF INTERBASE}
    FQueryEmpBio: TZetaCursor; // BIOMETRICO
    {$ENDIF}
    function GetGafeteTress(var sGafete: String): Boolean;
    procedure SetFecha(const Value: TDate);
    procedure SetFechaHora(const dValue: TDateTime);
    procedure SetRespuesta(const Value: String);
    {$IFNDEF INTERBASE}
    function GetBiometricoTress(var sGafete: String): Boolean; // Biometrico
    {$ENDIF}
  protected
    { Protected declarations }
    function CheckTipoGafete(const sToken, sData: String): Boolean;
    function GetCodigo: String; virtual;
    function SetEmpleado(var sGafete: String): Boolean;
    function SetGafete(var sGafete: String): Boolean; virtual;
    {$IFNDEF INTERBASE}
    function GetCodigoBio: String; virtual; // BIOMETRICO
    {$ENDIF}
  public
    { Public declarations }
    property Credencial: TDigito read FCredencial write FCredencial;
    property Empresa   : TDigito read FEmpresa write FEmpresa;
    property Empleado  : TNumEmp read FEmpleado write FEmpleado;
    property EsManual  : Boolean read FEsManual write FEsManual;
    property Fecha     : TDate read FFecha write SetFecha;
    property FechaHora : TDateTime write SetFechaHora;
    property Hora      : String read FHora write FHora;
    property Provider  : TdmZetaServerProvider read FProvider write FProvider;
    property QueryEmpID: TZetaCursor read FQueryEmpID write FQueryEmpID;
    property Respuesta : String read FRespuesta write SetRespuesta;
    property TipoGafete: eTipoGafete read FTipoGafete write FTipoGafete;
    function EmpresaOK(Company: TCafeteria): Boolean; virtual;
    procedure Init; virtual;
    procedure SetError(const sValue: String);
    {$IFNDEF INTERBASE}
    property QueryEmpBio: TZetaCursor read FQueryEmpBio write FQueryEmpBio; // BIOMETRICO
    {$ENDIF}

    constructor Create;
    destructor Destroy; override;

  end;

  TChecadaAcceso = class(TChecada)
  private
    { Private declarations }
    FTipo     : eTipoChecadas;
    FRegla    : TCodigoRegla;
    FCaseta   : TCodigo;
    FPassed   : Boolean;
    FStatusDia: eStatusAusencia;
    FHorario  : TCodigo;
    FTurno    : TCodigo;
    procedure SetRegla(const Value: TCodigoRegla);
    procedure SetCaseta(const Value: TCodigo);
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Caseta   : TCodigo read FCaseta write SetCaseta;
    property Horario  : TCodigo read FHorario write FHorario;
    property Passed   : Boolean read FPassed write FPassed;
    property Regla    : TCodigoRegla read FRegla write SetRegla;
    property Tipo     : eTipoChecadas read FTipo write FTipo;
    property Turno    : TCodigo read FTurno write FTurno;
    property StatusDia: eStatusAusencia read FStatusDia write FStatusDia;
    function SetChecada(const sData: String; iYear: Word): Boolean;
    function SetGafete(var sGafete: String): Boolean; override;
    function EsEntrada: Boolean;
    function TipoDesconocido: Boolean;
    function EmpresaOK(Company: TCafeteria): Boolean; override;
    procedure Init; override;
  end;

  TChecadaComida = class(TChecada)
  private
    { Private declarations }
    FEsFueraLinea: Boolean;
    FEsInvitacion: Boolean;
    FExtras      : Boolean;
    FInvitador   : TInvitador;
    FNumComidas  : Integer;
    FRegla       : TCodigoRegla;
    FReloj       : TReloj;
    FTipoComida  : Char;
    function MealType: Word;
    procedure SetExtras(const lValue: Boolean);
    procedure SetRegla(const iValue: TCodigoRegla);
  protected
    { Protected declarations }
    function GetCodigo: String; override;
    {$IFNDEF INTERBASE}
    function GetCodigoBio: String; override; // BIOMETRICO
    {$ENDIF}
  public
    { Public declarations }
    property EsInvitacion: Boolean read FEsInvitacion write FEsInvitacion;
    property Extras      : Boolean read FExtras write SetExtras;
    property EsFueraLinea      : Boolean read FEsFueraLinea write FEsFueraLinea;
    property Invitador   : TInvitador read FInvitador write FInvitador;
    property NumComidas  : Integer read FNumComidas write FNumComidas;
    property Regla       : TCodigoRegla read FRegla write SetRegla;
    property Reloj       : TReloj read FReloj write FReloj;
    property TipoComida  : Char read FTipoComida write FTipoComida;
    function GetChecadaText: String;
    function SetGafete(var sGafete: String): Boolean; override;
    function SetChecada(const sData: String; iYear: Word): Boolean;
    function EmpresaOK(Company: TCafeteria): Boolean; override;
    procedure Init; override;
  end;

  TListaReglas = class;

  TRegla = class(TObject)
  private
    { Private declarations }
    FListaReglas  : TListaReglas;
    FFiltro       : TZetaEvaluador;
    FColFiltro    : Integer;
    FFormulaFiltro: String;
    function GetQuery: TZetaCursor;
  protected
    { Protected declarations }
    property ListaReglas: TListaReglas read FListaReglas;
    property Query      : TZetaCursor read GetQuery;
    function CodigoAsStr: String; virtual; abstract;
    function EvaluaFiltro: Boolean;
    function GetFormula(const sFormula: String): String;
    procedure PreparaEvaluadores; virtual;
  public
    { Public declarations }
    property Filtro       : TZetaEvaluador read FFiltro;
    property FormulaFiltro: String read FFormulaFiltro write FFormulaFiltro;
    property ColFiltro    : Integer read FColFiltro;
    constructor Create(Lista: TListaReglas);
    destructor Destroy; override;
    procedure Init; virtual;
    procedure PreparaUnEvaluador(oEvaluador: TZetaEvaluador; const iColumna: Integer);
  end;

  TReglaAcceso = class(TRegla)
  private
    { Private declarations }
    FCodigo        : TCodigoRegla;
    FEvaluadorValor: TZetaEvaluador;
    FFormulaValor  : String;
    FColValor      : Integer;
    FLetrero       : String;
    FValor         : String;
    FTipo          : eAccesoRegla;
    FSeveridad     : eTipoBitacora;
    FIsOk          : Boolean;
    procedure SetLetrero(const Value: String);
    function GetValor: String;
    procedure SetValor(const Value: String);
  protected
    { Protected declarations }
    function CodigoAsStr: String; override;
    procedure PreparaEvaluadores; override;
  public
    { Public declarations }
    destructor Destroy; override;
    property Codigo: TCodigoRegla read FCodigo write FCodigo;
    property ColValor: Integer read FColValor write FColValor;
    property FormulaValor: String read FFormulaValor write FFormulaValor;
    property EvaluadorValor: TZetaEvaluador read FEvaluadorValor write FEvaluadorValor;
    property Letrero: String read FLetrero write SetLetrero;
    property Valor: String read GetValor write SetValor;
    property IsOk: Boolean read FIsOk;
    property Tipo: eAccesoRegla read FTipo write FTipo;
    property Severidad: eTipoBitacora read FSeveridad write FSeveridad;
    function Evaluar(Checada: TChecadaAcceso): Boolean;
    procedure Init; override;
  end;

  TReglaCafe = class(TRegla)
  private
    { Private declarations }
    FCodigo             : TCodigoRegla;
    FExtras             : TZetaEvaluador;
    FYaConsumidas       : TZetaEvaluador;
    FFormulaExtras      : String;
    FFormulaYaConsumidas: String;
    FLimite             : Integer;
    FMsgFracaso         : String;
    FTipos              : String;
    FNoHayTipos         : Boolean;
    FColExtras          : Integer;
    FColConsumidas      : Integer;
    procedure SetMsgFracaso(const Value: String);
    procedure SetTipos(const sValue: String);
  protected
    { Protected declarations }
    function CodigoAsStr: String; override;
    procedure PreparaEvaluadores; override;
  public
    { Public declarations }
    constructor Create(Lista: TListaReglas);
    destructor Destroy; override;
    property Codigo: TCodigoRegla read FCodigo write FCodigo;
    property FormulaExtras: String read FFormulaExtras write FFormulaExtras;
    property FormulaYaConsumidas: String read FFormulaYaConsumidas write FFormulaYaConsumidas;
    property Extras: TZetaEvaluador read FExtras;
    property YaConsumidas: TZetaEvaluador read FYaConsumidas;
    property ColExtras: Integer read FColExtras;
    property ColConsumidas: Integer read FColConsumidas;
    property Limite: Integer read FLimite write FLimite;
    property MsgFracaso: String read FMsgFracaso write SetMsgFracaso;
    property Tipos: String read FTipos write SetTipos;
    function Evaluar(Checada: TChecadaComida): Boolean;
    procedure Init; override;
  end;

  TListaReglas = class(TObject)
  private
    { Private declarations }
    FCafeteria: TCafeteria;
    {$IFNDEF INTERBASE}
    FHuella: THuella; // BIOMETRICO
    {$ENDIF}
    FEvalPreparados: Boolean;
    FLista         : TList;
    FListaReglas   : TListaReglas;
    FQuery         : TZetaCursor;
    FSQLBroker     : TSQLBroker;
    function GetCreator: TZetaCreator;
  protected
    { Protected declarations }
    property Cafeteria: TCafeteria read FCafeteria;
    {$IFNDEF INTERBASE}
    property Huella: THuella read FHuella; // BIOMETRICO
    {$ENDIF}
    property EvalPreparados: Boolean read FEvalPreparados write FEvalPreparados;
    property ListaReglas   : TListaReglas read FListaReglas;
    function AbreQuery(const iEmpleado: TNumEmp; const dFecha: TDate): Boolean;
    procedure Clear; virtual; abstract;
    procedure Delete(const Index: Integer); virtual; abstract;
    procedure AgregandoRelacion(Sender: TObject);
  public
    { Public declarations }
    property Query       : TZetaCursor read FQuery;
    property oZetaCreator: TZetaCreator read GetCreator;
    property SQLBroker   : TSQLBroker read FSQLBroker;
    constructor Create(Cafeteria: TCafeteria);
    destructor Destroy; override;
    function Count: Integer;
    procedure InitBroker;
    procedure Prepare;
  end;

  TListaReglasAcceso = class(TListaReglas)
  private
    { Private declarations }
    function GetRegla(Index: Integer): TReglaAcceso;
    procedure PreparaEvaluadores;
  protected
    { Protected declarations }
    procedure Clear; override;
    procedure Delete(const Index: Integer); override;
  public
    { Public declarations }
    property Regla[Index: Integer]: TReglaAcceso read GetRegla;
    function GetIndex(const iCodigo: TCodigoRegla): Integer;
    function AddRegla: TReglaAcceso;
    function Evaluar(Checada: TChecadaAcceso): Boolean;
    procedure Init;
  end;

  TListaReglasCafe = class(TListaReglas)
  private
    { Private declarations }
    FReply: String;
    function GetRegla(Index: Integer): TReglaCafe;
    procedure PreparaEvaluadores;
  protected
    { Protected declarations }
    procedure Clear; override;
    procedure Delete(const Index: Integer); override;
  public
    { Public declarations }
    property Regla[Index: Integer]: TReglaCafe read GetRegla;
    function GetIndex(const iCodigo: TCodigoRegla): Integer;
    function Evaluar(Checada: TChecadaComida): Boolean;
    function AddRegla: TReglaCafe;
    procedure EvaluarMsgExito(Checada: TChecadaComida);
    procedure Init(const sMsgExito: String);
  end;

  TCafeteria = class(TObject)
  private
    { Private declarations }
    oCreator        : TZetaCreator;
    oZetaProvider   : TdmZetaServerProvider;
    FPeriodo        : TZetaCursor;
    FComida1        : TZetaCursor;
    FComida2        : TZetaCursor;
    FComida3        : TZetaCursor;
    FComida4        : TZetaCursor;
    FComida5        : TZetaCursor;
    FAcceso1        : TZetaCursor;
    FLeeEmpleado    : TZetaCursorLocate;
    FLeeInvitador   : TZetaCursorLocate;
    FLeeInvitaciones: TZetaCursorLocate;
    FLeeComidas     : TZetaCursorLocate;
    FLeeFotoEmp     : TZetaCursorLocate;
    {$IFNDEF INTERBASE}
    FObtieneHuellas: TZetaCursorLocate; // BIOMETRICO
    {$ENDIF}
    FEmpleadoDatos     : TZetaCursor;
    FEmpleadoStatus    : TZetaCursor;
    FHorarioDatos      : TZetaCursor;
    FAccessCount       : TZetaCursor;
    FUsaEmpleado       : Boolean;
    FInicial           : TDate;
    FFinal             : TDate;
    FFecha             : TDate;
    FFechaInit         : TDate;
    FCodigo            : TCodigo;
    FNombre            : String;
    FAliasName         : String;
    FUserName          : String;
    FPassword          : String;
    FDatos             : String;
    FDigito            : TDigito;
    FMsgExito          : String;
    FAgregarConError   : Boolean;
    FTipoPeriodo       : eTipoPeriodo;
    FReglasCafe        : TListaReglasCafe;
    FReglasAcceso      : TListaReglasAcceso;
    FChecadaAcceso     : TChecadaAcceso;
    FChecadaComida     : TChecadaComida;
    FRitmos            : TRitmos;
    FQueries           : TCommonQueries;
    FUsaAccesos        : Boolean;
    FUsaCafeteria      : Boolean;
    FSupervisorNivel   : Integer;
    FSupervisorNombre  : String;
    FOutput            : TDataset;
    FCuantosCafeteria  : Integer;
    FCuantosAccesos    : Integer;
    FListaDispCafeteria: TStringList;
    FListaDispAccesos  : TStringList;
    {$IFDEF BOSE}
    FBitacora: TZetaCursor;
    {$ENDIF}
    function AutorizaEmpleado: Boolean;
    function AutorizaInvitacion: Boolean;
    function GetFechaInicial: TDate;
    function GetFechaFinal: TDate;
    function GetStatusEmpleado(const iEmpleado: TNumEmp; const dReferencia: TDate;
      var Incidencia: TCodigo): eStatusEmpleado;
    function GetUsaCafeteria: Boolean;
    function PuedeEvaluarHorario(const dFecha: TDate): Boolean;
    procedure AgregaBlob(const eTipo: eTipoKind; const sLetrero: String; const oData: TBlobData);
    procedure AgregaDato(const eTipo: eTipoKind; const sLetrero, sValor: String);
    procedure AgregaEntero(const eTipo: eTipoKind; const sLetrero: String; const iValor: Integer);
    procedure AgregaFlotante(const eTipo: eTipoKind; const sLetrero: String;
      const rValor: Extended);
    procedure AgregaReglaEvaluacion(const sLetrero, sValor: String; const eSeveridad: eTipoBitacora;
      const lIsOk: Boolean);
    procedure AgregaReglaInformativa(const sLetrero, sValor: String);
    procedure BuscaPeriodo;
    procedure PreparaQueries;
    procedure PreparaRitmos;
    procedure DesPreparaQueries;
    procedure DesPreparaRitmos;
    procedure PreparaParametrosCafeteria;
    procedure PreparaParametrosAccesos;
    procedure GetDatosEmpleado(const iEmpleado: TNumEmp);
    procedure SetChecadaAcceso(Value: TChecadaAcceso);
    procedure SetChecadaComida(Value: TChecadaComida);
    procedure InitDatosComida(Checada: TChecada);
  protected
    { Protected declarations }
    property FechaInicial : TDate read GetFechaInicial;
    property FechaFinal   : TDate read GetFechaFinal;
    property PeriodoInicio: TDate read FInicial;
    property PeriodoFin   : TDate read FFinal;
    property ReglasCafe   : TListaReglasCafe read FReglasCafe;
    property ReglasAcceso : TListaReglasAcceso read FReglasAcceso;
  public
    { Public declarations }
    property Codigo            : TCodigo read FCodigo write FCodigo;
    property ChecadaAcceso     : TChecadaAcceso read FChecadaAcceso write SetChecadaAcceso;
    property ChecadaComida     : TChecadaComida read FChecadaComida write SetChecadaComida;
    property Creator           : TZetaCreator read oCreator;
    property Nombre            : String read FNombre write FNombre;
    property AliasName         : String read FAliasName write FAliasName;
    property UserName          : String read FUserName write FUserName;
    property Password          : String read FPassword write FPassword;
    property Datos             : String read FDatos write FDatos;
    property AgregarConError   : Boolean read FAgregarConError write FAgregarConError;
    property Output            : TDataset read FOutput write FOutput;
    property Digito            : TDigito read FDigito write FDigito;
    property MsgExito          : String read FMsgExito write FMsgExito;
    property TipoPeriodo       : eTipoPeriodo read FTipoPeriodo write FTipoPeriodo;
    property UsaCafeteria      : Boolean read GetUsaCafeteria write FUsaCafeteria;
    property UsaAccesos        : Boolean read FUsaAccesos write FUsaAccesos;
    property CuantosCafeteria  : Integer read FCuantosCafeteria write FCuantosCafeteria;
    property CuantosAccesos    : Integer read FCuantosAccesos write FCuantosAccesos;
    property ListaDispCafeteria: TStringList read FListaDispCafeteria write FListaDispCafeteria;
    property ListaDispAccesos  : TStringList read FListaDispAccesos write FListaDispAccesos;
    constructor Create;
    destructor Destroy; override;
    function GetDatasetEmpleado: TZetaCursorLocate;
    function GetDatasetHistorial: TZetaCursorLocate;
    function GetDatasetFotoEmp: TZetaCursorLocate;
    function AutorizaComida: Boolean;
    procedure AutorizaAcceso(Dataset: TDataset; var lExisteEmp: Boolean);
    procedure BuildDatasetsCafe(const lComidas, lMuestraFoto: Boolean);
    procedure BuildDetalles(const dInicial, dFinal: TDateTime; const sTipos: String;
      const sReloj: TReloj; Dataset: TDataset);
    procedure BuildTotales(const dInicial, dFinal: TDateTime; const sTipos: String;
      const sReloj: TReloj; Dataset: TDataset);
    procedure Close;
    procedure EscribeAcceso;
    procedure EscribeComida;
    {$IFDEF BOSE}
    procedure EscribeBitacoraError(const iEmpleado, iRegla, iSegundos, iStatus: Integer;
      const dFecha: TDate; const sHora, sEstacion, sMensaje: String; iTipo: Integer = 0);
    {$ENDIF}
    procedure Init(CallBack: TCallBack);

    procedure ReconectarEmpresa;

    procedure PrepararLeeEmpleado;
    procedure PrepararLeeInvitador;
    procedure PrepararLeeComidas;
    procedure PrepararLeeFotoEmp;
    procedure PrepararLeeInvitaciones;
    procedure PrepararObtieneHuellas;

    procedure PrepararEmpleadoDatos;
    procedure PrepararEmpleadoStatus;
    procedure PrepararHorarioDatos;
    procedure PrepararAccessCount;

    procedure PrepararComida1;
    procedure PrepararComida2;
    procedure PrepararComida3;
    procedure PrepararComida4;
    procedure PrepararComida5;

    procedure PrepararAcceso1;
    procedure PrepararPeriodo;
{$ifdef BOSE}
    procedure PrepararBitacora;
{$endif}

    procedure SetTipoChecadaAcceso;
    function CafeteriaValida(const sReloj: String): Boolean;
    function AccesosValido(const sCaseta: String): Boolean;
    function TieneDigito: Boolean;
    {$IFNDEF INTERBASE}
    function GetDatasetHuellas: TZetaCursorLocate; // BIOMETRICO
    {$ENDIF}
  end;

  {$IFNDEF INTERBASE}

// BIOMETRICO
  THuella = class(TObject)
  private
    { Private declarations }
    FNumeroBiometrico: Integer;
    FEmpresa         : String;
    FEmpleado        : Integer;
    FIndice          : Integer;
    FHuella          : String;
    FInvitador       : Integer;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property NumeroBiometrico: Integer read FNumeroBiometrico write FNumeroBiometrico;
    property Empresa         : String read FEmpresa write FEmpresa;
    property Empleado        : Integer read FEmpleado write FEmpleado;
    property Indice          : Integer read FIndice write FIndice;
    property Huella          : String read FHuella write FHuella;
    property Invitador       : Integer read FInvitador write FInvitador;
    procedure Init;
  end;
  {$ENDIF}

implementation

uses
  DBClient, ZGlobalTress, ZetaEntidad, ZetaTipoEntidad, ZetaServerTools, ZetaCommonTools, CafeteraUtils;

const
  K_GAFETE_LENGTH = 11;
  K_LEN_CODIGO_IV = 3;
  K_ANCHO_VALUE   = 40;
  K_ANCHO_LETRERO = 40;

  R_RECHAZO                        = 'RECHAZADA';
  R_INVITADOR_NO_EXISTE            = 'Invitador No Existe';
  R_INVITADOR_SUSPENDIDO           = 'Invitador Suspendido';
  R_INVITACION_RECHAZADA           = 'No Autoriza Invitaci�n';
  R_EMPLEADO_NO_EXISTE             = 'Empleado No Existe';
  R_EMPLEADO_NO_ENCONTRADO         = 'Empleado Inexistente';
  R_EMPLEADO_ES_BAJA               = 'Empleado Es Baja (%s)';
  R_EMPLEADO_CREDENCIAL_INCORRECTA = 'Letra Credencial Incorrecta';
  // R_TIPO_COMIDA_INVALIDO           = 'Tipo de Comida INVALIDO';
  R_TIPO_COMIDA_INVALIDO           = 'Tipo de Comida NO VALIDO';
  R_ACEPTADA                       = 'PROVECHO';
  R_RECHAZADA                      = 'COMIDA RECHAZADA';
  R_INDETERMINADA                  = 'INDETERMINADA';
  R_EMPRESA_INVALIDA               = 'Empresa Invalida';
  R_COMIDA_YA_EXISTE               = 'Ya Est� Registrada Su Comida';
  R_INVITACION_YA_EXISTE           = 'Ya Est� Registrada Su Invitaci�n';
  R_ACCESO_YA_EXISTE               = 'Checada Repetida';
  R_CANCELACION                    = 'Cancelaci�n Aceptada';
  K_DIAS_EVALUAR_HORARIO           = 3;
  R_BIENVENIDO                     = 'BIENVENIDO';

type
  eSQL = (Q_BUSCA_PERIODO, Q_CAFE_AGREGA_COMIDA, Q_CAFE_AGREGA_INVITACION, Q_CAFE_LEE_REGLAS,
    Q_CAFE_AUTORIZA_COMIDA, Q_CAFE_AUTORIZA_INVITACION, Q_CAFE_UPDATE_COMIDA, Q_GET_EMPLEADO,
    Q_GET_EMPLEADO_DATOS, Q_GET_EMPLEADO_STATUS, Q_GET_COMIDAS, Q_GET_INVITADOR, Q_GET_INVITACIONES,
    Q_TOT_COMIDAS, Q_TOT_INVITACIONES, Q_DETALLE_COMIDAS, Q_ACCESO_AGREGA, Q_HORARIO_LEE,
    Q_ACCESS_COUNT, Q_ACCESS_LEE_REGLAS, Q_LEE_FOTO_EMP
    {$IFDEF BOSE}
    , Q_AGREGA_BITACORA
    {$ENDIF}
    {$IFNDEF INTERBASE}
    , Q_LEE_HUELLAS // BIOMETRICO
    {$ENDIF}
    , Q_EMP_ID, Q_EMP_BIO
    );

function GetSQLScript(const eSQLNumero: eSQL): String;
begin
  case eSQLNumero of
    {$IFDEF QUINCENALES}
    Q_BUSCA_PERIODO:
      Result := 'select PE_ASI_INI, PE_ASI_FIN, PE_NUMERO, PE_YEAR from PERIODO where ' +
        '( PE_YEAR = :Year ) and ' + '( PE_TIPO = :Tipo ) and ' + ' ( PE_ASI_INI <= :Fecha ) and ' +
        '( PE_ASI_FIN >= :Fecha1 )';
    {$ELSE}
    Q_BUSCA_PERIODO:
      Result := 'select PE_FEC_INI, PE_FEC_FIN, PE_NUMERO, PE_YEAR from PERIODO where ' +
        '( PE_YEAR = :Year ) and ' + '( PE_TIPO = :Tipo ) and ' + ' ( PE_FEC_INI <= :Fecha ) and ' +
        '( PE_FEC_FIN >= :Fecha1 )';
    {$ENDIF}
    Q_CAFE_AGREGA_COMIDA:
      Result := 'insert into CAF_COME (' + 'CB_CODIGO,' + 'CF_FECHA,' + 'CF_HORA,' + 'CF_TIPO,' +
        'CF_COMIDAS,' + 'CF_RELOJ,' + 'CL_CODIGO,' + 'CF_REG_EXT, CF_OFFLINE ) values (' + ':CB_CODIGO,' +
        ':CF_FECHA,' + ':CF_HORA,' + ':CF_TIPO,' + ':CF_COMIDAS,' + ':CF_RELOJ,' + ':CL_CODIGO,' +
        ':CF_REG_EXT, :CF_OFFLINE )';
    Q_CAFE_AGREGA_INVITACION:
      Result := 'insert into CAF_INV (' + 'IV_CODIGO,' + 'CF_FECHA,' + 'CF_HORA,' + 'CF_TIPO,' +
        'CF_COMIDAS,' + 'CF_RELOJ,' + 'CF_REG_EXT, CF_OFFLINE ) values (' + ':IV_CODIGO,' + ':CF_FECHA,' +
        ':CF_HORA,' + ':CF_TIPO,' + ':CF_COMIDAS,' + ':CF_RELOJ,' + ':CF_REG_EXT, :CF_OFFLINE )';
    Q_CAFE_LEE_REGLAS:
      Result := 'select C.CL_CODIGO, ' + 'C.CL_LETRERO, ' + 'C.CL_TIPOS, ' + 'C.CL_LIMITE, ' +
        'C.CL_EXTRAS, ' + 'C.CL_TOTAL, ' + 'C.CL_FILTRO, ' +
        '( select Q.QU_FILTRO from QUERYS Q where ( Q.QU_CODIGO = C.CL_QUERY ) ) as QUERY ' +
        'from CAFREGLA C where C.CL_ACTIVO = ' + EntreComillas(K_GLOBAL_SI);
    {$IFDEF MULT_TIPO_COMIDA}
    Q_CAFE_AUTORIZA_COMIDA:
      Result := 'select AUTORIZACION, FECHABAJA from SP_CAFE_AUTORIZA_COMIDA( :EMPLEADO, :FECHA, :HORA, :CREDENCIAL, :TIPOCOMIDA )';
    {$ELSE}
    Q_CAFE_AUTORIZA_COMIDA:
      Result := 'select AUTORIZACION, FECHABAJA from SP_CAFE_AUTORIZA_COMIDA( :EMPLEADO, :FECHA, :HORA, :CREDENCIAL )';
    {$ENDIF}
    Q_CAFE_AUTORIZA_INVITACION:
      Result := 'select AUTORIZACION, EMPLEADO from SP_CAFE_AUTORIZA_INVITACION( :INVITADOR, :FECHA, :HORA, :TIPOCOMIDA, :NUMCOMIDAS )';
    Q_CAFE_UPDATE_COMIDA:
      Result := 'update CAF_COME set CF_COMIDAS = CF_COMIDAS + :NumComidas , CF_OFFLINE = :CF_OFFLINE where ' +
        '( CB_CODIGO = :Empleado ) and ' + '( CF_FECHA = :Fecha ) and ' + '( CF_HORA = :Hora )  ' +
      {$IFDEF MULT_TIPO_COMIDA}
        'and ( CF_TIPO = :TipoComida )';
    {$ENDIF}
          { Q_GET_EMPLEADO: Result := 'select C.CB_CODIGO, ' + K_PRETTYNAME + ' PrettyName, IMAGEN.IM_BLOB Foto, '+
                                    'C.CB_TURNO, TURNO.TU_DESCRIP, 0 EMPLEADO from COLABORA C '+
                                    'left outer join TURNO on ( TURNO.TU_CODIGO = C.CB_TURNO ) '+
                                    'left outer join IMAGEN on ( IMAGEN.CB_CODIGO = C.CB_CODIGO ) and ( IMAGEN.IM_TIPO = ''%s'' ) ' +
                                    'where ( C.CB_CODIGO = :Empleado )'; }

    Q_GET_EMPLEADO:
      Result := 'select C.CB_CODIGO, ' + K_PRETTYNAME + ' PrettyName, ' +
        'C.CB_TURNO, TURNO.TU_DESCRIP, 0 EMPLEADO from COLABORA C ' +
        'left outer join TURNO on ( TURNO.TU_CODIGO = C.CB_TURNO ) ' +
        'where ( C.CB_CODIGO = :Empleado )';

    Q_GET_EMPLEADO_DATOS:
      Result := 'select C.CB_CODIGO, ' + K_PRETTYNAME +
        ' PrettyName, C.CB_CREDENC, I.IM_BLOB Foto, ' +
        'A.CB_CODIGO as EXISTE, A.HO_CODIGO HO_CODIGO, A.AU_STATUS AU_STATUS, ' +
      {$IFDEF INTERBASE}
        '( select RESULTADO from AUTORIZACIONES_SELECT( C.CB_CODIGO, A.AU_FECHA, 7 ) ) HRS_EXTRAS,'
        + '( select RESULTADO from AUTORIZACIONES_SELECT( C.CB_CODIGO, A.AU_FECHA, 8 ) ) DESCANSO, '
        + '( select RESULTADO from AUTORIZACIONES_SELECT( C.CB_CODIGO, A.AU_FECHA, 5 ) ) PER_CG, ' +
        '( select RESULTADO from AUTORIZACIONES_SELECT( C.CB_CODIGO, A.AU_FECHA, 6 ) ) PER_SG, ' +
        '( select RESULTADO from AUTORIZACIONES_SELECT( C.CB_CODIGO, A.AU_FECHA, 9 ) ) PER_CG_ENT, '
        + '( select RESULTADO from AUTORIZACIONES_SELECT( C.CB_CODIGO, A.AU_FECHA, 10 ) ) PER_SG_ENT, '
        +
      {$ENDIF}
      {$IFDEF MSSQL}
        'dbo.AUTORIZACIONES_SELECT_HORAS( C.CB_CODIGO, A.AU_FECHA, 7 ) HRS_EXTRAS,' +
        'dbo.AUTORIZACIONES_SELECT_HORAS( C.CB_CODIGO, A.AU_FECHA, 8 ) DESCANSO,' +
        'dbo.AUTORIZACIONES_SELECT_HORAS( C.CB_CODIGO, A.AU_FECHA, 5 ) PER_CG,' +
        'dbo.AUTORIZACIONES_SELECT_HORAS( C.CB_CODIGO, A.AU_FECHA, 6 ) PER_SG,' +
        'dbo.AUTORIZACIONES_SELECT_HORAS( C.CB_CODIGO, A.AU_FECHA, 9 ) PER_CG_ENT,' +
        'dbo.AUTORIZACIONES_SELECT_HORAS( C.CB_CODIGO, A.AU_FECHA, 10 ) PER_SG_ENT,' +
      {$ENDIF}
        'C.CB_TURNO, T.TU_DESCRIP, N.TB_ELEMENT SUP_NAME from COLABORA C ' +
        'left join AUSENCIA A on ( A.CB_CODIGO = C.CB_CODIGO ) and ( A.AU_FECHA = :Fecha ) ' +
        'left outer join TURNO T on ( T.TU_CODIGO = C.CB_TURNO ) ' +
        'left outer join NIVEL%0:d N on ( N.TB_CODIGO = C.CB_NIVEL%0:d ) ' +
        'left outer join IMAGEN I on ( I.CB_CODIGO = C.CB_CODIGO ) and ( I.IM_TIPO = ''%1:s'' ) ' +
        'where ( C.CB_CODIGO = :Empleado )';
    {$IFDEF INTERBASE}
    Q_GET_EMPLEADO_STATUS:
      Result := 'select RESULTADO, INCIDENCIA from SP_STATUS_INCIDENCIA( :Fecha, :Empleado )';
    {$ENDIF}
    {$IFDEF MSSQL}
    Q_GET_EMPLEADO_STATUS:
      Result := 'execute procedure SP_STATUS_INCIDENCIA( :Fecha, :Empleado, :Resultado, :Incidencia )';
    {$ENDIF}
    Q_GET_COMIDAS:
      Result := 'select CB_CODIGO, CF_FECHA, CF_HORA, CF_TIPO, ' +
        'CF_COMIDAS, CF_EXTRAS, CF_RELOJ, US_CODIGO, ' +
        'CL_CODIGO, CF_REG_EXT from CAF_COME where ' + '( CB_CODIGO = :Empleado ) and ' +
        '( CF_FECHA >= :Inicio ) and ' + '( CF_FECHA <= :Fin ) ' +
        'order by CF_FECHA, CF_HORA'{$IFDEF MULT_TIPO_COMIDA} + ' ,CF_TIPO'{$ENDIF};
    Q_GET_INVITADOR:
      Result := 'select I.IV_CODIGO CB_CODIGO, I.IV_NOMBRE PrettyName, 0 Foto, ' +
        ''' '' CB_TURNO, '' '' TU_DESCRIP, I.CB_CODIGO EMPLEADO' +
      {$IFNDEF INTERBASE} ', IV_ID_BIO, IV_ID_GPO ' + {$ENDIF}
        ' from INVITA I where ( I.IV_CODIGO = :Invitador )';
    Q_GET_INVITACIONES:
      Result := 'select IV_CODIGO CB_CODIGO, CF_FECHA, CF_HORA, CF_TIPO, ' +
        'CF_COMIDAS, CF_EXTRAS, CF_RELOJ, US_CODIGO, ' + 'CL_CODIGO, CF_REG_EXT from CAF_INV where '
        + '( IV_CODIGO = :Invitador ) and ' + '( CF_FECHA >= :Inicio ) and ' +
        '( CF_FECHA <= :Fin ) ' + 'order by CF_FECHA, CF_HORA'{$IFDEF MULT_TIPO_COMIDA} +
        ' ,CF_TIPO'{$ENDIF};
    Q_TOT_COMIDAS:
      Result := 'select CF_TIPO, SUM( CF_COMIDAS ) CF_COMIDAS, SUM( CF_EXTRAS ) CF_EXTRAS ' +
        'from CAF_COME where ' + '( CF_RELOJ = :Reloj ) and ' + '( CF_FECHA >= :FechaInicial ) and '
        + '( CF_FECHA <= :FechaFinal ) and ' + '( CF_TIPO in ( %s ) ) ' + 'group by CF_TIPO';
    Q_TOT_INVITACIONES:
      Result := 'select CF_TIPO, SUM( CF_COMIDAS ) CF_COMIDAS, SUM( CF_EXTRAS ) CF_EXTRAS ' +
        'from CAF_INV where ' + '( CF_RELOJ = :Reloj ) and ' + '( CF_FECHA >= :FechaInicial ) and '
        + '( CF_FECHA <= :FechaFinal ) and ' + '( CF_TIPO in ( %s ) ) ' + 'group by CF_TIPO';
    Q_DETALLE_COMIDAS:
      Result := 'select M.CB_CODIGO, M.CF_FECHA, M.CF_HORA, M.CF_COMIDAS, M.CF_EXTRAS, M.CF_TIPO, 0 AS CF_CLASE, '
        + 'CAST( ' + K_PRETTYNAME + ' as VARCHAR( 50 ) ) PRETTYNAME ' + 'from CAF_COME M ' +
        'left outer join COLABORA C on ( C.CB_CODIGO = M.CB_CODIGO ) ' +
        'where ( M.CF_FECHA >= :Inicial1 ) and ' + '( M.CF_FECHA <= :Final1 ) and ' +
        '( M.CF_RELOJ = :Reloj1 ) and ' + '( M.CF_TIPO in ( %s ) ) ' + 'union ' +
        'select I.IV_CODIGO, I.CF_FECHA, I.CF_HORA, I.CF_COMIDAS, I.CF_EXTRAS, I.CF_TIPO, 0 AS CF_CLASE, '
        + 'CAST( IV.IV_NOMBRE as VARCHAR( 50 ) ) PRETTYNAME ' + 'from CAF_INV I ' +
        'left outer join INVITA IV on ( IV.IV_CODIGO = I.IV_CODIGO ) ' +
        'where ( I.CF_FECHA >= :Inicial2 ) and ' + '( I.CF_FECHA <= :Final2 ) and ' +
        '( I.CF_RELOJ = :Reloj2 ) and ' + '( I.CF_TIPO in ( %s ) )';
    Q_ACCESO_AGREGA:
      Result := 'insert into ACCESLOG( CB_CODIGO, AL_FECHA, AL_HORA, AL_ENTRADA, AL_OK_SIST, AE_CODIGO, AL_CASETA ) values ( :CB_CODIGO, :AL_FECHA, :AL_HORA, :AL_ENTRADA, :AL_OK_SIST, :AE_CODIGO, :AL_CASETA )';
    Q_HORARIO_LEE:
      Result := 'select HO_DESCRIP, HO_TIPO, HO_INTIME, HO_IN_TEMP, HO_LASTOUT, HO_OUTTIME ' +
        'from HORARIO where ( HO_CODIGO = :Codigo )';
    {$IFDEF INTERBASE}
    Q_ACCESS_COUNT:
      Result := 'select RESULTADO from EMPLEADO_ADENTRO( :Empleado, :Fecha )';
    {$ENDIF}
    {$IFDEF MSSQL}
    Q_ACCESS_COUNT:
      Result := 'select RESULTADO = dbo.EMPLEADO_ADENTRO( :Empleado, :Fecha )';
    {$ENDIF}
{
          Q_ACCESS_COUNT: Result := 'select COUNT(*) ACCESOS from ACCESLOG where '+
                                    '( CB_CODIGO = :Empleado ) and '+
                                    '( AL_FECHA = :Fecha ) and '+
                                    '( ( ( US_CODIGO = 0 ) and ( AL_OK_SIST = ''%0:s'' ) ) or ( ( US_CODIGO <> 0 ) and ( AL_OK_MAN = ''%0:s'' ) ) )';
}
    Q_ACCESS_LEE_REGLAS:
      Result := 'select A.AE_CODIGO, A.AE_TIPO, A.AE_LETRERO, A.AE_FORMULA, A.AE_SEVERI, A.AE_FILTRO, '
        + '( select Q.QU_FILTRO from QUERYS Q where ( Q.QU_CODIGO = A.QU_CODIGO ) ) as QUERY ' +
        'from ACCREGLA A where ' + '( A.AE_ACTIVO = ''%s'' ) ' +
        'order by A.AE_TIPO, A.AE_SEVERI desc, A.AE_CODIGO';

    Q_LEE_FOTO_EMP:
      Result := 'select IM_BLOB  FOTO from IMAGEN ' +
        'where ( CB_CODIGO = :Empleado ) and ( IM_TIPO = %s )';
    {$IFDEF BOSE}
    Q_AGREGA_BITACORA:
      Result := 'Insert into Z_BIT_CAFE ' +
        '(CB_CODIGO, BC_FECHA, BC_HORA, BC_ESTACIO, BC_TIPO, BC_REGLA, BC_TIEMPO, BC_STATUS, BC_MENSAJE) '
        + 'Values' +
        '(:CB_CODIGO, :BC_FECHA, :BC_HORA, :BC_ESTACIO, :BC_TIPO, :BC_REGLA, :BC_TIEMPO, :BC_STATUS, :BC_MENSAJE)';
    {$ENDIF}
    {$IFNDEF INTERBASE}
    Q_LEE_HUELLAS:
      Result := 'SELECT ID_NUMERO, CM_CODIGO, CB_CODIGO, HU_INDICE, HU_HUELLA, IV_CODIGO FROM V_HUELLAS';
      // BIOMETRICO
    {$ENDIF}

    Q_EMP_ID:
      Result := 'select A.CB_CODIGO, A.IV_CODIGO, A.CM_CODIGO, B.CM_DIGITO ' + 'from EMP_ID A ' +
        'left outer join COMPANY B on ( B.CM_CODIGO = A.CM_CODIGO ) ' + 'where ( A.ID_NUMERO = :GAFETE ) ' +
        'and ( B.CM_DIGITO <> '''' ) and ( B.CM_DIGITO is not NULL )';

    Q_EMP_BIO:
      Result := 'SELECT E.CB_CODIGO, E.IV_CODIGO, E.CM_CODIGO, C.CM_DIGITO, E.ID_NUMERO ' +
        'FROM EMP_BIO AS E left outer join COMPANY AS C ON E.CM_CODIGO = C.CM_CODIGO ' +
        'WHERE (E.ID_NUMERO = :GAFETE) AND (C.CM_DIGITO <> '''') AND (C.CM_DIGITO IS NOT NULL)';

  end;
end;

function MakeFormula(const sExpresion: String): String;
const
  K_TOKEN_FORMULA = '@';
begin
  if ZetaCommonTools.StrLleno(sExpresion) and (Pos(K_TOKEN_FORMULA, sExpresion) <> 1) then
    Result := K_TOKEN_FORMULA + sExpresion
  else
    Result := sExpresion;
end;

function EsUnaFormula(var sExpresion: String): Boolean; // Si Expr es F�rmula, empieza con @ //
begin
  if (Pos('@', sExpresion) = 1) then begin
    sExpresion := Copy(sExpresion, 2, MAXINT);
    Result := True;
  end
  else
    Result := False;
end;

function EsEntero(const sValor: String): Boolean;
begin
  try
    StrToInt(Trim(sValor));
    Result := True;
  except
    Result := False;
  end;
end;

procedure ValidaError(EvResultado: TQREvResult; const sMensaje: String);
var
  ErrorNumero                     : Integer;
  ErrorDescrip, ErrorMsg, ErrorExp: String;
begin
  if HuboErrorEvaluador(EvResultado, ErrorNumero, ErrorDescrip, ErrorMsg, ErrorExp) then begin
    raise EErrorEvaluador.Create(sMensaje + CR_LF + 'Error N�mero: ' + IntToStr(ErrorNumero) + CR_LF
        + ErrorDescrip + CR_LF + ErrorMsg + CR_LF + 'En la Expresi�n: ' + ErrorExp);
  end;
end;

{ ******** TChecada ******** }

procedure TChecada.SetFecha(const Value: TDate);
begin
  FFecha := Trunc(Value);
end;

procedure TChecada.SetRespuesta(const Value: String);
begin
  FRespuesta := Value;
end;

procedure TChecada.SetError(const sValue: String);
begin
  Respuesta := 'ERROR: ' + sValue;
end;

constructor TChecada.Create;
begin
  inherited;
  FProviderComparte := TdmZetaServerProvider.Create(nil);
  FProviderComparte.SetReadUncommited;

end;

destructor TChecada.Destroy;
begin
  FreeAndNil( FProviderComparte );
  inherited;
end;

function TChecada.EmpresaOK(Company: TCafeteria): Boolean;
begin
  Result := (Company.Digito = Self.Empresa);
end;

procedure TChecada.Init;
begin

  FProviderComparte.EmpresaActiva := FProviderComparte.Comparte;

  FEmpresa := '0';
  FEmpleado := 0;
  FEsManual := True;
  FCredencial := ' ';
  FFecha := Now;
  FHora := '';
  Respuesta := R_INDETERMINADA;
end;

procedure TChecada.SetFechaHora(const dValue: TDateTime);
begin
  Fecha := dValue;
  Hora := FormatDateTime(K_TIME_FORMAT, dValue);
end;

function TChecada.GetCodigo: String;
const
  K_LEN_CODIGO_EMP = 9;
begin
  Result := ZetaCommonTools.PadLCar(IntToStr(QueryEmpID.FieldByName('CB_CODIGO').AsInteger),
    K_LEN_CODIGO_EMP, '0');
end;

function TChecada.GetGafeteTress(var sGafete: String): Boolean;
begin

  with FProviderComparte do begin
    if ( Assigned( FQueryEmpID ) )  then
      FreeAndNil( FQueryEmpID );

    FQueryEmpID := CreateQuery;
    PreparaQuery(FQueryEmpID, GetSQLScript(Q_EMP_ID));
  end;


  with QueryEmpID do begin
    Active := False;
    Provider.ParamAsVarChar(FQueryEmpID, 'GAFETE', sGafete, K_ANCHO_DESCRIPCION);
    Active := True;
    Result := not IsEmpty;
    if Result then begin
      sGafete := Format('%1.1s%s ', [FieldByName('CM_DIGITO').AsString, GetCodigo]);
    end;
    Active := False;
  end;
end;

{$IFNDEF INTERBASE}

// BIOMETRICO
function TChecada.GetCodigoBio: String;
const
  K_LEN_CODIGO_EMP = 9;
begin
  Result := ZetaCommonTools.PadLCar(IntToStr(QueryEmpBio.FieldByName('CB_CODIGO').AsInteger),
    K_LEN_CODIGO_EMP, '0');
end;

// BIOMETRICO
function TChecada.GetBiometricoTress(var sGafete: String): Boolean;
{ var
   sCodigo : string; }
begin

  //FreeAndNil( FQueryEmpBio );

  with FProviderComparte do begin
   if ( Assigned( FQueryEmpBio ) )  then
      FreeAndNil( FQueryEmpBio );

    FQueryEmpBio := CreateQuery;
    PreparaQuery(FQueryEmpBio, GetSQLScript(Q_EMP_BIO));
  end;

  with QueryEmpBio do begin
    Active := False;
    Provider.ParamAsVarChar(FQueryEmpBio, 'GAFETE', sGafete, K_ANCHO_DESCRIPCION);
    Active := True;
    Result := not IsEmpty;
    if Result then begin
               // sCodigo := K_PREFIJO_INVITADOR + ZetaCommonTools.PadLCar( IntToStr( FieldByName( 'IV_CODIGO' ).AsInteger ), K_LEN_CODIGO_IV, '0' );
      sGafete := Format('%1.1s%s ', [FieldByName('CM_DIGITO').AsString, GetCodigoBio]);
    end;
    Active := False;
  end;
end;
{$ENDIF}

function TChecada.SetGafete(var sGafete: String): Boolean;
begin
  case TipoGafete of
    egProximidad:
      Result := GetGafeteTress(sGafete);
    {$IFNDEF INTERBASE}
          // BIOMETRICO
    egBiometrico:
      Result := GetBiometricoTress(sGafete);
    {$ENDIF}
    else
      Result := True;
  end;
  if Result then begin
    case Length(sGafete) of
      6:
        FEmpresa := '0';
      7: begin
          FEmpresa := sGafete[1];
          sGafete := Copy(sGafete, 2, 6);
        end;
      8 .. 11: begin
          FEmpresa := sGafete[1];
          sGafete := Copy(sGafete, 2, Length(sGafete) - 1);
        end;
      else begin
          Result := False;
          {$IFNDEF INTERBASE}
          if (TipoGafete = egBiometrico) then
            Respuesta := 'HUELLA INVALIDA'
          else
            {$ENDIF}
            Respuesta := 'CREDENCIAL INVALIDA';

        end;
    end;
    if Result then begin
      FCredencial := sGafete[Length(sGafete)];
    end;
  end
  else
    {$IFNDEF INTERBASE}
    if (TipoGafete = egBiometrico) then
      Respuesta := 'HUELLA INEXISTENTE'
    else
      {$ENDIF}
      Respuesta := 'TARJETA INEXISTENTE';
end;

function TChecada.CheckTipoGafete(const sToken, sData: String): Boolean;
begin
  Result := (Pos(sToken, sData) > 0);
  if Result then begin
    TipoGafete := egTress;

    if (Pos((sToken + TOKEN_PROXIMIDAD), sData) > 0) then
      TipoGafete := egProximidad;

    {$IFNDEF INTERBASE}
          // BIOMETRICO
    if (Pos((sToken + TOKEN_BIOMETRICO), sData) > 0) then
      TipoGafete := egBiometrico;
    {$ENDIF}
  end
  else
    // Respuesta := 'GAFETE INVALIDO';
    Respuesta := 'GAFETE NO VALIDO';
end;

function TChecada.SetEmpleado(var sGafete: String): Boolean;
begin
  sGafete := Copy(sGafete, 1, Length(sGafete) - 1);
  FEmpleado := ZetaCommonTools.StrAsInteger(sGafete);
  if (FEmpleado > 0) then
    Result := True
  else begin
    Result := False;
    // Respuesta := '# DE EMPLEADO INVALIDO';
    Respuesta := '# DE EMPLEADO NO VALIDO';
  end;
end;

{ ******** TChecadaAcceso ******** }

procedure TChecadaAcceso.Init;
begin
  inherited Init;
  FCaseta := VACIO;
  FTipo := chDesconocido;
  FRegla := 0;
  FPassed := False;
end;

function TChecadaAcceso.EmpresaOK(Company: TCafeteria): Boolean;
begin
  Result := inherited EmpresaOK(Company) and Company.UsaAccesos and Company.AccesosValido(Caseta);
end;

procedure TChecadaAcceso.SetRegla(const Value: TCodigoRegla);
begin
  FRegla := Value;
end;

function TChecadaAcceso.SetGafete(var sGafete: String): Boolean;
begin
  Result := inherited SetGafete(sGafete);
  if Result then begin
    Result := SetEmpleado(sGafete);
  end;
end;

function TChecadaAcceso.SetChecada(const sData: String; iYear: Word): Boolean;
var
  iStart : Integer;
  sGafete: String;
begin
  Init;
  Result := CheckTipoGafete(TOKEN_ACCESOS, sData);
  if Result then begin
    FCaseta := Copy(sData, 1, K_ANCHO_CODIGO);
    iStart := Pos(TOKEN_ACCESOS, sData);
    if TipoGafete = egProximidad then
      sGafete := QuitaCerosIzquierda(Copy(sData, iStart + 2, K_GAFETE_LENGTH - 1)) // Quitar la ~
    else
      sGafete := Copy(sData, iStart + 1, K_GAFETE_LENGTH);
    Result := SetGafete(sGafete);
    if Result then begin
      if (iYear = 0) then
        iYear := TheYear(Now);
      FFecha := EncodeDate(iYear, StrToInt(Copy(sData, (iStart + 12), 2)),
        StrToInt(Copy(sData, (iStart + 14), 2)));
      FHora := Copy(sData, (iStart + 16), 4);
      FTipo := eTipoChecadas(StrToInt(Copy(sData, (iStart + 20), 1)));
    end;
  end;
end;

procedure TChecadaAcceso.SetCaseta(const Value: TCodigo);
begin
// FCaseta := Copy( Value, 1, K_ANCHO_CODIGO );
  FCaseta := Copy(Value, 1, K_ANCHO_RELOJLINX);
end;

function TChecadaAcceso.TipoDesconocido: Boolean;
begin
  case Tipo of
    chEntrada:
      Result := False;
    chSalida:
      Result := False;
    else
      Result := True;
  end;
end;

function TChecadaAcceso.EsEntrada: Boolean;
begin
  case Tipo of
    chEntrada:
      Result := True;
    else
      Result := False;
  end;
end;

{ ************* TChecadaComida ********************* }

procedure TChecadaComida.Init;
begin
  inherited Init;
  FEsInvitacion := False;;
  FInvitador    := 0;
  FReloj        := '';
  FNumComidas   := 0;
  FTipoComida   := '1';
  FRegla        := 0;
  FExtras       := False;
end;

function TChecadaComida.EmpresaOK(Company: TCafeteria): Boolean;
begin
  Result := inherited EmpresaOK(Company) and Company.UsaCafeteria and Company.CafeteriaValida(Reloj);
end;

function TChecadaComida.GetCodigo: String;
begin



  with QueryEmpID do begin
    if (FieldByName('IV_CODIGO').AsInteger > 0) then { Es Invitador }
    begin
      Result := K_PREFIJO_INVITADOR + ZetaCommonTools.PadLCar
        (IntToStr(FieldByName('IV_CODIGO').AsInteger), K_LEN_CODIGO_IV, '0')
    end else begin
      Result := inherited GetCodigo;
    end;
  end;
end;

{$IFNDEF INTERBASE}

// BIOMETRICO
function TChecadaComida.GetCodigoBio: String;
begin
  with QueryEmpBio do begin
    if (FieldByName('IV_CODIGO').AsInteger > 0) then { Es Invitador }
      Result := K_PREFIJO_INVITADOR + ZetaCommonTools.PadLCar
        (IntToStr(FieldByName('IV_CODIGO').AsInteger), K_LEN_CODIGO_IV, '0')
    else
      Result := inherited GetCodigoBio;
  end;
end;
{$ENDIF}

function TChecadaComida.SetGafete(var sGafete: String): Boolean;
begin
  Result := inherited SetGafete(sGafete);
  if Result then begin
    FEsInvitacion := (Copy(sGafete, 1, Length(K_PREFIJO_INVITADOR)) = K_PREFIJO_INVITADOR);
    if FEsInvitacion then begin
      FEmpleado := 0;
      sGafete := Copy(sGafete, 3, K_LEN_CODIGO_IV);
      FInvitador := ZetaCommonTools.StrAsInteger(sGafete);
      if (FInvitador <= 0) then begin
        Result := False;
        // Respuesta := '# DE INVITADOR INVALIDO';
        Respuesta := '# DE INVITADOR NO VALIDO';
      end;
    end else begin
      FInvitador := 0;
      Result := SetEmpleado(sGafete);
    end;
  end;
end;

function TChecadaComida.SetChecada(const sData: String; iYear: Word): Boolean;
var
  iStart : Integer;
  sGafete: String;
begin
  Init;
  Result := CheckTipoGafete(TOKEN_CAFETERIA, sData);
  if Result then begin
    FReloj := Copy(sData, 1, K_ANCHO_RELOJLINX);
    iStart := Pos(TOKEN_CAFETERIA, sData);
    case TipoGafete of
      egProximidad, egBiometrico: // BIOMETRICO
        begin
          iStart := Pos(TOKEN_CAFETERIA, sData);
          sGafete := QuitaCerosIzquierda(Copy(sData, iStart + 2, K_GAFETE_LENGTH - 1));
        end;
      else
        sGafete := Copy(sData, iStart + 1, K_GAFETE_LENGTH);
    end;

    Result := SetGafete(sGafete);
    if Result then begin
      if (iYear = 0) then
        iYear := TheYear(Now);
      FFecha := EncodeDate(iYear, StrToInt(Copy(sData, (iStart + 12), 2)),
        StrToInt(Copy(sData, (iStart + 14), 2)));
      FHora := Copy(sData, (iStart + 16), 4);
      FNumComidas := StrToInt(Copy(sData, (iStart + 20), 2));
      FTipoComida := sData[iStart + 22];
      FRegla := 0;
      FExtras := False;
      if (MealType in [1, 2, 3, 4, 5, 6, 7, 8, 9]) then
        Respuesta := R_INDETERMINADA
      else begin
        Respuesta := R_TIPO_COMIDA_INVALIDO;
        Result := False;
      end;
    end;
  end;
end;

{$IFDEF FALSE}
// La checada de Cafeter�a es algo as�:
//
// GAFETE TRESS:
// =============
// 1234567890#E123456789CMMDDHHNN99T
//
// donde
//
// Concepto     Ancho Descripci�n
// ----------   ----- -----------------------------------------------------------
// 1234567890   10	   Informaci�n del reloj donde se hizo la checada (no se usa)
// # 	     1	   Token para identificar la checada como que es Cafeter�a
// E 	     1	   Caracter que identifica a la empresa (ver globales de empresa)
// 123456789    9	   N�mero de empleado � es el c�digo de invitador (en cuyo caso empieza con la letra I )
// C	     1	   Letra de credencial del empleado
// MM	     2	   Mes de la checada (00 al 12)
// DD	     2	   D�a de la checada (01 al 31, dependiendo del mes )
// HH	     2	   Hora de la checada ( 00 a 23)
// NN	     2	   Minutos de la checada ( 00 a 59 )
// 99	     2	   Comidas pedidas
// T	     1	   Tipo de comida pedida ( 1,2,3,4,5,6,7,8,9)
//
//
// GAFETE PROXIMIDAD:
// ==================
// 1234567890#~1234567890MMDDHHNN99T
//
// donde
//
// Concepto     Ancho Descripci�n
// ----------   ----- -----------------------------------------------------------
// 1234567890   10	   Informaci�n del reloj donde se hizo la checada (no se usa)
// # 	     1	   Token para identificar la checada como que es Cafeter�a
// ~ 	     1	   Caracter que indica que se us� un lector de Proximidad
// 1234567890   10	   Gafete le�do por el lector de proximidad (justificado con ceros a la izquierda)
// MM	     2	   Mes de la checada (00 al 12)
// DD	     2	   D�a de la checada (01 al 31, dependiendo del mes )
// HH	     2	   Hora de la checada ( 00 a 23)
// NN	     2	   Minutos de la checada ( 00 a 59 )
// 99	     2	   Comidas pedidas
// T	     1	   Tipo de comida pedida ( 1,2,3,4,5,6,7,8,9)
//
// BIOMETRICO
//
// GAFETE BIOMETRICO:
// ==================
// 1234567890#$1234567890MMDDHHNN99T
//
// donde
//
// Concepto    Ancho Descripci�n
// ----------  ----- -----------------------------------------------------------
// 1234567890   10   Informaci�n del reloj donde se hizo la checada (no se usa)
// # 	     1	   Token para identificar la checada como que es Cafeter�a
// $ 	     1	   Caracter que indica que se us� un lector biometrico
// 1234567890   10   Gafete le�do por el lector biometrico (justificado con ceros a la izquierda)
// MM	     2	   Mes de la checada (00 al 12)
// DD	     2	   D�a de la checada (01 al 31, dependiendo del mes )
// HH	     2	   Hora de la checada ( 00 a 23)
// NN	     2	   Minutos de la checada ( 00 a 59 )
// 99	     2	   Comidas pedidas
// T	     1	   Tipo de comida pedida ( 1,2,3,4,5,6,7,8,9)
//
{$ENDIF}
{$IFDEF FALSE}
//La Checada de Accesos es algo as�:
//
//  GAFETE TRESS: = = = = = = = = = = = = = AAAAAA7890 &E123456789CMMDDHHNNI
//
//  donde
//
//  Concepto Ancho Descripci�n - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  -AAAAAA 6 C�digo de La Caseta donde se hizo La Checada 1234 4 Filler(no se usa)
//  & 1 Token para identificar La Checada como que es de Accesos E 1 Caracter que identifica a La
//  Empresa(ver globales de Empresa)123456789 9 N�mero de Empleado C 1 Letra de Credencial del
//  Empleado MM 2 Mes de La Checada(00 al 12)DD 2 D�a de La Checada(01 al 31, dependiendo del Mes)
//  HH 2 Hora de La Checada(00 a 23)NN 2 Minutos de La Checada(00 a 59)
//  I 1 Numero que indica si es Desconocida(0), Entrada(1)o Salida(2)
//
//  GAFETE PROXIMIDAD: = = = = = = = = = = = = = = = = = = 1234567890 & ~ 1234567890 MMDDHHNNI
//
//  donde
//
//  Concepto Ancho Descripci�n - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  -AAAAAA 6 C�digo de La Caseta donde se hizo La Checada 1234 4 Filler(no se usa)
//  & 1 Token para identificar La Checada como que es de Accesos ~ 1 Caracter que indica que se us� un
//  lector de PROXIMIDAD 1234567890 10 GAFETE le�do por el lector de PROXIMIDAD
//  (justificado con ceros a La izquierda)MM 2 Mes de La Checada(00 al 12)
//  DD 2 D�a de La Checada(01 al 31, dependiendo del Mes)HH 2 Hora de La Checada(00 a 23)
//  NN 2 Minutos de La Checada(00 a 59)I 1 Numero que indica si es Desconocida(0),
//  Entrada(1)o Salida(2)
{$ENDIF}
function TChecadaComida.GetChecadaText: String;
begin
  Result := '#' + IntToStr(Empleado) + '; ' + FechaCorta(Fecha) + ' - ' + Copy(Hora, 1, 2) + ':' +
    Copy(Hora, 3, 2) + ' = ' + IntToStr(NumComidas) + ' Comida(s) Tipo ' + TipoComida + '->' +
    Respuesta;
end;

function TChecadaComida.MealType: Word;
begin
  Result := (Ord(FTipoComida) - 48);
end;

procedure TChecadaComida.SetExtras(const lValue: Boolean);
begin
  FExtras := FExtras or lValue;
end;

procedure TChecadaComida.SetRegla(const iValue: TCodigoRegla);
begin
  FRegla := iValue;
end;

{ ************* TRegla *************** }

constructor TRegla.Create(Lista: TListaReglas);
begin
  FListaReglas := Lista;
  FFiltro := nil;
end;

destructor TRegla.Destroy;
begin
  FreeAndNil(FFiltro);
  inherited Destroy;
end;

function TRegla.EvaluaFiltro: Boolean;
begin
  Result := True;
  if Assigned(FFiltro) then begin
    ValidaError(FFiltro.Value, Format('Error en Filtro De La Regla # %s', [CodigoAsStr]));
    Result := FFiltro.ValorBooleano;
  end;
end;

function TRegla.GetQuery: TZetaCursor;
begin
  Result := FListaReglas.Query;
end;

function TRegla.GetFormula(const sFormula: String): String;
begin
  Result := ZEvaluador.StrTransVar(sFormula, 'AUSENCIA', 'TARJETAXY');
end;

procedure TRegla.Init;
begin
  if ZetaCommonTools.StrLleno(FormulaFiltro) then begin
    FFiltro := TZetaEvaluador.Create(ListaReglas.oZetaCreator);
    Filtro.Entidad := enEmpleado;
    ListaReglas.InitBroker;
    FColFiltro := ListaReglas.SQLBroker.Agente.AgregaColumna(LimpiaFormula(GetFormula(FormulaFiltro)
        ), False, enEmpleado, tgBooleano, 0, Format('Regla # %s: Filtro', [CodigoAsStr]));
  end;
end;

procedure TRegla.PreparaUnEvaluador(oEvaluador: TZetaEvaluador; const iColumna: Integer);
var
  sTransformada: String;
begin
  if Assigned(oEvaluador) then begin
          { En la 'Formula' guardo la posici�n de la columna en el agente }
    sTransformada := ListaReglas.SQLBroker.Agente.GetColumna(iColumna).Formula;

    if oEvaluador.DataSets.Count = 1 then
      oEvaluador.DataSets.Clear;

    oEvaluador.AgregaDataset(Query);
    oEvaluador.Prepare(sTransformada);
  end;
end;

procedure TRegla.PreparaEvaluadores;
begin
  PreparaUnEvaluador(Filtro, ColFiltro);
end;

{ ************* TReglaAcceso *************** }

destructor TReglaAcceso.Destroy;
begin
  FreeAndNil(FEvaluadorValor);
  inherited Destroy;
end;

function TReglaAcceso.CodigoAsStr: String;
begin
  Result := Format('%d', [Codigo]);
end;

function TReglaAcceso.GetValor: String;
begin
  case Tipo of
    arInformativo:
      Result := FValor;
    else
      if IsOk then
        Result := R_BIENVENIDO
      else
        Result := Letrero;
  end;
end;

procedure TReglaAcceso.SetValor(const Value: String);
begin
  FValor := Value;
end;

procedure TReglaAcceso.SetLetrero(const Value: String);
begin
  if (FLetrero <> Value) then begin
    FLetrero := Value;
    if StrVacio(FLetrero) then
      FLetrero := R_RECHAZO;
  end;
end;

procedure TReglaAcceso.Init;
begin
  if ZetaCommonTools.StrLleno(FormulaValor) then begin
    FEvaluadorValor := TZetaEvaluador.Create(ListaReglas.oZetaCreator);
    EvaluadorValor.Entidad := enEmpleado;
    ListaReglas.InitBroker;
    FColValor := ListaReglas.SQLBroker.Agente.AgregaColumna(GetFormula(FormulaValor), False,
      enEmpleado, tgBooleano, 0, Format('Regla # %s: F�rmula', [CodigoAsStr]));
  end;
  inherited Init;
end;

procedure TReglaAcceso.PreparaEvaluadores;
begin
  PreparaUnEvaluador(EvaluadorValor, ColValor);
  inherited PreparaEvaluadores;
end;

function TReglaAcceso.Evaluar(Checada: TChecadaAcceso): Boolean;
begin
  with Checada do begin
    case Self.Tipo of
      arInformativo:
        Result := True;
      arEvalEntrada:
        Result := (Tipo = chEntrada);
      arEvalSalida:
        Result := (Tipo = chSalida);
      arEvaluacion:
        Result := True;
      else
        Result := False;
    end;
    Result := Result and EvaluaFiltro; { La regla aplica a este Empleado? }
    if Result then begin
      FIsOk := True;
      if Assigned(EvaluadorValor) then begin
        ValidaError(EvaluadorValor.Value, Format('Error En F�rmula De La Regla # %s',
            [CodigoAsStr]));
        case Self.Tipo of
          arInformativo:
            Valor := EvaluadorValor.ValorTexto;
          arEvalEntrada:
            FIsOk := EvaluadorValor.ValorBooleano;
          arEvalSalida:
            FIsOk := EvaluadorValor.ValorBooleano;
          arEvaluacion:
            FIsOk := EvaluadorValor.ValorBooleano;
        end;
      end;
    end;
  end;
end;

{ ************* TReglaCafe *************** }

constructor TReglaCafe.Create(Lista: TListaReglas);
begin
  inherited Create(Lista);
  FTipos := '123456789';
  FNoHayTipos := False;
end;

destructor TReglaCafe.Destroy;
begin
  FreeAndNil(FYaConsumidas);
  FreeAndNil(FExtras);
  inherited Destroy;
end;

function TReglaCafe.CodigoAsStr: String;
begin
  Result := Format('%d', [Codigo]);
end;

procedure TReglaCafe.SetMsgFracaso(const Value: String);
begin
  if (FMsgFracaso <> Value) then begin
    FMsgFracaso := Value;
    if StrVacio(FMsgFracaso) then
      FMsgFracaso := R_RECHAZO;
  end;
end;

procedure TReglaCafe.SetTipos(const sValue: String);
begin
  if (FTipos <> sValue) then begin
    FTipos := Trim(sValue);
    FNoHayTipos := StrVacio(FTipos);
  end;
end;

procedure TReglaCafe.Init;
begin
  if ZetaCommonTools.StrLleno(FormulaExtras) then begin
    FExtras := TZetaEvaluador.Create(ListaReglas.oZetaCreator);
    Extras.Entidad := enEmpleado;
    ListaReglas.InitBroker;
    FColExtras := ListaReglas.SQLBroker.Agente.AgregaColumna(GetFormula(FormulaExtras), False,
      enEmpleado, tgAutomatico, 0, Format('Regla # %s: Comidas Extras', [CodigoAsStr]));
  end;
  if ZetaCommonTools.StrLleno(FormulaYaConsumidas) then begin
    FYaConsumidas := TZetaEvaluador.Create(ListaReglas.oZetaCreator);
    YaConsumidas.Entidad := enEmpleado;
    ListaReglas.InitBroker;
    FColConsumidas := ListaReglas.SQLBroker.Agente.AgregaColumna(GetFormula(FormulaYaConsumidas),
      False, enEmpleado, tgAutomatico, 0, Format('Regla # %s: Comidas Consumidas', [CodigoAsStr]));
  end;
  inherited Init;
end;

procedure TReglaCafe.PreparaEvaluadores;
begin
  PreparaUnEvaluador(Extras, ColExtras);
  PreparaUnEvaluador(YaConsumidas, ColConsumidas);
  inherited PreparaEvaluadores;
end;

function TReglaCafe.Evaluar(Checada: TChecadaComida): Boolean;
var
  iConsumidas, iExtras: Integer;
begin
  with Checada do begin
    Result := False;
    Regla := Codigo;
    if FNoHayTipos or (Pos(TipoComida, Self.Tipos) > 0)
    then { La regla aplica a este Tipo de Comida? }
    begin
      if EvaluaFiltro then { La regla aplica a este Empleado? }
      begin
        if Assigned(FYaConsumidas) then begin
          ValidaError(FYaConsumidas.Value,
            Format('Error En F�rmula De Comidas Consumidas De La Regla # %s', [CodigoAsStr]));
          iConsumidas := FYaConsumidas.ValorEntero;
        end
        else
          iConsumidas := 0;
        iConsumidas := iConsumidas + NumComidas;
        if (iConsumidas <= Self.Limite) then { Compara consumidos contra el L�mite }
        begin
          Result := True;
          Regla := 0;
        end else begin
          if Assigned(Self.FExtras) then begin
            ValidaError(Self.FExtras.Value,
              Format('Error En F�rmula De Comidas Extras De La Regla # %s', [CodigoAsStr]));
            iExtras := Self.FExtras.ValorEntero;
          end
          else
            iExtras := 0;
          if (iConsumidas <= (Limite + iExtras)) then begin
            Result := True;
            Extras := True;
            Regla := 0;
          end
          else
            Respuesta := MsgFracaso;
        end;
      end else begin
        Result := True;
        Regla := 0;
      end;
    end else begin
      Result := True;
      Regla := 0;
    end;
  end;
end;

{ ************* TListaReglas *************** }

constructor TListaReglas.Create(Cafeteria: TCafeteria);
begin
  FLista := TList.Create;
  FCafeteria := Cafeteria;
  FEvalPreparados := False;
end;

destructor TListaReglas.Destroy;
begin
  Clear;
  FreeAndNil(FLista);
  if Assigned(FQuery) then
    FreeAndNil(FQuery);
  if Assigned(FSQLBroker) then
    FreeAndNil(FSQLBroker);
  inherited Destroy;
end;

function TListaReglas.GetCreator: TZetaCreator;
begin
  Result := Cafeteria.Creator;
end;

function TListaReglas.Count: Integer;
begin
  Result := FLista.Count;
end;

procedure TListaReglas.AgregandoRelacion(Sender: TObject);
var
  oColabora: TEntidad;
begin
  oColabora := oZetaCreator.dmEntidadesTress.GetEntidad(enEmpleado);
  if oColabora <> NIL then begin
    with oColabora do begin
      AddRelacion(enTarjetaXY, 'CB_CODIGO,:Fecha');
    end;
  end;
end;

procedure TListaReglas.InitBroker;
begin
  if not Assigned(FSQLBroker) then begin
    FSQLBroker := TSQLBroker.Create(oZetaCreator);
    SQLBroker.Init(enEmpleado);
    oZetaCreator.RegistraFunciones([efComunes, efCafe]);
  end;
end;

procedure TListaReglas.Prepare;
begin
  if Assigned(FSQLBroker) then begin
    with FSQLBroker do begin
      Agente.AgregaColumna('TARJETAXY.CB_CODIGO', True, enTarjetaXY, tgAutomatico, 10);
      Agente.AgregaFiltro('COLABORA.CB_CODIGO=:Empleado', True, enEmpleado);
      if not SuperSQL.Construye(Agente) then
        raise Exception.Create(SQLBroker.Agente.ListaErrores.Text);
      {$IFDEF CAROLINA}
      Agente.SQL.SaveToFile('CofeeShop.SQL');
      {$ENDIF}
      FQuery := oZetaProvider.CreateQuery(StrTransVar(StrTransVar(Agente.SQL.Text,
            'COLABORA.:Fecha', ':Fecha'), 'TARJETAXY', 'AUSENCIA'));
    end;
  end;
end;

function TListaReglas.AbreQuery(const iEmpleado: TNumEmp; const dFecha: TDate): Boolean;

   procedure  PrepararQuery;
   begin
    if Assigned(FSQLBroker) then begin
    with FSQLBroker do begin
       if FQuery <> nil then
        FreeAndNil(FQuery);

        FQuery := oZetaProvider.CreateQuery(StrTransVar(StrTransVar(Agente.SQL.Text,
            'COLABORA.:Fecha', ':Fecha'), 'TARJETAXY', 'AUSENCIA'));
    end;
   end;
   end;
begin
  PrepararQuery;

  with Query do begin
    Active := False;
    with oZetaCreator.oZetaProvider do begin
      ParamAsInteger(Query, 'Empleado', iEmpleado);
      ParamAsDate(Query, 'Fecha', dFecha);
    end;
    Active := True;
    Result := not Eof;
  end;
end;

{ ************* TListaReglasAcceso *************** }

procedure TListaReglasAcceso.Clear;
var
  I: Integer;
begin
  with FLista do { Muy probable que ya est�n destruidos todos los Queries }
  begin
    for I := (Count - 1) downto 0 do begin
      Self.Delete(I);
    end;
    Clear;
  end;
end;

function TListaReglasAcceso.GetRegla(Index: Integer): TReglaAcceso;
begin
  with FLista do begin
    if (Index >= 0) and (Index < Count) then
      Result := TReglaAcceso(Items[Index])
    else
      Result := NIL;
  end;
end;

function TListaReglasAcceso.AddRegla: TReglaAcceso;
begin
  Result := TReglaAcceso.Create(Self);
  FLista.Add(Result);
end;

procedure TListaReglasAcceso.Delete(const Index: Integer);
begin
  Regla[Index].Free;
  FLista.Delete(Index);
end;

function TListaReglasAcceso.GetIndex(const iCodigo: TCodigoRegla): Integer;
var
  I: Integer;
begin
  Result := -1;
  with FLista do begin
    for I := 0 to (Count - 1) do begin
      if (Regla[I].Codigo = iCodigo) then begin
        Result := I;
        Break;
      end;
    end;
  end;
end;

procedure TListaReglasAcceso.Init;
var
  I: Integer;
begin
  with FLista do begin
    for I := 0 to (Count - 1) do begin
      with Regla[I] do begin
        Init;
      end;
    end;
  end;
end;

procedure TListaReglasAcceso.PreparaEvaluadores;
var
  I: Integer;
begin
  //if not EvalPreparados then begin
    with FLista do begin
      for I := 0 to (Count - 1) do begin
        with Regla[I] do begin
          PreparaEvaluadores;
        end;
      end;
    end;
    //EvalPreparados := True;
  //end;
end;

function TListaReglasAcceso.Evaluar(Checada: TChecadaAcceso): Boolean;
var
  I         : Integer;
  eSeveridad: eTipoBitacora;
begin
  Result := True;
  if Assigned(SQLBroker) then begin
    with Checada do begin
      if not AbreQuery(Empleado, Fecha) then begin
        Respuesta := R_EMPLEADO_NO_ENCONTRADO;
        Result := False;
      end;
    end;
    if Result then begin
      PreparaEvaluadores;
      eSeveridad := eTipoBitacora(0);
      with FLista do begin
        for I := 0 to (Count - 1) do begin
          with Regla[I] do begin
            if Evaluar(Checada) then begin
              case Tipo of
                arInformativo:
                  Cafeteria.AgregaReglaInformativa(Letrero, Valor);
                arEvalEntrada:
                  Cafeteria.AgregaReglaEvaluacion(Letrero, Valor, Severidad, IsOk);
                arEvalSalida:
                  Cafeteria.AgregaReglaEvaluacion(Letrero, Valor, Severidad, IsOk);
                arEvaluacion:
                  Cafeteria.AgregaReglaEvaluacion(Letrero, Valor, Severidad, IsOk);
              end;
              Result := Result and (IsOk or (Severidad <= tbAdvertencia));
              // Las Advertencias si aceptan las checadas
              if (not IsOk) then begin
                if (eSeveridad < Severidad) then begin
                  with Checada do begin
                    Regla := Codigo;
                    Respuesta := Letrero;
                  end;
                  eSeveridad := Severidad;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

{ ************* TListaReglasCafe *************** }

procedure TListaReglasCafe.Clear;
var
  I: Integer;
begin
  with FLista do { Muy probable que ya est�n destruidos todos los Queries }
  begin
    for I := (Count - 1) downto 0 do begin
      Self.Delete(I);
    end;
    Clear;
  end;
end;

function TListaReglasCafe.GetRegla(Index: Integer): TReglaCafe;
begin
  with FLista do begin
    if (Index >= 0) and (Index < Count) then
      Result := TReglaCafe(Items[Index])
    else
      Result := NIL;
  end;
end;

function TListaReglasCafe.AddRegla: TReglaCafe;
begin
  Result := TReglaCafe.Create(Self);
  FLista.Add(Result);
end;

procedure TListaReglasCafe.Delete(const Index: Integer);
begin
  Regla[Index].Free;
  FLista.Delete(Index);
end;

function TListaReglasCafe.GetIndex(const iCodigo: TCodigoRegla): Integer;
var
  I: Integer;
begin
  Result := -1;
  with FLista do begin
    for I := 0 to (Count - 1) do begin
      if (Regla[I].Codigo = iCodigo) then begin
        Result := I;
        Break;
      end;
    end;
  end;
end;

procedure TListaReglasCafe.Init(const sMsgExito: String);
var
  I: Integer;
begin
  with FLista do begin
    for I := 0 to (Count - 1) do begin
      with Regla[I] do begin
        Init;
      end;
    end;
  end;
  FReply := oZetaCreator.oZetaProvider.GetGlobalString(K_GLOBAL_LETRERO);
  if ZetaCommonTools.StrVacio(FReply) then
    FReply := R_ACEPTADA;
end;

procedure TListaReglasCafe.EvaluarMsgExito(Checada: TChecadaComida);
begin
  with Checada do begin
    Respuesta := FReply;
  end;
end;

procedure TListaReglasCafe.PreparaEvaluadores;
var
  I: Integer;
begin
  //if not EvalPreparados then begin
  with FLista do begin
    for I := 0 to (Count - 1) do begin
      with Regla[I] do begin
        PreparaEvaluadores;
      end;
    end;
  end;
 {   EvalPreparados := False;
  end;}
end;

function TListaReglasCafe.Evaluar(Checada: TChecadaComida): Boolean;
var
  I: Integer;
begin
  Result := True;
  if Assigned(SQLBroker) then begin
    with Checada do begin
      if not AbreQuery(Empleado, Fecha) then begin
        Respuesta := R_EMPLEADO_NO_ENCONTRADO;
        Result := False;
      end;
    end;
    if Result then begin
      PreparaEvaluadores;
      with FLista do begin
        for I := 0 to (Count - 1) do begin
          if not Regla[I].Evaluar(Checada) then begin
            Result := False;
            Break;
          end;
        end;
      end;
    end;
  end;
  if Result then begin
    EvaluarMsgExito(Checada);
  end;
end;

{ ************* TCafeteria *************** }

constructor TCafeteria.Create;
begin
  oZetaProvider := TdmZetaServerProvider.Create(NIL);
  oZetaProvider.SetReadUncommited;
  oCreator := TZetaCreator.Create(oZetaProvider);

  PreparaQueries; // Se preparan objetos para determinar tipos de dia
  PreparaRitmos;

  FReglasCafe := TListaReglasCafe.Create(Self);
  FReglasAcceso := TListaReglasAcceso.Create(Self);
  FNombre := 'Compa��a Nueva';
  FDigito := '0';
  FMsgExito := R_ACEPTADA;
  FInicial := NullDateTime;
  FFinal := NullDateTime;
  FFecha := NullDateTime;
  FFechaInit := NullDateTime;
  FListaDispCafeteria := TStringList.Create;
  FListaDispAccesos := TStringList.Create;
end;

destructor TCafeteria.Destroy;
begin
  Close;
  FreeAndNil(FReglasAcceso);
  FreeAndNil(FReglasCafe);
  DesPreparaRitmos;
  DesPreparaQueries;
  FreeAndNil(FListaDispCafeteria);
  FreeAndNil(FListaDispAccesos);
  FreeAndNil(oCreator); // Se destruyen todos los objetos creados por el Creator
  FreeAndNil(oZetaProvider);
  inherited Destroy;
end;

procedure TCafeteria.PreparaQueries;
  begin
    if not Assigned(FQueries) then
    FQueries := TCommonQueries.Create(oZetaProvider);
end;

procedure TCafeteria.DesPreparaQueries;
begin
  FreeAndNil(FQueries);
end;

procedure TCafeteria.PreparaRitmos;
begin
  if not Assigned(FRitmos) then begin
    FRitmos := TRitmos.Create(oZetaProvider);
    if Assigned(FQueries) then
      FRitmos.Queries := FQueries;
  end;
end;

procedure TCafeteria.DesPreparaRitmos;
begin
  FreeAndNil(FRitmos);
end;

function TCafeteria.GetFechaInicial: TDate;
begin
  Result := ChecadaComida.Fecha;
end;

function TCafeteria.GetFechaFinal: TDate;
begin
  Result := ChecadaComida.Fecha;
end;

function TCafeteria.GetUsaCafeteria: Boolean;
begin
  Result := FUsaCafeteria and TieneDigito;
end;

function TCafeteria.CafeteriaValida(const sReloj: String): Boolean;
begin
  Result := CuantosCafeteria = 0;
  if not Result then
    Result := ListaDispCafeteria.IndexOf(sReloj) >= 0;
end;

function TCafeteria.AccesosValido(const sCaseta: String): Boolean;
begin
  Result := CuantosAccesos = 0;
  if not Result then
    Result := ListaDispAccesos.IndexOf(sCaseta) >= 0;
end;

procedure TCafeteria.SetChecadaAcceso(Value: TChecadaAcceso);
begin
  FChecadaAcceso := Value;
end;

procedure TCafeteria.SetChecadaComida(Value: TChecadaComida);
begin
  FChecadaComida := Value;
end;


procedure TCafeteria.PrepararLeeEmpleado;
begin
    if ( FLeeEmpleado <> nil )  then
    begin
        FreeAndNil( FLeeEmpleado ) ;
    end;

    with oZetaProvider do
    begin
          FLeeEmpleado := CreateQueryLocate;

                           { AP(30/Nov/2007): Se cambi� a partir de que dejo de funcionar SQLOptions.soFetchBlobs ahora la foto se lee en FLeeFotoEmp
                           PreparaQuery( FLeeEmpleado, Format( GetSQLScript( Q_GET_EMPLEADO ), [ K_FOTO ] ) ); }
          PreparaQuery(FLeeEmpleado, GetSQLScript(Q_GET_EMPLEADO));
    end;
end;

procedure TCafeteria.PrepararLeeInvitador;
begin

    if ( FLeeInvitador <> nil )  then
    begin
        FreeAndNil( FLeeInvitador ) ;
    end;


    with oZetaProvider do
    begin
                     { Datos Del Invitador }
          FLeeInvitador := CreateQueryLocate;
          PreparaQuery(FLeeInvitador, GetSQLScript(Q_GET_INVITADOR));
    end;
end;

procedure TCafeteria.PrepararLeeComidas;
begin

    if ( FLeeComidas <> nil )  then
    begin
        FreeAndNil( FLeeComidas ) ;
    end;

    with oZetaProvider do
    begin
                     { Historial Comidas / Invitaciones }
          FLeeComidas := CreateQueryLocate;
          PreparaQuery(FLeeComidas, GetSQLScript(Q_GET_COMIDAS));
    end;
end;

procedure TCafeteria.PrepararLeeFotoEmp;
begin

    if ( FLeeFotoEmp <> nil )  then
    begin
        FreeAndNil( FLeeFotoEmp ) ;
    end;

    with oZetaProvider do
    begin
                     { Foto del Empleado }
          FLeeFotoEmp := CreateQueryLocate;
          PreparaQuery(FLeeFotoEmp, Format(GetSQLScript(Q_LEE_FOTO_EMP), [EntreComillas(K_FOTO)]));
    end;
end;

procedure TCafeteria.PrepararLeeInvitaciones;
begin
    if ( FLeeInvitaciones <> nil )  then
    begin
        FreeAndNil( FLeeInvitaciones ) ;
    end;

    with oZetaProvider do
    begin
          FLeeInvitaciones := CreateQueryLocate;
          PreparaQuery(FLeeInvitaciones, GetSQLScript(Q_GET_INVITACIONES));
    end;
end;

procedure TCafeteria.PrepararObtieneHuellas;
begin

    if ( FObtieneHuellas <> nil )  then
    begin
        FreeAndNil( FObtieneHuellas ) ;
    end;

    with oZetaProvider do
    begin
          FObtieneHuellas := CreateQueryLocate; // BIOMETRICO
          PreparaQuery(FObtieneHuellas, GetSQLScript(Q_LEE_HUELLAS)); // BIOMETRICO
    end;
end;

procedure TCafeteria.PrepararEmpleadoDatos;
begin

    if ( FEmpleadoDatos <> nil )  then
    begin
        FreeAndNil( FEmpleadoDatos ) ;
    end;

    with oZetaProvider do
    begin
                { Datos Del Empleado }
          FEmpleadoDatos := CreateQuery(Format(GetSQLScript(Q_GET_EMPLEADO_DATOS),
              [ZetaCommonTools.iMax(FSupervisorNivel, 1), K_FOTO]));
    end;
end;

procedure TCafeteria.PrepararEmpleadoStatus;
begin

    if ( FEmpleadoStatus <> nil )  then
    begin
        FreeAndNil( FEmpleadoStatus ) ;
    end;

    with oZetaProvider do
    begin
          FEmpleadoStatus := CreateQuery(GetSQLScript(Q_GET_EMPLEADO_STATUS));
          ParamSalida(FEmpleadoStatus, 'Resultado');
          ParamSalida(FEmpleadoStatus, 'Incidencia');
    end;
end;


procedure TCafeteria.PrepararHorarioDatos;
begin

    if ( FHorarioDatos <> nil )  then
    begin
        FreeAndNil( FHorarioDatos ) ;
    end;

    with oZetaProvider do
    begin
          FHorarioDatos := CreateQuery(GetSQLScript(Q_HORARIO_LEE));
    end;
end;

procedure TCafeteria.PrepararAccessCount;
begin
    if ( FAccessCount <> nil )  then
    begin
        FreeAndNil( FAccessCount ) ;
    end;

    with oZetaProvider do
    begin
          FAccessCount := CreateQuery(GetSQLScript(Q_ACCESS_COUNT));
    end;
end;

procedure TCafeteria.PrepararComida1;
begin

  if ( FComida1 <> nil )  then
  begin
      FreeAndNil( FComida1 ) ;
  end;

  with oZetaProvider do
  begin
    FComida1 := CreateQuery(GetSQLScript(Q_CAFE_AGREGA_COMIDA));
  end;
end;

procedure TCafeteria.PrepararComida2;
begin

  if ( FComida2 <> nil )  then
  begin
      FreeAndNil( FComida2 ) ;
  end;


  with oZetaProvider do
  begin
        FComida2 := CreateQuery(GetSQLScript(Q_CAFE_AGREGA_INVITACION));
  end;
end;

procedure TCafeteria.PrepararComida3;
begin

  if ( FComida3 <> nil )  then
  begin
      FreeAndNil( FComida3 ) ;
  end;


  with oZetaProvider do
  begin
        FComida3 := CreateQuery(GetSQLScript(Q_CAFE_AUTORIZA_COMIDA));
  end;
end;
procedure TCafeteria.PrepararComida4;
begin

  if ( FComida4 <> nil )  then
  begin
      FreeAndNil( FComida4 ) ;
  end;

  with oZetaProvider do
  begin

        FComida4 := CreateQuery(GetSQLScript(Q_CAFE_AUTORIZA_INVITACION));
  end;
end;

procedure TCafeteria.PrepararComida5;
begin

  if ( FComida5 <> nil )  then
  begin
      FreeAndNil( FComida5 ) ;
  end;

  with oZetaProvider do
  begin
        FComida5 := CreateQuery(GetSQLScript(Q_CAFE_UPDATE_COMIDA));
  end;
end;

procedure TCafeteria.PrepararAcceso1;
begin

  if ( FAcceso1 <> nil )  then
  begin
      FreeAndNil( FAcceso1 ) ;
  end;


  with oZetaProvider do
  begin
        FAcceso1 := CreateQuery(GetSQLScript(Q_ACCESO_AGREGA));
  end;
end;

procedure TCafeteria.PrepararPeriodo;
begin

  if ( FPeriodo <> nil )  then
  begin
      FreeAndNil( FPeriodo ) ;
  end;

  with oZetaProvider do
  begin
      FPeriodo := CreateQuery(GetSQLScript(Q_BUSCA_PERIODO));
  end;
end;

{$IFDEF BOSE}
procedure TCafeteria.PrepararBitacora;
begin

  if ( FBitacora <> nil )  then
  begin
      FreeAndNil( FBitacora ) ;
  end;

  with oZetaProvider do
  begin
      FBitacora := CreateQuery(GetSQLScript(Q_AGREGA_BITACORA));
  end;
end;
{$ENDIF}


procedure TCafeteria.Init(CallBack: TCallBack);
var
  oParametros: TZetaParams;
begin
  FFechaInit := Date;
  oParametros := TZetaParams.Create;

  FPeriodo := nil;
  FObtieneHuellas := nil;
  FLeeInvitaciones := nil;
  FLeeInvitador := nil;
  FLeeComidas := nil;
  FLeeFotoEmp := nil;
  FComida5 := nil;
  FComida4 := nil;
  FComida3 := nil;
  FComida2 := nil;
  FComida1 := nil;
  {$IFDEF BOSE}
  FBitacora := nil;
  {$ENDIF}
  FAcceso1 := nil;
  FAccessCount := nil;
  FHorarioDatos := nil;
  FEmpleadoStatus := nil;
  FEmpleadoDatos := nil;
  FLeeEmpleado := nil;

  try
    with oParametros do begin
      AddInteger('Year', 0);
      AddInteger('Tipo', 0);
      AddInteger('Numero', 0);
      AddString('RegistroPatronal', '');
      AddInteger('IMSSYear', 0);
      AddInteger('IMSSMes', 0);
      AddInteger('IMSSTipo', 0);
      AddDate('FechaAsistencia', FFechaInit);
      AddDate('FechaDefault', FFechaInit);
      AddInteger('YearDefault', TheYear(FFechaInit));
      AddInteger('EmpleadoActivo', 0);
      AddString('NombreUsuario', '');
      AddString('CodigoEmpresa', '');
    end;
    try
      with oZetaProvider do begin

        AsignaParamList(oParametros.VarValues);
        EmpresaActiva := VarArrayOf([AliasName, UserName, Password, 0, '']);
        InitGlobales;
        FSupervisorNivel := ZetaCommonTools.iMin(GetGlobalInteger(K_GLOBAL_NIVEL_SUPERVISOR),
          K_GLOBAL_NIVEL_MAX);
        FSupervisorNombre := GetGlobalString(K_GLOBAL_NIVEL_BASE + FSupervisorNivel);
        TipoPeriodo := eTipoPeriodo(tpSemanal); { eTipoPeriodo( GetGlobalInteger( 0 ) ) };  //Pendiente
                { Datos Del Empleado }
        if UsaCafeteria then begin
          AgregarConError := GetGlobalBooleano(K_GLOBAL_AGREGAR_CON_ERROR);
                     {
                     ERG ( 2.4.97 : Ya no se lee el d�gito de Globales, se inicializa desde invocaci�n a AddCafeteria()
                     Digito := GetGlobalString( K_GLOBAL_DIGITO_EMPRESA );
 }
          MsgExito := GetGlobalString(K_GLOBAL_LETRERO);
                     { Datos Del Empleado }

          PrepararLeeEmpleado;
          PrepararLeeInvitador;
          PrepararLeeComidas;
          PrepararLeeFotoEmp;
          PrepararLeeInvitaciones;
          PrepararObtieneHuellas;

        end;

        if UsaAccesos then begin
            PrepararEmpleadoDatos;
            PrepararEmpleadoStatus;
            PrepararHorarioDatos;
            PrepararAccessCount;
        end;

      end;
      FRitmos.RitmosBegin(FFechaInit - K_DIAS_EVALUAR_HORARIO, FFechaInit + K_DIAS_EVALUAR_HORARIO);
      // Prepara la semana alrededor de la fecha actual ( eg. Hoy = Jueves: Prepara del Lunes al Domingo )
    except
      on e: Exception do begin
        raise;
      end;
    end;
  finally
    oParametros.Free;
  end;
  if Assigned(CallBack) then
    CallBack(Nombre + ': Leyendo Reglas', 1);
  try
    if UsaCafeteria then begin
      with oZetaProvider.CreateQuery(GetSQLScript(Q_CAFE_LEE_REGLAS)) do begin
        try
          Active := True;
          while not Eof do begin
            with ReglasCafe.AddRegla do begin
              Codigo := FieldByName('CL_CODIGO').AsInteger;
              MsgFracaso := FieldByName('CL_LETRERO').AsString;
              Tipos := FieldByName('CL_TIPOS').AsString;
              Limite := FieldByName('CL_LIMITE').AsInteger;
              FormulaExtras := FieldByName('CL_EXTRAS').AsString;
              FormulaYaConsumidas := FieldByName('CL_TOTAL').AsString;
              FormulaFiltro := ConcatFiltros(FieldByName('CL_FILTRO').AsString,
                FieldByName('QUERY').AsString);
            end;
            Next;
          end;
        finally
          Free;
        end;
      end;
    end;
    if UsaAccesos then begin
      with oZetaProvider.CreateQuery(Format(GetSQLScript(Q_ACCESS_LEE_REGLAS), [K_GLOBAL_SI])) do
      begin
        try
          Active := True;
          while not Eof do begin
            with ReglasAcceso.AddRegla do begin
              Codigo := FieldByName('AE_CODIGO').AsInteger;
              Letrero := FieldByName('AE_LETRERO').AsString;
              Tipo := eAccesoRegla(FieldByName('AE_TIPO').AsInteger);
              Severidad := eTipoBitacora(FieldByName('AE_SEVERI').AsInteger);
              FormulaValor := FieldByName('AE_FORMULA').AsString;
              FormulaFiltro := ConcatFiltros(FieldByName('AE_FILTRO').AsString,
                FieldByName('QUERY').AsString);
            end;
            Next;
          end;
        finally
          Free;
        end;
      end;
    end;
  except
    on e: Exception do begin
      raise;
    end;
  end;
  if Assigned(CallBack) then
    CallBack(Nombre + ': Preparando Reglas', 1);
  try
    if UsaCafeteria then
    begin
      with ReglasCafe do
      begin
        Init(MsgExito);
        Prepare;
      end;
    end;
    if UsaAccesos then
    begin
      with ReglasAcceso do
      begin
        Init;
        Prepare;
      end;
    end;
  except
    on e: Exception do
    begin
      raise;
    end;
  end;
  if Assigned(CallBack) then
    CallBack(Nombre + ': Preparando Queries', 1);
  try
    with oZetaProvider do
    begin
      if UsaCafeteria then
      begin
        PrepararComida1;
        PrepararComida2;
        PrepararComida3;
        PrepararComida4;
        PrepararComida5;
      end;
      if UsaAccesos then
      begin
        PrepararAcceso1;
      end;
      PrepararPeriodo;
    end;
  except
    on e: Exception do
    begin
      raise;
    end;
  end;

  {$IFDEF BOSE}
     // Preparar Bitacora
  if Assigned(CallBack) then
    CallBack(Nombre + ': Preparando Bitacora', 1);
  try
    with oZetaProvider do
    begin
      if UsaCafeteria then
      begin
        PrepararBitacora;
      end;
    end;
  except
    on e: Exception do
    begin
      raise;
    end;
  end;
  {$ENDIF}
  if Assigned(CallBack) then
    CallBack(Nombre + ' Inicializada', 1);
end;

procedure TCafeteria.Close;
begin
  FreeAndNil(FPeriodo);
  if UsaCafeteria then begin
    {$IFNDEF INTERBASE}
    FreeAndNil(FObtieneHuellas); // BIOMETRICO
    {$ENDIF}
    FreeAndNil(FLeeInvitaciones);
    FreeAndNil(FLeeInvitador);
    FreeAndNil(FLeeComidas);
    FreeAndNil(FLeeFotoEmp);
    FreeAndNil(FComida5);
    FreeAndNil(FComida4);
    FreeAndNil(FComida3);
    FreeAndNil(FComida2);
    FreeAndNil(FComida1);
    {$IFDEF BOSE}
    FreeAndNil(FBitacora);
    {$ENDIF}
  end;
  if UsaAccesos then begin
    FreeAndNil(FAcceso1);
    FreeAndNil(FAccessCount);
    FreeAndNil(FHorarioDatos);
    FreeAndNil(FEmpleadoStatus);
    FreeAndNil(FEmpleadoDatos);
  end;
  FreeAndNil(FLeeEmpleado);
  FRitmos.RitmosEnd;
  FReglasCafe.Clear;
end;

function TCafeteria.PuedeEvaluarHorario(const dFecha: TDate): Boolean;
begin
     { Puede Evaluar hasta el d�a de la Inicializaci�n ( FFechaInit ) dos dias antes y
       dos dias despu�s ya que est� preparada una semana
       eg. Si FFechaInit = Jueves, RitmosBegin puede investigar el horario de
           Lunes | Martes | Miercoles | Jueves | Viernes | Sabado | Domingo
           Entonces la checada puede ser evaluada si es del Martes al Sabado ya que
           al revisar horarios de madrugada para el martes puede acabar siendo Lunes y
           al revisar el Sabado puede pasarse al Domingo
 }
  Result := (dFecha >= (FFechaInit - (K_DIAS_EVALUAR_HORARIO - 1))) and
    (dFecha <= (FFechaInit + (K_DIAS_EVALUAR_HORARIO - 1)));
end;

procedure TCafeteria.ReconectarEmpresa;
var
  oEmpresa : Variant;
begin
    try
      try
        oZetaProvider.adoEmpresa.Connected := FALSE;
      except
      on e: Exception do begin
        //
      end;
      end;
    finally

    end;

    oZetaProvider.EmpresaActiva := oZetaProvider.EmpresaActiva;
end;

procedure TCafeteria.InitDatosComida(Checada: TChecada);
begin
  with oZetaProvider.DatosComida do begin
    Empleado := Checada.Empleado;
    Fecha := Checada.Fecha;
    Hora := Checada.Hora;
    EmpresaNombre := Self.Nombre;
    Turno := VACIO;
    Horario := VACIO;
    StatusDia := eStatusAusencia(0);
    StatusEmpleado := eStatusEmpleado(0);
    TipoChecada := eTipoChecadas(0);
    Estacion := VACIO;
  end;
end;

procedure TCafeteria.PreparaParametrosAccesos;
var
  dChecada   : TDate;
  sHora      : String;
  oDatosTurno: TEmpleadoDatosTurno;
begin
  InitDatosComida(ChecadaAcceso);
  with ChecadaAcceso do begin
    if PuedeEvaluarHorario(Fecha) then begin
      dChecada := Fecha;
      sHora := Hora;
      oDatosTurno := FRitmos.GetEmpleadoDatosTurno(Empleado, dChecada);
      if StrLleno(oDatosTurno.Codigo) and StrLleno(oDatosTurno.StatusHorario.Horario) then begin
        FRitmos.DeterminaDia(Empleado, dChecada, sHora, oDatosTurno);
        with oDatosTurno do begin
          ChecadaAcceso.Turno := Codigo;
          with StatusHorario do begin
            ChecadaAcceso.Horario := Horario;
            ChecadaAcceso.StatusDia := Status;
          end;
        end;
      end;
      Fecha := dChecada;
      Hora := sHora;
    end;
  end;
  with oZetaProvider.ParamList do begin
    AddInteger('EmpleadoActivo', ChecadaAcceso.Empleado);
    AddDate('FechaDefault', ChecadaAcceso.Fecha);
    AddInteger('YearDefault', TheYear(ChecadaAcceso.Fecha));
  end;
  if (FFecha <> ChecadaAcceso.Fecha) then begin
    FFecha := ChecadaAcceso.Fecha;
  end;
  with oZetaProvider.DatosComida do begin
    Estacion := Trim(ChecadaAcceso.Caseta); // Caseta en la que se pretende registrar acceso
  end;
  BuscaPeriodo;
end;

procedure TCafeteria.PreparaParametrosCafeteria;
var
  dChecada   : TDate;
  sHora      : String;
  oDatosTurno: TEmpleadoDatosTurno;
begin
  InitDatosComida(ChecadaComida);
  with ChecadaComida do begin
    if (not EsInvitacion) then begin
      if PuedeEvaluarHorario(Fecha) then begin
        dChecada := Fecha;
        sHora := Hora;
        oDatosTurno := FRitmos.GetEmpleadoDatosTurno(Empleado, dChecada);
        if StrLleno(oDatosTurno.Codigo) and StrLleno(oDatosTurno.StatusHorario.Horario) then begin
          FRitmos.DeterminaDia(Empleado, dChecada, sHora, oDatosTurno);
          with oZetaProvider.DatosComida do begin
            Turno := oDatosTurno.Codigo;
            Horario := oDatosTurno.StatusHorario.Horario;
            StatusDia := oDatosTurno.StatusHorario.Status;
          end;
        end;
        Fecha := dChecada;
        Hora := sHora;
      end;
    end;
  end;
  with oZetaProvider.DatosComida do begin
    Fecha := ChecadaComida.Fecha;
    Hora := ChecadaComida.Hora;
    TipoComida := ChecadaComida.TipoComida;
    NumComidas := ChecadaComida.NumComidas;
    Estacion := Trim(ChecadaComida.Reloj);
  end;
  with oZetaProvider.ParamList do begin
    AddInteger('EmpleadoActivo', ChecadaComida.Empleado);
    AddDate('FechaDefault', ChecadaComida.Fecha);
    AddInteger('YearDefault', TheYear(ChecadaComida.Fecha));
  end;
  if (FFecha <> ChecadaComida.Fecha) then begin
    FFecha := ChecadaComida.Fecha;
  end;
  BuscaPeriodo;
end;

function TCafeteria.AutorizaInvitacion: Boolean;
begin
  Result := False;
  PrepararComida4;

  with ChecadaComida do begin
    with oZetaProvider do begin
      ParamAsInteger(FComida4, 'Invitador', Invitador);
      ParamAsDate(FComida4, 'Fecha', Fecha);
      ParamAsString(FComida4, 'Hora', Hora);
      ParamAsString(FComida4, 'TipoComida', TipoComida);
      ParamAsInteger(FComida4, 'NumComidas', NumComidas);
    end;
  end;
  with FComida4 do begin
    Active := True;
    if Eof then begin
      with ChecadaComida do begin
        Empleado := 0;
        Respuesta := R_INVITACION_RECHAZADA;
      end;
    end else begin
      with ChecadaComida do begin
        Empleado := FieldByName('Empleado').AsInteger;
        case FieldByName('Autorizacion').AsInteger of
          0:
            Result := True;
          1:
            Respuesta := R_INVITADOR_NO_EXISTE;
          2:
            Respuesta := R_INVITADOR_SUSPENDIDO;
          3:
            Respuesta := R_INVITACION_RECHAZADA;
          4:
            Respuesta := R_INVITACION_YA_EXISTE;
          else
            Respuesta := R_INVITACION_RECHAZADA;
        end;
      end;
    end;
    Active := False;
  end;
end;

function TCafeteria.AutorizaEmpleado: Boolean;
begin
  Result := False;
  PrepararComida3;

  with ChecadaComida do begin
    with oZetaProvider do begin
      ParamAsInteger(FComida3, 'Empleado', Empleado);
      ParamAsDate(FComida3, 'Fecha', Fecha);
      ParamAsString(FComida3, 'Hora', Hora);
      ParamAsString(FComida3, 'Credencial', Credencial);
      {$IFDEF MULT_TIPO_COMIDA}
      ParamAsChar(FComida3, 'TipoComida', TipoComida, K_ANCHO_CODIGO1);
      {$ENDIF}
    end;
  end;
  with FComida3 do begin
    Active := True;
    with ChecadaComida do begin
      if Eof then
        Respuesta := R_RECHAZADA
      else begin
        case FieldByName('Autorizacion').AsInteger of
          0:
            Result := True;
          1:
            Respuesta := R_EMPLEADO_NO_EXISTE;
          2:
            Respuesta := Format(R_EMPLEADO_ES_BAJA, [FormatDateTime('dd/mmm/yyyy', FieldByName('FechaBaja').AsDateTime)]);
          3:
            Respuesta := R_EMPLEADO_CREDENCIAL_INCORRECTA;
          4: begin
              Result := True;
              Respuesta := R_COMIDA_YA_EXISTE;
            end;
          else
            Respuesta := R_RECHAZADA;
        end;
      end;
    end;
    Active := False;
  end;
end;

procedure TCafeteria.AgregaDato(const eTipo: eTipoKind; const sLetrero, sValor: String);
begin
  with Output do begin
    Append;
    FieldByName('AC_KIND').AsInteger := Ord(eTipo);
    FieldByName('AC_LABEL').AsString := Copy(sLetrero, 1, K_ANCHO_LETRERO);
    FieldByName('AC_VALUE').AsString := Copy(sValor, 1, K_ANCHO_VALUE);
    Post;
  end;
end;

procedure TCafeteria.AgregaEntero(const eTipo: eTipoKind; const sLetrero: String;
  const iValor: Integer);
begin
  AgregaDato(eTipo, sLetrero, IntToStr(iValor));
end;

procedure TCafeteria.AgregaFlotante(const eTipo: eTipoKind; const sLetrero: String;
  const rValor: Extended);
begin
  AgregaDato(eTipo, sLetrero, FormatFloat('0.00', rValor));
end;

procedure TCafeteria.AgregaReglaInformativa(const sLetrero, sValor: String);
begin
  if ZetaCommonTools.StrLleno(sValor) then begin
    AgregaDato(tkInfo, sLetrero, sValor);
  end;
end;

procedure TCafeteria.AgregaReglaEvaluacion(const sLetrero, sValor: String;
  const eSeveridad: eTipoBitacora; const lIsOk: Boolean);
begin
  with Output do begin
    if not lIsOk then begin
      Append;
      FieldByName('AC_KIND').AsInteger := Ord(tkEval);
      FieldByName('AC_TIPO').AsInteger := Ord(eSeveridad);
      FieldByName('AC_OK').AsBoolean := lIsOk;
      FieldByName('AC_LABEL').AsString := Copy(sLetrero, 1, K_ANCHO_LETRERO);
      FieldByName('AC_VALUE').AsString := Copy(sValor, 1, K_ANCHO_VALUE);
      Post;
    end;
  end;
end;

procedure TCafeteria.AgregaBlob(const eTipo: eTipoKind; const sLetrero: String;
  const oData: TBlobData);
begin
  with Output do begin
    Append;
    FieldByName('AC_KIND').AsInteger := Ord(eTipo);
    FieldByName('AC_LABEL').AsString := Copy(sLetrero, 1, K_ANCHO_LETRERO);
    TBlobField(FieldByName('AC_BLOB')).AsBytes := oData;
    Post;
  end;
end;

procedure TCafeteria.SetTipoChecadaAcceso;
var
  iAccesos: Integer;
begin
  PrepararAccessCount;

  with ChecadaAcceso do begin
    if TipoDesconocido then begin
      with oZetaProvider do begin
        ParamAsInteger(FAccessCount, 'Empleado', Empleado);
        ParamAsDate(FAccessCount, 'Fecha', Fecha);
      end;
      with FAccessCount do begin
        Active := True;
        if IsEmpty then
          iAccesos := 0
        else
          iAccesos := FieldByName('RESULTADO').AsInteger;
        Active := False;
      end;
      if System.Odd(iAccesos) then
        Tipo := chSalida
      else
        Tipo := chEntrada;
    end;
  end;
end;

procedure TCafeteria.AutorizaAcceso(Dataset: TDataset; var lExisteEmp: Boolean);
var
  sIncidencia: TCodigo;
  sHorario   : String;
  eStatus    : eStatusEmpleado;
begin
  FOutput := Dataset;
  if Assigned(FOutput) then begin
    with Output do begin
      Active := True;
    end;
    PreparaParametrosAccesos;
    with ChecadaAcceso do begin
      PrepararEmpleadoDatos;
      with FEmpleadoDatos do begin
        Active := False;
        with oZetaProvider do begin
          ParamAsInteger(FEmpleadoDatos, 'Empleado', Empleado);
          ParamAsDate(FEmpleadoDatos, 'Fecha', Fecha);
        end;
        Active := True;
        lExisteEmp := (not IsEmpty);
        if (not lExisteEmp) then begin
          Passed := False;
          Respuesta := R_EMPLEADO_NO_EXISTE;
        end else begin
          if ZetaCommonTools.StrVacio(Credencial) or
            (Credencial = FieldByName('CB_CREDENC').AsString) then
            Passed := True
          else begin
            Passed := False;
            Respuesta := R_EMPLEADO_CREDENCIAL_INCORRECTA;
          end;
          { Basta con que exista el empleado para que muestre los datos del mismo.
          if Passed then begin }
          if (FieldByName('EXISTE').AsInteger > 0) then begin
            Horario := FieldByName('HO_CODIGO').AsString;
            StatusDia := eStatusAusencia(FieldByName('AU_STATUS').AsInteger);
          end;

          PrepararHorarioDatos;
          oZetaProvider.ParamAsString(FHorarioDatos, 'Codigo', Horario);
          with FHorarioDatos do begin
            Active := True;
            if IsEmpty then
              sHorario := ''
            else
              sHorario := FieldByName('HO_DESCRIP').AsString;
            Active := False;
          end;
          eStatus := GetStatusEmpleado(Empleado, Fecha, sIncidencia);
          AgregaEntero(tkNumero, 'Empleado #', FieldByName('CB_CODIGO').AsInteger);
          AgregaDato(tkNombre, 'Nombre', FieldByName('PRETTYNAME').AsString);
          AgregaDato(tkEmpresa, 'Empresa', Self.Nombre);
          AgregaEntero(tkStatus, 'Status', Ord(eStatus));
          AgregaDato(tkHorario, 'Horario', sHorario);
          AgregaDato(tkTurno, 'Turno', FieldByName('TU_DESCRIP').AsString);
          if (FSupervisorNivel <> 0) then
            AgregaDato(tkSuper, FSupervisorNombre, FieldByName('SUP_NAME').AsString);
          AgregaBlob(tkFoto, 'Foto', FieldByName('FOTO').AsBytes);
          AgregaFlotante(tkExtras, 'Horas Extras', FieldByName('HRS_EXTRAS').AsFloat);
          AgregaFlotante(tkDescanso, 'Descanso', FieldByName('DESCANSO').AsFloat);
          AgregaFlotante(tkPermisoCG, 'Permiso C/G', FieldByName('PER_CG').AsFloat);
          AgregaFlotante(tkPermisoSG, 'Permiso S/G', FieldByName('PER_SG').AsFloat);
          AgregaFlotante(tkPermisoCGEntrada, 'Permiso C/G Entrada',
            FieldByName('PER_CG_ENT').AsFloat);
          AgregaFlotante(tkPermisoSGEntrada, 'Permiso S/G Entrada',
            FieldByName('PER_SG_ENT').AsFloat);
          with oZetaProvider.DatosComida do begin
            Fecha := ChecadaAcceso.Fecha;
            Hora := ChecadaAcceso.Hora;
            Horario := ChecadaAcceso.Horario;
            StatusDia := ChecadaAcceso.StatusDia;
            if ZetaCommonTools.StrVacio(Turno) then
              Turno := FieldByName('CB_TURNO').AsString;
            StatusEmpleado := eStatus;
          end;
        { end; }
        end;
        Active := False;
      end;
      if Passed then begin
        SetTipoChecadaAcceso;
        with oZetaProvider.DatosComida do begin
          TipoChecada := ChecadaAcceso.Tipo; // Para que se pueda evaluar en reglas
        end;
        if FReglasAcceso.Evaluar(ChecadaAcceso) then begin
          Respuesta := R_BIENVENIDO;
        end else begin
          Passed := False;
          Respuesta := R_RECHAZO;
        end;
      end;
    end;
  end;
end;

function TCafeteria.AutorizaComida: Boolean;
begin
  if (ChecadaComida.NumComidas < 0) then begin
    Result := True;
    ChecadaComida.Respuesta := R_CANCELACION;
  end else begin
    PreparaParametrosCafeteria;
    if ChecadaComida.EsInvitacion then begin
      Result := AutorizaInvitacion;
      if Result then
        FReglasCafe.EvaluarMsgExito(ChecadaComida);
    end else if AutorizaEmpleado then
      Result := FReglasCafe.Evaluar(ChecadaComida)
    else
      Result := False;
  end;
end;

procedure TCafeteria.EscribeAcceso;
begin
  PrepararAcceso1;
  with oZetaProvider do begin
    //EmpiezaTransaccion;
    try
      with ChecadaAcceso do begin
        ParamAsInteger(FAcceso1, 'CB_CODIGO', Empleado);
        ParamAsDate(FAcceso1, 'AL_FECHA', Fecha);
        ParamAsString(FAcceso1, 'AL_HORA', Hora);
        ParamAsBoolean(FAcceso1, 'AL_ENTRADA', EsEntrada);
        ParamAsBoolean(FAcceso1, 'AL_OK_SIST', Passed);
        ParamAsInteger(FAcceso1, 'AE_CODIGO', Regla);
        ParamAsString(FAcceso1, 'AL_CASETA', Caseta);
        Ejecuta(FAcceso1);
      end;
      //TerminaTransaccion(True);
    except
      on e: Exception do begin
        //TerminaTransaccion(False);
        if PK_VIOLATION(e) then begin
          with ChecadaAcceso do begin
            Passed := False;
            Respuesta := R_ACCESO_YA_EXISTE;
          end;
        end else
          raise;
      end;
    end;
  end;
end;

procedure TCafeteria.EscribeComida;
var
  sCar: String;
begin
  with oZetaProvider do begin
    //EmpiezaTransaccion;
    try
      with ChecadaComida do begin
        if EsInvitacion then begin
          PrepararComida2;
          ParamAsInteger(FComida2, 'IV_CODIGO', Invitador);
          ParamAsDate(FComida2, 'CF_FECHA', Fecha);
          ParamAsString(FComida2, 'CF_HORA', Hora);
          ParamAsString(FComida2, 'CF_TIPO', TipoComida);
          ParamAsFloat(FComida2, 'CF_COMIDAS', NumComidas);
          ParamAsString(FComida2, 'CF_RELOJ', Reloj);
          ParamAsBoolean(FComida2, 'CF_REG_EXT', Extras);
          ParamAsBoolean(FComida2, 'CF_OFFLINE', EsFueraLinea);
          Ejecuta(FComida2);
        end else begin
          PrepararComida1;
          ParamAsInteger(FComida1, 'CB_CODIGO', Empleado);
          ParamAsDate(FComida1, 'CF_FECHA', Fecha);
          ParamAsString(FComida1, 'CF_HORA', Hora);
          ParamAsString(FComida1, 'CF_TIPO', TipoComida);
          ParamAsFloat(FComida1, 'CF_COMIDAS', NumComidas);
          ParamAsString(FComida1, 'CF_RELOJ', Reloj);
          ParamAsInteger(FComida1, 'CL_CODIGO', Regla);
          ParamAsBoolean(FComida1, 'CF_REG_EXT', Extras);
          ParamAsBoolean(FComida1, 'CF_OFFLINE', EsFueraLinea);
          try
            Ejecuta(FComida1);
          except
            on e: Exception do begin
              if PK_VIOLATION(e) then begin
                PrepararComida5;
                ParamAsInteger(FComida5, 'NumComidas', NumComidas);
                ParamAsInteger(FComida5, 'Empleado', Empleado);
                ParamAsDate(FComida5, 'Fecha', Fecha);
                ParamAsString(FComida5, 'Hora', Hora);
                {$IFDEF MULT_TIPO_COMIDA}
                ParamAsChar(FComida5, 'TipoComida', TipoComida, K_ANCHO_CODIGO1);
                {$ENDIF}
                ParamAsBoolean(FComida5, 'CF_OFFLINE', EsFueraLinea);
                Ejecuta(FComida5);
              end else
                raise;
            end;
          end;
        end;
      end;
      //TerminaTransaccion(True);
    except
      on e: Exception do begin
        sCar := e.Message;
        //TerminaTransaccion(False);
        raise;
      end;
    end;
  end;
end;

{$IFDEF BOSE}

procedure TCafeteria.EscribeBitacoraError(const iEmpleado, iRegla, iSegundos, iStatus: Integer;
  const dFecha: TDate; const sHora, sEstacion, sMensaje: String; iTipo: Integer = 0);
var
  sCar     : String;
  FProvider: TdmZetaServerProvider;
  oCursor  : TZetaCursor;

begin
  FProvider := GetZetaProvider(NIL);
  try
    with FProvider do begin
      EmpresaActiva := Comparte;
      oCursor := CreateQuery(GetSQLScript(Q_AGREGA_BITACORA));
      try
        //EmpiezaTransaccion;
        try
                   // Pasar Parametros
          ParamAsInteger(oCursor, 'CB_CODIGO', iEmpleado);
          ParamAsDate(oCursor, 'BC_FECHA', dFecha);
          ParamAsString(oCursor, 'BC_HORA', sHora);
          ParamAsString(oCursor, 'BC_ESTACIO', sEstacion);
          ParamAsInteger(oCursor, 'BC_TIPO', iTipo);
          ParamAsInteger(oCursor, 'BC_REGLA', iRegla);
          ParamAsInteger(oCursor, 'BC_TIEMPO', iSegundos);
          ParamAsInteger(oCursor, 'BC_STATUS', iStatus);
          ParamAsString(oCursor, 'BC_MENSAJE', sMensaje);
          try
            Ejecuta(oCursor);
          except
            on e: Exception do begin
              raise;
            end;
          end; // try

          //TerminaTransaccion(True);
        except
          on e: Exception do begin
            sCar := e.Message;
            //TerminaTransaccion(False);
            raise;
          end;
        end;
      finally
        FreeAndNil(oCursor);
      end;
    end;
  finally
    FreeAndNil(FProvider);
  end;
end;
{$ENDIF}

procedure TCafeteria.GetDatosEmpleado(const iEmpleado: TNumEmp);
begin
  PrepararLeeEmpleado;

  with FLeeEmpleado do begin
    Active := False;
    {$IFDEF FIREBIRD}
    oZetaProvider.ParamAsIntegerLocate(FLeeEmpleado, 'Empleado', iEmpleado);
    {$ELSE}
    oZetaProvider.ParamAsInteger(FLeeEmpleado, 'Empleado', iEmpleado);
    {$ENDIF}
    Active := True;
  end;
end;

function TCafeteria.GetStatusEmpleado(const iEmpleado: TNumEmp; const dReferencia: TDate;
  var Incidencia: TCodigo): eStatusEmpleado;
begin
  {$IFDEF INTERBASE}
  with FEmpleadoStatus do begin
    Active := False;
    with oZetaProvider do begin
      ParamAsInteger(FEmpleadoStatus, 'Empleado', iEmpleado);
      ParamAsDate(FEmpleadoStatus, 'Fecha', dReferencia);
    end;
    Active := True;
    if Eof then begin
      Result := steAnterior;
      Incidencia := '';
    end else begin
      Result := eStatusEmpleado(FieldByName('RESULTADO').AsInteger);
      Incidencia := FieldByName('INCIDENCIA').AsString;
    end;
    Active := False;
  end;
  {$ENDIF}
  {$IFDEF MSSQL}
  PrepararEmpleadoStatus;
  with oZetaProvider do begin
    ParamAsInteger(FEmpleadoStatus, 'Empleado', iEmpleado);
    ParamAsDate(FEmpleadoStatus, 'Fecha', dReferencia);
    Ejecuta(FEmpleadoStatus);
    Result := eStatusEmpleado(GetParametro(FEmpleadoStatus, 'Resultado').AsInteger);
    Incidencia := GetParametro(FEmpleadoStatus, 'Incidencia').AsString;
  end;
  {$ENDIF}
end;

function TCafeteria.GetDatasetEmpleado: TZetaCursorLocate;
begin
  if ChecadaComida.EsInvitacion and not FUsaEmpleado then
    Result := FLeeInvitador
  else
    Result := FLeeEmpleado;
end;

function TCafeteria.GetDatasetHistorial: TZetaCursorLocate;
begin
  if ChecadaComida.EsInvitacion then
    Result := FLeeInvitaciones
  else
    Result := FLeeComidas;
end;

function TCafeteria.GetDatasetFotoEmp: TZetaCursorLocate;
begin
    Result := FLeeFotoEmp;
end;

procedure TCafeteria.BuscaPeriodo;
begin
  if (FFecha < FInicial) or (FFecha > FFinal) then begin
    PrepararPeriodo; //AV: Siempre prepara el query cuando se usa

    with oZetaProvider do begin
      ParamAsInteger(FPeriodo, 'Year', ZetaCommonTools.TheYear(FFecha));
      ParamAsInteger(FPeriodo, 'Tipo', Ord(FTipoPeriodo));
      ParamAsDate(FPeriodo, 'Fecha', FFecha);
      ParamAsDate(FPeriodo, 'Fecha1', FFecha);
    end;
    with FPeriodo do begin
      Active := True;
      if Eof then begin
        FInicial := FFecha;
        FFinal := FFecha;
      end else begin
        FInicial := Trunc(Fields[0].AsDateTime);
        FFinal := Trunc(Fields[1].AsDateTime);
      end;
      with oZetaProvider do begin
        with ParamList do begin
          AddInteger('Year', FieldByName('PE_YEAR').AsInteger);
          AddInteger('Tipo', Ord(FTipoPeriodo));
          AddInteger('Numero', FieldByName('PE_NUMERO').AsInteger);
        end;
        GetDatosPeriodo;
      end;
      Active := False;
    end;
  end;
end;

procedure TCafeteria.BuildDatasetsCafe(const lComidas, lMuestraFoto: Boolean);

  procedure TraeFotoEmpleado;
  begin
    PrepararLeeFotoEmp;
    with FLeeFotoEmp do begin
      Active := False;
      with oZetaProvider do begin
        {$IFDEF FIREBIRD}
        ParamAsIntegerLocate(FLeeFotoEmp, 'Empleado', ChecadaComida.Empleado);
        {$ELSE}
        ParamAsInteger(FLeeFotoEmp, 'Empleado', ChecadaComida.Empleado);
        {$ENDIF}
      end;
      Active := True;
    end;
  end;

begin
  with ChecadaComida do begin
    if EsInvitacion then
    begin
      PrepararLeeInvitador;
      with FLeeInvitador do
      begin
        Active := False;
        {$IFDEF FIREBIRD}
        oZetaProvider.ParamAsIntegerLocate(FLeeInvitador, 'Invitador', Invitador);
        {$ELSE}
        oZetaProvider.ParamAsInteger(FLeeInvitador, 'Invitador', Invitador);
        {$ENDIF}
        Active := True;
        with FieldByName('Empleado') do
        begin
          FUsaEmpleado := (AsInteger > 0);
          if FUsaEmpleado then
          begin
            GetDatosEmpleado(AsInteger);
            if (lMuestraFoto) then
                TraeFotoEmpleado;
          end;
        end;
      end;
      if lComidas then
      begin

        PrepararLeeInvitaciones;

        with FLeeInvitaciones do begin
          Active := False;
          with oZetaProvider do begin
            {$IFDEF FIREBIRD}
            ParamAsIntegerLocate(FLeeInvitaciones, 'Invitador', Invitador);
            ParamAsDateLocate(FLeeInvitaciones, 'Inicio', PeriodoInicio);
            ParamAsDateLocate(FLeeInvitaciones, 'Fin', PeriodoFin);
            {$ELSE}
            ParamAsInteger(FLeeInvitaciones, 'Invitador', Invitador);
            ParamAsDate(FLeeInvitaciones, 'Inicio', PeriodoInicio);
            ParamAsDate(FLeeInvitaciones, 'Fin', PeriodoFin);
            {$ENDIF}
          end;
          Active := True;
        end;
      end;
    end
    else
    begin
      FUsaEmpleado := True;
      GetDatosEmpleado(Empleado);
      if lComidas then begin
        with FLeeComidas do begin
          PrepararLeeComidas;
          Active := False;
          with oZetaProvider do begin
            {$IFDEF FIREBIRD}
            ParamAsIntegerLocate(FLeeComidas, 'Empleado', Empleado);
            ParamAsDateLocate(FLeeComidas, 'Inicio', PeriodoInicio);
            ParamAsDateLocate(FLeeComidas, 'Fin', PeriodoFin);
            {$ELSE}
            ParamAsInteger(FLeeComidas, 'Empleado', Empleado);
            ParamAsDate(FLeeComidas, 'Inicio', PeriodoInicio);
            ParamAsDate(FLeeComidas, 'Fin', PeriodoFin);
            {$ENDIF}
          end;
          Active := True;
        end;
      end;
      if (lMuestraFoto) then
      begin
          TraeFotoEmpleado;
      end;
    end;
  end;
end;

procedure TCafeteria.BuildTotales(const dInicial, dFinal: TDateTime; const sTipos: String; const sReloj: TReloj; Dataset: TDataset);

  procedure AgregaTotales(const sTipo: String; const iComidas, iExtras: Integer);
  begin
    with Dataset do begin
      if Locate('CF_TIPO', sTipo, [loCaseInsensitive]) then
        Edit
      else begin
        Append;
        FieldByName('CF_TIPO').AsString := sTipo;
      end;
      with FieldByName('CF_COMIDAS') do begin
        AsInteger := AsInteger + iComidas;
      end;
      with FieldByName('CF_EXTRAS') do begin
        AsInteger := AsInteger + iExtras;
      end;
      Post;
    end;
  end;

  procedure SumaDatasets(const eScript: eSQL);
  var
    FTotales: TZetaCursor;
    I: Integer;
  begin
    with oZetaProvider do begin
      FTotales := CreateQuery(Format(GetSQLScript(eScript), [sTipos]));
      try
        ParamAsString(FTotales, 'Reloj', Copy(sReloj, 1, 4));
        ParamAsDate(FTotales, 'FechaInicial', dInicial);
        ParamAsDate(FTotales, 'FechaFinal', dFinal);
        with FTotales do begin
          Active := True;
          if FieldDefs.Count <> DataSet.FieldDefs.Count then begin
            DataSet.Close;
            CopyStructure(FTotales, DataSet);
            for I := 0 to DataSet.FieldCount - 1 do
              DataSet.Fields[I].ReadOnly := False;
            TClientDataSet(DataSet).CreateDataSet;
          end;
          while not Eof do begin
            AgregaTotales(FieldByName('CF_TIPO').AsString, FieldByName('CF_COMIDAS').AsInteger, FieldByName('CF_EXTRAS').AsInteger);
            Next;
          end;
          Active := False;
        end;
      finally
        FTotales.Free;
      end;
    end;
  end;

begin
  SumaDatasets(Q_TOT_COMIDAS);
  SumaDatasets(Q_TOT_INVITACIONES);
end;

procedure TCafeteria.BuildDetalles(const dInicial, dFinal: TDateTime; const sTipos: String; const sReloj: TReloj; Dataset: TDataset);
var
  FDetalle: TZetaCursor;
  I: Integer;

  function GetCodigo(const iCodigo, iClase: Integer): String;
  begin
    case iClase of
      0:
        Result := ZetaCommonTools.PadL(IntToStr(iCodigo), 9);
      1:
        Result := K_PREFIJO_INVITADOR + ZetaCommonTools.PadL(IntToStr(iCodigo), 5);
    end;
  end;

  procedure AgregaDetalle(const sCodigo, sNombre, sTipo, sHora: String; const iComidas, iExtras: Integer; const dFecha: TDate);
  begin
    with Dataset do begin
      Append;
      FieldByName('CB_CODIGO').AsString := sCodigo;
      FieldByName('CF_FECHA').AsDateTime := dFecha;
      FieldByName('CF_HORA').AsString := sHora;
      FieldByName('CF_COMIDAS').AsInteger := iComidas;
      FieldByName('CF_EXTRAS').AsInteger := iExtras;
      FieldByName('CF_TIPO').AsString := sTipo;
      FieldByName('PRETTYNAME').AsString := sNombre;
      Post;
    end;
  end;

begin
  with oZetaProvider do begin
    FDetalle := CreateQuery(Format(GetSQLScript(Q_DETALLE_COMIDAS), [sTipos, sTipos]));
    try
      ParamAsString(FDetalle, 'Reloj1', Copy(sReloj, 1, 4));
      ParamAsString(FDetalle, 'Reloj2', Copy(sReloj, 1, 4));
      ParamAsDate(FDetalle, 'Inicial1', dInicial);
      ParamAsDate(FDetalle, 'Inicial2', dInicial);
      ParamAsDate(FDetalle, 'Final1', dFinal);
      ParamAsDate(FDetalle, 'Final2', dFinal);
      with FDetalle do begin
        Active := True;
        if FieldDefs.Count <> DataSet.FieldDefs.Count then begin
          DataSet.Close;
          CopyStructure(FDetalle, DataSet, False);
          for I := 0 to DataSet.FieldCount - 1 do
            DataSet.Fields[I].ReadOnly := False;
          TClientDataSet(DataSet).CreateDataSet;
        end;
        while not Eof do begin
          AgregaDetalle(GetCodigo(FieldByName('CB_CODIGO').AsInteger, FieldByName('CF_CLASE').AsInteger), FieldByName('PRETTYNAME').AsString,
            FieldByName('CF_TIPO').AsString, FieldByName('CF_HORA').AsString, FieldByName('CF_COMIDAS').AsInteger, FieldByName('CF_EXTRAS').AsInteger,
            FieldByName('CF_FECHA').AsDateTime);
          Next;
        end;
        Active := False;
      end;
    finally
      FDetalle.Free;
    end;
  end;
end;

function TCafeteria.TieneDigito: Boolean;
begin
  Result := StrLleno(Digito);
end;

{$IFNDEF INTERBASE}

// BIOMETRICO
function TCafeteria.GetDatasetHuellas: TZetaCursorLocate;
begin
  //PrepararObtieneHuellas; //AV: Siempre prepara el query
  Result := FObtieneHuellas;
end;

{ ******** THuella ******** } // BIOMETRICO
procedure THuella.Init;
begin
  FNumeroBiometrico := 0;
  FEmpresa := VACIO;
  FEmpleado := 0;
  FIndice := 0;
  FHuella := VACIO;
  FInvitador := 0;
end;
{$ENDIF}

end.
