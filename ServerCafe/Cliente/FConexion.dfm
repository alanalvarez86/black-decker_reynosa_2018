inherited Conexion: TConexion
  Left = 922
  Top = 170
  Caption = 'Configuraci'#243'n de Conexi'#243'n'
  ClientHeight = 150
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 114
    BevelOuter = bvNone
    ExplicitTop = 114
    inherited OK_DevEx: TcxButton
      OnClick = OKClick
    end
  end
  object GroupBox1: TGroupBox [1]
    Left = 8
    Top = 3
    Width = 359
    Height = 105
    TabOrder = 1
    object Label5: TLabel
      Left = 151
      Top = 47
      Width = 122
      Height = 13
      Caption = 'Valores v'#225'lidos 1 a 65535'
    end
    object Label2: TLabel
      Left = 30
      Top = 47
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Puerto TCP'
    end
    object lblServidor: TLabel
      Left = 40
      Top = 20
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Direcci'#243'n'
    end
    object lblSegundos: TLabel
      Left = 151
      Top = 74
      Width = 46
      Height = 13
      Caption = 'segundos'
    end
    object lblIntervalo: TLabel
      Left = 12
      Top = 74
      Width = 73
      Height = 13
      Alignment = taRightJustify
      Caption = 'Actualizar cada'
    end
    object edtServidor: TZetaEdit
      Left = 94
      Top = 16
      Width = 251
      Height = 21
      TabOrder = 0
      Text = 'localhost'
    end
    object edtIntervalo: TZetaNumero
      Left = 94
      Top = 70
      Width = 51
      Height = 21
      Mascara = mnDias
      TabOrder = 2
      Text = '0'
    end
    object edtPuerto: TZetaNumero
      Left = 94
      Top = 43
      Width = 51
      Height = 21
      Mascara = mnDias
      TabOrder = 1
      Text = '0'
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
