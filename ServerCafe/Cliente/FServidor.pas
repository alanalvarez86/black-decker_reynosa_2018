unit FServidor;

interface

uses
  ZBaseDlgModal_DevEx, Vcl.Forms, Vcl.StdCtrls, Vcl.Buttons, System.Classes, Vcl.Controls, Vcl.ExtCtrls, ZetaHora, Vcl.Mask, ZetaNumero,
  ZBaseDlgModal, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, TressMorado2013, Vcl.ImgList, cxButtons;

type
  TServidor = class(TZetaDlgModal_DevEx)
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    edtPuerto: TZetaNumero;
    edtHora: TZetaHora;
    Reglas_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure btnReglasClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    class function Execute: Boolean;
  end;

var
  Servidor: TServidor;

implementation

uses
  SysUtils, ZetaDialogo, CafeteraConsts, {$IFDEF SERVERCAFE}FCafetera{$ELSE}DServerCafeUI{$ENDIF};
{$R *.dfm}

{ TForm1 }

class function TServidor.Execute: Boolean;
begin
  with TServidor.Create(Application) do begin
    Result := ShowModal = mrOk;
    Free;
  end;
end;

procedure TServidor.FormCreate(Sender: TObject);
begin
  inherited;
  {$IFDEF SERVERCAFE}
  edtPuerto.Valor := CafeServer.PortNumber;
  edtHora.Valor   := FormatDateTime('hhnn', CafeServer.GetGlobal(K_IDENT_HORA_REINICIO, K_CAFETERA_HORA_REINICIO));
  {$ELSE}
  edtPuerto.Valor := ServerCafeUI.Puerto;
  edtHora.Valor   := FormatDateTime('hhnn', ServerCafeUI.Reinicio);
  {$ENDIF}
end;

procedure TServidor.OKClick(Sender: TObject);
begin
  inherited;
  if not((edtPuerto.ValorEntero > 0) and (edtPuerto.ValorEntero < 65536)) then begin
    zError(Caption, 'El Puerto TCP no est� en el rango permitido.', 0);
    edtPuerto.SetFocus;
  end else begin
    {$IFDEF SERVERCAFE}
    CafeServer.SetGlobal(K_IDENT_PUERTO, edtPuerto.ValorEntero);
    CafeServer.SetGlobal(K_IDENT_HORA_REINICIO, edtHora.HourValue);
    {$ELSE}
    ServerCafeUI.SetParametros(edtPuerto.ValorEntero, edtHora.HourValue);
    {$ENDIF}
  end;
end;

procedure TServidor.btnReglasClick(Sender: TObject);
begin
  inherited;
  {$IFDEF SERVERCAFE}
  CafeServer.Start;//CafeServer.Restart;
  {$ELSE}
  ServerCafeUI.Restart
  {$ENDIF}
end;

end.
