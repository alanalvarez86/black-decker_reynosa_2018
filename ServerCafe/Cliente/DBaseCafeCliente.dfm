object dmBaseCafeCliente: TdmBaseCafeCliente
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 180
  Width = 287
  object HTTPRIO: THTTPRIO
    HTTPWebNode.Agent = 'Borland SOAP 1.2'
    HTTPWebNode.UseUTF8InHeader = True
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    HTTPWebNode.WebNodeOptions = []
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 24
    Top = 24
  end
  object cdsEmpleado: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 24
  end
  object ConnTimer: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = ConnTimerTimer
    Left = 24
    Top = 80
  end
end
