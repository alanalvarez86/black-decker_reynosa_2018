unit FBaseServerCafeUI;

interface

uses
  SysUtils, Types, Vcl.Forms, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Buttons, Vcl.Controls, System.Classes, Vcl.ImgList,
  System.Actions, Vcl.ActnList, Vcl.StdActns, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxBarBuiltInMenu, cxControls, cxContainer,
  cxEdit, cxClasses, dxSkinsForm, cxTextEdit, cxMemo, cxPC, cxButtons;

type
  TBaseServerCafeUI = class(TForm)
    PanelInferior: TPanel;
    StatusBar: TStatusBar;
    ImageList: TImageList;
    ActionList: TActionList;
    actConfigServidor: TAction;
    actSalir: TFileExit;
    cxImageList24_PanelBotones: TcxImageList;
    Salir_DevEx: TcxButton;
    Servidor_DevEx: TcxButton;
    DevEx_SkinController: TdxSkinController;
    PageControl_DevEx: TcxPageControl;
    Bitacora_DevEx: TcxTabSheet;
    Errores_DevEx: TcxMemo;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure actConfigServidorExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  protected
    procedure Init; virtual; abstract;
  public
    { Public declarations }
  end;

var
  BaseServerCafeUI: TBaseServerCafeUI;

implementation

uses
  CafeteraConsts, ZetaHelpCafe, FServidor, Dialogs, ZetaDialogo, ZetaRegistryCliente;

{$R *.dfm}

procedure TBaseServerCafeUI.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := ZConfirm('Confirmaci�n', '�Desea cerrar la aplicaci�n?', 0, mbNo)
end;

procedure TBaseServerCafeUI.FormCreate(Sender: TObject);
begin
  Application.Title := SERVICE_DISPLAYNAME;
  Caption           := Application.Title;
  HelpContext       := H00009_Ejecutando_manualmente_el_programa_Servidor;
  ZetaRegistryCliente.InitClientRegistry;
end;

procedure TBaseServerCafeUI.FormDestroy(Sender: TObject);
begin
     ZetaRegistryCliente.ClearClientRegistry;
end;

procedure TBaseServerCafeUI.actConfigServidorExecute(Sender: TObject);
begin
  TServidor.Execute;
end;

end.
