// Fuente: http://delphi.about.com/od/vclusing/a/delphi-grab-bar-splitter.htm
unit GrabSplitter;

interface

uses
  Math, Windows, Types, Messages, Graphics, Controls, ExtCtrls;

type

  TSplitter = class(ExtCtrls.TSplitter)
  private
    fMouseIn        : Boolean;
    property MouseIn: Boolean read fMouseIn;
    procedure CMMouseEnter(var AMsg: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var AMsg: TMessage); message CM_MOUSELEAVE;
  protected
    procedure Paint; override;
  end;

implementation

procedure TSplitter.CMMouseEnter(var AMsg: TMessage);
begin
  fMouseIn := True;
  Refresh;
end;

procedure TSplitter.CMMouseLeave(var AMsg: TMessage);
begin
  fMouseIn := False;
  Refresh;
end;

procedure TSplitter.Paint;
const
  BM_SIZE  = 5;
  BM_START = 45;
  BM_END   = BM_START + 180;
var
  R     : TRect;
  X, Y  : Integer;
  DX, DY: Integer;
  i     : Integer;
  Buffer: TBitmap;
  dTo   : Integer;
  Center: Integer;
  Radius: Integer;
  Sin   : Extended;
  Cos   : Extended;
  Color1: TColor;
  Color2: TColor;
begin
  // inherited NO
  // Beveled property = FALSE (never mind what has been set)

  R := ClientRect;

  Canvas.Brush.Color := Color;
  Canvas.FillRect(ClientRect);

  X := (R.Left + R.Right) div 2;
  Y := (R.Top  + R.Bottom) div 2;

  if (Align in [alLeft, alRight]) then begin
    DX := 0;
    DY := BM_SIZE + 1;
    X := X - (BM_SIZE div 2);
  end else begin
    DX := BM_SIZE + 1;
    DY := 0;
    Y  := Y - (BM_SIZE div 2);
  end;

  Buffer := TBitmap.Create;
  Center := (BM_SIZE div 2);
  Radius := (BM_SIZE div 2);
  if BM_SIZE mod 2 = 0 then begin
    Dec(Radius);
    Dec(Center);
  end;
  try
    Buffer.SetSize(BM_SIZE, BM_SIZE);
    Buffer.Canvas.Brush.Color := Color;
    Buffer.Canvas.FillRect(Rect(0, 0, BM_SIZE, BM_SIZE));

    if MouseIn then begin
      Color1 := clBtnShadow;
      Color2 := clBtnHighlight;
      dTo := 5;
    end else begin
      Color1 := clBtnHighlight;
      Color2 := clBtnShadow;
      dTo := 5;
    end;

    dec(X, (DX * dTo) div 2);
    dec(Y, (DY * dTo) div 2);

    SinCos((BM_START * 2 * Pi) / 360, Sin, Cos);
    Buffer.Canvas.MoveTo(Center + Round(Radius * Cos), Center - Round(Radius * Sin));

    Buffer.Canvas.Pen.Color := Color1;
    Buffer.Canvas.AngleArc(Center, Center, Radius, BM_START, 180);

    Buffer.Canvas.Pen.Color := Color2;
    Buffer.Canvas.AngleArc(Center, Center, Radius, BM_END, 180);

    for i := 1 to dTo do begin
      Canvas.Draw(X, Y, Buffer);
      inc(X, DX);
      inc(Y, DY);
    end;
  finally
    Buffer.Free;
  end;
end;

end.
