unit FSupuestosRH_DevEx;

interface

uses
  Windows, Messages, SysUtils, {$ifndef VER130}Variants,{$endif} Classes, Graphics, Controls, Forms,
  Dialogs,  DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  ZetaKeyLookup_DevEx;

type
  TSupuestosRH_DevEx = class(TBaseGridLectura_DevEx)
    PanelBoton: TPanel;
    Escenarios: TZetaKeyLookup_DevEx;
    lblEscenario: TLabel;
    SR_FECHA: TcxGridDBColumn;
    SR_TIPO: TcxGridDBColumn;
    EV_CODIGO: TcxGridDBColumn;
    SR_CUANTOS: TcxGridDBColumn;
    DESCRIPCION: TcxGridDBColumn;
    PRIORIDAD: TcxGridDBColumn;
    US_DESCRIP: TcxGridDBColumn;
 {   procedure ZetaDBGridDBTableViewDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
  }  procedure FormCreate(Sender: TObject);
    procedure EscenariosValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean); // (JB) Escenarios de presupuestos T1060 CR1872
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
    // (JB) Escenarios de presupuestos T1060 CR1872
	procedure DisConnect;override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;    
  public
    { Public declarations }
  end;

var
  SupuestosRH_DevEx: TSupuestosRH_DevEx;

implementation

uses dCatalogos, dSistema, DPresupuestos,
     ZetaCommonTools,
     ZAccesosTress,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.dfm}

procedure TSupuestosRH_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos:=  D_PRESUP_SUPUESTO_PER;
     HelpContext := H_SUPUESTOS_RH;
end;

// (JB) Escenarios de presupuestos T1060 CR1872
procedure TSupuestosRH_DevEx.Connect;
begin
     with dmPresupuestos do
     begin
          // (JB) Primero conecta el ZetaKeyLookup para despues filtrar
          with cdsEscenarios do
          begin
               Conectar;
               // Unicamente cuando el Lookup no tiene nada le asignara el ES_CODIGO que tiene el CDS.
               if ( strVacio( Escenarios.Llave ) ) then
                   Escenarios.Llave := FieldByName('ES_CODIGO').AsString;
          end;
          // (JB) Listo para filtrar!

          dmCatalogos.cdsEventos.Conectar;
          dmSistema.cdsUsuarios.Conectar;

          cdsEventosAltas.Conectar;
          cdsSupuestosRH.Conectar;

          // (JB) Es importante siempre asegurarse que la forma este conectando sus componentes a
          //      los datos.
          DataSource.DataSet:= cdsSupuestosRH;
          Escenarios.lookupdataset := cdsEscenarios;

          EscenarioActivo := Escenarios.Llave;
          RefrescarSupuestosRH( Escenarios.Llave );
     end;
end;

procedure TSupuestosRH_DevEx.Refresh;
begin
     dmPresupuestos.cdsSupuestosRH.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSupuestosRH_DevEx.Agregar;
begin
     dmPresupuestos.cdsSupuestosRH.Agregar;
end;

procedure TSupuestosRH_DevEx.Borrar;
begin
     dmPresupuestos.cdsSupuestosRH.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSupuestosRH_DevEx.Modificar;
begin
     dmPresupuestos.cdsSupuestosRH.Modificar;
end;

{
procedure TSupuestosRH_DevEx.ZetaDBGridDBTableViewDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
          State: TGridDrawState);
begin
     with ZetaDBGridDBTableView do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Font.Color := clWhite;
                    Brush.Color := clNavy;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    with Font do
                    begin
                         if zStrToBool( dmPresupuestos.cdsSupuestosRH.FieldByName( 'ACTIVO' ).AsString ) then
                            Color := ZetaDBGridDBTableView.Font.Color
                         else
                             Color := clRed;
                    end;
               end;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;
 }
// (JB) Escenarios de presupuestos T1060 CR1872
procedure TSupuestosRH_DevEx.DisConnect;
begin
     inherited;
     Escenarios.LookUpDataset := NIL;
end;

//(JB) Escenarios de presupuestos T1060 CR1872
procedure TSupuestosRH_DevEx.EscenariosValidKey(Sender: TObject);
begin
     inherited;
     // Refresh;
     dmPresupuestos.EscenarioActivo := Escenarios.Llave;
     dmPresupuestos.RefrescarSupuestosRH( Escenarios.Llave );
end;

//(JB) Escenarios de presupuestos T1060 CR1872
function TSupuestosRH_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar(sMensaje);
     if ( Result ) then
     begin
          if strVacio( Escenarios.Llave ) then
          begin
               Result := False;
               sMensaje := 'El C�digo de Escenario no puede quedar Vac�o.';
          end;

     end; 
end;

procedure TSupuestosRH_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('EV_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSupuestosRH_DevEx.ZetaDBGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;

    with ZetaDBGridDBTableView do
          begin
               with ACanvas do
               begin
                    Brush.Color := clWhite;
                    with Font do
                    begin
                         if zStrToBool( dmPresupuestos.cdsSupuestosRH.FieldByName( 'ACTIVO' ).AsString ) then
                            Color := ACanvas.Font.Color
                        else
                            Color := clRed;
                    end;
               end;
          end;
end;

end.
