unit FProcPresupuestos;
                  
interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, ToolWin, Db, ExtCtrls, ImgList,
     ZBaseConsulta,
     ZBaseConsultaBotones;

type
  TProcPresupuestos = class(TBaseBotones)
    DefinirPeriodos: TToolButton;
    SimulacionPresupuesto: TToolButton;
    ImprimeSimulacion: TToolButton;
    ToolButton1: TToolButton;
    GenerarPoliza: TToolButton;
    ImprimirPoliza: TToolButton;
    ExportarPoliza: TToolButton;
    PrepararPresupuestos: TToolButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ProcPresupuestos: TProcPresupuestos;

implementation

{$R *.DFM}

uses FTressShell,
     ZetaCommonClasses;

{ TProcEmpleado }

procedure TProcPresupuestos.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H10016_Procesos_empleados;
end;

end.
