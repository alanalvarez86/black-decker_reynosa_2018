inherited GridEscenario_DevEx: TGridEscenario_DevEx
  Left = 457
  Top = 530
  Caption = 'Escenarios'
  ClientHeight = 171
  ClientWidth = 812
  ExplicitWidth = 812
  ExplicitHeight = 171
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 812
    ExplicitWidth = 812
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 553
      ExplicitWidth = 553
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 547
        ExplicitLeft = 824
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 812
    Height = 152
    ExplicitWidth = 812
    ExplicitHeight = 152
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object ES_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'ES_CODIGO'
      end
      object ES_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'ES_ELEMENT'
      end
      object ES_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'ES_INGLES'
      end
      object ES_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'ES_NUMERO'
      end
      object ES_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'ES_TEXTO'
      end
      object LLAVE: TcxGridDBColumn
        Caption = 'Calcular'
        DataBinding.FieldName = 'LLAVE'
        Visible = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
