inherited CatEventosAltas_DevEx: TCatEventosAltas_DevEx
  Left = 205
  Top = 169
  Caption = 'Contrataciones'
  ClientHeight = 161
  ClientWidth = 530
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 530
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 204
      inherited textoValorActivo2: TLabel
        Width = 198
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 530
    Height = 142
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object EV_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'EV_CODIGO'
      end
      object EV_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'EV_DESCRIP'
      end
      object EV_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'EV_ACTIVO'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 16
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 2097504
  end
  inherited ActionList: TActionList
    Left = 314
    Top = 32
  end
  inherited PopupMenu1: TPopupMenu
    Left = 280
    Top = 32
  end
end
