inherited WizPresupSimulacion_DevEx: TWizPresupSimulacion_DevEx
  Left = 517
  Top = 98
  Caption = 'Simulaci'#243'n de presupuesto'
  ClientHeight = 583
  ClientWidth = 515
  ExplicitWidth = 521
  ExplicitHeight = 611
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 515
    Height = 583
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000017352474200AECE1CE90000000467414D410000B18F0BFC61050000
      00097048597300000EBC00000EBC0195BC724900000293494441545847ED5721
      701341148D40202A100804025181402010880A4405B2A202518140202B100866
      960BA2C9EE6522E80CA21281405454542010880A4404A2A222A2A20251515151
      41DFFBFD7BB3D9DD5C72493A83C89BF99DCBFEDDF7DFFDFFF776DB9A05C6983B
      CEB857AE70476561FF5863DFF54DFF9EBA6F0F0CC26065E14E61FF427385BD28
      DBAEDF33BD559DBE389094E412240A0CBB8C7E5F2133FBC8D00B5D3E3B4A53AE
      918CA451904B8CEFC1FF18761FCF1F21EE6F34873640C6B65832A59C0C5F5F2C
      FE1D91D1CE9CE97E60509D5E01EBEEC2F716428E9BACAB50575F365A69EC6B06
      D1E9B500CF4B64E520E541B9DAE517664EA7DEA0D7766E4C7D0F49A6D31AE3A6
      77EC67F0C47D02B3DFE58590EE27A9D30D39AE3C7383A9076752526954FE891D
      6A43BCFDF63CFB5B021BFB1E5C6711B7584E00278E74FC2CFB9B35965A67B7A8
      3DF1BF1301DC4EC19E3FF7E381FFA0AE27E05BCF359F7081B36F761EB199ABF1
      9C00E5925D312E7D203C2E4DF70D9B88DB1681B7303EC8CC3B611977CDEE8AD2
      223B530AF0601006833F0900A3B85C7D07BA6D930F50630121505BFF650C8379
      9BEA133C97000F6932EC63BF8ECF1DD379A8EE5A2C44001112F1598727622960
      296029E0FF16C0134CE74DC42C02F869468C4F7E9D08E89AEEB38A88833C2EF5
      A0D175593411E0CF13E10E635180A82ADC8FD0214EDC725DDBEDE0347BA03C23
      9846002F247219CD1F5887232FD9339DE7FA7D8FAFE0F86DBFD1AF5305750220
      FA29FA690FBEE442C21831D708E4D230FE427284B46D4AD6320290E60DACFB15
      AEA1918B17DF690F2C815ED3B741308C09390631D5CD07CFFB0892FC3F80B1E4
      42D2187C5BBE35DF3E0E30DEEC4F66836B956631A8E9131AC7BEB2FE3AFDF610
      F609AC76B7D4A3D5BA06F0AE9C05E7C13EE20000000049454E44AE426082}
    ExplicitWidth = 515
    ExplicitHeight = 583
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que se encarga de aplicar los supuestos, y calcula todas' +
        ' las n'#243'minas que se tengan configuradas.'
      Header.Title = 'Simulaci'#243'n de Presupuestos'
      ExplicitWidth = 493
      ExplicitHeight = 443
      object iYearLBL: TLabel
        Left = 158
        Top = 99
        Width = 22
        Height = 13
        Alignment = taRightJustify
        Caption = 'A'#241'o:'
        FocusControl = YearPresup
        Transparent = True
      end
      object YearPresup: TZetaNumero
        Left = 182
        Top = 96
        Width = 41
        Height = 21
        Mascara = mnDias
        TabOrder = 1
        Text = '0'
        OnExit = YearPresupExit
      end
      object GBSupuestosRH: TcxGroupBox
        Left = 86
        Top = 284
        Hint = ''
        Caption = ' Aplicar supuestos de kardex '
        TabOrder = 3
        Height = 146
        Width = 323
        object LblFechaRH1: TLabel
          Left = 61
          Top = 70
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicial:'
          Enabled = False
          Transparent = True
        end
        object LblFechaRH2: TLabel
          Left = 66
          Top = 102
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = 'Final:'
          Enabled = False
          Transparent = True
        end
        object RBTodosSupuestos: TcxRadioButton
          Left = 38
          Top = 19
          Width = 113
          Height = 17
          Caption = 'Todos'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = RBTodosSupuestosClick
          Transparent = True
        end
        object RBRangoSupuestos: TcxRadioButton
          Left = 38
          Top = 45
          Width = 120
          Height = 17
          Caption = 'Rango de fechas'
          TabOrder = 1
          OnClick = RBTodosSupuestosClick
          Transparent = True
        end
        object FechaRH1: TZetaFecha
          Left = 94
          Top = 66
          Width = 115
          Height = 22
          Cursor = crArrow
          Enabled = False
          TabOrder = 2
          Text = '21/oct/98'
          Valor = 36089.000000000000000000
        end
        object FechaRH2: TZetaFecha
          Left = 94
          Top = 95
          Width = 115
          Height = 22
          Cursor = crArrow
          Enabled = False
          TabOrder = 3
          Text = '21/oct/98'
          Valor = 36089.000000000000000000
        end
      end
      object GroupBox1: TcxGroupBox
        Left = 86
        Top = 124
        Hint = ''
        Caption = ' C'#225'lculo de N'#243'minas '
        TabOrder = 2
        Height = 146
        Width = 321
        object lblMesInicial: TLabel
          Left = 40
          Top = 79
          Width = 52
          Height = 13
          Alignment = taRightJustify
          Caption = 'Mes inicial:'
          Enabled = False
          FocusControl = MesInicial
          Transparent = True
        end
        object lblMesFinal: TLabel
          Left = 47
          Top = 104
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Mes final:'
          Enabled = False
          FocusControl = MesFinal
          Transparent = True
        end
        object MesInicial: TZetaKeyCombo
          Left = 94
          Top = 75
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          Enabled = False
          ParentCtl3D = False
          TabOrder = 2
          OnChange = MesesChange
          ListaFija = lfMes13
          ListaVariable = lvPuesto
          Offset = 1
          Opcional = False
          EsconderVacios = False
        end
        object MesFinal: TZetaKeyCombo
          Left = 94
          Top = 100
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          Enabled = False
          ParentCtl3D = False
          TabOrder = 3
          OnChange = MesesChange
          ListaFija = lfMes13
          ListaVariable = lvPuesto
          Offset = 1
          Opcional = False
          EsconderVacios = False
        end
        object RBRangoNominas: TcxRadioButton
          Left = 38
          Top = 19
          Width = 113
          Height = 17
          Caption = 'Todo el a'#241'o'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = RBRangoNominasClick
          Transparent = True
        end
        object RBRangoMeses: TcxRadioButton
          Left = 38
          Top = 45
          Width = 120
          Height = 17
          Caption = 'Rango de meses'
          TabOrder = 1
          OnClick = RBRangoNominasClick
          Transparent = True
        end
      end
      object GroupBox2: TcxGroupBox
        Left = 84
        Top = 13
        Hint = ''
        Caption = 'Escenario'
        TabOrder = 0
        Height = 65
        Width = 323
        object ES_CODIGO: TZetaKeyLookup_DevEx
          Left = 35
          Top = 22
          Width = 262
          Height = 21
          LookupDataset = dmPresupuestos.cdsEscenarios
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 493
      ExplicitHeight = 443
      inherited GrupoParametros: TcxGroupBox
        TabOrder = 1
        ExplicitWidth = 493
        ExplicitHeight = 281
        Height = 281
        Width = 493
      end
      inherited cxGroupBox1: TcxGroupBox
        TabOrder = 2
        ExplicitWidth = 493
        Width = 493
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 425
          ExplicitHeight = 74
          Width = 425
          AnchorY = 57
        end
      end
      object ProgressPanelGlobal: TPanel
        Left = 0
        Top = 376
        Width = 493
        Height = 67
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object ProgressBarGlobal: TcxProgressBar
          Left = 47
          Top = 9
          Hint = ''
          TabOrder = 0
          Width = 400
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      ExplicitWidth = 493
      ExplicitHeight = 443
      inherited sCondicionLBl: TLabel
        Left = 43
        Top = 184
        ExplicitLeft = 43
        ExplicitTop = 184
      end
      inherited sFiltroLBL: TLabel
        Left = 68
        Top = 213
        ExplicitLeft = 68
        ExplicitTop = 213
      end
      inherited Seleccionar: TcxButton
        Left = 171
        Top = 301
        ExplicitLeft = 171
        ExplicitTop = 301
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 95
        Top = 181
        ExplicitLeft = 95
        ExplicitTop = 181
      end
      inherited sFiltro: TcxMemo
        Left = 95
        Top = 212
        Style.IsFontAssigned = True
        ExplicitLeft = 95
        ExplicitTop = 212
      end
      inherited GBRango: TGroupBox
        Left = 95
        Top = 51
        ExplicitLeft = 95
        ExplicitTop = 51
      end
      inherited bAjusteISR: TcxButton
        Left = 409
        Top = 213
        ExplicitLeft = 409
        ExplicitTop = 213
      end
    end
    object TabAguinaldo: TdxWizardControlPage
      Hint = ''
      Header.Description = 
        'Si se desea presupuestar el pago de aguinaldo, marque la casilla' +
        ' y complete los par'#225'metros.'
      Header.Title = 'Calcular aguinaldo'
      object GBAguinaldo: TcxGroupBox
        Left = 1
        Top = 10
        Hint = ''
        TabOrder = 1
        Height = 377
        Width = 476
        object Label2: TLabel
          Left = 70
          Top = 32
          Width = 68
          Height = 13
          Alignment = taRightJustify
          Caption = 'Pagar en mes:'
          FocusControl = ZKAguinaldoMes
          Transparent = True
        end
        object FechaInicialLBL: TLabel
          Left = 76
          Top = 97
          Width = 62
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha inicial:'
          Transparent = True
        end
        object FechaFinalLBL: TLabel
          Left = 83
          Top = 129
          Width = 55
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha final:'
          Transparent = True
        end
        object FaltasLBL: TLabel
          Left = 58
          Top = 163
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descontar faltas:'
          Transparent = True
        end
        object IncapacidadesLBL: TLabel
          Left = 14
          Top = 206
          Width = 124
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descontar incapacidades:'
          Transparent = True
        end
        object AnticiposLBL: TLabel
          Left = 41
          Top = 249
          Width = 97
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descontar anticipos:'
          Transparent = True
        end
        object Label3: TLabel
          Left = 97
          Top = 64
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo:'
          FocusControl = ZKAguinaldoMes
          Transparent = True
        end
        object ZKAguinaldoMes: TZetaKeyCombo
          Left = 144
          Top = 28
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfMes13
          ListaVariable = lvPuesto
          Offset = 1
          Opcional = False
          EsconderVacios = False
        end
        object ZAguinaldoFechaIni: TZetaFecha
          Left = 144
          Top = 92
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 2
          Text = '21/oct/98'
          Valor = 36089.000000000000000000
        end
        object ZAguinaldoFechaFin: TZetaFecha
          Left = 144
          Top = 124
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 3
          Text = '21/oct/98'
          Valor = 36089.000000000000000000
        end
        object AguinaldoFaltas: TcxMemo
          Left = 144
          Top = 157
          Hint = ''
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Courier New'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 4
          Height = 34
          Width = 286
        end
        object AguinaldoIncapacidades: TcxMemo
          Left = 144
          Top = 202
          Hint = ''
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Courier New'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 5
          Height = 34
          Width = 286
        end
        object AguinaldoAnticipos: TcxMemo
          Left = 144
          Top = 247
          Hint = ''
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Courier New'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 6
          Height = 34
          Width = 286
        end
        object RGAguinaldoProp: TcxRadioGroup
          Left = 144
          Top = 295
          Hint = ''
          Caption = ' Proporcional a '
          Properties.Items = <
            item
              Caption = 'Fecha de ingreso'
              Value = '0'
            end
            item
              Caption = 'Fecha de antig'#252'edad'
              Value = '1'
            end>
          ItemIndex = 0
          TabOrder = 7
          Height = 66
          Width = 185
        end
        object BtnAnticipos: TcxButton
          Left = 432
          Top = 248
          Width = 25
          Height = 25
          Hint = 'Constructor de f'#243'rmulas'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
            00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
            0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
            0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
            8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 10
          OnClick = BtnAnticiposClick
        end
        object BtnIncapacidades: TcxButton
          Left = 432
          Top = 203
          Width = 25
          Height = 25
          Hint = 'Constructor de f'#243'rmulas'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
            00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
            0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
            0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
            8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 9
          OnClick = BtnIncapacidadesClick
        end
        object BtnFaltas: TcxButton
          Left = 432
          Top = 158
          Width = 25
          Height = 25
          Hint = 'Constructor de f'#243'rmulas'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
            00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
            0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
            0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
            8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          OnClick = BtnFaltasClick
        end
        object ZKAguinaldoPeriodo: TZetaKeyCombo
          Left = 144
          Top = 60
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfTipoPeriodoMes
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
      end
      object CBAguinaldoCalcular: TcxCheckBox
        Left = 16
        Top = 3
        Hint = ''
        Caption = 'Calcular aguinaldo'
        ParentBackground = False
        ParentColor = False
        Style.Color = clWhite
        TabOrder = 0
        OnClick = CBAguinaldoCalcularClick
        Width = 113
      end
    end
    object TabPTU: TdxWizardControlPage
      Hint = ''
      Header.Description = 
        'Si se desea presupuestar el reparto de utilidades, marque la cas' +
        'illa y complete los par'#225'metros.'
      Header.Title = 'Calcular PTU'
      object GBPTU: TcxGroupBox
        Left = 6
        Top = 13
        Hint = ''
        TabOrder = 1
        Height = 409
        Width = 481
        object Label4: TLabel
          Left = 55
          Top = 24
          Width = 68
          Height = 13
          Alignment = taRightJustify
          Caption = 'Pagar en mes:'
          FocusControl = ZKPTUMes
          Transparent = True
        end
        object Label10: TLabel
          Left = 82
          Top = 56
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo:'
          FocusControl = ZKPTUMes
          Transparent = True
        end
        object UtilidadLBL: TLabel
          Left = 39
          Top = 88
          Width = 82
          Height = 13
          Caption = 'Utilidad a repartir:'
          FocusControl = ZNPTUUtilidad
          Transparent = True
        end
        object SumaDiasLBL: TLabel
          Left = 54
          Top = 119
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Caption = 'Suma de d'#237'as:'
          FocusControl = PTUSumaDias
          Transparent = True
        end
        object PercepcionesLBL: TLabel
          Left = 55
          Top = 165
          Width = 68
          Height = 13
          Alignment = taRightJustify
          Caption = 'Percepciones:'
          FocusControl = PTUPercepciones
          Transparent = True
        end
        object FiltroDirectorLBL: TLabel
          Left = 45
          Top = 210
          Width = 78
          Height = 13
          Alignment = taRightJustify
          Caption = 'Filtro de director:'
          FocusControl = PTUFiltroDirector
          Transparent = True
        end
        object FiltroEventualLBL: TLabel
          Left = 39
          Top = 300
          Width = 84
          Height = 13
          Alignment = taRightJustify
          Caption = 'Filtro de eventual:'
          FocusControl = PTUFiltroEventual
          Transparent = True
        end
        object FiltroPlantaLBL: TLabel
          Left = 51
          Top = 255
          Width = 72
          Height = 13
          Alignment = taRightJustify
          Caption = 'Filtro de planta:'
          FocusControl = PTUFiltroPlanta
          Transparent = True
        end
        object SalarioTopeLBL: TLabel
          Left = 64
          Top = 345
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Salario tope:'
          FocusControl = ZNPTUSalarioTope
          Transparent = True
        end
        object ConceptoLBL: TLabel
          Left = 74
          Top = 377
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'Concepto:'
          FocusControl = KLPTUConcepto
          Transparent = True
        end
        object ZKPTUMes: TZetaKeyCombo
          Left = 127
          Top = 20
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfMes13
          ListaVariable = lvPuesto
          Offset = 1
          Opcional = False
          EsconderVacios = False
        end
        object ZNPTUUtilidad: TZetaNumero
          Left = 127
          Top = 84
          Width = 145
          Height = 21
          Mascara = mnPesos
          TabOrder = 2
          Text = '0.00'
        end
        object PTUSumaDias: TcxMemo
          Left = 127
          Top = 116
          Hint = ''
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Courier New'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 3
          Height = 34
          Width = 280
        end
        object PTUPercepciones: TcxMemo
          Left = 127
          Top = 161
          Hint = ''
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Courier New'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 4
          Height = 34
          Width = 280
        end
        object PTUFiltroDirector: TcxMemo
          Left = 127
          Top = 206
          Hint = ''
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Courier New'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 5
          Height = 34
          Width = 280
        end
        object BtnSumaDias: TcxButton
          Left = 409
          Top = 117
          Width = 25
          Height = 25
          Hint = 'Constructor de f'#243'rmulas'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
            00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
            0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
            0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
            8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 10
          OnClick = BtnSumaDiasClick
        end
        object BtnPercepciones: TcxButton
          Left = 409
          Top = 162
          Width = 25
          Height = 25
          Hint = 'Constructor de f'#243'rmulas'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
            00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
            0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
            0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
            8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 11
          OnClick = BtnPercepcionesClick
        end
        object BtnFiltroDirector: TcxButton
          Left = 409
          Top = 207
          Width = 25
          Height = 25
          Hint = 'Constructor de f'#243'rmulas'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
            00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
            0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
            0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
            8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 12
          OnClick = BtnFiltroDirectorClick
        end
        object BtnFiltroPlanta: TcxButton
          Left = 409
          Top = 252
          Width = 25
          Height = 25
          Hint = 'Constructor de f'#243'rmulas'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
            00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
            0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
            0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
            8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 13
          OnClick = BtnFiltroPlantaClick
        end
        object BtnFiltroEventual: TcxButton
          Left = 409
          Top = 297
          Width = 25
          Height = 25
          Hint = 'Constructor de f'#243'rmulas'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
            00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
            0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
            000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
            0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
            8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 14
          OnClick = BtnFiltroEventualClick
        end
        object PTUFiltroPlanta: TcxMemo
          Left = 127
          Top = 251
          Hint = ''
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Courier New'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 6
          Height = 34
          Width = 280
        end
        object PTUFiltroEventual: TcxMemo
          Left = 127
          Top = 296
          Hint = ''
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Courier New'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 7
          Height = 34
          Width = 280
        end
        object ZNPTUSalarioTope: TZetaNumero
          Left = 127
          Top = 341
          Width = 87
          Height = 21
          Mascara = mnPesos
          TabOrder = 8
          Text = '0.00'
        end
        object KLPTUConcepto: TZetaKeyLookup_DevEx
          Left = 127
          Top = 373
          Width = 308
          Height = 21
          Filtro = 'CO_NUMERO < 1000'
          LookupDataset = dmCatalogos.cdsConceptos
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 9
          TabStop = True
          WidthLlave = 60
        end
        object ZKPTUPeriodo: TZetaKeyCombo
          Left = 127
          Top = 52
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfTipoPeriodoMes
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
      end
      object CBPTUCalcular: TcxCheckBox
        Left = 20
        Top = 3
        Hint = ''
        Caption = 'Calcular PTU'
        ParentBackground = False
        ParentColor = False
        Style.Color = clWhite
        TabOrder = 0
        OnClick = CBPTUCalcularClick
        Width = 90
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 320
    Top = 19
  end
end
