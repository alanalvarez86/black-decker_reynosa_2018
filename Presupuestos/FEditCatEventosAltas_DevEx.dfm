inherited EditCatEventosAltas_DevEx: TEditCatEventosAltas_DevEx
  Top = 217
  Caption = 'Contrataciones'
  ClientHeight = 445
  ClientWidth = 494
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 409
    Width = 494
    inherited OK_DevEx: TcxButton
      Left = 329
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 408
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 494
    inherited ValorActivo2: TPanel
      Width = 168
      inherited textoValorActivo2: TLabel
        Width = 162
      end
    end
  end
  object PanelGeneral: TPanel [3]
    Left = 0
    Top = 47
    Width = 494
    Height = 84
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 121
      Top = 8
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 98
      Top = 32
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object Label3: TLabel
      Left = 320
      Top = 8
      Width = 3
      Height = 13
    end
    object EmpleadoLbl: TLabel
      Left = 5
      Top = 56
      Width = 152
      Height = 13
      Alignment = taRightJustify
      Caption = 'Usar clasificaci'#243'n del empleado:'
      FocusControl = CB_CODIGO
    end
    object EV_ACTIVO: TDBCheckBox
      Left = 430
      Top = 8
      Width = 56
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Activo:'
      DataField = 'EV_ACTIVO'
      DataSource = DataSource
      TabOrder = 3
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object EV_CODIGO: TZetaDBEdit
      Left = 160
      Top = 4
      Width = 97
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 10
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'EV_CODIGO'
      DataSource = DataSource
    end
    object EV_DESCRIP: TDBEdit
      Left = 160
      Top = 28
      Width = 230
      Height = 21
      DataField = 'EV_DESCRIP'
      DataSource = DataSource
      TabOrder = 1
    end
    object CB_CODIGO: TZetaDBKeyLookup_DevEx
      Left = 160
      Top = 52
      Width = 329
      Height = 21
      LookupDataset = dmCliente.cdsEmpleadoLookUp
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 2
      TabStop = True
      WidthLlave = 80
      DataField = 'CB_CODIGO'
      DataSource = DataSource
    end
  end
  object PageControl: TcxPageControl [4]
    Left = 0
    Top = 131
    Width = 494
    Height = 278
    Align = alClient
    TabOrder = 4
    Properties.ActivePage = TabContratacion
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 274
    ClientRectLeft = 4
    ClientRectRight = 490
    ClientRectTop = 24
    object TabContratacion: TcxTabSheet
      Caption = 'Contrataci'#243'n'
      object EV_PUESTOLbl: TLabel
        Left = 91
        Top = 8
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
        Color = clBtnFace
        ParentColor = False
      end
      object CB_CLASIFIlbl: TLabel
        Left = 65
        Top = 32
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
      end
      object CB_HORARIOlbl: TLabel
        Left = 96
        Top = 56
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object Label4: TLabel
        Left = 43
        Top = 81
        Width = 83
        Height = 13
        Caption = 'Registro patronal:'
      end
      object Label6: TLabel
        Left = 48
        Top = 105
        Width = 78
        Height = 13
        Caption = 'Tipo de N'#243'mina:'
      end
      object EV_PUESTO: TZetaDBKeyLookup_DevEx
        Left = 130
        Top = 4
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_PUESTO'
        DataSource = DataSource
      end
      object EV_CLASIFI: TZetaDBKeyLookup_DevEx
        Left = 130
        Top = 28
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_CLASIFI'
        DataSource = DataSource
      end
      object EV_TURNO: TZetaDBKeyLookup_DevEx
        Left = 130
        Top = 52
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTurnos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_TURNO'
        DataSource = DataSource
      end
      object EV_PATRON: TZetaDBKeyLookup_DevEx
        Left = 130
        Top = 77
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsRPatron
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_PATRON'
        DataSource = DataSource
      end
      object GrupoContrato: TGroupBox
        Left = 40
        Top = 123
        Width = 393
        Height = 65
        Caption = ' Contrato '
        TabOrder = 5
        object Label5: TLabel
          Left = 63
          Top = 16
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
        end
        object EV_CONTRAT: TZetaDBKeyLookup_DevEx
          Left = 90
          Top = 14
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsContratos
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'EV_CONTRAT'
          DataSource = DataSource
        end
        object EV_BAJA: TDBCheckBox
          Left = 90
          Top = 41
          Width = 199
          Height = 17
          Alignment = taLeftJustify
          Caption = #191' Dar de baja al vencer el contrato ?'
          DataField = 'EV_BAJA'
          DataSource = DataSource
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
      object EV_NOMINA: TZetaDBKeyCombo
        Left = 130
        Top = 101
        Width = 190
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 4
        ListaFija = lfTipoPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
        DataField = 'EV_NOMINA'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
    object TabArea: TcxTabSheet
      Caption = 'Area'
      object EV_NIVEL1lbl: TLabel
        Left = 146
        Top = 8
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object EV_NIVEL2lbl: TLabel
        Left = 146
        Top = 32
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object EV_NIVEL3lbl: TLabel
        Left = 146
        Top = 56
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object EV_NIVEL4lbl: TLabel
        Left = 146
        Top = 80
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object EV_NIVEL5lbl: TLabel
        Left = 146
        Top = 104
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object EV_NIVEL6lbl: TLabel
        Left = 146
        Top = 128
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object EV_NIVEL7lbl: TLabel
        Left = 146
        Top = 152
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object EV_NIVEL8lbl: TLabel
        Left = 146
        Top = 176
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object EV_NIVEL9lbl: TLabel
        Left = 146
        Top = 199
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object EV_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 184
        Top = 195
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_NIVEL9'
        DataSource = DataSource
      end
      object EV_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 184
        Top = 172
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_NIVEL8'
        DataSource = DataSource
      end
      object EV_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 184
        Top = 148
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_NIVEL7'
        DataSource = DataSource
      end
      object EV_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 184
        Top = 124
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_NIVEL6'
        DataSource = DataSource
      end
      object EV_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 184
        Top = 100
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_NIVEL5'
        DataSource = DataSource
      end
      object EV_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 184
        Top = 76
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_NIVEL4'
        DataSource = DataSource
      end
      object EV_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 184
        Top = 52
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_NIVEL3'
        DataSource = DataSource
      end
      object EV_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 184
        Top = 28
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_NIVEL2'
        DataSource = DataSource
      end
      object EV_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 184
        Top = 4
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_NIVEL1'
        DataSource = DataSource
      end
    end
    object TabSalario: TcxTabSheet
      Caption = 'Salario'
      object EV_OTRAS_1Lbl: TLabel
        Left = 27
        Top = 127
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Percepci'#243'n Fija # 1:'
      end
      object EV_OTRAS_2Lbl: TLabel
        Left = 27
        Top = 150
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Percepci'#243'n Fija # 2:'
      end
      object EV_OTRAS_3Lbl: TLabel
        Left = 27
        Top = 173
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Percepci'#243'n Fija # 3:'
      end
      object EV_OTRAS_4Lbl: TLabel
        Left = 27
        Top = 195
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Percepci'#243'n Fija # 4:'
      end
      object EV_OTRAS_5Lbl: TLabel
        Left = 27
        Top = 218
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Percepci'#243'n Fija # 5:'
      end
      object EV_SALARIOlbl: TLabel
        Left = 57
        Top = 31
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Salario Diario:'
      end
      object CB_PER_VARlbl: TLabel
        Left = 14
        Top = 55
        Width = 108
        Height = 13
        Caption = 'Promedio de Variables:'
      end
      object CB_ZONA_GElbl: TLabel
        Left = 39
        Top = 79
        Width = 83
        Height = 13
        Caption = 'Zona Geogr'#225'fica:'
      end
      object CB_TABLASSlbl: TLabel
        Left = 13
        Top = 104
        Width = 109
        Height = 13
        Caption = 'Tabla de Prestaciones:'
      end
      object EV_OTRAS_1: TZetaDBKeyLookup_DevEx
        Left = 125
        Top = 123
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsOtrasPer
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 50
        DataField = 'EV_OTRAS_1'
        DataSource = DataSource
      end
      object EV_OTRAS_2: TZetaDBKeyLookup_DevEx
        Left = 125
        Top = 146
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsOtrasPer
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 50
        DataField = 'EV_OTRAS_2'
        DataSource = DataSource
      end
      object EV_OTRAS_3: TZetaDBKeyLookup_DevEx
        Left = 125
        Top = 169
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsOtrasPer
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 50
        DataField = 'EV_OTRAS_3'
        DataSource = DataSource
      end
      object EV_OTRAS_4: TZetaDBKeyLookup_DevEx
        Left = 125
        Top = 191
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsOtrasPer
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 50
        DataField = 'EV_OTRAS_4'
        DataSource = DataSource
      end
      object EV_OTRAS_5: TZetaDBKeyLookup_DevEx
        Left = 125
        Top = 214
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsOtrasPer
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        WidthLlave = 50
        DataField = 'EV_OTRAS_5'
        DataSource = DataSource
      end
      object EV_AUTOSAL: TDBCheckBox
        Left = 17
        Top = 8
        Width = 121
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Salario por Tabulador:'
        DataField = 'EV_AUTOSAL'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = EV_AUTOSALClick
      end
      object EV_SALARIO: TZetaDBNumero
        Left = 125
        Top = 27
        Width = 90
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        DataField = 'EV_SALARIO'
        DataSource = DataSource
      end
      object EV_PER_VAR: TZetaDBNumero
        Left = 125
        Top = 51
        Width = 90
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
        DataField = 'EV_PER_VAR'
        DataSource = DataSource
      end
      object EV_ZONA_GE: TDBComboBox
        Left = 125
        Top = 75
        Width = 50
        Height = 21
        Style = csDropDownList
        DataField = 'EV_ZONA_GE'
        DataSource = DataSource
        ItemHeight = 13
        Items.Strings = (
          'A'
          'B'
          'C')
        TabOrder = 3
      end
      object EV_TABLASS: TZetaDBKeyLookup_DevEx
        Left = 125
        Top = 100
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsSSocial
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 50
        DataField = 'EV_TABLASS'
        DataSource = DataSource
      end
    end
    object TabOtros: TcxTabSheet
      Caption = 'Otros'
      ImageIndex = 3
      object LblNivel0: TLabel
        Left = 46
        Top = 14
        Width = 81
        Height = 13
        Alignment = taRightJustify
        Caption = 'Confidencialidad:'
      end
      object EV_NIVEL0: TZetaDBKeyLookup_DevEx
        Left = 130
        Top = 10
        Width = 335
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 55
        DataField = 'EV_NIVEL0'
        DataSource = DataSource
      end
      object EV_BAN_ELE: TDBCheckBox
        Left = 32
        Top = 33
        Width = 111
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Banca Electronica:'
        DataField = 'EV_BAN_ELE'
        DataSource = DataSource
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 428
    Top = 9
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 4194312
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 80
    Top = 56
    DockControlHeights = (
      0
      0
      28
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 3670064
  end
end
