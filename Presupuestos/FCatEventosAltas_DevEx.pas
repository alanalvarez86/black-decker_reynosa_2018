unit FCatEventosAltas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls,
     StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
     TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
     cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
     cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
     cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatEventosAltas_DevEx = class(TBaseGridLectura_DevEx)
    EV_CODIGO: TcxGridDBColumn;
    EV_DESCRIP: TcxGridDBColumn;
    EV_ACTIVO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatEventosAltas_DevEx: TCatEventosAltas_DevEx;

implementation

uses dPresupuestos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZAccesosTress;

{$R *.DFM}

{ TCatEventos }

procedure TCatEventosAltas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     IndexDerechos:= D_PRESUP_CONTRATACIONES;
     HelpContext := H_CAT_EVENTOS_ALTAS;
end;

procedure TCatEventosAltas_DevEx.Connect;
begin
     with dmPresupuestos do
     begin
          cdsEventosAltas.Conectar;
          DataSource.DataSet:= cdsEventosAltas;
     end;
end;

procedure TCatEventosAltas_DevEx.Refresh;
begin
     dmPresupuestos.cdsEventosAltas.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatEventosAltas_DevEx.Agregar;
begin
     dmPresupuestos.cdsEventosAltas.Agregar;
end;

procedure TCatEventosAltas_DevEx.Borrar;
begin
     dmPresupuestos.cdsEventosAltas.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatEventosAltas_DevEx.Modificar;
begin
     dmPresupuestos.cdsEventosAltas.Modificar;
end;

procedure TCatEventosAltas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Contrataciones', 'Contrataciones', 'EV_CODIGO', dmPresupuestos.cdsEventosAltas );
end;

procedure TCatEventosAltas_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('EV_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.

