unit ZetaEvaluacionTools;

interface

const
     K_GLOBAL_RUTA_VIRTUAL = 3005;
type
    eTipoCorreo = ( tpPlantilla, tpTexto );
    eStatusEvaluador = ( esteNoHaComenzado, esteEnProceso, esteTerminado );

function GetStatusEvaluador( const eValor: eStatusEvaluador ): String;

implementation

function GetStatusEvaluador( const eValor: eStatusEvaluador ): String;
begin
     case eValor of
          esteNoHaComenzado: Result := 'No ha comenzado';
          esteEnProceso: Result := 'En proceso';
          esteTerminado: Result := 'Termin�';
     else
         Result := '???';
     end;
end;

end.
