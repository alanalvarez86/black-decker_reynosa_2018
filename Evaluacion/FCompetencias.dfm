inherited Competencias: TCompetencias
  Caption = 'Competencias'
  ClientHeight = 295
  ClientWidth = 686
  ExplicitWidth = 686
  ExplicitHeight = 295
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 686
    ExplicitWidth = 808
    inherited ValorActivo2: TPanel
      Width = 427
      ExplicitWidth = 549
      inherited textoValorActivo2: TLabel
        Width = 421
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 787
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 686
    Height = 276
    ExplicitLeft = 8
    ExplicitTop = 22
    ExplicitWidth = 1052
    ExplicitHeight = 276
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      Styles.Content = cxStyle1
      Styles.Header = cxStyle2
      Styles.Indicator = cxStyle2
      object CM_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CM_CODIGO'
        Styles.Content = cxStyle3
        Width = 82
      end
      object CM_DESCRIP: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'CM_DESCRIP'
        Styles.Content = cxStyle4
        Width = 200
      end
      object CM_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'CM_INGLES'
        Styles.Content = cxStyle5
        Width = 200
      end
      object TC_DESCRIP: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TC_DESCRIP'
        Styles.Content = cxStyle6
        Width = 180
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 48
    Top = 112
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
  end
end
