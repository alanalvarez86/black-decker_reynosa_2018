unit FGlobalCorreosEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     ZetaCommonClasses,
     ZBaseGlobal_DevEx,
     ZetaNumero, ZBaseGlobal, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
     Vcl.Menus, Vcl.ImgList, cxButtons;

type
  TGlobalCorreosEdit = class(TBaseGlobal_DevEx)
    RutaVirtualLBL: TLabel;
    RutaVirtual: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
  public
    { Public declarations }
    procedure Cargar; override;
    procedure DesCargar; override;
  end;

var
  GlobalCorreosEdit: TGlobalCorreosEdit;

implementation

uses DEvaluacion,
     ZGlobalTress,
     ZetaDialogo,
     ZetaEvaluacionTools,
     ZAccesosTress,
     ZHelpContext;

{$R *.DFM}

procedure TGlobalCorreosEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_SISTEMA_GLOBALES_EMPRESA_CONFIGURAR_SERVIDOR_WEB;
     IndexDerechos := D_GLOBALES_EVALUACION;
     {$ifdef OFICIAL}
     RutaVirtual.Tag := K_GLOBAL_RUTA_VIRTUAL;
     {$endif}
     FParametros := TZetaParams.Create;
end;

procedure TGlobalCorreosEdit.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FParametros );
     inherited;
end;

procedure TGlobalCorreosEdit.Cargar;
begin
     dmEvaluacion.GlobalesCargar( FParametros );
     with FParametros do
     begin
          RutaVirtual.Text := ParamByName( Format( '%d', [ K_GLOBAL_RUTA_VIRTUAL ] ) ).AsString;
     end;
end;

procedure TGlobalCorreosEdit.DesCargar;
begin
     with FParametros do
     begin
          Clear;
          AddString( Format( '%d', [ K_GLOBAL_RUTA_VIRTUAL ] ), Self.RutaVirtual.Text );
     end;
     try
        dmEvaluacion.GlobalesDescargar( FParametros );
        LastAction := K_EDICION_MODIFICACION;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( Caption, 'Error al escribir globales', Error, 0 );
           end;
     end;
end;

end.
