unit FEditCriterioEvaluar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask,
     ZBaseEdicion_DevEx,
     ZetaSmartLists,
     ZetaDBTextBox,
     ZetaKeyCombo,
     ZetaNumero,
     ZetaKeyLookup, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, cxContainer, cxEdit, ZetaKeyLookup_DevEx, cxGroupBox,
  dxBarExtItems, dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditCriterioEvaluar = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    ztbTitulo: TZetaTextBox;
    lblCompetencia: TLabel;
    CM_CODIGO: TZetaDBTextBox;
    EC_ORDEN: TZetaDBTextBox;
    Label3: TLabel;
    EC_NOMBRE: TDBEdit;
    pnControles: TPanel;
    GroupBox1: TcxGroupBox;
    EC_DETALLE: TDBMemo;
    lblImportancia: TLabel;
    EC_POS_PES: TZetaDBKeyLookup_DevEx;
    EC_PESO: TZetaDBNumero;
    lblPeso: TLabel;
    Label7: TLabel;
    EC_GET_COM: TZetaDBKeyCombo;
    EC_DOCUMENlbl: TLabel;
    EC_DOCUMEN: TDBEdit;
    BuscarDocumento: TcxButton;
    OpenDialog: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure BuscarDocumentoClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetControles;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  EditCriterioEvaluar: TEditCriterioEvaluar;

implementation

{$R *.DFM}

uses DEvaluacion,
     DCliente,
     ZHelpContext,
     ZAccesosTress,
     ZetaBuscador_DevEx,
     ZetaCommonLists,
     ZetaCommonclasses,
     ZetaCommonTools;

{ TEditCriterioEvaluar }

procedure TEditCriterioEvaluar.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CRITERIOS_EVAL;
     HelpContext := H_DISENO_ENCUESTA_CRITERIOS_EVALUAR_AGREGAR_REGISTRO;
     FirstControl := EC_NOMBRE;
     EC_POS_PES.LookupDataSet := dmEvaluacion.cdsNivelesEnc;
     EC_GET_COM.ListaFija := lfComentariosMGR;
end;

procedure TEditCriterioEvaluar.Connect;
begin
     with dmEvaluacion do
     begin
          SetControles;
          DataSource.DataSet := cdsCriteriosEval;
     end;
end;

procedure TEditCriterioEvaluar.SetControles;
var
   lLlenoPesEsc: Boolean;
begin
     with dmCliente do
     begin
          ztbTitulo.Caption := Format( '%d = %s', [ Encuesta, Nombre ] );
          lLlenoPesEsc := StrLleno( PesoEsc );
     end;
     with dmEvaluacion.cdsNivelesEnc do
     begin
          Refrescar;
          if lLlenoPesEsc then
          begin
               EC_POS_PES.DataField := 'EC_POS_PES';
               EC_POS_PES.Filtro := Format( '( ET_CODIGO = %d ) and ( EE_CODIGO = %s )', [ dmCliente.Encuesta, EntreComillas( dmCliente.PesoEsc ) ] )
          end
          else
          begin
               EC_POS_PES.DataField := VACIO;
               EC_POS_PES.Filtro := VACIO;
          end;
     end;
     lblImportancia.Enabled := lLlenoPesEsc;
     EC_POS_PES.Enabled := lLlenoPesEsc;
     lblPeso.Enabled := not lLlenoPesEsc;
     EC_PESO.Enabled := not lLlenoPesEsc;
     CM_CODIGO.Visible := ZetaCommonTools.StrLleno( dmEvaluacion.cdsCriteriosEval.FieldByName( 'CM_CODIGO' ).AsString );
     lblCompetencia.Visible := CM_CODIGO.Visible;
end;

procedure TEditCriterioEvaluar.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Criterios a evaluar', 'ENC_NIVE', 'EL_ORDEN', dmEvaluacion.cdsCriteriosEval );
end;

procedure TEditCriterioEvaluar.EscribirCambios;
begin
     if dmEvaluacion.ModificaDisenoEncuesta then
        inherited EscribirCambios;
end;

procedure TEditCriterioEvaluar.BuscarDocumentoClick(Sender: TObject);
begin
     inherited;
     with EC_DOCUMEN do
     begin
          with OpenDialog do
          begin
               FileName := Text;
               if Execute then
               begin
                    with dmEvaluacion do
                    begin
                         CambiarDocumentacion( cdsCriteriosEval, 'EC_DOCUMEN', ExtractFileName( FileName ) );
                    end;
               end;
          end;
     end;
end;

end.

