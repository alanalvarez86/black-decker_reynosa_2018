inherited EmpleadosEvaluar: TEmpleadosEvaluar
  Left = 423
  Top = 450
  Caption = 'Empleados a evaluar'
  ClientWidth = 561
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 561
    inherited ValorActivo2: TPanel
      Width = 302
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 561
    Height = 62
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 31
      Top = 12
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status:'
    end
    object Label2: TLabel
      Left = 448
      Top = 12
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Cantidad:'
      Visible = False
    end
    object ztbCuantos: TZetaTextBox
      Left = 496
      Top = 10
      Width = 60
      Height = 17
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      ShowAccelChar = False
      Visible = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label3: TLabel
      Left = 14
      Top = 36
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
    end
    object btnBuscaEmp: TSpeedButton
      Left = 529
      Top = 31
      Width = 23
      Height = 22
      Hint = 'Filtrar Lista Por Empleado'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
      ParentShowHint = False
      ShowHint = True
      Visible = False
      OnClick = btnBuscaEmpClick
    end
    object znEmpleado: TZetaNumero
      Left = 434
      Top = 32
      Width = 93
      Height = 21
      Mascara = mnEmpleado
      TabOrder = 2
      UseEnterKey = True
      Visible = False
    end
    object SJ_STATUS: TComboBox
      Left = 66
      Top = 8
      Width = 119
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
    object btnFiltrar: TBitBtn
      Left = 380
      Top = 8
      Width = 97
      Height = 45
      Caption = 'Filtrar'
      TabOrder = 3
      OnClick = btnFiltrarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777777777777770000070000077777000E00BFBFB07777777E0BFBF000777
        7700E0FBFBFBF0777700E0BFBF0000007777E0FBFBFBFBFB0799E0BF00000000
        7799000BFB077777777777700077777777007777777777777700777777777777
        7777777777777777700077777777777770007777777777777777}
    end
    object CB_CODIGO: TZetaKeyLookup
      Left = 66
      Top = 32
      Width = 300
      Height = 21
      TabOrder = 1
      TabStop = True
      WidthLlave = 70
    end
  end
  object GridEmp: TZetaDBGrid [2]
    Left = 0
    Top = 81
    Width = 561
    Height = 173
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit, dgMultiSelect]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'N�mero'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        Title.Caption = 'Nombre'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SJ_STATUS'
        Title.Caption = 'Status'
        Width = 80
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'SJ_NUM_EVA'
        Title.Alignment = taRightJustify
        Title.Caption = 'Evaluadores'
        Width = 70
        Visible = True
      end>
  end
  object StatusBar1: TStatusBar [3]
    Left = 0
    Top = 254
    Width = 561
    Height = 19
    Panels = <
      item
        Text = 'Cantidad:'
        Width = 50
      end>
    SimplePanel = False
  end
  inherited DataSource: TDataSource
    Left = 520
    Top = 0
  end
end
