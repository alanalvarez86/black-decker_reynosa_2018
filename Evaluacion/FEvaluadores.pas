unit FEvaluadores;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, StdCtrls, Buttons, Mask, ZetaNumero,
  ZetaDBTextBox, Grids, DBGrids, ZetaDBGrid, ZetaKeyLookup, ComCtrls;

type
  TEvaluadores = class(TBaseConsulta)
    Panel1: TPanel;
    Label2: TLabel;
    ztbCuantos: TZetaTextBox;
    Label3: TLabel;
    btnBuscaEmp: TSpeedButton;
    znUsuario: TZetaNumero;
    btnFiltrar: TBitBtn;
    Grid1: TZetaDBGrid;
    US_CODIGO: TZetaKeyLookup;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    EV_STATUS: TComboBox;
    procedure btnBuscaEmpClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure Grid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure LlenaListaStatus;
    procedure CuantosRegistros;
    procedure CargaFiltro;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    //procedure DoLookup; override;
  end;

var
  Evaluadores: TEvaluadores;

implementation

{$R *.DFM}
uses DEvaluacion,
     DSistema,
     ZHelpContext,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists;

{ TEvaluadores }
procedure TEvaluadores.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     US_CODIGO.LookupDataSet := dmSistema.cdsUsuarios;
     HelpContext := H_RESULTADOS_ENCUESTA_EVALUADORES;
end;

procedure TEvaluadores.FormShow(Sender: TObject);
begin
     inherited;
     US_CODIGO.Valor :=  0;
     LLenaListaStatus;
end;

procedure TEvaluadores.Agregar;
begin
     dmEvaluacion.cdsEvaluadores.Agregar;
end;

procedure TEvaluadores.Borrar;
begin
     dmEvaluacion.cdsEvaluadores.Borrar;
end;

procedure TEvaluadores.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmEvaluacion do
     begin
          if not cdsEvaluadores.Active or cdsEvaluadores.HayQueRefrescar then
          begin
               cdsEvaluadores.Filtered := False;
               CargaFiltro;
               cdsEvaluadores.ResetDataChange;
          end;
          DataSource.DataSet := cdsEvaluadores;
     end;
end;

{procedure TEvaluadores.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( self.Caption, '', 'CB_CODIGO', dmEvaluacion.cdsEmpEvaluar );
end;}

procedure TEvaluadores.ImprimirForma;
begin
  inherited;
end;

procedure TEvaluadores.Modificar;
begin
     dmEvaluacion.cdsEvaluadores.Modificar;
end;

procedure TEvaluadores.Refresh;
begin
     CargaFiltro;
end;

procedure TEvaluadores.LlenaListaStatus;
begin
     with EV_STATUS do
     begin
          ZetaCommonLists.LlenaLista( lfStatusEvaluaciones, Items );
          Items.Insert( 0, 'Todas' );
          ItemIndex := 0;
     end;
end;

procedure TEvaluadores.CuantosRegistros;
begin
     //ztbCuantos.Caption := inttoStr( dmEvaluacion.cdsEvaluadores.RecordCount );
     StatusBar1.Panels[0].Text := 'Cantidad : ' + inttoStr( dmEvaluacion.cdsEvaluadores.RecordCount );
end;

procedure TEvaluadores.btnBuscaEmpClick(Sender: TObject);
var
   sUsuario, sNombre: String;
begin
     inherited;
     if dmSistema.cdsUsuarios.Search( VACIO, sUsuario, sNombre ) then
     begin
          znUsuario.Valor := strtoint( sUsuario );
     end;

end;

procedure TEvaluadores.btnFiltrarClick(Sender: TObject);
begin
     CargaFiltro;
end;

procedure TEvaluadores.CargaFiltro;
var
   iValor: Integer;
   sFiltro, sFiltros: String;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion.cdsEvaluadores do
        begin
             DisableControls;
             try
                iValor := US_CODIGO.Valor;
                case( EV_STATUS.ItemIndex )of
                      1: sFiltro := '(SE_TERMINA < SE_TOTAL)';
                      2: sFiltro := '(SE_NUEVA > 0)';
                      3: sFiltro := 'SE_PROCESO > 0';
                      4: sFiltro := 'SE_TERMINA > 0';
                      5: sFiltro := 'SE_NUEVA >= SE_TOTAL';
                      6: sFiltro := 'SE_PROCESO >= SE_TOTAL';
                      7: sFiltro := 'SE_TERMINA >= SE_TOTAL';
                else
                    sFiltro := VACIO;
                end;
                if( ( sFiltro = VACIO ) and ( iValor = 0 ) )then
                    sFiltros := VACIO
                else
                begin
                     if( iValor = 0 )then
                         sFiltros := sFiltro
                     else
                         sFiltros := ConcatFiltros( Format( 'US_CODIGO=%d',[ iValor ] ), sFiltro );
                end;
                dmEvaluacion.ObtieneEvaluadores( sFiltros );
             finally
                    EnableControls;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     CuantosRegistros;
end;

function TEvaluadores.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden agregar evaluadores desde esta forma';
     Result := False;
end;

function TEvaluadores.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden borrar evaluadores desde esta forma';
     Result := False;
end;

procedure TEvaluadores.Grid1DrawColumnCell( Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
                                            State: TGridDrawState);
begin
     with Grid1 do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Font.Color := clWhite;
                    Brush.Color := clNavy;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    with Font do
                    begin
                         with dmEvaluacion.cdsEvaluadores do
                         begin
                              if( FieldByName('SE_TERMINA').AsInteger = FieldByName('SE_TOTAL').AsInteger )then
                                  Color := clRed
                              else
                              begin
                                   if( FieldByName('SE_NUEVA').AsInteger = FieldByName('SE_TOTAL').AsInteger )then
                                       Color := Grid1.Font.Color
                                   else
                                       Color := clBlue;
                              end;
                         end;
                    end;
               end;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;

end.
