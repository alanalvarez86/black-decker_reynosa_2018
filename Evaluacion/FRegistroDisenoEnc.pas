unit FRegistroDisenoEnc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsultaBotones, ComCtrls, ImgList, Db, ToolWin, ExtCtrls;

type
  TRegistroDisenoEnc = class(TBaseBotones)
    Registros: TToolButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RegistroDisenoEnc: TRegistroDisenoEnc;

implementation

uses FTressShell,
     ZHelpContext;

{$R *.DFM}

procedure TRegistroDisenoEnc.FormCreate(Sender: TObject);
begin
     inherited;
     //HelpContext := H_DISENO_ENCUESTA_REGISTROS;
end;

end.
