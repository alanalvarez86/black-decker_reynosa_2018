object dmProcesos: TdmProcesos
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 283
  Top = 161
  Height = 479
  Width = 741
  object cdsASCII: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 128
    Top = 16
    object cdsASCIIRenglon: TStringField
      DisplayWidth = 2048
      FieldName = 'Renglon'
      Size = 2048
    end
  end
  object cdsDeclaraGlobales: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 88
  end
  object cdsDataSet: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 16
  end
end
