unit FCatUsuariosEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, Grids, DBGrids,
     ZBaseEdicion_DevEx,
     ZetaDBTextBox,
     ZetaEdit,
     ZetaNumero,
     ZetaDBGrid,
     ZetaKeyCombo,
     ZetaKeyLookup,
     ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx, cxContainer,
  cxEdit, cxGroupBox;

type
  TCatUsuariosEdit = class(TBaseEdicion_DevEx)
    PanelControles: TPanel;
    RO_NOMBRElbl: TLabel;
    US_CODIGOlbl: TLabel;
    US_JEFElbl: TLabel;
    US_CODIGO: TZetaDBTextBox;
    US_NOMBRE: TZetaDBTextBox;
    US_JEFE: TZetaDBKeyLookup_DevEx;
    eMailGB: TcxGroupBox;
    US_EMAILlbl: TLabel;
    US_EMAIL: TZetaDBEdit;
    US_FORMATOlbl: TLabel;
    US_FORMATO: TZetaDBKeyCombo;
    US_DOMAIN: TDBEdit;
    US_DOMAINlbl: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  CatUsuariosEdit: TCatUsuariosEdit;

implementation

uses DEvaluacion,
     DSistema,
     ZHelpContext,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaBuscador,
     ZetaDialogo,
     ZAccesosTress,
     DGlobal,
     ZGlobalTress;

{$R *.DFM}


procedure TCatUsuariosEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_SISTEMA_USUARIOS_MODIFICAR_REGISTROS;
     IndexDerechos := D_EVAL_REPORTA;
     FirstControl := US_JEFE;
     US_JEFE.LookupDataset := dmSistema.cdsUsuarios;
     US_FORMATO.ListaFija := lfEMailType;
end;

procedure TCatUsuariosEdit.FormShow(Sender: TObject);
begin
     inherited;

     US_JEFE.Filtro := Format( 'US_CODIGO <> %d', [ dmEvaluacion.GetUsuarioActual ] );
     US_JEFE.Enabled := not Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES);
end;

procedure TCatUsuariosEdit.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmEvaluacion do
     begin
          Datasource.Dataset := cdsUsuariosRol;
     end;
end;

procedure TCatUsuariosEdit.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     US_JEFE.Filtro := Format( 'US_CODIGO <> %d', [ dmEvaluacion.GetUsuarioActual ] );
end;

procedure TCatUsuariosEdit.DoLookup;
begin
     inherited;
     //ZetaBuscador.BuscarCodigo( 'Rol', 'Rol', 'RO_CODIGO', dmEvaluacion.cdsRoles );
end;

function TCatUsuariosEdit.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se permite agregar usuarios en esta pantalla';
end;

function TCatUsuariosEdit.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se permite borrar usuarios en esta pantalla';
end;


end.
