unit FEncuestas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, StdCtrls, Buttons, Grids, DBGrids,
     ZBaseConsulta,ZBaseGridLectura_DevEx,
     ZetaDBGrid, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxTextEdit, cxClasses, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEncuestas = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    btnCopiarEncuesta: TcxButton;
    btnExportar: TcxButton;
    btnImportar: TcxButton;
    SaveDialog: TSaveDialog;
    OpenDialog: TOpenDialog;
    ET_CODIGO: TcxGridDBColumn;
    ET_NOMBRE: TcxGridDBColumn;
    ET_FEC_INI: TcxGridDBColumn;
    ET_FEC_FIN: TcxGridDBColumn;
    ET_STATUS: TcxGridDBColumn;
    ET_NUM_COM: TcxGridDBColumn;
    ET_NUM_SUJ: TcxGridDBColumn;
    ET_NUM_EVA: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    procedure btnCopiarEncuestaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Encuestas: TEncuestas;

implementation

{$R *.DFM}

uses DEvaluacion,
     DGlobal,
     ZHelpContext,
     ZGlobalTress,
     ZetaBuscador_DevEx,
     ZImprimeForma,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZAccesosTress,
     ZAccesosMgr,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     FCopiaEncuesta,
     FTressShell;

{ TEncuestas }

procedure TEncuestas.FormCreate(Sender: TObject);
begin
  inherited;
  HelpContext := H_CATALOGOS_ENCUESTAS;
end;

procedure TEncuestas.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('ET_NOMBRE'), K_SIN_TIPO , '', skCount);
     inherited;
     btnCopiarEncuesta.Enabled := ZAccesosMgr.CheckDerecho( D_ENCUESTAS, K_DERECHO_SIST_KARDEX );
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TEncuestas.Agregar;
begin
     dmEvaluacion.cdsEncuestas.Agregar;
     DoBestFit;
end;

procedure TEncuestas.Borrar;
begin
     dmEvaluacion.cdsEncuestas.Borrar;
     TressShell.SetUltimaEncuesta;
     DoBestFit;
end;

procedure TEncuestas.Connect;
begin
     with dmEvaluacion do
     begin
          cdsEncuestas.Conectar;
          DataSource.DataSet := cdsEncuestas;
     end;
end;

procedure TEncuestas.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( Self.Caption, 'ENCUESTA', 'ET_CODIGO', dmEvaluacion.cdsEncuestas );
end;

procedure TEncuestas.ImprimirForma;
begin
     inherited;
     ZImprimeForma.ImprimeUnaForma( enEncuesta, dmEvaluacion.cdsEncuestas );
end;

procedure TEncuestas.Modificar;
begin
     dmEvaluacion.cdsEncuestas.Modificar;
     DoBestFit;
end;

procedure TEncuestas.Refresh;
begin
     dmEvaluacion.cdsEncuestas.Refrescar;
     DoBestFit;
end;

procedure TEncuestas.btnCopiarEncuestaClick(Sender: TObject);
var
   FCopiaEncuesta: TCopiaEncuesta;
begin
     inherited;
     FCopiaEncuesta := TCopiaEncuesta.Create( Application );
     try
        with FCopiaEncuesta do
        begin
             ShowModal;
        end;
     finally
            FreeAndNil( FCopiaEncuesta );
     end;
end;

procedure TEncuestas.btnExportarClick(Sender: TObject);
var
   sFileName, sNombre: String;
   oCursor: TCursor;
   oArchivo: TStrings;
   myEncoding : TEncoding;
begin
     inherited;
     with dmEvaluacion do
     begin
          if cdsEncuestas.IsEmpty then
          begin
               ZetaDialogo.ZInformation( Self.Caption, 'No existen encuestas para exportar', 0 );
          end
          else
          begin
               sNombre := cdsEncuestas.FieldbyName('ET_NOMBRE').AsString;
               sFileName := Format( '%s.EN1', [ sNombre ] );
               with SaveDialog do
               begin
                    FileName := sFileName;
                    InitialDir := Global.GetGlobalString( K_GLOBAL_DIR_PLANT );
                    if Execute then
                    begin
                         oCursor := Screen.Cursor;
                         Screen.Cursor := crHourglass;
                         oArchivo := TStringList.Create;
                         try
                            myEncoding := TEncoding.Unicode; //(@am): Se agrega en vs 2016 para asignar el tipo de encoding.
                            with oArchivo do
                            begin
                                 Add( ExportaEncuesta( cdsEncuestas.FieldbyName( 'ET_CODIGO' ).AsInteger ) );
                                 SaveToFile( FileName, myEncoding ); //(@am): Con Unicode, los carateres latinos se ven correctamente en Notepad y desde el CMD.
                            end;
                         finally
                                FreeAndNil( oArchivo );
                                Screen.Cursor := oCursor;
                         end;
                         ZetaDialogo.ZInformation( Self.Caption, Format( 'Encuesta "%s" exportada con �xito', [ sNombre ] ), 0 );
                    end;
               end;
          end;
     end;
end;

procedure TEncuestas.btnImportarClick(Sender: TObject);
var
   sArchivo: String;
   oCursor: TCursor;
   lImportada: Boolean;
begin
     inherited;
     with OpenDialog do
     begin
          Title := 'Seleccionar encuesta';
          InitialDir := Global.GetGlobalString( K_GLOBAL_DIR_PLANT );
     end;
     sArchivo := ZetaClientTools.AbreDialogo( OpenDialog, VACIO, 'EN1' );
     if StrLleno( sArchivo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             lImportada := dmEvaluacion.ImportaEncuesta( sArchivo );
          finally
                 Screen.Cursor := oCursor;
          end;
          if lImportada then
             ZetaDialogo.ZInformation( Self.Caption, 'Encuesta importada con �xito', 0 );
     end;
     DoBestFit;
end;

procedure TEncuestas.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     ET_CODIGO.Options.Grouping  := FALSE;
     ET_FEC_FIN.Options.Grouping:= FALSE;
     ET_FEC_INI.Options.Grouping:= FALSE;
     ET_NOMBRE.Options.Grouping:= FALSE;
     ET_NUM_COM.Options.Grouping:= FALSE;
     ET_NUM_EVA.Options.Grouping:= FALSE;
     ET_NUM_SUJ.Options.Grouping:= FALSE;
     ET_STATUS.Options.Grouping:= TRUE;
end;



end.
