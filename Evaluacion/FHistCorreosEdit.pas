unit FHistCorreosEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, OleCtrls, SHDocVw,
     ZBaseEdicion_DevEx,
     ZetaDBTextBox,
     ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons, cxContainer, cxEdit, cxGroupBox;

type
  THistCorreosEdit = class(TBaseEdicion_DevEx)
    TareaGB: TcxGroupBox;
    WP_FOLIOlbl: TLabel;
    PASOACTUALlbl: TLabel;
    WM_CODIGOlbl: TLabel;
    PanelDatos: TPanel;
    WO_CClbl: TLabel;
    WO_SUBJECTlbl: TLabel;
    WO_BODYlbl: TLabel;
    WO_SUBTYPElbl: TLabel;
    WO_ENVIADOlbl: TLabel;
    WO_FEC_OUTlbl: TLabel;
    WO_SUBJECT: TZetaDBTextBox;
    WO_SUBTYPE: TZetaDBTextBox;
    WO_ENVIADO: TZetaDBTextBox;
    WO_FEC_OUT: TZetaDBTextBox;
    WP_FOLIO: TZetaDBTextBox;
    WM_CODIGO: TZetaDBTextBox;
    PASOACTUAL: TZetaDBTextBox;
    WS_NOMBRE: TZetaDBTextBox;
    WO_BODY: TDBMemo;
    WO_CC: TDBMemo;
    WO_STATUSlbl: TLabel;
    WO_STA_TXT: TZetaDBTextBox;
    RemitenteGB: TcxGroupBox;
    WO_FROM_NAlbl: TLabel;
    WO_FROM_NA: TZetaDBTextBox;
    WO_FROM_AD: TZetaDBTextBox;
    REMITENTElbl: TLabel;
    REMITENTE: TZetaDBTextBox;
    DestinatarioGB: TcxGroupBox;
    WO_TOlbl: TLabel;
    WO_TO: TDBMemo;
    DESTINOlbl: TLabel;
    DESTINO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  HistCorreosEdit: THistCorreosEdit;

implementation

uses ZAccesosTress,
     ZetaCommonClasses,
     DEvaluacion,
     ZHelpContext;

{$R *.DFM}

{ THistCorreosEdit }

procedure THistCorreosEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_HISTORIAL_CORREOS;
     IndexDerechos := D_EVAL_CORREOS;
     OK_Devex.Visible := False;
end;

procedure THistCorreosEdit.FormShow(Sender: TObject);
begin
     inherited;
     dxBarButton_ExportarBtn.Enabled:=FALSE;
     dxBarButton_CortarBtn.Enabled:=FALSE;
     dxBarButton_CopiarBtn.Enabled:=FALSE;
     dxBarButton_PegarBtn.Enabled:=FALSE;
     dxBarButton_AgregarBtn.Enabled:=FALSE;
     dxBarButton_BorrarBtn.Enabled:=FALSE;
     dxBarButton_ModificarBtn.Enabled:=FALSE;
end;

procedure THistCorreosEdit.Connect;
begin
     Datasource.Dataset := dmEvaluacion.cdsCorreos;
end;

function THistCorreosEdit.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No es posible agregar correos';
end;

function THistCorreosEdit.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No es posible borrar correos';
end;

function THistCorreosEdit.PuedeImprimir(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Cambio debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_BAJA );
     if not Result then
        sMensaje := 'No est� permitido imprimir correos';
end;

function THistCorreosEdit.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No es posible modificar correos';
end;

end.
