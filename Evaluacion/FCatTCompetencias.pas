unit FCatTCompetencias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, StdCtrls, Grids, DBGrids, ZetaDBGrid,ZBaseGridLectura_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxClasses, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TTipoCompetencias = class(TBaseGridLectura_DevEx)
    TC_CODIGO: TcxGridDBColumn;
    TC_DESCRIP: TcxGridDBColumn;
    TC_INGLES: TcxGridDBColumn;
    TC_NUMERO: TcxGridDBColumn;
    TC_TEXTO: TcxGridDBColumn;
    TC_TIPO: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  TipoCompetencias: TTipoCompetencias;

implementation

{$R *.DFM}

uses DEvaluacion,
     ZHelpContext,
     ZetaBuscador_DevEx,
     ZetaCommonLists,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonClasses;


{ TTipoCompetencias }
procedure TTipoCompetencias.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_CATALOGOS_TIPOS_COMPETENCIA;
end;

procedure TTipoCompetencias.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TC_CODIGO'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TTipoCompetencias.Agregar;
begin
     dmEvaluacion.cdsTCompetencia.Agregar;
     DoBestFit;
end;

procedure TTipoCompetencias.Borrar;
begin
     dmEvaluacion.cdsTCompetencia.Borrar;
     DoBestFit;
end;

procedure TTipoCompetencias.Connect;
begin
     with dmEvaluacion do
     begin
          cdsTCompetencia.Conectar;
          DataSource.DataSet:= cdsTCompetencia;
     end;
end;

procedure TTipoCompetencias.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'tipo de competencia', 'TC_CODIGO', dmEvaluacion.cdsTCompetencia );
end;

procedure TTipoCompetencias.ImprimirForma;
begin
  inherited;

end;

procedure TTipoCompetencias.Modificar;
begin
     dmEvaluacion.cdsTCompetencia.Modificar;
     DoBestFit;
end;

procedure TTipoCompetencias.Refresh;
begin
     dmEvaluacion.cdsTCompetencia.Refrescar;
     DoBestFit;
end;

procedure TTipoCompetencias.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := FALSE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := FALSE; //Muestra la caja de agrupamiento
end;



end.
