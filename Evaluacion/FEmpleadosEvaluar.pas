unit FEmpleadosEvaluar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, ExtCtrls, StdCtrls, Mask, Buttons, Grids, DBGrids, ComCtrls,
     ZBaseConsulta,
     ZetaNumero,
     ZetaDBTextBox,
     ZetaKeyCombo,
     ZetaDBGrid,
     ZetaKeyLookup;

type
  TEmpleadosEvaluar = class(TBaseConsulta)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    ztbCuantos: TZetaTextBox;
    Label3: TLabel;
    znEmpleado: TZetaNumero;
    GridEmp: TZetaDBGrid;
    SJ_STATUS: TComboBox;
    btnBuscaEmp: TSpeedButton;
    btnFiltrar: TBitBtn;
    StatusBar1: TStatusBar;
    CB_CODIGO: TZetaKeyLookup;
    procedure btnBuscaEmpClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure LlenaListaStatus;
    procedure CuantosRegistros;
    procedure CargarFiltros;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  EmpleadosEvaluar: TEmpleadosEvaluar;

implementation

{$R *.DFM}

uses DEvaluacion,
     ZHelpContext,
     ZetaMsgDlg,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZetaBuscador,
     ZImprimeForma,
     ZetaCommonTools,
     ZetaBuscaEmpleado,
     ZetaCommonLists,
     DCliente;

{ TEmpleadosEvaluar }

procedure TEmpleadosEvaluar.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     CB_CODIGO.LookupDataSet := dmCliente.cdsEmpleadoLookup;
     HelpContext := H_DISENO_ENCUESTA_EMPLEADOS_EVALUAR;
end;

procedure TEmpleadosEvaluar.FormShow(Sender: TObject);
begin
     inherited;
     CB_CODIGO.Valor := 0;
     LLenaListaStatus;
end;

procedure TEmpleadosEvaluar.Connect;
begin
     with dmEvaluacion do
     begin
          if not cdsEmpEvaluar.Active or cdsEmpEvaluar.HayQueRefrescar then
          begin
               CargarFiltros;
               cdsEmpEvaluar.ResetDataChange;
          end;
          DataSource.DataSet := cdsEmpEvaluar;
     end;
end;

procedure TEmpleadosEvaluar.Agregar;
begin
     dmEvaluacion.cdsEmpEvaluar.Agregar;
end;

procedure TEmpleadosEvaluar.Borrar;
begin
     with GridEmp do
     begin
          with SelectedRows do
          begin
               if ( Count > 1 ) then
               begin
                    if ZetaMsgDlg.ConfirmaCambio( Format( '� Desea borrar estos %d empleados ?', [ Count ] ) ) then
                    begin
                         dmEvaluacion.BorrarEmpleadosPorEvaluar( SelectedRows );
                    end;
               end
               else
                   if ( Count = 1 ) then
                   begin
                        with dmEvaluacion do
                        begin
                             BorrarEmpleadoPorEvaluar( cdsEmpEvaluar.FieldByName( 'CB_CODIGO' ).AsInteger );
                        end;
                   end
                   else
                       ZetaDialogo.zWarning( '� Atenci�n !', 'No se escogi�n ning�n empleado para borrar', 0, mbOk );
          end;
     end;
end;

procedure TEmpleadosEvaluar.LlenaListaStatus;
begin
     ZetaCommonLists.LlenaLista( lfStatusSujeto, SJ_STATUS.Items );
     SJ_STATUS.Items.Insert( 0, 'Todos' );
     SJ_STATUS.ItemIndex := 0;
end;

procedure TEmpleadosEvaluar.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( self.Caption, 'SUJETO', 'CB_CODIGO', dmEvaluacion.cdsEmpEvaluar );
end;

procedure TEmpleadosEvaluar.ImprimirForma;
begin
     inherited;
     ZImprimeForma.ImprimeUnaForma( enEvalua, dmEvaluacion.cdsEmpEvaluar );
end;

procedure TEmpleadosEvaluar.Modificar;
begin
     dmEvaluacion.cdsEmpEvaluar.Modificar;
end;

procedure TEmpleadosEvaluar.Refresh;
begin
     CargarFiltros;
end;

procedure TEmpleadosEvaluar.CuantosRegistros;
begin
     StatusBar1.Panels[0].Text := 'Cantidad : ' + inttoStr( dmEvaluacion.cdsEmpEvaluar.RecordCount );
end;

procedure TEmpleadosEvaluar.btnBuscaEmpClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if ZetaBuscaEmpleado.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
        znEmpleado.Valor :=  StrToIntDef( sKey, 0 );
end;

procedure TEmpleadosEvaluar.btnFiltrarClick(Sender: TObject);
begin
     CargarFiltros;
end;

procedure TEmpleadosEvaluar.CargarFiltros;
var
   iValor: Integer;
   iIndice: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion do
        begin
             iValor := CB_CODIGO.Valor;
             iIndice := SJ_STATUS.ItemIndex;
             ObtieneSujetosEvaluar( iValor, ( iIndice - 1 ), cdsEmpEvaluar );
             EmpleadoEvaluar := iValor;
             StatusEmpEvaluar := ( iIndice - 1 );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     CuantosRegistros;
end;

end.
