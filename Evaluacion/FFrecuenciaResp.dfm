inherited FrecuenciaResp: TFrecuenciaResp
  Left = 294
  Top = 149
  Caption = 'Frecuencia de respuestas'
  ClientHeight = 518
  ClientWidth = 692
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 348
    Width = 692
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  inherited PanelIdentifica: TPanel
    Width = 692
    inherited ValorActivo2: TPanel
      Width = 433
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 692
    Height = 158
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 400
      Top = 135
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Pregunta:'
      Visible = False
    end
    object Label5: TLabel
      Left = 8
      Top = 133
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Relaci�n:'
    end
    object EP_NUMERO1: TZetaKeyLookup
      Left = 451
      Top = 131
      Width = 331
      Height = 21
      TabOrder = 0
      TabStop = True
      Visible = False
      WidthLlave = 70
    end
    object gbPregunta: TGroupBox
      Left = 8
      Top = 8
      Width = 577
      Height = 116
      Caption = ' Pregunta: '
      TabOrder = 1
      object lblFolio: TLabel
        Left = 21
        Top = 20
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Folio:'
      end
      object btnBuscaPregunta: TSpeedButton
        Left = 124
        Top = 14
        Width = 25
        Height = 25
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
          DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
          8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
          C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
          7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
        OnClick = btnBuscaPreguntaClick
      end
      object lblTexto: TLabel
        Left = 16
        Top = 42
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Texto:'
      end
      object EP_NUMERO: TZetaNumero
        Left = 48
        Top = 16
        Width = 73
        Height = 21
        Mascara = mnMinutos
        MaxLength = 3
        TabOrder = 0
        Text = '0'
        UseEnterKey = True
        OnExit = EP_NUMEROExit
        OnKeyPress = EP_NUMEROKeyPress
      end
      object EP_DESCRIP: TMemo
        Left = 48
        Top = 42
        Width = 521
        Height = 65
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -8
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Lines.Strings = (
          '')
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 1
      end
      object btnFiltrar: TBitBtn
        Left = 490
        Top = 11
        Width = 78
        Height = 28
        Caption = 'Filtrar'
        TabOrder = 2
        OnClick = btnFiltrarClick
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          777777777777777770000070000077777000E00BFBFB07777777E0BFBF000777
          7700E0FBFBFBF0777700E0BFBF0000007777E0FBFBFBFBFB0799E0BF00000000
          7799000BFB077777777777700077777777007777777777777700777777777777
          7777777777777777700077777777777770007777777777777777}
      end
    end
    object EV_RELACIO: TZetaKeyCombo
      Left = 56
      Top = 129
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
    end
  end
  object Grafica: TDBChart [3]
    Left = 0
    Top = 351
    Width = 692
    Height = 167
    AllowPanning = pmNone
    AllowZoom = False
    AnimatedZoom = True
    AnimatedZoomSteps = 5
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    BackWall.Pen.Visible = False
    LeftWall.Brush.Color = clWhite
    Title.Text.Strings = (
      'Frecuencia de respuestas')
    AxisVisible = False
    Chart3DPercent = 25
    ClipPoints = False
    Frame.Visible = False
    View3DOptions.Elevation = 315
    View3DOptions.Orthogonal = False
    View3DOptions.Perspective = 0
    View3DOptions.Rotation = 360
    View3DWalls = False
    Align = alClient
    TabOrder = 2
    OnDragDrop = GraficaDragDrop
    object Series1: THorizBarSeries
      Active = False
      ColorEachPoint = True
      Marks.ArrowLength = 20
      Marks.Style = smsPercent
      Marks.Visible = False
      SeriesColor = clRed
      ShowInLegend = False
      XValues.DateTime = False
      XValues.Name = 'Bar'
      XValues.Multiplier = 1
      XValues.Order = loNone
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object Series2: TPieSeries
      Marks.ArrowLength = 8
      Marks.Style = smsPercent
      Marks.Visible = True
      SeriesColor = clGreen
      OtherSlice.Text = 'Other'
      PieValues.DateTime = False
      PieValues.Name = 'Pie'
      PieValues.Multiplier = 1
      PieValues.Order = loNone
    end
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 177
    Width = 692
    Height = 171
    Align = alTop
    TabOrder = 3
    object ZetaDBGrid1: TZetaDBGrid
      Left = 1
      Top = 1
      Width = 690
      Height = 150
      Align = alClient
      DataSource = DataSource
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnTitleClick = ZetaDBGrid1TitleClick
      Columns = <
        item
          Expanded = False
          FieldName = 'EL_NOMBRE'
          Title.Caption = 'Respuesta'
          Width = 500
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'CUANTOS'
          Title.Alignment = taRightJustify
          Title.Caption = 'Frecuencia'
          Visible = True
        end>
    end
    object StatusBar1: TStatusBar
      Left = 1
      Top = 151
      Width = 690
      Height = 19
      Panels = <
        item
          Text = 'Cantidad:'
          Width = 50
        end>
      SimplePanel = False
    end
  end
  inherited DataSource: TDataSource
    Left = 512
    Top = 0
  end
end
