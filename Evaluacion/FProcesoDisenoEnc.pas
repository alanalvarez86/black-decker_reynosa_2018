unit FProcesoDisenoEnc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsultaBotones, ImgList, Db, ToolWin, ComCtrls, ExtCtrls;

type
  TProcesoDisenoEnc = class(TBaseBotones)
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ProcesoDisenoEnc: TProcesoDisenoEnc;

implementation

uses ZHelpContext;

{$R *.DFM}

procedure TProcesoDisenoEnc.FormCreate(Sender: TObject);
begin
  inherited;
  HelpContext := H_DISENO_ENCUESTA_PROCESOS;
end;

end.
