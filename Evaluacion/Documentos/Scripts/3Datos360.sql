

exec sp_addtype GUID, 'uniqueidentifier', 'NOT NULL'
go

exec sp_addtype CuatroDecimales, 'money', 'NOT NULL'
exec sp_bindefault Cero, CuatroDecimales
go



CREATE TABLE ESCALA (
       SC_CODIGO            Codigo,
       SC_NOMBRE            Descripcion,
       SC_NIVELES           FolioChico
)
go


ALTER TABLE ESCALA
       ADD CONSTRAINT PK_ESCALA PRIMARY KEY (SC_CODIGO)
go


CREATE TABLE ESCNIVEL (
       SC_CODIGO            Codigo,
       SN_ORDEN             FolioChico,
       SN_NOMBRE            Descripcion,
       SN_CORTO             Referencia,
       SN_DESCRIP           Formula,
       SN_VALOR             CuatroDecimales
)
go


ALTER TABLE ESCNIVEL
       ADD CONSTRAINT PK_ESCNIVEL PRIMARY KEY (SC_CODIGO, SN_ORDEN)
go


CREATE TABLE PREGUNTA (
       PG_FOLIO             FolioGrande,
       PG_ORDEN             FolioChico,
       CM_CODIGO            Codigo,
       PG_DESCRIP           Formula,
       PG_TIPO              Status,
       PG_CONSEJO           Formula
)
go


ALTER TABLE PREGUNTA
       ADD CONSTRAINT PK_PREGUNTA PRIMARY KEY (PG_FOLIO)
go


CREATE TABLE ENCUESTA (
       ET_CODIGO            FolioGrande,
       ET_NOMBRE            DescLarga,
       ET_FEC_INI           Fecha,
       ET_DESCRIP           Formula,
       ET_FEC_FIN           Fecha,
       ET_STATUS            Status,
       ET_NIVELES           FolioChico,
       ET_PESOS             FolioChico,
       ET_TOT_PES           CuatroDecimales,
       ET_NUM_COM           FolioChico,
       ET_NUM_SUJ           FolioGrande,
       ET_FIN_SUJ           FolioGrande,
       ET_NUM_PRE           FolioChico,
       ET_NUM_EVA           FolioGrande,
       ET_FIN_EVA           FolioGrande,
       ET_USR_ADM           Usuario,
       ET_ESCALA            Descripcion,
       ET_MSG_INI           Memo,
       ET_MSG_FIN           Memo,
       ET_GET_COM           Booleano,
       ET_TXT_COM           Descripcion,
       ET_ESTILO            Status,
       ET_ESC_DEF           Codigo,
       ET_ESC_PES           Codigo
)
go


ALTER TABLE ENCUESTA
       ADD  CONSTRAINT PK_ENCUESTA PRIMARY KEY (ET_CODIGO)
go


CREATE TABLE ENC_COMP (
       ET_CODIGO            FolioGrande,
       EC_ORDEN             FolioChico,
       CM_CODIGO            Codigo,
       EC_PESO              CuatroDecimales,
       EC_NOMBRE            DescLarga,
       EC_TOT_PES           CuatroDecimales,
       EC_DETALLE           Memo,
       EC_NUM_PRE           FolioChico,
       EC_GET_COM           Status,
       EC_POS_PES           FolioChico
)
go


ALTER TABLE ENC_COMP
       ADD CONSTRAINT PK_ENC_COMP PRIMARY KEY (ET_CODIGO, EC_ORDEN)
go


CREATE TABLE ENC_PREG (
       ET_CODIGO            FolioGrande,
       EC_ORDEN             FolioChico,
       EP_ORDEN             FolioChico,
       EP_NUMERO            FolioChico,
       EP_DESCRIP           Formula,
       PG_FOLIO             FolioGrande,
       EP_PESO              CuatroDecimales,
       EP_TIPO              Status,
       EP_GET_COM           Status,
       EP_GET_NA            Status,
       EE_CODIGO            Codigo,
       EP_POS_PES           FolioChico
)
go


ALTER TABLE ENC_PREG
       ADD CONSTRAINT PK_ENC_PREG PRIMARY KEY (ET_CODIGO, EC_ORDEN, EP_ORDEN)
go


CREATE TABLE ENCNIVEL (
       ET_CODIGO            FolioGrande,
       EL_ORDEN             FolioChico,
       EL_NOMBRE            Descripcion,
       EL_CORTO             Referencia,
       EL_DESCRIP           Formula,
       EL_VALOR             CuatroDecimales
)
go


ALTER TABLE ENCNIVEL
       ADD CONSTRAINT PK_ENCNIVEL PRIMARY KEY (ET_CODIGO, EL_ORDEN)
go




CREATE TABLE SUJETO (
       ET_CODIGO            FolioGrande,
       CB_CODIGO            NumeroEmpleado,
       SJ_STATUS            Status,
       SJ_NUM_EVA           FolioChico,
       SJ_FIN_EVA           FolioChico,
       SJ_TOTAL             CuatroDecimales
)
go


ALTER TABLE SUJETO
       ADD  CONSTRAINT PK_SUJETO PRIMARY KEY (ET_CODIGO, CB_CODIGO)
go


CREATE TABLE EVALUA (
       EV_FOLIO             GUID,
       US_CODIGO            Usuario,
       ET_CODIGO            FolioGrande,
       CB_CODIGO            NumeroEmpleado,
       EV_RELACIO           Status,
       EV_STATUS            Status,
       EV_TOTAL             CuatroDecimales,
       EV_COMENTA           Memo,
       EV_FEC_INI           Fecha,
       EV_FEC_FIN           Fecha,
       EV_FIN_PRE           FolioChico
)
go

CREATE UNIQUE INDEX XAK1EVALUA ON EVALUA
(
       ET_CODIGO,
       CB_CODIGO,
       US_CODIGO
)
go


ALTER TABLE EVALUA
       ADD  CONSTRAINT PK_EVALUA PRIMARY KEY (EV_FOLIO)
go

CREATE INDEX XIE1EVALUA ON EVALUA
(
       ET_CODIGO,
       CB_CODIGO
)
go

CREATE TABLE ENC_RELA (
       ET_CODIGO            FolioGrande,
       ER_TIPO              Status,
       ER_NUM_MIN           FolioChico,
       ER_NOMBRE            Descripcion,
       ER_NUM_MAX           FolioChico,
       ER_PESO              CuatroDecimales,
       ER_POS_PES	    FolioChico
)
go


ALTER TABLE ENC_RELA
       ADD CONSTRAINT PK_ENC_RELA PRIMARY KEY (ET_CODIGO, ER_TIPO)
go


CREATE TABLE EVA_COMP (
       EV_FOLIO             GUID,
       VC_ORDEN             FolioChico,
       VC_COMENTA           Memo,
       VC_TOTAL             CuatroDecimales,
       VC_INCLUYE           Booleano,
       EC_ORDEN             FolioChico
)
go


ALTER TABLE EVA_COMP
       ADD CONSTRAINT PK_EVA_COMP PRIMARY KEY (EV_FOLIO, VC_ORDEN)
go


CREATE TABLE EVA_PREG (
       EV_FOLIO             GUID,
       VC_ORDEN             FolioChico,
       VP_ORDEN             FolioChico,
       VP_RESPUES           CuatroDecimales,
       VP_COMENTA           Formula,
       EP_NUMERO            FolioChico,
       VP_CUAL              FolioChico
)
go


ALTER TABLE EVA_PREG
       ADD CONSTRAINT PK_EVA_PREG PRIMARY KEY (EV_FOLIO, VC_ORDEN, VP_ORDEN)
go




CREATE TABLE SUJ_COMP (
       ET_CODIGO            FolioGrande,
       CB_CODIGO            NumeroEmpleado,
       EC_ORDEN             FolioChico,
       SO_COMENTA           Formula,
       SO_NUM_R0            NumeroEmpleado,
       SO_SUM_R0            CuatroDecimales,
       SO_NUM_R1            NumeroEmpleado,
       SO_SUM_R1            CuatroDecimales,
       SO_NUM_R2            NumeroEmpleado,
       SO_SUM_R2            CuatroDecimales,
       SO_NUM_R3            NumeroEmpleado,
       SO_SUM_R3            CuatroDecimales,
       SO_NUM_R4            NumeroEmpleado,
       SO_SUM_R4            CuatroDecimales,
       SO_NUM_R5            NumeroEmpleado,
       SO_SUM_R5            CuatroDecimales,
       SO_NUM_TOT           NumeroEmpleado,
       SO_SUM_TOT           CuatroDecimales,
       SO_TOTAL             CuatroDecimales,
       SO_FINAL             CuatroDecimales
)
go


ALTER TABLE SUJ_COMP
       ADD CONSTRAINT PK_SUJ_COMP PRIMARY KEY (ET_CODIGO, CB_CODIGO, EC_ORDEN)
go

CREATE TABLE SUJ_PREG (
       ET_CODIGO            FolioGrande,
       CB_CODIGO            NumeroEmpleado,
       EC_ORDEN             FolioChico,
       EP_ORDEN             FolioChico,
       EP_NUMERO            FolioChico,
       SP_COMENTA           Formula,
       SP_NUM_R0            NumeroEmpleado,
       SP_SUM_R0            CuatroDecimales,
       SP_NUM_R1            NumeroEmpleado,
       SP_SUM_R1            CuatroDecimales,
       SP_NUM_R2            NumeroEmpleado,
       SP_SUM_R2            CuatroDecimales,
       SP_NUM_R3            NumeroEmpleado,
       SP_SUM_R3            CuatroDecimales,
       SP_NUM_R4            NumeroEmpleado,
       SP_SUM_R4            CuatroDecimales,
       SP_NUM_R5            NumeroEmpleado,
       SP_SUM_R5            CuatroDecimales,
       SP_NUM_TOT           NumeroEmpleado,
       SP_SUM_TOT           CuatroDecimales,
       SP_TOTAL             CuatroDecimales,
       SP_FINAL             CuatroDecimales
)
go

ALTER TABLE SUJ_PREG
       ADD CONSTRAINT PK_SUJ_PREG PRIMARY KEY (ET_CODIGO, CB_CODIGO, EC_ORDEN, EP_ORDEN)
go

CREATE TABLE ENC_ESCA (
       ET_CODIGO            FolioGrande,
       EE_CODIGO            Codigo,
       EE_NOMBRE            Descripcion,
       EE_NIVELES           FolioChico,
       EE_NA_NOMB           Descripcion,
       EE_NA_CORT           Referencia
)
go


ALTER TABLE ENC_ESCA
       ADD CONSTRAINT PK_ENC_ESCA PRIMARY KEY (ET_CODIGO, EE_CODIGO);
GO

CREATE TABLE ENC_NIVE (
       ET_CODIGO            FolioGrande,
       EE_CODIGO            Codigo,
       EL_ORDEN             FolioChico,
       EL_NOMBRE            Descripcion,
       EL_CORTO             Referencia,
       EL_DESCRIP           Formula,
       EL_VALOR             CuatroDecimales
)
GO

ALTER TABLE ENC_NIVE
       ADD CONSTRAINT PK_ENC_NIVE PRIMARY KEY (ET_CODIGO, EE_CODIGO, EL_ORDEN);
GO




ALTER TABLE ESCNIVEL
       ADD CONSTRAINT FK_EscNivel_Escala
       FOREIGN KEY (SC_CODIGO)
       REFERENCES ESCALA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE ENC_RELA
       ADD CONSTRAINT FK_Enc_Rela_Encuesta
       FOREIGN KEY (ET_CODIGO)
       REFERENCES ENCUESTA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE ENC_COMP
       ADD CONSTRAINT FK_Enc_Comp_Encuesta
       FOREIGN KEY (ET_CODIGO)
       REFERENCES ENCUESTA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE ENC_PREG
       ADD CONSTRAINT FK_Enc_Preg_Enc_Comp
       FOREIGN KEY (ET_CODIGO, EC_ORDEN)
       REFERENCES ENC_COMP
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE SUJETO
       ADD CONSTRAINT FK_Sujeto_Encuesta
       FOREIGN KEY (ET_CODIGO)
       REFERENCES ENCUESTA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE SUJETO
       ADD CONSTRAINT FK_Sujeto_Colabora
       FOREIGN KEY (CB_CODIGO)
       REFERENCES COLABORA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE EVA_COMP
       ADD CONSTRAINT FK_Eva_Comp_Evalua
       FOREIGN KEY (EV_FOLIO)
       REFERENCES EVALUA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE EVA_PREG
       ADD CONSTRAINT FK_Eva_Preg_Eva_Comp
       FOREIGN KEY (EV_FOLIO, VC_ORDEN)
       REFERENCES EVA_COMP
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE EVALUA
       ADD CONSTRAINT FK_Evalua_Encuesta
       FOREIGN KEY (ET_CODIGO)
       REFERENCES ENCUESTA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE PREGUNTA
       ADD CONSTRAINT FK_Pregunta_Competen
       FOREIGN KEY (CM_CODIGO)
       REFERENCES COMPETEN
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE SUJ_COMP
       ADD CONSTRAINT FK_Suj_Comp_Sujeto
       FOREIGN KEY (ET_CODIGO, CB_CODIGO)
       REFERENCES SUJETO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
go

ALTER TABLE SUJ_PREG
       ADD CONSTRAINT FK_Suj_Preg_Suj_Comp
       FOREIGN KEY (ET_CODIGO, CB_CODIGO, EC_ORDEN)
       REFERENCES SUJ_COMP
       ON DELETE CASCADE
       ON UPDATE CASCADE;
go

ALTER TABLE ENC_ESCA
       ADD CONSTRAINT FK_Enc_Esca_Encuesta
       FOREIGN KEY (ET_CODIGO)
       REFERENCES ENCUESTA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
go

ALTER TABLE ENC_NIVE
       ADD CONSTRAINT FK_Enc_Niv_Enc_Esca
       FOREIGN KEY (ET_CODIGO, EE_CODIGO)
       REFERENCES ENC_ESCA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
go

CREATE TABLE [COMPETEN] (
	[CM_CODIGO] [Codigo] NOT NULL ,
	[TC_CODIGO] [Codigo] NOT NULL ,
	[CM_DESCRIP] [Descripcion] NOT NULL ,
	[CM_INGLES] [Descripcion] NOT NULL ,
	[CM_NUMERO] [Pesos] NOT NULL ,
	[CM_TEXTO] [Descripcion] NOT NULL ,
	[CM_DETALLE] [Memo] NULL ,
	CONSTRAINT [PK_COMPETEN] PRIMARY KEY  CLUSTERED 
	(
		[CM_CODIGO]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_COMPETEN_TCOMPETE] FOREIGN KEY 
	(
		[TC_CODIGO]
	) REFERENCES [TCOMPETE] (
		[TC_CODIGO]
	) ON UPDATE CASCADE 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [TCOMPETE] (
	[TC_CODIGO] [Codigo] NOT NULL ,
	[TC_DESCRIP] [Descripcion] NOT NULL ,
	[TC_INGLES] [Descripcion] NOT NULL ,
	[TC_NUMERO] [Pesos] NOT NULL ,
	[TC_TEXTO] [Descripcion] NOT NULL ,
	[TC_TIPO] [Status] NOT NULL ,
	CONSTRAINT [PK_TCOMPETE] PRIMARY KEY  CLUSTERED 
	(
		[TC_CODIGO]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


/* NO hay FK de EVALUA hacia SUJETO porque el EMPLEADO es opcional en "Encuestas" */
CREATE TRIGGER TD_SUJETO ON SUJETO AFTER DELETE AS
BEGIN
	SET NOCOUNT ON

	delete EVALUA
	from   EVALUA, Deleted
	where  EVALUA.ET_CODIGO = DELETED.ET_CODIGO and EVALUA.CB_CODIGO = Deleted.CB_CODIGO
END
GO

CREATE TRIGGER TU_SUJETO ON SUJETO AFTER UPDATE AS
BEGIN
  IF UPDATE( CB_CODIGO )
  BEGIN
  	SET NOCOUNT ON
	declare @NewEmpleado NumeroEmpleado, @OldEmpleado NumeroEmpleado
	declare @OldCodigo FolioGrande

	select @NewEmpleado = CB_CODIGO from   Inserted;
	select @OldEmpleado = CB_CODIGO, @OldCodigo = ET_CODIGO from   Deleted;

	UPDATE EVALUA
	SET    CB_CODIGO = @NewEmpleado
	WHERE  ET_CODIGO = @OldCodigo and CB_CODIGO = @OldEmpleado
  END
END
GO

CREATE TRIGGER TU_COMPETEN ON COMPETEN AFTER UPDATE AS
BEGIN
  IF UPDATE( CM_CODIGO )
  BEGIN
	SET NOCOUNT ON
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = CM_CODIGO from   Inserted;
	select @OldCodigo = CM_CODIGO from   Deleted;

	UPDATE ENC_COMP
	SET    CM_CODIGO = @NewCodigo
	WHERE  CM_CODIGO = @OldCodigo
  END
END
GO

CREATE TRIGGER TD_COMPETEN ON COMPETEN AFTER DELETE AS
BEGIN
	SET NOCOUNT ON
	update ENC_COMP
	set    ENC_COMP.CM_CODIGO = ''
	from   ENC_COMP, Deleted
	where  ENC_COMP.CM_CODIGO = Deleted.CM_CODIGO
END
GO

CREATE TRIGGER TU_PREGUNTA ON PREGUNTA AFTER UPDATE AS
BEGIN
  IF UPDATE( PG_FOLIO )
  BEGIN
	SET NOCOUNT ON
	declare @NewCodigo FolioGrande, @OldCodigo FolioGrande;

	select @NewCodigo = PG_FOLIO from   Inserted;
	select @OldCodigo = PG_FOLIO from   Deleted;

	UPDATE ENC_PREG
	SET    PG_FOLIO = @NewCodigo
	WHERE  PG_FOLIO = @OldCodigo
  END
END
GO

CREATE TRIGGER TD_PREGUNTA ON PREGUNTA AFTER DELETE AS
BEGIN
	SET NOCOUNT ON
	update ENC_PREG
	set    ENC_PREG.PG_FOLIO = 0
	from   ENC_PREG, Deleted
	where  ENC_PREG.PG_FOLIO = Deleted.PG_FOLIO
END
GO


CREATE TRIGGER TU_ENC_ESCA ON ENC_ESCA AFTER UPDATE AS
BEGIN
  IF UPDATE( EE_CODIGO )
  BEGIN
	SET NOCOUNT ON
	declare @NewCodigo Codigo, @OldCodigo Codigo
	declare @OldEncuesta FolioGrande

	select @NewCodigo = EE_CODIGO from   Inserted;
	select @OldCodigo = EE_CODIGO, @OldEncuesta = ET_CODIGO from   Deleted;

	UPDATE ENC_PREG
	SET    EE_CODIGO = @NewCodigo
	WHERE  ET_CODIGO = @OldEncuesta and EE_CODIGO = @OldCodigo

	UPDATE ENCUESTA
	SET    ET_ESC_DEF = @NewCodigo	
	WHERE  ET_CODIGO = @OldEncuesta and ET_ESC_DEF = @OldCodigo

	UPDATE ENCUESTA
	SET    ET_ESC_PES = @NewCodigo	
	WHERE  ET_CODIGO = @OldEncuesta and ET_ESC_PES = @OldCodigo
  END
END
GO

CREATE TRIGGER TD_ENC_ESCA ON ENC_ESCA AFTER DELETE AS
BEGIN
	SET NOCOUNT ON
	update ENC_PREG
	set    ENC_PREG.EE_CODIGO = ''
	from   ENC_PREG, Deleted
	where  ENC_PREG.ET_CODIGO = Deleted.ET_CODIGO and ENC_PREG.EE_CODIGO = Deleted.EE_CODIGO

	update ENCUESTA
	set    ENCUESTA.ET_ESC_DEF = ''
	from   ENCUESTA, Deleted
	where  ENCUESTA.ET_CODIGO = Deleted.ET_CODIGO and ENCUESTA.ET_ESC_DEF = Deleted.EE_CODIGO

	update ENCUESTA
	set    ENCUESTA.ET_ESC_PES = ''
	from   ENCUESTA, Deleted
	where  ENCUESTA.ET_CODIGO = Deleted.ET_CODIGO and ENCUESTA.ET_ESC_PES = Deleted.EE_CODIGO
END
GO


