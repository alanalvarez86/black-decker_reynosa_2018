unit FEditEmpEvaluar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, ComCtrls, ZetaDBTextBox,
  ImgList;

type
  TEditEmpEvaluar = class(TZetaDlgModal)
    Panel1: TPanel;
    Label1: TLabel;
    ztbTitulo: TZetaTextBox;
    Label2: TLabel;
    ztbEmpleado: TZetaTextBox;
    Panel2: TPanel;
    btnAgregar: TBitBtn;
    btnModificar: TBitBtn;
    btnBorrar: TBitBtn;
    tvArbol: TTreeView;
    ImageList1: TImageList;
    Label3: TLabel;
    ztbStatus: TZetaTextBox;
    cbListoEvaluar: TCheckBox;
    Label4: TLabel;
    SJ_NUM_EVA: TZetaTextBox;
    procedure FormShow(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure tvArbolDblClick(Sender: TObject);
    procedure btnBorrarClick(Sender: TObject);
    procedure btnModificarClick(Sender: TObject);
    procedure tvArbolDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure tvArbolDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure cbListoEvaluarClick(Sender: TObject);
    procedure RefrescaStatusSujeto( const iStatus: Integer );
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FEmpleado: Integer;
    function CambiaCodigoEvalua(const iCodigoOrigen, iCodigoDestino: Integer): boolean;
    procedure RefrescaNumeroSujetos;
    procedure BorraEvaluador( const oNodo: TTreeNode );
  public
    { Public declarations }
  end;

var
  EditEmpEvaluar: TEditEmpEvaluar;

const
     Q_AUTO_EVALUACION = 0;

implementation

{$R *.DFM}
uses DEvaluacion,
     DCliente,
     DSistema,
     FAgregarEmpleado,
     FModificaEmpleado,
     ZHelpContext,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo;


procedure TEditEmpEvaluar.FormCreate(Sender: TObject);
begin
  inherited;
  HelpContext := H_DISENO_ENCUESTA_EMPLEADOS_EVALUAR_AGREGAR_REGISTRO;
end;
     
procedure TEditEmpEvaluar.FormShow(Sender: TObject);
begin
     inherited;
     with dmEvaluacion do
     begin
          FEmpleado := cdsEmpEvaluar.FieldByName('CB_CODIGO').AsInteger;
          ztbTitulo.Caption := InttoStr( dmCliente.Encuesta ) + ' = ' + dmCliente.Nombre;
          ztbEmpleado.Caption := InttoStr( FEmpleado ) + ' = ' +
                                 cdsEmpEvaluar.FieldByName('PRETTYNAME').AsString;
          ArbolEvaluadores := tvArbol;
          CreaArbolEmpEvaluar;
          tvArbol.SetFocus;
     end;
     RefrescaNumeroSujetos;
end;

procedure TEditEmpEvaluar.RefrescaStatusSujeto( const iStatus: Integer );
begin
     ztbStatus.Caption := ObtieneElemento( lfStatusSujeto, iStatus );
end;

procedure TEditEmpEvaluar.btnAgregarClick(Sender: TObject);
var
   iRelacion: Integer;
   oNodo, oNodoPadre: TTreeNode;
begin
     with dmEvaluacion do
     begin
          if( tvArbol.Items.Count > 0 )then
          begin
               tvArbol.SetFocus;
               oNodo := tvArbol.Selected;
               if( oNodo.Parent = nil )then
                   iRelacion := Integer( oNodo.Data )
               else
               begin
                    oNodoPadre := oNodo.Parent;
                    iRelacion := Integer( oNodoPadre.Data )
               end;
               FAgregarEmpleado.AbreAgregaEmpleadoEvaluar( iRelacion, ( iRelacion = Q_AUTO_EVALUACION ) );
               //MA: Para que cuando se agregue se refresque el numero de evaluadores sin perder filtros
               ObtieneSujetosEvaluar( EmpleadoEvaluar, StatusEmpEvaluar, cdsEmpEvaluar );
               cdsEmpEvaluar.Locate( 'CB_CODIGO', FEmpleado, [] );
               RefrescaNumeroSujetos;
          end
          else
              ZetaDialogo.ZInformation( self.Caption, 'No se han creado las relaciones para los usuarios', 0 );
     end;
end;

procedure TEditEmpEvaluar.RefrescaNumeroSujetos;
begin
     SJ_NUM_EVA.Caption := InttoStr( dmEvaluacion.cdsEmpEvaluar.FieldByName('SJ_NUM_EVA').AsInteger );
     with cbListoEvaluar do
     begin
          Checked := ( dmEvaluacion.cdsEmpEvaluar.FieldByName('SJ_STATUS').AsInteger = Ord( ssListo ) );
          Enabled := ( dmEvaluacion.cdsEmpEvaluar.FieldByName('SJ_STATUS').AsInteger <= 1 );
     end;
     RefrescaStatusSujeto( dmEvaluacion.cdsEmpEvaluar.FieldByName('SJ_STATUS').AsInteger );
end;

procedure TEditEmpEvaluar.tvArbolDblClick(Sender: TObject);
var
   oNodo: TTreeNode;
   iNodoSeleccionado: Integer;
begin
     inherited;
     oNodo := tvArbol.Selected;
     if( oNodo.Parent = nil )then
     begin
          oNodo.Expand( True );
          iNodoSeleccionado := Integer( oNodo.Data );
          FAgregarEmpleado.AbreAgregaEmpleadoEvaluar( iNodoSeleccionado, ( iNodoSeleccionado = Q_AUTO_EVALUACION ) );
     end
     else
         FModificaEmpleado.AbreModificarEmpleadoEvaluar;
     RefrescaNumeroSujetos;
end;

procedure TEditEmpEvaluar.btnBorrarClick(Sender: TObject);
var
   oNodo: TTreeNode;
   iNumPreguntas: Integer;
begin
     inherited;
     with dmEvaluacion do
     begin
          oNodo := tvArbol.Selected;
          if( ( oNodo <> Nil ) and ( oNodo.Parent <> Nil ) )then
          begin
               if( ZetaDialogo.ZConfirm( self.Caption, '�Desea borrar el registro?', 0, mbNo ) )then
               begin
                    iNumPreguntas := ObtenerPreguntasContestadas(  Integer( oNodo.Data ), cdsEmpEvaluar.FieldByName('CB_CODIGO').AsInteger );
                    if( iNumPreguntas > 0 )then
                    begin
                         if( ZetaDialogo.ZConfirm( self.Caption, Format( ' El evaluador seleccionado tiene %d preguntas contestadas.' + CR_LF +
                                                                         CR_LF + '�Desea borrarlo?', [ iNumPreguntas ] ), 0, mbNo ) )then
                             BorraEvaluador( oNodo );
                    end
                    else
                        BorraEvaluador( oNodo );
               end;
          end
          else
              ZetaDialogo.ZInformation( self.Caption, 'Se debe de seleccionar a un usuario', 0 );
     end;
end;

procedure TEditEmpEvaluar.BorraEvaluador( const oNodo: TTreeNode );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion do
        begin
             BorraEvaluadoArbol( oNodo, dmEvaluacion.cdsEmpEvaluar.FieldByName('CB_CODIGO').AsInteger );
             //MA: Para que cuando borre se refresque el numero de evaluadores sin perder filtros
             ObtieneSujetosEvaluar( EmpleadoEvaluar, StatusEmpEvaluar, cdsEmpEvaluar );
             cdsEmpEvaluar.Locate( 'CB_CODIGO', FEmpleado, [] );
        end;
        RefrescaNumeroSujetos;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditEmpEvaluar.btnModificarClick(Sender: TObject);
begin
     inherited;
     FModificaEmpleado.AbreModificarEmpleadoEvaluar;
end;

procedure TEditEmpEvaluar.tvArbolDragDrop(Sender, Source: TObject; X, Y: Integer);
var
   oNodoOrigen, oNodoDestino, oNodoDestinoFinal: TTreeNode;
   iCodigoOrigen, iCodigoDestino: Integer;
begin
     inherited;
     oNodoOrigen:= tvArbol.Selected;
     oNodoDestino:= tvArbol.GetNodeAt( X, Y );
     if( oNodoDestino.Parent <> Nil )then
         oNodoDestinoFinal := oNodoDestino.Parent
     else
         oNodoDestinoFinal := oNodoDestino;
     iCodigoOrigen := Integer( oNodoOrigen.Data );
     iCodigoDestino := Integer( oNodoDestinoFinal.Data );
     if( Assigned( oNodoDestinoFinal ) )then //and ( oNodoDestino.Parent = nil ) and ( oNodoOrigen.Parent <> nil ) )then
     begin
          if CambiaCodigoEvalua( iCodigoOrigen, iCodigoDestino ) then
          begin
               tvArbol.Items.BeginUpdate;
               try
                  oNodoOrigen.MoveTo( oNodoDestinoFinal, naAddChild );
                  oNodoDestinoFinal.Expand( True );
               finally
                      tvArbol.Items.EndUpdate;
               end;
          end;
     end;
end;

procedure TEditEmpEvaluar.tvArbolDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
   oNodoDestino, oNodoSeleccionado: TTreeNode;
begin
     inherited;
     Accept := ( Sender is TTreeView );
     if ( Accept ) then
     begin
          with TTreeView( Sender ) do
          begin
               oNodoDestino := GetNodeAt( X, Y );
               oNodoSeleccionado := Selected;
               Accept:= ( Assigned( oNodoDestino ) ) and ( oNodoDestino <> oNodoSeleccionado ) and
                        ( not oNodoDestino.HasAsParent( oNodoSeleccionado ) );  //Esta Condici�n es para evitar la Relaci�n Circular
          end;
     end;
end;

function TEditEmpEvaluar.CambiaCodigoEvalua(const iCodigoOrigen, iCodigoDestino: Integer): boolean;
begin
     with dmSistema.cdsUsuarios do
     begin
          Result := Locate( 'US_CODIGO', iCodigoOrigen, [] );
          if ( Result ) then
          begin
               with dmEvaluacion do
               begin
                    GrabaSujetoRelacion( FieldByName('US_CODIGO').AsInteger, iCodigoDestino,  cdsEmpEvaluar.FieldByName('CB_CODIGO').AsInteger, False );
               end;
          end;
     end;
end;

procedure TEditEmpEvaluar.cbListoEvaluarClick(Sender: TObject);
var
   iEmpleado, iFiltrar: Integer;
begin
     inherited;
     iFiltrar := 0;
     with dmEvaluacion do
     begin
          if( EmpleadoEvaluar > 0 )then
          begin
               iEmpleado := EmpleadoEvaluar;
               iFiltrar := EmpleadoEvaluar;
          end
          else
          begin
               iEmpleado := FEmpleado;//cdsEmpEvaluar.FieldByName('CB_CODIGO').AsInteger;
          end;
          CambiaStatusSujeto( iEmpleado, cbListoEvaluar.Checked );
          ObtieneSujetosEvaluar( iFiltrar, StatusEmpEvaluar, cdsEmpEvaluar );
          with cdsEmpEvaluar do
          begin
               DisableControls;
               try
                  Locate( 'CB_CODIGO', iEmpleado, [] );
               finally
                      EnableControls;
               end;
          end;
     end;
     if(  cbListoEvaluar.Checked )then
          RefrescaStatusSujeto( Ord( ssListo ) )
     else
         RefrescaStatusSujeto( Ord( ssNuevo ) );
end;

procedure TEditEmpEvaluar.FormKeyPress(Sender: TObject; var Key: Char);
begin
     inherited;
     if( Key = #27 )then
         Self.Close;
end;



end.
