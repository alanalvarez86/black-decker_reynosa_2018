unit DThreads;

interface

uses Classes, SysUtils, Forms, Windows, ComObj, ActiveX, Variants,
     ZBaseThreads,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaClientTools,
{$ifdef DOS_CAPAS}
     DServerEvaluacion;
{$else}
     Evaluacion_TLB;
{$endif}


type
{ *** Threads de Evaluacion *** }
  TEvaluacionThread = class( TBaseThread )
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerEvaluacion;
    property Servidor: TdmServerEvaluacion read GetServidor;
{$else}
    function GetServidor: IdmServerEvaluacionDisp;
    property Servidor: IdmServerEvaluacionDisp read GetServidor;
{$endif}
  protected
    { Protected declarations }
    function Procesar: OleVariant; override;
  end;


implementation

uses ZetaCommonTools,
     DCliente,
     DEvaluacion,
     DProcesos,
     DConsultas;


{ ******* TEvaluacionThread ******** }

{$ifdef DOS_CAPAS}
function TEvaluacionThread.GetServidor: TdmServerEvaluacion;
begin
     Result := DCliente.dmCliente.ServerEvaluacion;
end;
{$else}
function TEvaluacionThread.GetServidor: IdmServerEvaluacionDisp;
begin
     Result := IdmServerEvaluacionDisp( CreateServer( CLASS_dmServerEvaluacion ) );
end;
{$endif}

function TEvaluacionThread.Procesar: OleVariant;
begin
     case Proceso of
          prDEAgregaEmpEvaluarGlobal   :
          begin
               if VarIsNull( Employees ) then
                   Result := Servidor.AgregaEmpEvaluarGlobal( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.AgregaEmpEvaluarGlobalLista( Empresa, Employees, Parametros.VarValues );
          end;
          prDEPrepararEvaluaciones :
          begin
               Result := Servidor.PrepararEvaluaciones( Empresa, Parametros.VarValues );
          end;
          prDECalcularResultados :
          begin
               Result := Servidor.CalcularResultados( Empresa, Parametros.VarValues );
          end;
          prDERecalcularPromedios :
          begin
               Result := Servidor.RecalcularPromedios( Empresa, Parametros.VarValues );
          end;
          prDECierraEvaluaciones:
          begin
               if VarIsNull( Employees ) then
                   Result := Servidor.AgregaEmpEvaluarGlobal( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.AgregaEmpEvaluarGlobalLista( Empresa, Employees, Parametros.VarValues );
          end;
          prDECorreosInvitacion:
          begin
               if VarIsNull( Employees ) then
                   Result := Servidor.CorreosInvitacion( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.CorreosInvitacionLista( Empresa, Employees, Parametros.VarValues );
          end;
          prDECorreosNotificacion:
          begin
               Result := Servidor.CorreosNotificacion( Empresa, Parametros.VarValues );
          end;
          prDEAsignaRelaciones:
          begin
               if VarIsNull( Employees ) then
                   Result := Servidor.AsignaRelaciones( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.AsignaRelacionesLista( Empresa, Employees, Parametros.VarValues );
          end;
          prDECreaEncuestaSimple:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CreaEncuestaSimple( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.CreaEncuestaSimpleLista( Empresa, Employees, Parametros.VarValues )
          end;
          prDEAgregarEncuesta:
          begin
                  Result := Servidor.AgregarEncuestaParametros( Empresa, Parametros.VarValues )
          end;
     end;
end;


end.
