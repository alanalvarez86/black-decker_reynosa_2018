unit FEditPregxCriterio;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ComCtrls,
     ZBaseEdicion_DevEx,
     ZetaSmartLists,
     ZetaDBTextBox,
     ZetaKeyLookup,
     ZetaNumero,
     ZetaKeyCombo, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  cxControls, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList, cxNavigator,
  cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxGroupBox;

type
  TEditPreguntaCriterio = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    lblTitulo: TLabel;
    lblTituloCriterio: TLabel;
    zbTitulo: TZetaTextBox;
    GroupBox1: TcxGroupBox;
    EP_ORDENlbl: TLabel;
    EP_ORDEN: TZetaDBTextBox;
    EP_NUMEROlbl: TLabel;
    EP_NUMERO: TZetaDBTextBox;
    zbTituloCriterio: TZetaTextBox;
    EP_DESCRIPgb: TcxGroupBox;
    EP_DESCRIP: TDBMemo;
    EE_CODIGOlbl: TLabel;
    EE_CODIGO: TZetaDBKeyLookup_DevEx;
    EP_POS_PES: TZetaDBKeyLookup_DevEx;
    EP_POS_PESlbl: TLabel;
    EP_PESOlbl: TLabel;
    EP_PESO: TZetaDBNumero;
    EP_TIPO: TZetaDBKeyCombo;
    EP_TIPOlbl: TLabel;
    EP_GET_COMlbl: TLabel;
    EP_GET_COM: TZetaDBKeyCombo;
    EP_GET_NA: TZetaDBKeyCombo;
    EP_GET_NAlbl: TLabel;
    btnCreaEscala: TcxButton;
    EP_OBLIGA: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure DataSourceStateChange(Sender: TObject);
    procedure btnCreaEscalaClick(Sender: TObject);
    procedure EP_TIPOClick(Sender: TObject);
    procedure EP_OBLIGAClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FUpdating: Boolean;
    procedure SetControles;
    procedure SetControlesTipoRespuesta;
    procedure PuedeCrearEscalas;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  EditPreguntaCriterio: TEditPreguntaCriterio;

implementation

{$R *.DFM}

uses DEvaluacion,
     ZetaDialogo,
     ZHelpContext,
     ZAccesosTress,
     ZetaBuscador_DevEx,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZAccesosMgr,
     DCliente;

{ TEditPreguntaCriterio }

procedure TEditPreguntaCriterio.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_PREG_CRITERIO;
     FirstControl := EP_DESCRIP;
     EP_TIPO.ListaFija := lfEstiloRespuesta;
     EP_GET_COM.ListaFija := lfComentariosPedir;
     EP_GET_NA.ListaFIja := lfComentariosMGR;
     with dmEvaluacion do
     begin
          EE_CODIGO.LookupDataSet := cdsEscalasEnc;
          EP_POS_PES.LookupDataSet := cdsNivelesEnc;
     end;
     HelpContext := H_DISENO_ENCUESTA_PREGUNTAS_CRITERIO_AGREGAR_REGISTRO;
     {$ifdef BRAZEWAY}
     EP_DESCRIP.MaxLength := 1024;
     {$endif}
end;

procedure TEditPreguntaCriterio.FormShow(Sender: TObject);
begin
     inherited;
     ActiveControl := Cancelar_DevEx;
end;

procedure TEditPreguntaCriterio.Connect;
begin
     with dmEvaluacion do
     begin
          cdsEscalasEnc.Refrescar;
          cdsNivelesEnc.Refrescar;
          DataSource.DataSet := cdsPregCriterio;
     end;
     SetControles;
     SetControlesTipoRespuesta;
     FUpdating := False;
end;

procedure TEditPreguntaCriterio.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Preguntas por criterio', 'ENC_PREG', 'EP_ORDEN', dmEvaluacion.cdsPregCriterio );
end;

procedure TEditPreguntaCriterio.SetControles;
var
   lTienePeso: Boolean;
begin
     lTienePeso := StrLleno( dmCliente.PesoEsc );
     with dmEvaluacion do
     begin
          zbTitulo.Caption := IntToStr( dmCliente.Encuesta ) + ' = ' + dmCliente.Nombre;
          zbTituloCriterio.Caption := IntToStr( cdsCriteriosxPreg.FieldByName( 'EC_ORDEN' ).AsInteger) + ' = ' + cdsCriteriosxPreg.FieldByName( 'EC_NOMBRE' ).AsString;
          cdsNivelesEnc.Refrescar;
          if lTienePeso then
          begin
               EP_POS_PES.DataField := 'EP_POS_PES';
               EP_POS_PES.Filtro := ' ET_CODIGO = '+ IntToStr( dmCliente.Encuesta ) + ' and EE_CODIGO = ' + EntreComillas( dmCliente.PesoEsc )
          end
          else
          begin
               EP_POS_PES.DataField := VACIO;
               EP_POS_PES.Filtro := VACIO;
          end;
     end;
     EP_POS_PESlbl.Enabled := lTienePeso;
     EP_POS_PES.Enabled := lTienePeso;
     EP_PESOlbl.Enabled := not lTienePeso;
     EP_PESO.Enabled := not lTienePeso;
     PuedeCrearEscalas;
end;

procedure TEditPreguntaCriterio.SetControlesTipoRespuesta;
begin
     with EP_GET_COM do
     begin
          Visible := ( eEstiloRespuesta( Self.EP_TIPO.Valor ) <> erTextoLibre );
          EP_GET_COMlbl.Visible := Visible;
          EP_OBLIGA.Visible := not Visible;
     end;
end;

procedure TEditPreguntaCriterio.PuedeCrearEscalas;
begin
     btnCreaEscala.Enabled := CheckDerecho( D_ESCALAS, K_DERECHO_ALTA ) and
                              not ( dmEvaluacion.cdsPregCriterio.State in [ dsInsert, dsEdit ] );
end;

procedure TEditPreguntaCriterio.EscribirCambios;
begin
     if dmEvaluacion.ModificaDisenoEncuesta then
     begin
          FUpdating := True;
          try
             inherited EscribirCambios;
          finally
                 FUpdating := False;
          end;
     end;
end;

procedure TEditPreguntaCriterio.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if not FUpdating then
     begin
          EP_OBLIGA.Checked := ( eComentariosPedir( Self.EP_GET_COM.Valor ) = cpedObligatorio );
     end;
     SetControlesTipoRespuesta;
end;

procedure TEditPreguntaCriterio.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     PuedeCrearEscalas;
end;

procedure TEditPreguntaCriterio.btnCreaEscalaClick(Sender: TObject);
begin
     inherited;
     with dmEvaluacion.cdsEscalasEncuesta do
     begin
          Conectar;
          Agregar;
     end;
     dmEvaluacion.cdsEscalasEnc.Refrescar;
end;

procedure TEditPreguntaCriterio.EP_TIPOClick(Sender: TObject);
begin
     inherited;
     SetControlesTipoRespuesta;
end;

procedure TEditPreguntaCriterio.EP_OBLIGAClick(Sender: TObject);
begin
     inherited;
     FUpdating := True;
     try
        if EP_OBLIGA.Checked then
           EP_GET_COM.Valor := Ord( cpedObligatorio )
        else
           EP_GET_COM.Valor := Ord( cpedOpcional );
     finally
            FUpdating := False;
     end;
end;

end.
