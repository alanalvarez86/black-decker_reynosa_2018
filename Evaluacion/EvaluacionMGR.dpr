program EvaluacionMGR;

uses
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBaseSistema in '..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  dBaseTressDiccionario in '..\DataModules\dBaseTressDiccionario.pas',
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseGlobal in '..\Tools\ZBaseGlobal.pas',
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FSistBaseEditEmpresas in '..\Sistema\FSistBaseEditEmpresas.pas',
  ZBaseEdicion in '..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  ZBaseEditAccesos in '..\Tools\ZBaseEditAccesos.pas' {ZetaEditAccesos},
  DBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  ZBaseArbolShell in '..\Tools\ZBaseArbolShell.pas' {BaseArbolShell},
  DConsultas in '..\DataModules\DConsultas.pas' {dmConsultas: TDataModule},
  DBaseCliente in '..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  DEntidades in '..\Evaluador\DEntidades.pas' {dmEntidades: TDataModule},
  DCliente in 'DCliente.pas' {dmCliente: TDataModule},
  ZBaseEdicionRenglon in '..\Tools\ZBaseEdicionRenglon.pas' {BaseEdicionRenglon},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseGridEdicion in '..\Tools\ZBaseGridEdicion.pas' {BaseGridEdicion},
  ZBaseConsultaBotones in '..\Tools\ZBaseConsultaBotones.pas' {BaseBotones},
  ZWizardBasico in '..\Tools\ZWizardBasico.pas' {WizardBasico},
  FTressShell in 'FTressShell.pas' {TressShell},
  ZBaseWizard in '..\Tools\ZBaseWizard.pas' {BaseWizard},
  ZBaseWizardFiltro in '..\Tools\ZBaseWizardFiltro.pas' {BaseWizardFiltro},
  ZBasicoSelectGrid in '..\Tools\ZBasicoSelectGrid.pas' {BasicoGridSelect},
  ZBaseSelectGrid in '..\Tools\ZBaseSelectGrid.pas' {BaseGridSelect};

{$R *.RES}


procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Administrador Evaluación/Encuestas';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
            Free;
       end;
  end;
end.

