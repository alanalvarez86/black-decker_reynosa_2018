unit DGlobalComparte;

interface

uses Forms, Controls, DB, Classes, StdCtrls, SysUtils,
     {$ifdef DOS_CAPAS}
     DServerEvaluacion,
     {$else}
     Evaluacion_TLB,
     {$endif}
     ZGlobalComparte,
     ZetaCommonLists,
     DBaseGlobal;

type
  TdmGlobalComparte = class( TDataModule )
  private
    { Private declarations }
    {$ifdef DOS_CAPAS}
    function GetServer: TdmServerEvaluacion;
    {$else}
    function GetServer: IdmServerEvaluacionDisp;
    {$endif}
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property Server: TdmServerEvaluacion read GetServer;
    {$else}
    property Server: IdmServerEvaluacionDisp read GetServer;
    {$endif}
    function GetGlobales( Parametros: TZetaParams ): Variant; override;
    procedure GrabaGlobales( const aGlobalServer: Variant; const lActualizaDiccion: Boolean; var ErrorCount: Integer ); override;
  public
    { Public declarations }
    function ObtieneTipoGlobal( const Indice: Integer ): eTipoGlobal; override;
  end;

var
   GlobalComparte: TdmGlobalComparte;

implementation

uses DCliente,
     DDiccionario,
     ZetaCommonClasses,
     ZetaCommonTools;

{********* TdmGlobal ******** }

{$ifdef DOS_CAPAS}
function TdmGlobalComparte.GetServer: TdmServerEvaluacion;
{$else}
function TdmGlobalComparte.GetServer: IdmServerEvaluacionDisp;
{$endif}
begin
     Result := dmCliente.ServerEvaluacion;
end;

function TdmGlobalComparte.GetGlobales( Parametros: TZetaParams ): Variant;
begin
     Result := Server.GetGlobales;
end;

procedure TdmGlobalComparte.GrabaGlobales(const aGlobalServer: Variant; const lActualizaDiccion: Boolean; var ErrorCount: Integer);
begin
     Server.GrabaGlobales( aGlobalServer, ErrorCount );
end;

function TdmGlobalComparte.ObtieneTipoGlobal( const Indice: Integer ): eTipoGlobal;
begin
     Result := ZGlobalComparte.GetTipoGlobal( PosToGlobal( Indice ) );
end;

end.
