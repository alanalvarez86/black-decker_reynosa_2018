unit FCatPreguntas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Buttons, StdCtrls, DBCtrls, ComCtrls, Grids, DBGrids, Db, ExtCtrls,
     ZBaseGridLectura_DevEx, ZetaClientDataSet,
     ZetaDBGrid,
     ZetaSmartLists,
     ZetaKeyCombo,
     ZetaKeyLookup, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxTextEdit, cxClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, cxContainer, cxGroupBox, cxMemo, cxDBEdit;

type
  eSubeBaja = ( eSubir, eBajar );
  TCatPreguntas = class(TBaseGridLectura_DevEx)
    pnControles: TPanel;
    dsMaster: TDataSource;
    GroupBox1: TcxGroupBox;
    pnNombre: TPanel;
    Label1: TLabel;
    CM_DESCRIP: TZetaKeyCombo;
    pnDetalle: TPanel;
    CM_DETALLE: TcxDBMemo;
    pnLeftDescrip: TPanel;
    Label2: TLabel;
    Splitter1: TSplitter;
    pnFlechas: TPanel;
    zmlArriba: TcxButton;
    zmlAbajo: TcxButton;
    PG_ORDEN: TcxGridDBColumn;
    PG_DESCRIP: TcxGridDBColumn;
    PG_FOLIO: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure zmlArribaClick(Sender: TObject);
    procedure zmlAbajoClick(Sender: TObject);
    procedure CM_DESCRIPChange(Sender: TObject);
    procedure GridPregCellClick(Column: TColumn);

  private
    procedure SetControles;
    procedure LlenaCompetencias;
    procedure SubeBajaPregunta(eAccion: eSubeBaja);
    procedure RefrescaGrid(iPosicion: Integer;DataSet: TZetaClientDataset );
    procedure ObtenerPreguntasxCompetencia;
    procedure ConfigAgrupamiento;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatPreguntas: TCatPreguntas;

implementation

{$R *.DFM}

uses DEvaluacion,
     FBuscaTexto,
     ZHelpContext,
     ZetaDialogo,
     ZetaMsgDlg,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaCommonTools;

{ TCatPreguntas }

procedure TCatPreguntas.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_CATALOGOS_PREGUNTAS;
end;

procedure TCatPreguntas.FormShow(Sender: TObject);
const
K_INICIO_GRID = 1; // DChavez: variable que guarda el numero en el que queremos que inicie el grid
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('PG_ORDEN'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
     if( CM_DESCRIP.Descripcion = VACIO )then
     begin
           LlenaCompetencias;
           ObtenerPreguntasxCompetencia;
     end;
     SetControles;
     {if dmEvaluacion.cdsPreguntas.RecordCount > 1  then        DCHAVEZ: Si se requiere que al inicio aparezcan ordenados este codigo lo hace
     begin
          RefrescaGrid( K_INICIO_GRID , TZetaClientDataset(dmEvaluacion.cdsPreguntas) );
     end; }
end;


procedure TCatPreguntas.SetControles;
const
     K_PANEL_CON_TEXTO = 110;
     K_PANEL_SIN_TEXTO = 60;
begin
     with dmEvaluacion do
     begin
          zmlArriba.Enabled := ( cdsPreguntas.RecordCount > 0 );
          zmlAbajo.Enabled := zmlArriba.Enabled;
          if( strLleno( cdsCompetencia.FieldByName('CM_DETALLE').AsString ) )then
               pnControles.Height := K_PANEL_CON_TEXTO
          else
              pnControles.Height := K_PANEL_SIN_TEXTO;
     end;
end;

procedure TCatPreguntas.Agregar;
begin
     dmEvaluacion.cdsPreguntas.Agregar;
     DoBestFit;
end;

procedure TCatPreguntas.Borrar;
begin
     with dmEvaluacion do
     begin
          VieneConsultaPreg := True;
          cdsPreguntas.Borrar;
     end;
     ObtenerPreguntasxCompetencia;
     DoBestFit;
end;

procedure TCatPreguntas.Connect;
begin
     with dmEvaluacion do
     begin
          if not cdsPreguntas.Active or cdsPreguntas.HayQueRefrescar then
          begin
               if not( BorroPregunta )then
                  LlenaCompetencias;
               cdsPreguntas.Conectar;
               ObtenerPreguntasxCompetencia;
          end;
          dsMaster.DataSet := cdsCompetencia;
          DataSource.DataSet := cdsPreguntas;
     end;
     //ObtenerPreguntasxCompetencia;
end;

procedure TCatPreguntas.DoLookup;
begin
     inherited;
     FBuscaTexto.BuscarTexto( 'Texto', 'pregunta', 'PG_DESCRIP', dmEvaluacion.cdsPreguntas );
     //ZetaBuscador.BuscarCodigo( 'Texto', 'pregunta', 'PG_DESCRIP', dmEvaluacion.cdsPreguntas );
end;

procedure TCatPreguntas.ImprimirForma;
begin
  inherited;
end;

procedure TCatPreguntas.Modificar;
begin
     dmEvaluacion.cdsPreguntas.Modificar;
     DoBestFit;
end;

procedure TCatPreguntas.Refresh;
begin
     dmEvaluacion.cdsPreguntas.Refrescar;
     DoBestFit;
end;

procedure TCatPreguntas.zmlArribaClick(Sender: TObject);
begin
     inherited;
     SubeBajaPregunta( eSubir );
end;

procedure TCatPreguntas.zmlAbajoClick(Sender: TObject);
begin
     inherited;
     SubeBajaPregunta( eBajar );
end;

procedure TCatPreguntas.SubeBajaPregunta(eAccion: eSubeBaja);
const
     K_PRIMER_PREG = 1;
var
   iPosActual, iNuevaPos: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion do
        begin
             iPosActual := cdsPreguntas.FieldByName('PG_ORDEN').AsInteger;
             case( eAccion ) of
                   eSubir: begin
                                if( iPosActual = K_PRIMER_PREG )then
                                    ZetaDialogo.ZError( 'Competencias', '� Es El Inicio de la Lista de Preguntas !', 0 )
                                else
                                begin
                                     iNuevaPos := iPosActual - 1;
                                     CambiaOrdenPreguntas( CM_DESCRIP.Llave, iPosActual, iNuevaPos );
                                     RefrescaGrid( iNuevaPos, TZetaClientDataset(dmEvaluacion.cdsPreguntas) );
                                end;
                           end;
                   eBajar: begin
                                if( iPosActual = dmEvaluacion.cdsPreguntas.RecordCount  )then
                                    ZetaDialogo.ZError( 'Competencias', '� Es El Final de la Lista de Preguntas !', 0 )
                                else
                                begin
                                     iNuevaPos := iPosActual + 1;
                                     CambiaOrdenPreguntas( CM_DESCRIP.Llave, iPosActual, iNuevaPos );
                                     RefrescaGrid( iNuevaPos, TZetaClientDataset(dmEvaluacion.cdsPreguntas) );
                                end;
                           end;
             else
                 ZetaDialogo.ZError( 'Competencias', '� Operaci�n No V�lida !', 0 )
             end;
        end;
     finally
             Screen.Cursor := oCursor;
     end;
     Application.ProcessMessages;
end;


procedure TCatPreguntas.RefrescaGrid( iPosicion: Integer;DataSet: TZetaClientDataset );
var
   sField, sIndex, sFieldName: String;
   lDescendente: TIndexOptions;
begin
     with DataSet do
     begin
          lDescendente := [];
          AddIndex( sField, sFieldName, lDescendente);  //DChavez: se agrega un index vacio para que siempre ordene de manera descendente
          IndexDefs.Update; //Es necesario para que refleje que se agrego un Index.
          IndexName := sField;
     end;

     Refresh;
     dmEvaluacion.cdsPreguntas.Locate( 'PG_ORDEN', iPosicion, [] );
end;

procedure TCatPreguntas.LlenaCompetencias;
begin
      with CM_DESCRIP.Lista do
      begin
           BeginUpdate;
           try
              Clear;
              with dmEvaluacion.cdsCompetencia do
              begin
                   Conectar;
                   First;
                   //Add( 'X8G7=Todos' );
                   while not EOF do
                   begin
                        Add( FieldByName('CM_CODIGO').AsString + '=' + FieldByName('CM_DESCRIP').AsString );
                        Next;
                   end;
              end;
           finally
                  EndUpdate;
           end;
           CM_DESCRIP.ItemIndex := 0;
      end;
end;

procedure TCatPreguntas.CM_DESCRIPChange(Sender: TObject);
begin
     inherited;
     ObtenerPreguntasxCompetencia;
     DoBestFit;
end;

procedure TCatPreguntas.ObtenerPreguntasxCompetencia;
var
   sCodCompete: String;
   oCursor: TCursor;
begin
     sCodCompete := CM_DESCRIP.Llave;
     CM_DETALLE.Clear;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion do
        begin
             BorroPregunta := False;
             cdsCompetencia.Locate('CM_CODIGO', sCodCompete, [] );
             CM_DETALLE.Text := dmEvaluacion.cdsCompetencia.FieldbyName('CM_DETALLE').AsString;
             GetPreguntasPorCompetencia( sCodCompete );
           //  ZetaDBGrid.OrdenarPor( ZetaDBGridDBTableView.Columns[0], TZetaClientDataset(dmEvaluacion.cdsPreguntas),'PG_ORDEN' );
             SetControles;
        end;
     finally
         Screen.Cursor := oCursor;
     end;
end;

procedure TCatPreguntas.GridPregCellClick(Column: TColumn);
begin
     inherited;
     SetControles;
end;

procedure TCatPreguntas.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := FALSE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := FALSE; //Muestra la caja de agrupamiento
end;


end.
