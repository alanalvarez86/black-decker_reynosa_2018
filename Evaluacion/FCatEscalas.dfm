inherited CatEscalas: TCatEscalas
  Left = 205
  Top = 294
  Caption = 'Escalas'
  ClientHeight = 279
  ClientWidth = 496
  ExplicitWidth = 496
  ExplicitHeight = 279
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 496
    ExplicitWidth = 496
    inherited ValorActivo2: TPanel
      Width = 237
      ExplicitWidth = 237
      inherited textoValorActivo2: TLabel
        Width = 231
        ExplicitLeft = 151
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 496
    Height = 260
    ExplicitWidth = 496
    ExplicitHeight = 260
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      Styles.Content = cxStyle1
      Styles.Header = cxStyle2
      Styles.Indicator = cxStyle2
      object SC_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'SC_CODIGO'
        Styles.Content = cxStyle3
        Width = 90
      end
      object SC_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'SC_NOMBRE'
        Styles.Content = cxStyle4
        Width = 300
      end
      object SC_NIVELES: TcxGridDBColumn
        Caption = 'Niveles'
        DataBinding.FieldName = 'SC_NIVELES'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        Styles.Content = cxStyle5
        Width = 50
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 40
    Top = 80
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
  end
end
