unit FPerfilEvaluadores;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta,ZBaseGridLectura_DevEx,
  Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxTextEdit,
  cxClasses, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TPerfilEvaluadores = class(TBaseGridLectura_DevEx)
    ZetaDBGridDBTableView1: TcxGridDBTableView;
    ZetaDBGridDBTableView1ER_NOMBRE: TcxGridDBColumn;
    ZetaDBGridDBTableView1ER_NUM_MIN: TcxGridDBColumn;
    ZetaDBGridDBTableView1ER_NUM_MAX: TcxGridDBColumn;
    ZetaDBGridDBTableView1ER_PESO: TcxGridDBColumn;
    ZetaDBGridDBTableView1EL_NOMBRE: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    ER_NOMBRE: TcxGridDBColumn;
    ER_NUM_MIN: TcxGridDBColumn;
    ER_NUM_MAX: TcxGridDBColumn;
    ER_PESO: TcxGridDBColumn;
    EL_NOMBRE: TcxGridDBColumn;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    Procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  PerfilEvaluadores: TPerfilEvaluadores;

implementation

{$R *.DFM}
uses DEvaluacion,
     ZHelpContext,
     ZetaBuscador_DevEx,
     ZImprimeForma,
     ZetaCommonLists,
     ZetaTipoEntidad;

{ TPerfilEvaluadores }
procedure TPerfilEvaluadores.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     HelpContext := H_DISENO_ENCUESTA_PERFIL_EVALUADORES;
end;

procedure TPerfilEvaluadores.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('ER_NOMBRE'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TPerfilEvaluadores.Connect;
begin
     with dmEvaluacion do
     begin
          cdsPerfilEvaluador.Conectar;
          DataSource.DataSet := cdsPerfilEvaluador;
     end;
end;

procedure TPerfilEvaluadores.Agregar;
begin
     with dmEvaluacion do
     begin
          if( ModificaDisenoEncuesta )then
              cdsPerfilEvaluador.Agregar;
     end;
     DoBestFit;
end;

procedure TPerfilEvaluadores.Borrar;
begin
     with dmEvaluacion do
     begin
          if( ModificaDisenoEncuesta )then
              cdsPerfilEvaluador.Borrar;
     end;
     DoBestFit;
end;

procedure TPerfilEvaluadores.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( self.Caption, 'ENC_RELA', 'ER_TIPO', dmEvaluacion.cdsPerfilEvaluador );
end;

procedure TPerfilEvaluadores.ImprimirForma;
begin
     inherited;
     //ZImprimeForma.ImprimeUnaForma( enSujComp, dmEvaluacion.cdsEscalasEncuesta );
end;

procedure TPerfilEvaluadores.Modificar;
begin
     dmEvaluacion.cdsPerfilEvaluador.Modificar;
     DoBestFit;
end;

procedure TPerfilEvaluadores.Refresh;
begin
     dmEvaluacion.cdsPerfilEvaluador.Refrescar;
     DoBestFit;
end;

procedure TPerfilEvaluadores.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     ER_NOMBRE.Options.Grouping  := TRUE;
     ER_NUM_MIN.Options.Grouping:= FALSE;
     ER_NUM_MAX.Options.Grouping:= FALSE;
     ER_PESO.Options.Grouping:= FALSE;
     EL_NOMBRE.Options.Grouping:= TRUE;
end;


end.
