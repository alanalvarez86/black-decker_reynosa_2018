inherited AnalisisCriterio: TAnalisisCriterio
  Left = 441
  Top = 228
  Caption = 'An'#225'lisis por criterio'
  ClientHeight = 426
  ClientWidth = 678
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 237
    Width = 678
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  inherited PanelIdentifica: TPanel
    Width = 678
    inherited ValorActivo2: TPanel
      Width = 419
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 678
    Height = 51
    Align = alTop
    TabOrder = 1
    DesignSize = (
      678
      51)
    object Label1: TLabel
      Left = 4
      Top = 7
      Width = 90
      Height = 13
      Alignment = taRightJustify
      Caption = 'Mostrar Promedios:'
    end
    object Label2: TLabel
      Left = 565
      Top = 7
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Cantidad:'
      Visible = False
    end
    object ztbCuantos: TZetaTextBox
      Left = 613
      Top = 5
      Width = 60
      Height = 17
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      ShowAccelChar = False
      Visible = False
      Brush.Color = clBtnFace
      Border = True
    end
    object lblPrimeros: TLabel
      Left = 51
      Top = 31
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Primeros:'
    end
    object Label5: TLabel
      Left = 202
      Top = 7
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Relaci'#243'n:'
    end
    object edPrimeros: TZetaNumero
      Left = 95
      Top = 27
      Width = 95
      Height = 21
      Mascara = mnEmpleado
      TabOrder = 1
      UseEnterKey = True
    end
    object znPromedios: TComboBox
      Left = 95
      Top = 3
      Width = 95
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = znPromediosChange
      Items.Strings = (
        'Todos'
        'M'#225's altos'
        'M'#225's bajos')
    end
    object btnFiltrar: TBitBtn
      Left = 370
      Top = 3
      Width = 97
      Height = 45
      Caption = 'Filtrar'
      TabOrder = 3
      OnClick = btnFiltrarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777777777777770000070000077777000E00BFBFB07777777E0BFBF000777
        7700E0FBFBFBF0777700E0BFBF0000007777E0FBFBFBFBFB0799E0BF00000000
        7799000BFB077777777777700077777777007777777777777700777777777777
        7777777777777777700077777777777770007777777777777777}
    end
    object EV_RELACIO: TComboBox
      Left = 248
      Top = 3
      Width = 119
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
    end
  end
  object Grafica: TDBChart [3]
    Left = 0
    Top = 240
    Width = 678
    Height = 186
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      'An'#225'lisis por criterio')
    Align = alClient
    TabOrder = 2
    object Series1: THorizBarSeries
      Marks.Callout.Brush.Color = clBlack
      Marks.Style = smsValue
      Marks.Visible = False
      ShowInLegend = False
      Gradient.Direction = gdLeftRight
      XValues.Name = 'Bar'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 70
    Width = 678
    Height = 167
    Align = alTop
    TabOrder = 3
    object Grid1: TZetaDBGrid
      Left = 1
      Top = 1
      Width = 676
      Height = 146
      Align = alClient
      DataSource = DataSource
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnTitleClick = Grid1TitleClick
      Columns = <
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'EC_ORDEN'
          Title.Alignment = taRightJustify
          Title.Caption = '#'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EC_NOMBRE'
          Title.Caption = 'Nombre'
          Width = 300
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'CUANTAS'
          Title.Alignment = taRightJustify
          Title.Caption = 'Evaluaciones'
          Width = 70
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'MINIMO'
          Title.Alignment = taRightJustify
          Title.Caption = 'M'#237'nimo'
          Width = 60
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'PROMEDIO'
          Title.Alignment = taRightJustify
          Title.Caption = 'Promedio'
          Width = 60
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'MAXIMO'
          Title.Alignment = taRightJustify
          Title.Caption = 'M'#225'ximo'
          Width = 60
          Visible = True
        end>
    end
    object StatusBar1: TStatusBar
      Left = 1
      Top = 147
      Width = 676
      Height = 19
      Panels = <
        item
          Text = 'Cantidad:'
          Width = 50
        end>
    end
  end
  inherited DataSource: TDataSource
    Left = 528
    Top = 0
  end
end
