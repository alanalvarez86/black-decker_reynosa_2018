unit DEvaluacion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient, DbGrids, ComCtrls, Variants,
     {$ifdef DOS_CAPAS}
     DServerEvaluacion,
     {$else}
     Evaluacion_TLB,
     {$endif}
     ZetaEvaluacionTools,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaBusqueda_DevEx,
     ZetaServerDataSet;

const
     K_TODOS_CODIGO = 'XGZ37';
     K_TODAS_DESCRIPCION = 'Todas';

type
  TCriterioEval = class( TObject )
  private
     { Private declarations }
     FCodigo: String;
     FTipoCriterio: String;
     FDescripcion: String;
  public
     { Public declarations }
     property Codigo: String read FCodigo write FCodigo;
     property TipoCriterio: String read FTipoCriterio write FTipoCriterio;
     property Descripcion: String read FDescripcion write FDescripcion;
  end;
  TCriteriosEval = class( TStringList )
  private
     { Private declarations }
     function GetCriterio( Index: Integer ): TCriterioEval;
  public
     { Public declarations }
     constructor Create;
     destructor Destroy; override;
     property Criterio[ Index: Integer ]: TCriterioEval read GetCriterio;
     function AddCriterio( const sDescripcion: String ): TCriterioEval;
     procedure Clear; override;
  end;
  TTipoCompetencia = class( TObject )
  private
     { Private declarations }
     FCodigo: String;
     FDescripcion: String;
  public
     { Public declarations }
     property Codigo: String read FCodigo write FCodigo;
     property Descripcion: String read FDescripcion write FDescripcion;
  end;
  {
  TGlobalesEvaluacion = record
    ServerName: String;
    UserName: String;
    UserPassword: String;
    EmailPort: Integer;
    EmailTimeOut: Integer;
    RutaVirtual: String;
  end;
  }
  TFiltroRelaciones = set of eTipoEvaluaEnc;
  TdmEvaluacion = class(TDataModule)
    cdsCompetencia: TZetaLookupDataSet;
    cdsTCompetencia: TZetaLookupDataSet;
    cdsPreguntas: TZetaLookupDataSet;
    cdsEscalas: TZetaLookupDataSet;
    cdsCriteriosEval: TZetaClientDataSet;
    cdsEditEscala: TZetaClientDataSet;
    cdsPregCriterio: TZetaClientDataSet;
    cdsCriteriosxPreg: TZetaClientDataSet;
    cdsEscalasEncuesta: TZetaClientDataSet;
    cdsPerfilEvaluador: TZetaClientDataSet;
    cdsEmpEvaluar: TZetaClientDataSet;
    cdsEncuestas: TZetaLookupDataSet;
    cdsCriterios: TZetaLookupDataSet;
    cdsAgregaCriterios: TServerDataSet;
    cdsAgregaEvaluador: TServerDataSet;
    cdsNivelesEnc: TZetaLookupDataSet;
    cdsEscalaNivEnc: TZetaClientDataSet;
    cdsEscalasEnc: TZetaLookupDataSet;
    cdsEvalua: TZetaClientDataSet;
    cdsTotalesEncuesta: TZetaClientDataSet;
    cdsSujeto: TZetaClientDataSet;
    cdsEvaluadores: TZetaClientDataSet;
    cdsUsuarios: TZetaLookupDataSet;
    cdsEvaluaciones: TZetaClientDataSet;
    cdsAnalisisCriterio: TZetaClientDataSet;
    cdsAnalisisPregunta: TZetaClientDataSet;
    cdsCriteriosEnc: TZetaLookupDataSet;
    cdsFrecuenciaResp: TZetaClientDataSet;
    cdsPreguntasEnc: TZetaLookupDataSet;
    cdsResultadoCriterio: TZetaClientDataSet;
    cdsResultadoPregunta: TZetaClientDataSet;
    cdsAgregaEmpEvaluar: TZetaClientDataSet;
    cdsNivelesEscala: TZetaLookupDataSet;
    cdsTotalesEvaluacion: TZetaClientDataSet;
    cdsTotalesEvaluador: TZetaClientDataSet;
    cdsTotalesEvaluado: TZetaClientDataSet;
    cdsDatosxEvaluado: TZetaClientDataSet;
    cdsEvaluadosLookup: TZetaLookupDataSet;
    cdsEvaluadoresLookup: TZetaLookupDataSet;
    cdsCorreos: TZetaClientDataSet;
    cdsUsuariosRol: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsCompetenciaAlAdquirirDatos(Sender: TObject);
    procedure cdsCompetenciaAlCrearCampos(Sender: TObject);
    procedure cdsTCompetenciaAlAdquirirDatos(Sender: TObject);
    procedure cdsTCompetenciaAlCrearCampos(Sender: TObject);
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure TC_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure FijaStatusEvaluadorGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure EL_NOMBREOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure No_CerosGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure EV_RELACIOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure MemoGetText( Sender: TField; var Text: String; DisplayText: Boolean);
    procedure US_JEFEGetText( Sender: TField; var Text: String; DisplayText: Boolean );
    procedure cdsEscalasAlAdquirirDatos(Sender: TObject);
    procedure cdsPreguntasAlAdquirirDatos(Sender: TObject);
    procedure cdsCompetenciaAfterDelete(DataSet: TDataSet);
    procedure cdsPreguntasAlBorrar(Sender: TObject);
    procedure cdsTCompetenciaAfterDelete(DataSet: TDataSet);
    procedure cdsEscalasAfterDelete(DataSet: TDataSet);
    procedure cdsCompetenciaAlEnviarDatos(Sender: TObject);
    procedure cdsCompetenciaAlModificar(Sender: TObject);
    procedure cdsCompetenciaBeforePost(DataSet: TDataSet);
    procedure cdsPreguntasAlEnviarDatos(Sender: TObject);
    procedure cdsPreguntasAlModificar(Sender: TObject);
    procedure cdsPreguntasBeforePost(DataSet: TDataSet);
    procedure cdsPreguntasNewRecord(DataSet: TDataSet);
    procedure cdsTCompetenciaAlModificar(Sender: TObject);
    procedure cdsTCompetenciaAlEnviarDatos(Sender: TObject);
    procedure cdsTCompetenciaBeforePost(DataSet: TDataSet);
    procedure cdsPreguntasAfterDelete(DataSet: TDataSet);
    procedure cdsEscalasAlModificar(Sender: TObject);
    procedure cdsEditEscalaAlAdquirirDatos(Sender: TObject);
    procedure cdsEditEscalaAlAgregar(Sender: TObject);
    procedure cdsEditEscalaAlEnviarDatos(Sender: TObject);
    procedure cdsEditEscalaBeforeDelete(DataSet: TDataSet);
    procedure cdsEditEscalaBeforeInsert(DataSet: TDataSet);
    procedure cdsEditEscalaBeforePost(DataSet: TDataSet);
    procedure cdsEscalasAlAgregar(Sender: TObject);
    procedure cdsCriteriosEvalAlAdquirirDatos(Sender: TObject);
    procedure cdsCriteriosEvalAfterDelete(DataSet: TDataSet);
    procedure cdsCriteriosEvalAlCrearCampos(Sender: TObject);
    procedure cdsCriteriosEvalAlBorrar(Sender: TObject);
    procedure cdsCriteriosEvalAlEnviarDatos(Sender: TObject);
    procedure cdsCriteriosEvalAlModificar(Sender: TObject);
    procedure cdsCriteriosEvalNewRecord(DataSet: TDataSet);
    procedure cdsCriteriosEvalBeforePost(DataSet: TDataSet);
    procedure cdsCriteriosxPregAlAdquirirDatos(Sender: TObject);
    procedure cdsPregCriterioAlCrearCampos(Sender: TObject);
    procedure cdsPregCriterioAlBorrar(Sender: TObject);
    procedure cdsPregCriterioAlAdquirirDatos(Sender: TObject);
    procedure cdsPregCriterioAlModificar(Sender: TObject);
    procedure cdsPregCriterioAlEnviarDatos(Sender: TObject);
    procedure cdsPregCriterioBeforePost(DataSet: TDataSet);
    procedure cdsPregCriterioNewRecord(DataSet: TDataSet);
    procedure cdsEscalasEncuestaAlAdquirirDatos(Sender: TObject);
    procedure cdsPerfilEvaluadorAlAdquirirDatos(Sender: TObject);
    procedure cdsPerfilEvaluadorAlCrearCampos(Sender: TObject);
    procedure cdsEncuestasAlAdquirirDatos(Sender: TObject);
    procedure cdsEncuestasAlCrearCampos(Sender: TObject);
    procedure cdsEncuestasAlAgregar(Sender: TObject);
    procedure cdsEncuestasAlEnviarDatos(Sender: TObject);
    procedure cdsEncuestasAlModificar(Sender: TObject);
    procedure cdsEncuestasAfterDelete(DataSet: TDataSet);
    procedure cdsEncuestasBeforePost(DataSet: TDataSet);
    procedure cdsEncuestasNewRecord(DataSet: TDataSet);
    procedure cdsCriteriosAlAdquirirDatos(Sender: TObject);
    procedure cdsNivelesEncAlAdquirirDatos(Sender: TObject);
    procedure cdsEscalasEncuestaAlModificar(Sender: TObject);
    procedure cdsEscalaNivEncAfterInsert(DataSet: TDataSet);
    procedure cdsEscalaNivEncAlCrearCampos(Sender: TObject);
    procedure cdsEscalaNivEncNewRecord(DataSet: TDataSet);
    procedure cdsEscalasEncuestaBeforePost(DataSet: TDataSet);
    procedure cdsEscalasEncuestaAlAgregar(Sender: TObject);
    procedure cdsEscalasEncuestaAlEnviarDatos(Sender: TObject);
    procedure cdsEscalasEncuestaBeforeInsert(DataSet: TDataSet);
    procedure cdsEscalasEncuestaNewRecord(DataSet: TDataSet);
    procedure cdsEscalasEncuestaAfterDelete(DataSet: TDataSet);
    procedure cdsPerfilEvaluadorAlModificar(Sender: TObject);
    procedure cdsPerfilEvaluadorAfterDelete(DataSet: TDataSet);
    procedure cdsPerfilEvaluadorAlEnviarDatos(Sender: TObject);
    procedure cdsPerfilEvaluadorBeforePost(DataSet: TDataSet);
    procedure cdsPerfilEvaluadorNewRecord(DataSet: TDataSet);
    procedure cdsEscalasEncAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpEvaluarAlModificar(Sender: TObject);
    procedure cdsEmpEvaluarAlAgregar(Sender: TObject);
    procedure cdsEmpEvaluarAlCrearCampos(Sender: TObject);
    procedure cdsEvaluaAlAdquirirDatos(Sender: TObject);
    procedure cdsTotalesEncuestaAlAdquirirDatos(Sender: TObject);
    procedure cdsTotalesEncuestaAlCrearCampos(Sender: TObject);
    procedure cdsSujetoAlAdquirirDatos(Sender: TObject);
    procedure cdsSujetoAlCrearCampos(Sender: TObject);
    procedure cdsUsuariosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsEvaluadoresAlCrearCampos(Sender: TObject);
    procedure cdsEvaluacionesAlCrearCampos(Sender: TObject);
    procedure cdsAnalisisPreguntaAlAdquirirDatos(Sender: TObject);
    procedure cdsCriteriosEncAlAdquirirDatos(Sender: TObject);
    procedure cdsCriteriosEncGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsPreguntasEncAlAdquirirDatos(Sender: TObject);
    procedure cdsPreguntasEncGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsFrecuenciaRespAlCrearCampos(Sender: TObject);
    procedure cdsResultadoCriterioAlAdquirirDatos(Sender: TObject);
    procedure cdsResultadoPreguntaAlAdquirirDatos(Sender: TObject);
    procedure cdsNivelesEncGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsEscalasEncGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsAgregaEmpEvaluarAlAdquirirDatos(Sender: TObject);
    procedure cdsAgregaEmpEvaluarAlCrearCampos(Sender: TObject);
    procedure cdsAgregaEmpEvaluarAlEnviarDatos(Sender: TObject);
    procedure cdsAgregaEmpEvaluarReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsEscalasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsNivelesEscalaAfterInsert(DataSet: TDataSet);
    procedure cdsNivelesEscalaAlCrearCampos(Sender: TObject);
    procedure cdsNivelesEscalaNewRecord(DataSet: TDataSet);
    procedure cdsNivelesEscalaGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsEscalasBeforePost(DataSet: TDataSet);
    procedure cdsNivelesEscalaBeforeInsert(DataSet: TDataSet);
    procedure cdsEscalaNivEncBeforeInsert(DataSet: TDataSet);
    procedure cdsPerfilEvaluadorAlBorrar(Sender: TObject);
    procedure cdsEvaluadosLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsEvaluadosLookupAlAdquirirDatos(Sender: TObject);
    procedure cdsEvaluadoresLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsEvaluadoresLookupAlCrearCampos(Sender: TObject);
    procedure cdsEvaluadoresLookupAlAdquirirDatos(Sender: TObject);
    procedure cdsCorreosAlCrearCampos(Sender: TObject);
    procedure cdsCorreosAlModificar(Sender: TObject);
    procedure cdsCorreosCalcFields(DataSet: TDataSet);
    procedure cdsUsuariosRolAfterCancel(DataSet: TDataSet);
    procedure cdsUsuariosRolAfterScroll(DataSet: TDataSet);
    procedure cdsUsuariosRolAlAdquirirDatos(Sender: TObject);
    procedure cdsUsuariosRolAlEnviarDatos(Sender: TObject);
    procedure cdsUsuariosRolAlModificar(Sender: TObject);
    procedure cdsUsuariosRolAlCrearCampos(Sender: TObject);
    procedure cdsEvaluadoresLookupLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
  private
    { Private declarations }
    iOrden: Integer;
    FOrden: Integer;
    //MA: Variables para enterar a las formas que depende de EditempEvaluar cuales son los filtros
    FEmpleadoEvaluar: Integer;
    FStatusEmpEvaluar: Integer;
    FVieneConsultaPreg: Boolean;
    FBorroPregunta: Boolean;
    FArbolEvaluadores: TTreeView;
    FTotalesEvaluaciones: TZetaParams;
    FTotalesEvaluadores: TZetaParams;
    FTotalesEvaluados: TZetaParams;
    FTotalesxEvaluado: TZetaParams;
    {$ifdef DOS_CAPAS}
    function GetServerEvaluacion: TdmServerEvaluacion;
    property ServerEvaluacion: TdmServerEvaluacion read GetServerEvaluacion;
    {$else}
    FServidor: IdmServerEvaluacionDisp;
    function GetServerEvaluacion: IdmServerEvaluacionDisp;
    property ServerEvaluacion: IdmServerEvaluacionDisp read GetServerEvaluacion;
    {$endif}
    function BorraUnEmpleadoPorEvaluar( const iEmpleado: TNumEmp; const lPrompt: Boolean = True ): Boolean;
    function CreaNodo( const sNombre: String; const iImage: Integer ): TTreeNode;
    function CambiaStatusEnDiseno: Boolean;
    function CambiaStatusAbierta: Boolean;
    function CambiaStatusCerrada: Boolean;
    function AnalizaEncuesta: Integer;
    procedure GetRolesUsuario(const iUsuario: Integer);
    procedure BorrarPreguntas(CM_CODIGO: String; iPosicion: Integer);
    procedure cdsBeforePost( DataSet: TDataSet; const sCampo: String );
    procedure SN_ORDENOnChange(Sender: TField);
    procedure EL_ORDENOnChange(Sender: TField);
    procedure EL_VALOROnChange(Sender: TField);
    procedure SN_VALOROnChange(Sender: TField);
    procedure EC_POS_PESOnChange(Sender: TField);
    procedure EP_POS_PESOnChange(Sender: TField);
    procedure ER_POS_PESOnChange(Sender: TField);
    procedure ER_TIPOOnChange(Sender: TField);
    procedure CB_CODIGOAgregaEmpEvaluarValidate( Sender: TField );
    procedure CB_CODIGOAgregaEmpEvaluarChange( Sender: TField );
    procedure GetEscalasNiveles( const sEscala: string );
    procedure GetEscalasNivelesEnc( const sEscala: string );
    procedure ObtenerCriteriosxEncuesta( oClientDataSet: TZetaClientDataSet );
    procedure InicializaEncuesta( const iEncuesta: Integer );
    procedure ObtieneTotalesEncuesta( const iEncuesta: Integer );
    procedure ObtieneTotalesPorEvaluado( const iEmpleado: Integer );
    procedure CargaValoresTotalesEvaluacion( const oData: OleVariant );
    procedure CargaValoresTotalesEvaluador( const oData: OleVariant );
    procedure CargaValoresTotalesEvaluado( const oData: OleVariant );
  public
    { Public declarations }
    property VieneConsultaPreg: Boolean read FVieneConsultaPreg write FVieneConsultaPreg;
    property BorroPregunta: Boolean read FBorroPregunta write FBorroPregunta;
    property Orden: Integer read FOrden write FOrden;
    property ArbolEvaluadores: TTreeView read FArbolEvaluadores write FArbolEvaluadores;
    property EmpleadoEvaluar: Integer read FEmpleadoEvaluar write FEmpleadoEvaluar;
    property StatusEmpEvaluar: Integer read FStatusEmpEvaluar write FStatusEmpEvaluar;
    property TotalesEvaluaciones: TZetaParams read FTotalesEvaluaciones write FTotalesEvaluaciones;
    property TotalesEvaluadores: TZetaParams read FTotalesEvaluadores write FTotalesEvaluadores;
    property TotalesEvaluados: TZetaParams read FTotalesEvaluados write FTotalesEvaluados;
    property TotalesxEvaluado: TZetaParams read FTotalesxEvaluado write FTotalesxEvaluado;
    function CheckAuto: Boolean;
    function GetUsuarioActual: Integer;
    function ObtieneSNOrdenMaximo( const oDataSet: TZetaClientDataSet; oCampo: TField ): Integer;
    function ObtieneEvaluador( var sEvaluador: String; var iRelacion: Integer ): Boolean;
    function AgregaSujetoArbol( const iSujeto: Integer; const iRelacion: Integer; const iEmpleado: Integer; const lInserta: Boolean ): Boolean;
    function ObtenerPreguntasContestadas( const iUsuario, iEmpleado: Integer): Integer;
    function ValidaCambioStatusEncuesta( const eStEncuesta: eStatusEncuesta ): Boolean;
    function ModificaDisenoEncuesta( const lUtilizaValorActivo: Boolean = True; const oStatus: eStatusEncuesta = stenDiseno ): Boolean;
    function ExportaEncuesta( const iEncuesta: Integer ): WideString;
    function ImportaEncuesta( const sArchivo: String ): Boolean;
    function ParametrosRutaVirtual( const lEncuestaActiva: Boolean; var sRutaVirtual: WideString ): String;
    procedure CambiarDocumentacion( Dataset: TDataset; const sFieldName, sDocumento: String );
    procedure BorrarEmpleadosPorEvaluar( Lista: TStringList );
    procedure BorrarEmpleadoPorEvaluar( const iEmpleado: TNumEmp );
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
    procedure GetPreguntasPorCompetencia( sCodCompetencia: String );
    procedure CambiaOrdenPreguntas( CM_CODIGO: String; iPosActual: Integer; iPosNueva: Integer );
    procedure AgregaNivelesxEscala;
    procedure AgregaNivelesEncuesta;
    procedure CambiaOrdenCriterios( iPosActual: Integer; iPosNueva: Integer );
    procedure GetPreguntasPorCriterio( const iCriterio: Integer );
    procedure CambiaOrdenPreguntasCriterio( iCriterio: Integer; iPosActual: Integer; iPosNueva: Integer );
    procedure ObtieneSujetosEvaluar( const iEmpleado: Integer; const iStatus: Integer; const oDataSet: TZetaClientDataSet  );
    procedure LlenaCriterios( Lista: TCriteriosEval );
    procedure LlenaTiposCompetencia( Lista : TStrings; const lPonerTodas: Boolean = FALSE; const lEsCombo: Boolean = FALSE );
    procedure LlenaEvaluadores( Lista: TStrings );
    procedure CopiaEncuesta( const sEncuesta: String; const lCopiaEmp, lCopiaEvaluador: Boolean );
    procedure CreaServerAgregaCriterios;
    procedure CreaServerAgregaEvaluador;
    procedure AgregaCompetencias;
    procedure LlenaDataSetCriterios( const Lista: TStrings );
    procedure LlenaDataSetEvaluadores( const Lista: TStrings );
    procedure GrabaEncuesta( const lGrabaCriterio: Boolean );
    procedure CreaArbolEmpEvaluar;
    procedure BorraEvaluadoArbol( const oNodoSelec: TTreeNode; const iEmpleado: Integer );
    procedure LlenaPerfilEvaluadores( const oLista: TStrings; const lPonerTodas: Boolean = FALSE; const lEsCombo: Boolean = TRUE ); overload;
    procedure LlenaPerfilEvaluadores( const oLista: TStrings; aFiltro: TFiltroRelaciones; const lPonerTodas: Boolean = FALSE; const lEsCombo: Boolean = TRUE ); overload;
    procedure GrabaSujetoRelacion( const iSujeto: Integer; const iRelacion: Integer; const iEmpleado: Integer; const lInserta: Boolean );
    procedure ObtenerAnalisisCriterio( const iMostrar, iRelacion, iOrden: Integer );
    procedure ObtenerAnalisisPreguntas( const iMostrar, iRelacion, iCriterio, iOrden: Integer );
    procedure ObtenerFrecuenciaResp( const iPregunta, iRelacion: Integer );
    procedure ObtenerResultadosCriterio( const iMostrar, iOrden: Integer );
    procedure ObtenerResultadoPregunta( const iMostrar, iOrden: Integer );
    procedure ObtieneEvaluadores( const sFiltros: String );
    procedure ObtieneEvaluaciones( const sFiltro: String );
    procedure ObtieneEmpleadosEvaluados( const iEmpleado, iStatus: Integer );
    procedure ObtieneEscalasEncuesta( const iEncuesta: Integer );
    procedure RegistraEmpleadosEvaluar;
    procedure CambiaStatusSujeto( const iEmpleado:Integer; const lListoEvaluar: Boolean );
    procedure CopiaEscalasEncuesta;
    procedure GetListaCorreos( Parametros: TZetaParams );
    procedure GlobalesCargar( Parametros: TZetaParams );
    procedure GlobalesDescargar( Parametros: TZetaParams );
  end;

var
  dmEvaluacion: TdmEvaluacion;

const
     K_UNO = 1;
     K_ESPACIO = ' ';
     K_PROMPT_CAMBIO_STATUS = 'Cambio de status';

     {
     Array de Globales en comparte para correos}
     {
     EMAIL_SERVER   = 0;
     EMAIL_USER     = 1;
     EMAIL_PASSWORD = 2;
     EMAIL_PORT     = 3;
     EMAIL_TIMEOUT  = 4;
     RUTA_VIRTUAL = 5;
     }

implementation

uses DCliente,
     DSistema,
     ZetaDialogo,
     ZetaMsgDlg,
     ZReconcile,
     ZBaseEdicion_DevEx,
     ZcxWizardBasico,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaBusqueda,
     ZBaseGridEdicion_DevEx,
     FEditaCompetencias,
     FEditaPreguntas,
     FEditTCompetencias,
     FEditaEscalas,
     FEditCriterioEvaluar,
     FEditPregxCriterio,
     FEditEscalaEncuesta,
     FEditPerfilEvaluador,
     FEditEmpEvaluar_DevEx,
     FEditEncuesta,
     FAutoClasses,
     FTressShell,
     FGridEmpEvaluar,
     FWizAgregarEncuesta_DevEx,
     FHistCorreosEdit,
     FCatUsuariosEdit;

{$R *.DFM}

{ ************ TCriteriosEval ************* }

constructor TCriteriosEval.Create;
begin
     inherited Create;
end;

destructor TCriteriosEval.Destroy;
begin
     Clear;
     inherited Destroy;
end;

procedure TCriteriosEval.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Criterio[ i ].Free;
     end;
     inherited Clear;
end;

function TCriteriosEval.AddCriterio( const sDescripcion: String ): TCriterioEval;
begin
     Result := TCriterioEval.Create;
     Result.Descripcion := sDescripcion;
     AddObject( sDescripcion, Result );
end;

function TCriteriosEval.GetCriterio( Index: Integer ): TCriterioEval;
begin
     Result := TCriterioEval( Objects[ Index ] );
end;

{ ************ TdmEvaluacion ************** }

{$ifdef DOS_CAPAS}
function TdmEvaluacion.GetServerEvaluacion: TdmServerEvaluacion;
begin
     Result := DCliente.dmCliente.ServerEvaluacion;
end;
{$else}
function TdmEvaluacion.GetServerEvaluacion: IdmServerEvaluacionDisp;
begin
     Result := IdmServerEvaluacionDisp( dmCliente.CreaServidor( CLASS_dmServerEvaluacion, FServidor ) );
end;
{$endif}

function TdmEvaluacion.CheckAuto: Boolean;
begin
     Result := True;
     //Result := Autorizacion.OkModulo( OkEvaluacion, True );
     if not Result then
         ZInformation( 'Versi�n Demo', 'Operaci�n No Permitida En Versi�n Demo' , 0);
end;

procedure TdmEvaluacion.DataModuleCreate(Sender: TObject);
begin
     FTotalesEvaluaciones := TZetaParams.Create;
     FTotalesEvaluadores := TZetaParams.Create;
     FTotalesEvaluados := TZetaParams.Create;
     FTotalesxEvaluado := TZetaParams.Create;
     dmSistema.cdsUsuarios.OnGetRights := cdsUsuariosGetRights;
end;

procedure TdmEvaluacion.NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
begin
     if Dentro( enEncuesta, Entidades ) OR ( Estado = stEvaluacion ) then
     begin
          cdsEncuestas.SetDataChange;
          cdsCriteriosEval.SetDataChange;
          cdsCriteriosxPreg.SetDataChange;
          cdsPregCriterio.SetDataChange;
          cdsEscalasEncuesta.SetDataChange;
          cdsPerfilEvaluador.SetDataChange;
          cdsEmpEvaluar.SetDataChange;
          cdsEvalua.SetDataChange;
          cdsTotalesEncuesta.SetDatachange;
          cdsSujeto.SetDataChange;
          cdsEvaluaciones.SetDataChange;
          cdsEvaluadores.SetDataChange;
          cdsAnalisisCriterio.SetDataChange;
          cdsAnalisisPregunta.SetDataChange;
          cdsFrecuenciaResp.SetDatachange;
          cdsResultadoCriterio.SetDataChange;
          cdsResultadoPregunta.SetDataChange;
          cdsPreguntasEnc.SetDataChange;
     end;
     if Dentro( enEscalas, Entidades ) then
     begin
          cdsEscalas.SetDataChange;
     end;
     if Dentro( enEncEsca, Entidades ) then
     begin
          cdsEscalasEncuesta.SetDataChange;
     end;
     if Dentro( enPreguntas, Entidades ) then
     begin
          cdsPreguntas.SetDataChange;
     end;
     if Dentro( enTCompetencia, Entidades ) then
         cdsTCompetencia.SetDataChange;
     if Dentro( enCompetencias, Entidades ) then
         cdsCompetencia.SetDataChange;
     if Dentro( enSujeto, Entidades ) then
         cdsEmpEvaluar.SetDataChange;
     if Dentro( enEncComp, Entidades ) then
         cdsCriteriosEval.SetDataChange;
     if Dentro( enEncPreg, Entidades ) then
         cdsPregCriterio.SetDataChange;
     if Dentro( enEncRela, Entidades ) then
         cdsPerfilEvaluador.SetDataChange;
     if Dentro( enSujComp, Entidades ) then
         cdsResultadoCriterio.SetDataChange;
     if Dentro( enSujPreg, Entidades ) then
         cdsResultadoPregunta.SetDataChange;
     if Dentro( enVEvaComp, Entidades ) then
         cdsAnalisisCriterio.SetDataChange;
     if Dentro( enVEvaPreg, Entidades ) then
         cdsAnalisisPregunta.SetDataChange;
end;


procedure TdmEvaluacion.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FTotalesxEvaluado );
     FreeAndNil( FTotalesEvaluados );
     FreeAndNil( FTotalesEvaluadores );
     FreeAndNil( FTotalesEvaluaciones );
end;

{ TdmEvaluacion }
procedure TdmEvaluacion.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;

procedure TdmEvaluacion.cdsBeforePost( DataSet: TDataSet; const sCampo: String );
begin
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( sCampo ).AsString ) then
             DataBaseError( 'El c�digo no puede quedar vac�o' );
     end;
end;

{ cdsCompetencia }

procedure TdmEvaluacion.cdsCompetenciaAlAdquirirDatos(Sender: TObject);
begin
     cdsTCompetencia.Conectar;
     cdsCompetencia.Data := ServerEvaluacion.GetCatHabilidades( dmCliente.Empresa );
end;

procedure TdmEvaluacion.cdsCompetenciaAlCrearCampos(Sender: TObject);
begin
     cdsTCompetencia.Conectar;
     cdsCompetencia.CreateSimpleLookup( cdsTCompetencia, 'TC_DESCRIP', 'TC_CODIGO' );
end;

procedure TdmEvaluacion.cdsCompetenciaAfterDelete(DataSet: TDataSet);
begin
     cdsCompetencia.Enviar;
end;

procedure TdmEvaluacion.cdsCompetenciaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsCompetencia do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerEvaluacion.GrabaCompetencia( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enCompetencias, enPreguntas ] );
               end;
          end;
     end;
end;

procedure TdmEvaluacion.cdsCompetenciaAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCompetencias, TEditCompetencias );
end;

procedure TdmEvaluacion.cdsCompetenciaBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( cdsCompetencia, 'CM_CODIGO' );
     with cdsCompetencia do
     begin
          if StrVacio( FieldByName( 'CM_DESCRIP' ).AsString ) then
             DataBaseError( 'El nombre de la competencia no puede quedar vac�o' );
          if StrVacio( FieldByName( 'CM_DETALLE' ).AsString ) then
              FieldByName( 'CM_DETALLE' ).AsString := K_ESPACIO;
     end;
end;

{ cdsTCompetencia }

procedure TdmEvaluacion.cdsTCompetenciaAlAdquirirDatos(Sender: TObject);
begin
     cdsTCompetencia.Data := ServerEvaluacion.GetCatCompetencias( dmCliente.Empresa );
end;

procedure TdmEvaluacion.cdsTCompetenciaAlCrearCampos(Sender: TObject);
begin
     cdsTCompetencia.FieldByName( 'TC_TIPO' ).OnGetText := TC_TIPOGetText;
end;

procedure TdmEvaluacion.cdsTCompetenciaAfterDelete(DataSet: TDataSet);
begin
     cdsTCompetencia.Enviar;
end;

procedure TdmEvaluacion.TC_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
        Text:= ObtieneElemento( lfTipoCompete, ( Sender.AsInteger ) )
     else
         Text:= Sender.AsString;
end;

procedure TdmEvaluacion.cdsTCompetenciaAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditTCompetencias, TEditTCompetencias );
end;

procedure TdmEvaluacion.cdsTCompetenciaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsTCompetencia do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if( Reconcile( ServerEvaluacion.GrabaTCompetencia( dmCliente.Empresa, Delta, ErrorCount ) ) )then
                   TressShell.SetDataChange( [ enTCompetencia ] );
          end;
     end;
end;

procedure TdmEvaluacion.cdsTCompetenciaBeforePost(DataSet: TDataSet);
begin
      cdsBeforePost( cdsTCompetencia, 'TC_CODIGO' );
end;

{cdsPreguntas}
procedure TdmEvaluacion.GetPreguntasPorCompetencia( sCodCompetencia: String );
begin
     cdsPreguntas.Data := ServerEvaluacion.GetCatPreguntas( dmCliente.Empresa, sCodCompetencia );
end;

procedure TdmEvaluacion.CambiaOrdenPreguntas( CM_CODIGO: String; iPosActual: Integer; iPosNueva: Integer );
begin
     ServerEvaluacion.CambiaPregunta( dmCliente.Empresa, CM_CODIGO, iPosActual, iPosNueva );
end;

procedure TdmEvaluacion.cdsPreguntasAlAdquirirDatos(Sender: TObject);
begin
     with cdsCompetencia do
     begin
          if Active or not( IsEmpty ) then
          begin
               GetPreguntasPorCompetencia( FieldByName( 'CM_CODIGO' ).AsString );
          end;
     end;
end;

procedure TdmEvaluacion.BorrarPreguntas( CM_CODIGO: String; iPosicion: Integer );
begin
     ServerEvaluacion.BorraPregunta( dmCliente.Empresa, CM_CODIGO, iPosicion );
     FBorroPregunta := True;
end;

procedure TdmEvaluacion.cdsPreguntasAlBorrar(Sender: TObject);
begin
     if( ZetaMsgDlg.ConfirmaCambio( '� Desea borrar este registro ?' ) )then
     begin
          BorrarPreguntas( cdsCompetencia.FieldByName('CM_CODIGO').AsString, cdsPreguntas.FieldByName('PG_ORDEN').AsInteger );
          if ( FVieneConsultaPreg )then
             FVieneConsultaPreg := False
          else
              TressShell.SetDataChange( [ enPreguntas ] );
     end;
end;

procedure TdmEvaluacion.cdsPreguntasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPreguntas do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerEvaluacion.GrabaPregunta( dmCliente.Empresa, Delta, ErrorCount ) );
                    //TressShell.SetDataChange( [ enPreguntas ] );
          end;
     end;
end;

procedure TdmEvaluacion.cdsPreguntasAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditaPreguntas, TEditaPreguntas );
end;

procedure TdmEvaluacion.cdsPreguntasBeforePost(DataSet: TDataSet);
begin
     if ZetaCommonTools.StrVacio( cdsPreguntas.FieldByName( 'PG_DESCRIP' ).AsString ) then
        DataBaseError( 'La descripci�n de la pregunta no puede quedar vac�a' );
end;

procedure TdmEvaluacion.cdsPreguntasNewRecord(DataSet: TDataSet);
begin
     with cdsPreguntas do
     begin
          FieldByName('PG_FOLIO').AsInteger := ServerEvaluacion.SiguientePregunta( dmCliente.Empresa );
          FieldByName('PG_ORDEN').AsInteger := ( RecordCount + K_UNO );
          FieldByName('CM_CODIGO').AsString := cdsCompetencia.FieldByName('CM_CODIGO').AsString;
     end;
end;

procedure TdmEvaluacion.cdsPreguntasAfterDelete(DataSet: TDataSet);
begin
     cdsPreguntas.Enviar;
end;

{cdsEscalas}
procedure TdmEvaluacion.cdsEscalasAlAdquirirDatos(Sender: TObject);
begin
     cdsEscalas.Data := ServerEvaluacion.GetCatEscalas( dmCliente.Empresa );
end;

procedure TdmEvaluacion.cdsEscalasAfterDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if( Reconcile( ServerEvaluacion.GrabaCatEscalas( dmCliente.Empresa, Delta, ErrorCount ) ) ) then
               begin
                    TressShell.SetDataChange( [ enEscalas ] );
               end;
          end;
     end;
end;

procedure TdmEvaluacion.cdsEscalasAlModificar(Sender: TObject);
var
   sCodigo: string;
begin
     cdsEditEscala.Refrescar;
     sCodigo := cdsEscalas.FieldByName( 'SC_CODIGO' ).AsString;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditaEscalas, TEditaEscalas );
     cdsEscalas.Locate( 'SC_CODIGO', sCodigo, [] );
end;

procedure TdmEvaluacion.cdsEscalasAlAgregar(Sender: TObject);
begin
     cdsEditEscala.Agregar;
end;

function TdmEvaluacion.ObtieneSNOrdenMaximo( const oDataSet: TZetaClientDataSet; oCampo: TField ): Integer;
var
   lHayHueco: Boolean;
   i, iTemp, iTempSig: Integer;
begin
     lHayHueco := False;
     with oDataSet do
     begin
          DisableControls;
          try
             Last;
             iTemp := oCampo.AsInteger;
             if( RecordCount > 1 )then
             begin
                  for i := 1 to RecordCount do
                  begin
                       iTemp := oCampo.AsInteger;
                       Next;
                       iTempSig := oCampo.AsInteger;
                       lHayHueco := ( ( iTemp + K_UNO ) <> iTempSig );
                       if( lHayHueco )then
                          break;
                  end;
             end;
             if not( lHayHueco )then
             begin
                  iTemp := oCampo.AsInteger;
                  for i := 1 to RecordCount do
                  begin
                       if( iTemp < oCampo.AsInteger )then
                           iTemp := oCampo.AsInteger;
                       Next;
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
     Result := iTemp + K_UNO;
end;

procedure TdmEvaluacion.cdsEscalasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

procedure TdmEvaluacion.cdsEscalasBeforePost(DataSet: TDataSet);
begin
     
end;

{ cdsEditEscala }
procedure TdmEvaluacion.cdsEditEscalaAlAdquirirDatos(Sender: TObject);
begin
     cdsEscalas.Conectar;
     GetEscalasNiveles( cdsEscalas.FieldByName( 'SC_CODIGO' ).AsString );
end;

procedure TdmEvaluacion.GetEscalasNiveles( const sEscala: string );
var
   oNivelesEscala: OleVariant;
begin
     cdsEscalas.Conectar;
     cdsEditEscala.Data := ServerEvaluacion.GetCatNivelesEscala( dmCliente.Empresa, sEscala, oNivelesEscala );
     cdsNivelesEscala.Data := oNivelesEscala;
end;

procedure TdmEvaluacion.cdsEditEscalaAlAgregar(Sender: TObject);
begin
     GetEscalasNiveles( VACIO );
     cdsEditEscala.Append;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditaEscalas, TEditaEscalas );
end;

procedure TdmEvaluacion.cdsEditEscalaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   oEscNiv: OleVariant;
begin
     ErrorCount := 0;
     with cdsNivelesEscala do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;
     with cdsEditEscala do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) or
             ( cdsNivelesEscala.ChangeCount > 0 ) then
          begin
               if ( Reconcile( ServerEvaluacion.GrabaEscalaNiveles( dmCliente.Empresa, DeltaNull, cdsNivelesEscala.DeltaNull, ErrorCount, oEscNiv ) ) ) and
                  ( (  cdsNivelesEscala.ChangeCount = 0 ) or cdsNivelesEscala.Reconcile( oEscNiv ) ) then
               begin
                    TressShell.SetDataChange( [ enEscalas ] );
                    cdsEditEscala.MergeChangeLog;
               end
               else
               begin
                     with cdsEditEscala do
                     begin
                          if ( ChangeCount > 0 ) or ( cdsNivelesEscala.ChangeCount > 0 ) then
                          begin
                               Edit;
                          end;
                     end;
               end;
          end;
     end;
end;

procedure TdmEvaluacion.cdsEditEscalaBeforeDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with cdsEscalas do
     begin
          if Reconcile( ServerEvaluacion.BorraEscala(  dmCliente.Empresa,
                                                        FieldByName( 'SC_CODIGO' ).AsString,
                                                        ErrorCount ) ) then
          begin
               TressShell.SetDataChange( [ enEscalas ] );
               cdsEditEscala.MergeChangeLog;
          end;
     end;
end;

procedure TdmEvaluacion.cdsEditEscalaBeforeInsert(DataSet: TDataSet);
begin
     cdsNivelesEscala.EmptyDataSet;
end;

procedure TdmEvaluacion.cdsEditEscalaBeforePost(DataSet: TDataSet);
begin
     if( strVacio( cdsNivelesEscala.FieldByName('SN_NOMBRE').AsString ) )then
     begin
          if (cdsNivelesEscala.RecordCount > 0 ) then
             cdsNivelesEscala.Delete;
          DatabaseError( 'El nombre no puede quedar vac�o' );
     end;
     cdsBeforePost( cdsEditEscala, 'SC_CODIGO' );
end;


{ ******** cdsNivelesEscala ***********}
procedure TdmEvaluacion.AgregaNivelesxEscala;
begin
     with cdsNivelesEscala do
     begin
          iOrden := ObtieneSNOrdenMaximo( cdsNivelesEscala, FieldByName('SN_ORDEN')  );
          Insert;
          FieldByName('SN_ORDEN').AsInteger := iOrden;
          FieldByName( 'SC_CODIGO' ).AsString := cdsEditEscala.FieldByName( 'SC_CODIGO' ).AsString;
          PostData;
          iOrden := 0;
     end;
end;

procedure TdmEvaluacion.SN_ORDENOnChange(Sender: TField);
begin
     with cdsNivelesEscala do
     begin
          FieldByName('SN_VALOR').AsFloat := FieldByName('SN_ORDEN').AsInteger;
     end;
end;

procedure TdmEvaluacion.cdsNivelesEscalaAfterInsert(DataSet: TDataSet);
begin
     with cdsEditEscala do
     begin
          if ( State = dsBrowse ) then
               Edit;
     end;
end;

procedure TdmEvaluacion.cdsNivelesEscalaAlCrearCampos(Sender: TObject);
begin
     with cdsNivelesEscala do
     begin
          MaskPesos('SN_VALOR');
          FieldByName( 'SN_ORDEN' ).OnChange := SN_ORDENOnChange;
          FieldbyName( 'SN_VALOR' ).OnChange := SN_VALOROnChange;
     end;
end;

procedure TdmEvaluacion.SN_VALOROnChange(Sender: TField);
begin
     with cdsNivelesEscala do
     begin
          if( strVacio( FieldByName('SN_VALOR').AsString ) )then
              FieldByName('SN_VALOR').AsFloat := 0.0;
     end;
end;

procedure TdmEvaluacion.cdsNivelesEscalaNewRecord(DataSet: TDataSet);
begin
     if( iOrden = 0 )then
         AgregaNivelesxEscala;
end;

procedure TdmEvaluacion.cdsNivelesEscalaBeforeInsert(DataSet: TDataSet);
begin
     with cdsNivelesEscala do
     begin
          if( ( not IsEmpty ) and ( strVacio( FieldByName('SN_NOMBRE').AsString ) ) )then
          begin
               if (cdsNivelesEscala.RecordCount > 0 ) then
               begin
                    Delete;
               end;
               DatabaseError( 'El nombre no puede quedar vac�o' );
          end;
     end;
end;

procedure TdmEvaluacion.cdsNivelesEscalaGetRights( Sender: TZetaClientDataSet; const iRight: Integer;
  var lHasRights: Boolean);
begin
     lHasRights := False;
end;

{ cdsCriteriosEval }

procedure TdmEvaluacion.cdsCriteriosEvalAlAdquirirDatos(Sender: TObject);
begin
     FOrden := 0;
     ObtenerCriteriosxEncuesta( cdsCriteriosEval );
end;

procedure TdmEvaluacion.ObtenerCriteriosxEncuesta( oClientDataSet: TZetaClientDataSet );
begin
     oClientDataSet.Data := ServerEvaluacion.GetCriteriosEvaluar( dmCliente.Empresa, dmCliente.Encuesta );
end;

procedure TdmEvaluacion.cdsCriteriosEvalAfterDelete(DataSet: TDataSet);
begin
     cdsCriteriosEval.Enviar;
end;

procedure TdmEvaluacion.CambiaOrdenCriterios( iPosActual: Integer; iPosNueva: Integer );
begin
     ServerEvaluacion.CambiaCriterios( dmCliente.Empresa, dmCliente.Encuesta, iPosActual, iPosNueva );
end;

procedure TdmEvaluacion.cdsCriteriosEvalAlCrearCampos(Sender: TObject);
begin
     cdsCompetencia.Conectar;
     with cdsCriteriosEval do
     begin
          MaskPesos( 'EC_PESO' );
          ListaFija( 'EC_GET_COM', lfComentariosMgr );
          CreateSimpleLookup( cdsCompetencia, 'CM_DESCRIP', 'CM_CODIGO' );
          FieldByName( 'EC_POS_PES' ).OnChange := EC_POS_PESOnChange;
     end;
end;

procedure TdmEvaluacion.cdsCriteriosEvalAlBorrar(Sender: TObject);
begin
     if ModificaDisenoEncuesta then
     begin
          if( ZetaMsgDlg.ConfirmaCambio( '� Desea borrar este registro ?' ) )then
          begin
               ServerEvaluacion.BorraCriterio( dmCliente.Empresa, dmCliente.Encuesta, cdsCriteriosEval.FieldByName( 'EC_ORDEN' ).AsInteger );
               cdsCriteriosEval.Refrescar;
          end;
     end;
end;

procedure TdmEvaluacion.cdsCriteriosEvalAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsCriteriosEval do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerEvaluacion.GrabaCriteriosEval( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enEncComp, enEncPreg ] );
               end;
          end;
     end;
end;

procedure TdmEvaluacion.cdsCriteriosEvalAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCriterioEvaluar, TEditCriterioEvaluar );
end;

procedure TdmEvaluacion.cdsCriteriosEvalNewRecord(DataSet: TDataSet);
begin
     with cdsCriteriosEval do
     begin
          FieldByName( 'ET_CODIGO' ).AsInteger := dmCliente.Encuesta;
          FieldByName( 'EC_ORDEN' ).AsInteger := cdsCriteriosEval.RecordCount + 1;
          FieldByName( 'EC_POS_PES' ).AsInteger := K_UNO;
          FieldByName( 'EC_PESO' ).AsInteger := K_UNO;
          FieldByName( 'EC_GET_COM' ).AsInteger := 0;
          FieldByName( 'CM_CODIGO' ).AsString := VACIO;
     end;
end;

procedure TdmEvaluacion.cdsCriteriosEvalBeforePost(DataSet: TDataSet);
begin
     with cdsCriteriosEval do
     begin
          if ( FieldByName( 'EC_PESO' ).AsFloat < K_UNO ) and ( StrVacio( dmCliente.PesoEsc ) ) then
              DataBaseError( Format( 'El peso debe ser mayor � igual a %d', [ K_UNO ] ) );
          if StrVacio( FieldByName( 'EC_DETALLE' ).AsString ) then
              FieldByName( 'EC_DETALLE' ).AsString := K_ESPACIO;
     end;
end;

procedure TdmEvaluacion.EC_POS_PESOnChange(Sender: TField);
begin
     cdsNivelesEnc.Conectar;
     with cdsCriteriosEval do
     begin
          if( strLleno( dmCliente.PesoEsc ) )then
          begin
               FieldByName('EC_PESO').AsFloat := cdsNivelesEnc.FieldByName('EL_VALOR').AsFloat;
               FieldByName('EL_NOMBRE').AsString := cdsNivelesEnc.FieldByName('EL_NOMBRE').AsString;
          end;
     end;
end;

{ cdsPregCriterio }

procedure TdmEvaluacion.GetPreguntasPorCriterio( const iCriterio: Integer );
begin
     with ServerEvaluacion do
     begin
          {Para que se actualice ET_NUM_PRE y no afecte en el web}
          ContadoresDEncuesta( dmCliente.Empresa, dmCliente.Encuesta );
          cdsPregCriterio.Data := GetPreguntasCriterio( dmCliente.Empresa, dmCliente.Encuesta, iCriterio );
     end;
end;

procedure TdmEvaluacion.cdsPregCriterioAlAdquirirDatos(Sender: TObject);
begin
     //Descomente la linea porque si se comenta, no se refresca el grid cuando se cambia la posicion de las
     //preguntas ni se refresca la posicion de la pregunta en la encuesta.
     GetPreguntasPorCriterio( cdsCriteriosxPreg.FieldByName( 'EC_ORDEN' ).AsInteger );
end;

procedure TdmEvaluacion.cdsPregCriterioAlCrearCampos(Sender: TObject);
begin
     with cdsPregCriterio do
     begin
          ListaFija( 'EP_GET_COM', lfComentariosPedir );
          MaskPesos( 'EP_PESO' );
          FieldByName( 'EP_POS_PES' ).OnChange := EP_POS_PESOnChange;
     end;
end;

procedure TdmEvaluacion.CambiaOrdenPreguntasCriterio( iCriterio: Integer; iPosActual: Integer; iPosNueva: Integer );
begin
     ServerEvaluacion.CambiaPreguntaCriterio( dmCliente.Empresa, dmCliente.Encuesta, iCriterio, iPosActual, iPosNueva );
end;

procedure TdmEvaluacion.cdsPregCriterioAlBorrar(Sender: TObject);
var
   iCriterio, iPregunta: Integer;
begin
     if ModificaDisenoEncuesta then
     begin
          iCriterio := cdsCriteriosxPReg.FieldByName( 'EC_ORDEN' ).AsInteger;
          iPregunta := cdsPregCriterio.FieldByName( 'EP_ORDEN' ).AsInteger;
          if ZetaMsgDlg.ConfirmaCambio( '� Desea borrar este registro ?' ) then
          begin
               ServerEvaluacion.BorraPreguntaCriterio( dmCliente.Empresa, dmCliente.Encuesta, iCriterio, iPregunta );
               cdsPregCriterio.Refrescar;
          end;
     end;
end;

procedure TdmEvaluacion.cdsPregCriterioAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditPreguntaCriterio, TEditPreguntaCriterio );
end;

procedure TdmEvaluacion.cdsPregCriterioNewRecord(DataSet: TDataSet);
begin
     with cdsPregCriterio do
     begin
          FieldByName( 'ET_CODIGO' ).AsInteger := dmCliente.Encuesta;
          FieldByName( 'EC_ORDEN' ).AsInteger := cdsCriteriosxPreg.FieldByName( 'EC_ORDEN' ).AsInteger;
          FieldByName( 'EP_ORDEN' ).AsInteger := cdsPregCriterio.RecordCount + 1;
          FieldByName( 'EE_CODIGO' ).AsString := dmCliente.EscalaDef;
          FieldByName( 'CM_CODIGO' ).AsString := cdsCriteriosxPreg.FieldByName( 'CM_CODIGO' ).AsString;
          FieldByName( 'EP_POS_PES' ).AsInteger := K_UNO;
          FieldByName( 'EP_PESO' ).AsFloat := K_UNO;
          FieldByName( 'EP_TIPO' ).AsInteger := Ord( erComboBox );
          FieldByName( 'EP_GET_COM' ).AsInteger := Ord( cpedNo );
          FieldByName( 'EP_GET_NA' ).AsInteger := Ord( cmDefault );
     end;
end;

procedure TdmEvaluacion.cdsPregCriterioBeforePost(DataSet: TDataSet);
begin
     with cdsPregCriterio do
     begin
          if ( FieldByName( 'EP_PESO' ).AsFloat < K_UNO ) then
             DataBaseError( Format( 'El peso debe ser mayor � igual a %d', [ K_UNO ] ) );
          if ZetaCommonTools.StrVacio( FieldByName( 'EP_DESCRIP' ).AsString ) then
             DataBaseError( 'El texto de la pregunta no puede quedar vac�o' );
          if ZetaCommonTools.StrVacio( FieldByName( 'EE_CODIGO' ).AsString ) then
             DataBaseError( 'El c�digo de escala no puede quedar vac�o' );
     end;
end;

procedure TdmEvaluacion.EP_POS_PESOnChange(Sender: TField);
begin
     cdsNivelesEnc.Conectar;
     with cdsPregCriterio do
     begin
          if StrLleno( dmCliente.PesoEsc ) then
          begin
               FieldByName( 'EP_PESO' ).AsFloat := cdsNivelesEnc.FieldByName( 'EL_VALOR' ).AsFloat;
               FieldByName( 'EL_NOMBRE' ).AsString := cdsNivelesEnc.FieldByName( 'EL_NOMBRE' ).AsString;
          end;
     end;
end;

procedure TdmEvaluacion.cdsPregCriterioAlEnviarDatos(Sender: TObject);
var
   iPosicion: Integer;
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPregCriterio do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerEvaluacion.GrabaPreguntasCriterio( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    iPosicion := FieldByName( 'EP_ORDEN' ).AsInteger;
                    Refrescar;
                    Locate( 'EP_ORDEN', iPosicion, [] );
               end;
          end;
     end;
end;

{ *** cdsCriteriosxPReg *** }

procedure TdmEvaluacion.cdsCriteriosxPregAlAdquirirDatos(Sender: TObject);
begin
     ObtenerCriteriosxEncuesta( cdsCriteriosxPreg );
end;

{ *** cdsEscalasEncuesta *** }

procedure TdmEvaluacion.cdsEscalasEncuestaAlAdquirirDatos(Sender: TObject);
begin
     cdsEscalasEncuesta.Data := ServerEvaluacion.GetEscalasEncuesta( dmCliente.Empresa, dmCliente.Encuesta );
end;

procedure TdmEvaluacion.cdsEscalasEncuestaAlModificar(Sender: TObject);
begin
     GetEscalasNivelesEnc( cdsEscalasEncuesta.FieldByName( 'EE_CODIGO' ).AsString );
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEscalaEncuesta, TEditEscalaEncuesta );
end;

procedure TdmEvaluacion.cdsEscalasEncuestaBeforePost(DataSet: TDataSet);
begin
     if StrVacio( cdsEscalasEncuesta.FieldByName( 'EE_NOMBRE' ).AsString ) then
         DataBaseError( 'El nombre de la escala no puede quedar vac�o' );
     if StrVacio( cdsEscalaNivEnc.FieldByName( 'EL_NOMBRE' ).AsString ) then
     begin
          if (cdsEscalaNivEnc.RecordCount > 0 ) then
          begin
               cdsEscalaNivEnc.Delete;
          end;
          DataBaseError( 'El nombre puede quedar vac�o' );
     end;
     cdsBeforePost( cdsEscalasEncuesta, 'EE_CODIGO' );
end;

procedure TdmEvaluacion.GetEscalasNivelesEnc( const sEscala: string );
begin
     with dmCliente do
     begin
          cdsEscalaNivEnc.Data := ServerEvaluacion.GetEscalaEncuestaNiv( Empresa, Encuesta, sEscala );
     end;
end;

procedure TdmEvaluacion.cdsEscalasEncuestaAlAgregar(Sender: TObject);
begin
     if ModificaDisenoEncuesta then
     begin
          GetEscalasNivelesEnc( VACIO );
          cdsEscalasEncuesta.Append;
          ZBaseEdicion_DevEx.ShowFormaEdicion( EditEscalaEncuesta, TEditEscalaEncuesta );
     end;
end;

procedure TdmEvaluacion.cdsEscalasEncuestaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   oEscNivEnc: OleVariant;
begin
     ErrorCount := 0;
     with cdsEscalaNivEnc do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;
     with cdsEscalasEncuesta do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) or
             ( cdsEscalaNivEnc.ChangeCount > 0 ) then
          begin
               if ( Reconcile( ServerEvaluacion.GrabaEscalaNivelesEnc( dmCliente.Empresa, DeltaNull, cdsEscalaNivEnc.DeltaNull, ErrorCount, oEscNivEnc ) ) ) and
                  ( (  cdsEscalaNivEnc.ChangeCount = 0 ) or cdsEscalaNivEnc.Reconcile( oEscNivEnc ) ) then
               begin
                    TressShell.SetDataChange( [ enEncEsca ] );
                    cdsEscalasEncuesta.MergeChangeLog;
                    // Se hace la llamada al server porque en el servidor no se conoce el C�digo de Encuesta
                    // al modificar solo el hijo.
                    ServerEvaluacion.ContadoresDEncuesta( dmCliente.Empresa, dmCliente.Encuesta );
               end
               else
               begin
                     with cdsEscalasEncuesta do
                     begin
                          if ( ChangeCount > 0 ) or ( cdsEscalaNivEnc.ChangeCount > 0 ) then
                          begin
                               Edit;
                          end;
                     end;
               end;
          end;
     end;
end;

procedure TdmEvaluacion.cdsEscalasEncuestaBeforeInsert(DataSet: TDataSet);
begin
     cdsEscalaNivEnc.EmptyDataSet;
end;

procedure TdmEvaluacion.cdsEscalasEncuestaNewRecord(DataSet: TDataSet);
begin
     with cdsEscalasEncuesta do
     begin
          FieldByName('ET_CODIGO').AsInteger := dmCliente.Encuesta;
     end;
end;

procedure TdmEvaluacion.cdsEscalasEncuestaAfterDelete(DataSet: TDataSet);
begin
     cdsEscalasEncuesta.Enviar;
end;

procedure TdmEvaluacion.CopiaEscalasEncuesta;
var
   sLlave, sDescripcion: String;
begin
     with cdsEscalas do
     begin
          Conectar;
          Search( VACIO, sLlave, sDescripcion );
          if( strLleno( sLlave ) )then
          begin
               if( cdsEscalasEncuesta.Locate( 'EE_CODIGO', sLlave, [] ) )then
                   ZetaDialogo.ZInformation( 'Informaci�n', 'La escala seleccionada ya existe en las escalas de encuesta', 0 )
               else
               begin
                    if( ServerEvaluacion.CopiaEscalasEncuesta( dmCliente.Empresa, dmCliente.Encuesta, sLlave ) <= 0 )then
                    begin
                         ZetaDialogo.ZInformation('Informaci�n', 'La escala selecciona fu� copiada a la Encuesta con �xito', 0 );
                         TressShell.SetDataChange( [ enEncEsca ] );
                    end
                    else
                        ZetaDialogo.ZInformation('Informaci�n', 'No se pudo copiar la escala seleccionada, error al copiar escala', 0 );
               end;
          end;
     end;
end;

{ *** cdsPerfilEvaluador *** }

procedure TdmEvaluacion.cdsPerfilEvaluadorAlAdquirirDatos(Sender: TObject);
begin
     cdsPerfilEvaluador.Data := ServerEvaluacion.GetPerfilEvaluador( dmCliente.Empresa, dmCliente.Encuesta );
end;

procedure TdmEvaluacion.cdsPerfilEvaluadorAlCrearCampos(Sender: TObject);
begin
     with cdsPerfilEvaluador do
     begin
          MaskPesos('ER_PESO');
          FieldByName( 'ER_TIPO' ).OnChange := ER_TIPOOnChange;
          FieldByName( 'ER_POS_PES' ).OnChange := ER_POS_PESOnChange;
     end;
end;

procedure TdmEvaluacion.ER_POS_PESOnChange(Sender: TField);
begin
     cdsNivelesEnc.Conectar;
     with cdsPerfilEvaluador do
     begin
          if( strLleno( dmCliente.PesoEsc ) )then
          begin
               FieldByName('ER_PESO').AsFloat := cdsNivelesEnc.FieldByName('EL_VALOR').AsFloat;
               FieldByName('EL_NOMBRE').AsString := cdsNivelesEnc.FieldByName('EL_NOMBRE').AsString;
          end;
     end;
end;

procedure TdmEvaluacion.ER_TIPOOnChange(Sender: TField);
begin
     with cdsPerfilEvaluador do
     begin
          FieldByName('ER_NOMBRE').AsString := ObtieneElemento( lfTipoEvaluaEnc, FieldByName( 'ER_TIPO' ).AsInteger );
     end;
end;

procedure TdmEvaluacion.cdsPerfilEvaluadorAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditPerfilEvaluador, TEditPerfilEvaluador );
end;

procedure TdmEvaluacion.cdsPerfilEvaluadorAfterDelete(DataSet: TDataSet);
begin
     cdsPerfilEvaluador.Enviar;
end;

procedure TdmEvaluacion.cdsPerfilEvaluadorAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPerfilEvaluador do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerEvaluacion.GrabaPerfilEvaluador( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enEncRela ] );
               end;
          end;
     end;
end;

procedure TdmEvaluacion.cdsPerfilEvaluadorBeforePost(DataSet: TDataSet);
begin
     with cdsPerfilEvaluador do
     begin
          if( strVacio(FieldByName('ER_NOMBRE').AsString) )then
              DataBaseError( 'El nombre no puede quedar vac�o' );
          if( FieldByName('ER_NUM_MAX').AsInteger < FieldByName('ER_NUM_MIN').AsInteger )then
              DataBaseError( 'El valor m�ximo debe de ser mayor � igual al valor m�nimo' );
          if( strVacio( dmCliente.PesoEsc ) and ( FieldByName('ER_PESO').AsFloat < 1 ) )then
              DataBaseError( 'El peso debe de ser mayor � igual a 1' );
          if( ( FieldbyName('ER_TIPO').AsInteger = Ord( tevMismo ) ) and
            ( ( FieldByName('ER_NUM_MIN').AsInteger = 0 ) or ( FieldByName('ER_NUM_MAX').AsInteger = 0 ) ) )then
              DataBaseError( 'El m�nimo/m�ximo de evaluadores no puede ser cero' );

     end;
end;

procedure TdmEvaluacion.cdsPerfilEvaluadorNewRecord(DataSet: TDataSet);
var
   eMaximoLF, eMaxTipo: eTipoEvaluaEnc;
begin
     eMaximoLF := High( eTipoEvaluaEnc );
     eMaxTipo := eTipoEvaluaEnc( ServerEvaluacion.GetMaxTipoEvaluador( dmCliente.Empresa, dmCliente.Encuesta ) );
     with cdsPerfilEvaluador do
     begin
          FieldByName('ER_POS_PES').AsInteger := K_UNO;
          FieldByName('ER_PESO').AsFloat := K_UNO;
          if ( eMaxTipo >= eMaximoLF ) then
              FieldByName('ER_TIPO').AsInteger := Ord( eMaximoLF )
          else
          begin
               if( RecordCount > 0 )then
                   FieldByName('ER_TIPO').AsInteger := Ord( eMaxTipo ) + 1
               else
                   FieldByName('ER_TIPO').AsInteger := Ord( eMaxTipo );
          end;
          FieldByName('ET_CODIGO').AsInteger := dmCliente.Encuesta;
          FieldbyName('ER_NUM_MIN').AsInteger := 1;
          FieldbyName('ER_NUM_MAX').AsInteger := 1;
     end;
end;

procedure TdmEvaluacion.LlenaPerfilEvaluadores( const oLista: TStrings; aFiltro: TFiltroRelaciones; const lPonerTodas: Boolean = FALSE; const lEsCombo: Boolean = TRUE );
var
   eTipo: eTipoEvaluaEnc;
   sDescripcion: String;
begin
     with oLista do
     begin
          BeginUpdate;
          try
             Clear;
             if lPonerTodas then
                 Add( Format( '-1=%s', [ K_TODAS_DESCRIPCION ] ) );
             with cdsPerfilEvaluador do
             begin
                  DisableControls;
                  try
                     Refrescar;
                     First;
                     while not EOF do
                     begin
                          eTipo := eTipoEvaluaEnc( FieldByName( 'ER_TIPO' ).AsInteger );
                          if not ( eTipo in aFiltro ) then
                          begin
                               case eTipo of
                                    tevMismo: sDescripcion := 'Autoevaluaci�n';
                               else
                                   sDescripcion := FieldByName( 'ER_NOMBRE' ).AsString;
                               end;
                               if lEsCombo then
                                   Add( IntToStr( Ord( eTipo ) ) + '=' + sDescripcion )
                               else
                                   AddObject( sDescripcion,  TObject( FieldByName( 'ER_TIPO' ).AsInteger ) );
                          end;
                          Next;
                     end;
                  finally
                         EnableControls;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TdmEvaluacion.LlenaPerfilEvaluadores( const oLista: TStrings; const lPonerTodas: Boolean = FALSE; const lEsCombo: Boolean = TRUE );
begin
     LlenaPerfilEvaluadores( oLista, [], lPonerTodas, lEsCombo );
end;

procedure TdmEvaluacion.cdsPerfilEvaluadorAlBorrar(Sender: TObject);
begin
     if ModificaDisenoEncuesta then
     begin
          if( ZetaMsgDlg.ConfirmaCambio( '� Desea borrar este registro ?' ) )then
              cdsPerfilEvaluador.Delete;
     end;
end;

{ *** cdsEmpEvaluar *** }

procedure TdmEvaluacion.cdsEmpEvaluarAlModificar(Sender: TObject);
var
   oCursor: TCursor;
   oEditEmpEvaluar: TEditEmpEvaluar_DevEx;
begin
     with cdsPerfilEvaluador do
     begin
          Conectar;
          if( RecordCount > 0 )then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  oEditEmpEvaluar := TEditEmpEvaluar_DevEx.Create( Application );
                  try
                     oEditEmpEvaluar.ShowModal;
                  finally
                         FreeAndNil( oEditEmpEvaluar );
                  end;
               finally
                      Screen.Cursor := oCursor;
               end;
          end
          else
              ZetaDialogo.ZInformation('Empleados a evaluar', 'No se tienen registrados perfiles de evaluadores', 0 );
     end;
end;

procedure TdmEvaluacion.ObtieneSujetosEvaluar( const iEmpleado: Integer; const iStatus: Integer; const oDataSet: TZetaClientDataSet );
begin
     oDataset.Data := ServerEvaluacion.GetEmpEvaluar( dmCliente.Empresa, dmCliente.Encuesta, iEmpleado, iStatus );
end;

procedure TdmEvaluacion.GrabaSujetoRelacion( const iSujeto: Integer; const iRelacion: Integer; const iEmpleado: Integer; const lInserta: Boolean );
begin
     ServerEvaluacion.AgregaEvalua( dmCliente.Empresa, dmCliente.Encuesta, iSujeto, iRelacion, iEmpleado, 0, lInserta );
     TressShell.SetDataChange( [ enEncuesta ] );
     cdsEmpEvaluar.Locate( 'CB_CODIGO', iEmpleado, [] );
end;

function TdmEvaluacion.AgregaSujetoArbol( const iSujeto: Integer; const iRelacion: Integer; const iEmpleado: Integer; const lInserta: Boolean ): Boolean;
var
   i, iSujetoIns: Integer;
   oNodoSelec: TTreeNode;
   lExisteUsuario: Boolean;
begin
     Result := True;
     iSujetoIns := iSujeto;
     if not( lInserta )then
     begin
          oNodoSelec := FArbolEvaluadores.Selected;
          iSujetoIns := Integer( oNodoSelec.Data );
          GrabaSujetoRelacion( iSujetoIns, iRelacion, iEmpleado, lInserta );
          CreaArbolEmpEvaluar;
          cdsEmpEvaluar.Locate( 'CB_CODIGO', iEmpleado, [] );
     end
     else
     begin
          with FArbolEvaluadores do
          begin
               lExisteUsuario := False;
               for i:= 0 to ( Items.Count - 1 ) do
               begin
                    if( ( Items[i].Parent <> nil )and( Integer(Items[i].Data) = iSujetoIns ) )then
                    begin
                         lExisteUsuario := True;
                         break;
                    end;
               end
          end;
          if not( lExisteUsuario )then
          begin
               GrabaSujetoRelacion( iSujetoIns, iRelacion, iEmpleado, lInserta );
               CreaArbolEmpEvaluar;
               cdsEmpEvaluar.Locate( 'CB_CODIGO', iEmpleado, [] );
          end
          else
          begin
               ZetaDialogo.ZInformation( 'Empleados a Evaluar', Format( '%s ya est� en la lista de Evaluadores', [ dmSistema.cdsUsuarios.FieldByName('US_NOMBRE').AsString ] ), 0 );
               Result := False;
          end;
     end;
end;

procedure TdmEvaluacion.CreaArbolEmpEvaluar;
var
   oNodoPadre, oNodoHijo: TTreeNode;
begin
     FArbolEvaluadores.Items.Clear;
     with dmSistema.cdsUsuarios do
     begin
          Filtered := False;
          Conectar;
     end;
     cdsEvalua.Refrescar;
     FArbolEvaluadores.Items.BeginUpdate;
     try
        with cdsPerfilEvaluador do
        begin
             First;
             while not EOF do
             begin
                  oNodoPadre := FArbolEvaluadores.Items.AddObject( CreaNodo( FieldByName('ER_NOMBRE').AsString, 0 ),
                                                                             FieldByName('ER_NOMBRE').AsString,
                                                                             Pointer( FieldByName('ER_TIPO').AsInteger ) );
                  cdsEvalua.First;
                  while not cdsEvalua.EOF do
                  begin
                       if( cdsEvalua.FieldByName('EV_RELACIO').AsInteger = FieldByName('ER_TIPO').AsInteger )then
                       begin
                            if( dmSistema.cdsUsuarios.Locate( 'US_CODIGO', cdsEvalua.FieldByName('US_CODIGO').AsInteger, [] ) ) then
                            begin
                                 oNodoHijo:= FArbolEvaluadores.Items.AddChildObject( oNodoPadre, dmSistema.cdsUsuarios.FieldByName('US_NOMBRE').AsString,
                                                                                                 Pointer(dmSistema.cdsUsuarios.FieldByName('US_CODIGO').AsInteger) );
                                 oNodoHijo.ImageIndex := 2;
                                 oNodoHijo.SelectedIndex := 4;
                            end;
                       end;
                       cdsEvalua.Next;
                  end;
                  Next;
             end;
        end;
        FArbolEvaluadores.FullExpand;
        dmSistema.cdsUsuarios.Filtered := True;
        finally
            FArbolEvaluadores.Items.EndUpdate;
     end;
end;

function TdmEvaluacion.CreaNodo( const sNombre: String; const iImage: Integer ): TTreeNode;
var
   oNodo: TTreeNode;
begin
     oNodo := TTreeNode.Create( FArbolEvaluadores.Items );
     with oNodo do
     begin
          Text := sNombre;
          ImageIndex := 0;
     end;
     Result := oNodo;
end;

procedure TdmEvaluacion.BorraEvaluadoArbol( const oNodoSelec: TTreeNode; const iEmpleado: Integer );
begin
     ServerEvaluacion.BorraEvalua( dmCliente.Empresa, dmCliente.Encuesta, iEmpleado, Integer( oNodoSelec.Data ) );
     TressShell.SetDataChange( [ enEncuesta ] );
     cdsEmpEvaluar.Locate( 'CB_CODIGO', iEmpleado, [] );
     CreaArbolEmpEvaluar;
end;

function TdmEvaluacion.ObtieneEvaluador( var sEvaluador: String; var iRelacion: Integer ): Boolean;
var
   oNodoSelec, oNodoPadre: TTreeNode;
begin
     Result := False;
     if( FArbolEvaluadores <> nil )then
     begin
          with dmSistema.cdsUsuarios do
          begin
               Filtered := False;
               oNodoSelec := FArbolEvaluadores.Selected;
               if( ( oNodoSelec <> nil ) and ( Locate( 'US_CODIGO', Integer( oNodoSelec.Data ), [] ) ) )then
               begin
                    oNodoPadre := oNodoSelec.Parent;
                    sEvaluador := InttoStr(FieldByName('US_CODIGO').AsInteger) + ' = ' +FieldByName('US_NOMBRE').AsString;
                    if( oNodoPadre <> nil )then
                    begin
                         iRelacion := Integer( oNodoPadre.Data );
                         Result := True;
                    end
                    else
                        Result := False;
               end;
          end;
     end;
end;

procedure TdmEvaluacion.cdsEmpEvaluarAlCrearCampos(Sender: TObject);
begin
     with cdsEmpEvaluar do
     begin
          ListaFija( 'SJ_STATUS', lfStatusSujeto );
     end;
end;

procedure TdmEvaluacion.ObtieneTotalesPorEvaluado( const iEmpleado: Integer );
var
   sNombre: String;
   oStatusEvaluacion: eStatusEvaluacion;
begin
     with dmCliente do
     begin
          cdsDatosxEvaluado.Data := ServerEvaluacion.GetDatosXEvaluado( Empresa, Encuesta, iEmpleado );
     end;
     sNombre := VACIO;
     //***** Inicializa Valores ******//
     with TotalesxEvaluado do
     begin
         for oStatusEvaluacion := Low( eStatusEvaluacion ) to High( eStatusEvaluacion ) do
          begin
               sNombre := ObtieneElemento( lfStatusEvaluacion, Ord( oStatusEvaluacion ) );
               AddInteger( sNombre, 0 );
          end;
     end;

     with cdsDatosxEvaluado do
     begin
          First;
          while not EOF do
          begin
               case( FieldByName('EV_STATUS').AsInteger )of
                     Ord( sevNueva ): sNombre := ObtieneElemento( lfStatusEvaluacion, Ord( sevNueva ) );
                     Ord( sevLista ): sNombre := ObtieneElemento( lfStatusEvaluacion, Ord( sevLista ) );
                     Ord( sevPreparada ): sNombre := ObtieneElemento( lfStatusEvaluacion, Ord( sevPreparada ) );
                     Ord( sevEnProceso ): sNombre := ObtieneElemento( lfStatusEvaluacion, Ord( sevEnProceso ) );
                     Ord( sevTerminada ): sNombre := ObtieneElemento( lfStatusEvaluacion, Ord( sevTerminada ) );
               else
                   ZetaDialogo.ZError( 'Error', 'Status de la evaluaci�n inv�lido', 0 );
               end;
               TotalesxEvaluado.ParamByName( sNombre ).AsInteger := FieldByName('CUANTOS').AsInteger;
               Next;
          end;
     end;
end;

procedure TdmEvaluacion.BorrarEmpleadosPorEvaluar( Lista: TStringList );
var
   i: Integer;
   lBorrado: Boolean;
begin
     lBorrado := False;
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with cdsEmpEvaluar do
               begin
          			if( Locate('CB_CODIGO',StrToInt(Lista[i]), [] ) ) then
                        if BorraUnEmpleadoPorEvaluar( FieldByName( 'CB_CODIGO' ).AsInteger, False ) then
                           lBorrado := True;
               end;
          end;
     end;
     if lBorrado then
        TressShell.SetDataChange( [ enSujeto ] );
end;

procedure TdmEvaluacion.BorrarEmpleadoPorEvaluar( const iEmpleado: TNumEmp );
begin
     if BorraUnEmpleadoPorEvaluar( iEmpleado, True ) then
        TressShell.SetDataChange( [ enSujeto ] );
end;

function TdmEvaluacion.BorraUnEmpleadoPorEvaluar( const iEmpleado: TNumEmp; const lPrompt: Boolean = True ): Boolean;
const
     K_YA_HAY_CREADAS = 'Se tienen %0:d evaluaciones%1:sya creadas para este evaluado ( # %2:d )%1:s%1:s�Desea borrarlo?';
     K_YA_HAY_AVANCE = 'De un total de %0:d evaluaciones, se tiene%1:sun %2:.2f %3:s de avance que se perder� al%1:seliminar al evaluado( # %4:d )%1:s%1:s�Desea borrarlo?';
var
   rAvance: Real;
   iNuevas, iListas, iPreparadas, iEnProceso, iTerminadas, iTotal: Integer;
begin
     Result := False;
     ObtieneTotalesPorEvaluado( iEmpleado );
     with TotalesxEvaluado do
     begin
          iNuevas := Items[ Ord( sevNueva ) ].AsInteger;
          iListas := Items[ Ord( sevLista ) ].AsInteger;
          iPreparadas := Items[ Ord( sevPreparada ) ].AsInteger;
          iEnProceso := Items[ Ord( sevEnProceso ) ].AsInteger;
          iTerminadas := Items[ Ord( sevTerminada ) ].AsInteger;
     end;
     iTotal := ( iNuevas + iListas + iPreparadas + iEnProceso + iTerminadas );
     if ( iTotal > 0 ) then
     begin
          if ZetaDialogo.ZConfirm( 'Confirmaci�n', Format( K_YA_HAY_CREADAS, [ iTotal, CR_LF, iEmpleado ] ), 0 , mbNo ) then
          begin
               rAvance := 100 * ( ( iEnProceso + iTerminadas ) / iTotal );
               if ( rAvance > 0 ) then
               begin
                    if ZetaDialogo.ZConfirm( 'Advertencia', Format( K_YA_HAY_AVANCE, [ iTotal, CR_LF, rAvance, '%', iEmpleado ] ), 0, mbNo ) then
                    begin
                         with dmCliente do
                         begin
                              ServerEvaluacion.BorraEmpEvaluar( Empresa, Encuesta, iEmpleado );
                         end;
                         Result := True;
                    end;
               end
               else
               begin
                    with dmCliente do
                    begin
                         ServerEvaluacion.BorraEmpEvaluar( Empresa, Encuesta, iEmpleado );
                    end;
                    Result := True;
               end;
          end;
     end
     else
     begin
          if not lPrompt or ZetaMsgDlg.ConfirmaCambio( '� Desea borrar este empleado ?' ) then
          begin
               with dmCliente do
               begin
                    ServerEvaluacion.BorraEmpEvaluar( Empresa, Encuesta, iEmpleado );
               end;
               Result := True;
          end;
     end;
end;

procedure TdmEvaluacion.cdsEmpEvaluarAlAgregar(Sender: TObject);
begin
     RegistraEmpleadosEvaluar;
end;

function TdmEvaluacion.ObtenerPreguntasContestadas( const iUsuario, iEmpleado: Integer): Integer;
begin
     with dmCliente do
     begin
          Result := ServerEvaluacion.GetPregContestadas( Empresa,  iUsuario, iEmpleado, Encuesta );
     end;
end;

{ *** cdsEncuestas *** }

procedure TdmEvaluacion.cdsEncuestasAlAdquirirDatos(Sender: TObject);
begin
     cdsEncuestas.Data := ServerEvaluacion.GetEncuestas( dmCliente.Empresa );
end;

procedure TdmEvaluacion.cdsEncuestasAlCrearCampos(Sender: TObject);
begin
     with cdsEncuestas do
     begin
          MaskFecha( 'ET_FEC_INI' );
          MaskFecha( 'ET_FEC_FIN' );
          ListaFija( 'ET_STATUS', lfStatusEncuesta );
     end;
end;

procedure TdmEvaluacion.cdsEncuestasAlAgregar(Sender: TObject);
begin
     with cdsEncuestas do
     begin
          DisableControls;
          try
             Append;
             if not( ZcxWizardBasico.ShowWizard( TAgregarEncuesta_DevEx ) ) and ( State in [dsEdit,dsInsert] )then
                Cancel;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmEvaluacion.cdsEncuestasNewRecord(DataSet: TDataSet);
begin
     with cdsEncuestas do
     begin
          FieldByName( 'ET_USR_ADM' ).AsInteger := dmCliente.Usuario;
          FieldByName( 'ET_ESCALA' ).AsString := 'Escala';
          FieldByName( 'ET_GET_COM' ).AsString := K_GLOBAL_SI;
          FieldByName( 'ET_CONSOLA' ).AsString := K_GLOBAL_SI;
          FieldByName( 'ET_TXT_COM' ).AsString := 'Comentarios';
          FieldByName( 'ET_ESTILO' ).AsInteger := Ord( erComboBox );
          FieldByName( 'ET_FEC_INI' ).AsDateTime := dmCliente.FechaDefault;

          FieldByName( 'ET_FEC_FIN' ).AsDateTime := dmCliente.FechaDefault;
     end;
end;

procedure TdmEvaluacion.cdsEncuestasBeforePost(DataSet: TDataSet);
const
K_ESPACIO=' ';
begin
     with cdsEncuestas do
     begin
          {
          if StrVacio( FieldByName( 'ET_NOMBRE' ).AsString ) then
             DatabaseError( 'El nombre de la Encuesta/Evaluaci�n no puede quedar vac�o' );
          }
          if ( FieldByName('ET_FEC_INI').AsDateTime > FieldByName('ET_FEC_FIN').AsDateTime ) then
             DatabaseError( 'La fecha de incio de la encuesta debe de ser menor � igual que la fecha de fin' );
          if ( FieldByName('ET_USR_ADM').AsInteger <= 0 ) then
             DatabaseError( 'Se debe de asignar un responsable a la encuesta' );
          if StrVacio( FieldByName( 'ET_ESC_DEF' ).AsString ) then
             DatabaseError( 'Se debe asignar una escala de evaluaci�n' );
          if ( eComoCalificar( FieldByName( 'ET_PTS_CAL' ).AsInteger ) = eccaNoUsar ) then
             FieldByName( 'ET_ESC_CAL' ).AsString := VACIO;
          if( StrVacio(FieldByName('ET_MSG_INI'). AsString )  ) then
              FieldByName('ET_MSG_INI'). AsString := K_ESPACIO;
          if ( strVacio(FieldByName('ET_MSG_FIN'). AsString ) ) then
              FieldByName('ET_MSG_FIN'). AsString :=  K_ESPACIO ;
     end;
end;

procedure TdmEvaluacion.cdsEncuestasAlEnviarDatos(Sender: TObject);
begin
     GrabaEncuesta( True );
end;

procedure TdmEvaluacion.GrabaEncuesta( const lGrabaCriterio: Boolean );
var
   ErrorCount: Integer;
begin
     with cdsEncuestas do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerEvaluacion.GrabaEncuesta( dmCliente.Empresa, Delta, cdsAgregaCriterios.Data, cdsAgregaEvaluador.Data, lGrabaCriterio, ErrorCount ) );
               TressShell.SetDataChange( [ enEncComp, enEncRela ] );
          end;
     end;
end;

procedure TdmEvaluacion.CopiaEncuesta( const sEncuesta: String; const lCopiaEmp, lCopiaEvaluador: Boolean );
begin
     with cdsEncuestas do
     begin
          ServerEvaluacion.CopiaEncuesta( dmCliente.Empresa, FieldByName('ET_CODIGO').AsInteger, sEncuesta, lCopiaEmp, lCopiaEvaluador );
          Refrescar;
     end;
end;

procedure TdmEvaluacion.CambiarDocumentacion( Dataset: TDataset; const sFieldName, sDocumento: String );
begin
     with Dataset do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          FieldByName( sFieldName ).AsString := sDocumento;
     end;
end;

procedure TdmEvaluacion.cdsEncuestasAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEncuesta, TEditEncuesta );
end;

procedure TdmEvaluacion.cdsEncuestasAfterDelete(DataSet: TDataSet);
begin
     GrabaEncuesta( False );
end;

procedure TdmEvaluacion.InicializaEncuesta( const iEncuesta: Integer );
begin
     ServerEvaluacion.InicializaEncuesta( dmCliente.Empresa, iEncuesta );
end;

function TdmEvaluacion.CambiaStatusEnDiseno: Boolean;
const
     K_AVISO = 'Existen evaluaciones en proceso que se perder�n %0:ssi se cambia el status de la encuesta%0:s%0:s�Desea continuar?';
     K_CONFIRMACION = 'De un total de %1:d evaluaciones, se tiene%0:sun %2:3.2n %3:s de avance que se perder� con el%0:scambio de status%0:s%0:s�Desea cambiar el status?';
var
   rAvance: Real;
   oCursor: TCursor;
   iNumEvaluaciones, iEnProceso, iTerminadas: Integer;
begin
     Result := False;
     rAvance := 0;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        iNumEvaluaciones := AnalizaEncuesta;
        iEnProceso := TotalesEvaluaciones.Items[ Ord( sevEnProceso ) ].AsInteger; //ParamByName('EnProceso').AsInteger;
        iTerminadas := TotalesEvaluaciones.Items[ Ord( sevTerminada ) ].AsInteger; //ParamByName('Terminadas').AsInteger;
        if ( iNumEvaluaciones > 0 ) then
           rAvance := ( ( iEnProceso + iTerminadas ) / iNumEvaluaciones ) * 100;
        if ( rAvance > 0 ) then
        begin
             if ZetaDialogo.zConfirm( K_PROMPT_CAMBIO_STATUS, Format( K_AVISO, [ CR_LF ] ), 0, mbNo ) then
             begin
                  if ZetaDialogo.zConfirm( 'Advertencia', Format( K_CONFIRMACION, [ CR_LF, iNumEvaluaciones, rAvance, '%' ] ), 0, mbNo ) then
                  begin
                       Result := True;
                       InicializaEncuesta( cdsEncuestas.FieldByName( 'ET_CODIGO' ).AsInteger );
                  end;
             end;
        end
        else
            Result := True;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TdmEvaluacion.CambiaStatusAbierta: Boolean;
const
     K_AVISO = 'La encuesta no cuenta con evaluaciones preparadas para%0:sser contestadas, por lo que no puede ser abierta';
     K_CONFIRMACION = 'Todav�a existen evaluaciones pendientes%0:sde ser preparadas%0:s%0:s�Desea abrir la encuesta?';
var
   oCursor: TCursor;
   iNuevas, iNumEvaluaciones: Integer;
begin
     Result := True;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        iNumEvaluaciones := AnalizaEncuesta;
        iNuevas := TotalesEvaluaciones.Items[ Ord( sevNueva ) ].AsInteger;
        if ( iNuevas = iNumEvaluaciones ) then
        begin
             ZetaDialogo.zInformation( K_PROMPT_CAMBIO_STATUS, Format( K_AVISO, [ CR_LF ] ), 0 );
             Result := False;
        end
        else
        begin
             if ( ( iNuevas > 0 ) and ( iNuevas < iNumEvaluaciones ) ) then
             begin
                  Result := ZetaDialogo.ZConfirm( K_PROMPT_CAMBIO_STATUS, Format( K_CONFIRMACION, [ CR_LF ] ), 0, mbNo );
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TdmEvaluacion.CambiaStatusCerrada: Boolean;
const
     K_CONFIRMACION = 'Esta por cerrarse la encuesta, por lo que%0:s ya no estar� disponible para los evaluadores%0:s%0:s�Desea cerrar la encuesta?';
var
   oCursor: TCursor;
   iNuevas, iNumEvaluaciones: Integer;
begin
     Result := True;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        iNumEvaluaciones := AnalizaEncuesta;
        iNuevas := TotalesEvaluaciones.Items[ Ord( sevNueva ) ].AsInteger;
        if ( iNuevas <> iNumEvaluaciones ) then
        begin
             Result := ZetaDialogo.ZConfirm( K_PROMPT_CAMBIO_STATUS, Format( K_CONFIRMACION, [ CR_LF ] ), 0, mbNo );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TdmEvaluacion.AnalizaEncuesta: Integer;
begin
      with cdsEncuestas do
      begin
           Result := FieldbyName( 'ET_NUM_EVA' ).AsInteger;
           ObtieneTotalesEncuesta( FieldByName( 'ET_CODIGO' ).AsInteger );
      end;
end;

function TdmEvaluacion.ValidaCambioStatusEncuesta( const eStEncuesta: eStatusEncuesta ): Boolean;
begin
     case eStEncuesta of
          stenDiseno: Result := CambiaStatusEnDiseno;
          stenAbierta: Result := CambiaStatusAbierta;
          stenCerrada: Result := CambiaStatusCerrada;
     else
         ZetaDialogo.ZError( 'Error', 'Status de Encuesta inv�lido', 0 );
         Result := False;
     end;
end;

{ *** cdsCriterios *** }

procedure TdmEvaluacion.LlenaCriterios( Lista: TCriteriosEval );
var
   FCriterio: TCriterioEval;
begin
     with cdsCriterios do
     begin
          DisableControls;
          try
             Refrescar;
             First;
             while not EOF do
             begin
                  //MA: Estos objetos se detruyen dentro de la forma WizAgregarEncuesta
                  FCriterio := Lista.AddCriterio( FieldByName( 'CM_DESCRIP' ).AsString );
                  with FCriterio do
                  begin
                       Codigo:= FieldByName( 'CM_CODIGO' ).AsString;
                       TipoCriterio := FieldByName( 'TC_CODIGO' ).AsString;
                  end;
                  Next;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmEvaluacion.LlenaTiposCompetencia( Lista : TStrings; const lPonerTodas: Boolean = FALSE; const lEsCombo: Boolean = FALSE );
var
   FTiposCompetencia: TTipoCompetencia;
begin
     with cdsTCompetencia do
     begin
          Lista.BeginUpdate;
          DisableControls;
          try
             if lPonerTodas then
             begin
                  Lista.Add( Format( '%s=Todos', [ K_TODOS_CODIGO ] ) );
             end;
             Refrescar;
             First;
             while not EOF do
             begin
                  //MA: Estos objetos se detruyen dentro de la forma WizAgregarEncuesta
                  if lEsCombo then
                      Lista.Add( FieldByName( 'TC_CODIGO' ).AsString + '=' + FieldByName( 'TC_DESCRIP' ).AsString )
                  else
                  begin
                       FTiposCompetencia := TTipoCompetencia.Create;
                       with FTiposCompetencia do
                       begin
                            Codigo:= FieldByName( 'TC_CODIGO' ).AsString;
                            Descripcion := FieldByName( 'TC_DESCRIP' ).AsString;
                       end;
                       Lista.AddObject( FieldByName( 'TC_DESCRIP' ).AsString, FTiposCompetencia );
                  end;
                  Next;
             end;
          finally
                 EnableControls;
                 Lista.EndUpdate;
          end;
     end;
end;

procedure TdmEvaluacion.CreaServerAgregaCriterios;
begin
     with cdsAgregaCriterios do
     begin
          InitTempDataset;
          AddStringField( 'CM_CODIGO', K_ANCHO_CODIGO ); { C�digo de Competencia }
          AddStringField( 'CM_DESCRIP', K_ANCHO_DESCRIPCION ); { Descripci�n de Competencia }
          CreateTempDataset;
     end;
end;

procedure TdmEvaluacion.CreaServerAgregaEvaluador;
begin
     with cdsAgregaEvaluador do
     begin
          InitTempDataset;
          AddIntegerField( 'ER_TIPO' ); { Tipo de Evaluador }
          AddStringField( 'ER_DESCRIP', K_ANCHO_DESCRIPCION ); { Descripci�n de Evaluador }
          CreateTempDataset;
     end;
end;

procedure TdmEvaluacion.LlenaEvaluadores( Lista: TStrings );
var
   eType: eTipoEvaluaEnc;
begin
     with Lista do
     begin
          Clear;
          BeginUpdate;
          try
             for eType := Low( eTipoEvaluaEnc ) to High( eTipoEvaluaEnc ) do
             begin
                  AddObject( ZetaCommonLists.ObtieneElemento( lfTipoEvaluaEnc, Ord( eType ) ), TObject( eType ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TdmEvaluacion.LlenaDataSetCriterios( const Lista: TStrings );
var
   i: Integer;
   Criterio: TCriterioEval;
begin
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               Criterio := TCriterioEval( Objects[ i ] );
               with cdsAgregaCriterios do
               begin
                    Insert;
                    FieldByName( 'CM_CODIGO' ).AsString := Criterio.Codigo;
                    FieldByName( 'CM_DESCRIP' ).AsString := Criterio.Descripcion;
                    Post;
               end;
          end;
     end;
end;

procedure TdmEvaluacion.LlenaDataSetEvaluadores( const Lista: TStrings );
var
   i: Integer;
   eType: eTipoEvaluaEnc;
begin
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if Assigned( Objects[ i ] ) then
                  eType := eTipoEvaluaEnc( Objects[ i ] )
               else
                   eType := eTipoEvaluaEnc( 0 );
               with cdsAgregaEvaluador do
               begin
                    Insert;
                    FieldByName( 'ER_TIPO' ).AsInteger := Ord( eType );
                    FieldByName( 'ER_DESCRIP' ).AsString := Strings[ i ];
                    Post;
               end;
          end;
     end;
end;

procedure TdmEvaluacion.cdsCriteriosAlAdquirirDatos(Sender: TObject);
begin
     cdsCriterios.Data := ServerEvaluacion.GetCatHabilidades( dmCliente.Empresa );
end;

procedure TdmEvaluacion.AgregaCompetencias;
var
   iCriterios: Integer;
begin
     with dmCliente do
     begin
          iCriterios := 0;
          if( Assigned( cdsCriteriosEval ) )then
              iCriterios := cdsCriteriosEval.RecordCount;
          ServerEvaluacion.AgregaCriterios( Empresa, Encuesta, iCriterios, cdsAgregaCriterios.Data );
          TressShell.SetDatachange( [ enEncPreg ] );
     end;
end;

{ *** cdsNivelesEnc *** }

procedure TdmEvaluacion.cdsNivelesEncAlAdquirirDatos(Sender: TObject);
var
  sFilter: string;
begin
     sFilter := ' ET_CODIGO = '+ inttoStr( dmCliente.Encuesta ) + ' and EE_CODIGO = ' + EntreComillas( dmCliente.PesoEsc );
     cdsEncuestas.Conectar;
     with cdsNivelesEnc do
     begin
          //Filtered := False;
          //Filter := ' ET_CODIGO = '+ inttoStr( dmCliente.Encuesta ) + ' and EE_CODIGO = ' + EntreComillas( dmCliente.PesoEsc );
          Data := ServerEvaluacion.GetCatNivelesEnc( dmCliente.Empresa, sFilter );
          //Filtered := True;
     end;
end;

procedure TdmEvaluacion.cdsNivelesEncGetRights( Sender: TZetaClientDataSet;
                                                const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

{ *** cdsEscalaNivEnc *** }

procedure TdmEvaluacion.cdsEscalaNivEncAfterInsert(DataSet: TDataSet);
begin
     with cdsEscalasEncuesta do
     begin
          if ( State = dsBrowse ) then
               Edit;
     end;
end;

procedure TdmEvaluacion.cdsEscalaNivEncAlCrearCampos(Sender: TObject);
begin
     with cdsEscalaNivEnc do
     begin
          MaskPesos( 'EL_VALOR' );
          FieldByName( 'EL_ORDEN' ).OnChange := EL_ORDENOnChange;
          FieldByName( 'EL_VALOR' ).OnChange := EL_VALOROnChange;
     end;
end;

procedure TdmEvaluacion.cdsEscalaNivEncNewRecord(DataSet: TDataSet);
begin
     if ( iOrden = 0 ) then
     begin
          AgregaNivelesEncuesta;
     end;
end;

procedure TdmEvaluacion.AgregaNivelesEncuesta;
begin
     with cdsEscalaNivEnc do
     begin
          iOrden := ObtieneSNOrdenMaximo( cdsEscalaNivEnc, FieldByName('EL_ORDEN') );
          Insert;
          FieldByName('ET_CODIGO').AsInteger := dmCliente.Encuesta;
          FieldByName('EL_ORDEN').AsInteger := iOrden;
          FieldByName( 'EE_CODIGO' ).AsString := cdsEscalasEncuesta.FieldByName( 'EE_CODIGO' ).AsString;
          PostData;
          iOrden := 0;
     end;
end;

procedure TdmEvaluacion.EL_ORDENOnChange(Sender: TField);
begin
     with cdsEscalaNivEnc do
     begin
          FieldByName('EL_VALOR').AsFloat := FieldByName('EL_ORDEN').AsInteger;
     end;
end;

procedure TdmEvaluacion.EL_VALOROnChange(Sender: TField);
begin
     with cdsEscalaNivEnc do
     begin
          if( strVacio( FieldByName('EL_VALOR').AsString ) )then
              FieldByName('EL_VALOR').AsFloat := 0.0;
     end;
end;

procedure TdmEvaluacion.cdsEscalaNivEncBeforeInsert(DataSet: TDataSet);
begin
     with cdsEscalaNivEnc do
     begin
          if( ( not IsEmpty ) and strVacio( FieldByName('EL_NOMBRE').AsString ) )then
          begin
               if( cdsEscalaNivEnc.RecordCount>0 ) then
               begin
                    Delete;
               end;
               DatabaseError( 'El nombre no puede quedar vac�o' );
          end;
     end;
end;

{ *** cdsEscalasEnc *** }

procedure TdmEvaluacion.cdsEscalasEncAlAdquirirDatos(Sender: TObject);
begin
     ObtieneEscalasEncuesta( dmCliente.Encuesta );
end;

procedure TdmEvaluacion.ObtieneEscalasEncuesta( const iEncuesta: Integer );
begin
     with cdsEscalasEnc do
     begin
          Data := ServerEvaluacion.GetEscalasEncuesta( dmCliente.Empresa, iEncuesta );
     end;
end;

procedure TdmEvaluacion.cdsEscalasEncGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

{ *** cdsEvalua *** }

procedure TdmEvaluacion.cdsEvaluaAlAdquirirDatos(Sender: TObject);
begin
     with cdsEvalua do
     begin
          Data := ServerEvaluacion.GetEvalua( dmCliente.Empresa, dmCliente.Encuesta, cdsEmpEvaluar.FieldByName('CB_CODIGO').AsInteger );
     end;
end;

{ *** cdsTotalesEncuesta *** }

procedure TdmEvaluacion.cdsTotalesEncuestaAlAdquirirDatos(Sender: TObject);
begin
     ObtieneTotalesEncuesta( dmCliente.Encuesta );
end;

procedure TdmEvaluacion.CargaValoresTotalesEvaluacion( const oData: OleVariant );
var
   sNombre: String;
   oStatusEvaluacion: eStatusEvaluacion;
begin
     with cdsTotalesEvaluacion do
     begin
          Data := oData;
          //******** Inicializa valores ****************//
          for oStatusEvaluacion := Low( eStatusEvaluacion ) to High( eStatusEvaluacion ) do
          begin
               sNombre := ObtieneElemento( lfStatusEvaluacion, Ord( oStatusEvaluacion ) );
               TotalesEvaluaciones.AddInteger( sNombre, 0 );
          end;
          First;
          while not EOF do
          begin
               case( FieldByName('EV_STATUS').AsInteger )of
                     Ord( sevNueva ): sNombre := ObtieneElemento( lfStatusEvaluacion, Ord( sevNueva ) );
                     Ord( sevLista ): sNombre := ObtieneElemento( lfStatusEvaluacion, Ord( sevLista ) );
                     Ord( sevPreparada ): sNombre := ObtieneElemento( lfStatusEvaluacion, Ord( sevPreparada ) );
                     Ord( sevEnProceso ): sNombre := ObtieneElemento( lfStatusEvaluacion, Ord( sevEnProceso ) );
                     Ord( sevTerminada ): sNombre := ObtieneElemento( lfStatusEvaluacion, Ord( sevTerminada ) );
               else
                   ZetaDialogo.ZError( 'Error', 'Status de la evaluaci�n inv�lido', 0 );
               end;
               TotalesEvaluaciones.ParamByName( sNombre ).AsInteger :=  FieldByName('CUANTOS').AsInteger;
               Next;
          end;
     end;
end;

procedure TdmEvaluacion.CargaValoresTotalesEvaluador( const oData: OleVariant );
const
     K_NO_EMPEZADO = 'NO_EMPEZADO';
     K_CONTESTARON_ALGUNAS = 'CONTEST_ALGUNAS';
     K_TERMINARON = 'TERMINARON';
     K_VAL_NO_EMPEZADO = 2;
     K_VAL_CONTESTARON_ALGUNAS = 3;
     K_VAL_TERMINARON = 4;
var
   sNombre: String;
begin
     with cdsTotalesEvaluador do
     begin
          Data := oData;
          //******** Inicializa valores ****************//
          with TotalesEvaluadores do
          begin
               AddInteger( K_NO_EMPEZADO, 0 );
               AddInteger( K_CONTESTARON_ALGUNAS, 0 );
               AddInteger( K_TERMINARON, 0 );
          end;
          First;
          while not EOF do
          begin
               with TotalesEvaluadores do
               begin
                    case( FieldByName('SE_STATUS').AsInteger )of
                          K_VAL_NO_EMPEZADO: sNombre := K_NO_EMPEZADO;
                          K_VAL_CONTESTARON_ALGUNAS: sNombre := K_CONTESTARON_ALGUNAS;
                          K_VAL_TERMINARON: sNombre := K_TERMINARON;
                    else
                        ZetaDialogo.ZError( 'Error', 'Status de evaluador inv�lido', 0 );
                    end;
                    ParamByName( sNombre ).AsInteger := FieldByName('CUANTOS').AsInteger;
               end;
               Next;
          end;
     end;
end;

procedure TdmEvaluacion.CargaValoresTotalesEvaluado( const oData: OleVariant );
var
   sNombre: String;
   oStatusSujeto: eStatusSujeto;
begin
     
     with cdsTotalesEvaluado do
     begin
          Data := oData;
          //******** Inicializa valores ****************//
          for oStatusSujeto := Low( eStatusSujeto ) to High( eStatusSujeto ) do
          begin
               sNombre := ObtieneElemento( lfStatusSujeto, Ord( oStatusSujeto ) );
               TotalesEvaluados.AddInteger( sNombre, 0 );
          end;
          First;
          while not EOF do
          begin
               case( FieldByName('SJ_STATUS').AsInteger )of
                     Ord( ssNuevo ): sNombre := ObtieneElemento( lfStatusSujeto, Ord( ssNuevo ) );
                     Ord( ssListo ): sNombre := ObtieneElemento( lfStatusSujeto, Ord( ssListo ) );
                     Ord( ssPreparado ): sNombre := ObtieneElemento( lfStatusSujeto, Ord( ssPreparado ) );
                     Ord( ssEnProceso ): sNombre := ObtieneElemento( lfStatusSujeto, Ord( ssEnProceso ) );
                     Ord( ssTerminado ): sNombre := ObtieneElemento( lfStatusSujeto, Ord( ssTerminado ) );
               else
                   ZetaDialogo.ZError( 'Error', 'Status del evaluado inv�lido', 0 );
               end;
               TotalesEvaluados.ParamByName( sNombre ).AsInteger := FieldByName('CUANTOS').AsInteger;
               Next;
          end;
     end;
end;

procedure TdmEvaluacion.ObtieneTotalesEncuesta( const iEncuesta: Integer );
var
   Evaluaciones, Evaluadores, Evaluados: OleVariant;
begin
     with cdsTotalesEncuesta do
     begin
          Data := ServerEvaluacion.GetDatosEncuesta( dmCliente.Empresa, iEncuesta, Evaluaciones, Evaluadores, Evaluados );
     end;
     //******** Evaluaciones *************//
     CargaValoresTotalesEvaluacion( Evaluaciones ) ;
     //******* Evaluadores ***************//
     CargaValoresTotalesEvaluador( Evaluadores );
     //******* Evaluados *************//
     CargaValoresTotalesEvaluado( Evaluados );
end;

procedure TdmEvaluacion.cdsTotalesEncuestaAlCrearCampos(Sender: TObject);
begin
     with cdsTotalesEncuesta do
     begin
          ListaFija( 'ET_STATUS', lfStatusEncuesta );
     end;
end;

{ *** cdsSujeto *** }

procedure TdmEvaluacion.cdsSujetoAlAdquirirDatos(Sender: TObject);
begin
     //ObtieneSujetosEvaluar( 0, 0, cdsSujeto );
end;

procedure TdmEvaluacion.ObtieneEmpleadosEvaluados( const iEmpleado, iStatus: Integer );
begin
     ObtieneSujetosEvaluar( iEmpleado, iStatus, cdsSujeto );
end;

procedure TdmEvaluacion.cdsSujetoAlCrearCampos(Sender: TObject);
begin
     with cdsSujeto do
     begin
          ListaFija( 'SJ_STATUS', lfStatusSujeto );
     end;
end;

{ *** cdsUsuarios *** }

procedure TdmEvaluacion.cdsUsuariosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

{ *** cdsEvaluadores  *** }

procedure TdmEvaluacion.ObtieneEvaluadores( const sFiltros: String );
begin
     with dmCliente do
     begin
          cdsEvaluadores.Data:= ServerEvaluacion.GetEvaluadores( Empresa, Encuesta, sFiltros );
          cdsEvaluadoresLookup.Data := cdsEvaluadores.Data;
     end;
end;

procedure TdmEvaluacion.cdsEvaluadoresAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsEvaluadores do
     begin
          FieldByName( 'SE_NUEVA' ).OnGetText := No_CerosGetText;
          FieldByName( 'SE_PROCESO' ).OnGetText := No_CerosGetText;
          FieldByName( 'SE_TERMINA' ).OnGetText := No_CerosGetText;
          FieldByName( 'SE_STATUS' ).OnGetText := FijaStatusEvaluadorGetText;
          //CreateSimpleLookup( dmSistema.cdsUsuarios, 'US_NOMBRE', 'US_CODIGO' );
     end;
end;

procedure TdmEvaluacion.FijaStatusEvaluadorGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          with Sender.DataSet do
          begin
               if IsEmpty then
                  Text := VACIO
               else
               begin
                    if ( FieldByName( 'SE_TERMINA' ).AsInteger = FieldByName( 'SE_TOTAL' ).AsInteger ) then
                        Text := ZetaEvaluacionTools.GetStatusEvaluador( esteTerminado )
                    else
                    begin
                         if ( FieldbyName( 'SE_NUEVA' ).AsInteger = FieldByName( 'SE_TOTAL' ).AsInteger ) then
                            Text := ZetaEvaluacionTools.GetStatusEvaluador( esteNoHaComenzado )
                         else
                             Text := ZetaEvaluacionTools.GetStatusEvaluador( esteEnProceso );
                    end;
               end;
          end;
     end
     else
         Text:= VACIO;
end;

procedure TdmEvaluacion.No_CerosGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if( Sender.AsInteger = 0 )then
              Text:= VACIO
          else
              Text := Sender.AsString;
     end
     else
         Text:= Sender.AsString;
end;

{ *** cdsEvaluaciones *** }

procedure TdmEvaluacion.ObtieneEvaluaciones( const sFiltro: String );
begin
     with cdsEvaluaciones do
     begin
          Data:= ServerEvaluacion.GetEvaluaciones( dmCliente.Empresa, dmCliente.Encuesta, sFiltro );
     end;
end;

procedure TdmEvaluacion.cdsEvaluacionesAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsEvaluaciones do
     begin
          FieldByName( 'EV_RELACIO' ).OnGetText := EV_RELACIOGetText;
          ListaFija( 'EV_STATUS', lfStatusEvaluacion );
          CreateSimpleLookup( dmSistema.cdsUsuarios, 'US_NOMBRE', 'US_CODIGO' );
     end;
end;

procedure TdmEvaluacion.EV_RELACIOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.DataSet.IsEmpty then
         Text := VACIO
     else
     begin
          if DisplayText then
          begin
               Sender.Alignment := taLeftJustify;
               with cdsPerfilEvaluador do
               begin
                    Conectar;
                    if IsEmpty then
                       Text := VACIO
                    else
                    begin
                         if Locate( 'ER_TIPO', Sender.AsInteger, [] ) then
                             Text := cdsPerfilEvaluador.FieldByName( 'ER_NOMBRE' ).AsString
                         else
                             Text := VACIO;
                    end;
               end;
          end
          else
              Text:= Sender.AsString;
     end;
end;

{ *** cdsAnalisisCriterio *** }

procedure TdmEvaluacion.ObtenerAnalisisCriterio( const iMostrar, iRelacion, iOrden: Integer );
begin
     with cdsAnalisisCriterio do
     begin
          Data:= ServerEvaluacion.GetAnalisisCriterio( dmCliente.Empresa, dmCliente.Encuesta, iMostrar, iRelacion, iOrden );
     end;
end;

{ ***  cdsAnalisisPreguntas *** }

procedure TdmEvaluacion.ObtenerAnalisisPreguntas( const iMostrar, iRelacion, iCriterio, iOrden: Integer );
begin
     with cdsAnalisisPregunta do
     begin
          Data:= ServerEvaluacion.GetAnalisisPreguntas( dmCliente.Empresa, dmCliente.Encuesta, iMostrar, iRelacion, iCriterio, iOrden );
     end;
end;

procedure TdmEvaluacion.cdsAnalisisPreguntaAlAdquirirDatos(Sender: TObject);
begin

end;

{ *** cdsCriteriosEnc *** }

procedure TdmEvaluacion.cdsCriteriosEncAlAdquirirDatos(Sender: TObject);
begin
     ObtenerCriteriosxEncuesta( cdsCriteriosEnc );
end;

procedure TdmEvaluacion.cdsCriteriosEncGetRights( Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

{ *** cdsPreguntasEnc *** }

procedure TdmEvaluacion.cdsPreguntasEncAlAdquirirDatos(Sender: TObject);
begin
     with cdsPreguntasEnc do
     begin
          Data := ServerEvaluacion.GetPreguntasEnc( dmCliente.Empresa, dmCliente.Encuesta );
     end;
end;

procedure TdmEvaluacion.ObtenerFrecuenciaResp( const iPregunta, iRelacion: Integer );
begin
     with cdsFrecuenciaResp do
     begin
          Data := ServerEvaluacion.GetFrecuenciaResp( dmCliente.Empresa, dmCliente.Encuesta, iPregunta, iRelacion );
     end;
end;

procedure TdmEvaluacion.cdsPreguntasEncGetRights( Sender: TZetaClientDataSet; const iRight: Integer;
                                                  var lHasRights: Boolean);
begin
     lHasRights := False;
end;

procedure TdmEvaluacion.cdsFrecuenciaRespAlCrearCampos(Sender: TObject);
begin
     with cdsFrecuenciaResp do
     begin
          FieldByName('EL_NOMBRE').OnGetText := EL_NOMBREOnGetText;
     end;
end;

procedure TdmEvaluacion.EL_NOMBREOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if( strVacio( Sender.AsString ) )then
          begin
               if( cdsFrecuenciaResp.RecordCount > 0 )then
                   Text:= '* Sin respuesta *'
               else
                   Text:= Sender.AsString;
          end
          else
              Text:= Sender.AsString;
     end
     else
         Text:= Sender.AsString;
end;

{ *** cdsResultadoCriterio *** }

procedure TdmEvaluacion.ObtenerResultadosCriterio( const iMostrar, iOrden: Integer );
begin
     with cdsResultadoCriterio do
     begin
          Data := ServerEvaluacion.GetResultadoCriterio( dmCliente.Empresa, dmCliente.Encuesta, dmCliente.Empleado, iMostrar, iOrden );
     end;
end;

procedure TdmEvaluacion.cdsResultadoCriterioAlAdquirirDatos( Sender: TObject);
begin
     //ObtenerResultadosCriterio( 0 );
end;

{ *** cdsResultadoPregunta *** }

procedure TdmEvaluacion.cdsResultadoPreguntaAlAdquirirDatos(Sender: TObject);
begin
     //ObtenerResultadoPregunta( 0 );
end;

procedure TdmEvaluacion.ObtenerResultadoPregunta( const iMostrar, iOrden: Integer );
begin
     with cdsResultadoPregunta do
     begin
          Data := ServerEvaluacion.GetResultadoPregunta( dmCliente.Empresa, dmCliente.Encuesta, dmCliente.Empleado, iMostrar, iOrden );
     end;
end;

{ *** cdsAgregaEmpEvaluar *** }

procedure TdmEvaluacion.cdsAgregaEmpEvaluarAlAdquirirDatos( Sender: TObject);
begin
     with cdsAgregaEmpEvaluar do
     begin
          Data := ServerEvaluacion.GetSujetos( dmCliente.Empresa );
     end;
end;

procedure TdmEvaluacion.RegistraEmpleadosEvaluar;
begin
     ZBaseGridEdicion_DevEx.ShowGridEdicion( GridEmpEvaluar, TGridEmpEvaluar, TRUE );
end;

procedure TdmEvaluacion.cdsAgregaEmpEvaluarAlCrearCampos(Sender: TObject);
begin
     with cdsAgregaEmpEvaluar.FieldByName( 'CB_CODIGO' ) do
     begin
          OnValidate := CB_CODIGOAgregaEmpEvaluarValidate;
          OnChange := CB_CODIGOAgregaEmpEvaluarChange;
     end;
end;

procedure TdmEvaluacion.CB_CODIGOAgregaEmpEvaluarValidate( Sender: TField );
begin
     with Sender do
     begin
          if ( AsInteger > 0 ) then
          begin
               if ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( AsInteger ) ) = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
                    DataBaseError( 'No Existe el Empleado #' + IntToStr( AsInteger ) );
          end
          else
              DataBaseError( 'N�mero de Empleado Debe Ser Mayor a Cero' );
     end;
end;

procedure TdmEvaluacion.CB_CODIGOAgregaEmpEvaluarChange( Sender: TField );
begin
     with cdsAgregaEmpEvaluar do
     begin
          FieldByName('ET_CODIGO').AsInteger := dmCliente.Encuesta;
          FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
     end;
end;

procedure TdmEvaluacion.cdsAgregaEmpEvaluarAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsAgregaEmpEvaluar do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerEvaluacion.AgregaEmpEvaluar( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enSujeto ] );
               end;
          end;
     end;
end;

procedure TdmEvaluacion.cdsAgregaEmpEvaluarReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind;
                                                           var Action: TReconcileAction);
var
   sError: String;
begin
      { PK_VIOLATION }
      if PK_VIOLATION( E ) then 
         sError := '� El Empleado ya existe !'
      else
          sError := E.Message;
      if ( E.Context <> VACIO ) then
          sError := sError + CR_LF + E.Context;
     ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 );
     case UpdateKind of
          ukDelete: Action := raCancel;
     else
         Action := raAbort;
     end;
end;

procedure TdmEvaluacion.CambiaStatusSujeto( const iEmpleado:Integer; const lListoEvaluar: Boolean );
begin
     with dmCliente do
     begin
          ServerEvaluacion.CambiaStatusSujeto( Empresa, Encuesta, iEmpleado, lListoEvaluar );
     end;
end;

function TdmEvaluacion.ModificaDisenoEncuesta( const lUtilizaValorActivo: Boolean = True; const oStatus: eStatusEncuesta = stenDiseno ): Boolean;
begin
     Result := True;
     if lUtilizaValorActivo then
     begin
          with dmCliente.GetDatosEncuestaActiva do
          begin
               if ( ( Status  =  stenAbierta ) or ( Status  =  stenCerrada ) ) then
               begin
                    ZetaDialogo.ZInformation( 'Cambio en dise�o de encuesta',
                                              'La encuesta se encuentra '''+ ObtieneElemento( lfStatusEncuesta, Ord( dmCliente.GetDatosEncuestaActiva.Status ) ) +''', para modificar ' +CR_LF+
                                              'su estructura es necesario cambiar el status a ''En dise�o''', 0 );
                    Result := False;
               end;
          end;
     end
     else
     begin
          with cdsEncuestas do
          begin
               if ( ( oStatus  =  stenAbierta  ) or ( oStatus  =  stenCerrada ) ) then
               begin
                    ZetaDialogo.ZInformation( 'Cambio en dise�o de encuesta',
                                              'La encuesta se encuentra '''+ ObtieneElemento( lfStatusEncuesta, FieldByName( 'ET_STATUS' ).AsInteger ) +''', para modificar ' +CR_LF+
                                              'su estructura es necesario cambiar el status a ''En dise�o''', 0 );
                    Result := False;
               end;
          end;
     end;
end;

{ *** cdsEvaluadosLookup *** }

procedure TdmEvaluacion.cdsEvaluadosLookupGetRights( Sender: TZetaClientDataSet; const iRight: Integer;
                                                     var lHasRights: Boolean);
begin
     lHasRights := False;
end;

procedure TdmEvaluacion.cdsEvaluadosLookupAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsEvaluadosLookup.Data := ServerEvaluacion.GetEmpEvaluar( Empresa, Encuesta, -1, Ord( ssPreparado ) );
     end;
end;

{ *** cdsEvaluadoresLookup *** }

procedure TdmEvaluacion.cdsEvaluadoresLookupGetRights( Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

procedure TdmEvaluacion.cdsEvaluadoresLookupAlCrearCampos(Sender: TObject);
begin
     //cdsEvaluadoresLookup.CreateSimpleLookup( dmSistema.cdsUsuarios, 'US_NOMBRE', 'US_CODIGO' );
end;

procedure TdmEvaluacion.cdsEvaluadoresLookupAlAdquirirDatos(Sender: TObject);
begin
     ObtieneEvaluadores( VACIO );
end;

{ **** cdsCorreos **** }

procedure TdmEvaluacion.GetListaCorreos( Parametros: TZetaParams );
begin
     cdsCorreos.Data := ServerEvaluacion.GetCorreos( Parametros.VarValues );
end;

procedure TdmEvaluacion.cdsCorreosAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsCorreos do
     begin
          MaskFecha( 'WO_FEC_IN' );
          MaskFecha( 'WO_FEC_OUT' );
          MaskBool( 'WO_ENVIADO' );
          ListaFija( 'WO_SUBTYPE', lfEmailType );
          FieldByName( 'WO_TO' ).OnGetText := MemoGetText;
          CreateCalculated( 'WO_STA_TXT', ftString, 15 );
          CreateSimpleLookup( dmSistema.cdsUsuarios, 'REMITENTE', 'US_ENVIA' );
          CreateSimpleLookup( dmSistema.cdsUsuarios, 'DESTINO', 'US_RECIBE' );
     end;
end;

procedure TdmEvaluacion.cdsCorreosAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( HistCorreosEdit, THistCorreosEdit );
end;

procedure TdmEvaluacion.cdsCorreosCalcFields(DataSet: TDataSet);
begin
     with cdsCorreos do
     begin
          FieldByName( 'WO_STA_TXT' ).AsString := ZetaCommonLists.ObtieneElemento( lfEVCorreoStatus, FieldByName( 'WO_STATUS' ).AsInteger );
     end;
end;

procedure TdmEvaluacion.MemoGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     { Se requiere condicionar para que al Hacer un }
     { cdsNomParam.Cancel no ocurra un Invalid BLOB Handle }
     if Assigned( Sender ) and Assigned( Sender.Dataset ) then
     begin
          if Sender.Dataset.State in [ dsBrowse ] then
             Text := Sender.AsString;
     end;
end;

{ ********** Manejo de Globales ************ }

{procedure TdmEvaluacion.SetGlobalesCorreos( const Valores: OleVariant );
begin
     with FGlobalesEvaluacion do
     begin
          ServerName := Valores[ EMAIL_SERVER ];
          UserName := Valores[ EMAIL_USER ];
          UserPassword := Valores[ EMAIL_PASSWORD ];
          EmailPort := Valores[ EMAIL_PORT ];
          EmailTimeout := Valores[ EMAIL_TIMEOUT ];
          RutaVirtual := Valores[ RUTA_VIRTUAL ];
     end;
end;

function TdmEvaluacion.GetGlobales: Boolean;
begin
     try
        SetGlobalesCorreos( ServerEvaluacion.GetGlobales );
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TdmEvaluacion.SetGlobales( const Valores: OleVariant );
begin
     try
        ServerEvaluacion.GrabaGlobales( Valores );
        SetGlobalesCorreos( Valores );
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;
}

{ *** cdsUsuariosRol *** }

procedure TdmEvaluacion.cdsUsuariosRolAfterCancel(DataSet: TDataSet);
begin
     GetRolesUsuario( GetUsuarioActual );
end;

function TdmEvaluacion.GetUsuarioActual: Integer;
begin
     Result := cdsUsuariosRol.FieldByName( 'US_CODIGO' ).AsInteger;
end;

procedure TdmEvaluacion.GetRolesUsuario(const iUsuario: Integer);
begin
     //cdsUsuarioRoles.Data := ServerEvaluacion.GetRolesUsuario( iUsuario );
end;

procedure TdmEvaluacion.cdsUsuariosRolAfterScroll(DataSet: TDataSet);
begin
     GetRolesUsuario( GetUsuarioActual );
end;

procedure TdmEvaluacion.cdsUsuariosRolAlAdquirirDatos(Sender: TObject);
begin
     cdsUsuariosRol.Data := ServerEvaluacion.GetUsuarios( dmCliente.GetGrupoActivo );
end;

procedure TdmEvaluacion.cdsUsuariosRolAlEnviarDatos(Sender: TObject);
var
   ErrorCount, iPosicion: Integer;
   Usuarios: OleVariant;
begin
     inherited;
     ErrorCount := 0;
     with cdsUsuariosRol do
     begin
          PostData;
          Usuarios := DeltaNull;
          if not VarIsNull( Usuarios ) then
          begin
               if ( Reconcile( ServerEvaluacion.GrabaUsuarios( GetUsuarioActual, Usuarios, ErrorCount ) ) ) then
               begin
                    iPosicion :=  FieldByName( 'US_CODIGO' ).AsInteger;
                    Refrescar;
                    Locate( 'US_CODIGO', iPosicion, [] );
               end;
          end;
     end;
end;

procedure TdmEvaluacion.cdsUsuariosRolAlModificar(Sender: TObject);
begin
     GetRolesUsuario( GetUsuarioActual );
     ZBaseEdicion_DevEx.ShowFormaEdicion( CatUsuariosEdit, TCatUsuariosEdit );
end;

procedure TdmEvaluacion.US_JEFEGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if ( Sender.AsInteger <= 0 ) then
             Text := VACIO
          else
             Text := Sender.AsString;
     end;
end;

procedure TdmEvaluacion.cdsUsuariosRolAlCrearCampos(Sender: TObject);
begin
     with cdsUsuariosRol do
     begin
          ListaFija( 'US_FORMATO', lfEmailType );
          FieldByName( 'US_JEFE' ).OnGetText := US_JEFEGetText;
     end;
end;

{ ***** Importar / Exportar Encuestas ***** }

function TdmEvaluacion.ExportaEncuesta( const iEncuesta: Integer ): WideString;
begin
     Result := ServerEvaluacion.ExportarEncuesta( dmCliente.Empresa, iEncuesta, dmCliente.Usuario );
end;

function TdmEvaluacion.ImportaEncuesta( const sArchivo: String ): Boolean;
var
   sChr: Char;
   oArchivo: TextFile;
   sXML: WideString;
begin
     Result := True;
     AssignFile( oArchivo, sArchivo );
     Reset( oArchivo );
     while not Eof( oArchivo )do
     begin
          Read( oArchivo , sChr );
          sXML := sXML + sChr;
     end;
     try
        try
           ServerEvaluacion.ImportarEncuesta( dmCliente.Empresa, sXML );
        except
           on Error: Exception do
           begin
                Result := False;
                Application.HandleException( Error );
                ZetaDialogo.ZError( 'Error', 'El archivo a importar no cuenta con el formato correcto', 0 );
           end;
        end;
        with TressShell do
        begin
             if Result then
             begin
                  SetDataChange( [ enEncuesta ] );
                  SetUltimaEncuesta;
             end;
        end;
     finally
            Close( oArchivo );
     end;
end;

function TdmEvaluacion.ParametrosRutaVirtual( const lEncuestaActiva: Boolean; var sRutaVirtual: WideString ): String;
const
     Q_PARAMETROS = 'Inicio.aspx?GUID=0&Empresa=%s&Encuesta=%d&Empleado=%d&Usuario=%d&Nombre=%s';
var
   sEmpresa: String;
   sNombre: WideString;
   iEmpleado, iEncuesta: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmCliente do
        begin
             sEmpresa := Empresa[ P_CODIGO ];
             if lEncuestaActiva then
                 iEncuesta := Encuesta
             else
             begin
                  with cdsEncuestas do
                  begin
                       Conectar;
                       iEncuesta := FieldbyName( 'ET_CODIGO' ).AsInteger;
                  end;
             end;
             ServerEvaluacion.GetParametrosURL( dmCliente.Empresa, sEmpresa, Usuario, sRutaVirtual, iEmpleado, sNombre );
             Result := Format( Q_PARAMETROS, [ sEmpresa, iEncuesta, iEmpleado, Usuario, sNombre  ] );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmEvaluacion.GlobalesCargar( Parametros: TZetaParams );
begin
     Parametros.VarValues := ServerEvaluacion.GetGlobales( dmCliente.Empresa );
end;

procedure TdmEvaluacion.GlobalesDescargar( Parametros: TZetaParams );
begin
     ServerEvaluacion.GrabaGlobales( dmCliente.Empresa, Parametros.VarValues );
end;

procedure TdmEvaluacion.cdsEvaluadoresLookupLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
     ZetaBusqueda_DevEx.ShowSearchForm( cdsEvaluadoresLookup, sFilter, sKey, sDescription, False, False );
end;

end.
