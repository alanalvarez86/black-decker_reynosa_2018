inherited WizCalcularResultados: TWizCalcularResultados
  Left = 420
  Top = 362
  Caption = 'Calcular resultados'
  ClientWidth = 443
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Width = 443
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Enabled = True
    end
    inherited Siguiente: TZetaWizardButton
      Enabled = False
    end
    inherited Salir: TZetaWizardButton
      Left = 358
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 275
      Enabled = True
    end
  end
  inherited PageControl: TPageControl
    Width = 443
    inherited Parametros: TTabSheet
      object GroupBox1: TGroupBox
        Left = 5
        Top = 64
        Width = 425
        Height = 105
        Caption = ' Datos de Encuesta: '
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N�mero:'
        end
        object Label2: TLabel
          Left = 16
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO: TZetaTextBox
          Left = 60
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE: TZetaTextBox
          Left = 60
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 435
        Lines.Strings = (
          
            'Se van a calcular resultados de acuerdo al valor activo seleccio' +
            'nado.'
          ''
          'Presione el bot�n '#39'Ejecutar'#39' para iniciar el proceso.')
      end
      inherited ProgressPanel: TPanel
        Width = 435
      end
    end
  end
end
