inherited WizCorreosInvitacion_DevEx: TWizCorreosInvitacion_DevEx
  Left = 306
  Top = 472
  Caption = 'Enviar invitaciones'
  ClientHeight = 480
  ClientWidth = 466
  ExplicitWidth = 472
  ExplicitHeight = 509
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 466
    Height = 480
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F40000000467414D410000B18F0BFC6105000000097048597300000EC300000E
      C301C76FA86400000349494441545847ED96CF4B545114C7877E4AB99008D228
      0B83E8D7A2A0226863E1A66851D0EC0AFAE1B2BF2067BCF3662ADFBBF7BDF981
      B3707031189533832024E4A2C07E402D0C443167C045410BDDB570E1C2859DEF
      9B77F5F9BC4F9F36B6A90B9FC5BBE79E73BEF79CE31D43FF977731C676B09111
      9B5CEEEB4E20BFDD489B9F5D22C4F3BD2C5BAA4FA5F20D147B9B93C67F65326F
      7677962A8B5B01043969FC176EA472AE0596D5BBCF49E3BFB652405A4F373B69
      FC57FA69FA80748816CB9F3AFACBAD2AD840E5A63B7810D05E278DFFE23CDB28
      1D228572810D8E3528294D36BB830721D00C24BB922D2AE75AC0F2F93A27CDF2
      122C734A682266C3442B4F648E498748B13C1E294CB583CE62E5E14621FF776E
      0186D17D9047F92D994F8F274F5745C4C47D5313F3D8C4A02C3915CB29FBC026
      972DC42500D5B562228F5C46DC7C601F32E366028FCF334D5C8090E4939E26E9
      4037F88541DC3C9559B7005497C74D4D67D645E4A68A3F0A919A45BAF9708AA5
      1AECCD44F688DBA996400072D085EB9C4A2CDA0200D7F834293A8303D1176397
      E9F6F3AA209B01B158DFE736C44E24CCC374E15199774980A999103147226EE3
      605772A0255AA8FC5005DC0888F1B8E743136262C829D7CC72CE15025C3873C1
      72AFF7444BE54155609BE254C2466523A2C56F43BA9EB35F3FF49B622F7873A9
      0510722E48C82E1AA89837387B39711DBF6EE170787BB47FFC9AD70E9F2CCBD6
      BBFBADC2570070CF052B4CB455035766E3BDEF4F62CF60C679AAD639DBDEF7F1
      B89CFA8E579357B1E7EDB78A350500CC05EFB4C23289AEF7554BEABC1DB01B31
      71177BB02D89D3AC2B645FD16F15EB0A9070C6EF203066C38CF36EAFDD8A0B01
      9B2BF9AA7EAB08268012223889683435634479A6CA5B9399FB2142302342DFEB
      8A584FC082C98C76FB56E8B7267E7AECABA09E7F27A167E143BE37D022D539C9
      5A02662CA65FAA06E2F7E87BDE63F7C59E1BA7655491135C33CAAA7340298002
      7C41B9FDFA1D143917F873A4CA0CA9CEAC12C063BC3760BF83B23C17F46BEBB5
      BB052CE0B5C2C1A0FD0E8A7B2EF0D4BBE7420A98C13B8D031BED7750DC7381C7
      8DBEA7B18FB28CEA4C3FF4A7FD0E8A9C0B3CF3947BD8FE6DAE61BF8362CF0584
      A02221A849B1AEA37F97EA3F40FFFA0A857E0358CFC7F7C5E707A40000000049
      454E44AE426082}
    ExplicitWidth = 466
    ExplicitHeight = 480
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Seleccionar la relaci'#243'n que los evaluadores deben cubrir para la' +
        ' generaci'#243'n de las invitaciones.'
      Header.Title = 'Configuraci'#243'n de la invitaci'#243'n'
      ParentFont = False
      ExplicitWidth = 444
      ExplicitHeight = 338
      object lblRelacion: TLabel
        Left = 11
        Top = 187
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Relaci'#243'n:'
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 52
        Width = 441
        Height = 105
        Caption = ' Datos de Encuesta: '
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label2: TLabel
          Left = 16
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO: TZetaTextBox
          Left = 60
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE: TZetaTextBox
          Left = 60
          Top = 64
          Width = 364
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object ENC_RELA: TZetaKeyCombo
        Left = 60
        Top = 184
        Width = 209
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 1
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object gbEvaluadores: TGroupBox
        Left = 0
        Top = 230
        Width = 441
        Height = 59
        Caption = ' Selecciona evaluadores: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object lblLista: TLabel
          Left = 31
          Top = 25
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = 'Lista:'
        end
        object btnLista: TcxButton
          Left = 405
          Top = 20
          Width = 21
          Height = 21
          Hint = 'Invocar B'#250'squeda de Empleados'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A4050000000000000000000000000000000000008B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CED2FFD0CCD0FF908791FF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC2BCC2FFFFFF
            FFFFFFFFFFFFD9D5D9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFB1ABB2FFFFFFFFFFFFFFFFFFF4F3F4FF988F99FF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFFA49DA5FFFAF9FAFFFFFFFFFFFAF9FAFFA39BA3FF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFAFA9B0FFDEDBDFFFF4F3F4FFFBFBFBFFE2DFE2FFF4F3F4FFFFFFFFFFFDFD
            FDFFAFA9B0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFFC5C0C6FFFFFFFFFFFFFFFFFFF8F7F8FFF2F1F2FFFFFF
            FFFFFFFFFFFFFFFFFFFFC0BAC0FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFFA39BA3FFFFFFFFFFF8F7F8FFBEB8
            BFFF8F8590FF8B818CFFA39BA3FFEDEBEDFFFFFFFFFFC2BCC2FF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CE
            D2FFFFFFFFFFBEB8BFFF8B818CFF8B818CFF8B818CFF8B818CFFA69FA7FFFFFF
            FFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFFE0DDE0FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B81
            8CFF8B818CFF908791FFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFC5C0
            C6FF8B818CFF8B818CFF8B818CFF8B818CFFAFA9B0FFFFFFFFFFEDEBEDFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFA39BA3FFFDFDFDFFFDFDFDFFBCB6BDFF948B95FF8D838EFFAFA9B0FFF4F3
            F4FFFFFFFFFFBCB6BDFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFFBAB4BBFFFFFFFFFFFFFFFFFFFDFD
            FDFFFAF9FAFFFFFFFFFFFFFFFFFFD0CCD0FF8D838EFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFA199A2FFD7D4D7FFEDEBEDFFEDEBEDFFD9D5D9FFAEA7AEFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnListaClick
        end
        object edLista: TZetaEdit
          Left = 62
          Top = 20
          Width = 341
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          LookUpBtn = btnLista
          ParentFont = False
          TabOrder = 0
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.AssignedValues = [wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 444
      ExplicitHeight = 338
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 444
        ExplicitHeight = 243
        Height = 243
        Width = 444
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 444
        Width = 444
        inherited Advertencia: TcxLabel
          Caption = 
            'Se enviaran los correos de invitaci'#243'n para los evaluadores selec' +
            'cionados en la encuesta activa.'
          Style.IsFontAssigned = True
          ExplicitLeft = 70
          ExplicitWidth = 372
          Width = 372
          AnchorY = 49
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      Header.AssignedValues = [wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 444
      ExplicitHeight = 338
      inherited sCondicionLBl: TLabel
        Left = 14
        Top = 165
        ExplicitLeft = 14
        ExplicitTop = 165
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        Top = 194
        ExplicitLeft = 39
        ExplicitTop = 194
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Top = 282
        Visible = True
        ExplicitLeft = 142
        ExplicitTop = 282
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        Top = 162
        ExplicitLeft = 66
        ExplicitTop = 162
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Top = 193
        Style.IsFontAssigned = True
        ExplicitLeft = 66
        ExplicitTop = 193
      end
      inherited GBRango: TGroupBox
        Left = 66
        Top = 32
        ExplicitLeft = 66
        ExplicitTop = 32
      end
      inherited bAjusteISR: TcxButton
        Left = 380
        Top = 193
        ExplicitLeft = 380
        ExplicitTop = 193
      end
    end
    object tsTipoCorreo: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvTitleFont]
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Configurar el estilo de correo electr'#243'nico a utilizar para el en' +
        'v'#237'o de las invitaciones a los evaluadores asignados.'
      Header.Title = 'Configuraci'#243'n del correo'
      ParentFont = False
      object rgEstiloCorreo: TcxRadioGroup
        Left = 2
        Top = 178
        Caption = ' Elegir estilo de correo: '
        Properties.Items = <
          item
            Caption = 'Plantilla de correo'
          end
          item
            Caption = 'Texto fijo'
          end>
        ItemIndex = 0
        TabOrder = 0
        Height = 105
        Width = 441
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 40
        Width = 441
        Height = 105
        Caption = ' Datos de Encuesta: '
        TabOrder = 1
        object Label3: TLabel
          Left = 16
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label4: TLabel
          Left = 16
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO2: TZetaTextBox
          Left = 60
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE2: TZetaTextBox
          Left = 60
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
    end
    object tsTipoTexto: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvTitleFont]
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Configurar el tema y el cuerpo del correo electr'#243'nico de las inv' +
        'itaciones a los evaluadores asignados.'
      Header.Title = 'Configuraci'#243'n del texto del correo'
      ParentFont = False
      object pnDatos: TPanel
        Left = 0
        Top = 0
        Width = 444
        Height = 97
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object lblTemaTexto: TLabel
          Left = 0
          Top = 9
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tema:'
        end
        object edTemaTexto: TEdit
          Left = 36
          Top = 5
          Width = 408
          Height = 21
          MaxLength = 255
          TabOrder = 0
        end
        object rgFormatoCorreo: TcxRadioGroup
          Left = 0
          Top = 32
          Caption = ' Formato del correo: '
          Properties.Items = <
            item
              Caption = 'Texto'
            end
            item
              Caption = 'HTML'
            end>
          ItemIndex = 0
          TabOrder = 1
          Height = 57
          Width = 444
        end
      end
      object gbMensaje: TGroupBox
        Left = 0
        Top = 97
        Width = 444
        Height = 241
        Align = alClient
        Caption = ' Mensaje: '
        Padding.Left = 8
        Padding.Top = 5
        Padding.Right = 5
        Padding.Bottom = 8
        TabOrder = 1
        object Mensaje: TMemo
          Left = 10
          Top = 20
          Width = 389
          Height = 211
          Align = alClient
          ScrollBars = ssVertical
          TabOrder = 0
        end
        object pnBoton: TPanel
          Left = 399
          Top = 20
          Width = 38
          Height = 211
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object btnArchivo: TcxButton
            Left = 9
            Top = 0
            Width = 26
            Height = 26
            Hint = 'Buscar archivo'
            OptionsImage.Glyph.Data = {
              36090000424D3609000000000000360000002800000018000000180000000100
              200000000000000900000000000000000000000000000000000000B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF34C2EDFF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9
              F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9
              F4FF80D9F4FF54CBF0FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF8FDDF5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCBEFFBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FFB7E9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFF3FBFEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FFEBF9FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF20BCECFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF14B8EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF4CC9F0FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF48C8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF70D4F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF97E0F6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFC7EEFAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FFCFF1FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFEFFAFEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF38C3EEFF40C5EFFF40C5EFFF40C5EFFF40C5EFFF40C5EFFF40C5
              EFFF40C5EFFF40C5EFFF40C5EFFF40C5EFFF40C5EFFF40C5EFFF40C5EFFF40C5
              EFFF40C5EFFF40C5EFFF40C5EFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF3CC4EEFF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
              F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
              F7FF9FE2F7FF64D0F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF9FE2F7FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFDBF4FCFF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
              F7FF9FE2F7FF64D0F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFF9FE2F7FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF24BDECFF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
              F1FF3CC4EEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
            OptionsImage.Margin = 1
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btnArchivoClick
          end
        end
      end
    end
    object tsTipoPlantilla: TdxWizardControlPage
      Header.AssignedValues = [wchvTitleFont]
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Configurar el tema y la plantilla a utilizar en el correo electr' +
        #243'nico de las invitaciones a los evaluadores asignados'
      Header.Title = 'Configuraci'#243'n de la plantilla'
      object lblTema: TLabel
        Left = 29
        Top = 201
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tema:'
      end
      object lblPlantilla: TLabel
        Left = 20
        Top = 236
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Plantilla:'
      end
      object btnDialogo: TcxButton
        Left = 400
        Top = 233
        Width = 21
        Height = 21
        Hint = 'Buscar archivo'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A40500000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
          FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
          F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
          F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
          F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
          F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
          FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = btnDialogoClick
      end
      object GroupBox3: TGroupBox
        Left = 3
        Top = 67
        Width = 438
        Height = 105
        Caption = ' Datos de Encuesta: '
        TabOrder = 0
        object Label5: TLabel
          Left = 16
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label6: TLabel
          Left = 16
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO3: TZetaTextBox
          Left = 60
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE3: TZetaTextBox
          Left = 60
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object edTema: TEdit
        Left = 65
        Top = 196
        Width = 356
        Height = 21
        MaxLength = 255
        TabOrder = 1
      end
      object edPlantilla: TEdit
        Left = 64
        Top = 233
        Width = 330
        Height = 21
        MaxLength = 255
        TabOrder = 2
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 32
    Top = 434
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Archivos XSL|*.xsl|Todos|*.*'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 60
    Top = 432
  end
  object OpenDialog2: TOpenDialog
    Filter = 'Archivo Texto|*.txt|Archivo HTML|*.html|Todos|*.*'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 100
    Top = 432
  end
end
