unit FWizAgregaEmpEvaluar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseWizardFiltro, ComCtrls, ZetaKeyLookup, ZetaEdit, StdCtrls, Buttons,
  ZetaWizard, ExtCtrls, ZetaDBTextBox, ZetaKeyCombo;

type
  TWizAgregaEmpEvaluar = class(TBaseWizardFiltro)
    Label1: TLabel;
    ztbEncuesta: TZetaTextBox;
    Label2: TLabel;
    US_CODIGO: TZetaKeyLookup;
    Label3: TLabel;
    zkcRelacion: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizAgregaEmpEvaluar: TWizAgregaEmpEvaluar;

implementation

{$R *.DFM}

uses DEvaluacion,
     DSistema,
     DCliente,
     DProcesos,
     ZHelpContext,
     ZetaCommonTools,
     ZetaCommonLists,
     ZBaseSelectGrid,
     FEvalAgregaempEvaluadorGridSelect;

procedure TWizAgregaEmpEvaluar.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := US_CODIGO;
     HelpContext := H_DISENO_ENCUESTA_PROCESOS_GENERAR_LISTA_EMPLEADOS_EVALUAR;
     dmSistema.cdsUsuarios.LookupName := 'evaluador';
     US_CODIGO.LookupDataSet := dmSistema.cdsUsuarios;
end;

procedure TWizAgregaEmpEvaluar.FormShow(Sender: TObject);
{
var
   i: Integer;
}
begin
     inherited;
     with dmEvaluacion do
     begin
          with zkcRelacion do
          begin
               LlenaPerfilEvaluadores( Lista, [ tevMismo ] );
               {
               Items.BeginUpdate;
               try
                  i := 0;
                  while i < ( Items.Count ) do
                  begin
                       if( strtoInt( zkcRelacion.Llaves[ i ] ) = Ord( tevMismo ) )then
                           Items.Delete( i );
                       i := i + 1;
                  end;
               finally
                      Items.EndUpdate;
               end;
               }
               if ( Items.Count > 0 ) then
                  zkcRelacion.ItemIndex := 0;
          end;
     end;
     dmSistema.cdsUsuarios.Conectar;
     ztbEncuesta.Caption := InttoStr( dmCliente.Encuesta ) + ' = ' + dmCliente.Nombre;
end;

procedure TWizAgregaEmpEvaluar.CargaParametros;
begin
     with Descripciones do
     begin
          AddString( 'Encuesta', ztbEncuesta.Caption );
          AddString( 'Evaluador', GetDescripLlave( US_CODIGO ) );
          AddString('Relacion', zkcRelacion.Descripcion );
     end;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'Encuesta', dmCliente.Encuesta );
          AddInteger( 'Evaluador', US_CODIGO.Valor );
          AddInteger( 'Relacion', strtoInt( zkcRelacion.Llaves[ zkcRelacion.ItemIndex ] ) );
     end;
end;

procedure TWizAgregaEmpEvaluar.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( PageControl.ActivePage = Parametros ) then
          begin
               if StrVacio( US_CODIGO.Llave ) then
                  CanMove := Error( '� No se ha seleccionado evaluador !', US_CODIGO )
               else if( dmEvaluacion.cdsPerfilEvaluador.RecordCount <= 0 )then
                    CanMove := Error( '� No se tienen registrados perfiles de evaluadores !', zkcRelacion )
               else if( strVacio( zkcRelacion.Descripcion ) )then
                    CanMove := Error( '� Es necesario seleccionar un perfil de evaluador !', zkcRelacion )
          end;
     end;
end;

procedure TWizAgregaEmpEvaluar.CargaListaVerificacion;
begin
     dmProcesos.AgregaEmpEvaluarGlobalGetLista( ParameterList );
end;

function TWizAgregaEmpEvaluar.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid.GridSelect( dmProcesos.cdsDataset, TEvalAgregaEmpEvaluadorGridSelect );
end;

function TWizAgregaEmpEvaluar.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AgregaEmpEvaluarGlobal( ParameterList, Verificacion );
end;

end.
