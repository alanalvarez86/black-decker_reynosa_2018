unit FCorreosInvitacionGridSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid, Db, StdCtrls, Grids, DBGrids, ZetaDBGrid, Buttons,
  ExtCtrls;

type
  TCorreosInvitacionGridSelect = class(TBaseGridSelect)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CorreosInvitacionGridSelect: TCorreosInvitacionGridSelect;

implementation

{$R *.DFM}

end.
