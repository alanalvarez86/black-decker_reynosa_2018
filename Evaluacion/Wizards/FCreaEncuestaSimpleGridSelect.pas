unit FCreaEncuestaSimpleGridSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid, Db, StdCtrls, Grids, DBGrids, ZetaDBGrid, Buttons,
  ExtCtrls;

type
  TCreaEncuestaSimpleGridSelect = class(TBaseGridSelect)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CreaEncuestaSimpleGridSelect: TCreaEncuestaSimpleGridSelect;

implementation

{$R *.DFM}

end.
