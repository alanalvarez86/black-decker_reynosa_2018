inherited WizCorreosNotificacion_DevEx: TWizCorreosNotificacion_DevEx
  Left = 427
  Top = 383
  Caption = 'Enviar notificaciones'
  ClientHeight = 486
  ClientWidth = 466
  ExplicitWidth = 472
  ExplicitHeight = 514
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 466
    Height = 486
    Header.AssignedValues = [wchvDescriptionFont, wchvGlyph, wchvTitleFont]
    Header.DescriptionFont.Style = [fsBold]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC100000EC101B891
      6BED000001EE49444154584763F8FFFFFF806286BA55373603F17F7AE0DA5537
      2E63730056C5B4C2A30E1875006107ACBED95FBBF27A03E5F8C67B0CB38198A0
      03EA975F5760A0020066B9FBE86683F0E077C0804701ADF11070C0CA1BAB8178
      3EA91818E49F31CCC282093A80DC44882BD1A1E3C1EF00600A2EA959753D8154
      5CBBEAFA6B74B3B061820EA005AE5F735E0E480B03313734C010005921ADF0C4
      737142932FF94F6DD9B68A196A2D02A02B0646C163507C528C57DEF80D33B37D
      CF6C95C99702FEF79FCC7E34F16220AA23902D0761521221280E275D0A60987C
      D11F03D7AEBA064F946D3B97E9801C00C2932E065C9C72DE9F136A04E50E005A
      C638E58A2FEBA48BFE2AC878C2B928F5FED3495A7DA73274265D8CF0863900E2
      08FF17132F048A800DC170C0AA1B362047108B1B569E57EA395C6D8A6C0191F8
      3BC8A1544884D7FF837C8BC502A230850EB80EC63D472B4DB0194E0CA68A037A
      4F14E862339C00FE3EE97C802215A2E0C6FF8675A745FA4F6789F49D2C1005E5
      79100DE24FBEEC2FD0BA7D8DD1C40B41466896DF9C7021800F6B22A405EE3ED2
      2009B31C980DF74CBA14C406B61C048085C6656C9AA8897B8ED64A402C0F9E3F
      E54A2813D46A0840AE1848C5C40250544CBAE45F07B49C112A8400D80C261613
      0B7ACEF830D5EFB787F2D0003683E987FF330000CF4AF27A9DF5A3AC00000000
      49454E44AE426082}
    Header.TitleFont.Height = 12
    Header.TitleFont.Style = [fsBold]
    ParentFont = False
    ExplicitWidth = 466
    ExplicitHeight = 486
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvGlyph, wchvTitleFont]
      Header.Glyph.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
        6000003A980000176F925FC546000000097048597300000EC100000EC101B891
        6BED000001EE49444154584763F8FFFFFF806286BA55373603F17F7AE0DA5537
        2E63730056C5B4C2A30E1875006107ACBED95FBBF27A03E5F8C67B0CB38198A0
        03EA975F5760A0020066B9FBE86683F0E077C0804701ADF11070C0CA1BAB8178
        3EA91818E49F31CCC282093A80DC44882BD1A1E3C1EF00600A2EA959753D8154
        5CBBEAFA6B74B3B061820EA005AE5F735E0E480B03313734C010005921ADF0C4
        737142932FF94F6DD9B68A196A2D02A02B0646C163507C528C57DEF80D33B37D
        CF6C95C99702FEF79FCC7E34F16220AA23902D0761521221280E275D0A60987C
        D11F03D7AEBA064F946D3B97E9801C00C2932E065C9C72DE9F136A04E50E005A
        C638E58A2FEBA48BFE2AC878C2B928F5FED3495A7DA73274265D8CF0863900E2
        08FF17132F048A800DC170C0AA1B362047108B1B569E57EA395C6D8A6C0191F8
        3BC8A1544884D7FF837C8BC502A230850EB80EC63D472B4DB0194E0CA68A037A
        4F14E862339C00FE3EE97C802215A2E0C6FF8675A745FA4F6789F49D2C1005E5
        79100DE24FBEEC2FD0BA7D8DD1C40B41466896DF9C7021800F6B22A405EE3ED2
        2009B31C980DF74CBA14C406B61C048085C6656C9AA8897B8ED64A402C0F9E3F
        E54A2813D46A0840AE1848C5C40250544CBAE45F07B49C112A8400D80C261613
        0B7ACEF830D5EFB787F2D0003683E987FF330000CF4AF27A9DF5A3AC00000000
        49454E44AE426082}
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Seleccionar la relaci'#243'n que los evaluadores deben cubrir para la' +
        ' generaci'#243'n de las notificaciones.'
      Header.Title = 'Configuraci'#243'n de la notificaci'#243'n.'
      ParentFont = False
      ExplicitWidth = 444
      ExplicitHeight = 346
      object GroupBox1: TcxGroupBox
        Left = 2
        Top = 30
        Align = alCustom
        Caption = ' Datos de Encuesta '
        TabOrder = 0
        Height = 105
        Width = 440
        object Label1: TLabel
          Left = 22
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label2: TLabel
          Left = 22
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO: TZetaTextBox
          Left = 63
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE: TZetaTextBox
          Left = 63
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object gbEvaluadores: TcxGroupBox
        Left = 1
        Top = 146
        Align = alCustom
        Caption = ' Selecciona evaluadores '
        TabOrder = 1
        Height = 184
        Width = 440
        object lblLista: TLabel
          Left = 37
          Top = 123
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = 'Lista:'
        end
        object ENC_RELAlbl: TLabel
          Left = 17
          Top = 48
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Relaci'#243'n:'
        end
        object lblStatus: TLabel
          Left = 29
          Top = 73
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Status:'
        end
        object btnLista: TcxButton
          Left = 395
          Top = 120
          Width = 21
          Height = 21
          Hint = 'Invocar b'#250'squeda de empleados'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A4050000000000000000000000000000000000008B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CED2FFD0CCD0FF908791FF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC2BCC2FFFFFF
            FFFFFFFFFFFFD9D5D9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFB1ABB2FFFFFFFFFFFFFFFFFFF4F3F4FF988F99FF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFFA49DA5FFFAF9FAFFFFFFFFFFFAF9FAFFA39BA3FF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFAFA9B0FFDEDBDFFFF4F3F4FFFBFBFBFFE2DFE2FFF4F3F4FFFFFFFFFFFDFD
            FDFFAFA9B0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFFC5C0C6FFFFFFFFFFFFFFFFFFF8F7F8FFF2F1F2FFFFFF
            FFFFFFFFFFFFFFFFFFFFC0BAC0FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFFA39BA3FFFFFFFFFFF8F7F8FFBEB8
            BFFF8F8590FF8B818CFFA39BA3FFEDEBEDFFFFFFFFFFC2BCC2FF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CE
            D2FFFFFFFFFFBEB8BFFF8B818CFF8B818CFF8B818CFF8B818CFFA69FA7FFFFFF
            FFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFFE0DDE0FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B81
            8CFF8B818CFF908791FFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFC5C0
            C6FF8B818CFF8B818CFF8B818CFF8B818CFFAFA9B0FFFFFFFFFFEDEBEDFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFA39BA3FFFDFDFDFFFDFDFDFFBCB6BDFF948B95FF8D838EFFAFA9B0FFF4F3
            F4FFFFFFFFFFBCB6BDFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFFBAB4BBFFFFFFFFFFFFFFFFFFFDFD
            FDFFFAF9FAFFFFFFFFFFFFFFFFFFD0CCD0FF8D838EFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFA199A2FFD7D4D7FFEDEBEDFFEDEBEDFFD9D5D9FFAEA7AEFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = btnListaClick
        end
        object ENC_RELA: TZetaKeyCombo
          Left = 64
          Top = 44
          Width = 209
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 2
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object lbStatus: TZetaKeyCombo
          Left = 64
          Top = 69
          Width = 209
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object PorRelacionStatus: TcxRadioButton
          Left = 19
          Top = 21
          Width = 150
          Height = 17
          Caption = 'Por Relaci'#243'n y Status'
          Checked = True
          Color = clBtnHighlight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          TabStop = True
          OnClick = PorRelacionStatusClick
          ParentBackground = False
        end
        object PorLista: TcxRadioButton
          Left = 20
          Top = 96
          Width = 113
          Height = 17
          Caption = 'Por Lista'
          Color = clBtnHighlight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          OnClick = PorListaClick
        end
        object edLista: TZetaEdit
          Left = 64
          Top = 120
          Width = 329
          Height = 21
          LookUpBtn = btnLista
          TabOrder = 4
        end
      end
    end
    object tsTipoCorreo: TdxWizardControlPage [1]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Configurar el estilo de correo electr'#243'nico a utilizar para el en' +
        'v'#237'o de las notificaciones a los evaluadores asignados.'
      Header.Title = 'Configuraci'#243'n del correo.'
      ParentFont = False
      object GroupBox2: TcxGroupBox
        Left = 3
        Top = 30
        Align = alCustom
        Caption = ' Datos de Encuesta '
        TabOrder = 0
        Height = 105
        Width = 440
        object Label3: TLabel
          Left = 22
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label4: TLabel
          Left = 22
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO2: TZetaTextBox
          Left = 64
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE2: TZetaTextBox
          Left = 64
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object rgEstiloCorreo: TcxRadioGroup
        Left = 3
        Top = 163
        Align = alCustom
        Caption = ' Elegir estilo de correo '
        Properties.Items = <
          item
            Caption = 'Plantilla de correo'
          end
          item
            Caption = 'Texto fijo'
          end>
        ItemIndex = 0
        TabOrder = 1
        Height = 105
        Width = 440
      end
    end
    object tsTipoPlantilla: TdxWizardControlPage [2]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Configurar el tema y la plantilla a utilizar en el correo electr' +
        #243'nico de las notificaciones a los evaluadores asignados.'
      Header.Title = 'Configuraci'#243'n de la plantilla.'
      PageVisible = False
      ParentFont = False
      object lblTema: TLabel
        Left = 35
        Top = 200
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tema:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblPlantilla: TLabel
        Left = 26
        Top = 237
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Plantilla:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object btnDialogo: TcxButton
        Left = 393
        Top = 234
        Width = 21
        Height = 21
        Hint = 'Buscar archivo'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A40500000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFDDF3
          FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3
          FAFFDDF3FAFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF10B7EAFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF3FC4EEFF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF3FC4EEFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF6AD1
          F0FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6AD1F0FFFCFC
          FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFFFCFCFCFF92DDF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF96DFF5FFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFBDEAF7FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
          FCFFE8F6FAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF53CA
          EFFF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCE
          F0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDDF3FAFFFCFC
          FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FFDDF3FAFFFCFCFCFFFCFCFCFFFCFCFCFFBDEAF7FF7ED7F2FF7ED7
          F2FF7ED7F2FF7ED7F2FF7ED7F2FF7ED7F2FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC1EBF8FFDDF3FAFFDDF3FAFFDDF3
          FAFF6ED2F1FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnClick = btnDialogoClick
      end
      object GroupBox3: TcxGroupBox
        Left = 3
        Top = 67
        Align = alCustom
        Caption = ' Datos de Encuesta  '
        TabOrder = 3
        Height = 105
        Width = 440
        object Label5: TLabel
          Left = 22
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label6: TLabel
          Left = 22
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO3: TZetaTextBox
          Left = 64
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE3: TZetaTextBox
          Left = 64
          Top = 64
          Width = 347
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object edTema: TEdit
        Left = 67
        Top = 196
        Width = 347
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        TabOrder = 0
      end
      object edPlantilla: TEdit
        Left = 67
        Top = 234
        Width = 321
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        TabOrder = 1
      end
    end
    object tsTipoTexto: TdxWizardControlPage [3]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Configurar el tema y el cuerpo del correo electr'#243'nico de las not' +
        'ificaciones a los evaluadores asignados.'
      Header.Title = 'Configuraci'#243'n del texto.'
      PageVisible = False
      ParentFont = False
      object pnDatos: TPanel
        Left = 0
        Top = 0
        Width = 444
        Height = 97
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object lblTemaTexto: TLabel
          Left = 5
          Top = 12
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tema:'
        end
        object edTemaTexto: TEdit
          Left = 39
          Top = 8
          Width = 404
          Height = 21
          MaxLength = 255
          TabOrder = 0
        end
        object rgFormatoCorreo: TcxRadioGroup
          Left = 0
          Top = 40
          Align = alBottom
          Caption = ' Formato del correo  '
          Properties.Items = <
            item
              Caption = 'Texto'
            end
            item
              Caption = 'HTML'
            end>
          ItemIndex = 0
          TabOrder = 1
          Height = 57
          Width = 444
        end
      end
      object gbMensaje: TcxGroupBox
        Left = 0
        Top = 97
        Align = alClient
        Caption = ' Mensaje '
        TabOrder = 1
        Height = 249
        Width = 444
        object Mensaje: TMemo
          Left = 12
          Top = 17
          Width = 384
          Height = 212
          Align = alCustom
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 0
        end
        object pnBoton: TPanel
          Left = 399
          Top = 16
          Width = 38
          Height = 197
          Align = alCustom
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object btnArchivo: TcxButton
            Left = 8
            Top = 2
            Width = 26
            Height = 26
            Hint = 'Buscar archivo'
            OptionsImage.Glyph.Data = {
              36090000424D3609000000000000360000002800000018000000180000000100
              200000000000000900000000000000000000000000000000000000B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF34C2EDFF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9
              F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9
              F4FF80D9F4FF54CBF0FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF8FDDF5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCBEFFBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FFB7E9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFF3FBFEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FFEBF9FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF20BCECFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF14B8EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF4CC9F0FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF48C8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF70D4F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF97E0F6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFC7EEFAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FFCFF1FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFEFFAFEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF38C3EEFF40C5EFFF40C5EFFF40C5EFFF40C5EFFF40C5EFFF40C5
              EFFF40C5EFFF40C5EFFF40C5EFFF40C5EFFF40C5EFFF40C5EFFF40C5EFFF40C5
              EFFF40C5EFFF40C5EFFF40C5EFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF3CC4EEFF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
              F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
              F7FF9FE2F7FF64D0F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF9FE2F7FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFDBF4FCFF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
              F7FF9FE2F7FF64D0F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFF9FE2F7FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF24BDECFF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
              F1FF3CC4EEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            OnClick = btnArchivoClick
          end
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 444
      ExplicitHeight = 346
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 444
        ExplicitHeight = 251
        Height = 251
        Width = 444
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 444
        Width = 444
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitWidth = 376
          Width = 376
          AnchorY = 57
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 144
    Top = 444
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Archivos XSL|*.xsl|Todos|*.*'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 372
    Top = 48
  end
  object OpenDialog2: TOpenDialog
    Filter = 'Archivo Texto|*.txt|Archivo HTML|*.html|Todos|*.*'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 404
    Top = 48
  end
end
