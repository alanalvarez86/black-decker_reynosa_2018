inherited WizCalcularResultados_DevEx: TWizCalcularResultados_DevEx
  Left = 41
  Top = 362
  Caption = 'Calcular resultados'
  ClientHeight = 297
  ClientWidth = 470
  ExplicitWidth = 476
  ExplicitHeight = 326
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 470
    Height = 297
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F40000000467414D410000B18F0BFC6105000000097048597300000EC300000E
      C301C76FA864000002E3494441545847BD56BF8B135110B690435444C542315A
      1D2876FE07112C142CB438B8DD1763440B0B0FB5B1125E923B48B26F73FBC388
      88C81516FE05A270871616828272368767631741F00AF5A2E676D799CD24E636
      2FBBB9DD8D1F7C30D9BC996FDEBC793FB66D151AD74EEB6561E825F1DE27DABC
      768EFE1E1F38E7DBF5B27E1F443D298BB5270DDED84DC3D3852F5E128B03A201
      8A92786B70632FB9A507C16BD764825296F559724B0730FB1D10B83920349C2D
      E89383E49E1C828BEB1291508AB2A8907B72D48B6241261246E88567E49E1CD8
      5832910836C93D39B492F65D2210C9D4760304DB4A03F688CD4B2192012AF05A
      2610C1F4964014C56389401417C93D39604F9F910884127C72E49E0EB4526D45
      263484CDD4D6BF0B388CB210B81D1092735C37E328F7019C19451A3E1EC0ECCE
      C3727C0D0AC3B735A8D2340D1B2FAABC9A81ADF9A94F7C05BFD1DFFF07B01C77
      BA09A04D9F37C19D67871C4B59F26CE6C9E8D8CA1BD760C768F868F01F265C2B
      F42F03DAB0ED6E065F4328E058EAB26BAA5764C4E4806B343C1C3AD70FD0AC87
      1ECBD8077A59BB0B638FA30FCED2B5D8093F80046E23378963E8A71C78A1D003
      B415140C23546AA79F4048892313C0D30C82C5BA888410BB30F886C59EB72DF5
      9467B06C908EAD3E942680CF29E8F257B2C0A3D2E2D61E0CEED8EC9763B217BE
      6DA91F911D1BD75F6D0D2480EBD7BFC5E2B23E573F8CC1D7CD4BB717E05846FB
      5DF5C659CF9C3A8AB6C7A7265C9B299B124071708E55F2207B158059421556D1
      FE63E6BE3916FB42DFB11AFF2AD029FBE0091797954A651F066FDBECC16A7526
      837653BF5AF0FB016CD81D195886D95E02D4E9D2607108BB6002836F58EACB1F
      66FE32DABF8D8B6657D4B5953C54E3A99F409DD727C169B49B6E4442CCFD18DC
      31950FD807BE6DAB8FBA9D0F1598812558EE2410E3D91DC5EE3604917B4B73B7
      FC86FC69E62FB8D6F449B4D7F5C2114CC24F605CC0464381306223D2F0F481C7
      30ACF967993012EF01CF60D9BF209A51251EABE3360000000049454E44AE4260
      82}
    ExplicitWidth = 470
    ExplicitHeight = 297
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso para el c'#225'lculo de los resultados obtenidos de la encues' +
        'ta seleccionada.'
      Header.Title = 'Calculo de resultados'
      ExplicitWidth = 448
      ExplicitHeight = 155
      object GroupBox1: TGroupBox
        Left = 11
        Top = 25
        Width = 426
        Height = 105
        Caption = ' Datos de Encuesta: '
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label2: TLabel
          Left = 16
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO: TZetaTextBox
          Left = 60
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE: TZetaTextBox
          Left = 60
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 448
      ExplicitHeight = 155
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 448
        ExplicitHeight = 60
        Height = 60
        Width = 448
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 448
        Width = 448
        inherited Advertencia: TcxLabel
          Caption = 
            'Se calcular'#225' el resultado final de la encuesta en base la inform' +
            'aci'#243'n obtenida por los evaluadores'
          Style.IsFontAssigned = True
          ExplicitLeft = 70
          ExplicitWidth = 376
          Width = 376
          AnchorY = 49
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
