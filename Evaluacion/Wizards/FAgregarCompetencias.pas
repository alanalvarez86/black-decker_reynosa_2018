unit FAgregarCompetencias;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, Db,
     DEvaluacion,
     ZBaseDlgModal,ZBaseDlgModal_DevEx,
     ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, Vcl.ImgList, cxButtons, cxControls, ZetaSmartLists_DevEx,
  cxContainer, cxEdit, cxListBox;

type
  TAgregarCompetencias = class(TZetaDlgModal_Devex)
    Label20: TLabel;
    Label11: TLabel;
    zmlDestino: TZetaSmartListBox_DevEx;
    ZetaSmartListsButton4: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton3: TZetaSmartListsButton_DevEx;
    ZetaSmartLists: TZetaSmartLists_DevEx;
    ZetaSmartListsButton2: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton1: TZetaSmartListsButton_DevEx;
    zmlFuente: TZetaSmartListBox_DevEx;
    procedure FormShow(Sender: TObject);
    procedure zmlFuenteDblClick(Sender: TObject);
    procedure zmlDestinoDblClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FCriterios: TCriteriosEval;
    function HayCompetenciasRepetidas: Boolean;
  public
    { Public declarations }
  end;

function ShowAgregarCompetencias: Boolean;

var
  AgregarCompetencias: TAgregarCompetencias;

implementation

{$R *.DFM}

uses ZetaCommonClasses,
     ZetaDialogo,
     ZHelpContext;

function ShowAgregarCompetencias: Boolean;
var
   oAgregarCompetencias: TAgregarCompetencias;
begin
     oAgregarCompetencias := TAgregarCompetencias.Create( Application );
     try
        with oAgregarCompetencias do
        begin
             ShowModal;
             Result := ( ModalResult = mrOk );
        end;
     finally
            FreeAndNil( oAgregarCompetencias );
     end;
end;

{ ******** TAgregarCompetencias ************ }

procedure TAgregarCompetencias.FormCreate(Sender: TObject);
begin
     inherited;
     FCriterios := TCriteriosEval.Create;
     HelpContext := H_DISENO_ENCUESTA_CRITERIOS_EVALUAR_AGREGAR_COMPETENCIAS;
end;

procedure TAgregarCompetencias.FormShow(Sender: TObject);
var
   i: Integer;
   Elemento: TCriterioEval;
begin
     inherited;
     dmEvaluacion.LlenaCriterios( FCriterios );
     with zmlFuente do
     begin
          with Items do
          begin
               Clear;
               BeginUpdate;
               try
                  for i := 0 to ( FCriterios.Count - 1 ) do
                  begin
                       Elemento := FCriterios.Criterio[ i ];
                       AddObject( Elemento.Descripcion, Elemento );
                  end;
               finally
                      EndUpdate;
               end;
          end;
          ItemIndex := 0;
     end;
end;

procedure TAgregarCompetencias.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FCriterios );
end;

function TAgregarCompetencias.HayCompetenciasRepetidas: Boolean;
var
   i: Integer;
begin
     Result := False;
     with zmlDestino.Items do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if dmEvaluacion.cdsCriteriosEval.Locate( 'CM_CODIGO', TCriterioEval( Objects[ i ] ).Codigo, [] ) then
               begin
                    Result := True;
                    Break;
               end;
          end;
     end;
end;

procedure TAgregarCompetencias.zmlFuenteDblClick(Sender: TObject);
begin
     inherited;
     ZetaSmartLists.Escoger;
end;

procedure TAgregarCompetencias.zmlDestinoDblClick(Sender: TObject);
begin
     inherited;
     ZetaSmartLists.Rechazar;
end;

procedure TAgregarCompetencias.OKClick(Sender: TObject);
const
     K_WARNING = 'Ya se encuentran listadas algunas de las%0:scompetencias que se quieren agregar%0:s%0:s� Desea continuar ?';
var
   oCursor: TCursor;
begin
     inherited;
     if not HayCompetenciasRepetidas or ZetaDialogo.ZConfirm( Self.Caption, Format( K_WARNING, [ CR_LF ] ), 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with dmEvaluacion do
             begin
                  CreaServerAgregaCriterios;
                  LlenaDataSetCriterios( zmlDestino.Items );
                  AgregaCompetencias;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
          Close;
          ModalResult := mrOk;
     end
     else
         ModalResult := mrNone;
end;

end.
