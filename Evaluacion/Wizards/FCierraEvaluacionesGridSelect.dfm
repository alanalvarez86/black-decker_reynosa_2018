inherited CierraEvaluacionesGridSelect: TCierraEvaluacionesGridSelect
  Left = 353
  Top = 394
  Caption = 'Encuestas/Evaluaciones'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ZetaDBGrid: TZetaDBGrid
    Columns = <
      item
        Expanded = False
        FieldName = 'ET_CODIGO'
        PickList.Strings = ()
        Title.Caption = 'Encuesta'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ET_NOMBRE'
        PickList.Strings = ()
        Title.Caption = 'Nombre'
        Width = 380
        Visible = True
      end>
  end
  inherited PanelSuperior: TPanel
    inherited PistaLBL: TLabel
      Left = 8
      Width = 81
      Caption = '&Busca Encuesta:'
    end
  end
end
