inherited WizCorreosNotificacion: TWizCorreosNotificacion
  Left = 427
  Top = 383
  Caption = 'Enviar notificaciones'
  ClientHeight = 304
  ClientWidth = 446
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 268
    Width = 446
    BeforeMove = WizardBeforeMove
    inherited Salir: TZetaWizardButton
      Left = 349
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 266
    end
  end
  inherited PageControl: TPageControl
    Width = 446
    Height = 268
    inherited Parametros: TTabSheet
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 438
        Height = 105
        Align = alTop
        Caption = ' Datos de Encuesta '
        TabOrder = 0
        object Label1: TLabel
          Left = 22
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label2: TLabel
          Left = 22
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO: TZetaTextBox
          Left = 64
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE: TZetaTextBox
          Left = 64
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object gbEvaluadores: TGroupBox
        Left = 0
        Top = 105
        Width = 438
        Height = 152
        Align = alTop
        Caption = ' Selecciona evaluadores '
        TabOrder = 1
        object lblLista: TLabel
          Left = 37
          Top = 123
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = 'Lista:'
        end
        object btnLista: TSpeedButton
          Left = 395
          Top = 117
          Width = 25
          Height = 25
          Hint = 'Invocar B'#250'squeda de Empleados'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33033333333333333F7F3333333333333000333333333333F777333333333333
            000333333333333F777333333333333000333333333333F77733333333333300
            033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
            33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
            3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
            33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
            333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
            333333773FF77333333333370007333333333333777333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btnListaClick
        end
        object ENC_RELAlbl: TLabel
          Left = 17
          Top = 48
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Relaci'#243'n:'
        end
        object lblStatus: TLabel
          Left = 29
          Top = 73
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Status:'
        end
        object edLista: TZetaEdit
          Left = 64
          Top = 119
          Width = 329
          Height = 21
          LookUpBtn = btnLista
          TabOrder = 2
        end
        object ENC_RELA: TZetaKeyCombo
          Left = 64
          Top = 44
          Width = 209
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
        object lbStatus: TZetaKeyCombo
          Left = 64
          Top = 69
          Width = 209
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 1
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
        object PorRelacionStatus: TRadioButton
          Left = 19
          Top = 21
          Width = 150
          Height = 17
          Caption = 'Por Relaci'#243'n y Status'
          Checked = True
          TabOrder = 3
          TabStop = True
          OnClick = PorRelacionStatusClick
        end
        object PorLista: TRadioButton
          Left = 20
          Top = 96
          Width = 113
          Height = 17
          Caption = 'Por Lista'
          TabOrder = 4
          OnClick = PorListaClick
        end
      end
    end
    object tsTipoCorreo: TTabSheet [1]
      Caption = 'tsTipoCorreo'
      ImageIndex = 2
      TabVisible = False
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 438
        Height = 105
        Align = alTop
        Caption = ' Datos de Encuesta '
        TabOrder = 0
        object Label3: TLabel
          Left = 22
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label4: TLabel
          Left = 22
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO2: TZetaTextBox
          Left = 64
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE2: TZetaTextBox
          Left = 64
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object rgEstiloCorreo: TRadioGroup
        Left = 0
        Top = 105
        Width = 438
        Height = 59
        Align = alTop
        Caption = ' Elegir estilo de correo '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Plantilla de correo'
          'Texto fijo')
        TabOrder = 1
      end
    end
    object tsTipoPlantilla: TTabSheet [2]
      Caption = 'tsTipoPlantilla'
      ImageIndex = 3
      TabVisible = False
      object lblTema: TLabel
        Left = 32
        Top = 132
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tema:'
      end
      object lblPlantilla: TLabel
        Left = 23
        Top = 157
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Plantilla:'
      end
      object btnDialogo: TSpeedButton
        Left = 412
        Top = 151
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
          B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
          B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
          0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
          55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
          55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
          55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
          5555575FFF755555555557000075555555555577775555555555}
        NumGlyphs = 2
        OnClick = btnDialogoClick
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 438
        Height = 105
        Align = alTop
        Caption = ' Datos de Encuesta  '
        TabOrder = 0
        object Label5: TLabel
          Left = 22
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label6: TLabel
          Left = 22
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO3: TZetaTextBox
          Left = 64
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE3: TZetaTextBox
          Left = 64
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object edTema: TEdit
        Left = 64
        Top = 128
        Width = 347
        Height = 21
        MaxLength = 255
        TabOrder = 1
      end
      object edPlantilla: TEdit
        Left = 64
        Top = 153
        Width = 347
        Height = 21
        MaxLength = 255
        TabOrder = 2
      end
    end
    object tsTipoTexto: TTabSheet [3]
      Caption = 'tsTipoTexto'
      ImageIndex = 4
      TabVisible = False
      object pnDatos: TPanel
        Left = 0
        Top = 0
        Width = 438
        Height = 97
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object lblTemaTexto: TLabel
          Left = 5
          Top = 12
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tema:'
        end
        object edTemaTexto: TEdit
          Left = 39
          Top = 8
          Width = 356
          Height = 21
          MaxLength = 255
          TabOrder = 0
        end
        object rgFormatoCorreo: TRadioGroup
          Left = 0
          Top = 40
          Width = 438
          Height = 57
          Align = alBottom
          Caption = ' Formato del correo  '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Texto'
            'HTML')
          TabOrder = 1
        end
      end
      object gbMensaje: TGroupBox
        Left = 0
        Top = 97
        Width = 438
        Height = 161
        Align = alClient
        Caption = ' Mensaje '
        TabOrder = 1
        object Mensaje: TMemo
          Left = 2
          Top = 15
          Width = 403
          Height = 144
          Align = alClient
          ScrollBars = ssVertical
          TabOrder = 0
        end
        object pnBoton: TPanel
          Left = 405
          Top = 15
          Width = 31
          Height = 144
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object btnArchivo: TSpeedButton
            Left = 4
            Top = 3
            Width = 25
            Height = 25
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
              55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
              B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
              B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
              0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
              55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
              55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
              55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
              5555575FFF755555555557000075555555555577775555555555}
            NumGlyphs = 2
            OnClick = btnArchivoClick
          end
        end
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 438
      end
      inherited ProgressPanel: TPanel
        Top = 191
        Width = 438
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Archivos XSL|*.xsl|Todos|*.*'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 372
    Top = 48
  end
  object OpenDialog2: TOpenDialog
    Filter = 'Archivo Texto|*.txt|Archivo HTML|*.html|Todos|*.*'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 404
    Top = 48
  end
end
