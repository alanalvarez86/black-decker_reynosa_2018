unit FWizRecalcularPromedios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseWizard, ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  ZetaDBTextBox;

type
  TWizRecalcularPromedios = class(TBaseWizard)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ET_CODIGO: TZetaTextBox;
    ET_NOMBRE: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
  private
    { Private declarations }
  protected
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizRecalcularPromedios: TWizRecalcularPromedios;

implementation

{$R *.DFM}

uses ZHelpContext,
     DCliente,
     DProcesos;

procedure TWizRecalcularPromedios.FormCreate(Sender: TObject);
begin
     inherited;
     //ParametrosControl := US_CODIGO;
     HelpContext := H_DISENO_ENCUESTA_PROCESOS_RECALCULAR_PROMEDIOS;
end;

procedure TWizRecalcularPromedios.FormShow(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          ET_CODIGO.Caption := InttoStr( Encuesta );
          ET_NOMBRE.Caption := Nombre;
     end;
end;

procedure TWizRecalcularPromedios.CargaParametros;
begin
     with Descripciones do
     begin
          AddString( 'Encuesta', ET_CODIGO.Caption + ' = ' + ET_NOMBRE.Caption );
     end;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'Encuesta', dmCliente.Encuesta );
     end;
end;

procedure TWizRecalcularPromedios.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( PageControl.ActivePage = Parametros ) then
          begin
               if( dmCliente.Encuesta <= 0 )then
                  CanMove := Error( '� No se han encontraso Encuestas v�lidas !', Siguiente );
          end;
     end;
end;

function TWizRecalcularPromedios.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.RecalcularPromedios( ParameterList );
end;

end.
