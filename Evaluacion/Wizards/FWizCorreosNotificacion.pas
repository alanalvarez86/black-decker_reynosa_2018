unit FWizCorreosNotificacion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls,
     ZBaseWizard,
     ZetaWizard,
     ZetaEdit,
     ZetaKeyCombo,
     ZetaDBTextBox;

type
  TWizCorreosNotificacion = class(TBaseWizard)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ET_CODIGO: TZetaTextBox;
    ET_NOMBRE: TZetaTextBox;
    gbEvaluadores: TGroupBox;
    lblLista: TLabel;
    btnLista: TSpeedButton;
    edLista: TZetaEdit;
    tsTipoCorreo: TTabSheet;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    ET_CODIGO2: TZetaTextBox;
    ET_NOMBRE2: TZetaTextBox;
    rgEstiloCorreo: TRadioGroup;
    tsTipoPlantilla: TTabSheet;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    ET_CODIGO3: TZetaTextBox;
    ET_NOMBRE3: TZetaTextBox;
    lblTema: TLabel;
    edTema: TEdit;
    edPlantilla: TEdit;
    lblPlantilla: TLabel;
    btnDialogo: TSpeedButton;
    tsTipoTexto: TTabSheet;
    pnDatos: TPanel;
    lblTemaTexto: TLabel;
    edTemaTexto: TEdit;
    rgFormatoCorreo: TRadioGroup;
    gbMensaje: TGroupBox;
    Mensaje: TMemo;
    pnBoton: TPanel;
    btnArchivo: TSpeedButton;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    ENC_RELA: TZetaKeyCombo;
    ENC_RELAlbl: TLabel;
    lblStatus: TLabel;
    lbStatus: TZetaKeyCombo;
    PorRelacionStatus: TRadioButton;
    PorLista: TRadioButton;
    procedure FormShow(Sender: TObject);
    procedure btnListaClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure btnDialogoClick(Sender: TObject);
    procedure btnArchivoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PorRelacionStatusClick(Sender: TObject);
    procedure PorListaClick(Sender: TObject);
  private
    procedure CargaDescripcionFinal;
    procedure ActivaControlesEvaluadores;{OP: 22.10.08}
    function BuscaEvaluador(const lConcatena: Boolean; const sLlave: String): String;
    function ValidaSettingsTipoPlantilla: String;
    function ValidaSettingsTipoTexto: String;
  protected
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizCorreosNotificacion: TWizCorreosNotificacion;

implementation

uses DCliente,
     DEvaluacion,
     DProcesos,
     ZetaEvaluacionTools,
     ZHelpContext,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

procedure TWizCorreosNotificacion.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_DISENO_ENCUESTA_PROCESOS_ENVIA_CORREOS_NOTIFICACION;
     ZetaCommonLists.LlenaLista( lfeMailType, rgFormatoCorreo.Items );
end;

procedure TWizCorreosNotificacion.FormShow(Sender: TObject);
var
   eStatus: eStatusEvaluador;
begin
     inherited;
     with dmCliente do
     begin
          ET_CODIGO.Caption := InttoStr( Encuesta );
          ET_NOMBRE.Caption := Nombre;
          ET_CODIGO2.Caption := ET_CODIGO.Caption;
          ET_NOMBRE2.Caption := ET_NOMBRE.Caption;
          ET_CODIGO3.Caption := ET_CODIGO.Caption;
          ET_NOMBRE3.Caption := ET_NOMBRE.Caption;
     end;
     with ENC_RELA do
     begin
          dmEvaluacion.LlenaPerfilEvaluadores( Lista, True );
          ItemIndex := 0;
     end;
     with dmEvaluacion do
     begin
          cdsEvaluadoresLookup.Refrescar;
     end;
     rgEstiloCorreo.ItemIndex := Ord( tpPlantilla );
     with lbStatus do
     begin
          with Items do
          begin
               BeginUpdate;
               try
                  for eStatus := Low( eStatusEvaluador ) to High( eStatusEvaluador ) do
                  begin
                       Add( ZetaEvaluacionTools.GetStatusEvaluador( eStatus ) );
                  end;
               finally
                      EndUpdate;
               end;
          end;
          ItemIndex := 0;
     end;
     edTema.Text := ET_NOMBRE.Caption;
     edTemaTexto.Text := ET_NOMBRE.Caption;

     {OP : 22.10.08}
     {Se inicializan los controles de "Selecciona Evaluadores".}
     ActivaControlesEvaluadores;
end;

procedure TWizCorreosNotificacion.CargaParametros;
const
     aCriterio: array[ False..True ] of String = ( 'Por Lista', 'Por Relacion' );
var
   eEstilo: eTipoCorreo;
   eFormato: eEmailType;
   sRelacion: String;
   lPorRelacion: Boolean;
begin
     lPorRelacion := PorRelacionStatus.Checked;
     eEstilo := eTipoCorreo( rgEstiloCorreo.ItemIndex );
     case eEstilo of
          tpPlantilla: eFormato := emtHTML;
     else
         eFormato := eEmailType( rgFormatoCorreo.ItemIndex );
     end;
     with Descripciones do
     begin
          AddString( 'Encuesta', ET_CODIGO.Caption + ' = ' + ET_NOMBRE.Caption );
          AddString( 'Criterio Seleccion', aCriterio[ lPorRelacion ] );
          if ( ENC_RELA.LlaveEntero < 0 ) then
             sRelacion := K_TODAS_DESCRIPCION
          else
              sRelacion := ENC_RELA.Descripcion;
          AddString( 'Relacion', sRelacion );
          AddString( 'Status', lbStatus.Items[ lbStatus.ItemIndex ] );
          if StrLleno( edLista.Text ) then
             AddString( 'Evaluadores', edLista.Text );
          AddString( 'Tipo de Correo', rgEstiloCorreo.Items[ rgEstiloCorreo.ItemIndex ] );
          {Descripciones para correos tipo plantilla}
          AddString( 'Tema Plantilla', edTema.Text );
          AddString( 'Plantilla', edPlantilla.Text );
          {Descripciones para correos tipo texto}
          AddString( 'Tema Texto', edTemaTexto.Text );
          AddString( 'Formato', ZetaCommonLists.ObtieneElemento( lfeMailType, Ord( eFormato ) ) );
          AddMemo( 'Mensaje', Mensaje.Text );
     end;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'Encuesta', dmCliente.Encuesta );
          AddBoolean( 'PorRelacion', lPorRelacion );
          AddInteger( 'Relacion', ENC_RELA.LlaveEntero );
          AddString( 'RelacionDesc', sRelacion );

          AddInteger( 'Status', lbStatus.Valor );

          AddString( 'Evaluadores', edLista.Text );
          AddInteger( 'TipoCorreo', Ord( eEstilo ) );
          AddInteger( 'Responsable', dmCliente.Responsable );

          {Parametros para tipo de correo plantilla}
          AddString( 'TemaPlantilla', edTema.Text );
          AddString( 'Plantilla', edPlantilla.Text );

          {Parametros para tipo de correo texto fijo}
          AddString( 'TemaTexto', edTemaTexto.Text );
          AddInteger( 'FormatoCorreo', Ord( eFormato ) );
          AddMemo( 'Mensaje', Mensaje.Lines.Text );
     end;
end;

procedure TWizCorreosNotificacion.btnListaClick(Sender: TObject);
begin
     inherited;
     with edLista do
     begin
          Text := BuscaEvaluador( True, Text );
     end;
end;

function TWizCorreosNotificacion.BuscaEvaluador( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     with dmEvaluacion.cdsEvaluadoresLookup do
     begin
          Search( '', sKey, sDescripcion );
          {
          if ( Search( '', sKey, sDescripcion ) ) then
          begin
          }
               if lConcatena and ZetaCommonTools.StrLleno( Text ) then
               begin
                    if StrLleno( sLlave ) then
                       Result := sLlave + ',' + sKey
                    else
                        Result := sKey;
               end
               else
                   Result := sKey;
          //end;
     end;
end;

procedure TWizCorreosNotificacion.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   sMensaje: String;
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( PageControl.ActivePage = Parametros ) then
          begin
               dmEvaluacion.cdsPerfilEvaluador.Conectar;
               if ( dmEvaluacion.cdsPerfilEvaluador.RecordCount <= 0 ) then
                  CanMove := Error( '� No se tienen registrados perfiles de evaluadores !', Siguiente );
               if PorLista.Checked and StrVacio( edLista.Text ) then
                  CanMove := Error( '� No se captur� la lista de evaluadores !', edLista );
          end
          else
          if ( PageControl.ActivePage = tsTipoCorreo ) then
          begin
               tsTipoPlantilla.Enabled := ( rgEstiloCorreo.ItemIndex = Ord( tpPlantilla ) );
               tsTipoTexto.Enabled := not tsTipoPlantilla.Enabled;
          end
          else
          if ( PageControl.ActivePage = tsTipoPlantilla ) then
          begin
               sMensaje := ValidaSettingsTipoPlantilla;
               if StrLleno( sMensaje ) then
                  CanMove := Error( sMensaje, Siguiente )
               else
                   CargaDescripcionFinal;
          end
          else
          if ( PageControl.ActivePage = tsTipoTexto ) then
          begin
               sMensaje := ValidaSettingsTipoTexto;
               if StrLleno( sMensaje ) then
                  CanMove := Error( sMensaje, Siguiente )
               else
                   CargaDescripcionFinal;
          end;
     end;
end;

function TWizCorreosNotificacion.ValidaSettingsTipoPlantilla: String;
begin
     Result := VACIO;
     if( strVacio( edTema.Text ) )then
         Result := 'El tema no puede quedar vac�o'
     else
         if( strVacio( edPlantilla.Text ) )then
             Result := 'Es necesario seleccionar la plantilla a utilizar'
         else
             if( not FileExists( edPlantilla.Text ) )then
                 Result := 'Archivo de plantilla no existe';
end;

function TWizCorreosNotificacion.ValidaSettingsTipoTexto: String;
begin
     Result := VACIO;
     if( strVacio( edTemaTexto.Text ) )then
         Result := 'El tema no puede quedar vac�o'
     else
         if( strVacio( Mensaje.Text ) )then
             Result := 'Es necesario que el mensaje tenga contenido';
end;

procedure TWizCorreosNotificacion.CargaDescripcionFinal;
begin
     Advertencia.Clear;
     Advertencia.Lines.Add( 'Advertencia'+ CR_LF+
                            'Se enviar�n los correos de notificaci�n para los' + CR_LF+
                            'evaluadores seleccionados en la encuesta activa ' +CR_LF+
                            CR_LF+'Presione el bot�n ''Ejecutar'' para iniciar el proceso ' );
end;

function TWizCorreosNotificacion.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CorreosNotificacion( ParameterList );
end;

procedure TWizCorreosNotificacion.btnDialogoClick(Sender: TObject);
begin
     inherited;
     OpenDialog1.Title := 'Seleccionar plantilla';
     edPlantilla.Text := ZetaClientTools.AbreDialogo( OpenDialog1, edPlantilla.Text, 'xsl' );
end;

procedure TWizCorreosNotificacion.btnArchivoClick(Sender: TObject);
var
   sFileName: String;
begin
     inherited;
     OpenDialog2.Title := 'Seleccionar archivo';
     sFileName := ZetaClientTools.AbreDialogo( OpenDialog2, sFileName, 'txt' );
     if( strLleno( sFileName ) )then
     begin
          Mensaje.Lines.LoadFromFile( sFileName );
     end;
end;

procedure TWizCorreosNotificacion.PorRelacionStatusClick(Sender: TObject);
begin
     inherited;
     PorLista.Checked := False;
     ActivaControlesEvaluadores; {OP: 22.10.08}
end;

procedure TWizCorreosNotificacion.PorListaClick(Sender: TObject);
begin
     inherited;
     PorRelacionStatus.Checked := False;
     ActivaControlesEvaluadores; {OP: 22.10.08}
end;

{OP: 22.10.08}
{ Se activan/desactivan controles en la secci�n de selecci�n de evaluador. }
{ Se utiliza como referencia el control de PorRelacionStatus para manejar
  el los siguientes casos: verdadero en el caso de seleccionar "Por Relaci�n
  y Status" y falso en caso de seleccionar "Por Lista".}
procedure TWizCorreosNotificacion.ActivaControlesEvaluadores;
var
   lBandera: Boolean;
begin
     lBandera := PorRelacionStatus.Checked;
     lblStatus.Enabled := lBandera;
     lbStatus.Enabled := lBandera;
     ENC_RELAlbl.Enabled := lBandera;
     ENC_RELA.Enabled := lBandera;
     lblLista.Enabled := Not lBandera;
     edLista.Enabled := Not lBandera;
     btnLista.Enabled := Not lBandera;

     {Limpia el lookup de evaluadores, en el caso de ser verdadera la bandera}
     if lBandera then
        edLista.Valor := VACIO;
end;

end.
