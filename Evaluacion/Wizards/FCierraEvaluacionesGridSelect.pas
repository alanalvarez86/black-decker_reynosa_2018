unit FCierraEvaluacionesGridSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid, Db, StdCtrls, Grids, DBGrids, ZetaDBGrid, Buttons,
  ExtCtrls;

type
  TCierraEvaluacionesGridSelect = class(TBaseGridSelect)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CierraEvaluacionesGridSelect: TCierraEvaluacionesGridSelect;

implementation

{$R *.DFM}

end.
