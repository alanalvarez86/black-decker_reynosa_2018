unit FWizAsignaRelaciones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseWizardFiltro, ComCtrls, ZetaKeyLookup, ZetaEdit, StdCtrls, Buttons,
  ZetaWizard, ExtCtrls, ZetaDBTextBox, CheckLst;

type
  TWizAsignaRelaciones = class(TBaseWizardFiltro)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ET_CODIGO: TZetaTextBox;
    ET_NOMBRE: TZetaTextBox;
    gbRelaciones: TGroupBox;
    cblRelaciones: TCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure LlenaGrupoRelaciones;
  protected
    procedure CargaParametros; override;
    procedure CargaListaVerificacion; override;
    function Verificar: Boolean; override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizAsignaRelaciones: TWizAsignaRelaciones;

implementation

uses DCliente,
     DProcesos,
     DEvaluacion,
     ZHelpContext,
     ZetaCommonLists,
     ZetaCommonTools,
     ZBaseSelectGrid,
     FAsignaRelacionesGridSelect;

{$R *.DFM}
procedure TWizAsignaRelaciones.FormCreate(Sender: TObject);
begin
  inherited;
  HelpContext := H_DISENO_ENCUESTA_PROCESOS_ASIGNACION_RELACIONES;
end;

procedure TWizAsignaRelaciones.FormShow(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          ET_CODIGO.Caption := InttoStr( Encuesta );
          ET_NOMBRE.Caption := Nombre;
     end;
     LlenaGrupoRelaciones;
end;

procedure TWizAsignaRelaciones.LlenaGrupoRelaciones;
const
     K_ALTURA_1 = 45;
     K_ALTURA_2 = 65;
     K_ALTURA_3 = 85;
     K_ALTURA_4 = 100;
var
   oCursor: TCursor;
   {
   i: Integer;,
   }
   iCuantos: Integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with cblRelaciones do
        begin
             dmEvaluacion.LlenaPerfilEvaluadores( Items, [ tevCliente, tevOtros ], FALSE, FALSE );
             {
             Items.BeginUpdate;
             try
                i := 0;
                while i < ( Items.Count ) do
                begin
                     if( ( Integer( Items.Objects[i] ) = Ord( tevCliente ) ) or
                         ( Integer( Items.Objects[i] ) = Ord( tevOtros ) ) )then
                     begin
                          Items.Delete(i);
                          i := i - 1;
                     end;
                     i := i + 1;
                end;
             finally
                    Items.EndUpdate;
             end;
             }
             iCuantos := Items.Count;
             Visible := ( iCuantos > 0 );
             gbRelaciones.Visible := Visible;
             if Visible then
             begin
                  case iCuantos of
                       1: gbRelaciones.Height := K_ALTURA_1;
                       2: gbRelaciones.Height := K_ALTURA_2;
                       3: gbRelaciones.Height := K_ALTURA_3;
                       4: gbRelaciones.Height := K_ALTURA_4;
                  else
                      gbRelaciones.Height := 0;
                  end;
                  ItemIndex := 0;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TWizAsignaRelaciones.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( PageControl.ActivePage = Parametros ) then
          begin
               if( not cblRelaciones.Visible )then
                  CanMove := Error( '� No se tienen registrados perfiles de evaluadores compatibles con el proceso !', Siguiente );
          end;
     end;
end;

procedure TWizAsignaRelaciones.CargaParametros;
var
   i: Integer;
   sRelaciones, sRelacionesDesc: String;
begin
     with Descripciones do
     begin
          AddString( 'Encuesta', ET_CODIGO.Caption + ' = ' + ET_NOMBRE.Caption );
          for i:=0 to ( cblRelaciones.Items.Count - 1 )do
          begin
               if( cblRelaciones.Checked[ i ] )then
               begin
                    sRelaciones := ConcatString( sRelaciones, InttoStr( Integer( cblRelaciones.Items.Objects[i] ) ), ',' );
                    sRelacionesDesc := ConcatString( sRelacionesDesc, cblRelaciones.Items.Strings[ i ], ',' );
               end;
          end;
          AddString( 'Relaciones', sRelacionesDesc );
     end;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'Encuesta', dmCliente.Encuesta );
          AddString( 'Relaciones', sRelaciones );
     end;
end;

procedure TWizAsignaRelaciones.CargaListaVerificacion;
begin
     dmProcesos.AsignaRelacionesGetLista( ParameterList );
end;

function TWizAsignaRelaciones.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid.GridSelect( dmProcesos.cdsDataset, TAsignaRelacionesGridSelect );
end;

function TWizAsignaRelaciones.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AsignaRelaciones( ParameterList, Verificacion );
end;



end.
