unit FWizCreaEncuesta;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, StdCtrls, Buttons, ExtCtrls, DBCtrls, Mask, Grids, DBGrids, Db,
     ZBaseWizard,
     ZetaWizard,
     ZetaKeyLookup,
     ZetaFecha,
     ZetaNumero,
     ZetaDBGrid,
     ZetaCommonClasses,
     ZetaEdit,
     ZBaseWizardFiltro;

type
  TWizCreaEncuesta = class(TBaseWizardFiltro)
    PanelTitulo1: TPanel;
    ET_NOMBRElbl: TLabel;
    ET_NOMBRE: TEdit;
    ET_DESCRIPlbl: TLabel;
    ET_DESCRIP: TMemo;
    ET_NUM_PRE: TZetaNumero;
    ET_NUM_PRElbl: TLabel;
    ET_FEC_INIlbl: TLabel;
    ET_FEC_INI: TZetaFecha;
    ET_USR_ADM: TZetaKeyLookup;
    ET_USR_ADMlbl: TLabel;
    ET_FEC_FIN: TZetaFecha;
    ET_FEC_FINlbl: TLabel;
    tsPregx: TTabSheet;
    gbPreguntas: TGroupBox;
    EP_DESCRIP: TMemo;
    pnLeyenda: TPanel;
    pnIzq: TPanel;
    gbRespuestas: TGroupBox;
    EL_NOMBRE: TMemo;
    pnDer: TPanel;
    tsBienvenida: TTabSheet;
    ET_MSG_INI: TMemo;
    tsDespedida: TTabSheet;
    ET_MSG_FIN: TMemo;
    ET_DOCUMENFind: TSpeedButton;
    ET_DOCUMEN: TEdit;
    ET_DOCUMENlbl: TLabel;
    OpenDialog: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure ET_NUM_PREExit(Sender: TObject);
    procedure ET_DOCUMENFindClick(Sender: TObject);
  private
    { Private declarations }
    FTotalPaginas: Integer;
    FNumPregunta: Integer;
    FPreguntas: Integer;
    function VerificaExistenDatos: Boolean;
    function RegresaMemos( const oTabSheet: TTabSheet; var oMemoEscalas: TMemo ): TMemo;
    procedure DoConnect;
    procedure Connect;
    procedure SetControles;
    procedure CreaPaginasPreguntas;
    procedure LiberaPaginas;
    procedure EscribePanelTitulo;
    procedure InicializaRespuestas;
    procedure RecolectaPreguntasRespuestas( const oParametros: TZetaParams );
    procedure CargaPaginas;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaParametros; override;
    procedure CargaListaVerificacion; override;
  public
    { Public declarations }
  end;

var
  WizCreaEncuesta: TWizCreaEncuesta;

implementation

uses DEvaluacion,
     DSistema,
     DCliente,
     DProcesos,
     ZHelpContext,
     ZBaseSelectGrid,
     ZetaDialogo,
     ZetaCommonTools,
     FCreaEncuestaSimpleGridSelect,
     FTressShell;

const
     K_OFFSET = 2;
     K_PREGUNTA = ' Pregunta # %d';
     K_TAB_PREGUNTA = 'tsPregunta';
     K_MEMO_PREGUNTAS = 'EP_DESCRIP';
     K_MEMO_RESPUESTAS = 'Respuestas';

{$R *.DFM}

procedure TWizCreaEncuesta.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := ET_NOMBRE;
     HelpContext := H_DISENO_ENCUESTA_PROCESOS_ENCUESTA_SIMPLE;
     PageControl.ActivePage := Parametros;
     ET_USR_ADM.LookupDataset := dmSistema.cdsUsuarios;
end;

procedure TWizCreaEncuesta.FormShow(Sender: TObject);
begin
     inherited;
     DoConnect;
     SetControles;
     CreaPaginasPreguntas;
     PageControl.ActivePage := Parametros;
     ActiveControl := ET_NOMBRE;
end;

procedure TWizCreaEncuesta.FormDestroy(Sender: TObject);
begin
     inherited;
     LiberaPaginas;
end;

procedure TWizCreaEncuesta.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
end;

procedure TWizCreaEncuesta.DoConnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
             except
                   on Error: Exception do
                      zExcepcion( '� Error al conectar forma !', '� Ventana no pudo ser conectada a sus datos !', Error, 0 );
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TWizCreaEncuesta.CargaParametros;
const
     K_TODOS = 0;
     K_RANGO = 1;
     K_LISTA = 2;
begin
     with Descripciones do
     begin
          AddString( 'Nombre', ET_NOMBRE.Text );
          AddString( 'Fecha_Inicial', ZetaCommonTools.FechaCorta( ET_FEC_INI.Valor ) );
          AddString( 'Fecha_Final', ZetaCommonTools.FechaCorta( ET_FEC_FIN.Valor ) );
          AddInteger( 'Preguntas', FPreguntas ); // GA: Antes era FNumPreguntas �xq?
          AddInteger( 'Administrador', ET_USR_ADM.Valor );
          {$ifdef FALSE}
          if( rbTodos.Checked )then
              AddString( 'Evaluadores', 'Todos' )
          else if( rbRango.Checked )then
                   AddString( 'Evaluadores', 'Del: ' + edInicial.ValorAsText + ' al: ' + edFinal.ValorAsText )
               else
                   AddString( 'Evaluadores', edLista.Text );
          {$endif}
     end;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddString( 'ET_NOMBRE', ET_NOMBRE.Text );
          AddString( 'ET_DOCUMEN', ET_DOCUMEN.Text );
          AddMemo( 'ET_DESCRIP', ET_DESCRIP.Text );
          AddDate( 'ET_FEC_INI', ET_FEC_INI.Valor );
          AddDate( 'ET_FEC_FIN', ET_FEC_FIN.Valor );
          AddInteger( 'ET_USR_ADM', ET_USR_ADM.Valor );
          AddInteger( 'ET_NUM_PRE', FPreguntas ); // GA: Antes era FNumPreguntas �xq?
          AddMemo( 'ET_MSG_INI', ET_MSG_INI.Text );
          AddMemo( 'ET_MSG_FIN', ET_MSG_FIN.Text );
          {$ifdef FALSE}
          if( rbTodos.Checked )then
              AddInteger( 'EVALUADORES', K_TODOS )
          else if( rbRango.Checked )then
               begin
                    AddInteger( 'EVALUADORES', K_RANGO );
                    AddInteger( 'RANGO_INI', edInicial.ValorEntero );
                    AddInteger( 'RANGO_FIN', edFinal.ValorEntero );
               end
               else
               begin
                    AddInteger( 'EVALUADORES', K_LISTA );
                    AddString( 'EVAL_LISTA', edLista.Text );
               end;
          {$endif}
          AddMemo( K_MEMO_PREGUNTAS + 'x', EP_DESCRIP.Text );
          AddMemo( K_MEMO_RESPUESTAS + 'x', EL_NOMBRE.Text );
     end;
     RecolectaPreguntasRespuestas( ParameterList );
end;

procedure TWizCreaEncuesta.RecolectaPreguntasRespuestas( const oParametros: TZetaParams );
var
   i: Integer;
   otsPregunta: TTabSheet;
   oPregunta, oRespuestas: TMemo;
begin
     with oParametros do
     begin
          if( FPreguntas > 1 )then
          begin
               for i := 0 to ( FPreguntas - K_OFFSET ) do
               begin
                    otsPregunta := PageControl.Pages[ i + K_OFFSET ];
                    oPregunta := RegresaMemos( otsPregunta, oRespuestas );
                    AddMemo( Format( K_MEMO_PREGUNTAS + '%d', [ i ] ), oPregunta.Text );
                    AddMemo( Format( K_MEMO_RESPUESTAS + '%d', [ i ] ), oRespuestas.Text );
               end;
          end;
     end;
end;

procedure TWizCreaEncuesta.SetControles;
const
     K_VALOR_INICIAL = 1;
begin
     FNumPregunta := 0;
     FPreguntas := K_VALOR_INICIAL;
     ET_NUM_PRE.Valor := K_VALOR_INICIAL;
     ET_FEC_INI.Valor := Date;
     ET_FEC_FIN.Valor := ET_FEC_INI.Valor;
     ET_USR_ADM.Valor := dmCliente.Usuario;
     InicializaRespuestas;
end;

procedure TWizCreaEncuesta.CargaListaVerificacion;
begin
     dmProcesos.CreaEncuestaSimpleGetLista( ParameterList );
end;

function TWizCreaEncuesta.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid.GridSelect( dmProcesos.cdsDataset, TCreaEncuestaSimpleGridSelect );
end;

function TWizCreaEncuesta.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CreaEncuestaSimple( ParameterList, Verificacion );
end;

procedure TWizCreaEncuesta.CreaPaginasPreguntas;

   procedure CreaGroupBoxPregunta( const oPadre: TTabSheet; const iValor: Integer );
   const
        K_GB_HEIGHT = 105;
        K_GB_WIDTH = 510;
   var
      oGroupBox: TGroupBox;
      oMemo: TMemo;
      opnLeyenda: TPanel;
   begin
        opnLeyenda := TPanel.Create( Self );
        with opnLeyenda do
        begin
             Align := alTop;
             Name := Format( 'pnLeyenda%d', [ iValor ] );
             Caption := pnLeyenda.Caption;
             BevelOuter := bvNone;
             Height := pnLeyenda.Height;
             Font.Color := pnLeyenda.Font.Color;
             Font.Style := pnLeyenda.Font.Style;
             Parent := oPadre;
        end;
        oGroupBox := TGroupBox.Create( Self );
        with oGroupBox do
        begin
             Align := alTop;
             Top := 0;
             Left := 0;
             Height := K_GB_HEIGHT;
             Width := K_GB_WIDTH;
             Caption := gbPreguntas.Caption;
             Name := Format( 'gbPregunta%d', [ iValor ] );
             Parent := oPadre;
        end;
        oMemo := TMemo.Create( Self );
        with oMemo do
        begin
             Align := alClient;
             Name := Format( K_MEMO_PREGUNTAS + '%d', [ iValor ] );
             Text := '';
             ScrollBars := ssVertical;
             MaxLength := 255;
             Parent := oGroupBox;
        end;
   end;

   procedure CreaGroupBoxEscalas( const oPadre: TTabSheet; const iValor: Integer );
   const
        K_PANEL_W = 150;
   var
      oGroupBox: TGroupBox;
      oPanelIzq, oPanelDer: TPanel;
      oMemo: TMemo;
   begin

        oGroupBox := TGroupBox.Create( Self );
        with oGroupBox do
        begin
             Align := alClient;
             Caption := gbRespuestas.Caption;
             Name := Format( 'gbRespuestas%d', [ iValor ] );
             Parent := oPadre;
        end;
        oPanelIzq := TPanel.Create( Self );
        with oPanelIzq do
        begin
             Align := alLeft;
             Name := Format( 'pnIzq%d', [ iValor ] );
             Caption := '';
             BevelOuter := bvNone;
             Width := K_PANEL_W;
             Parent := oPadre;
        end;
        oPanelDer := TPanel.Create( Self );
        with oPanelDer do
        begin
             Align := alRight;
             Name := Format( 'pnDer%d', [ iValor ] );
             Caption := '';
             BevelOuter := bvNone;
             Width := K_PANEL_W;
             Parent := oPadre;
        end;
        oMemo := TMemo.Create( Self );
        with oMemo do
        begin
             Align := alClient;
             Name := Format( 'EL_NOMBRE%d', [ iValor ] );
             Text := VACIO;//EL_NOMBRE.Text;
             ScrollBars := ssVertical;
             Font.Color := clBlue;
             Parent := oGroupBox;
        end;
   end;

var
   i: Integer;
   PaginaActiva, PaginaNueva: TTabSheet;
begin
     if ( PageControl.PageCount < FTotalPaginas ) then
     begin
          PaginaActiva := PageControl.ActivePage;
          for i := 0 to ( FPreguntas - K_OFFSET ) do
          begin
               PaginaNueva := TTabSheet.Create( Self );
               with PaginaNueva do
               begin
                    PageControl := Self.PageControl;
                    PageIndex := i + K_OFFSET;
                    Name := Format( K_TAB_PREGUNTA + '%d', [ i + K_OFFSET ] );
                    TabVisible := False;
                    Caption := Format( K_PREGUNTA, [ i + K_OFFSET ] );
               end;
               CreaGroupBoxPregunta( PaginaNueva, ( i + K_OFFSET ) );
               CreaGroupBoxEscalas( PaginaNueva, ( i + K_OFFSET ) );
          end;
          PageControl.ActivePage := PaginaActiva;
     end;
end;

procedure TWizCreaEncuesta.CargaPaginas;
begin
     FNumPregunta := 1;
     FPreguntas := ET_NUM_PRE.ValorEntero;
     FTotalPaginas := PageControl.PageCount + ( FPreguntas - 1 );
     EP_DESCRIP.Clear;
     InicializaRespuestas;
     LiberaPaginas;
     CreaPaginasPreguntas;
end;

procedure TWizCreaEncuesta.EscribePanelTitulo;
begin
     if ( PageControl.ActivePage = Parametros ) then
     begin
          PanelTitulo1.Caption := ' Generales';
     end
     else
         if ( PageControl.ActivePage = FiltrosCondiciones ) then
         begin
              PanelTitulo1.Caption := ' Participantes';
         end
         else
             if ( PageControl.ActivePage = tsBienvenida ) then
             begin
                  PanelTitulo1.Caption := ' Bienvenida';
             end
             else
                 if ( PageControl.ActivePage = tsDespedida ) then
                 begin
                      PanelTitulo1.Caption := ' Despedida';
                 end
                 else
                     if( PageControl.ActivePage = tsPregx )then
                     begin
                          PanelTitulo1.Caption := Format( K_PREGUNTA, [ 1 ] );
                     end
                     else
                         if ( Pos( K_TAB_PREGUNTA, PageControl.ActivePage.Name ) > 0 ) then
                         begin
                              PanelTitulo1.Caption := PageControl.ActivePage.Caption;
                         end;
end;

function TWizCreaEncuesta.RegresaMemos( const oTabSheet: TTabSheet; var oMemoEscalas: TMemo ): TMemo;
var
   i: Integer;
   oGroupBox: TGroupBox;
begin
     Result := nil;
     oMemoEscalas := nil;
     with oTabSheet do
     begin
          i := 0;
          while ( i < ControlCount ) do
          begin
               if ( Pos( 'gbPregunta', Controls[ i ].Name ) > 0 )then
               begin
                    oGroupBox := TGroupBox( Controls[ i ] );
                    Result :=  TMemo( oGroupBox.Controls[ 0 ] );
               end;
               if ( Pos( 'gbRespuestas', Controls[ i ].Name ) > 0 ) then
               begin
                    oGroupBox := TGroupBox( Controls[ i ] );
                    oMemoEscalas := TMemo( oGroupBox.Controls[ 0 ] );
                    Break;
               end;
               i := i + 1;
          end;
     end;
end;

procedure TWizCreaEncuesta.LiberaPaginas;
var
   i, j: Integer;
   sTabNombre: String;
   PaginaActiva: TTabSheet;
begin
     i := 0;
     PaginaActiva := PageControl.ActivePage;
     while( i < ( PageControl.PageCount - 1 ) )do
     begin
          sTabNombre := PageControl.Pages[i].Name;
          if( Pos( 'tsPregunta', sTabNombre ) > 0 )then
          begin
               j := 0;
               while( j < ( PageControl.Pages[i].ControlCount - 1 ) )do
               begin
                    PageControl.Pages[i].Controls[j].Free;
                    j := j + 1;
               end;
               PageControl.Pages[i].Free;
               i := i - 1 ;
          end;
          i := i + 1;
     end;
     PageControl.ActivePage := PaginaActiva;
end;

function TWizCreaEncuesta.VerificaExistenDatos: Boolean;
const
     K_PAGINAS_INICIO = 6;
var
   i, j: Integer;
   iTotalPaginas: Integer;
   oMemoPreg, oMemoEscalas: TMemo;
begin
     j := 2;
     Result := False;
     iTotalPaginas := PageControl.PageCount;
     if ( iTotalPaginas > K_PAGINAS_INICIO ) then
     begin
          for i := K_PAGINAS_INICIO to iTotalPaginas do
          begin
               oMemoPreg := RegresaMemos( PageControl.Pages[ j ], oMemoEscalas );
               Result := ( strLleno( oMemoPreg.Text ) or strLleno( oMemoEscalas.Text ) );
               if Result then
                  Break;
               j := j + 1;
          end;

     end;
end;

procedure TWizCreaEncuesta.InicializaRespuestas;
begin
     with EL_NOMBRE.Lines do
     begin
          Clear;
          Add( 'Respuesta#1' );
          Add( 'Respuesta#2' );
          Add( 'Respuesta#3' );
     end;
end;

procedure TWizCreaEncuesta.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const
     K_ERROR_PREGUNTAS = '� El texto de la pregunta no puede quedar vac�o !';
     K_ERROR_RESPUESTAS = '� La escala de respuestas para la pregunta no puede quedar vac�a !';
var
   oPregunta, oRespuesta: TMemo;
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( PageControl.ActivePage = Parametros ) then
          begin
               if ( dmCliente.Encuesta <= 0 )then
                  CanMove := Error( '� No se han encontraso Encuestas v�lidas !', Siguiente )
               else
                   if ( ET_NUM_PRE.ValorEntero <= 0 ) then
                      CanMove := Error( '� El n�mero de preguntas en la encuesta debe de ser mayor que 0 !', ET_NUM_PRE )
               else
                   if StrVacio( ET_NOMBRE.Text ) then
                      CanMove := Error( '� El nombre de la encuesta no puede quedar vac�o !', ET_NOMBRE )
                   else
                       if StrVacio( ET_USR_ADM.Llave ) then
                          CanMove := Error( 'Se debe asignar un responsable de Encuesta.', ET_USR_ADM );
               if CanMove then
               begin
                    CargaPaginas;
               end;
          end;
          if ( PageControl.ActivePage = tsPregx ) then
          begin
               if StrVacio( EP_DESCRIP.Text ) then
                  CanMove := Error( K_ERROR_PREGUNTAS, EP_DESCRIP )
               else
                   if StrVacio( EL_NOMBRE.Text ) then
                      CanMove := Error( K_ERROR_RESPUESTAS, EP_DESCRIP );
          end;
          if ( Pos( K_TAB_PREGUNTA, PageControl.ActivePage.Name ) > 0 ) then
          begin
               oPregunta := RegresaMemos( PageControl.ActivePage, oRespuesta  );
               if StrVacio( oPregunta.Text ) then
                  CanMove := Error( K_ERROR_PREGUNTAS, oPregunta )
               else
                   if StrVacio( oRespuesta.Text ) then
                      CanMove := Error( K_ERROR_RESPUESTAS, oPregunta );
          end;
     end;
end;

procedure TWizCreaEncuesta.WizardAfterMove(Sender: TObject);
var
   oPregunta, oRespuestas: TMemo;
begin
     inherited;
     EscribePanelTitulo;
     if ( PageControl.ActivePage = Parametros ) then
        ET_NOMBRE.SetFocus
     else
         if ( PageControl.ActivePage = tsBienvenida ) then
            ET_MSG_INI.SetFocus
         else
             if ( PageControl.ActivePage = tsDespedida ) then
                ET_MSG_FIN.SetFocus
             else
                 if ( PageControl.ActivePage = tsPregx ) then
                     EP_DESCRIP.SetFocus
                 else
                     if ( Pos( K_TAB_PREGUNTA, PageControl.ActivePage.Name ) > 0 ) then
                     begin
                          oPregunta := RegresaMemos( PageControl.ActivePage, oRespuestas );
                          oPregunta.SetFocus;
                     end;
     PanelTitulo1.Visible := ( PageControl.ActivePage <> Ejecucion );
end;

procedure TWizCreaEncuesta.ET_NUM_PREExit(Sender: TObject);
const
     K_PREGUNTA = 'Se perder� el contenido de las preguntas previamente creadas' + CR_LF + '� Desea cambiar el n�mero de preguntas ?';
begin
     inherited;
     if ( FPreguntas <> ET_NUM_PRE.ValorEntero ) and VerificaExistenDatos then
     begin
          if not zConfirm( Self.Caption, K_PREGUNTA, 0, mbNo ) then
          begin
               ET_NUM_PRE.Valor := FPreguntas;
          end;
     end;
end;

procedure TWizCreaEncuesta.ET_DOCUMENFindClick(Sender: TObject);
begin
     inherited;
     with ET_DOCUMEN do
     begin
          with OpenDialog do
          begin
               FileName := Text;
               if Execute then
               begin
                    Text := ExtractFileName( FileName );
               end;
          end;
     end;
end;

end.
