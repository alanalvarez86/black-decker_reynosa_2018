unit FWizCierraEvaluaciones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseWizardFiltro, ComCtrls, ZetaKeyLookup, ZetaEdit, StdCtrls, Buttons,
  ZetaWizard, ExtCtrls;

type
  TWizCierraEvaluaciones = class(TBaseWizardFiltro)
    btnVerifica: TBitBtn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizCierraEvaluaciones: TWizCierraEvaluaciones;

implementation

{$R *.DFM}

uses DProcesos,
     ZBaseSelectGrid,
     FCierraEvaluacionesGridSelect;

{********* TWizCierraEvaluaciones ************}
procedure TWizCierraEvaluaciones.FormCreate(Sender: TObject);
begin
     inherited;
     //ParametrosControl := US_CODIGO;
     //HelpContext := H10170_Curso_tomado_global;
end;

procedure TWizCierraEvaluaciones.CargaListaVerificacion;
begin
     dmProcesos.CierraEvaluacionesGetLista( ParameterList );
end;

procedure TWizCierraEvaluaciones.CargaParametros;
begin
     inherited CargaParametros;
end;

function TWizCierraEvaluaciones.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CierraEvaluaciones( ParameterList, Verificacion );
end;

function TWizCierraEvaluaciones.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid.GridSelect( dmProcesos.cdsDataset, TCierraEvaluacionesGridSelect );
end;

end.
