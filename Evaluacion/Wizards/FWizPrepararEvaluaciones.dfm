inherited WizPrepararEvaluaciones: TWizPrepararEvaluaciones
  Left = 345
  Top = 311
  Caption = 'Preparar Evaluaciones'
  ClientWidth = 449
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Width = 449
    BeforeMove = WizardBeforeMove
    inherited Salir: TZetaWizardButton
      Left = 362
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 279
    end
  end
  inherited PageControl: TPageControl
    Width = 449
    inherited Parametros: TTabSheet
      object GroupBox1: TGroupBox
        Left = 8
        Top = 64
        Width = 425
        Height = 105
        Caption = ' Datos de Encuesta: '
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N�mero:'
        end
        object Label2: TLabel
          Left = 16
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO: TZetaTextBox
          Left = 60
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE: TZetaTextBox
          Left = 60
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 441
        Lines.Strings = (
          
            'Se va a preparar la evaluaci�n de acuerdo al valor activo selecc' +
            'ionado.'
          ''
          'Presione el bot�n '#39'Ejecutar'#39' para iniciar el proceso.')
      end
      inherited ProgressPanel: TPanel
        Width = 441
        inherited ProgressBar: TProgressBar
          Left = 47
        end
      end
    end
  end
end
