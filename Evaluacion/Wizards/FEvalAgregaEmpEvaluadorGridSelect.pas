unit FEvalAgregaEmpEvaluadorGridSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid, Db, StdCtrls, Grids, DBGrids, ZetaDBGrid, Buttons,
  ExtCtrls;

type
  TEvalAgregaEmpEvaluadorGridSelect = class(TBaseGridSelect)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EvalAgregaEmpEvaluadorGridSelect: TEvalAgregaEmpEvaluadorGridSelect;

implementation

{$R *.DFM}

end.
