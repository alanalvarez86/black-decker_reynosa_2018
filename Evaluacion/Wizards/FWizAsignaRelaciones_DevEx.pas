unit FWizAsignaRelaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZCXBaseWizardFiltro,
  ComCtrls, ZetaKeyLookup, ZetaEdit, StdCtrls, Buttons,
  ZetaWizard, ExtCtrls, ZetaDBTextBox, CheckLst, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, cxCheckListBox;

type
  TWizAsignaRelaciones_DevEx = class(TBaseCXWizardFiltro)
    GroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ET_CODIGO: TZetaTextBox;
    ET_NOMBRE: TZetaTextBox;
    gbRelaciones: TcxGroupBox;
    cblRelaciones: TcxCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure LlenaGrupoRelaciones;
  protected
    procedure CargaParametros; override;
    procedure CargaListaVerificacion; override;
    function Verificar: Boolean; override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizAsignaRelaciones_DevEx: TWizAsignaRelaciones_DevEx;

implementation

uses DCliente,
     DProcesos,
     DEvaluacion,
     ZHelpContext,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaDialogo,
     ZBaseSelectGrid_DevEx,
     FAsignaRelacionesGridSelect_DevEx;

{$R *.DFM}
procedure TWizAsignaRelaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_DISENO_ENCUESTA_PROCESOS_ASIGNACION_RELACIONES;
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizAsignaRelaciones_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          ET_CODIGO.Caption := InttoStr( Encuesta );
          ET_NOMBRE.Caption := Nombre;
     end;
     LlenaGrupoRelaciones;
      //DevEx
     Advertencia.Caption := 'Al aplicar este proceso se asignarán las relaciones seleccionadas para cada uno de los responsables de la encuesta.'
end;

procedure TWizAsignaRelaciones_DevEx.LlenaGrupoRelaciones;
const
     K_ALTURA_1 = 45;
     K_ALTURA_2 = 65;
     K_ALTURA_3 = 85;
     K_ALTURA_4 = 100;
var
   oCursor: TCursor;
   oLista : TStrings;
   iCuantos : Integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     oLista := TStringList.Create;
     try
        with cblRelaciones do
        begin
           //  LlenaPerfilEvaluadores( const oLista: TStrings; aFiltro: TFiltroRelaciones; const lPonerTodas: Boolean = FALSE; const lEsCombo: Boolean = TRUE );
           // dmEvaluacion.LlenaPerfilEvaluadores( cblRelaciones.Items.CheckListBox.Items, [ tevCliente, tevOtros ], FALSE, FALSE );

             dmEvaluacion.LlenaPerfilEvaluadores( oLista, [ tevCliente, tevOtros ], FALSE, FALSE );
             with oLista do
             begin
                  for iCuantos:=0 to ( Count - 1 ) do
                  begin
                       Items.Add.Text :=oLista.Strings[iCuantos];
                  end;
             end;



               //(clbBDFuente.Items.Add).Text := FieldByName('DB_CODIGO').AsString + ': ' + FieldByName('DB_DESCRIP').AsString;


             {
             Items.BeginUpdate;
             try
                i := 0;
                while i < ( Items.Count ) do
                begin
                     if( ( Integer( Items.Objects[i] ) = Ord( tevCliente ) ) or
                         ( Integer( Items.Objects[i] ) = Ord( tevOtros ) ) )then
                     begin
                          Items.Delete(i);
                          i := i - 1;
                     end;
                     i := i + 1;
                end;
             finally
                    Items.EndUpdate;
             end;
             }
             iCuantos := Items.Count;
             Visible := ( iCuantos > 0 );
             gbRelaciones.Visible := Visible;
             if Visible then
             begin
                  case iCuantos of
                       1: gbRelaciones.Height := K_ALTURA_1;
                       2: gbRelaciones.Height := K_ALTURA_2;
                       3: gbRelaciones.Height := K_ALTURA_3;
                       4: gbRelaciones.Height := K_ALTURA_4;
                  else
                      gbRelaciones.Height := 0;
                  end;
                  ItemIndex := 0;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
            oLista.Free;
     end;
end;



procedure TWizAsignaRelaciones_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
      if Wizard.EsPaginaActual( Parametros ) and Wizard.Adelante then
      begin
           if( not cblRelaciones.Visible )then
           begin
                CanMove := FALSE;
                ZetaDialogo.ZError( 'Asignacion de relaciones',  'ˇ No se tienen registrados perfiles de evaluadores compatibles con el proceso !', 0 );
           end;
      end;
end;

procedure TWizAsignaRelaciones_DevEx.CargaParametros;
var
   i: Integer;
   sRelaciones, sRelacionesDesc: String;
begin
     inherited;

     with Descripciones do
     begin
          AddString( 'Encuesta', ET_CODIGO.Caption + ' = ' + ET_NOMBRE.Caption );
          for i:=0 to ( cblRelaciones.Items.Count - 1 )do
          begin
                // cblRelaciones.Checked[ i ]      old
               if( cblRelaciones.Items.Items[i].Checked  )then
               begin
                    sRelaciones := ConcatString( sRelaciones, InttoStr( Integer( cblRelaciones.Items.Objects[i] ) ), ',' );
                   // sRelacionesDesc := ConcatString( sRelacionesDesc, cblRelaciones.Items.Strings[ i ], ',' );
                    sRelacionesDesc := ConcatString( sRelacionesDesc, cblRelaciones.Items.Items[i].Text, ',');
               end;
          end;
          AddString( 'Relaciones', sRelacionesDesc );
     end;

    // inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'Encuesta', dmCliente.Encuesta );
          AddString( 'Relaciones', sRelaciones );
     end;
end;

procedure TWizAsignaRelaciones_DevEx.CargaListaVerificacion;
begin
     dmProcesos.AsignaRelacionesGetLista( ParameterList );
end;

function TWizAsignaRelaciones_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TAsignaRelacionesGridSelect_DevEx );
end;

function TWizAsignaRelaciones_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AsignaRelaciones( ParameterList, Verificacion );
end;



end.
