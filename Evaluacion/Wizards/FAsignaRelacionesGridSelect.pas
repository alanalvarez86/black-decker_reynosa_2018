unit FAsignaRelacionesGridSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid, Db, StdCtrls, Grids, DBGrids, ZetaDBGrid, Buttons,
  ExtCtrls;

type
  TAsignaRelacionesGridSelect = class(TBaseGridSelect)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AsignaRelacionesGridSelect: TAsignaRelacionesGridSelect;

implementation

{$R *.DFM}

end.
