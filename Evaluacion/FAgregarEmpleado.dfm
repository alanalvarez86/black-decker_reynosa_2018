inherited AgregarEmpleado: TAgregarEmpleado
  Left = 437
  Top = 144
  Caption = 'Agregar Evaluador'
  ClientHeight = 390
  ClientWidth = 434
  OldCreateOrder = True
  Position = poDesigned
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 354
    Width = 434
    inherited OK: TBitBtn
      Left = 266
      Caption = '&Agregar'
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 351
      Caption = '&Salir'
      Kind = bkClose
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 434
    Height = 57
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'Encuesta:'
    end
    object ztbTitulo: TZetaTextBox
      Left = 60
      Top = 6
      Width = 357
      Height = 17
      AutoSize = False
      Caption = 'ztbTitulo'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label2: TLabel
      Left = 6
      Top = 32
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
    end
    object ztbEmpleado: TZetaTextBox
      Left = 60
      Top = 30
      Width = 357
      Height = 17
      AutoSize = False
      Caption = 'ztbEmpleado'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 57
    Width = 434
    Height = 32
    Align = alTop
    TabOrder = 2
    object Label3: TLabel
      Left = 61
      Top = 9
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Buscar:'
    end
    object edBuscar: TEdit
      Left = 102
      Top = 5
      Width = 250
      Height = 21
      MaxLength = 30
      TabOrder = 0
      OnKeyUp = edBuscarKeyUp
    end
  end
  object ZetaDBGrid1: TZetaDBGrid
    Left = 0
    Top = 89
    Width = 434
    Height = 231
    Align = alClient
    DataSource = dsAgregaEmp
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = ZetaDBGrid1DblClick
    Columns = <
      item
        Alignment = taRightJustify
        Color = clInfoBk
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'N�mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 330
        Visible = True
      end>
  end
  object Panel3: TPanel
    Left = 0
    Top = 320
    Width = 434
    Height = 34
    Align = alBottom
    TabOrder = 4
    object Label4: TLabel
      Left = 56
      Top = 10
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Relaci�n:'
    end
    object zkcRelacion: TZetaKeyCombo
      Left = 107
      Top = 6
      Width = 250
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
    end
  end
  object dsAgregaEmp: TDataSource
    Left = 376
    Top = 57
  end
end
