unit FSistEditUsuarios_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, StdCtrls, DBCtrls, Mask, ComCtrls, ExtCtrls, Buttons,
     FSistBaseEditUsuarios_DevEx,
     ZetaDBTextBox,
     ZetaKeyCombo,
     ZetaKeyLookup,
     ZetaNumero,
     ZetaSmartLists;

type
  TSistEditUsuarios_DevEx = class(TSistBaseEditUsuarios_DevEx)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditUsuarios_DevEx: TSistEditUsuarios_DevEx;

implementation

{$R *.DFM}

end.
