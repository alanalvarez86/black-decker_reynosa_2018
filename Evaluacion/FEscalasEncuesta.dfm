inherited EscalasEncuesta: TEscalasEncuesta
  Left = 464
  Top = 230
  Caption = 'Escalas de Encuesta'
  ClientHeight = 228
  ClientWidth = 482
  ExplicitWidth = 482
  ExplicitHeight = 228
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 482
    ExplicitWidth = 482
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 223
      ExplicitWidth = 223
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 217
        ExplicitLeft = 413
      end
    end
  end
  object pnBoton: TPanel [1]
    Left = 0
    Top = 19
    Width = 482
    Height = 36
    Hint = 'Crear escalas'
    Align = alTop
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    object btnCopiaEscalas: TcxButton
      Left = 10
      Top = 5
      Width = 115
      Height = 26
      Hint = 'Copiar escalas'
      Caption = 'Copiar escalas'
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        2000000000000009000000000000000000000000000000000000D1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD5DA18FFF1F2AFFFD5DA18FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFDCE03CFFDDE0
        40FFDDE040FFDDE040FFD5DA18FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD5DA18FFF8F9D7FFFFFFFFFFF8F9D7FFD5DA
        18FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFEBED8FFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFE2E560FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD5DA18FFF8F9D7FFFFFFFFFFFFFFFFFFFFFFFFFFF8F9
        D7FFD5DA18FFD1D600FFD1D600FFD1D600FFEBED8FFFFFFFFFFFFCFCEFFFD4D9
        10FFD1D600FFFFFFFFFFE2E560FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD5DA18FFF8F9D7FFFEFEFBFFEEF0A3FFFFFFFFFFFFFFFFFFFFFF
        FFFFF8F9D7FFD5DA18FFD1D600FFEBED8FFFFFFFFFFFFFFFFFFFFFFFFFFFF4F5
        C3FFD4D910FFFFFFFFFFE2E560FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFF1F2AFFFFEFEFBFFE4E76CFFF8F9D7FFFBFCEBFFFEFEFBFFFFFF
        FFFFFFFFFFFFE7E978FFEBED8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF9FADFFFFFFFFFFFE0E454FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD5DA18FFE3E664FFF8F9D7FFFBFBE7FFE3E664FFFDFDF3FFFFFF
        FFFFEBED8FFFEBED8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFEBED8FFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD5DA18FFF3F5BFFFE3E664FFFDFDF3FFFFFFFFFFEBED
        8FFFEBED8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEBED8FFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFF6F7CBFFFFFFFFFFEBED8FFFEBED
        8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBED
        8FFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD5DA18FFE7E978FFEBED8FFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBED8FFFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFEBED8FFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBED8FFFE9EB83FFDCE0
        3CFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFE9EC87FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBED8FFFEBED8FFFFFFFFFFFFDFD
        F3FFDCE03CFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD4D910FFDCE03CFFFDFDF3FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFEBED8FFFEBED8FFFFFFFFFFFFFFFFFFFFFFF
        FFFFFDFDF3FFDCE03CFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFEBED8FFFF8F9D7FFD5DA18FFDCE03CFFFDFDF3FFFFFF
        FFFFFFFFFFFFFFFFFFFFEBED8FFFEBED8FFFFFFFFFFFFDFDF3FFFEFEFBFFFFFF
        FFFFFFFFFFFFFDFDF3FFDCE03CFFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFDADE30FFFFFFFFFFFFFFFFFFF8F9D7FFD5DA18FFDCE03CFFFDFD
        F3FFFFFFFFFFEBED8FFFE4E76CFFFFFFFFFFFDFDF3FFE3E664FFFBFCEBFFFFFF
        FFFFFFFFFFFFFFFFFFFFF6F7CBFFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFF3F4BBFFFFFFFFFFFFFFFFFFF8F9D7FFD5DA18FFDCE0
        3CFFE9EC87FFD1D600FFD3D80CFFF2F3B7FFE3E664FFFBFBE7FFFBFBE7FFEEF0
        9FFFFFFFFFFFF8F9D7FFD5DA18FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD7DC24FFFAFAE3FFFFFFFFFFFFFFFFFFF8F9D7FFD5D9
        14FFD1D600FFD1D600FFD1D600FFD1D600FFF0F1ABFFFBFBE7FFE3E664FFFDFD
        F3FFF8F9D7FFD5DA18FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD5DA18FFF3F5BFFFFFFFFFFFF0F1ABFFD2D7
        04FFD1D600FFD1D600FFD1D600FFD1D600FFD3D80CFFDFE350FFFDFDF3FFF8F9
        D7FFD5DA18FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFDDE040FFD2D704FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD2D704FFEAEC8BFFD5DA
        18FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
        00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = btnCopiaEscalasClick
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 55
    Width = 482
    Height = 173
    TabOrder = 2
    ExplicitTop = 55
    ExplicitWidth = 482
    ExplicitHeight = 173
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      Styles.Content = cxStyle6
      Styles.Header = cxStyle7
      Styles.Indicator = cxStyle7
      object EE_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'EE_CODIGO'
        Styles.Content = cxStyle8
        Width = 75
      end
      object EE_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'EE_NOMBRE'
        Styles.Content = cxStyle9
        Width = 300
      end
      object EE_NIVELES: TcxGridDBColumn
        Caption = 'Niveles'
        DataBinding.FieldName = 'EE_NIVELES'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        Styles.Content = cxStyle10
      end
    end
    object ZetaDBGridDBTableView1: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = DataSource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      Styles.Content = cxStyle1
      Styles.Header = cxStyle2
      Styles.Indicator = cxStyle2
      object ZetaDBGridDBTableView1EE_CODIGO: TcxGridDBColumn
        DataBinding.FieldName = 'EE_CODIGO'
        Styles.Content = cxStyle3
        Width = 75
      end
      object ZetaDBGridDBTableView1EE_NOMBRE: TcxGridDBColumn
        DataBinding.FieldName = 'EE_NOMBRE'
        Styles.Content = cxStyle4
        Width = 300
      end
      object ZetaDBGridDBTableView1EE_NIVELES: TcxGridDBColumn
        DataBinding.FieldName = 'EE_NIVELES'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        Styles.Content = cxStyle5
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 392
    Top = 0
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 144
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
  end
  object cxStyleRepository2: TcxStyleRepository
    Left = 184
    PixelsPerInch = 96
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
  end
end
