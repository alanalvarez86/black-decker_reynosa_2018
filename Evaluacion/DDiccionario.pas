unit DDiccionario;

interface
{$INCLUDE DEFINES.INC}
uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBaseTressDiccionario,
     DBaseDiccionario,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     ZetaCommonLists;

type
  TdmDiccionario = class(TdmBaseTressDiccionario)
    procedure cdsDiccionAlModificar(Sender: TObject);
  private
    { Private declarations }
  protected
  public
    { Public declarations }
    procedure SetLookupNames;override;
    procedure GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);override;
    function GetValorActivo({$ifdef RDD}const eValor: eRDDValorActivo{$else}const sValor: string{$endif}): string; override;
  end;

var
  dmDiccionario: TdmDiccionario;

implementation
uses DCliente,
     ZetaCommonClasses,
     FEditDiccion_DevEx,
     ZBaseEdicion_DevEx;

const
     K_ENCUESTA = '@ENCUESTA';

{$R *.DFM}

procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
     //dmSeleccion.SetLookupNames;
end;

procedure TdmDiccionario.cdsDiccionAlModificar(Sender: TObject);
begin
     inherited;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditDiccion_DevEx, TEditDiccion_DevEx );

end;

procedure TdmDiccionario.GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);
begin
     inherited GetListaClasifiModulo( oLista, lMigracion );
     {$ifdef RDD}
     {$else}
     AgregaClasifi( oLista, crEvaluacion );
     {$endif}
end;

function TdmDiccionario.GetValorActivo({$ifdef RDD}const eValor: eRDDValorActivo{$else}const sValor: string{$endif}): string;
begin
     {$ifdef RDD}
     with dmCliente do
     begin
          case eValor  of
               vaEvaluacion: Result := IntToStr( Encuesta );
          else
              Result := VACIO;
          end;
     end;
     {$else}
     with dmCliente do
     begin
          if ( sValor = K_ENCUESTA ) then
             Result := IntToStr( Encuesta )
          else
              Result := inherited GetValorActivo(  sValor );
     end;
     {$endif}
end;

end.
