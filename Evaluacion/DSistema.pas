unit DSistema;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBaseSistema,
     ZetaClientDataSet,
     ZetaServerDataSet;

type
  TdmSistema = class(TdmBaseSistema)
    cdsAdicionalesAccesos: TZetaClientDataSet;
    cdsGruposAdic: TZetaLookupDataSet;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmSistema: TdmSistema;

implementation

{$R *.DFM}

end.
