inherited Evaluadores: TEvaluadores
  Left = 279
  Top = 237
  Caption = 'Evaluadores'
  ClientHeight = 371
  ClientWidth = 763
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 763
    inherited ValorActivo2: TPanel
      Width = 504
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 763
    Height = 64
    Align = alTop
    TabOrder = 1
    object Label2: TLabel
      Left = 650
      Top = 12
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Cantidad:'
      Visible = False
    end
    object ztbCuantos: TZetaTextBox
      Left = 698
      Top = 10
      Width = 60
      Height = 17
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      ShowAccelChar = False
      Visible = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label3: TLabel
      Left = 26
      Top = 37
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = 'Evaluador:'
    end
    object btnBuscaEmp: TSpeedButton
      Left = 734
      Top = 31
      Width = 23
      Height = 22
      Hint = 'Filtrar Lista Por Empleado'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
      ParentShowHint = False
      ShowHint = True
      Visible = False
      OnClick = btnBuscaEmpClick
    end
    object Label1: TLabel
      Left = 10
      Top = 12
      Width = 67
      Height = 13
      Alignment = taRightJustify
      Caption = 'Evaluaciones:'
    end
    object znUsuario: TZetaNumero
      Left = 639
      Top = 32
      Width = 93
      Height = 21
      Mascara = mnEmpleado
      TabOrder = 2
      UseEnterKey = True
      Visible = False
    end
    object btnFiltrar: TBitBtn
      Left = 400
      Top = 9
      Width = 97
      Height = 45
      Caption = 'Filtrar'
      TabOrder = 1
      OnClick = btnFiltrarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777777777777770000070000077777000E00BFBFB07777777E0BFBF000777
        7700E0FBFBFBF0777700E0BFBF0000007777E0FBFBFBFBFB0799E0BF00000000
        7799000BFB077777777777700077777777007777777777777700777777777777
        7777777777777777700077777777777770007777777777777777}
    end
    object US_CODIGO: TZetaKeyLookup
      Left = 79
      Top = 33
      Width = 314
      Height = 21
      TabOrder = 0
      TabStop = True
      WidthLlave = 70
    end
    object EV_STATUS: TComboBox
      Left = 79
      Top = 8
      Width = 157
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
    end
  end
  object Grid1: TZetaDBGrid [2]
    Left = 0
    Top = 83
    Width = 763
    Height = 269
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDrawColumnCell = Grid1DrawColumnCell
    Columns = <
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'N�mero'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 250
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'SE_TOTAL'
        Title.Alignment = taRightJustify
        Title.Caption = 'Evaluaciones'
        Width = 70
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'SE_NUEVA'
        Title.Alignment = taRightJustify
        Title.Caption = 'Nuevas'
        Width = 50
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'SE_PROCESO'
        Title.Alignment = taRightJustify
        Title.Caption = 'En proceso'
        Width = 60
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'SE_TERMINA'
        Title.Alignment = taRightJustify
        Title.Caption = 'Terminadas'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SE_STATUS'
        Title.Caption = 'Status'
        Width = 90
        Visible = True
      end>
  end
  object StatusBar1: TStatusBar [3]
    Left = 0
    Top = 352
    Width = 763
    Height = 19
    Panels = <
      item
        Text = 'Cantidad:'
        Width = 50
      end>
    SimplePanel = False
  end
  inherited DataSource: TDataSource
    Left = 584
    Top = 8
  end
end
