unit FEditPerfilEvaluador;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx,
  Db, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons, StdCtrls,
  ZetaDBTextBox, ZetaKeyLookup, ZetaNumero, Mask, ZetaKeyCombo, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TEditPerfilEvaluador = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    ztbTitulo: TZetaTextBox;
    Label2: TLabel;
    lblImportancia: TLabel;
    lblMinimo: TLabel;
    Label5: TLabel;
    lblPeso: TLabel;
    lblMaximo: TLabel;
    ER_TIPO: TZetaDBKeyCombo;
    ER_NOMBRE: TDBEdit;
    ER_NUM_MIN: TZetaDBNumero;
    ER_NUM_MAX: TZetaDBNumero;
    ER_POS_PES: TZetaDBKeyLookup_DevEx;
    ER_PESO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure SetControles;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  EditPerfilEvaluador: TEditPerfilEvaluador;

implementation

{$R *.DFM}
uses DEvaluacion,
     ZHelpContext,
     ZetaCommonclasses,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaBuscador_DevEx,
     DCliente;

{ TEditPerfilEvaluador }
procedure TEditPerfilEvaluador.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_PERFIL_EVAL;
     FirstControl := ER_TIPO;
     ER_POS_PES.LookupDataSet := dmEvaluacion.cdsNivelesEnc;
     HelpContext := H_DISENO_ENCUESTA_PERFIL_EVALUADORES_AGREGAR_REGISTRO;
end;

procedure TEditPerfilEvaluador.Connect;
begin
     with dmEvaluacion do
     begin
          SetControles;
          DataSource.DataSet := cdsPerfilEvaluador;
     end;
end;

procedure TEditPerfilEvaluador.SetControles;
var
   lLlenoPesEsc: Boolean;
begin
     ztbTitulo.Caption := InttoStr( dmCliente.Encuesta ) + ' = ' + dmCliente.Nombre;
     lLlenoPesEsc := strLleno( dmCliente.PesoEsc );
     with dmEvaluacion.cdsNivelesEnc do
     begin
          Refrescar;
          if( lLlenoPesEsc )then
          begin
               ER_POS_PES.DataField := 'ER_POS_PES';
               ER_POS_PES.Filtro := ' ET_CODIGO = '+ inttoStr(dmCliente.Encuesta) + ' and EE_CODIGO = ' + EntreComillas( dmCliente.PesoEsc )
          end
          else
          begin
               ER_POS_PES.DataField := VACIO;
               ER_POS_PES.Filtro := VACIO;
          end;
     end;
     lblImportancia.Enabled := lLlenoPesEsc;
     ER_POS_PES.Enabled := lLlenoPesEsc;
     lblPeso.Enabled := not lLlenoPesEsc;
     ER_PESO.Enabled := not lLlenoPesEsc;
end;

procedure TEditPerfilEvaluador.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Perfil de evaluador', 'ENC_RELA', 'ER_TIPO', dmEvaluacion.cdsPerfilEvaluador );
end;

procedure TEditPerfilEvaluador.EscribirCambios;
begin
     if( dmEvaluacion.ModificaDisenoEncuesta )then
         inherited EscribirCambios;
end;

end.
