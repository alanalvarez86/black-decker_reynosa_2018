unit FEvaluaciones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx,
  Db, ExtCtrls, StdCtrls, Buttons, Mask, ZetaNumero,
  ZetaDBTextBox, Grids, DBGrids, ZetaDBGrid, ComCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxTextEdit, cxClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, ZetaKeyLookup_DevEx;

type
  TEvaluaciones = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label3: TLabel;
    btnFiltrar: TcxButton;
    Label4: TLabel;
    CB_CODIGO_GRID: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    US_CODIGO_GRID: TcxGridDBColumn;
    US_NOMBRE: TcxGridDBColumn;
    EV_RELACIO: TcxGridDBColumn;
    EV_STATUS: TcxGridDBColumn;
    EV_FIN_PRE: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    CB_CODIGO: TZetaKeyLookup_DevEx;
    US_CODIGO: TZetaKeyLookup_DevEx;
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    Procedure ConfigAgrupamiento;
    { Private declarations }
    procedure CargaFiltros;
    function ObtieneFiltro( const iStatus, iEmpleado, iEvaluador, iRelacion: Integer ): String;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  Evaluaciones: TEvaluaciones;

implementation

{$R *.DFM}

uses DEvaluacion,
     DSistema,
     DCliente,
     ZHelpContext,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaBuscaEmpleado_DevEx;

{ TEvaluaciones }
procedure TEvaluaciones.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     CB_CODIGO.LookupDataSet := dmCliente.cdsEmpleadoLookup;
     US_CODIGO.LookupDataset := dmSistema.cdsUsuarios;
     HelpContext := H_RESULTADOS_ENCUESTA_EVALUACIONES;
end;

procedure TEvaluaciones.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
     Refresh;
     DoBestFit;
end;

procedure TEvaluaciones.Agregar;
begin
     dmEvaluacion.cdsEvaluaciones.Agregar;
     DoBestFit;
end;

procedure TEvaluaciones.Borrar;
begin
     dmEvaluacion.cdsEvaluaciones.Borrar;
     DoBestFit;
end;

procedure TEvaluaciones.Connect;
begin
     with dmEvaluacion do
     begin
          if not cdsEvaluaciones.Active or cdsEvaluaciones.HayQueRefrescar then
          begin
               dmSistema.cdsUsuarios.Conectar;
               cdsEvaluaciones.Filtered := False;
               CargaFiltros;
               cdsEvaluaciones.ResetDataChange;
          end;
          DataSource.DataSet := cdsEvaluaciones;
     end;
end;

procedure TEvaluaciones.ImprimirForma;
begin
  inherited;
end;

procedure TEvaluaciones.Modificar;
begin
    dmEvaluacion.cdsEvaluaciones.Modificar;
    DoBestFit;
end;

function TEvaluaciones.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden modificar evaluaciones desde esta forma';
     Result := False;
end;

function TEvaluaciones.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden agregar evaluaciones desde esta forma';
     Result := False;
end;

function TEvaluaciones.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden borrar evaluaciones desde esta forma';
     Result := False;
end;

procedure TEvaluaciones.Refresh;
begin
     CargaFiltros;
     DoBestFit;
end;

procedure TEvaluaciones.btnFiltrarClick(Sender: TObject);
begin
     CargaFiltros;
     DoBestFit;
end;

procedure TEvaluaciones.CargaFiltros;
var
   sFiltro: String;
   iStatus, iEmpleado, iEvaluador, iRelacion: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion.cdsEvaluaciones do
        begin
             DisableControls;
             try
                // iStatus := EV_STATUS.ItemIndex;  old
                iEmpleado :=  CB_CODIGO.Valor;  //znEmpleado.ValorEntero;
                iEvaluador := US_CODIGO.Valor;
                // iRelacion := EV_RELACIO.ItemIndex;    old
                //Dchavez: Mandamos un valor negativo para que no filtre por status ni por relacion
                sFiltro := ObtieneFiltro( -1, iEmpleado, iEvaluador, -1 );
                dmEvaluacion.ObtieneEvaluaciones( sFiltro );
             finally
                    EnableControls;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TEvaluaciones.ObtieneFiltro( const iStatus, iEmpleado, iEvaluador, iRelacion: Integer ): String;
const
     Q_STATUS = ' and ( EV_STATUS = %d )';
     Q_EMPLEADO = ' and ( EVALUA.CB_CODIGO = %d )';
     Q_USUARIO = ' and ( EVALUA.US_CODIGO = %d )';
     Q_RELACION = ' and ( EV_RELACIO = %d )';
var
   iIndice: Integer;
   sStatus, sEmpleado, sEvaluador, sRelacion, sFiltro: String;
begin
     sFiltro := VACIO;
     sStatus := VACIO;
     sEmpleado := VACIO;
     sEvaluador := VACIO;
     sRelacion := VACIO;
     if( iStatus >= 0 )then
         sStatus := Format( Q_STATUS, [ iStatus ] );
     if( iEmpleado > 0 )then
         sEmpleado := Format( Q_EMPLEADO, [ iEmpleado ] );
     if( iEvaluador > 0 )then
         sEvaluador := Format( Q_USUARIO, [ iEvaluador ] );
     if( iRelacion >= 0 )then
         sRelacion := Format( Q_RELACION, [ iRelacion ] );
     sFiltro := sStatus + sEmpleado + sEvaluador + sRelacion;
     iIndice := Pos( 'and', sFiltro );
     if( iIndice > 0 )then
         Result := Copy( sFiltro, ( iIndice + 3 ), Length(sFiltro) )
     else
         Result := sFiltro;
end;


procedure TEvaluaciones.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     CB_CODIGO_GRID.Options.Grouping := FALSE;
     PRETTYNAME.Options.Grouping := FALSE;
     US_CODIGO_GRID.Options.Grouping := FALSE;
     US_NOMBRE.Options.Grouping := FALSE;
     EV_RELACIO.Options.Grouping := TRUE;
     EV_STATUS.Options.Grouping := TRUE;
     EV_FIN_PRE.Options.Grouping := FALSE;
end;

end.
