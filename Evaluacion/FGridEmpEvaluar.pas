unit FGridEmpEvaluar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ZBaseGridEdicion_DevEx,
     Db, Grids, DBGrids, ZetaDBGrid, DBCtrls,
     ZetaSmartLists, Buttons, ExtCtrls, StdCtrls, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarExtItems, dxBar, cxClasses,
     Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TGridEmpEvaluar = class(TBaseGridEdicion_DevEx)
    procedure FormCreate(Sender: TObject);
  private
    procedure BuscaEmpleado;
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Buscar; override;
  public
    { Public declarations }
  end;

var
  GridEmpEvaluar: TGridEmpEvaluar;

implementation

uses DEvaluacion,
     ZetaBuscaEmpleado_DevEx,
     ZAccesosTress,
     DCliente;

{$R *.DFM}
procedure TGridEmpEvaluar.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_AGREGAR_EMP_EVAL;
     //HelpContext := H00107_Registro_de_Consultas_Globales;
end;

procedure TGridEmpEvaluar.Buscar;
begin
     inherited;
     with ZetaDBGrid.SelectedField do
     begin
          if( FieldName = 'CB_CODIGO') then
              BuscaEmpleado;
     end;
end;

procedure TGridEmpEvaluar.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmEvaluacion.cdsAgregaEmpEvaluar do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'ET_CODIGO' ).AsInteger := dmCliente.Encuesta;
               FieldByName( 'CB_CODIGO' ).AsString := sKey;
               FieldByName( 'PRETTYNAME' ).AsString := sDescription;
          end;
     end;
end;



procedure TGridEmpEvaluar.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmEvaluacion do
     begin
          cdsAgregaEmpEvaluar.Refrescar;
          DataSource.DataSet:= cdsAgregaEmpEvaluar;
     end;
end;

end.
