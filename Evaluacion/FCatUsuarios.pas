unit FCatUsuarios;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
     cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
     cxDBData, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel,
     cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
     cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons;

type
  TCatUsuarios = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    EBuscaNombre: TEdit;
    BBusca: TcxButton;
    US_CODIGO_GRID: TcxGridDBColumn;
    US_NOMBRE_GRID: TcxGridDBColumn;
    US_EMAIL_GRID: TcxGridDBColumn;
    US_FORMATO_GRID: TcxGridDBColumn;
    US_JEFE_GRID: TcxGridDBColumn;
    NOM_JEFE_GRID: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure EBuscaNombreChange(Sender: TObject);
    procedure BBuscaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
    procedure FiltraUsuario;
    procedure LimpiaGrid;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CatUsuarios: TCatUsuarios;

implementation

{$R *.DFM}

uses DSistema,
     DEvaluacion,
     ZHelpContext,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{ *********** TSistUsuarios *********** }

procedure TCatUsuarios.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_SISTEMA_USUARIOS;
     IndexDerechos := D_EVAL_REPORTA;
end;

procedure TCatUsuarios.FormShow(Sender: TObject);
begin
      ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('US_CODIGO'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatUsuarios.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmEvaluacion do
     begin
          cdsUsuariosRol.Conectar;
          DataSource.DataSet:= cdsUsuariosRol;
     end;
end;

procedure TCatUsuarios.Refresh;
begin
     dmEvaluacion.cdsUsuariosRol.Refrescar;
     DoBestFit;
end;

function TCatUsuarios.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se permite agregar usuarios en esta pantalla';
end;

function TCatUsuarios.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se permite borrar usuarios en esta pantalla';
end;

procedure TCatUsuarios.Modificar;
begin
     dmEvaluacion.cdsUsuariosRol.Modificar;
     DoBestFit;
end;

procedure TCatUsuarios.EBuscaNombreChange(Sender: TObject);
begin
     inherited;
     BBusca.Down := Length(EBuscaNombre.Text)>0;
     FiltraUsuario;
     DoBestFit;
end;

procedure TCatUsuarios.BBuscaClick(Sender: TObject);
begin
     inherited;
     FiltraUsuario;
     DoBestFit;
end;

procedure TCatUsuarios.FiltraUsuario;
begin
     with dmEvaluacion.cdsUsuariosRol do
     begin
          Filtered := BBusca.Down;
          Filter := 'UPPER(US_NOMBRE) like '+ Chr(39) + '%' + UpperCase( EBuscaNombre.Text ) + '%' + Chr(39);
     end;
     LimpiaGrid;
end;

procedure TCatUsuarios.LimpiaGrid;
begin
   //  ZetadbGrid.SelectedRows.Clear;      DACP
end;

procedure TCatUsuarios.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     US_CODIGO_GRID.Options.Grouping  := FALSE;
     US_NOMBRE_GRID.Options.Grouping:= FALSE;
     US_EMAIL_GRID.Options.Grouping:= TRUE;
     US_FORMATO_GRID.Options.Grouping:= TRUE;
     US_JEFE_GRID.Options.Grouping:= TRUE;
     NOM_JEFE_GRID.Options.Grouping:= FALSE;
end;

end.


