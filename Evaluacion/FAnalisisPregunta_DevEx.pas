unit FAnalisisPregunta_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, StdCtrls, Buttons, Mask, ZetaNumero,
  ZetaDBTextBox, Grids, DBGrids, ZetaDBGrid, TeEngine, Series, TeeProcs,
  Chart, DBChart, ZetaKeyLookup, ComCtrls, ZBaseGridLectura_DevEx,
  VclTee.TeeGDIPlus, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Vcl.Menus, System.Actions,
  Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  cxSplitter, ZetaKeyLookup_DevEx, cxButtons;

type
  TAnalisisPregunta_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    ztbCuantos: TZetaTextBox;
    lblPrimeros: TLabel;
    edPrimeros: TZetaNumero;
    znPromedios: TComboBox;
    btnFiltrar: TcxButton;
    EV_RELACIO: TComboBox;
    Grafica: TDBChart;
    Series1: THorizBarSeries;
    EC_ORDEN: TZetaKeyLookup_DevEx;
    EP_NUMERO: TcxGridDBColumn;
    EP_DESCRIP: TcxGridDBColumn;
    CUANTAS: TcxGridDBColumn;
    MINIMO: TcxGridDBColumn;
    PROMEDIO: TcxGridDBColumn;
    MAXIMO: TcxGridDBColumn;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    cxSplitter1: TcxSplitter;
    procedure znPromediosChange(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Grid1TitleClick(Column: TColumn);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure InicializaValores;
    procedure LlenaListaRelacion;
    procedure CuantosRegistros;
    procedure CreaGrafica;
    procedure SetControles;
    procedure CargaFiltros;
    //procedure OrdenaGrid( const iOrdenarPor: Integer );
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  AnalisisPregunta_DevEx: TAnalisisPregunta_DevEx;

implementation

{$R *.DFM}
uses DEvaluacion,
     ZHelpContext,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists;

{ TAnalisisPregunta }
procedure TAnalisisPregunta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     with dmEvaluacion do
     begin
          cdsCriteriosEnc.Conectar;
          EC_ORDEN.LookupDataset := dmEvaluacion.cdsCriteriosEnc;
     end;
     HelpContext := H_RESULTADOS_ENCUESTA_ANALISIS_PREGUNTA;
end;

procedure TAnalisisPregunta_DevEx.FormShow(Sender: TObject);
begin
     InicializaValores;
     SetControles;
     inherited;
     LlenaListaRelacion;
     CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
     ApplyMinWidth;
     ZetaDBGridDBTableView.ApplyBestFit();
     DoBestFit;
end;

procedure TAnalisisPregunta_DevEx.Agregar;
begin
     dmEvaluacion.cdsAnalisisPregunta.Agregar;
end;

procedure TAnalisisPregunta_DevEx.Borrar;
begin
     dmEvaluacion.cdsAnalisisPregunta.Borrar;
end;

procedure TAnalisisPregunta_DevEx.Connect;
begin
     with dmEvaluacion do
     begin
          if not cdsAnalisisPregunta.Active or cdsAnalisisPregunta.HayQueRefrescar then
          begin
               cdsCriteriosEnc.Conectar;
               CargaFiltros;
               CreaGrafica;
               cdsAnalisisPregunta.ResetDataChange;
          end;
          DataSource.DataSet := cdsAnalisisPregunta;
     end;
end;

procedure TAnalisisPregunta_DevEx.InicializaValores;
begin
     znPromedios.ItemIndex := 0;
     edPrimeros.Valor := 5;
     EC_ORDEN.Llave := VACIO;
end;

procedure TAnalisisPregunta_DevEx.LlenaListaRelacion;
begin
     with dmEvaluacion.cdsPerfilEvaluador do
     begin
          EV_RELACIO.Clear;
          Conectar;
          First;
          DisableControls;
          try
             EV_RELACIO.Items.AddObject( 'Todas', TObject( 0 ) );
             while not EOF do
             begin
                  EV_RELACIO.Items.AddObject( FieldByName('ER_NOMBRE').AsString, TObject( FieldByName('ER_TIPO').AsInteger ) );
                  Next;
             end;
          finally
                 EnableControls;
          end;
     end;
     EV_RELACIO.ItemIndex := 0;
end;

procedure TAnalisisPregunta_DevEx.ImprimirForma;
begin
  inherited;
end;

procedure TAnalisisPregunta_DevEx.Modificar;
begin
     dmEvaluacion.cdsAnalisisPregunta.Modificar;
end;

function TAnalisisPregunta_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede agregar el registro desde esta forma';
     Result := False;
end;

function TAnalisisPregunta_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede borrar el registro desde esta forma';
     Result := False;
end;

function TAnalisisPregunta_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede modificar el registro desde esta forma';
     Result := False;
end;

procedure TAnalisisPregunta_DevEx.Refresh;
begin
     //dmEvaluacion.cdsAnalisisPregunta.Refrescar;
     CargaFiltros;
end;

procedure TAnalisisPregunta_DevEx.CuantosRegistros;
begin
     //ztbCuantos.Caption := inttoStr( dmEvaluacion.cdsAnalisisPregunta.RecordCount );
     //StatusBar1.Panels[0].Text := 'Cantidad : ' + inttoStr( dmEvaluacion.cdsAnalisisPregunta.RecordCount );
end;

procedure TAnalisisPregunta_DevEx.CreaGrafica;
begin
     with Series1 do
     begin
          with dmEvaluacion.cdsAnalisisPregunta do
          begin
               Clear;
               DisableControls;
               try
                  Last;
                  while not BOF do
                  begin
                       AddBar( FieldByName('PROMEDIO').AsFloat, '# '+InttoStr(FieldByName('EP_NUMERO').AsInteger), clDefault );
                       Prior;
                  end;
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TAnalisisPregunta_DevEx.SetControles;
begin
     edPrimeros.Enabled := ( znPromedios.ItemIndex <> 0 );
     lblPrimeros.Enabled := edPrimeros.Enabled;
     if( edPrimeros.Enabled )then
     begin
          if( edPrimeros.Valor  = 0 )then
              edPrimeros.Valor := 5;
     end
     else
         edPrimeros.Valor := 0;
end;

procedure TAnalisisPregunta_DevEx.znPromediosChange(Sender: TObject);
begin
     inherited;
     SetControles;
end;

procedure TAnalisisPregunta_DevEx.btnFiltrarClick(Sender: TObject);
begin
     CargaFiltros;
     DoBestFit;
end;

procedure TAnalisisPregunta_DevEx.CargaFiltros;
var
   iMostrarProm, iRelacion, iMostrar: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion.cdsAnalisisPregunta do
        begin
             DisableControls;
             try
                iMostrarProm := znPromedios.ItemIndex;
                iMostrar := edPrimeros.ValorEntero;
                if EV_RELACIO.ItemIndex > 0 then
                   iRelacion := Integer( EV_RELACIO.Items.Objects[ EV_RELACIO.ItemIndex ] ) //EV_RELACIO.ItemIndex;
                else
                    iRelacion := -1;
                //iRelacion := EV_RELACIO.ItemIndex;
                //OrdenaGrid( iMostrarProm );
                dmEvaluacion.ObtenerAnalisisPreguntas( iMostrar, ( iRelacion ), StrToIntDef(EC_ORDEN.Llave, 0), 0{iMostrarProm} );
                CreaGrafica;
             finally
                    EnableControls;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     CuantosRegistros;
end;

{procedure TAnalisisPregunta.OrdenaGrid( const iOrdenarPor: Integer );
begin
     with dmEvaluacion.cdsAnalisisPregunta do
     begin
          case( iOrdenarPor ) of
                0: IndexName := 'PG_FOLIOIndex';
                1: IndexName := 'PROMEDIOIndexD';
                2: IndexName := 'PROMEDIOIndex';
          else
              IndexName := VACIO;
          end;
     end;
end;}

procedure TAnalisisPregunta_DevEx.Grid1TitleClick(Column: TColumn);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        //Grid1.OrdenarPor( Column );
        CreaGrafica;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
