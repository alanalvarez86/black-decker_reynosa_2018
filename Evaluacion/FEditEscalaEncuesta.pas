unit FEditEscalaEncuesta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon_DevEx, Db, Grids, DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls,
  DBCtrls, ZetaSmartLists, Buttons, StdCtrls, Mask, ZetaDBTextBox, dbclient,
  ZetaEdit, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  cxControls, dxBarBuiltInMenu, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxPC, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditEscalaEncuesta = class(TBaseEdicionRenglon_Devex)
    Label1: TLabel;
    ztbTitulo: TZetaTextBox;
    Label2: TLabel;
    Label3: TLabel;
    EE_NOMBRE: TDBEdit;
    EE_CODIGO: TZetaDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect;override;
    procedure HabilitaControles; override;
    procedure DoCancelChanges; override;
    procedure EscribirCambios; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  EditEscalaEncuesta: TEditEscalaEncuesta;

implementation

{$R *.DFM}
uses DEvaluacion,
     ZHelpContext,
     ZetaDialogo,
     ZAccesosTress,
     DCliente;

{ TEditEscalaEncuesta }
procedure TEditEscalaEncuesta.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_ESCALAS;
     FirstControl := EE_CODIGO;
     HelpContext:= H_DISENO_ENCUESTA_ESCALAS_ENCUESTA_AGREGAR_REGISTRO;
end;

procedure TEditEscalaEncuesta.FormShow(Sender: TObject);
begin
     inherited;
     Datos.TabVisible := False;
     Tabla.TabVisible := False;
     PageControl.ActivePage := Tabla;
     self.Height := 400;
     self.Width := 780;
     ztbTitulo.Caption := inttoStr(dmCliente.Encuesta) + ' = ' + dmCliente.Nombre;

end;

procedure TEditEscalaEncuesta.Connect;
begin
     with dmEvaluacion do
     begin
          DataSource.DataSet := cdsEscalasEncuesta;
          dsRenglon.DataSet := cdsEscalaNivEnc;
     end;
end;

procedure TEditEscalaEncuesta.DoCancelChanges;
begin
     dmEvaluacion.cdsEscalaNivEnc.CancelUpdates;
     inherited DoCancelChanges;
end;

procedure TEditEscalaEncuesta.HabilitaControles;
begin
     inherited HabilitaControles;
end;

function TEditEscalaEncuesta.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede agregar escala desde esta forma';
     Result := False;
end;

function TEditEscalaEncuesta.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede borrar escala desde esta forma';
     Result := False;
end;

procedure TEditEscalaEncuesta.OKClick(Sender: TObject);
begin
     inherited;
     with DataSource do
     begin
          if ( TClientDataSet( DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
             ( not ( DataSet.State in [ dsEdit, dsInsert ] ) ) then    //y no se tienen cambios pendientes
             Close;
     end;
end;

procedure TEditEscalaEncuesta.BBAgregarClick(Sender: TObject);
begin
     inherited;
      GridRenglones.OrdenarPor( GridRenglones.Columns[0] );
end;

procedure TEditEscalaEncuesta.EscribirCambios;
begin
     with dmEvaluacion do
     begin
          if( ModificaDisenoEncuesta )then
          begin
               cdsEscalasEncuesta.FieldByName('EE_NIVELES').AsInteger := cdsEscalaNivEnc.RecordCount;
               inherited;
          end;
     end;
end;

procedure TEditEscalaEncuesta.BBBorrarClick(Sender: TObject);
begin
     if not( dmEvaluacion.cdsEscalasEncuesta.IsEmpty )then
     begin
          if ( ZetaDialogo.ZWarningConfirm( 'Escalas de Encuesta', '� Desea borrar este rengl�n ?', 0, mbCancel ) ) then
               inherited;
     end;
end;

end.
