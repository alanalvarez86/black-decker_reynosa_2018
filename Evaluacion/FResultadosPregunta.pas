unit FResultadosPregunta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx,
  Db, ExtCtrls, StdCtrls, Mask, ZetaNumero, Buttons,
  ZetaDBTextBox, Grids, DBGrids, ZetaDBGrid, ComCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxTextEdit, cxClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons;

type
  TResultadosPregunta = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label3: TLabel;
    lblPrimeros: TLabel;
    btnFiltrar: TcxButton;
    znPromedios: TComboBox;
    edPrimeros: TZetaNumero;
    EP_NUMERO_GRID: TcxGridDBColumn;
    EP_DESCRIP_GRID: TcxGridDBColumn;
    SP_NUM_TOT_GRID: TcxGridDBColumn;
    SP_TOTAL_GRID: TcxGridDBColumn;
    EC_NOMBRE_GRID: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    procedure znPromediosChange(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
    procedure InicializaValores;
    procedure SetControles;
    procedure CargarFiltros;
    //procedure OrdenaGrid( const iOrdenarPor: Integer );
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  ResultadosPregunta: TResultadosPregunta;

implementation

{$R *.DFM}
uses DEvaluacion,
     ZHelpContext,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists;

{ TResultadosPregunta }
procedure TResultadosPregunta.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext := H_RESULTADOS_INDIVIDUALES_RESULTADOS_PREGUNTA;
end;

procedure TResultadosPregunta.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('EP_NUMERO'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TResultadosPregunta.Agregar;
begin
     dmEvaluacion.cdsResultadoPregunta.Agregar;
end;

procedure TResultadosPregunta.Borrar;
begin
     dmEvaluacion.cdsResultadoPregunta.Borrar;
end;

procedure TResultadosPregunta.Connect;
begin
     InicializaValores;
     SetControles;
     with dmEvaluacion do
     begin
          //cdsResultadoPregunta.Conectar;
          CargarFiltros;
          DataSource.DataSet := cdsResultadoPregunta;
     end;
end;

procedure TResultadosPregunta.InicializaValores;
begin
     znPromedios.ItemIndex := 0;
     edPrimeros.Valor := 10;
end;

procedure TResultadosPregunta.SetControles;
begin
     edPrimeros.Enabled := ( znPromedios.ItemIndex <> 0 );
     lblPrimeros.Enabled := edPrimeros.Enabled;
     if( edPrimeros.Enabled )then
     begin
          if( edPrimeros.Valor  = 0 )then
              edPrimeros.Valor := 10;
     end
     else
         edPrimeros.Valor := 0;
end;

procedure TResultadosPregunta.ImprimirForma;
begin
  inherited;
end;

procedure TResultadosPregunta.Modificar;
begin
     dmEvaluacion.cdsResultadoPregunta.Modificar;
end;

function TResultadosPregunta.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede agregar el registro desde esta forma';
     Result := False;
end;

function TResultadosPregunta.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede borrar el registro desde esta forma';
     Result := False;
end;
function TResultadosPregunta.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede modificar el registro desde esta forma';
     Result := False;
end;

procedure TResultadosPregunta.Refresh;
begin
     //dmEvaluacion.cdsResultadoPregunta.Refrescar;
     CargarFiltros;
     DoBestFit;
end;

procedure TResultadosPregunta.znPromediosChange(Sender: TObject);
begin
     inherited;
     SetControles;
end;

procedure TResultadosPregunta.CargarFiltros;
var
   iMostrarProm, iMostrar: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion.cdsResultadoPregunta do
        begin
             DisableControls;
             try
                iMostrarProm := znPromedios.ItemIndex;
                iMostrar := edPrimeros.ValorEntero;
                //OrdenaGrid( iMostrarProm );
                dmEvaluacion.ObtenerResultadoPregunta( iMostrar, iMostrarProm );
             finally
                    EnableControls;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

{procedure TResultadosPregunta.OrdenaGrid( const iOrdenarPor: Integer );
begin
     with dmEvaluacion.cdsResultadoPregunta do
     begin
          case( iOrdenarPor ) of
                0: IndexName := 'EP_NUMEROIndex';
                1: IndexName := 'SP_TOTALIndexD';
                2: IndexName := 'SP_TOTALIndex';
          else
              IndexName := VACIO;
          end;
     end;
end;}

procedure TResultadosPregunta.btnFiltrarClick(Sender: TObject);
begin
     CargarFiltros;
     DoBestFit;
end;

procedure TResultadosPregunta.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     //Agrupamiento
     EP_NUMERO_GRID.Options.Grouping  := FALSE;
     EP_DESCRIP_GRID.Options.Grouping := FALSE;
     SP_NUM_TOT_GRID.Options.Grouping := TRUE;
     SP_TOTAL_GRID.Options.Grouping   := FALSE;
     EC_NOMBRE_GRID.Options.Grouping  := TRUE;
     //Filtrado
     EP_NUMERO_GRID.Options.Filtering  := FALSE;
     EP_DESCRIP_GRID.Options.Filtering := FALSE;
     SP_NUM_TOT_GRID.Options.Filtering := TRUE;
     SP_TOTAL_GRID.Options.Filtering   := FALSE;
     EC_NOMBRE_GRID.Options.Filtering  := TRUE;
end;


end.
