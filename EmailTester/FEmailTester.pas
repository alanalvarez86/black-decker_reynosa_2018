unit FEmailTester;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Mask, ZetaFecha, ShellApi, ZetaHora, CheckLst;

type
  TEmailChecker = class(TForm)
    GroupBox1: TGroupBox;
    ETressEmailDir: TEdit;
    Label1: TLabel;
    SBPrograma: TSpeedButton;
    OpenDialog: TOpenDialog;
    eListaUsuarios: TEdit;
    eListaReportes: TEdit;
    eFecha: TZetaFecha;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cbFrecuencia: TComboBox;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    sbLinea: TSpeedButton;
    eLineaComando: TEdit;
    BtnEjecutar: TBitBtn;
    bLinea: TBitBtn;
    Label7: TLabel;
    eEmpresa: TEdit;
    BCalendarizar: TBitBtn;
    BkupDaysLBL: TLabel;
    BkupDays: TCheckListBox;
    BkupTimeLBL: TLabel;
    BkupTime: TZetaHora;
    HoraAMPM: TLabel;
    FechaDia: TLabel;
    procedure SBProgramaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnEjecutarClick(Sender: TObject);
    procedure sbLineaClick(Sender: TObject);
    procedure bLineaClick(Sender: TObject);
    procedure BCalendarizarClick(Sender: TObject);
    procedure eFechaChange(Sender: TObject);
    procedure BkupDaysClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BkupTimeExit(Sender: TObject);
    procedure eLineaComandoExit(Sender: TObject);
  private
    { Private declarations }
    FParams : string;
    function GetAtCommand: String;
    procedure SetControls;
    procedure ConstruirLinea;
    procedure Ejecutar;
    procedure ShowError(const sMessage: String);
    function HayDiasMarcados: Boolean;
  protected
    { Protected declarations }
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;

var
  EmailChecker: TEmailChecker;

implementation

uses ZetaWinAPITools, ZetaCommonTools, ZetaMessages;

{$R *.DFM}

procedure TEmailChecker.FormCreate(Sender: TObject);
begin
     FParams := '';

     BLinea.Caption := 'Construir' + CHR(13) + CHR(10) +
                       'L�nea de' + CHR(13) + CHR(10) +
                       'Comandos';
     BCalendarizar.Caption := 'Calendarizar' + CHR(13) + CHR(10) +
                              'Tarea';
     Environment;
     Application.UpdateFormatSettings := FALSE;
end;

procedure TEmailChecker.FormShow(Sender: TObject);
begin
     ETressEmailDir.Text := ZetaMessages.SetFileNameDefaultPath( 'TressEMail.exe' );
     eFecha.Valor := Date;
     cbFrecuencia.ItemIndex := 1;
     eListaUsuarios.Text := '';
     eListaReportes.Text := '';

     SetControls;
end;

procedure TEmailChecker.eFechaChange(Sender: TObject);
begin
     FechaDia.Caption := ZetaCommonTools.DiaSemana( eFecha.Valor );
end;

procedure TEmailChecker.BkupDaysClick(Sender: TObject);
begin
     SetControls;
end;

procedure TEmailChecker.BkupTimeExit(Sender: TObject);
begin
     SetControls;
end;

procedure TEmailChecker.eLineaComandoExit(Sender: TObject);
begin
     SetControls;
end;

procedure TEmailChecker.SBProgramaClick(Sender: TObject);
begin
     OpenDialog.FileName := ETressEmailDir.Text;
     if OpenDialog.Execute then
        ETressEmailDir.Text := OpenDialog.FileName;
end;

procedure TEmailChecker.BtnEjecutarClick(Sender: TObject);
begin
     ConstruirLinea;
     Ejecutar;
end;

procedure TEmailChecker.bLineaClick(Sender: TObject);
begin
     ConstruirLinea;
end;

procedure TEmailChecker.sbLineaClick(Sender: TObject);
begin
     Ejecutar;
end;

procedure TEmailChecker.Ejecutar;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
   iResult : integer;
begin
     iResult := ShellExecute( Application.MainForm.Handle,
                   nil,
                   StrPCopy( zFileName, ExtractFileName(ETressEmailDir.Text) ),
                   StrPCopy( zParams, FParams ),
                   StrPCopy( zDir, QuitaComillas(ExtractFilePath(eLineaComando.Text)) ),
                   SW_SHOW );

     if iResult <= 32 then
     ShowMessage( 'Error en la linea de Comandos' );

end;

procedure TEmailChecker.ConstruirLinea;
var
   sFecha, sFrecuencia : string;

 function GetTexto(const sTexto,sParam : string): string;
 begin
      if Trim(sTexto) <> '' then
      begin
           Result := sParam + sTexto;
      end;
 end;

begin
     if Trim(eEmpresa.Text) = '' then
        raise Exception.Create('El C�digo de la Empresa no puede quedar Vaci�');

     if cbFrecuencia.ItemIndex > 0 then
        sFrecuencia := 'FREC=' + Copy(cbFrecuencia.Text,1,1);
     if eFecha.Valor <> Date then
        sFecha := 'FECHA=' + FormatDateTime('dd/mm/yyyy',eFecha.Valor);
     FParams := 'EMPRESA=' + eEmpresa.Text  + ' ' +
                sFrecuencia + ' ' +
                sFecha + ' ' +
                GetTexto(eListaUsuarios.Text, 'USUARIOS=') + ' ' +
                GetTexto(eListaReportes.Text, 'REPORTES=') ;

     if (cbFrecuencia.ItemIndex = 0) AND
        (Trim(eListaUsuarios.Text) = '') AND
        (Trim(eListaReportes.Text) = '') then
     begin
          raise Exception.Create('Se debe Especificar la Frecuencia, si no se Especifican Lista de Usuarios o Reportes');
     end;
     eLineaComando.Text := '"' + ETressEmailDir.Text + '" ' + FParams;
end;

procedure TEmailChecker.ShowError( const sMessage: String );
begin
     Dialogs.ShowMessage( sMessage );
end;

function TEmailChecker.GetAtCommand: String;
const
     K_AT = ' %s /every:%s ';
     K_DAYS: array[ 0..6 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Su', 'M', 'T', 'W', 'Th', 'F', 'S' );
var
   i: Integer;
   sHora, sDays: String;
begin
     sDays := '';
     for i := 0 to 6 do
     begin
          if BkupDays.Checked[ i ] then
          begin
               if ( sDays <> '' ) then
                  sDays := sDays + ',';
               sDays := sDays + K_DAYS[ i ];
          end;
     end;
     with BkupTime do
     begin
          sHora := Format( '%s:%s', [ Copy( Valor, 1, 2 ), Copy( Valor, 3, 2 ) ] );
     end;
     Result := Format( K_AT, [ sHora, sDays ] );
end;

procedure TEmailChecker.BCalendarizarClick(Sender: TObject);
var
   zFileName, zParams, zDir: array[ 0..255 ] of Char;
   iValue: DWord;
   FWindowsSystemFolder : string;
begin
     FWindowsSystemFolder := ZetaWinAPITools.GetWinSysDir;
     iValue := ShellApi.ShellExecute( 0,
                                      nil,
                                      StrPCopy( zFileName, Format( '%s\at.exe', [ FWindowsSystemFolder ] ) ),
                                      StrPCopy( zParams, Format( GetAtCommand + eLineaComando.Text, [] )),
                                      StrPCopy( zDir, FWindowsSystemFolder ),
                                      SW_HIDE );
     case iValue of
          {
          ERROR_FILE_NOT_FOUND: ShowError( 'The specified file was not found' );
          ERROR_PATH_NOT_FOUND: ShowError( 'The specified path was not found' );
          }
          ERROR_BAD_FORMAT: ShowError( 'The .EXE file is invalid (non-Win32 .EXE or error in .EXE image)' );
          SE_ERR_ACCESSDENIED: ShowError( 'The operating system denied access to the specified file' );
          SE_ERR_ASSOCINCOMPLETE: ShowError( 'The filename association is incomplete or invalid' );
          SE_ERR_DDEBUSY: ShowError( 'The DDE transaction could not be completed because other DDE transactions were being processed' );
          SE_ERR_DDEFAIL: ShowError( 'The DDE transaction failed' );
          SE_ERR_DDETIMEOUT: ShowError( 'The DDE transaction could not be completed because the request timed out' );
          SE_ERR_DLLNOTFOUND: ShowError( 'The specified dynamic-link library was not found' );
          SE_ERR_FNF: ShowError( 'The specified file was not found' );
          SE_ERR_NOASSOC: ShowError( 'There is no application associated with the given filename extension' );
          SE_ERR_OOM: ShowError( 'There was not enough memory to complete the operation' );
          SE_ERR_PNF: ShowError( 'The specified path was not found' );
          SE_ERR_SHARE: ShowError( 'A sharing violation occurred' );
     end;
end;

procedure TEmailChecker.SetControls;
begin
     BCalendarizar.Enabled := ZetaCommonTools.StrLleno( eLineaComando.Text ) and
                              HayDiasMarcados and ZetaCommonTools.StrLleno( BkupTime.Valor );
end;

function TEmailChecker.HayDiasMarcados: Boolean;
var
   i: Integer;
begin
     for i := 0 to 6 do
     begin
          Result := BkupDays.Checked[ i ];
          if Result then
             Break;
     end;
end;

procedure TEmailChecker.KeyPress( var Key: Char );
begin
     inherited KeyPress( Key );
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) and
                  ( not ( ActiveControl is TCustomMemo ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;

end.
