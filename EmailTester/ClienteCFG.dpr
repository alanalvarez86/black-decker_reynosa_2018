program ClienteCFG;

uses
  Forms,
  ZetaClientTools,
  FClientTest in 'FClientTest.pas' {ClientTester};

{$R *.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.Title := 'Configurador de Cliente de Tress';
  Application.CreateForm(TClientTester, ClientTester);
  Application.Run;
end.
