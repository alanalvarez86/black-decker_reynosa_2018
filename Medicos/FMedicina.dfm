inherited Medicina_DevEx: TMedicina_DevEx
  Left = 307
  Top = 230
  Caption = 'Medicinas'
  PixelsPerInch = 96
  TextHeight = 13
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 429
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ME_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ME_NOMBRE'
        Title.Caption = 'Descripci'#243'n'
        Width = 205
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ME_MEDIDA'
        Title.Caption = 'Medida'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ME_DESCRIP'
        Title.Caption = 'Observaciones'
        Width = 184
        Visible = True
      end>
  end
end
