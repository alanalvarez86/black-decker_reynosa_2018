inherited CampoEdit_DevEx: TCampoEdit_DevEx
  Left = 203
  Top = 206
  ActiveControl = Letrero
  Caption = 'Propiedades del Campo'
  ClientHeight = 140
  ClientWidth = 400
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  ExplicitWidth = 406
  ExplicitHeight = 168
  PixelsPerInch = 96
  TextHeight = 13
  object LetreroLBL: TLabel [0]
    Left = 60
    Top = 14
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = '&Descripci'#243'n:'
    FocusControl = Letrero
  end
  object ResumerLBL: TLabel [1]
    Left = 18
    Top = 38
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'Mostrar en Resumen:'
    FocusControl = ComoMostrar
  end
  object FormulaLBL: TLabel [2]
    Left = 4
    Top = 61
    Width = 115
    Height = 13
    Alignment = taRightJustify
    Caption = '&Valor Default al Agregar:'
    FocusControl = FormulaTexto
  end
  object lblExcede: TLabel [3]
    Left = 30
    Top = 80
    Width = 336
    Height = 13
    Caption = 
      'El N'#250'mero De Controles Exceden El Espacio Vertical De El Expedie' +
      'nte'#39
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  inherited PanelBotones: TPanel
    Top = 104
    Width = 400
    TabOrder = 5
    ExplicitTop = 104
    ExplicitWidth = 400
    inherited OK_DevEx: TcxButton
      Left = 234
      OnClick = OK_DevExClick
      ExplicitLeft = 234
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 314
      ExplicitLeft = 314
    end
  end
  object Letrero: TEdit [5]
    Left = 122
    Top = 10
    Width = 273
    Height = 21
    HelpContext = 101
    TabOrder = 0
  end
  object ComoMostrar: TComboBox [6]
    Left = 122
    Top = 34
    Width = 175
    Height = 21
    HelpContext = 101
    Style = csDropDownList
    TabOrder = 1
  end
  object FormulaTexto: TEdit [7]
    Left = 122
    Top = 57
    Width = 273
    Height = 21
    HelpContext = 101
    TabOrder = 2
  end
  object FormulaNumero: TZetaNumero [8]
    Left = 122
    Top = 57
    Width = 130
    Height = 21
    HelpContext = 101
    Mascara = mnHoras
    TabOrder = 3
    Text = '0.00'
  end
  object FormulaBool: TCheckBox [9]
    Left = 122
    Top = 59
    Width = 15
    Height = 17
    HelpContext = 101
    Caption = '                        '
    TabOrder = 4
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 7340168
  end
end
