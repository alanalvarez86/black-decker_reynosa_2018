unit FEditTablas2_DevEx;

interface

uses
  ZBaseEdicion_DevEx,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   Mask, DBCtrls, StdCtrls, ZetaEdit, Db, ExtCtrls, Buttons,
  ZetaClientDataSet, ZetaNumero, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
    TEditTablas2_DevEx = class(TBaseEdicion_DevEx)

    DBCodigoLBL: TLabel;
    TB_ELEMENT: TDBEdit;
    DBDescripcionLBL: TLabel;
    DBInglesLBL: TLabel;
    TB_INGLES: TDBEdit;
    Label2: TLabel;
    Label1: TLabel;
    TB_TEXTO: TDBEdit;
    TB_CODIGO: TZetaDBEdit;
    TB_NUMERO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
    fZetalookupDataset : TZetaLookupDataSet;
  protected
    procedure Connect;override;
    procedure DoLookup; override;
    procedure AfterCreate; virtual;
    property ZetaLookupDataset : TZetaLookupDataSet read fZetaLookupDataset write fZetaLookupDataset;
  public
    { Public declarations }
  end;

 type
    TTOEditTipoConsulta = class( TEditTablas2_DevEx )
       protected
              Procedure AfterCreate;override;
 end;
//MA:Clase Agregada por mi
 type
    TTOEditTipoEstudio = class ( TEditTablas2_DevEx )
       protected
              Procedure AfterCreate; override;
 end;

const
     K_MAX_LENGTH_DESCRIP = 30;

var
  EditTipoConsulta : TTOEditTipoConsulta;
  EditTipoEstudio  : TTOEditTipoEstudio; //MA:Linea agregada por mi\


implementation
uses
ZetaBuscador_DevEx,
DMedico, ZetaCommonClasses, ZAccesosTress, ZetaCommonLists, FAyudaContexto;

{$R *.DFM}

procedure TEditTablas2_DevEx.AfterCreate;
begin
end;

procedure TEditTablas2_DevEx.Connect;
begin
  Caption := ZetaLookupDataset.LookupName;
  DataSource.DataSet := ZetaLookupDataset;
end;

procedure TEditTablas2_DevEx.DoLookup;
begin
   inherited;
   ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', ZetaLookupdataset );
end;

procedure TEditTablas2_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  FirstControl := TB_CODIGO;
  AfterCreate;
  TB_ELEMENT.MaxLength := K_MAX_LENGTH_DESCRIP;
  TB_INGLES.MaxLength := K_MAX_LENGTH_DESCRIP;
  TB_TEXTO.MaxLength := K_MAX_LENGTH_DESCRIP;
end;

procedure TTOEditTipoConsulta.AfterCreate;
begin
     inherited;
     ZetaLookupDataset := dmMedico.cdsTConsulta;
     HelpContext := H00007_Catalogo_de_Tipos_de_Consulta;
     IndexDerechos := ZAccesosTress.D_CAT_TIPO_CONSULTA;
end;

//MA:Procedure agregado por mi
procedure TTOEditTipoEstudio.AfterCreate;
begin
     inherited;
     ZetaLookupDataset := dmMedico.cdsTEstudio;
     HelpContext := H00008_Catalogo_de_tipos_de_Estudio;
     IndexDerechos := ZAccesosTress.D_CAT_TIPO_EST_LAB;
end;


end.
