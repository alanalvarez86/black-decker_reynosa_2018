unit FEditHisIncapaci_DevEx;

interface

uses
  ZBaseEdicion_DevEx,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   DBCtrls, StdCtrls,
  Mask, Db, Buttons, ExtCtrls, ZetaDBTextBox,
  ZetaKeyCombo,
  ZetaKeyLookup, ZetaNumero, ZetaFecha, ZetaSmartLists, ComCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar,
  cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC;

type
    TEditHisIncapaci_DevEx = class(TBaseEdicion_DevEx)
    PageControl: TcxPageControl;
    tbGenerales: TcxTabSheet;
    LIN_FEC_INI: TLabel;
    LIN_DIAS: TLabel;
    LIN_FEC_FIN: TLabel;
    Label13: TLabel;
    Label11: TLabel;
    Label5: TLabel;
    Label10: TLabel;
    IN_TASA_IPLbl: TLabel;
    UsuarioLbl: TLabel;
    Label1: TLabel;
    IN_CAPTURA: TZetaDBTextBox;
    Label2: TLabel;
    IN_FEC_FIN: TZetaDBTextBox;
    US_DESCRIP: TZetaDBTextBox;
    IN_COMENTA: TDBEdit;
    IN_TIPO: TZetaDBKeyLookup_DevEx;
    IN_FEC_INI: TZetaDBFecha;
    IN_DIAS: TZetaDBNumero;
    IN_NUMERO: TDBEdit;
    IN_MOTIVO: TZetaDBKeyCombo;
    IN_FIN: TZetaDBKeyCombo;
    IN_TASA_IP: TZetaDBNumero;
    TabSheet2: TcxTabSheet;
    IN_SUA_INI: TZetaDBFecha;
    Label3: TLabel;
    IN_SUA_FIN: TZetaDBTextBox;
    Label4: TLabel;
    Label6: TLabel;
    IN_FEC_RH: TZetaDBFecha;
    procedure FormCreate(Sender: TObject);
    procedure IN_FINChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure Connect;override;
    procedure ImprimirForma;override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
  end;

var
  EditHisIncapaci_DevEx: TEditHisIncapaci_DevEx;

implementation

uses DMedico, dTablas, dSistema,ZetaTipoEntidad,
     ZImprimeForma,ZetaCommonLists, ZetaCommonClasses, ZAccesosTress, FAyudaContexto;

{$R *.DFM}

procedure TEditHisIncapaci_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IN_TIPO.Filtro := 'TB_INCIDEN =' + IntToStr( Ord( eiIncapacidad ) );
     TipoValorActivo1 := ZetacommonLists.stExpediente;
     HelpContext:= H81000_Incapacidades_Medicos;
     IndexDerechos := ZAccesosTress.D_PACIENTE_INCAPACIDADES;
     FirstControl := IN_FEC_INI;
     IN_TIPO.LookupDataset := dmTablas.cdsIncidencias;
end;

procedure TEditHisIncapaci_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := tbGenerales;
     IN_FINChange( self );
end;

procedure TEditHisIncapaci_DevEx.Connect;
begin
     dmTablas.cdsIncidencias.Conectar;
     dmSistema.cdsUsuarios.Conectar;

     with dmMedico do
     begin
          cdsHisIncapaci.Conectar;
          DataSource.DataSet := cdsHisIncapaci;
     end;
end;

procedure TEditHisIncapaci_DevEx.IN_FINChange(Sender: TObject);
begin
     inherited;
     IN_TASA_IPLbl.Enabled := ( IN_FIN.Valor = Ord( fiPermanente ));
     IN_TASA_IP.Enabled    := IN_TASA_IPLbl.Enabled;
end;

procedure TEditHisIncapaci_DevEx.ImprimirForma;
begin
//     TEditHisIncapaci_DevEx.ImprimeUnaForma( enIncapacidad, dmMedico.cdsHisIncapaci );
end;

function TEditHisIncapaci_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeAgregar( sMensaje );
end;

function TEditHisIncapaci_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje );
end;

function TEditHisIncapaci_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
end;

end.







