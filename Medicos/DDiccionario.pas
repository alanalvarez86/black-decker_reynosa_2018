unit DDiccionario;

interface
{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBaseTressDiccionario,
     DBaseDiccionario,
     ZetaCommonLists,
     ZetaClientDataSet,
     ZetaTipoEntidad ;

type
  TdmDiccionario = class(TdmBaseTressDiccionario)
  private
    { Private declarations }
  protected
  public
    { Public declarations }
    procedure SetLookupNames; override;
    procedure GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);override;
  end;

var
  dmDiccionario: TdmDiccionario;

implementation

uses dTablas;

{$R *.DFM}

procedure TdmDiccionario.GetListaClasifiModulo(oLista: TStrings;const lMigracion: Boolean);
begin
     inherited GetListaClasifiModulo( oLista, lMigracion );
     {$ifdef RDD}
     {$else}
     AgregaClasifi( oLista, crMedico );
     {$endif}
end;

procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
     //dmMedicos.SetLookupNames;
     dmTablas.SetLookupNames;
end;


end.
