inherited KardexEmbarazo: TKardexEmbarazo
  Left = 200
  Top = 153
  Caption = 'Embarazos'
  ClientWidth = 624
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 624
    ExplicitWidth = 624
    inherited ValorActivo2: TPanel
      Width = 365
      ExplicitWidth = 365
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 624
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'EM_FEC_UM'
        Title.Caption = 'F. U. M.'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EM_FEC_PP'
        Title.Caption = 'F. P. P'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EM_PRENAT'
        Title.Caption = 'Inicio Incapacidad'
        Width = 91
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EM_POSNAT'
        Title.Caption = 'Fin Incapacidad'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EM_INC_INI'
        Title.Caption = 'Inicio del Riesgo'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EM_INC_FIN'
        Title.Caption = 'Fin del Riesgo'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EM_COMENTA'
        Title.Caption = 'Observaciones'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Modific'#243
        Width = 175
        Visible = True
      end>
  end
end
