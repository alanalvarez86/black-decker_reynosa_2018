unit FGlobales;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, ComCtrls, ToolWin, StdCtrls, Buttons, ImgList,
     ZBaseGlobal,
     ZBaseConsulta,
     ZetaDBTextBox;

type
  TSistGlobales = class(TBaseConsulta)
    PanelTitulos: TPanel;
    ToolBar: TToolBar;
    ImageList: TImageList;
    BConfiguracion: TToolButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RfcLbl: TZetaDBTextBox;
    InfonavitLbl: TZetaDBTextBox;
    RazonLbl: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure BConfiguracionClick(Sender: TObject);
    procedure NivelesBtnClick(Sender: TObject);
    procedure AdicionalesBtnClick(Sender: TObject);
    procedure RecursosBtnClick(Sender: TObject);
    procedure NominaBtnClick(Sender: TObject);
    procedure AsistenciaBtnClick(Sender: TObject);
    procedure ImssBtnClick(Sender: TObject);
    procedure CapacitacionbtnClick(Sender: TObject);
    procedure VariablesBtnClick(Sender: TObject);
    procedure EmpleadosBtnClick(Sender: TObject);
    procedure CafeteriaBtnClick(Sender: TObject);
    procedure SupervisoresBtnClick(Sender: TObject);
    procedure RepEmailBtnClick(Sender: TObject);
    procedure SeguridadBtnClick(Sender: TObject);
    procedure ToolBarDblClick(Sender: TObject);
  private
    { Private declarations }
    function AbrirGlobal( GlobalClass: TBaseGlobalClass ): Integer;
    procedure AplicaGlobalDiccion;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  SistGlobales: TSistGlobales;

implementation

uses DGlobal,
     DCliente,
     ZGlobalTress,
     ZetaCommonClasses,
     FAutoClasses,
     FGlobalAsistencia,
     FGlobalAdicionales,
     FGlobalCapacitacion,
     FGlobalImss,
     FGlobalIdentificacion,
     FGlobalNiveles,
     FGlobalNomina,
     FGlobalRecursos,
     FGlobalVariables,
     FGlobalEmpleados,
     FGlobalCafeteria,
     FGlobalSupervisores,
     FGlobalSeguridad,
     FGlobalReportesViaEmail,
     FTressShell;

{$R *.DFM}

{ ************* TSistGlobales *************** }

procedure TSistGlobales.FormCreate(Sender: TObject);
begin
     inherited;
     EsConsulta := False;
     HelpContext := H60651_Globales_empresa;
end;

procedure TSistGlobales.Connect;
begin
     with Global do
     begin
          RFCLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RFC_EMPRESA );
          InfonavitLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_INFONAVIT_EMPRESA );
          RazonLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RAZON_EMPRESA );
     end;
end;

procedure TSistGlobales.Refresh;
begin
end;

procedure TSistGlobales.AplicaGlobalDiccion;
begin
     TressShell.CreaArbol( False );
end;

function TSistGlobales.AbrirGlobal( GlobalClass: TBaseGlobalClass ): Integer;
var
   Forma: TBaseGlobal;
begin
     Forma := GlobalClass.Create( Application );
     with Forma do
     begin
          try
             ShowModal;
             Result := LastAction;
          finally
                 Free;
          end;
     end;
end;

procedure TSistGlobales.BConfiguracionClick(Sender: TObject);
begin
     inherited;
     if ( AbrirGlobal( TGlobalIdentificacion ) = K_EDICION_MODIFICACION ) then
        Connect;
end;

procedure TSistGlobales.NivelesBtnClick(Sender: TObject);
begin
     inherited;
     if ( AbrirGlobal( TGlobalNiveles ) = K_EDICION_MODIFICACION ) then
        AplicaGlobalDiccion;
end;

procedure TSistGlobales.AdicionalesBtnClick(Sender: TObject);
begin
     inherited;
     if ( AbrirGlobal( TGlobalAdicionales ) = K_EDICION_MODIFICACION ) then
        AplicaGlobalDiccion;
end;

procedure TSistGlobales.RecursosBtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okRecursos ) then
     begin
          AbrirGlobal( TGlobalRecursos );
     end;
end;

procedure TSistGlobales.NominaBtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okNomina ) then
     begin
          AbrirGlobal( TGlobalNomina );
     end;
end;

procedure TSistGlobales.AsistenciaBtnClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalAsistencia );
end;

procedure TSistGlobales.ImssBtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okIMSS ) then
     begin
          AbrirGlobal( TGlobalImss );
     end;
end;

procedure TSistGlobales.CapacitacionbtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCursos ) then
     begin
          AbrirGlobal( TGlobalCapacitacion );
     end;
end;

procedure TSistGlobales.VariablesBtnClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalVariables );
end;

procedure TSistGlobales.EmpleadosBtnClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalEmpleados );
end;

procedure TSistGlobales.CafeteriaBtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCafeteria ) then
     begin
          AbrirGlobal( TGlobalCafeteria );
     end;
end;

procedure TSistGlobales.SupervisoresBtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okSupervisores ) then
     begin
          AbrirGlobal( TGlobalSupervisores );
     end;
end;

procedure TSistGlobales.RepEmailBtnClick(Sender: TObject);
begin
     inherited;
     //pendiente
     if dmCliente.ModuloAutorizado( okReportesMail ) then
     begin
          AbrirGlobal( TGlobalReportesViaEmail );
     end;
end;

procedure TSistGlobales.SeguridadBtnClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalSeguridad );
end;

procedure TSistGlobales.ToolBarDblClick(Sender: TObject);
begin
     inherited;
     IdentificacionBtnClick( Sender );
end;

end.

