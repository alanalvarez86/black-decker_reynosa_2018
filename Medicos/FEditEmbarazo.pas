unit FEditEmbarazo;

interface

uses
  //ZBaseEdicion
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaEdit, Mask,
  ZetaFecha, ZetaDBTextBox, ZetaKeyCombo;

type
  TEditEmbarazos = class(Tobject)
  //TEditEmbarazos = class(TBaseEdicion)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EM_FEC_UM: TZetaDBFecha;
    EM_PRENAT: TZetaDBFecha;
    EM_FEC_PP: TZetaDBFecha;
    EM_POSNAT: TZetaDBFecha;
    Label5: TLabel;
    US_NOMBRE: TZetaDBTextBox;
    Label6: TLabel;
    gbTermino: TGroupBox;
    LBLEM_FEC_FIN: TLabel;
    EM_FEC_FIN: TZetaDBFecha;
    EM_NORMAL: TDBRadioGroup;
    EM_FINAL: TDBCheckBox;
    EM_MORTAL: TDBRadioGroup;
    gbEmbRiesgo: TGroupBox;
    lblEM_INC_INI: TLabel;
    lblEM_INC_FIN: TLabel;
    EM_INC_INI: TZetaDBFecha;
    EM_INC_FIN: TZetaDBFecha;
    lblEM_OBS_RIE: TLabel;
    EM_OBS_RIE: TDBMemo;
    LBLEM_TERMINO: TLabel;
    EM_RIESGO: TDBCheckBox;
    EM_TERMINO: TZetaDBKeyCombo;
    EM_COMENTA: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure EM_FINALClick(Sender: TObject);
    procedure EM_RIESGOClick(Sender: TObject);
    procedure EM_FEC_UMChange(Sender: TObject);
    procedure EM_FEC_PPChange(Sender: TObject);
  private
  protected
    { Protected declarations }
    //procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditEmbarazos: TEditEmbarazos;

implementation

uses
//ZetaBuscador
DMedico, ZAccesosTress, ZetaCommonLists, DSistema, FAyudaContexto;

{$R *.DFM}

procedure TEditEmbarazos.FormCreate(Sender: TObject);
begin
     inherited;
     {IndexDerechos := ZAccesosTress.D_EMP_KARDEX_EMBARAZOS;
     FirstControl := EM_FEC_UM;
     TipoValorActivo1 := stExpediente;
     HelpContext:= H00105_Embarazos;}
end;

{procedure TEditEmbarazos.Connect;
begin
     with dmMedico do
     begin
          dmSistema.cdsUsuarios.Conectar;
          //DataSource.DataSet:= cdsKardexEmbarazo;
     end;
end;}

procedure TEditEmbarazos.EM_FINALClick(Sender: TObject);
begin
     inherited;
     with EM_FINAL do
     begin
          EM_FEC_FIN.Enabled := Checked;
          EM_TERMINO.Enabled := Checked;
          EM_MORTAL.Enabled := Checked;
          EM_NORMAL.Enabled := Checked;
          LBLEM_FEC_FIN.Enabled := Checked;
          LBLEM_TERMINO.Enabled := Checked;
     end;
end;

procedure TEditEmbarazos.EM_RIESGOClick(Sender: TObject);
begin
     inherited;
     with EM_RIESGO do
     begin
          lblEM_INC_INI.Enabled := Checked;
          lblEM_INC_FIN.Enabled := Checked;
          lblEM_OBS_RIE.Enabled := Checked;
          EM_INC_INI.Enabled := Checked;
          EM_INC_FIN.Enabled := Checked;
          EM_OBS_RIE.Enabled := Checked;
     end;
end;

procedure TEditEmbarazos.EM_FEC_UMChange(Sender: TObject);
const
     K_FUM = 0;
begin
     inherited;
        with dmMedico do
        begin
             if ( cdsKardexEmbarazo.State in [ dsInsert, dsEdit] ) then
                SincronizaFechas( K_FUM );
        end;
end;

procedure TEditEmbarazos.EM_FEC_PPChange(Sender: TObject);
const
     K_FPP = 1;
begin
     inherited;
     with dmMedico do
     begin
          if ( cdsKardexEmbarazo.State in [ dsInsert, dsEdit] ) then
             SincronizaFechas( K_FPP );
     end;
end;


end.
