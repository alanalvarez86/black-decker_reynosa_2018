inherited MedicinaEntr: TMedicinaEntr
  Left = 195
  Top = 175
  Caption = 'Medicinas Entregadas'
  ClientHeight = 290
  ClientWidth = 613
  ExplicitWidth = 613
  ExplicitHeight = 290
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 613
    ExplicitWidth = 590
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 354
      ExplicitWidth = 331
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 348
      end
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 613
    Height = 271
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ME_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ME_NOMBRE'
        Title.Caption = 'Descripci'#243'n'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MT_FECHA'
        Title.Caption = 'Fecha'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MT_CANTIDA'
        Title.Caption = 'Cantidad'
        Width = 105
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Modific'#243
        Width = 145
        Visible = True
      end>
  end
end
