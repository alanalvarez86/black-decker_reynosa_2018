unit FKardexConsultas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, Vcl.StdCtrls;

type
  TConsultas = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    //procedure DoLookup; override;
  end;

var
  Consultas: TConsultas;

implementation

uses DMedico,
     ZImprimeForma,
     //Zetabuscador,
     ZetaTipoEntidad,
     ZetaCommonLists,
     FAyudaContexto;

{$R *.DFM}

procedure TConsultas.FormCreate(Sender: TObject);
begin
  inherited;
  //CanLookup := True;
  HelpContext := H00003_Consulta;
  //TipoValorActivo1 := stEmpleado;
  TipoValorActivo1 := stExpediente;
end;

procedure TConsultas.Connect;
begin
     with dmMedico do
     begin
          cdsKardexConsultas.Conectar;
          DataSource.DataSet:= cdsKardexConsultas;
     end;
end;

procedure TConsultas.Refresh;
begin
     dmMedico.cdsKardexConsultas.Refrescar;
end;

procedure TConsultas.Agregar;
begin
     dmMedico.cdsKardexConsultas.Agregar;
end;

procedure TConsultas.Borrar;
begin
     dmMedico.cdsKardexConsultas.Borrar;
end;

procedure TConsultas.Modificar;
begin
     dmMedico.cdsKardexConsultas.Modificar;
end;

{procedure TConsultas.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', 'Consultas M�dicas', 'EX_CODIGO', dmMedico.cdsKardexConsultas );
end;}

function TConsultas.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := FALSE;
     With dmMedico do
     begin
          if cdsExpediente.Active then
          begin
               Result := inherited PuedeAgregar( sMensaje );
               if Result then
               begin
                    Result := ExisteExpediente;
                    if not Result then
                       sMensaje := 'El Expediente No Existe';
               end;
          end
          else
          begin
               if ( cdsKardexConsultas.FieldByname('EX_CODIGO').AsInteger <> 0 ) then
                  Result := True;
          end;
     end;
end;

procedure TConsultas.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enConsulta, dmMedico.cdsKardexConsultas );
end;

end.
