unit FEditIncaPermDlg_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, ExtCtrls, Buttons, ZBaseDlgModal, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, Vcl.ImgList,
  cxButtons, TressMorado2013;

type
  TEditIncaPermDlg_DevEx = class(TZetaDlgModal_DevEx)
    rgOperacion: TRadioGroup;
    Cancelar: TcxButton;
    OK: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure rgOperacionClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FOperacion: integer;
    procedure GetOperacion;
  public
    { Public declarations }
    property Operacion: integer read FOperacion;
  end;

var
  EditIncaPermDlg_DevEx: TEditIncaPermDlg_DevEx;

implementation

{$R *.DFM}

procedure TEditIncaPermDlg_DevEx.CancelarClick(Sender: TObject);
begin
  inherited;
  Close;

end;

procedure TEditIncaPermDlg_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     GetOperacion;
end;

procedure TEditIncaPermDlg_DevEx.GetOperacion;
begin
     FOperacion := rgOperacion.ItemIndex;
end;

procedure TEditIncaPermDlg_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     GetOperacion;
end;

procedure TEditIncaPermDlg_DevEx.rgOperacionClick(Sender: TObject);
begin
     inherited;
     GetOperacion;
end;

end.
