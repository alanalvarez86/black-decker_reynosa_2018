unit FKardexConsultas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, Vcl.StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TConsultas_DevEx = class(TBaseGridLectura_DevEx)
    CN_FECHA: TcxGridDBColumn;
    CN_HOR_INI: TcxGridDBColumn;
    CN_HOR_FIN: TcxGridDBColumn;
    CN_TIPO_CONS: TcxGridDBColumn;
    CN_MOTIVO: TcxGridDBColumn;
    DA_DESCRIP: TcxGridDBColumn;
    CN_IMSS: TcxGridDBColumn;
    CN_SUB_SEC: TcxGridDBColumn;
    US_NOMBRE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    //procedure DoLookup; override;
  end;

var
  Consultas_DevEx: TConsultas_DevEx;

implementation

uses DMedico,
     ZImprimeForma,
     //Zetabuscador,
     ZetaTipoEntidad,
     ZetaCommonLists,
     FAyudaContexto;

{$R *.DFM}

procedure TConsultas_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  //CanLookup := True;
  HelpContext := H00003_Consulta;
  //TipoValorActivo1 := stEmpleado;
  TipoValorActivo1 := stExpediente;
end;

procedure TConsultas_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

end;

procedure TConsultas_DevEx.Connect;
begin
     with dmMedico do
     begin
          cdsKardexConsultas.Conectar;
          DataSource.DataSet:= cdsKardexConsultas;
     end;
end;

procedure TConsultas_DevEx.Refresh;
begin
     dmMedico.cdsKardexConsultas.Refrescar;
end;

procedure TConsultas_DevEx.Agregar;
begin
     dmMedico.cdsKardexConsultas.Agregar;
end;

procedure TConsultas_DevEx.Borrar;
begin
     dmMedico.cdsKardexConsultas.Borrar;
end;

procedure TConsultas_DevEx.Modificar;
begin
     dmMedico.cdsKardexConsultas.Modificar;
end;

{procedure TConsultas.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', 'Consultas M�dicas', 'EX_CODIGO', dmMedico.cdsKardexConsultas );
end;}

function TConsultas_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := FALSE;
     With dmMedico do
     begin
          if cdsExpediente.Active then
          begin
               Result := inherited PuedeAgregar( sMensaje );
               if Result then
               begin
                    Result := ExisteExpediente;
                    if not Result then
                       sMensaje := 'El Expediente No Existe';
               end;
          end
          else
          begin
               if ( cdsKardexConsultas.FieldByname('EX_CODIGO').AsInteger <> 0 ) then
                  Result := True;
          end;
     end;
end;

procedure TConsultas_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enConsulta, dmMedico.cdsKardexConsultas );
end;

end.
