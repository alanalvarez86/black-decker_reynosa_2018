inherited EditClasificacion: TEditClasificacion
  Left = 213
  Top = 365
  HelpContext = 101
  ActiveControl = Codigo
  Caption = 'Clasificaci�n'
  ClientHeight = 105
  ClientWidth = 317
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object CodigoLBL: TLabel [0]
    Left = 28
    Top = 14
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C�&digo:'
    FocusControl = Codigo
  end
  object DescripcionLBL: TLabel [1]
    Left = 5
    Top = 36
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'D&escripci�n:'
  end
  inherited PanelBotones: TPanel
    Top = 69
    Width = 317
    BevelOuter = bvNone
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 149
      Default = True
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 234
    end
  end
  object Codigo: TEdit
    Left = 67
    Top = 10
    Width = 121
    Height = 21
    HelpContext = 101
    MaxLength = 6
    TabOrder = 0
  end
  object Descripcion: TEdit
    Left = 67
    Top = 32
    Width = 241
    Height = 21
    HelpContext = 101
    TabOrder = 1
  end
end
