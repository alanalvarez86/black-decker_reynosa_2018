unit FEditMedicina_DevEx;

interface

uses
  ZBaseEdicion_DevEx,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   ZetaNumero, Mask, DBCtrls, StdCtrls, ZetaEdit, Db,
  ExtCtrls, Buttons, ZetaKeyCombo, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
    TEditMedicina_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    ME_NOMBRE: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    ME_INGLES: TDBEdit;
    ME_NUMERO: TZetaDBNumero;
    Label5: TLabel;
    Label6: TLabel;
    ME_TEXTO: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    ME_MEDIDA: TDBEdit;
    ME_DESCRIP: TDBEdit;
    ME_CODIGO: TZetaDBEdit;
    procedure FormCreate(Sender: TObject);
 protected
    { Protected declarations }
     procedure Connect; override;
     procedure DoLookup; override;
  public
    { Public declarations }
  end;

const
     K_MAX_LENGTH_DESCRIP = 30;
     K_MAX_LENGTH_OBSERV = 50;

var
  EditMedicina_DevEx: TEditMedicina_DevEx;

implementation

uses
//ZetaBuscador
DMedico, ZAccesosTress, FAyudaContexto;

{$R *.DFM}

procedure TEditMedicina_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  IndexDerechos := ZAccesosTress.D_CAT_MEDICAMENTOS;
  FirstControl := ME_CODIGO;
  HelpContext:= H00005_Catalogo_de_Medicinas;
  ME_NOMBRE.MaxLength := K_MAX_LENGTH_DESCRIP;
  ME_TEXTO.MaxLength := K_MAX_LENGTH_DESCRIP;
  ME_INGLES.MaxLength := K_MAX_LENGTH_DESCRIP;
  ME_DESCRIP.MaxLength := K_MAX_LENGTH_OBSERV;
end;

procedure TEditMedicina_DevEx.Connect;
begin
     with dmMedico do
     begin
         DataSource.DataSet:= cdsMedicina;
     end;
end;

procedure TEditMedicina_DevEx.DoLookup;
begin
     inherited;
     //ZetaBuscador.BuscarCodigo( 'C�digo', 'Medicamentos', 'ME_CODIGO', dmMedico.cdsMedicina );
end;

end.
