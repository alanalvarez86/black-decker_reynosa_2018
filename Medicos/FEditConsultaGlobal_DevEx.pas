unit FEditConsultaGlobal_DevEx;

interface

uses
  ZBaseGridEdicion_DevEx,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ZetaCommonClasses, ExtCtrls, StdCtrls, ZetaKeyLookup, ZetaHora, Mask,
  ZetaFecha, ZetaEdit, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar,
  cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxTextEdit, cxMemo;

type
  TEditConsultaGlobal_DevEx = class(TBaseGridEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    CN_FECHA: TZetaFecha;
    CN_HOR_INI: TZetaHora;
    CN_HOR_FIN: TZetaHora;
    CN_TIPO: TZetaKeyLookup_DevEx;
    CN_MOTIVO: TZetaEdit;
    CN_OBSERVA: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
   { Private declarations }
    FParamList: TZetaParams;
    procedure BuscaExpediente;
  protected
    procedure Connect; override;
    procedure Buscar; override;
    //function Editing: Boolean; override;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  EditConsultaGlobal_DevEx: TEditConsultaGlobal_DevEx;

implementation
uses DMedico,
     FBuscaExpedientes,
     ZetaBuscaEmpleado_DevEx,
     ZetaCommonTools,
     ZAccesosTress,
     FAyudaContexto;

{$R *.DFM}

{ TEditConsultaGlobal }
procedure TEditConsultaGlobal_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  HelpContext := H00107_Registro_de_Consultas_Globales;
  FParamList := TZetaParams.Create;
  CN_TIPO.LookupDataset := dmMedico.cdsTConsulta;
end;

procedure TEditConsultaGlobal_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Self.ActiveControl := CN_FECHA;
     with dmMedico do
     begin
          CN_FECHA.Valor := DATE;
          CN_HOR_INI.Valor := FormatDateTime( 'hhmm', Now );
          CN_MOTIVO.Text := VACIO;
          CN_OBSERVA.Text := VACIO;
     end;
end;

procedure TEditConsultaGlobal_DevEx.Connect;
begin
     with dmMedico do
     begin
          cdsCodigos.Conectar;
          {CV: Se manda llamar el metodo Refrescar enves del Conectar porque necesitamos
          que siempre este vac�o. El conectar si esta abierto, no hace nada y el dataset se
          quedar�a con lo valores anteriores.}
          cdsConsultaGlobal.Refrescar;
          DataSource.DataSet:= cdsConsultaGlobal;
     end;
end;


procedure TEditConsultaGlobal_DevEx.BuscaExpediente;
var
   iExpediente, iEmpleado: Integer;
   sDescripcion: String;
begin
     if FBuscaExpedientes.BuscaExpedienteDialogo( VACIO, iExpediente, iEmpleado, sDescripcion, TRUE ) then
     begin
          with dmMedico.cdsConsultaGlobal do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'EX_CODIGO' ).AsInteger := iExpediente;
          end;
     end;
end;

procedure TEditConsultaGlobal_DevEx.Buscar;
begin
  inherited;
  with ZetaDBGrid.SelectedField do
  begin
       if ( FieldName = 'EX_CODIGO' )or(FieldName = 'CB_CODIGO') then
          BuscaExpediente;
  end;
end;

procedure TEditConsultaGlobal_DevEx.EscribirCambios;
begin
     {Se utiliza DatabaseError para poder levantar una excepci�n y que la
      forma no se cierra al momento de darle Ok, si alguna validaci�n
      se ejecuta}

     {No se requiere el INHERITED porque aqui se esta mandando llamar directamente
     el metodo que graba la informacion.}
     if (CN_FECHA.Valor = NULLDATETIME) then
     begin
          CN_FECHA.SetFocus;
          DatabaseError('La Fecha De Consulta No Puede Quedar Vac�a');
     end
     else if strVacio(CN_TIPO.Llave) then
          begin
               CN_TIPO.SetFocus;
               DatabaseError('El Tipo De Consulta No Puede Quedar Vac�o');
          end
          else if strVacio(CN_MOTIVO.Valor) then
               begin
                    CN_MOTIVO.SetFocus;
                    DatabaseError('El Motivo De La Consulta No Puede Quedar Vac�o');
               end
               else
               begin
                    with FParamList do
                    begin
                         AddDate( 'CN_FECHA', CN_FECHA.Valor );
                         AddString( 'CN_HOR_INI', CN_HOR_INI.Valor );
                         AddString( 'CN_HOR_FIN', CN_HOR_FIN.Valor );
                         AddString( 'CN_TIPO', CN_TIPO.Llave );
                         AddString( 'CN_MOTIVO', CN_MOTIVO.Valor );
                         AddString( 'CN_OBSERVA', CN_OBSERVA.Text );
                         AddInteger( 'US_CODIGO', dmMedico.cdsConsultaGlobal.FieldByName('US_CODIGO').AsInteger );
                         dmMedico.EscribeConsultaGlobal( VarValues );
                    end;
               end;
end;

procedure TEditConsultaGlobal_DevEx.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil( FParamList );
end;

end.
