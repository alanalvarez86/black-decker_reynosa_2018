unit FEditExpediente;

interface

uses
  //ZBaseEdicion, TDMULTIP
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask,
  ZetaFecha, ZetaEdit, ZetaDBTextBox, ZetaKeyCombo, ZetaKeyLookup,
  ZetaSmartLists;

type
  TArreglo = array[1..6] of string;

//  TEditExpediente = class(TBaseEdicion)
  TEditExpediente = class(Tobject)
    PageControl: TPageControl;
    tsGenerales: TTabSheet;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label13: TLabel;
    LCB_FEC_NAC: TLabel;
    ZEdad: TZetaTextBox;
    Label7: TLabel;
    btnSincroniza: TSpeedButton;
    EX_APE_PAT: TDBEdit;
    EX_APE_MAT: TDBEdit;
    EX_NOMBRES: TDBEdit;
    EX_FEC_NAC: TZetaDBFecha;
    EX_TIPO: TZetaDBKeyCombo;
    CB_CODIGO: TZetaDBKeyLookup;
    EX_SEXO: TZetaKeyCombo;
    btnDatosContrata: TBitBtn;
    Panel2: TPanel;
    GroupBox4: TGroupBox;
    Label19: TLabel;
    LEstado: TLabel;
    LCP: TLabel;
    LCiudad: TLabel;
    LColonia: TLabel;
    LDireccion: TLabel;
    EX_CALLE: TDBEdit;
    EX_COLONIA: TDBEdit;
    EX_CIUDAD: TDBEdit;
    EX_CODPOST: TDBEdit;
    EX_ESTADO: TZetaDBKeyLookup;
    EX_TEL: TDBEdit;
    Panel3: TPanel;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    US_NOMBRE: TZetaDBTextBox;
    EX_FEC_INI: TZetaDBFecha;
    EX_OBSERVA: TDBMemo;
    TipoPacientelbl: TLabel;
    Label8: TLabel;
    EX_NUM_EXT: TDBEdit;
    Label9: TLabel;
    EX_NUM_INT: TDBEdit;
    gbProteccionCivil: TGroupBox;
    lblTSangre: TLabel;
    lblAlergia: TLabel;
    EX_ALERGIA: TDBEdit;
    EX_TSANGRE: TDBComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure EX_SEXOChange(Sender: TObject);
    procedure EX_TIPOChange(Sender: TObject);
    procedure btnDatosContrataClick(Sender: TObject);
    procedure btnSincronizaClick(Sender: TObject);
    procedure CB_CODIGOValidKey(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure CambioEmpleadoChange(Sender: TObject);
  private
    FEmpleado : integer;
    FCambioEmpleado : Boolean;
    procedure IniciaGenero;
    procedure EnabledControles;
    procedure SincronizaDatosEmpleado( const lDialogoSincronizado: Boolean; const lSincroniza: Boolean = FALSE );
    function BuscaDiferencias(aArreglo: array of string): Boolean;
    procedure ActualizaDatos(aArreglo: array of string);
    procedure CambiaEmpleado;
    { Private declarations }
  protected
{    procedure Connect;override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;  }
  public
    { Public declarations }
  end;

var
  EditExpediente: TEditExpediente;

implementation
uses DMedico,
     DTablas,
     DCliente,
     ZetaDialogo,
     ZAccesosTress,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZAccesosMgr,
     FAyudaContexto,
     FDatosEmpleados_DevEx,
     FTressShell;

const
     D_TEXT_DATOS_CONTRATACION = 'Datos de Contrataci�n';

{$R *.DFM}

procedure TEditExpediente.FormCreate(Sender: TObject);
begin
     inherited;
     {IndexDerechos := D_EMP_SERV_EXPEDIENTE;
     FirstControl := EX_TIPO;
     HelpContext:= H00002_Expediente;}
     //TipoValorActivo1 := stExpediente;
     Ex_Estado.LookupDataset := dmTablas.cdsEstado;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
end;

{procedure TEditExpediente.Connect;
begin
     dmTablas.cdsEstado.Conectar;
     with dmMedico do
     begin
         // DataSource.DataSet := cdsExpediente;
     end;

     FCambioEmpleado := FALSE;
     IniciaGenero;
end;
    }
procedure TEditExpediente.FormShow(Sender: TObject);
begin
     inherited;
     {AgregarBtn.Visible := False;
     BorrarBtn.Visible := False;
     ModificarBtn.Visible := False;
     DBNavigator.Visible := False;   }
     with dmMedico do
     begin
          with Campos do
          begin
               //Datasource := Self.Datasource;
               //Forma := Self;
               Paginas := PageControl;
          end;
          Campos.ConstruyeForma( Clasificaciones );

          FEmpleado := cdsExpediente.FieldByName('CB_CODIGO').AsInteger;
     end;
     PageControl.ActivePageIndex := 0;
     EnabledControles;
     //EX_SEXO.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_SERV_EXPEDIENTE, K_DERECHO_CAMBIO );
    {
     if ( Modo = dsInsert ) then
          ValorActivo1.Caption := VACIO
     else
         TipoValorActivo1 := stExpediente;   }
end;

procedure TEditExpediente.FormDestroy(Sender: TObject);
begin
     inherited;
     dmMedico.Campos.SetRedraw;

end;

procedure TEditExpediente.OKClick(Sender: TObject);
begin
     if StrVacio(EX_OBSERVA.Text) then
        EX_OBSERVA.Text := ' ';

     inherited;

     {if ( ClientDataSet.ChangeCount = 0 ) then
     begin
          ModalResult := mrOk;
          if ( NOT ( dmMedico.cdsKardexConsultas.State in [dsEdit, dsInsert] ) ) then
          begin
               if FCambioEmpleado then
                  dmCliente.cdsExpediente.Refrescar;
               TressShell.ActualizaShell( dmMedico.cdsExpediente );
          end;
     end;}
end;

procedure TEditExpediente.IniciaGenero;
begin
     with dmMedico.cdsExpediente, EX_SEXO do
     begin
          if ( FieldByName( 'EX_SEXO' ).AsString = ObtieneElemento( lfSexo, Ord(esFemenino) ) ) then
             ItemIndex:= Ord( esFemenino )
          else
             ItemIndex:= Ord( esMasculino );
     end;
end;

procedure TEditExpediente.EX_SEXOChange(Sender: TObject);
var
   sSexo: String;
begin
     inherited;
     sSexo:= ObtieneElemento( lfSexo, EX_SEXO.ItemIndex );
     with dmMedico.cdsExpediente do
          if ( sSexo <> FieldByName( 'EX_SEXO' ).AsString ) then
          begin
              { if not Editing then
                  Edit;}
               FieldByName( 'EX_SEXO' ).AsString:= sSexo;
          end;
end;

procedure TEditExpediente.EnabledControles;
 var lVisible, lEmpleadoValido : Boolean;
     eTipo : eExTipo;
     sDesTipo : string;
begin
     eTipo := eExTipo(EX_TIPO.ItemIndex);

     sDesTipo := ObtieneElemento(lfExTipo,EX_TIPO.ItemIndex);
     if ( eTipo = exPariente ) then
        sDesTipo := sDesTipo + ' de: '
     else sDesTipo := sDesTipo + ': ';
     TipoPacientelbl.Caption :=  sDesTipo;

     lVisible := eTipo in [exEmpleado, exPariente];
     lEmpleadoValido := ( CB_CODIGO.Valor <> 0 );

     CB_CODIGO.Enabled := lVisible;
     TipoPacientelbl.Enabled := lVisible;
     btnSincroniza.Enabled := lVisible AND lEmpleadoValido ;
     btnDatosContrata.Enabled := ( eTipo = exEmpleado ) and lEmpleadoValido;
end;

procedure TEditExpediente.EX_TIPOChange(Sender: TObject);
begin
     inherited;
     EnabledControles;
     CambiaEmpleado;
end;

procedure TEditExpediente.btnDatosContrataClick(Sender: TObject);
begin
     //inherited;
     if ZAccesosMgr.CheckDerecho( D_EMP_SERV_EXPEDIENTE, K_DERECHO_SIST_KARDEX ) then
     begin
          if not strVacio( CB_CODIGO.Llave )then
          begin
               {if DatosEmpleado = NIL then
                  DatosEmpleado := TDatosEmpleado.Create(self);
               DatosEmpleado.ShowModal;}
          end
          else
              ZetaDialogo.ZInformation('Operaci�n No V�lida', 'No Existe Informaci�n De Ese Empleado', 0 );
     end
     else
         ZetaDialogo.ZInformation('Operaci�n No V�lida', 'No Tiene Permiso Para Consultar Datos De Contrataci�n', 0 );
end;

procedure TEditExpediente.btnSincronizaClick(Sender: TObject);
begin
     inherited;
     SincronizaDatosEmpleado( TRUE );
end;

function TEditExpediente.BuscaDiferencias( aArreglo : array of string ): Boolean;
 var i: integer;
begin
     Result := FALSE;
     for i := Low( aArreglo ) to High( aArreglo ) do
     begin
          with dmMedico.cdsExpediente do
          begin
               Result := FieldByName( 'EX' + aArreglo[i] ).AsString <>
                         dmCliente.cdsEmpleadoLookup.FieldByName( 'CB' + aArreglo[i] ).AsString;
               if Result then
                  Break;
          end;
     end;
end;

procedure TEditExpediente.ActualizaDatos( aArreglo : array of string );
 var i : integer;
     sField : string;
begin
     with dmMedico.cdsExpediente do
     begin
          if State = dsBrowse then
             Edit;

          for i := Low(aArreglo) to High(aArreglo) do
          begin
               with FieldByName( 'EX'+aArreglo[i] ) do
               begin
                    sField := 'CB'+aArreglo[i];
                    if DataType in [ ftDate, ftDateTime ] then
                       AsDateTime := dmCliente.cdsEmpleadoLookup.FieldByName( sField ).AsDateTime
                    else
                        AsString := dmCliente.cdsEmpleadoLookup.FieldByName( sField ).AsString;
               end;
          end;
     end;
end;

procedure TEditExpediente.SincronizaDatosEmpleado( const lDialogoSincronizado: Boolean; const lSincroniza: Boolean );

  const aPersonales : Array[ 0..6 ] of string =
     ( '_NOMBRES',
       '_APE_PAT',
       '_APE_MAT',
       '_SEXO',
       '_FEC_NAC',
       '_TSANGRE',
       '_ALERGIA');

  const aDireccion : Array[ 0..7 ] of string =
     ( '_CALLE',
       '_NUM_EXT',
       '_NUM_INT',
       '_COLONIA',
       '_CIUDAD',
       '_CODPOST',
       '_ESTADO',
       '_TEL');

  var lDiferencias : Boolean;
begin
     if ( CB_CODIGO.Valor <> 0 ) AND
        NOT dmCliente.cdsEmpleadoLookup.IsEmpty then
     begin
          lDiferencias := BuscaDiferencias( aPersonales ) OR
                          BuscaDiferencias( aDireccion );

          if lDiferencias then
          begin
              { if lSincroniza or ZetaDialogo.ZConfirm(Caption, '� Desea Actualizar los Datos del Paciente con los Datos que se Encuentran en Tress ?', 0, mbNo ) then
               begin
                     dmCliente.cdsEmpleadoLookup.GetDescripcion( CB_CODIGO.Llave );

                    if eExTipo( EX_TIPO.ItemIndex ) = exEmpleado then
                       ActualizaDatos( aPersonales );

                    ActualizaDatos( aDireccion );
                    IniciaGenero;
               end;}
          end
          else
          begin
              // if lDialogoSincronizado then
               //   ZetaDialogo.ZInformation( Caption, 'Los Datos del Paciente, ya est�n Sincronizados', 0 );
          end;
     end
     else
     begin
         // ZetaDialogo.ZError( Caption, 'Empleado No Existe', 0 );
     end;
end;

procedure TEditExpediente.CB_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     with dmMedico.cdsExpediente do
     begin
          if ( eExTipo( FieldByName('EX_TIPO').AsInteger ) in [ exEmpleado, exPariente ] ) AND
             ( FEmpleado <> FieldByName('CB_CODIGO').AsInteger ) then
          begin
               SincronizaDatosEmpleado( FALSE );
               FEmpleado := FieldByName('CB_CODIGO').AsInteger;
          end;
     end;
     EnabledControles;
     CambiaEmpleado;
end;

procedure TEditExpediente.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     with dmMedico.cdsExpediente do
     begin
          zEdad.Caption := Tiempo( FieldByName('EX_FEC_NAC').AsDaTeTime, DATE , etMeses );
     end;

end;

{procedure TEditExpediente.Agregar;
begin
     //No hace nada.
end;

procedure TEditExpediente.Borrar;
begin
     //No hace nada.
end;

procedure TEditExpediente.Modificar;
begin
     //No hace nada.
end;
 }
procedure TEditExpediente.CambioEmpleadoChange(Sender: TObject);
begin
     inherited;
     CambiaEmpleado;
end;

procedure TEditExpediente.CambiaEmpleado;
begin
     FCambioEmpleado := TRUE;
end;

end.

