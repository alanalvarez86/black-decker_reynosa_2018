unit FKardexEmbarazos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls;

type
  TKardexEmbarazo = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
    //procedure DoLookup; override;
  end;

var
  KardexEmbarazo: TKardexEmbarazo;

implementation

uses
//ZetaBuscador
DMedico, ZetaCommonLists, FAyudaContexto;

{$R *.DFM}

procedure TKardexEmbarazo.FormCreate(Sender: TObject);
begin
  inherited;
  //CanLookup := True;
  HelpContext := H00105_Embarazos;
  TipoValorActivo1 := stExpediente;
end;

procedure TKardexEmbarazo.Connect;
begin
     with dmMedico do
     begin
          cdsKardexEmbarazo.Conectar;
          DataSource.DataSet:= cdsKardexEmbarazo;
     end;
end;

procedure TKardexEmbarazo.Refresh;
begin
     dmMedico.cdsKardexEmbarazo.Refrescar;
end;

procedure TKardexEmbarazo.Agregar;
begin
     dmMedico.cdsKardexEmbarazo.Agregar;
end;

procedure TKardexEmbarazo.Borrar;
begin
     dmMedico.cdsKardexEmbarazo.Borrar;
end;

procedure TKardexEmbarazo.Modificar;
begin
     dmMedico.cdsKardexEmbarazo.Modificar;
end;

{procedure TKardexEmbarazo.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', 'Embarazos', 'EX_CODIGO', dmMedico.cdsKardexEmbarazo );
end;}

{Se requiere el PuedeAgregar para que no agregue
registros cuando la base de datos esta vacia.}
function TKardexEmbarazo.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     With dmMedico do
     begin
          if cdsExpediente.Active then
          begin
               Result := inherited PuedeAgregar( sMensaje );
               if Result then
               begin
                    Result := ExisteExpediente;
                    if not Result then
                       sMensaje := 'El Expediente No Existe';
               end;
          end
          else
          begin
               if ( cdsKardexEmbarazo.FieldByname('EX_CODIGO').AsInteger <> 0 ) then
                  Result := True;
          end;
     end;
end;


end.
