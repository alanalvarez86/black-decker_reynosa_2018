unit DCliente;

interface

{$DEFINE REPORTING}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, DBClient,
     DBaseCliente,
{$ifdef DOS_CAPAS}
     DServerCatalogos,
     DServerConsultas,
     DServerGlobal,
     DServerSistema,
     DServerTablas,
   {$IFDEF REPORTING}
      DServerReporting,
   {$else}
      DServerCalcNomina,
   {$endif}
     DServerReportes,
     DServerMedico,
     DServerRecursos,  
     DServerAsistencia,
{$else}
     ServerMedico_TLB,
     Asistencia_TLB,
{$endif}
     ZetaClientDataSet,
     ZetaCommonClasses,
     FExpediente_DevEx,
     ZetaCommonLists;

type TInfoExpediente = record
    Expediente: TNumEmp;
    Tipo: eExTipo;
    Empleado: TNumEmp;
    Nombre: String;
    Activo: Boolean;
    Baja: TDate;
    Ingreso: TDate;
    Estatus: String;
    Recontratable: Boolean;
end;

type
  TdmCliente = class(TBaseCliente)
    cdsExpediente: TZetaClientDataSet;
    cdsEmpleado: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsExpedienteAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FExpediente: TNumEmp;
    //FEmpleado: TNumEmp;
    FExpedienteBOF: Boolean;
    FExpedienteEOF: Boolean;
{$ifdef DOS_CAPAS}
    FServerCatalogos: TdmServerCatalogos;
    FServerConsultas: TdmServerConsultas;
    FServerGlobal: TdmServerGlobal;
    FServerSistema: TdmServerSistema;
    FServerTablas: TdmServerTablas;
    FServerReportes : TdmServerReportes;
    {$ifdef REPORTING}
    FServerReporteador: TdmServerReporting;
    {$else}
    FServerCalcNomina: TdmServerCalcNomina;
    {$endif}
    FServerMedico : TdmServerMedico;
    FServerRecursos : TdmServerRecursos;  
    FServerAsistencia : TdmServerAsistencia;
{$endif}
    function FetchExpedien(const iExpediente: TNumEmp): Boolean;
    function FetchNextExpedien(const iExpediente: TNumEmp): Boolean;
    function FetchPreviousExpedien(const iExpediente: TNumEmp): Boolean;
    function GetAsistenciaDescripcion: String;
    function GetEmpleado : integer;
  protected
    procedure InitCacheModules; override;
  public
    { Public declarations }
{$ifdef DOS_CAPAS}
    function GetServerMedico: TdmServerMedico;  
    function GetServerAsistencia: TdmServerAsistencia;
    property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerSistema: TdmServerSistema read FServerSistema;
    property ServerTablas: TdmServerTablas read FServerTablas;
    property ServerReportes: TdmServerReportes read FServerReportes;
    {$ifdef REPORTING}
    property ServerReporteador: TdmServerReporting read FServerReporteador;
    {$else}
    property ServerCalcNomina: TdmServerCalcNomina read FServerCalcNomina;
    {$endif}
    property ServerMedico: TdmServerMedico read FServerMedico;
    property ServerRecursos: TdmServerRecursos read FServerrecursos;  
    property ServerAsistencia: TdmServerAsistencia read FServerAsistencia;
{$else}
    FServidor: IdmServerMedicoDisp;    
    FServidorAsistencia: IdmServerAsistenciaDisp;

    function GetServerMedico: IdmServerMedicoDisp;
    property ServerMedico: IdmServerMedicoDisp read GetServerMedico;
    function GetServerAsistencia: IdmServerAsistenciaDisp;
    property ServerAsistencia: IdmServerAsistenciaDisp read GetServerAsistencia;
{$endif}
    property Empleado: TNumEmp read GetEmpleado;
    property Expediente: TNumEmp read FExpediente;
    function GetValorActivoStr(const eTipo: TipoEstado): String; override;
    function GetExpedienteAnterior: Boolean;
    function GetExpedienteInicial: Boolean;
    function GetExpedientePrimero: Boolean;
    function GetExpedienteSiguiente: Boolean;
    function GetExpedienteUltimo: Boolean;
    //function GetDatosEmpleadoActivo: TInfoEmpleado;
    function GetDatosExpedienteActivo: TInfoExpediente;
    function SetExpedienteNumero(const Value: TNumEmp): Boolean;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function GetExpedienteDescripcion: String;
    function GetEmpleadoDescripcion: String;
    procedure CargaActivosTodos(Parametros: TZetaParams); override;
    procedure CargaActivosIMSS( Parametros: TZetaParams );
    procedure CargaActivosPeriodo(Parametros: TZetaParams);
    procedure CargaActivosSistema(Parametros: TZetaParams);
    procedure InitActivosExpediente;
    procedure PosicionaSiguienteExpediente;
    function ExpedienteDownEnabled: Boolean;
    function ExpedienteUpEnabled: Boolean;
    function EsEmpleado: boolean;

    procedure GetStatusEmpleado(EmpleadoActivo: TInfoExpediente; var Caption, Hint : string; var Color : TColor );

  end;

var
  dmCliente: TdmCliente;

implementation

{$R *.DFM}

uses DTablas,
     DCatalogos,
     DDiccionario,
     DMedico,
     ZetaCommonTools,
     FTressShell,
     DGlobal,
     ZGlobalTress;

{ ************** TdmCliente *************** }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin

     inherited;
     ModoServicioMedico := TRUE;
     SetTipoLookupEmpleado( eLookEmpMedicos );

{$ifdef DOS_CAPAS}
     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerConsultas := TdmServerConsultas.Create( Self );
     FServerGlobal := TdmServerGlobal.Create( Self );
     FServerSistema := TdmServerSistema.Create( Self );
     FServerTablas := TdmServerTablas.Create( Self );
     FServerReportes := TdmServerReportes.Create( Self );
     {$ifdef REPORTING}
     FServerReporteador := TdmServerReporting.Create( self );
     {$else}
     FServerCalcNomina := TdmServerCalcNomina.Create( Self );
     {$endif}
     FServerMedico := TdmServerMedico.Create(Self);
     FServerRecursos := TdmServerRecursos.Create(Self);  
     FServerAsistencia := TdmServerAsistencia.Create(Self);
{$endif}
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     FreeAndNil( FServerRecursos);
     FreeAndNil( FServerMedico );
     FreeAndNil( FServerAsistencia );
     {$ifdef REPORTING}
     FreeAndNil( FServerReporteador );
     {$else}
     FreeAndNil( FServerCalcNomina );
     {$endif}

     FreeAndNil( FServerReportes );
     FreeAndNil( FServerTablas );
     FreeAndNil( FServerSistema );
     FreeAndNil( FServerGlobal );
     FreeAndNil( FServerConsultas );
     FreeAndNil( FServerCatalogos );
{$endif}
     inherited;
end;

{$ifdef DOS_CAPAS}
function TdmCliente.GetServerMedico: TdmServerMedico;
begin
     Result := ServerMedico;
end;
function TdmCliente.GetServerAsistencia: TdmServerAsistencia;
begin
     Result := ServerAsistencia;
end;
{$else}
function TdmCliente.GetServerMedico: IdmServerMedicoDisp;
begin
     Result := IdmServerMedicoDisp( CreaServidor( CLASS_dmServerMedico, FServidor ) );
end;
procedure TdmCliente.GetStatusEmpleado(EmpleadoActivo: TInfoExpediente;
  var Caption, Hint: string; var Color: TColor);
const
   aRecontratable: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( ZetaCommonClasses.CR_LF + 'NO RECONTRATAR', VACIO );
begin
      with EmpleadoActivo do
      begin
            if (not Global.GetGlobalBooleano(K_GLOBAL_CB_ACTIVO_AL_DIA) ) then
            begin
                 if Activo then
                 begin
                      if ( Baja = NullDateTime ) then
                      begin
                           Caption := 'Activo';
                           Hint := 'Ingres� ' + FechaCorta( Ingreso );
                           Color := clBlack;
                      end
                      else
                      begin
                           Caption := 'Reingreso';
                           Hint := 'Baja: ' + FechaCorta( Baja ) + ' Reingreso: ' + FechaCorta( Ingreso );
                           Color := clGreen;
                      end;
                 end
                 else
                 begin
                      Caption := 'Baja';
                      Hint := 'Baja desde ' + FechaCorta( Baja ) + aRecontratable[ Recontratable ];
                      Color := clRed;
                 end;
            end
            else
            begin
                 if Activo then
                 begin
                      if ( Baja = NullDateTime ) then
                      begin
                           if ( Ingreso > FechaDefault ) then
                           begin
                                Caption := 'Antes ingreso';
                                Hint := 'Ingresa ' + FechaCorta( Ingreso );
                                Color := clRed;
                           end
                           else
                           begin
                                Caption := 'Activo';
                                Hint := 'Ingres� ' + FechaCorta( Ingreso );
                                Color := clBlue;
                           end;
                      end
                      else if (Ingreso <= FechaDefault) then
                      begin
                           Caption := 'Reingreso';
                           Hint := 'Baja: ' + FechaCorta( Baja ) + ' Reingreso: ' + FechaCorta( Ingreso );
                           Color := clGreen;
                      end
                      else
                      begin
                           Caption := 'Baja';
                           Hint := 'Baja desde ' + FechaCorta( Baja )+ aRecontratable [ Recontratable ];
                           Color := clRed;
                      end;
                 end
                 else
                 begin
                      if not EsEmpleado then
                      begin
                           Caption := 'Antes ingreso';
                           //Hint := 'Baja desde ' + FechaCorta( Baja )+ aRecontratable[ Recontratable ];
                           Color := clBlue;
                      end
                      else if Baja < FechaDefault then
                      begin
                           Caption := 'Baja';
                           Hint := 'Baja desde ' + FechaCorta( Baja )+ aRecontratable[ Recontratable ];
                           Color := clRed;
                      end
                      else
                      begin
                           Caption := 'Activo';
                           Hint := 'Ingres� ' + FechaCorta( Ingreso );
                           Color := clBlack;
                      end;
                 end;
            end;
      end;

end;

function TdmCliente.GetServerAsistencia: IdmServerAsistenciaDisp;
begin
     Result := IdmServerAsistenciaDisp( CreaServidor( CLASS_dmServerAsistencia, FServidorAsistencia ) );
end;
{$endif}

procedure TdmCliente.InitCacheModules;
begin
     inherited;
     DataCache.AddObject( 'Tabla', dmTablas );
     DataCache.AddObject( 'Cat�logo', dmCatalogos );
     DataCache.AddObject( 'Diccionario', dmDiccionario );
     DataCache.AddObject( 'Servicios M�dicos', dmMedico );
end;

procedure TdmCliente.CargaActivosIMSS( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', VACIO );
          AddInteger( 'IMSSYear', 0 );
          AddInteger( 'IMSSMes', 0 );
          AddInteger( 'IMSSTipo', 0 );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddInteger( 'Year', TheYear( FechaDefault ) );
          AddInteger( 'Tipo', 0 );
          AddInteger( 'Numero', 0 );
     end;
end;

procedure TdmCliente.CargaActivosSistema( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaDefault );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( FechaDefault ) );
          AddInteger( 'EmpleadoActivo', Empleado );
          AddString( 'NombreUsuario', GetDatosUsuarioActivo.Nombre );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.InitActivosExpediente;
begin
     FExpedienteBOF := False;
     FExpedienteEOF := False;
     FExpediente := 0;
     FFechaDefault := Date;
     GetExpedienteInicial;
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     CargaActivosIMSS( Parametros );
     CargaActivosPeriodo( Parametros );
     CargaActivosSistema( Parametros );
end;


function TdmCliente.GetEmpleadoDescripcion: String;
begin
     with dmCliente.cdsEmpleadoLookUp do
          Result := 'Empleado ' + FieldByName('CB_CODIGO').AsString + ': ' +
                    FieldByName('PrettyName').AsString;
end;

function TdmCliente.GetExpedienteDescripcion: String;
begin
     with cdsExpediente do
     begin
          Result := 'Paciente ' + IntToStr( FExpediente ) + ': '+ FieldByName( 'PRETTYNAME' ).AsString;
     end;
end;

function TdmCliente.GetAsistenciaDescripcion: String;
begin
     Result := FechaCorta( FechaDefault );
end;

function TdmCliente.GetValorActivoStr(const eTipo: TipoEstado): String;
begin
     case eTipo of
          stExpediente: Result := GetExpedienteDescripcion;
          stEmpleado: Result := GetEmpleadoDescripcion;
          stFecha: Result := GetAsistenciaDescripcion;
     end;
end;

function TdmCliente.GetExpedienteAnterior: Boolean;
begin
     Result := FetchPreviousExpedien( FExpediente );
     if Result then
     begin
          with cdsExpediente do
          begin
               FExpediente := FieldByName( 'EX_CODIGO' ).AsInteger;
          end;
          FExpedienteEOF := False;
     end
     else
         FExpedienteBOF := True;
end;

function TdmCliente.GetExpedienteSiguiente: Boolean;
begin
     Result := FetchNextExpedien( FExpediente );
     if Result then
     begin
          with cdsExpediente do
          begin
               FExpediente := FieldByName( 'EX_CODIGO' ).AsInteger;
          end;
          FExpedienteBOF := False;
     end
     else
         FExpedienteEOF := True;
end;

function TdmCliente.GetExpedienteUltimo: Boolean;
var
   iNewExp: TNumEmp;
begin
     Result := FetchPreviousExpedien( MAXINT );
     if Result then
     begin
          with cdsExpediente do
          begin
               iNewExp := FieldByName( 'EX_CODIGO' ).AsInteger;
               FExpedienteBOF := False;
               FExpediente := iNewExp;
               FExpedienteEOF := ( iNewExp = FExpediente );
          end;
     end;
end;

function TdmCliente.FetchExpedien( const iExpediente: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := ServerMedico.ExpedienteActivo( Empresa, iExpediente, Datos );
     if Result then
     begin
          with cdsExpediente do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.FetchPreviousExpedien( const iExpediente: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := ServerMedico.ExpedienteAnterior( Empresa, iExpediente, Datos );
     if Result then
     begin
          with cdsExpediente do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.FetchNextExpedien( const iExpediente: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := ServerMedico.ExpedienteSiguiente( Empresa, iExpediente, Datos );
     if Result then
     begin
          with cdsExpediente do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.GetExpedientePrimero: Boolean;
var
   iNewExp: TNumEmp;
begin
     Result := FetchNextExpedien( 0 );
     if Result then
     begin
          with cdsExpediente do
          begin
               iNewExp := FieldByName( 'EX_CODIGO' ).AsInteger;
               FExpediente := iNewExp;
               FExpedienteBOF := ( iNewExp = FExpediente );
               FExpedienteEOF := False;
          end;
     end;
end;

function TdmCliente.GetExpedienteInicial: Boolean;
var
   Datos: OleVariant;
begin
     Result := ServerMedico.ExpedienteSiguiente( Empresa, 0, Datos );
     with cdsExpediente do
     begin
          Data := Datos;
          FExpediente := FieldByName( 'EX_CODIGO' ).AsInteger;
     end;
     FExpedienteBOF := True;
end;

function TdmCliente.GetDatosExpedienteActivo: TInfoExpediente;
begin
     with Result do
     begin
          Expediente := Self.Expediente;
          with cdsExpediente do
          begin
               Nombre := FieldByName( 'PRETTYNAME' ).AsString;
               Activo := zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
               Baja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
               Ingreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
               Tipo := eExTipo(FieldByName( 'EX_TIPO' ).AsInteger);
               Empleado := FieldByName( 'CB_CODIGO' ).AsInteger;

               {if((Activo) and (DateToStrSQL(Baja) = '12/30/1899'))then
                  Estatus := 'Activo'
               else if (Activo and (DateToStrSQL(Baja) <> '12/30/1899') ) then
                  Estatus := 'Reingreso'
               else
                  Estatus := 'Baja'; }

          end;
     end;
end;

procedure TdmCliente.cdsExpedienteAlAdquirirDatos(Sender: TObject);
begin
  inherited;
  FetchExpedien( FExpediente );
end;

function TdmCliente.SetExpedienteNumero( const Value: TNumEmp ): Boolean;
begin
     if ( FExpediente <> Value ) then
     begin
          Result := FetchExpedien( Value );
          if Result then
          begin
               with cdsExpediente do
               begin
                    FExpediente := FieldByName( 'EX_CODIGO' ).AsInteger;
               end;
               FExpedienteBOF := False;
               FExpedienteEOF := False;
          end;
     end
     else
         Result := True;
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
end;

function TdmCliente.GetEmpleado: integer;
begin
     Result := cdsExpediente.FieldByName( 'CB_CODIGO' ).AsInteger;
end;

procedure TdmCliente.PosicionaSiguienteExpediente;
begin
     if not GetExpedienteSiguiente then // Para posicionar en el siguiente empleado o el Ultimo
        GetExpedienteUltimo;
     TressShell.ActualizaShell( cdsExpediente );
     //TressShell.CambioExpedienteActivo;
end;

function TdmCliente.EsEmpleado: boolean;
begin
  Result := ( eExTipo( dmCliente.cdsExpediente.FieldByName( 'EX_TIPO' ).AsInteger ) = exEmpleado );
end;

function TdmCliente.ExpedienteDownEnabled: Boolean;
begin
     with cdsExpediente do
     begin
          Result := Active and not IsEmpty and not FExpedienteBOF;
     end;
end;

function TdmCliente.ExpedienteUpEnabled: Boolean;
begin
     with cdsExpediente do
     begin
          Result := Active and not IsEmpty and not FExpedienteEOF;
     end;
end;
end.

