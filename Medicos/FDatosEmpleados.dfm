inherited DatosEmpleado: TDatosEmpleado
  Left = 336
  Top = 191
  Caption = 'Datos del Empleado'
  ClientHeight = 276
  ClientWidth = 582
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 240
    Width = 582
    inherited OK: TBitBtn
      Left = 414
      Visible = False
    end
    inherited Cancelar: TBitBtn
      Left = 499
    end
  end
  inherited PanelSuperior: TPanel
    Width = 582
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 582
    inherited ValorActivo2: TPanel
      Width = 256
    end
  end
  object gbDatosEmpleado: TGroupBox [3]
    Left = 0
    Top = 51
    Width = 416
    Height = 189
    Align = alLeft
    Caption = ' Datos Del Empleado '
    TabOrder = 3
    object Label10: TLabel
      Left = 10
      Top = 128
      Width = 86
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Turno:'
    end
    object Label11: TLabel
      Left = 10
      Top = 75
      Width = 86
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Puesto:'
    end
    object Label12: TLabel
      Left = 10
      Top = 101
      Width = 86
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Clasificación:'
    end
    object Label14: TLabel
      Left = 10
      Top = 48
      Width = 86
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Contrato:'
    end
    object Label15: TLabel
      Left = 10
      Top = 22
      Width = 86
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Fecha de Ingreso:'
    end
    object TU_DESCRIP: TZetaDBTextBox
      Left = 153
      Top = 125
      Width = 250
      Height = 18
      AutoSize = False
      Caption = 'TU_DESCRIP'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'TU_DESCRIP'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object PU_DESCRIP: TZetaDBTextBox
      Left = 153
      Top = 72
      Width = 250
      Height = 18
      AutoSize = False
      Caption = 'PU_DESCRIP'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'PU_DESCRIP'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENT: TZetaDBTextBox
      Left = 153
      Top = 98
      Width = 250
      Height = 18
      AutoSize = False
      Caption = 'TB_ELEMENT'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CLASIFICACION'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CONTRATO: TZetaDBTextBox
      Left = 153
      Top = 45
      Width = 250
      Height = 18
      AutoSize = False
      Caption = 'CONTRATO'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CONTRATO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CB_FEC_ING: TZetaDBTextBox
      Left = 100
      Top = 19
      Width = 123
      Height = 18
      AutoSize = False
      Caption = 'CB_FEC_ING'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_FEC_ING'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENT2: TZetaDBTextBox
      Left = 153
      Top = 151
      Width = 250
      Height = 18
      AutoSize = False
      Caption = 'TB_ELEMENT2'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'ESTADOCIVIL'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label17: TLabel
      Left = 10
      Top = 154
      Width = 86
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Estado Civil:'
    end
    object CB_CONTRAT: TZetaDBTextBox
      Left = 100
      Top = 45
      Width = 50
      Height = 18
      AutoSize = False
      Caption = 'CB_CONTRAT'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_CONTRAT'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CB_PUESTO: TZetaDBTextBox
      Left = 100
      Top = 72
      Width = 50
      Height = 18
      AutoSize = False
      Caption = 'CB_PUESTO'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_PUESTO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CB_CLASIFI: TZetaDBTextBox
      Left = 100
      Top = 98
      Width = 50
      Height = 18
      AutoSize = False
      Caption = 'CB_CLASIFI'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_CLASIFI'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CB_TURNO: TZetaDBTextBox
      Left = 100
      Top = 125
      Width = 50
      Height = 18
      AutoSize = False
      Caption = 'CB_TURNO'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_TURNO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CB_EDO_CIV: TZetaDBTextBox
      Left = 100
      Top = 151
      Width = 50
      Height = 18
      AutoSize = False
      Caption = 'CB_EDO_CIV'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_EDO_CIV'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object FotoSwitch: TSpeedButton
      Left = 280
      Top = 14
      Width = 123
      Height = 24
      Hint = 'Mostrar Foto'
      AllowAllUp = True
      GroupIndex = 1
      Caption = 'Mostrar Foto'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        033333FFFF77777773F330000077777770333777773FFFFFF733077777000000
        03337F3F3F777777733F0797A770003333007F737337773F3377077777778803
        30807F333333337FF73707888887880007707F3FFFF333777F37070000878807
        07807F777733337F7F3707888887880808807F333333337F7F37077777778800
        08807F333FFF337773F7088800088803308073FF777FFF733737300008000033
        33003777737777333377333080333333333333F7373333333333300803333333
        33333773733333333333088033333333333373F7F33333333333308033333333
        3333373733333333333333033333333333333373333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      Spacing = -1
      OnClick = FotoSwitchClick
    end
  end
  object FOTOGRAFIA: TPDBMultiImage [4]
    Left = 416
    Top = 51
    Width = 166
    Height = 189
    Hint = 'Foto del Empleado'
    ImageReadRes = lAutoMatic
    ImageWriteRes = sAutoMatic
    DataEngine = deBDE
    JPegSaveQuality = 25
    JPegSaveSmooth = 0
    PNGInterLaced = True
    ImageDither = True
    UpdateAsJPG = False
    UpdateAsBMP = False
    UpdateAsGIF = False
    UpdateAsPCX = False
    UpdateAsPNG = False
    UpdateAsTIF = True
    Align = alClient
    Ctl3D = True
    DataField = 'FOTOGRAFIA'
    DataSource = DataSource
    ParentCtl3D = False
    RichTools = False
    ReadOnly = True
    Stretch = True
    TabOrder = 4
    TextLeft = 0
    TextTop = 0
    TextRotate = 0
    TextTransParent = False
    TifSaveCompress = sNONE
    UseTwainWindow = False
    Visible = False
  end
  inherited DataSource: TDataSource
    Left = 492
    Top = 17
  end
end
