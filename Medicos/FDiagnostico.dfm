inherited Diagnostico: TDiagnostico
  Left = 215
  Top = 453
  Caption = 'Diagn'#243'stico'
  ClientHeight = 239
  ClientWidth = 649
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 649
    ExplicitWidth = 649
    inherited ValorActivo2: TPanel
      Width = 390
      ExplicitWidth = 390
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 649
    Height = 220
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'DA_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DA_NOMBRE'
        Title.Caption = 'Descripci'#243'n'
        Width = 230
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DA_INGLES'
        Title.Caption = 'Ingl'#233's'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DA_NUMERO'
        Title.Caption = 'N'#250'mero'
        Width = 67
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DA_TEXTO'
        Title.Caption = 'Texto'
        Width = 150
        Visible = True
      end>
  end
end
