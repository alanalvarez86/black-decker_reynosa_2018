inherited EditIncaPermDlg: TEditIncaPermDlg
  Left = 435
  Top = 338
  Caption = 'Incapacidades/Permisos'
  ClientHeight = 150
  ClientWidth = 206
  OldCreateOrder = True
  OnShow = FormShow
  ExplicitWidth = 212
  ExplicitHeight = 178
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 114
    Width = 206
    ExplicitTop = 114
    ExplicitWidth = 206
    inherited OK: TBitBtn
      Left = 38
      OnClick = OKClick
      ExplicitLeft = 38
    end
    inherited Cancelar: TBitBtn
      Left = 123
      ExplicitLeft = 123
    end
  end
  object rgOperacion: TRadioGroup
    Left = 37
    Top = 20
    Width = 137
    Height = 78
    Caption = ' Registrar: '
    ItemIndex = 0
    Items.Strings = (
      '&Incapacidad'
      '&Permiso')
    TabOrder = 1
    OnClick = rgOperacionClick
  end
end
