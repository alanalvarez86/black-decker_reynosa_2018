unit FEditMedicinaEntr;

interface

uses
  //ZBaseEdicion
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaEdit, Mask,
  ZetaNumero, ZetaKeyLookup, ZetaDBTextBox, ZetaFecha;

type
  TEditMedEntr = class(TObject)
//  TEditMedEntr = class(TBaseEdicion)
    Label1: TLabel;
    ME_CODIGO: TZetaDBKeyLookup;
    MT_CANTIDA: TZetaDBNumero;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    US_NOMBRE: TZetaDBTextBox;
    Label5: TLabel;
    MT_FECHA: TZetaDBFecha;
    MT_COMENTA: TDBEdit;
    ME_MEDIDAlb: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure ME_CODIGOExit(Sender: TObject);
  Protected
    { Protected declarations }
    //procedure Connect; override;
    //procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditMedEntr: TEditMedEntr;

implementation

uses
     //ZetaBuscador
     DMedico, ZAccesosTress, ZetaCommonLists, DSistema, FAyudaContexto,
     ZetaCommonTools;

{$R *.DFM}

procedure TEditMedEntr.FormCreate(Sender: TObject);
begin
     inherited;
     {IndexDerechos := ZAccesosTress.D_MEDICINA_ENTR;
     FirstControl := ME_CODIGO;
     TipoValorActivo1 := stExpediente;
     HelpContext:= H00004_Medicina_entregada;}
     ME_CODIGO.LookupDataset := dmMedico.cdsMedicina;
end;

{procedure TEditMedEntr.Connect;
begin
     with dmMedico do
     begin
          cdsMedicina.Conectar;
        //  DataSource.DataSet:= cdsMedicinaEntr;
     end;
end;        }

{procedure TEditMedEntr.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', 'Medicina Entregada', 'ME_CODIGO', dmMedico.cdsMedicinaEntr );
end;}

procedure TEditMedEntr.ME_CODIGOExit(Sender: TObject);
begin
  inherited;
     with ME_MEDIDAlb do
     begin
          Caption := dmMedico.cdsMedicina.FieldByName('ME_MEDIDA').AsString;
          Visible := strLleno(ME_CODIGO.Llave);
     end;     
end;

end.


