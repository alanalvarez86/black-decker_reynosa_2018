unit FGlobalesMedico;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, ComCtrls, ToolWin, StdCtrls, Buttons, ImgList,
     //ZBaseGlobal,
     ZBaseConsulta,
     ZetaDBTextBox, dxSkinsCore, TressMorado2013, dxSkinsdxBarPainter,
  cxGraphics, dxBar, cxClasses;

type
  TGlobalesMedico = class(TBaseConsulta)
    PanelTitulos: TPanel;
    ImageList: TImageList;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RfcLbl: TZetaDBTextBox;
    InfonavitLbl: TZetaDBTextBox;
    RazonLbl: TZetaDBTextBox;
    dxBarDockControl1: TdxBarDockControl;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    Identificacion_DevEx: TdxBarLargeButton;
    MisDatos_DevEx: TdxBarLargeButton;
    lista_DevEx: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    Bloqueo_DevEx: TdxBarLargeButton;
    Bitacora_DevEx: TdxBarLargeButton;
    Seguridad_DevEx: TdxBarLargeButton;
    Reportes_DevEx: TdxBarLargeButton;
    Cafeteria_DevEx: TdxBarLargeButton;
    Asistencia_DevEx: TdxBarLargeButton;
    Nomina_DevEx: TdxBarLargeButton;
    Recursos_DevEx: TdxBarLargeButton;
    Adicionales_DevEx: TdxBarLargeButton;
    Areas_DevEx: TdxBarLargeButton;
    Supervisroes_DevEx: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    Capacitacion_DevEx: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    Imss_DevEx: TdxBarLargeButton;
    cxImageList1: TcxImageList;
    procedure FormCreate(Sender: TObject);
    procedure BConfiguracionClick(Sender: TObject);
    procedure Identificacion_DevExClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  GlobalesMedico: TGlobalesMedico;

implementation

uses DGlobal,
     DMedico,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonClasses,
     FAyudaContexto,
     ZGlobalTress;

{$R *.DFM}

{ ************* TSistGlobales *************** }

procedure TGlobalesMedico.FormCreate(Sender: TObject);
begin
     inherited;
     EsConsulta := False;
     HelpContext := H00101_Configurar_Expediente;
end;

procedure TGlobalesMedico.Identificacion_DevExClick(Sender: TObject);
begin
  inherited;
     if ZAccesosMgr.CheckDerecho(  D_SERV_GLOBALES, K_DERECHO_CAMBIO ) then
        dmMedico.AbrirConfiguracion
     else
         ZetaDialogo.ZInformation('Operaci�n No V�lida', 'No Se Tiene Permiso Para Modificar Globales', 0 );


end;

procedure TGlobalesMedico.Connect;
begin
     with Global do
     begin
          RFCLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RFC_EMPRESA );
          InfonavitLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_INFONAVIT_EMPRESA );
          RazonLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RAZON_EMPRESA );
     end;
end;



procedure TGlobalesMedico.BConfiguracionClick(Sender: TObject);
begin
     inherited;
     if ZAccesosMgr.CheckDerecho(  D_SERV_GLOBALES, K_DERECHO_CAMBIO ) then
        dmMedico.AbrirConfiguracion
     else
         ZetaDialogo.ZInformation('Operaci�n No V�lida', 'No Se Tiene Permiso Para Modificar Globales', 0 );
end;

procedure TGlobalesMedico.Refresh;
begin
end;

end.

