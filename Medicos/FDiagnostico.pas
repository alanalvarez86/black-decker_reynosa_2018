unit FDiagnostico;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls;

type
  TDiagnostico = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Diagnostico: TDiagnostico;

implementation

uses
  //ZetaBuscador
DMedico, FAyudaContexto;

{$R *.DFM}

procedure TDiagnostico.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := True;
  HelpContext := H00006_Catalogo_de_Diagnosticos;
end;

procedure TDiagnostico.Connect;
begin
     with dmMedico do
     begin
          cdsDiagnostico.Conectar;
          DataSource.DataSet:= cdsDiagnostico;
     end;
end;

procedure TDiagnostico.Refresh;
begin
     dmMedico.cdsDiagnostico.Refrescar;
end;

procedure TDiagnostico.Agregar;
begin
     dmMedico.cdsDiagnostico.Agregar;
end;

procedure TDiagnostico.Borrar;
begin
     dmMedico.cdsDiagnostico.Borrar;
end;

procedure TDiagnostico.Modificar;
begin
     dmMedico.cdsDiagnostico.Modificar;
end;

procedure TDiagnostico.DoLookup;
begin
     inherited;
     //ZetaBuscador.BuscarCodigo( 'C�digo', 'Diagn�stico', 'DA_CODIGO', dmMedico.cdsDiagnostico );
end;

end.
