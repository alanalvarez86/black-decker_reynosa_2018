unit FEditRecord;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, DBCtrls, ZetaFecha;

type
  TEditRecord = class(TForm)
    lblEmail: TLabel;
    lblNombre: TLabel;
    lblEmpresa: TLabel;
    bbOk: TBitBtn;
    bbCancel: TBitBtn;
    BL_EMAIL: TDBEdit;
    BL_NOMBRE: TDBEdit;
    BL_EMPRESA: TDBEdit;
    BL_ACTIVO: TDBCheckBox;
    BL_ALTA: TZetaDBFecha;
    lblFechaAlta: TLabel;
    BL_BAJA: TZetaDBFecha;
    lblFechaBaja: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EditRecord: TEditRecord;

implementation

uses DEMails,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

end.
