object GetDireccion: TGetDireccion
  Left = 0
  Top = 0
  Width = 483
  Height = 365
  ActiveControl = BLEMAIL
  Background.Fixed = False
  HandleTabs = False
  StyleSheet.Filename = 'D:\Sentinel\Boletin\Suscripcion\Templates\GetDireccion.html'
  SupportedBrowsers = [brIE, brNetscape6]
  TemplateProcessor = IWTemplate
  OnDefaultAction = IWAppFormDefaultAction
  DesignLeft = 314
  DesignTop = 127
  object DireccionLBL: TIWLabel
    Left = 19
    Top = 106
    Width = 103
    Height = 16
    ZIndex = 0
    Font.Color = clNone
    Font.Enabled = True
    Font.Size = 10
    Font.Style = []
    AutoSize = False
    Caption = 'Direcci�n e-Mail:'
  end
  object BLEMAIL: TIWEdit
    Left = 120
    Top = 103
    Width = 321
    Height = 21
    ZIndex = 0
    BGColor = clNone
    DoSubmitValidation = True
    Editable = True
    Font.Color = clNone
    Font.Enabled = True
    Font.Size = 10
    Font.Style = []
    FriendlyName = 'BL_EMAIL'
    MaxLength = 0
    ReadOnly = False
    Required = False
    ScriptEvents = <>
    TabOrder = 0
    PasswordPrompt = False
  end
  object AgregarBTN: TIWButton
    Left = 123
    Top = 151
    Width = 257
    Height = 45
    ZIndex = 0
    ButtonType = btButton
    Caption = 'Suscribir Al Bolet�n'
    Color = clAqua
    DoSubmitValidation = True
    Font.Color = clBlack
    Font.Enabled = True
    Font.Size = 10
    Font.Style = [fsBold]
    ScriptEvents = <>
    TabOrder = 1
    OnClick = AgregarBTNClick
  end
  object BorrarBTN: TIWButton
    Left = 123
    Top = 203
    Width = 257
    Height = 45
    ZIndex = 0
    ButtonType = btButton
    Caption = 'Borrar De Lista De Suscriptores'
    Color = clRed
    DoSubmitValidation = True
    Font.Color = clNone
    Font.Enabled = True
    Font.Size = 10
    Font.Style = [fsBold]
    ScriptEvents = <>
    TabOrder = 2
    OnClick = BorrarBTNClick
  end
  object SalirLNK: TIWLink
    Left = 232
    Top = 264
    Width = 65
    Height = 41
    ZIndex = 0
    Caption = 'Salir'
    Font.Color = clBlue
    Font.Enabled = True
    Font.Size = 15
    Font.Style = []
    ScriptEvents = <>
    DoSubmitValidation = False
    OnClick = SalirLNKClick
  end
  object IWTemplate: TIWTemplateProcessorHTML
    Enabled = True
    MasterFormTag = True
    TagType = ttIntraWeb
    Left = 64
    Top = 24
  end
end
