unit IWMensaje;
{PUBDIST}

interface

uses
  IWAppForm, IWApplication, IWTypes,
  Classes, Controls, SysUtils, Graphics,
  IWControl,  IWExtCtrls, IWCompLabel, IWLayoutMgr, IWTemplateProcessorHTML,
  IWHTMLControls;

type
  TMensaje = class(TIWAppForm)
    IWTimer: TIWTimer;
    MensajeLBL: TIWLabel;
    IWTemplate: TIWTemplateProcessorHTML;
    RegresarLNK: TIWLink;
    procedure IWTimerTimer(Sender: TObject);
    procedure RegresarLNKClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure SetError( const sValue: String );
    procedure SetMensaje( const sValue: String );
  end;

implementation

{$R *.dfm}

uses
  ServerController;

{ TMensaje }

procedure TMensaje.SetMensaje(const sValue: String);
begin
     with MensajeLBL do
     begin
          Font.Color := clGreen;
          Text := Format( '%s', [ sValue ] );
     end;
end;

procedure TMensaje.SetError(const sValue: String);
begin
     with MensajeLBL do
     begin
          Font.Color := clRed;
          Text := Format( '%s', [ sValue ] );
     end;
end;

procedure TMensaje.IWTimerTimer(Sender: TObject);
begin
     Release;
end;

procedure TMensaje.RegresarLNKClick(Sender: TObject);
begin
     Release;
end;

end.