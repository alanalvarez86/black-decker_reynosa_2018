unit FLibres;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, StdCtrls, Buttons, ExtCtrls;

type
  TFormLibres = class(TForm)
    DBGridLibres: TDBGrid;
    DataSource: TDataSource;
    PanelInferior: TPanel;
    Salir: TBitBtn;
    procedure DBGridLibresDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    function Conectar: Boolean;
    function GetSentinelLibre: Integer;
  end;

var
  FormLibres: TFormLibres;

function EscogeLibre( var iLibre: Integer ): Boolean;

implementation

uses ZetaDialogo,
     DSentinel;

{$R *.DFM}

function EscogeLibre( var iLibre: Integer ): Boolean;
begin
     if ( FormLibres = nil ) then
        FormLibres := TFormLibres.Create( Application );
     with FormLibres do
     begin
          if Conectar then
          begin
               ShowModal;
               if ( ModalResult = mrOK ) then
               begin
                    iLibre := GetSentinelLibre;
                    Result := True;
               end
               else
               begin
                    iLibre := 0;
                    Result := False;
               end;
          end
          else
          begin
               ZetaDialogo.ZError( '� Atenci�n !', 'No Hay Sentinelas Disponibles', 0 );
               iLibre := 0;
               Result := False;
          end;
     end;
end;

{ ****** TFormLibres ****** }

procedure TFormLibres.FormShow(Sender: TObject);
begin
     dmSentinel.AbrirLibres( Datasource );
end;

procedure TFormLibres.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     dmSentinel.ResetDatasource( Datasource );
end;

function TFormLibres.Conectar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Result := dmSentinel.ConectarLibres;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TFormLibres.GetSentinelLibre: Integer;
begin
     Result := dmSentinel.qLibre.FieldByName( 'SN_NUMERO' ).AsInteger;
end;

procedure TFormLibres.DBGridLibresDblClick(Sender: TObject);
begin
     ModalResult := mrOk;
end;

end.
