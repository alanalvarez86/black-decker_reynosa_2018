�
 TFORMINSTALACION 0$  TPF0TFormInstalacionFormInstalacionLeft"Top� Width Height}BorderIconsbiSystemMenu
biMaximize CaptionClaves de Instalaci�nColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top WidthHeight0AlignalTop
BevelOuterbvNoneTabOrder  TLabelLabel2LeftTopWidth0HeightCaptionNombre:Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel3LeftTop	Width=HeightCaption
Sistema #:Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TZetaDBTextBox
SI_RAZ_SOCLeftKTopWidthvHeightAutoSizeCaption
SI_RAZ_SOCFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField
SI_RAZ_SOC
DataSource	dsSistemaFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxSI_FOLIOLeftKTopWidthqHeightAutoSizeCaptionSI_FOLIOFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldSI_FOLIO
DataSource	dsSistemaFormatFloat%14.2nFormatCurrency%m  TBitBtnAgregarLeft�TopWidthKHeight*Hint Generar Una Clave De Instalaci�nAnchorsakTopakRight CaptionGenerarParentShowHintShowHint	TabOrder OnClickAgregarClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUUU�����_UWwwwwuUWwwwwuUUUUUUUUUUUU�Uu�UUUPP0UUUUWu���UUU�  UUUUuWww�UUP���UUUWUuWW�UU���UUUUUuW�UU��� UUUUWUw�UP���0UUWUUW��U�� 0UUuu�Uw��P�࿰�0UW�W_WW�� ���0Uw�u����  ��  0Uww�Www��  U[�UwwuUWw�P  UUP0UWwwUUW��U UUY�UUwuUUWwULayout
blGlyphTop	NumGlyphs   TPanelPanel3Left TopAWidthHeight AlignalBottomTabOrder TSpeedButtonImprimirBtnLeftTopWidthHeightHintImprimir Autorizaci�n
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 0      ?��������������wwwwwww�������wwwwwww        ���������������wwwwwww�������wwwwwww�������wwwwww        wwwwwww30����337���?330� 337�wss330����337��?�330�  337�swws330���3337��73330��3337�ss3330�� 33337��w33330  33337wws333	NumGlyphsParentShowHintShowHint	OnClickImprimirBtnClick  TBitBtnBitBtn1Left�TopWidthKHeightAnchorsakTopakRight Cancel	Caption&SalirDefault	ModalResultTabOrder 
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 8����w���������33?  DD@���DD������3��  33MP��33�����38��  33E���3333���38�?  33M]��3333�x�38�?  33E���3333���38�?  33M]��3333�x�38�?  33E���3333���38�?  33M]��3333�x�38�?  33E���3333���38�?  33M]��3333�x�38�?  33E���3333���38�?  33M]��3333�x�38�?  33DDDDD3333������?  33333333333�����3?  333   333333?���3?  333
��333333����3?  333   333333����3?  	NumGlyphs  TDBNavigatorDBNavigator1Left(TopWidthpHeight
DataSourcedsInstalacionVisibleButtonsnbFirstnbPriornbNextnbLast Flat	TabOrder  TBitBtnEscribirLeft� TopWidthKHeightHint Escribri Claves En Archivo TextoCaption	&EscribirParentShowHintShowHint	TabOrderOnClickEscribirClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333�33;3�3333�;�w{�w{�7����s3�    33wwwwww330����337�333330����337�333330����337�333330����3?��333��������ww�333w;������7w�3?�ww30��  337�3wws330���3337�37�330��3337�3w�330�� ;�337��w7�3�  3�33www3w�;�3;�3;�7s37s37s�33;333;s3373337	NumGlyphs  TBitBtnEnviarLeft� TopWidthKHeightHintEnviar Clave Por e-MailCaptionEn&viarParentShowHintShowHint	TabOrderVisible
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333333333333333333333333?�������        wwwwwwww�������33��337��w����37w3337�������33���37��ww��37wws?��������?�333www�����ws3337�������������        wwwwwwww33333333333333333333333333333333333333333333333333333333333333333333333333333333	NumGlyphs   	TGroupBoxClavesGBLeft Top0WidthHeightAlignalClientCaption! Claves De Instalaci�n Generadas TabOrder TZetaDBGrid
ZetaDBGridLeftTopWidthHeight AlignalClient
DataSourcedsInstalacionOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName	SN_NUMEROTitle.Caption
Sentinel #Width7Visible	 Expanded	FieldNameIN_FOLIOTitle.CaptionInstalaci�n #WidthBVisible	 Expanded	FieldNameIN_FECHATitle.CaptionFechaWidthFVisible	 Expanded	FieldNameIN_CLAVETitle.CaptionClaveWidth� Visible	 Expanded	FieldName	US_CODIGOTitle.CaptionUsuarioVisible	 Expanded	FieldName	DI_CODIGOTitle.CaptionDistribuidorWidthQVisible	 Expanded	FieldName
IN_COMENTATitle.Caption
ComentarioWidth� Visible	     TDataSource	dsSistemaLeftDTop  TSaveDialog
SaveDialog
DefaultExttxtFilter(Textos ( *.txt )|*.txt|Todos ( *.* )|*.*FilterIndex TitleEscoja El Archivo A GenerarLeftTop	  TDataSourcedsInstalacionOnDataChangedsInstalacionDataChangeLeft� Top�    