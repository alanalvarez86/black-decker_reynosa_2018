object dmSentinel: TdmSentinel
  OldCreateOrder = True
  OnDestroy = dmSentinelDestroy
  Left = 270
  Top = 213
  Height = 479
  Width = 741
  object tCiudad: TTable
    DatabaseName = 'dbSentinel'
    TableName = 'CIUDAD'
    Left = 32
    Top = 68
  end
  object tEstado: TTable
    DatabaseName = 'dbSentinel'
    TableName = 'ESTADO'
    Left = 32
    Top = 120
  end
  object tGrupo: TTable
    DatabaseName = 'dbSentinel'
    TableName = 'GRUPO'
    Left = 88
    Top = 120
  end
  object tDistribu: TTable
    DatabaseName = 'dbSentinel'
    TableName = 'DISTRIBU'
    Left = 87
    Top = 170
  end
  object tUsuario: TTable
    DatabaseName = 'dbSentinel'
    TableName = 'USUARIO'
    Left = 32
    Top = 220
  end
  object tVersion: TTable
    DatabaseName = 'dbSentinel'
    TableName = 'VERSION'
    Left = 88
    Top = 68
  end
  object tqSistema: TQuery
    DatabaseName = 'dbSentinel'
    OnFilterRecord = tqSistemaFilterRecord
    SQL.Strings = (
      'select Max(SI_FOLIO) ULTIMO'
      'from SISTEMA ')
    Left = 164
    Top = 16
  end
  object tqAutoriza: TQuery
    AfterOpen = tqAutorizaAfterOpen
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select A.SI_FOLIO, A.AU_FOLIO, A.AU_FECHA,'
      'A.DI_CODIGO, A.SN_NUMERO, A.VE_CODIGO,'
      'A.US_CODIGO, A.AU_NUM_EMP, A.AU_USERS,'
      'A.AU_COMENTA, A.AU_KIT_DIS,'
      'A.AU_CLAVE1, A.AU_CLAVE2,'
      'A.AU_FEC_GRA, A.AU_FEC_AUT, A.AU_FEC_PRE,'
      'A.AU_PAR_AUT, A.AU_PAR_PRE,'
      'A.AU_MOD_0,'
      'A.AU_MOD_1,'
      'A.AU_MOD_2,'
      'A.AU_MOD_3,'
      'A.AU_MOD_4,'
      'A.AU_MOD_5,'
      'A.AU_MOD_6,'
      'A.AU_MOD_7,'
      'A.AU_MOD_8,'
      'A.AU_MOD_9,'
      'A.AU_MOD_10,'
      'A.AU_MOD_11,'
      'A.AU_MOD_12,'
      'A.AU_MOD_13,'
      'A.AU_MOD_14,'
      'A.AU_MOD_15,'
      'A.AU_MOD_16,'
      'A.AU_MOD_17,'
      'A.AU_MOD_18,'
      'A.AU_MOD_19,'
      'A.AU_MOD_20,'
      'A.AU_MOD_21,'
      'A.AU_MOD_22,'
      'A.AU_MOD_23,'
      'A.AU_PRE_0,'
      'A.AU_PRE_1,'
      'A.AU_PRE_2,'
      'A.AU_PRE_3,'
      'A.AU_PRE_4,'
      'A.AU_PRE_5,'
      'A.AU_PRE_6,'
      'A.AU_PRE_7,'
      'A.AU_PRE_8,'
      'A.AU_PRE_9,'
      'A.AU_PRE_10,'
      'A.AU_PRE_11,'
      'A.AU_PRE_12,'
      'A.AU_PRE_13,'
      'A.AU_PRE_14,'
      'A.AU_PRE_15,'
      'A.AU_PRE_16,'
      'A.AU_PRE_17,'
      'A.AU_PRE_18,'
      'A.AU_PRE_19,'
      'A.AU_PRE_20,'
      'A.AU_PRE_21,'
      'A.AU_PRE_22,'
      'A.AU_PRE_23,'
      'A.AU_DEFINIT, A.AU_TIPOS,'
      'V.VE_DESCRIP, S.SI_RAZ_SOC,'
      'A.AU_SQL_BD, A.AU_PLATFORM,'
      'A.AU_VAL_LIM, A.AU_BD_LIM, '
      'SE.SN_SERVER'
      'from AUTORIZA A'
      'left outer join VERSION V on ( V.VE_CODIGO = A.VE_CODIGO )'
      'left outer join SISTEMA S on ( S.SI_FOLIO = A.SI_FOLIO )'
      'left outer join SENTINEL SE on (SE.SN_NUMERO = A.SN_NUMERO)'
      'where ( A.SI_FOLIO = :SI_FOLIO ) and ( A.SN_NUMERO = :SN_NUMERO)'
      'order by A.AU_FOLIO desc'
      ' '
      ' ')
    Left = 252
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
        Value = 120
      end
      item
        DataType = ftInteger
        Name = 'SN_NUMERO'
        ParamType = ptInput
      end>
  end
  object tqActual: TQuery
    AfterOpen = tqActualAfterOpen
    BeforeEdit = tqActualBeforeEdit
    AfterEdit = tqActualAfterEdit
    OnNewRecord = tqActualNewRecord
    OnUpdateRecord = tqActualUpdateRecord
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select SI_FOLIO,'
      '       SI_RAZ_SOC,'
      '       SI_EMPRESA,'
      '       SI_OTRO,'
      '       SI_CALLE,'
      '       GR_CODIGO,'
      '       SI_COLONIA,'
      '       SI_PERSONA,'
      '       CT_CODIGO,'
      '       SI_TEL1,'
      '       SI_TEL2,'
      '       ET_CODIGO,'
      '       PA_CODIGO,'
      '       SI_FAX,'
      '       SI_EMAIL,'
      '       SI_COMENTA,'
      '       SI_GTE_RH,'
      '       SI_GTE_CON,'
      '       SI_GTE_SIS,'
      '       SI_EMA_RH,'
      '       SI_EMA_CON,'
      '       SI_EMA_SIS,'
      '       SI_GIRO,'
      '       GI_CODIGO,'
      '       DI_CODIGO,'
      '       SI_MIGRADO,'
      '       SI_PROBLEMA,'
      '       SI_FEC_SOL,'
      '       SI_EMP_EMP,'
      '       SI_SIS_VIE,'
      '       AU_FOLIO,'
      '       AU_FECHA,'
      '       SN_NUMERO,'
      '       VE_CODIGO,'
      '       US_CODIGO,'
      '       AU_NUM_EMP,'
      '       AU_MOD_0,'
      '       AU_MOD_1,'
      '       AU_MOD_2,'
      '       AU_MOD_3,'
      '       AU_MOD_4,'
      '       AU_MOD_5,'
      '       AU_MOD_6,'
      '       AU_MOD_7,'
      '       AU_MOD_8,'
      '       AU_MOD_9,'
      '       AU_MOD_10,'
      '       AU_MOD_11,'
      '       AU_MOD_12,'
      '       AU_MOD_13,'
      '       AU_MOD_14,'
      '       AU_MOD_15,'
      '       AU_MOD_16,'
      '       AU_MOD_17,'
      '       AU_MOD_18,'
      '       AU_MOD_19,'
      '       AU_MOD_20,'
      '       AU_MOD_21,'
      '       AU_MOD_22,'
      '       AU_MOD_23,'
      '       AU_USERS,'
      '       AU_KIT_DIS,'
      '       AU_CLAVE1,'
      '       AU_CLAVE2,'
      '       AU_FEC_GRA,'
      '       AU_COMENTA,'
      '       AU_PRE_0,'
      '       AU_PRE_1,'
      '       AU_PRE_2,'
      '       AU_PRE_3,'
      '       AU_PRE_4,'
      '       AU_PRE_5,'
      '       AU_PRE_6,'
      '       AU_PRE_7,'
      '       AU_PRE_8,'
      '       AU_PRE_9,'
      '       AU_PRE_10,'
      '       AU_PRE_11,'
      '       AU_PRE_12,'
      '       AU_PRE_13,'
      '       AU_PRE_14,'
      '       AU_PRE_15,'
      '       AU_PRE_16,'
      '       AU_PRE_17,'
      '       AU_PRE_18,'
      '       AU_PRE_19,'
      '       AU_PRE_20,'
      '       AU_PRE_21,'
      '       AU_PRE_22,'
      '       AU_PRE_23,'
      '       AU_FEC_AUT,'
      '       AU_FEC_PRE,'
      '       AU_PAR_AUT,'
      '       AU_PAR_PRE,'
      '       AU_DEFINIT,'
      '       AU_TIPOS,'
      '       AU_SQL_BD, '
      '       AU_PLATFORM,'
      '       AU_VAL_LIM, '
      '       AU_BD_LIM, '
      '       VERSION.VE_DESCRIP as VERSION_DES,'
      '       SENTINEL.SN_SERVER'
      'from ACTUAL'
      
        'left outer join VERSION on ( VERSION.VE_CODIGO = ACTUAL.VE_CODIG' +
        'O ) '
      
        'left outer join SENTINEL on ( SENTINEL.SN_NUMERO = ACTUAL.SN_NUM' +
        'ERO ) '
      'where ( SI_FOLIO = :SI_FOLIO )')
    Left = 336
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
        Value = 0
      end>
  end
  object tqAgregaSistema: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'insert into SISTEMA('
      '       SI_FOLIO,'
      '       SI_RAZ_SOC,'
      '       SI_EMPRESA,'
      '       SI_OTRO,'
      '       SI_CALLE,'
      '       GR_CODIGO,'
      '       SI_COLONIA,'
      '       SI_PERSONA,'
      '       CT_CODIGO,'
      '       SI_TEL1,'
      '       SI_TEL2,'
      '       SI_FAX,'
      '       SI_EMAIL,'
      '       SI_COMENTA,'
      '       SI_GTE_RH,'
      '       SI_GTE_CON,'
      '       SI_GTE_SIS,'
      '       SI_EMA_RH,'
      '       SI_EMA_CON,'
      '       SI_EMA_SIS,'
      '       SI_GIRO,'
      '       GI_CODIGO,'
      '       PA_CODIGO,'
      '       SI_MIGRADO,'
      '       SI_PROBLEMA,'
      '       SI_FEC_SOL,'
      '       SI_EMP_EMP,'
      '       SI_SIS_VIE ) values ('
      '       :SI_FOLIO,'
      '       :SI_RAZ_SOC,'
      '       :SI_EMPRESA,'
      '       :SI_OTRO,'
      '       :SI_CALLE,'
      '       :GR_CODIGO,'
      '       :SI_COLONIA,'
      '       :SI_PERSONA,'
      '       :CT_CODIGO,'
      '       :SI_TEL1,'
      '       :SI_TEL2,'
      '       :SI_FAX,'
      '       :SI_EMAIL,'
      '       :SI_COMENTA,'
      '       :SI_GTE_RH,'
      '       :SI_GTE_CON,'
      '       :SI_GTE_SIS,'
      '       :SI_EMA_RH,'
      '       :SI_EMA_CON,'
      '       :SI_EMA_SIS,'
      '       :SI_GIRO,'
      '       :GI_CODIGO,'
      '       :PA_CODIGO,'
      '       :SI_MIGRADO,'
      '       :SI_PROBLEMA,'
      '       :SI_FEC_SOL,'
      '       :SI_EMP_EMP,'
      '       :SI_SIS_VIE )'
      '')
    Left = 164
    Top = 223
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_RAZ_SOC'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_EMPRESA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_OTRO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_CALLE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'GR_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_COLONIA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_PERSONA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'CT_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_TEL1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_TEL2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_FAX'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_EMAIL'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_COMENTA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_GTE_RH'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_GTE_CON'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_GTE_SIS'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_EMA_RH'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_EMA_CON'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_EMA_SIS'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_GIRO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'GI_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PA_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_MIGRADO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_PROBLEMA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_FEC_SOL'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_EMP_EMP'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_SIS_VIE'
        ParamType = ptUnknown
      end>
  end
  object tqBorraSistema: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'delete from SISTEMA where ( SI_FOLIO = :SI_FOLIO )')
    Left = 252
    Top = 223
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end>
  end
  object tqAgregaAutoriza: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'insert into AUTORIZA('
      '       SI_FOLIO,'
      '       AU_FOLIO,'
      '       AU_FECHA,'
      '       DI_CODIGO,'
      '       SN_NUMERO,'
      '       VE_CODIGO,'
      '       US_CODIGO,'
      '       AU_NUM_EMP,'
      '       AU_MOD_0,'
      '       AU_MOD_1,'
      '       AU_MOD_2,'
      '       AU_MOD_3,'
      '       AU_MOD_4,'
      '       AU_MOD_5,'
      '       AU_MOD_6,'
      '       AU_MOD_7,'
      '       AU_MOD_8,'
      '       AU_MOD_9,'
      '       AU_MOD_10,'
      '       AU_MOD_11,'
      '       AU_MOD_12,'
      '       AU_MOD_13,'
      '       AU_MOD_14,'
      '       AU_MOD_15,'
      '       AU_MOD_16,'
      '       AU_MOD_17,'
      '       AU_MOD_18,'
      '       AU_MOD_19,'
      '       AU_MOD_20,'
      '       AU_MOD_21,'
      '       AU_MOD_22,'
      '       AU_MOD_23,'
      '       AU_USERS,'
      '       AU_KIT_DIS,'
      '       AU_CLAVE1,'
      '       AU_CLAVE2,'
      '       AU_FEC_GRA,'
      '       AU_FEC_AUT,'
      '       AU_COMENTA,'
      '       AU_FEC_PRE,'
      '       AU_PAR_AUT,'
      '       AU_PAR_PRE,'
      '       AU_PRE_0,'
      '       AU_PRE_1,'
      '       AU_PRE_2,'
      '       AU_PRE_3,'
      '       AU_PRE_4,'
      '       AU_PRE_5,'
      '       AU_PRE_6,'
      '       AU_PRE_7,'
      '       AU_PRE_8,'
      '       AU_PRE_9,'
      '       AU_PRE_10,'
      '       AU_PRE_11,'
      '       AU_PRE_12,'
      '       AU_PRE_13,'
      '       AU_PRE_14,'
      '       AU_PRE_15,'
      '       AU_PRE_16,'
      '       AU_PRE_17,'
      '       AU_PRE_18,'
      '       AU_PRE_19,'
      '       AU_PRE_20,'
      '       AU_PRE_21,'
      '       AU_PRE_22,'
      '       AU_PRE_23,'
      '       AU_DEFINIT,'
      '       AU_TIPOS,'
      '       AU_SQL_BD,'
      '       AU_PLATFORM, '
      '       AU_VAL_LIM, '
      '       AU_BD_LIM ) values ('
      '       :SI_FOLIO,'
      '       :AU_FOLIO,'
      '       :AU_FECHA,'
      '       :DI_CODIGO,'
      '       :SN_NUMERO,'
      '       :VE_CODIGO,'
      '       :US_CODIGO,'
      '       :AU_NUM_EMP,'
      '       1,'
      '       :AU_MOD_1,'
      '       :AU_MOD_2,'
      '       :AU_MOD_3,'
      '       :AU_MOD_4,'
      '       :AU_MOD_5,'
      '       :AU_MOD_6,'
      '       :AU_MOD_7,'
      '       :AU_MOD_8,'
      '       :AU_MOD_9,'
      '       :AU_MOD_10,'
      '       :AU_MOD_11,'
      '       :AU_MOD_12,'
      '       :AU_MOD_13,'
      '       :AU_MOD_14,'
      '       :AU_MOD_15,'
      '       :AU_MOD_16,'
      '       :AU_MOD_17,'
      '       :AU_MOD_18,'
      '       :AU_MOD_19,'
      '       :AU_MOD_20,'
      '       :AU_MOD_21,'
      '       :AU_MOD_22,'
      '       :AU_MOD_23,'
      '       :AU_USERS,'
      '       :AU_KIT_DIS,'
      '       :AU_CLAVE1,'
      '       :AU_CLAVE2,'
      '       :AU_FEC_GRA,'
      '       :AU_FEC_AUT,'
      '       :AU_COMENTA,'
      '       :AU_FEC_PRE,'
      '       :AU_PAR_AUT,'
      '       :AU_PAR_PRE,'
      '       :AU_PRE_0,'
      '       :AU_PRE_1,'
      '       :AU_PRE_2,'
      '       :AU_PRE_3,'
      '       :AU_PRE_4,'
      '       :AU_PRE_5,'
      '       :AU_PRE_6,'
      '       :AU_PRE_7,'
      '       :AU_PRE_8,'
      '       :AU_PRE_9,'
      '       :AU_PRE_10,'
      '       :AU_PRE_11,'
      '       :AU_PRE_12,'
      '       :AU_PRE_13,'
      '       :AU_PRE_14,'
      '       :AU_PRE_15,'
      '       :AU_PRE_16,'
      '       :AU_PRE_17,'
      '       :AU_PRE_18,'
      '       :AU_PRE_19,'
      '       :AU_PRE_20,'
      '       :AU_PRE_21,'
      '       :AU_PRE_22,'
      '       :AU_PRE_23,'
      '       :AU_DEFINIT,'
      '       :AU_TIPOS,'
      '       :AU_SQL_BD,'
      '       :AU_PLATFORM, '
      '       :AU_VAL_LIM, '
      '       :AU_BD_LIM)'
      ' '
      ' '
      ' ')
    Left = 252
    Top = 122
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_FECHA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DI_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SN_NUMERO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'VE_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'US_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_NUM_EMP'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_8'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_9'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_10'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_11'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_12'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_13'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_14'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_15'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_16'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_17'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_18'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_19'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_20'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_21'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_22'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_MOD_23'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_USERS'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_KIT_DIS'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_CLAVE1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_CLAVE2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_FEC_GRA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_FEC_AUT'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_COMENTA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_FEC_PRE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PAR_AUT'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PAR_PRE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_8'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_9'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_10'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_11'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_12'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_13'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_14'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_15'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_16'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_17'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_18'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_19'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_20'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_21'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_22'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PRE_23'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_DEFINIT'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_TIPOS'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_SQL_BD'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_PLATFORM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_VAL_LIM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_BD_LIM'
        ParamType = ptUnknown
      end>
  end
  object tqModificaSistema: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'update SISTEMA set'
      '       SI_RAZ_SOC = :SI_RAZ_SOC,'
      '       SI_EMPRESA = :SI_EMPRESA,'
      '       SI_OTRO = :SI_OTRO,'
      '       SI_CALLE = :SI_CALLE,'
      '       GR_CODIGO = :GR_CODIGO,'
      '       SI_COLONIA = :SI_COLONIA,'
      '       SI_PERSONA = :SI_PERSONA,'
      '       CT_CODIGO = :CT_CODIGO,'
      '       SI_TEL1 = :SI_TEL1,'
      '       SI_TEL2 = :SI_TEL2,'
      '       SI_FAX = :SI_FAX,'
      '       SI_EMAIL = :SI_EMAIL,'
      '       SI_COMENTA = :SI_COMENTA,'
      '       SI_GTE_RH = :SI_GTE_RH,'
      '       SI_GTE_CON = :SI_GTE_CON,'
      '       SI_GTE_SIS = :SI_GTE_SIS,'
      '       SI_EMA_RH = :SI_EMA_RH,'
      '       SI_EMA_CON = :SI_EMA_CON,'
      '       SI_EMA_SIS = :SI_EMA_SIS,'
      '       SI_GIRO = :SI_GIRO,'
      '       GI_CODIGO = :GI_CODIGO,'
      '       PA_CODIGO = :PA_CODIGO,'
      '       SI_MIGRADO = :SI_MIGRADO,'
      '       SI_PROBLEMA = :SI_PROBLEMA,'
      '       SI_FEC_SOL = :SI_FEC_SOL,'
      '       SI_EMP_EMP = :SI_EMP_EMP,'
      '       SI_SIS_VIE = :SI_SIS_VIE'
      'where ( SI_FOLIO = :SI_FOLIO )')
    Left = 336
    Top = 223
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SI_RAZ_SOC'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_EMPRESA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_OTRO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_CALLE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'GR_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_COLONIA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_PERSONA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'CT_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_TEL1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_TEL2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_FAX'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_EMAIL'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_COMENTA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_GTE_RH'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_GTE_CON'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_GTE_SIS'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_EMA_RH'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_EMA_CON'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_EMA_SIS'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_GIRO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'GI_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PA_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_MIGRADO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_PROBLEMA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_FEC_SOL'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_EMP_EMP'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_SIS_VIE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end>
  end
  object qSentinel: TQuery
    AfterOpen = qSentinelAfterOpen
    DatabaseName = 'dbSentinel'
    RequestLive = True
    SQL.Strings = (
      
        'select SN_NUMERO,  SN_MODELO,  SN_FEC_INI, SN_TIPO,SI_FOLIO,SN_N' +
        'OMBRE,SN_SERVER,SN_ACTIVO '
      'from SENTINEL order by SN_NUMERO desc'
      ' ')
    Left = 164
    Top = 70
  end
  object tSentinel: TTable
    AfterOpen = tSentinelAfterOpen
    BeforePost = tSentinelBeforePost
    DatabaseName = 'dbSentinel'
    TableName = 'SENTINEL'
    Left = 32
    Top = 266
  end
  object dbSentinel: TDatabase
    AliasName = 'Sentinel'
    DatabaseName = 'dbSentinel'
    LoginPrompt = False
    Params.Strings = (
      'USER NAME=SYSDBA'
      'PASSWORD=m')
    SessionName = 'Default'
    Left = 32
    Top = 16
  end
  object qVersion: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select VE_CODIGO, VE_DESCRIP '
      'from VERSION order by VE_CODIGO')
    Left = 252
    Top = 284
  end
  object qAsignado: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select SN_NUMERO, SI_FOLIO from ASIGNADO')
    Left = 252
    Top = 68
  end
  object qUsuario: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select US_CODIGO, US_PASSWRD'
      'from USUARIO'
      'where ( UPPER( US_CODIGO ) = :US_CODIGO )'
      'order by US_CODIGO')
    Left = 336
    Top = 122
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'US_CODIGO'
        ParamType = ptUnknown
      end>
  end
  object qLibre: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select SN_NUMERO from LIBRE '
      'order by SN_NUMERO'
      '')
    Left = 336
    Top = 68
  end
  object qHistoSen: TQuery
    AfterOpen = tqAutorizaAfterOpen
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select A.SI_FOLIO, A.AU_FOLIO, A.AU_FECHA,'
      'A.DI_CODIGO, A.SN_NUMERO, A.VE_CODIGO,'
      'A.US_CODIGO, A.AU_NUM_EMP, A.AU_USERS,'
      'A.AU_COMENTA, A.AU_KIT_DIS,'
      'A.AU_CLAVE1, A.AU_CLAVE2,'
      'A.AU_FEC_GRA, A.AU_FEC_AUT, A.AU_FEC_PRE,'
      'A.AU_PAR_AUT, A.AU_PAR_PRE,'
      'A.AU_MOD_0,'
      'A.AU_MOD_1,'
      'A.AU_MOD_2,'
      'A.AU_MOD_3,'
      'A.AU_MOD_4,'
      'A.AU_MOD_5,'
      'A.AU_MOD_6,'
      'A.AU_MOD_7,'
      'A.AU_MOD_8,'
      'A.AU_MOD_9,'
      'A.AU_MOD_10,'
      'A.AU_MOD_11,'
      'A.AU_MOD_12,'
      'A.AU_MOD_13,'
      'A.AU_MOD_14,'
      'A.AU_MOD_15,'
      'A.AU_MOD_16,'
      'A.AU_MOD_17,'
      'A.AU_MOD_18,'
      'A.AU_MOD_19,'
      'A.AU_MOD_20,'
      'A.AU_MOD_21,'
      'A.AU_MOD_22,'
      'A.AU_MOD_23,'
      'A.AU_PRE_0,'
      'A.AU_PRE_1,'
      'A.AU_PRE_2,'
      'A.AU_PRE_3,'
      'A.AU_PRE_4,'
      'A.AU_PRE_5,'
      'A.AU_PRE_6,'
      'A.AU_PRE_7,'
      'A.AU_PRE_8,'
      'A.AU_PRE_9,'
      'A.AU_PRE_10,'
      'A.AU_PRE_11,'
      'A.AU_PRE_12,'
      'A.AU_PRE_13,'
      'A.AU_PRE_14,'
      'A.AU_PRE_15,'
      'A.AU_PRE_16,'
      'A.AU_PRE_17,'
      'A.AU_PRE_18,'
      'A.AU_PRE_19,'
      'A.AU_PRE_20,'
      'A.AU_PRE_21,'
      'A.AU_PRE_22,'
      'A.AU_PRE_23,'
      'A.AU_DEFINIT, A.AU_TIPOS,'
      'V.VE_DESCRIP, S.SI_RAZ_SOC,'
      'A.AU_SQL_BD, A.AU_PLATFORM'
      'from AUTORIZA A'
      'left outer join VERSION V on ( V.VE_CODIGO = A.VE_CODIGO )'
      'left outer join SISTEMA S on ( S.SI_FOLIO = A.SI_FOLIO )'
      'where ( A.SN_NUMERO = :SN_NUMERO )'
      'order by A.AU_FOLIO desc'
      ' ')
    Left = 164
    Top = 122
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SN_NUMERO'
        ParamType = ptUnknown
      end>
  end
  object tqValidaSentinel: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select COUNT(*) from ACTUAL where '
      '( SN_NUMERO = :SN_NUMERO ) and'
      '( SI_FOLIO <> :SI_FOLIO )')
    Left = 252
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SN_NUMERO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end>
  end
  object tqRazonSocial: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select SI_RAZ_SOC from SISTEMA'
      'where ( SI_FOLIO = :SI_FOLIO )')
    Left = 164
    Top = 173
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end>
  end
  object tGiro: TTable
    DatabaseName = 'dbSentinel'
    TableName = 'GIRO'
    Left = 88
    Top = 214
  end
  object tPais: TTable
    DatabaseName = 'dbSentinel'
    TableName = 'PAIS'
    Left = 32
    Top = 174
  end
  object tqExisteSentinel: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select COUNT(*) from SENTINEL where ( SN_NUMERO = :SN_NUMERO )')
    Left = 336
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SN_NUMERO'
        ParamType = ptUnknown
      end>
  end
  object tqExistenClaves: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select COUNT(*) from AUTORIZA where'
      '( SI_FOLIO = :SI_FOLIO ) and'
      '( AU_CLAVE1 = :AU_CLAVE1 ) and'
      '( AU_CLAVE2 = :AU_CLAVE2 )'
      ' ')
    Left = 166
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_CLAVE1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AU_CLAVE2'
        ParamType = ptUnknown
      end>
  end
  object tqInstalaciones: TQuery
    CachedUpdates = True
    AfterOpen = tqInstalacionesAfterOpen
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select SI_FOLIO, SN_NUMERO, DI_CODIGO, US_CODIGO,'
      'IN_FOLIO, IN_FECHA, IN_CLAVE, IN_COMENTA'
      'from INSTALACION where ( SI_FOLIO = :SI_FOLIO )'
      'order by SN_NUMERO, IN_FOLIO desc'
      ''
      ' ')
    Left = 168
    Top = 336
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
        Value = 0
      end>
  end
  object tqMaxInstalacion: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select MAX( IN_FOLIO ) from INSTALACION where'
      '( SI_FOLIO = :SI_FOLIO ) and'
      '( SN_NUMERO = :SN_NUMERO )'
      ' ')
    Left = 252
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SN_NUMERO'
        ParamType = ptUnknown
      end>
  end
  object tqAddInstalacion: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'insert into INSTALACION( SN_NUMERO,'
      '                         SI_FOLIO,'
      '                         IN_FOLIO,'
      '                         DI_CODIGO,'
      '                         US_CODIGO,'
      '                         IN_FECHA,'
      '                         IN_CLAVE,'
      '                         IN_COMENTA ) values ('
      '                         :SN_NUMERO,'
      '                         :SI_FOLIO,'
      '                         :IN_FOLIO,'
      '                         :DI_CODIGO,'
      '                         :US_CODIGO,'
      '                         :IN_FECHA,'
      '                         :IN_CLAVE,'
      '                         :IN_COMENTA )'
      ''
      ' '
      ' ')
    Left = 336
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SN_NUMERO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'IN_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DI_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'US_CODIGO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'IN_FECHA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'IN_CLAVE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'IN_COMENTA'
        ParamType = ptUnknown
      end>
  end
  object tqUsoSentinel: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select COUNT(*) from ASIGNACION where'
      '( SI_FOLIO = :SI_FOLIO ) and'
      '( SN_NUMERO = :SN_NUMERO )'
      ' ')
    Left = 336
    Top = 285
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SN_NUMERO'
        ParamType = ptUnknown
      end>
  end
  object tTerminales: TTable
    AfterOpen = tTerminalesAfteropen
    BeforePost = tTerminalesBeforePost
    OnNewRecord = tTerminalesNewRecord
    DatabaseName = 'dbSentinel'
    TableName = 'TERMINAL'
    Left = 88
    Top = 262
  end
  object tqTerminales: TQuery
    CachedUpdates = True
    AfterOpen = tqActualAfterOpen
    BeforeEdit = tqActualBeforeEdit
    AfterEdit = tqActualAfterEdit
    AfterCancel = qSentinelAfterOpen
    OnNewRecord = tqActualNewRecord
    OnUpdateRecord = tqActualUpdateRecord
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select '
      '      TE_CODIGO,'
      '      TE_NSERIE,'
      '      TE_DESCRIP,'
      '      TE_NUMERO,'
      '      TE_ACTIVO,'
      '      TE_TEXTO ,'
      '      SN_NUMERO,'
      '      TE_TIPO'
      'from '
      '  TERMINAL'
      'where '
      'SN_NUMERO = :Sentinel'
      '')
    Left = 408
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Sentinel'
        ParamType = ptInput
      end>
  end
  object tqActualizaTerminal: TQuery
    CachedUpdates = True
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'update TERMINAL set SN_NUMERO =  :Sentinel where '
      'SN_NUMERO = :SentAnterior  and SI_FOLIO = :SI_FOLIO')
    Left = 408
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Sentinel'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SentAnterior'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SI_FOLIO'
        ParamType = ptInput
      end>
  end
  object tqSentVirtual: TQuery
    CachedUpdates = True
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'update SENTINEL set SN_SERVER = :Server '
      'where SN_NUMERO = :Sentinel ')
    Left = 416
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'Server'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'Sentinel'
        ParamType = ptInput
      end>
  end
  object tqServerVirt: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select  SN_SERVER  from SENTINEL '
      'where SN_NUMERO = :Sentinel ')
    Left = 408
    Top = 176
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Sentinel'
        ParamType = ptInput
      end>
  end
  object tqAgregaHistorico3DT: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'insert into HIST3DT'
      '('
      '  SN_NUMERO'
      ', SI_FOLIO'
      ', HI_FEC_ING'
      ', HI_COMENTA'
      ', HI_3DTXML'
      ',HI_NOMBRE'
      ',HI_FEC_DOC'
      ')'
      'values'
      '('
      '  :SN_NUMERO'
      ', :SI_FOLIO'
      ', :HI_FEC_ING'
      ', :HI_COMENTA'
      ', :HI_3DTXML'
      ',:HI_NOMBRE'
      ',:HI_FEC_DOC'
      ')')
    Left = 468
    Top = 231
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SN_NUMERO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'HI_FEC_ING'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'HI_COMENTA'
        ParamType = ptUnknown
      end
      item
        DataType = ftBlob
        Name = 'HI_3DTXML'
        ParamType = ptUnknown
        Value = '0'
      end
      item
        DataType = ftString
        Name = 'HI_NOMBRE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'HI_FEC_DOC'
        ParamType = ptUnknown
      end>
  end
  object tHistorico3DT: TTable
    BeforePost = tTerminalesBeforePost
    OnNewRecord = tTerminalesNewRecord
    DatabaseName = 'dbSentinel'
    TableName = 'HIST3DT'
    Left = 24
    Top = 318
  end
  object tqBuscaTerminal: TQuery
    AfterOpen = tTerminalesAfteropen
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select'
      '      TE_NSERIE,'
      '      S.SI_RAZ_SOC,'
      '      TE_DESCRIP,'
      '      TE_NUMERO,'
      '      TE_ACTIVO,'
      '      TE_TEXTO ,'
      '      SN_NUMERO,'
      '      TE_TIPO'
      'from'
      '  TERMINAL T'
      '  left outer join SISTEMA S ON S.SI_FOLIO = T.SI_FOLIO'
      'where'
      '  TE_NSERIE like :Pista1 OR'
      '  TE_TEXTO like :Pista2 OR'
      '  S.SI_RAZ_SOC like :Pista3'
      '')
    Left = 416
    Top = 288
    ParamData = <
      item
        DataType = ftString
        Name = 'Pista1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'Pista2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'Pista3'
        ParamType = ptInput
      end>
  end
  object qSentinelPorSist: TQuery
    AfterOpen = qSentinelAfterOpen
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      
        'select Distinct(A.SN_NUMERO),S.SN_TIPO,S.SN_NOMBRE,S.SN_ACTIVO,S' +
        '.SN_FEC_INI,S.SN_MODELO from AUTORIZA A'
      'left outer join SENTINEL S on S.SN_NUMERO = A.SN_NUMERO'
      'WHERE A.si_folio = :Sistema'
      'order by A.SN_NUMERO DESC')
    Left = 484
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Sistema'
        ParamType = ptInput
      end>
  end
  object tqSistemaLook: TQuery
    DatabaseName = 'dbSentinel'
    OnFilterRecord = tqSistemaFilterRecord
    SQL.Strings = (
      'select SI_FOLIO ,SI_RAZ_SOC,SI_EMPRESA, GR_CODIGO,ET_CODIGO,'
      
        'SI_PROBLEMA,SI_FEC_SOL,S.DI_CODIGO,S.CT_CODIGO,D.DI_NOMBRE,C.CT_' +
        'DESCRIP,SI_SUCURSAL'
      'from SISTEMA S'
      'left outer join CIUDAD C on ( C.CT_CODIGO = S.CT_CODIGO )'
      'left outer join DISTRIBU D on ( D.DI_CODIGO = S.DI_CODIGO )'
      'ORDER BY SI_FOLIO DESC')
    Left = 156
    Top = 384
  end
  object tSistema: TTable
    BeforePost = tSistemaBeforePost
    OnNewRecord = tSistemaNewRecord
    DatabaseName = 'dbSentinel'
    TableName = 'SISTEMA'
    Left = 88
    Top = 318
  end
  object tAutoriza: TTable
    AfterInsert = tAutorizaAfterInsert
    AfterEdit = tAutorizaAfterInsert
    BeforePost = tAutorizaBeforePost
    DatabaseName = 'dbSentinel'
    TableName = 'AUTORIZA'
    Left = 88
    Top = 374
  end
  object qGetNextSentVirtual: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select Max(SN_NUMERO) Ultimo from SENTINEL'
      'where SN_TIPO = 99')
    Left = 512
    Top = 120
  end
  object tqUltimaAutoriza: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select Max(AU_FOLIO) Ultimo from AUTORIZA'
      'where SI_FOLIO = :Sistema')
    Left = 560
    Top = 200
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Sistema'
        ParamType = ptInput
      end>
  end
  object tqHistorico3DTOrdenado: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'select SN_NUMERO,'
      'SI_FOLIO,'
      'HI_FEC_DOC,'
      'HI_FEC_ING,'
      'HI_COMENTA,'
      'HI_3DTXML,'
      'HI_NOMBRE'
      'from HIST3DT '
      'where SI_FOLIO = :SI_FOLIO and SN_NUMERO = :SN_NUMERO'
      'order by HI_FEC_DOC DESC')
    Left = 440
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SN_NUMERO'
        ParamType = ptUnknown
      end>
  end
  object tqModificaHist3DT: TQuery
    DatabaseName = 'dbSentinel'
    SQL.Strings = (
      'UPDATE HIST3DT SET '
      '    SN_NUMERO = :SN_NUMERO,'
      '    SI_FOLIO = :SI_FOLIO'
      'WHERE'
      '    HI_COMENTA = :HI_COMENTA AND '
      '    SN_NUMERO = 0 AND '
      '    SI_FOLIO = 0')
    Left = 584
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SN_NUMERO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'HI_COMENTA'
        ParamType = ptUnknown
      end>
  end
end
