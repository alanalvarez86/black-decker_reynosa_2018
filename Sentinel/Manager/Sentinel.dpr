program Sentinel;

uses
  SysUtils,
  Forms,
  FPrincipal in 'FPrincipal.pas' {FormPrincipal},
  FAcceso in 'FAcceso.pas' {Acceso},
  FCatalogo in 'FCatalogo.pas' {FormCatalogo},
  ZetaLicenseClasses in '..\..\Servidor\ZetaLicenseClasses.pas',
  DatosSistema3dt in 'DatosSistema3dt.pas';

{$R *.RES}
{$R WINDOWSXP.RES}

procedure Environment; // Para Ajustar El Ambiente De Ejecucion //
const
     aMeses: array[ 1..12 ] of PChar = ( 'Enero',
                                         'Febrero',
                                         'Marzo',
                                         'Abril',
                                         'Mayo',
                                         'Junio',
                                         'Julio',
                                         'Agosto',
                                         'Septiembre',
                                         'Octubre',
                                         'Noviembre',
                                         'Diciembre' );
    aDiasSemana: array[ 1..7 ] of PChar= ( 'Domingo',
                                           'Lunes',
                                           'Martes',
                                           'Mi�rcoles',
                                           'Jueves',
                                           'Viernes',
                                           'S�bado' );
var
   i: Integer;
begin
     ShortDateFormat := 'dd/mm/yyyy';
     LongDateFormat  := 'dddd, dd MMMM, yyyy';
     for i := 1 to 12 do
     begin
          LongMonthNames[ i ] := aMeses[ i ];
          ShortMonthNames[ i ] := Copy( LongMonthNames[ i ], 1, 3 );
     end;
     for i := 1 to 7 do
     begin
          LongDayNames[ i ] := aDiasSemana[ i ];
          ShortDayNames[ i ] := Copy( LongDayNames[ i ], 1, 3 );
     end;
end;

begin
  Environment;
  Application.Initialize;
  Application.Title := 'Sentinel';
  Application.CreateForm(TFormPrincipal, FormPrincipal);
  if FAcceso.Login then
  begin
       with FormPrincipal do
       begin
            Show;
            Update;
            Conectar;
       end;
       Application.Run;
  end;
end.
