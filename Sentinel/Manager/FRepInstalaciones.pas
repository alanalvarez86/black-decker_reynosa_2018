unit FRepInstalaciones;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, Quickrpt, Qrctrls,
     DRep;

type
  TRepInstalaciones = class(TForm)
    qrp1: TQuickRep;
    PageHeaderBand1: TQRBand;
    QRLabel5: TQRLabel;
    QRShape1: TQRShape;
    QRTitulo: TQRLabel;
    QRImage1: TQRImage;
    QRLabel18: TQRLabel;
    QRSysData1: TQRSysData;
    QRShape4: TQRShape;
    PageFooterBand1: TQRBand;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRShape7: TQRShape;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    DetailBand1: TQRBand;
    SN_NUMERO: TQRDBText;
    SI_RAZ_SOC: TQRDBText;
    SI_RAZ_SOClbl: TQRLabel;
    SN_NUMEROlbl: TQRLabel;
    SI_FOLIOlbl: TQRLabel;
    IN_CLAVElbl: TQRLabel;
    AU_NUM_EMPlbl: TQRLabel;
    SI_FOLIO: TQRDBText;
    IN_FOLIO: TQRDBText;
    IN_FECHA: TQRDBText;
    VE_DESCRIPlbl: TQRLabel;
    IN_CLAVE: TQRDBText;
    procedure QRSysData1Print(sender: TObject; var Value: String);
    procedure SI_RAZ_SOCPrint(sender: TObject; var Value: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RepInstalaciones: TRepInstalaciones;

implementation

{$R *.DFM}

procedure TRepInstalaciones.QRSysData1Print(sender: TObject; var Value: String);
begin
     Value := FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now );
end;

procedure TRepInstalaciones.SI_RAZ_SOCPrint(sender: TObject; var Value: String);
begin
     Value := Copy( Value, 1, 100 );
end;

end.
