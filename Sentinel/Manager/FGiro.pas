unit FGiro;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, Grids, DBGrids,
     DBCtrls, ExtCtrls, StdCtrls,
     FCatalogo, Buttons;

type
  TFormGiro = class(TFormCatalogo)
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormGiro: TFormGiro;

implementation

uses DSentinel;

{$R *.DFM}

procedure TFormGiro.FormShow(Sender: TObject);
begin
     inherited;
     dmSentinel.AbrirGiro( dsCatalogo );
end;

procedure TFormGiro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     dmSentinel.ResetDatasource( dsCatalogo );
end;

end.
