
CREATE DATABASE "c:\3win_sen\Sentinel.gdb" USER "SYSDBA" PASSWORD "m" PAGE_SIZE = 4096;

CREATE DOMAIN Memo BLOB;

CREATE DOMAIN Fecha DATE DEFAULT "12/30/1899" NOT NULL;

CREATE DOMAIN Empleados INTEGER DEFAULT 0 NOT NULL;

CREATE DOMAIN FolioChico SMALLINT DEFAULT 0 NOT NULL;

CREATE DOMAIN FolioSistema INTEGER DEFAULT 0 NOT NULL;

CREATE DOMAIN NumSentinel INTEGER DEFAULT 0 NOT NULL;

CREATE DOMAIN Status SMALLINT DEFAULT 0 NOT NULL;

CREATE DOMAIN Usuario SMALLINT DEFAULT 0 NOT NULL;

CREATE DOMAIN Booleano CHAR(1) DEFAULT "N" NOT NULL;

CREATE DOMAIN Codigo CHAR(8) DEFAULT "" NOT NULL;

CREATE DOMAIN Codigo1 CHAR(1) DEFAULT "" NOT NULL;

CREATE DOMAIN Descripcion VARCHAR(30) DEFAULT "" NOT NULL;

CREATE DOMAIN Hora CHAR(5) DEFAULT "" NOT NULL;

CREATE DOMAIN Observaciones VARCHAR(50) DEFAULT "" NOT NULL;

CREATE DOMAIN Passwd VARCHAR(30) DEFAULT "" NOT NULL;

CREATE DOMAIN UsuarioCorto VARCHAR(15) DEFAULT "" NOT NULL;

CREATE TABLE ESTADO (
       ET_CODIGO            Codigo,
       ET_DESCRIP           Descripcion
);


ALTER TABLE ESTADO
       ADD PRIMARY KEY (ET_CODIGO);


CREATE TABLE CIUDAD (
       CT_CODIGO            Codigo,
       CT_DESCRIP           Descripcion,
       ET_CODIGO            Codigo
);


ALTER TABLE CIUDAD
       ADD PRIMARY KEY (CT_CODIGO);


CREATE TABLE GRUPO (
       GR_CODIGO            Codigo,
       GR_DESCRIP           Descripcion
);


ALTER TABLE GRUPO
       ADD PRIMARY KEY (GR_CODIGO);


CREATE TABLE SISTEMA (
       SI_FOLIO             FolioSistema,
       SI_RAZ_SOC           Observaciones,
       SI_EMPRESA           Observaciones,
       SI_OTRO              Observaciones,
       SI_CALLE             Descripcion,
       GR_CODIGO            Codigo,
       SI_COLONIA           Descripcion,
       SI_PERSONA           Observaciones,
       CT_CODIGO            Codigo,
       SI_TEL1              Descripcion,
       SI_TEL2              Descripcion,
       SI_FAX               Descripcion,
       SI_EMAIL             Observaciones,
       SI_COMENTA           Memo,
       SI_GTE_RH            Descripcion,
       SI_GTE_CON           Descripcion,
       SI_GTE_SIS           Descripcion,
       SI_EMA_RH            Observaciones,
       SI_EMA_CON           Observaciones,
       SI_EMA_SIS           Observaciones,
       SI_GIRO              Observaciones
);


ALTER TABLE SISTEMA
       ADD PRIMARY KEY (SI_FOLIO);


CREATE TABLE DISTRIBU (
       DI_CODIGO            Codigo,
       DI_NOMBRE            Descripcion,
       DI_PERSONA           Descripcion,
       DI_EMAIL             Descripcion,
       DI_TEL1              Descripcion,
       DI_TEL2              Descripcion,
       DI_FAX               Descripcion
);


ALTER TABLE DISTRIBU
       ADD PRIMARY KEY (DI_CODIGO);


CREATE TABLE USUARIO (
       US_CODIGO            UsuarioCorto,
       US_NOMBRE            Descripcion,
       US_PASSWRD           Passwd,
       US_NIVEL             Status
);


ALTER TABLE USUARIO
       ADD PRIMARY KEY (US_CODIGO);


CREATE TABLE VERSION (
       VE_CODIGO            Codigo,
       VE_DESCRIP           Descripcion
);


ALTER TABLE VERSION
       ADD PRIMARY KEY (VE_CODIGO);


CREATE TABLE SENTINEL (
       SN_NUMERO            NumSentinel,
       SN_MODELO            Codigo,
       SN_FEC_INI           Fecha
);


ALTER TABLE SENTINEL
       ADD PRIMARY KEY (SN_NUMERO);


CREATE TABLE AUTORIZA (
       SI_FOLIO             FolioSistema,
       AU_FOLIO             FolioChico,
       AU_FECHA             Fecha,
       DI_CODIGO            Codigo,
       AU_HORA              Hora,
       SN_NUMERO            NumSentinel,
       VE_CODIGO            Codigo,
       US_CODIGO            UsuarioCorto,
       AU_NUM_EMP           Empleados,
       AU_MOD_0             Booleano,
       AU_MOD_1             Booleano,
       AU_MOD_2             Booleano,
       AU_MOD_3             Booleano,
       AU_MOD_4             Booleano,
       AU_MOD_5             Booleano,
       AU_MOD_6             Booleano,
       AU_MOD_7             Booleano,
       AU_MOD_8             Booleano,
       AU_MOD_9             Booleano,
       AU_USERS             FolioChico,
       AU_DEMO              Booleano,
       AU_CLAVE1            Descripcion,
       AU_CLAVE2            Descripcion,
       AU_FEC_GRA           Fecha,
       AU_COMENTA           Memo,
       AU_FEC_0             Fecha,
       AU_FEC_1             Fecha,
       AU_FEC_2             Fecha,
       AU_FEC_3             Fecha,
       AU_FEC_4             Fecha,
       AU_FEC_5             Fecha,
       AU_FEC_6             Fecha,
       AU_FEC_7             Fecha,
       AU_FEC_8             Fecha,
       AU_FEC_9             Fecha
);


ALTER TABLE AUTORIZA
       ADD PRIMARY KEY (SI_FOLIO, AU_FOLIO);

CREATE VIEW ACTUAL (
  SI_FOLIO,
  SI_RAZ_SOC,
  SI_EMPRESA,
  SI_OTRO,
  SI_CALLE,
  GR_CODIGO,
  SI_COLONIA,
  SI_PERSONA,
  CT_CODIGO,
  SI_TEL1,
  SI_TEL2,
  ET_CODIGO,
  SI_FAX,
  SI_EMAIL,
  SI_COMENTA,
  SI_GTE_RH,
  SI_GTE_CON,
  SI_GTE_SIS,
  SI_EMA_RH,
  SI_EMA_CON,
  SI_EMA_SIS,
  SI_GIRO,
  DI_CODIGO,
  AU_FOLIO,
  AU_FECHA,
  AU_HORA,
  SN_NUMERO,
  VE_CODIGO,
  US_CODIGO,
  AU_NUM_EMP,
  AU_MOD_0,
  AU_MOD_1,
  AU_MOD_2,
  AU_MOD_3,
  AU_MOD_4,
  AU_MOD_5,
  AU_MOD_6,
  AU_MOD_7,
  AU_MOD_8,
  AU_MOD_9,
  AU_USERS,
  AU_DEMO,
  AU_CLAVE1,
  AU_CLAVE2,
  AU_FEC_GRA,
  AU_COMENTA,
  AU_FEC_0,
  AU_FEC_1,
  AU_FEC_2,
  AU_FEC_3,
  AU_FEC_4,
  AU_FEC_5,
  AU_FEC_6,
  AU_FEC_7,
  AU_FEC_8,
  AU_FEC_9
) AS
SELECT 
       SI_FOLIO,
       SI_RAZ_SOC,
       SI_EMPRESA,
       SI_OTRO,
       SI_CALLE,
       GR_CODIGO,
       SI_COLONIA,
       SI_PERSONA,
       CT_CODIGO,
       SI_TEL1,
       SI_TEL2,
       ET_CODIGO,
       SI_FAX,
       SI_EMAIL,
       SI_COMENTA,
       SI_GTE_RH,
       SI_GTE_CON,
       SI_GTE_SIS,
       SI_EMA_RH,
       SI_EMA_CON,
       SI_EMA_SIS,
       SI_GIRO,
       DI_CODIGO,
       AU_FOLIO,
       AU_FECHA,
       AU_HORA,
       SN_NUMERO,
       VE_CODIGO,
       US_CODIGO,
       AU_NUM_EMP,
       AU_MOD_0,
       AU_MOD_1,
       AU_MOD_2,
       AU_MOD_3,
       AU_MOD_4,
       AU_MOD_5,
       AU_MOD_6,
       AU_MOD_7,
       AU_MOD_8,
       AU_MOD_9,
       AU_USERS,
       AU_DEMO,
       AU_CLAVE1,
       AU_CLAVE2,
       AU_FEC_GRA,
       AU_COMENTA,
       AU_FEC_0,
       AU_FEC_1,
       AU_FEC_2,
       AU_FEC_3,
       AU_FEC_4,
       AU_FEC_5,
       AU_FEC_6,
       AU_FEC_7,
       AU_FEC_8,
       AU_FEC_9

FROM SISTEMA
LEFT OUTER JOIN AUTORIZA
ON AUTORIZA.SI_FOLIO = SISTEMA.SI_FOLIO AND
   AUTORIZA.AU_FOLIO = ( SELECT MAX(AU_FOLIO) FROM AUTORIZA
                         WHERE AUTORIZA.SI_FOLIO = SISTEMA.SI_FOLIO )
LEFT OUTER JOIN CIUDAD
ON CIUDAD.CT_CODIGO = SISTEMA.CT_CODIGO
;


ALTER TABLE CIUDAD
       ADD FOREIGN KEY (ET_CODIGO)
                             REFERENCES ESTADO;


ALTER TABLE SISTEMA
       ADD FOREIGN KEY (GR_CODIGO)
                             REFERENCES GRUPO;


ALTER TABLE SISTEMA
       ADD FOREIGN KEY (CT_CODIGO)
                             REFERENCES CIUDAD;


ALTER TABLE AUTORIZA
       ADD FOREIGN KEY (DI_CODIGO)
                             REFERENCES DISTRIBU;


ALTER TABLE AUTORIZA
       ADD FOREIGN KEY (US_CODIGO)
                             REFERENCES USUARIO;


ALTER TABLE AUTORIZA
       ADD FOREIGN KEY (VE_CODIGO)
                             REFERENCES VERSION;


ALTER TABLE AUTORIZA
       ADD FOREIGN KEY (SN_NUMERO)
                             REFERENCES SENTINEL;


ALTER TABLE AUTORIZA
       ADD FOREIGN KEY (SI_FOLIO)
                             REFERENCES SISTEMA;



CREATE EXCEPTION ERWIN_PARENT_INSERT_RESTRICT "Cannot INSERT Parent table because Child table exists.";
CREATE EXCEPTION ERWIN_PARENT_UPDATE_RESTRICT "Cannot UPDATE Parent table because Child table exists.";
CREATE EXCEPTION ERWIN_PARENT_DELETE_RESTRICT "Cannot DELETE Parent table because Child table exists.";
CREATE EXCEPTION ERWIN_CHILD_INSERT_RESTRICT "Cannot INSERT Child table because Parent table does not exist.";
CREATE EXCEPTION ERWIN_CHILD_UPDATE_RESTRICT "Cannot UPDATE Child table because Parent table does not exist.";
CREATE EXCEPTION ERWIN_CHILD_DELETE_RESTRICT "Cannot DELETE Child table because Parent table does not exist.";

SET TERM !!;

CREATE TRIGGER tD_ESTADO FOR ESTADO AFTER DELETE AS
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* DELETE trigger on ESTADO */
DECLARE VARIABLE numrows INTEGER;
BEGIN
    /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
    /* ESTADO R/24 CIUDAD ON PARENT DELETE RESTRICT */
    select count(*)
      from CIUDAD
      where
        /*  %JoinFKPK(CIUDAD,OLD," = "," and") */
        CIUDAD.ET_CODIGO = OLD.ET_CODIGO into numrows;
    IF (numrows > 0) THEN
    BEGIN
      EXCEPTION ERWIN_PARENT_DELETE_RESTRICT;
    END


  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
END !!

CREATE TRIGGER tU_ESTADO FOR ESTADO AFTER UPDATE AS
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* UPDATE trigger on ESTADO */
DECLARE VARIABLE numrows INTEGER;
BEGIN
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* ESTADO R/24 CIUDAD ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.ET_CODIGO <> NEW.ET_CODIGO) THEN
  BEGIN
    update CIUDAD
      set
        /*  %JoinFKPK(CIUDAD,NEW," = ",",") */
        CIUDAD.ET_CODIGO = NEW.ET_CODIGO
      where
        /*  %JoinFKPK(CIUDAD,OLD," = "," and") */
        CIUDAD.ET_CODIGO = OLD.ET_CODIGO;
  END


  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
END !!

CREATE TRIGGER tD_CIUDAD FOR CIUDAD AFTER DELETE AS
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* DELETE trigger on CIUDAD */
DECLARE VARIABLE numrows INTEGER;
BEGIN
    /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
    /* CIUDAD Tiene SISTEMA ON PARENT DELETE RESTRICT */
    select count(*)
      from SISTEMA
      where
        /*  %JoinFKPK(SISTEMA,OLD," = "," and") */
        SISTEMA.CT_CODIGO = OLD.CT_CODIGO into numrows;
    IF (numrows > 0) THEN
    BEGIN
      EXCEPTION ERWIN_PARENT_DELETE_RESTRICT;
    END


  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
END !!

CREATE TRIGGER tU_CIUDAD FOR CIUDAD AFTER UPDATE AS
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* UPDATE trigger on CIUDAD */
DECLARE VARIABLE numrows INTEGER;
BEGIN
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* CIUDAD Tiene SISTEMA ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.CT_CODIGO <> NEW.CT_CODIGO) THEN
  BEGIN
    update SISTEMA
      set
        /*  %JoinFKPK(SISTEMA,NEW," = ",",") */
        SISTEMA.CT_CODIGO = NEW.CT_CODIGO
      where
        /*  %JoinFKPK(SISTEMA,OLD," = "," and") */
        SISTEMA.CT_CODIGO = OLD.CT_CODIGO;
  END


  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
END !!

CREATE TRIGGER tD_GRUPO FOR GRUPO AFTER DELETE AS
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* DELETE trigger on GRUPO */
DECLARE VARIABLE numrows INTEGER;
BEGIN
    /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
    /* GRUPO Le pertenecen SISTEMA ON PARENT DELETE RESTRICT */
    select count(*)
      from SISTEMA
      where
        /*  %JoinFKPK(SISTEMA,OLD," = "," and") */
        SISTEMA.GR_CODIGO = OLD.GR_CODIGO into numrows;
    IF (numrows > 0) THEN
    BEGIN
      EXCEPTION ERWIN_PARENT_DELETE_RESTRICT;
    END


  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
END !!

CREATE TRIGGER tU_GRUPO FOR GRUPO AFTER UPDATE AS
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* UPDATE trigger on GRUPO */
DECLARE VARIABLE numrows INTEGER;
BEGIN
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* GRUPO Le pertenecen SISTEMA ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.GR_CODIGO <> NEW.GR_CODIGO) THEN
  BEGIN
    update SISTEMA
      set
        /*  %JoinFKPK(SISTEMA,NEW," = ",",") */
        SISTEMA.GR_CODIGO = NEW.GR_CODIGO
      where
        /*  %JoinFKPK(SISTEMA,OLD," = "," and") */
        SISTEMA.GR_CODIGO = OLD.GR_CODIGO;
  END


  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
END !!

CREATE TRIGGER tD_SISTEMA FOR SISTEMA AFTER DELETE AS
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* DELETE trigger on SISTEMA */
DECLARE VARIABLE numrows INTEGER;
BEGIN
    /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
    /* SISTEMA R/18 AUTORIZA ON PARENT DELETE CASCADE */
    delete from AUTORIZA
      where
        /*  %JoinFKPK(AUTORIZA,OLD," = "," and") */
        AUTORIZA.SI_FOLIO = OLD.SI_FOLIO;


  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
END !!

CREATE TRIGGER tU_SISTEMA FOR SISTEMA AFTER UPDATE AS
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* UPDATE trigger on SISTEMA */
DECLARE VARIABLE numrows INTEGER;
BEGIN
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* SISTEMA R/18 AUTORIZA ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.SI_FOLIO <> NEW.SI_FOLIO) THEN
  BEGIN
    update AUTORIZA
      set
        /*  %JoinFKPK(AUTORIZA,NEW," = ",",") */
        AUTORIZA.SI_FOLIO = NEW.SI_FOLIO
      where
        /*  %JoinFKPK(AUTORIZA,OLD," = "," and") */
        AUTORIZA.SI_FOLIO = OLD.SI_FOLIO;
  END


  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
END !!

CREATE TRIGGER tD_DISTRIBU FOR DISTRIBU AFTER DELETE AS
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* DELETE trigger on DISTRIBU */
DECLARE VARIABLE numrows INTEGER;
BEGIN
    /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
    /* DISTRIBU R/23 AUTORIZA ON PARENT DELETE RESTRICT */
    select count(*)
      from AUTORIZA
      where
        /*  %JoinFKPK(AUTORIZA,OLD," = "," and") */
        AUTORIZA.DI_CODIGO = OLD.DI_CODIGO into numrows;
    IF (numrows > 0) THEN
    BEGIN
      EXCEPTION ERWIN_PARENT_DELETE_RESTRICT;
    END


  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
END !!

CREATE TRIGGER tU_DISTRIBU FOR DISTRIBU AFTER UPDATE AS
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* UPDATE trigger on DISTRIBU */
DECLARE VARIABLE numrows INTEGER;
BEGIN
  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
  /* DISTRIBU R/23 AUTORIZA ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.DI_CODIGO <> NEW.DI_CODIGO) THEN
  BEGIN
    update AUTORIZA
      set
        /*  %JoinFKPK(AUTORIZA,NEW," = ",",") */
        AUTORIZA.DI_CODIGO = NEW.DI_CODIGO
      where
        /*  %JoinFKPK(AUTORIZA,OLD," = "," and") */
        AUTORIZA.DI_CODIGO = OLD.DI_CODIGO;
  END


  /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
END !!

CREATE TRIGGER tD_USUARIO FOR USUARIO AFTER DELETE AS
  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
  /* DELETE trigger on USUARIO */
DECLARE VARIABLE numrows INTEGER;
BEGIN
    /* ERwin Builtin Thu Mar 04 08:39:19 1999 */
    /* USUARIO Registr� AUTORIZA ON PARENT DELETE RESTRICT */
    select count(*)
      from AUTORIZA
      where
        /*  %JoinFKPK(AUTORIZA,OLD," = "," and") */
        AUTORIZA.US_CODIGO = OLD.US_CODIGO into numrows;
    IF (numrows > 0) THEN
    BEGIN
      EXCEPTION ERWIN_PARENT_DELETE_RESTRICT;
    END


  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
END !!

CREATE TRIGGER tU_USUARIO FOR USUARIO AFTER UPDATE AS
  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
  /* UPDATE trigger on USUARIO */
DECLARE VARIABLE numrows INTEGER;
BEGIN
  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
  /* USUARIO Registr� AUTORIZA ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.US_CODIGO <> NEW.US_CODIGO) THEN
  BEGIN
    update AUTORIZA
      set
        /*  %JoinFKPK(AUTORIZA,NEW," = ",",") */
        AUTORIZA.US_CODIGO = NEW.US_CODIGO
      where
        /*  %JoinFKPK(AUTORIZA,OLD," = "," and") */
        AUTORIZA.US_CODIGO = OLD.US_CODIGO;
  END


  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
END !!

CREATE TRIGGER tD_VERSION FOR VERSION AFTER DELETE AS
  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
  /* DELETE trigger on VERSION */
DECLARE VARIABLE numrows INTEGER;
BEGIN
    /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
    /* VERSION Registrada AUTORIZA ON PARENT DELETE RESTRICT */
    select count(*)
      from AUTORIZA
      where
        /*  %JoinFKPK(AUTORIZA,OLD," = "," and") */
        AUTORIZA.VE_CODIGO = OLD.VE_CODIGO into numrows;
    IF (numrows > 0) THEN
    BEGIN
      EXCEPTION ERWIN_PARENT_DELETE_RESTRICT;
    END


  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
END !!

CREATE TRIGGER tU_VERSION FOR VERSION AFTER UPDATE AS
  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
  /* UPDATE trigger on VERSION */
DECLARE VARIABLE numrows INTEGER;
BEGIN
  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
  /* VERSION Registrada AUTORIZA ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.VE_CODIGO <> NEW.VE_CODIGO) THEN
  BEGIN
    update AUTORIZA
      set
        /*  %JoinFKPK(AUTORIZA,NEW," = ",",") */
        AUTORIZA.VE_CODIGO = NEW.VE_CODIGO
      where
        /*  %JoinFKPK(AUTORIZA,OLD," = "," and") */
        AUTORIZA.VE_CODIGO = OLD.VE_CODIGO;
  END


  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
END !!

CREATE TRIGGER tD_SENTINEL FOR SENTINEL AFTER DELETE AS
  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
  /* DELETE trigger on SENTINEL */
DECLARE VARIABLE numrows INTEGER;
BEGIN
    /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
    /* SENTINEL R/19 AUTORIZA ON PARENT DELETE RESTRICT */
    select count(*)
      from AUTORIZA
      where
        /*  %JoinFKPK(AUTORIZA,OLD," = "," and") */
        AUTORIZA.SN_NUMERO = OLD.SN_NUMERO into numrows;
    IF (numrows > 0) THEN
    BEGIN
      EXCEPTION ERWIN_PARENT_DELETE_RESTRICT;
    END


  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
END !!

CREATE TRIGGER tU_SENTINEL FOR SENTINEL AFTER UPDATE AS
  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
  /* UPDATE trigger on SENTINEL */
DECLARE VARIABLE numrows INTEGER;
BEGIN
  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
  /* SENTINEL R/19 AUTORIZA ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.SN_NUMERO <> NEW.SN_NUMERO) THEN
  BEGIN
    update AUTORIZA
      set
        /*  %JoinFKPK(AUTORIZA,NEW," = ",",") */
        AUTORIZA.SN_NUMERO = NEW.SN_NUMERO
      where
        /*  %JoinFKPK(AUTORIZA,OLD," = "," and") */
        AUTORIZA.SN_NUMERO = OLD.SN_NUMERO;
  END


  /* ERwin Builtin Thu Mar 04 08:39:20 1999 */
END !!

