unit FRepUsoSentinel;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, Quickrpt, Qrctrls;

type
  TRepUsoSentinel = class(TForm)
    qrp1: TQuickRep;
    PageHeaderBand1: TQRBand;
    QRLabel5: TQRLabel;
    QRShape1: TQRShape;
    QRTitulo: TQRLabel;
    QRImage1: TQRImage;
    QRLabel18: TQRLabel;
    QRSysData1: TQRSysData;
    QRShape4: TQRShape;
    QRBand1: TQRBand;
    SN_NUMERO: TQRDBText;
    SI_FOLIO: TQRDBText;
    SN_NUMEROlbl: TQRLabel;
    SI_FOLIOlbl: TQRLabel;
    SI_RAZ_SOC: TQRDBText;
    SI_RAZ_SOClbl: TQRLabel;
    procedure QRSysData1Print(sender: TObject; var Value: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RepUsoSentinel: TRepUsoSentinel;

implementation

uses DRep;

{$R *.DFM}

procedure TRepUsoSentinel.QRSysData1Print(sender: TObject; var Value: String);
begin
     Value := FormatDateTime( 'dd/mmm/yyyy', Now );
end;

end.
