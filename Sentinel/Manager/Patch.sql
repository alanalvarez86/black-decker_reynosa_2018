connect "D:\3Win_13\Datos\Sentinel.gdb" USER "SYSDBA" PASSWORD "m";
create domain DESC_LARGA VARCHAR(255) default "" NOT NULL;
alter table SISTEMA add SI_SOC_RAZ DESC_LARGA,
                    add SI_STREET DESC_LARGA,
                    add SI_COLONY DESC_LARGA;
update SISTEMA set SI_SOC_RAZ = SI_RAZ_SOC,
                   SI_STREET = SI_CALLE,
                   SI_COLONY = SI_COLONIA;
drop view LIBRE;
drop view ASIGNADO;
drop view ACTUAL;
alter table SISTEMA drop SI_RAZ_SOC,
                    drop SI_CALLE,
                    drop SI_COLONIA;
alter table SISTEMA add SI_RAZ_SOC DESC_LARGA,
                    add SI_CALLE DESC_LARGA,
                    add SI_COLONIA DESC_LARGA;
update SISTEMA set SI_RAZ_SOC = SI_SOC_RAZ,
                   SI_CALLE = SI_STREET,
                   SI_COLONIA = SI_COLONY;
alter table SISTEMA drop SI_SOC_RAZ,
                    drop SI_STREET,
                    drop SI_COLONY;
create table GIRO( GI_CODIGO CODIGO PRIMARY KEY,
                   GI_DESCRIP DESCRIPCION );
insert into GIRO( GI_CODIGO, GI_DESCRIP ) values ( 'MAQ', 'Maquiladora' );
create table PAIS( PA_CODIGO CODIGO PRIMARY KEY,
                   PA_DESCRIP DESCRIPCION );
insert into PAIS( PA_CODIGO, PA_DESCRIP ) values ( 'MEX', 'M�xico' );
alter table SISTEMA add SI_MIGRADO BOOLEANO,
                    add SI_PROBLEMA BOOLEANO,
                    add SI_FEC_SOL FECHA,
                    add PA_CODIGO CODIGO,
                    add GI_CODIGO CODIGO,
                    add SI_EMP_EMP EMPLEADOS,
                    add SI_SIS_VIE DESCRIPCION;
update SISTEMA set GI_CODIGO = 'MAQ';
update SISTEMA set PA_CODIGO = 'MEX';
commit;
alter table SISTEMA add foreign key ( GI_CODIGO ) references GIRO ( GI_CODIGO ) on delete set default on update cascade;
alter table SISTEMA add foreign key ( PA_CODIGO ) references PAIS ( PA_CODIGO ) on delete set default on update cascade;
create view ACTUAL (
       SI_FOLIO,
       SI_RAZ_SOC,
       SI_EMPRESA,
       SI_OTRO,
       SI_CALLE,
       GR_CODIGO,
       SI_COLONIA,
       SI_PERSONA,
       CT_CODIGO,
       SI_TEL1,
       SI_TEL2,
       ET_CODIGO,
       PA_CODIGO,
       SI_FAX,
       SI_EMAIL,
       SI_COMENTA,
       SI_GTE_RH,
       SI_GTE_CON,
       SI_GTE_SIS,
       SI_EMA_RH,
       SI_EMA_CON,
       SI_EMA_SIS,
       SI_GIRO,
       GI_CODIGO,
       DI_CODIGO,
       SI_MIGRADO,
       SI_PROBLEMA,
       SI_FEC_SOL,
       SI_EMP_EMP,
       SI_SIS_VIE,
       AU_FOLIO,
       AU_FECHA,
       SN_NUMERO,
       VE_CODIGO,
       US_CODIGO,
       AU_NUM_EMP,
       AU_MOD_0,
       AU_MOD_1,
       AU_MOD_2,
       AU_MOD_3,
       AU_MOD_4,
       AU_MOD_5,
       AU_MOD_6,
       AU_MOD_7,
       AU_MOD_8,
       AU_MOD_9,
       AU_USERS,
       AU_KIT_DIS,
       AU_CLAVE1,
       AU_CLAVE2,
       AU_FEC_GRA,
       AU_COMENTA,
       AU_PRE_0,
       AU_PRE_1,
       AU_PRE_2,
       AU_PRE_3,
       AU_PRE_4,
       AU_PRE_5,
       AU_PRE_6,
       AU_PRE_7,
       AU_PRE_8,
       AU_PRE_9,
       AU_FEC_AUT,
       AU_FEC_PRE,
       AU_PAR_AUT,
       AU_PAR_PRE,
       AU_DEFINIT,
       AU_TIPOS,
       AU_SQL_SVR,
       AU_PLATFOM ) as
select SISTEMA.SI_FOLIO,
       SI_RAZ_SOC,
       SI_EMPRESA,
       SI_OTRO,
       SI_CALLE,
       GR_CODIGO,
       SI_COLONIA,
       SI_PERSONA,
       CT_CODIGO,
       SI_TEL1,
       SI_TEL2,
       ET_CODIGO,
       PA_CODIGO,
       SI_FAX,
       SI_EMAIL,
       SI_COMENTA,
       SI_GTE_RH,
       SI_GTE_CON,
       SI_GTE_SIS,
       SI_EMA_RH,
       SI_EMA_CON,
       SI_EMA_SIS,
       SI_GIRO,
       GI_CODIGO,
       DI_CODIGO,
       SI_MIGRADO,
       SI_PROBLEMA,
       SI_FEC_SOL,
       SI_EMP_EMP,
       SI_SIS_VIE,
       AU_FOLIO,
       AU_FECHA,
       SN_NUMERO,
       VE_CODIGO,
       US_CODIGO,
       AU_NUM_EMP,
       AU_MOD_0,
       AU_MOD_1,
       AU_MOD_2,
       AU_MOD_3,
       AU_MOD_4,
       AU_MOD_5,
       AU_MOD_6,
       AU_MOD_7,
       AU_MOD_8,
       AU_MOD_9,
       AU_USERS,
       AU_KIT_DIS,
       AU_CLAVE1,
       AU_CLAVE2,
       AU_FEC_GRA,
       AU_COMENTA,
       AU_PRE_0,
       AU_PRE_1,
       AU_PRE_2,
       AU_PRE_3,
       AU_PRE_4,
       AU_PRE_5,
       AU_PRE_6,
       AU_PRE_7,
       AU_PRE_8,
       AU_PRE_9,
       AU_FEC_AUT,
       AU_FEC_PRE,
       AU_PAR_AUT,
       AU_PAR_PRE,
       AU_DEFINIT,
       AU_TIPOS,
       AU_SQL_SVR,
       AU_PLATFOM
from SISTEMA
left outer join AUTORIZA on ( AUTORIZA.SI_FOLIO = SISTEMA.SI_FOLIO )
left outer join CIUDAD on ( CIUDAD.CT_CODIGO = SISTEMA.CT_CODIGO )
where ( AUTORIZA.AU_FOLIO = ( select MAX( X.AU_FOLIO) from AUTORIZA X where ( X.SI_FOLIO = SISTEMA.SI_FOLIO ) ) )

create view LIBRE ( SN_NUMERO ) as
select SN_NUMERO from SENTINEL where
( ( select COUNT(*) from ACTUAL where ( ACTUAL.SN_NUMERO = SENTINEL.SN_NUMERO ) ) = 0 );

create VIEW ASIGNADO( SN_NUMERO, SI_FOLIO ) as
select SENTINEL.SN_NUMERO, ACTUAL.SI_FOLIO
from SENTINEL
left outer join ACTUAL on ( ACTUAL.SN_NUMERO = SENTINEL.SN_NUMERO );









