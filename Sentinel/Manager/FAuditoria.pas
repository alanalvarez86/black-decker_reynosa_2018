unit FAuditoria;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, Mask, ComCtrls,
     ZetaFecha;

type
  TAuditoria = class(TForm)
    InicioLBL: TLabel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    Inicio: TZetaFecha;
    FinalLBL: TLabel;
    Final: TZetaFecha;
    Salida: TRadioGroup;
    ArchivoLBL: TLabel;
    Archivo: TEdit;
    ArchivoFind: TSpeedButton;
    SaveDialog: TSaveDialog;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ArchivoFindClick(Sender: TObject);
    procedure SalidaClick(Sender: TObject);
  private
    { Private declarations }
    function ToPrinter: Boolean;
    procedure SetControls;
  public
    { Public declarations }
  end;

var
  Auditoria: TAuditoria;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     DRep,
     DSentinel;

{$R *.DFM}

procedure TAuditoria.FormShow(Sender: TObject);
begin
     Inicio.Valor := ZetaCommonTools.FirstDayOfMonth( Now );
     Final.Valor := ZetaCommonTools.LastDayOfMonth( Now );
     Archivo.Text := 'C:\Temp\auditoria.xls';
     ActiveControl := Inicio;
     SetControls;
end;

function TAuditoria.ToPrinter: Boolean;
begin
     Result := ( Salida.ItemIndex = 0 );
end;

procedure TAuditoria.SetControls;
begin
     with Archivo do
     begin
          Enabled := not ToPrinter;
          ArchivoLBL.Enabled := Enabled;
          ArchivoFind.Enabled := Enabled;
     end;
end;

procedure TAuditoria.ArchivoFindClick(Sender: TObject);
begin
     with Archivo do
     begin
          with SaveDialog do
          begin
               FileName := ExtractFileName( Text );
               InitialDir := ExtractFilePath( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TAuditoria.SalidaClick(Sender: TObject);
begin
     SetControls;
end;

procedure TAuditoria.OKClick(Sender: TObject);
var
   dStart, dEnd: TDate;
   oCursor: TCursor;
begin
     dStart := Inicio.Valor;
     dEnd := Final.Valor;
     if ( dStart > dEnd ) then
     begin
          ZetaDialogo.zError( 'ˇ Error !', 'Error En Los Parámetros Del Reporte', 0 );
          ActiveControl := Inicio;
     end
     else
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if dmRep.ImprimeAuditoria( dStart, dEnd, ToPrinter, Archivo.Text ) then
                ModalResult := mrOk
             else
                 ActiveControl := Inicio;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

end.
