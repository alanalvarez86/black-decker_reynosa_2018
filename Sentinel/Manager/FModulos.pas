unit FModulos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, Mask, ComCtrls,
     ZetaFecha, CheckLst;

type
  TModuleLister = class(TForm)
    Salida: TRadioGroup;
    PanelInferior: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    SaveDialog: TSaveDialog;
    PanelArchivo: TPanel;
    ArchivoLBL: TLabel;
    Archivo: TEdit;
    ArchivoFind: TSpeedButton;
    ModulosGB: TGroupBox;
    ModuleList: TCheckListBox;
    SistemasVigentes: TCheckBox;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ArchivoFindClick(Sender: TObject);
    procedure SalidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ModuleListClick(Sender: TObject);
  private
    { Private declarations }
    function ModuloSeleccionado: Boolean;
    function ToPrinter: Boolean;
    procedure SetControls;
  public
    { Public declarations }
  end;

var
  ModuleLister: TModuleLister;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     FAutoClasses,
     FAutoServer,
     FAutoMaster,
     DRep,
     DSentinel;

{$R *.DFM}

procedure TModuleLister.FormCreate(Sender: TObject);
var
   i: Integer;
begin
     with AutoMaster do
     begin
          GetModulos( Self.ModuleList, True );
     end;
     with ModuleList do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               Checked[ i ] := False;
          end;
     end;
end;

procedure TModuleLister.FormShow(Sender: TObject);
begin
     Archivo.Text := 'C:\Temp\ModulosPorCliente.xls';
     ActiveControl := ModuleList;
     SetControls;
end;

function TModuleLister.ToPrinter: Boolean;
begin
     Result := ( Salida.ItemIndex = 0 );
end;

function TModuleLister.ModuloSeleccionado: Boolean;
var
   i: Integer;
begin
     Result := False;
     with ModuleList do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               if Checked[ i ] then
               begin
                    Result := True;
                    Break;
               end;
          end;
     end;
end;

procedure TModuleLister.SetControls;
begin
     with Archivo do
     begin
          Enabled := not ToPrinter;
          ArchivoLBL.Enabled := Enabled;
          ArchivoFind.Enabled := Enabled;
     end;
     OK.Enabled := ModuloSeleccionado;
end;

procedure TModuleLister.ArchivoFindClick(Sender: TObject);
begin
     with Archivo do
     begin
          with SaveDialog do
          begin
               FileName := ExtractFileName( Text );
               InitialDir := ExtractFilePath( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TModuleLister.SalidaClick(Sender: TObject);
begin
     SetControls;
end;

procedure TModuleLister.ModuleListClick(Sender: TObject);
begin
     SetControls;
end;

procedure TModuleLister.OKClick(Sender: TObject);
var
   oCursor: TCursor;
   i, iModulos: DWord;
   eModulo: TModulos;
begin
     if ModuloSeleccionado then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             iModulos := 0;
             for eModulo := Low( TModulos ) to High( TModulos ) do
             begin
                  i := Ord( eModulo );
                  if Self.ModuleList.Checked[ i ] then
                  begin
                       iModulos := FAutoClasses.BitOn( iModulos, i );
                  end;
             end;
             if dmRep.ImprimeModulosUsados( iModulos, False, SistemasVigentes.Checked, ToPrinter, Archivo.Text ) then
                ModalResult := mrOk
             else
                 ActiveControl := ModuleList;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

end.
