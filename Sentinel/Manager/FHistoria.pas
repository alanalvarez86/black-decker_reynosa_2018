unit FHistoria;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, StdCtrls, DBCtrls,
     ExtCtrls, Mask, ComCtrls, Buttons, Db, DbTables,
     ZetaFecha,
     ZetaDBTextBox,
     ZetaKeyCombo;

type
  TFormHistoria = class(TForm)
    Panel1: TPanel;
    PageControl: TPageControl;
    Actual: TTabSheet;
    Modulos: TTabSheet;
    Label6: TLabel;
    Label4: TLabel;
    Label14: TLabel;
    Label9: TLabel;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DataSource: TDataSource;
    ImprimirBtn: TSpeedButton;
    GroupBox2: TGroupBox;
    AU_MOD_0: TDBCheckBox;
    AU_MOD_1: TDBCheckBox;
    AU_MOD_2: TDBCheckBox;
    AU_MOD_3: TDBCheckBox;
    AU_MOD_4: TDBCheckBox;
    AU_MOD_5: TDBCheckBox;
    AU_MOD_6: TDBCheckBox;
    AU_MOD_9: TDBCheckBox;
    AU_FEC_AUTlbl: TLabel;
    GroupBox3: TGroupBox;
    AU_PRE_0: TDBCheckBox;
    AU_PRE_1: TDBCheckBox;
    AU_PRE_2: TDBCheckBox;
    AU_PRE_3: TDBCheckBox;
    AU_PRE_4: TDBCheckBox;
    AU_PRE_5: TDBCheckBox;
    AU_PRE_6: TDBCheckBox;
    AU_PRE_9: TDBCheckBox;
    AU_FEC_PRElbl: TLabel;
    Label1: TLabel;
    Label5: TLabel;
    AU_FECHA: TZetaDBTextBox;
    AU_FOLIO: TZetaDBTextBox;
    DBNavigator1: TDBNavigator;
    SI_RAZ_SOC: TZetaDBTextBox;
    SI_FOLIO: TZetaDBTextBox;
    DI_CODIGO: TZetaDBTextBox;
    AU_NUM_EMP: TZetaDBTextBox;
    AU_USERS: TZetaDBTextBox;
    SN_NUMERO: TZetaDBTextBox;
    VE_DESCRIP: TZetaDBTextBox;
    AU_COMENTA: TDBMemo;
    Label10: TLabel;
    AU_FEC_AUT: TZetaDBTextBox;
    AU_FEC_PRE: TZetaDBTextBox;
    AU_MOD_7: TDBCheckBox;
    AU_MOD_8: TDBCheckBox;
    AU_PRE_7: TDBCheckBox;
    AU_PRE_8: TDBCheckBox;
    AU_KIT_DIS: TZetaDBTextBox;
    AU_MOD_10: TDBCheckBox;
    AU_MOD_11: TDBCheckBox;
    AU_MOD_12: TDBCheckBox;
    AU_PRE_10: TDBCheckBox;
    AU_PRE_11: TDBCheckBox;
    AU_PRE_12: TDBCheckBox;
    AU_MOD_13: TDBCheckBox;
    AU_PRE_13: TDBCheckBox;
    Label11: TLabel;
    AU_PLATFORM: TZetaDBTextBox;
    AU_SQL_BD: TZetaDBTextBox;
    Label12: TLabel;
    AU_MOD_14: TDBCheckBox;
    AU_MOD_16: TDBCheckBox;
    AU_MOD_15: TDBCheckBox;
    AU_PRE_14: TDBCheckBox;
    AU_PRE_15: TDBCheckBox;
    AU_PRE_16: TDBCheckBox;
    Label13: TLabel;
    Label15: TLabel;
    AU_CLAVE2: TZetaDBTextBox;
    AU_CLAVE1: TZetaDBTextBox;
    Escribir: TBitBtn;
    SaveDialog: TSaveDialog;
    Enviar: TBitBtn;
    AU_MOD_18: TDBCheckBox;
    AU_MOD_17: TDBCheckBox;
    AU_PRE_18: TDBCheckBox;
    AU_PRE_17: TDBCheckBox;
    AU_MOD_19: TDBCheckBox;
    AU_MOD_20: TDBCheckBox;
    AU_PRE_19: TDBCheckBox;
    AU_PRE_20: TDBCheckBox;
    AU_MOD_21: TDBCheckBox;
    AU_MOD_22: TDBCheckBox;
    AU_PRE_21: TDBCheckBox;
    AU_PRE_22: TDBCheckBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    Label16: TLabel;
    AU_BD_LIM: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ImprimirBtnClick(Sender: TObject);
    procedure AU_KIT_DISGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure AU_PLATFORMGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure AU_SQL_BDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure EscribirClick(Sender: TObject);
    procedure AU_BD_LIMGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
  private
    { Private declarations }
    FQuery: TDataset;
    function GetExpirationInfo( const dValue: TDate ): String;
  public
    { Public declarations }
    property Query: TDataset read FQuery write FQuery;
  end;

var
  FormHistoria: TFormHistoria;

implementation

uses ZetaWinAPITools,
     FAutoClasses,
     FAutoServer,
     FAutoMaster,
     DRep,
     ZetaLicenseClasses,
     DSentinel;

{$R *.DFM}

{ ********* TFormHistoria ********* }

procedure TFormHistoria.FormCreate(Sender: TObject);

function GetModuleName( const eModule: TModulos ): String;
begin
     Result := FAutoClasses.GetModuloName( eModule );
end;

begin
     AU_MOD_0.Caption := GetModuleName( okRecursos );
     AU_MOD_1.Caption := GetModuleName( okNomina );
     AU_MOD_2.Caption := GetModuleName( okIMSS );
     AU_MOD_3.Caption := GetModuleName( okAsistencia );
     AU_MOD_4.Caption := GetModuleName( okCursos );
     AU_MOD_5.Caption := GetModuleName( okSupervisores );
     AU_MOD_6.Caption := GetModuleName( okCafeteria );
     AU_MOD_7.Caption := GetModuleName( okLabor );
     AU_MOD_8.Caption := GetModuleName( okKiosko );
     AU_MOD_9.Caption := GetModuleName( okValidador );
     AU_MOD_10.Caption := GetModuleName( okGraficas );
     AU_MOD_11.Caption := GetModuleName( okReportesMail );
     AU_MOD_12.Caption := GetModuleName( okHerramientas );
     AU_MOD_13.Caption := GetModuleName( okL5Poll );
     AU_MOD_14.Caption := GetModuleName( okSeleccion );
     AU_MOD_15.Caption := GetModuleName( okEnfermeria );
     AU_MOD_16.Caption := GetModuleName( okMonitor );
     AU_MOD_17.Caption := GetModuleName( okPortal );
     AU_MOD_18.Caption := GetModuleName( okPlanCarrera );
     AU_MOD_19.Caption := GetModuleName( okAccesos );
     AU_MOD_20.Caption := GetModuleName( okVisitantes );
     AU_MOD_21.Caption := GetModuleName( okEvaluacion );
     AU_MOD_22.Caption := GetModuleName( okWorkflow );
     AU_PRE_0.Caption := GetModuleName( okRecursos );
     AU_PRE_1.Caption := GetModuleName( okNomina );
     AU_PRE_2.Caption := GetModuleName( okIMSS );
     AU_PRE_3.Caption := GetModuleName( okAsistencia );
     AU_PRE_4.Caption := GetModuleName( okCursos );
     AU_PRE_5.Caption := GetModuleName( okSupervisores );
     AU_PRE_6.Caption := GetModuleName( okCafeteria );
     AU_PRE_7.Caption := GetModuleName( okLabor );
     AU_PRE_8.Caption := GetModuleName( okKiosko );
     AU_PRE_9.Caption := GetModuleName( okValidador );
     AU_PRE_10.Caption := GetModuleName( okGraficas );
     AU_PRE_11.Caption := GetModuleName( okReportesMail );
     AU_PRE_12.Caption := GetModuleName( okHerramientas );
     AU_PRE_13.Caption := GetModuleName( okL5Poll );
     AU_PRE_14.Caption := GetModuleName( okSeleccion );
     AU_PRE_15.Caption := GetModuleName( okEnfermeria );
     AU_PRE_16.Caption := GetModuleName( okMonitor );
     AU_PRE_17.Caption := GetModuleName( okPortal );
     AU_PRE_18.Caption := GetModuleName( okPlanCarrera );
     AU_PRE_19.Caption := GetModuleName( okAccesos );
     AU_PRE_20.Caption := GetModuleName( okVisitantes );
     AU_PRE_21.Caption := GetModuleName( okEvaluacion );
     AU_PRE_22.Caption := GetModuleName( okWorkflow );
end;

procedure TFormHistoria.FormShow(Sender: TObject);
begin
     dmSentinel.AbrirHistoria( Datasource, Query );
     PageControl.ActivePage := Actual;
end;

procedure TFormHistoria.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     dmSentinel.ResetDatasource( Datasource );
end;

procedure TFormHistoria.ImprimirBtnClick(Sender: TObject);
begin
     DRep.InitReportes;
     with Datasource.Dataset do
     begin
          if True then
             dmRep.ImprimeClaveAnterior( FieldByName( 'SI_FOLIO' ).AsInteger, FieldByName( 'AU_FOLIO' ).AsInteger )
          else
              dmRep.ImprimeClaveHTML( FieldByName( 'SI_FOLIO' ).AsInteger, FieldByName( 'AU_FOLIO' ).AsInteger, 'D:\Sentinel\clave.htm' );
     end;
end;

procedure TFormHistoria.AU_KIT_DISGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := IntToBoolText( Sender.AsInteger );
end;

procedure TFormHistoria.AU_PLATFORMGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := FAutoClasses.GetPlatformName( TPlataforma( Sender.AsInteger ) );
end;

procedure TFormHistoria.AU_SQL_BDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := FAutoClasses.GetSQLEngineName( TSQLEngine( Sender.AsInteger ) );
end;

function TFormHistoria.GetExpirationInfo( const dValue: TDate ): String;
begin
     if ( dValue = FAutoClasses.GetFechaDefinitiva ) then
     begin
          Result := ' ( Sin Vencimiento )';
     end
     else
     if ( dValue > NullDateTime ) and ( dValue <> FAutoClasses.GetFechaDefinitiva ) then
     begin
          Result := Format( ' Hasta %s', [ FormatDateTime( 'dd/mmm/yyyy', dValue ) ] );
     end
     else
     begin
          Result := '';
     end;
end;

procedure TFormHistoria.EscribirClick(Sender: TObject);
const
     K_FECHA = 'dd/mmm/yyyy';
     K_MODULO = '%25.25s : %s';
var
   sFileName: String;
   oCursor: TCursor;
   lOk: Boolean;
   FDatos: TStrings;
   i: Integer;
begin
     sFileName := Format( '%sClaves_%s.txt', [ ZetaWinAPITools.FormatDirectory( ZetaWinAPITools.GetTempDir ),IntToStr( Datasource.Dataset.FieldByName( 'SN_NUMERO' ).AsInteger ) ] );
     lOk := False;
     with SaveDialog do
     begin
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          if Execute then
          begin
               sFileName := FileName;
               lOk := True;
          end;
     end;
     if lOk then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             FDatos := TStringList.Create;
             try
                with FDatos do
                begin
                     with Datasource.Dataset do
                     begin
                          Add( 'Empresa #:    ' + IntToStr( FieldByName( 'SI_FOLIO' ).AsInteger ) );
                          Add( 'Nombre:       ' + FieldByName( 'SI_RAZ_SOC' ).AsString );
                          Add( 'Movimiento #: ' + IntToStr( FieldByName( 'AU_FOLIO' ).AsInteger ) );
                          Add( 'Fecha:        ' + FormatDateTime( K_FECHA, FieldByName( 'AU_FECHA' ).AsDateTime ) );
                          Add( '' );
                          Add( '-------------- Datos de las Claves -----------------------' );
                          Add( '' );
                          Add( 'Distribuidor:     ' + FieldByName( 'DI_CODIGO' ).AsString );
                          Add( 'Empleados:        ' + IntToStr( FieldByName( 'AU_NUM_EMP' ).AsInteger ) );
                          Add( 'Licencias:        ' + IntToStr( FieldByName( 'AU_USERS' ).AsInteger ) );
                          Add( '# de Sentinel     ' + IntToStr( FieldByName( 'SN_NUMERO' ).AsInteger ) );
                          Add( 'Versi�n:          ' + FieldByName( 'VE_DESCRIP' ).AsString );
                          Add( 'Kit Distribuidor: ' + IntToBoolText( FieldByName( 'AU_KIT_DIS' ).AsInteger ) );
                          Add( 'Plataforma:       ' + FAutoClasses.GetPlatformName( TPlataforma( FieldByName( 'AU_PLATFORM' ).AsInteger ) ) );
                          Add( 'Base De Datos:    ' + FAutoClasses.GetSQLEngineName( TSQLEngine( FieldByName( 'AU_SQL_BD' ).AsInteger ) ) );
                          Add( 'L�mite BD Prod.:  ' + IntToStr( GetIntervaloLimiteBDConfig(FieldByName( 'AU_BD_LIM' ).AsInteger ) ) );
                          Add( 'Par�metro:        ' + IntToStr( FieldByName( 'AU_PAR_AUT' ).AsInteger ) );
                          Add( 'Clave # 1:        ' + FieldByName( 'AU_CLAVE1' ).AsString );
                          Add( 'Clave # 2:        ' + FieldByName( 'AU_CLAVE2' ).AsString );
                          Add( '' );
                          Add( Format( '-------- M�dulos Autorizados%s -----------------------', [ GetExpirationInfo( FieldByName( 'AU_FEC_AUT' ).AsDateTime ) ] ) );
                          Add( '' );
                          for i := 1 to Ord( High( TModulos ) ) do
                          begin
                               Add( Format( K_MODULO, [ AutoMaster.GetModuloStr( TModulos( i ) ), IntToBoolText( FieldByName( Format( 'AU_MOD_%d', [ i ] ) ).AsInteger ) ] ) );
                          end;
                          Add( '' );
                          Add( Format( '-------- M�dulos Prestados%s -----------------------', [ GetExpirationInfo( FieldByName( 'AU_FEC_PRE' ).AsDateTime ) ] ) );
                          Add( '' );
                          for i := 1 to Ord( High( TModulos ) ) do
                          begin
                               Add( Format( K_MODULO, [ AutoMaster.GetModuloStr( TModulos( i ) ), IntToBoolText( FieldByName( Format( 'AU_PRE_%d', [ i ] ) ).AsInteger ) ] ) );
                          end;
                     end;
                     SaveToFile( sFileName );
                end;
             finally
                    FDatos.Free;
             end;
             CallNotePad( sFileName );
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TFormHistoria.AU_BD_LIMGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
     Text := IntToStr( GetIntervaloLimiteBDConfig(Sender.AsInteger ) );
end;

end.
