CREATE VIEW ACTUAL (
       SI_FOLIO,
       SI_RAZ_SOC,
       SI_EMPRESA,
       SI_OTRO,
       SI_CALLE,
       GR_CODIGO,
       SI_COLONIA,
       SI_PERSONA,
       CT_CODIGO,
       SI_TEL1,
       SI_TEL2,
       ET_CODIGO,
       SI_FAX,
       SI_EMAIL,
       SI_COMENTA,
       SI_GTE_RH,
       SI_GTE_CON,
       SI_GTE_SIS,
       SI_EMA_RH,
       SI_EMA_CON,
       SI_EMA_SIS,
       SI_GIRO,
       DI_CODIGO
) AS
SELECT 
       SI_FOLIO,
       SI_RAZ_SOC,
       SI_EMPRESA,
       SI_OTRO,
       SI_CALLE,
       GR_CODIGO,
       SI_COLONIA,
       SI_PERSONA,
       CT_CODIGO,
       SI_TEL1,
       SI_TEL2,
       ET_CODIGO,
       SI_FAX,
       SI_EMAIL,
       SI_COMENTA,
       SI_GTE_RH,
       SI_GTE_CON,
       SI_GTE_SIS,
       SI_EMA_RH,
       SI_EMA_CON,
       SI_EMA_SIS,
       SI_GIRO,
       AUTORIZA.DI_CODIGO
FROM SISTEMA
LEFT OUTER JOIN AUTORIZA
ON AUTORIZA.SI_FOLIO = SISTEMA.SI_FOLIO AND
   AUTORIZA.AU_FOLIO = ( SELECT MAX(AU_FOLIO) FROM AUTORIZA
                         WHERE AUTORIZA.SI_FOLIO = SISTEMA.SI_FOLIO )
