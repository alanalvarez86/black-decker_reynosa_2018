unit IWAddExito;
{PUBDIST}

interface

uses
  IWAppForm, IWApplication, IWTypes,
  Classes, Controls, SysUtils,
  IWControl,  IWExtCtrls, IWCompLabel, IWLayoutMgr, IWTemplateProcessorHTML,
  IWHTMLControls;

type
  TAddExito = class(TIWAppForm)
    IWTimer: TIWTimer;
    ExitoLBL: TIWLabel;
    IWLabel1: TIWLabel;
    IWLabel2: TIWLabel;
    IWLabel3: TIWLabel;
    IWTemplate: TIWTemplateProcessorHTML;
    RegresarLNK: TIWLink;
    procedure IWTimerTimer(Sender: TObject);
    procedure RegresarLNKClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure SetDireccion( const sValue: String );
  end;

implementation
{$R *.dfm}

uses
  ServerController;

{ TAddExito }

procedure TAddExito.SetDireccion(const sValue: String);
begin
     ExitoLBL.Text := Format( '%s', [ sValue ] );
end;

procedure TAddExito.IWTimerTimer(Sender: TObject);
begin
     Release;
end;

procedure TAddExito.RegresarLNKClick(Sender: TObject);
begin
     Release;
end;

end.