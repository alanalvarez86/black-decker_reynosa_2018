unit ServerController;
{PUBDIST}

interface

uses
  Classes,
  DSuscripciones,
  IWServerControllerBase, IWAppForm, IWApplication,
  SysUtils;

type
  TIWServerController = class(TIWServerControllerBase)
    procedure IWServerControllerBaseNewSession(ASession: TIWApplication; var VMainForm: TIWAppForm);
    procedure IWServerControllerBaseCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  // This is a class which you can add variables to that are specific to the user. Add variables
  // to this class instead of creating global variables. This object can references by using:
  //   UserSession
  // So if a variable named UserName of type string is added, it can be referenced by using:
  //   UserSession.UserName
  // Such variables are similar to globals in a normal application, however these variables are
  // specific to each user.
  //
  // See the IntraWeb Manual for more details.
  TUserSession = class(TComponent)
  public
    DSuscripciones: TdmSuscripciones;
    //
    constructor Create(AOwner: TComponent); override;
  end;

// Procs
  function UserSession: TUserSession;

implementation
{$R *.dfm}

uses
  IWInit;

function UserSession: TUserSession;
begin
     Result := TUserSession(RWebApplication.Data);
end;

{ TUserSession }

constructor TUserSession.Create(AOwner: TComponent);
begin
     inherited;
     DSuscripciones := TdmSuscripciones.Create(AOwner);
end;

{ TIWServerController }

procedure TIWServerController.IWServerControllerBaseCreate( Sender: TObject);
begin
     {$ifdef EJECUTABLE}
     Port := 33600;
     {$else}
     Port := 80;
     {$endif}
end;

procedure TIWServerController.IWServerControllerBaseNewSession(ASession: TIWApplication; var VMainForm: TIWAppForm);
begin
     ASession.Data := TUserSession.Create(ASession);
end;

end.
