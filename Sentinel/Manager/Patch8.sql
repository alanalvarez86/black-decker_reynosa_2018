connect "DELL-GERMAN2:D:\Sentinel\sentinel.gdb" USER "SYSDBA" PASSWORD "m";
/*connect "F-SERVER:D:\Datos\Users\Lety\Sentinel\sentinel.gdb" USER "SYSDBA" PASSWORD "m";*/

CREATE DOMAIN INSTALLCODE AS VARCHAR(255) DEFAULT '' NOT NULL;
CREATE DOMAIN COMENTARIO AS VARCHAR(255) DEFAULT '' NOT NULL;

create table INSTALACION(
       SN_NUMERO      NUMSENTINEL,    /* # de Sentinel */
       SI_FOLIO       FOLIOSISTEMA,   /* # de Empresa */
       DI_CODIGO      CODIGO,         /* Distribuidor */
       US_CODIGO      USUARIOCORTO,   /* Usuario */
       IN_FOLIO       FOLIOCHICO,     /* # de Instalacion */
       IN_FECHA       FECHA,          /* Fecha de Generacion */
       IN_CLAVE       INSTALLCODE,    /* Clave Generada */
       IN_COMENTA     COMENTARIO      /* Comentario */
);

alter table INSTALACION add primary key ( SN_NUMERO, SI_FOLIO, IN_FOLIO );

create exception SENTINEL_ASIGNADO_A_SISTEMA 'Sentinel No Se Puede Borrar Porque Est� Asignado A Un Sistema';
create exception USUARIO_AUTORIZACIONES 'Usuario No Puede Ser Borrado Porque Ya Gener� Claves';
create exception USUARIO_CLAVES 'Usuario No Puede Ser Borrado Porque Ya Gener� Claves De Instalaci�n';
create exception DISTRIB_USUARIOS 'Distribuidor No Puede Ser Borrado Porque Tiene Usuarios Asignados';
create exception DISTRIB_AUTORIZACIONES 'Distribuidor No Puede Ser Borrado Porque Tiene Claves Generadas';
create exception DISTRIB_CLAVES 'Distribuidor No Puede Ser Borrado Porque Tiene Claves De Instalaci�n Generadas';

drop trigger TD_SISTEMA;
drop trigger TU_SISTEMA;
drop trigger TD_SENTINEL;
drop trigger TU_SENTINEL;
drop trigger TD_USUARIO;
drop trigger TU_USUARIO;
drop trigger TD_DISTRIBU;
drop trigger TU_DISTRIBU;

set term !! ;

/*  Trigger TD_SISTEMA  */
CREATE TRIGGER TD_SISTEMA FOR SISTEMA AFTER DELETE AS
BEGIN
    delete from AUTORIZA where ( AUTORIZA.SI_FOLIO = OLD.SI_FOLIO );
    delete from INSTALACION where ( INSTALACION.SI_FOLIO = OLD.SI_FOLIO );
END !!

/*  Trigger TU_SISTEMA  */
create trigger TU_SISTEMA FOR SISTEMA AFTER UPDATE AS
BEGIN
  IF (OLD.SI_FOLIO <> NEW.SI_FOLIO) THEN
  BEGIN
       update AUTORIZA set AUTORIZA.SI_FOLIO = NEW.SI_FOLIO
       where ( AUTORIZA.SI_FOLIO = OLD.SI_FOLIO );
       update INSTALACION set INSTALACION.SI_FOLIO = NEW.SI_FOLIO
       where ( INSTALACION.SI_FOLIO = OLD.SI_FOLIO );
  END
END !!

/*  Trigger TD_SENTINEL  */
create trigger TD_SENTINEL FOR SENTINEL AFTER DELETE
AS
  declare variable NUMROWS INTEGER;
BEGIN
     select COUNT(*) from AUTORIZA where
     ( AUTORIZA.SN_NUMERO = OLD.SN_NUMERO ) into :NUMROWS;
     IF ( NUMROWS > 0 ) THEN
     BEGIN
          EXCEPTION SENTINEL_ASIGNADO_A_SISTEMA;
     END
     delete from INSTALACION where ( INSTALACION.SN_NUMERO = OLD.SN_NUMERO );
END !!

/*  Trigger TU_SENTINEL  */
create trigger TU_SENTINEL FOR SENTINEL AFTER UPDATE
AS
BEGIN
     IF (OLD.SN_NUMERO <> NEW.SN_NUMERO) THEN
     BEGIN
          update AUTORIZA set AUTORIZA.SN_NUMERO = NEW.SN_NUMERO
          where ( AUTORIZA.SN_NUMERO = OLD.SN_NUMERO );
          update INSTALACION set INSTALACION.SN_NUMERO = NEW.SN_NUMERO
          where ( INSTALACION.SN_NUMERO = OLD.SN_NUMERO );
     END
END !!

/*  Trigger TD_USUARIO  */
create trigger TD_USUARIO FOR USUARIO AFTER DELETE
AS
  declare variable NUMROWS INTEGER;
BEGIN
     select COUNT(*) from AUTORIZA
     where ( AUTORIZA.US_CODIGO = OLD.US_CODIGO )into :NUMROWS;
     IF ( NUMROWS > 0 ) THEN
     BEGIN
          EXCEPTION USUARIO_AUTORIZACIONES;
     END
     select COUNT(*) from INSTALACION
     where ( INSTALACION.US_CODIGO = OLD.US_CODIGO )into :NUMROWS;
     IF ( NUMROWS > 0 ) THEN
     BEGIN
          EXCEPTION USUARIO_CLAVES;
     END
END !!

/*  Trigger TU_USUARIO  */
create trigger TU_USUARIO FOR USUARIO AFTER UPDATE
AS
BEGIN
  IF (OLD.US_CODIGO <> NEW.US_CODIGO) THEN
  BEGIN
       update AUTORIZA set AUTORIZA.US_CODIGO = NEW.US_CODIGO
       where ( AUTORIZA.US_CODIGO = OLD.US_CODIGO );
       update INSTALACION set INSTALACION.US_CODIGO = NEW.US_CODIGO
       where ( INSTALACION.US_CODIGO = OLD.US_CODIGO );
  END
END !!

/*  Trigger TD_DISTRIBU  */
create trigger TD_DISTRIBU FOR DISTRIBU AFTER DELETE
AS
  declare variable NUMROWS INTEGER;
BEGIN
     select COUNT(*) from USUARIO
     where ( USUARIO.DI_CODIGO = OLD.DI_CODIGO ) into :NUMROWS;
     IF (NUMROWS > 0) THEN
     BEGIN
          EXCEPTION DISTRIB_USUARIOS;
     END
     select COUNT(*) from AUTORIZA
     where ( AUTORIZA.DI_CODIGO = OLD.DI_CODIGO ) into :NUMROWS;
     IF (NUMROWS > 0) THEN
     BEGIN
          EXCEPTION DISTRIB_AUTORIZACIONES;
     END
     select COUNT(*) from INSTALACION
     where ( INSTALACION.DI_CODIGO = OLD.DI_CODIGO ) into :NUMROWS;
     IF (NUMROWS > 0) THEN
     BEGIN
          EXCEPTION DISTRIB_CLAVES;
     END
END !!

/*  Trigger TU_DISTRIBU  */
create trigger TU_DISTRIBU FOR DISTRIBU AFTER UPDATE
AS
BEGIN
     IF (OLD.DI_CODIGO <> NEW.DI_CODIGO) THEN
     BEGIN
          update USUARIO set USUARIO.DI_CODIGO = NEW.DI_CODIGO
          where ( USUARIO.DI_CODIGO = OLD.DI_CODIGO );
          update AUTORIZA set AUTORIZA.DI_CODIGO = NEW.DI_CODIGO
          where ( AUTORIZA.DI_CODIGO = OLD.DI_CODIGO );
          update INSTALACION set INSTALACION.DI_CODIGO = NEW.DI_CODIGO
          where ( INSTALACION.DI_CODIGO = OLD.DI_CODIGO );
     END
END !!

set term ; !!

commit;
