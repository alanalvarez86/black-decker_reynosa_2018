unit FSendCodes;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Psock, NMsmtp, ExtCtrls,
     ComCtrls, IniFiles, Db, DbTables,
     ZetaAsciiFile;
     
type
  TCodeSender = class(TForm)
    Email: TNMSMTP;
    OpenDialog: TOpenDialog;
    StatusBar: TStatusBar;
    ServidorLBL: TLabel;
    Servidor: TEdit;
    UsuarioLBL: TLabel;
    Usuario: TEdit;
    RemitenteLBL: TLabel;
    Remitente: TEdit;
    RemitenteDireccionLBL: TLabel;
    RemitenteDireccion: TEdit;
    ArchivoLBL: TLabel;
    Archivo: TEdit;
    TemaLBL: TLabel;
    Tema: TEdit;
    ArchivoFind: TSpeedButton;
    Salir: TBitBtn;
    EnviarEMails: TBitBtn;
    DoTest: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EmailAuthenticationFailed(var Handled: Boolean);
    procedure EmailConnectionFailed(Sender: TObject);
    procedure EmailConnectionRequired(var Handled: Boolean);
    procedure EmailFailure(Sender: TObject);
    procedure EmailInvalidHost(var Handled: Boolean);
    procedure EmailRecipientNotFound(Recipient: String);
    procedure ArchivoFindClick(Sender: TObject);
    procedure EnviarEMailsClick(Sender: TObject);
  private
    { Private declarations }
    FRecords: Integer;
    FBitacora: TAsciiLog;
    function ConectaHost: Boolean;
    function DesconectaHost: Boolean;
    function GetFromAddress: String;
    function GetFromName: String;
    function GetHost: String;
    function GetUserID: String;
    function IBQuery: TQuery;
    procedure Conectar;
    procedure Desconectar;
    procedure Enviar;
    procedure SendEmail( const sDestinatario: String );
    procedure SetControls;
    procedure ShowStatus(const sMensaje: String);
    procedure MuestraError(const sError: String);
    procedure MuestraExcepcion(const sError: String; Error: Exception);
  public
    { Public declarations }
  end;

var
  CodeSender: TCodeSender;

implementation

uses DEMails,
     ZetaCommonTools,
     ZetaDialogo;

{$R *.DFM}

{ ******** TBulletinSender ************ }

procedure TCodeSender.FormCreate(Sender: TObject);
begin
     FBitacora := TAsciiLog.Create;
end;

procedure TCodeSender.FormShow(Sender: TObject);
var
   iYear, iMes, iDia: Word;
begin
     DecodeDate( Now, iYear, iMes, iDia );
     Tema.Text := Format( 'Bolet�n De %s Del %d', [ ZetaCommonTools.MesConLetraMes( iMes ), iYear ] );
     SetControls;
end;

procedure TCodeSender.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FBitacora );
end;

function TCodeSender.IBQuery: TQuery;
begin
     Result := EmailCFG.tqMailSender;
end;

procedure TCodeSender.MuestraError( const sError: String );
begin
     FBitacora.WriteTexto( sError );
end;

procedure TCodeSender.MuestraExcepcion( const sError: String; Error: Exception );
begin
     FBitacora.WriteException( 0, sError, Error );
end;

procedure TCodeSender.SetControls;
begin
end;

procedure TCodeSender.ShowStatus( const sMensaje: String );
begin
     StatusBar.SimpleText := sMensaje;
end;

function TCodeSender.GetFromAddress: String;
begin
     Result := RemitenteDireccion.Text;
end;

function TCodeSender.GetFromName: String;
begin
     Result := Remitente.Text;
end;

function TCodeSender.GetHost: String;
begin
     Result := Servidor.Text;
end;

function TCodeSender.GetUserID: String;
begin
     Result := Usuario.Text;
end;

function TCodeSender.ConectaHost: Boolean;
begin
     with Email do
     begin
          if not Connected then
          begin
               ClearParameters;
               Host := GetHost;
               UserID := GetUserId;
               PostMessage.FromAddress := GetFromAddress;
               PostMessage.FromName := GetFromName;
               Connect;
          end;
          Result := Connected;
     end;
end;

function TCodeSender.DesconectaHost: Boolean;
begin
     with Email do
     begin
          if Connected then
          begin
               Disconnect;
          end;
          Result := Connected;
     end;
end;

procedure TCodeSender.SendEmail( const sDestinatario: String );
begin
     with Email do
     begin
          with PostMessage do
          begin
               Date := DateToStr( Now );
               Subject := Tema.Text;
               Attachments.Clear;
               ToCarbonCopy.Clear;
               ToBlindCarbonCopy.Clear;
               with ToAddress do
               begin
                    Clear;
                    Add( sDestinatario );
               end;
               with Body do
               begin
                    Clear;
                    LoadFromFile( Archivo.Text );
               end;
          end;
          SendMail;
          Sleep( 500 );
     end;
end;

procedure TCodeSender.EmailAuthenticationFailed(var Handled: Boolean);
begin
     MuestraError( 'eMail Authentication Failed' );
     Handled := True;
end;

procedure TCodeSender.EmailConnectionFailed(Sender: TObject);
begin
     MuestraError( 'eMail Connection Failed' );
end;

procedure TCodeSender.EmailConnectionRequired(var Handled: Boolean);
begin
     MuestraError( 'eMail Connection Required' );
     Handled := True;
end;

procedure TCodeSender.EmailFailure(Sender: TObject);
begin
     MuestraError( 'eMail General Failure' );
end;

procedure TCodeSender.EmailInvalidHost(var Handled: Boolean);
begin
     MuestraError( Format( 'Servidor Inv�lido: %s', [ eMail.Host ] ) );
     Handled := True;
end;

procedure TCodeSender.EmailRecipientNotFound(Recipient: String);
begin
     MuestraError( Format( 'Destinatario No Encontrado: %s', [ Recipient ] ) );
end;

procedure TCodeSender.ArchivoFindClick(Sender: TObject);
begin
     with Archivo do
     begin
          with OpenDialog do
          begin
               InitialDir := ExtractFilePath( Text );
               FileName := ExtractFileName( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TCodeSender.Conectar;
const
     {$ifdef FALSE}
     Q_EMAIL_TEST = 'gandradeus@msn.com';
     {$endif}
     Q_EMAIL_TEST = 'gandrade@tress.com.mx';
     Q_EMAIL_COUNT = 'select COUNT(*) from BOLETIN %s';
     Q_EMAIL_LIST = 'select BL_EMAIL from BOLETIN %s order by BL_EMAIL';
var
   oCursor: TCursor;
   sFiltro: String;
begin
     if DoTest.Checked then
        sFiltro := Format( '( BOLETIN.BL_EMAIL = ''%s'' )', [ Q_EMAIL_TEST ] )
     else
         sFiltro := Format( '( BOLETIN.BL_ACTIVO = %d )', [ K_ALTA ] );
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with IBQuery do
           begin
                Active := False;
                with SQL do
                begin
                     Clear;
                     if ( sFiltro = '' ) then
                        Add( Format( Q_EMAIL_COUNT, [ '' ] ) )
                     else
                         Add( Format( Q_EMAIL_COUNT, [ Format( 'where %s', [ sFiltro ] ) ] ) );
                end;
                Active := True;
                if IsEmpty then
                   FRecords := 0
                else
                    FRecords := Fields[ 0 ].AsInteger;
                Active := False;
                if ( FRecords > 0 ) then
                begin
                     ShowStatus( Format( '%6.0n Direcciones', [ FRecords / 1 ] ) );
                     with SQL do
                     begin
                          Clear;
                          if ( sFiltro = '' ) then
                             Add( Format( Q_EMAIL_LIST, [ '' ] ) )
                          else
                              Add( Format( Q_EMAIL_LIST, [ Format( 'where %s', [ sFiltro ] ) ] ) );
                     end;
                     Active := True;
                end
                else
                    DatabaseError( 'No Hay Direcciones De e-Mail Que Cumplan Con El Filtro Especificado' );
           end;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TCodeSender.Desconectar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with IBQuery do
           begin
                Active := False;
           end;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TCodeSender.Enviar;
var
   oCursor: TCursor;
   i, j: Integer;
   sDireccion: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FBitacora.Init( ChangeFileExt( Application.ExeName, '.log' ) );
        try
           with FBitacora do
           begin
                WriteTexto( '' );
                WriteTimeStamp( StringOfChar( '*', 10 ) + ' %s ' + StringOfChar( '*', 10 ) );
                WriteTexto( '' );
           end;
           try
              Conectar;
              if ConectaHost then
              begin
                   try
                      with IBQuery do
                      begin
                           if Active then
                           begin
                                i := 0;
                                j := 0;
                                while not Eof do
                                begin
                                     Inc( i );
                                     ShowStatus( Format( 'Enviando Mensaje a Direcci�n %d de %d', [ i, FRecords ] ) );
                                     sDireccion := FieldByName( 'BL_EMAIL' ).AsString;
                                     try
                                        SendEMail( sDireccion );
                                        Inc( j );
                                     except
                                           on Error: Exception do
                                           begin
                                                MuestraExcepcion( Format( 'Error Al Enviar e-Mail A %s', [ sDireccion ] ), Error );
                                           end;
                                     end;
                                     Next;
                                end;
                                ShowStatus( Format( 'El Mensaje Fu� Enviado A %d de %d Direcciones ( %5.2n %s )', [ j, FRecords, 100 * j / FRecords, '%' ] ) );
                           end;
                      end;
                   except
                         on Error: Exception do
                         begin
                              Application.HandleException( Error );
                         end;
                   end;
              end
              else
                  MuestraError( Format( 'El Servidor De Email ( %s ) No Se Pudo Conectar', [ GetHost ] ) );
              Desconectar;
           finally
                  DesconectaHost;
           end;
           with FBitacora do
           begin
                WriteTexto( '' );
                WriteTimeStamp( StringOfChar( '*', 10 ) + ' %s ' + StringOfChar( '*', 10 ) );
                WriteTexto( '' );
           end;
        finally
               FBitacora.Close;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TCodeSender.EnviarEMailsClick(Sender: TObject);
begin
     Enviar;
end;

end.
