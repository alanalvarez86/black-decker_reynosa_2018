unit FLecturaSen;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ComCtrls, checklst, ExtCtrls, Buttons,
     ZetaDBTextBox;

type
  TLecturaSen = class(TForm)
    PageControl: TPageControl;
    Generales: TTabSheet;
    ModuloInfo: TTabSheet;
    SerialNumberLBL: TLabel;
    EmpresaLBL: TLabel;
    EmpleadosLBL: TLabel;
    VersionLBL: TLabel;
    VencimientoLBL: TLabel;
    LicenciasLBL: TLabel;
    EsKit: TCheckBox;
    InstalacionesLBL: TLabel;
    SerialNumber: TZetaTextBox;
    Empresa: TZetaTextBox;
    Empleados: TZetaTextBox;
    Version: TZetaTextBox;
    Vencimiento: TZetaTextBox;
    Licencias: TZetaTextBox;
    Instalaciones: TZetaTextBox;
    KitLbl: TLabel;
    Modulos: TGroupBox;
    Autorizados: TCheckListBox;
    Prestamos: TGroupBox;
    Prestados: TCheckListBox;
    Panel2: TPanel;
    Cerrar: TBitBtn;
    Label1: TLabel;
    Temporal: TCheckBox;
    NombreLBL: TLabel;
    Nombre: TZetaTextBox;
    PlataformaLBL: TLabel;
    Plataforma: TZetaTextBox;
    SQLEngineLBL: TLabel;
    SQLEngine: TZetaTextBox;
    StatusBar: TStatusBar;
    IntentosLBL: TLabel;
    IniciaValidacion: TZetaTextBox;
    Label2: TLabel;
    MaxBDConfig: TZetaTextBox;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LecturaSen: TLecturaSen;

implementation

uses FAutoMaster,
     ZetaDialogo,
     ZetaLicenseClasses;

{$R *.DFM}

procedure TLecturaSen.FormShow(Sender: TObject);
begin
     PageControl.ActivePage := Generales;
     with AutoMaster do
     begin
          if Load then
          begin
               Self.SerialNumber.Caption := IntToStr( NumeroSerie );
               Self.Empresa.Caption := IntToStr( Empresa );
               Self.Empleados.Caption := GetEmpleadosStr;
               Self.Version.Caption := Version;

               Self.Temporal.Checked := TieneVencimiento;
               Self.Vencimiento.Caption := GetVencimientoStr;
               Self.VencimientoLBL.Visible := Self.Temporal.Checked;
               Self.Vencimiento.Visible := Self.Temporal.Checked;

               Self.Licencias.Caption := IntToStr( Usuarios );
               Self.EsKit.Checked := EsKit;
               Self.Instalaciones.Caption := IntToStr( Instalaciones );

               //Self.Intentos.Caption := IntToStr( Intentos );
               Self.IniciaValidacion.Caption :=IntToStr( IniciaValidacion  );
               Self.MaxBDConfig.Caption :=IntToStr( GetIntervaloLimiteBDConfig( LimiteConfigBD)  );

               Self.Plataforma.Caption := GetPlataformaStr;
               Self.SQLEngine.Caption := GetSQLEngineStr;
               if not EsKit then
               begin
                    if ( Length( GetCaducidadStr ) > 12 ) then
                       Self.Prestamos.Caption := 'Prestados ' + GetCaducidadStr + ' '
                    else
                        Self.Prestamos.Caption := 'Prestados Hasta El ' + GetCaducidadStr + ' ';
               end;
               GetModulos( Self.Autorizados );
               GetPrestamos( Self.Prestados );
               Self.StatusBar.SimpleText := StatusMsg;
          end
          else
              ZetaDialogo.zError( '� Error Al Leer Sentinel !', StatusMsg, 0 );
     end;
end;

end.
