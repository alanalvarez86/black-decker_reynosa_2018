unit FInstalaciones;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, StdCtrls, DBCtrls,
     ExtCtrls, Mask, ComCtrls, Buttons, Db, DbTables, Grids, DBGrids,
     ZetaDBTextBox,
     ZetaDBGrid;

type
  TFormInstalacion = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    Label2: TLabel;
    Label3: TLabel;
    dsSistema: TDataSource;
    ImprimirBtn: TSpeedButton;
    DBNavigator1: TDBNavigator;
    SI_RAZ_SOC: TZetaDBTextBox;
    SI_FOLIO: TZetaDBTextBox;
    Escribir: TBitBtn;
    SaveDialog: TSaveDialog;
    Enviar: TBitBtn;
    ClavesGB: TGroupBox;
    ZetaDBGrid: TZetaDBGrid;
    dsInstalacion: TDataSource;
    Agregar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ImprimirBtnClick(Sender: TObject);
    procedure EscribirClick(Sender: TObject);
    procedure AgregarClick(Sender: TObject);
    procedure dsInstalacionDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure Conectar;
  public
    { Public declarations }
  end;

var
  FormInstalacion: TFormInstalacion;

implementation

uses ZetaWinAPITools,
     FInstalacionAlta,
     DSentinel,
     DRep;

{$R *.DFM}

{ ********* TFormHistoria ********* }

procedure TFormInstalacion.FormShow(Sender: TObject);
begin
     Conectar;
end;

procedure TFormInstalacion.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     with dmSentinel do
     begin
          ResetDatasource( dsSistema );
          ResetDatasource( dsInstalacion );
     end;
end;

procedure TFormInstalacion.Conectar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmSentinel.AbrirInstalaciones( dsSistema, dsInstalacion );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TFormInstalacion.dsInstalacionDataChange(Sender: TObject; Field: TField);
begin
     ImprimirBtn.Enabled := not dsInstalacion.Dataset.IsEmpty;
     Escribir.Enabled := ImprimirBtn.Enabled;
end;

procedure TFormInstalacion.ImprimirBtnClick(Sender: TObject);
begin
     DRep.InitReportes;
     with dsInstalacion.Dataset do
     begin
          dmRep.ImprimeClaveInstalacion( FieldByName( 'SI_FOLIO' ).AsInteger,
                                         FieldByName( 'SN_NUMERO' ).AsInteger,
                                         FieldByName( 'IN_FOLIO' ).AsInteger )
     end;
end;

procedure TFormInstalacion.EscribirClick(Sender: TObject);
const
     K_FECHA = 'dd/mmm/yyyy';
     K_MODULO = '%25.25s : %s';
var
   sFileName: String;
   oCursor: TCursor;
   lOk: Boolean;
   FDatos: TStrings;
begin
     sFileName := Format( '%sInstalacion.txt', [ ZetaWinAPITools.FormatDirectory( ZetaWinAPITools.GetTempDir ) ] );
     lOk := False;
     with SaveDialog do
     begin
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          if Execute then
          begin
               sFileName := FileName;
               lOk := True;
          end;
     end;
     if lOk then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             FDatos := TStringList.Create;
             try
                with FDatos do
                begin
                     with dsSistema.Dataset do
                     begin
                          Add( '-------------- Clave de Instalación -----------------------' );
                          Add( '' );
                          Add( 'Empresa #:     ' + IntToStr( FieldByName( 'SI_FOLIO' ).AsInteger ) );
                          Add( 'Nombre:        ' + FieldByName( 'SI_RAZ_SOC' ).AsString );
                     end;
                     with dsInstalacion.Dataset do
                     begin
                          Add( 'Sentinel #:    ' + IntToStr( FieldByName( 'SN_NUMERO' ).AsInteger ) );
                          Add( 'Instalación #: ' + IntToStr( FieldByName( 'IN_FOLIO' ).AsInteger ) );
                          Add( 'Fecha:         ' + FormatDateTime( K_FECHA, FieldByName( 'IN_FECHA' ).AsDateTime ) );
                          Add( 'Distribuidor:  ' + FieldByName( 'DI_CODIGO' ).AsString );
                          Add( 'Clave:         ' + FieldByName( 'IN_CLAVE' ).AsString );
                          Add( 'Comentarios:   ' + FieldByName( 'IN_COMENTA' ).AsString );
                     end;
                     SaveToFile( sFileName );
                end;
             finally
                    FDatos.Free;
             end;
             CallNotePad( sFileName );
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TFormInstalacion.AgregarClick(Sender: TObject);
var
   FAlta: TInstalacionAlta;
begin
     FAlta := TInstalacionAlta.Create( Self );
     try
        with FAlta do
        begin
             ShowModal;
             if ( ModalResult = mrOk ) then
             begin
                  Conectar;
             end;
        end;
     finally
            FreeAndNil( FAlta );
     end;
end;

end.
