unit FEditDistribuidor;

interface

uses Windows, Messages, SysUtils, Classes,
     Graphics, Controls, Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Db,
     DBCtrls, Mask;

type
  TEditDistribuidor = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DataSource: TDataSource;
    dsCiudad: TDataSource;
    Panellnferior: TPanel;
    Cerrar: TBitBtn;
    OK: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    function Dataset: TDataset;
  public
    { Public declarations }
    procedure Conectar( const lAgregar: Boolean );
  end;

var
  EditDistribuidor: TEditDistribuidor;

procedure EditarDistribuidor( const lAgregar: Boolean );

implementation

uses DSentinel;

{$R *.DFM}

procedure EditarDistribuidor( const lAgregar: Boolean );
begin
     if ( EditDistribuidor = nil ) then
        EditDistribuidor := TEditDistribuidor.Create( Application );
     with EditDistribuidor do
     begin
          Conectar( lAgregar );
          ShowModal;
     end;
end;

{ *********** TEditDistribuidor ********** }

procedure TEditDistribuidor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     with Dataset do
     begin
          if ( State in [ dsEdit, dsInsert ] ) then
             Cancel;
     end;
     with dmSentinel do
     begin
          ResetDatasource( dsCiudad );
          ResetDatasource( Datasource );
     end;
end;

function TEditDistribuidor.Dataset: TDataset;
begin
     Result := Datasource.Dataset;
end;

procedure TEditDistribuidor.Conectar( const lAgregar: Boolean );
begin
     with dmSentinel do
     begin
          AbrirCiudad( dsCiudad );
          AbrirDistribuidor( Datasource );
     end;
     with Dataset do
     begin
          if lAgregar then
          begin
               if ( State = dsBrowse ) then
                  Append;
          end
          else
              if ( State = dsBrowse ) then
                 Edit;
     end;
end;

procedure TEditDistribuidor.OKClick(Sender: TObject);
begin
     Dataset.Post;
end;

end.
