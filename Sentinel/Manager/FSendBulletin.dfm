object BulletinSender: TBulletinSender
  Left = 435
  Top = 191
  Width = 429
  Height = 349
  Caption = 'Enviar Bolet�n Tress'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object eMailGB: TGroupBox
    Left = 0
    Top = 105
    Width = 421
    Height = 162
    Align = alClient
    Caption = ' e-Mail '
    TabOrder = 0
    object ServidorLBL: TLabel
      Left = 90
      Top = 16
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = '&Servidor:'
      FocusControl = Servidor
    end
    object UsuarioLBL: TLabel
      Left = 93
      Top = 38
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '&Usuario:'
      FocusControl = Usuario
    end
    object RemitenteLBL: TLabel
      Left = 81
      Top = 59
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = '&Remitente:'
      FocusControl = Remitente
    end
    object RemitenteDireccionLBL: TLabel
      Left = 14
      Top = 80
      Width = 118
      Height = 13
      Alignment = taRightJustify
      Caption = 'D&irecci�n Del Remitente:'
      FocusControl = RemitenteDireccion
    end
    object ArchivoLBL: TLabel
      Left = 31
      Top = 101
      Width = 101
      Height = 13
      Alignment = taRightJustify
      Caption = 'Arc&hivo Del Mensaje:'
      FocusControl = Archivo
    end
    object ArchivoFind: TSpeedButton
      Left = 389
      Top = 97
      Width = 23
      Height = 22
      Hint = 'Buscar El Archivo Del Mensaje a Enviar'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = ArchivoFindClick
    end
    object TemaLBL: TLabel
      Left = 40
      Top = 123
      Width = 92
      Height = 13
      Alignment = taRightJustify
      Caption = '&Tema Del Mensaje:'
      FocusControl = Tema
    end
    object Servidor: TEdit
      Left = 134
      Top = 13
      Width = 253
      Height = 21
      TabOrder = 0
      Text = '209.205.207.194'
    end
    object Usuario: TEdit
      Left = 134
      Top = 34
      Width = 253
      Height = 21
      TabOrder = 1
      Text = 'boletintress'
    end
    object Remitente: TEdit
      Left = 134
      Top = 55
      Width = 253
      Height = 21
      TabOrder = 2
      Text = 'Boletin Tress'
    end
    object RemitenteDireccion: TEdit
      Left = 134
      Top = 76
      Width = 253
      Height = 21
      TabOrder = 3
      Text = 'boletintress@tress.com.mx'
    end
    object Archivo: TEdit
      Left = 134
      Top = 97
      Width = 253
      Height = 21
      TabOrder = 4
      Text = 'boletintress@tress.com.mx'
    end
    object Tema: TEdit
      Left = 134
      Top = 119
      Width = 253
      Height = 21
      TabOrder = 5
      Text = 'Bolet�n De Abril Del 2002'
    end
  end
  object DatabaseGB: TGroupBox
    Left = 0
    Top = 0
    Width = 421
    Height = 105
    Align = alTop
    Caption = ' Lista De Direcciones De e-Mail '
    TabOrder = 1
    object DatabaseLBL: TLabel
      Left = 60
      Top = 16
      Width = 73
      Height = 13
      Alignment = taRightJustify
      Caption = '&Base de Datos:'
      FocusControl = Database
    end
    object DBUsuarioLBL: TLabel
      Left = 94
      Top = 38
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '&Usu&ario:'
      FocusControl = DBUsuario
    end
    object DBPasswordLBL: TLabel
      Left = 84
      Top = 59
      Width = 49
      Height = 13
      Alignment = taRightJustify
      Caption = '&Password:'
      FocusControl = DBPassword
    end
    object DBFiltroLBL: TLabel
      Left = 108
      Top = 80
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = '&Filtro:'
      FocusControl = DBFiltro
    end
    object Database: TEdit
      Left = 134
      Top = 13
      Width = 281
      Height = 21
      TabOrder = 0
      Text = 'DELL-GERMAN:D:\Sentinel\sentinel.gdb'
    end
    object DBUsuario: TEdit
      Left = 134
      Top = 34
      Width = 281
      Height = 21
      TabOrder = 1
      Text = 'SYSDBA'
    end
    object DBPassword: TEdit
      Left = 134
      Top = 55
      Width = 281
      Height = 21
      TabOrder = 2
      Text = 'm'
    end
    object DBFiltro: TEdit
      Left = 134
      Top = 76
      Width = 281
      Height = 21
      TabOrder = 3
      Text = '( BL_ACTIVO = 2 )'
    end
  end
  object PanelInferior: TPanel
    Left = 0
    Top = 267
    Width = 421
    Height = 36
    Align = alBottom
    TabOrder = 2
    object Enviar: TBitBtn
      Left = 191
      Top = 5
      Width = 89
      Height = 27
      Hint = 'Enviar e-Mails'
      Caption = '&Enviar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = EnviarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00370777033333
        3330337F3F7F33333F3787070003333707303F737773333373F7007703333330
        700077337F3333373777887007333337007733F773F333337733700070333333
        077037773733333F7F37703707333300080737F373333377737F003333333307
        78087733FFF3337FFF7F33300033330008073F3777F33F777F73073070370733
        078073F7F7FF73F37FF7700070007037007837773777F73377FF007777700730
        70007733FFF77F37377707700077033707307F37773F7FFF7337080777070003
        3330737F3F7F777F333778080707770333333F7F737F3F7F3333080787070003
        33337F73FF737773333307800077033333337337773373333333}
      NumGlyphs = 2
    end
    object Conectar: TBitBtn
      Left = 6
      Top = 6
      Width = 89
      Height = 25
      Hint = 'Conectar A Base De Datos De Lista De Direcciones'
      Caption = 'C&onectar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = ConectarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00550000000005
        555555777777777FF5555500000000805555557777777777FF555550BBBBB008
        05555557F5FFF7777FF55550B000B030805555F7F777F7F777F550000000B033
        005557777777F7F5775550BBBBB00033055557F5FFF777F57F5550B000B08033
        055557F77757F7F57F5550BBBBB08033055557F55557F7F57F5550BBBBB00033
        055557FFFFF777F57F5550000000703305555777777757F57F555550FFF77033
        05555557FFFFF7FF7F55550000000003055555777777777F7F55550777777700
        05555575FF5555777F55555003B3B3B00555555775FF55577FF55555500B3B3B
        005555555775FFFF77F555555570000000555555555777777755}
      NumGlyphs = 2
    end
    object Desconectar: TBitBtn
      Left = 99
      Top = 6
      Width = 89
      Height = 25
      Hint = 'Desconectar A Base De Datos De Lista De Direcciones'
      Caption = 'Desco&nectar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = DesconectarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00550000000005
        555555777777777FF5555500000000805555557777777777FF555550BBBBB008
        05555557F5FFF7777FF55550B000B03080555557F77757F777F55550BBBBB033
        00555557F55557F577555550BBBBB03305555557FFFFF7F57F55555000000033
        05555557777777F57F555550BBBBB03305555557F5FFF7F57F555550B000B033
        05555557F77757F57F555550BBBBB03305555557F55557F57F555550BBBBB033
        05555557FFFFF7FF7F55550000000003055555777777777F7F55550777777700
        05555575FF5555777F5555500B3B3B300555555775FF55577FF555555003B3B3
        005555555775FFFF77F555555570000000555555555777777755}
      NumGlyphs = 2
    end
    object Salir: TBitBtn
      Left = 340
      Top = 5
      Width = 75
      Height = 25
      Hint = 'Salir del Programa'
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Kind = bkClose
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 303
    Width = 421
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object Email: TNMSMTP
    Port = 25
    TimeOut = 5000
    ReportLevel = 16
    OnInvalidHost = EmailInvalidHost
    OnConnectionFailed = EmailConnectionFailed
    OnConnectionRequired = EmailConnectionRequired
    PostMessage.ToAddress.Strings = (
      'cveliz@tress.com.mx')
    PostMessage.Body.Strings = (
      
        '{\rtf1\ansi\ansicpg1252\deff0\deftab720{\fonttbl{\f0\fswiss MS S' +
        'ans Serif;}{\f1\froman\fcharset2 Symbol;}{\f2\fswiss\fprq2 Trebu' +
        'chet MS;}{\f3\froman Times New Roman;}}'
      '{\colortbl\red0\green0\blue0;\red0\green128\blue0;}'
      
        '\deflang1033\horzdoc{\*\fchars }{\*\lchars }\pard\plain\f2\fs20\' +
        'cf1\b N\'#39'famero  Nombre           Puesto  inca4    Ingreso    Ac' +
        'tivo  '
      
        '\par ======= ================ ======= ======== ========== ======' +
        '= '
      '\par '
      '\par 001     AYUDANTE COCINA B          '
      
        '\par     200 Guti\'#39'e9rrez L\'#39'f3pez  001        48.00 27/Sep/00  ' +
        'S       '
      
        '\par   44048 Lopez Perez, Yo  001         0.00 29/Feb/00  S     ' +
        '  '
      '\par '
      '\par 003     OPERADOR B                 '
      
        '\par     254 DURAN LOPEZ, OS  003        48.51 06/Jul/95  N     ' +
        '  '
      '\par '
      '\par 004     OPERADOR C                 '
      
        '\par     231 ESPINO ESPINOSA  004        24.70 03/Jul/95  N     ' +
        '  '
      
        '\par     242 SEGUNDO CORTES,  004        42.18 06/Jul/95  N     ' +
        '  '
      
        '\par     251 CAMACHO RIVERA,  004        42.18 06/Jul/95  N     ' +
        '  '
      
        '\par     252 CONTRERAS SOTO,  004        32.19 06/Jul/95  N     ' +
        '  '
      '\par '
      '\par 005     OPERADOR A                 '
      
        '\par     217 DUNCAN PEREZ, G  005        67.00 03/Jul/95  S     ' +
        '  '
      
        '\par     250 QUIROGA TAMAYO,  005        67.00 06/Jul/95  S     ' +
        '  '
      
        '\par     256 ARJONA BANUET,   005        67.00 06/Jul/95  S     ' +
        '  '
      
        '\par     267 PARTIDA PINEDO,  005        67.00 10/Jul/95  S     ' +
        '  '
      
        '\par     270 PEDRAZA CASTILL  005        67.00 10/Jul/95  S     ' +
        '  '
      '\par '
      '\par 006     TECNICO C                  '
      
        '\par     213 MENDEZ SERRANO,  006        82.00 03/Jul/95  S     ' +
        '  '
      
        '\par    4906 RAMOS ESCOBEDO,  006        79.00 10/Jul/95  S     ' +
        '  '
      '\par '
      '\par 007     TECNICO A                  '
      
        '\par     204 HERNANDEZ GUTIE  007       141.00 23/Nov/00  S     ' +
        '  '
      
        '\par     230 MIJARES NARVAEZ  007       141.00 03/Jul/95  S     ' +
        '  '
      
        '\par     262 JIMENEZ OCHOA,   007       141.00 06/Jul/95  S     ' +
        '  '
      
        '\par     266 RAMIREZ ROMO, O  007       141.00 10/Jul/95  S     ' +
        '  '
      '\par '
      '\par 008     TECNICO B                  '
      
        '\par     279 ACOSTA CLINTON,  008       105.00 10/Jul/95  S     ' +
        '  '
      
        '\par   44046 Montejo BARRIOS  008       105.00 24/Feb/00  N     ' +
        '  '
      '\par '
      '\par 010     TECNICO SR                 '
      
        '\par    3147 DE LA ROSA VILL  010       136.51 15/May/00  S     ' +
        '  '
      '\par '
      '\par 011     LIDER A                    '
      
        '\par     227 CESTO PEREZ, LE  011       141.00 03/Jul/95  S     ' +
        '  '
      
        '\par     253 DIAZ MASCARENO,  011       141.00 19/Oct/95  S     ' +
        '  '
      
        '\par     261 COSTA SORIA, AR  011       141.00 06/Jul/95  S     ' +
        '  '
      '\par '
      '\par 012     LIDER B                    '
      
        '\par     268 MENDOZA ORTIZ,   012       105.00 10/Jul/95  S     ' +
        '  '
      
        '\par     269 RIVERA STANLEY,  012       110.00 10/Jul/95  S     ' +
        '  '
      
        '\par     278 HERNANDEZ BERMU  012       110.00 10/Jul/95  S     ' +
        '  '
      
        '\par     243 QUIROZ CABRERA,  012       110.00 06/Jul/95  N     ' +
        '  '
      '\par '
      '\par 014     LIDER SR                   '
      
        '\par    3132 CARDENAS QUINTA  014       136.51 20/Jul/99  S     ' +
        '  '
      
        '\par    7598 ANGULO DEL BARR  014       181.00 10/Jul/95  S     ' +
        '  '
      
        '\par     205 LOPEZ GARDU\'#39'd1O,   014       181.00 27/Jun/95  N  ' +
        '     '
      '\par '
      '\par 017     OPERADOR MONTACARGAS A     '
      
        '\par     212 ORMAZABAL BOJOR  017        65.34 30/Jun/95  N     ' +
        '  '
      
        '\par     240 PRADO OCEGUERA,  017        79.00 03/Jul/95  N     ' +
        '  '
      '\par '
      '\par 018     OPERADOR MONTACARGAS B     '
      
        '\par     255 GUZMAN GUADIAN,  018        53.82 06/Jul/95  N     ' +
        '  '
      
        '\par     265 ORTEGA ZEPEDA,   018        67.00 10/Jul/95  N     ' +
        '  '
      '\par '
      '\par 047     ASISTENTE OF. BILINGUE A   '
      
        '\par     263 HERNANDEZ MARTI  047       141.00 06/Jul/95  S     ' +
        '  '
      '\par '
      '\par 048     ASISTENTE OF. BILINGUE B   '
      
        '\par     232 LAGUARDIA RAMIR  048        73.60 03/Jul/95  S     ' +
        '  '
      
        '\par    1555 BARON RAMIREZ,   048        73.60 29/Jun/95  N     ' +
        '  '
      '\par '
      '\par 061     INGENIERO A                '
      
        '\par     206 SANCHEZ JIMENEZ  061       245.00 27/Jun/95  N     ' +
        '  '
      
        '\par     211 TORRECILLAS ARE  061       307.80 30/Jun/95  N     ' +
        '  '
      '\par '
      '\par 062     INGENIERO B                '
      
        '\par     208 MEJIA BRAVO, RI  062       274.00 28/Jun/95  N     ' +
        '  '
      
        '\par   44047 Mora Cardenas,   062       100.00 28/Feb/99  N     ' +
        '  '
      '\par '
      '\par }'
      ' ')
    PostMessage.LocalProgram = 'TressBulletinSender'
    EncodeType = uuMime
    ClearParams = False
    SubType = mtHtml
    Charset = 'us-ascii'
    OnRecipientNotFound = EmailRecipientNotFound
    OnFailure = EmailFailure
    OnAuthenticationFailed = EmailAuthenticationFailed
    Left = 8
    Top = 16
  end
  object IBODatabase: TIBODatabase
    Params.Strings = (
      'PASSWORD=m'
      'USER NAME=SYSDBA'
      'SERVER=DELL-GERMAN'
      'PATH=D:\Sentinel\sentinel.gdb')
    AfterConnect = IBODatabaseAfterConnect
    AfterDisconnect = IBODatabaseAfterDisconnect
    Left = 8
    Top = 128
  end
  object IBOQuery: TIBOQuery
    Params = <>
    DatabaseName = 'D:\Sentinel\sentinel.gdb'
    IB_Connection = IBODatabase
    RecordCountAccurate = True
    Left = 8
    Top = 161
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'htm'
    Filter = 
      'Archivos HTM (*.htm)|*.htm|Archivos HTML (*.html)|*.html|Textos ' +
      '( *.txt )|*.txt|Todos ( *.* )|*.*'
    FilterIndex = 0
    Title = 'Seleccione el Archivo del Mensaje'
    Left = 48
    Top = 129
  end
end
