�
 TCUBO 0�"  TPF0TCuboCuboLeft� Top� Width HeightwCaptionGrid de DecisionesFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TDecisionPivotDecisionPivot1Left Top WidthHeight)ButtonAutoSize	DecisionSourceDecisionSource1GroupLayoutxtHorizontalGroupsxtRows	xtColumnsxtSummaries ButtonSpacingButtonWidth@ButtonHeightGroupSpacing
BorderWidthBorderStylebsNoneAlignalTopTabOrder   TPageControlPageControl1Left Top)WidthHeight3
ActivePage	TabSheet1AlignalClientTabOrder 	TTabSheet	TabSheet1CaptionConteo TDecisionGridDecisionGrid1Left Top WidthHeightDefaultColWidthdDefaultRowHeightCaptionColorclActiveCaptionCaptionFont.CharsetDEFAULT_CHARSETCaptionFont.ColorclCaptionTextCaptionFont.Height�CaptionFont.NameMS Sans SerifCaptionFont.Style 	DataColorclInfoBkDataSumColorclNoneDataFont.CharsetDEFAULT_CHARSETDataFont.ColorclWindowTextDataFont.Height�DataFont.NameMS Sans SerifDataFont.Style LabelFont.CharsetDEFAULT_CHARSETLabelFont.ColorclWindowTextLabelFont.Height�LabelFont.NameMS Sans SerifLabelFont.Style 
LabelColor	clBtnFaceLabelSumColorclInactiveCaptionDecisionSourceDecisionSource1
Dimensions	FieldNameGrupoColorclNone	AlignmenttaCenter	Subtotals	 	FieldNameCiudadColorclNone	AlignmenttaCenter	Subtotals	 	FieldName	ET_CODIGOColorclNone	AlignmenttaCenter	Subtotals	 	FieldNameDistribuidorColorclNone	AlignmenttaCenter	Subtotals	 	FieldNameVendedorColorclNone	AlignmenttaCenter	Subtotals	 	FieldName	EmpleadosColorclNone	AlignmenttaCenter	Subtotals	 	FieldNameSUMARHColorclNone	AlignmenttaCenter	Subtotals	 	FieldName
SUMANOMINAColorclNone	AlignmenttaCenter	Subtotals	 	FieldNameSUMAIMSSColorclNone	AlignmenttaCenter	Subtotals	 	FieldNameSUMASISTENCIAColorclNone	AlignmenttaCenter	Subtotals	 	FieldNameCLAVEDEFINIColorclNone	AlignmenttaCenter	Subtotals	 	FieldNameNUMEMPColorclNone	AlignmenttaCenter	Subtotals	 	FieldNameSistemasColorclNone	AlignmenttaCenter	Subtotals	 	FieldNameProm. EmpleadosColorclNone	AlignmenttaCenter	Subtotals	  Totals	ShowCubeEditorAlignalClientColor	clBtnFaceGridLineWidthGridLineColorclWindowTextTabOrder    	TTabSheet	TabSheet2CaptionGr�fica TDecisionGraphDecisionGraph1Left Top WidthHeightDecisionSourceDecisionSource1BottomWall.ColorclWhiteFoot.Font.CharsetDEFAULT_CHARSETFoot.Font.ColorclRedFoot.Font.Height�Foot.Font.NameArialFoot.Font.StylefsItalic LeftWall.Color��� Title.Text.StringsTDecisionGraph AlignalClientTabOrder  
TBarSeriesSeries1ActiveMarks.ArrowLengthMarks.VisibleTitleGrupoSeriesColorclRedXValues.DateTimeXValues.NameXXValues.Multiplier       ��?XValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.Multiplier       ��?YValues.OrderloNone
IdentifierTemplate: GR_CODIGOStyle=  
TBarSeriesSeries8ActiveMarks.ArrowLengthMarks.VisibleTitleCiudadSeriesColorclRedXValues.DateTimeXValues.NameXXValues.Multiplier       ��?XValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.Multiplier       ��?YValues.OrderloNone
IdentifierTemplate: CT_CODIGOStyle=  
TBarSeriesSeries9ActiveMarks.ArrowLengthMarks.VisibleTitleTemplate: ET_CODIGOSeriesColorclRedXValues.DateTimeXValues.NameXXValues.Multiplier       ��?XValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.Multiplier       ��?YValues.OrderloNone
IdentifierTemplate: ET_CODIGOStyle=  
TBarSeriesSeries10ActiveMarks.ArrowLengthMarks.VisibleTitleDistribuidorSeriesColorclRedXValues.DateTimeXValues.NameXXValues.Multiplier       ��?XValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.Multiplier       ��?YValues.OrderloNone
IdentifierTemplate: DI_CODIGOStyle=  
TBarSeriesSeries12ActiveMarks.ArrowLengthMarks.VisibleTitleVendedorSeriesColorclRedXValues.DateTimeXValues.NameXXValues.Multiplier       ��?XValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.Multiplier       ��?YValues.OrderloNone
IdentifierTemplate: VE_CODIGOStyle=  
TBarSeriesSeries4ActiveMarks.ArrowLengthMarks.VisibleTitle	EmpleadosSeriesColorclRedXValues.DateTimeXValues.NameXXValues.Multiplier       ��?XValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.Multiplier       ��?YValues.OrderloNone
IdentifierTemplate: EmpleadosStyle=  
TBarSeriesSeries3ActiveColorEachPoint	Marks.ArrowLengthMarks.VisibleTitleGrupoSeriesColorclRedXValues.DateTimeXValues.NameXXValues.Multiplier       ��?XValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.Multiplier       ��?YValues.OrderloNone
Identifier1D Template: GR_CODIGOStyle(  
TBarSeries ActiveMarks.ArrowLengthMarks.Visible	SeriesColorclGreenXValues.DateTimeXValues.NameXXValues.Multiplier       ��?XValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.Multiplier       ��?YValues.OrderloNone
Identifier::Style     TDecisionCubeDecisionCube1DataSetDecisionQuery1DimensionMap
ActiveFlag
diAsNeeded	FieldTypeftString	FieldnameGRUPOBaseNameACTUAL.GR_CODIGONameGrupoDerivedFrom�DimensionTypedimDimensionBinTypebinNone
ValueCount Active	 
ActiveFlag
diAsNeeded	FieldTypeftString	FieldnameCIUDADBaseNameACTUAL.CT_CODIGONameCiudadDerivedFrom�DimensionTypedimDimensionBinTypebinNone
ValueCount Active	 
ActiveFlag
diAsNeeded	FieldTypeftString	Fieldname	ET_CODIGOBaseNameACTUAL.ET_CODIGONameEstadoDerivedFrom�DimensionTypedimDimensionBinTypebinNone
ValueCount Active	 
ActiveFlag
diAsNeeded	FieldTypeftString	FieldnameDISTRIBUIDORBaseNameACTUAL.DI_CODIGONameDistribuidorDerivedFrom�DimensionTypedimDimensionBinTypebinNone
ValueCount Active	 
ActiveFlag
diAsNeeded	FieldTypeftString	FieldnameVENDEDORBaseNameACTUAL.VE_CODIGONameVendedorDerivedFrom�DimensionTypedimDimensionBinTypebinNone
ValueCount Active	 
ActiveFlag
diAsNeeded	FieldType	ftInteger	FieldnameNUMEROEMPELADOSBaseNameACTUAL.AU_NUM_EMPName	EmpleadosDerivedFrom�DimensionTypedimDimensionBinTypebinNone
ValueCount Active	 
ActiveFlag
diAsNeeded	FieldType	ftInteger	FieldnameSUMARHBaseNameACTUAL.AU_MOD_0NameRecursos HumanosDerivedFrom�DimensionTypedimSumBinTypebinNone
ValueCount�Active	 
ActiveFlag
diAsNeeded	FieldType	ftInteger	Fieldname
SUMANOMINABaseNameACTUAL.AU_MOD_1NameN�minaDerivedFrom�DimensionTypedimSumBinTypebinNone
ValueCount�Active	 
ActiveFlag
diAsNeeded	FieldType	ftInteger	FieldnameSUMAIMSSBaseNameACTUAL.AU_MOD_2NameIMSS/InfonavitDerivedFrom�DimensionTypedimSumBinTypebinNone
ValueCount�Active	 
ActiveFlag
diAsNeeded	FieldType	ftInteger	FieldnameSUMASISTENCIABaseNameACTUAL.AU_MOD_3Name
AsistenciaDerivedFrom�DimensionTypedimSumBinTypebinNone
ValueCount�Active	 
ActiveFlag
diAsNeeded	FieldType	ftInteger	FieldnameCLAVEDEFINIBaseNameACTUAL.AU_DEFINITNameClave DefinitivaDerivedFrom�DimensionTypedimSumBinTypebinNone
ValueCount�Active	 
ActiveFlag
diAsNeeded	FieldType	ftInteger	FieldnameNUMEMPBaseNameACTUAL.AU_NUM_EMPNameN�mero de EmpleadosDerivedFrom�DimensionTypedimSumBinTypebinNone
ValueCount�Active	 
ActiveFlag
diAsNeeded	FieldType	ftInteger	FieldnameCOUNTBaseNameACTUAL.AU_NUM_EMPNameSistemasDerivedFrom�DimensionTypedimCountBinTypebinNone
ValueCount�Active	 
ActiveFlag
diAsNeeded	FieldType	ftUnknown	FieldnameAverage of ACTUAL.AU_NUM_EMPBaseNameACTUAL.AU_NUM_EMPNameProm. EmpleadosDerivedFromDimensionType
dimAverageBinTypebinNone
ValueCount�Active	  ShowProgressDialog	MaxDimensions
MaxSummaries
MaxCells Left� Top  TDecisionQueryDecisionQuery1DatabaseName
dbSentinelSQL.StringsX  SELECT GR_CODIGO as Grupo, CT_CODIGO as Ciudad, ET_CODIGO, DI_CODIGO as Distribuidor, VE_CODIGO as Vendedor, AU_NUM_EMP as NumeroEmpelados, SUM( AU_MOD_0 ) as SumaRH, SUM( AU_MOD_1 ) as SumaNomina, SUM( AU_MOD_2 ) as SumaIMSS, SUM( AU_MOD_3 ) as SumAsistencia, SUM( AU_DEFINIT ) as ClaveDefini, SUM( AU_NUM_EMP ) as NumEmp, COUNT( AU_NUM_EMP ) FROM ACTUALJGROUP BY GR_CODIGO, CT_CODIGO, ET_CODIGO, DI_CODIGO, VE_CODIGO, AU_NUM_EMP Left� Top   TDecisionSourceDecisionSource1DecisionCubeDecisionCube1ControlTypextCheck
SparseRows
SparseColsLeft� Top DimensionCountSummaryCountCurrentSummary 
SparseRows
SparseColsDimensionInfo� � ��   �����    