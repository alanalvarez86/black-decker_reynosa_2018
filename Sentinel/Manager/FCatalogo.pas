unit FCatalogo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ExtCtrls, DBCtrls, StdCtrls, Buttons;

type
  TFormCatalogo = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    dsCatalogo: TDataSource;
    DBNavigator1: TDBNavigator;
    Salir: TBitBtn;
    procedure dsCatalogoStateChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormCatalogo: TFormCatalogo;

implementation

{$R *.DFM}

procedure TFormCatalogo.dsCatalogoStateChange(Sender: TObject);
begin
     with dsCatalogo do
     begin
          if Assigned( Dataset ) then
             Salir.Enabled := ( Dataset.State = dsBrowse )
          else
              Salir.Enabled := True;
     end;
end;

end.
