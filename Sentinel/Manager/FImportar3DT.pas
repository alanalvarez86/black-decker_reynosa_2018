unit FImportar3DT;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, Mask, ComCtrls,
     ZetaFecha, CheckLst, FileCtrl,ZetaServerTools, DatosSistema3dt,
     xmldom, XMLIntf, msxmldom, XMLDoc;

type
  TImportar3DT = class(TForm)
    PanelInferior: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    PanelArchivo: TPanel;
    ArchivoLBL: TLabel;
    Directorio3DT: TEdit;
    ArchivoFind: TSpeedButton;
    AvanceGB: TGroupBox;
    Memo1: TMemo;
    ProgressBar1: TProgressBar;
    dlgImportar: TOpenDialog;
    Comentarios: TEdit;
    Label1: TLabel;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ArchivoFindClick(Sender: TObject);
    procedure SalidaClick(Sender: TObject);
    procedure ModuleListClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetControls;
    procedure ExportarAXML;
    procedure SubirAHistorico;

  public
    { Public declarations }
  end;

var
  Importar3DT: TImportar3DT;

procedure FindFiles(FilesList: TStringList; StartDir, FileMask: string);

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     FAutoClasses,
     FAutoServer,
     FAutoMaster,
     DRep,
     DSentinel;

{$R *.DFM}



procedure TImportar3DT.FormShow(Sender: TObject);
begin
     Directorio3DT.Text := '.';
     ProgressBar1.Step := 0;
     Memo1.Clear;
     SetControls;
end;



procedure TImportar3DT.SetControls;
begin
end;

procedure TImportar3DT.ArchivoFindClick(Sender: TObject);
var
   sDirectory : string;
begin
          with Directorio3DT do
          begin
               sDirectory := Text;
               if SelectDirectory( sDirectory, [ sdAllowCreate ], 0 ) then
                  Text := sDirectory;
          end;

end;

procedure TImportar3DT.SalidaClick(Sender: TObject);
begin
     SetControls;
end;

procedure TImportar3DT.ModuleListClick(Sender: TObject);
begin
     SetControls;
end;

procedure TImportar3DT.OKClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
         ExportarAXML;
         SubirAHistorico;
     finally
            Screen.Cursor := oCursor;
     end;

end;

procedure TImportar3DT.ExportarAXML;
var
   FilesList : TStringList;
   i, iFile : integer;
   archivoSL : TStringList;
   archivoDestino : TStringList;
   sDestino : string;
  lOk : boolean;

begin
  FilesList := TStringList.Create;
  try
    FindFiles(FilesList, Directorio3DT.Text,  '*.3dt');
    //ListBox1.Items.Assign(FilesList);
    AvanceGB.Caption := 'Avance: (*.3dt Encontrados: '+IntToStr(FilesList.Count) +') ';
  finally

  end;

  Memo1.Clear;
  Memo1.Lines.Add( 'Archivos *.3dt Encontrados:  '+IntToStr(FilesList.Count) +' ' );

  ProgressBar1.Min := 0;
  ProgressBar1.Max := FilesList.Count;
  for iFile:=0 to FilesList.Count -1 do
  begin
     archivoSL := TStringList.Create;
     archivoDestino := TStringList.Create;


     sDestino := StrTransform( FilesList[iFile], '.3dt', '');
     sDestino := StrTransform( sDestino, '.', '_');
     sDestino := sDestino + '.xml';
     Memo1.Lines.Add( Format( 'Decodificando %s a %s  ', [FilesList[iFile], sDestino]) );


     archivoSL.LoadFromFile( FilesList[iFile] );
     archivoDestino.Clear;
     lOk := True;
     for i := 0 to archivoSL.Count-1 do
     begin
         try
            if( lOK ) then
              archivoDestino.Add( Decrypt( archivoSL[i]))
            else
                break;
         except
              on Error: Exception do
              begin
                   lOk := False;
                   Memo1.Lines.Add( Format ( 'Error al decodificar %s :  %s', [FilesList[iFile], Error.Message] )  )
              end;
         end;
     end;


     if ( lOk ) then
          archivoDestino.SaveToFile(sDestino);
     ProgressBar1.StepBy(1);
     Application.ProcessMessages;
     FreeAndNil( archivoSL );
     FreeAndNil( archivoDestino );
  end;

  FilesList.Free;
end;

procedure TImportar3DT.SubirAHistorico;
const
  K_PIPE = '|';
var
   FilesList : TStringList;
   iFile : integer;
   dFechaDoc : TDateTime;
 // lOk : boolean;
  docXML : IXMLTressDTType;

function setISOtoDateTime(strDT: string) : TDateTime;
var
  // Delphi settings save vars
  ShortDF, ShortTF : string;
  TS, DS : char;
  // conversion vars
  dd : TDateTime;
begin
   // save Delphi settings
  DS := DateSeparator;
  TS := TimeSeparator;
  ShortDF := ShortDateFormat;
  ShortTF := ShortTimeFormat;

  // set Delphi settings for string to date/time
  DateSeparator := '-';
  ShortDateFormat := 'yyyy-mm-dd';
  TimeSeparator := ':';
  ShortTimeFormat := 'hh:mm:ss';

  // convert test string to datetime
  dd := 0;
  try
    dd := StrToDate( strDT );
  except
    on EConvertError do
      ShowMessage('Error in converting : ' + strDT);
  end;

  // restore Delphi settings
  DateSeparator := DS;
  ShortDateFormat := ShortDF;
  TimeSeparator := TS;
  ShortTimeFormat := ShortTF;

  Result := dd;
end;



begin
  FilesList := TStringList.Create;
  try
    FindFiles(FilesList, Directorio3DT.Text,  '*.xml');
  finally

  end;

  Memo1.Lines.Add( 'Archivos *.xml Encontrados:  '+IntToStr(FilesList.Count) +' ' );

  ProgressBar1.Min := 0;
  ProgressBar1.Max := FilesList.Count;
  for iFile:=0 to FilesList.Count -1 do
  begin
     Memo1.Lines.Add( Format( 'Importando %s a Historico ', [FilesList[iFile]]) );

//     lOk := True;
     try
        docXML := LoadTressDT(FilesList[iFile] );
        dFechaDoc := setISOtoDateTime(docXML.Fecha );
        Memo1.Lines.Add( Format( 'Empresa: %d,  #Sentinel: %d,  Fecha de Documento: %s', [docXML.Sentinel.Empresa, docXML.Sentinel.Numero, docXML.Fecha ] ) );
        //(@am):
        dmSentinel.AgregaHistorico3DT(docXML.Sentinel.Numero, docXML.Sentinel.Empresa,dFechaDoc, docXML.Servidor.Nombre + K_PIPE + Comentarios.Text, FilesList[iFile] );
     except
           on Error: Exception do
           begin
  //              lOk := False;

                Memo1.Lines.Add( Format ( 'Error al Importar a Histórico %s :  %s', [FilesList[iFile], GetExceptionInfo( Error ) ] )  )
           end;
      end;

     ProgressBar1.StepBy(1);
     Application.ProcessMessages;
  end;

  FilesList.Free;
end;


procedure FindFiles(FilesList: TStringList; StartDir, FileMask: string);
var
  SR: TSearchRec;
  DirList: TStringList;
  IsFound: Boolean;
  i: integer;

begin
  if StartDir[length(StartDir)] <> '\' then
    StartDir := StartDir + '\';

  { Build a list of the files in directory StartDir
     (not the directories!)                         }

  IsFound :=
    FindFirst(StartDir+FileMask, faAnyFile-faDirectory, SR) = 0;
  while IsFound do begin
    FilesList.Add(StartDir + SR.Name);
    IsFound := FindNext(SR) = 0;
  end;
  FindClose(SR);

  // Build a list of subdirectories
  DirList := TStringList.Create;
  IsFound := FindFirst(StartDir+'*.*', faAnyFile, SR) = 0;
  while IsFound do begin
    if ((SR.Attr and faDirectory) <> 0) and
         (SR.Name[1] <> '.') then
      DirList.Add(StartDir + SR.Name);
    IsFound := FindNext(SR) = 0;
  end;
  FindClose(SR);

  // Scan the list of subdirectories
  for i := 0 to DirList.Count - 1 do
    FindFiles(FilesList, DirList[i], FileMask);

  DirList.Free;
end;


end.
