unit FVencimientos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, Mask,
     ZetaFecha,
     ZetaNumero, ComCtrls;

type
  TVencimientos = class(TForm)
    FechaLBL: TLabel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    Fecha: TZetaFecha;
    PorLBL: TLabel;
    Dias: TZetaNumero;
    DiasUpDown: TUpDown;
    DiasLBL: TLabel;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Vencimientos: TVencimientos;

implementation

uses ZetaDialogo,
     DRep,
     DSentinel;

{$R *.DFM}

procedure TVencimientos.FormShow(Sender: TObject);
begin
     Fecha.Valor := Date();
     Dias.Valor := 7;
     ActiveControl := Fecha;
end;

procedure TVencimientos.OKClick(Sender: TObject);
var
   dStart, dEnd: TDate;
   oCursor: TCursor;
begin
     dStart := Fecha.Valor;
     dEnd := dStart + ( Dias.ValorEntero - 1 );
     if ( dStart > dEnd ) then
     begin
          ZetaDialogo.zError( 'ˇ Error !', 'Error En Los Parámetros Del Reporte', 0 );
          ActiveControl := Dias;
     end
     else
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if dmRep.ImprimeVencimientos( dStart, dEnd ) then
                ModalResult := mrOk
             else
                 ActiveControl := Dias;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

end.
