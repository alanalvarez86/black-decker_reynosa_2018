unit FRepVence;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, Quickrpt, Qrctrls;

type
  TRepVence = class(TForm)
    qrp1: TQuickRep;
    PageHeaderBand1: TQRBand;
    QRLabel5: TQRLabel;
    QRShape1: TQRShape;
    QRTitulo: TQRLabel;
    QRImage1: TQRImage;
    QRLabel18: TQRLabel;
    QRSysData1: TQRSysData;
    QRShape4: TQRShape;
    QRGroup1: TQRGroup;
    DI_NOMBRElbl: TQRLabel;
    DetailBand1: TQRBand;
    DI_NOMBRE: TQRDBText;
    DI_PERSONAlbl: TQRLabel;
    QRDBText1: TQRDBText;
    DI_EMAILlbl: TQRLabel;
    DI_EMAIL: TQRDBText;
    DI_TEL1lbl: TQRLabel;
    QRDBText2: TQRDBText;
    DI_TEL2: TQRDBText;
    SI_FOLIOlbl: TQRLabel;
    SI_RAZ_SOClbl: TQRLabel;
    AU_FEC_AUTlbl: TQRLabel;
    QRDBText3: TQRDBText;
    SI_RAZ_SOC: TQRDBText;
    AU_FEC_AUT: TQRDBText;
    SI_PERSONAlbl: TQRLabel;
    SI_PERSONA: TQRDBText;
    SI_EMAIL: TQRDBText;
    SI_EMAILlbl: TQRLabel;
    SI_TEL1: TQRDBText;
    SI_TEL1lbl: TQRLabel;
    SI_TEL2: TQRDBText;
    SI_FAXlbl: TQRLabel;
    SI_FAX: TQRDBText;
    QRLabel1: TQRLabel;
    QRDBText4: TQRDBText;
    procedure QRSysData1Print(sender: TObject; var Value: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RepVence: TRepVence;

implementation

uses DRep;

{$R *.DFM}

procedure TRepVence.QRSysData1Print(sender: TObject; var Value: String);
begin
     Value := FormatDateTime( 'dd/mmm/yyyy', Now );
end;

end.
