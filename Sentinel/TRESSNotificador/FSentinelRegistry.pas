unit FSentinelRegistry;

{$define VALIDAEMPLEADOSGLOBAL}

interface

uses Windows, Messages, SysUtils, Classes,
     FAutoServer,
     FAutoClasses,
     ZetaRegistryServer;

type
  TSentinelLog = procedure(const sMensaje: String) of object;
  TSentinelServer = class( TAutoServer )
  private
    { Private declarations }
    procedure SentinelInit;
  public
    { Public declarations }
    function SentinelTest( const sClave1, sClave2: String ): Boolean;
    function SentinelWrite: Boolean;
    procedure SentinelLoad( const iEmpleados, iUsuarios, iModulos: Integer;PlataformaVirt:TPlataforma );
    {$ifdef DEBUGSENTINEL}
    function SentinelDelete: Boolean;
    {$EndIf}

  end;
  TSentinelRegistryServer = class( TZetaRegistryServer )
  private
    { Private declarations }
    function GetSentinelServerDependency: string;
    procedure SetSentinelServerDependency(const Value: string);
    function GetClave1: String;
    function GetClave2: String;
    function GetEmpleados: Integer;
    function GetModulos: Integer;
    function GetPlataforma: TPlataforma;
    function GetUsuarios: Integer;
    function GetArchivoLicencia: String;
    // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
    function GetSentinelAdvertenciaEnvio: String;
    function GetSentinelRestriccionEnvio: String;
    procedure SetClave1(const Value: String);
    procedure SetClave2(const Value: String);
    procedure SetEmpleados(const Value: Integer);
    procedure SetModulos(const Value: Integer);
    procedure SetPlataforma(const Value: TPlataforma);
    procedure SetUsuarios(const Value: Integer);
    procedure SetArchivoLicencia( const Value: String );
    // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
    procedure SetSentinelAdvertenciaEnvio( const Value: String );
    procedure SetSentinelRestriccionEnvio( const Value: String );
  public
    { Public declarations }
    property SentinelServerDependency: string read GetSentinelServerDependency write SetSentinelServerDependency;
    property Usuarios: Integer read GetUsuarios write SetUsuarios;
    property Empleados: Integer read GetEmpleados write SetEmpleados;
    property Modulos: Integer read GetModulos write SetModulos;
    property Plataforma:TPlataforma read GetPlataforma write SetPlataforma;
    property Clave1: String read GetClave1 write SetClave1;
    property Clave2: String read GetClave2 write SetClave2;
    property ArchivoLicencia: String read GetArchivoLicencia write SetArchivoLicencia;
    // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
    property SentinelAdvertenciaEnvio: String read GetSentinelAdvertenciaEnvio write SetSentinelAdvertenciaEnvio;
    property SentinelRestriccionEnvio: String read GetSentinelRestriccionEnvio write SetSentinelRestriccionEnvio;
  end;
  TdmSentinelWrite = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializa( EventLog, ErrorLog: TSentinelLog ): Boolean;
    {$ifdef DEBUGSENTINEL}
    function EliminaClave{( EventLog, ErrorLog: TSentinelLog )}: Boolean; //EventLog Pendiente
    {$EndIf}
  end;


function RenovarAutorizacion( EventLog, ErrorLog: TSentinelLog ): Boolean;
function ExisteSentinelVirtual : Boolean;
function CheckComputerName: Boolean;
function CheckComputerInfo: Boolean;
//(@am): Agregadas para obtener el valor de las variables FServidor, FBIOSId, FOSProductId
function LicServidor: string;
function LicBIOSId: string;
function LicOSProductId: string;
{$ifdef DEBUGSENTINEL}
function BorrarSentinel:Boolean;
{$EndIf}
//Valida llave de Nombre de PC + Renueva Autorizacion
function ValidarSentinelVirtual(EventLog, ErrorLog: TSentinelLog) : Boolean;

procedure ObtenDatosServidor;

var
    FEmpleadosConteoVirtual : integer;
    FDesactivarConteoVirtual, FConteoVirtualPorThread : boolean;

implementation

uses ZetaCommonClasses,
     ZetaLicenseMgr,
     ZetaWinAPITools,
     DZetaServerProvider,
     ZetaServerTools;

const
     SENTINEL_SECTION = 'Sentinel';
     SENTINEL_SERVER_DEPENDENCY = 'SentinelServerDependency';
     SENTINEL_EMPLEADOS = 'SentinelEmpleados';
     SENTINEL_USUARIOS = 'SentinelUsuarios';
     SENTINEL_MODULOS = 'SentinelModulos';
     SENTINEL_PLATAFORMA = 'SentinelPlataforma';
     SENTINEL_CLAVE1 = 'SentinelClave1';
     SENTINEL_CLAVE2 = 'SentinelClave2';
     SENTINEL_ARCHIVOLICENCIA = 'SentinelArchivoLicencia';
     // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
     SENTINEL_ADVERTENCIA_ENVIO = 'SentinelAdvertenciaEnvio';
     SENTINEL_RESTRICCION_ENVIO = 'SentinelRestriccionEnvio';

var
    FServidor: string;
    FBIOSId: string;
    FOSProductId: string;
    FNumSentinel: integer;
    FEmpresa: integer;


procedure ObtenDatosServidor;
const K_PIPE ='|';
var
   oArchivo: TStrings;
   sLinea: string;
   P,I : Integer;
   FRegistry: TSentinelRegistryServer;
procedure LimpiaDatos;
var I : Integer;
begin
    I := 0;
    for I := 0 to 4 do
    begin
        P := Pos( CR ,oArchivo[I]);
        if (P<>0) and (P = Length(oArchivo[I])) then
          oArchivo[I] :=  Copy(oArchivo[I],0,Length(oArchivo[I])-1); //Borra ultimo caracter por ser CR
        P := Pos( LF ,oArchivo[I]);
        if (P<>0) and (P = Length(oArchivo[I])) then
          oArchivo[I] :=  Copy(oArchivo[I],0,Length(oArchivo[I])-1); //Borra ultimo caracter por ser LF
    end;
end;
begin
     oArchivo := TStringList.Create;
     FRegistry := TSentinelRegistryServer.Create( True );
     FServidor := VACIO;
     FBIOSId   := VACIO;
     FOSProductId := VACIO;
     FNumSentinel := 0;
     FEmpresa := 0;
     try
        if FileExists( FRegistry.ArchivoLicencia )then
        begin
             oArchivo.Delimiter := ',';
             oArchivo.LoadFRomFile( FRegistry.ArchivoLicencia );
             if( oArchivo.Count > 0 )then
             begin
                  sLinea := Decrypt( oArchivo.Strings[ 0 ] );
                  I:=0;
                  oArchivo.Clear;
                  while (Length(sLinea) > 0) do
                  begin
                        P := Pos( K_PIPE ,sLinea);
                        if P<>0 then
                        begin
                            oArchivo.Add(Copy(sLinea,1,P-1));
                            sLinea   := Copy(sLinea,P+1,Length(sLinea)-1);
                        end
                        else
                          sLinea:='';
                        I:=I+1;
                  end;

                  if( oArchivo.Count > 4 )then  //@(am): Ahora son 5 datos los de la licencia ( cuenta del 0-4)
                  begin
                       LimpiaDatos;
                       FServidor := oArchivo[0];
                       FBIOSId := oArchivo[1];
                       FOSProductId := oArchivo[2];
                       FNumSentinel := StrToIntDef( oArchivo[3], 0 );
                       FEmpresa := StrToIntDef( oArchivo[4], 0 );
                  end
                  else if(oArchivo.Count > 2) then //@(am): Licencia de 2015 o anteriores (cuenta del 0-2)
                  begin
                       FServidor := oArchivo[0];
                       FNumSentinel := StrToIntDef( oArchivo[1], 0 );
                       FEmpresa := StrToIntDef( oArchivo[2], 0 );
                  end;
             end;
        end;
     finally
            FreeAndNil( FRegistry );
            FreeAndNil(oArchivo);
     end;
end;

function LicServidor: string;
begin
    Result :=  FServidor;
end;
function LicBIOSId: string;
begin
   Result := FBIOSId;
end;
function LicOSProductId: string;
begin
   Result := FOSProductId;
end;

function ExisteSentinelVirtual : boolean;
   var
    oRegistrySentinel: TSentinelRegistryServer;
   begin
       try
          oRegistrySentinel := TSentinelRegistryServer.Create(False);
          Result :=  Length( oRegistrySentinel.ArchivoLicencia ) > 0;  
       finally
           FreeAndNil( oRegistrySentinel );
       end;
   end;

function CheckComputerName: Boolean;
begin
     ObtenDatosServidor;
     Result := ( AnsiUpperCase( ZetaWinAPITools.GetComputerName ) = AnsiUpperCase( FServidor ) );
end;

//@am:
function CheckComputerInfo: Boolean;
   //@av: 2016/08/08
   function AnsiUpperCaseTrim( sValue : string ) : string;
   begin
        Result := AnsiUpperCase( sValue );
        Result := Trim( Result );
   end;
begin
     {$IFDEF DEBUGSENTINEL}
     ObtenDatosServidor;

     Result := ( ( AnsiUpperCaseTrim( ZetaWinAPITools.GetComputerName ) = AnsiUpperCaseTrim( FServidor ))      and
                 ( AnsiUpperCaseTrim( ZetaWinAPITools.GetOSSerial )     = AnsiUpperCaseTrim( FOSProductId ) )  and
                 ( AnsiUpperCaseTrim( ZetaWinAPITools.GetBiosSerial )   = AnsiUpperCaseTrim( FBIOSId )) );
     {$endif}
end;

function RenovarAutorizacion( EventLog, ErrorLog: TSentinelLog ): Boolean;
var
   FWriter: TdmSentinelWrite;
begin
     FWriter := TdmSentinelWrite.Create;
     try
        Result := FWriter.Inicializa( EventLog, ErrorLog );
     finally
            FreeAndNil( FWriter );
     end;
end;

{$ifdef DEBUGSENTINEL}
function BorrarSentinel:Boolean;
var
   FWriter: TdmSentinelWrite;
begin
     FWriter := TdmSentinelWrite.Create;
     try
        Result := FWriter.EliminaClave;
     finally
            FreeAndNil( FWriter );
     end;
end;
{$EndIf}

function ValidarSentinelVirtual(EventLog, ErrorLog: TSentinelLog) : boolean;
var
 oRegistrySentinel: TSentinelRegistryServer;
begin
    Result := False;
    try
       oRegistrySentinel := TSentinelRegistryServer.Create(False);
       if (FSentinelRegistry.CheckComputerInfo) then
          Result := RenovarAutorizacion( EventLog, ErrorLog)
       else
           ErrorLog('No podrá Autorizarse Guardia Virtual, El Servidor No Está Autorizado');
    finally
        FreeAndNil( oRegistrySentinel );
    end;
end;

{ ******** TCafeRegistryServer ********* }

function TSentinelRegistryServer.GetClave1: String;
begin
     Result := Registry.ReadString( SENTINEL_SECTION, SENTINEL_CLAVE1, '' );
end;

function TSentinelRegistryServer.GetClave2: String;
begin
     Result := Registry.ReadString( SENTINEL_SECTION, SENTINEL_CLAVE2, '' );
end;

function TSentinelRegistryServer.GetEmpleados: Integer;
begin
     Result := Registry.ReadInteger( SENTINEL_SECTION, SENTINEL_EMPLEADOS, 0 );
end;

function TSentinelRegistryServer.GetModulos: Integer;
begin
     Result := Registry.ReadInteger( SENTINEL_SECTION, SENTINEL_MODULOS, 0 );
end;

function TSentinelRegistryServer.GetPlataforma: TPlataforma;
begin
     Result := TPlataforma(Registry.ReadInteger( SENTINEL_SECTION, SENTINEL_PLATAFORMA, 0 ));
end;

function TSentinelRegistryServer.GetSentinelServerDependency: string;
const
     DEFAULT_DEPENDENCY = ''; //'MSSQLSERVER';
begin
     Result := Registry.ReadString( SENTINEL_SECTION, SENTINEL_SERVER_DEPENDENCY, DEFAULT_DEPENDENCY );
end;

function TSentinelRegistryServer.GetUsuarios: Integer;
begin
     Result := Registry.ReadInteger( SENTINEL_SECTION, SENTINEL_USUARIOS, 0 );
end;

function TSentinelRegistryServer.GetArchivoLicencia: String;
begin
     Result := Registry.ReadString( SENTINEL_SECTION, SENTINEL_ARCHIVOLICENCIA, '' );
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
function TSentinelRegistryServer.GetSentinelAdvertenciaEnvio: String;
begin
     Result := Registry.ReadString( SENTINEL_SECTION, SENTINEL_ADVERTENCIA_ENVIO, VACIO );
end;

function TSentinelRegistryServer.GetSentinelRestriccionEnvio: String;
begin
     Result := Registry.ReadString( SENTINEL_SECTION, SENTINEL_RESTRICCION_ENVIO, VACIO );
end;

procedure TSentinelRegistryServer.SetClave1(const Value: String);
begin
     Registry.WriteString( SENTINEL_SECTION, SENTINEL_CLAVE1, Value );
end;

procedure TSentinelRegistryServer.SetClave2(const Value: String);
begin
     Registry.WriteString( SENTINEL_SECTION, SENTINEL_CLAVE2, Value );
end;

procedure TSentinelRegistryServer.SetEmpleados(const Value: Integer);
begin
     Registry.WriteInteger( SENTINEL_SECTION, SENTINEL_EMPLEADOS, Value );
end;

procedure TSentinelRegistryServer.SetModulos(const Value: Integer);
begin
     Registry.WriteInteger( SENTINEL_SECTION, SENTINEL_MODULOS, Value );
end;

procedure TSentinelRegistryServer.SetPlataforma(const Value: TPlataforma);
begin
     Registry.WriteInteger( SENTINEL_SECTION, SENTINEL_PLATAFORMA, Ord(Value) );
end;

procedure TSentinelRegistryServer.SetSentinelServerDependency(const Value: string);
begin
     Registry.WriteString( SENTINEL_SECTION, SENTINEL_SERVER_DEPENDENCY, Value );
end;

procedure TSentinelRegistryServer.SetUsuarios(const Value: Integer);
begin
     Registry.WriteInteger( SENTINEL_SECTION, SENTINEL_USUARIOS, Value );
end;

procedure TSentinelRegistryServer.SetArchivoLicencia(const Value: String);
begin
     Registry.WriteString( SENTINEL_SECTION, SENTINEL_ARCHIVOLICENCIA, Value );
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
procedure TSentinelRegistryServer.SetSentinelAdvertenciaEnvio(const Value: String);
begin
    Registry.WriteString( SENTINEL_SECTION, SENTINEL_ADVERTENCIA_ENVIO, Value );
end;

procedure TSentinelRegistryServer.SetSentinelRestriccionEnvio(const Value: String);
begin
     Registry.WriteString( SENTINEL_SECTION, SENTINEL_RESTRICCION_ENVIO, Value );
end;

{ ********* TSentinelServer ********* }

procedure TSentinelServer.SentinelInit;
begin
     FAutoInfo.NumeroSerie := FNumSentinel;
     Empresa := FEmpresa;
     SQLType := engMSSQL;
     SQLEngine := engMSSQL;
     AppType := atCorporativa;
     Plataforma := ptCorporativa;
     Version := FAutoClasses.UnloadVersion( 0 );
end;

procedure TSentinelServer.SentinelLoad(const iEmpleados, iUsuarios, iModulos: Integer;PlataformaVirt:TPlataforma);
begin
     SentinelInit;
     Plataforma := PlataformaVirt;
     Empleados := iEmpleados;
     Usuarios := iUsuarios;
     Modulos := iModulos;
end;

function TSentinelServer.SentinelTest(const sClave1, sClave2: String): Boolean;
begin
     Result := ActualizarTest( sClave1, sClave2, FAutoInfo.NumeroSerie );
end;

function TSentinelServer.SentinelWrite: Boolean;
var
   oZetaProvider: TdmZetaServerProvider;
   oLicenseMgr: TLicenseMgr;
   sTempData: String;

begin
     oZetaProvider := TdmZetaServerProvider.Create( nil );
     try
        oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
{$ifdef VALIDAEMPLEADOSGLOBAL}
        oZetaProvider.EmpresaActiva := oZetaProvider.Comparte;

        oLicenseMgr.EmpleadosConteo := 0;

        if FConteoVirtualPorThread then
        begin
              oLicenseMgr.EmpleadosGetGlobal;
        end
        else
        begin
          {$ifdef TRESSCFG}
           if FDesactivarConteoVirtual   then
           begin
                oLicenseMgr.EmpleadosConteo := FEmpleadosConteoVirtual;
           end
           else
           begin
              if (FEmpleadosConteoVirtual <= 0 )  then
                 FEmpleadosConteoVirtual := oLicenseMgr.EmpleadosGetGlobal
              else
                  oLicenseMgr.EmpleadosConteo := FEmpleadosConteoVirtual;

           end;
         {$else}
           FEmpleadosConteoVirtual := oLicenseMgr.EmpleadosGetGlobal
         {$endif}

        end;


{$endif}
        try
           AutoInfoPrepare( False );
           with FSentinel do
           begin
                CargarDatos( FAutoInfo );
{$ifdef VALIDAEMPLEADOSGLOBAL}
                EmpleadosConteo := oLicenseMgr.EmpleadosConteo;
{$endif}
                sTempData := GetDataString;
           end;
           oLicenseMgr.AutoSetData( Self, sTempData );
           Result := True;
        finally
               FreeAndNil( oLicenseMgr );
        end;
     finally
            FreeAndNil( oZetaProvider );
     end;
end;

{$ifdef DEBUGSENTINEL}
function TSentinelServer.SentinelDelete: Boolean;
var
   oZetaProvider: TdmZetaServerProvider;
   oLicenseMgr: TLicenseMgr;
begin
     Result := False;
     oZetaProvider := TdmZetaServerProvider.Create( nil );
     try
        oLicenseMgr := TLicenseMgr.Create( oZetaProvider );

        try
           oLicenseMgr.DeleteData(Self);
           Result := True;
        finally
               FreeAndNil( oLicenseMgr );
        end;
     finally
            FreeAndNil( oZetaProvider );
     end;
end;
{$endif}

{ ********** TdmSentinelWrite ******** }

function TdmSentinelWrite.Inicializa( EventLog, ErrorLog: TSentinelLog ): Boolean;
var
   oSentinelServer: TSentinelServer;
   FRegistry: TSentinelRegistryServer;
   sClave1, sClave2: String;
begin
     Result := False;
     try
        oSentinelServer := TSentinelServer.Create;
        try
           with oSentinelServer do
           begin
                FRegistry := TSentinelRegistryServer.Create( False );
                try
                   sClave1 := VACIO;
                   sClave2 := VACIO;
                   if FileExists( FRegistry.ArchivoLicencia )then
                   begin
                        SentinelLoad( FRegistry.Empleados, FRegistry.Usuarios, FRegistry.Modulos, FRegistry.Plataforma   );
                        sClave1 := FRegistry.Clave1;
                        sClave2 := FRegistry.Clave2;
                   end
                   else
                       ErrorLog( Format( 'No se encontró archivo licencia de Guardia Virtual %s', [ FormatDateTime( 'dd/MMM/yyyy hh:nn AM/PM', Now ) ] ) );
                finally
                       FreeAndNil( FRegistry );
                end;
                if SentinelTest( sClave1, sClave2 ) then
                begin
                     if SentinelWrite then
                     begin
                          EventLog( Format( 'Autorización Renovada %s', [ FormatDateTime( 'dd/MMM/yyyy hh:nn AM/PM', Now ) ] ) );
                          Result := True;
                     end
                     else
                     begin
                          ErrorLog( Format( 'Autorización NO FUE Renovada %s', [ FormatDateTime( 'dd/MMM/yyyy hh:nn AM/PM', Now ) ] ) );
                     end;
                end
                else
                begin
                     ErrorLog( 'Error En Autorización: ' + StatusMsg );
                end;
           end;
        finally
               FreeAndNil( oSentinelServer );
        end;
     except
           on Error: Exception do
           begin
                ErrorLog( 'Error Al Autorizar: ' + Error.Message );
           end;
     end;
end;

{$ifdef DEBUGSENTINEL}
function TdmSentinelWrite.EliminaClave{( EventLog, ErrorLog: TSentinelLog )}:Boolean;
var
   oSentinelServer: TSentinelServer;
   FRegistry: TSentinelRegistryServer; 
begin
     Result := False;
     oSentinelServer := TSentinelServer.Create;
     try
           with oSentinelServer do
           begin
                FRegistry := TSentinelRegistryServer.Create( False );
                try
                   if not FileExists( FRegistry.ArchivoLicencia )then
                        Result := SentinelDelete
                   else
                       //ErrorLog( Format( 'Error al intentar borrar el archivo licencia de Guardia Virtual %s', [ FormatDateTime( 'dd/MMM/yyyy hh:nn AM/PM', Now ) ] ) )
                       ;
                finally
                       FreeAndNil( FRegistry );
                end;
           end;
     finally
               FreeAndNil( oSentinelServer );
     end;
end;
{$EndIf}
end.
