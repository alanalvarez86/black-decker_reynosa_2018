object SentinelWrite: TSentinelWrite
  Left = 285
  Top = 195
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Administrar Autorizaciones'
  ClientHeight = 271
  ClientWidth = 335
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 335
    Height = 230
    ActivePage = tabSentinel
    Align = alClient
    TabOrder = 0
    object tabSentinel: TTabSheet
      Caption = 'Autorizaci'#243'n'
      object UsuariosLBL: TLabel
        Left = 37
        Top = 24
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = '&Usuarios:'
        Enabled = False
        FocusControl = Usuarios
      end
      object EmpleadosLBL: TLabel
        Left = 26
        Top = 46
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = '&Empleados:'
        Enabled = False
        FocusControl = Empleados
      end
      object ModulosLBL: TLabel
        Left = 30
        Top = 92
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = '&Par'#225'metro:'
        Enabled = False
        FocusControl = Modulos
      end
      object Clave1LBL: TLabel
        Left = 35
        Top = 114
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clave #&1:'
        Enabled = False
        FocusControl = Clave1
      end
      object Clave2LBL: TLabel
        Left = 35
        Top = 137
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clave #&2:'
        Enabled = False
        FocusControl = Clave2
      end
      object lblMensajes: TLabel
        Left = 174
        Top = 157
        Width = 70
        Height = 26
        Alignment = taCenter
        Caption = 'Servidor de Pruebas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        WordWrap = True
      end
      object Label1: TLabel
        Left = 29
        Top = 5
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Servidor:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblNombreServer: TLabel
        Left = 83
        Top = 5
        Width = 5
        Height = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RegistrarAuto: TBitBtn
        Left = 84
        Top = 158
        Width = 86
        Height = 25
        Hint = 'Registrar autorizaci'#243'n'
        Caption = '&Registrar'
        Default = True
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnClick = RegistrarAutoClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object Clave2: TEdit
        Left = 83
        Top = 133
        Width = 149
        Height = 21
        Enabled = False
        TabOrder = 4
      end
      object Clave1: TEdit
        Left = 83
        Top = 110
        Width = 149
        Height = 21
        Enabled = False
        TabOrder = 3
      end
      object Usuarios: TZetaNumero
        Left = 83
        Top = 20
        Width = 83
        Height = 21
        Enabled = False
        Mascara = mnEmpleado
        TabOrder = 0
      end
      object Empleados: TZetaNumero
        Left = 83
        Top = 42
        Width = 83
        Height = 21
        Enabled = False
        Mascara = mnEmpleado
        TabOrder = 1
      end
      object Modulos: TZetaNumero
        Left = 83
        Top = 88
        Width = 83
        Height = 21
        Enabled = False
        Mascara = mnEmpleado
        TabOrder = 2
      end
      object PlataformaVirtual: TComboBox
        Left = 85
        Top = 66
        Width = 150
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 6
      end
    end
    object tabServicio: TTabSheet
      Caption = 'Servicio'
      ImageIndex = 1
      object DependenciaLBL: TLabel
        Left = 3
        Top = 22
        Width = 67
        Height = 13
        Alignment = taRightJustify
        Caption = 'Dependencia:'
        Enabled = False
        FocusControl = Dependencia
      end
      object Dependencia: TEdit
        Left = 73
        Top = 18
        Width = 168
        Height = 21
        Enabled = False
        TabOrder = 0
      end
      object Escribir: TBitBtn
        Left = 78
        Top = 61
        Width = 86
        Height = 25
        Hint = 'Registrar autorizaci'#243'n'
        Caption = '&Escribir'
        Default = True
        Enabled = False
        TabOrder = 1
        OnClick = EscribirClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
    end
    object tabLicencia: TTabSheet
      Caption = 'Licencia'
      ImageIndex = 2
      object Label2: TLabel
        Left = 2
        Top = 22
        Width = 134
        Height = 13
        Caption = 'Licencia de Sentinel Virtual: '
      end
      object btnBuscaArchivo: TSpeedButton
        Left = 304
        Top = 36
        Width = 23
        Height = 22
        Hint = 'Busca Archivo de Licencia'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          777777777777777770000070000077777000E00BFBFB07777777E0BFBF000777
          7700E0FBFBFBF0777700E0BFBF0000007777E0FBFBFBFBFB0799E0BF00000000
          7799000BFB077777777777700077777777007777777777777700777777777777
          7777777777777777700077777777777770007777777777777777}
        ParentShowHint = False
        ShowHint = True
        OnClick = btnBuscaArchivoClick
      end
      object ArchivoLicencia: TZetaEdit
        Left = 1
        Top = 37
        Width = 300
        Height = 21
        Hint = 'Archivo con Licencia de Sentinel Virtual'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnChange = ArchivoLicenciaChange
      end
      object btnEscribirArchivoLicencia: TBitBtn
        Left = 120
        Top = 65
        Width = 86
        Height = 25
        Hint = 'Escribe el Archivo de Licencia especificado'
        Caption = '&Escribir'
        Default = True
        Enabled = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = btnEscribirArchivoLicenciaClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
    end
  end
  object PanelBotones: TPanel
    Left = 0
    Top = 230
    Width = 335
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      335
      41)
    object ReadOnlyLBL: TLabel
      Left = 8
      Top = 8
      Width = 123
      Height = 26
      Alignment = taCenter
      AutoSize = False
      Caption = 'No tiene derechos de administrador'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
      WordWrap = True
    end
    object Cancelar: TBitBtn
      Left = 238
      Top = 8
      Width = 86
      Height = 25
      Hint = 'Salir Sin Grabar Sentinela'
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      TabOrder = 0
      OnClick = CancelarClick
      Kind = bkClose
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = '.lic'
    Filter = 'Archivo de Licencia (*.lic)|*.lic'
    Left = 300
    Top = 24
  end
end
