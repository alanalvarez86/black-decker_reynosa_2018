unit FEditCasDiarias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, StdCtrls, ZetaEdit, Mask, ZetaHora, ExtCtrls, DB,
  DBCtrls, ZetaSmartLists, Buttons, ZetaDBTextBox, 
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013,
   cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditCasDiarias_DevEx = class(TBaseEdicion_DevEx)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    AL_HORA: TZetaDBHora;
    US_CODIGO: TZetaDBTextBox;
    AE_CODIGO: TZetaDBTextBox;
    AL_CASETA: TZetaDBTextBox;
    AE_REGLADES: TZetaDBTextBox;
    AL_COMENT: TDBEdit;
    AL_OK_MAN: TDBCheckBox;
    RGEntradaSalida: TRadioGroup;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure CasetaPuntualClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RGEntradaSalidaClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetEntradaSalida( const lEntrada: Boolean );
  public
    { Public declarations }
  protected
    { Public declarations}
    procedure Connect;override;
  end;

var
  EditCasDiarias_DevEx: TEditCasDiarias_DevEx;

implementation
uses ZetaCommonClasses,
     ZetaCommonLists,
     ZAccesosTress,
     DCafeteria,
     ZetaDialogo,
     dCatalogos,
     DAsistencia,
     DCliente,
     DSistema,
     DBaseSistema,
     ZetaCommonTools;

{$R *.dfm}

procedure TEditCasDiarias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_EMP_CASETA_DIARIAS;
     FirstControl := AL_HORA ;
     IndexDerechos := ZAccesosTress.D_EMP_CASETA_DIARIAS;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;     
end;

procedure TEditCasDiarias_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     RGEntradaSalida.ItemIndex := BoolToInt ( not zStrToBool( dmcafeteria.cdsCasdiarias.FieldByName( 'AL_ENTRADA' ).AsString ));
     AE_REGLADES.Caption := dmCatalogos.cdsAccReglas.GetDescripcion( DataSource.DataSet.FieldByName('AE_CODIGO').AsString );
end;

procedure TEditCasDiarias_DevEx.Connect;
begin
     with dmCafeteria do
     begin
          cdsCasDiarias.Conectar;
          DataSource.DataSet := cdsCasDiarias;
     end;
     with dmSistema do
     begin
          cdsUsuariosLookup.Conectar;
          cdsUsuarios.Conectar;
     end;
     with dmCatalogos do
     begin
          cdsHorarios.Conectar;
          cdsAccReglas.Conectar;
     end;
end;

procedure TEditCasDiarias_DevEx.CasetaPuntualClick(Sender: TObject);
begin
     inherited;
     with dmCafeteria.CdsCasDiarias do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;     
          FieldByName( 'AL_HORA' ).AsString := dmCafeteria.GetHoraEntSalEmp( dmCafeteria.cdsCasDiarias, dmCliente.Empleado, dmCliente.FechaDefault, TBitBtn(Sender).Tag );
     end;
end;

procedure TEditCasDiarias_DevEx.SetEntradaSalida( const lEntrada: Boolean );
begin
      with dmCafeteria.cdsCasDiarias do
      begin
           if not ( State in [ dsEdit, dsInsert ] ) then
              Edit;
           FieldByName( 'AL_ENTRADA' ).AsString := zBoolToStr( lEntrada );
      end;          
end;

procedure TEditCasDiarias_DevEx.RGEntradaSalidaClick(Sender: TObject);
begin
     with dmCafeteria.cdsCasDiarias do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          SetEntradaSalida( ( RGEntradaSalida.ItemIndex = 0 ) );
     end;
     inherited;
end;

end.

