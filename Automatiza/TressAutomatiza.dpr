program TressAutomatiza;
{$APPTYPE CONSOLE}
{$R *.RES}
uses
     MidasLib,
     SysUtils,
     ZetaClientTools,
     DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
     DAutomatiza in 'DAutomatiza.pas' {dmAutomatiza: TDataModule};

const
     K_OPERACION = 'OPERACION';
     K_COD_OPERA = 'T';
     K_COD_FECHA = 'F';
var
     sComando, sOperacion     : String;
begin
     ZetaClientTools.InitDCOM;
     try
          dmAutomatiza := TdmAutomatiza.Create ( nil );
          with dmAutomatiza do
          begin
               sOperacion := ListaParametros.Values [K_OPERACION];
               if ( ParamCount >= 2 ) then //EMPRESA = <> , OPERACION =<>
                	Procesar
               else
               begin
                    sComando := Format ( '%s %s=<X> %s=', [ParamStr ( 0 ), P_EMPRESA, P_OPERACION] );
                    DespliegaAyuda ( sComando );
               end;
          end;
     finally
          FreeAndNil ( dmAutomatiza );
     end;
end.
