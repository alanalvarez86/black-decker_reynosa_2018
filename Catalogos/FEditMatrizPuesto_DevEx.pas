unit FEditMatrizPuesto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, ZetaDBTextBox, StdCtrls, Mask, ZetaNumero, DBCtrls,
  Db, ExtCtrls, FCatEntNivel_DevEx,
  Buttons, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx;

type
  TEditMatrizPuesto_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label2: TLabel;
    ZPuesto: TZetaTextBox;
    Label1: TLabel;
    CU_CODIGO: TZetaDBKeyLookup_DevEx;
    Label3: TLabel;
    EN_DIAS: TZetaDBNumero;
    EN_OPCIONA: TDBCheckBox;
    gbReprogramacion: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    EN_REPROG: TDBCheckBox;
    EN_RE_DIAS: TZetaDBNumero;
    gbProgramacion: TGroupBox;
    gbProgramarA: TDBRadioGroup;
    Label4: TLabel;
    bModifica_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gbProgramarAChange(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure bModifica_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
  protected
    procedure Connect; override;
  public
  end;

var
  EditMatrizPuesto_DevEx: TEditMatrizPuesto_DevEx;

implementation

uses FTressShell,ZetaCommonClasses, dCatalogos, dGlobal, ZBaseDlgModal_DevEx,
     ZAccesosTress, ZGlobalTress, ZetaClientTools;

{$R *.DFM}

{ TEditMatrizPuesto }

procedure TEditMatrizPuesto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAPA_MATRIZ_PUESTO;
     HelpContext:= H60644_Matriz_puesto;
     FirstControl := CU_CODIGO;
     CU_CODIGO.LookupDataset := dmCatalogos.cdsCursos;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditMatrizPuesto_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ZPuesto.Caption := Datasource.DataSet.FieldByName( 'PU_CODIGO' ).AsString + ' = ' + dmCatalogos.cdsPuestos.FieldByName( 'PU_DESCRIP' ).AsString;
     gbProgramacion.Visible := Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) > 0;  
     if gbProgramacion.Visible then
     begin
          gbProgramacion.Caption := 'Programación por ' + Global.GetDescNivelProg(K_NIVEL_PROGRAMACION_CURSOS);
          Height :=  GetScaledHeight( 382 );//+40 Se aumentó la altura debido a los Large Fonts; //DevEx (by am): Se agumentaron 3 px. por la nueva imagen
     end
     else
          Height :=  GetScaledHeight( 275 );//+40 Se aumentó la altura debido a los Large Fonts; //DevEx (by am): Se agumentaron 3 px. por la nueva imagen
end;

procedure TEditMatrizPuesto_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          cdsMatrizCurso.Conectar;
          Datasource.Dataset := cdsMatrizCurso;
     end;
end;

procedure TEditMatrizPuesto_DevEx.gbProgramarAChange(Sender: TObject);
begin
     inherited;
     bModifica_DevEx.Enabled := ( gbProgramarA.Value = 'S' );
end;

procedure TEditMatrizPuesto_DevEx.OK_DevExClick(Sender: TObject);
begin
  if ( gbProgramarA.Value = 'N' ) then
        with dmCatalogos.cdsEntNivel do
        begin
             First;
             while not EOF do
                   Delete;
        end;
     inherited;

end;

procedure TEditMatrizPuesto_DevEx.bModifica_DevExClick(Sender: TObject);
begin
  inherited;
     ShowDlgModal( CatEntNivel_DevEx, TCatEntNivel_DevEx );
end;

procedure TEditMatrizPuesto_DevEx.SetEditarSoloActivos;
begin
     CU_CODIGO.EditarSoloActivos := TRUE;
end;

end.



