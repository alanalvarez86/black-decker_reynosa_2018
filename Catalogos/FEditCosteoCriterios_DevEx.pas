unit FEditCosteoCriterios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, DBCtrls, Buttons,
  StdCtrls, ZetaEdit, Mask, ZetaNumero, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditCosteoCriterios_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    CritCostoNombre: TDBEdit;
    TB_ACTIVO: TDBCheckBox;
    Label2: TLabel;
    CritCostoID: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditCosteoCriterios_DevEx: TEditCosteoCriterios_DevEx;

implementation

{$R *.dfm}

uses
     DCatalogos,
     ZetaCommonClasses,
     ZetaBuscador_DevEx,
     ZAccesosTress;

{ TEditCosteoCriterios }

procedure TEditCosteoCriterios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := CritCostoID;
     HelpContext:= H00003_Edit_Costeo_Criterios;
     IndexDerechos := D_COSTEO_CRITERIOS;
     CritCostoNombre.Text := VACIO;
     CritCostoID.Text := VACIO;
end;

procedure TEditCosteoCriterios_DevEx.Connect;
begin
     DataSource.DataSet:= dmCatalogos.cdsCosteoCriterios;
end;

procedure TEditCosteoCriterios_DevEx.DoLookup;
begin
  inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Criterios de Costeo', 'CritCostoID', dmCatalogos.cdsCosteoCriterios );
end;

end.
