unit FEditCatTiposPoliza_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZetaNumero, Mask, DBCtrls,
  StdCtrls, ZetaEdit, DB, ExtCtrls, Buttons,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  ZetaKeyLookup_DevEx, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TEditCatTiposPoliza_DevEx = class(TBaseEdicion_DevEx)
    DBCodigoLBL: TLabel;
    PT_CODIGO: TZetaDBEdit;
    PT_NOMBRE: TDBEdit;
    PT_INGLES: TDBEdit;
    PT_NUMERO: TZetaDBNumero;
    PT_TEXTO: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    DBInglesLBL: TLabel;
    lblNombre: TLabel;
    FormatoLBL: TLabel;
    PT_REPORTE: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
      procedure DoLookup; override;
  protected
     procedure Connect; override;
  end;

var
  EditCatTiposPoliza_DevEx: TEditCatTiposPoliza_DevEx;

implementation

uses
    DReportes,
    zReportTools,
    dCatalogos,
    ZetaCommonClasses,
    ZetaCommonTools,
    ZAccesosTress,
    ZetaBuscador_DevEx;

{$R *.dfm}

{ TFEditCatTipoPoliza }

procedure TEditCatTiposPoliza_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl  := PT_CODIGO;
     IndexDerechos := D_CAT_NOMINA_POLIZAS;
     HelpContext   := H_Cat_TiposPoliza;
end;

procedure TEditCatTiposPoliza_DevEx.Connect;
begin
     with dmReportes do
     begin
          cdsLookUpReportes.Conectar;
          PT_REPORTE.LookupDataset := cdsLookUpReportes;
     end;

     with dmCatalogos do
     begin
          cdsTiposPoliza.Conectar;
          DataSource.DataSet := cdsTiposPoliza;
     end;

end;

procedure TEditCatTiposPoliza_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Self.Caption, 'PT_CODIGO', dmCatalogos.cdsTiposPoliza );
end;

end.
