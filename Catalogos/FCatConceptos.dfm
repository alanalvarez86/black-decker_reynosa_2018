inherited CatConceptos: TCatConceptos
  Left = 356
  Top = 152
  Caption = 'Conceptos de N'#243'mina'
  ClientHeight = 154
  ClientWidth = 839
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ZetaDBGrid1: TZetaDBGrid [0]
    Left = 0
    Top = 73
    Width = 839
    Height = 81
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CO_NUMERO'
        Title.Caption = 'Concepto'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CO_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 209
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CO_TIPO'
        Title.Caption = 'Tipo'
        Width = 127
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CO_SUB_CTA'
        Title.Caption = 'Subcuenta'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CO_ACTIVO'
        Title.Caption = 'Activo'
        Width = 38
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CO_CALCULA'
        Title.Caption = 'Calcular'
        Width = 43
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CO_USO_NOM'
        Title.Caption = 'Uso de N'#243'mina'
        Width = 85
        Visible = True
      end>
  end
  inherited PanelIdentifica: TPanel
    Width = 839
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 513
    end
  end
  object pnlImportarConceptos: TPanel [2]
    Left = 0
    Top = 19
    Width = 839
    Height = 54
    Align = alTop
    TabOrder = 2
    object btnImportarConceptos: TSpeedButton
      Left = 6
      Top = 4
      Width = 115
      Height = 45
      Hint = 'Importar Conceptos'
      Caption = 'Importar Conceptos'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888800008888888888888888888800008888777777778888888800008800
        00000000788888880000880BFFFBFFF0777777880000880F444444F000000078
        0000880FFBFFFBF0FBFFF0780000880F444444F04444F0780000880BFFFBFFF0
        FFFBF0780000880F444444F04444F0780000880FFBFFFBF0FBFFF0780000880F
        44F000004477F0780000880BFFF0FFF0FF0007780000880F44F0FB00F70A0778
        0000880FFBF0F0FF000A00080000880000000F470AAAAA080000888888880FFB
        000A00080000888888880000770A088800008888888888888800088800008888
        88888888888888880000}
      Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      Spacing = 0
      OnClick = btnImportarConceptosClick
    end
  end
  inherited DataSource: TDataSource
    Left = 144
    Top = 32
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'XML'
    Filter = 'Documentos (*.XML)|*.XML|Todos los archivos (*.*)|*.*'
    Options = [ofHideReadOnly]
    Title = 'Seleccione el Archivo a Importar'
    Left = 302
    Top = 426
  end
end
