unit FEditCatCertificaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, Mask, ZetaNumero, ZetaEdit,
  ComCtrls, DB, ExtCtrls, Buttons, ZBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxDBEdit, dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator,
  cxDBNavigator, cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC;

type
  TEditCatCertificaciones_DevEx = class(TBaseEdicion_DevEx)
    PageControl: TcxPageControl;
    TabGenerales: TcxTabSheet;
    TabSheet2: TcxTabSheet;
    Encabezado: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    CI_NUMERO: TZetaDBNumero;
    CI_RENOVAR: TZetaDBNumero;
    Label5: TLabel;
    CI_RESUMEN: TcxDBMemo;
    CI_NOMBRE: TDBEdit;
    CI_INGLES: TDBEdit;
    CI_TEXTO: TDBEdit;
    CI_CODIGO: TZetaDBEdit;
    CI_DETALLE: TcxDBMemo;
    CI_ACTIVO: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure Connect; override;
    procedure DoLookup; override;
  end;

var
  EditCatCertificaciones_DevEx: TEditCatCertificaciones_DevEx;

implementation

uses dCatalogos,
     dTablas,
     ZetaCommonClasses,
     ZetaBuscador_DevEx,
     ZAccesosTress;

{$R *.dfm}

procedure TEditCatCertificaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_Cat_Certificaciones;
     IndexDerechos := D_CAT_CERTIFICACIONES;
     FirstControl := CI_CODIGO;
     CI_RESUMEN.Properties.MaxLength := K_MAX_VARCHAR;
end;

procedure TEditCatCertificaciones_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage :=  TabGenerales;
     FirstControl := CI_CODIGO;
     CI_CODIGO.Enabled := True;
     CI_DETALLE.Enabled := True;
end;

procedure TEditCatCertificaciones_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCertificaciones.Conectar;
          Datasource.Dataset := cdsCertificaciones;
     end;
end;

procedure TEditCatCertificaciones_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo',Self.Caption,'CI_CODIGO', dmCatalogos.cdsCertificaciones );
end;

end.
