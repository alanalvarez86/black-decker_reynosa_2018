unit FCalendarioRitmo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseCalendarioRitmo_DevEx, ComCtrls, StdCtrls, Spin, ZetaKeyCombo, Buttons,
  ExtCtrls, Grids,
  ZetaCommonClasses, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
   TressMorado2013, Menus, cxTextEdit, cxButtons,
  cxGroupBox;

type
  TCalendarioRitmo_DevEx = class(TBaseCalendarioRitmo_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FRitmo : TRitmo;
    FDescripTurno : String;
  protected
    procedure SetInfoCeldaFecha( const dFecha: TDate; const sDay: String; const iCol, iRow: Integer ); override;
  public
    { Public declarations }
  end;

var
  CalendarioRitmo_DevEx: TCalendarioRitmo_DevEx;

procedure ShowCalendarioRitmo( const sTurno, sDescrip: String; const oDatosRitmo: TDatosRitmo );

implementation

uses ZetaCommonTools;

{$R *.DFM}

procedure ShowCalendarioRitmo( const sTurno, sDescrip: String; const oDatosRitmo: TDatosRitmo );
begin
     CalendarioRitmo_DevEx := TCalendarioRitmo_DevEx.Create( Application );
     try
        with CalendarioRitmo_DevEx do
        begin
             FRitmo := TRitmo.Create( sTurno );
             try
                with oDatosRitmo do
                begin
                     FRitmo.SetPatron( Inicio, Patron, Horario1, Horario2, Horario3 );
                end;
                Fecha := Date;
                FDescripTurno := sDescrip;
                Init;
                ShowModal;
             finally
                FreeAndNil( FRitmo );
             end;
        end;
     finally
         FreeAndNil( CalendarioRitmo_DevEx );
     end;
end;

{ *********** TCalendarioRitmo ************ }

procedure TCalendarioRitmo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     LblNinguno.Visible := FALSE; { No aplica en Calendario Turnos }
     PanelShowNinguno.Visible := FALSE;
     HelpContext:= H66141_Calendario_del_turno_ritmico;
end;

procedure TCalendarioRitmo_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PanelSuperior.Caption := Format( '%s - %s', [ FRitmo.Codigo, FDescripTurno ] );
{
     with dmCatalogos.cdsTurnos do
     begin
          PanelSuperior.Caption := Format( '%s - %s', [ FieldByName( 'TU_CODIGO' ).AsString,
                                                        FieldByName( 'TU_DESCRIP' ).AsString ] );
     end;
}
end;

procedure TCalendarioRitmo_DevEx.SetInfoCeldaFecha(const dFecha: TDate; const sDay: String;
          const iCol, iRow: Integer);
begin
     with StringGrid, FRitmo.GetStatusHorario( dFecha ) do
     begin
          Cells[ iCol, iRow ] := PadL( sDay, LONGITUD_DIA ) + Horario;
          Objects[ iCol, iRow ] := TObject( Ord( Status ) );
     end;
end;

end.
