inherited CatTools_DevEx: TCatTools_DevEx
  Left = 1000
  Top = 567
  Caption = 'Herramientas'
  ClientHeight = 281
  ClientWidth = 567
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 567
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 241
      inherited textoValorActivo2: TLabel
        Width = 235
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 567
    Height = 262
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object TO_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TO_CODIGO'
      end
      object TO_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TO_DESCRIP'
      end
      object TO_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'TO_INGLES'
      end
      object TO_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'TO_NUMERO'
        Width = 66
      end
      object TO_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'TO_TEXTO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
