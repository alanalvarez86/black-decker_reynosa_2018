unit FCatTurnos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  TCatTurnos_DevEx = class(TBaseGridLectura_DevEx)
    TU_CODIGO: TcxGridDBColumn;
    TU_DESCRIP: TcxGridDBColumn;
    TU_DIAS: TcxGridDBColumn;
    TU_JORNADA: TcxGridDBColumn;
    TU_SUB_CTA: TcxGridDBColumn;
    TU_ACTIVO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatTurnos_DevEx: TCatTurnos_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatTurnos }

procedure TCatTurnos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;
     CanLookup := True;
     HelpContext:= H60614_Turnos;
end;

procedure TCatTurnos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsTurnos.Conectar;
          DataSource.DataSet:= cdsTurnos;
     end;
end;

procedure TCatTurnos_DevEx.Refresh;
begin
     dmCatalogos.cdsTurnos.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatTurnos_DevEx.Agregar;
begin
     dmCatalogos.cdsTurnos.Agregar;
end;

procedure TCatTurnos_DevEx.Borrar;
begin
     dmCatalogos.cdsTurnos.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatTurnos_DevEx.Modificar;
begin
     dmCatalogos.cdsTurnos.Modificar;
end;

procedure TCatTurnos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Turnos', 'TU_CODIGO', dmCatalogos.cdsTurnos );
end;

procedure TCatTurnos_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     TU_CODIGO.Options.Grouping:= FALSE;
     TU_DESCRIP.Options.Grouping:= FALSE;
     TU_JORNADA.Options.Grouping:= FALSE;
     TU_SUB_CTA.Options.Grouping:= FALSE;
     TU_ACTIVO.Options.Grouping:= FALSE;
end;

procedure TCatTurnos_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;
     //DevEx (by am):Configuracion Especial para agrupamiento
     ConfigAgrupamiento;
end;

end.
