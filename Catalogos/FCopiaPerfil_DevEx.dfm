inherited CopiaPerfil_DevEx: TCopiaPerfil_DevEx
  Left = 376
  Top = 152
  Caption = 'Copiar Perfil'
  ClientHeight = 181
  ClientWidth = 395
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 145
    Width = 395
    TabOrder = 2
    inherited OK_DevEx: TcxButton
      Left = 231
      ModalResult = 0
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 310
      Cancel = True
    end
  end
  object GroupBox1: TcxGroupBox [1]
    Left = 13
    Top = 75
    Caption = ' Al puesto '
    TabOrder = 1
    Height = 58
    Width = 369
    object Label2: TLabel
      Left = 16
      Top = 24
      Width = 36
      Height = 13
      Caption = 'Puesto:'
    end
    object CB_PUESTO: TZetaKeyLookup_DevEx
      Left = 56
      Top = 20
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
    end
  end
  object GroupBox2: TcxGroupBox [2]
    Left = 13
    Top = 9
    Caption = ' Copiar perfil de  '
    TabOrder = 0
    Height = 57
    Width = 369
    object Label1: TLabel
      Left = 27
      Top = 25
      Width = 26
      Height = 13
      Caption = 'Perfil:'
    end
    object Perfil: TZetaKeyLookup_DevEx
      Left = 56
      Top = 21
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
