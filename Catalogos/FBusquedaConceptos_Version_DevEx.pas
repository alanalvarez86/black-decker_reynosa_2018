unit FBusquedaConceptos_Version_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZetaClientDataSet, DB, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, ZetaBusqueda_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013, cxControls,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxClasses, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TBusquedaConceptos_Version_DevEx = class(TBusqueda_DevEx)
    DBGrid_DevExDBTableViewColumn1: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }

  protected

  public
    { Public declarations }
  end;

var
  BusquedaConceptos_Version_DevEx: TBusquedaConceptos_Version_DevEx;

  function BusquedaConceptoDevEx_ShowSearchForm( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String): Boolean;
implementation

{$R *.dfm}

function BusquedaConceptoDevEx_ShowSearchForm( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String): Boolean;
var
   oBusqueda: TBusquedaConceptos_Version_DevEx;
begin
//   if ( Busqueda = nil ) then
//   Se agreg� un objeto TBusqueda para manejar multiples instancias
//   de la forma de busqueda

     oBusqueda := TBusquedaConceptos_Version_DevEx.Create( Application );
     try
        with oBusqueda do
        begin
             //oBusqueda.Refrescar_DevEx.Visible := true;
             Dataset := LookupDataset;
             Filtro := sFilter;
             Codigo := sKey;
             ShowModal;
             Result := ( ModalResult = mrOk );
             if Result then
             begin
                  sKey := Codigo;
                  sDescription := LookupDataset.GetDescription;
             end;
        end;
     finally
            oBusqueda.Free;
     end;
end;

procedure TBusquedaConceptos_Version_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
//  Refrescar_DevEx.Visible:= true;

end;

procedure TBusquedaConceptos_Version_DevEx.FormShow(Sender: TObject);
begin
  inherited;
//  Refrescar_DevEx.Visible:= true;
end;


end.
