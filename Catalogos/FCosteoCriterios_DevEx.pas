unit FCosteoCriterios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, System.Actions;

type
  TCosteoCriterios_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  CosteoCriterios_DevEx: TCosteoCriterios_DevEx;

implementation

uses
    ZetaCommonClasses,
    dCatalogos;

{$R *.dfm}

{ TCosteoCriterios }

procedure TCosteoCriterios_DevEx.Agregar;
begin
     dmCatalogos.cdsCosteoCriterios.Agregar;
end;

procedure TCosteoCriterios_DevEx.Borrar;
begin
     dmCatalogos.cdsCosteoCriterios.Borrar;
end;

procedure TCosteoCriterios_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCosteoCriterios.Conectar;
          DataSource.DataSet:= cdsCosteoCriterios;
     end;
end;

procedure TCosteoCriterios_DevEx.FormCreate(Sender: TObject);
begin
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;
     HelpContext:= H00003_Costeo_Criterios;
end;

procedure TCosteoCriterios_DevEx.Modificar;
begin
     dmCatalogos.cdsCosteoCriterios.Modificar;
end;

procedure TCosteoCriterios_DevEx.Refresh;
begin
     dmCatalogos.cdsCosteoCriterios.Refrescar;
end;

procedure TCosteoCriterios_DevEx.FormShow(Sender: TObject);
begin
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0],0,'',SkCount );
  inherited;

end;

end.
