unit FCatConceptos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DBGrids, Db, ExtCtrls, Grids,
     ZBaseConsulta,
     ZetaDBGrid, Buttons;
     
function AbreDialogo( OpenDialog: TOpenDialog; sExt: String): Boolean;

type
  TCatConceptos = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    pnlImportarConceptos: TPanel;
    btnImportarConceptos: TSpeedButton;
    OpenDialog: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure btnImportarConceptosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatConceptos: TCatConceptos;

implementation

uses DCatalogos,
     ZetaDialogo,
     ZImprimeForma,
     ZetaTipoEntidad,
     ZetaBuscaEntero,
     ZetaCommonClasses,
     ZetaClientTools,
     ZetaCommonTools,
     ZetaClientDataSet,
     FImportarConceptos,
     ZAccesosMgr,
     ZAccesosTress;

function AbreDialogo( OpenDialog: TOpenDialog; sExt: String ): Boolean;
begin
     with OpenDialog do
     begin
          DefaultExt := sExt;
          if StrVacio( Filter ) then
             Filter := sExt + '|' + '*.' + sExt + '|' + 'Todos' + '|' + '*.*' ;
          FileName := ExtractFileName( ExtractFilePath (Application.Name) + 'Archivo.xml' );
          InitialDir := ExtractFilePath( Application.Name );
          Result := Execute;
     end;
end;


{$R *.DFM}
                
const
     K_CAT_IMPORTAR_CONCEPTOS = ZetaCommonClasses.K_DERECHO_SIST_KARDEX;
{ TCatConceptos }

procedure TCatConceptos.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H60621_Catalogo_conceptos;
end;

procedure TCatConceptos.Connect;
begin
     with dmCatalogos do
     begin
          cdsConceptos.Conectar;
          DataSource.DataSet:= cdsConceptos;
     end;
end;

procedure TCatConceptos.FormShow(Sender: TObject);
begin
     inherited;
     btnImportarConceptos.Enabled := CheckDerecho( D_CAT_NOMINA_CONCEPTOS, K_CAT_IMPORTAR_CONCEPTOS );
end;

procedure TCatConceptos.Refresh;
begin
     dmCatalogos.cdsConceptos.Refrescar;
end;

procedure TCatConceptos.Agregar;
begin
     dmCatalogos.cdsConceptos.Agregar;
end;

procedure TCatConceptos.Borrar;
begin
     dmCatalogos.cdsConceptos.Borrar;
end;

procedure TCatConceptos.Modificar;
begin
     dmCatalogos.cdsConceptos.Modificar;
end;

procedure TCatConceptos.DoLookup;
begin
     inherited;
     ZetaBuscaEntero.BuscarCodigo( 'N�mero', 'Concepto de N�mina', 'CO_NUMERO', dmCatalogos.cdsConceptos );
end;

{
function TCatConceptos.ConceptoValido( var sMensaje : string ) : Boolean;
begin
     Result := DataSource.DataSet.FieldByName('CO_NUMERO').AsInteger < 1000;
     if NOT Result then
        sMensaje := 'Los Conceptos Mayores a 1000 son para uso Exclusivo del Sistema';
end;

function TCatConceptos.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := ConceptoValido(sMensaje);
end;
}

procedure TCatConceptos.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enConcepto, dmCatalogos.cdsConceptos );
end;

procedure TCatConceptos.btnImportarConceptosClick(Sender: TObject);
var
   cdsXML : TZetaClientDataSet;
   sStringPrueba: String;
   impConceptos: TImportarConceptos;
begin
     cdsXML := TZetaClientDataSet.Create(Self);
     with cdsXML do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';
     end;

     try
         if (AbreDialogo( OpenDialog, 'xml' )) then
         begin
              cdsXML.LoadFromFile(OpenDialog.FileName);
              sStringPrueba := cdsXML.FieldByName ('CO_FORMULA').AsString;

              impConceptos := TImportarConceptos.Create(Self);
              impConceptos.mostrarConceptos(cdsXML);
         end;
     except
           ZError(Caption, Format ('Archivo inv�lido (''%s'')' + CR_LF + 'Favor de seleccionar otro archivo', [OpenDialog.FileName]), 0);
     end;
end;

end.
