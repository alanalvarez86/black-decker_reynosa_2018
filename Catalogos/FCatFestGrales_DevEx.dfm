inherited CatFestGrales_DevEx: TCatFestGrales_DevEx
  Left = 437
  Top = 208
  Caption = 'D'#237'as Festivos Generales'
  ClientWidth = 498
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 498
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 172
      inherited textoValorActivo2: TLabel
        Width = 166
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 498
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object FE_MES: TcxGridDBColumn
        Caption = 'Mes'
        DataBinding.FieldName = 'FE_MES'
        Options.Grouping = False
      end
      object FE_DIA: TcxGridDBColumn
        Caption = 'D'#237'a'
        DataBinding.FieldName = 'FE_DIA'
        Options.Grouping = False
      end
      object FE_YEAR: TcxGridDBColumn
        Caption = 'A'#241'o'
        DataBinding.FieldName = 'FE_YEAR'
        Options.Grouping = False
      end
      object FE_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'FE_DESCRIP'
        Options.Grouping = False
      end
      object FE_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'FE_TIPO'
      end
      object FE_CAMBIO: TcxGridDBColumn
        Caption = 'Cambiar Por'
        DataBinding.FieldName = 'FE_CAMBIO'
        Options.Grouping = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
