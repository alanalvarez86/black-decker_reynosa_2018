unit FCatTPeriodos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, StdCtrls,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, System.Actions;

type
  TCatTPeriodos_DevEx = class(TBaseGridLectura_DevEx)
    TP_CLAS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
   { Public declarations }
  end;

var
  CatTPeriodos_DevEx: TCatTPeriodos_DevEx;

implementation

uses DCatalogos,
     ZetaCommonClasses;

{$R *.DFM}

procedure TCatTPeriodos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_TIPOS_D_PERIODO; // act. HC
end;

procedure TCatTPeriodos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsTPeriodos.Conectar;
          DataSource.DataSet:= cdsTPeriodos;
     end;
end;

procedure TCatTPeriodos_DevEx.Refresh;
begin
     dmCatalogos.cdsTPeriodos.Refrescar;
end;

procedure TCatTPeriodos_DevEx.Agregar;
begin
     dmCatalogos.cdsTPeriodos.Agregar;
end;

procedure TCatTPeriodos_DevEx.Borrar;
begin
     dmCatalogos.cdsTPeriodos.Borrar;
end;

procedure TCatTPeriodos_DevEx.Modificar;
begin
     dmCatalogos.cdsTPeriodos.Modificar;
end;

procedure TCatTPeriodos_DevEx.FormShow(Sender: TObject);
begin
    CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0],0,'',SkCount );
    inherited;

end;

end.
