inherited EditCatValNivel_DevEx: TEditCatValNivel_DevEx
  Left = 401
  Top = 250
  Caption = 'Escala de Subfactor'
  ClientHeight = 512
  ClientWidth = 419
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 24
    Top = 60
    Width = 39
    Height = 13
    Caption = 'Plantilla:'
  end
  object Label2: TLabel [1]
    Left = 28
    Top = 108
    Width = 35
    Height = 13
    Caption = 'Escala:'
  end
  object Label3: TLabel [2]
    Left = 27
    Top = 132
    Width = 36
    Height = 13
    Caption = 'C'#243'digo:'
  end
  object Label4: TLabel [3]
    Left = 23
    Top = 156
    Width = 40
    Height = 13
    Caption = 'Nombre:'
  end
  object Label5: TLabel [4]
    Left = 4
    Top = 180
    Width = 59
    Height = 13
    Caption = 'Descripci'#243'n:'
  end
  object Label6: TLabel [5]
    Left = 32
    Top = 276
    Width = 31
    Height = 13
    Caption = 'Ingl'#233's:'
  end
  object Label7: TLabel [6]
    Left = 4
    Top = 300
    Width = 59
    Height = 13
    Caption = 'Descripci'#243'n:'
  end
  object Label8: TLabel [7]
    Left = 23
    Top = 396
    Width = 40
    Height = 13
    Caption = 'N'#250'mero:'
  end
  object Label9: TLabel [8]
    Left = 33
    Top = 423
    Width = 30
    Height = 13
    Caption = 'Texto:'
  end
  object Label10: TLabel [9]
    Left = 36
    Top = 448
    Width = 27
    Height = 13
    Caption = 'Color:'
  end
  object Label11: TLabel [10]
    Left = 14
    Top = 84
    Width = 49
    Height = 13
    Caption = 'Subfactor:'
  end
  object Label12: TLabel [11]
    Left = 240
    Top = 132
    Width = 36
    Height = 13
    Caption = 'Puntos:'
  end
  inherited PanelBotones: TPanel
    Top = 476
    Width = 419
    TabOrder = 16
    inherited OK_DevEx: TcxButton
      Left = 255
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 334
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 419
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 93
      inherited textoValorActivo2: TLabel
        Width = 87
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 20
  end
  object VL_CODIGO: TZetaDBNumero [15]
    Left = 72
    Top = 56
    Width = 65
    Height = 21
    Color = clBtnFace
    Enabled = False
    Mascara = mnEmpleado
    TabOrder = 1
    DataField = 'VL_CODIGO'
    DataSource = DataSource
  end
  object VN_ORDEN: TZetaDBNumero [16]
    Left = 72
    Top = 104
    Width = 65
    Height = 21
    Color = clBtnFace
    Enabled = False
    Mascara = mnEmpleado
    TabOrder = 5
    DataField = 'VN_ORDEN'
    DataSource = DataSource
  end
  object VN_CODIGO: TDBEdit [17]
    Left = 72
    Top = 128
    Width = 129
    Height = 21
    CharCase = ecUpperCase
    DataField = 'VN_CODIGO'
    DataSource = DataSource
    TabOrder = 6
  end
  object VN_NOMBRE: TDBEdit [18]
    Left = 72
    Top = 152
    Width = 329
    Height = 21
    DataField = 'VN_NOMBRE'
    DataSource = DataSource
    TabOrder = 8
  end
  object VN_DESCRIP: TcxDBMemo [19]
    Left = 72
    Top = 176
    DataBinding.DataField = 'VN_DESCRIP'
    DataBinding.DataSource = DataSource
    Properties.ScrollBars = ssVertical
    TabOrder = 9
    Height = 89
    Width = 329
  end
  object VN_INGLES: TDBEdit [20]
    Left = 72
    Top = 272
    Width = 329
    Height = 21
    DataField = 'VN_INGLES'
    DataSource = DataSource
    TabOrder = 10
  end
  object VN_DES_ING: TcxDBMemo [21]
    Left = 72
    Top = 296
    DataBinding.DataField = 'VN_DES_ING'
    DataBinding.DataSource = DataSource
    Properties.ScrollBars = ssVertical
    TabOrder = 11
    Height = 89
    Width = 329
  end
  object VN_TEXTO: TDBEdit [22]
    Left = 72
    Top = 419
    Width = 329
    Height = 21
    DataField = 'VN_TEXTO'
    DataSource = DataSource
    TabOrder = 13
  end
  object VN_NUMERO: TZetaDBNumero [23]
    Left = 72
    Top = 392
    Width = 121
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 12
    Text = '0.00'
    DataField = 'VN_NUMERO'
    DataSource = DataSource
  end
  object VL_NOMBRE: TEdit [24]
    Left = 144
    Top = 56
    Width = 257
    Height = 21
    Color = clBtnFace
    Enabled = False
    TabOrder = 2
    Text = 'VL_NOMBRE'
  end
  object VN_COLOR: TColorBox [25]
    Left = 72
    Top = 444
    Width = 145
    Height = 22
    DefaultColorColor = clWindow
    NoneColorColor = clWindow
    Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor]
    ItemHeight = 16
    TabOrder = 14
    OnChange = VN_COLORChange
  end
  object VS_ORDEN: TZetaDBNumero [26]
    Left = 72
    Top = 80
    Width = 65
    Height = 21
    Color = clBtnFace
    Enabled = False
    Mascara = mnEmpleado
    TabOrder = 3
    DataField = 'VS_ORDEN'
    DataSource = DataSource
  end
  object VS_NOMBRE: TEdit [27]
    Left = 144
    Top = 80
    Width = 257
    Height = 21
    Color = clBtnFace
    Enabled = False
    TabOrder = 4
    Text = 'VS_NOMBRE'
  end
  object VN_PUNTOS: TZetaDBNumero [28]
    Left = 280
    Top = 128
    Width = 121
    Height = 21
    Mascara = mnPesos
    TabOrder = 7
    Text = '0.00'
    DataField = 'VN_PUNTOS'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 388
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
