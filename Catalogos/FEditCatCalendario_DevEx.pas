unit FEditCatCalendario_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, StdCtrls, Mask, ZetaFecha, Db,
  ExtCtrls, Buttons, DBCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TEditCatCalendario_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    CU_CODIGO: TZetaDBKeyLookup_DevEx;
    Label2: TLabel;
    CC_FECHA: TZetaDBFecha;
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Connect; override;
  public
  end;

var
  EditCatCalendario_DevEx: TEditCatCalendario_DevEx;

implementation

uses dCatalogos, ZetaCommonClasses, ZAccesosTress;

{$R *.DFM}

{ NO TENDRA BUSQUEDA POR QUE SE TIENEN VARIOS REGISTROS CON EL MISMO CODIGO }

procedure TEditCatCalendario_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAPA_CALENDARIO;
     FirstControl := CU_CODIGO;
     HelpContext:= H60645_Calendario;
     CU_CODIGO.LookupDataset := dmCatalogos.cdsCursos;
end;

procedure TEditCatCalendario_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          DataSource.DataSet:= cdsCalendario;
     end;
end;

end.
