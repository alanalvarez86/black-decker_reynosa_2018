unit FWizPerfil_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, ZetaNumero, ZetaKeyCombo,
  DBCtrls, Mask, ZetaFecha,
  DB, ZetaDBTextBox, ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, ZetaCXWizard,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, ZetaKeyLookup_DevEx;

type
  TWizPerfil_DevEx = class(TcxBaseWizard)
    PU_CODIGO: TZetaDBKeyLookup_DevEx;
    Label1: TLabel;
    Label2: TLabel;
    PF_FECHA: TZetaDBFecha;
    GroupBox1: TcxGroupBox;
    Label3: TLabel;
    PF_CONTRL1: TDBEdit;
    PF_CONTRL2: TDBEdit;
    Label4: TLabel;
    Descripcion: TdxWizardControlPage;
    Label5: TLabel;
    PU_CODIGO_2: TZetaDBTextBox;
    PU_DESCRIP: TZetaDBTextBox;
    PF_DESCRIP: TDBMemo;
    Objetivos: TdxWizardControlPage;
    Label6: TLabel;
    PU_CODIGO_3: TZetaDBTextBox;
    PU_DESCRIP_3: TZetaDBTextBox;
    PF_OBJETIV: TDBMemo;
    Publicacion: TdxWizardControlPage;
    Label7: TLabel;
    PU_CODIGO_4: TZetaDBTextBox;
    PU_DESCRIP_4: TZetaDBTextBox;
    PF_POSTING: TDBMemo;
    Requisitos: TdxWizardControlPage;
    Panel1: TPanel;
    PU_DESCRIP_5: TZetaDBTextBox;
    PU_CODIGO_5: TZetaDBTextBox;
    Label8: TLabel;
    GroupBox5: TcxGroupBox;
    Label10: TLabel;
    PF_ESTUDIO: TZetaDBKeyCombo;
    PF_EXP_PTO: TZetaDBNumero;
    PF_EDADMIN: TZetaDBNumero;
    Label13: TLabel;
    PF_EDADMAX: TZetaDBNumero;
    Label12: TLabel;
    Label11: TLabel;
    GroupBox6: TcxGroupBox;
    GroupBox7: TcxGroupBox;
    PF_SEXO: TDBRadioGroup;
    PF_TEXTO1_LBL: TLabel;
    PF_TEXTO1: TDBEdit;
    PF_TEXTO2_LBL: TLabel;
    PF_TEXTO2: TDBEdit;
    Label9: TLabel;
    Label14: TLabel;
    PF_NUMERO1: TZetaDBNumero;
    PF_NUMERO2: TZetaDBNumero;
    DataSource: TDataSource;
    anios: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
              var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure setEditarSoloActivos;

  private
    { Private declarations }
  protected
    { Protected}
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizPerfil_DevEx: TWizPerfil_DevEx;

implementation
uses
    ZetaCommonClasses,
    ZetaDialogo,
    dCatalogos,
    ZetaCommonTools,
    ZetaCommonLists,
    DCliente;

{$R *.dfm}

{ TWizContPerfilNuevo }

procedure TWizPerfil_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := PU_CODIGO;
     HelpContext := H_CAT_PERFIL_NUEVO;
     with dmCatalogos do
     begin
          DataSource.DataSet := dmCatalogos.cdsPerfiles;
          PU_CODIGO.LookupDataset := cdsPuestos;
     end;
      //DevEx
     Parametros.PageIndex := 0;
     Descripcion.PageIndex := 1;
     Objetivos.PageIndex := 2;
     Publicacion.PageIndex := 3;
     Requisitos.PageIndex := 4;
     Ejecucion.PageIndex := 5;
     //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;

end;

function TWizPerfil_DevEx.EjecutarWizard: Boolean;
begin
     with dmCatalogos.cdsPerfiles do
     begin
          Enviar;
          Result := ( ChangeCount = 0 );
     end;
end;

procedure TWizPerfil_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     self.ActiveControl := nil;
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if StrVacio( PU_CODIGO.Llave ) then
                  CanMove := Error( 'Falta especificar el puesto', PU_CODIGO );
               if CanMove and ( not dmCatalogos.PuedeCrearPerfil( PU_CODIGO.Llave ) ) then
                  CanMove := Error( 'El puesto seleccionado ya tiene asignado un perfil', PU_CODIGO );
          end;
     end;
end;

procedure TWizPerfil_DevEx.WizardAfterMove(Sender: TObject);
var
   oControl: TWinControl;
   lEnfocar: Boolean;
begin
     inherited;
    oControl := PU_CODIGO;
    //if PageControl.ActivePage = Descripcion then
    if WizardControl.ActivePage = Descripcion then  //DevEx
         oControl := PF_DESCRIP
    else if WizardControl.ActivePage = Objetivos then
         oControl := PF_OBJETIV
    else if WizardControl.ActivePage = Publicacion then
         oControl := PF_POSTING
    else if WizardControl.ActivePage = Requisitos then
         oControl := PF_ESTUDIO
    else
        self.ActiveControl := nil;    //DevEx

     with oControl do
     begin
          lEnfocar := Visible and Enabled and CanFocus;
     end;
     if lEnfocar then
        self.ActiveControl := oControl;
end;

//DevEx
procedure TWizPerfil_DevEx.CargaParametros;

begin
     inherited;
     with Descripciones do
     begin
           AddString( 'Puesto', PU_CODIGO.Llave + ': ' + PU_CODIGO.Descripcion );
           AddString( 'Fecha' , PF_FECHA.Text);
           AddString( 'C�digos de control (1)' , PF_CONTRL1.Text );
           AddString( 'C�digos de control (2)', PF_CONTRL2.Text );
           AddString( 'Descripci�n general', PF_DESCRIP.Text );
           AddString( 'Objetivos del puesto', PF_OBJETIV.Text);
           AddString( 'Publicaci�n', PF_POSTING.Text);
           AddString( 'Escolaridad' , PF_ESTUDIO.Text);
           AddString( 'A�os de experiencia', Trim(PF_EXP_PTO.ValorAsText));
           AddString( 'Edad entre', Trim(PF_EDADMIN.ValorAsText) + ' y ' + Trim(PF_EDADMAX.ValorAsText));
           AddString( 'Sexo', PF_SEXO.Value );
           AddString( 'Textos adicionales (1)', PF_TEXTO1.Text);
           AddString( 'Textos adicionales (2)', PF_TEXTO2.Text);
           AddString( 'N�meros adicionales (1)',  Trim(PF_NUMERO1.ValorAsText));
           AddString( 'N�meros adicionales (2)',  Trim(PF_NUMERO2.ValorAsText));
     end;
end;


procedure TWizPerfil_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Al aplicar este proceso se crear� el perfil con las indicaciones se�aladas.';
end;

procedure TWizPerfil_DevEx.SetEditarSoloActivos;
begin
     PU_CODIGO.EditarSoloActivos := TRUE;
end;

end.
