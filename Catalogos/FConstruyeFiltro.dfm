object ConstruyeFiltro: TConstruyeFiltro
  Left = 397
  Top = 293
  Width = 509
  Height = 300
  Caption = 'Constructor de Filtros y Condiciones'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox2: TGroupBox
    Left = 0
    Top = 0
    Width = 501
    Height = 234
    Align = alClient
    TabOrder = 0
    object ZetaSmartListsButton3: TZetaSmartListsButton
      Left = 213
      Top = 19
      Width = 25
      Height = 25
      Enabled = False
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
        3333333333090333333333333309033333333333330903333333333333090333
        3333333333090333333333300009000033333330999999903333333309999903
        3333333309999903333333333099903333333333309990333333333333090333
        3333333333090333333333333330333333333333333033333333}
      Tipo = bsSubir
      SmartLists = SmartListFiltro
    end
    object ZetaSmartListsButton4: TZetaSmartListsButton
      Left = 213
      Top = 44
      Width = 25
      Height = 25
      Enabled = False
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
        3333333333303333333333333309033333333333330903333333333330999033
        3333333330999033333333330999990333333333099999033333333099999990
        3333333000090000333333333309033333333333330903333333333333090333
        3333333333090333333333333309033333333333330003333333}
      Tipo = bsBajar
      SmartLists = SmartListFiltro
    end
    object GBAbierto: TGroupBox
      Left = 246
      Top = 43
      Width = 233
      Height = 129
      TabOrder = 5
      object Label4: TLabel
        Left = 12
        Top = 12
        Width = 25
        Height = 13
        Caption = 'Filtro:'
      end
      object EAbiertoFiltro: TMemo
        Left = 12
        Top = 28
        Width = 209
        Height = 89
        Lines.Strings = (
          'EAbiertoFiltro')
        MaxLength = 255
        ScrollBars = ssVertical
        TabOrder = 0
        OnExit = EAbiertoFiltroExit
      end
    end
    object GBCampoFecha: TGroupBox
      Left = 246
      Top = 43
      Width = 181
      Height = 81
      Caption = 'GBCampoFecha'
      TabOrder = 3
      object Label1: TLabel
        Left = 12
        Top = 22
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicial:'
      end
      object Label3: TLabel
        Left = 17
        Top = 48
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Final:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object EFechaInicial: TZetaFecha
        Left = 48
        Top = 17
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '15/Dec/97'
        Valor = 35779.000000000000000000
        OnExit = EFechaInicialExit
      end
      object EFechaFinal: TZetaFecha
        Left = 48
        Top = 43
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '09/Dec/97'
        Valor = 35773.000000000000000000
        OnExit = EFechaInicialExit
      end
    end
    object GBRangoLista: TGroupBox
      Left = 246
      Top = 43
      Width = 207
      Height = 186
      TabOrder = 7
      object RBActivoRangoValores: TRadioButton
        Left = 11
        Top = 29
        Width = 113
        Height = 17
        Caption = 'A&ctivo'
        TabOrder = 1
        OnClick = RBActivoRangoValoresClick
      end
      object RBTodosRangoValores: TRadioButton
        Left = 11
        Top = 13
        Width = 113
        Height = 17
        Caption = '&Todos'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = RBTodosRangoValoresClick
      end
      object RBListaRangoValores: TRadioButton
        Left = 11
        Top = 45
        Width = 134
        Height = 17
        Caption = 'S&elecci'#243'n'
        TabOrder = 2
        OnClick = RBListaRangoValoresClick
      end
      object CLBSeleccionValores: TCheckListBox
        Left = 11
        Top = 61
        Width = 185
        Height = 113
        ItemHeight = 13
        TabOrder = 3
        OnExit = CLBSeleccionValoresExit
      end
    end
    object GBRangoEntidad: TGroupBox
      Left = 246
      Top = 43
      Width = 233
      Height = 186
      TabOrder = 6
      object lbInicialRangoEntidad: TLabel
        Left = 27
        Top = 71
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicial:'
      end
      object lbFinalRangoEntidad: TLabel
        Left = 32
        Top = 99
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Final:'
      end
      object bFinalRangoEntidad: TSpeedButton
        Left = 198
        Top = 93
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33033333333333333F7F3333333333333000333333333333F777333333333333
          000333333333333F777333333333333000333333333333F77733333333333300
          033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
          33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
          3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
          33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
          333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
          333333773FF77333333333370007333333333333777333333333}
        NumGlyphs = 2
        OnClick = bFinalRangoEntidadClick
      end
      object bInicialRangoEntidad: TSpeedButton
        Left = 198
        Top = 65
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33033333333333333F7F3333333333333000333333333333F777333333333333
          000333333333333F777333333333333000333333333333F77733333333333300
          033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
          33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
          3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
          33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
          333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
          333333773FF77333333333370007333333333333777333333333}
        NumGlyphs = 2
        OnClick = bInicialRangoEntidadClick
      end
      object bListaRangoEntidad: TSpeedButton
        Left = 198
        Top = 136
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33033333333333333F7F3333333333333000333333333333F777333333333333
          000333333333333F777333333333333000333333333333F77733333333333300
          033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
          33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
          3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
          33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
          333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
          333333773FF77333333333370007333333333333777333333333}
        NumGlyphs = 2
        OnClick = bListaRangoEntidadClick
      end
      object RBTodosRangoEntidad: TRadioButton
        Left = 8
        Top = 16
        Width = 113
        Height = 17
        Caption = '&Todos'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = RBTodosRangoEntidadClick
      end
      object RBActivoRangoEntidad: TRadioButton
        Left = 8
        Top = 32
        Width = 57
        Height = 17
        Caption = 'A&ctivo'
        TabOrder = 1
        OnClick = RBActivoRangoEntidadClick
      end
      object RBRangoRangoEntidad: TRadioButton
        Left = 8
        Top = 48
        Width = 57
        Height = 17
        Caption = '&Rango'
        TabOrder = 2
        OnClick = RBRangoRangoEntidadClick
      end
      object RBListaRangoEntidad: TRadioButton
        Left = 8
        Top = 120
        Width = 73
        Height = 17
        Caption = '&Lista'
        TabOrder = 3
        OnClick = RBListaRangoEntidadClick
      end
      object EInicialRangoEntidad: TZetaEdit
        Left = 62
        Top = 67
        Width = 131
        Height = 21
        LookUpBtn = bInicialRangoEntidad
        TabOrder = 4
        OnExit = RBRangoRangoEntidadClick
      end
      object EFinalRangoEntidad: TZetaEdit
        Left = 62
        Top = 95
        Width = 131
        Height = 21
        LookUpBtn = bFinalRangoEntidad
        TabOrder = 5
        OnExit = RBRangoRangoEntidadClick
      end
      object EListaRangoEntidad: TZetaEdit
        Left = 24
        Top = 138
        Width = 169
        Height = 21
        LookUpBtn = bListaRangoEntidad
        TabOrder = 6
        OnExit = EListaRangoEntidadExit
      end
    end
    object RGBooleano: TGroupBox
      Left = 246
      Top = 43
      Width = 233
      Height = 94
      TabOrder = 4
      object RGSeleccionValores: TRadioGroup
        Left = 16
        Top = 14
        Width = 169
        Height = 72
        Ctl3D = True
        ItemIndex = 0
        Items.Strings = (
          'Si'
          'No'
          'Todos')
        ParentCtl3D = False
        TabOrder = 0
        OnClick = RGSeleccionValoresClick
      end
    end
    object GroupBox3: TGroupBox
      Left = 10
      Top = 14
      Width = 199
      Height = 186
      Caption = 'Filtrar Por'
      TabOrder = 0
      object LbFiltro: TZetaSmartListBox
        Left = 5
        Top = 20
        Width = 188
        Height = 160
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object BAgregaFiltro: TBitBtn
      Left = 10
      Top = 204
      Width = 100
      Height = 25
      Caption = '&Agrega Filtro'
      TabOrder = 1
      OnClick = BAgregaFiltroClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
        333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
        0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
        0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
        33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
        B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
        3BB33773333773333773B333333B3333333B7333333733333337}
      NumGlyphs = 2
    end
    object bBorraFiltro: TBitBtn
      Left = 110
      Top = 204
      Width = 100
      Height = 25
      Caption = '&Borra Filtro'
      Enabled = False
      TabOrder = 2
      OnClick = bBorraFiltroClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
        305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
        005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
        B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
        B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
        B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
        B0557777FF577777F7F500000E055550805577777F7555575755500000555555
        05555777775555557F5555000555555505555577755555557555}
      NumGlyphs = 2
    end
    object EFormulaFiltro: TEdit
      Left = 246
      Top = 19
      Width = 207
      Height = 21
      Enabled = False
      TabOrder = 8
    end
  end
  object PanelInferior: TPanel
    Left = 0
    Top = 234
    Width = 501
    Height = 32
    Align = alBottom
    TabOrder = 1
    object OK: TBitBtn
      Left = 332
      Top = 3
      Width = 75
      Height = 25
      Caption = '&OK'
      ModalResult = 1
      TabOrder = 0
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object Cancelar: TBitBtn
      Left = 415
      Top = 3
      Width = 75
      Height = 25
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object SmartListFiltro: TZetaSmartLists
    BorrarAlCopiar = False
    Control = LbFiltro
    CopiarObjetos = False
    ListaDisponibles = LbFiltro
    ListaEscogidos = LbFiltro
    AlSeleccionar = SmartListFiltroAlSeleccionar
    Left = 464
  end
end
