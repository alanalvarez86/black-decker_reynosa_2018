unit FPercepcionesFijas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons, cxControls,
  ZetaSmartLists_DevEx, cxContainer, cxEdit, cxListBox;

type
  TPercepcionesFijas_DevEx = class(TZetaDlgModal_DevEx)
    ZetaSmartListsButton4: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton3: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton2: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton1: TZetaSmartListsButton_DevEx;
    SBMas_DevEx: TcxButton;
    LBSeleccion: TZetaSmartListBox_DevEx;
    LBDisponibles: TZetaSmartListBox_DevEx;
    Label22: TLabel;
    Label20: TLabel;
    ZetaSmartLists: TZetaSmartLists_DevEx;
    procedure ZetaSmartListsCambios(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZetaSmartListsAlSeleccionar(Sender: TObject; var Objeto: TObject; Texto: String);
    //procedure OKClick(Sender: TObject);
    //procedure SBMasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure SBMas_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure LlenaPercep_Fija;
  public
    { Public declarations }
  end;

var
  PercepcionesFijas_DevEx: TPercepcionesFijas_DevEx;

implementation

uses ZetaCommonTools,
     ZetaDialogo,
     dCatalogos,
     dRecursos,
     FDatosPer_DevEx;

{$R *.DFM}

{ TPercepcionesFijas }

procedure TPercepcionesFijas_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmRecursos.LlenaOtrasPer( LBDisponibles.Items );
     LlenaPercep_fija;
     with dmCatalogos.cdsPuestos do
     begin
          self.Caption := Format( 'Asignar Percepciones Fijas al Puesto: %s = %s', [ FieldByName( 'PU_CODIGO' ).AsString,
                                  FieldByName( 'PU_DESCRIP' ).AsString ] );
     end;
     Ok_DevEx.Enabled := FALSE;
end;

procedure TPercepcionesFijas_DevEx.LlenaPercep_Fija;
var
   iPos: Integer;
begin
    LimpiaLista( LBSeleccion.Items );
     with LbDisponibles.Items do
     begin
          BeginUpdate;
          try
             with dmCatalogos.cdsPercepFijas do
             begin
                  First;
                  while NOT eof do
                  begin
                       iPos := IndexOfName( FieldByName( 'PF_CODIGO' ).AsString );
                       if ( iPos >= 0 ) then
                       begin
                            LBDisponibles.ItemIndex := iPos;
                            ZetaSmartLists.Escoger;   // Pasar� el Objeto a LbSeleccionados
                       end;
                       Next;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
     LbDisponibles.ItemIndex := 0;
end;

{procedure TPercepcionesFijas_DevEx.OKClick(Sender: TObject);
var
   i: Integer;
begin
     with dmCatalogos.cdsPercepFijas do
     begin
          Open;
          EmptyDataSet;
          if LbSeleccion.Items.Count > 0 then
          begin
               for i:= 0 to LbSeleccion.Items.Count -1 do
               begin
                    Append;
                    FieldByName('PU_CODIGO').AsString := dmCatalogos.cdsPuestos.FieldByName('PU_CODIGO').AsString;
                    FieldByName('PF_FOLIO').AsInteger := ( i + 1 );
                    FieldByName('PF_CODIGO').AsString := TOtrasPer(LbSeleccion.Items.Objects[i]).Codigo;
               end;
          end;
          Enviar;
     end;
     inherited;
end;

procedure TPercepcionesFijas_DevEx.SBMasClick(Sender: TObject);
var
   iIndex : Integer;
   oOtrasPer : TOtrasPer;
   DatosPer : TDatosPer;
begin
     inherited;
     with LBSeleccion do
     begin
          iIndex := ItemIndex;
          if ( iIndex >= 0  ) then
          begin
               if ( Items.Objects[iIndex] <> Nil ) then
               begin
                    DatosPer := TDatosPer.Create( self );
                    try
                       with DatosPer do
                       begin
                            oOtrasPer := TOtrasPer( Items.Objects[ iIndex ] );
                            ZCodigo.Caption :=  Items[ iIndex ];
                            ZMonto.Valor    := oOtrasPer.Monto;
                            ZGravado.Valor  := oOtrasPer.Gravado;
                            ZCriterio.Valor := Ord(oOtrasPer.Imss);
                            ShowModal;
                       end;
                    finally
                           FreeAndNil( DatosPer );
                    end;
               end
               else
                   ZInformation(Caption,' La Parte Gravada no ha sido Calculada, al Guardar el Registro se Calcular�',0);
          end;
     end;
end;}

procedure TPercepcionesFijas_DevEx.ZetaSmartListsCambios(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     Ok_DevEx.Enabled := TRUE;
end;

procedure TPercepcionesFijas_DevEx.ZetaSmartListsAlSeleccionar(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SBMas_DevEx.Enabled := ( LBSeleccion.Items.Count > 0 );
end;

procedure TPercepcionesFijas_DevEx.OK_DevExClick(Sender: TObject);
var
   i: Integer;
begin
    with dmCatalogos.cdsPercepFijas do
     begin
          Open;
          EmptyDataSet;
          if LbSeleccion.Items.Count > 0 then
          begin
               for i:= 0 to LbSeleccion.Items.Count -1 do
               begin
                    Append;
                    FieldByName('PU_CODIGO').AsString := dmCatalogos.cdsPuestos.FieldByName('PU_CODIGO').AsString;
                    FieldByName('PF_FOLIO').AsInteger := ( i + 1 );
                    FieldByName('PF_CODIGO').AsString := TOtrasPer(LbSeleccion.Items.Objects[i]).Codigo;
               end;
          end;
          Enviar;
     end;
     inherited;
end;

procedure TPercepcionesFijas_DevEx.SBMas_DevExClick(Sender: TObject);
var
   iIndex : Integer;
   oOtrasPer : TOtrasPer;
   DatosPer_DevEx : TDatosPer_DevEx;
begin
     inherited;
     with LBSeleccion do
     begin
          iIndex := ItemIndex;
          if ( iIndex >= 0  ) then
          begin
               if ( Items.Objects[iIndex] <> Nil ) then
               begin
                    DatosPer_DevEx := TDatosPer_DevEx.Create( self ); //DevEx (by am): Se creo la nueva forma y su invoccion se cambio aqui.
                    try
                       with DatosPer_DevEx do
                       begin
                            oOtrasPer := TOtrasPer( Items.Objects[ iIndex ] );
                            ZCodigo.Caption :=  Items[ iIndex ];
                            ZMonto.Valor    := oOtrasPer.Monto;
                            ZGravado.Valor  := oOtrasPer.Gravado;
                            ZCriterio.Valor := Ord(oOtrasPer.Imss);
                            ShowModal;
                       end;
                    finally
                           FreeAndNil( DatosPer_DevEx );
                    end;
               end
               else
                   ZInformation(Caption,' La Parte Gravada no ha sido Calculada, al Guardar el Registro se Calcular�',0);
          end;
     end;
end;

end.
