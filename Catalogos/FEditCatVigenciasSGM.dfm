inherited EditCatVigenciasSGM: TEditCatVigenciasSGM
  Left = 559
  Top = 201
  Caption = 'Vigencia'
  ClientHeight = 452
  ClientWidth = 448
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 36
    Top = 387
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [1]
    Left = 27
    Top = 364
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label8: TLabel [2]
    Left = 18
    Top = 340
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Costo Fijo:'
  end
  inherited PanelBotones: TPanel
    Top = 416
    Width = 448
    inherited OK: TBitBtn
      Left = 280
    end
    inherited Cancelar: TBitBtn
      Left = 365
    end
  end
  inherited PanelSuperior: TPanel
    Width = 448
  end
  inherited PanelIdentifica: TPanel
    Width = 448
    inherited ValorActivo2: TPanel
      Width = 122
    end
  end
  object PV_TEXTO: TDBEdit [6]
    Left = 72
    Top = 383
    Width = 313
    Height = 21
    DataField = 'PV_TEXTO'
    DataSource = DataSource
    TabOrder = 9
  end
  object Panel1: TPanel [7]
    Left = 0
    Top = 51
    Width = 448
    Height = 54
    Align = alTop
    TabOrder = 3
    object Label3: TLabel
      Left = 18
      Top = 10
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = 'No. P'#243'liza:'
    end
    object Label4: TLabel
      Left = 12
      Top = 29
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descrpci'#243'n:'
    end
    object Label5: TLabel
      Left = 194
      Top = 10
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object zTxtNoPoliza: TZetaTextBox
      Left = 72
      Top = 8
      Width = 121
      Height = 17
      AutoSize = False
      Caption = 'zTxtNoPoliza'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object zTxtDescripcion: TZetaTextBox
      Left = 72
      Top = 28
      Width = 361
      Height = 17
      AutoSize = False
      Caption = 'zTxtDescripcion'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object zTxtTipoDescrip: TZetaTextBox
      Left = 221
      Top = 8
      Width = 211
      Height = 17
      AutoSize = False
      Caption = 'zTxtTipoDescrip'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  object Panel2: TPanel [8]
    Left = 0
    Top = 105
    Width = 448
    Height = 36
    Align = alTop
    TabOrder = 4
    object DBCodigoLBL: TLabel
      Left = 14
      Top = 12
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Referencia:'
    end
    object PV_REFEREN: TZetaDBEdit
      Left = 72
      Top = 8
      Width = 113
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      Text = 'PV_REFEREN'
      ConfirmEdit = True
      DataField = 'PV_REFEREN'
      DataSource = DataSource
    end
  end
  object GroupBox1: TGroupBox [9]
    Left = 0
    Top = 141
    Width = 448
    Height = 77
    Align = alTop
    Caption = ' Vigencia de la P'#243'liza '
    TabOrder = 5
    object Label6: TLabel
      Left = 52
      Top = 24
      Width = 17
      Height = 13
      Alignment = taRightJustify
      Caption = 'De:'
    end
    object Label7: TLabel
      Left = 57
      Top = 48
      Width = 12
      Height = 13
      Alignment = taRightJustify
      Caption = 'Al:'
    end
    object PV_FEC_INI: TZetaDBFecha
      Left = 72
      Top = 21
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '21/Mar/12'
      Valor = 40989.000000000000000000
      DataField = 'PV_FEC_INI'
      DataSource = DataSource
    end
    object PV_FEC_FIN: TZetaDBFecha
      Left = 72
      Top = 45
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '21/Mar/12'
      Valor = 40989.000000000000000000
      DataField = 'PV_FEC_FIN'
      DataSource = DataSource
    end
  end
  object GroupBox2: TGroupBox [10]
    Left = 0
    Top = 218
    Width = 448
    Height = 116
    Align = alTop
    Caption = ' Condiciones '
    TabOrder = 6
    object PV_OBSERVA: TDBMemo
      Left = 2
      Top = 15
      Width = 444
      Height = 99
      Align = alClient
      DataField = 'PV_CONDIC'
      DataSource = DataSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 255
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object PV_NUMERO: TZetaDBNumero [11]
    Left = 72
    Top = 360
    Width = 121
    Height = 21
    Mascara = mnPesos
    TabOrder = 8
    Text = '0.00'
    DataField = 'PV_NUMERO'
    DataSource = DataSource
  end
  object PV_CFIJO: TZetaDBNumero [12]
    Left = 72
    Top = 336
    Width = 121
    Height = 21
    Mascara = mnPesos
    TabOrder = 7
    Text = '0.00'
    DataField = 'PV_CFIJO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 412
    Top = 113
  end
end
