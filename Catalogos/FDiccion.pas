unit FDiccion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls,
  Grids, DBGrids, ZetaDBGrid, StdCtrls, ZetaKeyCombo;

type
  TDiccion = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    CBEntidades: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure CBEntidadesChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;

  public
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
  end;

var
  Diccion: TDiccion;

implementation
uses DBaseDiccionario,
     DDiccionario,
     ZetaCommonClasses;

{$R *.DFM}
{ TSistDiccion }

procedure TDiccion.Connect;
begin
     with dmDiccionario do
     begin
          ConectaDiccion;
          DataSource.DataSet:= cdsDiccion;
     end;
end;

procedure TDiccion.Refresh;
begin
     {with CbEntidades do
          dmDiccionario.Entidad := TObjetoEntidad( Items.Objects[ ItemIndex ] ).Entidad;
     dmDiccionario.cdsEditDiccion.Refrescar;}
     with CBEntidades do
          dmDiccionario.RefrescaFormaConsulta(TObjetoEntidad( Items.Objects[ ItemIndex ] ).Entidad);
end;

procedure TDiccion.FormShow(Sender: TObject);
begin
     inherited;
     dmDiccionario.GetTablas( CBEntidades.Items );
     CBEntidades.ItemIndex := 0;
     CBEntidadesChange(Sender);
end;

procedure TDiccion.CBEntidadesChange(Sender: TObject);
begin
     inherited;
     Refresh;
end;

procedure TDiccion.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_DICCIONARIO;
     {$ELSE}
            {$ifdef EVALUACION}
            HelpContext := 0;
            {$else}
            HelpContext := H60653_Diccionario_datos;
            {$endif}
     {$endif}     
end;

procedure TDiccion.Agregar;
begin
     dmDiccionario.cdsDiccion.Agregar;
end;

procedure TDiccion.Borrar;
begin
     dmDiccionario.cdsDiccion.Borrar;
end;

procedure TDiccion.Modificar;
begin
     dmDiccionario.cdsDiccion.Modificar;
end;

function TDiccion.PuedeAgregar(var sMensaje: String): Boolean;
begin
        Result:= False;
        sMensaje := 'No Se Puede Agregar Al Diccionario De Datos';
end;

function TDiccion.PuedeBorrar(var sMensaje: String): Boolean;
begin
        Result:= False;
        sMensaje := 'No Se Puede Borrar En El Diccionario De Datos';
end;

end.
