inherited EditFestTurno_DevEx: TEditFestTurno_DevEx
  Left = 495
  Top = 197
  Caption = 'D'#237'as Festivos por Turno'
  ClientHeight = 284
  ClientWidth = 469
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 11
    Top = 227
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object Label5: TLabel [1]
    Left = 46
    Top = 80
    Width = 24
    Height = 13
    Caption = 'Tipo:'
  end
  object TurnoLbl: TLabel [2]
    Left = 39
    Top = 48
    Width = 31
    Height = 13
    Caption = 'Turno:'
  end
  object zTurno: TZetaTextBox [3]
    Left = 76
    Top = 46
    Width = 273
    Height = 17
    AutoSize = False
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object Label4: TLabel [4]
    Left = 14
    Top = 118
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Aplica para:'
  end
  inherited PanelBotones: TPanel
    Top = 248
    Width = 469
    TabOrder = 7
    inherited OK_DevEx: TcxButton
      Left = 305
      ParentBiDiMode = True
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 384
      Cancel = True
      ParentBiDiMode = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 469
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 143
      inherited textoValorActivo2: TLabel
        Width = 137
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 0
    Top = 8
    TabOrder = 11
  end
  object FE_DESCRIP: TDBEdit [8]
    Left = 74
    Top = 225
    Width = 264
    Height = 21
    DataField = 'FE_DESCRIP'
    DataSource = DataSource
    TabOrder = 4
  end
  object FE_TIPO: TDBRadioGroup [9]
    Left = 75
    Top = 70
    Width = 384
    Height = 33
    Columns = 2
    DataField = 'FE_TIPO'
    DataSource = DataSource
    Items.Strings = (
      'D'#237'a Festivo'
      'Intercambio de D'#237'a')
    TabOrder = 1
    Values.Strings = (
      '0'
      '1')
    OnChange = FE_TIPOChange
  end
  object GBTodos: TcxGroupBox [10]
    Left = 75
    Top = 147
    TabOrder = 10
    Height = 72
    Width = 191
    object FE_DIALbl: TLabel
      Left = 12
      Top = 40
      Width = 21
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'a:'
    end
    object FE_MESLbl: TLabel
      Left = 10
      Top = 16
      Width = 23
      Height = 13
      Alignment = taRightJustify
      Caption = 'Mes:'
    end
    object FE_MES: TZetaDBKeyCombo
      Left = 39
      Top = 14
      Width = 145
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      DropDownCount = 12
      ItemHeight = 13
      MaxLength = 1
      ParentCtl3D = False
      TabOrder = 0
      ListaFija = lfMeses
      ListaVariable = lvPuesto
      Offset = 1
      Opcional = False
      EsconderVacios = False
      DataField = 'FE_MES'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object FE_DIA: TZetaDBNumero
      Left = 39
      Top = 38
      Width = 40
      Height = 21
      Mascara = mnDias
      TabOrder = 1
      Text = '0'
      UseEnterKey = True
      DataField = 'FE_DIA'
      DataSource = DataSource
    end
  end
  object GBAplicaPara: TcxGroupBox [11]
    Left = 268
    Top = 147
    TabOrder = 3
    Height = 71
    Width = 191
    object FE_CAMBIOLbl: TLabel
      Left = 5
      Top = 40
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cambiar por:'
    end
    object FechaLbl: TLabel
      Left = 31
      Top = 16
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object FE_CAMBIO: TZetaDBFecha
      Left = 68
      Top = 38
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '23/dic/97'
      Valor = 35787.000000000000000000
      DataField = 'FE_CAMBIO'
      DataSource = DataSource
    end
    object zFecha: TZetaFecha
      Left = 68
      Top = 14
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '20/may/09'
      Valor = 39953.000000000000000000
      OnChange = zFechaChange
    end
  end
  object RGVigencia: TRadioGroup [12]
    Left = 75
    Top = 108
    Width = 384
    Height = 33
    Columns = 2
    Items.Strings = (
      'Todos los a'#241'os'
      'Solamente un a'#241'o')
    TabOrder = 2
    OnClick = RGVigenciaClick
  end
  inherited DataSource: TDataSource
    Left = 400
    Top = 213
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 40
    Top = 56
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 4194312
  end
end
