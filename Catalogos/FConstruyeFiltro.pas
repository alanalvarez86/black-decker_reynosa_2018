unit FConstruyeFiltro;

interface
{$INCLUDE DEFINES.INC}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ZetaEdit, ExtCtrls, Mask, ZetaFecha, Buttons, ZetaSmartLists,
  FBuscaCampos,  ZetaCommonLists, ZReportTools, ZetaTipoEntidad, checklst;

type
  TConstruyeFiltro = class(TForm)
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    LbFiltro: TZetaSmartListBox;
    BAgregaFiltro: TBitBtn;
    bBorraFiltro: TBitBtn;
    ZetaSmartListsButton3: TZetaSmartListsButton;
    ZetaSmartListsButton4: TZetaSmartListsButton;
    GBCampoFecha: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    EFechaInicial: TZetaFecha;
    EFechaFinal: TZetaFecha;
    RGBooleano: TGroupBox;
    GBAbierto: TGroupBox;
    Label4: TLabel;
    EAbiertoFiltro: TMemo;
    PanelInferior: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    SmartListFiltro: TZetaSmartLists;
    GBRangoEntidad: TGroupBox;
    lbInicialRangoEntidad: TLabel;
    lbFinalRangoEntidad: TLabel;
    bFinalRangoEntidad: TSpeedButton;
    bInicialRangoEntidad: TSpeedButton;
    bListaRangoEntidad: TSpeedButton;
    RBTodosRangoEntidad: TRadioButton;
    RBActivoRangoEntidad: TRadioButton;
    RBRangoRangoEntidad: TRadioButton;
    RBListaRangoEntidad: TRadioButton;
    EInicialRangoEntidad: TZetaEdit;
    EFinalRangoEntidad: TZetaEdit;
    EListaRangoEntidad: TZetaEdit;
    GBRangoLista: TGroupBox;
    RBTodosRangoValores: TRadioButton;
    RBActivoRangoValores: TRadioButton;
    RBListaRangoValores: TRadioButton;
    CLBSeleccionValores: TCheckListBox;
    RGSeleccionValores: TRadioGroup;
    EFormulaFiltro: TEdit;
    procedure BAgregaFiltroClick(Sender: TObject);
    procedure bBorraFiltroClick(Sender: TObject);
    procedure SmartListFiltroAlSeleccionar(Sender: TObject;
      var Objeto: TObject; Texto: String);
    procedure FormShow(Sender: TObject);
    procedure RGSeleccionValoresClick(Sender: TObject);
    procedure EAbiertoFiltroExit(Sender: TObject);
    procedure RBTodosRangoEntidadClick(Sender: TObject);
    procedure RBActivoRangoEntidadClick(Sender: TObject);
    procedure RBRangoRangoEntidadClick(Sender: TObject);
    procedure EListaRangoEntidadExit(Sender: TObject);
    procedure RBListaRangoEntidadClick(Sender: TObject);
    procedure bInicialRangoEntidadClick(Sender: TObject);
    procedure bFinalRangoEntidadClick(Sender: TObject);
    procedure bListaRangoEntidadClick(Sender: TObject);
    procedure RBTodosRangoValoresClick(Sender: TObject);
    procedure RBActivoRangoValoresClick(Sender: TObject);
    procedure RBListaRangoValoresClick(Sender: TObject);
    procedure EFechaInicialExit(Sender: TObject);
    procedure CLBSeleccionValoresExit(Sender: TObject);
    procedure LbFiltroDblClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FEntidadActiva : TipoEntidad;
    FFiltro: String;
    FConRelaciones: Boolean;
    function  GetFiltro: String;
    function  MakeFiltro: string;
    //function  GetCampoMaster( Tipo : eTipoCampo; oContainer : TCampoMaster; sTitulo : string ):TCampoMaster;
    procedure BorraDato( oLista : TZetaSmartLists; oAgregar, oBorrar : TBitBtn;
                         oControl : TWinControl );
    procedure EnabledFiltro( bEnable : Boolean );
    procedure ActualizaFiltro( oFiltro : TFiltroOpciones );

    function  GetTipoFiltroActivo : eTipoRango;
    procedure FiltroActivo;
    procedure ActualizaRangoEntidad(iTipoRango : eTipoRangoActivo; i : integer );
    procedure ActualizaObjetoRangoEntidad( iTipoRango : eTipoRangoActivo );
    procedure ActualizaListaRangoValores(i : integer);
    procedure ActualizaObjetoRangoValores( iTipoRango : eTipoRangoActivo );
    procedure ActualizaRangoValores(iTipoRango : eTipoRangoActivo; i : integer );
    function  BuscaDialogoEntidad( iEntidad : integer; oEdit : TCustomEdit ) : Boolean;
    procedure AgregaFiltro;
  public
    property EntidadActiva : TipoEntidad read FEntidadActiva write FEntidadActiva;
    property Filtro: String read GetFiltro write FFiltro;
    property ConRelaciones: Boolean read FConRelaciones write FConRelaciones;
  end;

  function GetConstruyeFiltro( iEntidad: TipoEntidad; var sFiltro: String;const lConRelaciones: Boolean=TRUE ): Boolean;

var
  ConstruyeFiltro: TConstruyeFiltro;

implementation

uses DCliente,
     FTressShell,
     ZetaClientTools,
     ZetaCommonTools,
     ZetaCommonClasses, DDiccionario;

{$R *.DFM}


function GetConstruyeFiltro( iEntidad: TipoEntidad; var sFiltro: String; const lConRelaciones: Boolean=TRUE ): Boolean;
begin
     ConstruyeFiltro := TConstruyeFiltro.Create( Application.MainForm );

     with ConstruyeFiltro do
     begin
          try
             if iEntidad <> enNinguno then EntidadActiva := iEntidad;
             ConRelaciones := lConRelaciones;
             ShowModal;
             Result:= ( ModalResult = mrOK );

             sFiltro:= Filtro;
          finally
                 Free;
          end;
     end;
end;


// TConstruyeFiltro

function TConstruyeFiltro.GetFiltro: String;
begin
     Result:= MakeFiltro;
end;

function TConstruyeFiltro.MakeFiltro: string;
 var i:integer;
     lDiccion : Boolean;

begin
     Result:= '';
     with LBFiltro.Items do
          for i:=0 to Count - 1 do
               Result := Result + ' AND ' + ZReportTools.GetStringFiltro(TFiltroOpciones(Objects[i]), lDiccion, TRUE );

     Result := Copy( Result, 5, MaxInt );

end;

procedure TConstruyeFiltro.AgregaFiltro;
 var oFiltro : TFiltroOpciones;
     oDiccion : TDiccionRecord;
begin

     if AntesDeAgregarDato( FEntidadActiva, FALSE, FALSE, oDiccion, SmartListFiltro, FConRelaciones ) then
     begin
          oFiltro := TFiltroOpciones.Create;
          DiccionToCampoMaster(oDiccion, oFiltro);
          with oFiltro do
          begin
               TipoRangoActivo := eTipoRangoActivo( oDiccion.DI_RANGOAC );
               ValorActivo := oDiccion.DI_VALORAC;
               Numero := oDiccion.DI_NUMERO;
               TipoRango := eTipoRango(oDiccion.DI_TRANGO);

               case TipoRango of
                    rNinguno :
                    {$ifdef RDD}
                    begin
                         case oFiltro.TipoCampo of
                              tgFloat : RangoInicial := ObtieneElemento( lfRDDDefaultsFloat, iMin( oDiccion.DI_RANGOAC, Ord( High(eRDDDefaultsFloat))) );
                              tgTexto :  RangoInicial := ObtieneElemento( lfRDDDefaultsTexto, iMin( oDiccion.DI_RANGOAC, Ord( High(eRDDDefaultsTexto))) );
                         end;
                    end;
                    {$else}
                    RangoInicial := ValorActivo;
                    {$endif}



                    rFechas :
                    begin
                         FechaInicial := dmCliente.FechaDefault;
                         FechaFinal := FechaInicial;
                    end;
                    rBool :
                    {$ifdef RDD}
                    Posicion:= oDiccion.DI_RANGOAC;
                    {$else}
                    begin
                         if ValorActivo = K_GLOBAL_SI then Posicion:=0
                         else Posicion := 1;
                    end;
                    {$endif}

               end;
          end;

          DespuesDeAgregarDato( oFiltro,
                                SmartListFiltro,
                                BBorraFiltro,
                                LBFiltro );
     end;
end;

procedure TConstruyeFiltro.BorraDato( oLista : TZetaSmartLists; oAgregar, oBorrar : TBitBtn;
                                      oControl : TWinControl );
 var i : integer;
     oCampo : TCampoMaster;
begin
     i := oLista.ListaDisponibles.ItemIndex;
     if i >= 0 then
     begin
          oCampo := TCampoMaster(oLista.ListaDisponibles.Items.Objects[ i ]);
          oCampo.Free;
          //oLista.ListaDisponibles.Items.Objects[ i ].Free;
          oLista.ListaDisponibles.Items.Delete(i);
     end;

     oBorrar.Enabled := oLista.ListaDisponibles.Items.Count > 0;

     if oBorrar.Enabled then
     begin
          if i >= oLista.ListaDisponibles.Items.Count then i := oLista.ListaDisponibles.Items.Count - 1;
          oLista.SelectEscogido( i );
          if oControl.Enabled then oControl.SetFocus
          else oLista.ListaDisponibles.SetFocus;
     end
     else oAgregar.SetFocus;
end;

procedure TConstruyeFiltro.EnabledFiltro( bEnable : Boolean );
begin
     EFormulaFiltro.Text := '';
     GBRangoEntidad.Visible := bEnable;
     GBRangoLista.Visible := bEnable;
     GBCampoFecha.Visible := bEnable;
     GbAbierto.Visible := bEnable;
     RgBooleano.Visible := bEnable;
end;

procedure TConstruyeFiltro.ActualizaListaRangoValores(i : integer);
begin
     with TFiltroOpciones( lbFiltro.Items.Objects[ i ] ) do
     begin
          GbRangoLista.Caption := Titulo;
          ActualizaRangoValores( TipoRangoActivo, i );
          dmDiccionario.LlenaLista( Ord( ListasFijas( Numero ) ),  CLBSeleccionValores.Items );

          DecodeTexto( CLBSeleccionValores, Lista, Numero );
     end;
end;

procedure TConstruyeFiltro.ActualizaFiltro( oFiltro : TFiltroOpciones );
 procedure ActualizaAbierto;
 begin
      with oFiltro do
      begin
           GbAbierto.Caption := Titulo;
           EAbiertoFiltro.Text := RangoInicial;
           EAbiertoFiltro.SetFocus;
      end;
 end;

 procedure ActualizaLlaves;
 begin
      with oFiltro do
      begin
           GbRangoEntidad.Caption := Titulo;
           EInicialRangoEntidad.Text := RangoInicial;
           EFinalRangoEntidad.Text := RangoFinal;
           EListaRangoEntidad.Text := Lista;
      end;
      ActualizaRangoEntidad( oFiltro.TipoRangoActivo, lbFiltro.ItemIndex );
 end;

 procedure ActualizaStatus;
 begin
      ActualizaListaRangoValores( lbFiltro.ItemIndex );
 end;

 procedure ActualizaFechas;
 begin
      with oFiltro do
      begin
           GbCampoFecha.Caption := Titulo;
           //EFechaInicial.Text := RangoInicial;
           //EFechaFinal.Text := RangoFinal;
           if StrVacio( RangoInicial ) then RangoInicial := FormatDateTime( 'dd/mm/yyyy',dmCliente.FechaDefault );
           if StrVacio( RangoFinal ) then RangoFinal := FormatDateTime( 'dd/mm/yyyy', dmCliente.FechaDefault );
           EFechaInicial.Valor := StrToDate( RangoInicial );
           EFechaFinal.Valor := StrToDate( RangoFinal );
      end;
 end;

 procedure ActualizaBool;
 begin
      with oFiltro do
      begin
           RGBooleano.Caption := Titulo;
           RGSeleccionValores.ItemIndex := Posicion;
      end;
 end;

begin
     FiltroActivo;
     EFormulaFiltro.Text := oFiltro.Formula;
     case GetTipoFiltroActivo of
          rNinguno : ActualizaAbierto;
          rRangoEntidad: ActualizaLlaves;
          rRangoListas: ActualizaStatus;
          rFechas: ActualizaFechas;
          rBool: ActualizaBool;
     end;
end;

procedure TConstruyeFiltro.FiltroActivo;
begin
     with TFiltroOpciones( LbFiltro.Items.Objects[ LbFiltro.ItemIndex ] ) do
     begin
          GBRangoEntidad.Visible := TipoRango = rRangoEntidad;
          GBRangoLista.Visible := TipoRango = rRangoListas;
          GBCampoFecha.Visible := TipoRango = rFechas;
          GbAbierto.Visible := TipoRango = rNinguno;
          RgBooleano.Visible := TipoRango = rBool;
     end;
end;

function TConstruyeFiltro.GetTipoFiltroActivo : eTipoRango;
begin
     Result :=  TFiltroOpciones( lbFiltro.Items.Objects[ LbFiltro.ItemIndex ] ).TipoRango;
end;

procedure TConstruyeFiltro.ActualizaRangoEntidad(iTipoRango : eTipoRangoActivo; i : integer );
 var oCampo : TFiltroOpciones;
begin
     //CLBRangoEntidad.Checked[ i ] := iTipoRango <> raTodos;

     //Todos
     RBTodosRangoEntidad.Enabled := TRUE;
     RBTodosRangoEntidad.Checked := iTipoRango = raTodos;

     //Activo
     oCampo := TFiltroOpciones( LbFiltro.Items.Objects[ i ] );

     {$ifdef RDD}
     RBActivoRangoEntidad.Enabled := oCampo.ValorActivo <> vaSinValor;
     {$else}
     RBActivoRangoEntidad.Enabled := StrLleno( oCampo.ValorActivo ) ;
     {$endif}

     RBActivoRangoEntidad.Checked := iTipoRango = raActivo;

     //Rango
     RBRangoRangoEntidad.Enabled := TRUE;
     RBRangoRangoEntidad.Checked := iTipoRango = raRango;
     lbInicialRangoEntidad.Enabled := iTipoRango = raRango;
     EInicialRangoEntidad.Enabled := iTipoRango = raRango;
     bInicialRangoEntidad.Enabled := ( iTipoRango = raRango ) AND ( oCampo.Numero >= 0 );
     lbFinalRangoEntidad.Enabled := iTipoRango = raRango;
     EFinalRangoEntidad.Enabled := iTipoRango = raRango;
     bFinalRangoEntidad.Enabled := ( iTipoRango = raRango ) AND ( oCampo.Numero >= 0 );

     //Lista
     RBListaRangoEntidad.Enabled := TRUE;
     RBListaRangoEntidad.Checked := iTipoRango = raLista;
     eListaRangoEntidad.Enabled := iTipoRango = raLista;
     bListaRangoEntidad.Enabled := ( iTipoRango = raLista ) AND ( oCampo.Numero >= 0 );
end;

procedure TConstruyeFiltro.ActualizaObjetoRangoEntidad( iTipoRango : eTipoRangoActivo );
 var i : integer;
     lCambio : Boolean;
begin
     i := lbFiltro.ItemIndex;
     with TFiltroOpciones( lbFiltro.Items.Objects[ i ] ) do
     begin
          lCambio := ( TipoRangoActivo <> iTipoRango ) OR
                     ( RangoInicial <> EInicialRangoEntidad.Text ) OR
                     ( RangoFinal <> EFinalRangoEntidad.Text ) OR
                     ( Lista <> EListaRangoEntidad.Text );

          if lCambio then
          begin
               TipoRangoActivo := iTipoRango;
               RangoInicial := EInicialRangoEntidad.Text;
               RangoFinal := EFinalRangoEntidad.Text;
               Lista := EListaRangoEntidad.Text;
          end;
     end;
end;

function TConstruyeFiltro.BuscaDialogoEntidad( iEntidad : integer; oEdit : TCustomEdit ) : Boolean;
 var sLlave, sDescripcion : string;

begin
     sLlave := oEdit.Text;
     Result := TressShell.BuscaDialogo( TipoEntidad( iEntidad ), '', sLlave, sDescripcion );

     if Result then
        oEdit.Text := sLlave;
end;



//Eventos


procedure TConstruyeFiltro.BAgregaFiltroClick(Sender: TObject);
begin
     AgregaFiltro;
end;

procedure TConstruyeFiltro.bBorraFiltroClick(Sender: TObject);
begin
     BorraDato( SmartListFiltro, bAgregaFiltro, bBorraFiltro, LBFiltro );
     if LBFiltro.Items.Count = 0 then EnabledFiltro( FALSE );
end;

procedure TConstruyeFiltro.SmartListFiltroAlSeleccionar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     inherited;
     if Objeto <> NIL then
     begin
          ActualizaFiltro( TFiltroOpciones( Objeto ) );
     end
     else EnabledFiltro( FALSE );
end;

procedure TConstruyeFiltro.FormShow(Sender: TObject);
begin
     EnabledFiltro( False );
     SmartListFiltro.SelectEscogido( 0 );
end;

procedure TConstruyeFiltro.RGSeleccionValoresClick(Sender: TObject);
begin
     TFiltroOpciones( lbFiltro.Items.Objects[ lbFiltro.ItemIndex ] ).Posicion := RGSeleccionValores.ItemIndex;
     //HayCambio;
end;

procedure TConstruyeFiltro.EAbiertoFiltroExit(Sender: TObject);
begin
     with TFiltroOpciones( lbFiltro.Items.Objects[ lbFiltro.ItemIndex ] ) do
          if RangoInicial <> EAbiertoFiltro.Text then
          begin
               RangoInicial:= EAbiertoFiltro.Text;
              // HayCambio;
          end;
end;

procedure TConstruyeFiltro.RBTodosRangoEntidadClick(Sender: TObject);
begin
     inherited;
     ActualizaRangoEntidad( raTodos, LbFiltro.ItemIndex );
     ActualizaObjetoRangoEntidad( raTodos );
end;

procedure TConstruyeFiltro.RBActivoRangoEntidadClick(Sender: TObject);
begin
     ActualizaRangoEntidad( raActivo, lbFiltro.ItemIndex );
     ActualizaObjetoRangoEntidad( raActivo );
end;

procedure TConstruyeFiltro.RBRangoRangoEntidadClick(Sender: TObject);
begin
     inherited;
     ActualizaRangoEntidad( raRango, lbFiltro.ItemIndex );
     ActualizaObjetoRangoEntidad( raRango );
end;

procedure TConstruyeFiltro.RBListaRangoEntidadClick(Sender: TObject);
begin
     inherited;
     ActualizaRangoEntidad( raLista, lbFiltro.ItemIndex );
     ActualizaObjetoRangoEntidad( raLista );
end;

procedure TConstruyeFiltro.EListaRangoEntidadExit(Sender: TObject);
begin
     ActualizaRangoEntidad( raLista, lbFiltro.ItemIndex );
     ActualizaObjetoRangoEntidad( raLista );
end;

procedure TConstruyeFiltro.bInicialRangoEntidadClick(Sender: TObject);
begin
     inherited;
     if BuscaDialogoEntidad( TFiltroOpciones(lbFiltro.Items.Objects[ lbFiltro.ItemIndex ]).Numero, EInicialRangoEntidad ) then
        ActualizaObjetoRangoEntidad( raRango );
end;

procedure TConstruyeFiltro.bFinalRangoEntidadClick(Sender: TObject);
begin
     if BuscaDialogoEntidad( TFiltroOpciones(lbFiltro.Items.Objects[ lbFiltro.ItemIndex ]).Numero,
                             EFinalRangoEntidad ) then
        ActualizaObjetoRangoEntidad( raRango );
end;

procedure TConstruyeFiltro.bListaRangoEntidadClick(Sender: TObject);
var sLlave,sDescripcion : string;
     iNumero : integer;
begin
     inherited;
     iNumero := TFiltroOpciones(lbFiltro.Items.Objects[ lbFiltro.ItemIndex ]).Numero;
     if TressShell.BuscaDialogo( TipoEntidad( iNumero ), '', sLlave, sDescripcion ) then
     begin
        if StrLleno( EListaRangoEntidad.Text ) then
           EListaRangoEntidad.Text := EListaRangoEntidad.Text + ','+ sLlave
        else EListaRangoEntidad.Text := sLlave;
        ActualizaObjetoRangoEntidad( raLista );
     end;
end;


procedure TConstruyeFiltro.RBTodosRangoValoresClick(Sender: TObject);
begin
     ActualizaRangoValores( raTodos, lbFiltro.ItemIndex );
     ActualizaObjetoRangoValores( raTodos );
end;

procedure TConstruyeFiltro.RBActivoRangoValoresClick(Sender: TObject);
begin
     ActualizaRangoValores( raRango, lbFiltro.ItemIndex );
     ActualizaObjetoRangoValores( raRango );
end;

procedure TConstruyeFiltro.RBListaRangoValoresClick(Sender: TObject);
begin
     ActualizaRangoValores( raLista, lbFiltro.ItemIndex );
     ActualizaObjetoRangoValores( raLista );
end;

procedure TConstruyeFiltro.ActualizaRangoValores(iTipoRango : eTipoRangoActivo; i : integer );
begin
     //Todos
     RBTodosRangoValores.Enabled := TRUE;
     RBTodosRangoValores.Checked := iTipoRango = raTodos;

     //Activo
     {$ifdef RDD}
     RBActivoRangoValores.Enabled := TFiltroOpciones( lbFiltro.Items.Objects[ i ] ).ValorActivo <> vaSinValor;
     {$else}
     RBActivoRangoValores.Enabled := TFiltroOpciones( lbFiltro.Items.Objects[ i ] ).ValorActivo <> VACIO;
     {$endif}
     RBActivoRangoValores.Checked := iTipoRango = raActivo;

     //Lista
     RBListaRangoValores.Enabled := TRUE;
     RBListaRangoValores.Checked := iTipoRango = raLista;
     CLBSeleccionValores.Enabled := iTipoRango = raLista;

end;

procedure TConstruyeFiltro.ActualizaObjetoRangoValores( iTipoRango : eTipoRangoActivo );
 var i : integer;
begin
     i := lbFiltro.ItemIndex;
     with TFiltroOpciones( lbFiltro.Items.Objects[ i ] ) do
     begin
          TipoRangoActivo := iTipoRango;
          //Lista := DecodeLista( Numero );
     end;
end;

procedure TConstruyeFiltro.EFechaInicialExit(Sender: TObject);
begin
     with TFiltroOpciones( lbFiltro.Items.Objects[ lbFiltro.ItemIndex ] ) do
     begin
          RangoInicial := FormatDateTime( 'dd/mm/yyyy', EFechaInicial.Valor );
          RangoFinal := FormatDateTime( 'dd/mm/yyyy', EFechaFinal.Valor );
     end;
end;

procedure TConstruyeFiltro.CLBSeleccionValoresExit(Sender: TObject);
begin
     with TFiltroOpciones( lbFiltro.Items.Objects[ lbFiltro.ItemIndex ] ) do
          Lista := DecodeLista( CLBSeleccionValores, Numero );
end;
procedure TConstruyeFiltro.LbFiltroDblClick(Sender: TObject);
begin
     //No hace nada - No quitar!!!!
end;

procedure TConstruyeFiltro.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
     if ActiveControl <> OK then
        OK.SetFocus;
end;

end.
