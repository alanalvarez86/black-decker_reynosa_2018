inherited CatCertificaciones_DevEx: TCatCertificaciones_DevEx
  Left = 240
  Top = 424
  Caption = 'Certificaciones'
  ClientHeight = 283
  ClientWidth = 601
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 601
    inherited ValorActivo2: TPanel
      Width = 342
      inherited textoValorActivo2: TLabel
        Width = 336
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 601
    Height = 264
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object CI_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CI_CODIGO'
        MinWidth = 70
      end
      object CI_NOMBRE: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'CI_NOMBRE'
        MinWidth = 90
        Width = 90
      end
      object CI_RESUMEN: TcxGridDBColumn
        Caption = 'Resumen'
        DataBinding.FieldName = 'CI_RESUMEN'
        MinWidth = 90
        Width = 90
      end
      object CI_RENOVAR: TcxGridDBColumn
        Caption = 'Renovar'
        DataBinding.FieldName = 'CI_RENOVAR'
        Width = 70
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 488
    Top = 232
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end