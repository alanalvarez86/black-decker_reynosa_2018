unit FEditCatMaestros_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, Buttons, DBCtrls, StdCtrls, ExtCtrls,
  Mask, ZetaEdit, ZetaNumero, ExtDlgs,
  ClipBrd, ZetaKeyCombo, ComCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxPC, ZetaKeyLookup_DevEx, imageenview, ieview,
  ieopensavedlg, imageenproc, hyieutils;

type
  TEditCatMaestros_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    MA_CODIGO: TZetaDBEdit;
    lblCodigo: TLabel;
    lblNombre: TLabel;
    MA_NOMBRE: TDBEdit;
    PageControl: TcxPageControl;
    tbGenerales_DevEx: TcxTabSheet;
    tbImagen_DevEx: TcxTabSheet;
    tbSTPS_DevEx: TcxTabSheet;
    PC_CODIGO: TZetaDBKeyLookup_DevEx;
    MA_ACTIVO: TDBCheckBox;
    MA_NUMERO: TZetaDBNumero;
    MA_TEL: TDBEdit;
    MA_CIUDAD: TDBEdit;
    MA_DIRECCI: TDBEdit;
    MA_EMPRESA: TDBEdit;
    MA_TEXTO: TDBEdit;
    MA_RFC: TDBEdit;
    MA_CEDULA: TDBEdit;
    lblTelefono: TLabel;
    lblCiudad: TLabel;
    lblCalle: TLabel;
    lblEmpresa: TLabel;
    lblTexto: TLabel;
    lblNumero: TLabel;
    lblRFC: TLabel;
    lblCedula: TLabel;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    MA_TAGENTE: TZetaDBKeyCombo;
    MA_NAGENTE: TZetaDBEdit;
    GrabarDisco_DevEx: TcxButton;
    LeerDisco_DevEx: TcxButton;
    BorraImagen_DevEx: TcxButton;
    Imagen: TImageEnView;
    SaveImageEnDialog: TSaveImageEnDialog;
    OpenImageEnDialog: TOpenImageEnDialog;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure GrabarDisco_DevExClick(Sender: TObject);
    procedure BorraImagen_DevExClick(Sender: TObject);
    procedure LeerDisco_DevExClick(Sender: TObject);
    procedure dxBarButton_PegarBtnClick(Sender: TObject);
    procedure dxBarButton_CortarBtnClick(Sender: TObject);
    procedure dxBarButton_CopiarBtnClick(Sender: TObject);
    procedure ImagenClick(Sender: TObject);
    procedure ImagenExit(Sender: TObject);
    procedure ImagenEnter(Sender: TObject);
  private
    LimpiarImagen: Boolean;
    Borde: Boolean;
    procedure GrabaImagen;
    procedure CrearBorde;
    procedure EliminarBorde;
  protected
    procedure Connect; override;
    procedure DoLookup; override;
  public
  end;

var
  EditCatMaestros_DevEx: TEditCatMaestros_DevEx;

const
    K_DELETE = 'D';
implementation

uses ZetaCommonClasses, dCatalogos, ZAccesosTress, ZetaBuscador_DevEx, FToolsImageEn, ZetaCommonTools;

{$R *.DFM}

procedure TEditCatMaestros_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAPA_MAESTROS;
     HelpContext:= H60642_Maestros;
     FirstControl :=MA_CODIGO;
     LimpiarImagen := FALSE;
end;

procedure TEditCatMaestros_DevEx.Connect;
begin
     dmCatalogos.cdsProvCap.Conectar;
     Datasource.Dataset := dmCatalogos.cdsMaestros;
end;

procedure TEditCatMaestros_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Maestro', 'Maestro', 'MA_CODIGO', dmCatalogos.cdsMaestros );
end;

procedure TEditCatMaestros_DevEx.dxBarButton_CopiarBtnClick(Sender: TObject);
begin
     EliminarBorde;
     if Assigned( ActiveControl ) and ( ActiveControl.Enabled ) and ( ActiveControl is TImageEnView ) then
     begin
          TImageEnView( ActiveControl ).Proc.CopyToClipboard;
     end
     else
         inherited;
end;

procedure TEditCatMaestros_DevEx.dxBarButton_CortarBtnClick(Sender: TObject);
begin
     EliminarBorde;
     if Assigned( ActiveControl ) and ( ActiveControl.Enabled ) and ( ActiveControl is TImageEnView ) then
     begin
          with TImageEnView( ActiveControl ) do
          begin
               Proc.CopyToClipboard;
               Clear;
               Update;
          end;

          if not (dmCatalogos.cdsMaestros.State = dsEdit) then
              dmCatalogos.cdsMaestros.Edit;
          dmCatalogos.cdsMaestros.FieldByName('MA_IMAGEN').AsString := K_DELETE;
     end
     else
         inherited;
end;

procedure TEditCatMaestros_DevEx.dxBarButton_PegarBtnClick(Sender: TObject);
begin
     EliminarBorde;
     if Assigned( ActiveControl ) and ( ActiveControl.Enabled ) and ( ActiveControl is TImageEnView ) then
     begin
          if Clipboard.HasFormat ( CF_PICTURE ) then
          begin
               with TImageEnView( ActiveControl ) do
               begin
                    Clear;
                    Proc.PasteFromClipboard;
                    Update;
               end;
               GrabaImagen;
          end;
     end
     else
         inherited;
end;

procedure TEditCatMaestros_DevEx.GrabaImagen;
begin
     FToolsImageEn.AsignaImagenABlob( Imagen, dmCatalogos.cdsMaestros, 'MA_IMAGEN' );
end;

procedure TEditCatMaestros_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;

     Borde := FALSE;
     if (dmCatalogos.cdsMaestros.State = dsInsert) and (LimpiarImagen = TRUE)then
     begin
          Imagen.Clear;
          LimpiarImagen := FALSE;
     end
     else if ( Field = nil ) and ( dmCatalogos.cdsMaestros.State = dsBrowse ) then
     begin
        FToolsImageEn.AsignaBlobAImagen( Imagen, dmCatalogos.cdsMaestros, 'MA_IMAGEN' );
        LimpiarImagen := TRUE;
     end;

     GrabarDisco_DevEx.Enabled:=
        (dmCatalogos.cdsMaestros.FieldByName('MA_IMAGEN').AsString <> VACIO)
        and (dmCatalogos.cdsMaestros.FieldByName('MA_IMAGEN').AsString <> K_DELETE);
     BorraImagen_DevEx.Enabled:= GrabarDisco_DevEx.Enabled;
end;

procedure TEditCatMaestros_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage :=  tbGenerales_DevEx;
     Borde := FALSE;
end;

procedure TEditCatMaestros_DevEx.GrabarDisco_DevExClick(Sender: TObject);
const
     aImagenExt: array[ 1..7 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'JPG','JPG','BMP', 'GIF', 'TIF', 'PCX', 'PNG' );
begin
     inherited;
     with SaveImageEnDialog do
     begin
          FilterIndex := 0;
          FileName := VACIO;
          if Execute then
          begin
               if strVacio( DefaultExt ) then
                  DefaultExt := aImagenExt[ FilterIndex ];
               Imagen.IO.Params.JPEG_Quality := 75;
               Imagen.IO.SaveToFile( FileName );
          end;
     end;
end;

procedure TEditCatMaestros_DevEx.ImagenClick(Sender: TObject);
begin
  CrearBorde;
end;

procedure TEditCatMaestros_DevEx.BorraImagen_DevExClick(Sender: TObject);
begin
    { Imagen.Clear;
    GrabaImagen; }
    Imagen.Clear;
    if not (dmCatalogos.cdsMaestros.State = dsEdit) then
        dmCatalogos.cdsMaestros.Edit;

    dmCatalogos.cdsMaestros.FieldByName('MA_IMAGEN').AsString := K_DELETE;
end;

procedure TEditCatMaestros_DevEx.LeerDisco_DevExClick(Sender: TObject);
begin
     inherited;
     if OpenImageEnDialog.Execute then
     begin
          // Imagen.Clear;
          Imagen.IO.LoadFromFile( OpenImageEnDialog.FileName );
          GrabaImagen;
     end;
end;

procedure TEditCatMaestros_DevEx.ImagenEnter(Sender: TObject);
begin
    CrearBorde
end;

procedure TEditCatMaestros_DevEx.ImagenExit(Sender: TObject);
{var
  OrigWidth, origHeight: integer;
  oLeft, oTop, oRight, oBottom: integer;}
begin
    EliminarBorde;
end;

procedure TEditCatMaestros_DevEx.CrearBorde;
var
  OrigWidth, origHeight: integer;
  oLeft: integer;
begin
    if (not Borde) and
      (dmCatalogos.cdsMaestros.FieldByName('MA_IMAGEN').AsString <> VACIO)
        and (dmCatalogos.cdsMaestros.FieldByName('MA_IMAGEN').AsString <> K_DELETE) then
    begin
      Borde := TRUE;
      OrigWidth := Imagen.IEBitmap.Width;
      OrigHeight := Imagen.IEBitmap.Height;
      oLeft := 4;

      // Color para el borde.
      Imagen.BackGround := TColor($4D3B4B);

      Imagen.Proc.ImageResize(OrigWidth + oLeft * 2, OrigHeight + oLeft * 2, iehCenter, ievCenter);

      // Restaurar background color.
      Imagen.Background := clBtnFace;
    end;
end;

procedure TEditCatMaestros_DevEx.EliminarBorde;
var
  OrigWidth, origHeight: integer;
  oLeft: integer;
begin
    if Borde then
    begin
      Borde := FALSE;
      OrigWidth := Imagen.IEBitmap.Width;
      OrigHeight := Imagen.IEBitmap.Height;
      oLeft := 4;

      Imagen.Proc.ImageResize(OrigWidth - oLeft * 2, OrigHeight - oLeft * 2, iehCenter, ievCenter);

      // Restaurar background color.
      Imagen.Background := clBtnFace;
    end;
end;

end.
