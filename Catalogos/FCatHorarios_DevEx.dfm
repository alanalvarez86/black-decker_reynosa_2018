inherited CatHorarios_DevEx: TCatHorarios_DevEx
  Left = 456
  Top = 335
  Caption = 'Horarios'
  ClientHeight = 257
  ClientWidth = 595
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 595
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 269
      inherited textoValorActivo2: TLabel
        Width = 263
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 595
    Height = 238
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object HO_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'HO_CODIGO'
      end
      object HO_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'HO_DESCRIP'
      end
      object HO_INTIME: TcxGridDBColumn
        Caption = 'Entrada'
        DataBinding.FieldName = 'HO_INTIME'
      end
      object HO_OUTTIME: TcxGridDBColumn
        Caption = 'Salida'
        DataBinding.FieldName = 'HO_OUTTIME'
      end
      object HO_JORNADA: TcxGridDBColumn
        Caption = 'Horas'
        DataBinding.FieldName = 'HO_JORNADA'
      end
      object HO_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'HO_TIPO'
      end
      object HO_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'HO_ACTIVO'
      end
    end
  end
  inherited DataSource: TDataSource
    Top = 152
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
