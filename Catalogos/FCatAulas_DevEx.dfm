inherited CatAulas_DevEx: TCatAulas_DevEx
  Left = 380
  Top = 272
  Caption = 'Aulas'
  ClientWidth = 512
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 512
    ExplicitWidth = 512
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 253
      ExplicitWidth = 253
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 247
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 512
    ExplicitWidth = 512
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object AL_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'AL_CODIGO'
        MinWidth = 80
        Width = 80
      end
      object AL_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'AL_NOMBRE'
        MinWidth = 80
        Width = 250
      end
      object AL_CUPO: TcxGridDBColumn
        Caption = 'Cupo'
        DataBinding.FieldName = 'AL_CUPO'
        MinWidth = 80
      end
      object AL_ACTIVA: TcxGridDBColumn
        Caption = 'Activa'
        DataBinding.FieldName = 'AL_ACTIVA'
        MinWidth = 60
        Width = 80
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
