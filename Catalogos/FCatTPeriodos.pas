unit FCatTPeriodos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TCatTPeriodos = class(TBaseConsulta)
    GridTPeriodos: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Modificar; override;
  public
   { Public declarations }
  end;

var
  CatTPeriodos: TCatTPeriodos;

implementation

uses DCatalogos,
     ZetaCommonClasses;

{$R *.DFM}

procedure TCatTPeriodos.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_TIPOS_D_PERIODO; // act. HC
end;

procedure TCatTPeriodos.Connect;
begin
     with dmCatalogos do
     begin
          cdsTPeriodos.Conectar;
          DataSource.DataSet:= cdsTPeriodos;
     end;
end;

procedure TCatTPeriodos.Refresh;
begin
     dmCatalogos.cdsTPeriodos.Refrescar;
end;

function TCatTPeriodos.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar En Esta Pantalla';
end;

function TCatTPeriodos.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar En Esta Pantalla';
end;

procedure TCatTPeriodos.Modificar;
begin
     dmCatalogos.cdsTPeriodos.Modificar;
end;

end.
