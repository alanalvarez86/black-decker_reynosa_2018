inherited EditCatTiposPension: TEditCatTiposPension
  Left = 353
  Top = 152
  Caption = 'Tipo de Pensi'#243'n'
  ClientHeight = 521
  ClientWidth = 523
  PixelsPerInch = 96
  TextHeight = 13
  object DBCodigoLBL: TLabel [0]
    Left = 35
    Top = 59
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label2: TLabel [1]
    Left = 31
    Top = 125
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object DBInglesLBL: TLabel [2]
    Left = 40
    Top = 103
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object lblNombre: TLabel [3]
    Left = 31
    Top = 81
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre:'
  end
  object Label1: TLabel [4]
    Left = 32
    Top = 450
    Width = 94
    Height = 13
    Caption = 'Aplicar en N'#243'minas:'
  end
  object Label3: TLabel [5]
    Left = 40
    Top = 148
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  inherited PanelBotones: TPanel
    Top = 485
    Width = 523
    inherited OK: TBitBtn
      Left = 355
    end
    inherited Cancelar: TBitBtn
      Left = 440
    end
  end
  inherited PanelSuperior: TPanel
    Width = 523
  end
  inherited PanelIdentifica: TPanel
    Width = 523
    inherited ValorActivo2: TPanel
      Width = 197
      inherited textoValorActivo2: TLabel
        Width = 191
      end
    end
  end
  object TP_CODIGO: TZetaDBEdit [9]
    Left = 74
    Top = 55
    Width = 73
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 3
    Text = 'TP_CODIGO'
    ConfirmEdit = True
    DataField = 'TP_CODIGO'
    DataSource = DataSource
  end
  object TP_NOMBRE: TDBEdit [10]
    Left = 74
    Top = 77
    Width = 383
    Height = 21
    DataField = 'TP_NOMBRE'
    DataSource = DataSource
    TabOrder = 4
  end
  object TP_INGLES: TDBEdit [11]
    Left = 74
    Top = 99
    Width = 383
    Height = 21
    DataField = 'TP_INGLES'
    DataSource = DataSource
    TabOrder = 5
  end
  object PT_NUMERO: TZetaDBNumero [12]
    Left = 74
    Top = 121
    Width = 101
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 6
    Text = '0.00'
    DataField = 'TP_NUMERO'
    DataSource = DataSource
  end
  object GroupBox1: TGroupBox [13]
    Left = 37
    Top = 177
    Width = 225
    Height = 265
    Caption = ' Conceptos Pago Pensi'#243'n Alimenticia '
    TabOrder = 8
    object btnRemoverPensiones: TSpeedButton
      Left = 192
      Top = 84
      Width = 25
      Height = 25
      Hint = 'Quitar Concepto'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
        305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
        005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
        B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
        B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
        B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
        B0557777FF577777F7F500000E055550805577777F7555575755500000555555
        05555777775555557F5555000555555505555577755555557555}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnRemoverPensionesClick
    end
    object TP_PERCEP: TDBCheckBox
      Left = 9
      Top = 18
      Width = 151
      Height = 17
      Caption = 'Todas las Percepciones'
      DataField = 'TP_PERCEP'
      DataSource = DataSource
      TabOrder = 0
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object TP_PRESTA: TDBCheckBox
      Left = 9
      Top = 34
      Width = 160
      Height = 17
      Caption = 'Todas las Prestaciones'
      DataField = 'TP_PRESTA'
      DataSource = DataSource
      TabOrder = 1
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object btnSelConceptosPago: TBitBtn
      Left = 191
      Top = 56
      Width = 26
      Height = 25
      Hint = 'Seleccionar Conceptos'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnSelConceptosPagoClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777777777777000777777777777033007777777777030B30777777777030B
        0B077777770030B0B000770000330B0B08F070333300B0B0FFF0037B7B3B0B08
        F8F007BB3B73B0FFFFF00BB3B3BB00F8F8F088303B3B70FFFFF0070703B3B0F8
        F8F00B803B3B70CCCCC070BB8BB7000000007700800077777777}
    end
    object TP_CO_PENS: TListBox
      Left = 8
      Top = 56
      Width = 177
      Height = 201
      ItemHeight = 13
      TabOrder = 3
    end
  end
  object GroupBox2: TGroupBox [14]
    Left = 266
    Top = 177
    Width = 226
    Height = 266
    Caption = ' Conceptos Deducci'#243'n Pensi'#243'n Alimenticia '
    TabOrder = 9
    object btnRemoverDescuento: TSpeedButton
      Left = 192
      Top = 84
      Width = 25
      Height = 25
      Hint = 'Quitar Concepto'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
        305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
        005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
        B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
        B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
        B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
        B0557777FF577777F7F500000E055550805577777F7555575755500000555555
        05555777775555557F5555000555555505555577755555557555}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnRemoverDescuentoClick
    end
    object btnSelConceptosDeduc: TBitBtn
      Left = 191
      Top = 56
      Width = 26
      Height = 25
      Hint = 'Seleccionar Conceptos'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnSelConceptosDeducClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777777777777000777777777777033007777777777030B30777777777030B
        0B077777770030B0B000770000330B0B08F070333300B0B0FFF0037B7B3B0B08
        F8F007BB3B73B0FFFFF00BB3B3BB00F8F8F088303B3B70FFFFF0070703B3B0F8
        F8F00B803B3B70CCCCC070BB8BB7000000007700800077777777}
    end
    object TP_CO_DESC: TListBox
      Left = 8
      Top = 56
      Width = 177
      Height = 201
      ItemHeight = 13
      TabOrder = 1
    end
  end
  object TP_APL_NOR: TDBCheckBox [15]
    Left = 137
    Top = 448
    Width = 76
    Height = 19
    Caption = 'Normal'
    DataField = 'TP_APL_NOR'
    DataSource = DataSource
    TabOrder = 10
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object TP_APL_LIQ: TDBCheckBox [16]
    Left = 227
    Top = 448
    Width = 88
    Height = 19
    Caption = 'Liquidaci'#243'n'
    DataField = 'TP_APL_LIQ'
    DataSource = DataSource
    TabOrder = 11
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object TP_APL_IND: TDBCheckBox [17]
    Left = 331
    Top = 448
    Width = 97
    Height = 19
    Caption = 'Indemnizaci'#243'n'
    DataField = 'TP_APL_IND'
    DataSource = DataSource
    TabOrder = 12
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object TP_TEXTO: TDBEdit [18]
    Left = 74
    Top = 144
    Width = 383
    Height = 21
    DataField = 'TP_TEXTO'
    DataSource = DataSource
    TabOrder = 7
  end
  inherited DataSource: TDataSource
    Left = 391
  end
end
