{$HINTS OFF}
unit FCatMatrizPuesto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, StdCtrls,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, ZetaKeyLookup_DevEx, System.Actions;

type
  TCatMatrizPuesto_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    LookUpPuesto: TZetaKeyLookup_DevEx;
    CU_CODIGO: TcxGridDBColumn;
    CU_NOMBRE: TcxGridDBColumn;
    CU_REVISIO: TcxGridDBColumn;
    EN_DIAS: TcxGridDBColumn;
    EN_OPCIONA: TcxGridDBColumn;
    EN_LISTA: TcxGridDBColumn;
    procedure LookUpPuestoValidKey(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure ConfigAgrupamiento;
  protected
    procedure Connect; override;
    procedure DisConnect;override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
  end;

var
  CatMatrizPuesto_DevEx: TCatMatrizPuesto_DevEx;

implementation

uses dCatalogos, dCliente, ZetaCommonClasses, FAutoClasses;

{$R *.DFM}

{ TCatMatrizPuesto }

procedure TCatMatrizPuesto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H60644_Matriz_puesto;
end;

procedure TCatMatrizPuesto_DevEx.DisConnect;
begin
     inherited;
     LookUpPuesto.LookUpDataset := NIL;
end;

procedure TCatMatrizPuesto_DevEx.Connect;
begin
     LookUpPuesto.LookUpDataset :=  dmCatalogos.cdsPuestos;
     with dmCatalogos do
     begin
          with cdsPuestos do
          begin
               Conectar;
               //First;
               LookUpPuesto.Llave := FieldByName('PU_CODIGO').AsString;
          end;
          SetMatriz( FALSE, LookUpPuesto.Llave );
          cdsCursos.Conectar;
          cdsMatrizCurso.Conectar;
          DataSource.DataSet:= cdsMatrizCurso;
          DoRefresh;
     end;
end;

procedure TCatMatrizPuesto_DevEx.Refresh;
begin
     dmCatalogos.cdsMatrizCurso.Refrescar;
     ZetaDBGridDBTableView.Columns[ 0 ].DataBinding.FieldName := 'CU_CODIGO';
     ZetaDBGridDBTableView.Columns[ 2 ].DataBinding.FieldName := 'CU_REVISIO';
     ZetaDBGridDBTableView.Columns[ 3 ].DataBinding.FieldName := 'EN_DIAS';
     ZetaDBGridDBTableView.Columns[ 4 ].DataBinding.FieldName := 'EN_OPCIONA';
     ZetaDBGridDBTableView.Columns[ 5 ].DataBinding.FieldName := 'EN_LISTA';
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatMatrizPuesto_DevEx.LookUpPuestoValidKey(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TCatMatrizPuesto_DevEx.Agregar;
begin
     dmCatalogos.cdsMatrizCurso.Agregar;
end;

procedure TCatMatrizPuesto_DevEx.Borrar;
begin
     dmCatalogos.cdsMatrizCurso.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatMatrizPuesto_DevEx.Modificar;
begin
     dmCatalogos.cdsMatrizCurso.Modificar;
end;

function TCatMatrizPuesto_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeAgregar( sMensaje );
end;

function TCatMatrizPuesto_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeModificar( sMensaje );
end;

function TCatMatrizPuesto_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeBorrar( sMensaje );
end;

procedure TCatMatrizPuesto_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     CU_CODIGO.Options.Grouping:= FALSE;
     CU_NOMBRE.Options.Grouping:= FALSE;
     CU_REVISIO.Options.Grouping:= FALSE;
     EN_DIAS.Options.Grouping:= FALSE;
     EN_LISTA.Options.Grouping:= FALSE;
end;

procedure TCatMatrizPuesto_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited;
     ConfigAgrupamiento;
end;

end.
