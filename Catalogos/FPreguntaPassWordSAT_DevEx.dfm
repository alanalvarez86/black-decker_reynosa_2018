inherited PreguntaPassWordSAT_DevEx: TPreguntaPassWordSAT_DevEx
  Left = 446
  Top = 246
  Caption = 'Contrase'#241'a'
  ClientHeight = 120
  ClientWidth = 232
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 84
    Width = 232
    inherited OK_DevEx: TcxButton
      Left = 66
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 146
      Top = 5
      Cancel = True
    end
  end
  object GroupBox1: TcxGroupBox [1]
    Left = 15
    Top = 8
    Caption = 'Contrase'#241'a de la Llave Privada'
    TabOrder = 1
    Height = 65
    Width = 201
    object ePassWord: TZetaEdit
      Left = 40
      Top = 28
      Width = 121
      Height = 21
      HideSelection = False
      PasswordChar = '*'
      TabOrder = 0
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
