unit FCatCursos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  TCatCursos_DevEx = class(TBaseGridLectura_DevEx)
    CU_CODIGO: TcxGridDBColumn;
    CU_DESCRIP: TcxGridDBColumn;
    CU_REVISIO: TcxGridDBColumn;
    CU_ACTIVO: TcxGridDBColumn;
    CU_STPS: TcxGridDBColumn;
    CU_CLASIFI: TcxGridDBColumn;
    CU_CLASE: TcxGridDBColumn;
    CU_HORAS: TcxGridDBColumn;
    MA_CODIGO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(Sender: TObject);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(
      Sender: TcxGridTableView; AColumn: TcxGridColumn);
    procedure DataSourceUpdateData(Sender: TObject);
  private
    AColumn: TcxGridDBColumn;
    procedure OrdenarPor( Column: TcxGridDBColumn );
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ApplyMinWidth; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatCursos_DevEx: TCatCursos_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZetaClientTools,
     ZetaClientDataSet;

{$R *.DFM}

{ TCatCursos }

procedure TCatCursos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H60641_Cursos;
end;

procedure TCatCursos_DevEx.Connect;
begin
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          DataSource.DataSet:= cdsCursos;
     end;
end;

procedure TCatCursos_DevEx.Refresh;
begin
     dmCatalogos.cdsCursos.Refrescar;
     //ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCursos_DevEx.Agregar;
begin
     dmCatalogos.cdsCursos.Agregar;
end;

procedure TCatCursos_DevEx.Borrar;
begin
     dmCatalogos.cdsCursos.Borrar;
     //ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCursos_DevEx.Modificar;
begin
     dmCatalogos.cdsCursos.Modificar;
end;

procedure TCatCursos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Curso', 'Curso', 'CU_CODIGO', dmCatalogos.cdsCursos );
end;

procedure TCatCursos_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     CU_CODIGO.Options.Grouping:= FALSE;
     CU_DESCRIP.Options.Grouping:= FALSE;
     CU_REVISIO.Options.Grouping:= FALSE;
     CU_ACTIVO.Options.Grouping:= FALSE;
     CU_STPS.Options.Grouping:= FALSE;
     CU_HORAS.Options.Grouping:= FALSE;
end;

procedure TCatCursos_DevEx.FormShow(Sender: TObject);
begin
     {***Se comenta temporalmente la sumatoria y se desactiva la funcionalidad de filtrado del grid pues se presento
     un BUG de Lentitud en esta forma por la cantidad de registros; la creacion de la columna de sumatoria no puede
     convivir con la solucion que se dio al problema (GridMode=True)***}

     //CreaColumaSumatoria(ZetaDbGridDBtableView.Columns[0],0 , '' , SkCount );
     //inherited; //Se comenta lo que se ejecutaba en al clase padre, pues GridMode no permite ApplyBestFit
     //DevEx (by am):Configuracion Especial para agrupamiento
     ConfigAgrupamiento;

     {***Se implementan las propiedades de la clase padre que se quiere mantener***}
     ZetaDBGridDBTableView.FilterBox.Visible := fvNever;  //Para que nunca muestre el filterbox inferior
     ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False; //Para que no aparezca el Custom Dialog
     ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;  //No se utilizara el filtrado, porque no funciona bien.
     ApplyMinWidth;
end;

procedure TCatCursos_DevEx.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TCatCursos_DevEx.ZetaDBGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
     OrdenarPor( AColumn );
end;

procedure TCatCursos_DevEx.OrdenarPor( Column: TcxGridDBColumn );
begin
     if Column.DataBinding.Field <> NIL then
     begin
          with Column.DataBinding.Field do
          begin
               if ( Dataset is TZetaClientDataset ) then
               begin
                    case FieldKind of
                         fkData: TZetaClientDataset( Dataset ).IndexFieldNames := FieldName;
                         fkLookup: TZetaClientDataset( Dataset ).IndexFieldNames := KeyFields;
                    end;
               end;
          end;
     end;
end;

procedure TCatCursos_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].Width := Columns[i].MinWidth; //Para que se ajuste la columna al minwidth;
          end;
     end;
end;

procedure TCatCursos_DevEx.DataSourceUpdateData(Sender: TObject);
begin
  //Para que no ejecute el codigo de la herencia.
end;

end.


