inherited EditCatValPlantillas_DevEx: TEditCatValPlantillas_DevEx
  Left = 349
  Top = 273
  Caption = 'Plantilla de Valuaci'#243'n'
  ClientHeight = 483
  ClientWidth = 477
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 56
    Top = 56
    Width = 36
    Height = 13
    Caption = 'C'#243'digo:'
  end
  object Label2: TLabel [1]
    Left = 52
    Top = 80
    Width = 40
    Height = 13
    Caption = 'Nombre:'
  end
  object Label3: TLabel [2]
    Left = 33
    Top = 104
    Width = 59
    Height = 13
    Caption = 'Descripci'#243'n:'
  end
  object Label4: TLabel [3]
    Left = 15
    Top = 260
    Width = 77
    Height = 13
    Caption = 'Tabla de Grado:'
  end
  object Label5: TLabel [4]
    Left = 24
    Top = 332
    Width = 68
    Height = 13
    Caption = 'Configuraci'#243'n:'
  end
  object Label6: TLabel [5]
    Left = 31
    Top = 424
    Width = 61
    Height = 13
    Caption = 'Valuaciones:'
  end
  object Label7: TLabel [6]
    Left = 296
    Top = 56
    Width = 33
    Height = 13
    Caption = 'Status:'
  end
  object Label12: TLabel [7]
    Left = 59
    Top = 236
    Width = 33
    Height = 13
    Caption = 'Fecha:'
  end
  object Label13: TLabel [8]
    Left = 61
    Top = 212
    Width = 31
    Height = 13
    Caption = 'Ingl'#233's:'
  end
  object Label14: TLabel [9]
    Left = 52
    Top = 284
    Width = 40
    Height = 13
    Caption = 'N'#250'mero:'
  end
  object Label15: TLabel [10]
    Left = 62
    Top = 308
    Width = 30
    Height = 13
    Caption = 'Texto:'
  end
  inherited PanelBotones: TPanel
    Top = 447
    Width = 477
    TabOrder = 13
    inherited OK_DevEx: TcxButton
      Left = 313
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 392
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 477
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 151
      inherited textoValorActivo2: TLabel
        Width = 145
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 17
  end
  object VL_CODIGO: TZetaDBNumero [14]
    Left = 96
    Top = 52
    Width = 100
    Height = 21
    Color = clBtnFace
    Enabled = False
    Mascara = mnEmpleado
    TabOrder = 1
    DataField = 'VL_CODIGO'
    DataSource = DataSource
  end
  object VL_NOMBRE: TDBEdit [15]
    Left = 96
    Top = 76
    Width = 364
    Height = 21
    DataField = 'VL_NOMBRE'
    DataSource = DataSource
    TabOrder = 2
  end
  object VL_TAB_PTS: TZetaDBKeyLookup_DevEx [16]
    Left = 96
    Top = 256
    Width = 364
    Height = 21
    LookupDataset = dmTablas.cdsNumericas
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 7
    TabStop = True
    WidthLlave = 60
    DataField = 'VL_TAB_PTS'
    DataSource = DataSource
  end
  object GroupBox1: TcxGroupBox [17]
    Left = 96
    Top = 328
    TabOrder = 10
    Height = 87
    Width = 364
    object Label8: TLabel
      Left = 24
      Top = 20
      Width = 44
      Height = 13
      Caption = 'Factores:'
    end
    object Label9: TLabel
      Left = 8
      Top = 52
      Width = 60
      Height = 13
      Caption = 'Subfactores:'
    end
    object Label10: TLabel
      Left = 184
      Top = 20
      Width = 88
      Height = 13
      Caption = 'Puntaci'#243'n m'#237'nima:'
    end
    object Label11: TLabel
      Left = 184
      Top = 52
      Width = 89
      Height = 13
      Caption = 'Puntaci'#243'n m'#225'xima:'
    end
    object VL_NUM_FAC: TZetaDBNumero
      Left = 72
      Top = 16
      Width = 65
      Height = 21
      Color = clBtnFace
      Enabled = False
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
      DataField = 'VL_NUM_FAC'
      DataSource = DataSource
    end
    object VL_NUM_SUB: TZetaDBNumero
      Left = 72
      Top = 48
      Width = 65
      Height = 21
      Color = clBtnFace
      Enabled = False
      Mascara = mnDias
      TabOrder = 1
      Text = '0'
      DataField = 'VL_NUM_SUB'
      DataSource = DataSource
    end
    object VL_MIN_PTS: TZetaDBNumero
      Left = 280
      Top = 16
      Width = 65
      Height = 21
      Color = clBtnFace
      Enabled = False
      Mascara = mnPesos
      TabOrder = 2
      Text = '0.00'
      DataField = 'VL_MIN_PTS'
      DataSource = DataSource
    end
    object VL_MAX_PTS: TZetaDBNumero
      Left = 280
      Top = 48
      Width = 65
      Height = 21
      Color = clBtnFace
      Enabled = False
      Mascara = mnPesos
      TabOrder = 3
      Text = '0.00'
      DataField = 'VL_MAX_PTS'
      DataSource = DataSource
    end
  end
  object VL_COMENTA: TcxDBMemo [18]
    Left = 96
    Top = 100
    DataBinding.DataField = 'VL_COMENTA'
    DataBinding.DataSource = DataSource
    Properties.ScrollBars = ssVertical
    TabOrder = 4
    Height = 105
    Width = 364
  end
  object VL_STATUS: TZetaDBKeyCombo [19]
    Left = 336
    Top = 52
    Width = 124
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 3
    TabStop = False
    OnChange = VL_STATUSChange
    ListaFija = lfStatusPlantilla
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'VL_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object VL_NUM_PTO: TZetaDBNumero [20]
    Left = 96
    Top = 420
    Width = 65
    Height = 21
    Color = clBtnFace
    Enabled = False
    Mascara = mnDias
    TabOrder = 11
    Text = '0'
    DataField = 'VL_NUM_PTO'
    DataSource = DataSource
  end
  object VL_FECHA: TZetaDBFecha [21]
    Left = 96
    Top = 232
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 6
    Text = '11/may/07'
    Valor = 39213.000000000000000000
    DataField = 'VL_FECHA'
    DataSource = DataSource
  end
  object VL_INGLES: TDBEdit [22]
    Left = 96
    Top = 208
    Width = 364
    Height = 21
    DataField = 'VL_INGLES'
    DataSource = DataSource
    TabOrder = 5
  end
  object VL_TEXTO: TDBEdit [23]
    Left = 96
    Top = 304
    Width = 329
    Height = 21
    DataField = 'VL_TEXTO'
    DataSource = DataSource
    TabOrder = 9
  end
  object VL_NUMERO: TZetaDBNumero [24]
    Left = 96
    Top = 280
    Width = 115
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 8
    Text = '0.00'
    DataField = 'VL_NUMERO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
