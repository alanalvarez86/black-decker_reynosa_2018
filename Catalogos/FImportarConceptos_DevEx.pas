unit FImportarConceptos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, StdCtrls, ExtCtrls, ZetaClientDataSet,
  Buttons, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxButtons, ImgList, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxTextEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxContainer, cxGroupBox,
  cxRadioGroup;

type
  TImportarConceptos_DevEx = class(TForm)
    TodosAlgunos: TcxRadioGroup;
    dsConceptos: TDataSource;
    PanelBotones: TPanel;
    cxImageList24_PanelBotones: TcxImageList;
    OK: TcxButton;
    Cancelar: TcxButton;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel1: TcxGridLevel;
    DBGrid: TcxGridDBTableView;
    DBGridCO_NUMERO: TcxGridDBColumn;
    DBGridCO_DESCRIP: TcxGridDBColumn;
    procedure TodosAlgunosClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure DBGridDblClick(Sender: TObject);
    procedure DBGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }   
    function Todos: Boolean; 
    procedure agregarEditarConcepto;
  public
    { Public declarations }
    procedure mostrarConceptos (cdsXML : TZetaClientDataSet);
  end;

var
  ImportarConceptos_DevEx: TImportarConceptos_DevEx;

implementation

uses DCatalogos, ZetaDialogo, ZetaCommonClasses;

{$R *.dfm}
            
function TImportarConceptos_DevEx.Todos: Boolean;
begin
     Result := ( TodosAlgunos.ItemIndex = 0 );
end;

procedure TImportarConceptos_DevEx.mostrarConceptos (cdsXML : TZetaClientDataSet);
begin
     dsConceptos.DataSet := cdsXML;
     ShowModal;
end;

procedure TImportarConceptos_DevEx.TodosAlgunosClick(Sender: TObject);
begin
     with DBGrid do
     begin
          if Todos then
             OptionsSelection.MultiSelect := False
          else
              OptionsSelection.MultiSelect := True;
     end;
end;

procedure TImportarConceptos_DevEx.CancelarClick(Sender: TObject);
begin
     Close;
end;

procedure TImportarConceptos_DevEx.OKClick(Sender: TObject);
var
   iCamposRows: Integer;
   sMensajeFinal: String;
   mrRespuesta: TModalResult;
begin
     sMensajeFinal := '';
     mrRespuesta := mrYes;

     // Quitar Validaci�n de CO_NUMERO > 999
     dmCatalogos.cdsConceptos.FieldByName( 'CO_NUMERO' ).OnValidate := nil;

     if Todos then
     begin
          iCamposRows := 0;
          dsConceptos.DataSet.First;
          while not dsConceptos.DataSet.Eof do
          begin

              if dmCatalogos.cdsConceptos.Locate('CO_NUMERO', dsConceptos.DataSet.FieldByName ('CO_NUMERO').AsString, []) then
              begin
                   if mrRespuesta <> mrYesToAll then
                      mrRespuesta := ZetaDialogo.ZYesToAll( Caption, 'El cat�logo ya contiene el concepto # ' + dsConceptos.DataSet.FieldByName ('CO_NUMERO').AsString
                            + '.' + CR_LF + 'Actual: ' + dmCatalogos.cdsConceptos.FieldByName( 'CO_DESCRIP' ).AsString +
                            CR_LF + 'Nuevo: ' + dsConceptos.DataSet.FieldByName( 'CO_DESCRIP' ).AsString +
                            CR_LF + '� Desea Sobreescribirlo ?', 0, mbYesToAll );
                  dmCatalogos.cdsConceptos.Edit;
              end
              else
                  dmCatalogos.cdsConceptos.Insert;

              if (mrRespuesta = mrYes) or (mrRespuesta = mrYesToAll) then
              begin
                  agregarEditarConcepto;
                  iCamposRows := iCamposRows + 1;
              end;

              dsConceptos.DataSet.Next;
          end;
          sMensajeFinal := Format ('Fueron importados %d conceptos con �xito.', [iCamposRows]);
     end
     else
     begin
          if ( DBGrid.Controller.SelectedRecordCount > 0 ) then
          begin
               for iCamposRows := 0 to ( DBGrid.Controller.SelectedRecordCount - 1 ) do
               begin
                    DBGrid.Controller.SelectedRecords[iCamposRows].Focused := true;
                    dsConceptos.DataSet.GotoBookMark( TBookMark( DBGrid.DataController.DataSet.GetBookmark) );

                    if dmCatalogos.cdsConceptos.Locate('CO_NUMERO', dsConceptos.DataSet.FieldByName ('CO_NUMERO').AsString, []) then
                    begin
                         if mrRespuesta <> mrYesToAll then
                            mrRespuesta := ZetaDialogo.ZYesToAll( Caption, 'El cat�logo ya contiene el concepto # ' + dsConceptos.DataSet.FieldByName ('CO_NUMERO').AsString
                                  + '.' + CR_LF + 'Actual: ' + dmCatalogos.cdsConceptos.FieldByName( 'CO_DESCRIP' ).AsString +
                                  CR_LF + 'Nuevo: ' + dsConceptos.DataSet.FieldByName( 'CO_DESCRIP' ).AsString +
                                  CR_LF + '� Desea Sobreescribirlo ?', 0, mbYesToAll );
                        dmCatalogos.cdsConceptos.Edit;
                    end
                    else
                        dmCatalogos.cdsConceptos.Insert;

                    if (mrRespuesta = mrYes) or (mrRespuesta = mrYesToAll) then
                    begin
                       agregarEditarConcepto;

                        sMensajeFinal := sMensajeFinal + CR_LF + 'El concepto: #' + dsConceptos.DataSet.FieldByName ('CO_NUMERO').AsString
                                      + ' - ' + dsConceptos.DataSet.FieldByName ('CO_DESCRIP').AsString + ' fue importado con �xito.';
                    end;

                    dsConceptos.DataSet.Next;
               end;
          end;
     end;

     // Mostrar mensaje final
     ZInformation(Caption, sMensajeFinal, 0);
     
     // Devolver Validaci�n de CO_NUMERO > 999
     dmCatalogos.cdsConceptos.FieldByName( 'CO_NUMERO' ).OnValidate := dmCatalogos.cdsConceptosCO_NUMEROValidate;
          
     // Cerrar
     Close;
end;

procedure TImportarConceptos_DevEx.agregarEditarConcepto;
var iCampos: Integer;
begin
     for iCampos:= 0 to (dsConceptos.DataSet.FieldCount-1) do
     begin
          if dsConceptos.DataSet.FieldList[iCampos].FieldName <> 'LLAVE' then
             dmCatalogos.cdsConceptos.FieldByName(dsConceptos.DataSet.FieldList[iCampos].FieldName).Value :=
                 dsConceptos.DataSet.FieldList[iCampos].Value;
     end;

     dmCatalogos.cdsConceptos.Enviar;
end;

procedure TImportarConceptos_DevEx.DBGridDblClick(Sender: TObject);
begin
     OK.Click;
end;

procedure TImportarConceptos_DevEx.DBGridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    if OK.Visible and OK.Enabled then
        OK.Click;
  end;
end;

end.
