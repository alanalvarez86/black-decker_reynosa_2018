unit FCatMatrizPerfilesCompetencias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, StdCtrls,
  ZbaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxTextEdit,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  ZetaKeyLookup_DevEx, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, TressMorado2013,
  dxSkinsDefaultPainters, System.Actions;

type
  TCatMatrizPerfilComps_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    lblCatalogo: TLabel;
    lookUpCatalogo: TZetaKeyLookup_DevEx;
    procedure lookUpCatalogoValidKey(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure AplicaFiltro;
  protected
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;

    procedure Refresh;override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
  end;

var
  CatMatrizPerfilComps_DevEx: TCatMatrizPerfilComps_DevEx;

implementation

uses dCatalogos, dCliente, ZetaCommonClasses, FAutoClasses,
ZetaCommonTools, dTablas;

{$R *.DFM}

{ TCatMatrizPuesto }

procedure TCatMatrizPerfilComps_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_MatrizFunciones;
end;


procedure TCatMatrizPerfilComps_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCatPerfiles.Conectar;
          cdsMatPerfilComps.Conectar;
          DataSource.DataSet:= cdsMatPerfilComps;
          lookUpCatalogo.LookUpDataset:= cdsCatPerfiles;
          AplicaFiltro;
     end;
end;

procedure TCatMatrizPerfilComps_DevEx.lookUpCatalogoValidKey(Sender: TObject);
begin
     inherited;
     AplicaFiltro;
end;

procedure TCatMatrizPerfilComps_DevEx.Agregar;
begin
     dmCatalogos.cdsMatPerfilComps.Agregar;
end;

procedure TCatMatrizPerfilComps_DevEx.Borrar;
begin
     dmCatalogos.cdsMatPerfilComps.Borrar;
end;

procedure TCatMatrizPerfilComps_DevEx.Modificar;
begin
     dmCatalogos.cdsMatPerfilComps.Modificar;
end;

function TCatMatrizPerfilComps_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeAgregar( sMensaje );
end;

function TCatMatrizPerfilComps_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeModificar( sMensaje );
end;

function TCatMatrizPerfilComps_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeBorrar( sMensaje );
end;

Procedure TCatMatrizPerfilComps_DevEx.AplicaFiltro;
begin
     with dmCatalogos.cdsMatPerfilComps do
     begin
          Filtered := False;
          Filter := Format(' CP_CODIGO = ''%s'' ',[lookUpCatalogo.Llave]);
          Filtered := True;
     end;
end;

procedure TCatMatrizPerfilComps_DevEx.Refresh;
begin
     inherited;
     dmCatalogos.cdsMatPerfilComps.Refrescar;
     AplicaFiltro;
end;


procedure TCatMatrizPerfilComps_DevEx.FormShow(Sender: TObject);
begin
     CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
     inherited;
end;

end.
