unit FCatCamposPerfil_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ZetaKeyCombo, DB, ExtCtrls,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons, ZetaSmartLists_DevEx;

type
  TCatCamposPerfil_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Panel8: TPanel;
    zmlArriba: TZetaSmartListsButton_DevEx;
    zmlAbajo: TZetaSmartListsButton_DevEx;
    Label1: TLabel;
    SECCIONES: TZetaKeyCombo;
    DF_ORDEN: TcxGridDBColumn;
    DF_TITULO: TcxGridDBColumn;
    DF_CAMPO: TcxGridDBColumn;
    DF_CONTROL: TcxGridDBColumn;
    DF_LIMITE: TcxGridDBColumn;
    procedure SECCIONESChange(Sender: TObject);
    procedure zmlArribaAbajoClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure LlenaSecciones;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatCamposPerfil_DevEx: TCatCamposPerfil_DevEx;

implementation
uses
    dCatalogos,
    ZetaBuscador_DevEX,
    ZetaCommonClasses,
    ZetaDialogo,
    ZetaCommonLists;

{$R *.dfm}

{ TCatCamposPerfil }

procedure TCatCamposPerfil_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stNinguno;
     TipoValorActivo2 := stNinguno;
     HelpContext      := H_CAT_CAMPOS_PERF;
     CanLookup     := True;
end;

procedure TCatCamposPerfil_DevEx.Connect;
begin
      LlenaSecciones;
      with dmCatalogos do
      begin
           SeccionCampos := SECCIONES.Llave;
           cdsCamposPerfil.Refrescar;
           DataSource.DataSet := cdsCamposPerfil;
      end;
end;

procedure TCatCamposPerfil_DevEx.Refresh;
begin
     dmCatalogos.cdsCamposPerfil.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCamposPerfil_DevEx.Agregar;
begin
     inherited;
     if ( dmCatalogos.cdsSeccionesPerfil.IsEmpty )then
     begin
          zInformation( Caption, 'No existen secciones definidas', 0 )
     end
     else
         dmCatalogos.cdsCamposPerfil.Agregar;
end;

procedure TCatCamposPerfil_DevEx.Borrar;
begin
     with dmCatalogos.cdsCamposPerfil do
     begin
          Borrar;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCamposPerfil_DevEx.Modificar;
begin
     inherited;
     dmCatalogos.cdsCamposPerfil.Modificar;
end;

procedure TCatCamposPerfil_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'T�tulo', 'Campos de Perfil', 'DF_TITULO', dmCatalogos.cdsCamposPerfil );
end;

procedure TCatCamposPerfil_DevEx.SECCIONESChange(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          SeccionCampos := SECCIONES.Llave;
          cdsCamposPerfil.Refrescar;
     end;
end;

procedure TCatCamposPerfil_DevEx.LlenaSecciones;
begin
     with SECCIONES.Lista do
     begin
          BeginUpdate;
          SECCIONES.Sorted := False;
          try
             Clear;
             with dmCatalogos.cdsSeccionesPerfil do
             begin
                  Conectar;
                  First;
                  while ( not EOF ) do
                  begin
                       Add( FieldByName('DT_CODIGO').AsString + ' = ' + FieldByName('DT_NOMBRE').AsString );
                       Next;
                  end;
                  First;
             end;
          finally
                 EndUpdate;
          end;
          SECCIONES.ItemIndex := 0;
     end;
end;

procedure TCatCamposPerfil_DevEx.zmlArribaAbajoClick(Sender: TObject);
var sMensaje:string;
begin
     if ( PuedeModificar (sMensaje) ) then
     begin
     	  with dmCatalogos do
     	  begin
               SubeBajaOrden( eSubeBaja( TZetaSmartListsButton_DevEx(Sender).Tag ), dmCatalogos.cdsCamposPerfil, SECCIONES.Llave,VACIO );
          end;
     end
     else
         zInformation( Caption, 'No tiene permisos para cambiar el orden', 0 );
end;

procedure TCatCamposPerfil_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     zmlArriba.Enabled := not NoHayDatos;
     zmlAbajo.Enabled := not NoHayDatos;
end;

procedure TCatCamposPerfil_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('DF_ORDEN'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
