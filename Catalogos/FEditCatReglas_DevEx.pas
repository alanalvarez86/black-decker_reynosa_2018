unit FEditCatReglas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, Buttons, DBCtrls, StdCtrls, ComCtrls,
  ZetaNumero, Mask, ZetaTipoEntidad, 
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  cxControls,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  cxPCdxBarPopupMenu, cxPC, ZetaKeyLookup_DevEx,
  cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit, TressMorado2013,
  dxSkinsDefaultPainters, dxSkinsdxBarPainter, dxSkinscxPCPainter;

type
  TEditCatReglas_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    PageControl: TcxPageControl;
    TabGenerales: TcxTabSheet;
    TabSheet2: TcxTabSheet;
    TabSheet3: TcxTabSheet;
    TabSheet4: TcxTabSheet;
    Label1: TLabel;
    Label3: TLabel;
    CL_ACTIVO: TDBCheckBox;
    CL_LETRERO: TDBEdit;
    Label2: TLabel;
    CL_TIPOS: TDBEdit;
    Label4: TLabel;
    CL_LIMITE: TZetaDBNumero;
    Label5: TLabel;
    CL_TOTAL: TcxDBMemo;
    ConsumidasBtn: TcxButton;
    Label6: TLabel;
    CL_EXTRAS: TcxDBMemo;
    ExtrasBtn: TcxButton;
    Label7: TLabel;
    CL_FILTRO: TcxDBMemo;
    FiltroBtn: TcxButton;
    CL_QUERY: TZetaDBKeyLookup_DevEx;
    Label8: TLabel;
    CL_CODIGO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    procedure ConsumidasBtnClick(Sender: TObject);
    procedure ExtrasBtnClick(Sender: TObject);
    procedure FiltroBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
  protected
    procedure Connect; override;
    procedure DoLookup; override;
  public
  end;

var
  EditCatReglas_DevEx: TEditCatReglas_DevEx;

implementation

uses dCatalogos, ZAccesosTress, ZConstruyeFormula, ZetaCommonLists,
     ZetaCommonClasses, ZetaBuscaEntero_DevEx;

{$R *.DFM}

procedure TEditCatReglas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAFE_REGLAS;
     HelpContext:= H60661_Reglas;
     FirstControl := CL_CODIGO;
     CL_QUERY.LookupDataset := dmCatalogos.cdsCondiciones;
end;

procedure TEditCatReglas_DevEx.Connect;
begin
     dmCatalogos.cdsCondiciones.Conectar;
     Datasource.Dataset := dmCatalogos.cdsReglas;
end;

procedure TEditCatReglas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'N�mero', 'Regla de Cafeter�a', 'CL_CODIGO', dmCatalogos.cdsReglas );
end;

procedure TEditCatReglas_DevEx.ConsumidasBtnClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsReglas do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CL_TOTAL').AsString := GetFormulaConst( enEmpleado, CL_TOTAL.Lines.Text, CL_TOTAL.SelStart, evBase );
     end;
end;

procedure TEditCatReglas_DevEx.ExtrasBtnClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsReglas do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CL_EXTRAS').AsString := GetFormulaConst( enEmpleado, CL_EXTRAS.Lines.Text, CL_EXTRAS.SelStart, evBase );
     end;
end;

procedure TEditCatReglas_DevEx.FiltroBtnClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsReglas do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CL_FILTRO').AsString := GetFormulaConst( enEmpleado , CL_FILTRO.Lines.Text, CL_FILTRO.SelStart, evBase );
     end;
end;

procedure TEditCatReglas_DevEx.FormShow(Sender: TObject);
begin
  inherited;
     PageControl.ActivePageIndex := 0;
     CL_TOTAL.Properties.ScrollBars := ssVertical;
     CL_EXTRAS.Properties.ScrollBars := ssVertical;
     CL_FILTRO.Properties.ScrollBars := ssVertical;
end;

end.
