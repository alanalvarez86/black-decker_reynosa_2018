unit FPrerequisitosCurso_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, StdCtrls,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ZetaKeyLookup_DevEx, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  TPrerequisitosCurso_DevEx = class(TBaseGridLectura_DevEx)
    pnCurso: TPanel;
    CU_CODIGO: TZetaKeyLookup_DevEx;
    lblCurso: TLabel;
    ZetaDBGridLevel1: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
    procedure CU_CODIGOValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure FijaLlaveCurso;
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  PrerequisitosCurso_DevEx: TPrerequisitosCurso_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

procedure TPrerequisitosCurso_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext:= H_Prerequisitos_curso;
     CU_CODIGO.LookUpDataSet := dmCatalogos.cdsCursos;
end;

procedure TPrerequisitosCurso_DevEx.FormShow(Sender: TObject);
begin
 CreaColumaSumatoria(ZetaDBGridDBTAbleView.Columns[0],0 ,'' ,SkCount ); 
     inherited;
     //FijaLlaveCurso;
end;

procedure TPrerequisitosCurso_DevEx.Connect;
begin
     with dmCatalogos.cdsCursos do
     begin
          Conectar;
          if strVacio( CU_CODIGO.Llave ) then
          begin
               CU_CODIGO.Llave := FieldByName( 'CU_CODIGO' ).AsString;
               FijaLlaveCurso;
          end;
     end;
     with dmCatalogos do
     begin
          cdsPrerequisitosCurso.Conectar;
          DataSource.DataSet:= dmCatalogos.cdsPrerequisitosCurso;
     end;
end;

procedure TPrerequisitosCurso_DevEx.Refresh;
begin
     dmCatalogos.cdsPrerequisitosCurso.Refrescar;
end;

procedure TPrerequisitosCurso_DevEx.Agregar;
begin
     dmCatalogos.cdsPrerequisitosCurso.Agregar;
end;

procedure TPrerequisitosCurso_DevEx.Borrar;
begin
     dmCatalogos.cdsPrerequisitosCurso.Borrar;
end;

procedure TPrerequisitosCurso_DevEx.Modificar;
begin
     dmCatalogos.cdsPrerequisitosCurso.Modificar;
end;

procedure TPrerequisitosCurso_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Prerrequisitos por Curso', 'CP_CURSO', dmCatalogos.cdsPrerequisitosCurso );
end;

procedure TPrerequisitosCurso_DevEx.CU_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     FijaLlaveCurso;
end;

procedure TPrerequisitosCurso_DevEx.FijaLlaveCurso;
begin
     dmCatalogos.PrerequisitosCurso := CU_CODIGO.Llave;
     DoRefresh;
end;

function TPrerequisitosCurso_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          if strVacio( CU_CODIGO.Llave )then
          begin
               sMensaje := 'Es necesario seleccionar un curso';
               Result := False;
          end;
     end;
end;

end.
