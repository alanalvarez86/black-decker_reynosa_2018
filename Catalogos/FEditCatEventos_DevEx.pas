unit FEditCatEventos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, Buttons, DBCtrls, StdCtrls, ExtCtrls, ComCtrls, Mask,
  ZetaKeyCombo, ZetaKeyLookup_DevEx, ZetaNumero, ZetaFecha, ZetaEdit, ZetaTipoEntidad,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, cxContainer,
  cxEdit, cxTextEdit, cxMemo, cxDBEdit, cxGroupBox;

type
  TEditCatEventos_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Splitter1: TSplitter;
    Label1: TLabel;
    Label2: TLabel;
    EV_ACTIVO: TDBCheckBox;
    Label3: TLabel;
    Label4: TLabel;
    PageControl: TcxPageControl;
    TabFiltros: TcxTabSheet;
    TabCambios: TcxTabSheet;
    TabSalarios: TcxTabSheet;
    Label5: TLabel;
    Label6: TLabel;
    EV_FILTRO: TcxDBMemo;
    Label7: TLabel;
    Label8: TLabel;
    TabArea: TcxTabSheet;
    EV_PUESTOLbl: TLabel;
    CB_CLASIFIlbl: TLabel;
    CB_HORARIOlbl: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    CB_SALARIOlbl: TLabel;
    EV_CODIGO: TZetaDBEdit;
    EV_DESCRIP: TDBEdit;
    EV_PRIORID: TZetaDBKeyCombo;
    EV_INCLUYE: TZetaDBKeyCombo;
    Label12: TLabel;
    EV_ANT_INI: TZetaDBNumero;
    EV_ANT_FIN: TZetaDBNumero;
    EV_QUERY: TZetaDBKeyLookup_DevEx;
    EV_PUESTO: TZetaDBKeyLookup_DevEx;
    EV_CLASIFI: TZetaDBKeyLookup_DevEx;
    EV_TURNO: TZetaDBKeyLookup_DevEx;
    EV_KARDEX: TZetaDBKeyLookup_DevEx;
    EV_TABLASS: TZetaDBKeyLookup_DevEx;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    EV_NIVEL9: TZetaDBKeyLookup_DevEx;
    EV_NIVEL8: TZetaDBKeyLookup_DevEx;
    EV_NIVEL7: TZetaDBKeyLookup_DevEx;
    EV_NIVEL6: TZetaDBKeyLookup_DevEx;
    EV_NIVEL5: TZetaDBKeyLookup_DevEx;
    EV_NIVEL4: TZetaDBKeyLookup_DevEx;
    EV_NIVEL3: TZetaDBKeyLookup_DevEx;
    EV_NIVEL2: TZetaDBKeyLookup_DevEx;
    EV_NIVEL1: TZetaDBKeyLookup_DevEx;
    EV_AUTOSAL: TDBCheckBox;
    TabBaja: TcxTabSheet;
    TabReingreso: TcxTabSheet;
    EV_BAJA: TDBCheckBox;
    EV_FEC_BSSLbl: TLabel;
    EV_FEC_BSS: TZetaDBFecha;
    EV_MOT_BAJLbl: TLabel;
    EV_MOT_BAJ: TZetaDBKeyLookup_DevEx;
    GBUltimaNomina: TcxGroupBox;
    EV_NOMTIPOLbl: TLabel;
    EV_NOMNUMELbl: TLabel;
    EV_NOMYEARLbl: TLabel;
    EV_NOMTIPO: TZetaDBKeyCombo;
    EV_NOMYEAR: TDBEdit;
    EV_ALTA: TDBCheckBox;
    EV_PATRONLbl: TLabel;
    EV_PATRON: TZetaDBKeyLookup_DevEx;
    Label9: TLabel;
    EV_NOMNUME: TZetaDBNumero;
    EV_SALARIO: TcxDBMemo;
    TB_OP1: TZetaDBKeyLookup_DevEx;
    TB_OP2: TZetaDBKeyLookup_DevEx;
    TB_OP3: TZetaDBKeyLookup_DevEx;
    TB_OP4: TZetaDBKeyLookup_DevEx;
    TB_OP5: TZetaDBKeyLookup_DevEx;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EV_TABULA: TDBCheckBox;
    SBCO_FORMULA: TcxButton;
    EV_M_ANTIG: TDBCheckBox;
    GroupBox1: TcxGroupBox;
    CB_CONTRATlbl: TLabel;
    EV_CONTRAT: TZetaDBKeyLookup_DevEx;
    EV_FEC_CON: TDBCheckBox;
    TabGenerales: TcxTabSheet;
    EV_FORMULA: TcxDBMemo;
    sbFormula: TcxButton;
    Label18: TLabel;
    Label19: TLabel;
    sbFiltro: TcxButton;
    EV_CAMPO: TZetaDBKeyCombo;
    RGTipoSaldo: TDBRadioGroup;
    EV_M_SVAC: TDBCheckBox;
    CB_NIVEL10lbl: TLabel;
    EV_NIVEL10: TZetaDBKeyLookup_DevEx;
    EV_NIVEL11: TZetaDBKeyLookup_DevEx;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL12lbl: TLabel;
    EV_NIVEL12: TZetaDBKeyLookup_DevEx;
    EV_TIPONOM_gb: TcxGroupBox;
    EV_TIPNOM_lbl: TLabel;
    EV_TIPNOM: TZetaDBKeyCombo;
    EV_CAMBNOM: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EV_ALTAClick(Sender: TObject);
    procedure EV_TABULAClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure EV_BAJAClick(Sender: TObject);
    procedure sbFiltroClick(Sender: TObject);
    procedure sbFormulaClick(Sender: TObject);
    procedure SBCO_FORMULAClick(Sender: TObject);
    procedure EV_M_ANTIGClick(Sender: TObject);
    procedure EV_NIVEL12ValidLookup(Sender: TObject);
    procedure EV_NIVEL11ValidLookup(Sender: TObject);
    procedure EV_NIVEL10ValidLookup(Sender: TObject);
    procedure EV_NIVEL9ValidLookup(Sender: TObject);
    procedure EV_NIVEL8ValidLookup(Sender: TObject);
    procedure EV_NIVEL7ValidLookup(Sender: TObject);
    procedure EV_NIVEL6ValidLookup(Sender: TObject);
    procedure EV_NIVEL5ValidLookup(Sender: TObject);
    procedure EV_NIVEL4ValidLookup(Sender: TObject);
    procedure EV_NIVEL3ValidLookup(Sender: TObject);
    procedure EV_NIVEL2ValidLookup(Sender: TObject);
    procedure EV_CAMBNOMClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    procedure DialogoFormula( const sCampo : String; oMemo : TcxDBMemo);
    procedure SetControlesTipoNomina( const lEnabled: Boolean );
    procedure InitControlesLookup;
    procedure SetCamposNivel;
    procedure SetControlesBaja(const lEnabled: Boolean);
    procedure SetControlesAlta(const lEnabled: Boolean);
    procedure SetControlTabula(const lEnabled: Boolean);
    procedure SetControlFechaContrato;
    procedure SetCamposAdicionales;
    procedure SetControlSaldoVacaciones;
    {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$ifend}

    {$ifdef ACS}procedure SetPosicionNiveles;{ACS}{$endif}
    {$ifdef ACS}
    procedure LimpiaLookUpOlfKey;{ACS}
    {$endif}
  protected
    procedure Connect; override;
    procedure DoLookup; override;
  public
  end;

var
  EditCatEventos_DevEx: TEditCatEventos_DevEx;

{$ifdef ACS}
const
     K_ALT_DEF = 24;
     K_FORMA_ACS = 495;
{$endif}

implementation

uses dCatalogos, dTablas, dGlobal, ZetaCommonTools, ZetaClientTools, ZetaCommonClasses,
     ZetaCommonLists, ZAccesosTress, ZConstruyeFormula, ZetaBuscador_DevEx, ZGlobalTress;

{$R *.DFM}

procedure TEditCatEventos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     EV_NIVEL10.DataSource := DataSource;
     EV_NIVEL11.DataSource := DataSource;
     EV_NIVEL12.DataSource := DataSource;
     {
     EV_NIVEL10.Visible := True;
     EV_NIVEL11.Visible := True;
     EV_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     }
     CB_NIVEL12lbl.Top := CB_NIVEL1lbl.Top;
     CB_NIVEL11lbl.Top := CB_NIVEL12lbl.Top + K_ALT_DEF;
     CB_NIVEL10lbl.Top := CB_NIVEL11lbl.Top + K_ALT_DEF;
     CB_NIVEL9lbl.Top   := CB_NIVEL10lbl.Top + K_ALT_DEF;
     CB_NIVEL8lbl.Top  := CB_NIVEL9lbl.Top + K_ALT_DEF;
     CB_NIVEL7lbl.Top  := CB_NIVEL8lbl.Top + K_ALT_DEF;
     CB_NIVEL6lbl.Top  := CB_NIVEL7lbl.Top + K_ALT_DEF;
     CB_NIVEL5lbl.Top  := CB_NIVEL6lbl.Top + K_ALT_DEF;
     CB_NIVEL4lbl.Top  := CB_NIVEL5lbl.Top + K_ALT_DEF;
     CB_NIVEL3lbl.Top  := CB_NIVEL4lbl.Top + K_ALT_DEF;
     CB_NIVEL2lbl.Top  := CB_NIVEL3lbl.Top + K_ALT_DEF;
     CB_NIVEL1lbl.Top  := CB_NIVEL2lbl.Top + K_ALT_DEF;

     EV_NIVEL12.Top := EV_NIVEL1.Top;
     EV_NIVEL11.Top := EV_NIVEL12.Top + K_ALT_DEF;
     EV_NIVEL10.Top := EV_NIVEL11.Top + K_ALT_DEF;
     EV_NIVEL9.Top   := EV_NIVEL10.Top + K_ALT_DEF;
     EV_NIVEL8.Top  := EV_NIVEL9.Top + K_ALT_DEF;
     EV_NIVEL7.Top  := EV_NIVEL8.Top + K_ALT_DEF;
     EV_NIVEL6.Top  := EV_NIVEL7.Top + K_ALT_DEF;
     EV_NIVEL5.Top  := EV_NIVEL6.Top + K_ALT_DEF;
     EV_NIVEL4.Top  := EV_NIVEL5.Top + K_ALT_DEF;
     EV_NIVEL3.Top  := EV_NIVEL4.Top + K_ALT_DEF;
     EV_NIVEL2.Top  := EV_NIVEL3.Top + K_ALT_DEF;
     EV_NIVEL1.Top  := EV_NIVEL2.Top + K_ALT_DEF;

     EV_NIVEL12.TabOrder := 1;
     EV_NIVEL11.TabOrder := 2;
     EV_NIVEL10.TabOrder := 3;
     EV_NIVEL9.TabOrder := 4;
     EV_NIVEL8.TabOrder := 5;
     EV_NIVEL7.TabOrder := 6;
     EV_NIVEL6.TabOrder := 7;
     EV_NIVEL5.TabOrder := 8;
     EV_NIVEL4.TabOrder := 9;
     EV_NIVEL3.TabOrder := 10;
     EV_NIVEL2.TabOrder := 11;
     EV_NIVEL1.TabOrder := 12;

     Self.Height := K_FORMA_ACS;
     {$endif}
     IndexDerechos := D_CAT_GRALES_EVENTOS;
     HelpContext:= H60635_Eventos;
     FirstControl := EV_CODIGO;
     InitControlesLookup;
     SetCamposAdicionales;

     {$ifdef PRESUPUESTOS2}
     Caption := 'Cambios y Recortes';
     {$endif}

     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditCatEventos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := TabFiltros;
     SetControlesAlta( EV_ALTA.Checked );
     SetControlesBaja( EV_BAJA.Checked );
     SetControlTabula( EV_TABULA.Checked );
     SetControlFechaContrato;
     SetControlSaldoVacaciones;
     EV_NOMTIPO.ListaFija:=lfTipoPeriodo; //acl
     EV_TIPNOM.ListaFija:=lfTipoPeriodo; //JB
     {$ifdef ACS}SetPosicionNiveles;{ACS}{$endif}
     EV_ALTA.Enabled := ( not Global.GetGlobalBooleano( K_GLOBAL_RH_VALIDA_ORGANIGRAMA ) );    // No se permiten reingresos por eventos si hay plazas (No se puede dejar vacia)
     {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     CambiosVisuales;
     {$ifend}
end;

procedure TEditCatEventos_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsMovKardex.Conectar;
          cdsMotivoBaja.Conectar;
          {$ifdef ACS}
          LimpiaLookUpOlfKey;
          {$endif}
          if EV_NIVEL1.Visible then cdsNivel1.Conectar;
          if EV_NIVEL2.Visible then cdsNivel2.Conectar;
          if EV_NIVEL3.Visible then cdsNivel3.Conectar;
          if EV_NIVEL4.Visible then cdsNivel4.Conectar;
          if EV_NIVEL5.Visible then cdsNivel5.Conectar;
          if EV_NIVEL6.Visible then cdsNivel6.Conectar;
          if EV_NIVEL7.Visible then cdsNivel7.Conectar;
          if EV_NIVEL8.Visible then cdsNivel8.Conectar;
          if EV_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if EV_NIVEL10.Visible then cdsNivel10.Conectar;
          if EV_NIVEL11.Visible then cdsNivel11.Conectar;
          if EV_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     with dmCatalogos do
     begin
          cdsCondiciones.Conectar;
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
          cdsSSocial.Conectar;
          cdsTurnos.Conectar;
          cdsContratos.Conectar;
          cdsRPatron.Conectar;
          cdsOtrasPer.Conectar;
          DataSource.Dataset := cdsEventos;
     end;
end;

procedure TEditCatEventos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Evento', 'Evento', 'EV_CODIGO', dmCatalogos.cdsEventos );
end;

procedure TEditCatEventos_DevEx.InitControlesLookup;
begin
     with dmCatalogos do
     begin
          EV_QUERY.LookupDataSet := cdsCondiciones;
          EV_PUESTO.LookupDataSet := cdsPuestos;
          EV_CLASIFI.LookupDataSet := cdsClasifi;
          EV_TABLASS.LookupDataSet := cdsSSocial;
          EV_TURNO.LookupDataSet := cdsTurnos;
          EV_CONTRAT.LookupDataSet := cdsContratos;
          TB_OP1.LookupDataSet := cdsOtrasPer;
          TB_OP2.LookupDataSet := cdsOtrasPer;
          TB_OP3.LookupDataSet := cdsOtrasPer;
          TB_OP4.LookupDataSet := cdsOtrasPer;
          TB_OP5.LookupDataSet := cdsOtrasPer;
          EV_PATRON.LookupDataSet := cdsRPatron;
     end;
     with dmTablas do
     begin
          EV_KARDEX.LookupDataSet := cdsMovKardex;
          EV_NIVEL1.LookupDataSet := cdsNivel1;
          EV_NIVEL2.LookupDataSet := cdsNivel2;
          EV_NIVEL3.LookupDataSet := cdsNivel3;
          EV_NIVEL4.LookupDataSet := cdsNivel4;
          EV_NIVEL5.LookupDataSet := cdsNivel5;
          EV_NIVEL6.LookupDataSet := cdsNivel6;
          EV_NIVEL7.LookupDataSet := cdsNivel7;
          EV_NIVEL8.LookupDataSet := cdsNivel8;
          EV_NIVEL9.LookupDataSet := cdsNivel9;
          {$ifdef ACS}
          EV_NIVEL10.LookupDataset := dmTablas.cdsNivel10;
          EV_NIVEL11.LookupDataset := dmTablas.cdsNivel11;
          EV_NIVEL12.LookupDataset := dmTablas.cdsNivel12;
          {$endif}
          EV_MOT_BAJ.LookupDataSet := cdsMotivoBaja;
     end;
     SetCamposNivel;
end;

procedure TEditCatEventos_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, EV_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, EV_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, EV_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, EV_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, EV_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, EV_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, EV_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, EV_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, EV_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, EV_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, EV_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, EV_NIVEL12 );
     {$endif}
end;

procedure TEditCatEventos_DevEx.SetCamposAdicionales;
const                       //CB_ZONA_GE,
     // AP( 20/May/2008 ): Sug 904 Historial Infonavit: Se eliminaron los campos referentes a infonavit
     K_CAMPOS_ADICIONALES = 'CB_EDO_CIV,CB_ESTADO,CB_ESTUDIO,CB_MED_TRA,CB_VIVECON,CB_VIVEEN,CB_SEXO,CB_ZONA,' +
                            'CB_CHECA,CB_EST_HOY,CB_HABLA,CB_PASAPOR,CB_FEC_RES,CB_FEC_NAC,CB_LAST_EV,CB_NEXT_EV,' +
                            'CB_BAN_ELE,CB_CARRERA,CB_CIUDAD,CB_CODPOST,CB_CREDENC,CB_CURP,CB_CALLE,CB_COLONIA,' +
                            'CB_EST_HOR,CB_EXPERIE,CB_IDIOMA,CB_LA_MAT,CB_LUG_NAC,CB_MAQUINA,CB_NACION,CB_RFC,' +
                            'CB_SEGSOC,CB_TEL,CB_EVALUA,CB_NIVEL0,CB_CLINICA,CB_G_LOG_1,' +
                            'CB_G_LOG_2,CB_G_LOG_3,CB_G_FEC_1,CB_G_FEC_2,CB_G_FEC_3,CB_G_NUM_1,CB_G_NUM_2,CB_G_NUM_3,' +
                            'CB_G_TEX_1,CB_G_TEX_2,CB_G_TEX_3,CB_G_TEX_4,CB_G_TAB_1,CB_G_TAB_2,CB_G_TAB_3,CB_G_TAB_4,' +
                            'CB_ID_NUM,CB_CANDIDA,CB_ENT_NAC, CB_COD_COL, CB_G_LOG_4,CB_G_LOG_5,CB_G_LOG_6,CB_G_LOG_7,CB_G_LOG_8,CB_G_FEC_4,' +
                            {$ifndef DOS_CAPAS}'CB_ID_BIO,CB_GP_COD,'+{$endif} // SYNERGY
                            'CB_G_FEC_5,CB_G_FEC_6,CB_G_FEC_7,CB_G_FEC_8,CB_G_NUM_4,CB_G_NUM_5,CB_G_NUM_6,CB_G_NUM_7,' +
                            'CB_G_NUM_8,CB_G_NUM_9,CB_G_NUM10,CB_G_NUM11,CB_G_NUM12,CB_G_NUM13,CB_G_NUM14,CB_G_NUM15,' +
                            'CB_G_NUM16,CB_G_NUM17,CB_G_NUM18,CB_G_TEX_6,CB_G_TEX_7,CB_G_TEX_8,CB_G_TEX_9,CB_G_TEX10,' +
                            'CB_G_TEX11,CB_G_TEX12,CB_G_TEX13,CB_G_TEX14,CB_G_TEX15,CB_G_TEX16,CB_G_TEX17,CB_G_TEX18,' +
                            'CB_G_TEX19,CB_G_TAB_5,CB_G_TAB_6,CB_G_TAB_7,CB_G_TAB_8,CB_G_TAB_9,CB_G_TAB10,CB_G_TAB11,' +
                            'CB_G_TAB12,CB_G_TAB13,CB_G_TAB14,CB_SUB_CTA,CB_NETO,CB_DISCAPA,CB_INDIGE,CB_FONACOT,CB_EMPLEO,' +
                            'CB_G_TEX20,CB_G_TEX21,CB_G_TEX22,CB_G_TEX23,CB_G_TEX24, CB_NUM_EXT, CB_NUM_INT, CB_TDISCAP,'+
                            'CB_ESCUELA,CB_TESCUEL,CB_TITULO,CB_YTITULO,CB_CTA_VAL,CB_CTA_GAS,CB_MUNICIP,EV_TIPNOM,EV_CAMBNOM,' +
                            'CB_ALERGIA,CB_TSANGRE,CB_BANCO,CB_REGIMEN';
                            // (JB) Se agrega campos
var
   oListaTemp: TStringList;
   i: Integer;
begin
     oListaTemp := TStringList.Create;
     try
        // Agregar Campos Adicionales a TStringList para que se ordenen
        with oListaTemp do
        begin
             Sorted := FALSE;
             CommaText := K_CAMPOS_ADICIONALES;
             Sorted := TRUE;
        end;
        // Traspasar oListaTemp a EV_CAMPO.Lista
        with EV_CAMPO.Lista do
        begin
             BeginUpdate;
             try
                Clear;
                Add( ' =< Ninguno >' );        // Requiere poner espacio en blanco para que funcione TZetaDBKeyCombo - No afecta al grabar, queda almacenado como string vacia
                for i := 0 to oListaTemp.Count - 1 do
                    Add( Format( '%0:s=%0:s', [ oListaTemp[i] ] ) );    // Agrega los campos ya ordenados
             finally
                EndUpdate;
             end;
        end;
     finally
        FreeAndNil( oListaTemp );
     end;
end;

procedure TEditCatEventos_DevEx.EV_BAJAClick(Sender: TObject);
begin
     inherited;
     SetControlesBaja( EV_BAJA.Checked );
end;

procedure TEditCatEventos_DevEx.SetControlesBaja( const lEnabled: Boolean );
begin
     EV_MOT_BAJ.Enabled     := lEnabled;
     EV_MOT_BAJLbl.Enabled  := lEnabled;
     EV_NOMTIPOLbl.Enabled  := lEnabled;
     EV_NOMTIPO.Enabled     := lEnabled;
     EV_NOMNUMELbl.Enabled  := lEnabled;
     EV_NOMNUME.Enabled     := lEnabled;
     EV_NOMYEARLbl.Enabled  := lEnabled;
     EV_NOMYEAR.Enabled     := lEnabled;
     GBUltimaNomina.Enabled := lEnabled;
     EV_FEC_BSSLbl.Enabled := Enabled;
     with EV_FEC_BSS do
     begin
          Enabled:= lEnabled;
          if lEnabled and ( Valor = NullDateTime ) then
             Valor := date
          else if not lEnabled and ( Valor <> NullDateTime ) then
             Valor := NullDateTime;
     end;
end;

// (JB) Se agrega metodo en caso de que sea necesario interaccion a futuro
//      con otros controles.
procedure TEditCatEventos_DevEx.SetControlesTipoNomina( const lEnabled: Boolean );
begin
     EV_TIPNOM.Enabled := lEnabled;
end;

procedure TEditCatEventos_DevEx.EV_ALTAClick(Sender: TObject);
begin
     inherited;
     SetControlesAlta( EV_ALTA.Checked );
end;

procedure TEditCatEventos_DevEx.SetControlesAlta( const lEnabled: Boolean );
begin
     EV_PATRON.Enabled := lEnabled;
     EV_PATRONLbl.Enabled := lEnabled;
     EV_M_ANTIG.Enabled := lEnabled;
     SetControlSaldoVacaciones;
end;

procedure TEditCatEventos_DevEx.EV_TABULAClick(Sender: TObject);
begin
     inherited;
     SetControlTabula( EV_TABULA.Checked );
end;

procedure TEditCatEventos_DevEx.SetControlTabula( const lEnabled: Boolean );
begin
     EV_AUTOSAL.Enabled := lEnabled;
end;

procedure TEditCatEventos_DevEx.DataSourceDataChange(Sender: TObject;  Field: TField);
begin
     inherited;
     if ( Field = nil ) or ( Field.FieldName = 'EV_CONTRAT' ) then
        SetControlFechaContrato;
end;

procedure TEditCatEventos_DevEx.SetControlFechaContrato;
begin
     EV_FEC_CON.Enabled := StrLleno(EV_CONTRAT.Llave);
end;

procedure TEditCatEventos_DevEx.SBCO_FORMULAClick(Sender: TObject);
begin
     DialogoFormula( 'EV_SALARIO', EV_SALARIO );
end;

procedure TEditCatEventos_DevEx.sbFiltroClick(Sender: TObject);
begin
     inherited;
     DialogoFormula( 'EV_FILTRO', EV_FILTRO );
end;

procedure TEditCatEventos_DevEx.sbFormulaClick(Sender: TObject);
begin
     inherited;
     DialogoFormula( 'EV_FORMULA', EV_FORMULA );
end;

procedure TEditCatEventos_DevEx.DialogoFormula( const sCampo : String; oMemo : TcxDBMemo);
begin
     with dmCatalogos.cdsEventos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( sCampo ).AsString:= GetFormulaConst( enEmpleado , oMemo.Lines.Text, oMemo.SelStart, evBase  );
     end;
end;

{ACS: Se establece la relaci�n del nivel seleccionado con el nivel que tiene relacionado
      (nivel 12 al 2, el nivel 1 no tiene relaci�n por ser el nivel inferior).}
procedure TEditCatEventos_DevEx.EV_NIVEL12ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString ) and EV_NIVEL11.Visible then
           DataSource.DataSet.FieldByName( 'EV_NIVEL11' ).AsString := dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatEventos_DevEx.EV_NIVEL11ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString ) and EV_NIVEL10.Visible then
           DataSource.DataSet.FieldByName( 'EV_NIVEL10' ).AsString := dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatEventos_DevEx.EV_NIVEL10ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString ) and EV_NIVEL9.Visible then
           DataSource.DataSet.FieldByName( 'EV_NIVEL9' ).AsString := dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatEventos_DevEx.EV_NIVEL9ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString ) and EV_NIVEL8.Visible then
           DataSource.DataSet.FieldByName( 'EV_NIVEL8' ).AsString := dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatEventos_DevEx.EV_NIVEL8ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString ) and EV_NIVEL7.Visible then
           DataSource.DataSet.FieldByName( 'EV_NIVEL7' ).AsString := dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatEventos_DevEx.EV_NIVEL7ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString ) and EV_NIVEL6.Visible then
           DataSource.DataSet.FieldByName( 'EV_NIVEL6' ).AsString := dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatEventos_DevEx.EV_NIVEL6ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString ) and EV_NIVEL5.Visible then
           DataSource.DataSet.FieldByName( 'EV_NIVEL5' ).AsString := dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatEventos_DevEx.EV_NIVEL5ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString ) and EV_NIVEL4.Visible then
           DataSource.DataSet.FieldByName( 'EV_NIVEL4' ).AsString := dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatEventos_DevEx.EV_NIVEL4ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString ) and EV_NIVEL3.Visible then
           DataSource.DataSet.FieldByName( 'EV_NIVEL3' ).AsString := dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatEventos_DevEx.EV_NIVEL3ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString ) and EV_NIVEL2.Visible then
           DataSource.DataSet.FieldByName( 'EV_NIVEL2' ).AsString := dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatEventos_DevEx.EV_NIVEL2ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString ) and EV_NIVEL1.Visible then
           DataSource.DataSet.FieldByName( 'EV_NIVEL1' ).AsString := dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

{$ifdef ACS}
{ACS: Este procedimiento re-ajustar� la posici�n de los controles de los niveles.}
procedure TEditCatEventos_DevEx.SetPosicionNiveles;
const
     K_ALTURA_DEF = 24;
     K_TOP_N1 = 265;
     K_TOP_N1_LBL = 269;
     K_TOP_N2 = 241;
     K_TOP_N2_LBL = 245;
     K_TOP_N3 = 217;
     K_TOP_N3_LBL = 221;
     K_TOP_N4 = 193;
     K_TOP_N4_LBL = 197;
     K_TOP_N5 = 169;
     K_TOP_N5_LBL = 173;
     K_TOP_N6 = 145;
     K_TOP_N6_LBL = 149;
     K_TOP_N7 = 121;
     K_TOP_N7_LBL = 125;
     K_TOP_N8 = 97;
     K_TOP_N8_LBL = 101;
     K_TOP_N9 = 73;
     K_TOP_N9_LBL = 77;
     K_TOP_N10 = 49;
     K_TOP_N10_LBL = 53;
     K_TOP_N11 = 25;
     K_TOP_N11_LBL = 29;
     K_TOP_N12 = 1;
     K_TOP_N12_LBL = 5;
var
     iNivelesNoVisibles, iTotalHorizontal: Integer;
begin
     {Se obtiene la cantidad total de niveles no visibles}
     iNivelesNoVisibles := 0;
    // iTotalHorizontal := 0;
     if Not EV_NIVEL12.Visible then
        inc( iNivelesNoVisibles );
     if Not EV_NIVEL11.Visible then
        inc( iNivelesNoVisibles );
     if Not EV_NIVEL10.Visible then
        inc( iNivelesNoVisibles );
     if Not EV_NIVEL9.Visible then
        inc( iNivelesNoVisibles );
     if Not EV_NIVEL8.Visible then
        inc( iNivelesNoVisibles );
     if Not EV_NIVEL7.Visible then
        inc( iNivelesNoVisibles );
     if Not EV_NIVEL6.Visible then
        inc( iNivelesNoVisibles );
     if Not EV_NIVEL5.Visible then
        inc( iNivelesNoVisibles );
     if Not EV_NIVEL4.Visible then
        inc( iNivelesNoVisibles );
     if Not EV_NIVEL3.Visible then
        inc( iNivelesNoVisibles );
     if Not EV_NIVEL2.Visible then
        inc( iNivelesNoVisibles );

     iTotalHorizontal := ( iNivelesNoVisibles * K_ALTURA_DEF );
     {Se re-ajustan todos los controles en la posici�n horizontal}
     EV_NIVEL12.Top := K_TOP_N12 - iTotalHorizontal;
     CB_NIVEL12lbl.Top := K_TOP_N12_LBL - iTotalHorizontal;
     EV_NIVEL11.Top := K_TOP_N11 - iTotalHorizontal;
     CB_NIVEL11lbl.Top := K_TOP_N11_LBL - iTotalHorizontal;
     EV_NIVEL10.Top := K_TOP_N10 - iTotalHorizontal;
     CB_NIVEL10lbl.Top := K_TOP_N10_LBL - iTotalHorizontal;
     EV_NIVEL9.Top := K_TOP_N9 - iTotalHorizontal;
     CB_NIVEL9lbl.Top := K_TOP_N9_LBL - iTotalHorizontal;
     EV_NIVEL8.Top := K_TOP_N8 - iTotalHorizontal;
     CB_NIVEL8lbl.Top := K_TOP_N8_LBL - iTotalHorizontal;
     EV_NIVEL7.Top := K_TOP_N7 - iTotalHorizontal;
     CB_NIVEL7lbl.Top := K_TOP_N7_LBL - iTotalHorizontal;
     EV_NIVEL6.Top := K_TOP_N6 - iTotalHorizontal;
     CB_NIVEL6lbl.Top := K_TOP_N6_LBL - iTotalHorizontal;
     EV_NIVEL5.Top := K_TOP_N5 - iTotalHorizontal;
     CB_NIVEL5lbl.Top := K_TOP_N5_LBL - iTotalHorizontal;
     EV_NIVEL4.Top := K_TOP_N4 - iTotalHorizontal;
     CB_NIVEL4lbl.Top := K_TOP_N4_LBL - iTotalHorizontal;
     EV_NIVEL3.Top := K_TOP_N3 - iTotalHorizontal;
     CB_NIVEL3lbl.Top := K_TOP_N3_LBL - iTotalHorizontal;
     EV_NIVEL2.Top := K_TOP_N2 - iTotalHorizontal;
     CB_NIVEL2lbl.Top := K_TOP_N2_LBL - iTotalHorizontal;
     EV_NIVEL1.Top := K_TOP_N1 - iTotalHorizontal;
     CB_NIVEL1lbl.Top := K_TOP_N1_LBL - iTotalHorizontal;
end;
{$endif}

{$ifdef ACS}
{Este procedimiento sirve para eliminar cualquier OLDKEY de los lookups de niveles}
procedure TEditCatEventos_DevEx.LimpiaLookUpOlfKey;
begin
          EV_NIVEL1.ResetMemory;
          EV_NIVEL2.ResetMemory;
          EV_NIVEL3.ResetMemory;
          EV_NIVEL4.ResetMemory;
          EV_NIVEL5.ResetMemory;
          EV_NIVEL6.ResetMemory;
          EV_NIVEL7.ResetMemory;
          EV_NIVEL8.ResetMemory;
          EV_NIVEL9.ResetMemory;
          EV_NIVEL10.ResetMemory;
          EV_NIVEL11.ResetMemory;
          EV_NIVEL12.ResetMemory;
end;
{$endif}

procedure TEditCatEventos_DevEx.SetControlSaldoVacaciones;
begin
     EV_M_SVAC.Enabled:= EV_M_ANTIG.Enabled and EV_M_ANTIG.Checked;
     RGTipoSaldo.Enabled:= EV_M_SVAC.Enabled and EV_M_SVAC.Checked;
end;

procedure TEditCatEventos_DevEx.EV_M_ANTIGClick(Sender: TObject);
begin
     inherited;
     SetControlSaldoVacaciones;
end;

procedure TEditCatEventos_DevEx.EV_CAMBNOMClick(Sender: TObject);
begin
  inherited;
  // (JB) Se agrega interactividad al Click del CheckBox
  SetControlesTipoNomina( EV_CAMBNOM.Checked );
end;

{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TEditCatEventos_DevEx.CambiosVisuales;
var
  I : Integer;
begin
     for I := 0 to TabArea.ControlCount - 1 do
     begin
          if TabArea.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               TabArea.Controls[I].Left := K_WIDTH_MEDIUM_LEFT -(TabArea.Controls[I].Width+2);
          end;
          if TabArea.Controls[I].ClassNameIs( 'TZetaDbKeyLookup_DevEx' ) then
          begin
               if ( TabArea.Controls[I].Visible ) then
               begin
                    TabArea.Controls[I].Width := K_WIDTH_LOOKUP;
                    TZetaDbKeyLookup_DevEx( TabArea.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                    TabArea.Controls[I].Left := K_WIDTH_MEDIUM_LEFT;
               end;
          end;
     end;
end;
{$ifend}

procedure TEditCatEventos_DevEx.SetEditarSoloActivos;
begin
     EV_CONTRAT.EditarSoloActivos := TRUE;
     EV_PUESTO.EditarSoloActivos := TRUE;
     EV_CLASIFI.EditarSoloActivos := TRUE;
     EV_TURNO.EditarSoloActivos := TRUE;
     EV_PATRON.EditarSoloActivos := TRUE;
     EV_NIVEL1.EditarSoloActivos := TRUE;
     EV_NIVEL2.EditarSoloActivos := TRUE;
     EV_NIVEL3.EditarSoloActivos := TRUE;
     EV_NIVEL4.EditarSoloActivos := TRUE;
     EV_NIVEL5.EditarSoloActivos := TRUE;
     EV_NIVEL6.EditarSoloActivos := TRUE;
     EV_NIVEL7.EditarSoloActivos := TRUE;
     EV_NIVEL8.EditarSoloActivos := TRUE;
     EV_NIVEL9.EditarSoloActivos := TRUE;
     EV_NIVEL10.EditarSoloActivos := TRUE;
     EV_NIVEL11.EditarSoloActivos := TRUE;
     EV_NIVEL12.EditarSoloActivos := TRUE;
     EV_TABLASS.EditarSoloActivos := TRUE;
end;

end.

