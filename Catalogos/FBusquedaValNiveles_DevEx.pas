unit FBusquedaValNiveles_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DBCtrls, ComCtrls, DB,
  ZetaClientDataset, ZBaseDlgModal_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters, ImgList,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls, cxPC,
  cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid;

type
  TBusquedaValNiveles_DevEx = class(TZetaDlgModal_DevEx)

    PageControl2: TcxPageControl;
    TabSheet3: TcxTabSheet;
    VN_DESCRIP: TcxDBMemo;
    TabSheet4: TcxTabSheet;
    VN_DES_ING: TcxDBMemo;
    Splitter1: TSplitter;
    DataSource: TDataSource;

    VN_ORDEN: TcxGridDBColumn;
    VN_CODIGO: TcxGridDBColumn;
    VN_NOMBRE: TcxGridDBColumn;
    VN_PUNTOS: TcxGridDBColumn;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    DBGrid: TcxGrid;
 

    procedure OK_DevExClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);

  private
    { Private declarations }
    FDataset: TZetaLookupDataset;
    FOrden: Integer;
    FFiltro: String;
    lCambioFiltro: Boolean;
    procedure Connect;
    procedure Disconnect;
    procedure SetDataset( Value: TZetaLookupDataset );
    procedure RemoveFilter;
    procedure SetFilter;
    procedure AsignaValorOrden;
    function GetAltura: Integer;
    procedure ApplyMinWidth;
    { Private declarations }
  public
    { Public declarations }
    property Dataset: TZetaLookupDataset read FDataset write SetDataset;
    property Orden: Integer read FOrden write FOrden;
    property Filtro: String read FFiltro write FFiltro;
  end;

function BuscaNivelDialogo( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String ): Boolean;

var
  BusquedaValNiveles_DevEx: TBusquedaValNiveles_DevEx;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.dfm}

function BuscaNivelDialogo( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String ): Boolean;
begin
     Result := False;
     if ( BusquedaValNiveles_DevEx = nil ) then
        BusquedaValNiveles_DevEx := TBusquedaValNiveles_DevEx.Create( Application );
     if ( BusquedaValNiveles_DevEx <> nil ) then
     begin
          with BusquedaValNiveles_DevEx do
          begin
               Dataset := LookupDataset;
               lCambioFiltro:= ( Filtro <> sFilter );
               if lCambioFiltro then
                  Filtro := sFilter;
               Orden := StrToIntDef( sKey, 0 );
               ShowModal;
               if ( ModalResult = mrOk ) and ( Orden <> 0 ) then
               begin
                    sKey := IntToStr( Orden );
                    sDescription := Dataset.GetDescription;
                    Result := True;
               end;
          end;
     end;
end;

{ ******* TBusquedaValNiveles ********* }

procedure TBusquedaValNiveles_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     ActiveControl := DBGrid;
     Connect;
     ClientHeight:= GetAltura;

      //Desactiva la posibilidad de agrupar
      ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
      //Esconde la caja de agrupamiento
      ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
      //Para que nunca muestre el filterbox inferior
      ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
      //Para que no aparezca el Custom Dialog
      ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
      //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
      ZetaDBGridDBTableView.ApplyBestFit();

      ZetaDBGridDBTableView.OptionsData.Editing := False;
end;

procedure TBusquedaValNiveles_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     Disconnect;
     Action := caHide;
end;

//(@am):Para el keypress en DBTableViews funcione
procedure TBusquedaValNiveles_DevEx.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
     if ( ActiveControl.parent.classname = 'TZetaCXGrid' )then
     begin
        if Key = Chr( VK_RETURN )then
        begin
             if OK_DevEx.Visible and OK_DevEx.Enabled then
             begin
                  Key := #0; //Limpiamos el Key para que no se ejecute el ENTER de la clase padre
                  OK_DevEx.Click;
             end;
        end;
     end;
     inherited;  //Se ejecuta KeyPress de la clase padre
end;

procedure TBusquedaValNiveles_DevEx.Connect;
begin
     SetFilter;
     DataSource.Dataset := Dataset;
     DataSet.Conectar;
end;

procedure TBusquedaValNiveles_DevEx.Disconnect;
var
   bAnterior: TBookMark;
begin
     DataSource.Dataset := nil;
     with DataSet do
     begin
          if Filtered then
          begin
               bAnterior := GetBookMark;
               RemoveFilter;
               GotoBookMark( bAnterior );
               FreeBookMark( bAnterior );
          end;
     end;
end;

procedure TBusquedaValNiveles_DevEx.SetDataset(Value: TZetaLookupDataset);
begin
     if ( FDataset <> Value ) then
     begin
          FDataset := Value;
     end;
end;

procedure TBusquedaValNiveles_DevEx.SetFilter;
begin
     if ( Filtro <> '' ) then
     begin
          with Dataset do
          begin
               DisableControls;
               try
                  Filtered := False;
                  Filter := Filtro;
                  Filtered := True;
                  if ( Orden <> 0 ) then
                     Locate('VN_ORDEN',Orden,[]);
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TBusquedaValNiveles_DevEx.RemoveFilter;
begin
     with Dataset do
     begin
          if Filtered then
          begin
               Filtered := False;
               Filter := '';
          end;
     end;
end;

procedure TBusquedaValNiveles_DevEx.AsignaValorOrden;
begin
     FOrden := Dataset.FieldByName( 'VN_ORDEN' ).AsInteger;
end;

procedure TBusquedaValNiveles_DevEx.ZetaDBGridDBTableViewDblClick(Sender: TObject);
begin
     inherited;
     AsignaValorOrden;
     ModalResult:= mrOk;
end;

procedure TBusquedaValNiveles_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     AsignaValorOrden;
end;

function TBusquedaValNiveles_DevEx.GetAltura: Integer;
const
     K_MAX_ALTURA = 560;
     K_MIN_ALTURA = 335;
     aValMaxMin: array [ FALSE..TRUE ] of Integer = ( 560, 335 );
     K_PIXELES_X_REGISTRO = 65;
var
   iAltura: Integer;
begin
     with DataSet do
     begin
          iAltura:= ( RecordCount * K_PIXELES_X_REGISTRO );
          Result:= iMin( K_MAX_ALTURA, iMax( iAltura, K_MIN_ALTURA ) );
          //Result:= iAltura - ( iAltura - aValMaxMin[ RecordCount <= 5 ] );
     end;
end;

procedure TBusquedaValNiveles_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

end.
