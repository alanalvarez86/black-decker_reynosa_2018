unit FEditCatRPatron_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, DBCtrls, ComCtrls, ExtCtrls, Buttons, Db, Grids, DBGrids,
     ZBaseEdicionRenglon_DevEx,
     ZetaKeyCombo,
     ZetaNumero,
     ZetaDBGrid,
     ZetaEdit,
     ZetaMessages, ZetaKeyLookup_DevEx, 
     cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinscxPCPainter,
     cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
     ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons,
     cxContainer, cxEdit, cxGroupBox, cxListBox, cxRadioGroup;

type
  TEditCatRPatron_DevEx = class(TBaseEdicionRenglon_DevEx)
    Label3: TLabel;
    TB_INGLES: TDBEdit;
    Label5: TLabel;
    TB_TEXTO: TDBEdit;
    Label6: TLabel;
    Label4: TLabel;
    TB_NUMREG: TDBEdit;
    TB_MODULO: TZetaDBKeyCombo;
    Label7: TLabel;
    Label1: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    Label2: TLabel;
    TB_NUMERO: TZetaDBNumero;
    RS_CODIGO: TZetaDBKeyLookup_DevEx;
    lbRazonsocial: TLabel;
    Label8: TLabel;
    TB_CALLE: TDBEdit;
    TB_NUMEXT: TDBEdit;
    TB_NUMINT: TDBEdit;
    Label9: TLabel;
    Label10: TLabel;
    TB_COLONIA: TDBEdit;
    TB_CIUDAD: TDBEdit;
    TB_ENTIDAD: TZetaDBKeyLookup_DevEx;
    TB_CODPOST: TDBEdit;
    TB_TEL: TDBEdit;
    TB_EMAIL: TDBEdit;
    TB_FAX: TDBEdit;
    TB_WEB: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    TB_CLASE: TZetaDBKeyCombo;
    TB_FRACC: TZetaDBNumero;
    lblClase: TLabel;
    lblFraccion: TLabel;
    STPS: TcxTabSheet;
    GBPrimaRiesgo: TcxGroupBox;
    TB_STYPS: TDBCheckBox;
    GroupBox1: TcxGroupBox;
    TB_SIEM: TDBEdit;
    TB_PLANPRE: TDBCheckBox;
    Label19: TLabel;
    TB_PLANPER: TDBCheckBox;
    TB_PLANFOL: TDBEdit;
    lblFolioPlan: TLabel;
    TB_ACTIVO: TDBCheckBox;
    tsConfidencialidad: TcxTabSheet;
    gbConfidencialidad: TcxGroupBox;
    btSeleccionarConfiden: TcxButton;
    listaConfidencialidad: TcxListBox;
    rbConfidenTodas: TcxRadioButton;
    rbConfidenAlgunas: TcxRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure TB_PLANPERClick(Sender: TObject);
    procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations } 
    lValoresConfidencialidad : TStringList;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure SetControlesFraccion;
    procedure SetControlesFormaDC4;   
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;
  protected
    { Protected declarations }
    procedure Connect;override;
  public
    { Public declarations }
  end;

var
  EditCatRPatron_DevEx: TEditCatRPatron_DevEx;

implementation

uses ZetaCommonClasses,
     ZAccesosTress,
     DCatalogos,
     dTablas, {OP: 09/06/08}
     ZetaCommonLists,
     FSeleccionarConfidencialidad_DevEx, dSistema, DCliente, ZetaCommonTools;

{$R *.DFM}

procedure TEditCatRPatron_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H60631_Registros_patronales;
     IndexDerechos := ZAccesosTress.D_CAT_GRALES_PATRONALES;
     FirstControl :=  TB_CODIGO;
     RS_CODIGO.LookupDataSet:= dmCatalogos.cdsRSocial;
     lValoresConfidencialidad := TStringList.Create;
end;

procedure TEditCatRPatron_DevEx.Connect;
begin     
     dmSistema.cdsNivel0.Conectar;
     with dmCatalogos do
     begin
          cdsRSocial.Conectar;
          cdsRPatron.Conectar;
          Datasource.Dataset := cdsRPatron;

          cdsPRiesgo.Conectar;
          dsRenglon.DataSet := cdsPRiesgo;
     end;
     dmTablas.cdsEstado.Conectar;{OP: 09/06/08}
end;

procedure TEditCatRPatron_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  FillListaConfidencialidad;
end;

procedure TEditCatRPatron_DevEx.WMExaminar(var Message: TMessage);
begin
     with dmCatalogos.cdsPRiesgo do
     begin
          Next;
          if EOF then
             Append;
     end;
end;

procedure TEditCatRPatron_DevEx.SetControlesFraccion;
begin
     TB_FRACC.Enabled:= (TB_CLASE.Valor <> Ord(claseNingun));
     lblFraccion.Enabled:= TB_FRACC.Enabled;
end;

procedure TEditCatRPatron_DevEx.TB_PLANPERClick(Sender: TObject);
begin
     inherited;
     SetControlesFormaDC4;
end;

procedure TEditCatRPatron_DevEx.SetControlesFormaDC4;
begin
     TB_PLANFOL.Enabled := TB_PLANPER.Checked or TB_PLANPRE.Checked ;
     lblFolioPlan.Enabled := TB_PLANFOL.Enabled;
end;

procedure TEditCatRPatron_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;

procedure TEditCatRPatron_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditCatRPatron_DevEx.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('TB_NIVEL0').AsString );
   end;
end;

procedure TEditCatRPatron_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;
                                       
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEditCatRPatron_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('TB_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEditCatRPatron_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfidenClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TEditCatRPatron_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited; 
     SetControlesFraccion;
     SetControlesFormaDC4;
     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
                  GetConfidencialidad
               else if (State in [dsInsert]) then
                    rbConfidenTodas.Checked := TRUE;
          end;
     end;
end;

end.
