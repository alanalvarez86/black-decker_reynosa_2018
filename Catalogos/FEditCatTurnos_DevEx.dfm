inherited EditCatTurnos_DevEx: TEditCatTurnos_DevEx
  Left = 427
  Top = 159
  ActiveControl = TU_CODIGO
  Caption = 'Turnos'
  ClientHeight = 381
  ClientWidth = 551
  ExplicitWidth = 557
  ExplicitHeight = 410
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 345
    Width = 551
    TabOrder = 2
    ExplicitTop = 345
    ExplicitWidth = 551
    inherited OK_DevEx: TcxButton
      Left = 385
      ExplicitLeft = 385
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 464
      ExplicitLeft = 464
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 551
    TabOrder = 4
    ExplicitTop = 26
    ExplicitWidth = 551
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 317
      end
    end
    inherited ValorActivo2: TPanel
      Width = 225
      ExplicitWidth = 225
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 219
        ExplicitLeft = 139
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Width = 72
    ExplicitWidth = 72
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 45
    Width = 551
    Height = 54
    Align = alTop
    TabOrder = 0
    object TU_CODIGOlbl: TLabel
      Left = 49
      Top = 8
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'Turno:'
      FocusControl = TU_CODIGO
    end
    object TU_DESCRIPlbl: TLabel
      Left = 21
      Top = 30
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
      FocusControl = TU_DESCRIP
    end
    object TU_CODIGO: TZetaDBEdit
      Tag = 99
      Left = 84
      Top = 4
      Width = 60
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'TU_CODIGO'
      DataSource = DataSource
    end
    object TU_DESCRIP: TDBEdit
      Left = 84
      Top = 26
      Width = 230
      Height = 21
      DataField = 'TU_DESCRIP'
      DataSource = DataSource
      TabOrder = 2
    end
    object TU_ACTIVO: TDBCheckBox
      Left = 325
      Top = 28
      Width = 52
      Height = 17
      Alignment = taLeftJustify
      BiDiMode = bdLeftToRight
      Caption = 'Activo:'
      DataField = 'TU_ACTIVO'
      DataSource = DataSource
      ParentBiDiMode = False
      TabOrder = 3
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object bbMostrarCalendario_DevEx: TcxButton
      Left = 146
      Top = 2
      Width = 21
      Height = 21
      Hint = 'Mostrar Calendario del Turno'
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5
        BFFFECD5BFFFF5EADFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5
        BFFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB5
        8FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5BFFFECD5BFFFF5EA
        DFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5BFFFFAF4EFFFFFFF
        FFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF6
        F1FFDDB58FFFDDB58FFFEEDAC7FFE1BE9DFFDDB58FFFE1BE9DFFEEDAC7FFDDB5
        8FFFDDB58FFFF6ECE3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
        7FFFDDB58FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFBF9FFF0DFCFFFF0DF
        CFFFF8EFE7FFF2E3D5FFF0DFCFFFF2E3D5FFF8EFE7FFF0DFCFFFF0DFCFFFFBF7
        F3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4
        EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB58FFFECD5
        BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF7F3FFE2BF9FFFE2BF9FFFF0DFCFFFE5C7
        ABFFE2BF9FFFE5C7ABFFF0DFCFFFE2BF9FFFE2BF9FFFF8EFE7FFFFFFFFFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFFAF4EFFFFFFFFFFFE5C7ABFFE1BE9DFFDFBA97FFFBF6F1FFFFFFFFFFFFFF
        FFFFDEB793FFE2BF9FFFE2BF9FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFF3E4D7FFFFFFFFFFE2BF9FFFFAF4EFFFECD5
        BFFFFAF4EFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFE2BF9FFFFFFFFFFFF8EF
        E7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE1BE
        9DFFDAAF87FFF2E3D5FFE5C7ABFFE1BD9BFFE2BF9FFFE2BF9FFFDEB691FFF8F0
        E9FFDBB189FFE1BE9DFFD9AD83FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      PaintStyle = bpsGlyph
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = bbMostrarCalendario_DevExClick
    end
  end
  object PageControl_DevEx: TcxPageControl [4]
    Left = 0
    Top = 99
    Width = 551
    Height = 246
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = TabDatos_DevEx
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 245
    ClientRectLeft = 1
    ClientRectRight = 550
    ClientRectTop = 21
    object TabDatos_DevEx: TcxTabSheet
      Caption = 'Generales'
      object lblTipoJornada: TLabel
        Left = 12
        Top = 162
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo Jornada:'
        Visible = False
      end
      object lblSubCuenta: TLabel
        Left = 23
        Top = 186
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Subcuenta:'
        FocusControl = TU_SUB_CTA
      end
      object Label1: TLabel
        Left = 10
        Top = 112
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Jornada IMSS:'
      end
      object Label6: TLabel
        Left = 50
        Top = 64
        Width = 30
        Height = 13
        Caption = 'Texto:'
      end
      object Label5: TLabel
        Left = 40
        Top = 40
        Width = 40
        Height = 13
        Caption = 'N'#250'mero:'
      end
      object Label3: TLabel
        Left = 49
        Top = 15
        Width = 31
        Height = 13
        Caption = 'Ingl'#233's:'
      end
      object TU_HORARIOlbl: TLabel
        Left = 56
        Top = 88
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object Label7: TLabel
        Left = 12
        Top = 138
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Jornada SAT:'
      end
      object TU_TIP_JT: TZetaDBKeyCombo
        Left = 84
        Top = 158
        Width = 229
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 5
        Visible = False
        ListaFija = lfTipoJornadaTrabajo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        LlaveNumerica = True
      end
      object TU_SUB_CTA: TDBEdit
        Left = 84
        Top = 182
        Width = 230
        Height = 21
        DataField = 'TU_SUB_CTA'
        DataSource = DataSource
        TabOrder = 6
      end
      object gbJornada: TGroupBox
        Left = 344
        Top = 7
        Width = 193
        Height = 119
        Caption = ' Jornada '
        TabOrder = 7
        object TU_JORNADAlbl: TLabel
          Left = 45
          Top = 14
          Width = 67
          Height = 13
          Alignment = taRightJustify
          Caption = 'D'#237'as Jornada:'
        end
        object Label2: TLabel
          Left = 40
          Top = 35
          Width = 72
          Height = 13
          Alignment = taRightJustify
          Caption = 'Horas Jornada:'
        end
        object TU_DOBLESlbl: TLabel
          Left = 48
          Top = 56
          Width = 64
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tope Dobles:'
        end
        object TU_DOMINGOlbl: TLabel
          Left = 34
          Top = 98
          Width = 78
          Height = 13
          Alignment = taRightJustify
          Caption = 'Prima Dominical:'
        end
        object Label4: TLabel
          Left = 23
          Top = 77
          Width = 89
          Height = 13
          Alignment = taRightJustify
          Caption = 'Dias base a pagar:'
        end
        object TU_DIAS: TZetaDBNumero
          Left = 115
          Top = 10
          Width = 72
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          UseEnterKey = True
          DataField = 'TU_DIAS'
          DataSource = DataSource
        end
        object TU_JORNADA: TZetaDBNumero
          Left = 115
          Top = 31
          Width = 72
          Height = 21
          Mascara = mnHoras
          TabOrder = 1
          Text = '0.00'
          UseEnterKey = True
          DataField = 'TU_JORNADA'
          DataSource = DataSource
        end
        object TU_DOBLES: TZetaDBNumero
          Left = 115
          Top = 52
          Width = 72
          Height = 21
          Mascara = mnHoras
          TabOrder = 2
          Text = '0.00'
          UseEnterKey = True
          DataField = 'TU_DOBLES'
          DataSource = DataSource
        end
        object TU_DOMINGO: TZetaDBNumero
          Left = 115
          Top = 94
          Width = 72
          Height = 21
          Mascara = mnHoras
          TabOrder = 4
          Text = '0.00'
          UseEnterKey = True
          DataField = 'TU_DOMINGO'
          DataSource = DataSource
        end
        object TU_DIAS_BA: TZetaDBNumero
          Left = 115
          Top = 73
          Width = 72
          Height = 21
          Mascara = mnNumeroGlobal
          TabOrder = 3
          Text = '0.00'
          UseEnterKey = True
          DataField = 'TU_DIAS_BA'
          DataSource = DataSource
        end
      end
      object gbVacaciones: TGroupBox
        Left = 344
        Top = 129
        Width = 193
        Height = 84
        Caption = ' Valor De Cada D'#237'a En Vacaciones '
        TabOrder = 8
        object lblHabil: TLabel
          Left = 84
          Top = 17
          Width = 27
          Height = 13
          Alignment = taRightJustify
          Caption = 'H'#225'bil:'
        end
        object lblSabado: TLabel
          Left = 20
          Top = 40
          Width = 91
          Height = 13
          Alignment = taRightJustify
          Caption = 'S'#225'bado Trabajado:'
        end
        object lblFestivo: TLabel
          Left = 21
          Top = 63
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descanso/Festivo:'
        end
        object TU_VACA_HA: TZetaDBNumero
          Left = 114
          Top = 13
          Width = 72
          Height = 21
          Mascara = mnHoras
          TabOrder = 0
          Text = '0.00'
          DataField = 'TU_VACA_HA'
          DataSource = DataSource
        end
        object TU_VACA_SA: TZetaDBNumero
          Left = 114
          Top = 36
          Width = 72
          Height = 21
          Mascara = mnHoras
          TabOrder = 1
          Text = '0.00'
          DataField = 'TU_VACA_SA'
          DataSource = DataSource
        end
        object TU_VACA_DE: TZetaDBNumero
          Left = 114
          Top = 59
          Width = 72
          Height = 21
          Mascara = mnHoras
          TabOrder = 2
          Text = '0.00'
          DataField = 'TU_VACA_DE'
          DataSource = DataSource
        end
      end
      object TB_TEXTO: TDBEdit
        Left = 84
        Top = 62
        Width = 230
        Height = 21
        DataField = 'TU_TEXTO'
        DataSource = DataSource
        TabOrder = 2
      end
      object TB_NUMERO: TZetaDBNumero
        Left = 84
        Top = 38
        Width = 121
        Height = 21
        Mascara = mnNumeroGlobal
        TabOrder = 1
        Text = '0.00'
        UseEnterKey = True
        DataField = 'TU_NUMERO'
        DataSource = DataSource
      end
      object TB_INGLES: TDBEdit
        Left = 84
        Top = 13
        Width = 230
        Height = 21
        DataField = 'TU_INGLES'
        DataSource = DataSource
        TabOrder = 0
      end
      object TU_TIP_JOR: TZetaDBKeyCombo
        Left = 84
        Top = 110
        Width = 229
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 4
        ListaFija = lfTipoJornada
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'TU_TIP_JOR'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object TU_HORARIO: TZetaDBKeyCombo
        Left = 84
        Top = 86
        Width = 229
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 3
        ListaFija = lfTipoTurno
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'TU_HORARIO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object TIPO_JORNADA_SAT: TZetaDBKeyLookup_DevEx
        Left = 84
        Top = 134
        Width = 231
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        WidthLlave = 60
        DataField = 'TU_SAT_JOR'
        DataSource = DataSource
      end
    end
    object TabHorarios_DevEx: TcxTabSheet
      Caption = 'Horarios'
      object DiaFestLbl: TLabel
        Left = 4
        Top = 193
        Width = 135
        Height = 13
        Alignment = taRightJustify
        Caption = 'Horario Festivos Trabajados:'
      end
      object gbHorSemana: TGroupBox
        Left = 0
        Top = 0
        Width = 549
        Height = 185
        Align = alTop
        Caption = ' D'#237'as de la Semana: '
        TabOrder = 0
        object Dia1Lbl: TLabel
          Left = 24
          Top = 21
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Domingo:'
        end
        object Dia2Lbl: TLabel
          Left = 24
          Top = 43
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Domingo:'
        end
        object Dia3Lbl: TLabel
          Left = 24
          Top = 65
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Domingo:'
        end
        object Dia4Lbl: TLabel
          Left = 24
          Top = 86
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Domingo:'
        end
        object Dia5Lbl: TLabel
          Left = 24
          Top = 107
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Domingo:'
        end
        object Dia6Lbl: TLabel
          Left = 24
          Top = 128
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Domingo:'
        end
        object Dia7Lbl: TLabel
          Left = 24
          Top = 149
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Domingo:'
        end
        object TU_TIP_1: TZetaDBKeyCombo
          Left = 384
          Top = 17
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfStatusAusencia
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'TU_TIP_1'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object TU_TIP_2: TZetaDBKeyCombo
          Left = 384
          Top = 39
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
          ListaFija = lfStatusAusencia
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'TU_TIP_2'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object TU_TIP_3: TZetaDBKeyCombo
          Left = 384
          Top = 61
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 5
          ListaFija = lfStatusAusencia
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'TU_TIP_3'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object TU_TIP_4: TZetaDBKeyCombo
          Left = 384
          Top = 83
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 7
          ListaFija = lfStatusAusencia
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'TU_TIP_4'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object TU_TIP_5: TZetaDBKeyCombo
          Left = 384
          Top = 105
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 9
          ListaFija = lfStatusAusencia
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'TU_TIP_5'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object TU_TIP_6: TZetaDBKeyCombo
          Left = 384
          Top = 127
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 11
          ListaFija = lfStatusAusencia
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'TU_TIP_6'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object TU_TIP_7: TZetaDBKeyCombo
          Left = 384
          Top = 149
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 13
          ListaFija = lfStatusAusencia
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'TU_TIP_7'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object TU_HOR_1: TZetaDBKeyLookup_DevEx
          Left = 74
          Top = 17
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsHorarios
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'TU_HOR_1'
          DataSource = DataSource
        end
        object TU_HOR_2: TZetaDBKeyLookup_DevEx
          Left = 74
          Top = 39
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsHorarios
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
          DataField = 'TU_HOR_2'
          DataSource = DataSource
        end
        object TU_HOR_3: TZetaDBKeyLookup_DevEx
          Left = 74
          Top = 61
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsHorarios
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 4
          TabStop = True
          WidthLlave = 60
          DataField = 'TU_HOR_3'
          DataSource = DataSource
        end
        object TU_HOR_4: TZetaDBKeyLookup_DevEx
          Left = 74
          Top = 83
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsHorarios
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 6
          TabStop = True
          WidthLlave = 60
          DataField = 'TU_HOR_4'
          DataSource = DataSource
        end
        object TU_HOR_5: TZetaDBKeyLookup_DevEx
          Left = 74
          Top = 105
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsHorarios
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 8
          TabStop = True
          WidthLlave = 60
          DataField = 'TU_HOR_5'
          DataSource = DataSource
        end
        object TU_HOR_6: TZetaDBKeyLookup_DevEx
          Left = 74
          Top = 127
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsHorarios
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 10
          TabStop = True
          WidthLlave = 60
          DataField = 'TU_HOR_6'
          DataSource = DataSource
        end
        object TU_HOR_7: TZetaDBKeyLookup_DevEx
          Left = 74
          Top = 149
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsHorarios
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 12
          TabStop = True
          WidthLlave = 60
          DataField = 'TU_HOR_7'
          DataSource = DataSource
        end
      end
      object TU_HOR_FES: TZetaDBKeyLookup_DevEx
        Left = 144
        Top = 189
        Width = 385
        Height = 21
        LookupDataset = dmCatalogos.cdsHorarios
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'TU_HOR_FES'
        DataSource = DataSource
      end
    end
    object TabRitmicos_DevEx: TcxTabSheet
      Caption = 'R'#237'tmicos'
      ImageIndex = 2
      object TU_RIT_PATlbl: TLabel
        Left = 85
        Top = 11
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Patr'#243'n:'
        FocusControl = TU_RIT_PAT
      end
      object TU_RIT_INIlbl: TLabel
        Left = 46
        Top = 144
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio de Ritmo:'
      end
      object TU_RIT_INI: TZetaDBFecha
        Left = 122
        Top = 140
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '23/dic./97'
        UseEnterKey = True
        Valor = 35787.000000000000000000
        DataField = 'TU_RIT_INI'
        DataSource = DataSource
      end
      object TU_RIT_PAT: TcxDBMemo
        Left = 122
        Top = 8
        DataBinding.DataField = 'TU_RIT_PAT'
        DataBinding.DataSource = DataSource
        Properties.ScrollBars = ssVertical
        Properties.OnChange = TU_RIT_PATChange
        TabOrder = 1
        OnKeyPress = TU_RIT_PATKeyPress
        Height = 129
        Width = 365
      end
    end
    object tsConfidencialidad_DevEx: TcxTabSheet
      Caption = 'Confidencialidad'
      ImageIndex = 3
      object gbConfidencialidad: TGroupBox
        Left = 0
        Top = 0
        Width = 549
        Height = 224
        Align = alClient
        TabOrder = 0
        object listaConfidencialidad: TListBox
          Left = 118
          Top = 56
          Width = 285
          Height = 125
          TabStop = False
          ExtendedSelect = False
          ItemHeight = 13
          TabOrder = 2
        end
        object rbConfidenTodas: TRadioButton
          Left = 118
          Top = 18
          Width = 113
          Height = 17
          Caption = 'Todas'
          TabOrder = 0
          OnClick = rbConfidenTodasClick
        end
        object rbConfidenAlgunas: TRadioButton
          Left = 118
          Top = 36
          Width = 156
          Height = 17
          Caption = 'Aplica algunas'
          TabOrder = 1
          OnClick = rbConfidenAlgunasClick
        end
        object btSeleccionarConfiden_DevEx: TcxButton
          Left = 408
          Top = 56
          Width = 26
          Height = 26
          Hint = 'Seleccionar Confidencialidad'
          OptionsImage.Glyph.Data = {
            36090000424D3609000000000000360000002800000018000000180000000100
            200000000000000900000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFAFE7F8FF3CC4EEFF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFCBEF
            FBFF5CCEF1FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF78D6F3FFFFFFFFFFFFFFFFFFE7F8
            FDFF70D4F3FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEFFAFEFF80D9F4FF10B7
            EAFF00B2E9FF0CB6EAFF38C3EEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF38C3EEFF18B9EBFF00B2E9FF00B2
            E9FF97E0F6FFE3F7FDFFC7EEFAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFBFEC
            F9FFFBFEFFFFFFFFFFFFFFFFFFFF5CCEF1FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEBF9FDFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFE7F8FDFF0CB6EAFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF20BCECFFF7FDFEFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83DAF4FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF83DAF4FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FDFEFF24BDECFF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0CB6EAFFE7F8
            FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABE6F8FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CF
            F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3EDFAFF34C2EDFF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FFCBEFFBFFFFFFFFFFDFF5FCFF58CDF1FF00B2E9FF00B2E9FF10B7EAFF0CB6
            EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF3CC4EEFF7CD7F4FF08B4EAFF00B2E9FF04B3E9FF70D4F3FFEFFAFEFF74D5
            F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF50CAF0FFDBF4FCFFFFFFFFFFFFFFFFFF68D1
            F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF04B3E9FFB7E9F9FFFFFFFFFFFFFFFFFFCBEFFBFF3CC4EEFF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF60CFF1FFCBEFFBFF5CCEF1FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          PaintStyle = bpsGlyph
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btSeleccionarConfiden_DevExClick
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 204
    Top = 44
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF98A1E2FFF6F7FDFFB7BEEBFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFB1B8
          E9FFF6F7FDFFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF8792DEFFDFE2F6FFA1A9E5FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8
          A1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF8CE2B8FFE9F9
          F1FF53D396FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF51D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF6FDAA7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF51D395FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF59D59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF61D79FFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4ED293FF53D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF8AE1B7FFE6F9F0FF69D9A4FF4ED293FF4ED293FF4ED2
          93FF5ED69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF51D395FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF64D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF67D8A2FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF69D9A4FFEFFBF5FFFFFFFFFFDEF7EBFF51D395FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF6FDAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF56D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF81DFB1FF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF5160CFFF6B78D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6E7BD7FF7482D9FF6370D4FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFF969FE2FF6370D4FF969FE2FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4656CCFF4656CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4656CCFF4656CCFF6E7BD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF6875D6FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF5463D0FF7482D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      26
      0)
    inherited BarSuperior: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarControlContainerItem_DBNavigator'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_AgregarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_BorrarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ModificarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_BuscarBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_ImprimirBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ImprimirFormaBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ExportarBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_CortarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_CopiarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_PegarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_UndoBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_bImpSubCuentas'
        end>
    end
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
    object dxBarButton_bImpSubCuentas: TdxBarButton
      Category = 0
      Hint = 'Importaci'#243'n de Subcuentas Contables'
      Visible = ivAlways
      ImageIndex = 12
      OnClick = dxBarButton_bImpSubCuentasClick
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000190F061C6F431A7C9C5F24AFCC7B2FE3D78033EFB36C
          29C78F55229F39230E4000000000000000000000000000000000000000001D11
          0720000000004F2F1358D78033EFE58A36FFE58A36FFE58A36FFE58A36FFE58A
          36FFE58A36FFE58A36FF9C5F24AF20130824000000000000000000000000AC67
          28BFB66E2BCBE58A36FFE58A36FFC7792EDF6B4119784429104C3D250E44613A
          176CAC6728BFE58A36FFE58A36FFC5772EDB150D05180000000000000000C577
          2EDBE58A36FFE58A36FFA86428BB0B06030C0000000000000000000000000000
          00000000000040270F48D98433F3E58A36FFB36C29C70402010400000000D780
          33EFE58A36FFE58A36FFE08834FB3D250E440000000000000000000000000000
          0000000000000000000039230E40E58A36FFE58A36FF4429104C00000000E58A
          36FFD78033EF814E1E8F190F061C000000000000000000000000000000000000
          0000000000000000000000000000AC6728BFE58A36FF965A23A7150D0518683F
          18740E0903100000000000000000000000000000000000000000000000000000
          00000000000000000000000000004429104CAC6728BFA46227B7000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000321E0C383923
          0E40190F061C0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000AE6A29C3E58A
          36FF965A23A70000000000000000000000000000000000000000000000000000
          000000000000000000000000000007040208563414602416082859361564E58A
          36FFDE8633F7120B041400000000000000000000000000000000000000000000
          0000000000002718092C83501F93D78033EFE58A36FF1D1107200E090310DE86
          33F7E58A36FFA46227B704020104000000000000000000000000000000000000
          00002718092CCC7B2FE3E58A36FFE58A36FFE58A36FF0E090310000000005936
          1564E58A36FFE58A36FF9C5F24AF1D1107200000000000000000000000000000
          00001D110720AC6728BFE58A36FFE58A36FFE08834FB00000000000000000000
          00007D4B1E8BE58A36FFE58A36FFDE8633F79C5F24AF7A491D877D4B1E8BA161
          26B3DE8633F7E58A36FFE58A36FFAC6728BFC0742ED700000000000000000000
          0000000000005232135CCF7C31E7E58A36FFE58A36FFE58A36FFE58A36FFE58A
          36FFE58A36FFD27E32EB4B2D125400000000150D051800000000000000000000
          000000000000000000000B06030C5D38166888522097B66E2BCBAE6A29C3814E
          1E8F5232135C120B041400000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000103
          0204379366B350D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF3EA473C7050D09100000000000000000183E
          2C4C50D293FF1F5239641E4F37601E4F37601E4F37601E4F37601E4F37601E4F
          37601E4F37601E4F37601F5239644BC78BF322593E6C00000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF000000000000000000000000000000000000000000000000235C
          40704BC78BF350D293FF50D293FF4BC78BF3122E203800000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000004DCA
          8DF750D293FF50D293FF4FCF91FB1A4530540000000000000000000000001E4F
          376050D293FF00000000000000000000000000000000000000000000000050D2
          93FF50D293FF4FCF91FB1A453054000000000000000000000000000000001E4F
          376050D293FF00000000000000000000000000000000000000000000000050D2
          93FF4FCF91FB1A45305400000000000000000000000000000000000000001538
          274450D293FF22593E6C1E4F37601E4F37601E4F37601E4F37601F52396450D2
          93FF1D4B355C0000000000000000000000000000000000000000000000000000
          00002E7855934DCA8DF750D293FF50D293FF50D293FF50D293FF50D293FF235C
          4070000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00004858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF3F4DB2DF242C6680313C8BAF4858CCFF4858CCFF3642
          99BF242C66803A47A6CF4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF0000000000000000000000000000
          00001B214D601B214D601B214D601B214D601B214D601B214D601B214D601B21
          4D601B214D601B214D601B214D601B214D600000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF00000000000000004858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF00000000000000000000
          00000000000000000000000000004858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000004858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF0000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000003B4E500082ABAF002C3A3C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000004D656800BDF8FF0099C9CF00181F200000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000004D6568004A616400030404000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000060808002C3A3C0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000006A8C8F00AAE1E7009CCCD30003
          0404000000000000000000000000000000000000000000000000000000000000
          000000000000000000000050696C00A4D9DF00B9F3FB00BDF8FF00BDF8FF004D
          6568000000000000000000000000000000000000000000000000000000000000
          0000000000000061808300B6EFF700BDF8FF00BDF8FF00BDF8FF00BDF8FF00AD
          E5EB000C10100000000000000000000000000000000000000000000000000000
          0000000000000012171800B3EBF300BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF0066888B0000000000000000000000000000000000000000000000000000
          00000000000000000000005C787C00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00B9F3FB001B232400000000000000000000000000000000000000000000
          000000000000000000000006080800A4D9DF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00BDF8FF0082ABAF00000000000000000000000000000000000000000000
          00000000000000000000000000000041555800BDF8FF00BDF8FF00BDF8FF00BD
          F8FF008EBABF00232F3000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000091BEC300BDF8FF00A1D5DB0038
          4A4C000000000000000000121718000C10100000000000000000000000000000
          000000000000000000000000000000000000002632340050696C000304040000
          0000000608080059747800B3EBF3006180830000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000038
          4A4C00A1D5DB00BDF8FF00BDF8FF005670740000000000000000000000000000
          0000000000000000000000000000000000000000000000000000007FA7AB00BD
          F8FF00BDF8FF0099C9CF00324244000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000003E5154009C
          CCD3004A61640003040400000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000B623878129C
          58BF129C58BF129C58BF129C58BF129C58BF000000000210091414AB62D315B6
          67DF14AB62D3021009140000000000000000000000000000000003170D1C16C3
          70EF18D077FF18D077FF18D077FF18D077FF000000000F8B50AB18D077FF18D0
          77FF0A58326C000000000000000000000000000000000000000000000000094E
          2D6018D077FF18D077FF13AF63D706341E400844275418D077FF15B667DF108F
          52AF00000000000000000E7845930F824A9F0107040800000000000000000000
          00000F8B50AB18D077FF18D077FF0B5E367416BC6CE7052A1834094E2D60094E
          2D60094E2D60094E2D60129E5AC318D077FF12A65FCB02140B18000000000000
          00000210091415C06EEB18D077FF18D077FF129E5AC30C653A7C18D077FF18D0
          77FF18D077FF18D077FF18D077FF18D077FF18D077FF14B86AE3000000000000
          0000000000000844275418D077FF18D077FF15B667DF073E234C18CD74FB18D0
          77FF18D077FF18D077FF18D077FF18D077FF18D077FF0C683C80000000000000
          000000000000020D071017C570F318D077FF18D077FF09512E64000000000000
          000000000000000000000F824A9F16C972F7094E2D6000000000000000000000
          0000000000000F824A9F18D077FF18D077FF18D077FF16C370EF03170D1C0000
          0000000000000000000006372044052E1A380000000000000000000000000000
          0000073E234C18D077FF18D077FF16BC6CE718D077FF18D077FF119154B30000
          000000000000000000000000000000000000000000000000000000000000020D
          071014B86AE318D077FF18CD74FB052716300F8B50AB18D077FF18D077FF094E
          2D60000000000000000000000000000000000000000000000000000000000F82
          4A9F18D077FF18D077FF0C683C80000000000210091414B86AE318D077FF16C3
          70EF03170D1C000000000000000000000000000000000000000004211328129C
          58BF129C58BF0F8B50AB000302040000000000000000073B2148129C58BF129C
          58BF0B6238780000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002B212D932B212D930101010400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001C161D604B3B4DFF4B3B4DFF392C3BC302020208000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000E0B0E30473748F34B3B4DFF4B3B4DFF2E242F9B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000605
          0614423442DF4B3B4DFF4B3B4DFF3C2F3DCB0202020800000000000000000000
          000000000000000000000000000000000000000000000000000001010104362B
          38BB4B3B4DFF4B3B4DFF443547EB0806081C0000000000000000000000000000
          000000000000100D1138322834AB4A394CFB4B3B4DFF443546E73B2D3BC74B3B
          4DFF4B3B4DFF4A394CFB14101444000000000000000000000000000000000000
          0000191319544A394CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF241D257C00000000000000000000000000000000000000000B08
          0B244A394CFB4B3B4DFF3E3241D71B151C5C130F1340211A2270443546E74B3B
          4DFF4A394CFB0706071800000000000000000000000000000000000000002F25
          2F9F4B3B4DFF3D3140D30403040C0000000000000000000000000806081C4435
          46E74B3B4DFF29202A8B00000000000000000000000000000000000000004234
          42DF4B3B4DFF1A141A580000000000000000000000000000000000000000241D
          257C4B3B4DFF3C2F3DCB00000000000000000000000000000000000000004B3B
          4DFF4B3B4DFF120E123C00000000000000000000000000000000000000001C16
          1D604B3B4DFF423442DF00000000000000000000000000000000000000004033
          41DB4B3B4DFF1D171E6400000000000000000000000000000000000000002921
          2B8F4B3B4DFF392C3BC300000000000000000000000000000000000000002921
          2B8F4B3B4DFF443546E7070607180000000000000000000000000D0A0D2C4938
          4BF74B3B4DFF231C247800000000000000000000000000000000000000000806
          081C443546E74B3B4DFF443546E7231C24781C161D60271E2883473748F34B3B
          4DFF433545E30202020800000000000000000000000000000000000000000000
          0000100D1138453647EF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF3D31
          40D30806081C0000000000000000000000000000000000000000000000000000
          00000000000005040510261E2780372B3ABF423442DF362B38BB2019206C0403
          040C000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000221B14287D634A9419130E1D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000067513D7AD8AA7FFF896C51A20000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000002A211831D3A67CF9D8AA7FFF3B2F234600000000000000003026
          1C39352A1F3F31271D3A31271D3A3A2E22454537295201010001000000000000
          0000000000000000000058453468D8AA7FFFA27F5FBF0000000000000000C99E
          76EDD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF6B543F7E01010001000000000000
          000000000000000000000807050AD8AA7FFFD0A47BF60000000000000000B48E
          6AD5D8AA7FFFD8AA7FFFD8AA7FFF6A533E7D0000000000000000000000000000
          0000000000000000000000000000D8AA7FFFD5A77DFB00000000000000009B7A
          5BB7D8AA7FFFD8AA7FFFCFA37AF4000000000000000000000000000000000000
          00000000000000000000231B1429D8AA7FFFC09771E300000000000000008A6D
          51A3D8AA7FFFD8AA7FFFD8AA7FFF785F478E0000000000000000000000000000
          000000000000000000009A795BB6D8AA7FFF725A43870000000000000000A380
          60C0BD956FDF2B221933D8AA7FFFD8AA7FFF725A438704030205000000000000
          0000100D091384684E9CD8AA7FFFCEA279F30E0B081100000000000000005946
          3469261E162D000000004E3D2E5CD8AA7FFFD8AA7FFFC79D75EB8E7054A89373
          56ADD2A57CF8D8AA7FFFD3A67CF931271D3A0000000000000000000000000000
          000000000000000000000000000035291F3EA38060C0D8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFF957558B0241C152A000000000000000000000000000000000000
          00000000000000000000000000000000000016110D1A524130618F7154A9886B
          50A14B3B2C590D0A070F00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000000000004D3F0F50DFB72BE7D6B12BDFD6B12BDFD6B1
          2BDFD6B12BDFD6B12BDFD6B12BDFDBB42CE3231C072400000000000000000000
          0000000000000000000000000000967B1E9B5C4C126000000000000000000000
          0000000000000000000000000000997E1F9F5C4C126000000000000000000000
          00004D3F0F50997E1F9F997E1F9FD2AD29DB5C4C126000000000000000000000
          0000000000000000000000000000997E1F9F5C4C126000000000000000001F19
          0620E9C02FF3997E1F9F997E1F9FD2AD29DB5C4C126000000000000000000000
          0000000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40D6B12BDF0000000000000000997E1F9F5C4C126000000000000000000000
          0000000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40D6B12BDF0000000000000000997E1F9F5C4C126000000000000000000000
          0000000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40D6B12BDF0000000000000000997E1F9F6F5C16741F1906200F0D03100000
          0000000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40D6B12BDF00000000000000003A2F0B3CE9C02FF3F6CA31FFF6CA31FF6F5C
          1674000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40D6B12BDF0000000000000000000000003A2F0B3CE9C02FF3F6CA31FF997E
          1F9F000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40D6B12BDF000000000000000000000000000000004D3F0F50F1C72FFB997E
          1F9F000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40DFB72BE73E330C403E330C402A23082C000000000000000051421054DBB4
          2CE3B89725BFB89725BFB89725BFDBB42CE32620082800000000000000000C09
          020CBB9A25C3F6CA31FFF6CA31FFF6CA31FF7B65198000000000000000001713
          05181F190620A58721ABA58721AB0C09020C0000000000000000000000000000
          000013100414CFA928D7F6CA31FFF6CA31FFD2AD29DB00000000000000000000
          000000000000997E1F9F997E1F9F000000000000000000000000000000000000
          00000000000017130518CFA928D7F6CA31FFD6B12BDF00000000000000000000
          000000000000997E1F9F997E1F9F000000000000000000000000000000000000
          0000000000000000000017130518D6B12BDFDBB42CE31F1906201F1906201F19
          06201F190620B19122B78A721B8F000000000000000000000000000000000000
          000000000000000000000000000026200828DFB72BE7F6CA31FFF6CA31FFF6CA
          31FFF6CA31FFC4A126CB26200828000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000005040510271E2883261E278007060718000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000302732A73E3241D7392C3BC3473748F30F0C10340000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000362B38BB2C232E9700000000302732A7362B38BB0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001C161D60453647EF0403040C141014444B3B4DFF0101
          0104000000000000000000000000000000000000000000000000000000000000
          0000000000000000000002020208443547EB231C2478171218504B3B4DFF0504
          0510000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000281F2987443547EB49384BF7302631A30C09
          0C28130F1340130F1340130F1340130F13400504051000000000000000000000
          0000000000000000000002020208130F13404B3B4DFF362A37B7332835AF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF443547EB1D171E640C090C28362A
          37B7423442DF453647EF4B3B4DFF4B3B4DFF4B3B4DFF4A394CFB3C2F3DCB3328
          35AF29212B8F221B23741A141A58120E123C09070A20000000003D303FCF3529
          35B31C161D601C161D60130F13404B3B4DFF322834AB4A394CFB070607180000
          00000000000000000000000000000000000000000000000000003D303FCF2C23
          2E9700000000010101041E181F684B3B4DFF211A22704B3B4DFF231C24780000
          0000000000000000000000000000000000000000000000000000130F13404435
          46E7473748F3443547EB49384BF7211A22700B080B244A394CFB423442DF0101
          0104000000000000000000000000000000000000000000000000000000000605
          0614171218501612174C0605061400000000000000002F252F9F4B3B4DFF1612
          174C000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000B080B244A394CFB3529
          35B3000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000029212B8F4B3B
          4DFF09070A200000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000060506143027
          32A7281F29870000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000B080B240000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000465B640084ADBD008AB4C5008EBACC0075A3A6272D2B43B2794EB77A61
          4991745B4489725A4387B48D6AD47A6048900000000000000000000000000000
          00000085AEBE00B2E9FF0083ABBB008DB9CA006A969669695B9C835A3888120E
          0B15503F2F5F2A211932654F3B7781654C980000000000000000000000000000
          00000085AEBE0089B3C40000000000000000000000005F584B83805B3D893E31
          254A8D6F53A65B47356B5B48366C6D5640810000000000000000000000000000
          00000086AFC00090BDCF0000000000000000000000007D573A83765E478D120E
          0A153A2E224506050307594634697F644B960000000000000000000000000000
          00000086AFC00090BDCF000000000000000000000000715942857A6149910000
          00000C09070E69533E7DC6A17BF1846C53A20000000000000000000000000000
          00000086AFC00090BDCF0000000000000000000000007C634A9485694E9D0000
          0000493B2E59DDAA7DFFB07340B0130C07130000000000000000000000000000
          00000086AFC00090BDCF000000000000000000000000513A2958B28D69D35F4D
          3B73B78458C79769409F00000000000000000000000000000000000000000000
          00000086AFC00090BDCF000000000000000000000000000000003D281A3D6845
          2A68302013302554617A00577B7B000000000000000000000000000000000000
          00000086AFC00091BED000000000000000000000000000000000000000000000
          0000000000000092CECF00617D8B000000000000000000000000000000000000
          00000085AFBF0086AFC00000000000516A7400607D89005A7880006087890051
          7373000000000085ACBF00607D89000000000000000000000000000000000000
          00000083ACBC008EBACC001C252800A9DDF200B2E9FF00B2E9FF00B2E9FF00A9
          DDF2001D2529008DB9CA005B7883000000000000000000000000000000000000
          0000007DA4B300B2E9FF00B2E9FF00B2E9FF00B0E6FC00A5D8EC00B0E6FC00B2
          E9FF00B2E9FF00B2E9FF00506872000000000000000000000000000000000000
          0000002733380041555D0042565E0086AFC0009ACADD00263237009ECFE30085
          AEBE0042565E0040545C00181F22000000000000000000000000000000000000
          000000000000000000000000000000000000005069730095C4D6004D656F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001F1908258B721D8F8B721D8F8B721D8F8B721D8F8B73
          1D908B721D8F382E0D3E00000000000000000000000000000000000000000000
          000000000000000000006250166CF7CB33FFF7CB33FFF7CB33FFF7CB33FFF7CB
          33FFF7CB33FF8B721D9100000000000000000000000000000000000000000000
          00000000000000000000604F1465F8CC34FFF8CC34FFF7CB33FFF7CB33FFF7CB
          33FFF8CC34FF8B721D9000000000000000000000000000000000000000000000
          00000000000000000000614F1465F7CB33FFF7CB33FFF8CC34FFF9CD35FFF7CB
          33FFF7CB33FF8B721D8F000000000000000000000000000000000403040C1A14
          1A581C161D60120E123C604F1463F7CB33FFF6CA32FFF8CC34FFF6CA32FFF7CB
          33FFF8CC34FF8B721D8F070607181C161D601B151C5C06050614362A37B74B3B
          4DFF4B3B4DFF2F252F9F604E1363F7CB33FFF7CB33FFF7CB33FFF8CC34FFF7CB
          33FFF7CB33FF8B721D8F1410134149384BF7443546E73E3241D74B3B4DFF4B3B
          4DFF4B3B4DFF2F252F9F6250166AF8CC34FFF8CC34FFF8CC34FFF7CB33FFF8CC
          34FFF7CB33FF8B731D9014101341271E2883010101044B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF302732A717120E2F6C58208C6C58208C6E5A22916F5B23966F5A
          23966F5B23982F26195E15111648403341DB322834AB4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF473748F3221B22711C161D601C161D601C161D601C161D601C16
          1D601C161D601D171E643D303FCF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF3D303FCF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF443546E70E0B0E303027
          32A7372B3ABF362B38BB1712185009070A2009070A2009070A2009070A200907
          0A2009070A20261E2780372B3ABF372B3ABF322834AB130F1340000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000001010001020200020403010405040105110E03120C0A
          020D141004150000000000000000000000000000000000000000000000000000
          00000000000000000000EEC32DFBEEC32DFBEEC32DFBEEC32DFBEBC12DF8EDC2
          2DFAEBC12DF8E6BC2CF200000000000000000000000000000000000000000000
          00000000000000000000EFC42DFCF3C72FFFB89728C2B59525BC1B16061DF7CB
          33FFF7CB33FFE3BA2BEF00000000000000000000000000000000000000000000
          00000000000000000000F1C52EFEF3C72FFF000000006553156804030104EEC4
          30F7F7CB33FFEAC02DF700000000000000000000000000000000000000000000
          00000000000000000000F1C52EFEF3C72FFF00000000604F1463040301041814
          0519AD8E24B2EAC02DF700000000000000000000000000000000171218503D30
          3FCF423442DF29202A8BF1C52EFEF3C72FFF0000000091771E964438114D100D
          0310947A1F99EAC02DF7100D081D423442DF3E3241D71D171E64423442DF4B3B
          4DFF4B3B4DFF2F252F9FF2C62EFFF3C72FFF00000000BD9B27C3F8CC34FFB595
          25BCF8CC34FFEAC02DF714100921392C3BC3271E2883473748F34B3B4DFF4B3B
          4DFF4B3B4DFF2F252F9FECC12DF9F5C931FF65531568EEC430F7F8CC34FFF8CC
          34FFF7CB33FFECC12DF9120F091D2B212D930403040C4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF392C3BC31713061C261F0C2C251E0B2D251E0B2D251E0B2C251F
          0B2D2B231242080605102B221D4F4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF3E3241D7372B3ABF372B3ABF372B3ABF372B3ABF372B
          3ABF372B3ABF3B2D3BC74A394CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4C3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4A394CFB4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2E242F9B4B3B
          4DFF4B3B4DFF4B3B4DFF403341DB372B3ABF372B3ABF372B3ABF372B3ABF372B
          3ABF372B3ABF453647EF4B3B4DFF4B3B4DFF4B3B4DFF362B38BB000000000E0B
          0E30130F1340130F134001010104000000000000000000000000000000000000
          00000000000007060718130F1340130F1340100D113800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000043
          5961007092A0006E8E9C006E8E9C006E8E9C006E8E9C006E8E9C006E8E9C006E
          8E9C006E8E9C006E8E9C006E8E9C007092A000526B7500000000000000000097
          C5D900B2EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF000102020005060700A7
          DAEF00B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00171F22001F292D00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0034454B003D505800B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0059
          747F00607D8900B2E9FF00B2E9FF00B2E9FF00B3E9FF004E6771005A758000B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0067869300202A2E358B
          61A9368D63AB0041555D00B2E9FF00B2E9FF00B3E9FF00698997007EA5B500B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF007EA5B51333243E3FB486DE50D2
          93FF4ED093FC20553B6700334349008EB9CB00B2E9FF008CB7C80097C5D800B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF004960692972548C4ECF94FD49C9
          92F64FCB8FF850D293FF235D417100546E7800B2E9FF00A1D3E7001D262A0018
          1F220012171900121719001217190013181B00121719010A0B0E00000000307D
          589840A674CA4FD092FD3A9A6BBA000B0E1000181F22001F282C000000000000
          00000000000000000000000000000000000000000000000000000000000041AD
          7AD250D293FF4ECC8FF80F251A2D000000000000000000000000000000000068
          879400A9DDF200A9DDF200A9DDF200A9DDF20057717C0A18111D45B67FDD50D2
          93FF4AC58AF00816111B0080A8B800B2E9FF00799EAE00000000000000000075
          97A600B2E9FF00B2E9FF00B2E9FF00B2E9FF005A7681338D65AB50D192FF43B2
          7CD82F7C579700080B0C00B2E9FF00B2E9FF00799DAD0000000000000000007E
          A2B300B2EAFF00B2E9FF00B2E9FF00B2E9FF0089B5C62059416D50D192FF46BA
          82E344B47DDA317D5899071B182200090C0D0006070800000000000000000043
          5860007092A0006E8E9C006E8E9C007092A000536D78000000003C9E6EC050D2
          94FF50D293FF4CC78CF2040B070D000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000010101378F
          64AE3DA171C40000000000000000000000000000000000000000}
      end>
  end
end
