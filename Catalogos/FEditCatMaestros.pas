unit FEditCatMaestros;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, Buttons, DBCtrls, StdCtrls, ExtCtrls,
  Mask, ZetaEdit, ZetaNumero, ZetaSmartLists, ZetaKeyLookup, ExtDlgs,
  TDMULTIP,ClipBrd, ZetaKeyCombo, ComCtrls;

type
  TEditCatMaestros = class(TBaseEdicion)
    PageControl1: TPageControl;
    tbGenerales: TTabSheet;
    tbImagen: TTabSheet;
    tbSTPS: TTabSheet;
    lblCedula: TLabel;
    lblRFC: TLabel;
    lblNumero: TLabel;
    lblTexto: TLabel;
    lblEmpresa: TLabel;
    lblCalle: TLabel;
    lblCiudad: TLabel;
    lblTelefono: TLabel;
    MA_CEDULA: TDBEdit;
    MA_RFC: TDBEdit;
    MA_TEXTO: TDBEdit;
    MA_EMPRESA: TDBEdit;
    MA_DIRECCI: TDBEdit;
    MA_CIUDAD: TDBEdit;
    MA_TEL: TDBEdit;
    MA_NUMERO: TZetaDBNumero;
    MA_ACTIVO: TDBCheckBox;
    PC_CODIGO: TZetaDBKeyLookup;
    SavePictureDialog: TSavePictureDialog;
    OpenPictureDialog: TOpenPictureDialog;
    Label1: TLabel;
    Imagen: TPDBMultiImage;
    LeerDisco: TSpeedButton;
    GrabarDisco: TSpeedButton;
    BorraImagen: TSpeedButton;
    Panel1: TPanel;
    MA_CODIGO: TZetaDBEdit;
    lblCodigo: TLabel;
    lblNombre: TLabel;
    MA_NOMBRE: TDBEdit;
    GroupBox1: TGroupBox;
    MA_TAGENTE: TZetaDBKeyCombo;
    Label2: TLabel;
    MA_NAGENTE: TZetaDBEdit;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure LeerDiscoClick(Sender: TObject);
    procedure GrabarDiscoClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure BorraImagenClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure DoLookup; override;
  public
  end;

var
  EditCatMaestros: TEditCatMaestros;

implementation

uses ZetaCommonClasses, dCatalogos, ZAccesosTress, ZetaBuscador;

{$R *.DFM}

procedure TEditCatMaestros.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAPA_MAESTROS;
     HelpContext:= H60642_Maestros;
     FirstControl :=MA_CODIGO;
end;

procedure TEditCatMaestros.Connect;
begin
     dmCatalogos.cdsProvCap.Conectar;
     Datasource.Dataset := dmCatalogos.cdsMaestros;
end;

procedure TEditCatMaestros.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Maestro', 'Maestro', 'MA_CODIGO', dmCatalogos.cdsMaestros );
end;

procedure TEditCatMaestros.LeerDiscoClick(Sender: TObject);
begin
     if OpenPictureDialog.Execute then
     begin
          with dmCatalogos.cdsMaestros do
               if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                  Edit;
          Imagen.LoadFromFile( OpenPictureDialog.FileName );
     end;
end;

procedure TEditCatMaestros.GrabarDiscoClick(Sender: TObject);
begin
     SavePictureDialog.FilterIndex := 0;
     SavePictureDialog.FileName := '';
     if SavePictureDialog.Execute then
        with Imagen do
             case SavePictureDialog.FilterIndex of
                  1,2 :
                  begin
                       if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'JPG';
                       SaveToFileAsJPG(SavePictureDialog.FileName);
                  end;
                  3:
                  begin
                       if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'BMP';
                       SaveToFileAsBMP(SavePictureDialog.FileName);
                  end;
                  4:
                  begin
                       if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'GIF';
                       SaveToFileAsGIF(SavePictureDialog.FileName);
                  end;
                  5:
                  begin
                       if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'TIF';
                       SaveToFileAsTIF(SavePictureDialog.FileName);
                  end;
                  6:
                  begin
                       if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'PCX';
                       SaveToFileAsPCX(SavePictureDialog.FileName);
                  end;
                  7:
                  begin
                       if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'PNG';
                       SaveToFileAsPNG(SavePictureDialog.FileName);
                  end;
             end;
end;

procedure TEditCatMaestros.OKClick(Sender: TObject);
begin
     if ( dmCatalogos.cdsMaestros.FieldByName('MA_IMAGEN').AsString <> VACIO ) then
     begin
          with Imagen do
          begin
               CutToClipboard;
               UpdateAsJPG:=True;
               PastefromClipboard;
          end;
     end;
     inherited;
end;

procedure TEditCatMaestros.BorraImagenClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsMaestros do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          TBlobField(FieldByName('MA_IMAGEN')).Clear;
          Imagen.ResetImage;
     end;
end;

procedure TEditCatMaestros.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     GrabarDisco.Enabled:= dmCatalogos.cdsMaestros.FieldByName('MA_IMAGEN').AsString <> VACIO;
     BorraImagen.Enabled:= GrabarDisco.Enabled;
end;

procedure TEditCatMaestros.FormShow(Sender: TObject);
begin
     inherited;
     PageControl1.ActivePage :=  tbGenerales;
end;

end.
