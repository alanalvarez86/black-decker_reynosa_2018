unit FCosteoCriteriosPorConcepto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls,
  StdCtrls, ZetaKeyLookup_DevEx, ZBaseGridLectura_DevEx, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, cxContainer, cxGroupBox;

type
  TCosteoCriteriosPorConcepto_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    BAgregaPuestos: TcxButton;
    GroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    GruposDeCosteo: TZetaKeyLookup_DevEx;
    CriteriosDeCosteo: TZetaKeyLookup_DevEx;
    BtnFiltrar: TcxButton;
    GpoCostoCodigo: TcxGridDBColumn;
    CritCostoID: TcxGridDBColumn;
    CO_NUMERO: TcxGridDBColumn;
    Descripcion: TcxGridDBColumn;
    procedure FiltrosValidLookup(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AgregarConceptosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure RealizaConsulta;
    { Private declarations }
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Borrar; override;
  public
    { Public declarations }
  end;
var
  CosteoCriteriosPorConcepto_DevEx: TCosteoCriteriosPorConcepto_DevEx;

implementation

{$R *.dfm}

uses
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     DCatalogos, DBClient;

const
     K_MSJ_NOGRUPO_CRITERIO_SEL = 'Para agregar un Concepto es necesario indicar Grupo y Criterio o dejar ambos Vac�os.';

     
procedure TCosteoCriteriosPorConcepto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H00003_Costeo_CriteriosPorConcepto;
end;

procedure TCosteoCriteriosPorConcepto_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCosteoGrupos.Conectar;
          cdsCosteoCriterios.Conectar;
          GruposDeCosteo.LookupDataset := cdsCosteoGrupos;
          CriteriosDeCosteo.LookupDataset := cdsCosteoCriterios;
          Datasource.Dataset := cdsCosteoCriteriosPorConcepto;
          RealizaConsulta;
     end;
end;

procedure TCosteoCriteriosPorConcepto_DevEx.Agregar;
begin
     dmCatalogos.CosteoAgregaConceptos;
end;

procedure TCosteoCriteriosPorConcepto_DevEx.Borrar;
begin
     dmCatalogos.cdsCosteoCriteriosPorConcepto.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCosteoCriteriosPorConcepto_DevEx.Modificar;
begin
     with dmCatalogos do
          with cdsCosteoCriteriosPorConcepto do
          begin
               GrupoDeCosteo := FieldByName('GpoCostoCodigo').AsString;
               CriterioDeCosteo := FieldByName('CritCostoID').AsInteger;

               cdsCosteoGrupos.Locate('GpoCostoCodigo',FieldByName('GpoCostoCodigo').AsString,[]);
               cdsCosteoCriterios.Locate('CritCostoID',CriterioDeCosteo,[]);

               Modificar;
          end;
end;

procedure TCosteoCriteriosPorConcepto_DevEx.Refresh;
begin
     RealizaConsulta;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCosteoCriteriosPorConcepto_DevEx.FiltrosValidLookup( Sender: TObject);
begin
     RealizaConsulta;
end;

procedure TCosteoCriteriosPorConcepto_DevEx.RealizaConsulta;
begin
     with dmCatalogos do
     begin
          GrupoDeCosteo := GruposDeCosteo.Llave;
          CriterioDeCosteo := CriteriosDeCosteo.Valor;
          with cdsCosteoCriteriosPorConcepto do
          begin
               Refrescar;
               Filter := VACIO;
               if StrLleno(GruposDeCosteo.Llave) then
                  Filter := ConcatFiltros( Filter, Format( 'GpoCostoCodigo = %s', [ EntreComillas( GruposDeCosteo.Llave ) ] ) );
               if (CriteriosDeCosteo.Valor <> 0) then
                  Filter := ConcatFiltros( Filter, Format( 'CritCostoID = %d', [ CriteriosDeCosteo.Valor ] ) );

               Filtered := Filter <> VACIO;
          end
     end
end;

procedure TCosteoCriteriosPorConcepto_DevEx.AgregarConceptosClick(Sender: TObject);
begin
     with dmCatalogos do
     begin
          try
             cdsCosteoCriteriosPorConcepto.DisableControls;
             CosteoAgregaConceptos;
             RealizaConsulta;
          finally
                 cdsCosteoCriteriosPorConcepto.EnableControls;
          end;
     end
end;

procedure TCosteoCriteriosPorConcepto_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('GpoCostoCodigo'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

end.
