unit FCatTipoSAT_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, System.Actions;

type
  TCatTipoSAT_DevEx = class(TBaseGridLectura_DevEx)
    TB_CODIGO: TcxGridDBColumn;
    RS_NOMBRE: TcxGridDBColumn;
    TB_SAT_CLA: TcxGridDBColumn;
    TB_SAT_NUM: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
    procedure RefrescaTiposSAT;
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatTipoSAT_DevEx: TCatTipoSAT_DevEx;

implementation

uses dCatalogos,
     ZetaCommonClasses,
     ZetaBuscador_DevEx;

{$R *.dfm}

procedure TCatTipoSAT_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     RefrescaTiposSAT;
     CanLookup := True;
     HelpContext:= H70079_TiposConceptosSAT;
end;

//Catalogos



procedure TCatTipoSAT_DevEx.RefrescaTiposSAT;
begin
     dmCatalogos.cdsTiposSAT.Refrescar;
     DataSource.DataSet :=  dmCatalogos.cdsTiposSAT;
end;

procedure TCatTipoSAT_DevEx.Connect;
begin
     inherited;
     with dmCatalogos do
     begin
          cdsTiposSAT.Conectar;
          DataSource.DataSet:= cdsTiposSAT;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatTipoSAT_DevEx.Refresh;
begin
     inherited;
     dmCatalogos.cdsTiposSAT.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatTipoSAT_DevEx.Agregar;
begin
     inherited;
     dmCatalogos.cdsTiposSAT.Agregar;
end;

procedure TCatTipoSAT_DevEx.Borrar;
begin
     inherited;
     dmCatalogos.cdsTiposSAT.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatTipoSAT_DevEx.Modificar;
begin
     inherited;
     dmCatalogos.cdsTiposSAT.Modificar;
end;

procedure TCatTipoSAT_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Tipo Percepci�n/Deducci�n SAT', 'TB_CODIGO', dmCatalogos.cdsTiposSAT );
end;


procedure TCatTipoSAT_DevEx.FormShow(Sender: TObject);
begin
 ApplyMinWidth;
 CreacolumaSumatoria(ZetaDbGridDbTableView.Columns[0],0,'',skCount);
  inherited;
  ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TCatTipoSAT_DevEx.ZetaDBGridDBTableViewCellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
    //dmCatalogos.cdsTiposSAT.Modificar;
end;




end.
