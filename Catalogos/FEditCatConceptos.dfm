inherited EditCatConceptos: TEditCatConceptos
  Left = 21
  Caption = 'Conceptos de N'#243'mina'
  ClientHeight = 430
  ClientWidth = 526
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 394
    Width = 526
    inherited OK: TBitBtn
      Left = 358
    end
    inherited Cancelar: TBitBtn
      Left = 443
    end
  end
  inherited PanelSuperior: TPanel
    Width = 526
    inherited BuscarBtn: TSpeedButton
      Visible = True
    end
    inherited ImprimirBtn: TSpeedButton
      Left = 241
    end
    inherited ImprimirFormaBtn: TSpeedButton
      Left = 265
    end
    inherited CortarBtn: TSpeedButton
      Left = 300
    end
    inherited CopiarBtn: TSpeedButton
      Left = 324
    end
    inherited PegarBtn: TSpeedButton
      Left = 348
    end
    inherited UndoBtn: TSpeedButton
      Left = 372
    end
    object bImpSubCuentas: TSpeedButton [10]
      Left = 209
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Importaci'#243'n de Subcuentas Contables'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clSilver
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
        B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
        B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
        0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
        55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
        55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
        55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
        5555575FFF755555555557000075555555555577775555555555}
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = bImpSubCuentasClick
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 526
    inherited ValorActivo2: TPanel
      Width = 200
      inherited textoValorActivo2: TLabel
        Width = 194
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 51
    Width = 526
    Height = 51
    Align = alTop
    TabOrder = 3
    object CO_NUMEROLbl: TLabel
      Left = 53
      Top = 8
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero:'
    end
    object LblDescrip: TLabel
      Left = 34
      Top = 30
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object CO_NUMERO: TZetaDBNumero
      Left = 101
      Top = 4
      Width = 70
      Height = 21
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
      UseEnterKey = True
      ConfirmEdit = True
      DataField = 'CO_NUMERO'
      DataSource = DataSource
    end
    object CO_DESCRIP: TDBEdit
      Left = 101
      Top = 26
      Width = 300
      Height = 21
      DataField = 'CO_DESCRIP'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  object PageControl: TPageControl [4]
    Left = 0
    Top = 102
    Width = 526
    Height = 292
    ActivePage = TabTimbrado
    Align = alClient
    TabOrder = 4
    object TabGenerales: TTabSheet
      Caption = 'Generales'
      object LblCondicion: TLabel
        Left = 39
        Top = 90
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Condici'#243'n:'
      end
      object LblTipo: TLabel
        Left = 64
        Top = 13
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object Label12: TLabel
        Left = 34
        Top = 112
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Subcuenta:'
        FocusControl = CO_SUB_CTA
      end
      object LblUsoNomina: TLabel
        Left = 12
        Top = 67
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Uso de N'#243'mina:'
      end
      object CO_ACTIVO: TDBCheckBox
        Left = 54
        Top = 29
        Width = 57
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Activo:'
        DataField = 'CO_ACTIVO'
        DataSource = DataSource
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CO_CALCULA: TDBCheckBox
        Left = 5
        Top = 46
        Width = 106
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Calcular Siempre:'
        DataField = 'CO_CALCULA'
        DataSource = DataSource
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CO_TIPO: TZetaDBKeyCombo
        Left = 98
        Top = 9
        Width = 198
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        OnChange = CO_TIPOChange
        ListaFija = lfTipoConcepto
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'CO_TIPO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object CO_QUERY: TZetaDBKeyLookup
        Left = 98
        Top = 86
        Width = 320
        Height = 21
        LookupDataset = dmCatalogos.cdsCondiciones
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 80
        DataField = 'CO_QUERY'
        DataSource = DataSource
      end
      object GBPercepcion: TGroupBox
        Left = 81
        Top = 133
        Width = 153
        Height = 131
        Caption = ' Percepci'#243'n '
        TabOrder = 5
        object CO_MENSUAL: TDBCheckBox
          Left = 21
          Top = 18
          Width = 110
          Height = 17
          Caption = 'Mensual'
          DataField = 'CO_MENSUAL'
          DataSource = DataSource
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CO_A_PTU: TDBCheckBox
          Left = 21
          Top = 41
          Width = 110
          Height = 17
          Caption = 'Acumula a PTU'
          DataField = 'CO_A_PTU'
          DataSource = DataSource
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CO_G_ISPT: TDBCheckBox
          Left = 21
          Top = 87
          Width = 110
          Height = 17
          Caption = 'Gravada ISPT'
          DataField = 'CO_G_ISPT'
          DataSource = DataSource
          TabOrder = 3
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CO_G_IMSS: TDBCheckBox
          Left = 21
          Top = 64
          Width = 110
          Height = 17
          Caption = 'Gravada IMSS'
          DataField = 'CO_G_IMSS'
          DataSource = DataSource
          TabOrder = 2
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CO_ISN: TDBCheckBox
          Left = 21
          Top = 110
          Width = 110
          Height = 17
          Caption = 'Gravada ISN'
          DataField = 'CO_ISN'
          DataSource = DataSource
          TabOrder = 4
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
      object GBImprimir: TGroupBox
        Left = 241
        Top = 133
        Width = 121
        Height = 131
        Caption = ' Imprimir '
        TabOrder = 6
        object CO_IMPRIME: TDBCheckBox
          Left = 26
          Top = 51
          Width = 80
          Height = 17
          Caption = 'En Recibo'
          DataField = 'CO_IMPRIME'
          DataSource = DataSource
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CO_LISTADO: TDBCheckBox
          Left = 26
          Top = 75
          Width = 80
          Height = 17
          Caption = 'En Listado'
          DataField = 'CO_LISTADO'
          DataSource = DataSource
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
      object CO_SUB_CTA: TDBEdit
        Left = 98
        Top = 108
        Width = 320
        Height = 21
        DataField = 'CO_SUB_CTA'
        DataSource = DataSource
        TabOrder = 4
      end
      object CO_USO_NOM: TZetaDBKeyCombo
        Left = 98
        Top = 63
        Width = 198
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 7
        OnChange = CO_TIPOChange
        ListaFija = lfUsoConceptoNomina
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'CO_USO_NOM'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
    object TabCalculo: TTabSheet
      Caption = 'C'#225'lculo'
      object LblFormulaCalc: TLabel
        Left = 52
        Top = 12
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'F'#243'rmula:'
      end
      object SBCO_FORMULA: TSpeedButton
        Left = 427
        Top = 9
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
        OnClick = SBCO_FORMULAClick
      end
      object LblFormulaAlterna: TLabel
        Left = 52
        Top = 146
        Width = 205
        Height = 13
        Alignment = taRightJustify
        Caption = 'F'#243'rmula Alterna (Quincenales y mensuales):'
      end
      object SBCO_FRM_ALT: TSpeedButton
        Left = 427
        Top = 143
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
        OnClick = SBCO_FRM_ALTClick
      end
      object CO_FORMULA: TDBMemo
        Left = 52
        Top = 37
        Width = 400
        Height = 104
        DataField = 'CO_FORMULA'
        DataSource = DataSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object CO_FRM_ALT: TDBMemo
        Left = 52
        Top = 171
        Width = 399
        Height = 92
        DataField = 'CO_FRM_ALT'
        DataSource = DataSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 1
      end
    end
    object TabExento: TTabSheet
      Caption = 'Exento ISPT'
      object LblFormulaExento: TLabel
        Left = 52
        Top = 12
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'F'#243'rmula:'
      end
      object SBco_x_ispt: TSpeedButton
        Left = 427
        Top = 9
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
        OnClick = SBco_x_isptClick
      end
      object CO_X_ISPT: TDBMemo
        Left = 52
        Top = 37
        Width = 400
        Height = 155
        DataField = 'CO_X_ISPT'
        DataSource = DataSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabIndividual: TTabSheet
      Caption = 'ISPT Individual'
      object LblFormulaIndiv: TLabel
        Left = 52
        Top = 12
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'F'#243'rmula:'
      end
      object SBco_imp_cal: TSpeedButton
        Left = 427
        Top = 9
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
        OnClick = SBco_imp_calClick
      end
      object CO_IMP_CAL: TDBMemo
        Left = 52
        Top = 37
        Width = 400
        Height = 155
        DataField = 'CO_IMP_CAL'
        DataSource = DataSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabRecibo: TTabSheet
      Caption = 'Recibo'
      object LblFormulaRecibo: TLabel
        Left = 52
        Top = 12
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'F'#243'rmula:'
      end
      object SBco_recibo: TSpeedButton
        Left = 427
        Top = 9
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
        OnClick = SBco_reciboClick
      end
      object CO_RECIBO: TDBMemo
        Left = 52
        Top = 37
        Width = 400
        Height = 155
        DataField = 'CO_RECIBO'
        DataSource = DataSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object CO_CAMBIA: TDBCheckBox
        Left = 51
        Top = 198
        Width = 188
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Cambiar de columna si es negativo:'
        DataField = 'CO_CAMBIA'
        DataSource = DataSource
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CO_SUMRECI: TDBCheckBox
        Left = 90
        Top = 214
        Width = 149
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Sumar en Recibo Mensual:'
        DataField = 'CO_SUMRECI'
        DataSource = DataSource
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
    object TabNotas: TTabSheet
      Caption = 'Notas/Documentaci'#243'n'
      ImageIndex = 5
      object Label1: TLabel
        Left = 58
        Top = 8
        Width = 74
        Height = 13
        Caption = 'Observaciones:'
      end
      object CO_NOTA: TDBMemo
        Left = 58
        Top = 24
        Width = 401
        Height = 97
        DataField = 'CO_NOTA'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object GroupBox1: TGroupBox
        Left = 58
        Top = 128
        Width = 257
        Height = 89
        Caption = 'Documento'
        TabOrder = 1
        object btnAgregarDocumento: TSpeedButton
          Left = 10
          Top = 12
          Width = 24
          Height = 24
          Hint = 'Insertar Documento'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = AgregarDocumentoBtnClick
        end
        object btnBorraDocumento: TSpeedButton
          Left = 42
          Top = 12
          Width = 24
          Height = 24
          Hint = 'Borrar Documento'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BorrarDocumentoBtnClick
        end
        object btnEditarDocumento: TSpeedButton
          Left = 74
          Top = 12
          Width = 24
          Height = 24
          Hint = 'Modificar Documento'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = EditarDocumentoBtnClick
        end
        object btnVerDocumento: TSpeedButton
          Left = 118
          Top = 11
          Width = 105
          Height = 22
          Caption = 'Ver Documento'
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            1800000000000003000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000000000FFFFFF000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000FFFFFFFFFFFFFFFFFF
            8080008080008080008080008080008080008080008080008080008080008080
            00800000FFFFFFFFFFFFFFFFFF000000C0C0C080800080800080800080800080
            8000808000808000808000808000C0C0C0000000FFFFFFFF0000FFFFFFFFFFFF
            FFFFFF808000808000808000808000808000808000808000808000C0C0C0C0C0
            C0FF00FFFF0000FF0000FFFFFFFFFFFF000000C0C0C080800080800080800080
            8000C0C0C0FFFF00FFFF00FFFF00000000FF0000FF0000FF0000FFFFFFFFFFFF
            FFFFFFFFFFFF808000C0C0C0FFFF00FFFF00FFFF00FFFF00FFFF00C0C0C0FF00
            00FF0000FF0000FF0000FFFF00FFFF00FFFF00000000FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF0000000000FFFF00FF0000FF0000FF00FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFF008000C0C0C0FFFF00FFFF00FFFF00C0C0C000FFFF00FF
            FF00FFFF00FFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C0C0C080800080
            8000808000C0C0C00000000000FF0000FF00FFFF00FFFF00FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF808000808000808000800000FFFFFF0000FF0000
            FF0000FF00FFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C0C0C080
            8000C0C0C0000000FFFFFFFFFFFF0000FF0000FF0000FF00FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808000800000FFFFFFFFFFFFFFFFFFFFFF
            FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C0
            C0C0000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
            0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          OnClick = btnVerDocumentoClick
        end
        object lblDescripcionArchivo: TLabel
          Left = 8
          Top = 40
          Width = 244
          Height = 28
          Alignment = taCenter
          AutoSize = False
          Caption = 
            'algo algo algo algo algo algo algo algo algo algo alalgo alalgo ' +
            'alalgo alalgo alalgo al'
          Layout = tlCenter
          WordWrap = True
        end
        object lblTipoArchivo: TLabel
          Left = 8
          Top = 72
          Width = 244
          Height = 15
          Alignment = taCenter
          AutoSize = False
          Caption = 
            'algo algo algo algo algo algo algo algo algo algo algo algo algo' +
            ' algo algo algo'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'L'#237'mites/Acceso'
      ImageIndex = 6
      object GroupBox2: TGroupBox
        Left = 33
        Top = 16
        Width = 208
        Height = 58
        Caption = '                                          '
        TabOrder = 0
        object lblMonto1: TLabel
          Left = 24
          Top = 24
          Width = 33
          Height = 13
          Caption = 'Monto:'
          Enabled = False
        end
        object CO_LIM_INF: TZetaDBNumero
          Left = 63
          Top = 22
          Width = 121
          Height = 21
          Enabled = False
          Mascara = mnPesos
          TabOrder = 0
          Text = '0.00'
          DataField = 'CO_LIM_INF'
          DataSource = DataSource
        end
      end
      object GroupBox3: TGroupBox
        Left = 273
        Top = 16
        Width = 208
        Height = 58
        Caption = '                                            '
        TabOrder = 3
        object lblMonto: TLabel
          Left = 24
          Top = 24
          Width = 33
          Height = 13
          Caption = 'Monto:'
          Enabled = False
        end
        object CO_LIM_SUP: TZetaDBNumero
          Left = 63
          Top = 22
          Width = 121
          Height = 21
          Enabled = False
          Mascara = mnPesos
          TabOrder = 0
          Text = '0.00'
          DataField = 'CO_LIM_SUP'
          DataSource = DataSource
        end
      end
      object GpoAcceso: TGroupBox
        Left = 33
        Top = 85
        Width = 449
        Height = 132
        Caption = ' Acceso a registrar excepciones '
        TabOrder = 4
        object TodosUsuarios: TRadioButton
          Left = 13
          Top = 21
          Width = 169
          Height = 17
          Caption = 'Todos los grupos de usuarios'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = TodosUsuariosClick
        end
        object AlgunosUsuarios: TRadioButton
          Left = 13
          Top = 41
          Width = 172
          Height = 17
          Caption = 'Grupo de usuarios con derecho'
          TabOrder = 1
          OnClick = AlgunosUsuariosClick
        end
        object btnElegirGrupos: TBitBtn
          Left = 405
          Top = 58
          Width = 26
          Height = 25
          Hint = 'Asignar Permisos de Acceso'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnElegirGruposClick
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            777777777777777000777777777777033007777777777030B30777777777030B
            0B077777770030B0B000770000330B0B08F070333300B0B0FFF0037B7B3B0B08
            F8F007BB3B73B0FFFFF00BB3B3BB00F8F8F088303B3B70FFFFF0070703B3B0F8
            F8F00B803B3B70CCCCC070BB8BB7000000007700800077777777}
        end
        object CO_GPO_ACC: TDBMemo
          Left = 32
          Top = 58
          Width = 369
          Height = 57
          Color = clBtnFace
          DataField = 'CO_GPO_ACC'
          DataSource = DataSource
          Enabled = False
          ReadOnly = True
          TabOrder = 3
        end
      end
      object CO_VER_INF: TDBCheckBox
        Left = 45
        Top = 14
        Width = 124
        Height = 17
        Caption = 'Aplicar L'#237'mite Inferior'
        DataField = 'CO_VER_INF'
        DataSource = DataSource
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CO_VER_INFClick
      end
      object CO_VER_SUP: TDBCheckBox
        Left = 284
        Top = 14
        Width = 124
        Height = 17
        Caption = 'Aplicar L'#237'mite Superior  '
        DataField = 'CO_VER_SUP'
        DataSource = DataSource
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CO_VER_SUPClick
      end
    end
    object TabTimbrado: TTabSheet
      Caption = 'Timbrado'
      ImageIndex = 7
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 518
        Height = 73
        Align = alTop
        Caption = ' Monto Positivo: '
        TabOrder = 0
        object Label2: TLabel
          Left = 71
          Top = 18
          Width = 93
          Height = 13
          Alignment = taRightJustify
          Caption = 'Clase de Concepto:'
        end
        object lblTipoPositivo: TLabel
          Left = 12
          Top = 43
          Width = 153
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Percepci'#243'n/Deducci'#243'n:'
        end
        object CO_SAT_CLP: TZetaDBKeyCombo
          Left = 174
          Top = 14
          Width = 130
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          OnChange = CO_SAT_CLPChange
          ListaFija = lfClaseConceptosSAT
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'CO_SAT_CLP'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object CO_SAT_TPP: TZetaDBKeyLookup
          Left = 174
          Top = 37
          Width = 320
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 80
          DataField = 'CO_SAT_TPP'
          DataSource = DataSource
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 73
        Width = 518
        Height = 73
        Align = alTop
        Caption = ' Monto Negativo: '
        TabOrder = 1
        object Label4: TLabel
          Left = 71
          Top = 18
          Width = 93
          Height = 13
          Alignment = taRightJustify
          Caption = 'Clase de Concepto:'
        end
        object lblTipoNegativo: TLabel
          Left = 12
          Top = 43
          Width = 153
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Percepci'#243'n/Deducci'#243'n:'
        end
        object CO_SAT_CLN: TZetaDBKeyCombo
          Left = 174
          Top = 14
          Width = 130
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          OnChange = CO_SAT_CLNChange
          ListaFija = lfClaseConceptosSAT
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'CO_SAT_CLN'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object CO_SAT_TPN: TZetaDBKeyLookup
          Left = 174
          Top = 37
          Width = 320
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 80
          DataField = 'CO_SAT_TPN'
          DataSource = DataSource
        end
      end
      object GroupBox6: TGroupBox
        Left = 0
        Top = 146
        Width = 518
        Height = 73
        Align = alTop
        TabOrder = 2
        object Label6: TLabel
          Left = 33
          Top = 18
          Width = 131
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tratamiento de Percepci'#243'n:'
        end
        object lblConceptoExento: TLabel
          Left = 10
          Top = 43
          Width = 155
          Height = 13
          Alignment = taRightJustify
          Caption = 'Concepto para Gravado/Exento:'
        end
        object CO_SAT_EXE: TZetaDBKeyCombo
          Left = 174
          Top = 14
          Width = 130
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          OnChange = CO_SAT_EXEChange
          ListaFija = lfTipoExentoSAT
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'CO_SAT_EXE'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object CO_SAT_CON: TZetaDBKeyLookup
          Left = 174
          Top = 37
          Width = 320
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 80
          DataField = 'CO_SAT_CON'
          DataSource = DataSource
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 407
    Top = 60
  end
end
