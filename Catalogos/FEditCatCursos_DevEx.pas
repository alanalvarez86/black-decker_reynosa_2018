unit FEditCatCursos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon_DevEx,  Db, Buttons,
  DBCtrls, StdCtrls, ExtCtrls, Mask, ComCtrls,
  Grids, DBGrids, ZetaDBGrid, ZetaNumero, ZetaEdit,
  ZetaFecha, ZetaMessages, ZetaKeyCombo,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx,
  cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit;


type
  TEditCatCursos_DevEx = class(TBaseEdicionRenglon_DevEx)
    OpenDialog: TOpenDialog;
    lblRevisio: TLabel;
    CU_REVISIO: TDBEdit;
    Label13: TLabel;
    CU_FEC_REV: TZetaDBFecha;
    Label3: TLabel;
    CU_INGLES: TDBEdit;
    CU_NUMERO: TZetaDBNumero;
    Label7: TLabel;
    Label10: TLabel;
    CU_TEXTO: TDBEdit;
    CU_HORAS: TZetaDBNumero;
    Label9: TLabel;
    Label6: TLabel;
    CU_CLASIFI: TZetaDBKeyLookup_DevEx;
    Label11: TLabel;
    CU_CLASE: TZetaDBKeyLookup_DevEx;
    Label8: TLabel;
    MA_CODIGO: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    CU_COSTO1: TZetaDBNumero;
    Label5: TLabel;
    CU_COSTO2: TZetaDBNumero;
    Label12: TLabel;
    CU_COSTO3: TZetaDBNumero;
    Label15: TLabel;
    CU_DOCUM: TDBEdit;
    ZFecha: TZetaDBFecha;
    TabSheet1_DevEx: TcxTabSheet;
    TabSheet2_DevEx: TcxTabSheet;
    TabSheet3_DevEx: TcxTabSheet;
    CU_MODALID: TZetaDBKeyCombo;
    CU_OBJETIV: TZetaDBKeyCombo;
    AT_CODIGO: TZetaDBKeyLookup_DevEx;
    CU_STPS: TDBCheckBox;
    lblModalidad: TLabel;
    lblObjetivo: TLabel;
    lblAreTemCur: TLabel;
    btnSelArchivo_DevEx: TcxButton;
    btnVerDocumento_DevEx: TcxButton;
    CU_TEXTO1: TDBMemo;
    CU_TEXTO2: TDBMemo;
    Label2: TLabel;
    Label14: TLabel;
    Label1: TLabel;
    CU_FOLIO: TDBEdit;
    CU_CODIGO: TZetaDBEdit;
    CU_ACTIVO: TDBCheckBox;
    CU_NOMBRE: TDBMemo;
    procedure FormCreate(Sender: TObject);
    //procedure btnSelArchivoClick(Sender: TObject);
    procedure CU_DOCUMChange(Sender: TObject);
    //procedure btnVerDocumentoClick(Sender: TObject);
    procedure CU_FOLIOKeyPress(Sender: TObject; var Key: Char);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure GridRenglonesColExit(Sender: TObject);
    procedure GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;State: TGridDrawState);
    //procedure CancelarClick(Sender: TObject);
    //procedure OKClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure CU_STPSClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure btnSelArchivo_DevExClick(Sender: TObject);
    procedure btnVerDocumento_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }  
    FInsertando: Boolean;
    procedure ControlFecha;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure HabilitaControlesSTPS(lHabilitar: Boolean);
  protected
    { Protected declarations }  
    procedure Connect; override;
    procedure Agregar; override;
    procedure DoLookup; override;
    procedure FocusFirstControl; override;
    procedure HabilitaControles; override;
    procedure DoCancelChanges;override;
    procedure EscribirCambios;override;
    procedure KeyPress(var Key: Char); override; { TWinControl }    
  public
  end;
  TDBGridHack = class(TDBGrid);

var
  EditCatCursos_DevEx: TEditCatCursos_DevEx;

implementation

uses dCatalogos,ZetaDialogo, dTablas, ZetaCommonClasses,ZetaCommonTools,ZAccesosTress, ZetaBuscador_DevEx,ZetaClientTools;

{$R *.DFM}

procedure TEditCatCursos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAPA_CURSOS;
     HelpContext:= H60641_Cursos;
     FirstControl := CU_CODIGO;
     {$ifndef BOSE}
     //CU_TEXTO1.MaxLength := K_MAX_VARCHAR; v2010 CR 1678
     //CU_TEXTO2.MaxLength := K_MAX_VARCHAR;
     {$endif}
     GridRenglones.Options := [ dgEditing,
                                dgTitles,
                                dgIndicator,
                                dgColumnResize,
                                dgColLines,
                                dgRowLines,
                                dgTabs ];      
     with dmTablas do
     begin
          CU_CLASIFI.LookupDataset := cdsTipoCursos;
          CU_CLASE.LookupDataset := cdsClasifiCurso;
     end;
     MA_CODIGO.LookupDataset := dmCatalogos.cdsMaestros;
     AT_CODIGO.LookupDataset := dmTablas.cdsAreaTemCur;

     //DevEx (by am): Agregado para detectar el movimiento del scroll sobre el grid de edicion
     TDBGridHack( GridRenglones ).OnMouseWheel:= MyMouseWheel;

     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditCatCursos_DevEx.MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
     Handled := True;
     with GridRenglones.DataSource.DataSet do
     begin
          if WheelDelta > 0 then
             Prior
          else
              Next;
     end;
end;

procedure TEditCatCursos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage:= Datos;
     FInsertando:= FALSE;
     HabilitaControles;
end;

procedure TEditCatCursos_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsTipoCursos.Conectar;
          cdsClasifiCurso.Conectar;
          cdsAreaTemCur.Conectar;  
     end;
     with dmCatalogos do
     begin
          cdsMaestros.Conectar;
          cdsCursos.Conectar;
          cdsHistRev.Conectar;
          Datasource.Dataset := cdsCursos;
          { MV: (21/Sep/2005) Se esta tomando el ancho del campo en la base de datos }
          {$ifdef BOSE}
          Self.CU_TEXTO1.MaxLength := cdsCursos.FieldByName( 'CU_TEXTO1' ).Size;
          Self.CU_TEXTO2.MaxLength := cdsCursos.FieldByName( 'CU_TEXTO2' ).Size;
          {$endif}
          dsRenglon.DataSet := cdsHistRev;          
     end;
end;

procedure TEditCatCursos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Curso', 'Curso', 'CU_CODIGO', dmCatalogos.cdsCursos );
end;

procedure TEditCatCursos_DevEx.FocusFirstControl;
begin
     PageControl.ActivePage :=  Datos;
     inherited FocusFirstControl;
end;

{procedure TEditCatCursos_DevEx.btnSelArchivoClick(Sender: TObject);
var
   sPath : string;
begin
     sPath := VACIO;
     sPath := ZetaClientTools.AbreDialogo( OpenDialog, CU_DOCUM.Text, 'doc' );
     if( strLleno( sPath ) )then
     begin
          with DataSource.DataSet do
          begin
               if( not( State  in [ dsEdit,dsInsert ] ) )then
               begin
                    Edit;
               end;
               FieldByName('CU_DOCUM').AsString := sPath;
          end;
     end;
end;}

procedure TEditCatCursos_DevEx.CU_DOCUMChange(Sender: TObject);
begin
     inherited;
     btnVerDocumento_DevEx.Enabled := ( not StrVacio( CU_DOCUM.Text ) );
end;

{procedure TEditCatCursos_DevEx.btnVerDocumentoClick(Sender: TObject);
var
   sPath: string;
begin
     inherited;
     sPath := CU_DOCUM.Text;
     if ( FileExists( sPath ) )then
        ZetaClientTools.ExecuteFile( sPath,'','',SW_SHOWDEFAULT  )
     else
         ZetaDialogo.zError( '� Error al abrir archivo !',Format( 'No existe el archivo en la ruta: %s',[ sPath ] ), 0 );
end;}

procedure TEditCatCursos_DevEx.CU_FOLIOKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key = ZetaCommonClasses.UnaCOMILLA ) then
        Key := Chr( 0 );
     inherited KeyPress( Key );
end;

procedure TEditCatCursos_DevEx.HabilitaControles;
begin
     inherited HabilitaControles;
     CU_REVISIO.Enabled := InsertInicial OR FInsertando;
     CU_FEC_REV.Enabled := CU_REVISIO.Enabled;
     PageControl.Pages[3].TabVisible:= not CU_REVISIO.Enabled;
end;

procedure TEditCatCursos_DevEx.DoCancelChanges;
begin
     dmCatalogos.cdsHistRev.CancelUpdates;
     FInsertando:= FALSE;
     inherited DoCancelChanges;
end;

procedure TEditCatCursos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited DataSourceDataChange(Sender, Field);
     with dmCatalogos do
     begin
          if ( Field = Nil ) and ( cdsCursos.State in [ dsBrowse, dsInactive ] ) and
             ( cdsCursos.ChangeCount = 0 ) and ( cdsHistRev.ChangeCount = 0 )  then
          begin
               GetCursosRevision( cdsCursos.FieldByName('CU_CODIGO').AsString );
          end;
     end;
     HabilitaControlesSTPS(CU_STPS.Checked);
end;

procedure TEditCatCursos_DevEx.Agregar;
begin
     if PageControl.ActivePage <> Tabla then
        FInsertando:= TRUE;
     inherited;
end;

procedure TEditCatCursos_DevEx.EscribirCambios;
begin
     inherited;
     if ( dmCatalogos.cdsCursos.ChangeCount = 0 ) then
     begin
          FInsertando:= FALSE;
          HabilitaControles;
     end;
end;

procedure TEditCatCursos_DevEx.GridRenglonesColExit(Sender: TObject);
begin
  inherited;
     with GridRenglones do
     begin
          if ( SelectedField <> nil ) and
             ( SelectedField.FieldName = 'CH_FECHA' ) and
             ( zFecha.Visible ) then
          begin
               zFecha.Visible:= False;
          end;
     end;
end;

procedure TEditCatCursos_DevEx.GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
   iCurrentRow, iRecNo: Integer;
begin
     with GridRenglones, Canvas, Brush do
    begin
         if ( State = [] ) then
         begin
              iCurrentRow := Rect.Top div (1+Rect.Bottom-Rect.Top);
              iRecNo:= TDBGridHack( GridRenglones ).Row;
              if iCurrentRow = iRecNo then
              begin
                   //Color:= clBtnShadow;
                   Color:=  RGB(237,230,249);//RGB(156,129,139); //morado row seleccionado
                   Font.Color := clWindowText;
              end;
         end
         else if gdSelected in State then
         begin
              //Color:= clHighlight;
              Color:= RGB(212,188,251);//RGB(136,174,213); //Azul de celda seleccionado
              Font.Color := clHighlightText; //Texto en blanco para celda seleccionada
         end;
         if ( gdFocused in State ) then
         begin
          if ( Column.FieldName = 'CH_FECHA' ) then
          begin
               with zFecha do
               begin
                    Left := Rect.Left + GridRenglones.Left;
                    Top := Rect.Top + GridRenglones.Top;
                    Width := Column.Width + 2;
                    Visible := True;
               end;
          end;
          end;
         DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
end;


procedure TEditCatCursos_DevEx.KeyPress( var Key: Char );
begin
     if GridEnfocado then
     begin
          if ( ( Key <> chr(9) ) and ( key <> #0 ) ) then
          begin
               if ( GridRenglones.SelectedField.FieldName = 'CH_FECHA' ) then
               begin
                    ZFecha.SetFocus;
                    SendMessage(ZFecha.Handle, WM_Char, word(Key), 0);
                    Key:= #0;
               end
               else
               begin
                    if ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
                    begin
                         with GridRenglones do
                         begin
                              Key := #0;
                              SelectedIndex := PosicionaSiguienteColumna;
                              if ( Selectedindex = PrimerColumna ) then
                                 SetOk;
                         end;
                    end
                    else
                    begin
                         if ( Key = Chr( VK_ESCAPE ) ) then
                         begin
                              SeleccionaPrimerColumna;
                         end;
                    end;
               end;
          end;
     end
     else
     begin
          if ( ActiveControl = zFecha ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
          begin
               Key := #0;
               with GridRenglones do
               begin
                    SetFocus;
                    SelectedField := dmCatalogos.cdsHistRev.FieldByName( 'CH_REVISIO' );
               end;
          end;
     end;
     inherited;
end;


procedure TEditCatCursos_DevEx.ControlFecha;
begin
    if PageControl.ActivePage = Tabla then
        SeleccionaPrimerColumna;
end;

{procedure TEditCatCursos_DevEx.CancelarClick(Sender: TObject);
begin
     inherited;
     ControlFecha;
end;

procedure TEditCatCursos_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     ControlFecha;
end;}

procedure TEditCatCursos_DevEx.BBBorrarClick(Sender: TObject);
begin
     inherited;
     ControlFecha;
end;

procedure TEditCatCursos_DevEx.BBAgregarClick(Sender: TObject);
begin
     inherited;
     SeleccionaPrimerColumna;
end;

procedure TEditCatCursos_DevEx.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador( Message.LParam ) = exEnter ) then
     begin
          GridRenglones.SelectedIndex := PosicionaSiguienteColumna;
          if ( GridRenglones.SelectedIndex = PrimerColumna ) then
             Setok;
     end
end;

procedure TEditCatCursos_DevEx.PageControlChange(Sender: TObject);
begin
     inherited;
     with PageControl do
     begin
          if ( ActivePage = Tabla ) then
          begin
               SeleccionaPrimerColumna;
               ControlFecha;
          end;
     end;
end;

procedure TEditCatCursos_DevEx.CU_STPSClick(Sender: TObject);
begin
     inherited;
     HabilitaControlesSTPS(CU_STPS.Checked);
end;

procedure TEditCatCursos_DevEx.HabilitaControlesSTPS(lHabilitar:Boolean);
begin
     AT_CODIGO.Enabled := lHabilitar;
     CU_OBJETIV.Enabled := lHabilitar;
     CU_MODALID.Enabled := lHabilitar;
     
     lblAreTemCur.Enabled := lHabilitar; 
     lblObjetivo.Enabled := lHabilitar;
     lblModalidad.Enabled := lHabilitar;
end;


procedure TEditCatCursos_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     ControlFecha;
end;

procedure TEditCatCursos_DevEx.btnSelArchivo_DevExClick(Sender: TObject);
var
   sPath : string;
begin
     sPath := VACIO;
     sPath := ZetaClientTools.AbreDialogo( OpenDialog, CU_DOCUM.Text, 'doc' );
     if( strLleno( sPath ) )then
     begin
          with DataSource.DataSet do
          begin
               if( not( State  in [ dsEdit,dsInsert ] ) )then
               begin
                    Edit;
               end;
               FieldByName('CU_DOCUM').AsString := sPath;
          end;
     end;
end;

procedure TEditCatCursos_DevEx.btnVerDocumento_DevExClick(Sender: TObject);
var
   sPath: string;
begin
     inherited;
     sPath := CU_DOCUM.Text;
     if ( FileExists( sPath ) )then
        ZetaClientTools.ExecuteFile( sPath,'','',SW_SHOWDEFAULT  )
     else
         ZetaDialogo.zError( '� Error al abrir archivo !',Format( 'No existe el archivo en la ruta: %s',[ sPath ] ), 0 );
end;

procedure TEditCatCursos_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     ControlFecha;
end;

procedure TEditCatCursos_DevEx.SetEditarSoloActivos;
begin
     MA_CODIGO.EditarSoloActivos := TRUE;
end;

end.
