unit FCatTablasAmortizacion_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid;

type
  TCatTablasAmortizacion_DevEx = class(TBaseGridLectura_DevEx)
    AT_CODIGO: TcxGridDBColumn;
    AT_DESCRIP: TcxGridDBColumn;
    AT_INGLES: TcxGridDBColumn;
    AT_NUMERO: TcxGridDBColumn;
    AT_ACTIVO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations}
    procedure DoLookup; override;
  end;

var
  CatTablasAmortizacion_DevEx: TCatTablasAmortizacion_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatMaestros }

procedure TCatTablasAmortizacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_Cat_TablasAmortizacion;
end;

procedure TCatTablasAmortizacion_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsTablasAmortizacion.Conectar;
          DataSource.DataSet:= cdsTablasAmortizacion;
     end;
end;

procedure TCatTablasAmortizacion_DevEx.Refresh;
begin
     dmCatalogos.cdsTablasAmortizacion.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatTablasAmortizacion_DevEx.Agregar;
begin
     dmCatalogos.cdsTablasAmortizacion.Agregar;
end;

procedure TCatTablasAmortizacion_DevEx.Borrar;
begin
     dmCatalogos.cdsTablasAmortizacion.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatTablasAmortizacion_DevEx.Modificar;
begin
     dmCatalogos.cdsTablasAmortizacion.Modificar;
end;

procedure TCatTablasAmortizacion_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Tabla', 'AT_CODIGO', dmCatalogos.cdsTablasAmortizacion );
end;

procedure TCatTablasAmortizacion_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
  inherited;

   //columna sumatoria: cuantos
   CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('AT_CODIGO'), K_SIN_TIPO , '', skCount);
   ZetaDBGridDBTableView.ApplyBestFit();

end;

end.
