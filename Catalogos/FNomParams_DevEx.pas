unit FNomParams_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  TNomParams_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  NomParams_DevEx: TNomParams_DevEx;

implementation

uses DCatalogos,
     ZetaCommonClasses,
     ZetaBuscaEntero_DevEx;

{$R *.DFM}

{ TNomParams }

procedure TNomParams_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H60622_Parametros_nomina;
end;

procedure TNomParams_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsNomParam.Conectar;
          DataSource.DataSet := cdsNomParam;
     end;
end;

procedure TNomParams_DevEx.Refresh;
begin
     dmCatalogos.cdsNomParam.Refrescar;
end;

procedure TNomParams_DevEx.Agregar;
begin
     dmCatalogos.cdsNomParam.Agregar;
end;

procedure TNomParams_DevEx.Borrar;
begin
     dmCatalogos.cdsNomParam.Borrar;
end;

procedure TNomParams_DevEx.Modificar;
begin
     dmCatalogos.cdsNomParam.Modificar;
end;

procedure TNomParams_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'Folio', 'Param�tro de N�mina', 'NP_FOLIO', dmCatalogos.cdsNomParam );
end;

procedure TNomParams_DevEx.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  {***(@am):No se ordena por esta columna. La version 2013 tampoco lo hace***}
  if ( TcxGridDBColumn(AColumn).DataBinding.FieldName <> 'NP_FORMULA' ) then
    inherited;
end;

procedure TNomParams_DevEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(ZetaDbGridDBtableView.Columns[0],0 , '' , SkCount );
  inherited;
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := true;
end;

end.
