unit FEditCatVigenciasSGM;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion, ZetaKeyLookup, ZetaNumero, Mask, DBCtrls,
  StdCtrls, ZetaEdit, DB, ExtCtrls, ZetaSmartLists, Buttons, ZetaFecha,
  ZetaDBTextBox;

type
  TEditCatVigenciasSGM = class(TBaseEdicion)
    PV_TEXTO: TDBEdit;
    Label1: TLabel;
    Panel1: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Panel2: TPanel;
    PV_REFEREN: TZetaDBEdit;
    DBCodigoLBL: TLabel;
    GroupBox1: TGroupBox;
    PV_FEC_INI: TZetaDBFecha;
    PV_FEC_FIN: TZetaDBFecha;
    Label6: TLabel;
    Label7: TLabel;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    PV_OBSERVA: TDBMemo;
    PV_NUMERO: TZetaDBNumero;
    zTxtNoPoliza: TZetaTextBox;
    zTxtDescripcion: TZetaTextBox;
    zTxtTipoDescrip: TZetaTextBox;
    PV_CFIJO: TZetaDBNumero;
    Label8: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);

  private
    procedure HabilitaControls(const lHabilita: Boolean);

    { Private declarations }
  public
    { Public declarations }
      procedure DoLookup; override;
  protected
     procedure Connect; override;
     function PuedeModificar(var sMensaje: String): Boolean;override;
     function PuedeAgregar(var sMensaje: String): Boolean;override;
     function PuedeBorrar(var sMensaje: String): Boolean;override;
     procedure EscribirCambios;override;
  end;

var
  EditCatVigenciasSGM: TEditCatVigenciasSGM;

implementation

uses
    DReportes,
    zReportTools,
    dCatalogos,
    ZetaCommonClasses,
    ZetaCommonTools,
    ZetaCommonLists,
    ZAccesosTress,
    ZAccesosMgr,
    ZetaBuscador;

{$R *.dfm}

{ TFEditCatTipoPoliza }

procedure TEditCatVigenciasSGM.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl  := PV_REFEREN;
     IndexDerechos := D_CAT_GRAL_SEG_GASTOS_MEDICOS;
     HelpContext   := H_Cat_SGM;
end;

procedure TEditCatVigenciasSGM.Connect;
begin
     with dmCatalogos do
     begin
          cdsVigenciasSGM.Conectar;
          DataSource.DataSet := cdsVigenciasSGM;
     end;
end;

procedure TEditCatVigenciasSGM.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', Self.Caption, 'PV_REFEREN', dmCatalogos.cdsSegGastosMed );
end;

procedure TEditCatVigenciasSGM.FormShow(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsSegGastosMed do
     begin
          zTxtNoPoliza.Caption := FieldByName('PM_NUMERO').AsString;
          zTxtDescripcion.Caption := FieldByName('PM_DESCRIP').AsString;
          zTxtTipoDescrip.Caption := ObtieneElemento( lfTipoSGM,FieldByName('PM_TIPO').AsInteger );
     end;
end;

procedure TEditCatVigenciasSGM.HabilitaControls(const lHabilita:Boolean);
begin
     PV_REFEREN.Enabled := lHabilita;
     PV_FEC_INI.Enabled := lHabilita;
     PV_FEC_FIN.Enabled := lHabilita;
     PV_OBSERVA.Enabled := lHabilita;
     PV_TEXTO.Enabled := lHabilita;
     PV_NUMERO.Enabled := lHabilita;
end;

procedure TEditCatVigenciasSGM.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     if Field = nil then
     begin
          if dmCatalogos.cdsVigenciasSGM.state = dsBrowse then
          begin
               HabilitaControls(   ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CAMBIO )  );
          end
          else
          if dmCatalogos.cdsVigenciasSGM.state = dsInsert then
          begin
               HabilitaControls( True );
          end;
     end;
end;

function TEditCatVigenciasSGM.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No Tiene Permiso Para Agregar Registros';
     Result := ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CAMBIO );
end;

function TEditCatVigenciasSGM.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No Tiene Permiso Para Borrar Registros';
     Result := ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CAMBIO );
end;

function TEditCatVigenciasSGM.PuedeModificar(
  var sMensaje: String): Boolean;
begin
     sMensaje := 'No Tiene Permiso Para Modificar Registros';
     Result := ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CAMBIO );
end;

procedure TEditCatVigenciasSGM.EscribirCambios;
var
   bPos : Boolean;
   bAnterior: TBookMark;
begin
     with dmCatalogos.cdsVigenciasSGM do
     begin
          bAnterior := GetBookMark;
          bPos := State = dsInsert;
          Post;
          if bPos then
             Last
          else
          begin
               GotoBookMark( bAnterior );
               FreeBookMark( bAnterior );
          end;

     end;
     //inherited;
end;

end.
