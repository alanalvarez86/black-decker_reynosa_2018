unit FEditPerfil_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, ZetaNumero, StdCtrls,
  ZetaKeyCombo, DBCtrls, ZetaFecha, Mask, DB, Buttons,
  Grids, DBGrids, ImgList, ZetaDBGrid, ZetaDBTextBox, ZBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  cxNavigator, cxDBNavigator, cxButtons, cxContainer, cxEdit, cxGroupBox,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, cxTextEdit, cxMemo,
  cxDBEdit, ZetaSmartLists_DevEx, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid;

type
  TEditPerfil_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label8: TLabel;
    PageControl: TcxPageControl;
    Generales: TcxTabSheet;
    TabSheet2: TcxTabSheet;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    PF_ESTUDIO: TZetaDBKeyCombo;
    PF_EXP_PTO: TZetaDBNumero;
    PF_EDADMIN: TZetaDBNumero;
    PF_EDADMAX: TZetaDBNumero;
    PF_SEXO: TDBRadioGroup;
    GroupBox7: TcxGroupBox;
    PF_TEXTO1_LBL: TLabel;
    PF_TEXTO2_LBL: TLabel;
    PF_TEXTO1: TDBEdit;
    PF_TEXTO2: TDBEdit;
    GroupBox6: TcxGroupBox;
    Label9: TLabel;
    Label14: TLabel;
    PF_NUMERO1: TZetaDBNumero;
    PF_NUMERO2: TZetaDBNumero;
    TabSheet3: TcxTabSheet;
    Panel2: TPanel;
    lvwSecciones: TListView;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel7: TPanel;
    bArribaUbicacion: TZetaSmartListsButton_DevEx;
    bAbajo: TZetaSmartListsButton_DevEx;
    AgregarReg: TcxButton;
    BorrarReg: TcxButton;
    ModificarReg: TcxButton;
    ImageList: TImageList;
    dsDescPerfil: TDataSource;
    PU_CODIGO: TZetaDBTextBox;
    PU_DESCRIP: TZetaDBTextBox;
    Label4: TLabel;
    SeccionSplitter: TSplitter;
    GroupBox2: TcxGroupBox;
    PF_DESCRIP: TcxDBMemo;
    GroupBox3: TcxGroupBox;
    PF_OBJETIV: TcxDBMemo;
    GroupBox4: TcxGroupBox;
    PF_POSTING: TcxDBMemo;
    GroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    PF_CONTRL1: TDBEdit;
    PF_CONTRL2: TDBEdit;
    PF_FECHA: TZetaDBFecha;
    Label2: TLabel;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    zDBGrid: TcxGrid;
  {  ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    zDBGrid: TZetaCXGrid;

   }

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lvwSeccionesClick(Sender: TObject);
    procedure bArribaAbajoUbicacionClick(Sender: TObject);
    procedure AgregarRegClick(Sender: TObject);
    procedure BorrarRegClick(Sender: TObject);
    procedure ModificarRegClick(Sender: TObject);
    procedure ZDBGridTitleClick(Column: TColumn);
  private
    { Private declarations }
    procedure CrearListaSecciones;
    procedure DesplegarColumnasPorSeccion;
    procedure ApplyMinWidth;
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
  end;

const
     K_ANCHO_COLUMNA_TEXT = 150;
     K_SOLO_UNA_COL = 1;
     K_COLUMNA_ORDEN_ANCHO = 14;
var
  EditPerfil_DevEx: TEditPerfil_DevEx;

implementation
uses
    dCatalogos,
    ZetaDialogo,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaCommonTools,
    ZAccesosTress;

{$R *.dfm}

{ TEditPerfil }
procedure TEditPerfil_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_DESC_PERF_PUESTOS;
     HelpContext   := H_CAT_PERFILES;
     FirstControl  := PF_CONTRL1;
end;

procedure TEditPerfil_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     PageControl.ActivePage := Generales;
     CrearListaSecciones;
     inherited;
     dxBarButton_AgregarBtn.Visible := ivNever;
     dxBarButton_BorrarBtn.Visible := ivNever;
     dxBarControlContainerItem_DBNavigator.Visible := ivNever;

      //Desactiva la posibilidad de agrupar
      ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
      //Esconde la caja de agrupamiento
      ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
      //Para que nunca muestre el filterbox inferior
      ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
      //Para que no aparezca el Custom Dialog
      ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
      //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
      ZetaDBGridDBTableView.ApplyBestFit();

      ZetaDBGridDBTableView.OptionsData.Editing := False;

end;

procedure TEditPerfil_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsPerfiles.Conectar;
          DataSource.DataSet := cdsPerfiles;
          cdsDescPerfil.Refrescar;
          dsDescPerfil.DataSet := cdsDescPerfil;
          ZetaDBGridDBTableView.DataController.DataSource := dsDescPerfil;
          if cdsSeccionesPerfil.RecordCount > 0 then //existen Secciones ?
          begin
               {$ifdef VER130}
               lvwSecciones.Selected := lvwSecciones.Items[ 0 ];
               {$else}
               lvwSecciones.ItemIndex := 0;
               {$endif}
               lvwSeccionesClick( self );
          end;
     end;
     DesplegarColumnasPorSeccion;

end;

procedure TEditPerfil_DevEx.Agregar;
begin
//  inherited;
//  En esta forma no se puede agregar perfiles de puesto
end;

procedure TEditPerfil_DevEx.Borrar;
begin
//  inherited;
//  En esta forma no se puede borrar Perfiles de puesto
end;

procedure TEditPerfil_DevEx.lvwSeccionesClick(Sender: TObject);
begin
     inherited;
     with lvwSecciones do
     begin
          {$ifdef VER130}
          if ( Selected <> nil ) then
          begin
               dmCatalogos.FiltraDatosSeccion( Selected.SubItems[ 0 ], Selected.Caption );
          {$else}
          if ( ItemIndex >= 0 ) then
          begin
               dmCatalogos.FiltraDatosSeccion( Items[ ItemIndex ].SubItems[ 0 ], Items[ ItemIndex ].Caption );
          {$endif}
               self.DesplegarColumnasPorSeccion;
          end;
     end;
end;

procedure TEditPerfil_DevEx.bArribaAbajoUbicacionClick(Sender: TObject);
var
   sMensaje:string;
   iOrden : Integer;
begin
     if ( not dmCatalogos.cdsDescPerfil.IsEmpty )then
     begin
         if ( PuedeModificar (sMensaje) ) then
         begin
              with dmCatalogos do
              begin
                   iOrden:= SubeBajaOrden( eSubeBaja( TZetaSmartListsButton_DevEx(Sender).Tag ), cdsDescPerfil, SeccionCampos,
                                           cdsPerfiles.FieldByName( 'PU_CODIGO' ).AsString );
                   DesplegarColumnasPorSeccion;
                   cdsDescPerfil.Locate( 'DP_ORDEN', iOrden, [] );
              end;
         end
         else
             zInformation( Caption, 'No tiene permiso para cambiar el orden', 0 );
     end
     else
         zInformation( Caption, 'No hay datos para cambiar orden', 0 );

end;

procedure TEditPerfil_DevEx.AgregarRegClick(Sender: TObject);
var
   sMensaje:string;
begin
     with dmCatalogos do
     begin
          if cdsCamposPerfil.Locate('DT_CODIGO', SeccionCampos,[]) then
          begin
               if ( PuedeModificar ( sMensaje ) ) then
               begin
                    dmCatalogos.cdsDescPerfil.Agregar;
               end
               else
                   zInformation( Caption, 'No tiene permiso para agregar', 0 );
          end
          else
              zInformation( Caption, 'No existen campos definidos para esta secci�n', 0 );
     end;
end;

procedure TEditPerfil_DevEx.ModificarRegClick(Sender: TObject);
var
   sMensaje:string;
begin
     with dmCatalogos.cdsDescPerfil do
     begin
          if ( not IsEmpty ) then
          begin
               if ( PuedeModificar ( sMensaje ) ) then
               begin
                    Modificar
               end
               else
                   zInformation( Caption, 'No tiene permiso para modificar', 0 );
          end
          else zInformation( Caption, 'No hay datos para modificar', 0 );
     end;
end;

procedure TEditPerfil_DevEx.BorrarRegClick(Sender: TObject);
var
   sMensaje:string;
begin
     with dmCatalogos.cdsDescPerfil do
     begin
          if ( not IsEmpty ) then
          begin
               if ( PuedeModificar (sMensaje) ) then
               begin
                    Borrar;
                    Refrescar;
                    DesplegarColumnasPorSeccion;
               end
               else
                   zInformation( Caption, 'No tiene permiso para borrar', 0 );
          end
          else zInformation( Caption, 'No hay datos para borrar', 0 );
     end;
end;

procedure TEditPerfil_DevEx.CrearListaSecciones;
var
   NewItem : TListItem;
begin
     with lvwSecciones.Items do
     begin
          BeginUpdate;
          try
             Clear;
             with dmCatalogos.cdsSeccionesPerfil do
             begin
                  Conectar;
                  if RecordCount > 0 then
                  begin
                       First;
                       AgregarReg.Enabled:= True;
                       ModificarReg.Enabled:= True;
                       BorrarReg.Enabled:= True;
                       while ( not Eof ) do
                       begin
                            NewItem := Add;
                            if Assigned( NewItem ) then
                            begin
                                 NewItem.Caption := FieldByName('DT_NOMBRE').AsString;
                                 NewItem.SubItems.Add( FieldByName('DT_CODIGO').AsString );
                            end;
                            Next;
                       end;
                  end
                  else
                  begin
                       ZetaDialogo.ZError(Self.Caption,'No existen secciones de perfil creadas',0);
                       AgregarReg.Enabled:= False;
                       ModificarReg.Enabled:= False;
                       BorrarReg.Enabled:= False;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TEditPerfil_DevEx.DesplegarColumnasPorSeccion;
const
     K_ANCHO_CURSOR = 33;
var
   sSeccion: string;
   lExisteSeccion: Boolean;
   cColumna :TcxGridDBColumn;
begin
     { Antes de invocar a este metodo debe refrescar cdsCamposPerfil, invocando el m�todo: dmCatalogos.FiltraDatosSeccion }
     with lvwSecciones do
     begin
          {$ifdef VER130}
          lExisteSeccion := ( Selected <> nil );
          if lExisteSeccion then
          begin
               sSeccion := Selected.SubItems[0];
          end;
          {$else}
          lExisteSeccion := ( ItemIndex > -1 );
          if lExisteSeccion then
          begin
               sSeccion := Items[ ItemIndex ].SubItems[0];
          end;
          {$endif}
     end;

     with ZetaDBGridDBTableView do
     begin
          BeginUpdate;
          try

             //Clear;
             ClearItems;
             //with Add do
             cColumna := CreateColumn;
             with cColumna do
             begin
                  DataBinding.FieldName := 'DP_ORDEN';
                  Caption   := '#';
                  Width           := K_COLUMNA_ORDEN_ANCHO;
                  HeaderAlignmentHorz := taRightJustify;
             end;

             if lExisteSeccion then
             begin
                  with dmCatalogos.cdsDescPerfil do
                  begin
                       Filtered := False;
                       Filter   := Format('DT_CODIGO = %s',[EntreComillas( sSeccion )]);
                       Filtered := True;
                  end;
                  if ( not dmCatalogos.cdsCamposPerfil.IsEmpty ) then
                  begin
                       with dmCatalogos.cdsCamposPerfil do
                       begin
                            First;
                            while( not Eof )do
                            begin
                                 if( dmCatalogos.cdsDescPerfil.FindField( FieldByName('DF_CAMPO').AsString ) <> Nil )then
                                 begin
                                      cColumna := CreateColumn;
                                      //with Add do
                                      with  cColumna do
                                      begin   
                                           //FieldName := FieldByName('DF_CAMPO').AsString;
                                           DataBinding.FieldName := FieldByName('DF_CAMPO').AsString;
                                           //Title.Caption := FieldByName('DF_TITULO').AsString;
                                           Caption := FieldByName('DF_TITULO').AsString;
                                           //Title.Alignment := taCenter;
                                           if RecordCount = K_SOLO_UNA_COL then
                                           begin
                                                Width := zDBGrid.Width - (  K_ANCHO_CURSOR + K_COLUMNA_ORDEN_ANCHO ) ;
                                           end
                                           else
                                           //if ( Field.DataType in [ ftString, ftMemo, ftBlob ] ) then
                                           if ( DataBinding.Field.DataType in [ ftString, ftMemo, ftBlob ] ) then
                                              Width := K_ANCHO_COLUMNA_TEXT;
                                      end;
                                 end
                                 else ZetaDialogo.ZError(Self.Caption,'El campo: '+FieldByName('DF_CAMPO').AsString+' no se ha definido en la Descripci�n de Puestos',0);
                                 Next;
                            end;
                       end;
                  end; // no existen campos en la secci�n
             end;
          finally
                 EndUpdate;
          end;
     end;

end;

procedure TEditPerfil_DevEx.ZDBGridTitleClick(Column: TColumn);
begin                                                                                                                                        
     // Se deshabilita la funcionalidad de Ordenar por que afecta el ordenado por Flechas !!
end;


procedure TEditPerfil_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;


end.
