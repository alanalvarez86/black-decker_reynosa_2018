inherited EditTablaAmortizacion_DevEx: TEditTablaAmortizacion_DevEx
  Left = 590
  Top = 87
  Caption = 'Tabla de Cotizaci'#243'n SGM'
  ClientHeight = 661
  ClientWidth = 425
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl: TcxPageControl [0]
    Top = 200
    Width = 425
    Height = 425
    ClientRectBottom = 423
    ClientRectRight = 423
    inherited Datos: TcxTabSheet
      Caption = 'Resumen'
      TabVisible = False
      object DedLbl: TLabel
        Left = 182
        Top = 156
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = '# de Abonos:'
      end
    end
    inherited Tabla: TcxTabSheet
      Caption = 'Costos P'#243'lizas SGM'
      inherited Panel2: TPanel [0]
        Width = 421
        inherited BBAgregar_DevEx: TcxButton
          Left = 6
        end
        inherited BBBorrar_DevEx: TcxButton
          Left = 146
        end
        inherited BBModificar_DevEx: TcxButton
          Left = 286
        end
      end
      inherited GridRenglones: TZetaDBGrid [1]
        Width = 421
        Height = 366
        Columns = <
          item
            Expanded = False
            FieldName = 'AC_EDADINI'
            Title.Caption = 'Edad Inicio'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AC_EDADFIN'
            Title.Caption = 'Edad Fin'
            Width = 59
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'AC_CHOMBRE'
            Title.Caption = 'Hombres'
            Width = 91
            Visible = True
          end
          item
            Color = clHighlightText
            Expanded = False
            FieldName = 'AC_CMUJER'
            Title.Caption = 'Mujeres'
            Width = 91
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PV_SDEPEND'
            Title.Caption = 'Dependientes'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'PV_CONDIC'
            Title.Caption = 'Condiciones'
            Visible = False
          end>
      end
    end
  end
  inherited PanelBotones: TPanel [1]
    Top = 625
    Width = 425
    inherited OK_DevEx: TcxButton
      Left = 261
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 340
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel [3]
    Width = 425
    inherited ValorActivo2: TPanel
      Width = 99
      inherited textoValorActivo2: TLabel
        Width = 93
      end
    end
  end
  inherited Panel1: TPanel [4]
    Width = 425
    Height = 150
    object Label8: TLabel
      Left = 33
      Top = 11
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label1: TLabel
      Left = 10
      Top = 33
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object Label7: TLabel
      Left = 38
      Top = 55
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'Ingl'#233's:'
    end
    object Label2: TLabel
      Left = 29
      Top = 77
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero:'
    end
    object Label3: TLabel
      Left = 39
      Top = 99
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'Texto:'
    end
    object AT_CODIGO: TDBEdit
      Left = 72
      Top = 8
      Width = 83
      Height = 21
      CharCase = ecUpperCase
      DataField = 'AT_CODIGO'
      DataSource = DataSource
      TabOrder = 0
    end
    object AT_DESCRIP: TDBEdit
      Left = 72
      Top = 30
      Width = 324
      Height = 21
      DataField = 'AT_DESCRIP'
      DataSource = DataSource
      TabOrder = 1
    end
    object AT_INGLES: TDBEdit
      Left = 72
      Top = 52
      Width = 325
      Height = 21
      DataField = 'AT_INGLES'
      DataSource = DataSource
      TabOrder = 2
    end
    object AT_NUMERO: TZetaDBNumero
      Left = 72
      Top = 74
      Width = 121
      Height = 21
      Mascara = mnPesos
      TabOrder = 3
      Text = '0.00'
      DataField = 'AT_NUMERO'
      DataSource = DataSource
    end
    object AT_TEXTO: TDBEdit
      Left = 72
      Top = 96
      Width = 325
      Height = 21
      DataField = 'AT_TEXTO'
      DataSource = DataSource
      TabOrder = 4
    end
    object AT_ACTIVO: TDBCheckBox
      Left = 346
      Top = 8
      Width = 50
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Activo'
      DataField = 'AT_ACTIVO'
      DataSource = DataSource
      TabOrder = 5
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
  end
  inherited DataSource: TDataSource
    Left = 329
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited dsRenglon: TDataSource
    Left = 389
    Top = 0
  end
end
