inherited EditCatCondiciones: TEditCatCondiciones
  Left = 330
  Top = 175
  ActiveControl = QU_CODIGO
  Caption = 'Condiciones'
  ClientHeight = 360
  ClientWidth = 612
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 104
    Top = 46
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
    FocusControl = QU_DESCRIP
  end
  object QU_DESCRIPlbl: TLabel [1]
    Left = 81
    Top = 70
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
    FocusControl = QU_DESCRIP
  end
  object QU_FILTROlbl: TLabel [2]
    Left = 100
    Top = 94
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'F'#243'rmula:'
    FocusControl = QU_FILTRO
  end
  object Label2: TLabel [3]
    Left = 113
    Top = 215
    Width = 27
    Height = 13
    Caption = 'Nivel:'
  end
  object lblUs_Codigo: TLabel [4]
    Left = 98
    Top = 294
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modific'#243':'
  end
  object sbtnContructor: TSpeedButton [5]
    Left = 574
    Top = 91
    Width = 25
    Height = 25
    Hint = 'Constructor de Filtros y Condiciones'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000010000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500000000055
      555557777777775F55550FFFFFFFFF0555557F5555555F7FFF5F0FEEEEEE0000
      05007F555555777775770FFFFFF0BFBFB00E7F5F5557FFF557770F0EEEE000FB
      FB0E7F75FF57775555770FF00F0FBFBFBF0E7F57757FFFF555770FE0B00000FB
      FB0E7F575777775555770FFF0FBFBFBFBF0E7F5575FFFFFFF5770FEEE0000000
      FB0E7F555777777755770FFFFF0B00BFB0007F55557577FFF7770FEEEEE0B000
      05557F555557577775550FFFFFFF0B0555557FF5F5F57575F55500F0F0F0F0B0
      555577F7F7F7F7F75F5550707070700B055557F7F7F7F7757FF5507070707050
      9055575757575757775505050505055505557575757575557555}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = sbtnContructorClick
  end
  object US_CODIGO: TZetaDBTextBox [6]
    Left = 145
    Top = 293
    Width = 60
    Height = 17
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'US_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object TImage [7]
    Left = 170
    Top = 10
    Width = 29
    Height = 24
    Picture.Data = {
      07544269746D617042010000424D420100000000000076000000280000001100
      0000110000000100040000000000CC0000000000000000000000100000001000
      0000000000000000BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0
      C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
      FF007777777777777777700000007777777777777777700000007777777774F7
      77777000000077777777444F77777000000077777774444F7777700000007000
      00444F44F7777000000070FFF444F0744F777000000070F8884FF0774F777000
      000070FFFFFFF07774F77000000070F88888F077774F7000000070FFFFFFF077
      7774F000000070F88777F07777774000000070FFFF00007777777000000070F8
      8707077777777000000070FFFF00777777777000000070000007777777777000
      0000777777777777777770000000}
    Stretch = True
    Visible = False
  end
  object OkImg: TImage [8]
    Left = 539
    Top = 212
    Width = 29
    Height = 24
    Picture.Data = {
      07544269746D617042010000424D420100000000000076000000280000001100
      0000110000000100040000000000CC0000000000000000000000100000001000
      0000000000000000BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0
      C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
      FF007777777777777777700000007777777777777777700000007777777774F7
      77777000000077777777444F77777000000077777774444F7777700000007000
      00444F44F7777000000070FFF444F0744F777000000070F8884FF0774F777000
      000070FFFFFFF07774F77000000070F88888F077774F7000000070FFFFFFF077
      7774F000000070F88777F07777774000000070FFFF00007777777000000070F8
      8707077777777000000070FFFF00777777777000000070000007777777777000
      0000777777777777777770000000}
    Stretch = True
    Visible = False
  end
  object ErrorImg: TImage [9]
    Left = 539
    Top = 212
    Width = 29
    Height = 24
    Picture.Data = {
      07544269746D6170F6000000424DF60000000000000076000000280000001000
      0000100000000100040000000000800000000000000000000000100000001000
      0000000000000000BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0
      C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
      FF0077777777777777777777777777777777777700000000007777770FFFFFFF
      F07777770FFFFFFFF077771F0F888888F077711F0F85BFB8F0777711F11BFBF8
      F077777151788888F077777511FFFFFFF07775111F1FFF00007771570FF1FF0F
      077777770FFFFF00777777770000000777777777777777777777777777777777
      7777}
    Stretch = True
    Visible = False
  end
  object lblQU_ORDEN: TLabel [10]
    Left = 109
    Top = 272
    Width = 32
    Height = 13
    Caption = 'Orden:'
  end
  inherited PanelBotones: TPanel
    Top = 324
    Width = 612
    TabOrder = 8
    inherited OK: TBitBtn
      Left = 448
    end
    inherited Cancelar: TBitBtn
      Left = 533
    end
  end
  inherited PanelSuperior: TPanel
    Width = 612
    TabOrder = 9
    inherited BuscarBtn: TSpeedButton
      Visible = True
    end
  end
  object QU_CODIGO: TZetaDBEdit [13]
    Tag = 99
    Left = 145
    Top = 42
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
    ConfirmEdit = True
    DataField = 'QU_CODIGO'
    DataSource = DataSource
  end
  object QU_DESCRIP: TDBEdit [14]
    Left = 145
    Top = 66
    Width = 425
    Height = 21
    DataField = 'QU_DESCRIP'
    DataSource = DataSource
    TabOrder = 2
  end
  object QU_FILTRO: TDBMemo [15]
    Left = 145
    Top = 90
    Width = 425
    Height = 117
    DataField = 'QU_FILTRO'
    DataSource = DataSource
    MaxLength = 255
    ScrollBars = ssVertical
    TabOrder = 3
  end
  inherited PanelIdentifica: TPanel
    Width = 612
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 286
    end
  end
  object QU_NIVEL: TZetaDBKeyCombo [17]
    Left = 145
    Top = 210
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
    ListaFija = lfNivelUsuario
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'QU_NIVEL'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object TestBtn: TBitBtn [18]
    Left = 448
    Top = 210
    Width = 87
    Height = 25
    Caption = 'Pro&bar'
    TabOrder = 10
    OnClick = TestBtnClick
    Glyph.Data = {
      42010000424D4201000000000000760000002800000011000000110000000100
      040000000000CC00000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      7777700000007777777777777777700000007777777774F77777700000007777
      7777444F77777000000077777774444F777770000000700000444F44F7777000
      000070FFF444F0744F777000000070F8884FF0774F777000000070FFFFFFF077
      74F77000000070F88888F077774F7000000070FFFFFFF0777774F000000070F8
      8777F07777774000000070FFFF00007777777000000070F88707077777777000
      000070FFFF007777777770000000700000077777777770000000777777777777
      777770000000}
    Margin = 20
  end
  object QU_NAVEGA: TDBCheckBox [19]
    Left = 78
    Top = 232
    Width = 80
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Navegaci'#243'n:'
    DataField = 'QU_NAVEGA'
    DataSource = DataSource
    TabOrder = 5
    ValueChecked = 'S'
    ValueUnchecked = 'N'
    OnClick = QU_NAVEGAClick
  end
  object QU_ORDEN: TZetaDBNumero [20]
    Left = 145
    Top = 268
    Width = 60
    Height = 21
    Mascara = mnDias
    TabOrder = 7
    Text = '0'
    DataField = 'QU_ORDEN'
    DataSource = DataSource
  end
  object QU_CANDADO: TDBCheckBox [21]
    Left = 13
    Top = 250
    Width = 145
    Height = 17
    Alignment = taLeftJustify
    Caption = 'S'#243'lo Autor Puede Cambiar:'
    DataField = 'QU_CANDADO'
    DataSource = DataSource
    TabOrder = 6
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited DataSource: TDataSource
    Left = 294
    Top = 219
  end
end
