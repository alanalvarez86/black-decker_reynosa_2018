unit FCatOtrasPer_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatOtrasPer_DevEx = class(TBaseGridLectura_DevEx)
    TB_CODIGO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    TB_TIPO: TcxGridDBColumn;
    TB_MONTO: TcxGridDBColumn;
    TB_TASA: TcxGridDBColumn;
    TB_IMSS: TcxGridDBColumn;
    TB_ISPT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations}
    procedure DoLookup; override;
  end;

var
  CatOtrasPer_DevEx: TCatOtrasPer_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatOtrasPer }

procedure TCatOtrasPer_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H60613_Percepciones_fijas;
end;

procedure TCatOtrasPer_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsOtrasPer.Conectar;
          DataSource.DataSet:= cdsOtrasPer;
     end;
end;

procedure TCatOtrasPer_DevEx.Refresh;
begin
     dmCatalogos.cdsOtrasPer.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatOtrasPer_DevEx.Agregar;
begin
     dmCatalogos.cdsOtrasPer.Agregar;
end;

procedure TCatOtrasPer_DevEx.Borrar;
begin
     dmCatalogos.cdsOtrasPer.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatOtrasPer_DevEx.Modificar;
begin
     dmCatalogos.cdsOtrasPer.Modificar;
end;

procedure TCatOtrasPer_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Otras Percepciones', 'TB_CODIGO', dmCatalogos.cdsOtrasPer );
end;

procedure TCatOtrasPer_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     TB_CODIGO.Options.Grouping:= FALSE;
     TB_ELEMENT.Options.Grouping:= FALSE;
     TB_MONTO.Options.Grouping:= FALSE;
     TB_TASA.Options.Grouping:= FALSE;
     TB_IMSS.Options.Grouping:= FALSE;
     TB_ISPT.Options.Grouping:= FALSE;
end;

procedure TCatOtrasPer_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;
     //DevEx (by am):Configuracion Especial para agrupamiento
     ConfigAgrupamiento;
end;

end.
