unit FEditCatValFactores_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, DBCtrls, 
  StdCtrls, Mask, ZetaNumero, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, cxContainer, cxEdit,
  cxTextEdit, cxMemo, cxDBEdit;

type
  TEditCatValFactores_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    VL_CODIGO: TZetaDBNumero;
    VF_ORDEN: TZetaDBNumero;
    VF_CODIGO: TDBEdit;
    VF_NOMBRE: TDBEdit;
    VF_DESCRIP: TcxDBMemo;
    VF_INGLES: TDBEdit;
    VF_DES_ING: TcxDBMemo;
    VF_TEXTO: TDBEdit;
    VF_NUMERO: TZetaDBNumero;
    VL_NOMBRE: TEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
     procedure Connect; override;
  public
    { Public declarations }
    procedure DoLookup; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
  end;

var
  EditCatValFactores_DevEx: TEditCatValFactores_DevEx;

implementation

uses dCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZAccesosTress;

{$R *.dfm}

{ TEditCatValFactores_DevEx }

procedure TEditCatValFactores_DevEx.Connect;
begin
     inherited;
     with dmCatalogos do
     begin
          DataSource.DataSet := cdsValFactores;
          VL_NOMBRE.Text:=cdsValPlantilla.GetDescripcion(VL_CODIGO.Text);
     end;
end;

procedure TEditCatValFactores_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Factor de Valulación', Self.Caption, 'VF_ORDEN', dmCatalogos.cdsValFactores );
end;

procedure TEditCatValFactores_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := VF_CODIGO;
     IndexDerechos := D_CAT_VAL_FACTORES;
     HelpContext:= H_VALUACION_FACTORES;
end;

function TEditCatValFactores_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
        Result:= dmCatalogos.PlantillaEnDiseno( dmCatalogos.PlantillaActiva, sMensaje, 'Agregar' );
end;

function TEditCatValFactores_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
        Result:= dmCatalogos.PlantillaEnDiseno( dmCatalogos.PlantillaActiva, sMensaje, 'Borrar' );
end;

end.
