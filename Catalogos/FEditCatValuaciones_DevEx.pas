unit FEditCatValuaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, DB, ExtCtrls, DBCtrls, 
  StdCtrls, ZetaNumero, Mask, ZetaFecha, ZetaKeyLookup_DevEx, ZetaDBTextBox,
  ComCtrls, ZetaKeyCombo, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, cxContainer,
  cxEdit, cxTextEdit, cxMemo, cxDBEdit, cxGroupBox, Buttons;

type
  TEditCatValuaciones_DevEx = class(TBaseEdicion_DevEx)
    PCPuntos: TcxPageControl;
    tsPuntos: TcxTabSheet;
    PageControl2: TcxPageControl;
    TabSheet3: TcxTabSheet;
    TabSheet4: TcxTabSheet;
    Panel2: TPanel;
    Label1: TLabel;
    VT_ORDEN: TZetaDBTextBox;
    Label2: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    VT_COMENTA: TcxDBMemo;
    tsValuacion: TcxTabSheet;
    Label11: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    BtnRecalculo: TcxButton;
    Label20: TLabel;
    GroupBox1: TcxGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    VP_GRADO: TZetaDBNumero;
    GroupBox2: TcxGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    PanelValuacion: TPanel;
    Label6: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Anterior: TcxButton;
    Siguiente: TcxButton;
    dsPuntos: TDataSource;
    VP_FOLIO: TZetaDBTextBox;
    VL_CODIGO: TZetaDBTextBox;
    PU_CODIGO: TZetaDBTextBox;
    VL_NOMBRE: TZetaDBTextBox;
    PU_DESCRIP: TZetaDBTextBox;
    VP_FECHA: TZetaDBTextBox;
    VP_STATUS: TZetaDBKeyCombo;
    VP_COMENTA: TcxDBMemo;
    VP_PT_NOM: TDBEdit;
    VP_PT_GRAD: TZetaDBNumero;
    VP_FEC_INI: TZetaDBFecha;
    VP_FEC_FIN: TZetaDBFecha;
    US_NOMBRE: TZetaDBTextBox;
    VT_CUAL: TZetaDBKeyLookup_DevEx;
    VP_PT_CODE: TDBEdit;
    VF_DESCRIP: TcxMemo;
    VF_CODIGO: TZetaTextBox;
    VF_NOMBRE: TZetaTextBox;
    VS_DESCRIP: TcxMemo;
    VS_CODIGO: TZetaTextBox;
    VS_NOMBRE: TZetaDBTextBox;
    VT_PUNTOS: TZetaDBTextBox;
    VP_PUNTOS: TZetaTextBox;
    VP_NUM_FIN: TZetaTextBox;
    Label21: TLabel;
    procedure FormShow(Sender: TObject);
    procedure AnteriorClick(Sender: TObject);
    procedure SiguienteClick(Sender: TObject);
    procedure dsPuntosDataChange(Sender: TObject; Field: TField);
    procedure BtnRecalculoClick(Sender: TObject);
    procedure tsValuacionShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure VP_STATUSChange(Sender: TObject);
  private
    { Private declarations }
    FPuntos: Double;
    FRespuestasFinal: Integer;
    procedure CreaCamposFactores;
    procedure SumaPuntos;
    procedure RecorreSiguiente;
    procedure RecorreAnterior;
    procedure InitControles;
    procedure ReasignaLookup;
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCatValuaciones_DevEx: TEditCatValuaciones_DevEx;

implementation

uses dCatalogos,
     dTablas,
     dCliente,
     ZAccesosTress,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo;

{$R *.dfm}

{ TEditCatValuaciones }

procedure TEditCatValuaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_VALUACIONES;
     HelpContext:= H_VALUACIONES;
   
end;

procedure TEditCatValuaciones_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dsPuntos.AutoEdit := DataSource.AutoEdit;
     InitControles;
end;

procedure TEditCatValuaciones_DevEx.Connect;
begin
     inherited;
     with dmCatalogos do
     begin
          cdsValPuntos.Refrescar;
          cdsPuntosNivel.Refrescar;
          DataSource.DataSet := cdsValuaciones;
          dsPuntos.DataSet:= cdsValPuntos;
     end;
end;

procedure TEditCatValuaciones_DevEx.CreaCamposFactores;
begin
     with dmCatalogos do
     begin
          if ( cdsSubfactores.Locate( 'VS_ORDEN', cdsValPuntos.FieldByName('VT_ORDEN').AsInteger, [] ) ) then
          begin
               VS_DESCRIP.Text:= cdsSubfactores.FieldByName('VS_DESCRIP').AsString;
               VS_CODIGO.Caption:= cdsSubfactores.FieldByName('VS_CODIGO').AsString;

               if( cdsValfactores.Locate('VF_ORDEN', cdsSubfactores.FieldByName('VF_ORDEN').AsInteger, [] ) ) then
               begin
                    VF_DESCRIP.Text:= cdsValfactores.FieldByName('VF_DESCRIP').AsString;
                    VF_NOMBRE.Caption:= cdsValfactores.FieldByName('VF_NOMBRE').AsString;
                    VF_CODIGO.Caption:= cdsValfactores.FieldByName('VF_CODIGO').AsString;
               end;
          end;
     end;
end;

procedure TEditCatValuaciones_DevEx.InitControles;
begin
     PCPuntos.ActivePage := tsPuntos;
     Siguiente.Enabled:= TRUE;
     Anterior.Enabled:= FALSE;
     ActiveControl := VT_CUAL;
     with dmCatalogos.cdsValPuntos do
     begin
          First;
          if ( ( ( dsPuntos.DataSet.FieldByName( 'VT_CUAL' ).AsInteger <> 0 ) or
             ( RecordCount = 0 ) ) and
             ( dmCatalogos.AgregandoVal = TRUE ) ) then
          begin
               Repeat
                     RecorreSiguiente;
               Until (  FieldByName('VT_CUAL').AsInteger = 0 ) or ( EOF );
          end
          else
          begin
               ReasignaLookup;
          end;
     end;
end;

procedure TEditCatValuaciones_DevEx.SumaPuntos;
var
   iRespuestas: Integer;
begin
     inherited;
     dmCatalogos.ObtieneTotalesValuacion( FPuntos, iRespuestas );
     VP_PUNTOS.Caption := FormatFloat( '#,0.00', FPuntos  );
     VP_NUM_FIN.Caption := IntToStr( iRespuestas );
     FRespuestasFinal:= iRespuestas;
end;

procedure TEditCatValuaciones_DevEx.ReasignaLookup;
begin
     with VT_CUAL do
     begin
          SetLlaveDescripcion( VACIO, VACIO );
          Valor := dsPuntos.DataSet.FieldByName( 'VT_CUAL' ).AsInteger;
     end;
end;

procedure TEditCatValuaciones_DevEx.RecorreSiguiente;
begin
     if PCPuntos.ActivePage = tsPuntos then
     begin
          with dsPuntos.DataSet do
          begin
               Next;
               ReasignaLookup;
               ActiveControl := VT_CUAL;
               Anterior.Enabled := TRUE;
               if EOF then
               begin
                    PCPuntos.ActivePage := tsValuacion;
                    ActiveControl := VP_STATUS;
                    Siguiente.Enabled := FALSE;
               end;
          end;
     end;
end;

procedure TEditCatValuaciones_DevEx.RecorreAnterior;
begin
     if PCPuntos.ActivePage = tsPuntos then
     begin
          with dsPuntos.DataSet do
          begin
               Prior;
               ReasignaLookup;
               Siguiente.Enabled := TRUE;

               if BOF then
               begin
                    Anterior.Enabled := FALSE;
               end;
          end;
     end
     else
     begin
          if ( not dsPuntos.DataSet.IsEmpty ) then
          begin
               PCPuntos.ActivePage := tsPuntos;
               ReasignaLookup;
               Siguiente.Enabled := TRUE;
          end
          else
              Anterior.Enabled := FALSE;
     end;
     with VT_CUAL do
     begin
          if CanFocus then
             VT_CUAL.SetFocus;
     end;
end;

procedure TEditCatValuaciones_DevEx.dsPuntosDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( ( dsPuntos.DataSet.Active ) and ( Field = NIL ) ) then
     begin
          with dmCatalogos do
          begin
               VT_CUAL.Filtro:= Format( '( VS_ORDEN = %d )',[ cdsValPuntos.FieldByName('VT_ORDEN').AsInteger ]);
          end;
          CreaCamposFactores;
     end;
end;

procedure TEditCatValuaciones_DevEx.tsValuacionShow(Sender: TObject);
begin
     if ( dmCatalogos.cdsValPuntos.State <> dsInactive ) then
        SumaPuntos;
end;

procedure TEditCatValuaciones_DevEx.AnteriorClick(Sender: TObject);
begin
     RecorreAnterior;
end;

procedure TEditCatValuaciones_DevEx.SiguienteClick(Sender: TObject);
begin
     RecorreSiguiente;
end;

procedure TEditCatValuaciones_DevEx.BtnRecalculoClick(Sender: TObject);
begin
     with dmCatalogos do
     begin
          if cdsValPlantilla.Locate('VL_CODIGO', IntToStr( cdsValuaciones.FieldByName('VL_CODIGO').AsInteger ), [] )  then
          begin
               if not ( cdsValuaciones.State in [ dsInsert, dsEdit ] ) then
                  cdsValuaciones.Edit;
               cdsValuaciones.FieldByName('VP_GRADO').AsFloat:= dmTablas.GetNumericaCuota( cdsValPlantilla.FieldByName('VL_TAB_PTS').AsInteger,
                                                                cdsValuaciones.FieldByName('VP_FECHA').AsDateTime,
                                                                FPuntos );
          end;
     end;
end;

procedure TEditCatValuaciones_DevEx.VP_STATUSChange(Sender: TObject);
begin
     inherited;
     { AP: Se cambi� porque hasta aqui VP_NUM_FIN no trae el valor real cuando no ha grabado.
     if ( eStatusValuacion( VP_STATUS.ItemIndex ) = svTerminada ) and
        ( dmCatalogos.cdsValuaciones.FieldByName('VP_NUM_FIN').AsInteger
         <> dmCatalogos.cdsValPuntos.RecordCount )then }
     if ( eStatusValuacion( VP_STATUS.ItemIndex ) = svTerminada ) and
        ( FRespuestasFinal <> dmCatalogos.cdsValPuntos.RecordCount )then
     begin
          if not ( ZetaDialogo.ZWarningConfirm('Advertencia', 'No se han terminado de evaluar todos los subfactores, � Desea Continuar con el '+
                                                     ' Cambio de status ?',0, mbCancel ) ) then
          begin
               VP_STATUS.ItemIndex:= DataSource.DataSet.FieldByName( 'VP_STATUS' ).AsInteger;
          end;

     end;
end;

end.


