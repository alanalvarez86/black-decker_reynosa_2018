unit FCopiaPerfil_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ZBaseDlgModal_DevEx, ZetaKeyLookup_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons, cxControls, cxContainer,
  cxEdit, cxGroupBox;

type
  TCopiaPerfil_DevEx = class(TZetaDlgModal_DevEx)
    GroupBox1: TcxGroupBox;
    CB_PUESTO: TZetaKeyLookup_DevEx;
    Label2: TLabel;
    GroupBox2: TcxGroupBox;
    Perfil: TZetaKeyLookup_DevEx;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure setEditarSoloActivos;
  private
    { Private declarations }
    FPuesto:string;
  public
    { Public declarations }
    property Puesto: string read FPuesto write FPuesto;
  end;

var
  CopiaPerfil_DevEx: TCopiaPerfil_DevEx;

implementation

uses
    ZetaCommonClasses,
    ZetaDialogo,
    dCatalogos;

{$R *.dfm}

procedure TCopiaPerfil_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          cdsPerfilesPuesto.Conectar;
          Perfil.LookupDataSet := cdsPerfilesPuesto;
          CB_PUESTO.LookupDataSet :=  cdsPuestos;
     end;
     HelpContext := H_COPIAR_PERFIL;
     //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TCopiaPerfil_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Perfil.Llave := FPuesto;
     CB_PUESTO.Llave := VACIO;
     CB_PUESTO.SetFocus;
end;

procedure TCopiaPerfil_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     if  ( not( CB_PUESTO.Llave = VACIO ) ) then
     begin
          with dmCatalogos do
          begin
               if ( PuedeCrearPerfil(CB_PUESTO.Llave) )then
               begin
                    CopiarPerfil( Perfil.Llave, CB_PUESTO.Llave );
                    Close;
               end
               else
               begin
                    ZetaDialogo.zError( 'Puesto con perfil', 'El puesto seleccionado ya tiene asignado un perfil', 0 );
               end;
          end;
     end
     else ZetaDialogo.zError( 'Puesto con perfil', 'El puesto no puede quedar vac�o', 0 );
end;


procedure TCopiaPerfil_DevEx.SetEditarSoloActivos;
begin
     CB_PUESTO.EditarSoloActivos := TRUE;
end;


end.
