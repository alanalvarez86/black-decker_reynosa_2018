unit FCatPerfilesPuesto_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ExtCtrls, ZBaseGridLectura_DevEx,
  cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TCatPerfilesPuesto_DevEx = class(TBaseGridLectura_DevEx)
    Panel2: TPanel;
    bCopiar: TcxButton;
    PU_CODIGO: TcxGridDBColumn;
    PU_DESCRIP: TcxGridDBColumn;
    PF_CONTRL1: TcxGridDBColumn;
    PF_CONTRL2: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure bCopiarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DoLookup; override;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
end;

var
  CatPerfilesPuesto_DevEx: TCatPerfilesPuesto_DevEx;

implementation

uses
    dCatalogos,
    FCopiaPerfil_DevEx,
    ZetaCommonClasses,
    ZetaDialogo,
    ZetaBuscador_DevEx;

{$R *.dfm}

{ TCatPerfilesPuesto }

procedure TCatPerfilesPuesto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_CAT_PERFILES_PUESTO;
end;

procedure TCatPerfilesPuesto_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsPerfilesPuesto.Conectar;
          DataSource.DataSet := cdsPerfilesPuesto;
     end;
end;

procedure TCatPerfilesPuesto_DevEx.Refresh;
begin
     dmCatalogos.cdsPerfilesPuesto.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatPerfilesPuesto_DevEx.Agregar;
begin
     dmCatalogos.cdsPerfilesPuesto.Agregar;
end;

procedure TCatPerfilesPuesto_DevEx.Borrar;
begin
     dmCatalogos.cdsPerfilesPuesto.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatPerfilesPuesto_DevEx.Modificar;
begin
     inherited;
     dmCatalogos.cdsPerfilesPuesto.Modificar;
end;

procedure TCatPerfilesPuesto_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo',Self.Caption, 'PU_CODIGO', dmCatalogos.cdsPerfilesPuesto );
end;

procedure TCatPerfilesPuesto_DevEx.bCopiarClick(Sender: TObject);
var sMensaje:string;
begin
     inherited;
     if ( PuedeAgregar (sMensaje) ) then
     begin
          if CopiaPerfil_DevEx = NIL then
             CopiaPerfil_DevEx := TCopiaPerfil_DevEx.Create( Self );
          try
             with CopiaPerfil_DevEx do
             begin
                  Puesto := dmCatalogos.cdsPerfilesPuesto.FieldByName('PU_CODIGO').AsString;
                  ShowModal;
             end;
          finally
                 FreeAndNil(CopiaPerfil_DevEx);
          end;
          ZetaDBGrid.SetFocus;
     end
     else
         zInformation( Caption, 'No tiene permisos para copiar perfil', 0 );
end;

procedure TCatPerfilesPuesto_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     bCopiar.Enabled := ( not NoHayDatos );
end;

procedure TCatPerfilesPuesto_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('PU_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

end.
