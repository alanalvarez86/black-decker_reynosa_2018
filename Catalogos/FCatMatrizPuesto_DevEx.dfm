inherited CatMatrizPuesto_DevEx: TCatMatrizPuesto_DevEx
  Left = 526
  Top = 230
  Caption = 'Matriz de Entrenamiento por Puesto'
  ClientWidth = 601
  ExplicitWidth = 601
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 601
    TabOrder = 2
    ExplicitWidth = 601
    inherited Slider: TSplitter
      Left = 378
      ExplicitLeft = 378
    end
    inherited ValorActivo1: TPanel
      Width = 362
      ExplicitWidth = 362
      inherited textoValorActivo1: TLabel
        Width = 356
      end
    end
    inherited ValorActivo2: TPanel
      Left = 381
      Width = 220
      ExplicitLeft = 381
      ExplicitWidth = 220
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 214
        ExplicitLeft = 3
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 601
    Height = 46
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 37
      Top = 16
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Puesto:'
    end
    object LookUpPuesto: TZetaKeyLookup_DevEx
      Left = 80
      Top = 12
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = LookUpPuestoValidKey
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 65
    Width = 601
    Height = 159
    ExplicitTop = 65
    ExplicitWidth = 601
    ExplicitHeight = 159
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CU_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CU_CODIGO'
      end
      object CU_NOMBRE: TcxGridDBColumn
        Caption = 'Curso'
        DataBinding.FieldName = 'CU_NOMBRE'
      end
      object CU_REVISIO: TcxGridDBColumn
        Caption = 'Revisi'#243'n'
        DataBinding.FieldName = 'CU_REVISIO'
      end
      object EN_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as de Antig'#252'edad'
        DataBinding.FieldName = 'EN_DIAS'
      end
      object EN_OPCIONA: TcxGridDBColumn
        Caption = 'Opcional'
        DataBinding.FieldName = 'EN_OPCIONA'
      end
      object EN_LISTA: TcxGridDBColumn
        Caption = 'Lista'
        DataBinding.FieldName = 'EN_LISTA'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
