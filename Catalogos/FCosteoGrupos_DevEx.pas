unit FCosteoGrupos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TCosteoGrupos_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  CosteoGrupos_DevEx: TCosteoGrupos_DevEx;

implementation

{$R *.dfm}

uses
     ZetaCommonClasses,
     DCatalogos;
     
procedure TCosteoGrupos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H00002_Costeo_Grupos;
end;

procedure TCosteoGrupos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCosteoGrupos.Conectar;
          DataSource.DataSet:= cdsCosteoGrupos;
     end;
end;

procedure TCosteoGrupos_DevEx.Agregar;
begin
     inherited;
     dmCatalogos.cdsCosteoGrupos.Agregar;
end;

procedure TCosteoGrupos_DevEx.Borrar;
begin
     inherited;
     dmCatalogos.cdsCosteoGrupos.Borrar;
end;

procedure TCosteoGrupos_DevEx.Modificar;
begin
     inherited;
     dmCatalogos.cdsCosteoGrupos.Modificar;
end;

procedure TCosteoGrupos_DevEx.Refresh;
begin
     inherited;
     dmCatalogos.cdsCosteoGrupos.Refrescar;
end;

procedure TCosteoGrupos_DevEx.FormShow(Sender: TObject);
begin
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0],0,'',SkCount );
  inherited;

end;

end.
