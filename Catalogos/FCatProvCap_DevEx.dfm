inherited CatProvCap_DevEx: TCatProvCap_DevEx
  Left = 314
  Top = 232
  Caption = 'Proveedores de Capacitaci'#243'n'
  ClientHeight = 290
  ClientWidth = 760
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 760
    inherited ValorActivo2: TPanel
      Width = 501
      inherited textoValorActivo2: TLabel
        Width = 495
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 760
    Height = 271
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object PC_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'PC_CODIGO'
      end
      object PC_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PC_NOMBRE'
      end
      object PC_DIRECCI: TcxGridDBColumn
        Caption = 'Direcci'#243'n'
        DataBinding.FieldName = 'PC_DIRECCI'
      end
      object PC_CIUDAD: TcxGridDBColumn
        Caption = 'Ciudad'
        DataBinding.FieldName = 'PC_CIUDAD'
      end
      object PC_TEL: TcxGridDBColumn
        Caption = 'Tel'#233'fono'
        DataBinding.FieldName = 'PC_TEL'
      end
      object PC_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'PC_ACTIVO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
