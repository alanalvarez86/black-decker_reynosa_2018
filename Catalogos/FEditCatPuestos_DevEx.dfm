inherited EditCatPuestos_DevEx: TEditCatPuestos_DevEx
  Left = 435
  Top = 293
  Caption = 'Puestos'
  ClientHeight = 470
  ClientWidth = 585
  ExplicitWidth = 591
  ExplicitHeight = 499
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 434
    Width = 585
    TabOrder = 3
    ExplicitTop = 434
    ExplicitWidth = 585
    inherited OK_DevEx: TcxButton
      Left = 418
      ExplicitLeft = 418
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 497
      ExplicitLeft = 497
    end
    object btnFijas_DevEx: TcxButton
      Left = 8
      Top = 5
      Width = 129
      Height = 26
      Hint = 'Percepciones Fijas'
      Caption = 'Percepciones &Fijas'
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        200000000000000900000000000000000000000000000000000050D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF79DDACFFAFEB
        CEFFD0F3E2FFF4FCF8FFF7FDFAFFD6F4E6FFB2EBD0FF87E0B5FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF5ED69BFFC0EFD8FFFFFFFFFFFAFE
        FCFFD3F4E4FFADEACCFFAAE9CAFFCEF2E1FFFAFEFCFFFFFFFFFFCBF2DFFF63D7
        9FFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF76DCABFFF4FCF8FFFAFEFCFF97E4BFFF55D3
        96FF50D293FF50D293FF50D293FF50D293FF55D396FF92E3BCFFF4FCF8FFF4FC
        F8FF81DFB1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF5BD59AFFE9F9F1FFE6F9F0FF71DAA7FF50D293FF5ED6
        9BFF95E4BDFFB5ECD1FFB5ECD1FF95E4BDFF63D79FFF50D293FF63D79FFFD6F4
        E6FFF1FBF7FF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFC3EFDAFFFCFEFDFF71DAA7FF50D293FF8AE1B7FFF1FB
        F7FFFFFFFFFFE6F9F0FFE6F9F0FFFFFFFFFFF7FDFAFF9AE5C1FF50D293FF63D7
        9FFFF4FCF8FFCEF2E1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF71DAA7FFFFFFFFFFA8E9C9FF50D293FF7FDEB0FFFFFFFFFFFFFF
        FFFFFAFEFCFF97E4BFFF97E4BFFFFAFEFCFFFFFFFFFFFFFFFFFF9AE5C1FF50D2
        93FF92E3BCFFFFFFFFFF84DFB3FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF9DE6C2FFFFFFFFFF5BD59AFF58D498FFECFAF3FFFFFFFFFFFCFE
        FDFF6BD9A4FF58D498FF6EDAA6FF63D79FFFF4FCF8FFFFFFFFFFF7FDFAFF63D7
        9FFF55D396FFF7FDFAFFB2EBD0FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFCEF2E1FFE1F7ECFF50D293FF84DFB3FFFFFFFFFFFFFFFFFFE1F7
        ECFF92E3BCFFB2EBD0FFBDEED6FF6EDAA6FFBAEDD5FFFFFFFFFFFFFFFFFF95E4
        BDFF50D293FFCEF2E1FFD9F5E7FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFF7FDFAFFB8EDD3FF50D293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFADEACCFF69D8A2FF50D293FFD9F5E7FFFFFFFFFFFFFFFFFFB5EC
        D1FF50D293FFAAE9CAFFFCFEFDFF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFF4FCF8FFBDEED6FF50D293FFA0E6C4FFFFFFFFFFFFFFFFFFFFFF
        FFFF8FE2BAFF50D293FF76DCABFFCBF2DFFFFFFFFFFFFFFFFFFFFFFFFFFFB2EB
        D0FF50D293FFADEACCFFFAFEFCFF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFC8F1DDFFE9F9F1FF50D293FF7FDEB0FFFFFFFFFFFFFFFFFFEFFB
        F5FF50D293FF95E4BDFFBDEED6FFECFAF3FFFFFFFFFFFFFFFFFFFFFFFFFF8FE2
        BAFF50D293FFD3F4E4FFD9F5E7FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF9DE6C2FFFFFFFFFF66D8A1FF55D396FFE1F7ECFFFFFFFFFFFFFF
        FFFF66D8A1FF6BD9A4FF6BD9A4FF5BD59AFFEFFBF5FFFFFFFFFFF1FBF7FF5ED6
        9BFF55D396FFFAFEFCFFB2EBD0FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF6EDAA6FFFFFFFFFFB2EBD0FF50D293FF71DAA7FFF7FDFAFFFFFF
        FFFFEFFBF5FF8AE1B7FF84DFB3FFDEF7EBFFFFFFFFFFFFFFFFFF8AE1B7FF50D2
        93FF9AE5C1FFFFFFFFFF7FDEB0FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFB2EBD0FFFFFFFFFF81DFB1FF50D293FF6BD9A4FFD9F5
        E7FFFFFFFFFFF7FDFAFFF7FDFAFFFFFFFFFFE9F9F1FF7CDDAEFF50D293FF6EDA
        A6FFFAFEFCFFC5F0DBFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF55D396FFDEF7EBFFF1FBF7FF7CDDAEFF50D293FF53D3
        95FF71DAA7FF95E4BDFF9AE5C1FF74DBA9FF55D396FF50D293FF6BD9A4FFE4F8
        EEFFF1FBF7FF5ED69BFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF69D8A2FFDEF7EBFFFFFFFFFFB5ECD1FF5ED6
        9BFF50D293FF50D293FF50D293FF50D293FF5BD59AFFAAE9CAFFFCFEFDFFECFA
        F3FF79DDACFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF53D395FFA8E9C9FFFCFEFDFFFFFF
        FFFFE6F9F0FFC5F0DBFFC5F0DBFFE6F9F0FFFFFFFFFFFFFFFFFFC5F0DBFF5BD5
        9AFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF69D8A2FF9AE5
        C1FFC0EFD8FFE9F9F1FFEFFBF5FFC8F1DDFFA0E6C4FF76DCABFF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnFijas_DevExClick
    end
    object btnTools_DevEx: TcxButton
      Left = 142
      Top = 5
      Width = 107
      Height = 26
      Hint = 'Herramientas'
      Caption = '&Herramientas'
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        20000000000000090000000000000000000000000000000000003399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF76BBFFFFCCE5FFFFACD5FFFF56ABFFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF49A4
        FFFF96CBFFFF369BFFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF50A7FFFFFFFFFFFFBCDDFFFFE5F2FFFFF5FAFFFF56ABFFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF80BFFFFFF2F9
        FFFFFCFDFFFF53A9FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF89C4FFFFF5FAFFFF3399FFFF80BFFFFFFFFFFFFFECF5FFFF53A9
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3D9EFFFFC2E1FFFFFFFFFFFFFFFF
        FFFF99CCFFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF49A4FFFFFFFFFFFFCCE5FFFFECF5FFFFFFFFFFFFFFFFFFFFE5F2
        FFFF46A3FFFF3399FFFF3399FFFF3399FFFF73B9FFFFFFFFFFFFFFFFFFFFDFEF
        FFFF399CFFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF8DC6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF80BFFFFF3399FFFF3399FFFF46A3FFFFDFEFFFFFFFFFFFFFF2F9FFFF60AF
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF8DC6FFFFFFFFFFFFFFFFFFFFFFFFFFFF8DC6
        FFFF50A7FFFF3399FFFF46A3FFFFDFEFFFFFFCFDFFFF7DBEFFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF7DBEFFFFFCFDFFFF8DC6FFFF8DC6
        FFFFFCFDFFFF76BBFFFF83C1FFFFFCFDFFFF76BBFFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF50A7FFFF8DC6FFFFFFFF
        FFFFFFFFFFFFFCFDFFFF76BBFFFF50A7FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF8DC6FFFFECF5FFFF56AB
        FFFFDCEDFFFFFFFFFFFFFCFDFFFF76BBFFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF8DC6FFFFECF5FFFF56ABFFFFBCDD
        FFFFBCDDFFFFB2D9FFFFFCFDFFFF76BBFFFF80BFFFFFD5EAFFFFF5FAFFFFDFEF
        FFFFACD5FFFF409FFFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF8DC6FFFFECF5FFFF56ABFFFFBCDDFFFFBCDD
        FFFF56ABFFFFE9F4FFFF76BBFFFFA5D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE5F2FFFF409FFFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF8DC6FFFFECF5FFFF56ABFFFFBCDDFFFFBCDDFFFF56AB
        FFFFE9F4FFFF76BBFFFF60AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFACD5FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF70B7FFFFECF5FFFF56ABFFFFBCDDFFFFBCDDFFFF56ABFFFFE9F4
        FFFF76BBFFFF3399FFFF9FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2F1FFFF89C4
        FFFFC9E4FFFFFFFFFFFFECF5FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFFC5E2FFFF93C9FFFFBCDDFFFFBCDDFFFF56ABFFFFE9F4FFFF76BB
        FFFF3399FFFF3399FFFFB2D9FFFFFFFFFFFFFFFFFFFFF5FAFFFF46A3FFFF3399
        FFFF369BFFFFBCDDFFFFFFFFFFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFFC5E2FFFFFFFFFFFFCCE5FFFF56ABFFFFE9F4FFFF76BBFFFF3399
        FFFF3399FFFF3399FFFF93C9FFFFFFFFFFFFFFFFFFFFD9ECFFFF3399FFFF3399
        FFFF3399FFFF369BFFFF99CCFFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF6DB6FFFFF9FCFFFFF2F9FFFFE5F2FFFF76BBFFFF3399FFFF3399
        FFFF3399FFFF3399FFFF4DA6FFFFF5FAFFFFFFFFFFFFFFFFFFFF8DC6FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF409FFFFF76BBFFFF399CFFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF7DBEFFFFF9FCFFFFFFFFFFFFFFFFFFFF8DC6
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF53A9FFFFA2D1FFFFC2E1FFFFACD5
        FFFF46A3FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
        FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnTools_DevExClick
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 585
    TabOrder = 0
    ExplicitTop = 26
    ExplicitWidth = 585
    inherited Splitter: TSplitter
      Left = 177
      Width = 30
      ExplicitLeft = 177
      ExplicitWidth = 30
    end
    inherited ValorActivo1: TPanel
      Width = 177
      ExplicitWidth = 177
      inherited textoValorActivo1: TLabel
        Width = 171
      end
    end
    inherited ValorActivo2: TPanel
      Left = 207
      Width = 378
      ExplicitLeft = 207
      ExplicitWidth = 378
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 372
        ExplicitLeft = 292
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Width = 72
    ExplicitWidth = 72
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 45
    Width = 585
    Height = 52
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 60
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 37
      Top = 30
      Width = 59
      Height = 13
      Caption = 'Descripci'#243'n:'
    end
    object PU_CODIGO: TZetaDBEdit
      Left = 100
      Top = 4
      Width = 60
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'PU_CODIGO'
      DataSource = DataSource
    end
    object PU_DESCRIP: TDBEdit
      Left = 100
      Top = 25
      Width = 301
      Height = 21
      DataField = 'PU_DESCRIP'
      DataSource = DataSource
      TabOrder = 1
    end
    object PU_ACTIVO: TDBCheckBox
      Left = 349
      Top = 6
      Width = 52
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Activo:'
      DataField = 'PU_ACTIVO'
      DataSource = DataSource
      TabOrder = 2
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
  end
  object PageControl_DevEx: TcxPageControl [4]
    Left = 0
    Top = 97
    Width = 585
    Height = 337
    Align = alClient
    TabOrder = 2
    Properties.ActivePage = tsGenerales_DevEx
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 336
    ClientRectLeft = 1
    ClientRectRight = 584
    ClientRectTop = 21
    object tsGenerales_DevEx: TcxTabSheet
      Caption = 'Generales'
      object TLabel
        Left = 115
        Top = 108
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Subcuenta:'
      end
      object Label12: TLabel
        Left = 136
        Top = 12
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object Label9: TLabel
        Left = 117
        Top = 184
        Width = 52
        Height = 13
        Caption = 'Funciones:'
      end
      object Label7: TLabel
        Left = 113
        Top = 155
        Width = 56
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n'
      end
      object Label8: TLabel
        Left = 136
        Top = 170
        Width = 12
        Height = 13
        Caption = 'de'
      end
      object Label6: TLabel
        Left = 139
        Top = 84
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Texto:'
      end
      object Label5: TLabel
        Left = 129
        Top = 60
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object Label3: TLabel
        Left = 138
        Top = 36
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ingl'#233's:'
      end
      object PU_SUB_CTA: TDBEdit
        Left = 173
        Top = 104
        Width = 274
        Height = 21
        DataField = 'PU_SUB_CTA'
        DataSource = DataSource
        TabOrder = 4
      end
      object PU_TIPO: TZetaDBKeyCombo
        Left = 173
        Top = 8
        Width = 121
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfTipoPuesto
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'PU_TIPO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object PU_DETALLE: TDBMemo
        Left = 173
        Top = 128
        Width = 274
        Height = 112
        DataField = 'PU_DETALLE'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 5
      end
      object PU_TEXTO: TDBEdit
        Left = 173
        Top = 80
        Width = 274
        Height = 21
        DataField = 'PU_TEXTO'
        DataSource = DataSource
        TabOrder = 3
      end
      object PU_NUMERO: TZetaDBNumero
        Left = 173
        Top = 56
        Width = 121
        Height = 21
        Mascara = mnNumeroGlobal
        TabOrder = 2
        Text = '0.00'
        UseEnterKey = True
        DataField = 'PU_NUMERO'
        DataSource = DataSource
      end
      object PU_INGLES: TDBEdit
        Left = 173
        Top = 32
        Width = 274
        Height = 21
        DataField = 'PU_INGLES'
        DataSource = DataSource
        TabOrder = 1
      end
    end
    object tsContratacion_DevEx: TcxTabSheet
      Caption = 'Contrataci'#243'n'
      object Label21: TLabel
        Left = 107
        Top = 139
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Checa Tarjeta:'
      end
      object Label4: TLabel
        Left = 115
        Top = 20
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
      end
      object Arealbl: TLabel
        Left = 152
        Top = 116
        Width = 25
        Height = 13
        Alignment = taRightJustify
        BiDiMode = bdLeftToRight
        Caption = 'Area:'
        ParentBiDiMode = False
      end
      object Label29: TLabel
        Left = 93
        Top = 68
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = 'Registro Patronal:'
      end
      object Label27: TLabel
        Left = 95
        Top = 92
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Contrato:'
      end
      object Label26: TLabel
        Left = 144
        Top = 43
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object PU_CHECA: TZetaDBKeyCombo
        Left = 181
        Top = 135
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 5
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'PU_CHECA'
        DataSource = DataSource
        LlaveNumerica = False
      end
      object PU_CLASIFI: TZetaDBKeyLookup_DevEx
        Left = 181
        Top = 16
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'PU_CLASIFI'
        DataSource = DataSource
      end
      object PU_AREA: TZetaDBKeyLookup_DevEx
        Left = 181
        Top = 112
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'PU_AREA'
        DataSource = DataSource
      end
      object PU_PATRON: TZetaDBKeyLookup_DevEx
        Left = 181
        Top = 64
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'PU_PATRON'
        DataSource = DataSource
      end
      object PU_CONTRAT: TZetaDBKeyLookup_DevEx
        Left = 181
        Top = 88
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'PU_CONTRAT'
        DataSource = DataSource
      end
      object PU_TURNO: TZetaDBKeyLookup_DevEx
        Left = 181
        Top = 40
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'PU_TURNO'
        DataSource = DataSource
      end
    end
    object tsAreas_DevEx: TcxTabSheet
      Caption = 'Areas'
      object PU_NIVEL12lbl: TLabel
        Left = 107
        Top = 278
        Width = 49
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel #12:'
        Visible = False
      end
      object PU_NIVEL11lbl: TLabel
        Left = 107
        Top = 254
        Width = 49
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel #11:'
        Visible = False
      end
      object PU_NIVEL10lbl: TLabel
        Left = 107
        Top = 230
        Width = 49
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel #10:'
        Visible = False
      end
      object PU_NIVEL9lbl: TLabel
        Left = 113
        Top = 206
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel #9:'
      end
      object PU_NIVEL8lbl: TLabel
        Left = 113
        Top = 182
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel #8:'
      end
      object PU_NIVEL7lbl: TLabel
        Left = 113
        Top = 158
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel #7:'
      end
      object PU_NIVEL1lbl: TLabel
        Left = 113
        Top = 14
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel #1:'
      end
      object PU_NIVEL6lbl: TLabel
        Left = 113
        Top = 134
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel #6:'
      end
      object PU_NIVEL5lbl: TLabel
        Left = 113
        Top = 110
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel #5:'
      end
      object PU_NIVEL4lbl: TLabel
        Left = 113
        Top = 86
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel #4:'
      end
      object PU_NIVEL3lbl: TLabel
        Left = 113
        Top = 62
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel #3:'
      end
      object PU_NIVEL2lbl: TLabel
        Left = 113
        Top = 38
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel #2:'
      end
      object PU_NIVEL12: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 274
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = PU_NIVEL12ValidLookup
        DataField = 'PU_NIVEL12'
      end
      object PU_NIVEL11: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 250
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = PU_NIVEL11ValidLookup
        DataField = 'PU_NIVEL11'
      end
      object PU_NIVEL10: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 226
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = PU_NIVEL10ValidLookup
        DataField = 'PU_NIVEL10'
      end
      object PU_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 202
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PU_NIVEL9ValidLookup
        DataField = 'PU_NIVEL9'
        DataSource = DataSource
      end
      object PU_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 178
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PU_NIVEL8ValidLookup
        DataField = 'PU_NIVEL8'
        DataSource = DataSource
      end
      object PU_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 154
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PU_NIVEL7ValidLookup
        DataField = 'PU_NIVEL7'
        DataSource = DataSource
      end
      object PU_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 10
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'PU_NIVEL1'
        DataSource = DataSource
      end
      object PU_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 130
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PU_NIVEL6ValidLookup
        DataField = 'PU_NIVEL6'
        DataSource = DataSource
      end
      object PU_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 106
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PU_NIVEL5ValidLookup
        DataField = 'PU_NIVEL5'
        DataSource = DataSource
      end
      object PU_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 82
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PU_NIVEL4ValidLookup
        DataField = 'PU_NIVEL4'
        DataSource = DataSource
      end
      object PU_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 58
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PU_NIVEL3ValidLookup
        DataField = 'PU_NIVEL3'
        DataSource = DataSource
      end
      object PU_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 34
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PU_NIVEL2ValidLookup
        DataField = 'PU_NIVEL2'
        DataSource = DataSource
      end
    end
    object tsSalario_DevEx: TcxTabSheet
      Caption = 'Salario'
      ImageIndex = 3
      object LblTabulador: TLabel
        Left = 110
        Top = 28
        Width = 107
        Height = 13
        Alignment = taRightJustify
        Caption = ' Salario por Tabulador:'
      end
      object Label28: TLabel
        Left = 108
        Top = 124
        Width = 109
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tabla de Prestaciones:'
      end
      object Label33: TLabel
        Left = 106
        Top = 76
        Width = 111
        Height = 13
        Alignment = taRightJustify
        Caption = 'Promedio de  Variables:'
      end
      object LblSalario: TLabel
        Left = 182
        Top = 52
        Width = 35
        Height = 13
        Alignment = taRightJustify
        Caption = 'Salario:'
      end
      object Label31: TLabel
        Left = 134
        Top = 100
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Zona Geogr'#225'fica:'
      end
      object PU_ZONA_GE: TDBComboBox
        Left = 221
        Top = 96
        Width = 60
        Height = 21
        Style = csDropDownList
        DataField = 'PU_ZONA_GE'
        DataSource = DataSource
        Items.Strings = (
          'A'
          'B'
          'C')
        TabOrder = 3
      end
      object GBRangoSal: TGroupBox
        Left = 0
        Top = 173
        Width = 583
        Height = 142
        Align = alBottom
        Caption = ' Rango Salarial: '
        TabOrder = 5
        object Label16: TLabel
          Left = 152
          Top = 16
          Width = 65
          Height = 13
          Alignment = taRightJustify
          Caption = 'Valor M'#237'nimo:'
        end
        object Label17: TLabel
          Left = 151
          Top = 64
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Valor M'#225'ximo:'
        end
        object Label18: TLabel
          Left = 158
          Top = 40
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Valor Medio:'
        end
        object Label19: TLabel
          Left = 153
          Top = 88
          Width = 64
          Height = 13
          Alignment = taRightJustify
          Caption = 'Encuesta #1:'
        end
        object Label20: TLabel
          Left = 153
          Top = 112
          Width = 64
          Height = 13
          Alignment = taRightJustify
          Caption = 'Encuesta #2:'
        end
        object PU_SAL_MIN: TZetaDBNumero
          Left = 221
          Top = 12
          Width = 100
          Height = 21
          Mascara = mnPesos
          TabOrder = 0
          Text = '0.00'
          DataField = 'PU_SAL_MIN'
          DataSource = DataSource
        end
        object PU_SAL_MAX: TZetaDBNumero
          Left = 221
          Top = 60
          Width = 100
          Height = 21
          Mascara = mnPesos
          TabOrder = 2
          Text = '0.00'
          DataField = 'PU_SAL_MAX'
          DataSource = DataSource
        end
        object PU_SAL_MED: TZetaDBNumero
          Left = 221
          Top = 36
          Width = 100
          Height = 21
          Mascara = mnPesos
          TabOrder = 1
          Text = '0.00'
          DataField = 'PU_SAL_MED'
          DataSource = DataSource
        end
        object PU_SAL_EN1: TZetaDBNumero
          Left = 221
          Top = 84
          Width = 100
          Height = 21
          Mascara = mnPesos
          TabOrder = 3
          Text = '0.00'
          DataField = 'PU_SAL_EN1'
          DataSource = DataSource
        end
        object PU_SAL_EN2: TZetaDBNumero
          Left = 221
          Top = 108
          Width = 100
          Height = 21
          Mascara = mnPesos
          TabOrder = 4
          Text = '0.00'
          DataField = 'PU_SAL_EN2'
          DataSource = DataSource
        end
      end
      object PU_AUTOSAL: TZetaDBKeyCombo
        Left = 221
        Top = 24
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        DropDownCount = 3
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'PU_AUTOSAL'
        DataSource = DataSource
        LlaveNumerica = False
      end
      object PU_TABLASS: TZetaDBKeyLookup_DevEx
        Left = 221
        Top = 120
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'PU_TABLASS'
        DataSource = DataSource
      end
      object PU_PER_VAR: TZetaDBNumero
        Left = 221
        Top = 72
        Width = 100
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
        DataField = 'PU_PER_VAR'
        DataSource = DataSource
      end
      object PU_SALARIO: TZetaDBNumero
        Left = 221
        Top = 48
        Width = 100
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        DataField = 'PU_SALARIO'
        DataSource = DataSource
      end
    end
    object tsOrganigrama_DevEx: TcxTabSheet
      Caption = 'Organigrama'
      ImageIndex = 4
      object GBPresupuesto: TGroupBox
        Left = 0
        Top = 0
        Width = 583
        Height = 315
        Align = alClient
        Caption = ' Presupuesto: '
        TabOrder = 0
        object Label13: TLabel
          Left = 188
          Top = 25
          Width = 46
          Height = 13
          Caption = 'Costo #1:'
        end
        object Label14: TLabel
          Left = 188
          Top = 50
          Width = 46
          Height = 13
          Caption = 'Costo #2:'
        end
        object Label15: TLabel
          Left = 188
          Top = 74
          Width = 46
          Height = 13
          Caption = 'Costo #3:'
        end
        object PU_COSTO1: TZetaDBNumero
          Left = 237
          Top = 21
          Width = 121
          Height = 21
          Mascara = mnPesosEmpresa
          TabOrder = 0
          Text = '0.00'
          DataField = 'PU_COSTO1'
          DataSource = DataSource
        end
        object PU_COSTO2: TZetaDBNumero
          Left = 237
          Top = 46
          Width = 121
          Height = 21
          Mascara = mnPesosEmpresa
          TabOrder = 1
          Text = '0.00'
          DataField = 'PU_COSTO2'
          DataSource = DataSource
        end
        object PU_COSTO3: TZetaDBNumero
          Left = 237
          Top = 70
          Width = 121
          Height = 21
          Mascara = mnPesosEmpresa
          TabOrder = 2
          Text = '0.00'
          DataField = 'PU_COSTO3'
          DataSource = DataSource
        end
      end
    end
    object cxTabSheet1: TcxTabSheet
      Caption = 'SAT'
      ImageIndex = 8
      ExplicitLeft = 2
      object GroupBox2: TGroupBox
        Left = 88
        Top = 24
        Width = 441
        Height = 62
        Caption = 'Clase de Riesgo de Puesto SAT'
        TabOrder = 0
        object Label11: TLabel
          Left = 17
          Top = 29
          Width = 30
          Height = 13
          Caption = 'Clave:'
        end
        object RIESGO_PUESTO_SAT: TZetaDBKeyLookup_DevEx
          Left = 53
          Top = 25
          Width = 379
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'PU_SAT_RSG'
          DataSource = DataSource
        end
      end
    end
    object STPS_DevEx: TcxTabSheet
      Caption = 'STPS'
      ImageIndex = 5
      object GroupBox1: TGroupBox
        Left = 80
        Top = 16
        Width = 441
        Height = 62
        Caption = ' Cat'#225'logo Nacional de Ocupaciones '
        TabOrder = 0
        object Label10: TLabel
          Left = 8
          Top = 29
          Width = 106
          Height = 13
          Caption = 'Clave ('#193'rea/Subarea):'
        end
        object ON_CODIGO: TZetaDBKeyLookup_DevEx
          Left = 117
          Top = 25
          Width = 300
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'PU_CLAVE'
          DataSource = DataSource
        end
      end
    end
    object tsConfidencialidad_DevEx: TcxTabSheet
      Caption = 'Confidencialidad'
      ImageIndex = 6
      object gbConfidencialidad: TGroupBox
        Left = 0
        Top = 0
        Width = 583
        Height = 315
        Align = alClient
        TabOrder = 0
        object rbConfidenTodas: TRadioButton
          Left = 102
          Top = 26
          Width = 113
          Height = 17
          Caption = 'Todas'
          TabOrder = 0
          OnClick = rbConfidenTodasClick
        end
        object rbConfidenAlgunas: TRadioButton
          Left = 102
          Top = 44
          Width = 156
          Height = 17
          Caption = 'Aplica algunas'
          TabOrder = 1
          OnClick = rbConfidenAlgunasClick
        end
        object listaConfidencialidad: TListBox
          Left = 102
          Top = 64
          Width = 371
          Height = 169
          TabStop = False
          ExtendedSelect = False
          ItemHeight = 13
          TabOrder = 2
        end
        object btSeleccionarConfiden_DevEx: TcxButton
          Left = 476
          Top = 64
          Width = 26
          Height = 26
          Hint = 'Seleccionar Confidencialidad'
          OptionsImage.Glyph.Data = {
            36090000424D3609000000000000360000002800000018000000180000000100
            200000000000000900000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFAFE7F8FF3CC4EEFF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFCBEF
            FBFF5CCEF1FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF78D6F3FFFFFFFFFFFFFFFFFFE7F8
            FDFF70D4F3FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEFFAFEFF80D9F4FF10B7
            EAFF00B2E9FF0CB6EAFF38C3EEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF38C3EEFF18B9EBFF00B2E9FF00B2
            E9FF97E0F6FFE3F7FDFFC7EEFAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFBFEC
            F9FFFBFEFFFFFFFFFFFFFFFFFFFF5CCEF1FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEBF9FDFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFE7F8FDFF0CB6EAFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF20BCECFFF7FDFEFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83DAF4FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF83DAF4FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FDFEFF24BDECFF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0CB6EAFFE7F8
            FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABE6F8FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CF
            F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3EDFAFF34C2EDFF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FFCBEFFBFFFFFFFFFFDFF5FCFF58CDF1FF00B2E9FF00B2E9FF10B7EAFF0CB6
            EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF3CC4EEFF7CD7F4FF08B4EAFF00B2E9FF04B3E9FF70D4F3FFEFFAFEFF74D5
            F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF50CAF0FFDBF4FCFFFFFFFFFFFFFFFFFF68D1
            F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF04B3E9FFB7E9F9FFFFFFFFFFFFFFFFFFCBEFFBFF3CC4EEFF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF60CFF1FFCBEFFBFF5CCEF1FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          PaintStyle = bpsGlyph
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btSeleccionarConfiden_DevExClick
        end
      end
    end
    object tbCompetencias_DevEx: TcxTabSheet
      Caption = 'Grupo Competencias'
      ImageIndex = 7
      object gridRenglones: TZetaDBGrid
        Left = 0
        Top = 32
        Width = 583
        Height = 283
        Align = alClient
        DataSource = dsGpoCompetencias
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnColExit = gridRenglonesColExit
        OnDrawColumnCell = gridRenglonesDrawColumnCell
        Columns = <
          item
            Color = clHighlightText
            Expanded = False
            FieldName = 'CP_CODIGO'
            Title.Caption = 'C'#243'digo'
            Width = 108
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'CP_ELEMENT'
            ReadOnly = True
            Title.Caption = 'Grupo de Competencias'
            Width = 304
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PP_PESO'
            ReadOnly = True
            Title.Caption = 'Peso'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'PP_ACTIVO'
            Title.Caption = 'Activo'
            Visible = True
          end>
        object btnBuscarPerfil_DevEx: TcxButton
          Left = 104
          Top = 18
          Width = 17
          Height = 17
          OptionsImage.Glyph.Data = {
            BA030000424DBA0300000000000036000000280000000F0000000F0000000100
            20000000000084030000000000000000000000000000000000008B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFC9C4C9FFA098A1FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFFFF
            FFFFFAFAFAFF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF9B939CFFFFFFFFFFFFFFFFFFA69E
            A7FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8C82
            8DFF928993FF908691FF908791FFF7F6F7FFFFFFFFFFBFB9BFFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFABA4ACFFF7F6F7FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFDAD7DAFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFFA49DA5FFFFFFFFFFEDEBEDFFA8A1A9FFB8B2
            B8FFF0EFF0FFFFFFFFFF999099FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFFF0EFF0FFD7D3D7FF8B818CFF8B818CFF8B818CFF938A
            94FFFFFFFFFFC9C4C9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8F8590FFFFFFFFFFA39BA4FF8B818CFF8B818CFF8B818CFF8B818CFFD4D0
            D4FFF0EFF0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8F85
            90FFFFFFFFFFB3ADB4FF8B818CFF8B818CFF8B818CFF8B818CFFE3E1E3FFE6E4
            E6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFDDDA
            DDFFFAFAFAFF938A94FF8B818CFF8B818CFFB1ABB2FFFFFFFFFFAFA8B0FF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908691FFF4F3
            F4FFFFFFFFFFEDEBEDFFE9E7E9FFFFFFFFFFD7D3D7FF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8E848FFFB5AF
            B6FFD7D3D7FFD7D3D7FFA9A2AAFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
          PaintStyle = bpsGlyph
          SpeedButtonOptions.CanBeFocused = False
          TabOrder = 0
          Visible = False
          OnClick = btnBuscarPerfil_DevExClick
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 583
        Height = 32
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object BBAgregar_DevEx: TcxButton
          Left = 8
          Top = 3
          Width = 129
          Height = 25
          Hint = 'Agregar Rengl'#243'n'
          Caption = ' Agregar Rengl'#243'n'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            200000000000440800000000000000000000000000000000000050D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF5ED6
            9BFFE1F7ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFE9F9F1FF6BD9A4FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF9AE5C1FFE6F9
            F0FF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDD
            AEFF7CDDAEFFE1F7ECFFA5E8C8FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FFA8E9C9FFD3F4E4FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FFD3F4E4FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FFA8E9C9FFD3F4E4FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFD3F4
            E4FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFA8E9C9FFD3F4E4FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFD3F4E4FFA8E9
            C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FFA8E9C9FFD3F4E4FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FFD3F4E4FFA8E9C9FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFA8E9C9FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FFD3F4E4FFA8E9C9FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FFA8E9C9FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFD3F4E4FFA8E9C9FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFA8E9
            C9FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FFD3F4E4FFA8E9C9FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF97E4BFFFEFFB
            F5FFBDEED6FFBDEED6FFB5ECD1FF58D498FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFD3F4E4FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFB2EBD0FFFFFF
            FFFFFFFFFFFFFFFFFFFFAFEBCEFF50D293FF50D293FF50D293FF50D293FF50D2
            93FFD3F4E4FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFC0EFD8FFFFFF
            FFFFFFFFFFFFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D293FFD3F4
            E4FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF53D395FFC5F0DBFFFFFF
            FFFFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D293FFD3F4E4FFA8E9
            C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF53D395FFC5F0DBFFDEF7
            EBFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFFE6F9F0FFA2E7C6FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF53D395FFC8F1DDFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD3F4E4FF63D79FFF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BBAgregar_DevExClick
        end
        object BBBorrar_DevEx: TcxButton
          Left = 140
          Top = 3
          Width = 129
          Height = 25
          Hint = 'Borrar Rengl'#243'n'
          Caption = '  Borrar Rengl'#243'n'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            20000000000044080000000000000000000000000000000000004858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5F6D
            D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF5F6DD2FF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5F6DD2FFFFFF
            FFFFFFFFFFFFB7BEEBFF8D97DFFFF1F2FBFFFFFFFFFFF1F2FBFF8D97DFFFB7BE
            EBFFFFFFFFFFFFFFFFFF5F6DD2FF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5F6DD2FFFFFFFFFFFFFF
            FFFF8D97DFFF4858CCFFE8EAF9FFFFFFFFFFE8EAF9FF4858CCFF8D97DFFFFFFF
            FFFFFFFFFFFF5F6DD2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF5F6DD2FFFFFFFFFFFFFFFFFF8D97
            DFFF4858CCFFE8EAF9FFFFFFFFFFE8EAF9FF4858CCFF8D97DFFFFFFFFFFFFFFF
            FFFF5F6DD2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF5F6DD2FFFFFFFFFFFFFFFFFF8D97DFFF4858
            CCFFE8EAF9FFFFFFFFFFE8EAF9FF4858CCFF8D97DFFFFFFFFFFFFFFFFFFF5F6D
            D2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF5F6DD2FFFFFFFFFFFFFFFFFF8D97DFFF4858CCFFE8EA
            F9FFFFFFFFFFE8EAF9FF4858CCFF8D97DFFFFFFFFFFFFFFFFFFF5F6DD2FF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF5F6DD2FFFFFFFFFFFFFFFFFF8D97DFFF4858CCFFE8EAF9FFFFFF
            FFFFE8EAF9FF4858CCFF8D97DFFFFFFFFFFFFFFFFFFF5F6DD2FF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF5F6DD2FFFFFFFFFFFFFFFFFF8D97DFFF4858CCFFE8EAF9FFFFFFFFFFE8EA
            F9FF4858CCFF8D97DFFFFFFFFFFFFFFFFFFF5F6DD2FF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5F6D
            D2FFFFFFFFFFFFFFFFFFC6CBEFFFA4ACE6FFF4F5FCFFFFFFFFFFF4F5FCFFA4AC
            E6FFC6CBEFFFFFFFFFFFFFFFFFFF5F6DD2FF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5968D1FFD1D5
            F2FFD1D5F2FFD1D5F2FFD1D5F2FFD1D5F2FFD1D5F2FFD1D5F2FFD1D5F2FFD1D5
            F2FFD1D5F2FFD1D5F2FF5968D1FF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFFD1D5F2FFD1D5F2FFD1D5F2FFD1D5
            F2FFD1D5F2FFD1D5F2FFD1D5F2FFD1D5F2FFD1D5F2FFD1D5F2FFD1D5F2FFD1D5
            F2FFD1D5F2FF5968D1FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFF5F6DD2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF5F6DD2FF5F6DD2FF5F6DD2FF8792DEFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF9BA4E3FF5F6DD2FF5F6DD2FF5F6DD2FF4B5B
            CDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF7682D9FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF8D97DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BBBorrar_DevExClick
        end
        object BBModificar_DevEx: TcxButton
          Left = 272
          Top = 3
          Width = 129
          Height = 25
          Hint = 'Modificar Rengl'#243'n'
          Caption = ' Modificar Rengl'#243'n'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            200000000000440800000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF97E0F6FF97E0F6FF2CBFEDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2
            F7FFFFFFFFFFFFFFFFFFB7E9F9FF44C7EFFF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFF
            FFFFFBFEFFFF9FE2F7FF18B9EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFAFE7F8FF24BD
            ECFF00B2E9FF0CB6EAFF68D1F2FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF10B7EAFF00B2E9FF14B8EBFF4CC9
            F0FFFFFFFFFFFFFFFFFF64D0F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFDFF5FCFFFFFFFFFFFFFF
            FFFFFFFFFFFFEBF9FDFF10B7EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FFA3E3F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF8BDCF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF1CBAEBFFF7FDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFBFEFFFF28BEECFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF80D9F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFB7E9F9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF08B4EAFFE3F7FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7E9
            F9FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF5CCEF1FFFFFFFFFFFFFFFFFFD7F3FCFF48C8EFFF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFB7E9F9FF6CD3F2FF04B3E9FF00B2E9FF1CBAEBFFA3E3F7FF8BDC
            F5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF08B4EAFF80D9F4FFF7FDFEFFFFFFFFFFA3E3F7FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF40C5EFFFE3F7FDFFFFFFFFFFF7FDFEFF7CD7F4FF08B4EAFF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF18B9
            EBFFBFECF9FFA7E4F7FF1CBAEBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BBModificar_DevExClick
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 416
    Top = 44
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 16253360
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF98A1E2FFF6F7FDFFB7BEEBFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFB1B8
          E9FFF6F7FDFFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF8792DEFFDFE2F6FFA1A9E5FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8
          A1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF8CE2B8FFE9F9
          F1FF53D396FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF51D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF6FDAA7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF51D395FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF59D59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF61D79FFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4ED293FF53D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF8AE1B7FFE6F9F0FF69D9A4FF4ED293FF4ED293FF4ED2
          93FF5ED69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF51D395FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF64D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF67D8A2FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF69D9A4FFEFFBF5FFFFFFFFFFDEF7EBFF51D395FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF6FDAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF56D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF81DFB1FF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF5160CFFF6B78D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6E7BD7FF7482D9FF6370D4FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFF969FE2FF6370D4FF969FE2FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4656CCFF4656CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4656CCFF4656CCFF6E7BD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF6875D6FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF5463D0FF7482D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 144
    Top = 248
    DockControlHeights = (
      0
      0
      26
      0)
    inherited BarSuperior: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarControlContainerItem_DBNavigator'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_AgregarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_BorrarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ModificarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_BuscarBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_ImprimirBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ImprimirFormaBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ExportarBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_CortarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_CopiarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_PegarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_UndoBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_bImpSubCuentas'
        end>
    end
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
    object dxBarButton_bImpSubCuentas: TdxBarButton
      Category = 0
      Hint = 'Importaci'#243'n de Subcuentas Contables'
      Visible = ivAlways
      ImageIndex = 12
      OnClick = dxBarButton_bImpSubCuentasClick
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 16253000
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000190F061C6F431A7C9C5F24AFCC7B2FE3D78033EFB36C
          29C78F55229F39230E4000000000000000000000000000000000000000001D11
          0720000000004F2F1358D78033EFE58A36FFE58A36FFE58A36FFE58A36FFE58A
          36FFE58A36FFE58A36FF9C5F24AF20130824000000000000000000000000AC67
          28BFB66E2BCBE58A36FFE58A36FFC7792EDF6B4119784429104C3D250E44613A
          176CAC6728BFE58A36FFE58A36FFC5772EDB150D05180000000000000000C577
          2EDBE58A36FFE58A36FFA86428BB0B06030C0000000000000000000000000000
          00000000000040270F48D98433F3E58A36FFB36C29C70402010400000000D780
          33EFE58A36FFE58A36FFE08834FB3D250E440000000000000000000000000000
          0000000000000000000039230E40E58A36FFE58A36FF4429104C00000000E58A
          36FFD78033EF814E1E8F190F061C000000000000000000000000000000000000
          0000000000000000000000000000AC6728BFE58A36FF965A23A7150D0518683F
          18740E0903100000000000000000000000000000000000000000000000000000
          00000000000000000000000000004429104CAC6728BFA46227B7000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000321E0C383923
          0E40190F061C0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000AE6A29C3E58A
          36FF965A23A70000000000000000000000000000000000000000000000000000
          000000000000000000000000000007040208563414602416082859361564E58A
          36FFDE8633F7120B041400000000000000000000000000000000000000000000
          0000000000002718092C83501F93D78033EFE58A36FF1D1107200E090310DE86
          33F7E58A36FFA46227B704020104000000000000000000000000000000000000
          00002718092CCC7B2FE3E58A36FFE58A36FFE58A36FF0E090310000000005936
          1564E58A36FFE58A36FF9C5F24AF1D1107200000000000000000000000000000
          00001D110720AC6728BFE58A36FFE58A36FFE08834FB00000000000000000000
          00007D4B1E8BE58A36FFE58A36FFDE8633F79C5F24AF7A491D877D4B1E8BA161
          26B3DE8633F7E58A36FFE58A36FFAC6728BFC0742ED700000000000000000000
          0000000000005232135CCF7C31E7E58A36FFE58A36FFE58A36FFE58A36FFE58A
          36FFE58A36FFD27E32EB4B2D125400000000150D051800000000000000000000
          000000000000000000000B06030C5D38166888522097B66E2BCBAE6A29C3814E
          1E8F5232135C120B041400000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000103
          0204379366B350D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF3EA473C7050D09100000000000000000183E
          2C4C50D293FF1F5239641E4F37601E4F37601E4F37601E4F37601E4F37601E4F
          37601E4F37601E4F37601F5239644BC78BF322593E6C00000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B881DF28694A8000000000000000001E4F
          376050D293FF000000000000000000000000000000000000000000000000235C
          40704BC78BF350D293FF50D293FF4BC78BF3122E203800000000000000001E4F
          376050D293FF0000000000000000000000000000000000000000000000004DCA
          8DF750D293FF50D293FF4FCF91FB1A4530540000000000000000000000001E4F
          376050D293FF00000000000000000000000000000000000000000000000050D2
          93FF50D293FF4FCF91FB1A453054000000000000000000000000000000001E4F
          376050D293FF00000000000000000000000000000000000000000000000050D2
          93FF4FCF91FB1A45305400000000000000000000000000000000000000001538
          274450D293FF22593E6C1E4F37601E4F37601E4F37601E4F37601F52396450D2
          93FF1D4B355C0000000000000000000000000000000000000000000000000000
          00002E7855934DCA8DF750D293FF50D293FF50D293FF50D293FF50D293FF235C
          4070000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00004858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF3F4DB2DF242C6680313C8BAF4858CCFF4858CCFF3642
          99BF242C66803A47A6CF4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF364299BF000000001B214D604858CCFF4858CCFF242C
          6680000000002D37809F4858CCFF4858CCFF0000000000000000000000000000
          00004858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF0000000000000000000000000000
          00001B214D601B214D601B214D601B214D601B214D601B214D601B214D601B21
          4D601B214D601B214D601B214D601B214D600000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF00000000000000004858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF00000000000000000000
          00000000000000000000000000004858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000004858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF0000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000003B4E500082ABAF002C3A3C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000004D656800BDF8FF0099C9CF00181F200000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000004D6568004A616400030404000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000060808002C3A3C0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000006A8C8F00AAE1E7009CCCD30003
          0404000000000000000000000000000000000000000000000000000000000000
          000000000000000000000050696C00A4D9DF00B9F3FB00BDF8FF00BDF8FF004D
          6568000000000000000000000000000000000000000000000000000000000000
          0000000000000061808300B6EFF700BDF8FF00BDF8FF00BDF8FF00BDF8FF00AD
          E5EB000C10100000000000000000000000000000000000000000000000000000
          0000000000000012171800B3EBF300BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF0066888B0000000000000000000000000000000000000000000000000000
          00000000000000000000005C787C00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00B9F3FB001B232400000000000000000000000000000000000000000000
          000000000000000000000006080800A4D9DF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00BDF8FF0082ABAF00000000000000000000000000000000000000000000
          00000000000000000000000000000041555800BDF8FF00BDF8FF00BDF8FF00BD
          F8FF008EBABF00232F3000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000091BEC300BDF8FF00A1D5DB0038
          4A4C000000000000000000121718000C10100000000000000000000000000000
          000000000000000000000000000000000000002632340050696C000304040000
          0000000608080059747800B3EBF3006180830000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000038
          4A4C00A1D5DB00BDF8FF00BDF8FF005670740000000000000000000000000000
          0000000000000000000000000000000000000000000000000000007FA7AB00BD
          F8FF00BDF8FF0099C9CF00324244000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000003E5154009C
          CCD3004A61640003040400000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000B623878129C
          58BF129C58BF129C58BF129C58BF129C58BF000000000210091414AB62D315B6
          67DF14AB62D3021009140000000000000000000000000000000003170D1C16C3
          70EF18D077FF18D077FF18D077FF18D077FF000000000F8B50AB18D077FF18D0
          77FF0A58326C000000000000000000000000000000000000000000000000094E
          2D6018D077FF18D077FF13AF63D706341E400844275418D077FF15B667DF108F
          52AF00000000000000000E7845930F824A9F0107040800000000000000000000
          00000F8B50AB18D077FF18D077FF0B5E367416BC6CE7052A1834094E2D60094E
          2D60094E2D60094E2D60129E5AC318D077FF12A65FCB02140B18000000000000
          00000210091415C06EEB18D077FF18D077FF129E5AC30C653A7C18D077FF18D0
          77FF18D077FF18D077FF18D077FF18D077FF18D077FF14B86AE3000000000000
          0000000000000844275418D077FF18D077FF15B667DF073E234C18CD74FB18D0
          77FF18D077FF18D077FF18D077FF18D077FF18D077FF0C683C80000000000000
          000000000000020D071017C570F318D077FF18D077FF09512E64000000000000
          000000000000000000000F824A9F16C972F7094E2D6000000000000000000000
          0000000000000F824A9F18D077FF18D077FF18D077FF16C370EF03170D1C0000
          0000000000000000000006372044052E1A380000000000000000000000000000
          0000073E234C18D077FF18D077FF16BC6CE718D077FF18D077FF119154B30000
          000000000000000000000000000000000000000000000000000000000000020D
          071014B86AE318D077FF18CD74FB052716300F8B50AB18D077FF18D077FF094E
          2D60000000000000000000000000000000000000000000000000000000000F82
          4A9F18D077FF18D077FF0C683C80000000000210091414B86AE318D077FF16C3
          70EF03170D1C000000000000000000000000000000000000000004211328129C
          58BF129C58BF0F8B50AB000302040000000000000000073B2148129C58BF129C
          58BF0B6238780000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002B212D932B212D930101010400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001C161D604B3B4DFF4B3B4DFF392C3BC302020208000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000E0B0E30473748F34B3B4DFF4B3B4DFF2E242F9B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000605
          0614423442DF4B3B4DFF4B3B4DFF3C2F3DCB0202020800000000000000000000
          000000000000000000000000000000000000000000000000000001010104362B
          38BB4B3B4DFF4B3B4DFF443547EB0806081C0000000000000000000000000000
          000000000000100D1138322834AB4A394CFB4B3B4DFF443546E73B2D3BC74B3B
          4DFF4B3B4DFF4A394CFB14101444000000000000000000000000000000000000
          0000191319544A394CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF241D257C00000000000000000000000000000000000000000B08
          0B244A394CFB4B3B4DFF3E3241D71B151C5C130F1340211A2270443546E74B3B
          4DFF4A394CFB0706071800000000000000000000000000000000000000002F25
          2F9F4B3B4DFF3D3140D30403040C0000000000000000000000000806081C4435
          46E74B3B4DFF29202A8B00000000000000000000000000000000000000004234
          42DF4B3B4DFF1A141A580000000000000000000000000000000000000000241D
          257C4B3B4DFF3C2F3DCB00000000000000000000000000000000000000004B3B
          4DFF4B3B4DFF120E123C00000000000000000000000000000000000000001C16
          1D604B3B4DFF423442DF00000000000000000000000000000000000000004033
          41DB4B3B4DFF1D171E6400000000000000000000000000000000000000002921
          2B8F4B3B4DFF392C3BC300000000000000000000000000000000000000002921
          2B8F4B3B4DFF443546E7070607180000000000000000000000000D0A0D2C4938
          4BF74B3B4DFF231C247800000000000000000000000000000000000000000806
          081C443546E74B3B4DFF443546E7231C24781C161D60271E2883473748F34B3B
          4DFF433545E30202020800000000000000000000000000000000000000000000
          0000100D1138453647EF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF3D31
          40D30806081C0000000000000000000000000000000000000000000000000000
          00000000000005040510261E2780372B3ABF423442DF362B38BB2019206C0403
          040C000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000221B14287D634A9419130E1D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000067513D7AD8AA7FFF896C51A20000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000002A211831D3A67CF9D8AA7FFF3B2F234600000000000000003026
          1C39352A1F3F31271D3A31271D3A3A2E22454537295201010001000000000000
          0000000000000000000058453468D8AA7FFFA27F5FBF0000000000000000C99E
          76EDD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF6B543F7E01010001000000000000
          000000000000000000000807050AD8AA7FFFD0A47BF60000000000000000B48E
          6AD5D8AA7FFFD8AA7FFFD8AA7FFF6A533E7D0000000000000000000000000000
          0000000000000000000000000000D8AA7FFFD5A77DFB00000000000000009B7A
          5BB7D8AA7FFFD8AA7FFFCFA37AF4000000000000000000000000000000000000
          00000000000000000000231B1429D8AA7FFFC09771E300000000000000008A6D
          51A3D8AA7FFFD8AA7FFFD8AA7FFF785F478E0000000000000000000000000000
          000000000000000000009A795BB6D8AA7FFF725A43870000000000000000A380
          60C0BD956FDF2B221933D8AA7FFFD8AA7FFF725A438704030205000000000000
          0000100D091384684E9CD8AA7FFFCEA279F30E0B081100000000000000005946
          3469261E162D000000004E3D2E5CD8AA7FFFD8AA7FFFC79D75EB8E7054A89373
          56ADD2A57CF8D8AA7FFFD3A67CF931271D3A0000000000000000000000000000
          000000000000000000000000000035291F3EA38060C0D8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFF957558B0241C152A000000000000000000000000000000000000
          00000000000000000000000000000000000016110D1A524130618F7154A9886B
          50A14B3B2C590D0A070F00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000000000004D3F0F50DFB72BE7D6B12BDFD6B12BDFD6B1
          2BDFD6B12BDFD6B12BDFD6B12BDFDBB42CE3231C072400000000000000000000
          0000000000000000000000000000967B1E9B5C4C126000000000000000000000
          0000000000000000000000000000997E1F9F5C4C126000000000000000000000
          00004D3F0F50997E1F9F997E1F9FD2AD29DB5C4C126000000000000000000000
          0000000000000000000000000000997E1F9F5C4C126000000000000000001F19
          0620E9C02FF3997E1F9F997E1F9FD2AD29DB5C4C126000000000000000000000
          0000000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40D6B12BDF0000000000000000997E1F9F5C4C126000000000000000000000
          0000000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40D6B12BDF0000000000000000997E1F9F5C4C126000000000000000000000
          0000000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40D6B12BDF0000000000000000997E1F9F6F5C16741F1906200F0D03100000
          0000000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40D6B12BDF00000000000000003A2F0B3CE9C02FF3F6CA31FFF6CA31FF6F5C
          1674000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40D6B12BDF0000000000000000000000003A2F0B3CE9C02FF3F6CA31FF997E
          1F9F000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40D6B12BDF000000000000000000000000000000004D3F0F50F1C72FFB997E
          1F9F000000000000000000000000997E1F9F5C4C126000000000000000003E33
          0C40DFB72BE73E330C403E330C402A23082C000000000000000051421054DBB4
          2CE3B89725BFB89725BFB89725BFDBB42CE32620082800000000000000000C09
          020CBB9A25C3F6CA31FFF6CA31FFF6CA31FF7B65198000000000000000001713
          05181F190620A58721ABA58721AB0C09020C0000000000000000000000000000
          000013100414CFA928D7F6CA31FFF6CA31FFD2AD29DB00000000000000000000
          000000000000997E1F9F997E1F9F000000000000000000000000000000000000
          00000000000017130518CFA928D7F6CA31FFD6B12BDF00000000000000000000
          000000000000997E1F9F997E1F9F000000000000000000000000000000000000
          0000000000000000000017130518D6B12BDFDBB42CE31F1906201F1906201F19
          06201F190620B19122B78A721B8F000000000000000000000000000000000000
          000000000000000000000000000026200828DFB72BE7F6CA31FFF6CA31FFF6CA
          31FFF6CA31FFC4A126CB26200828000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000005040510271E2883261E278007060718000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000302732A73E3241D7392C3BC3473748F30F0C10340000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000362B38BB2C232E9700000000302732A7362B38BB0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001C161D60453647EF0403040C141014444B3B4DFF0101
          0104000000000000000000000000000000000000000000000000000000000000
          0000000000000000000002020208443547EB231C2478171218504B3B4DFF0504
          0510000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000281F2987443547EB49384BF7302631A30C09
          0C28130F1340130F1340130F1340130F13400504051000000000000000000000
          0000000000000000000002020208130F13404B3B4DFF362A37B7332835AF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF443547EB1D171E640C090C28362A
          37B7423442DF453647EF4B3B4DFF4B3B4DFF4B3B4DFF4A394CFB3C2F3DCB3328
          35AF29212B8F221B23741A141A58120E123C09070A20000000003D303FCF3529
          35B31C161D601C161D60130F13404B3B4DFF322834AB4A394CFB070607180000
          00000000000000000000000000000000000000000000000000003D303FCF2C23
          2E9700000000010101041E181F684B3B4DFF211A22704B3B4DFF231C24780000
          0000000000000000000000000000000000000000000000000000130F13404435
          46E7473748F3443547EB49384BF7211A22700B080B244A394CFB423442DF0101
          0104000000000000000000000000000000000000000000000000000000000605
          0614171218501612174C0605061400000000000000002F252F9F4B3B4DFF1612
          174C000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000B080B244A394CFB3529
          35B3000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000029212B8F4B3B
          4DFF09070A200000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000060506143027
          32A7281F29870000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000B080B240000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000465B640084ADBD008AB4C5008EBACC0075A3A6272D2B43B2794EB77A61
          4991745B4489725A4387B48D6AD47A6048900000000000000000000000000000
          00000085AEBE00B2E9FF0083ABBB008DB9CA006A969669695B9C835A3888120E
          0B15503F2F5F2A211932654F3B7781654C980000000000000000000000000000
          00000085AEBE0089B3C40000000000000000000000005F584B83805B3D893E31
          254A8D6F53A65B47356B5B48366C6D5640810000000000000000000000000000
          00000086AFC00090BDCF0000000000000000000000007D573A83765E478D120E
          0A153A2E224506050307594634697F644B960000000000000000000000000000
          00000086AFC00090BDCF000000000000000000000000715942857A6149910000
          00000C09070E69533E7DC6A17BF1846C53A20000000000000000000000000000
          00000086AFC00090BDCF0000000000000000000000007C634A9485694E9D0000
          0000493B2E59DDAA7DFFB07340B0130C07130000000000000000000000000000
          00000086AFC00090BDCF000000000000000000000000513A2958B28D69D35F4D
          3B73B78458C79769409F00000000000000000000000000000000000000000000
          00000086AFC00090BDCF000000000000000000000000000000003D281A3D6845
          2A68302013302554617A00577B7B000000000000000000000000000000000000
          00000086AFC00091BED000000000000000000000000000000000000000000000
          0000000000000092CECF00617D8B000000000000000000000000000000000000
          00000085AFBF0086AFC00000000000516A7400607D89005A7880006087890051
          7373000000000085ACBF00607D89000000000000000000000000000000000000
          00000083ACBC008EBACC001C252800A9DDF200B2E9FF00B2E9FF00B2E9FF00A9
          DDF2001D2529008DB9CA005B7883000000000000000000000000000000000000
          0000007DA4B300B2E9FF00B2E9FF00B2E9FF00B0E6FC00A5D8EC00B0E6FC00B2
          E9FF00B2E9FF00B2E9FF00506872000000000000000000000000000000000000
          0000002733380041555D0042565E0086AFC0009ACADD00263237009ECFE30085
          AEBE0042565E0040545C00181F22000000000000000000000000000000000000
          000000000000000000000000000000000000005069730095C4D6004D656F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001F1908258B721D8F8B721D8F8B721D8F8B721D8F8B73
          1D908B721D8F382E0D3E00000000000000000000000000000000000000000000
          000000000000000000006250166CF7CB33FFF7CB33FFF7CB33FFF7CB33FFF7CB
          33FFF7CB33FF8B721D9100000000000000000000000000000000000000000000
          00000000000000000000604F1465F8CC34FFF8CC34FFF7CB33FFF7CB33FFF7CB
          33FFF8CC34FF8B721D9000000000000000000000000000000000000000000000
          00000000000000000000614F1465F7CB33FFF7CB33FFF8CC34FFF9CD35FFF7CB
          33FFF7CB33FF8B721D8F000000000000000000000000000000000403040C1A14
          1A581C161D60120E123C604F1463F7CB33FFF6CA32FFF8CC34FFF6CA32FFF7CB
          33FFF8CC34FF8B721D8F070607181C161D601B151C5C06050614362A37B74B3B
          4DFF4B3B4DFF2F252F9F604E1363F7CB33FFF7CB33FFF7CB33FFF8CC34FFF7CB
          33FFF7CB33FF8B721D8F1410134149384BF7443546E73E3241D74B3B4DFF4B3B
          4DFF4B3B4DFF2F252F9F6250166AF8CC34FFF8CC34FFF8CC34FFF7CB33FFF8CC
          34FFF7CB33FF8B731D9014101341271E2883010101044B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF302732A717120E2F6C58208C6C58208C6E5A22916F5B23966F5A
          23966F5B23982F26195E15111648403341DB322834AB4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF473748F3221B22711C161D601C161D601C161D601C161D601C16
          1D601C161D601D171E643D303FCF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF3D303FCF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF443546E70E0B0E303027
          32A7372B3ABF362B38BB1712185009070A2009070A2009070A2009070A200907
          0A2009070A20261E2780372B3ABF372B3ABF322834AB130F1340000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000001010001020200020403010405040105110E03120C0A
          020D141004150000000000000000000000000000000000000000000000000000
          00000000000000000000EEC32DFBEEC32DFBEEC32DFBEEC32DFBEBC12DF8EDC2
          2DFAEBC12DF8E6BC2CF200000000000000000000000000000000000000000000
          00000000000000000000EFC42DFCF3C72FFFB89728C2B59525BC1B16061DF7CB
          33FFF7CB33FFE3BA2BEF00000000000000000000000000000000000000000000
          00000000000000000000F1C52EFEF3C72FFF000000006553156804030104EEC4
          30F7F7CB33FFEAC02DF700000000000000000000000000000000000000000000
          00000000000000000000F1C52EFEF3C72FFF00000000604F1463040301041814
          0519AD8E24B2EAC02DF700000000000000000000000000000000171218503D30
          3FCF423442DF29202A8BF1C52EFEF3C72FFF0000000091771E964438114D100D
          0310947A1F99EAC02DF7100D081D423442DF3E3241D71D171E64423442DF4B3B
          4DFF4B3B4DFF2F252F9FF2C62EFFF3C72FFF00000000BD9B27C3F8CC34FFB595
          25BCF8CC34FFEAC02DF714100921392C3BC3271E2883473748F34B3B4DFF4B3B
          4DFF4B3B4DFF2F252F9FECC12DF9F5C931FF65531568EEC430F7F8CC34FFF8CC
          34FFF7CB33FFECC12DF9120F091D2B212D930403040C4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF392C3BC31713061C261F0C2C251E0B2D251E0B2D251E0B2C251F
          0B2D2B231242080605102B221D4F4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF3E3241D7372B3ABF372B3ABF372B3ABF372B3ABF372B
          3ABF372B3ABF3B2D3BC74A394CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4C3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4A394CFB4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2E242F9B4B3B
          4DFF4B3B4DFF4B3B4DFF403341DB372B3ABF372B3ABF372B3ABF372B3ABF372B
          3ABF372B3ABF453647EF4B3B4DFF4B3B4DFF4B3B4DFF362B38BB000000000E0B
          0E30130F1340130F134001010104000000000000000000000000000000000000
          00000000000007060718130F1340130F1340100D113800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000043
          5961007092A0006E8E9C006E8E9C006E8E9C006E8E9C006E8E9C006E8E9C006E
          8E9C006E8E9C006E8E9C006E8E9C007092A000526B7500000000000000000097
          C5D900B2EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF000102020005060700A7
          DAEF00B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00171F22001F292D00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0034454B003D505800B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0059
          747F00607D8900B2E9FF00B2E9FF00B2E9FF00B3E9FF004E6771005A758000B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0067869300202A2E358B
          61A9368D63AB0041555D00B2E9FF00B2E9FF00B3E9FF00698997007EA5B500B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF007EA5B51333243E3FB486DE50D2
          93FF4ED093FC20553B6700334349008EB9CB00B2E9FF008CB7C80097C5D800B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF004960692972548C4ECF94FD49C9
          92F64FCB8FF850D293FF235D417100546E7800B2E9FF00A1D3E7001D262A0018
          1F220012171900121719001217190013181B00121719010A0B0E00000000307D
          589840A674CA4FD092FD3A9A6BBA000B0E1000181F22001F282C000000000000
          00000000000000000000000000000000000000000000000000000000000041AD
          7AD250D293FF4ECC8FF80F251A2D000000000000000000000000000000000068
          879400A9DDF200A9DDF200A9DDF200A9DDF20057717C0A18111D45B67FDD50D2
          93FF4AC58AF00816111B0080A8B800B2E9FF00799EAE00000000000000000075
          97A600B2E9FF00B2E9FF00B2E9FF00B2E9FF005A7681338D65AB50D192FF43B2
          7CD82F7C579700080B0C00B2E9FF00B2E9FF00799DAD0000000000000000007E
          A2B300B2EAFF00B2E9FF00B2E9FF00B2E9FF0089B5C62059416D50D192FF46BA
          82E344B47DDA317D5899071B182200090C0D0006070800000000000000000043
          5860007092A0006E8E9C006E8E9C007092A000536D78000000003C9E6EC050D2
          94FF50D293FF4CC78CF2040B070D000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000010101378F
          64AE3DA171C40000000000000000000000000000000000000000}
      end>
  end
  object dsGpoCompetencias: TDataSource
    Left = 464
    Top = 44
  end
end
