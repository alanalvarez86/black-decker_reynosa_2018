unit FEditCatConceptos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, Buttons, DBCtrls, StdCtrls, ExtCtrls, Mask, dbClient,
  ZetaKeyCombo, ZetaKeyLookup, ComCtrls, ZetaNumero, ZetaCommonLists,
  ZetaCommonClasses, ZetaTipoEntidad, ZetaSmartLists, ZetaClientDataSet,
  ZetaDBTextBox, ZetaEdit;

type
  TEditCatConceptos = class(TBaseEdicion)
    Panel1: TPanel;
    CO_NUMEROLbl: TLabel;
    LblDescrip: TLabel;
    PageControl: TPageControl;
    TabGenerales: TTabSheet;
    TabCalculo: TTabSheet;
    TabExento: TTabSheet;
    LblCondicion: TLabel;
    CO_ACTIVO: TDBCheckBox;
    CO_CALCULA: TDBCheckBox;
    LblTipo: TLabel;
    CO_TIPO: TZetaDBKeyCombo;
    TabIndividual: TTabSheet;
    LblFormulaCalc: TLabel;
    CO_FORMULA: TDBMemo;
    LblFormulaIndiv: TLabel;
    CO_IMP_CAL: TDBMemo;
    LblFormulaExento: TLabel;
    CO_X_ISPT: TDBMemo;
    CO_QUERY: TZetaDBKeyLookup;
    GBPercepcion: TGroupBox;
    CO_MENSUAL: TDBCheckBox;
    CO_A_PTU: TDBCheckBox;
    CO_G_ISPT: TDBCheckBox;
    CO_G_IMSS: TDBCheckBox;
    GBImprimir: TGroupBox;
    TabRecibo: TTabSheet;
    LblFormulaRecibo: TLabel;
    CO_RECIBO: TDBMemo;
    CO_IMPRIME: TDBCheckBox;
    CO_LISTADO: TDBCheckBox;
    CO_NUMERO: TZetaDBNumero;
    CO_DESCRIP: TDBEdit;
    SBco_x_ispt: TSpeedButton;
    SBco_imp_cal: TSpeedButton;
    SBco_recibo: TSpeedButton;
    SBCO_FORMULA: TSpeedButton;
    Label12: TLabel;
    CO_SUB_CTA: TDBEdit;
    bImpSubCuentas: TSpeedButton;
    CO_CAMBIA: TDBCheckBox;
    TabNotas: TTabSheet;
    CO_NOTA: TDBMemo;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    btnAgregarDocumento: TSpeedButton;
    btnBorraDocumento: TSpeedButton;
    btnEditarDocumento: TSpeedButton;
    btnVerDocumento: TSpeedButton;
    lblDescripcionArchivo: TLabel;
    lblTipoArchivo: TLabel;
    TabSheet1: TTabSheet;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GpoAcceso: TGroupBox;
    TodosUsuarios: TRadioButton;
    AlgunosUsuarios: TRadioButton;
    lblMonto1: TLabel;
    CO_LIM_INF: TZetaDBNumero;
    CO_LIM_SUP: TZetaDBNumero;
    lblMonto: TLabel;
    CO_VER_INF: TDBCheckBox;
    CO_VER_SUP: TDBCheckBox;
    btnElegirGrupos: TBitBtn;
    CO_GPO_ACC: TDBMemo;
    CO_SUMRECI: TDBCheckBox;
    CO_ISN: TDBCheckBox;
    LblFormulaAlterna: TLabel;
    CO_FRM_ALT: TDBMemo;
    SBCO_FRM_ALT: TSpeedButton;
    LblUsoNomina: TLabel;
    CO_USO_NOM: TZetaDBKeyCombo;
    TabTimbrado: TTabSheet;
    GroupBox4: TGroupBox;
    Label2: TLabel;
    lblTipoPositivo: TLabel;
    CO_SAT_CLP: TZetaDBKeyCombo;
    CO_SAT_TPP: TZetaDBKeyLookup;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    lblTipoNegativo: TLabel;
    CO_SAT_CLN: TZetaDBKeyCombo;
    CO_SAT_TPN: TZetaDBKeyLookup;
    GroupBox6: TGroupBox;
    Label6: TLabel;
    lblConceptoExento: TLabel;
    CO_SAT_EXE: TZetaDBKeyCombo;
    CO_SAT_CON: TZetaDBKeyLookup;
    procedure SBco_FORMULAClick(Sender: TObject);
    procedure SBco_x_isptClick(Sender: TObject);
    procedure SBco_imp_calClick(Sender: TObject);
    procedure SBco_reciboClick(Sender: TObject);
    procedure CO_TIPOChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure bImpSubCuentasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AgregarDocumentoBtnClick(Sender: TObject);
    procedure BorrarDocumentoBtnClick(Sender: TObject);
    procedure EditarDocumentoBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnVerDocumentoClick(Sender: TObject);
    procedure AlgunosUsuariosClick(Sender: TObject);
    procedure TodosUsuariosClick(Sender: TObject);
    procedure btnElegirGruposClick(Sender: TObject);
    procedure CO_VER_INFClick(Sender: TObject);
    procedure CO_VER_SUPClick(Sender: TObject);
    procedure SBCO_FRM_ALTClick(Sender: TObject);
    procedure CO_SAT_CLPChange(Sender: TObject);
    procedure CO_SAT_CLNChange(Sender: TObject);
    procedure CO_SAT_EXEChange(Sender: TObject);
  private
    FNavegando:Boolean;
    procedure ControlesGBPercepciones( iValor : Integer );
    procedure ControlesGBImprimir( iValor : Integer );
    procedure SetControls;
    procedure ActivarControlesConcepto;
    procedure EnabledControlDocumento;
    procedure AgregaDocumento;
    procedure DialogoAgregaDocumento(const lAgregando: Boolean);
    procedure SetControlesAcceso;
    procedure SetControlGposAcceso;
    procedure ControlesVerificar;
    procedure SetControlesTimbrado(Field: TField);
    procedure SetControlesTimbradoUX(iCOT_SAT_CLP, iCOT_SAT_CLN,  iCON_SAT_EXE : integer );

  protected
    procedure HabilitaControles; override;
    procedure Connect; override;
    procedure DoLookup; override;
    procedure ImprimirForma; override;
    procedure EscribirCambios;override;
   public
  end;

var
  EditCatConceptos: TEditCatConceptos;

implementation

uses dCatalogos,
     ZImprimeForma,
     ZAccesosTress,
     ZConstruyeFormula,
     ZImportaSubCuentas,
     ZetaBuscaEntero,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaFilesTools,
     FCatConGposAcceso,
     FEditDocumento;

{$R *.DFM}

procedure TEditCatConceptos.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_NOMINA_CONCEPTOS;
     PageControl.ActivePage := TabGenerales;
     FirstControl := CO_NUMERO;
     HelpContext:= H60621_Catalogo_conceptos;
     CO_QUERY.LookupDataset := dmCatalogos.cdsCondiciones;
end;

procedure TEditCatConceptos.Connect;
begin
     with dmCatalogos do
     begin
          cdsCondiciones.Conectar;
          cdsTiposSAT.Conectar;

          CO_SAT_TPP.LookupDataset := cdsTiposSAT;
          CO_SAT_TPN.LookupDataset := cdsTiposSAT;
          CO_SAT_CON.LookupDataset := cdsConceptosLookUp;

          // se requiere conectar cdsConceptosLookup antes de de que cdsConceptos entre en edici�n
          // si la transf. de Data se hace despu�s, cdsConceptos regresa a dsBrowse y provoca error
          // El LookUp conecta tambi�n a cdsConceptos
          cdsConceptosLookUp.Conectar;
          DataSource.DataSet:= cdsConceptos;



     end;



     EnabledControlDocumento;
end;

procedure TEditCatConceptos.FormShow(Sender: TObject);
begin
     inherited;
     SetControls;
     FNavegando := False;
end;

procedure TEditCatConceptos.FormDestroy(Sender: TObject);
begin
     inherited;
     ZetaFilesTools.BorraArchivosTemporales;
end;

procedure TEditCatConceptos.SetControls;
var
   lAutoEdit: Boolean;
begin
     //Se deshabilitan los botones si el usuario no tiene derecho de modificar.
     lAutoEdit := DataSource.AutoEdit;
     bImpSubCuentas.Enabled := lAutoEdit;
     SBCO_FORMULA.Enabled := lAutoEdit;
     SBco_x_ispt.Enabled := lAutoEdit;
     SBco_imp_cal.Enabled := lAutoEdit;
     SBco_recibo.Enabled := lAutoEdit;
end;

procedure TEditCatConceptos.CO_TIPOChange(Sender: TObject);
begin
     inherited;
     ControlesGBPercepciones( CO_TIPO.Valor );
     ControlesGBImprimir( CO_TIPO.Valor );
end;

procedure TEditCatConceptos.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) OR ( Field.FieldName = 'CO_TIPO' ) then
     begin
          if ( Field = nil ) then
             ActivarControlesConcepto
          else
              ControlesGBPercepciones( DataSource.DataSet.FieldByName( 'CO_TIPO' ).AsInteger );
     end;

     if ( ( Field = nil ) or ( ( Field.FieldName = 'CO_VER_ACC' ) or
                               ( Field.FieldName = 'CO_VER_INF' ) or
                               ( Field.FieldName = 'CO_VER_SUP' ) )  )then
     begin
          if ( ( Field = nil )and ( not( DataSource.DataSet.State in [dsEdit,dsInsert]) ) )then
          begin
               FNavegando := True;
               SetControlGposAcceso;
          end
          else
              ControlesVerificar;
     end;

     EnabledControlDocumento;


     SetControlesTimbrado( Field );

end;

procedure TEditCatConceptos.SetControlGposAcceso;
begin
     TodosUsuarios.Checked := ( NOT zStrToBool( DataSource.DataSet.FieldByName('CO_VER_ACC').AsString ) );
     AlgunosUsuarios.Checked := (not TodosUsuarios.Checked);
     FNavegando := False;
end;

procedure TEditCatConceptos.ActivarControlesConcepto;
var
   lEnabled : Boolean;
begin
     lEnabled := ( DataSource.DataSet.State <> dsInactive ) and
                 ( DataSource.DataSet.FieldByName( 'CO_NUMERO' ).AsInteger < 1000 );
     LblDescrip.Enabled := lEnabled;
     CO_DESCRIP.Enabled := lEnabled;
     LblTipo.Enabled := lEnabled;
     CO_TIPO.Enabled := lEnabled;
     CO_ACTIVO.Enabled := lEnabled;
     CO_CALCULA.Enabled := lEnabled;
     LblCondicion.Enabled := lEnabled;
     CO_QUERY.Enabled := lEnabled;

     GBImprimir.Enabled := lEnabled;
     CO_IMPRIME.Enabled := lEnabled;
     CO_LISTADO.Enabled := lEnabled;

     LblFormulaCalc.Enabled := lEnabled;
     CO_FORMULA.Enabled := lEnabled;
     SBCO_FORMULA.Enabled := lEnabled;

     LblFormulaExento.Enabled := lEnabled;
     CO_X_ISPT.Enabled := lEnabled;
     SBco_x_ispt.Enabled := lEnabled;

     LblFormulaIndiv.Enabled := lEnabled;
     CO_IMP_CAL.Enabled := lEnabled;
     SBco_imp_cal.Enabled := lEnabled;

     LblFormulaRecibo.Enabled := lEnabled;
     CO_RECIBO.Enabled := lEnabled;
     SBco_recibo.Enabled := lEnabled;
     CO_CAMBIA.Enabled := lEnabled;

     CO_VER_INF.Enabled := lEnabled;
     CO_VER_SUP.Enabled := lEnabled;

     AlgunosUsuarios.Enabled := lEnabled;
     TodosUsuarios.Enabled := lEnabled;

     GpoAcceso.Enabled := lEnabled;

     if lEnabled then
     begin
          ControlesGBPercepciones( DataSource.DataSet.FieldByName( 'CO_TIPO' ).AsInteger );
          SetControls;    // Checar AutoEdit en botones
     end
     else
     begin
          GBPercepcion.Enabled := lEnabled;
          CO_MENSUAL.Enabled := lEnabled;
          CO_A_PTU.Enabled   := lEnabled;
          CO_G_IMSS.Enabled  := lEnabled;
          CO_G_ISPT.Enabled  := lEnabled;
          CO_ISN.Enabled     := lEnabled;
     end;
end;

procedure TEditCatConceptos.ControlesGBImprimir( iValor : Integer );
var
   lConceptosApoyo : Boolean;
begin
     if Inserting then
     begin
          lConceptosApoyo:= ( eTipoConcepto ( iValor ) in [ coCaptura,coResultados,coCalculo ] );
          with dmCatalogos.cdsConceptos do
          begin
               FieldByName( 'CO_IMPRIME' ).AsString := zBoolToStr( NOT lConceptosApoyo );
               FieldByName( 'CO_LISTADO' ).AsString := zBoolToStr( NOT lConceptosApoyo );
          end;
     end;
end;

procedure TEditCatConceptos.ControlesGBPercepciones( iValor : Integer );
var
   lEnabled : Boolean;
begin
     with GBPercepcion do
     begin
          lEnabled:= ( iValor = Ord( coPercepcion ) );
          Enabled := lEnabled OR ( iValor = Ord( coPrestacion ) );
          CO_MENSUAL.Enabled := lEnabled;
          CO_A_PTU.Enabled   := lEnabled;
          CO_G_IMSS.Enabled  := Enabled;
          CO_G_ISPT.Enabled  := Enabled;
          CO_ISN.Enabled := Enabled;
     end;
end;

procedure TEditCatConceptos.HabilitaControles;
begin
     CO_NUMERO.Enabled:= Inserting;
     CO_NUMEROLbl.Enabled:= Inserting;
     if Inserting then
        FirstControl:= CO_NUMERO
     else
        FirstControl:= CO_DESCRIP;

     inherited;

     bImpSubCuentas.Enabled := not Editing and DataSource.AutoEdit;
end;

procedure TEditCatConceptos.SBCO_FORMULAClick(Sender: TObject);
begin
     with dmCatalogos.cdsConceptos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CO_FORMULA').AsString := GetFormulaConst( enNomina , CO_FORMULA.Lines.Text, CO_FORMULA.SelStart, evNomina );
     end;
end;

procedure TEditCatConceptos.SBco_x_isptClick(Sender: TObject);
begin
     with dmCatalogos.cdsConceptos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CO_X_ISPT').AsString := GetFormulaConst( enNomina , CO_X_ISPT.Lines.Text , CO_X_ISPT.SelStart, evNomina );
     end;
end;

procedure TEditCatConceptos.SBco_imp_calClick(Sender: TObject);
begin
     with dmCatalogos.cdsConceptos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CO_IMP_CAL').AsString := GetFormulaConst( enNomina , CO_IMP_CAL.Lines.Text, CO_IMP_CAL.SelStart, evNomina );
     end;
end;

procedure TEditCatConceptos.SBco_reciboClick(Sender: TObject);
begin
     with dmCatalogos.cdsConceptos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CO_RECIBO').AsString := GetFormulaConst( enNomina , CO_RECIBO.Lines.Text, CO_RECIBO.SelStart, evNomina  );
     end;
end;

procedure TEditCatConceptos.DoLookup;
begin
     inherited;
     ZetaBuscaEntero.BuscarCodigo( 'N�mero', 'Concepto de N�mina', 'CO_NUMERO', dmCatalogos.cdsConceptos );
end;

procedure TEditCatConceptos.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enConcepto, dmCatalogos.cdsConceptos );
end;

procedure TEditCatConceptos.bImpSubCuentasClick(Sender: TObject);
begin
     inherited;
     ZImportaSubCuentas.ImportaArchivo( TZetaClientDataSet( DataSource.DataSet ), 'CO_NUMERO', 'CO_SUB_CTA' );
end;


procedure TEditCatConceptos.AgregarDocumentoBtnClick(Sender: TObject);
begin
     AgregaDocumento;
end;

procedure TEditCatConceptos.BorrarDocumentoBtnClick(Sender: TObject);
begin
     with dmCatalogos do
     begin
          if ( ZetaDialogo.ZConfirm(Caption, '� Desea Borrar el Documento ' + cdsConceptos.FieldByName('CO_D_NOM').AsString + ' ?', 0, mbNo ) ) then
          begin
               dmCatalogos.BorraDocumento;
               EnabledControlDocumento;
          end;
     end;
end;

procedure TEditCatConceptos.EditarDocumentoBtnClick(Sender: TObject);
begin
     inherited;
     if StrLleno( dmCatalogos.cdsConceptos.FieldByName('CO_D_NOM').AsString ) then
     begin
          DialogoAgregaDocumento( FALSE );
     end
     else ZetaDialogo.ZError( Caption, 'El Documento No Contiene Informaci�n', 0 );
end;

procedure TEditCatConceptos.btnVerDocumentoClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.AbreDocumento;

end;

procedure TEditCatConceptos.EnabledControlDocumento;
 var lEnabled : Boolean;
     sDescripcion, sTipo : string;
     oColor : TColor;
begin
     with dmCatalogos.cdsConceptos do
     begin
          lEnabled := StrLleno( FieldByName('CO_D_NOM').AsString );

          if lEnabled then
          begin
               oColor := clNavy;
               sDescripcion := 'Archivo: '+ FieldByName('CO_D_NOM').AsString;
               sTipo := ZetaFilesTools.GetTipoDocumento( FieldByName('CO_D_EXT').AsString );
          end
          else
          begin
               with lblDescripcionArchivo do
               begin
                    oColor := clGreen;
                    sDescripcion := 'No hay Ning�n Documento Almacenado';
                    sTipo := VACIO;
               end;
          end;
     end;

     btnVerDocumento.Enabled := lEnabled ;
     btnBorraDocumento.Enabled := lEnabled;
     btnEditarDocumento.Enabled := lEnabled;

     with lblDescripcionArchivo do
     begin
          Font.Color:= oColor;
          Caption := sDescripcion;
     end;

     lblTipoArchivo.Caption := sTipo;

end;

procedure TEditCatConceptos.AgregaDocumento;
 var sDocumento : string;
begin
     sDocumento := dmCatalogos.cdsConceptos.FieldByName('CO_D_NOM').AsString;
     if StrVacio( sDocumento ) or ZetaDialogo.ZConfirm( Caption, '� Desea Sustituir el Documento: ' + sDocumento + ' por uno Nuevo ?', 0, mbNo ) then
     begin
          DialogoAgregaDocumento( TRUE );
     end;

end;

procedure TEditCatConceptos.DialogoAgregaDocumento( const lAgregando : Boolean );
begin
     with dmCatalogos.cdsConceptos do
     begin
          if FEditDocumento.EditarDocumento( lAgregando, FieldByName('CO_D_NOM').AsString, H_DOCUMENTO_CONCEPTOS, dmCatalogos.CargaDocumento ) then
          begin
               EnabledControlDocumento;
               if state in [ dsinsert,dsedit ] then
                  Modo:= dsEdit;
          end;
     end;
end;

procedure TEditCatConceptos.AlgunosUsuariosClick(Sender: TObject);
begin
     TodosUsuarios.Checked := False;
     SetControlesAcceso;
end;

procedure TEditCatConceptos.TodosUsuariosClick(Sender: TObject);
begin
     inherited;
     AlgunosUsuarios.Checked := False;
     SetControlesAcceso;
end;

procedure TEditCatConceptos.SetControlesAcceso;
begin
     if ( not FNavegando )then
     begin
          if not (DataSource.DataSet.State in [dsEdit,dsInsert])then
          begin
               DataSource.DataSet.Edit;
          end;
          DataSource.DataSet.FieldByName('CO_VER_ACC').AsString := zBoolToStr(not ( TodosUsuarios.Checked ) );
     end;

     btnElegirGrupos.Enabled := AlgunosUsuarios.Checked;
end;

procedure TEditCatConceptos.EscribirCambios;
begin
     //SetGruposAcceso;
     inherited;

end;

procedure TEditCatConceptos.btnElegirGruposClick(Sender: TObject);
begin
     inherited;
     try
        CatConGposAcceso := TCatConGposAcceso.Create(Self);
        CatConGposAcceso.ShowModal;
     finally
            CatConGposAcceso.Free;
     end;
end;

procedure TEditCatConceptos.ControlesVerificar;
begin
     CO_LIM_INF.Enabled := CO_VER_INF.Checked;
     CO_LIM_SUP.Enabled := CO_VER_SUP.Checked;
     lblMonto1.Enabled  := CO_VER_INF.Checked;
     lblMonto.Enabled := CO_VER_SUP.Checked;
     CO_GPO_ACC.Enabled := AlgunosUsuarios.Checked;
     btnElegirGrupos.Enabled := AlgunosUsuarios.Checked;
end;

procedure TEditCatConceptos.CO_VER_INFClick(Sender: TObject);
begin
     inherited;
     CO_LIM_INF.Enabled := CO_VER_INF.Checked;
     lblMonto1.Enabled  := CO_VER_INF.Checked;
end;

procedure TEditCatConceptos.CO_VER_SUPClick(Sender: TObject);
begin
  inherited;
      CO_LIM_SUP.Enabled := CO_VER_SUP.Checked;                                     
      lblMonto.Enabled := CO_VER_SUP.Checked;
end;

procedure TEditCatConceptos.SBCO_FRM_ALTClick(Sender: TObject);
begin
     with dmCatalogos.cdsConceptos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CO_FRM_ALT').AsString := GetFormulaConst( enNomina , CO_FRM_ALT.Lines.Text, CO_FRM_ALT.SelStart, evNomina );
     end;
end;

procedure TEditCatConceptos.SetControlesTimbrado(Field: TField);
begin

    with  DataSource.DataSet do
    begin
         SetControlesTimbradoUX(FieldByName('CO_SAT_CLP').AsInteger, FieldByName('CO_SAT_CLN').AsInteger,  FieldByName('CO_SAT_EXE').AsInteger );
    end;

end;

procedure TEditCatConceptos.SetControlesTimbradoUX(iCOT_SAT_CLP, iCOT_SAT_CLN,  iCON_SAT_EXE : integer );
begin

    with  DataSource.DataSet do
    begin
         CO_SAT_TPP.Enabled := ( iCOT_SAT_CLP in [1,2] );
         CO_SAT_TPN.Enabled := ( iCOT_SAT_CLN in [1,2] );
         CO_SAT_CON.Enabled := ( iCON_SAT_EXE in [3,6] );

         lblTipoPositivo.Enabled := CO_SAT_TPP.Enabled ;
         lblTipoNegativo.Enabled := CO_SAT_TPN.Enabled ;
         lblConceptoExento.Enabled := CO_SAT_CON.Enabled;

         if ( CO_SAT_TPP.Enabled ) then
             CO_SAT_TPP.Filtro := Format( '(TB_SAT_CLA = %d)', [ iCOT_SAT_CLP] );


         if ( CO_SAT_TPP.Enabled ) then
             CO_SAT_TPN.Filtro := Format( '(TB_SAT_CLA = %d)', [ iCOT_SAT_CLN] );

    end;

end;

procedure TEditCatConceptos.CO_SAT_CLPChange(Sender: TObject);
begin
  inherited;
  SetControlesTimbradoUX( CO_SAT_CLP.Valor, CO_SAT_CLN.Valor,  CO_SAT_EXE.Valor );
end;

procedure TEditCatConceptos.CO_SAT_CLNChange(Sender: TObject);
begin
  inherited;
  SetControlesTimbradoUX( CO_SAT_CLP.Valor, CO_SAT_CLN.Valor,  CO_SAT_EXE.Valor );
end;

procedure TEditCatConceptos.CO_SAT_EXEChange(Sender: TObject);
begin
  inherited;
  SetControlesTimbradoUX( CO_SAT_CLP.Valor, CO_SAT_CLN.Valor,  CO_SAT_EXE.Valor ); 
end;

end.

