inherited CatFolios_devEx: TCatFolios_devEx
  Left = 207
  Top = 166
  Caption = 'Folios de Recibos'
  ClientWidth = 426
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 426
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 100
      inherited textoValorActivo2: TLabel
        Width = 94
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 426
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object FL_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'FL_CODIGO'
        MinWidth = 65
        Width = 65
      end
      object FL_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'FL_DESCRIP'
        MinWidth = 100
        Width = 100
      end
      object FL_INICIAL: TcxGridDBColumn
        Caption = 'Folio Inicial'
        DataBinding.FieldName = 'FL_INICIAL'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 100
      end
      object FL_FINAL: TcxGridDBColumn
        Caption = 'Folio Final'
        DataBinding.FieldName = 'FL_FINAL'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 100
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end