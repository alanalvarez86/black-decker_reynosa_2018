inherited CatReglas_DevEx: TCatReglas_DevEx
  Left = 738
  Top = 261
  Caption = 'Reglas'
  ClientWidth = 669
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 669
    inherited ValorActivo2: TPanel
      Width = 410
      inherited textoValorActivo2: TLabel
        Width = 404
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 669
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object CL_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CL_CODIGO'
        Options.Grouping = False
      end
      object CL_LETRERO: TcxGridDBColumn
        Caption = 'Letrero'
        DataBinding.FieldName = 'CL_LETRERO'
        Options.Grouping = False
      end
      object CL_LIMITE: TcxGridDBColumn
        Caption = 'L'#237'mite'
        DataBinding.FieldName = 'CL_LIMITE'
        Options.Grouping = False
      end
      object CL_TIPOS: TcxGridDBColumn
        Caption = 'Tipos de Comida'
        DataBinding.FieldName = 'CL_TIPOS'
        Width = 98
      end
      object CL_ACTIVO: TcxGridDBColumn
        Caption = 'Activa'
        DataBinding.FieldName = 'CL_ACTIVO'
        Options.Grouping = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
