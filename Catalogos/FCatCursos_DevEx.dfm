inherited CatCursos_DevEx: TCatCursos_DevEx
  Left = 199
  Top = 315
  Caption = 'Cursos'
  ClientHeight = 281
  ClientWidth = 690
  ExplicitWidth = 690
  ExplicitHeight = 281
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 690
    ExplicitWidth = 690
    inherited Slider: TSplitter
      Left = 323
      ExplicitLeft = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      ExplicitWidth = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 364
      ExplicitLeft = 326
      ExplicitWidth = 364
      inherited textoValorActivo2: TLabel
        Width = 358
        ExplicitLeft = 278
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 690
    Height = 262
    ExplicitWidth = 690
    ExplicitHeight = 262
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CU_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CU_CODIGO'
      end
      object CU_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'CU_DESCRIP'
      end
      object CU_REVISIO: TcxGridDBColumn
        Caption = 'Revisi'#243'n'
        DataBinding.FieldName = 'CU_REVISIO'
      end
      object CU_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'CU_ACTIVO'
      end
      object CU_STPS: TcxGridDBColumn
        Caption = 'STPS'
        DataBinding.FieldName = 'CU_STPS'
      end
      object CU_CLASIFI: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'CU_CLASIFI'
      end
      object CU_CLASE: TcxGridDBColumn
        Caption = 'Clase'
        DataBinding.FieldName = 'CU_CLASE'
      end
      object CU_HORAS: TcxGridDBColumn
        Caption = 'Horas'
        DataBinding.FieldName = 'CU_HORAS'
      end
      object MA_CODIGO: TcxGridDBColumn
        Caption = 'Maestro'
        DataBinding.FieldName = 'MA_CODIGO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
