inherited EditCatPresta_DevEx: TEditCatPresta_DevEx
  Left = 336
  Top = 159
  Caption = 'Tabla de Prestaciones'
  ClientHeight = 309
  ClientWidth = 357
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 273
    Width = 357
    inherited OK_DevEx: TcxButton
      Left = 190
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 270
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 357
    inherited ValorActivo2: TPanel
      Width = 31
      inherited textoValorActivo2: TLabel
        Width = 25
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 357
    Height = 223
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvNone
    TabOrder = 7
    object TB_CODIGOlbl: TLabel
      Left = 84
      Top = 9
      Width = 94
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tabla Prestaciones:'
    end
    object TB_CODIGO: TZetaDBTextBox
      Left = 184
      Top = 7
      Width = 75
      Height = 17
      AutoSize = False
      Caption = 'TB_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'TB_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENTlbl: TLabel
      Left = 156
      Top = 31
      Width = 22
      Height = 13
      Alignment = taRightJustify
      Caption = 'A'#241'o:'
    end
    object Label3: TLabel
      Left = 119
      Top = 52
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Vacaciones:'
    end
    object TB_PRIMAlbl: TLabel
      Left = 93
      Top = 74
      Width = 85
      Height = 13
      Alignment = taRightJustify
      Caption = 'Prima Vacacional:'
    end
    object TB_DIAS_AGUIlbl: TLabel
      Left = 102
      Top = 97
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'as Aguinaldo:'
    end
    object TB_DOMINICALlbl: TLabel
      Left = 81
      Top = 156
      Width = 97
      Height = 13
      Alignment = taRightJustify
      Caption = 'Prima Dominical Fija:'
    end
    object Label2: TLabel
      Left = 95
      Top = 175
      Width = 83
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'as Adicionales:'
    end
    object Label6: TLabel
      Left = 74
      Top = 204
      Width = 104
      Height = 13
      Alignment = taRightJustify
      Caption = 'Factor de Integraci'#243'n:'
    end
    object PT_YEAR: TZetaDBNumero
      Left = 184
      Top = 27
      Width = 75
      Height = 21
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
      UseEnterKey = True
      DataField = 'PT_YEAR'
      DataSource = DataSource
    end
    object PT_DIAS_VA: TZetaDBNumero
      Left = 184
      Top = 48
      Width = 75
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 1
      Text = '0.00'
      UseEnterKey = True
      DataField = 'PT_DIAS_VA'
      DataSource = DataSource
    end
    object PT_PRIMAVA: TZetaDBNumero
      Left = 184
      Top = 70
      Width = 75
      Height = 21
      Mascara = mnTasa
      TabOrder = 2
      Text = '0.0 %'
      UseEnterKey = True
      DataField = 'PT_PRIMAVA'
      DataSource = DataSource
    end
    object PT_DIAS_AG: TZetaDBNumero
      Left = 184
      Top = 93
      Width = 75
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 3
      Text = '0.00'
      UseEnterKey = True
      DataField = 'PT_DIAS_AG'
      DataSource = DataSource
    end
    object PT_PAGO_7: TDBCheckBox
      Left = 50
      Top = 116
      Width = 147
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Pago 7o  D'#237'a Vacaciones:'
      DataField = 'PT_PAGO_7'
      DataSource = DataSource
      TabOrder = 4
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object PT_PRIMA_7: TDBCheckBox
      Left = 22
      Top = 136
      Width = 175
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Prima Vacacional sobre 7o. D'#237'a:'
      DataField = 'PT_PRIMA_7'
      DataSource = DataSource
      TabOrder = 5
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object PT_PRIMADO: TZetaDBNumero
      Left = 184
      Top = 154
      Width = 75
      Height = 21
      Mascara = mnTasa
      TabOrder = 6
      Text = '0.0 %'
      UseEnterKey = True
      DataField = 'PT_PRIMADO'
      DataSource = DataSource
    end
    object PT_DIAS_AD: TZetaDBNumero
      Left = 184
      Top = 176
      Width = 75
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 7
      Text = '0.00'
      UseEnterKey = True
      DataField = 'PT_DIAS_AD'
      DataSource = DataSource
    end
    object PT_FACTOR: TZetaDBNumero
      Left = 184
      Top = 199
      Width = 75
      Height = 21
      Mascara = mnTasa
      TabOrder = 8
      Text = '0.0 %'
      UseEnterKey = True
      DataField = 'PT_FACTOR'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 20
    Top = 273
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 12582928
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 32
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_ImprimirFormaBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 4194360
  end
end
