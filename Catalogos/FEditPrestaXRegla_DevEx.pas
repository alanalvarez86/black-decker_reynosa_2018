unit FEditPrestaXRegla_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
  FDialogoLista_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, ImgList, cxContainer, cxEdit,
  cxCheckListBox, cxButtons;

type
  TEditPrestaXRegla_DevEx = class(TDialogoLista_DevEx)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EditPrestaXRegla_DevEx: TEditPrestaXRegla_DevEx;

implementation
uses
    DCatalogos,
    DTablas;

{$R *.dfm}

procedure TEditPrestaXRegla_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          DataSetChecked:= cdsPrestaXRegla;
          DataSetInicial:= dmTablas.cdsTPresta;
     end;
end;

end.
