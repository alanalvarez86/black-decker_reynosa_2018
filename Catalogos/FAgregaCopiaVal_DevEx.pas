unit FAgregaCopiaVal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal_DevEx, ZetaKeyLookup_DevEx, StdCtrls, Mask, ZetaFecha,
  ZetaDBTextBox, ExtCtrls, DB, ZetaNumero, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, cxContainer, cxEdit, cxGroupBox, ImgList, cxButtons;

type
  TAgregaCopiaVal_DevEx = class(TZetaDlgModal_DevEx)
    PanelPlantilla: TPanel;
    Label6: TLabel;
    VL_CODIGO: TZetaDBTextBox;
    VL_NOMBRE: TZetaDBTextBox;
    GBCopia: TcxGroupBox;
    Label4: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    GBCrearVal: TcxGroupBox;
    Label3: TLabel;
    Label5: TLabel;
    DataSource: TDataSource;
    PU_CODIGOC: TZetaTextBox;
    PU_DESCRIPC: TZetaTextBox;
    VP_FECHAC: TZetaTextBox;
    PU_CODIGO: TZetaDBKeyLookup_DevEx;
    VP_FECHA: TZetaDBFecha;
    VP_FOLIO: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AgregaCopiaVal_DevEx: TAgregaCopiaVal_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaDialogo,
     dCatalogos,
     dCliente,
     ZetaCommonTools;

{$R *.dfm}

procedure TAgregaCopiaVal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          DataSource.DataSet:= cdsValuaciones;
     end;
     HelpContext := H_COPIA_VALUACION;
end;

procedure TAgregaCopiaVal_DevEx.FormShow(Sender: TObject);
const
     aCaption: array[FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Copiar Valuaci�n de Puestos',
                                                 'Crear Valuaci�n de Puestos' );
begin
     inherited;
     with dmCatalogos do
     begin
          GBCopia.Visible:= not AgregandoVal;
          Self.Caption:= aCaption[AgregandoVal];
          if not AgregandoVal then
          begin
               PU_CODIGO.Llave:= VACIO;
               PU_CODIGOC.Caption:= cdsValuaciones.FieldByName('PU_CODIGO').AsString;
               PU_DESCRIPC.Caption:= cdsValuaciones.FieldByName('PU_DESCRIP').AsString;
               VP_FOLIO.Valor:= cdsValuaciones.FieldByName('VP_FOLIO').AsInteger;
               VP_FECHAC.Caption:= cdsValuaciones.FieldByName('VP_FECHA').AsString;
          end;
          cdsValuaciones.Append;
     end;
end;

procedure TAgregaCopiaVal_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     if ( ModalResult = mrCancel ) then
        dmCatalogos.cdsValuaciones.CancelUpdates;
end;

procedure TAgregaCopiaVal_DevEx.OK_DevExClick(Sender: TObject);
var
   FParametros: TZetaParams;
   iFolio: Integer;
begin
     inherited;
     iFolio:= 0;
     if ( StrLLeno ( PU_CODIGO.Llave ) ) then
     begin
          with dmCatalogos do
          begin
               try
                  FParametros := TZetaParams.Create;
                  if AgregandoVal then
                  begin
                       FParametros.AddInteger( 'Plantilla', PlantillaActiva );
                       FParametros.AddInteger( 'Folio', iFolio );
                       AgregaCopiaValuacion( FParametros );
                  end
                  else
                  begin
                       iFolio:= VP_FOLIO.ValorEntero;
                       with FParametros do
                       begin
                            AddInteger('Folio', iFolio );
                            AddString( 'Puesto', PU_CODIGO.Llave );
                            AddDate('Fecha', VP_FECHA.Valor );
                            AddInteger( 'Usuario', dmCliente.Usuario );
                       end;
                       AgregaCopiaValuacion( FParametros );
                  end;
                  Close;
               finally
                      FreeAndNil(FParametros);
               end;
          end;
     end
     else ZetaDialogo.zError( Caption, 'El puesto no puede quedar vac�o', 0 );
end;

end.
