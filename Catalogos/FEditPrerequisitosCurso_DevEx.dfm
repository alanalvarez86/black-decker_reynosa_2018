inherited EditPrerequisitosCurso_DevEx: TEditPrerequisitosCurso_DevEx
  Caption = 'Prerrequisitos por Curso'
  ClientHeight = 273
  ClientWidth = 459
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel [0]
    Left = 0
    Top = 50
    Width = 459
    Height = 267
    Align = alTop
    TabOrder = 7
    object Label1: TLabel
      Left = 62
      Top = 7
      Width = 30
      Height = 13
      Caption = 'Curso:'
    end
    object ZCurso: TZetaTextBox
      Left = 202
      Top = 5
      Width = 248
      Height = 17
      AutoSize = False
      Caption = 'ZCurso'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label2: TLabel
      Left = 10
      Top = 29
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = 'Curso Requerido:'
    end
    object lblObservaciones: TLabel
      Left = 18
      Top = 76
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = 'Observaciones:'
    end
    object CU_CODIGO: TZetaDBTextBox
      Left = 94
      Top = 5
      Width = 104
      Height = 17
      AutoSize = False
      Caption = 'CU_CODIGO'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CU_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CP_CURSO: TZetaDBKeyLookup_DevEx
      Left = 94
      Top = 25
      Width = 359
      Height = 21
      LookupDataset = dmCatalogos.cdsCursos
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CP_CURSO'
      DataSource = DataSource
    end
    object CP_OPCIONA: TDBCheckBox
      Left = 45
      Top = 51
      Width = 62
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Opcional:'
      DataField = 'CP_OPCIONA'
      DataSource = DataSource
      TabOrder = 1
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object CP_COMENTA: TcxDBMemo
      Left = 94
      Top = 76
      DataBinding.DataField = 'CP_COMENTA'
      DataBinding.DataSource = DataSource
      Properties.ScrollBars = ssVertical
      TabOrder = 2
      Height = 120
      Width = 359
    end
  end
  inherited PanelBotones: TPanel
    Top = 237
    Width = 459
    TabOrder = 2
    inherited OK_DevEx: TcxButton
      Left = 294
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 373
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 459
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 133
      inherited textoValorActivo2: TLabel
        Width = 127
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 3
  end
  inherited DataSource: TDataSource
    Left = 12
    Top = 201
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 140
    Top = 134
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 10223722
  end
end
