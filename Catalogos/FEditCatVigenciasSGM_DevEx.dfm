inherited EditCatVigenciasSGM_DevEx: TEditCatVigenciasSGM_DevEx
  Left = 559
  Top = 201
  Caption = 'Vigencia'
  ClientHeight = 452
  ClientWidth = 448
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 36
    Top = 387
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [1]
    Left = 27
    Top = 364
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label8: TLabel [2]
    Left = 18
    Top = 340
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Costo Fijo:'
  end
  inherited PanelBotones: TPanel
    Top = 416
    Width = 448
    inherited OK_DevEx: TcxButton
      Left = 284
      ParentBiDiMode = True
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 363
      Cancel = True
      ParentBiDiMode = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 448
    inherited ValorActivo2: TPanel
      Width = 122
      inherited textoValorActivo2: TLabel
        Width = 116
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 13
  end
  object PV_TEXTO: TDBEdit [6]
    Left = 72
    Top = 383
    Width = 313
    Height = 21
    DataField = 'PV_TEXTO'
    DataSource = DataSource
    TabOrder = 9
  end
  object Panel1: TPanel [7]
    Left = 0
    Top = 50
    Width = 448
    Height = 54
    Align = alTop
    TabOrder = 3
    object Label3: TLabel
      Left = 18
      Top = 10
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = 'No. P'#243'liza:'
    end
    object Label4: TLabel
      Left = 12
      Top = 29
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descrpci'#243'n:'
    end
    object Label5: TLabel
      Left = 194
      Top = 10
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object zTxtNoPoliza: TZetaTextBox
      Left = 72
      Top = 8
      Width = 121
      Height = 17
      AutoSize = False
      Caption = 'zTxtNoPoliza'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object zTxtDescripcion: TZetaTextBox
      Left = 72
      Top = 28
      Width = 361
      Height = 17
      AutoSize = False
      Caption = 'zTxtDescripcion'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object zTxtTipoDescrip: TZetaTextBox
      Left = 221
      Top = 8
      Width = 211
      Height = 17
      AutoSize = False
      Caption = 'zTxtTipoDescrip'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  object Panel2: TPanel [8]
    Left = 0
    Top = 104
    Width = 448
    Height = 36
    Align = alTop
    TabOrder = 4
    object DBCodigoLBL: TLabel
      Left = 14
      Top = 12
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Referencia:'
    end
    object PV_REFEREN: TZetaDBEdit
      Left = 72
      Top = 8
      Width = 113
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      Text = 'PV_REFEREN'
      ConfirmEdit = True
      DataField = 'PV_REFEREN'
      DataSource = DataSource
    end
  end
  object GroupBox1: TcxGroupBox [9]
    Left = 0
    Top = 140
    Align = alTop
    Caption = ' Vigencia de la P'#243'liza '
    TabOrder = 5
    Height = 77
    Width = 448
    object Label6: TLabel
      Left = 52
      Top = 24
      Width = 17
      Height = 13
      Alignment = taRightJustify
      Caption = 'De:'
    end
    object Label7: TLabel
      Left = 57
      Top = 48
      Width = 12
      Height = 13
      Alignment = taRightJustify
      Caption = 'Al:'
    end
    object PV_FEC_INI: TZetaDBFecha
      Left = 72
      Top = 21
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '21/mar/12'
      Valor = 40989.000000000000000000
      DataField = 'PV_FEC_INI'
      DataSource = DataSource
    end
    object PV_FEC_FIN: TZetaDBFecha
      Left = 72
      Top = 45
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '21/mar/12'
      Valor = 40989.000000000000000000
      DataField = 'PV_FEC_FIN'
      DataSource = DataSource
    end
  end
  object GroupBox2: TcxGroupBox [10]
    Left = 0
    Top = 217
    Align = alTop
    Caption = ' Condiciones '
    TabOrder = 6
    Height = 116
    Width = 448
    object PV_OBSERVA: TcxDBMemo
      Left = 3
      Top = 15
      Align = alClient
      DataBinding.DataField = 'PV_CONDIC'
      DataBinding.DataSource = DataSource
      ParentFont = False
      Properties.MaxLength = 255
      Properties.ScrollBars = ssVertical
      TabOrder = 0
      Height = 91
      Width = 442
    end
  end
  object PV_NUMERO: TZetaDBNumero [11]
    Left = 72
    Top = 360
    Width = 121
    Height = 21
    Mascara = mnPesos
    TabOrder = 8
    Text = '0.00'
    DataField = 'PV_NUMERO'
    DataSource = DataSource
  end
  object PV_CFIJO: TZetaDBNumero [12]
    Left = 72
    Top = 336
    Width = 121
    Height = 21
    Mascara = mnPesos
    TabOrder = 7
    Text = '0.00'
    DataField = 'PV_CFIJO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 412
    Top = 113
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
