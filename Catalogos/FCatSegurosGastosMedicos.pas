unit FCatSegurosGastosMedicos;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls;

type
  TCatSegurosGastosMedicos = class(TBaseConsulta)
    GridPensiones: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DoLookup; override;
  protected
    { Public declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;

  end;

var
  CatSegurosGastosMedicos: TCatSegurosGastosMedicos;

implementation

uses ZetaDialogo,
     ZImprimeForma,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaBuscador,
     ZAccesosMgr,
     ZAccesosTress,
     dCatalogos;

{$R *.dfm}

procedure TCatSegurosGastosMedicos.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := True;
  HelpContext:= H_Cat_SGM;
end;

procedure TCatSegurosGastosMedicos.Connect;
begin
     with dmCatalogos do
     begin
          cdsSegGastosMed.Conectar;
          DataSource.DataSet := cdsSegGastosMed;
     end;
end;

procedure TCatSegurosGastosMedicos.Refresh;
begin
     dmCatalogos.cdsSegGastosMed.Refrescar;
end;

procedure TCatSegurosGastosMedicos.Agregar;
begin
     dmCatalogos.cdsSegGastosMed.Agregar;
end;

procedure TCatSegurosGastosMedicos.Borrar;
begin
     dmCatalogos.cdsSegGastosMed.Borrar;
end;

procedure TCatSegurosGastosMedicos.Modificar;
begin
     dmCatalogos.cdsSegGastosMed.Modificar;
end;

procedure TCatSegurosGastosMedicos.DoLookup;
begin
     ZetaBuscador.BuscarCodigo( 'Seguro de Gastos M�dicos', 'Seguros Gastos M�dicos', 'PM_CODIGO', dmCatalogos.cdsSegGastosMed );
end;

function TCatSegurosGastosMedicos.PuedeModificar(
  var sMensaje: String): Boolean;
begin
     sMensaje := 'No Tiene Derechos a ver Detalle de Seguros de Gastos M�dicos';
     Result := ZAccesosMgr.CheckDerecho(D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CONSULTA );
end;

end.
