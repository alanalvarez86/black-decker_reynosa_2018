inherited CosteoGrupos_DevEx: TCosteoGrupos_DevEx
  Left = 0
  Top = 0
  Caption = 'Grupos de Costeo'
  ClientHeight = 213
  ClientWidth = 522
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 522
    inherited ValorActivo2: TPanel
      Width = 263
      inherited textoValorActivo2: TLabel
        Width = 257
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 522
    Height = 194
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object GpoCostoCodigo: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'GpoCostoCodigo'
        MinWidth = 70
        Width = 70
      end
      object GpoCostoNombre: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'GpoCostoNombre'
        MinWidth = 100
        Width = 100
      end
      object GpoCostoActivo: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'GpoCostoActivo'
        MinWidth = 80
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 408
    Top = 24
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end