inherited CatTiposPension_DevEx: TCatTiposPension_DevEx
  Left = 1579
  Top = 213
  Caption = 'Tipos de Pensi'#243'n'
  ClientWidth = 991
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 991
    inherited ValorActivo2: TPanel
      Width = 732
      inherited textoValorActivo2: TLabel
        Width = 726
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 991
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object TP_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TP_CODIGO'
        MinWidth = 65
        Width = 65
      end
      object TP_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'TP_NOMBRE'
        MinWidth = 100
        Width = 100
      end
      object TP_APL_NOM: TcxGridDBColumn
        Caption = 'N'#243'mina'
        DataBinding.FieldName = 'TP_APL_NOM'
        Width = 191
      end
      object TP_CO_PENS_TXT: TcxGridDBColumn
        Caption = 'Percepciones y/o Prestaciones (+)'
        DataBinding.FieldName = 'TP_CO_PENS_TXT'
        MinWidth = 200
        Options.Filtering = False
        Width = 200
      end
      object TP_CO_DESC: TcxGridDBColumn
        Caption = 'Deducciones (-)'
        DataBinding.FieldName = 'TP_CO_DESC'
        MinWidth = 120
        Width = 120
      end
      object US_DESCRIP: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_DESCRIP'
        MinWidth = 100
        Width = 100
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 576
    Top = 224
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
