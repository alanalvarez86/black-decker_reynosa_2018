unit FCatSegurosGastosMedicos_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TCatSegurosGastosMedicos_DevEx = class(TBaseGridLectura_DevEx)
    PM_CODIGO: TcxGridDBColumn;
    PM_NUMERO: TcxGridDBColumn;
    PM_DESCRIP: TcxGridDBColumn;
    PM_TIPO: TcxGridDBColumn;
    PM_ASEGURA: TcxGridDBColumn;
    PM_BROKER: TcxGridDBColumn;
    PM_NOM_CT1: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DoLookup; override;
  protected
    { Public declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;

  end;

var
  CatSegurosGastosMedicos_DevEx: TCatSegurosGastosMedicos_DevEx;

implementation

uses ZetaDialogo,
     ZImprimeForma,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaBuscador_DevEx,
     ZAccesosMgr,
     ZAccesosTress,
     dCatalogos;

{$R *.dfm}

procedure TCatSegurosGastosMedicos_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := True;
  HelpContext:= H_Cat_SGM;
end;

procedure TCatSegurosGastosMedicos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsSegGastosMed.Conectar;
          DataSource.DataSet := cdsSegGastosMed;
     end;
end;

procedure TCatSegurosGastosMedicos_DevEx.Refresh;
begin
     dmCatalogos.cdsSegGastosMed.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatSegurosGastosMedicos_DevEx.Agregar;
begin
     dmCatalogos.cdsSegGastosMed.Agregar;
end;

procedure TCatSegurosGastosMedicos_DevEx.Borrar;
begin
     dmCatalogos.cdsSegGastosMed.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatSegurosGastosMedicos_DevEx.Modificar;
begin
     dmCatalogos.cdsSegGastosMed.Modificar;
end;

procedure TCatSegurosGastosMedicos_DevEx.DoLookup;
begin
     ZetaBuscador_DevEx.BuscarCodigo( 'Seguro de Gastos M�dicos', 'Seguros Gastos M�dicos', 'PM_CODIGO', dmCatalogos.cdsSegGastosMed );
end;

function TCatSegurosGastosMedicos_DevEx.PuedeModificar(
  var sMensaje: String): Boolean;
begin
     sMensaje := 'No Tiene Derechos a ver Detalle de Seguros de Gastos M�dicos';
     Result := ZAccesosMgr.CheckDerecho(D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CONSULTA );
end;

procedure TCatSegurosGastosMedicos_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
  inherited;

   //columna sumatoria: cuantos
   CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('PM_CODIGO'), K_SIN_TIPO , '', skCount);
   ZetaDBGridDBTableView.ApplyBestFit();

   //agrupar
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  //ver la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;


end;

end.
