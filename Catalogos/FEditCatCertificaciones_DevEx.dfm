inherited EditCatCertificaciones_DevEx: TEditCatCertificaciones_DevEx
  Left = 308
  Top = 272
  Caption = 'Certificaciones'
  ClientHeight = 506
  ClientWidth = 502
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 470
    Width = 502
    TabOrder = 4
    inherited OK_DevEx: TcxButton
      Left = 334
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 413
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 502
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 176
      inherited textoValorActivo2: TLabel
        Width = 170
      end
    end
  end
  object PageControl: TcxPageControl [3]
    Left = 0
    Top = 120
    Width = 502
    Height = 350
    Align = alClient
    TabOrder = 2
    Properties.ActivePage = TabGenerales
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 348
    ClientRectLeft = 2
    ClientRectRight = 500
    ClientRectTop = 27
    object TabGenerales: TcxTabSheet
      Caption = 'Generales'
      object Label3: TLabel
        Left = 37
        Top = 21
        Width = 31
        Height = 13
        Caption = 'Ingl'#233's:'
      end
      object TLabel
        Left = 28
        Top = 45
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object Label4: TLabel
        Left = 37
        Top = 69
        Width = 30
        Height = 13
        Caption = 'Texto:'
      end
      object TLabel
        Left = 24
        Top = 93
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = 'Renovar:'
      end
      object Label5: TLabel
        Left = 197
        Top = 93
        Width = 21
        Height = 13
        Caption = 'd'#237'as'
      end
      object TLabel
        Left = 20
        Top = 113
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Resumen:'
      end
      object CI_NUMERO: TZetaDBNumero
        Left = 71
        Top = 40
        Width = 121
        Height = 21
        Mascara = mnNumeroGlobal
        TabOrder = 1
        Text = '0.00'
        DataField = 'CI_NUMERO'
        DataSource = DataSource
      end
      object CI_RENOVAR: TZetaDBNumero
        Left = 71
        Top = 88
        Width = 121
        Height = 21
        Mascara = mnDias
        TabOrder = 3
        Text = '0'
        DataField = 'CI_RENOVAR'
        DataSource = DataSource
      end
      object CI_RESUMEN: TcxDBMemo
        Left = 73
        Top = 113
        DataBinding.DataField = 'CI_RESUMEN'
        DataBinding.DataSource = DataSource
        Properties.ScrollBars = ssVertical
        TabOrder = 4
        Height = 181
        Width = 358
      end
      object CI_INGLES: TDBEdit
        Left = 71
        Top = 16
        Width = 358
        Height = 21
        DataField = 'CI_INGLES'
        DataSource = DataSource
        TabOrder = 0
      end
      object CI_TEXTO: TDBEdit
        Left = 71
        Top = 64
        Width = 358
        Height = 21
        DataField = 'CI_TEXTO'
        DataSource = DataSource
        TabOrder = 2
      end
    end
    object TabSheet2: TcxTabSheet
      Caption = 'Observaciones'
      ImageIndex = 1
      object CI_DETALLE: TcxDBMemo
        Left = 0
        Top = 0
        Align = alClient
        DataBinding.DataField = 'CI_DETALLE'
        DataBinding.DataSource = DataSource
        Properties.ScrollBars = ssVertical
        TabOrder = 0
        Height = 321
        Width = 498
      end
    end
  end
  object Encabezado: TPanel [4]
    Left = 0
    Top = 50
    Width = 502
    Height = 70
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 37
      Top = 13
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 14
      Top = 37
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object CI_NOMBRE: TDBEdit
      Left = 75
      Top = 32
      Width = 358
      Height = 21
      DataField = 'CI_NOMBRE'
      DataSource = DataSource
      TabOrder = 1
    end
    object CI_CODIGO: TZetaDBEdit
      Left = 75
      Top = 8
      Width = 121
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'CI_CODIGO'
      DataSource = DataSource
    end
    object CI_ACTIVO: TDBCheckBox
      Left = 381
      Top = 10
      Width = 52
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Activo:'
      DataField = 'CI_ACTIVO'
      DataSource = DataSource
      TabOrder = 2
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
  end
  inherited DataSource: TDataSource
    Left = 460
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
