unit FCatValuaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, StdCtrls, ZetaNumero,
  Mask, ZetaFecha, ZetaKeyCombo, ZetaKeyLookup_DevEx, ZetaClientDataSet,
  ZetaCommonClasses, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxContainer, cxGroupBox, cxButtons,
  System.Actions;

type
  TCatValuaciones_DevEx = class(TBaseGridLectura_DevEx)
    PanelFiltros: TPanel;
    Plantilla: TZetaKeyLookup_DevEx;
    Label1: TLabel;
    GroupBox1: TcxGroupBox;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label2: TLabel;
    FiltrarFecha: TCheckBox;
    VP_GRADO: TZetaNumero;
    cbHistorial: TComboBox;
    VP_STATUS: TZetaKeyCombo;
    BtnCopiar: TcxButton;
    Puesto: TZetaKeyLookup_DevEx;
    VP_FOLIO: TcxGridDBColumn;
    PU_CODIGO: TcxGridDBColumn;
    PU_DESCRIP: TcxGridDBColumn;
    VP_FECHA: TcxGridDBColumn;
    VP_GRADO_GRID: TcxGridDBColumn;
    VP_PUNTOS: TcxGridDBColumn;
    VP_NUM_FIN: TcxGridDBColumn;
    VP_STATUS_GRID: TcxGridDBColumn;
    lbInicial: TLabel;
    FechaInicial: TZetaFecha;
    FechaFinal: TZetaFecha;
    lbFinal: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FiltrarFechaClick(Sender: TObject);
    procedure BtnFiltrarClick(Sender: TObject);
    procedure BtnCopiarClick(Sender: TObject);
    procedure PlantillaValidKey(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    FParametros: TZetaParams;
    procedure LlenaCombo;
    procedure EnableFecha;
    procedure MuestraFormaVal;
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
  public
    { Public declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  end;

const
     K_DERECHO_COPIAR_PUESTO = K_DERECHO_SIST_KARDEX;

var
  CatValuaciones_DevEx: TCatValuaciones_DevEx;

implementation

{$R *.dfm}

uses DCatalogos,
     ZetaCommonLists,
     ZetaCommonTools,
     DCliente,
     FAgregaCopiaVal_DevEx,
     ZetaDialogo,
     ZAccesosMgr,
     ZAccesosTress;

{ TCatValuaciones }

procedure TCatValuaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_VALUACIONES;
     FParametros := TZetaParams.Create;
     LlenaCombo;
     //VP_STATUS.Items.Insert( 0, 'Todos' );
     //VP_STATUS.ItemIndex := 0;
end;

procedure TCatValuaciones_DevEx.FormDestroy(Sender: TObject);
begin
     FParametros.Free;
     inherited;
end;

procedure TCatValuaciones_DevEx.FormShow(Sender: TObject);
const
     K_ULTIMAS = 1;
begin
     ApplyMinWidth;
     inherited;
     if ( dmCatalogos.PlantillaActiva > 0 ) then
        FechaInicial.Valor := dmCatalogos.cdsValPlantilla.FieldByName('VL_FECHA').AsDateTime
     else
         FechaInicial.Valor:= dmCliente.FechaDefault;
     FechaFinal.Valor := dmCliente.FechaDefault;
     cbHistorial.ItemIndex:= K_ULTIMAS;
     EnableFecha;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('VP_FOLIO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatValuaciones_DevEx.Connect;
begin
     inherited;
     with dmCatalogos do
     begin
          cdsValPlantilla.Conectar;
          cdsPuestos.Conectar;
          DataSource.DataSet := cdsValuaciones;
          Plantilla.Valor:= PlantillaActiva;
          Refresh;
     end;
end;

procedure TCatValuaciones_DevEx.Refresh;
const
     K_TODAS_INDEX = 2;
     K_TODAS = 'T';
var
   dInicial, dFinal: TDate;
   sHistorial: String;
begin
     dInicial:= NullDateTime;
     dFinal:= NullDateTime;
     with FiltrarFecha do
     begin
          if Checked then
          begin
               dInicial:= FechaInicial.Valor;
               dFinal:= FechaFinal.Valor;
          end;
     end;
     with cbHistorial do
     begin
          if ( ItemIndex <> K_TODAS_INDEX ) then
          begin
               sHistorial:= zBoolToStr(IntToBool(ItemIndex))
          end
          else
              sHistorial:= K_TODAS;

     end;
     with FParametros do
     begin
          AddInteger( 'Plantilla', Plantilla.Valor );
          //AddString( 'Puesto', Puesto.Llave );
          AddString( 'Puesto', '' );
          AddDate('FechaInicial', dInicial );
          AddDate('FechaFinal', dFinal );
          //AddInteger( 'Status', VP_STATUS.Valor -1 );
          AddInteger( 'Status', -1 );//obtendra todos los registros .. 
          AddString( 'Historial',sHistorial);
          //AddInteger( 'Grado', VP_GRADO.ValorEntero );
          AddInteger( 'Grado', 0 );
     end;
      {***(@am): Este cambio es temporal, mientras se define como realizar el ordenamiento despues de Refrescar en formas especiales***}
     if dmCatalogos.cdsValuaciones.IndexName <> '' then
        dmCatalogos.cdsValuaciones.IndexName := '';
     {***}
     dmCatalogos.RefrescarValuacion( FParametros );
     ZetaDBGridDBTableView.ApplyBestFit();
end;

function TCatValuaciones_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
          Result:= dmCatalogos.PlantillaLiberada( dmCatalogos.PlantillaActiva, sMensaje, 'Agregar' );
end;

procedure TCatValuaciones_DevEx.Agregar;
begin
     inherited;
     dmCatalogos.AgregandoVal:= TRUE;
     MuestraFormaVal;
end;

function TCatValuaciones_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
          Result:= dmCatalogos.PlantillaLiberada( dmCatalogos.PlantillaActiva, sMensaje, 'Borrar' );
end;

procedure TCatValuaciones_DevEx.Borrar;
begin
     inherited;
     dmCatalogos.cdsValuaciones.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

function TCatValuaciones_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
          Result:= dmCatalogos.PlantillaLiberada( dmCatalogos.PlantillaActiva, sMensaje, 'Modificar' );
end;

procedure TCatValuaciones_DevEx.Modificar;
begin
     inherited;
     dmCatalogos.cdsValuaciones.Modificar;
end;

procedure TCatValuaciones_DevEx.FiltrarFechaClick(Sender: TObject);
begin
     EnableFecha;
end;

procedure TCatValuaciones_DevEx.LlenaCombo;
begin
     with cbHistorial.Items do
     begin
          BeginUpdate;
          try
             Clear;
             Add( 'Anteriores' );
             Add( 'Ultimas' );
             Add( 'Todas' );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TCatValuaciones_DevEx.EnableFecha;
begin
     lbInicial.Enabled := Filtrarfecha.Checked;
     FechaInicial.Enabled:= Filtrarfecha.Checked;
     lbFinal.Enabled:=  Filtrarfecha.Checked;
     FechaFinal.Enabled:= Filtrarfecha.Checked;
end;

procedure TCatValuaciones_DevEx.MuestraFormaVal;
begin
          AgregaCopiaVal_DevEx := TAgregaCopiaVal_DevEx.Create( Self );
          try
             with AgregaCopiaVal_DevEx do
             begin
                  ShowModal;
             end;
          finally
                 FreeAndNil(AgregaCopiaVal_DevEx);
          end;
end;


procedure TCatValuaciones_DevEx.BtnFiltrarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TCatValuaciones_DevEx.BtnCopiarClick(Sender: TObject);
var
   sMensaje: String;
begin
     with dmCatalogos do
     begin
          if ( PlantillaLiberada( PlantillaActiva, sMensaje, 'Copiar Puesto' ) ) then
          begin
               AgregandoVal:= FALSE;
               MuestraFormaVal;
          end
          else
              ZInformation( Caption, sMensaje,0 );
     end;
end;



procedure TCatValuaciones_DevEx.PlantillaValidKey(Sender: TObject);
begin
     inherited;
     dmCatalogos.PlantillaActiva := Plantilla.Valor;
     Refresh;
end;


procedure TCatValuaciones_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     BtnCopiar.Enabled := ( not NoHayDatos ) and ZAccesosMgr.CheckDerecho( D_CAT_VALUACIONES, K_DERECHO_COPIAR_PUESTO );
end;

end.
