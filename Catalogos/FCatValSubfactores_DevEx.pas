unit FCatValSubfactores_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, StdCtrls, Mask, ZetaNumero,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, ZetaSmartLists_DevEx, ZetaKeyLookup_DevEx;

type
  TCatValSubfactores_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    VL_CODIGO: TZetaKeyLookup_DevEx;
    Panel2: TPanel;
    zmlArriba: TZetaSmartListsButton_DevEx;
    zmlAbajo: TZetaSmartListsButton_DevEx;
    VL_MAX_PTS: TZetaNumero;
    VS_ORDEN: TcxGridDBColumn;
    VS_CODIGO: TcxGridDBColumn;
    VS_NOMBRE: TcxGridDBColumn;
    VS_MAX_PTS: TcxGridDBColumn;
    VS_NUM_NIV: TcxGridDBColumn;
    VF_NOMBRE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure zmlArribaAbajoClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure VL_CODIGOValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
   { Public declarations }
   procedure DoLookup; override;
   function PuedeAgregar( var sMensaje: String ): Boolean; override;
   function PuedeBorrar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  CatValSubfactores_DevEx: TCatValSubfactores_DevEx;

implementation

uses DCatalogos,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     ZetaBuscador_DevEx;


{$R *.dfm}

{ TCatValSubfactores }

procedure TCatValSubfactores_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_VALUACION_SUBFACTORES;
end;

procedure TCatValSubfactores_DevEx.Agregar;
begin
     dmCatalogos.cdsSubfactores.Agregar;
end;

procedure TCatValSubfactores_DevEx.Borrar;
begin
     dmCatalogos.cdsSubfactores.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatValSubfactores_DevEx.Connect;
begin
     inherited;
     with dmCatalogos do
     begin
          cdsValPlantilla.Conectar;
          VL_CODIGO.Valor:= PlantillaActiva;
          if ( cdsValPlantilla.Locate( 'VL_CODIGO', PlantillaActiva, [] ) ) then
             VL_MAX_PTS.Valor:= dmCatalogos.cdsValPlantilla.FieldByName('VL_MAX_PTS').AsInteger;
          cdsValFactores.Conectar;
          cdsSubfactores.Conectar;
          DataSource.DataSet:= cdsSubfactores;
     end;
end;

procedure TCatValSubfactores_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Subfactor', 'Subfactor', 'VS_ORDEN', dmCatalogos.cdsSubfactores );
end;

procedure TCatValSubfactores_DevEx.Modificar;
begin
     dmCatalogos.cdsSubfactores.Modificar;
     with dmCatalogos.cdsValPlantilla do
     begin
          if ( Locate('VL_CODIGO', VL_CODIGO.Llave, [] ) ) then
             VL_MAX_PTS.Valor:= dmCatalogos.cdsValPlantilla.FieldByName('VL_MAX_PTS').AsInteger;
     end;
end;

procedure TCatValSubfactores_DevEx.Refresh;
begin
     dmCatalogos.cdsSubfactores.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatValSubfactores_DevEx.zmlArribaAbajoClick(Sender: TObject);
var sMensaje:string;
begin
     if  PuedeModificar(sMensaje) then
     begin
          with dmCatalogos do
     	  begin
               if ( PlantillaEnDiseno ( cdsSubfactores.FieldByName('VL_CODIGO').AsInteger, sMensaje, 'Cambiar Orden' ) ) then
                    SubeBajaOrden( eSubeBaja( TZetaSmartListsButton_DevEx(Sender).Tag ), cdsSubfactores,cdsSubfactores.FieldByName('VL_CODIGO').AsString,VACIO)
               else
                   ZInformation( Caption, sMensaje ,0 );
     	  end;
     end
     else
         zInformation( Caption, 'No tiene permisos para cambiar el orden', 0 );
end;

procedure TCatValSubfactores_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     zmlArriba.Enabled := not NoHayDatos;
     zmlAbajo.Enabled := not NoHayDatos;
end;

procedure TCatValSubfactores_DevEx.VL_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     dmCatalogos.PlantillaActiva := VL_CODIGO.Valor;
     with dmCatalogos.cdsValPlantilla do
     begin
          if ( Locate('VL_CODIGO', VL_CODIGO.Llave, [] ) ) then
             VL_MAX_PTS.Valor:= dmCatalogos.cdsValPlantilla.FieldByName('VL_MAX_PTS').AsInteger;
     end;
end;

function TCatValSubfactores_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
        Result:= dmCatalogos.PlantillaEnDiseno( dmCatalogos.PlantillaActiva, sMensaje, 'Agregar' );
end;

function TCatValSubfactores_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
        Result:= dmCatalogos.PlantillaEnDiseno( dmCatalogos.PlantillaActiva, sMensaje, 'Borrar' );
end;

procedure TCatValSubfactores_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('VS_ORDEN'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
end;

end.
