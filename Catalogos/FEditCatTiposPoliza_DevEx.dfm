inherited EditCatTiposPoliza_DevEx: TEditCatTiposPoliza_DevEx
  Left = 359
  Top = 289
  Caption = 'Tipo de P'#243'liza'
  ClientHeight = 237
  ClientWidth = 448
  PixelsPerInch = 96
  TextHeight = 13
  object DBCodigoLBL: TLabel [0]
    Left = 73
    Top = 56
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label1: TLabel [1]
    Left = 79
    Top = 144
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [2]
    Left = 69
    Top = 122
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object DBInglesLBL: TLabel [3]
    Left = 78
    Top = 100
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object lblNombre: TLabel [4]
    Left = 69
    Top = 78
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre:'
  end
  object FormatoLBL: TLabel [5]
    Left = 22
    Top = 166
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'Formato de &P'#243'liza:'
  end
  inherited PanelBotones: TPanel
    Top = 201
    Width = 448
    inherited OK_DevEx: TcxButton
      Left = 284
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 363
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 448
    inherited ValorActivo2: TPanel
      Width = 122
      inherited textoValorActivo2: TLabel
        Width = 116
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 12
  end
  object PT_CODIGO: TZetaDBEdit [9]
    Left = 113
    Top = 52
    Width = 72
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 3
    Text = 'PT_CODIGO'
    ConfirmEdit = True
    DataField = 'PT_CODIGO'
    DataSource = DataSource
  end
  object PT_NOMBRE: TDBEdit [10]
    Left = 113
    Top = 74
    Width = 280
    Height = 21
    DataField = 'PT_NOMBRE'
    DataSource = DataSource
    TabOrder = 4
  end
  object PT_INGLES: TDBEdit [11]
    Left = 113
    Top = 96
    Width = 280
    Height = 21
    DataField = 'PT_INGLES'
    DataSource = DataSource
    TabOrder = 5
  end
  object PT_NUMERO: TZetaDBNumero [12]
    Left = 113
    Top = 118
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 6
    Text = '0.00'
    DataField = 'PT_NUMERO'
    DataSource = DataSource
  end
  object PT_TEXTO: TDBEdit [13]
    Left = 113
    Top = 140
    Width = 280
    Height = 21
    DataField = 'PT_TEXTO'
    DataSource = DataSource
    TabOrder = 7
  end
  object PT_REPORTE: TZetaDBKeyLookup_DevEx [14]
    Left = 113
    Top = 162
    Width = 305
    Height = 21
    Filtro = 'RE_TIPO IN (3,4)'
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 8
    TabStop = True
    WidthLlave = 60
    DataField = 'PT_REPORTE'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 412
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
