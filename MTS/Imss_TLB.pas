unit Imss_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 2/10/2011 5:10:08 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20_Vsn_2011\MTS\Imss.tlb (1)
// LIBID: {746C6AC0-88D4-11D3-9707-0050DA04EA99}
// LCID: 0
// Helpfile: 
// HelpString: Tress Imss
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImssMajorVersion = 1;
  ImssMinorVersion = 0;

  LIBID_Imss: TGUID = '{746C6AC0-88D4-11D3-9707-0050DA04EA99}';

  IID_IdmServerImss: TGUID = '{746C6AC1-88D4-11D3-9707-0050DA04EA99}';
  CLASS_dmServerImss: TGUID = '{746C6AC3-88D4-11D3-9707-0050DA04EA99}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerImss = interface;
  IdmServerImssDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerImss = IdmServerImss;


// *********************************************************************//
// Interface: IdmServerImss
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {746C6AC1-88D4-11D3-9707-0050DA04EA99}
// *********************************************************************//
  IdmServerImss = interface(IAppServer)
    ['{746C6AC1-88D4-11D3-9707-0050DA04EA99}']
    function GetIMSSDatosBim(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                             EmpleadoNumero: Integer; const Patron: WideString): OleVariant; safecall;
    function GetMovIMSSDatosBim(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                                EmpleadoNumero: Integer; const Patron: WideString): OleVariant; safecall;
    function GetIMSSDatosMen(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                             EmpleadoNumero: Integer; const Patron: WideString): OleVariant; safecall;
    function GetMovIMSSDatosMen(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                                EmpleadoNumero: Integer; const Patron: WideString): OleVariant; safecall;
    function GetIMSSDatosTot(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                             const Patron: WideString): OleVariant; safecall;
    function GetIMSSHisMen(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                           const Patron: WideString): OleVariant; safecall;
    function GetMovIMSSHisMen(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                              const Patron: WideString): OleVariant; safecall;
    function GetIMSSHisBim(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                           const Patron: WideString): OleVariant; safecall;
    function GetMovIMSSHisBim(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                              const Patron: WideString): OleVariant; safecall;
    function GrabaIMSSDatosTot(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function CalcularTasaINFONAVIT(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CalcularPagosIMSS(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CalcularRecargosIMSS(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RevisarSUA(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ExportarSUA(Empresa: OleVariant; Parametros: OleVariant; out Empleados: OleVariant; 
                         out Movimientos: OleVariant; out Infonavit: OleVariant; 
                         out Incapacidades: OleVariant): OleVariant; safecall;
    function GetPrimaRiesgo(Empresa: OleVariant; Fecha: TDateTime; const Patron: WideString): Double; safecall;
    function CalcularPrimaRiesgo(Empresa: OleVariant; Parametros: OleVariant; out Datos: OleVariant): OleVariant; safecall;
    function GetDatosConciliaInfo(Empresa: OleVariant; ImssYear: Integer; ImssMes: Integer; 
                                  Empleado: Integer): OleVariant; safecall;
    function AjusteRetInfonavit(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AjusteRetInfonavitGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AjusteRetInfonavitLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ValidarMovimientosIDSE(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerImssDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {746C6AC1-88D4-11D3-9707-0050DA04EA99}
// *********************************************************************//
  IdmServerImssDisp = dispinterface
    ['{746C6AC1-88D4-11D3-9707-0050DA04EA99}']
    function GetIMSSDatosBim(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                             EmpleadoNumero: Integer; const Patron: WideString): OleVariant; dispid 1;
    function GetMovIMSSDatosBim(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                                EmpleadoNumero: Integer; const Patron: WideString): OleVariant; dispid 2;
    function GetIMSSDatosMen(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                             EmpleadoNumero: Integer; const Patron: WideString): OleVariant; dispid 3;
    function GetMovIMSSDatosMen(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                                EmpleadoNumero: Integer; const Patron: WideString): OleVariant; dispid 4;
    function GetIMSSDatosTot(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                             const Patron: WideString): OleVariant; dispid 5;
    function GetIMSSHisMen(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                           const Patron: WideString): OleVariant; dispid 6;
    function GetMovIMSSHisMen(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                              const Patron: WideString): OleVariant; dispid 7;
    function GetIMSSHisBim(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                           const Patron: WideString): OleVariant; dispid 8;
    function GetMovIMSSHisBim(Empresa: OleVariant; Year: Integer; Mes: Integer; Tipo: Integer; 
                              const Patron: WideString): OleVariant; dispid 9;
    function GrabaIMSSDatosTot(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 11;
    function CalcularTasaINFONAVIT(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 10;
    function CalcularPagosIMSS(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 12;
    function CalcularRecargosIMSS(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 13;
    function RevisarSUA(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 14;
    function ExportarSUA(Empresa: OleVariant; Parametros: OleVariant; out Empleados: OleVariant; 
                         out Movimientos: OleVariant; out Infonavit: OleVariant; 
                         out Incapacidades: OleVariant): OleVariant; dispid 15;
    function GetPrimaRiesgo(Empresa: OleVariant; Fecha: TDateTime; const Patron: WideString): Double; dispid 16;
    function CalcularPrimaRiesgo(Empresa: OleVariant; Parametros: OleVariant; out Datos: OleVariant): OleVariant; dispid 17;
    function GetDatosConciliaInfo(Empresa: OleVariant; ImssYear: Integer; ImssMes: Integer; 
                                  Empleado: Integer): OleVariant; dispid 301;
    function AjusteRetInfonavit(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 304;
    function AjusteRetInfonavitGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 305;
    function AjusteRetInfonavitLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 306;
    function ValidarMovimientosIDSE(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 302;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerImss provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerImss exposed by              
// the CoClass dmServerImss. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerImss = class
    class function Create: IdmServerImss;
    class function CreateRemote(const MachineName: string): IdmServerImss;
  end;

implementation

uses ComObj;

class function CodmServerImss.Create: IdmServerImss;
begin
  Result := CreateComObject(CLASS_dmServerImss) as IdmServerImss;
end;

class function CodmServerImss.CreateRemote(const MachineName: string): IdmServerImss;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerImss) as IdmServerImss;
end;

end.
