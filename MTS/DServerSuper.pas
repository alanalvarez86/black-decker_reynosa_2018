unit DServerSuper;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DServerSuper.pas                           ::
  :: Descripci�n: Programa principal de Super.dll            ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$define QUINCENALES}
{$define CAMBIO_TNOM}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ComServ,
     ComObj, VCLCom, StdVcl, {BdeMts,} DataBkr, DBClient, Db, MtsRdm, Mtx, Provider,
     Variants,
{$ifndef DOS_CAPAS}
     Super_TLB,
{$endif}
     DZetaServerProvider,
     DTarjeta,
     ZetaServerDataSet,
     ZAgenteSQL,
     ZCreator,
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZetaCommonClasses;

type
  eTipoSuper = ( eMisEmpleados, eDatosEmpleado, eSupervisores, eMisAsignados,
                 eBorraAsigna, eAgregaAsigna, eExisteAsigna, eHisKardex, eTarjeta,
                 eChecadas, eAgregaChecadas, eGrabaAutorizaciones, eBorraChecadas,
                 eGridAsistencia, eAgregaAsistencia, eModificaAsistencia, ePermIncaVaca,
                 eInvestigaStatusTarjeta, eAutorizaPrenomina, eHisGrupos, eHisCursos,eGridTarjetasPeriodo,
                 eEmpleadoFechaIng_Baja,eListaSupervisor {$ifdef COMMSCOPE}, eEvaluacionDiaria, eInicializaEvalDiaria {$endif} );
  eTablaSuper = ( eAsigna, eAusencia, eTablaChecadas, eTablaNomina, eSesion, eVistaCursos {$ifdef COMMSCOPE}, eEvaluacionDiariaGrabar {$endif} );

type
  TdmServerSuper = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerSuper {$endif} )
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
{$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
{$endif}
    FDataSet, FDatasetAdic, FDataSetAsig : TZetaCursor;
    FOldChecadas, FNewChecadas : TStringList;
    FVarChecadas : OleVariant;
    FTarjeta: TTarjeta;
    FCambioAplicado : Boolean;
    {$ifdef BITACORA_DLLS}
    FListaLog : TAsciiLog;
    FPaletita : string;
    FArchivoLog: string;
    FEmpresa:string;
    {$endif}
    procedure InitCreator;
    procedure InitTarjeta;
    procedure InitBitacoraChecadas;
    procedure ClearTarjeta;
    procedure ClearBitacoraChecadas;
    procedure InitRitmos;
    procedure InitQueries;
    function GetScript(const eTipo: eTipoSuper ): String;
    {
    function FormateaHora(const sHora: String): String;
    }
    function GetFiltroEmpleados: String;
    function GrabaListaAsistenciaParametros: String;
    function AjusteColectivoBuildDataSet: OleVariant;
    procedure SetTablaInfo(const eTipo: eTablaSuper);
    procedure SetFoto(const lEnabled: Boolean; var sFoto, sFotoJoin: String);
    procedure BeforeUpdateAsignaciones(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateAusencia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure AfterUpdateAusencia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure AfterUpdateAsistenciaGlobal(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure BeforeUpdateAsistenciaGlobal(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure UpdateErrorAsistenciaGlobal(Sender: TObject; DataSet: TzProviderClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind; var Response: TResolverResponse);
    procedure SetInfoTarjetaGlobal(FDestino: TZetaCursor; FOrigen: TzProviderClientDataSet);
    procedure BitacoraTarjeta( DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; const iEmpleado: Integer; const dFecha: TDate);
    procedure IncluirBitacora(const eClase: eClaseBitacora; const iEmpleado: Integer; const dFecha: TDate; const sMensaje: TBitacoraTexto; const sTexto: String);
    procedure BitacoraAutorizacion( const eClase: eClaseBitacora; const iEmpleado: Integer; const dFecha: TDate; const sMensaje: String; const ValorAnt, ValorNew: Currency; const sMotivo: String );
    procedure BitacoraCambioDetalle(const eClase: eClaseBitacora; const iEmpleado: Integer;const dFecha: TDate; const sOld, sNew, sDescripcion: String);
    procedure BitacoraCambio(const eClase: eClaseBitacora; const iEmpleado: Integer; const dFecha: TDate; const sMensaje, sOld, sNew: String);
    procedure BitacoraChecadas(const eClase: eClaseBitacora; const iEmpleado: Integer; const dFecha: TDate; const sMensaje: String);
    procedure SetCondiciones(var sCondicion, sFiltro: String);
    procedure BitacoraAgregar( const eClase: eClaseBitacora; UpdateKind: TUpdateKind; const iEmpleado: Integer; const dFecha: TDate; const sDescripcion: String);
    function TarjetasPeriodoBuildDataSet: OleVariant;
    function GrabarListaTarjetasAsistencia(const lGlobal:Boolean ;const oDelta:OleVariant; var ErrorCount: Integer ):OleVariant;
    procedure BeforeUpdateAsistenciaPeriodo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure InitLog(Empresa:Olevariant; const sPaletita: string);
    procedure EndLog;
    procedure AfterUpdateAutorizacionPreNomina(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
		 function GetFechaEvaluacionDiaria(Empresa: OleVariant): TDateTime;
      safecall;

{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetTarjeta(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; out Checadas: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaListaAsistencia(Empresa, oDelta, Parametros: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaTarjeta(Empresa, oDelta: OleVariant; const OldChecadas: WideString; VarChecadas: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpleados(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetHisKardex(Empresa: OleVariant; Empleado: Integer; out Otros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMisAsignaciones(Empresa: OleVariant; Fecha: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetListaSupervisores(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaAsignaciones(Empresa, oDelta: OleVariant; ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosEmpleado(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; ObtenerFoto: WordBool; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosEmpleadoAnterior(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; ObtenerFoto: WordBool; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosEmpleadoSiguiente(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; ObtenerFoto: WordBool;  out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AjusteColectivoGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetAutorizaPrenomina(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaAutorizaPrenomina(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSesion(Empresa: OleVariant; iFolio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetHisCursos(Empresa: OleVariant; Empleado: Integer): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTarjetasPeriodo(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaTarjetaPeriodo(Empresa, Parametros: OleVariant;out iErrorCount: Integer; oDelta: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetFechaLimite(Empresa: OleVariant): TDateTime; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEvaluacionDiaria(Empresa, Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaEvaluacionDiaria(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}

  end;
var
  dmServerSuper: TdmServerSuper;

implementation

uses
     ZetaSql,
     ZetaSQLBroker,
     ZetaServerTools,
     ZetaCommonTools,
     ZGlobalTress,
     ZEvaluador,
     DQueries;

{$R *.DFM}

procedure TdmServerSuper.InitLog( Empresa:Olevariant; const sPaletita: string );
begin
     {$ifdef BITACORA_DLLS}
     FPaletita := sPaletita;
     FArchivoLog := 'c:\BitacoraTress '+ FormatDateTime('dd_mmm_yy', Date()) +'.txt';

     if FListaLog = NIL then
     begin
          try
             FListaLog :=  TAsciiLog.Create;
             FListaLog.Init(FArchivoLog);
             try
                if NOT varisNull( Empresa ) then
                   try
                      FEmpresa :=  ' -- Alias: ' + Empresa[P_CODIGO] +
                                   ' Usuario: ' + InTToStr(Empresa[P_USUARIO])
                   except
                         FEmpresa := ' Error al leer Empresa[]';
                   end
                else
                    FEmpresa := '';
             except
                   FEmpresa := ' Error al leer Empresa[]';
             end;

             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) +
                                   ' DServerSuper :: INICIA :: '+  FPaletita +
                                   FEmpresa );
             FListaLog.Close;
          except
          end;
     end;
     {$endif}
end;

procedure TdmServerSuper.EndLog;
begin
     {$ifdef BITACORA_DLLS}
     try
        if FArchivoLog= '' then
           FArchivoLog := 'c:\BitacoraTress '+ FormatDateTime('dd_mmm_yy', Date()) +'.txt';
        if FListaLog = NIL then
        begin
             FListaLog :=  TAsciiLog.Create;
             FListaLog.Init(FArchivoLog);
             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) + ' DServerSuper :: TERMINA :: '+ FPaletita +' '+ FEmpresa);
        end
        else
        begin
             //FListaLog.Open(FArchivoLog);
             FListaLog.Init(FArchivoLog);
             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',NOw) + ' DServerSuper :: TERMINA :: '+ FPaletita +' '+ FEmpresa  );
             //FListaLog.Close;
        end;
        FListaLog.Close;
        FreeAndNil(FListaLog);
     except

     end;
     {$endif}
end;

class procedure TdmServerSuper.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerSuper.MtsDataModuleCreate(Sender: TObject);
begin
     {$ifdef BITACORA_DLLS}
     FListaLog := Nil;
     {$endif}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerSuper.MtsDataModuleDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerSuper.CierraEmpresa;
begin
end;
{$endif}

procedure TdmServerSuper.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerSuper.InitTarjeta;
begin
     InitCreator;
     InitQueries;
     FTarjeta := TTarjeta.Create( oZetaCreator );
end;

procedure TdmServerSuper.ClearTarjeta;
begin
     FreeAndNil( FTarjeta );
end;

procedure TdmServerSuper.InitRitmos;
begin
     InitCreator;
     InitQueries;
     oZetaCreator.PreparaRitmos;
end;

procedure TdmServerSuper.InitQueries;
begin
     InitCreator;
     oZetaCreator.PreparaQueries;
end;

procedure TdmServerSuper.InitBitacoraChecadas;
begin
     if ( not Assigned( FOldChecadas ) ) then
        FOldChecadas := TStringList.Create
     else
        FOldChecadas.Clear;
     if ( not Assigned( FNewChecadas ) ) then
        FNewChecadas := TStringList.Create
     else
        FNewChecadas.Clear;
end;

procedure TdmServerSuper.ClearBitacoraChecadas;
begin
     FreeAndNil( FOldChecadas );
     FreeAndNil( FNewChecadas );
end;

function TdmServerSuper.GetScript( const eTipo: eTipoSuper ): String;
begin
     Result := '';
     case eTipo of
          eMisEmpleados : Result := 'select LISTA.CB_CODIGO, LISTA.CB_NIVEL, LISTA.CB_TIPO, ' + K_PRETTYNAME +
                                    ' as PrettyName, %s CB_AREA, COLABORA.CB_PUESTO,' +
                                    '( select count(*) from CHECADAS where CHECADAS.CB_CODIGO = AUSENCIA.CB_CODIGO and CHECADAS.AU_FECHA = AUSENCIA.AU_FECHA and CHECADAS.CH_TIPO in (1,2) ) CHECADAS ' +
                                    ',(select dbo.SP_STATUS_EMP(''%s'',LISTA.CB_CODIGO)) STATUS'+
                                    ',(select count(*)  from CHECADAS where CHECADAS.CB_CODIGO = AUSENCIA.CB_CODIGO and '+
                                    'CHECADAS.AU_FECHA = AUSENCIA.AU_FECHA and CHECADAS.CH_TIPO = 1 and CHECADAS.CH_DESCRIP = 2 ) RETARDOS  '+
                                    'from SP_LISTA_EMPLEADOS( ''%s'', %d ) LISTA ' +
                                    'left outer join COLABORA on ( COLABORA.CB_CODIGO = LISTA.CB_CODIGO ) ' +
                                    'left outer join AUSENCIA on ( AUSENCIA.CB_CODIGO = LISTA.CB_CODIGO AND AUSENCIA.AU_FECHA = ''%s'' ) %s' ;
          eSupervisores : Result := 'select B.TB_CODIGO MI_CODIGO, A.TB_CODIGO, A.TB_ELEMENT,A.TB_ACTIVO  ' +
                                    'from   NIVEL%s A ' +
                                    '       left outer join NIVEL%s B on B.TB_CODIGO = A.TB_CODIGO and ' +
                                    '       B.TB_CODIGO in ( select CB_NIVEL from SUPER where US_CODIGO = %d )';
          eMisAsignados : Result := 'select A.CB_CODIGO, A.CB_NIVEL, A.US_CODIGO, ' + K_PRETTYNAME + ' as PRETTYNAME ' +
                                    'from   SP_LISTA_ASIGNA( ''%s'', %d ) A ' +
                                    '       join COLABORA B on A.CB_CODIGO = B.CB_CODIGO %s' +
                                    'order by A.CB_NIVEL, A.CB_CODIGO ';
          eBorraAsigna :  Result := 'delete from ASIGNA where CB_CODIGO = :Empleado and AS_FECHA = :Fecha';
          eAgregaAsigna : Result := 'insert into ASIGNA ( CB_CODIGO, AS_FECHA, CB_NIVEL, US_CODIGO ) ' +
                                    '       values ( :CB_CODIGO, :AS_FECHA, :CB_NIVEL, :US_CODIGO )';
          eHisKardex :    Result := 'select CB_CODIGO, CB_FECHA, CB_TIPO, CB_COMENTA, CB_MONTO, CB_FEC_CAP, CB_GLOBAL, '+
                                    '       CB_NIVEL, CB_STATUS, CB_NOTA, CB_PUESTO, CB_TURNO, CB_CLASIFI, CB_NIVEL1, ' +
                                    '       CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, ' +
                                    '       CB_NIVEL9,'+{$ifdef ACS}'CB_NIVEL10,CB_NIVEL11,CB_NIVEL12,'+{$endif}' CB_REINGRE, US_CODIGO, CB_RANGO_S '+
                                    'from   KARDEX '+
                                    'where  ( CB_CODIGO = %d ) and ( CB_TIPO not in (''CAMBIO'', ''PRESTA'', ''EVENTO'', ''BAJA'', ''ALTA'', ''RENOVA'' ) )';
          eTarjeta :      Result := 'select A.CB_CODIGO,A.AU_FECHA,A.AU_DES_TRA,A.AU_DOBLES,A.AU_EXTRAS,A.AU_HORAS,A.AU_PER_CG,'+
                                    '       A.AU_PER_SG,A.AU_TARDES,A.AU_TIPO,A.AU_TIPODIA,A.CB_CLASIFI,A.CB_TURNO,A.CB_PUESTO,'+
                                    '       A.HO_CODIGO,A.CB_NIVEL1,A.CB_NIVEL2,A.CB_NIVEL3,A.CB_NIVEL4,A.CB_NIVEL5,A.CB_NIVEL6,'+
                                    '       A.CB_NIVEL7,A.CB_NIVEL8,A.CB_NIVEL9,'+{$ifdef ACS}'A.CB_NIVEL10,A.CB_NIVEL11,A.CB_NIVEL12,'+{$endif}' A.AU_AUT_EXT,A.AU_AUT_TRA,A.AU_HOR_MAN,A.AU_STATUS,'+
                                    '       A.AU_NUM_EXT,A.AU_TRIPLES,A.US_CODIGO,A.AU_POSICIO,A.AU_HORASCK,A.PE_YEAR,A.PE_TIPO,A.AU_OUT2EAT,A.AU_STA_FES,A.AU_PRE_EXT,'+
                                    '       A.PE_NUMERO,A.HO_CODIGO HORARIO_ANT, A.AU_STATUS STATUS_ANT, ''N'' SETCAMBIOS, A.AU_OUT2EAT COMIDA_ANT, A.AU_STA_FES FESTIVO_ANT, '+
{$ifdef QUINCENALES}

                                    '       A.CB_SALARIO, ' +
{$endif}

                                    '       A.CB_NOMINA, ' +
{$ifdef INTERBASE}
                                    '       ( select RESULTADO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 7 ) ) HRS_EXTRAS,'+
                                    '       ( select MOTIVO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 7 ) ) M_HRS_EXTRAS,'+
                                    '       ( select US_COD_OK from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 7 ) ) OK_HRS_EXTRAS,'+

                                    '       ( select RESULTADO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 8 ) ) DESCANSO,'+
                                    '       ( select MOTIVO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 8 ) ) M_DESCANSO,'+
                                    '       ( select US_COD_OK from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 8 ) ) OK_DESCANSO,'+

                                    '       ( select RESULTADO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 5 ) ) PER_CG,'+
                                    '       ( select MOTIVO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 5 ) ) M_PER_CG,'+
                                    '       ( select US_COD_OK from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 5 ) ) OK_PER_CG,'+

                                    '       ( select RESULTADO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 6 ) ) PER_SG,'+
                                    '       ( select MOTIVO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 6 ) ) M_PER_SG,'+
                                    '       ( select US_COD_OK from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 6 ) ) OK_PER_SG,'+

                                    '       ( select RESULTADO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 9 ) ) PER_CG_ENT,'+
                                    '       ( select MOTIVO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 9 ) ) M_PER_CG_ENT,'+
                                    '       ( select US_COD_OK from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 9 ) ) OK_PER_CG_ENT,'+

                                    '       ( select RESULTADO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 10 ) ) PER_SG_ENT,'+
                                    '       ( select MOTIVO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 10 ) ) M_PER_SG_ENT,'+
                                    '       ( select US_COD_OK from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 10 ) ) OK_PER_SG_ENT,'+

                                    '       ( select RESULTADO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 11 ) ) PRE_FUERA_JOR,'+
                                    '       ( select MOTIVO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 11 ) ) M_PRE_FUERA_JOR,'+
                                    '       ( select US_COD_OK from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 11 ) ) OK_PRE_FUERA_JOR,'+

                                    '       ( select RESULTADO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 12 ) ) PRE_DENTRO_JOR,'+
                                    '       ( select MOTIVO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 12) ) M_PRE_DENTRO_JOR,'+
                                    '       ( select US_COD_OK from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, 12) ) OK_PRE_DENTRO_JOR,'+


{$endif}
{$ifdef MSSQL}
                                    '       ( dbo.AUTORIZACIONES_SELECT_HORAS( A.CB_CODIGO, A.AU_FECHA, 7 ) ) HRS_EXTRAS,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_MOTIVO( A.CB_CODIGO, A.AU_FECHA, 7 ) ) M_HRS_EXTRAS,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_APROBO( A.CB_CODIGO, A.AU_FECHA, 7 ) ) OK_HRS_EXTRAS,'+

                                    '       ( dbo.AUTORIZACIONES_SELECT_HORAS( A.CB_CODIGO, A.AU_FECHA, 8 ) ) DESCANSO,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_MOTIVO( A.CB_CODIGO, A.AU_FECHA, 8 ) ) M_DESCANSO,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_APROBO( A.CB_CODIGO, A.AU_FECHA, 8 ) ) OK_DESCANSO,'+

                                    '       ( dbo.AUTORIZACIONES_SELECT_HORAS( A.CB_CODIGO, A.AU_FECHA, 5 ) ) PER_CG,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_MOTIVO( A.CB_CODIGO, A.AU_FECHA, 5 ) ) M_PER_CG,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_APROBO( A.CB_CODIGO, A.AU_FECHA, 5 ) ) OK_PER_CG,'+

                                    '       ( dbo.AUTORIZACIONES_SELECT_HORAS( A.CB_CODIGO, A.AU_FECHA, 6 ) ) PER_SG,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_MOTIVO( A.CB_CODIGO, A.AU_FECHA, 6 ) ) M_PER_SG,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_APROBO( A.CB_CODIGO, A.AU_FECHA, 6 ) ) OK_PER_SG,'+

                                    '       ( dbo.AUTORIZACIONES_SELECT_HORAS( A.CB_CODIGO, A.AU_FECHA, 9 ) ) PER_CG_ENT,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_MOTIVO( A.CB_CODIGO, A.AU_FECHA, 9 ) ) M_PER_CG_ENT,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_APROBO( A.CB_CODIGO, A.AU_FECHA, 9 ) ) OK_PER_CG_ENT,'+

                                    '       ( dbo.AUTORIZACIONES_SELECT_HORAS( A.CB_CODIGO, A.AU_FECHA, 10 ) ) PER_SG_ENT,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_MOTIVO( A.CB_CODIGO, A.AU_FECHA, 10 ) ) M_PER_SG_ENT,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_APROBO( A.CB_CODIGO, A.AU_FECHA, 10 ) ) OK_PER_SG_ENT,'+

                                    '       ( dbo.AUTORIZACIONES_SELECT_HORAS( A.CB_CODIGO, A.AU_FECHA, 11 ) ) PRE_FUERA_JOR,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_MOTIVO( A.CB_CODIGO, A.AU_FECHA, 11 ) ) M_PRE_FUERA_JOR,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_APROBO( A.CB_CODIGO, A.AU_FECHA, 11 ) ) OK_PRE_FUERA_JOR,'+

                                    '       ( dbo.AUTORIZACIONES_SELECT_HORAS( A.CB_CODIGO, A.AU_FECHA, 12 ) ) PRE_DENTRO_JOR,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_MOTIVO( A.CB_CODIGO, A.AU_FECHA, 12 ) ) M_PRE_DENTRO_JOR,'+
                                    '       ( dbo.AUTORIZACIONES_SELECT_APROBO( A.CB_CODIGO, A.AU_FECHA, 12 ) ) OK_PRE_DENTRO_JOR,'+


{$endif}
                                    '       ( select P.PE_STATUS from PERIODO P where ( P.PE_YEAR = A.PE_YEAR ) and ( P.PE_TIPO = A.PE_TIPO ) and ( P.PE_NUMERO = A.PE_NUMERO ) ) PE_STATUS '+
                                    'from   AUSENCIA A ' +
                                    'where  ( A.CB_CODIGO = %d ) and ( A.AU_FECHA = ''%s'' )';
          eChecadas :     Result := 'select CB_CODIGO,AU_FECHA,CH_H_REAL,CH_H_AJUS,CH_DESCRIP,CH_TIPO,CH_HOR_ORD,CH_HOR_EXT,'+
                                    '       CH_HOR_DES, CH_HOR_DES as CH_APRUEBA, US_CODIGO,CH_SISTEMA,CH_RELOJ,CH_GLOBAL, US_COD_OK, CH_MOTIVO '+
                                    '       from CHECADAS where ( CB_CODIGO = %d ) and ( AU_FECHA = ''%s'' ) '+
                                    'order by CH_H_REAL';
          eAgregaChecadas: Result:= 'insert into CHECADAS( CB_CODIGO,AU_FECHA,CH_H_REAL,CH_H_AJUS,CH_GLOBAL,CH_RELOJ,CH_SISTEMA,US_CODIGO, CH_MOTIVO )'+
                                    'values ( :CB_CODIGO,:AU_FECHA,:CH_H_REAL,:CH_H_AJUS,:CH_GLOBAL,:CH_RELOJ,:CH_SISTEMA,:US_CODIGO, :CH_MOTIVO)';
          eGrabaAutorizaciones:Result:='execute procedure AUTORIZACIONES_CREAR_CHECADAS( :CB_CODIGO,:AU_FECHA,'+
                                       ':AU_AUT_EXT,:M_AU_AUT_EXT,:AU_AUT_TRA,:M_AU_AUT_TRA,:AU_PER_CG,:M_AU_PER_CG,'+
                                       ':AU_PCG_ENT,:M_AU_PCG_ENT,:AU_PER_SG,:M_AU_PER_SG,:AU_PSG_ENT,:M_AU_PSG_ENT,'+
                                       ':AU_PRE_FUERA_JOR,:M_AU_PRE_FUERA_JOR,:AU_PRE_DENTRO_JOR,:M_AU_PRE_DENTRO_JOR,'+
                                       ':CH_GLOBAL,:US_CODIGO )';
          eBorraChecadas: Result := 'delete from CHECADAS where CB_CODIGO = %d and AU_FECHA = ''%s'' and CH_TIPO < 5';
          eGridAsistencia :Result:= 'select LISTA.CB_CODIGO, ' + K_PRETTYNAME + ' as PrettyName, ' +
                                    'AUSENCIA.CB_CODIGO as EXISTE, AUSENCIA.AU_FECHA, ' +
                                    'AUSENCIA.HO_CODIGO HO_CODIGO, AUSENCIA.AU_STATUS, AUSENCIA.AU_HOR_MAN, AUSENCIA.AU_HORASCK, AUSENCIA.AU_NUM_EXT, AUSENCIA.US_CODIGO, AUSENCIA.AU_OUT2EAT, AUSENCIA.AU_STA_FES,' +
                                    'AUSENCIA.HO_CODIGO HORARIO_ANT, AUSENCIA.AU_STATUS STATUS_ANT, AUSENCIA.AU_TIPO, AUSENCIA.AU_OUT2EAT COMIDA_ANT, AUSENCIA.AU_STA_FES FESTIVO_ANT, ' +
{$ifdef INTERBASE}
                                    '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 1 ) ) CHECADA1, ' +
                                    '( select MOTIVO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 1 ) ) MC_CHECADA1, ' +
                                    '( select USUARIO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 1 ) ) US_CHECADA1, ' +

                                    '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 2 ) ) CHECADA2, ' +
                                    '( select MOTIVO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 2 ) ) MC_CHECADA2, ' +
                                    '( select USUARIO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 2 ) ) US_CHECADA2, ' +

                                    '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 3 ) ) CHECADA3, ' +
                                    '( select MOTIVO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 3 ) ) MC_CHECADA3, ' +
                                    '( select USUARIO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 3 ) ) US_CHECADA3, ' +

                                    '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 4 ) ) CHECADA4, ' +
                                    '( select MOTIVO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 4 ) ) MC_CHECADA4, ' +
                                    '( select USUARIO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 4 ) ) US_CHECADA4, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) ) HRS_EXTRAS,'+
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) ) M_HRS_EXTRAS, ' +
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) ) OK_HRS_EXTRAS, ' +
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) ) AP_HRS_EXTRAS, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) ) DESCANSO, ' +
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) ) M_DESCANSO, ' +
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) ) OK_DESCANSO, ' +
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) ) AP_DESCANSO, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) ) PER_CG, ' +
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) ) M_PER_CG, ' +
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) ) OK_PER_CG, ' +
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) ) AP_PER_CG, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) ) PER_SG, ' +
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) ) M_PER_SG, ' +
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) ) OK_PER_SG, ' +
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) ) AP_PER_SG, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) ) PER_CG_ENT, ' +
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) ) M_PER_CG_ENT, ' +
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) ) OK_PER_CG_ENT, ' +
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) ) AP_PER_CG_ENT, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) ) PER_SG_ENT, ' +
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) ) M_PER_SG_ENT, ' +
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) ) OK_PER_SG_ENT, ' +
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) ) AP_PER_SG_ENT, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 11 ) ) PRE_FUERA_JOR,'+
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 11 ) ) M_PRE_FUERA_JOR,'+
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 11) ) OK_PRE_FUERA_JOR,'+
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 11) ) AP_PRE_FUERA_JOR,'+

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 ) ) PRE_DENTRO_JOR,'+
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 ) ) M_PRE_DENTRO_JOR,'+
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 ) ) OK_PRE_DENTRO_JOR,'+
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 )) AP_PRE_DENTRO_JOR,'+
{$endif}
{$ifdef MSSQL}
                                    'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 1 ) CHECADA1, ' +
                                    'dbo.SP_CHECADA_MOT( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 1 ) MC_CHECADA1, '+
                                    'dbo.SP_CHECADA_USUARIO( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 1 ) US_CHECADA1, '+

                                    'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 2 ) CHECADA2, ' +
                                    'dbo.SP_CHECADA_MOT( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 2 ) MC_CHECADA2, '+
                                    'dbo.SP_CHECADA_USUARIO( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 2 ) US_CHECADA2, '+

                                    'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 3 ) CHECADA3, ' +
                                    'dbo.SP_CHECADA_MOT( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 3 ) MC_CHECADA3, '+
                                    'dbo.SP_CHECADA_USUARIO( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 3 ) US_CHECADA3, '+

                                    'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 4 ) CHECADA4, ' +
                                    'dbo.SP_CHECADA_MOT( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 4 ) MC_CHECADA4, '+
                                    'dbo.SP_CHECADA_USUARIO( AUSENCIA.AU_FECHA, LISTA.CB_CODIGO, 4 ) US_CHECADA4, '+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) HRS_EXTRAS,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) M_HRS_EXTRAS,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) OK_HRS_EXTRAS,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) AP_HRS_EXTRAS,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) DESCANSO,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) M_DESCANSO,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) OK_DESCANSO,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) AP_DESCANSO,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) PER_CG,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) M_PER_CG,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) OK_PER_CG,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) AP_PER_CG,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) PER_SG,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) M_PER_SG,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) OK_PER_SG,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) AP_PER_SG,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) PER_CG_ENT,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) M_PER_CG_ENT,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) OK_PER_CG_ENT,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) AP_PER_CG_ENT,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) PER_SG_ENT,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) M_PER_SG_ENT,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) OK_PER_SG_ENT,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) AP_PER_SG_ENT,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 11 ) PRE_FUERA_JOR,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 11 ) M_PRE_FUERA_JOR,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 11 ) OK_PRE_FUERA_JOR,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 11 ) AP_PRE_FUERA_JOR,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 )  PRE_DENTRO_JOR,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 )  M_PRE_DENTRO_JOR,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 )  OK_PRE_DENTRO_JOR,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( LISTA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 ) AP_PRE_DENTRO_JOR,'+
{$endif}
                                    '( select PERIODO.PE_STATUS from PERIODO where ( PERIODO.PE_YEAR = AUSENCIA.PE_YEAR ) and ' +
                                    '( PERIODO.PE_TIPO = AUSENCIA.PE_TIPO ) and ( PERIODO.PE_NUMERO = AUSENCIA.PE_NUMERO ) ) PE_STATUS ' +
                                    'from SP_LISTA_EMPLEADOS(  ''%s'', %d ) LISTA ' +
                                    'left join AUSENCIA on ( AUSENCIA.CB_CODIGO = LISTA.CB_CODIGO ) and ( AUSENCIA.AU_FECHA = ''%s'' ) ' +
                                    'join COLABORA on ( COLABORA.CB_CODIGO = LISTA.CB_CODIGO ) %s ' +
                                    'order by LISTA.CB_CODIGO';

          eAgregaAsistencia : Result := 'execute procedure ASISTENCIA_AGREGA( :CB_CODIGO,:AU_FECHA,:HO_CODIGO,:AU_HOR_MAN,' +
                                        ':AU_STATUS,:HRS_EXTRAS,:M_HRS_EXTRAS,:DESCANSO,:M_DESCANSO,:PER_CG,:M_PER_CG,:PER_CG_ENT,' +
                                        ':M_PER_CG_ENT,:PER_SG,:M_PER_SG,:PER_SG_ENT,:M_PER_SG_ENT, :PRE_FUERA_JOR,:M_PRE_FUERA_JOR,'+
                                        ':PRE_DENTRO_JOR,:M_PRE_DENTRO_JOR,:CHECADA1,:CHECADA2,:CHECADA3,' +
                                        ':CHECADA4,:MC_CHECADA1,:MC_CHECADA2,:MC_CHECADA3,:MC_CHECADA4,:US_CODIGO,:AU_OUT2EAT )';
          eModificaAsistencia:Result := 'execute procedure ASISTENCIA_MODIFICA( :CB_CODIGO,:AU_FECHA,:HO_CODIGO,:AU_HOR_MAN,' +
                                        ':AU_STATUS,:HRS_EXTRAS,:M_HRS_EXTRAS,:DESCANSO,:M_DESCANSO,:PER_CG,:M_PER_CG,:PER_CG_ENT,' +
                                        ':M_PER_CG_ENT,:PER_SG,:M_PER_SG,:PER_SG_ENT,:M_PER_SG_ENT,:PRE_FUERA_JOR,:M_PRE_FUERA_JOR,' +
                                        ':PRE_DENTRO_JOR,:M_PRE_DENTRO_JOR,'+
                                        ':CHECADA1,:CHECADA2,:CHECADA3,' +
                                        ':CHECADA4,:MC_CHECADA1,:MC_CHECADA2,:MC_CHECADA3,:MC_CHECADA4,:US_CODIGO,:AU_OUT2EAT )';
          ePermIncaVaca : Result := 'select CB_CODIGO, VA_FEC_INI CB_FECHA, CAST( ''VACA'' as CHAR(6)) CB_TIPO,'+
                                    '       VA_COMENTA as CB_COMENTA, VA_GOZO CB_MONTO, US_CODIGO '+
                                    'from   VACACION '+
                                    'where  ( CB_CODIGO = %d ) and ( VA_TIPO = 1 ) '+
                                    'union all '+
                                    'select CB_CODIGO, IN_FEC_INI, CAST( ''INCA'' as CHAR(6)),'+
                                    '       CAST( ''Incapacidad ['' ' + K_CONCATENA + ' IN_TIPO ' + K_CONCATENA + '''] '' ' + K_CONCATENA + ' IN_COMENTA as VARCHAR(100)),'+
                                    '       CAST( IN_DIAS as NUMERIC(15,2)), US_CODIGO '+
                                    'from   INCAPACI '+
                                    'where  ( CB_CODIGO = %d ) '+
                                    'union all '+
                                    'select CB_CODIGO, PM_FEC_INI, CAST( ''PERM'' as CHAR(6)),'+
                                    '       CAST( ''Permiso ['' ' + K_CONCATENA + ' PM_TIPO ' + K_CONCATENA + '''] '' ' + K_CONCATENA + ' PM_COMENTA as VARCHAR(100)),'+
                                    '       CAST( PM_DIAS as NUMERIC(15,2)), US_CODIGO '+
                                    'from   PERMISO '+
                                    'where ( CB_CODIGO = %d )';
{$ifdef INTERBASE}
          eDatosEmpleado :Result := 'select B.CB_CODIGO, ' + K_PRETTYNAME + ' as PRETTYNAME,'+
                                    '       B.CB_APE_PAT, B.CB_APE_MAT, B.CB_NOMBRES, B.CB_ACTIVO, B.CB_FEC_ING, B.CB_FEC_BAJ, B.CB_AREA, '+
                                    '       A.CB_AUTOSAL, A.CB_CLASIFI, A.CB_CONTRAT, A.CB_TURNO, A.CB_PUESTO, '+
                                    '       A.CB_NIVEL1, A.CB_NIVEL2, A.CB_NIVEL3, A.CB_NIVEL4, A.CB_NIVEL5, A.CB_NIVEL6,'+
                                    '       A.CB_NIVEL7, A.CB_NIVEL8, A.CB_NIVEL9,'+{$ifdef ACS}'A.CB_NIVEL10,A.CB_NIVEL11,A.CB_NIVEL12,'+{$endif}' A.CB_TABLASS, A.CB_PER_VAR, A.CB_ZONA_GE,'+
                                    '       A.CB_FEC_ANT, A.CB_SALARIO, A.CB_RANGO_S, C.TB_ELEMENT CONTRATO, %1:s as FOTOGRAFIA, CB_FEC_COV '+ {OP: 17-07-08}
                                    'from   SP_FECHA_KARDEX( ''%0:s'', %3:s ) A '+
                                    '       join COLABORA B on B.CB_CODIGO = %3:s ' +
                                    '       left outer join CONTRATO C on C.TB_CODIGO = A.CB_CONTRAT %2:s %4:s';
          eExisteAsigna : Result := 'select CB_NIVEL%s from SP_FECHA_KARDEX( :Fecha, :Empleado )';
{$endif}
{$ifdef MSSQL}
          eDatosEmpleado :Result := 'select B.CB_CODIGO, B.PRETTYNAME, B.CB_APE_PAT, B.CB_APE_MAT,' +
                                    'B.CB_NOMBRES, B.CB_ACTIVO, B.CB_FEC_ING, B.CB_FEC_BAJ,B.CB_AREA,' +
                                    'dbo.SP_KARDEX_CB_AUTOSAL( ''%0:s'', B.CB_CODIGO ) CB_AUTOSAL,' +
                                    'dbo.SP_KARDEX_CB_CLASIFI( ''%0:s'', B.CB_CODIGO ) CB_CLASIFI,' +
                                    'dbo.SP_KARDEX_CB_CONTRAT( ''%0:s'', B.CB_CODIGO ) CB_CONTRAT,' +
                                    'dbo.SP_KARDEX_CB_TURNO( ''%0:s'', B.CB_CODIGO ) CB_TURNO,' +
                                    'dbo.SP_KARDEX_CB_PUESTO( ''%0:s'', B.CB_CODIGO ) CB_PUESTO,' +
                                    'dbo.SP_KARDEX_CB_NIVEL1( ''%0:s'', B.CB_CODIGO ) CB_NIVEL1,' +
                                    'dbo.SP_KARDEX_CB_NIVEL2( ''%0:s'', B.CB_CODIGO ) CB_NIVEL2,' +
                                    'dbo.SP_KARDEX_CB_NIVEL3( ''%0:s'', B.CB_CODIGO ) CB_NIVEL3,' +
                                    'dbo.SP_KARDEX_CB_NIVEL4( ''%0:s'', B.CB_CODIGO ) CB_NIVEL4,' +
                                    'dbo.SP_KARDEX_CB_NIVEL5( ''%0:s'', B.CB_CODIGO ) CB_NIVEL5,' +
                                    'dbo.SP_KARDEX_CB_NIVEL6( ''%0:s'', B.CB_CODIGO ) CB_NIVEL6,' +
                                    'dbo.SP_KARDEX_CB_NIVEL7( ''%0:s'', B.CB_CODIGO ) CB_NIVEL7,' +
                                    'dbo.SP_KARDEX_CB_NIVEL8( ''%0:s'', B.CB_CODIGO ) CB_NIVEL8,' +
                                    'dbo.SP_KARDEX_CB_NIVEL9( ''%0:s'', B.CB_CODIGO ) CB_NIVEL9,' +
                                    {$ifdef ACS}
                                    'dbo.SP_KARDEX_CB_NIVEL10( ''%0:s'', B.CB_CODIGO ) CB_NIVEL10,' +
                                    'dbo.SP_KARDEX_CB_NIVEL11( ''%0:s'', B.CB_CODIGO ) CB_NIVEL11,' +
                                    'dbo.SP_KARDEX_CB_NIVEL12( ''%0:s'', B.CB_CODIGO ) CB_NIVEL12,' +
                                    {$endif}
                                    'dbo.SP_KARDEX_CB_TABLASS( ''%0:s'', B.CB_CODIGO ) CB_TABLASS,' +
                                    'dbo.SP_KARDEX_CB_PER_VAR( ''%0:s'', B.CB_CODIGO ) CB_PER_VAR,' +
                                    'dbo.SP_KARDEX_CB_ZONA_GE( ''%0:s'', B.CB_CODIGO ) CB_ZONA_GE,' +
                                    'dbo.SP_KARDEX_CB_FEC_ANT( ''%0:s'', B.CB_CODIGO ) CB_FEC_ANT,' +
                                    'dbo.SP_KARDEX_CB_SALARIO( ''%0:s'', B.CB_CODIGO ) CB_SALARIO,' +
                                    'dbo.SP_KARDEX_CB_RANGO_S( ''%0:s'', B.CB_CODIGO ) CB_RANGO_S,' +
                                    'C.TB_ELEMENT CONTRATO, %1:s as FOTOGRAFIA , CB_FEC_COV '+ {OP: 17-07-08}
                                    'from COLABORA B ' +
                                    'left outer join CONTRATO C on C.TB_CODIGO = dbo.SP_KARDEX_CB_CONTRAT( ''%0:s'', B.CB_CODIGO ) %2:s ' +
                                    'where B.CB_CODIGO = %3:s %4:s';
          eExisteAsigna : Result := 'select dbo.SP_KARDEX_CB_NIVEL%s( :Fecha, :Empleado ) CB_NIVEL%0:s';
{$endif}
          eInvestigaStatusTarjeta:
{$ifdef INTERBASE}
                 Result := 'Select RESULTADO from SP_INVESTIGA_STATUS( :Empleado, :Fecha )';
{$else}
                 Result := 'Select RESULTADO = dbo.SP_INVESTIGA_STATUS( :Empleado, :Fecha )';
{$endif}
        eAutorizaPrenomina: Result := 'select LISTA.CB_CODIGO, ' + K_PRETTYNAME + ' as PrettyName, NOMINA.NO_HORAS, '+
                                          'NOMINA.NO_DOBLES, NOMINA.NO_TRIPLES, NOMINA.NO_SUP_OK, NOMINA.NO_HORA_CG, '+
                                          'NOMINA.NO_HORA_SG, NOMINA.NO_DIAS_IN, NOMINA.NO_DIAS_VA, NOMINA.NO_FEC_OK, '+
                                          'NOMINA.NO_HOR_OK, NOMINA.PE_YEAR, NOMINA.PE_TIPO, NOMINA.PE_NUMERO, NOMINA.NO_SUP_OK as ULT_SUPER,(NOMINA.NO_DES_TRA + NOMINA.NO_FES_TRA)AS NO_DES_TRA '+
                                          'from SP_LISTA_EMPLEADOS( %s, %d ) LISTA '+
                                          'left outer join COLABORA on ( COLABORA.CB_CODIGO = LISTA.CB_CODIGO ) '+
                                          'left outer join NOMINA on ( NOMINA.CB_CODIGO = LISTA.CB_CODIGO ) where '+
                                          'NOMINA.PE_YEAR = %d and NOMINA.PE_TIPO = %d and NOMINA.PE_NUMERO = %d';

        {eHisGrupos: Result:= 'select SESION.SE_FOLIO, SESION.CU_CODIGO, SESION.MA_CODIGO, SESION.SE_LUGAR, SESION.SE_FEC_INI, SESION.SE_FEC_FIN,' +
                             'SESION.SE_HOR_INI, SESION.SE_HOR_FIN, SESION.SE_HORAS, SESION.SE_CUPO, SESION.SE_COMENTA,' +
                             'SESION.SE_REVISIO, SESION.SE_COSTO1, SESION.SE_COSTO2, SESION.SE_COSTO3, '+
                             '( select count(*) from KARCURSO where SE_FOLIO = SESION.SE_FOLIO ) as SE_APROBADO,' +
                             '( select count(*) from INSCRITO where SE_FOLIO = SESION.SE_FOLIO ) as SE_INSCRITO'+
                             ' FROM SESION '+
                             ' left outer join KARCURSO ON SESION.SE_FOLIO = KARCURSO.SE_FOLIO '+
                             'where KARCURSO.CB_CODIGO = %d'; }

       eHisCursos: Result:= 'SELECT KARCURSO.CB_CODIGO,KARCURSO.CU_CODIGO,' +
                             'KARCURSO.CB_CLASIFI,KARCURSO.CB_TURNO,'+
                             'KARCURSO.KC_EVALUA,KARCURSO.KC_FEC_TOM,'+
                             'KARCURSO.KC_FEC_FIN,KARCURSO.KC_HORAS,'+
                             'KARCURSO.CB_PUESTO,KARCURSO.CB_NIVEL1,'+
                              'KARCURSO.CB_NIVEL2,KARCURSO.CB_NIVEL3,KARCURSO.CB_NIVEL4,'+
                              'KARCURSO.CB_NIVEL5,KARCURSO.CB_NIVEL6,KARCURSO.CB_NIVEL7,KARCURSO.CB_NIVEL8,KARCURSO.CB_NIVEL9,'+{$ifdef ACS}'KARCURSO.CB_NIVEL10,KARCURSO.CB_NIVEL11,KARCURSO.CB_NIVEL12,'+{$endif}'KARCURSO.MA_CODIGO,KARCURSO.KC_REVISIO,'+
                              'KARCURSO.SE_FOLIO,KARCURSO.KC_EST '+
                              'FROM KARCURSO '+
                              'JOIN V_CURSO ON ( KARCURSO.CU_CODIGO = V_CURSO.CU_CODIGO ) '+
                              'WHERE ( ( KARCURSO.CB_CODIGO = %d ) )';

       eGridTarjetasPeriodo :Result:= 'select  AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, ' +
                                    'AUSENCIA.HO_CODIGO HO_CODIGO, AUSENCIA.AU_STATUS, AUSENCIA.AU_HOR_MAN, AUSENCIA.AU_HORASCK, AUSENCIA.AU_NUM_EXT, AUSENCIA.US_CODIGO, AUSENCIA.AU_OUT2EAT, AUSENCIA.AU_STA_FES,' +
                                    'AUSENCIA.HO_CODIGO HORARIO_ANT, AUSENCIA.AU_STATUS STATUS_ANT, AUSENCIA.AU_TIPO, AUSENCIA.AU_OUT2EAT COMIDA_ANT, AUSENCIA.AU_STA_FES FESTIVO_ANT, ' +
{$ifdef INTERBASE}
                                    '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 1 ) ) CHECADA1, ' +
                                    '( select MOTIVO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 1 ) ) MC_CHECADA1, ' +
                                    '( select USUARIO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 1 ) ) US_CHECADA1, ' +

                                    '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 2 ) ) CHECADA2, ' +
                                    '( select MOTIVO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 2 ) ) MC_CHECADA2, ' +
                                    '( select USUARIO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 2 ) ) US_CHECADA2, ' +

                                    '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 3 ) ) CHECADA3, ' +
                                    '( select MOTIVO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 3 ) ) MC_CHECADA3, ' +
                                    '( select USUARIO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 3 ) ) US_CHECADA3, ' +

                                    '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 4 ) ) CHECADA4, ' +
                                    '( select MOTIVO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 4 ) ) MC_CHECADA4, ' +
                                    '( select USUARIO from SP_GET_INFO_CHECADA( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 4 ) ) US_CHECADA4, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) ) HRS_EXTRAS,'+
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) ) M_HRS_EXTRAS, ' +
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) ) OK_HRS_EXTRAS, ' +
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) ) AP_HRS_EXTRAS, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) ) DESCANSO, ' +
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) ) M_DESCANSO, ' +
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) ) OK_DESCANSO, ' +
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) ) AP_DESCANSO, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) ) PER_CG, ' +
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) ) M_PER_CG, ' +
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) ) OK_PER_CG, ' +
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) ) AP_PER_CG, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) ) PER_SG, ' +
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) ) M_PER_SG, ' +
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) ) OK_PER_SG, ' +
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) ) AP_PER_SG, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) ) PER_CG_ENT, ' +
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) ) M_PER_CG_ENT, ' +
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) ) OK_PER_CG_ENT, ' +
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) ) AP_PER_CG_ENT, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) ) PER_SG_ENT, ' +
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) ) M_PER_SG_ENT, ' +
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) ) OK_PER_SG_ENT, ' +
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) ) AP_PER_SG_ENT, ' +

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 11 ) ) PRE_FUERA_JOR,'+
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 11 ) ) M_PRE_FUERA_JOR,'+
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 11) ) OK_PRE_FUERA_JOR,'+
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 11) ) AP_PRE_FUERA_JOR,'+

                                    '( select RESULTADO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 ) ) PRE_DENTRO_JOR,'+
                                    '( select MOTIVO from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 ) ) M_PRE_DENTRO_JOR,'+
                                    '( select US_COD_OK from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 ) ) OK_PRE_DENTRO_JOR,'+
                                    '( select HRS_APROB from AUTORIZACIONES_SELECT( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 )) AP_PRE_DENTRO_JOR '+
                                    ' %3:s ' +

{$endif}
{$ifdef MSSQL}
                                    'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 1 ) CHECADA1, ' +
                                    'dbo.SP_CHECADA_MOT( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 1 ) MC_CHECADA1, '+
                                    'dbo.SP_CHECADA_USUARIO( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 1 ) US_CHECADA1, '+

                                    'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 2 ) CHECADA2, ' +
                                    'dbo.SP_CHECADA_MOT( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 2 ) MC_CHECADA2, '+
                                    'dbo.SP_CHECADA_USUARIO( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 2 ) US_CHECADA2, '+

                                    'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 3 ) CHECADA3, ' +
                                    'dbo.SP_CHECADA_MOT( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 3 ) MC_CHECADA3, '+
                                    'dbo.SP_CHECADA_USUARIO( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 3 ) US_CHECADA3, '+

                                    'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 4 ) CHECADA4, ' +
                                    'dbo.SP_CHECADA_MOT( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 4 ) MC_CHECADA4, '+
                                    'dbo.SP_CHECADA_USUARIO( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 4 ) US_CHECADA4, '+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) HRS_EXTRAS,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) M_HRS_EXTRAS,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) OK_HRS_EXTRAS,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 7 ) AP_HRS_EXTRAS,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) DESCANSO,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) M_DESCANSO,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) OK_DESCANSO,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 8 ) AP_DESCANSO,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) PER_CG,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) M_PER_CG,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) OK_PER_CG,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 5 ) AP_PER_CG,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) PER_SG,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) M_PER_SG,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) OK_PER_SG,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 6 ) AP_PER_SG,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) PER_CG_ENT,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) M_PER_CG_ENT,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) OK_PER_CG_ENT,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 9 ) AP_PER_CG_ENT,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) PER_SG_ENT,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) M_PER_SG_ENT,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) OK_PER_SG_ENT,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 10 ) AP_PER_SG_ENT,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 11 ) PRE_FUERA_JOR,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 11 ) M_PRE_FUERA_JOR,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 11 ) OK_PRE_FUERA_JOR,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 11 ) AP_PRE_FUERA_JOR,'+

                                    'dbo.AUTORIZACIONES_SELECT_HORAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 )  PRE_DENTRO_JOR,'+
                                    'dbo.AUTORIZACIONES_SELECT_MOTIVO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 )  M_PRE_DENTRO_JOR,'+
                                    'dbo.AUTORIZACIONES_SELECT_APROBO( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 )  OK_PRE_DENTRO_JOR,'+
                                    'dbo.AUTORIZACIONES_SELECT_HRSAPROBADAS( AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA, 12 ) AP_PRE_DENTRO_JOR '+
                                    ' %3:s '+
{$endif}
                                    'from AUSENCIA  ' +
                                    'where AUSENCIA.CB_CODIGO = %0:d and ( AUSENCIA.AU_FECHA Between %1:s and %2:s ) ' +
                                    'and ( AUSENCIA.AU_TIPO <> %4:s and  AUSENCIA.AU_TIPO <> %5:s )'+
                                    {$ifdef CAMBIO_TNOM}
                                    '%6:s'+
                                    {$endif}
                                    'order by AUSENCIA.AU_FECHA';
           eEmpleadoFechaIng_Baja: Result := 'select CB_FEC_ING, CB_FEC_BAJ from COLABORA where ( CB_CODIGO = %d )';
           eListaSupervisor : Result :=
                                     {$ifdef MSSQL}
                                     'select dbo.SP_LISTA_DENTRO( :FECHA, %d ,%d ) SUPER_DENTRO ';
                                     {$endif}
                                     {$ifdef INTERBASE}
                                     'select RESULTADO as SUPER_DENTRO from SP_LISTA_DENTRO( :FECHA, %d ,%d )';
                                     {$endif}

{$ifdef COMMSCOPE}
           eEvaluacionDiaria: Result := 'select LISTA.CB_CODIGO, ' + K_PRETTYNAME + ' as PrettyName, PE_EVADIA.ED_FECHA, '+
                                        'COALESCE( PE_EVADIA.ED_ASISTEN, 0 ) ED_ASISTEN, COALESCE( PE_EVADIA.ED_CALIDAD, 0 ) ED_CALIDAD, COALESCE( PE_EVADIA.ED_PRODUCT, 0 ) ED_PRODUCT, '+
                                        'COALESCE( PE_EVADIA.ED_TRABAJO, 0 ) ED_TRABAJO, COALESCE( PE_EVADIA.ED_SEGURID, 0 ) ED_SEGURID, COALESCE( PE_EVADIA.ED_EVALUA, 0 ) ED_EVALUA, '+
                                        'COALESCE( PE_EVADIA.US_CODIGO, 0 ) US_CODIGO '+
                                        'from SP_LISTA_EMPLEADOS( %s, %d ) LISTA '+
                                        'left outer join COLABORA on ( COLABORA.CB_CODIGO = LISTA.CB_CODIGO ) '+
                                        'left outer join PE_EVADIA on ( PE_EVADIA.CB_CODIGO = LISTA.CB_CODIGO ) where '+
                                        'PE_EVADIA.ED_FECHA = %s order by LISTA.CB_CODIGO';
           eInicializaEvalDiaria: Result := '{CALL PE_ESCRIBE_EVALUACIONES_DIARIAS( :Fecha, :Usuario )}';
{$endif}
     end;
end;

procedure TdmServerSuper.SetTablaInfo(const eTipo: eTablaSuper );
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTipo of
               eAsigna: setInfo( 'ASIGNA', 'AS_FECHA,CB_CODIGO,CB_NIVEL,US_CODIGO', 'AS_FECHA,CB_CODIGO' );
               eAusencia : SetInfo( 'AUSENCIA',
                           'CB_CODIGO, AU_DES_TRA, AU_DOBLES, AU_EXTRAS, AU_FECHA, AU_HORAS, AU_PER_CG, AU_PER_SG, AU_TARDES, AU_TIPO, AU_TIPODIA,'+
                           'CB_CLASIFI, CB_TURNO, CB_PUESTO, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9, '+
                           'AU_AUT_EXT, AU_AUT_TRA, AU_HOR_MAN, HO_CODIGO, AU_STATUS,AU_NUM_EXT, AU_TRIPLES, US_CODIGO, AU_POSICIO, AU_HORASCK, PE_YEAR, PE_TIPO, PE_NUMERO, AU_OUT2EAT, AU_STA_FES, AU_PRE_EXT'
{$ifdef QUINCENALES}

                           + ', CB_SALARIO'
{$endif}
                           + ', CB_NOMINA'
                           ,
                           'CB_CODIGO, AU_FECHA' );
               eTablaNomina : SetInfo( 'NOMINA', 'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO,NO_HORAS, NO_DOBLES,NO_TRIPLES,NO_HORA_CG,NO_HORA_SG,NO_DIAS_IN,NO_DIAS_VA,'+
                                                 'NO_SUP_OK,NO_FEC_OK,NO_HOR_OK,NO_DES_TRA','PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO' );

               eSesion   : SetInfo( 'SESION', 'SE_FOLIO,CU_CODIGO,MA_CODIGO,SE_LUGAR,SE_FEC_INI,SE_FEC_FIN,SE_HOR_INI,SE_HOR_FIN,SE_HORAS,SE_CUPO,SE_COMENTA,SE_REVISIO,SE_COSTO1,SE_COSTO2,SE_COSTO3,SE_STATUS,US_CODIGO,SE_EST'{$ifdef ICUMEDICAL_CURSOS} + ',SE_D_RUTA,SE_D_EXT,SE_D_NOM' {$endif}, 'SE_FOLIO');
{$ifdef COMMSCOPE}
               eEvaluacionDiariaGrabar : SetInfo ( 'PE_EVADIA', 'CB_CODIGO,ED_FECHA,ED_ASISTEN,ED_CALIDAD,ED_PRODUCT,ED_TRABAJO,ED_SEGURID,ED_EVALUA,US_CODIGO', 'CB_CODIGO,ED_FECHA' );
{$endif}
          end;
     end;
end;

{
function TdmServerSuper.FormateaHora( const sHora : String ) : String;
begin
     if sHora <> '    ' then
          Result := Copy( sHora, 1, 2 ) + ':' + Copy( sHora, 3, 4 );
end;
}

procedure TdmServerSuper.SetCondiciones( var sCondicion, sFiltro: String );
var
   oEvaluador : TZetaEvaluador;
   sError : String;
begin
     InitCreator;
     with oZetaCreator do
     begin
          PreparaEntidades;
          RegistraFunciones([efComunes]);
     end;
     oEvaluador := TZetaEvaluador.Create( oZetaCreator );
     with oEvaluador do
     try
          oEvaluador.Entidad := enEmpleado;	// LA TABLA PRINCIPAL
          // Condicion
          if ( strLleno( sCondicion ) ) and ( not FiltroSQLValido( sCondicion, sError ) ) then
             DataBaseError( sError );
          // Filtro
          if ( strLleno( sFiltro ) ) and ( not FiltroSQLValido( sFiltro, sError ) ) then
             DataBaseError( sError );
     finally
            FreeAndNil(oEvaluador);
     end;
end;

function TdmServerSuper.GetEmpleados(Empresa, Parametros: OleVariant): OleVariant;
var
   sScript, sFecha, sArea : String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               sFecha := DateToStrSQL( ParamByName( 'Fecha' ).AsDate );
               if ( ParamByName( 'LaborActivado' ).AsBoolean ) then
                  sArea := 'COLABORA.CB_AREA'
               else
                  sArea := ''' ''';
          end;
          sScript:= Format( GetScript( eMisEmpleados ), [ sArea, sFecha, sFecha, UsuarioActivo, sFecha,
                                                          GetFiltroEmpleados ] );
          Result:= OpenSQL( Empresa, sScript, TRUE );
     end;
     SetComplete;
end;

function TdmServerSuper.GetFiltroEmpleados: String;
var
   iFiltroFijo: Integer;
   RangoLista, Condicion, Filtro, sRangoCondicion : String;

   function GetFiltroFijo: String;
   begin
        case iFiltroFijo of
             0 : Result:= VACIO;
             1 : Result := '( AUSENCIA.AU_EXTRAS > 0 )';
             2 : Result := '( AUSENCIA.AU_NUM_EXT > AUSENCIA.AU_EXTRAS )';
             3 : Result := '( ( AUSENCIA.AU_HORASCK = 0 and AUSENCIA.AU_EXTRAS = 0 and AUSENCIA.AU_NUM_EXT = 0 ) and 0 < ( select count(*) from ' +
                 'CHECADAS where CHECADAS.CB_CODIGO = AUSENCIA.CB_CODIGO ' +
                 'and CHECADAS.AU_FECHA = AUSENCIA.AU_FECHA and ' +
                 'CHECADAS.CH_TIPO in (1,2) ) )';
             4 : Result := '( 0 = ( select count(*) from CHECADAS where ' +
                 'CHECADAS.CB_CODIGO = AUSENCIA.CB_CODIGO and ' +
                 'CHECADAS.AU_FECHA = AUSENCIA.AU_FECHA and ' +
                 'CHECADAS.CH_TIPO in (1,2) ) )';
        end;
   end;

begin
     Result := VACIO;
     with oZetaProvider.ParamList do
     begin
          iFiltroFijo := ParamByName( 'FiltroFijo' ).AsInteger;
          RangoLista := ParamByName( 'RangoLista' ).AsString;
          Condicion := ParamByName( 'Condicion' ).AsString;
          Filtro := ParamByName( 'Filtro' ).AsString;
     end;
     sRangoCondicion:= RangoLista;

     if strLleno( Condicion ) or strLleno( Filtro ) then
     begin
          SetCondiciones( Condicion, Filtro );
          sRangoCondicion := ConcatFiltros( sRangoCondicion, Condicion );
          sRangoCondicion := ConcatFiltros( sRangoCondicion, Filtro );
     end;

     Result := ConcatFiltros( GetFiltroFijo, sRangoCondicion );
     // Nivel 0
     Result := ConcatFiltros( Result, GetFiltroNivel0( oZetaProvider.EmpresaActiva ) );

     if strLleno( Result ) then
        Result := 'where ' + Result;
end;

function TdmServerSuper.GetTarjeta(Empresa: OleVariant; Empleado: Integer;
         Fecha: TDateTime; out Checadas: OleVariant): OleVariant;
var
   sFecha: String;
begin
     InitLog(Empresa,'GetTarjeta');
     sFecha := ZetaCommonTools.DateToStrSQL( Fecha );
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( GetScript( eTarjeta ), [ Empleado, sFecha ] ), True );
          Checadas := OpenSQL( Empresa, Format( GetScript( eChecadas ), [ Empleado, sFecha ] ), True );
     end;
     EndLog;SetComplete;
end;

function TdmServerSuper.GrabaTarjeta(Empresa, oDelta: OleVariant; const OldChecadas: WideString; VarChecadas: OleVariant;
                                     var ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaTarjeta');
     InitBitacoraChecadas;
     try
        FOldChecadas.CommaText := OldChecadas;
        FVarChecadas := VarChecadas;
        SetTablaInfo( eAusencia );
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
            //AbreBitacora;   No tiene relacion con metodo EscribeBitacora
             FDataSet := CreateQuery( GetScript( eAgregaChecadas ) );
             try
                with TablaInfo do
                begin
                     BeforeUpdateRecord := BeforeUpdateAusencia;
                     AfterUpdateRecord := AfterUpdateAusencia;
                end;
                Result := GrabaTabla( Empresa, oDelta, ErrorCount );
             finally
                FreeAndNil( FDataSet );
             end;
        end;
     finally
        ClearBitacoraChecadas;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerSuper.BeforeUpdateAusencia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if UpdateKind = ukModify then
        with DeltaDS do
             Applied := ( not CambiaCampo( FieldByName( 'AU_STATUS' ) ) ) and ( not CambiaCampo( FieldByName( 'AU_OUT2EAT' ) ) ) and
                        ( not CambiaCampo( FieldByName( 'HO_CODIGO' ) ) ) and ( not CambiaCampo( FieldByName( 'AU_STA_FES' ) ) );
end;

procedure TdmServerSuper.AfterUpdateAusencia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   FAutorizaciones, FBorraChecadas: TZetaCursor;
   iEmpleado: Integer;
   dFecha: TDate;
   sTextoBitacora: String;

   procedure AgregaChecadas( FDestino: TZetaCursor );
   var
      i : Integer;
   begin
        FNewChecadas.Clear;
        with oZetaProvider do
        begin
             if not VarIsNull( FVarChecadas ) then
                for i := 0 to VarArrayHighBound( FVarChecadas, 1 ) do
                begin
                     ParamAsChar( FDestino, 'CH_H_REAL', FVarChecadas[i][0], K_ANCHO_HORA );
                     ParamAsChar( FDestino, 'CH_H_AJUS', FVarChecadas[i][0], K_ANCHO_HORA );
                     ParamAsChar( FDestino, 'CH_GLOBAL', FVarChecadas[i][1], K_ANCHO_BOOLEANO );
                     ParamAsVarChar( FDestino, 'CH_RELOJ', FVarChecadas[i][2], K_ANCHO_RELOJ );
                     ParamAsChar( FDestino, 'CH_SISTEMA', K_GLOBAL_NO, K_ANCHO_BOOLEANO );
                     ParamAsInteger( FDestino, 'US_CODIGO', FVarChecadas[i][3] );
                     ParamAsVarChar( FDestino, 'CH_MOTIVO', FVarChecadas[i][4], K_ANCHO_RELOJ );
                     Ejecuta( FDestino );
                     FNewChecadas.Add( FVarChecadas[i][0] );
                end;
        end;
   end;

   procedure SetInfoAutorizaciones( FDestino: TZetaCursor; FOrigen: TzProviderClientDataSet );
   begin
        with oZetaProvider, FOrigen do
        begin
             ParamAsInteger( FDestino, 'CB_CODIGO', CampoAsVar( FieldByName( 'CB_CODIGO' ) ) );
             ParamAsDate( FDestino, 'AU_FECHA', CampoAsVar( FieldByName( 'AU_FECHA' ) ) );
             ParamAsFloat( FDestino, 'AU_AUT_EXT', CampoAsVar( FieldByName( 'HRS_EXTRAS' ) ) );
             ParamAsChar( FDestino, 'M_AU_AUT_EXT', CampoAsVar( FieldByName( 'M_HRS_EXTRAS' ) ), 4 );
             ParamAsFloat( FDestino, 'AU_AUT_TRA', CampoAsVar( FieldByName( 'DESCANSO' ) ) );
             ParamAsChar( FDestino, 'M_AU_AUT_TRA', CampoAsVar( FieldByName( 'M_DESCANSO' ) ), 4 );
             ParamAsFloat( FDestino, 'AU_PER_CG', CampoAsVar( FieldByName( 'PER_CG' ) ) );
             ParamAsChar( FDestino, 'M_AU_PER_CG', CampoAsVar( FieldByName( 'M_PER_CG' ) ), 4 );
             ParamAsFloat( FDestino, 'AU_PCG_ENT', CampoAsVar( FieldByName( 'PER_CG_ENT' ) ) );
             ParamAsChar( FDestino, 'M_AU_PCG_ENT', CampoAsVar( FieldByName( 'M_PER_CG_ENT' ) ), 4 );
             ParamAsFloat( FDestino, 'AU_PER_SG', CampoAsVar( FieldByName( 'PER_SG' ) ) );
             ParamAsChar( FDestino, 'M_AU_PER_SG', CampoAsVar( FieldByName( 'M_PER_SG' ) ), 4 );
             ParamAsFloat( FDestino, 'AU_PSG_ENT', CampoAsVar( FieldByName( 'PER_SG_ENT' ) ) );
             ParamAsChar( FDestino, 'M_AU_PSG_ENT', CampoAsVar( FieldByName( 'M_PER_SG_ENT' ) ), 4 );
             ParamAsFloat( FDestino, 'AU_PRE_FUERA_JOR', CampoAsVar( FieldByName( 'PRE_FUERA_JOR' ) ) );
             ParamAsChar( FDestino, 'M_AU_PRE_FUERA_JOR', CampoAsVar( FieldByName( 'M_PRE_FUERA_JOR' ) ), 4 );
             ParamAsFloat( FDestino, 'AU_PRE_DENTRO_JOR', CampoAsVar( FieldByName( 'PRE_DENTRO_JOR' ) ) );
             ParamAsChar( FDestino, 'M_AU_PRE_DENTRO_JOR', CampoAsVar( FieldByName( 'M_PRE_DENTRO_JOR' ) ), 4 );
             ParamAsChar( FDestino, 'CH_GLOBAL', K_GLOBAL_NO, 1 );
             ParamAsInteger( FDestino, 'US_CODIGO', UsuarioActivo );
        end;
   end;

begin
     with DeltaDS do
     begin
          iEmpleado:= CampoAsVar( FieldByName( 'CB_CODIGO' ) );
          dFecha:= CampoAsVar( FieldByName( 'AU_FECHA' ) );
     end;
     if UpdateKind <> ukDelete then
     begin
          with oZetaProvider do
          begin
               //Borrar Checadas Anteriores
               FBorraChecadas := CreateQuery( Format( GetScript( eBorraChecadas ), [ iEmpleado, DateToStrSQL( dFecha ) ] ) );
               try
                  Ejecuta( FBorraChecadas );
               finally
                  FreeAndNil( FBorraChecadas );
               end;
               //Agregar Nuevas Checadas
               ParamAsInteger( FDataSet, 'CB_CODIGO', iEmpleado );
               ParamAsDate( FDataSet, 'AU_FECHA', dFecha );
               AgregaChecadas( FDataSet );
               //Grabar Autorizaciones
               FAutorizaciones := CreateQuery( GetScript( eGrabaAutorizaciones ) );
               try
                  SetInfoAutorizaciones( FAutorizaciones, DeltaDS );
                  Ejecuta( FAutorizaciones );
               finally
                  FreeAndNil( FAutorizaciones );
               end;
               // Calcula Tarjeta
               InitGlobales;
               InitTarjeta;
               try
                  with DeltaDS, FTarjeta do
                  begin
                       CalculaTarjetaBegin;
                       CalculaTarjeta( iEmpleado, dFecha, CampoAsVar( FieldByName( 'HO_CODIGO' ) ),
                                       eStatusAusencia( CampoAsVar( FieldByName( 'AU_STATUS' ) ) ),
                                       CampoAsVar( FieldByName( 'US_CODIGO' ) ) );
                       CalculaTarjetaEnd;
                  end;
               finally
                  ClearTarjeta;
               end;
               // Bitacora
               BitacoraTarjeta( DeltaDS, UpdateKind, iEmpleado, dFecha );
          end;
     end
     else
     begin
         sTextoBitacora := Format( 'Checadas del %s del Empleado %d: ', [ FormatDateTime( 'dd/mmm/yyyy', dFecha ), iEmpleado ] ) + CR_LF;
         IncluirBitacora( clbAjusteAsistencia, iEmpleado, dFecha, 'Borr� Tarjeta Diaria', sTextoBitacora + FOldChecadas.CommaText );
     end;
end;

function TdmServerSuper.GrabaListaAsistenciaParametros: String;
begin
     Result := 'Fecha: ' + ZetaCommonTools.FechaCorta( oZetaProvider.ParamList.ParamByName( 'FECHA_AJUSTE' ).AsDateTime );
end;

function TdmServerSuper.GrabaListaAsistencia(Empresa, oDelta, Parametros: OleVariant; var ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaListaAsistencia');
     SetTablaInfo( eAusencia );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          if OpenProcess( prASISAjusteColectivo, ParamList.ParamByName( 'Cambios' ).AsInteger, GrabaListaAsistenciaParametros ) then
          begin
               Result := GrabarListaTarjetasAsistencia(True, oDelta,ErrorCount );
          end;
          Result := CloseProcess;
     end;
     EndLog;SetComplete;
end;

function TdmServerSuper.GrabarListaTarjetasAsistencia(const lGlobal:Boolean ;const oDelta:OleVariant; var ErrorCount: Integer ):OleVariant;
begin
     with oZetaProvider do
     begin
          try
             if not VarIsNull( oDelta ) then             // Si no se tienen cambios no se procesa nada
             begin
                  FDataSet := CreateQuery( GetScript( eAgregaAsistencia ) );
                  FDataSetAdic := CreateQuery( GetScript( eModificaAsistencia ) );
                  ParamAsInteger( FDataSet, 'US_CODIGO', UsuarioActivo );
                  ParamAsInteger( FDataSetAdic, 'US_CODIGO', UsuarioActivo );
                  with TablaInfo do
                  begin
                       if lGlobal then
                       begin
                            BeforeUpdateRecord := BeforeUpdateAsistenciaGlobal;
                            OnUpdateError := UpdateErrorAsistenciaGlobal;
                       end
                       else
                           BeforeUpdateRecord := BeforeUpdateAsistenciaPeriodo;
                       { Si viene la llamada desde Modificar Asistencia de Pren�mina, el UpdateError lo controla el cliente }
                       AfterUpdateRecord := AfterUpdateAsistenciaGlobal;    //Calculo de Tarjeta y Bitacora
                  end;
                  InitGlobales;
                  InitTarjeta;
                  InitBitacoraChecadas;
                  try
                     FTarjeta.CalculaTarjetaBegin;
                     if lGlobal then
                        Result := GrabaTabla( EmpresaActiva, oDelta, ErrorCount )
                     else
                         Result := GrabaTablaGrid( EmpresaActiva, oDelta, ErrorCount );
                     FTarjeta.CalculaTarjetaEnd;
                  finally
                     ClearBitacoraChecadas;
                     ClearTarjeta;
                     FreeAndNil( FDataSet );
                     FreeAndNil( FDataSetAdic );
                  end;
             end;
          except
                on Error: Exception do
                begin
                     if ( Log <> nil ) then
                        Log.Excepcion( 0, 'Error Al Registrar Ajustes de Asistencia', Error )
                     else
                         DataBaseError('Error Al Registrar Ajustes de Asistencia' + CR_LF + Error.Message );
                end;
          end;
     end;
end;

procedure TdmServerSuper.BeforeUpdateAsistenciaGlobal(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     FCambioAplicado:= FALSE;
     with DeltaDS, oZetaProvider do
     begin
          try
              if ( UpdateKind = ukModify ) and ( CanContinue( CampoAsVar( FieldByName( 'CB_CODIGO' ) ), FALSE ) ) then    //Evita los registros Excluidos . NOTA no ukInsert por que vienen de SP_LISTA_EMPLEADOS
              begin
                   if ( CampoAsVar( FieldByName( 'EXISTE' ) ) > 0 ) then     // Existe la Tarjeta
                   begin
                        SetInfoTarjetaGlobal( FDataSetAdic, DeltaDS );
                        Ejecuta( FDataSetAdic );
                   end
                   else
                   begin
                        IncluirBitacora( clbAjusteAsistencia, CampoAsVar(FieldByName('CB_CODIGO')), CampoAsVar(FieldByName('AU_FECHA')), 'Agreg� Tarjeta Diaria',VACIO  );
                        SetInfoTarjetaGlobal( FDataSet, DeltaDS );
                        Ejecuta( FDataSet );
                   end;
                   FCambioAplicado:= TRUE;
              end;
           except
                on Error: Exception do
                begin
                     if ( Log <> nil ) then
                     begin
                          EmpiezaTransaccion;
                          try
                             Log.Excepcion( CampoAsVar(FieldByName( 'CB_CODIGO' ) ), 'Error Al Registrar Ajustes de Asistencia', Error );
                             TerminaTransaccion(True);
                          except
                                 RollBackTransaccion; 
                          end;
                     end
                     else
                         DataBaseError('Error Al Registrar Ajustes de Asistencia' + CR_LF + Error.Message );
                end;
           end;
     end;
     Applied := TRUE;
end;

procedure TdmServerSuper.AfterUpdateAsistenciaGlobal(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   iEmpleado: Integer;
   dFecha: TDate;

   procedure SetInfoChecadas;
   var
      i : Integer;
      sChecada : String;
   begin
        FOldChecadas.Clear;
        FNewChecadas.Clear;
        with DeltaDS do
             for i := 1 to 4 do
             begin
                  sChecada:= CampoOldAsVar( FieldByName( 'CHECADA' + IntToStr( i ) ) );
                  if ( StrLleno( sChecada ) ) and ( aMinutos( sChecada ) > 0 ) then
                     FOldChecadas.Add( sChecada );
                  sChecada:= CampoAsVar( FieldByName( 'CHECADA' + IntToStr( i ) ) );
                  if ( StrLleno( sChecada ) ) and ( aMinutos( sChecada ) > 0 ) then
                     FNewChecadas.Add( sChecada );
             end;
   end;

begin
     with DeltaDS, oZetaProvider do
     begin
          iEmpleado:= CampoAsVar( FieldByName( 'CB_CODIGO' ) );
          dFecha:= CampoAsVar( FieldByName( 'AU_FECHA' ) );
          if FCambioAplicado then    //Solo Aplica a Registros Modificados en el BeforeUpdate
          begin
               FTarjeta.CalculaTarjeta( iEmpleado, dFecha, CampoAsVar( FieldByName( 'HO_CODIGO' ) ),
                                        eStatusAusencia( CampoAsVar( FieldByName( 'AU_STATUS' ) ) ),
                                        CampoAsVar( FieldByName( 'US_CODIGO' ) ) );
               SetInfoChecadas;
               //Log.Evento( clbAjusteAsistencia, iEmpleado, dFecha, 'Se Ajusto Asistencia del Empleado' + FTemp );
               BitacoraTarjeta( DeltaDS, UpdateKind, iEmpleado, dFecha );
          end;
     end;
end;

procedure TdmServerSuper.UpdateErrorAsistenciaGlobal(Sender: TObject; DataSet: TzProviderClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind; var Response: TResolverResponse);
begin
     with oZetaProvider, DataSet do
          Log.Excepcion( CampoAsVar( FieldByName( 'CB_CODIGO' ) ), 'Error Al Registrar Ajuste de Asistencia ' +
                         FechaCorta( CampoAsVar( FieldByName( 'AU_FECHA' ) ) ), E );
     Response := rrSkip;
end;

procedure TdmServerSuper.SetInfoTarjetaGlobal( FDestino: TZetaCursor; FOrigen: TzProviderClientDataSet );
begin
     with oZetaProvider, FOrigen do
     begin
          ParamAsInteger( FDestino, 'CB_CODIGO', CampoAsVar( FieldByName( 'CB_CODIGO' ) ) );
          ParamAsDate( FDestino, 'AU_FECHA', CampoAsVar( FieldByName( 'AU_FECHA' ) ) );
          ParamAsChar( FDestino, 'HO_CODIGO', CampoAsVar( FieldByName( 'HO_CODIGO' ) ), 6 );
          ParamAsChar( FDestino, 'AU_HOR_MAN', CampoAsVar( FieldByName( 'AU_HOR_MAN' ) ), 1 );
          ParamAsInteger( FDestino, 'AU_STATUS', CampoAsVar( FieldByName( 'AU_STATUS' ) ) );
          ParamAsFloat( FDestino, 'HRS_EXTRAS', CampoAsVar( FieldByName( 'HRS_EXTRAS' ) ) );
          ParamAsChar( FDestino, 'M_HRS_EXTRAS', CampoAsVar( FieldByName( 'M_HRS_EXTRAS' ) ), 4 );
          ParamAsFloat( FDestino, 'DESCANSO', CampoAsVar( FieldByName( 'DESCANSO' ) ) );
          ParamAsChar( FDestino, 'M_DESCANSO', CampoAsVar( FieldByName( 'M_DESCANSO' ) ), 4 );
          ParamAsFloat( FDestino, 'PER_CG', CampoAsVar( FieldByName( 'PER_CG' ) ) );
          ParamAsChar( FDestino, 'M_PER_CG', CampoAsVar( FieldByName( 'M_PER_CG' ) ), 4 );
          ParamAsFloat( FDestino, 'PER_CG_ENT', CampoAsVar( FieldByName( 'PER_CG_ENT' ) ) );
          ParamAsChar( FDestino, 'M_PER_CG_ENT', CampoAsVar( FieldByName( 'M_PER_CG_ENT' ) ), 4 );
          ParamAsFloat( FDestino, 'PER_SG', CampoAsVar( FieldByName( 'PER_SG' ) ) );
          ParamAsChar( FDestino, 'M_PER_SG', CampoAsVar( FieldByName( 'M_PER_SG' ) ), 4 );
          ParamAsFloat( FDestino, 'PER_SG_ENT', CampoAsVar( FieldByName( 'PER_SG_ENT' ) ) );
          ParamAsChar( FDestino, 'M_PER_SG_ENT', CampoAsVar( FieldByName( 'M_PER_SG_ENT' ) ), 4 );
          ParamAsFloat( FDestino, 'PRE_FUERA_JOR', CampoAsVar( FieldByName( 'PRE_FUERA_JOR' ) ) );
          ParamAsChar( FDestino, 'M_PRE_FUERA_JOR', CampoAsVar( FieldByName( 'M_PRE_FUERA_JOR' ) ), 4 );
          ParamAsFloat( FDestino, 'PRE_DENTRO_JOR', CampoAsVar( FieldByName( 'PRE_DENTRO_JOR' ) ) );
          ParamAsChar( FDestino, 'M_PRE_DENTRO_JOR', CampoAsVar( FieldByName( 'M_PRE_DENTRO_JOR' ) ), 4 );
          ParamAsChar( FDestino, 'CHECADA1', CampoAsVar( FieldByName( 'CHECADA1' ) ), 4 );
          ParamAsChar( FDestino, 'CHECADA2', CampoAsVar( FieldByName( 'CHECADA2' ) ), 4 );
          ParamAsChar( FDestino, 'CHECADA3', CampoAsVar( FieldByName( 'CHECADA3' ) ), 4 );
          ParamAsChar( FDestino, 'CHECADA4', CampoAsVar( FieldByName( 'CHECADA4' ) ), 4 );

          ParamAsChar( FDestino, 'MC_CHECADA1', CampoAsVar( FieldByName( 'MC_CHECADA1' ) ), 4 );
          ParamAsChar( FDestino, 'MC_CHECADA2', CampoAsVar( FieldByName( 'MC_CHECADA2' ) ), 4 );
          ParamAsChar( FDestino, 'MC_CHECADA3', CampoAsVar( FieldByName( 'MC_CHECADA3' ) ), 4 );
          ParamAsChar( FDestino, 'MC_CHECADA4', CampoAsVar( FieldByName( 'MC_CHECADA4' ) ), 4 );

          ParamAsInteger( FDestino, 'AU_OUT2EAT', CampoAsVar( FieldByName( 'AU_OUT2EAT' ) ) );
     end;
end;

function TdmServerSuper.GetHisKardex(Empresa: OleVariant; Empleado: Integer; out Otros: OleVariant): OleVariant;
var
   sScript: String;
begin
     InitLog(Empresa,'GetHisKardex');
     with oZetaProvider do
     begin
          Result:= OpenSQL( Empresa, Format( GetScript( eHisKardex ), [Empleado] ), TRUE );
          sScript := Format( GetScript( ePermIncaVaca ), [Empleado, Empleado, Empleado] );
          Otros:= OpenSQL( Empresa, sScript, TRUE );
     end;
     EndLog;SetComplete;
end;

function TdmServerSuper.GetMisAsignaciones(Empresa: OleVariant; Fecha: TDateTime): OleVariant;
var
   sScript, sNivel0 : String;
begin
     InitLog(Empresa,'GetMisAsignaciones');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          sNivel0 := GetFiltroNivel0( Empresa );
          if strLleno( sNivel0 ) then
             sNivel0 := 'where ' + sNivel0;
          sScript:= Format( GetScript( eMisAsignados ), [ DateToStrSQL( Fecha ), UsuarioActivo, sNivel0 ] );
          Result:= OpenSQL( Empresa, sScript, TRUE );
     end;
     EndLog;SetComplete;
end;

function TdmServerSuper.GetListaSupervisores(Empresa: OleVariant): OleVariant;
var
   sScript, sNivel : String;
begin
     InitLog(Empresa,'GetListaSupervisores');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          InitGlobales;
          sNivel := Trim( IntToStr( GetGlobalInteger( ZGlobalTress.K_GLOBAL_NIVEL_SUPERVISOR ) ) );
          sScript:= Format( GetScript( eSupervisores ), [ sNivel, sNivel, UsuarioActivo ] );
          Result:= OpenSQL( Empresa, sScript, TRUE );
     end;
     EndLog;SetComplete;
end;

function TdmServerSuper.GrabaAsignaciones(Empresa, oDelta: OleVariant; ErrorCount: Integer): OleVariant;
var
   sNivel : String;
begin
     InitLog(Empresa,'GrabaAsignaciones');
     SetTablaInfo( eAsigna );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          //AbreBitacora;   No tiene relacion con metodo EscribeBitacora
          sNivel := Trim( IntToStr( GetGlobalInteger( ZGlobalTress.K_GLOBAL_NIVEL_SUPERVISOR ) ) );
          FDataSet := CreateQuery( GetScript( eBorraAsigna ) );
          FDataSetAdic := CreateQuery( GetScript( eAgregaAsigna ) );
          FDataSetAsig := CreateQuery( Format( GetScript( eExisteAsigna ), [ sNivel ] ) );
          try
             TablaInfo.BeforeUpdateRecord := BeforeUpdateAsignaciones;
             Result := GrabaTablaGrid( Empresa, oDelta, ErrorCount );
          finally
             FreeAndNil( FDataSet );
             FreeAndNil( FDataSetAdic );
             FreeAndNil( FDataSetAsig );
          end;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerSuper.BeforeUpdateAsignaciones(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   iEmpleado : Integer;
   sSuper : String;
   FechaSupervisor : TDate;

   function EsAsignacion : Boolean;
   begin
        Result := FALSE;
        with FDataSetAsig, OZetaProvider do
        begin
             ParamAsInteger( FDataSetAsig, 'Empleado', iEmpleado );
             ParamAsDate( FDataSetAsig, 'Fecha', FechaSupervisor );
             Active := TRUE;
             if not IsEmpty then
                Result := ( Trim( Fields[0].AsString ) <> Trim( sSuper ) );
             Active := FALSE;
        end;
   end;

begin
     with DeltaDS, oZetaProvider do
     begin
          iEmpleado := CampoAsVar( FieldByName( 'CB_CODIGO' ) );
          sSuper := CampoAsVar( FieldByName( 'CB_NIVEL' ) );
          FechaSupervisor := CampoAsVar( FieldByName( 'FECHA' ));
          // Borrar Asignacion
          ParamAsInteger( FDataSet, 'Empleado', iEmpleado  );
          ParamAsDate( FDataSet, 'Fecha', FechaSupervisor );
          Ejecuta( FDataSet );
          if EsAsignacion then   // Agregar Asignacion
          begin
               ParamAsInteger( FDataSetAdic, 'CB_CODIGO', iEmpleado );
               ParamAsDate( FDataSetAdic, 'AS_FECHA', FechaSupervisor );
               ParamAsChar( FDataSetAdic, 'CB_NIVEL', sSuper, K_ANCHO_CODIGO );
               ParamAsInteger( FDataSetAdic, 'US_CODIGO', UsuarioActivo );
               Ejecuta( FDataSetAdic );
          end;
          if strLleno( CampoAsVar( FieldByName( 'ANTERIOR' ) ) ) then
             EscribeBitacora( tbNormal, clbAsignaciones, iEmpleado, FechaSupervisor,
                              'Asignaci�n del Empleado del Supervisor ' +
                              CampoAsVar( FieldByName( 'ANTERIOR' ) ) + ' al ' + sSuper, ' ' )
          else
             EscribeBitacora( tbNormal, clbAsignaciones, iEmpleado, FechaSupervisor,
                              'Asignaci�n del Empleado al Supervisor: ' + sSuper, ' ' );
     end;
     Applied := TRUE;
end;

procedure TdmServerSuper.SetFoto( const lEnabled: Boolean; var sFoto, sFotoJoin : String );
begin
     if lEnabled then
     begin
          sFoto     := 'IMAGEN.IM_BLOB';
          sFotoJoin := 'left outer join IMAGEN on IMAGEN.CB_CODIGO = B.CB_CODIGO and IMAGEN.IM_TIPO = ''FOTO'' ';
     end
     else
     begin
          sFoto      := '0';
          sFotoJoin  := VACIO;
     end;
end;

function TdmServerSuper.GetDatosEmpleado(Empresa: OleVariant; Empleado: Integer;  Fecha: TDateTime; ObtenerFoto: WordBool; out Datos: OleVariant): WordBool;
var
   sScript, sFoto, sFotoJoin, sEmpleado : String;

   function GetFiltroEmpleadoNivel0: String;
   begin
        Result := ZetaServerTools.GetFiltroNivel0( oZetaProvider.EmpresaActiva );
        if strLleno( Result ) then
{$ifdef INTERBASE}
           Result := 'where ' + Result;
{$endif}
{$ifdef MSSQL}
           Result := ' and ' + Result;
{$endif}
   end;

begin
     InitLog(Empresa,'GetDatosEmpleado');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          SetFoto( ObtenerFoto, sFoto, sFotoJoin );
          sEmpleado := IntToStr( Empleado );

          sScript := Format( GetScript( eDatosEmpleado ), [ DateToStrSQL( Fecha ), sFoto,  sFotoJoin, sEmpleado, GetFiltroEmpleadoNivel0 ] );

          Datos := OpenSQL( Empresa, sScript, TRUE );
          Result := ( RecsOut > 0 );
     end;
     EndLog;SetComplete;
end;

function TdmServerSuper.GetDatosEmpleadoAnterior(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; ObtenerFoto: WordBool; out Datos: OleVariant): WordBool;
const
     EMPLEADOANTERIOR = '( select MAX( C2.CB_CODIGO ) from COLABORA C2 where ( C2.CB_CODIGO < %d ) %s )';
var
   sScript, sFoto, sFotoJoin, sEmpleado : String;
begin
     InitLog(Empresa,'GetDatosEmpleadoAnterior');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          SetFoto( ObtenerFoto, sFoto, sFotoJoin );
          sEmpleado := Format( EMPLEADOANTERIOR, [ Empleado, Nivel0( Empresa ) ] );

          sScript := Format( GetScript( eDatosEmpleado ), [ DateToStrSQL( Fecha ), sFoto,
                     sFotoJoin, sEmpleado, VACIO ] );

          Datos := OpenSQL( Empresa, sScript, TRUE );
          Result := ( RecsOut > 0 );
     end;
     EndLog;SetComplete;
end;

function TdmServerSuper.GetDatosEmpleadoSiguiente(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; ObtenerFoto: WordBool; out Datos: OleVariant): WordBool;
const
     EMPLEADOSIGUIENTE = '( select MIN( C2.CB_CODIGO ) from COLABORA C2 where ( C2.CB_CODIGO > %d ) %s )';
var
   sScript, sFoto, sFotoJoin, sEmpleado : String;
begin
     InitLog(Empresa,'GetDatosEmpleadoSiguiente');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          SetFoto( ObtenerFoto, sFoto, sFotoJoin );
          sEmpleado := Format( EMPLEADOSIGUIENTE, [ Empleado, Nivel0( Empresa ) ] );

          sScript := Format( GetScript( eDatosEmpleado ), [ DateToStrSQL( Fecha ), sFoto,
                     sFotoJoin, sEmpleado, VACIO ] );

          Datos := OpenSQL( Empresa, sScript, TRUE );
          Result := ( RecsOut > 0 );
     end;
     EndLog;SetComplete;
end;

function TdmServerSuper.AjusteColectivoGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'AjusteColectivoGetLista');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          Result := AjusteColectivoBuildDataSet;
     end;
     EndLog;SetComplete;
end;

function TdmServerSuper.AjusteColectivoBuildDataSet : OleVariant;
var
   cdsDataSet : TClientDataSet;
   iAnterior : Integer;
   dFecha : TDate;
   sFecha : String;
   aDefaults : array [ eAutorizaChecadas ] of variant;
   eComio : eOut2Eat;
   lCambiaComida : Boolean;

   function GetFiltroAsistencia : String;
   var
      Condicion, Filtro: String;
   begin
        Result := VACIO;
        with oZetaProvider.ParamList do
        begin
             Result := ConcatFiltros( Result, ParamByName( 'RangoLista' ).AsString );
             // Condicion y Filtro
             Condicion := ParamByName( 'Condicion' ).AsString;
             Filtro := ParamByName( 'Filtro' ).AsString;
             if strLleno( Condicion ) or strLleno( Filtro ) then
             begin
                  SetCondiciones( Condicion, Filtro );
                  Result := ConcatFiltros( Result, Condicion );
                  Result := ConcatFiltros( Result, Filtro );
             end;
             // Nivel 0
             Result := ConcatFiltros( Result, GetFiltroNivel0( oZetaProvider.EmpresaActiva ) );
        end;
        if StrLleno( Result ) then
           Result := 'where ' + Result;
   end;

   procedure InicializaInfoTurnos;
   begin
        with oZetaProvider do
        begin
             InitRitmos;
             InitTarjeta;
             FTarjeta.GetTurnoHorarioBegin( dFecha, dFecha );
        end;
   end;

   procedure DesPreparaInfoTurno;
   begin
        FTarjeta.GetTurnoHorarioEnd;
        ClearTarjeta;
   end;

   procedure LlenaArregloDefaults;
   begin
        with oZetaProvider.ParamList do
        begin
             iAnterior := ParamByName( 'D_ANTERIOR' ).AsInteger;
             aDefaults[acExtras] := VarArrayOf( [ ( ParamByName( 'D_EXTRAS' ).AsFloat > 0 ),
                                                    ParamByName( 'D_EXTRAS' ).AsFloat,
                                                    ParamByName( 'M_D_EXTRAS' ).AsString ] );
             aDefaults[acDescanso] := VarArrayOf( [ ( ParamByName( 'D_DES_TRA' ).AsFloat > 0 ),
                                                      ParamByName( 'D_DES_TRA' ).AsFloat,
                                                      ParamByName( 'M_D_DES_TRA' ).AsString ] );
             aDefaults[acConGoce] := VarArrayOf( [ ( ParamByName( 'D_PER_CG' ).AsFloat > 0 ),
                                                     ParamByName( 'D_PER_CG' ).AsFloat,
                                                     ParamByName( 'M_D_PER_CG' ).AsString ] );
             aDefaults[acConGoceEntrada] := VarArrayOf( [ ( ParamByName( 'D_CG_ENT' ).AsFloat > 0 ),
                                                            ParamByName( 'D_CG_ENT' ).AsFloat,
                                                            ParamByName( 'M_D_CG_ENT' ).AsString ] );
             aDefaults[acSinGoce] := VarArrayOf( [ ( ParamByName( 'D_PER_SG' ).AsFloat > 0 ),
                                                     ParamByName( 'D_PER_SG' ).AsFloat,
                                                     ParamByName( 'M_D_PER_SG' ).AsString ] );
             aDefaults[acSinGoceEntrada] := VarArrayOf( [ ( ParamByName( 'D_SG_ENT' ).AsFloat > 0 ),
                                                            ParamByName( 'D_SG_ENT' ).AsFloat,
                                                            ParamByName( 'M_D_SG_ENT' ).AsString ] );
             aDefaults[acPrepagFueraJornada]:= VarArrayOf( [( ParamByName( 'D_PRE_FUERA_JOR' ).AsFloat > 0 ),
                                                             ParamByName( 'D_PRE_FUERA_JOR' ).AsFloat,
                                                             ParamByName( 'M_D_PRE_FUERA_JOR' ).AsString ] );

             aDefaults[acPrepagDentroJornada]:= VarArrayOf( [ ( ParamByName( 'D_PRE_DENTRO_JOR' ).AsFloat > 0 ),
                                                                ParamByName( 'D_PRE_DENTRO_JOR' ).AsFloat,
                                                                ParamByName( 'M_D_PRE_DENTRO_JOR' ).AsString ] );

             lCambiaComida := ParamByName( 'CAMBIACOMIDA').AsBoolean;
             eComio := eOut2Eat( ParamByName( 'OUT2EAT' ).AsInteger );

        end;
   end;

   function PuedeModificar( const sAprobacion: string ): Boolean;
   begin
        with oZetaProvider do
        begin
             if GetGlobalBooleano( K_GLOBAL_APROBAR_AUTORIZACIONES ) then
             begin
                  Result := ParamList.ParamByName( 'APROBARAUTORIZACIONES' ).AsBoolean;
                  if NOT Result then
                  begin
                       with cdsDataset do
                            Result := (FieldByName( sAprobacion ).AsInteger = 0);
                  end;
             end
             else
             begin
                  Result := TRUE;
             end;
        end;
   end;

   procedure AplicaDefault( const eTipo : eAutorizaChecadas; oCampoValor, oCampoMotivo : TField; const lPuedeModificar: Boolean );
   var
      lReemplaza : Boolean;
      rDefault : TDiasHoras;
   begin
        if lPuedeModificar then
        begin
             rDefault := aDefaults[ eTipo ][1];
             lReemplaza := False;
             if ( oCampoValor.AsFloat = 0 ) then
                lReemplaza := True
             else
                case iAnterior of
                     0 : lReemplaza := False;
                     1 : begin
                              lReemplaza := True;
                              rDefault := rDefault + oCampoValor.AsFloat;
                         end;
                     2 : lReemplaza := True;
                end;

             if lReemplaza then
             begin
                  with cdsDataSet do
                  begin
                       if not ( State in [ dsEdit, dsInsert ] ) then
                          Edit;
                  end;
                  oCampoValor.AsFloat  := rDefault;
                  oCampoMotivo.AsString:= aDefaults[ eTipo ][2];
             end;
        end;
   end;

   procedure AplicaDefaultOut2Eat;
   begin
        with cdsDataset do
        begin
             if lCambiaComida AND
                ( ( FieldByName('EXISTE').AsInteger = 0 ) OR (iAnterior = 2) ) then
             begin
                  if not ( State in [ dsEdit, dsInsert ] ) then
                     Edit;
                  FieldByName( 'AU_OUT2EAT' ).AsInteger := Ord(eComio);
             end;
        end;
   end;

var
    FQuery : TZetaCursor;
    eStatus, eStatusLimite : eStatusPeriodo;
    lPuedeCambiarTarjeta,lPuedeAgregarTarjeta: Boolean;
begin
     with oZetaProvider do
     begin
          InitGlobales;
          lPuedeCambiarTarjeta := ParamList.ParamByName('PuedeCambiarTarjeta').AsBoolean;
          if NOT lPuedeCambiarTarjeta then
          begin
               FQuery := CreateQuery( GetScript(eInvestigaStatusTarjeta) );
               eStatusLimite := eStatusPeriodo( GetGlobalInteger(K_LIMITE_MODIFICAR_ASISTENCIA) );
          end
          else
          begin
               FQuery := NIL;
               eStatusLimite := spAfectadaTotal;
          end;
     end;

     cdsDataSet := TClientDataSet.Create( Self );
     try
        with oZetaProvider do
        begin
             GetDatosActivos;
             dFecha := ParamList.ParamByName( 'FECHA_AJUSTE' ).AsDateTime;
             sFecha := DateToStrSQL( dFecha );
             //sFecha := DateToStrSQL( FechaDefault );
             LlenaArregloDefaults;
             with cdsDataSet do
             begin
                  Data:= OpenSQL( EmpresaActiva, Format( GetScript( eGridAsistencia ), [ sFecha,
                                  UsuarioActivo, sFecha, GetFiltroAsistencia ] ), TRUE );
                  if RecordCount > 0 then
                  begin
                       { Se va a barrer el dataset para eliminar los registros nuevos ( EXISTE = 0 ),
                         cuando no se tiene derecho de agregar tarjetas }
                       lPuedeAgregarTarjeta := ParamList.ParamByName('PuedeAgregarTarjeta').AsBoolean;
                       if not lPuedeAgregarTarjeta then
                       begin
                            while not EOF do
                            begin
                                 if ( cdsDataSet.FieldByName('EXISTE').AsInteger = 0 )then
                                 begin
                                      Delete;
                                 end
                                 else
                                     Next;
                            end;
                            MergeChangeLog;
                            First;
                       end;
                       { Se va a barrer el dataset para eliminar los registros que
                       no entran por la validacion del Bloqueo de Status }
                       if NOT lPuedeCambiarTarjeta then
                       begin
                            while not EOF do
                            begin
                                 if FieldByName('PE_STATUS').IsNull then
                                 begin
                                      ParamAsInteger( FQuery, 'Empleado', FieldByName('CB_CODIGO').AsInteger );
                                      ParamAsDate( FQuery, 'Fecha', dFecha );
                                      FQuery.Active := TRUE;
                                      eStatus := eStatusPeriodo( FQuery.FieldByName( 'RESULTADO' ).AsInteger );
                                      FQuery.Active := FALSE;
                                 end
                                 else
                                 begin
                                      eStatus := eStatusPeriodo( FieldByName('PE_STATUS').AsInteger );
                                 end;

                                 if ValidaStatusNomina( eStatus, eStatusLimite ) then
                                    Next
                                 else
                                     Delete;
                            end;
                            MergeChangeLog; //Para que no quede nada en el Delta.
                            First;
                       end;

                       while not EOF do
                       begin
                            if ( FieldByName( 'PE_STATUS' ).AsInteger > GetGlobalInteger( ZGlobalTress.K_LIMITE_MODIFICAR_ASISTENCIA ) ) then
                               // En la 1.3 se Cancela la generaci�n del Query, quizas se pudiera filtrar y devolver solo las tarjetas nuevas
                               Raise Exception.Create( 'La N�mina A La Que Pertenece la Tarjeta Del Empleado (' + FieldByName('CB_CODIGO').AsString + ') Est� ' + ObtieneElemento( lfStatusPeriodo, FieldByName( 'PE_STATUS' ).AsInteger ) )
                            else if StrVacio( FieldByName( 'HO_CODIGO' ).AsString ) then
                            begin
                                 if not Assigned( FTarjeta ) then
                                    InicializaInfoTurnos;
                                 Edit;
                                 with FTarjeta.GetTurnoHorario( FieldByName( 'CB_CODIGO' ).AsInteger, dFecha ) do
                                 begin
                                      FieldByName( 'AU_FECHA' ).AsDateTime := dFecha;
                                      FieldByName( 'HO_CODIGO' ).AsString := Horario;
                                      FieldByName( 'AU_STATUS' ).AsInteger := Ord( Status );
                                      FieldByName( 'AU_HOR_MAN' ).AsString := K_GLOBAL_NO;
                                      FieldByName( 'HORARIO_ANT').AsString := Horario;      // Validar Cambios
                                      FieldByName( 'STATUS_ANT' ).AsInteger := Ord( Status );
                                 end;
                                 FieldByName( 'AU_HORASCK' ).AsFloat := 0.00;
                                 FieldByName( 'AU_NUM_EXT' ).AsFloat := 0.00;
                            end;
                            // Defaults de Autorizaciones
                            if aDefaults[acExtras][0] then
                               AplicaDefault( acExtras, FieldByName( 'HRS_EXTRAS' ), FieldByName( 'M_HRS_EXTRAS' ), PuedeModificar( 'OK_HRS_EXTRAS' )  );
                            if aDefaults[acDescanso][0] then
                               AplicaDefault( acDescanso, FieldByName( 'DESCANSO' ), FieldByName( 'M_DESCANSO' ), PuedeModificar( 'OK_DESCANSO' ) );
                            if aDefaults[acConGoce][0] then
                               AplicaDefault( acConGoce, FieldByName( 'PER_CG' ), FieldByName( 'M_PER_CG' ), PuedeModificar( 'OK_PER_CG' ) );
                            if aDefaults[acConGoceEntrada][0] then
                               AplicaDefault( acConGoceEntrada, FieldByName( 'PER_CG_ENT' ), FieldByName( 'M_PER_CG_ENT' ), PuedeModificar( 'OK_PER_CG_ENT' ) );
                            if aDefaults[acSinGoce][0] then
                               AplicaDefault( acSinGoce, FieldByName( 'PER_SG' ), FieldByName( 'M_PER_SG' ), PuedeModificar( 'OK_PER_SG' ) );
                            if aDefaults[acSinGoceEntrada][0] then
                               AplicaDefault( acSinGoceEntrada, FieldByName( 'PER_SG_ENT' ), FieldByName( 'M_PER_SG_ENT' ), PuedeModificar( 'OK_PER_SG_ENT' ) );
                            if aDefaults[acPrepagFueraJornada][0] then
                               AplicaDefault( acPrepagFueraJornada, FieldByName( 'PRE_FUERA_JOR' ), FieldByName( 'M_PRE_FUERA_JOR' ), PuedeModificar( 'OK_PRE_FUERA_JOR' ) );
                            if aDefaults[acPrepagDentroJornada][0] then
                               AplicaDefault( acPrepagDentroJornada, FieldByName( 'PRE_DENTRO_JOR' ), FieldByName( 'M_PRE_DENTRO_JOR' ), PuedeModificar( 'OK_PRE_DENTRO_JOR' ) );

                            //Aplica Default de Comida
                            AplicaDefaultOut2Eat;

                            Next;
                       end;
                  end;
                  if Assigned( FTarjeta ) then   // Si se investigaron Turnos
                     DesPreparaInfoTurno;
                  //MergeChangeLog;    No se realiza para que los cambios queden en el delta
                  Result:= Data;
             end;
        end;
     finally
        FreeAndNil(FQuery);
        FreeAndNil( cdsDataSet );
     end;
end;

{ *****************  METODOS DE BITACORA DE ASISTENCIA ****************** }

procedure TdmServerSuper.BitacoraTarjeta( DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; const iEmpleado:Integer; const dFecha: TDate );
// Cuando entra a este Metodo FOldChecadas y FNewChecadas ya tienen valores asignados

   function GetAnterior( oCampo: TField ): Currency;
   begin
        if UpdateKind = ukInsert then
           Result := 0.0
        else
           Result := oCampo.OldValue;
   end;

begin
     with DeltaDS do
     begin
          BitacoraCambio( clbCambioHorario, iEmpleado, dFecha, 'Cambi� Horario de %s a %s',
                          CampoAsVar( FieldByName('HORARIO_ANT') ), CampoAsVar( FieldByName( 'HO_CODIGO' ) ) );
          BitacoraCambio( clbCambioTipoDia, iEmpleado, dFecha, 'Cambi� Tipo de D�a de %s a %s',
                          ObtieneElemento( lfStatusAusencia, CampoAsVar( FieldByName( 'STATUS_ANT' ) ) ),
                          ObtieneElemento( lfStatusAusencia, CampoAsVar( FieldByName( 'AU_STATUS' ) ) ) );
          BitacoraCambioDetalle( clbCambioFestivo, iEmpleado, dFecha, ObtieneElemento( lfStatusFestivos, CampoAsVar( FieldByName('FESTIVO_ANT') ) ),
                                 ObtieneElemento( lfStatusFestivos, CampoAsVar( FieldByName('AU_STA_FES') ) ), 'Cambi� criterio para indicar si es festivo' );

          BitacoraCambioDetalle( clbCambioTomoComida, iEmpleado, dFecha, ObtieneElemento( lfOut2Eat, CampoAsVar( FieldByName('COMIDA_ANT') ) ),
                                 ObtieneElemento( lfOut2Eat, CampoAsVar( FieldByName('AU_OUT2EAT') ) ), 'Cambi� criterio para indicar si tom� comida');

          BitacoraAutorizacion( clbAutoHorasExtras, iEmpleado, dFecha, '%s Aut. de %s Horas Extras',
                                GetAnterior( FieldByName( 'HRS_EXTRAS' ) ), CampoAsVar( FieldByName( 'HRS_EXTRAS' ) ),
                                CampoAsVar( FieldByName( 'M_HRS_EXTRAS' ) ) );
          BitacoraAutorizacion( clbAutoDescanso, iEmpleado, dFecha, '%s Aut. de %s Horas de Descanso Trab.',
                                GetAnterior( FieldByName( 'DESCANSO' ) ), CampoAsVar( FieldByName( 'DESCANSO' ) ),
                                CampoAsVar( FieldByName( 'M_DESCANSO' ) ) );
          BitacoraAutorizacion( clbAutoPermisoCG, iEmpleado, dFecha, '%s Aut. de %s Horas de Permiso CG',
                                GetAnterior( FieldByName( 'PER_CG' ) ), CampoAsVar( FieldByName( 'PER_CG' ) ),
                                CampoAsVar( FieldByName( 'M_PER_CG' ) ) );
          BitacoraAutorizacion( clbAutoPermisoCGEnt, iEmpleado, dFecha, '%s Aut. de %s Horas de Permiso CG Ent.',
                                GetAnterior( FieldByName( 'PER_CG_ENT' ) ), CampoAsVar( FieldByName( 'PER_CG_ENT' ) ),
                                CampoAsVar( FieldByName( 'M_PER_CG_ENT' ) ) );
          BitacoraAutorizacion( clbAutoPermisoSG, iEmpleado, dFecha, '%s Aut. de %s Horas de Permiso SG',
                                GetAnterior( FieldByName( 'PER_SG' ) ), CampoAsVar( FieldByName( 'PER_SG' ) ),
                                CampoAsVar( FieldByName( 'M_PER_SG' ) ) );
          BitacoraAutorizacion( clbAutoPermisoSGEnt, iEmpleado, dFecha, '%s Aut. de %s Horas de Permiso SG Ent.',
                                GetAnterior( FieldByName( 'PER_SG_ENT' ) ), CampoAsVar( FieldByName( 'PER_SG_ENT' ) ),
                                CampoAsVar( FieldByName( 'M_PER_SG_ENT' ) ) );
          BitacoraAutorizacion( clbAutoPrepFueraJor, iEmpleado, dFecha, '%s Aut. de %s Horas Prep. fuera de jornada',
                                GetAnterior( FieldByName( 'PRE_FUERA_JOR' ) ), CampoAsVar( FieldByName( 'PRE_FUERA_JOR' ) ),
                                CampoAsVar( FieldByName( 'M_PRE_FUERA_JOR' ) ) );
          BitacoraAutorizacion( clbAutoPrepDentroJor, iEmpleado, dFecha, '%s Aut. de %s Horas Prep. dentro de jornada',
                                GetAnterior( FieldByName( 'PRE_DENTRO_JOR' ) ), CampoAsVar( FieldByName( 'PRE_DENTRO_JOR' ) ),
                                CampoAsVar( FieldByName( 'M_PRE_DENTRO_JOR' ) ) );
          BitacoraChecadas( clbAjusteAsistencia, iEmpleado, dFecha, '%s Checadas de Asistencia' );

          BitacoraAgregar( clbAjusteAsistencia, UpdateKind, iEmpleado, dFecha, 'Agreg� Tarjeta Diaria' );


     end;
end;

procedure TdmServerSuper.BitacoraAgregar( const eClase: eClaseBitacora; UpdateKind: TUpdateKind; const iEmpleado: Integer; const dFecha: TDate; const sDescripcion: String);
begin
     if ( UpdateKind = ukInsert ) then
        IncluirBitacora( eClase, iEmpleado, dFecha, sDescripcion, VACIO  );
end;

procedure TdmServerSuper.BitacoraCambio(const eClase: eClaseBitacora; const iEmpleado: Integer; const dFecha: TDate;
          const sMensaje, sOld, sNew: String);
begin
     if sOld <> sNew then
        IncluirBitacora( eClase, iEmpleado, dFecha, Format( sMensaje, [ sOld, sNew ] ), VACIO );
end;

procedure TdmServerSuper.BitacoraCambioDetalle(const eClase: eClaseBitacora; const iEmpleado: Integer;const dFecha: TDate;
          const sOld, sNew, sDescripcion: String);
begin
     if sOld <> sNew then
        IncluirBitacora( eClase, iEmpleado, dFecha, sDescripcion, Format( 'Antes: %s %sNuevo: %s', [ sOld, CR_LF, sNew ] ) );
end;

procedure TdmServerSuper.BitacoraAutorizacion( const eClase: eClaseBitacora; const iEmpleado: Integer;
          const dFecha: TDate; const sMensaje: String; const ValorAnt, ValorNew: Currency;
          const sMotivo: String );
const
     NOTA_MOTIVO = 'Motivo de Autorizacion : %s';
     NOTA_VACIO = ' ';
var
   sAccion, sHoras, sNota: String;
begin
     if ( ValorAnt <> ValorNew ) then
     begin
          sNota := NOTA_VACIO;
          if ( ValorAnt = 0 ) then
          begin
             sAccion := 'Agreg�';
             sHoras := FormatFloat('###,##0.00', ValorNew );
             sNota := Format( NOTA_MOTIVO, [ sMotivo ] );
          end
          else if ( ValorNew = 0 ) then
          begin
             sAccion := 'Borr�';
             sHoras := FormatFloat('###,##0.00', ValorAnt );
          end
          else
          begin
             sAccion := 'Cambi�';
             sHoras := FormatFloat('###,##0.00', ValorAnt ) + ' a ' +
                       FormatFloat('###,##0.00', ValorNew );
             sNota := Format( NOTA_MOTIVO, [ sMotivo ] );
          end;
          IncluirBitacora( eClase, iEmpleado, dFecha,
                           Format( sMensaje, [ sAccion, sHoras ] ), sNota );
     end;
end;

procedure TdmServerSuper.BitacoraChecadas(const eClase: eClaseBitacora; const iEmpleado: Integer; const dFecha: TDate;
          const sMensaje: String);
var
   sAccion, sChecadas : String;

   function ChecadasDiferentes( OldLista, NewLista : TStringList ): Boolean;
   var
      i : Integer;
   begin
        Result := FALSE;
        if ( OldLista.Count <> NewLista.Count ) then
           Result := True
        else if ( OldLista.Count > 0 ) then
           for i := 0 to OldLista.Count - 1 do
               if ( NewLista.IndexOf( OldLista.Strings[i] ) < 0 ) then
                  Result := TRUE;
   end;

   function SumaMinutos( oLista : TStringList ): Integer;
   var
      i : Integer;
   begin
        Result := 0;
        with oLista do
             if ( Count > 0 ) then
                for i := 0 to Count - 1 do
                    Result := Result + aMinutos( Strings[i] );
   end;

   function ListaChecadas( oLista : TStringList ): String;
   var
      i : Integer;
   begin
        Result := VACIO;
        with oLista do
             if ( Count > 0 ) then
                for i := 0 to Count - 1 do
                    if aMinutos( Strings[i] ) > 0 then
                       Result := ConcatString( Result, FormateaHora( Strings[i] ), ',' );
   end;

begin
     if ( ChecadasDiferentes( FOldChecadas, FNewChecadas ) ) then
     begin
          if ( SumaMinutos( FOldChecadas ) = 0 ) then
          begin
               sAccion := 'Agreg�';
               sChecadas := ListaChecadas( FNewChecadas );
          end
          else if ( SumaMinutos ( FNewChecadas ) = 0 ) then
          begin
               sAccion := 'Borr�';
               sChecadas := ListaChecadas( FOldChecadas );
          end
          else
          begin
               sAccion := 'Cambi�';
               sChecadas := 'Antes :' + CR_LF +
                            ListaChecadas( FOldChecadas ) + CR_LF +
                            'Despu�s :' + CR_LF +
                            ListaChecadas( FNewChecadas );
          end;
          IncluirBitacora( eClase, iEmpleado, dFecha,
                           Format( sMensaje, [ sAccion ] ), sChecadas );
     end;
end;

procedure TdmServerSuper.IncluirBitacora(const eClase: eClaseBitacora; const iEmpleado: Integer; const dFecha: TDate;
          const sMensaje: TBitacoraTexto; const sTexto: String);
begin
     with oZetaProvider do
     begin
          if Log = nil then
             EscribeBitacora( tbNormal, eClase, iEmpleado, dFecha, sMensaje, sTexto )
          else
             Log.Evento( eClase, iEmpleado, dFecha, sMensaje, sTexto );
     end;
end;

function TdmServerSuper.GetAutorizaPrenomina(Empresa, Parametros: OleVariant): OleVariant;
var
   sFecha: string;
begin
     InitLog(Empresa,'GetAutorizaPrenomina');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               sFecha := ZetaCommonTools.DateToStrSQLC( ParamByName( 'Fecha' ).AsDate );
               Result := OpenSQL( Empresa, Format( GetScript( eAutorizaPrenomina ), [ sFecha,
                                                                                       UsuarioActivo,
                                                                                       ParamByName( 'Year' ).AsInteger,
                                                                                       ParamByName( 'Tipo' ).AsInteger,
                                                                                       ParamByName( 'Numero' ).AsInteger ] ), True );
         end;
     end;
     EndLog;SetComplete;
end;

function TdmServerSuper.GrabaAutorizaPrenomina(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaAutorizaPrenomina');
     SetTablaInfo( eTablaNomina );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.AfterUpdateRecord := AfterUpdateAutorizacionPreNomina;
          Result := GrabaTablaGrid( Empresa, oDelta, ErrorCount );
     end;
     EndLog;SetComplete;
end;

procedure TdmServerSuper.AfterUpdateAutorizacionPreNomina(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   sAutorizacion,sDescripPeriodo,sText:string;
begin
     with oZetaProvider do
     begin
          with DeltaDS do
          begin
                sAutorizacion := 'Autoriz�';
                if CampoAsVar( FieldByName( 'NO_SUP_OK' ) ) = 0 then
                   sAutorizacion := 'Desautoriz�';

                sDescripPeriodo :=  IntToStr( CampoAsVar( FieldByName( 'PE_NUMERO' ) ) ) +' '+ ObtieneElemento( lfTipoPeriodo, CampoAsVar( FieldByName( 'PE_TIPO' ) ) ) +' del '+IntToStr( CampoAsVar( FieldByName( 'PE_YEAR' ) ) );
                sText := 'El usuario : '+IntToStr( UsuarioActivo)+' '+sAutorizacion+' la Pre-N�mina a las:'+CampoAsVar( FieldByName( 'NO_HOR_OK' ) ) +' del D�a:'+ DateToStr(CampoAsVar( FieldByName( 'NO_FEC_OK' ) ) );

                EscribeBitacora( tbNormal,
                                 clbAsisAutorizaPrenom,
                                 CampoAsVar( FieldByName( 'CB_CODIGO' ) ),
                                 Date(),
                                 Format('Se %s la Pre-N�mina %s ', [sAutorizacion,sDescripPeriodo] ),
                                 sText
                                 );
          end;
     end;
end;

function TdmServerSuper.GetSesion(Empresa: OleVariant; iFolio: Integer): OleVariant;
begin
     InitLog(Empresa,'GetSesion');
     SetTablaInfo( eSesion );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '( SE_FOLIO = %d )', [ iFolio ] );
          Result := GetTabla( Empresa );
     end;
     EndLog;SetComplete;
end;

function TdmServerSuper.GetHisCursos(Empresa: OleVariant; Empleado: Integer): OleVariant;
begin
     InitLog(Empresa,'GetHisCursos');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          Result := OpenSQL ( Empresa, Format( GetScript( eHisCursos ), [ Empleado ] ), True );
     end;
     EndLog;SetComplete;
end;

{Obtiene las tarjetas del periodo para modificarlas --------------------------------------------------- }
function TdmServerSuper.GetTarjetasPeriodo(Empresa,Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'GetTarjetasPeriodo');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          Result := TarjetasPeriodoBuildDataSet;
     end;
     EndLog;SetComplete;
end;

{ Construye las tarjetas del periodo para modificarlas ------------------------------------------------ }

function TdmServerSuper.TarjetasPeriodoBuildDataSet : OleVariant;
var
   cdsDataSet : TClientDataSet;
   iUsuario : Integer;
   dFechaInicial : TDateTime;
   dFechaFinal :TDateTime;
   dFechaTemp :TDate;
   iEmpleado :Integer;
   lPuedeModificarAnteriores,lEsSuper:Boolean;
   dFechaLimite:TDateTime;
   lEsFechaLimite :Boolean;
   sSuperDentro, sFiltroNomina :string;
   FListaSupervisor: TZetaCursor;

   procedure InicializaInfoTurnos;
   begin
        with oZetaProvider do
        begin
             InitRitmos;
             InitTarjeta;
             FTarjeta.GetTurnoHorarioBegin( dFechaInicial, dFechaFinal );
             oZetaCreator.Queries.GetStatusActEmpleadoBegin;
        end;
   end;
    {$ifdef CAMBIO_TNOM}
   procedure InicializaInfoNomina;
   begin
        FTarjeta.GetTipoNomBegin;
   end;

   procedure DespreparaInfoNomina;
   begin
        FTarjeta.GetTipoNomEnd;
   end;
   {$endif}

   procedure DesPreparaInfoTurno;
   begin
        oZetaCreator.Queries.GetStatusActEmpleadoEnd;
        FTarjeta.GetTurnoHorarioEnd;
        ClearTarjeta;
   end;

   //Obtiene las fecha inicial y final de emleados con periodos irregulares
   procedure ObtieneFechasDesfasadas(var dInicial,dFinal:TDate);
   var
      FDatosEmpleadoPrenomina: TZetaCursor;
   begin
        with oZetaProvider do
        begin
             GetDatosPeriodo;
             if ( DatosPeriodo.Inicio <> DatosPeriodo.InicioAsis ) or
                ( DatosPeriodo.Fin <> DatosPeriodo.FinAsis ) then     // Si no hay desfase no se puede presentar el cambio de fechas de inicio y fin
             begin
                  FDatosEmpleadoPrenomina := CreateQuery;
                  try
                     if AbreQueryScript( FDatosEmpleadoPrenomina, Format( GetScript ( eEmpleadoFechaIng_Baja), [ iEmpleado ] ) ) then
                     begin
                          with FDatosEmpleadoPrenomina do
                          begin
                               if ( not IsEmpty ) then
                               begin
                                    ZetaCommonTools.EsBajaenPeriodo( FieldByName( 'CB_FEC_ING' ).AsDateTime,
                                                                     FieldByName( 'CB_FEC_BAJ' ).AsDateTime,
                                                                     DatosPeriodo, dInicial,dFinal );       // Si la baja cae en el periodo revisa si hay que cambiar fechas
                               end;
                          end;
                     end;
                  finally
                     FreeAndNil( FDatosEmpleadoPrenomina );
                  end;
             end;
        end;
   end;

   function PuedeModificar( const sAprobacion: string ): Boolean;
   begin
        with oZetaProvider do
        begin
             if GetGlobalBooleano( K_GLOBAL_APROBAR_AUTORIZACIONES ) then
             begin
                  Result := ParamList.ParamByName( 'APROBARAUTORIZACIONES' ).AsBoolean;
                  if NOT Result then
                  begin
                       with cdsDataset do
                            Result := (FieldByName( sAprobacion ).AsInteger = 0);
                  end;
             end
             else
             begin
                  Result := TRUE;
             end;
        end;
   end;

   //Verficar la fecha limite de poder modificar tarjetas
   function VerificarFechaLimite:Boolean;
   begin
        Result := ( Not lEsFechaLimite ) or // Si no se tiene configurado fecha limite
                  ( dFechaTemp > dFechaLimite ) or // fecha posterior al limite
                  ( lPuedeModificarAnteriores );  // Puede modificar tarjetas anteriores
   end;

   function EsEmpleadoDelSupervisor: Boolean;
   begin
        with oZetaProvider, FListaSupervisor do
        begin
             Active := FALSE;
             ParamAsDate( FListaSupervisor, 'FECHA', dFechaTemp );
             Active := TRUE;
             Result := ( FieldByName('SUPER_DENTRO').AsInteger > 0 );
        end;
   end;

   procedure PreparaListaDelSupervisor;
   begin
        with oZetaProvider do
        begin
             FListaSupervisor := CreateQuery( Format( GetScript ( eListaSupervisor ), [ iUsuario, iEmpleado ] ) );
        end;
   end;

   procedure DesPreparaListaDelSupervisor;
   begin
        FreeAndNil( FListaSupervisor );
   end;

var
   lPuedeAgregarTarjeta: Boolean;
   dInicial,dFinal :TDate;
begin
     cdsDataSet := TClientDataSet.Create( Self );
     try
        with oZetaProvider do
        begin
             InitGlobales;
             //Cargar parametros
             dFechaInicial := ParamList.ParamByName( 'FechaIni' ).AsDateTime;
             dFechaFinal := ParamList.ParamByName( 'FechaFin' ).AsDateTime;
             iEmpleado := ParamList.ParamByName( 'Empleado' ).AsInteger;

             lPuedeAgregarTarjeta := ParamList.ParamByName('PuedeAgregarTarjeta').AsBoolean;

             lEsFechaLimite := ParamList.ParamByName('EsFechaLimite').AsBoolean;
             dFechaLimite := ParamList.ParamByName( 'FechaLimite' ).AsDateTime;
             lPuedeModificarAnteriores := ParamList.ParamByName( 'PuedeModificarAnteriores' ).AsBoolean;

             lEsSuper := ParamList.ParamByName( 'EsSupervisores' ).AsBoolean;

             //Ver las fechas desfasadas ...
             dInicial := dFechaInicial;
             dFinal := dFechaFinal;
             ObtieneFechasDesfasadas( dInicial, dFinal );

             //Columna de supervisores para ver si esta dentro de la lista de colaboradores en esa fecha
             if lEsSuper then
             begin
                  iUsuario := ParamList.ParamByName( 'UsuarioActivo' ).AsInteger;
                  {$ifdef MSSQL}
                  sSuperDentro := Format( ' , ( dbo.SP_LISTA_DENTRO( AUSENCIA.AU_FECHA, %d ,AUSENCIA.CB_CODIGO ) ) SUPER_DENTRO ',[ iUsuario ] );
                  {$endif}
                  {$ifdef INTERBASE}
                  sSuperDentro := Format( ' , ( select RESULTADO from SP_LISTA_DENTRO( AUSENCIA.AU_FECHA, %d ,AUSENCIA.CB_CODIGO )) SUPER_DENTRO ',[ iUsuario ] );
                  {$endif}
             end
             else
             begin
                 sSuperDentro := VACIO;
             end;

             {$ifdef CAMBIO_TNOM}
             sFiltroNomina:= Format( ' AND ( AUSENCIA.CB_NOMINA = %d )', [ Ord( oZetaProvider.DatosPeriodo.Tipo ) ] );
             {$endif}

             with cdsDataSet do
             begin
                  Data:= OpenSQL( EmpresaActiva, Format( GetScript( eGridTarjetasPeriodo ),
                                  [ iEmpleado,DateToStrSQLC( dInicial ),DateToStrSQLC( dFinal ), sSuperDentro,EntreComillas(K_I_INGRESO),EntreComillas(K_I_BAJA)
                                  {$ifdef CAMBIO_TNOM}
                                  ,sFiltroNomina
                                  {$endif}
                                   ] ), TRUE );
                 { Se van a gregar aquellas tarjetas del periodo que no se han registrado checadas }
                 if lPuedeAgregarTarjeta then
                 begin
                      PreparaListaDelSupervisor;
                      try
                         dFechaTemp := dInicial;
                         while dFechaTemp <= dFinal  do
                         begin
                              if ( not ( Locate ( 'AU_FECHA', VarArrayOf([dFechaTemp]) , [] ) ) ) and (  VerificarFechaLimite  ) and
                                 ( ( not lEsSuper ) or ( EsEmpleadoDelSupervisor ) ) then
                              begin
                                   if not Assigned( FTarjeta ) then
                                   begin
                                        InicializaInfoTurnos;
                                        {$ifdef CAMBIO_TNOM}
                                        InicializaInfoNomina;
                                        {$endif}
                                   end;
                                   if ( oZetaCreator.Queries.GetStatusActEmpleado( iEmpleado, dFechaTemp ) = steEmpleado )
                                   {$ifdef CAMBIO_TNOM}
                                      and ( ( FTarjeta.GetTipoNomina( iEmpleado, dFechaTemp ) = DatosPeriodo.Tipo ) )
                                   {$endif} then
                                   begin
                                        Append;
                                        with FTarjeta.GetTurnoHorario( iEmpleado, dFechaTemp ) do
                                        begin
                                             FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                                             FieldByName( 'AU_FECHA' ).AsDateTime := dFechaTemp;
                                             FieldByName( 'HO_CODIGO' ).AsString := Horario;
                                             FieldByName( 'AU_STATUS' ).AsInteger := Ord( Status );
                                             FieldByName( 'AU_HOR_MAN' ).AsString := K_GLOBAL_NO;
                                             FieldByName( 'HORARIO_ANT').AsString := Horario;      // Validar Cambios
                                             FieldByName( 'STATUS_ANT' ).AsInteger := Ord( Status );
                                        end;
                                        FieldByName( 'AU_HORASCK' ).AsFloat := 0.00;
                                        FieldByName( 'AU_NUM_EXT' ).AsFloat := 0.00;
                                        if lEsSuper then
                                           FieldByName( 'SUPER_DENTRO' ).AsInteger := 1;
                                   end;
                              end;
                              //Agregar un D�a
                              dFechaTemp := dFechaTemp + 1;
                         end;
                      finally
                             DesPreparaListaDelSupervisor;
                      end;
                 end;
                 if Assigned( FTarjeta ) then   // Si se investigaron Turnos
                 begin
                      {$ifdef CAMBIO_TNOM}
                      DespreparaInfoNomina;
                      {$endif}
                      DesPreparaInfoTurno;
                 end;
                  //MergeChangeLog;    No se realiza para que los cambios queden en el delta
                  Result:= Data;
             end;
        end;
     finally
        FreeAndNil( cdsDataSet );
     end;
 end;


 {Graba las tarjeta del periodo completo }
function TdmServerSuper.GrabaTarjetaPeriodo(Empresa,Parametros: OleVariant; out iErrorCount: Integer;oDelta: OleVariant): OleVariant;
begin
     InitLog(Empresa,'GrabaTarjetaPeriodo');
     //SetTablaInfo(eAusencia);
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          Result := GrabarListaTarjetasAsistencia( False, oDelta,iErrorCount );
     end;
     EndLog;SetComplete;
end;

procedure TdmServerSuper.BeforeUpdateAsistenciaPeriodo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     FCambioAplicado:= FALSE;
     with DeltaDS, oZetaProvider do
     begin
          if ( UpdateKind = ukInsert )  then
          begin
//               IncluirBitacora( clbAjusteAsistencia, CampoAsVar(FieldByName('CB_CODIGO')), CampoAsVar(FieldByName('AU_FECHA')), 'Modific� Tarjeta Diaria',VACIO  );
               SetInfoTarjetaGlobal( FDataSet, DeltaDS );
               Ejecuta( FDataSet );
               FCambioAplicado:= TRUE;
          end
          else
          begin
               if ( UpdateKind = ukModify ) then
               begin
                    SetInfoTarjetaGlobal( FDataSetAdic, DeltaDS );
                    Ejecuta( FDataSetAdic );
                    FCambioAplicado:= TRUE;
               end;
          end;

     end;
     Applied := TRUE;
end;

function TdmServerSuper.GetFechaLimite(Empresa: OleVariant): TDateTime;
begin
     InitLog(Empresa,'GetFechaLimite');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          InitGlobales;
          Result:= GetGlobalDate( K_GLOBAL_FECHA_LIMITE );
     end;
     EndLog;SetComplete;
end;


function TdmServerSuper.GetEvaluacionDiaria(Empresa, Parametros: OleVariant): OleVariant;
var
   sFecha: string;
   dFechaConsulta: TDate;

   procedure InicializaEvaluacionDiaria;
   begin
        {$ifdef COMMSCOPE}
        with oZetaProvider do
        begin
             FDataSet := CreateQuery( GetScript( eInicializaEvalDiaria ) );
             try
                ParamAsDateTime( FDataSet, 'Fecha', dFechaConsulta );
                ParamAsInteger( FDataSet, 'Usuario', UsuarioActivo );
                with FDataSet do
                begin
                     EmpiezaTransaccion;
                     try
                        Ejecuta( FDataset );
                        TerminaTransaccion( True );
                     except
                           on Error: Exception do
                           begin
                                TerminaTransaccion( False );
                           end;
                     end;
                     Active := False;
                end;
             finally
                    FreeAndNil( FDataset );
             end;
        end;
        {$endif}
   end;

begin
     {$ifdef COMMSCOPE}
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               sFecha := ZetaCommonTools.DateToStrSQLC( ParamByName( 'Fecha' ).AsDate );
               dFechaConsulta := ParamByName( 'Fecha' ).AsDate;
               if ( dFechaConsulta <> NullDateTime ) then
                  InicializaEvaluacionDiaria;
               Result := OpenSQL( Empresa, Format( GetScript( eEvaluacionDiaria ), [ sFecha,
                                                                                     UsuarioActivo,
                                                                                     sFecha ] ), True );
         end;
     end;
     {$endif}
     SetComplete;
end;

function TdmServerSuper.GrabaEvaluacionDiaria(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     {$ifdef COMMSCOPE}
     SetTablaInfo( eEvaluacionDiariaGrabar );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := GrabaTablaGrid( Empresa, oDelta, ErrorCount );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerSuper.GetFechaEvaluacionDiaria(Empresa: OleVariant): TDateTime;
begin
     {$ifdef COMMSCOPE}
     InitLog(Empresa,'GetFechaLimite');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          InitGlobales;
          Result:= GetGlobalDate( K_GLOBAL_FECHA_EVALUACION_DIARIA );
     end;
     EndLog;
     {$endif}
     SetComplete;
end;




{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerSuper,
    Class_dmServerSuper, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.





