unit Reportes_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 4/27/2007 8:41:03 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20\MTS\Reportes.tlb (1)
// LIBID: {B81B3ED0-88CE-11D3-8DEC-0050DA04EAA0}
// LCID: 0
// Helpfile: 
// HelpString: Tress Reportes
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ReportesMajorVersion = 1;
  ReportesMinorVersion = 0;

  LIBID_Reportes: TGUID = '{B81B3ED0-88CE-11D3-8DEC-0050DA04EAA0}';

  IID_IdmServerReportes: TGUID = '{B81B3ED1-88CE-11D3-8DEC-0050DA04EAA0}';
  CLASS_dmServerReportes: TGUID = '{B81B3ED3-88CE-11D3-8DEC-0050DA04EAA0}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerReportes = interface;
  IdmServerReportesDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerReportes = IdmServerReportes;


// *********************************************************************//
// Interface: IdmServerReportes
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B81B3ED1-88CE-11D3-8DEC-0050DA04EAA0}
// *********************************************************************//
  IdmServerReportes = interface(IAppServer)
    ['{B81B3ED1-88CE-11D3-8DEC-0050DA04EAA0}']
    function GetReportes(Empresa: OleVariant; iClasifActivo: Integer; iFavoritos: Integer; 
                         iSuscripciones: Integer; const sClasificaciones: WideString; 
                         lVerConfidencial: WordBool): OleVariant; safecall;
    function GetEditReportes(Empresa: OleVariant; iReporte: Integer; var CampoRep: OleVariant): OleVariant; safecall;
    function GetLookUpReportes(Empresa: OleVariant; lVerConfidencial: WordBool): OleVariant; safecall;
    function GrabaReporte(Empresa: OleVariant; oDelta: OleVariant; oCampoRep: OleVariant; 
                          out ErrorCount: Integer; var iReporte: Integer): OleVariant; safecall;
    function GetEscogeReporte(Empresa: OleVariant; Entidad: Integer; Tipo: Integer; 
                              lVerConfidencial: WordBool): OleVariant; safecall;
    function CampoRepFiltros(Empresa: OleVariant; Reporte: Integer): OleVariant; safecall;
    function BorraReporte(Empresa: OleVariant; iReporte: Integer): OleVariant; safecall;
    function GrabaPoliza(Empresa: OleVariant; Delta: OleVariant; CampoRep: OleVariant; 
                         out ErrorCount: Integer; var iReporte: Integer): OleVariant; safecall;
    function GetSuscripcion(Empresa: OleVariant; iFrecuencia: Integer; const sUsuarios: WideString; 
                            const sReportes: WideString; var Usuarios: OleVariant): OleVariant; safecall;
    function BorraFavoritos(Empresa: OleVariant; iReporte: Integer): OleVariant; safecall;
    function AgregaFavoritos(Empresa: OleVariant; iReporte: Integer): Integer; safecall;
    function GetEscogeReportes(Empresa: OleVariant; const Entidades: WideString; 
                               const Tipos: WideString; lVerConfidencial: WordBool): OleVariant; safecall;
    function GetPlantilla(const Nombre: WideString): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerReportesDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B81B3ED1-88CE-11D3-8DEC-0050DA04EAA0}
// *********************************************************************//
  IdmServerReportesDisp = dispinterface
    ['{B81B3ED1-88CE-11D3-8DEC-0050DA04EAA0}']
    function GetReportes(Empresa: OleVariant; iClasifActivo: Integer; iFavoritos: Integer; 
                         iSuscripciones: Integer; const sClasificaciones: WideString; 
                         lVerConfidencial: WordBool): OleVariant; dispid 1;
    function GetEditReportes(Empresa: OleVariant; iReporte: Integer; var CampoRep: OleVariant): OleVariant; dispid 2;
    function GetLookUpReportes(Empresa: OleVariant; lVerConfidencial: WordBool): OleVariant; dispid 3;
    function GrabaReporte(Empresa: OleVariant; oDelta: OleVariant; oCampoRep: OleVariant; 
                          out ErrorCount: Integer; var iReporte: Integer): OleVariant; dispid 4;
    function GetEscogeReporte(Empresa: OleVariant; Entidad: Integer; Tipo: Integer; 
                              lVerConfidencial: WordBool): OleVariant; dispid 5;
    function CampoRepFiltros(Empresa: OleVariant; Reporte: Integer): OleVariant; dispid 10;
    function BorraReporte(Empresa: OleVariant; iReporte: Integer): OleVariant; dispid 6;
    function GrabaPoliza(Empresa: OleVariant; Delta: OleVariant; CampoRep: OleVariant; 
                         out ErrorCount: Integer; var iReporte: Integer): OleVariant; dispid 11;
    function GetSuscripcion(Empresa: OleVariant; iFrecuencia: Integer; const sUsuarios: WideString; 
                            const sReportes: WideString; var Usuarios: OleVariant): OleVariant; dispid 7;
    function BorraFavoritos(Empresa: OleVariant; iReporte: Integer): OleVariant; dispid 8;
    function AgregaFavoritos(Empresa: OleVariant; iReporte: Integer): Integer; dispid 9;
    function GetEscogeReportes(Empresa: OleVariant; const Entidades: WideString; 
                               const Tipos: WideString; lVerConfidencial: WordBool): OleVariant; dispid 12;
    function GetPlantilla(const Nombre: WideString): OleVariant; dispid 13;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerReportes provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerReportes exposed by              
// the CoClass dmServerReportes. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerReportes = class
    class function Create: IdmServerReportes;
    class function CreateRemote(const MachineName: string): IdmServerReportes;
  end;

implementation

uses ComObj;

class function CodmServerReportes.Create: IdmServerReportes;
begin
  Result := CreateComObject(CLASS_dmServerReportes) as IdmServerReportes;
end;

class function CodmServerReportes.CreateRemote(const MachineName: string): IdmServerReportes;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerReportes) as IdmServerReportes;
end;

end.
