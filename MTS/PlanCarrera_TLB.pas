unit PlanCarrera_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 12/1/2005 1:16:34 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20\MTS\PlanCarrera.tlb (1)
// LIBID: {B5E33B61-653A-4A25-B6D9-AC84F0072F74}
// LCID: 0
// Helpfile: 
// HelpString: Tress Plan de Carrera
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  PlanCarreraMajorVersion = 1;
  PlanCarreraMinorVersion = 0;

  LIBID_PlanCarrera: TGUID = '{B5E33B61-653A-4A25-B6D9-AC84F0072F74}';

  IID_IdmServerPlanCarrera: TGUID = '{B710B62A-CA95-4591-93F6-E80AC0C84F0A}';
  CLASS_dmServerPlanCarrera: TGUID = '{CC4F552B-3686-412B-80AD-0AC8DC40C030}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerPlanCarrera = interface;
  IdmServerPlanCarreraDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerPlanCarrera = IdmServerPlanCarrera;


// *********************************************************************//
// Interface: IdmServerPlanCarrera
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B710B62A-CA95-4591-93F6-E80AC0C84F0A}
// *********************************************************************//
  IdmServerPlanCarrera = interface(IAppServer)
    ['{B710B62A-CA95-4591-93F6-E80AC0C84F0A}']
    function GetCatCompetencias(Empresa: OleVariant): OleVariant; safecall;
    function GetCatCalificaciones(Empresa: OleVariant): OleVariant; safecall;
    function GetCatDimensiones(Empresa: OleVariant): OleVariant; safecall;
    function GetCatAcciones(Empresa: OleVariant): OleVariant; safecall;
    function GetCatNiveles(Empresa: OleVariant): OleVariant; safecall;
    function GetCatFamilias(Empresa: OleVariant): OleVariant; safecall;
    function GetCatPuestos(Empresa: OleVariant): OleVariant; safecall;
    function GetCatHabilidades(Empresa: OleVariant): OleVariant; safecall;
    function GrabaCatCompetencias(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaCatCalificaciones(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaCatDimensiones(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaCatAcciones(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetNiveles(Empresa: OleVariant; const Nivel: WideString; out oNivelDime: OleVariant): OleVariant; safecall;
    function GrabaCatNiveles(Empresa: OleVariant; oDelta: OleVariant; oNivelDim: OleVariant; 
                             out ErrorCount: Integer; out oResultNivelDim: OleVariant): OleVariant; safecall;
    function BorraNivel(Empresa: OleVariant; const Nivel: WideString; out ErrorCount: Integer): OleVariant; safecall;
    function GetFamilia(Empresa: OleVariant; const Familia: WideString; out oCompFam: OleVariant): OleVariant; safecall;
    function GrabaCatFamilias(Empresa: OleVariant; oDelta: OleVariant; oCompFam: OleVariant; 
                              out ErrorCount: Integer; out oResultCompFam: OleVariant): OleVariant; safecall;
    function BorraFamilia(Empresa: OleVariant; const Familia: WideString; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaNiveles(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaFamilias(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetPuestos(Empresa: OleVariant; const Puesto: WideString; 
                        out oCompetenciaPuesto: OleVariant; out oPuestoDimension: OleVariant): OleVariant; safecall;
    function GrabaCatPuestos(Empresa: OleVariant; oDelta: OleVariant; 
                             oCompetenciaPuesto: OleVariant; oPuestoDimension: OleVariant; 
                             out ErrorCount: Integer; out oResultCompPto: OleVariant; 
                             out oResultPtoDim: OleVariant): OleVariant; safecall;
    function GrabaCompetencia(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetCatCompetencia(Empresa: OleVariant; const Competencia: WideString; 
                               out oCompetenciaMApa: OleVariant; 
                               out oCompetenciaCalifica: OleVariant): OleVariant; safecall;
    function GrabaCatCompetencia(Empresa: OleVariant; oDelta: OleVariant; 
                                 oCompetenciaMApa: OleVariant; oCompetenciaCalifica: OleVariant; 
                                 out ErrorCount: Integer; out oResultMapa: OleVariant; 
                                 out oResultCalifica: OleVariant): OleVariant; safecall;
    function BorraCompetencia(Empresa: OleVariant; const Competencia: WideString; 
                              out ErrorCount: Integer): OleVariant; safecall;
    function GetAccionesComp(Empresa: OleVariant; const Accion: WideString; out oCompAcc: OleVariant): OleVariant; safecall;
    function GrabaAccionesComp(Empresa: OleVariant; oDelta: OleVariant; oCompAcc: OleVariant; 
                               out ErrorCount: Integer; out oResultCompAcc: OleVariant): OleVariant; safecall;
    function BorraAccion(Empresa: OleVariant; const Accion: WideString; out ErrorCount: Integer): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerPlanCarreraDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B710B62A-CA95-4591-93F6-E80AC0C84F0A}
// *********************************************************************//
  IdmServerPlanCarreraDisp = dispinterface
    ['{B710B62A-CA95-4591-93F6-E80AC0C84F0A}']
    function GetCatCompetencias(Empresa: OleVariant): OleVariant; dispid 33;
    function GetCatCalificaciones(Empresa: OleVariant): OleVariant; dispid 34;
    function GetCatDimensiones(Empresa: OleVariant): OleVariant; dispid 35;
    function GetCatAcciones(Empresa: OleVariant): OleVariant; dispid 36;
    function GetCatNiveles(Empresa: OleVariant): OleVariant; dispid 37;
    function GetCatFamilias(Empresa: OleVariant): OleVariant; dispid 38;
    function GetCatPuestos(Empresa: OleVariant): OleVariant; dispid 39;
    function GetCatHabilidades(Empresa: OleVariant): OleVariant; dispid 40;
    function GrabaCatCompetencias(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 41;
    function GrabaCatCalificaciones(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 42;
    function GrabaCatDimensiones(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 43;
    function GrabaCatAcciones(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 44;
    function GetNiveles(Empresa: OleVariant; const Nivel: WideString; out oNivelDime: OleVariant): OleVariant; dispid 45;
    function GrabaCatNiveles(Empresa: OleVariant; oDelta: OleVariant; oNivelDim: OleVariant; 
                             out ErrorCount: Integer; out oResultNivelDim: OleVariant): OleVariant; dispid 46;
    function BorraNivel(Empresa: OleVariant; const Nivel: WideString; out ErrorCount: Integer): OleVariant; dispid 47;
    function GetFamilia(Empresa: OleVariant; const Familia: WideString; out oCompFam: OleVariant): OleVariant; dispid 48;
    function GrabaCatFamilias(Empresa: OleVariant; oDelta: OleVariant; oCompFam: OleVariant; 
                              out ErrorCount: Integer; out oResultCompFam: OleVariant): OleVariant; dispid 49;
    function BorraFamilia(Empresa: OleVariant; const Familia: WideString; out ErrorCount: Integer): OleVariant; dispid 50;
    function GrabaNiveles(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 51;
    function GrabaFamilias(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 52;
    function GetPuestos(Empresa: OleVariant; const Puesto: WideString; 
                        out oCompetenciaPuesto: OleVariant; out oPuestoDimension: OleVariant): OleVariant; dispid 53;
    function GrabaCatPuestos(Empresa: OleVariant; oDelta: OleVariant; 
                             oCompetenciaPuesto: OleVariant; oPuestoDimension: OleVariant; 
                             out ErrorCount: Integer; out oResultCompPto: OleVariant; 
                             out oResultPtoDim: OleVariant): OleVariant; dispid 54;
    function GrabaCompetencia(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 55;
    function GetCatCompetencia(Empresa: OleVariant; const Competencia: WideString; 
                               out oCompetenciaMApa: OleVariant; 
                               out oCompetenciaCalifica: OleVariant): OleVariant; dispid 56;
    function GrabaCatCompetencia(Empresa: OleVariant; oDelta: OleVariant; 
                                 oCompetenciaMApa: OleVariant; oCompetenciaCalifica: OleVariant; 
                                 out ErrorCount: Integer; out oResultMapa: OleVariant; 
                                 out oResultCalifica: OleVariant): OleVariant; dispid 57;
    function BorraCompetencia(Empresa: OleVariant; const Competencia: WideString; 
                              out ErrorCount: Integer): OleVariant; dispid 58;
    function GetAccionesComp(Empresa: OleVariant; const Accion: WideString; out oCompAcc: OleVariant): OleVariant; dispid 59;
    function GrabaAccionesComp(Empresa: OleVariant; oDelta: OleVariant; oCompAcc: OleVariant; 
                               out ErrorCount: Integer; out oResultCompAcc: OleVariant): OleVariant; dispid 61;
    function BorraAccion(Empresa: OleVariant; const Accion: WideString; out ErrorCount: Integer): OleVariant; dispid 60;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerPlanCarrera provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerPlanCarrera exposed by              
// the CoClass dmServerPlanCarrera. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerPlanCarrera = class
    class function Create: IdmServerPlanCarrera;
    class function CreateRemote(const MachineName: string): IdmServerPlanCarrera;
  end;

implementation

uses ComObj;

class function CodmServerPlanCarrera.Create: IdmServerPlanCarrera;
begin
  Result := CreateComObject(CLASS_dmServerPlanCarrera) as IdmServerPlanCarrera;
end;

class function CodmServerPlanCarrera.CreateRemote(const MachineName: string): IdmServerPlanCarrera;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerPlanCarrera) as IdmServerPlanCarrera;
end;

end.
