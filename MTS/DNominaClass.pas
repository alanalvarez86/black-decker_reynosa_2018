unit DNominaClass;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DNominaClass.pas                           ::
  :: Descripci�n: Clases para el C�lculo de N�mina           ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$define PRIMA_DOMINICAL}
{$define QUINCENALES}
{$define CAMBIO_TNOM }
{$define DEF_1628}  //Registro incorrecto de incidencias 'FSS' cuando existe una baja
{$define IMSS_VACA}   //Calculo de d�as IMSS cuando hay vacaciones
{.$undefine QUINCENALES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DB,
     ZCreator,
     ZetaCommonLists,
     ZetaCommonClasses,
     DQueries,
{$ifdef QUINCENALES}
     FEvaluadorPrenomina,
     DVacaciones,
{$endif}
     DZetaServerProvider;

const
     K_ANCHO_DIA_HORA = 1;
     K_ANCHO_REFERENCIA = 8;
     K_ANCHO_OBSERVACIONES = 30;
     K_I_INGRESO = 'IGR';
     K_DIAS_SUBSIDIO_INCAP = 3;
type
  TStatusEmpleado = record
   Activo: Boolean;
   FechaBaja: TDate;
   FechaIngreso: TDate;
  end;
  TNominaEmpleado = record
    Nomina: eTipoPeriodo;
    NominaFin : eTipoPeriodo;
    FechaCambioTNom: TDate;
  end;
  TNominaDiasHoras = record
    Turno: TCodigo;
    DiasTurno: Word;
    DiasAguinaldo: TDiasHoras;
    DiasAsistencia: Word;
    DiasAjuste: Word;
    DiasConGoce: Word;
    DiasSinGoce: Word;
    DiasNoTrabajados: Word;
    DiasIncapacidad: Word;
    DiasIncapSubsidioHabil: Word;
    DiasIncapSubsidioDescanso: Word;
    DiasIncapSinSubsidio: Word;
    DiasFaltaVacacion: Word;
    DiasVacaciones: TDiasHoras;
    DiasPrimaVaca: TDiasHoras;
    DiasSubsidioIncapacidad: TDiasHoras;
    DiasEM: TDiasHoras;
    DiasSS: TDiasHoras;
    DiasRetardo: Word;
    DiasSuspension: Word;
    DiasOtroPermiso: Word;
{$ifdef IMSS_VACA}
    DiasVacaTotales: Word;
    DiasVacaPagadosCalendario: Word;
{$endif}
    FaltasInjustificadas: Word;
    FaltasJustificadas: Word;
    HorasJornada: TDiasHoras;
    Horas: TDiasHoras;
    HorasConGoce: TDiasHoras;
    HorasSinGoce: TDiasHoras;
    HorasExtras: TDiasHoras;
    HorasDobles: TDiasHoras;
    HorasTriples: TDiasHoras;
    HorasTardes: TDiasHoras;
{$ifdef QUINCENALES}
    HorasNoTrabajadas: TDiasHoras;
    InicioAsistencia: TDate;
    FinAsistencia: TDate;
{$endif}
    HorasVacacionesTrabajadas: TDiasHoras;
    HorasFaltaVacacion: TDiasHoras;
    HorasPrimaDominical: TDiasHoras;
    HorasPrimaDominicalTotal : TDiasHoras;
    HorasAdicionales: TDiasHoras;
    HorasFestivoTrabajado: TDiasHoras;
    HorasFestivoPagado: TDiasHoras;
    HorasDescansoTrabajado: TDiasHoras;
    HorasExentas: TDiasHoras;
    Status: eStatusPeriodo;
    UsuarioReloj: Integer;
    TipoLiquidacion: eLiqNomina;
    HorasExtPrePagadas: TDiasHoras;
{$ifdef CAMBIO_TNOM}
    DiasPeriodoCal: Word;
{$endif}
    DiasHabiles: Word;
    DiasDescansoTrabajado: Word;
    DiasFestivos: Word;
    DiasFestivoTrabajado: Word;
  end;
  TDatosLiquidacion = record
    Empleado: TNumEmp;
    Global: Boolean;
    Ordinarias: TDiasHoras;
    Dobles: TDiasHoras;
    Triples: TDiasHoras;
    Tardes: TDiasHoras;
    Adicionales: TDiasHoras;
    PrimaDominical: TDiasHoras;
    Tipo: eLiqNomina;
    Vacaciones: TDiasHoras;
    VacacionesAntes: TDiasHoras;
    VacacionesActual: TDiasHoras;
    VacacionesFormula: String;
    Aguinaldo: TDiasHoras;
    AguinaldoFormula: String;
    Observaciones: String;
    Baja: TDate;                 { *** Se  agregaron estos Datos (2.4.97) *** }
    ConfirmarAguinaldo: Boolean;
    Incapacidades: TDiasHoras;
    Faltas: TDiasHoras;
    AguinaldoPagado: TDiasHoras;
    DiasEnCurso: TDiasHoras;
    DiasConsiderados: TDiasHoras;
    NominaExiste : Boolean;
    PrimaVaca: TDiasHoras;
    GlobalSimulacion:Boolean;
    FechaLiquidacion:TDate;
  end;



  TNomina = class( TObject )
  private
    { Private declarations }
    FCreator: TZetaCreator;
    FNoChecaUsarJornadas: Boolean;
{$ifndef QUINCENALES}
    FSumaHorarios: Boolean;           { Cache de dmGlobal.SumaHorarios }
{$endif}
    FExtraSabado: eHorasExtras;       { Cache de dmGlobal.ExtraSabado }
    FExtraFestivo: eHorasExtras;      { Cache de dmGlobal.ExtraFestivo }
    FExtraDescanso: eHorasExtras;     { Cache de dmGlobal.ExtraDescanso }
    FDOWCorte: Word;                  { Cache de dmGlobal.D�a de Corte de Horas Extras en Pren�mina }
    FPrimerDia: Word;                 { Cache de dmGlobal.PrimerDia e la Semana }
    FDiasHoras: TNominaDiasHoras;     { InMemory Variable de Dias / Horas de Nomina }
    {$ifndef CAMBIO_TNOM}
    FEmpleadoStatus: TZetaCursor;
    {$endif}
    FEmpleadoDatos: TZetaCursor;
    FPagoVacaciones: TZetaCursor;
{$ifdef IMSS_VACA}
    FDiasPagoVacaciones: TZetaCursor;
    FFechasPagoVacaciones: TZetaCursor;    
    FChkAusenciaFestivos: TZetaCursor;
    FDiasCotizaSinFV: Boolean;
    FRitmosPagoVaca: TRitmos;
    FQueriesPagoVaca: TCommonQueries;
{$endif}
    FFestivoEnDescanso: Boolean;
    FSubsidioIncapacidades: TZetaCursor;
    FIncapacidadAnterior: TZetaCursor;
    FLeeDiasExtras: TZetaCursor;
    FAcumulados: TZetaCursor;
    FTarjetasLee: TZetaCursor;
    FUpdateAusenciaFSS: TZetaCursor;
    {$ifdef PRIMA_DOMINICAL}
    FPrimaDominicalTotal: Boolean;
    {$endif}
    FFaltasAgrega: TZetaCursor;
    FFaltasBorra: TZetaCursor;
    FFaltasEdit: TZetaCursor;
    FFaltasExiste: TZetaCursor;
    FFaltasLee: TZetaCursor;
    FMovimienAgrega: TZetaCursor;
    FMovimienBorraAhorrosPrestamos: TZetaCursor;
    FMovimienEdit: TZetaCursor;
    FMovimienExiste: TZetaCursor;
    FNominaAgrega: TZetaCursor;
    FNominaBorra: TZetaCursor;
    FNominaClasifica: TZetaCursor;
    FNominaDiasHoras: TZetaCursor;
    FNominaExiste: TZetaCursor;
    FNominaJornada: TZetaCursor;
    FNominaLee: TZetaCursor;
    FNominaLiquidacion: TZetaCursor;
    FNominaStatus: TZetaCursor;
    FPermisosLee: TZetaCursor;
    FVacacionesLee: TZetaCursor;
    FIncapacidadesLee: TZetaCursor;
    FEmpleadoTipoNomina: TZetaCursor;
    FUpdateGlobalAutPrenom: TZetaCursor;
    FLeeFechaIncapGeneral: TZetaCursor;
{$ifdef CAMBIO_TNOM}
    FDatosEmpleadoTemp: TZetaCursor;
{$endif}
{$ifdef QUINCENALES}
    FNominaDiasOrdinarios: TZetaCursor;
    FNominaFormulasTotales: TZetaCursor;
    FEvaluador: TNominaEvaluator;
    FAplicaPlanVacacion: Boolean;
    FPlanVacacion: TPlanVacacion;
{$endif}
    FDatosTurno: TDatosTurno;
    FDatosJornada: TDatosJornada;
    FStatus: String;
    FRegla3x3: eRegla3x3;
    FDiasAplicaExento: eDiasAplicaExento;
    FPagoPrimaEnAniversario: Boolean;
    FPeriodoSimulacion: Word;
    FPrimaAniversario: TPrimaAniversario;
    FListaDescansos: TStrings;
    {$ifdef PRIMA_DOMINICAL}
    FPrimaDominicalCalc :TPrimaDominical;
    {$endif}
    FAplicaBloqueoPreNomina:Boolean;
    FIncidenciaIncapGeneral: String;
    function AgregaNomina(const iEmpleado: TNumEmp): String;

    function ClasificaNomina(const iEmpleado: TNumEmp): String;
    function DiasLaborados(const rHorasTrabajadas, rHorasJornada: TPesos; const eTipo: eClasifiPeriodo; const TipoJornada: eTipoJornada): TPesos;
    function ExisteNomina(const iEmpleado: TNumEmp; var NomPorFuera: Boolean ): Boolean;
    function GetQueries: TCommonQueries;
    function GetPagoVacaciones(const iEmpleado: TNumEmp; var rDiasPrimaVaca : TDiasHoras): TDiasHoras;
{$ifdef IMSS_VACA}
    function GetDiasPagoVacaciones(const iEmpleado: TNumEmp): Word;
{$endif}
    function GetSubsidioIncapacidad(const iEmpleado: TNumEmp): TDiasHoras;
    function GetRitmos: TRitmos;
{$ifdef CAMBIO_TNOM}
    procedure GetDatosEmpleado(const iEmpleado: TNumEmp; var FStatusEmp: TStatusEmpleado; var FNominaEmp: TNominaEmpleado; const lStatusDesFinPer: Boolean );
{$else}
    function GetStatusEmpleado(const iEmpleado: TNumEmp): TStatusEmpleado;
{$endif}
    function oZetaProvider: TdmZetaServerProvider;
{$ifdef QUINCENALES}
    function HayFuncionDias: Boolean;
    function HayFuncionesTotales: Boolean;
{$endif}
    procedure ActualizaJornada(const iEmpleado: TNumEmp; const sRotativo: String);
    procedure CalculaDiasHoras(const iEmpleado: TNumEmp);
    procedure CalculaDiasHorasBegin;
    procedure CalculaDiasHorasEnd;
    procedure CalculaDiasIMSS(const iDiasNoHay: Integer );
{$ifdef QUINCENALES}
    procedure CalculaTotales(const iEmpleado: TNumEmp; const lCheca: Boolean; const dIngreso, dBaja: TDate );
{$else}
    procedure CalculaTotales(const iEmpleado: TNumEmp; const lCheca: Boolean );
{$endif}
    procedure CalculaTotalesBegin;
    procedure CalculaTotalesEnd;
    procedure CambiaStatusPeriodo(const eStatus: eStatusPeriodo);
    procedure GetPagoVacacionesBegin;
    procedure GetPagoVacacionesEnd;
{$ifdef IMSS_VACA}
    function GetDiasVacaSinFestivos( const iEmpleado: Integer; const dStart, dEnd: TDate ): Word;
    procedure InicializaRitmosPagoVaca;
    procedure GetDiasPagoVacacionesBegin;
    procedure GetDiasPagoVacacionesEnd;
{$endif}
    procedure GetSubsidioIncapacidadBegin;
    procedure GetSubsidioIncapacidadEnd;
{$ifdef CAMBIO_TNOM}
    procedure GetDatosEmpleadoBegin;
    procedure GetDatosEmpleadoEnd;
{$else}
    procedure GetStatusEmpleadoBegin;
    procedure GetStatusEmpleadoEnd;
{$endif}
    procedure SetDiasIncapacidadAnteriorBegin;
    procedure SetDiasIncapacidadAnteriorEnd;
    procedure SetDiasIncapacidadAnterior( const iEmpleado: TNumEmp; const dInicial, dFinal: TDate );
    procedure GrabaDiasHoras(const iEmpleado: TNumEmp);
    procedure GrabaDiasHorasBegin;
    procedure GrabaDiasHorasEnd;
    procedure IncluyeNominaBegin;
    procedure IncluyeNominaEnd;
    procedure InitDatosTurnoJornada;
    procedure InitGlobales;
    procedure LeeDiasHoras(const iEmpleado: TNumEmp);
    procedure LeeDiasHorasBegin;
    procedure LeeDiasHorasEnd;
    procedure PrenominaBegin;
    procedure PrenominaEnd;
    procedure PreparaNominaBegin;
    procedure PreparaNominaEnd;
    procedure SetDiasHorasBegin;
    procedure SetDiasHorasEnd;
    procedure SetDiasHorasCeros;
    procedure SetDiasHorasCompletas( const iEmpleado: TNumEmp );
{$ifdef QUINCENALES}
    procedure InitEvaluador;
    procedure ClearEvaluador;
    procedure SetNominaEvaluador(const iEmpleado: TNumEmp);
    procedure SetNominaEvaluadorBegin;
    procedure SetNominaEvaluadorEnd;
    procedure SetPlanVacacionesBegin;
    procedure SetPlanVacacionesEnd;
    procedure SetPlanVacaciones( const iEmpleado: Integer; const dInicio, dFin: TDate );
    procedure SetPagoPrimaAniversarioBegin;
    procedure SetPagoPrimaAniversarioEnd;
    procedure SetPagoPrimaAniversario( const iEmpleado: Integer; const dAntig: TDate );
    procedure SumaDiasHoras(const iEmpleado: TNumEmp; const lCheca: Boolean; const dIngreso, dBaja: TDate );
{$else}
    procedure SumaDiasHoras(const iEmpleado: TNumEmp; const lCheca: Boolean );
{$endif}
    procedure Totaliza(const iEmpleado: TNumEmp; const lVerificaJornada: Boolean);
    procedure InitQueries;
    procedure InitRitmos;
    {M�todos para Grabar a Bitacora}
    //function GetBitacoraMasInfo(DataSet: TZetaCursor; eClase: eClaseBitacora): string;
  public
    { Public declarations }
    constructor Create(oCreator: TZetaCreator);
    destructor Destroy; override;
    property DiasHoras: TNominaDiasHoras read FDiasHoras;
    property Queries: TCommonQueries read GetQueries;
    property Ritmos: TRitmos read GetRitmos;
    property Status: String read FStatus;
    property MovimientExiste :TZetaCursor read FMovimienExiste;
    function CalculaStatusPeriodo: eStatusPeriodo;
    function GetDatosJornada( const sTurno: String; const dInicial, dFinal: TDate ): TDatosJornada;
    function IncluyeNomina(const iEmpleado: TNumEmp; const dIngreso, dAntig, dBaja: TDate; const TipoNomina: eTipoPeriodo; const lActivo: Boolean; const PeriodoBaja: TDatosPeriodo; const dCambioTNom: TDate ): Boolean;
    function InspeccionaNomina( const iEmpleado: TNumEmp; const lExiste: Boolean  ): Boolean;
    function NominaPrepara(const iEmpleado: TNumEmp): Boolean;
{$ifdef QUINCENALES}
    procedure CalculaPrenomina( const iEmpleado: TNumEmp; const lCheca: Boolean; const dIngreso, dBaja: TDate );
{$else}
    procedure CalculaPrenomina( const iEmpleado: TNumEmp; const lCheca: Boolean );
{$endif}
    procedure CalculaPrenominaBegin;
    procedure CalculaPrenominaEnd;
    procedure RefrescaPrenomina( const iEmpleado: TNumEmp );
    procedure RefrescaPrenominaBegin;
    procedure RefrescaPrenominaEnd;
    procedure ExcepcionDiaHora(const iEmpleado: TNumEmp; const dValue: TDate; const sDiaHora: String; const iMotivo: Integer; const rValue: TDiasHoras; const eOperacion: eOperacionMontos);
    procedure ExcepcionDiaHoraBegin;
    procedure ExcepcionDiaHoraEnd;
    procedure ExcepcionDiaAgrega(const iEmpleado: TNumEmp; const eMotivo: eMotivoFaltaDias; const rDias: TDiasHoras; const dValue: TDate );
    procedure ExcepcionHoraAgrega(const iEmpleado: TNumEmp; const eMotivo: eMotivoFaltaHoras; const rHoras: TDiasHoras; const dValue: TDate );
    procedure ExcepcionDiaHoraAgregaBegin;
    procedure ExcepcionDiaHoraAgregaEnd;
    procedure ExcepcionMonto( const iEmpleado: TNumEmp; const iConcepto: Integer; const sReferencia: String; const rValue: TPesos; const eOperacion: eOperacionMontos; const lPercepcion: Boolean );
    procedure ExcepcionMontoBegin;
    procedure ExcepcionMontoEnd;
    procedure ExcepcionMontoAgrega(const iEmpleado: TNumEmp; const iConcepto: Integer; const rValor: TPesos; const sReferencia: String; const lPercepcion: Boolean);
    procedure ExcepcionMontoAgregaBegin;
    procedure ExcepcionMontoAgregaEnd;
    procedure NominaPreparaBegin;
    procedure NominaPreparaEnd;
    procedure NominaTotaliza(const iEmpleado: TNumEmp);
    procedure GetDatosJornadaBegin;
    procedure GetDatosJornadaEnd;
    procedure VerificaStatus(const iEmpleado: TNumEmp);
    procedure VerificaRegistro(const iEmpleado: TNumEmp; const lMontos: Boolean = FALSE);
    procedure VerificaNominaBegin;
    procedure VerificaNominaEnd;
    procedure PreparaDiasHoras( const iEmpleado: TNumEmp );
    procedure PreparaDiasHorasBegin;
    procedure PreparaDiasHorasEnd;
    procedure Liquidacion(const Datos: TDatosLiquidacion; Ahorros,Prestamos: TDataset);
    procedure LiquidacionBegin;
    procedure LiquidacionEnd;
    {M�todos para Grabar a Bitacora}
    function GetBitacoraMensaje(eClase: eClaseBitacora): string;

  end;

implementation

uses ZetaCommonTools,
     ZetaServerTools,
     ZGlobalTress;

const
     Q_PERIODO_CAMBIA_STATUS = 0;
     Q_PERIODO_CALCULA_STATUS = 1;
     Q_EMPLEADO_STATUS = 2;
     Q_EMPLEADO_DATOS = 3;
     Q_EMPLEADO_CHECA = 4;
     Q_VACACIONES_PAGO = 5;
     Q_PRENOMINA_HORAS_EXTRAS = 6;
     Q_TARJETAS_HORARIO_LEE = 7;
     {$ifdef PRIMA_DOMINICAL}
     Q_CHECADAS_LEE = 8;
     {$else}
     Q_TARJETAS_LEE = 8;
     {$endif}
     Q_FALTAS_LEE = 9;
     Q_FALTAS_AGREGA = 10;
     Q_FALTAS_EDIT = 11;
     Q_FALTAS_EXISTE = 12;
     Q_FALTAS_LIQUIDACION_BORRA = 13;
     Q_MOVIMIEN_AGREGA = 14;
     Q_MOVIMIEN_EDIT = 15;
     Q_MOVIMIEN_EXISTE = 16;
     Q_MOVIMIEN_BORRA_AHORROS_PRESTAMOS = 18;
     Q_NOMINA_AGREGA = 19;
     Q_NOMINA_BORRA = 20;
     Q_NOMINA_CLASIFICA = 21;
     Q_NOMINA_DIAS_HORAS = 22;
     Q_NOMINA_EXISTE = 23;
     Q_NOMINA_JORNADA = 24;
     Q_NOMINA_LEE = 25;
     Q_NOMINA_LIQUIDACION = 26;
     Q_NOMINA_CAMBIA_STATUS = 27;
     Q_PERMISOS_LEE = 28;
     Q_VACACIONES_LEE = 29;
     Q_INCAPACIDADES_LEE = 30;
     Q_ACUMULADO_AHORROS = 31;
     Q_TARJETAS_UPDATE_FSS = 32;
     Q_EMPLEADO_TIPO_NOMINA = 33;
     Q_SUBSIDIO_INCAPACIDAD = 34;
{$ifdef QUINCENALES}
     Q_NOMINA_DIAS_ORDINARIOS = 35;
{$endif}
{$ifdef CAMBIO_TNOM}
     Q_DATOS_EMPLEADO_TEMP = 36;
{$endif}
{$ifdef IMSS_VACA}
     Q_DIAS_VACACIONES_PAGO = 37;
     Q_FECHAS_VACACIONES_PAGO = 38;
     Q_CHK_AUSENCIA_FESTIVOS = 39;
{$endif}
     Q_GLOBAL_UPDATE_BLOQUEO_PRENOM = 40;
     Q_INCAP_GENERAL_FECHA = 41;
     Q_INCAPACIDAD_ANTERIOR = 42;
     Q_NOMINA_FORMULAS_TOTALES = 43;

function DiasTipo( const eTipo: eClasifiPeriodo ): Integer;
const
     aDiasPeriodo: array[ eClasifiPeriodo ] of Word = ( 1, 6, 12, 13, 26, 9 );
begin
     Result := aDiasPeriodo[ eTipo ];
end;

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_PERIODO_CAMBIA_STATUS: Result := 'update PERIODO set PE_STATUS = :Status where '+
                                             '( PE_YEAR = :Year ) and '+
                                             '( PE_TIPO = :Tipo ) and '+
                                             '( PE_NUMERO = :Numero )';
{$ifdef INTERBASE}
          Q_PERIODO_CALCULA_STATUS: Result := 'select MAXSTATUS from SET_STATUS_PERIODO( :Year, :Tipo, :Numero, :Usuario )';
{$else}
          Q_PERIODO_CALCULA_STATUS: Result := '{CALL SET_STATUS_PERIODO( :Year, :Tipo, :Numero, :Usuario, :MaxStatus )}';
{$endif}
          Q_EMPLEADO_STATUS: Result := 'select CB_ACTIVO, CB_FEC_ING, CB_FEC_BAJ from COLABORA where ( CB_CODIGO = :Empleado )';
{$ifdef INTERBASE}
          Q_EMPLEADO_DATOS: Result := 'select CB_ACTIVO, CB_CHECA, CB_FEC_ING, CB_FEC_ANT, CB_FEC_BAJ, CB_FEC_NOM, '+
                                      'CB_NOMYEAR, CB_NOMTIPO, CB_NOMNUME, ' +
                                      '( select TIPNOM from SP_GET_NOMTIPO( %0:s, COLABORA.CB_CODIGO )  ) TU_NOMINA, '+
                                      '( select TURNO from SP_GET_TURNO( %0:s, COLABORA.CB_CODIGO ) ) TU_CODIGO '+
                                      'from COLABORA '+
                                      'where ( CB_CODIGO = :Empleado ) ';
{$endif}
{$ifdef MSSQL}
          Q_EMPLEADO_DATOS: Result := 'select CB_ACTIVO, CB_CHECA, CB_FEC_ING, CB_FEC_ANT, CB_FEC_BAJ, CB_FEC_NOM,' +
                                      'CB_NOMYEAR, CB_NOMTIPO, CB_NOMNUME, '+
                                      '(DBO.SP_GET_NOMTIPO( %0:s, COLABORA.CB_CODIGO ) ) TU_NOMINA, '+
                                      '(DBO.SP_GET_TURNO( %0:s, COLABORA.CB_CODIGO ) ) TU_CODIGO '+
                                      'from COLABORA '+
                                      'where (  COLABORA.CB_CODIGO = :Empleado ) ';
{$endif}

 {$ifdef CAMBIO_TNOM}
          Q_EMPLEADO_CHECA: Result := 'select CB_ACTIVO, CB_CHECA, CB_FEC_ING, CB_FEC_BAJ, CB_FEC_NOM  ' +
                                      'from COLABORA where ( CB_CODIGO = :Empleado )';
 {$else}
 {$ifdef QUINCENALES}
          Q_EMPLEADO_CHECA: Result := 'select CB_ACTIVO, CB_CHECA, CB_FEC_ING, CB_FEC_BAJ  ' +
                                      'from COLABORA where ( CB_CODIGO = :Empleado )';
 {$else}
          Q_EMPLEADO_CHECA: Result := 'select CB_CHECA from COLABORA where ( CB_CODIGO = :Empleado )';
 {$endif}
{$endif}
          Q_VACACIONES_PAGO: Result := 'select SUM( VA_PAGO ) as PAGO, SUM( VA_P_PRIMA ) as PRIMA_PV ' +
                                       'from VACACION where '+
                                       '( CB_CODIGO = :Empleado  ) and ( VA_TIPO = 1 ) and '+
                                       '( VA_NOMYEAR = :Year ) and ( VA_NOMTIPO = :Tipo ) and '+
                                       '( VA_NOMNUME = :Numero )';
{$ifdef IMSS_VACA}
          Q_DIAS_VACACIONES_PAGO: Result := 'select VA_FEC_INI, VA_FEC_FIN ' +
                                            'from VACACION where '+
                                            '( CB_CODIGO = :Empleado  ) and ( VA_TIPO = 1 ) and '+
                                            '( VA_NOMYEAR = :Year ) and ( VA_NOMTIPO = :Tipo ) and '+
                                            '( VA_NOMNUME = :Numero )';
          Q_FECHAS_VACACIONES_PAGO: Result := 'select Min( VA_FEC_INI ) as FECHA_INI, Max( VA_FEC_FIN ) as FECHA_FIN ' +
                                              'from VACACION where '+
                                              '( VA_TIPO = 1 ) and ( VA_NOMYEAR = :Year ) and ( VA_NOMTIPO = :Tipo ) and ( VA_NOMNUME = :Numero )';
          Q_CHK_AUSENCIA_FESTIVOS: Result := 'select AU_FECHA, HO_CODIGO, AU_STATUS, AU_TIPODIA, AU_STA_FES ' +
                                             'from AUSENCIA ' +
                                             'where ( CB_CODIGO = :Empleado ) and ( AU_FECHA >= :FechaIni ) and ( AU_FECHA <= :FechaFin )';
{$endif}
          Q_PRENOMINA_HORAS_EXTRAS: Result := 'select COUNT(*) as CUANTOS from AUSENCIA where '+
                                              '( CB_CODIGO = :Empleado ) and '+
                                              '( AU_FECHA >= :Fecha ) and '+
                                              '( AU_FECHA < :FechaInicial ) and '+
                                              '( AU_EXTRAS > 0 )';
          {$ifdef PRIMA_DOMINICAL}
          Q_TARJETAS_HORARIO_LEE: Result := 'select A.AU_TIPODIA, A.AU_STATUS, A.AU_HORAS, A.AU_EXTRAS, A.AU_DOBLES, '+
                                            'A.AU_PER_CG, A.AU_PER_SG, A.US_CODIGO, A.AU_TARDES, A.AU_DES_TRA, A.AU_FECHA, A.AU_PRE_EXT, '+
                                            'H.HO_JORNADA, H.HO_TIPO, A.AU_TIPO '+
{$ifdef QUINCENALES}
                                            ', A.AU_HORASNT ' +
{$endif}

{$ifdef CAMBIO_TNOM}
                                            ',A.CB_NOMINA '+
{$endif}

                                            'from AUSENCIA A left outer join HORARIO H on ( H.HO_CODIGO = A.HO_CODIGO ) '+
                                            'where '+
                                            '( A.CB_CODIGO = :Empleado ) and '+
                                            '( A.AU_FECHA >= :FechaInicial ) and '+
                                            '( A.AU_FECHA <= :FechaFinal ) '+

                                            'order by A.AU_FECHA';
          Q_CHECADAS_LEE: Result := 'select CH_TIPO, CH_DESCRIP, CH_H_AJUS from CHECADAS where '+
                                    '( CB_CODIGO = :Empleado ) and '+
                                    '( AU_FECHA = :Fecha ) and '+
                                    '( CH_TIPO in ( %d, %d, %d, %d ) ) and '+ { chEntrada,chSalida,chInicio,chFin }
                                    '( CH_DESCRIP in ( %d, %d, %d, %d ) ) '+      { dcOrdinaria,dcRetardo,dcPuntual, dcExtras }
                                    'order by CH_H_AJUS';
          {$else}
          Q_TARJETAS_HORARIO_LEE: Result := 'select AU_TIPODIA, AU_STATUS, AU_HORAS, AU_EXTRAS, AU_DOBLES, AU_PER_CG, '+
                                            'AU_PER_SG, US_CODIGO, AU_TARDES, AU_DES_TRA, AU_FECHA, AU_PRE_EXT'+
                                            'CAST( ( select HORARIO.HO_JORNADA from HORARIO where  '+
                                            '( HORARIO.HO_CODIGO = AUSENCIA.HO_CODIGO ) ) as FLOAT ) as HO_JORNADA '+
{$ifdef QUINCENALES}
                                            ', AU_HORASNT ' +
{$endif}
                                            'from AUSENCIA where '+
                                            '( AUSENCIA.CB_CODIGO = :Empleado ) and '+
                                            '( AU_FECHA >= :FechaInicial ) and '+
                                            '( AU_FECHA <= :FechaFinal ) '+
{$ifdef CAMBIO_TNOM}
                                            'and ( A.CB_NOMINA = :Nomina ) '+
{$endif}
                                            'order by AU_FECHA';
          Q_TARJETAS_LEE: Result := 'select AU_TIPODIA, AU_STATUS, AU_HORAS, AU_EXTRAS, AU_DOBLES, AU_PER_CG, '+
                                    'AU_PER_SG, US_CODIGO, AU_TARDES, AU_DES_TRA, AU_FECHA '+
{$ifdef QUINCENALES}
                                    ', AU_HORASNT ' +
{$endif}
                                    'from AUSENCIA where ( AUSENCIA.CB_CODIGO = :Empleado ) and '+
                                    '( AU_FECHA >= :FechaInicial ) and ( AU_FECHA <= :FechaFinal ) order by AU_FECHA';
          {$endif}
          Q_FALTAS_LEE: Result := 'select FA_DIA_HOR, FA_DIAS, FA_HORAS, FA_FEC_INI, FA_MOTIVO from FALTAS where '+
                                  '( CB_CODIGO = :Empleado ) and '+
                                  '( PE_YEAR = :Year ) and '+
                                  '( PE_TIPO = :Tipo ) and '+
                                  '( PE_NUMERO = :Numero ) '+
                                  'order by FA_FEC_INI, FA_MOTIVO';
          Q_FALTAS_AGREGA: Result := 'insert into FALTAS ( '+
                                     'PE_YEAR, '+
                                     'PE_TIPO, '+
                                     'PE_NUMERO, '+
                                     'CB_CODIGO, '+
                                     'FA_DIA_HOR, '+
                                     'FA_MOTIVO, '+
                                     'FA_FEC_INI, '+
                                     'FA_DIAS, '+
                                     'FA_HORAS ) values ( '+
                                     ':PE_YEAR, '+
                                     ':PE_TIPO, '+
                                     ':PE_NUMERO, '+
                                     ':CB_CODIGO, '+
                                     ':FA_DIA_HOR, '+
                                     ':FA_MOTIVO, '+
                                     ':FA_FEC_INI, '+
                                     ':FA_DIAS, '+
                                     ':FA_HORAS )';
          Q_FALTAS_EDIT: Result := 'update FALTAS set '+
                                   'FA_DIAS = :FA_DIAS, '+
                                   'FA_HORAS = :FA_HORAS where '+
                                   '( PE_YEAR = :Year ) and '+
                                   '( PE_TIPO = :Tipo ) and '+
                                   '( PE_NUMERO = :Numero ) and '+
                                   '( CB_CODIGO = :Empleado ) and '+
                                   '( FA_DIA_HOR = :DiaHora ) and '+
                                   '( FA_FEC_INI = :Fecha ) and '+
                                   '( FA_MOTIVO = :Motivo )';
          Q_FALTAS_EXISTE: Result := 'select FA_DIAS, FA_HORAS from FALTAS where '+
                                     '( PE_YEAR = :Year ) and '+
                                     '( PE_TIPO = :Tipo ) and '+
                                     '( PE_NUMERO = :Numero ) and '+
                                     '( CB_CODIGO = :Empleado ) and '+
                                     '( FA_DIA_HOR = :DiaHora ) and '+
                                     '( FA_FEC_INI = :Fecha ) and '+
                                     '( FA_MOTIVO = :Motivo )';
          Q_FALTAS_LIQUIDACION_BORRA: Result := 'delete from FALTAS where '+
                                                '( PE_YEAR = :Year ) and '+
                                                '( PE_TIPO = :Tipo ) and '+
                                                '( PE_NUMERO = :Numero ) and '+
                                                '( CB_CODIGO = :Empleado ) and '+
                                                '( ( ( FA_DIA_HOR = ''D'' ) and ( FA_MOTIVO in ( 6, 8, 15 ) ) ) or '+
                                                '( ( FA_DIA_HOR = ''H'' ) and ( FA_MOTIVO in ( 0, 1, 2, 3, 4, 5, 6 ) ) ) )';
          Q_MOVIMIEN_AGREGA: Result := 'insert into MOVIMIEN ( '+
                                       'PE_YEAR, '+
                                       'PE_TIPO, '+
                                       'PE_NUMERO, '+
                                       'CB_CODIGO, '+
                                       'CO_NUMERO, '+
                                       'MO_ACTIVO, '+
                                       'MO_PERCEPC, '+
                                       'MO_DEDUCCI, '+
                                       'MO_REFEREN, '+
                                       'US_CODIGO ) values ( '+
                                       ':PE_YEAR, '+
                                       ':PE_TIPO, '+
                                       ':PE_NUMERO, '+
                                       ':CB_CODIGO, '+
                                       ':CO_NUMERO, '+
                                       ':MO_ACTIVO, '+
                                       ':MO_PERCEPC, '+
                                       ':MO_DEDUCCI, '+
                                       ':MO_REFEREN, '+
                                       ':US_CODIGO )';
          Q_MOVIMIEN_EDIT: Result := 'update MOVIMIEN set '+
                                     'MO_PERCEPC = :MO_PERCEPC, '+
                                     'MO_DEDUCCI = :MO_DEDUCCI, '+
                                     'US_CODIGO = :US_CODIGO where '+
                                     '( PE_YEAR = :Year ) and '+
                                     '( PE_TIPO = :Tipo ) and '+
                                     '( PE_NUMERO = :Numero ) and '+
                                     '( CB_CODIGO = :Empleado ) and '+
                                     '( CO_NUMERO = :Concepto ) and '+
                                     '( MO_REFEREN = :Referencia )';
          Q_MOVIMIEN_EXISTE: Result := 'select MO_PERCEPC, MO_DEDUCCI from MOVIMIEN where '+
                                       '( PE_YEAR = :Year ) and '+
                                       '( PE_TIPO = :Tipo ) and '+
                                       '( PE_NUMERO = :Numero ) and '+
                                       '( CB_CODIGO = :Empleado ) and '+
                                       '( CO_NUMERO = :Concepto ) and '+
                                       '( MO_REFEREN = :Referencia )';
          Q_MOVIMIEN_BORRA_AHORROS_PRESTAMOS: Result := 'delete from MOVIMIEN where '+
                                                        '( PE_YEAR = :Year ) and '+
                                                        '( PE_TIPO = :Tipo ) and '+
                                                        '( PE_NUMERO = :Numero ) and '+
                                                        '( CB_CODIGO = :Empleado ) and '+
                                                        '( ( CO_NUMERO in ( select TB_CONCEPT from TAHORRO ) ) or '+
                                                        '( CO_NUMERO in ( select TB_RELATIV from TAHORRO ) ) or '+
                                                        '( CO_NUMERO in ( select  TB_CONCEPT from TPRESTA ) ) )';
{$ifdef INTERBASE}
          Q_NOMINA_AGREGA: Result := 'select ROTATIVO from SP_ADD_NOMINA( :Year, :Tipo, :Numero, :Empleado )';
{$else}
          Q_NOMINA_AGREGA: Result := 'EXECUTE PROCEDURE SP_ADD_NOMINA( :Year, :Tipo, :Numero, :Empleado, :Rotativo )';
{$endif}
          Q_NOMINA_BORRA: Result := 'delete from NOMINA where '+
                                    '( PE_YEAR = :Year ) and '+
                                    '( PE_TIPO = :Tipo ) and '+
                                    '( PE_NUMERO = :Numero ) and '+
                                    '( CB_CODIGO = :Empleado )';
{$ifdef INTERBASE}
          Q_NOMINA_CLASIFICA: Result := 'select ROTATIVO from SP_CLAS_NOMINA( :Year, :Tipo, :Numero, :Empleado )';
{$else}
          Q_NOMINA_CLASIFICA: Result := 'EXECUTE PROCEDURE SP_CLAS_NOMINA( :Year, :Tipo, :Numero, :Empleado, :Rotativo )';
{$endif}
          Q_NOMINA_DIAS_HORAS: Result := 'update NOMINA set '+
                                         'NO_ADICION = :NO_ADICION, '+
                                         'NO_EXTRAS = :NO_EXTRAS, '+
                                         'NO_DOBLES = :NO_DOBLES, '+
                                         'NO_TRIPLES = :NO_TRIPLES, '+
                                         'NO_EXENTAS = :NO_EXENTAS, '+
                                         'NO_HORAS = :NO_HORAS, '+
                                         'NO_TARDES = :NO_TARDES, '+
                                         'NO_DES_TRA = :NO_DES_TRA, '+
                                         'NO_FES_TRA = :NO_FES_TRA, '+
                                         'NO_FES_PAG = :NO_FES_PAG, '+
                                         'NO_VAC_TRA = :NO_VAC_TRA, '+
                                         'NO_HORA_PD = :NO_HORA_PD, '+
                                         'NO_HORAPDT = :NO_HORAPDT, '+
                                         'NO_HORA_CG = :NO_HORA_CG, '+
                                         'NO_HORA_SG = :NO_HORA_SG, '+
                                         'NO_PRE_EXT = :NO_PRE_EXT, '+
{$ifdef QUINCENALES}
                                         'NO_HORASNT = :NO_HORASNT, '+
                                         'NO_ASI_INI = :NO_ASI_INI, '+
                                         'NO_ASI_FIN = :NO_ASI_FIN, '+
                                         'NO_DIAS_BA = :NO_DIAS_BA, '+
{$endif}
                                         'NO_JORNADA = :NO_JORNADA, '+
                                         'NO_D_TURNO = :NO_D_TURNO, '+
                                         'NO_DIAS = :NO_DIAS, '+
                                         'NO_DIAS_AG = :NO_DIAS_AG, '+
                                         'NO_DIAS_AJ = :NO_DIAS_AJ, '+
                                         'NO_DIAS_AS = :NO_DIAS_AS, '+
                                         'NO_DIAS_FI = :NO_DIAS_FI, '+
                                         'NO_DIAS_NT = :NO_DIAS_NT, '+
                                         'NO_DIAS_FV = :NO_DIAS_FV, '+
                                         'NO_DIAS_VJ = :NO_DIAS_VJ, '+
                                         'NO_DIAS_RE = :NO_DIAS_RE, '+
                                         'NO_DIAS_IN = :NO_DIAS_IN, '+
                                         'NO_DIAS_IH = :NO_DIAS_IH, '+
                                         'NO_DIAS_ID = :NO_DIAS_ID, '+
                                         'NO_DIAS_IT = :NO_DIAS_IT, '+
                                         'NO_DIAS_SG = :NO_DIAS_SG, '+
                                         'NO_DIAS_CG = :NO_DIAS_CG, '+
                                         'NO_DIAS_FJ = :NO_DIAS_FJ, '+
                                         'NO_DIAS_SU = :NO_DIAS_SU, '+
                                         'NO_DIAS_OT = :NO_DIAS_OT, '+
                                         'NO_DIAS_VA = :NO_DIAS_VA, '+
                                         'NO_DIAS_PV = :NO_DIAS_PV, '+
                                         'NO_DIAS_SI = :NO_DIAS_SI, '+
                                         'NO_DIAS_SS = :NO_DIAS_SS, '+
                                         'NO_DIAS_EM = :NO_DIAS_EM, '+
{$ifdef IMSS_VACA}
                                         'NO_DIAS_VT = :NO_DIAS_VT, '+
                                         'NO_DIAS_VC = :NO_DIAS_VC, '+
{$endif}
                                         'NO_STATUS = :NO_STATUS, '+
                                         'NO_APROBA = :NO_APROBA, '+
{$IFDEF CAMBIO_TNOM}
                                         'NO_DIAS_PE = :NO_DIAS_PE,'+
{$ENDIF}
                             //            'US_CODIGO = :US_CODIGO, '+
                                         'NO_FACT_BA = :NO_FACT_BA,'+
                                         'NO_DIAS_DT = :NO_DIAS_DT,'+
                                         'NO_DIAS_FS = :NO_DIAS_FS,'+
                                         'NO_DIAS_FT = :NO_DIAS_FT,'+
                                         'NO_USER_RJ = :NO_USER_RJ where '+
                                         '( PE_YEAR = :Year ) and '+
                                         '( PE_TIPO = :Tipo ) and '+
                                         '( PE_NUMERO = :Numero ) and '+
                                         '( CB_CODIGO = :Empleado )';
          Q_NOMINA_EXISTE: Result := 'select NO_FUERA from NOMINA where '+
                                     '( CB_CODIGO = :Empleado ) and '+
                                     '( PE_YEAR = :Year ) and '+
                                     '( PE_TIPO = :Tipo ) and '+
                                     '( PE_NUMERO = :Numero )';
          Q_NOMINA_JORNADA: Result := 'update NOMINA set '+
                                      'NO_D_TURNO = :Dias, '+
                                      'NO_JORNADA = :Jornada where '+
                                      '( PE_YEAR = :Year ) and '+
                                      '( PE_TIPO = :Tipo ) and '+
                                      '( PE_NUMERO = :Numero ) and '+
                                      '( CB_CODIGO = :Empleado )';
          Q_NOMINA_LEE: Result := 'select CB_TURNO, NO_JORNADA, NO_D_TURNO, NO_DIAS_AS, NO_DIAS_NT, '+
                                  'NO_DIAS_FV, NO_DIAS_IN, NO_DIAS_SG, NO_DIAS_CG, NO_DIAS_AG, '+
                                  'NO_DIAS_AJ, NO_DIAS_VA, NO_DIAS_PV, NO_DIAS_FJ, NO_DIAS_FI, NO_DIAS_OT, '+
                                  'NO_DIAS_RE, NO_DIAS_SU, NO_DIAS_SI, NO_HORAS, NO_HORA_CG, NO_HORA_SG, '+
                                  'NO_EXTRAS, NO_DOBLES, NO_TRIPLES, NO_TARDES, NO_ADICION, NO_DES_TRA, '+
                                  'NO_DIAS_IH, NO_DIAS_ID, NO_DIAS_IT, '+
{$ifdef QUINCENALES}
                                  'NO_HORASNT, NO_ASI_INI, NO_ASI_FIN, ' +
{$endif}
{$ifdef CAMBIO_TNOM}
                                  'NO_DIAS_PE,  ' +
{$endif}
{$ifdef IMSS_VACA}
                                  'NO_DIAS_VT, NO_DIAS_VC, NO_FACT_BA, NO_DIAS_DT, NO_DIAS_FS, NO_DIAS_FT, ' +
{$endif}
                                  'NO_FES_TRA, NO_HORA_PD, NO_HORAPDT, NO_VAC_TRA, NO_USER_RJ, NO_LIQUIDA, NO_PRE_EXT from NOMINA where '+
                                  '( CB_CODIGO = :Empleado ) and '+
                                  '( PE_YEAR = :Year ) and '+
                                  '( PE_TIPO = :Tipo ) and '+
                                  '( PE_NUMERO = :Numero )';
          Q_NOMINA_LIQUIDACION: Result := 'update NOMINA set '+
                                          'NO_OBSERVA = :NO_OBSERVA, '+
                                          'NO_FEC_LIQ = :NO_FEC_LIQ, ' +
                                          'NO_LIQUIDA = :NO_LIQUIDA, '+
                                          'NO_GLOBAL = :NO_GLOBAL '+
                                          'where '+
                                          '( PE_YEAR = :Year ) and '+
                                          '( PE_TIPO = :Tipo ) and '+
                                          '( PE_NUMERO = :Numero ) and '+
                                          '( CB_CODIGO = :Empleado )';
          Q_NOMINA_CAMBIA_STATUS: Result := 'update NOMINA set '+
                                            'US_CODIGO = :US_CODIGO,' +
                                            'NO_STATUS = :NO_STATUS, '+
                                            'NO_APROBA = :NO_APROBA where '+
                                            '( PE_YEAR = :Year ) and '+
                                            '( PE_TIPO = :Tipo ) and '+
                                            '( PE_NUMERO = :Numero ) and '+
                                            '( CB_CODIGO = :Empleado )';
          Q_PERMISOS_LEE: Result := 'select PM_FEC_INI, PM_FEC_FIN, PM_CLASIFI from PERMISO where '+
                                    '( CB_CODIGO = :Empleado ) and '+
                                    '( PM_FEC_INI <= :FechaFinal ) and '+
                                    '( PM_FEC_FIN > :FechaInicial )';
          Q_VACACIONES_LEE: Result := 'select VA_FEC_INI, VA_FEC_FIN from VACACION where'+
                                      '( VA_TIPO = 1 ) and '+
                                      '( CB_CODIGO = :Empleado ) and '+
                                      '( VA_FEC_INI <= :FechaFinal ) and '+
                                      '( VA_FEC_FIN > :FechaInicial )';
          Q_INCAPACIDADES_LEE: Result := 'select IN_FEC_INI, IN_FEC_FIN, IN_TIPO, IN_MOTIVO from INCAPACI where '+
                                         '( CB_CODIGO = :Empleado ) and '+
                                         '( IN_FEC_INI <= :FechaFinal ) and '+
                                         '( IN_FEC_FIN > :FechaInicial )';
          Q_ACUMULADO_AHORROS: Result := 'select AC_ANUAL from ACUMULA where '+
                                         '( AC_YEAR = :Year ) and '+
                                         '( CB_CODIGO = :Empleado ) and '+
                                         '( CO_NUMERO = :Concepto )';
          Q_TARJETAS_UPDATE_FSS: Result := 'update AUSENCIA set AU_TIPO = %s where ' +
                                           '( CB_CODIGO = :Empleado ) and ' +
                                           '( AU_FECHA = :Fecha ) and ' +
                                           '( AU_TIPODIA = %d )';


          {$ifdef INTERBASE}
          Q_EMPLEADO_TIPO_NOMINA: Result := 'select (select TIPNOM from SP_GET_NOMTIPO( ' +
                                            'COLABORA.CB_FEC_ING, '+
                                            'COLABORA.CB_CODIGO )  ) TU_NOMINA, '+
                                            '       (select TIPNOM from SP_GET_NOMTIPO( %0:s, COLABORA.CB_CODIGO )  ) NOMINA_INI '+
                                            'from COLABORA '+
                                            'where COLABORA.CB_CODIGO = :Empleado ';
          {$endif}
          {$ifdef MSSQL}
          Q_EMPLEADO_TIPO_NOMINA: Result := 'select (DBO.SP_GET_NOMTIPO( ' +
                                            'COLABORA.CB_FEC_ING, ' +
                                            'COLABORA.CB_CODIGO ) ) TU_NOMINA, '+
                                            '       (DBO.SP_GET_NOMTIPO( %0:s, COLABORA.CB_CODIGO ) ) NOMINA_INI ' +
                                            'from COLABORA ' +
                                            'where COLABORA.CB_CODIGO = :Empleado ';
          {$endif}
          Q_SUBSIDIO_INCAPACIDAD: Result := 'select SUM( IN_DIASSUB ) as SUBSIDIO from INCAPACI '+
                                            'where ( CB_CODIGO = :Empleado ) and ( IN_NOMYEAR = :Year ) and '+
                                            '( IN_NOMTIPO = :Tipo ) and ( IN_NOMNUME = :Numero )';
{$ifdef QUINCENALES}
          Q_NOMINA_DIAS_ORDINARIOS: Result := 'update NOMINA set '+
                                              'NO_DIAS = :NO_DIAS where ' +
                                              '( PE_YEAR = :Year ) and '+
                                              '( PE_TIPO = :Tipo ) and '+
                                              '( PE_NUMERO = :Numero ) and '+
                                              '( CB_CODIGO = :Empleado )';
{$endif}
{$ifdef CAMBIO_TNOM}
        {$ifdef MSSQL}
        Q_DATOS_EMPLEADO_TEMP: Result := 'select CB_ACTIVO, CB_CHECA, CB_FEC_ING, CB_FEC_ANT, CB_FEC_BAJ, CB_FEC_NOM,' +
                                         '(DBO.SP_GET_NOMTIPO( %0:s, COLABORA.CB_CODIGO ) ) CB_NOMINA, '+
                                         '(DBO.SP_GET_NOMTIPO( %1:s, COLABORA.CB_CODIGO ) ) NOMINA_FIN '+
                                         'from COLABORA '+
                                         'where (  COLABORA.CB_CODIGO = :Empleado ) ';
        {$endif}
        {$ifdef INTERBASE}
        Q_DATOS_EMPLEADO_TEMP: Result := 'select CB_ACTIVO, CB_CHECA, CB_FEC_ING, CB_FEC_ANT, CB_FEC_BAJ, CB_FEC_NOM,' +
                                         '( select TIPNOM from SP_GET_NOMTIPO( %0:s, COLABORA.CB_CODIGO )  ) CB_NOMINA, '+
                                         '( select TIPNOM from SP_GET_NOMTIPO( %1:s, COLABORA.CB_CODIGO )  ) NOMINA_FIN '+
                                         'from COLABORA '+
                                         'where (  COLABORA.CB_CODIGO = :Empleado ) ';
        {$endif}
{$endif}
         Q_GLOBAL_UPDATE_BLOQUEO_PRENOM : Result := 'update GLOBAL set GL_FORMULA = :Formula where GL_CODIGO = %d';
         Q_INCAP_GENERAL_FECHA: Result := 'select IN_FEC_INI from INCAPACI ' +
                                          'where CB_CODIGO = :Empleado and IN_FEC_INI <= :Fecha and IN_FEC_FIN > :Fecha2 and ' +
                                          'IN_TIPO = %s and IN_MOTIVO = %d';
         Q_INCAPACIDAD_ANTERIOR: Result := 'select IN_FEC_INI, IN_FEC_FIN, IN_FEC_RH, IN_TIPO, IN_MOTIVO from INCAPACI where '+
                                           '( CB_CODIGO = :Empleado ) and '+
                                           '( IN_FEC_INI < IN_FEC_RH ) and ( IN_FEC_INI < :FechaInicial ) and ' +
                                           '( ( IN_FEC_RH >= :FechaInicial2 ) and ( IN_FEC_RH <= :FechaFinal ) )';
         Q_NOMINA_FORMULAS_TOTALES: Result := 'update NOMINA set NO_TOTAL_1 = :NO_TOTAL_1, NO_TOTAL_2 = :NO_TOTAL_2, ' +
                                              'NO_TOTAL_3 = :NO_TOTAL_3, NO_TOTAL_4 = :NO_TOTAL_4, NO_TOTAL_5 = :NO_TOTAL_5 ' +
                                              'where ( CB_CODIGO = :Empleado ) and ( PE_YEAR = :Year ) and ' +
                                              '( PE_TIPO = :Tipo ) and ( PE_NUMERO = :Numero )';

     end;
end;

{ ************ TNomina ************ }

constructor TNomina.Create( oCreator: TZetaCreator );
begin
     FCreator := oCreator;
end;

destructor TNomina.Destroy;
begin
     inherited Destroy;
end;

function TNomina.oZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

function TNomina.GetQueries: TCommonQueries;
begin
     Result := FCreator.Queries;
end;

function TNomina.GetRitmos: TRitmos;
begin
     Result := FCreator.Ritmos;
end;

procedure TNomina.InitGlobales;
begin
     with oZetaProvider do
     begin
          FPrimerDia := GetGlobalInteger( K_GLOBAL_PRIMER_DIA );
          FDOWCorte := FPrimerDia;
{$ifdef QUINCENALES}
          { Defecto con dia de corte, corregido por que afectaba en quincenales para obtener NO_EXENTAS  }
//          if ( FDOWCorte = 7 ) then { Offset porque antes Domingo = 7 (En Delphi Domingo = 1 ) }
//             FDOWCorte := 1
//          else
//              FDOWCorte := FDOWCorte + 1;
{$endif}
{$ifndef QUINCENALES}
          FSumaHorarios := GetGlobalBooleano( K_GLOBAL_SUMAR_JORNADAS );
{$endif}
          FExtraSabado := eHorasExtras( GetGlobalInteger( K_GLOBAL_TRATO_X_SAB ) );
          FExtraFestivo := eHorasExtras( GetGlobalInteger( K_GLOBAL_TRATO_X_DESC ) );
          FExtraDescanso := eHorasExtras( GetGlobalInteger( K_GLOBAL_TRATO_X_FESTIVO ) );
{$ifdef IMSS_VACA}
          FDiasCotizaSinFV := GetGlobalBooleano( K_GLOBAL_DIAS_COTIZADOS_FV );
{$endif}
          FFestivoEnDescanso := GetGlobalBooleano( K_GLOBAL_FESTIVO_EN_DESCANSOS );
     end;
end;

procedure TNomina.InitQueries;
begin
     FCreator.PreparaQueries;
end;

procedure TNomina.InitRitmos;
begin
     InitQueries;
     FCreator.PreparaRitmos;
end;

procedure TNomina.GetDatosJornadaBegin;
begin
     InitRitmos;
     with Ritmos do
     begin
          GetDatosJornadaBegin;
     end;
end;

procedure TNomina.GetDatosJornadaEnd;
begin
     with Ritmos do
     begin
          GetDatosJornadaEnd;
     end;
end;

function TNomina.GetDatosJornada( const sTurno: String; const dInicial, dFinal: TDate ): TDatosJornada;
begin
     Result := Ritmos.GetDatosJornada( sTurno, dInicial, dFinal );
end;

procedure TNomina.InitDatosTurnoJornada;
begin
     with FDiasHoras do
     begin
          FDatosTurno := Ritmos.GetDatosTurno( Turno );
          with oZetaProvider.DatosPeriodo do
          begin
{$ifdef QUINCENALES}
               FDatosJornada := GetDatosJornada( Turno, InicioAsis, FinAsis );
{$else}
               FDatosJornada := GetDatosJornada( Turno, Inicio, Fin );
{$endif}
          end;
     end;
end;

function TNomina.DiasLaborados( const rHorasTrabajadas, rHorasJornada: TPesos; const eTipo: eClasifiPeriodo ; const TipoJornada: eTipoJornada ): TPesos;
var
   rDiasTipo: TPesos;
begin
     if ( rHorasJornada = 0 ) or ( rHorasTrabajadas = 0 ) then
        Result := 0
     else
     begin
	         rDiasTipo := DiasTipo( eTipo );
	         { Si es semana reducida, regresa el n�mero de d�as }
	         if ( TipoJornada in [ tjReducida_1..tjReducida_5 ] ) then
             rDiasTipo := ( rDiasTipo / 6 * ( Ord( TipoJornada ) - 1 ) );
          Result := ZetaCommonTools.rMin( ZetaCommonTools.MiRound( rHorasTrabajadas / rHorasJornada * rDiasTipo, 2 ), 99 );
     end;
end;

procedure TNomina.CalculaDiasIMSS( const iDiasNoHay: Integer ); { D�as de IMSS }
var
   lJornadaVacia: Boolean;
   iDiasPer, iAusentismos : Integer;
   lPeriodoSemanales:Boolean;
{$ifdef IMSS_VACA}
   lHayDiasSinGoce:Boolean;
   iDiasNT: Word;
{$endif}
begin
     iDiasPer := oZetaProvider.DatosPeriodo.Dias;

     with FDiasHoras do
     begin
          lJornadaVacia := ( HorasJornada = 0 );
{$ifdef IMSS_VACA}
          if FDiasCotizaSinFV then
          begin
               // Correcci�n de caso:
               // #12128: Skyworks: IMSS_OBRERO() Ramas IV y CV dias cotizados en Negativo.
               // lHayDiasSinGoce := FALSE; //( ( DiasSinGoce > 0 ) and ( DiasSinGoce >= DiasAsistencia ) ); -- Siempre evaluar los dias IMSS independientemente de los d�as de asistencia vs. dias sin goce.
               // lHayDiasSinGoce := ( DiasFaltaVacacion = 0 ) and ( DiasSinGoce >= DiasAsistencia );      // Si hay vacaciones siempre se calculan los d�as, si no hay se revisa de la forma anterior con Dias sin goce >= Asistencia
               lHayDiasSinGoce := ( DiasVacaTotales = 0 ) and ( DiasSinGoce >= DiasAsistencia );      // Si hay vacaciones siempre se calculan los d�as, si no hay se revisa de la forma anterior con Dias sin goce >= Asistencia

               iDiasNT := ( DiasNoTrabajados - DiasFaltaVacacion );
          end
          else
          begin
               lHayDiasSinGoce := ( DiasSinGoce >= DiasAsistencia );
               iDiasNT := DiasNoTrabajados;
          end;
          if lHayDiasSinGoce or lJornadaVacia then
{$else}
          if ( DiasSinGoce >= DiasAsistencia ) or lJornadaVacia then
{$endif}
             DiasSS := 0
          else
          begin
               iAusentismos := FaltasInjustificadas +
                               FaltasJustificadas +
                               DiasSinGoce +
                               DiasSuspension;

               if FDatosJornada.TipoJornada in [tjReducida_1,tjReducida_2,tjReducida_3,tjReducida_4,tjReducida_5 ] then
               begin
                    iAusentismos := GetAusentismosJornadaReducida( FDatosJornada.TipoJornada, iAusentismos );
               end;
               DiasSS := iMax( 0, iDiasPer +
                                  iDiasNoHay -
                                  iAusentismos -
                                  DiasIncapacidad -
                                  {$ifdef IMSS_VACA}
                                  iDiasNT );
                                  {$else}
                                  DiasNoTrabajados );
                                  {$endif}
          end;
{$ifdef ANTES}
          if ( DiasNoTrabajados >= DiasTurno ) or lJornadaVacia then	{ Dias de Enfermedad y Maternidad }
{$else}
{
Se deben usar DiasFaltaVacacion en lugar de DiasNoTrabajados, dado que el primero es un subconjunto del segundo. Adicionalmente, si se usan los DiasNoTrabajados, se cuentan todos los d�as antes de un ingreso � los d�as posteriores a la baja, lo cual provoca que los DiasEM sean calculados incorrectamente.
Al usar DiasFaltaVacacion se compara contra DiasTurno para evitar reportar DiasEM cuando se tienen vacaciones durante todos los d�as del turno.
}
          {$ifdef IMSS_VACA}
          if FDiasCotizaSinFV then
             lPeriodoSemanales := FALSE   // La nueva l�gica aplica para todos los periodos
          else
          {$endif}
          lPeriodoSemanales := ( oZetaProvider.DatosPeriodo.Clasifi in [tpSemanal ] );
          if lJornadaVacia or ( ( lPeriodoSemanales ) and ( DiasFaltaVacacion >= DiasTurno ) ) then
{$endif}
             DiasEM := 0
          else
              DiasEM := iMax( 0, iDiasPer + iDiasNoHay - DiasIncapacidad -
              {$ifdef IMSS_VACA}
                              iDiasNT );
              {$else}
                              DiasNoTrabajados );
              {$endif}
     end;
end;

procedure TNomina.CambiaStatusPeriodo( const eStatus: eStatusPeriodo );
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FDataset := CreateQuery( GetSQLScript( Q_PERIODO_CAMBIA_STATUS ) ); { Para Cambiar El Status a spSinCalcular }
          try
             with DatosPeriodo do
             begin
                  ParamAsInteger( FDataset, 'Year', Year );
                  ParamAsInteger( FDataset, 'Tipo', Ord( Tipo ) );
                  ParamAsInteger( FDataset, 'Numero', Numero );
             end;
             ParamAsInteger( FDataset, 'Status', Ord( eStatus ) );
             EmpiezaTransaccion;
             try
                Ejecuta( FDataset );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        TerminaTransaccion( False );
                        Log.Excepcion( 0, 'Error Al Cambiar Status Del Per�odo', Error );
                   end;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
end;

function TNomina.CalculaStatusPeriodo: eStatusPeriodo;
var
   FDataSet: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FDataSet := CreateQuery( GetSQLScript( Q_PERIODO_CALCULA_STATUS ) ); { Para Calcular el Status del Per�odo }
          try
             with DatosPeriodo do
             begin
                  ParamAsInteger( FDataSet, 'Year', Year );
                  ParamAsInteger( FDataSet, 'Tipo', Ord( Tipo ) );
                  ParamAsInteger( FDataSet, 'Numero', Numero );
                  ParamAsInteger( FDataSet, 'Usuario', UsuarioActivo );
             end;
             with FDataSet do
             begin
                  EmpiezaTransaccion;
                  try
{$ifdef INTERBASE}
                     Active := True;
                     if Eof then
                        Result := eStatusPeriodo( 0 )
                     else
                         Result := eStatusPeriodo( FieldByName( 'MAXSTATUS' ).AsInteger );
{$else}
                     ParamSalida( FDataset, 'MaxStatus' );
                     Ejecuta( FDataset );
                     Result := eStatusPeriodo( GetParametro( FDataset, 'MaxStatus' ).AsInteger );
{$endif}
                     TerminaTransaccion( True );
                  except
                        on Error: Exception do
                        begin
                             TerminaTransaccion( False );
                             Result := eStatusPeriodo( 0 );
                        end;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          DQueries.ValidaLimiteBloqueo( DatosPeriodo, oZetaProvider, Result );
     end;
end;

{ ******* D�as de Vacaciones a Pagar ******* }

procedure TNomina.GetPagoVacacionesBegin;
begin
     with oZetaProvider do
     begin
          FPagoVacaciones := CreateQuery( GetSQLScript( Q_VACACIONES_PAGO ) );
     end;
end;

procedure TNomina.GetPagoVacacionesEnd;
begin
     FreeAndNil( FPagoVacaciones );
end;

function TNomina.GetPagoVacaciones( const iEmpleado: TNumEmp; var rDiasPrimaVaca : TDiasHoras ): TDiasHoras;
begin
     with FPagoVacaciones do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FPagoVacaciones, 'Empleado', iEmpleado );
               with DatosPeriodo do
               begin
                    ParamAsInteger( FPagoVacaciones, 'Year', Year );
                    ParamAsInteger( FPagoVacaciones, 'Tipo', Ord( Tipo ) );
                    ParamAsInteger( FPagoVacaciones, 'Numero', Numero );
               end;
          end;
          Active := True;
          if Eof then
          begin
               Result := 0;
               rDiasPrimaVaca := 0;
          end
          else
          begin
               Result := Fields[ 0 ].AsFloat;
               rDiasPrimaVaca := Fields[ 1 ].AsFloat;
          end;
          Active := False;
     end;
end;

{$ifdef IMSS_VACA}
{ ******* Rango de D�as de Vacaciones Pagadas ******* }

procedure TNomina.GetDiasPagoVacacionesBegin;
begin
     with oZetaProvider do
     begin
          FDiasPagoVacaciones := CreateQuery( GetSQLScript( Q_DIAS_VACACIONES_PAGO ) );
          FFechasPagoVacaciones := CreateQuery( GetSQLScript( Q_FECHAS_VACACIONES_PAGO ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FDiasPagoVacaciones, 'Year', Year );
               ParamAsInteger( FDiasPagoVacaciones, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FDiasPagoVacaciones, 'Numero', Numero );
               ParamAsInteger( FFechasPagoVacaciones, 'Year', Year );
               ParamAsInteger( FFechasPagoVacaciones, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FFechasPagoVacaciones, 'Numero', Numero );
          end;
          FChkAusenciaFestivos := CreateQuery( GetSQLScript( Q_CHK_AUSENCIA_FESTIVOS ) );
     end;
end;

procedure TNomina.GetDiasPagoVacacionesEnd;
begin
     if Assigned( FRitmosPagoVaca ) then
     begin
          FQueriesPagoVaca.GetStatusEmpleadoEnd;
          FRitmosPagoVaca.RitmosEnd;
          FreeAndNil( FQueriesPagoVaca );
          FreeAndNil( FRitmosPagoVaca );
     end;
     FreeAndNil( FChkAusenciaFestivos );
     FreeAndNil( FDiasPagoVacaciones );
     FreeAndNil( FFechasPagoVacaciones );
end;

procedure TNomina.InicializaRitmosPagoVaca;
var
   dInicio, dFinal: TDate;
begin
     FRitmosPagoVaca := TRitmos.Create( oZetaProvider );
     if not Assigned( FQueriesPagoVaca ) then
        FQueriesPagoVaca := TCommonQueries.Create( oZetaProvider );
     FRitmosPagoVaca.Queries := FQueriesPagoVaca;
     with FFechasPagoVacaciones do
     begin
          Active := TRUE;
          if ( not EOF ) then
          begin
               dInicio := FieldByName( 'FECHA_INI' ).AsDateTime;
               dFinal := FieldByName( 'FECHA_FIN' ).AsDateTime;
          end
          else
          begin
               with oZetaProvider.DatosPeriodo do
               begin
                    dInicio := FirstDayOfYear( Year );
                    dFinal := LastDayOfYear( Year );
               end;
          end;
          Active := FALSE;
     end;
     FRitmosPagoVaca.RitmosBegin( dInicio, dFinal );
     FQueriesPagoVaca.GetStatusEmpleadoBegin;
end;

function TNomina.GetDiasVacaSinFestivos( const iEmpleado: Integer; const dStart, dEnd: TDate ): Word;
var
   dAusencia: TDate;
   oStatusFestivo: eStatusFestivo;
   StatusEmpleado: eStatusEmpleado;
   StatusDia: eStatusAusencia;
   TipoDia: eTipoDiaAusencia;
   lExisteTarjeta, lFestivo: Boolean;
begin
     Result := 0;
     if not Assigned( FRitmosPagoVaca ) then
        InicializaRitmosPagoVaca;
     with FChkAusenciaFestivos do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FChkAusenciaFestivos, 'Empleado', iEmpleado );
               ParamAsDate( FChkAusenciaFestivos, 'FechaIni', dStart );
               ParamAsDate( FChkAusenciaFestivos, 'FechaFin', dEnd );
          end;
          Active := True;
          dAusencia := dStart;
          Repeat
                lExisteTarjeta := ( not EOF ) and ( FieldByName( 'AU_FECHA' ).AsDateTime = dAusencia );
                if lExisteTarjeta then
                begin
                     oStatusFestivo := eStatusFestivo( FieldByName('AU_STA_FES').AsInteger );
                     StatusDia := eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger );
                     Next; { Avanza para analizar siguiente fecha }
                end
                else
                begin
                     oStatusFestivo := esfAutomatico;
                     StatusDia := FRitmosPagoVaca.GetEmpleadoDatosTurno( iEmpleado, dAusencia, TRUE ).StatusHorario.Status;    // Considera los Intercambios de Horario en Dias Festivos
                end;
                case oStatusFestivo of
                     esfFestivo: lFestivo := True;
                     esfFestivoTransferido: lFestivo := False;
                else
                    lFestivo := FRitmosPagoVaca.Festivos.CheckFestivo( FDiasHoras.Turno, dAusencia );
                end;
                StatusEmpleado := FQueriesPagoVaca.GetStatusEmpleado( iEmpleado, dAusencia );
                TipoDia := ZetaServerTools.GetTipoDia( StatusEmpleado, lFestivo, FFestivoEnDescanso, FDiasCotizaSinFV, StatusDia );
                // Determinar si se acumula como d�a normal o de vacaciones (Quitar festivos y dias no trabajados)
                if ( TipoDia in [ daNormal, daVacaciones ] ) then
                   Result := Result + 1;
                dAusencia := dAusencia + 1;
          Until ( dAusencia > dEnd );
          Active := False;
     end;
end;

function TNomina.GetDiasPagoVacaciones( const iEmpleado: TNumEmp ): Word;
var
   dStart, dEnd: TDate;
begin
     Result := 0;
     with FDiasPagoVacaciones do
     begin
          Active := FALSE;
          with oZetaProvider do
          begin
               ParamAsInteger( FDiasPagoVacaciones, 'Empleado', iEmpleado );
               with DatosPeriodo do
               begin
                    ParamAsInteger( FDiasPagoVacaciones, 'Year', Year );
                    ParamAsInteger( FDiasPagoVacaciones, 'Tipo', Ord( Tipo ) );
                    ParamAsInteger( FDiasPagoVacaciones, 'Numero', Numero );
               end;
          end;
          Active := TRUE;
          while not EOF do
          begin
               { Ajusta l�mites de VACACIONES a los l�mites }
               { del per�odo de referencia }
               dStart := FieldByName( 'VA_FEC_INI' ).AsDateTime;
               dEnd := ( FieldByName( 'VA_FEC_FIN' ).AsDateTime - 1 );
               if ( dEnd >= dStart ) then     // Hay d�as de vacaciones
               begin
                    { rango de d�as de Vacaciones }
                    Result := Result + GetDiasVacaSinFestivos( iEmpleado, dStart, dEnd );
               end;
               Next;
          end;
          Active := FALSE;
     end;
end;
{$endif}

{ ******* D�as de Subsidio de Incapacidad a Pagar ******* }

procedure TNomina.GetSubsidioIncapacidadBegin;
begin
     with oZetaProvider do
     begin
          FSubsidioIncapacidades := CreateQuery( GetSQLScript( Q_SUBSIDIO_INCAPACIDAD ) );
     end;
end;

procedure TNomina.GetSubsidioIncapacidadEnd;
begin
     FreeAndNil( FSubsidioIncapacidades );
end;

function TNomina.GetSubsidioIncapacidad( const iEmpleado: TNumEmp ): TDiasHoras;
begin
     with FSubsidioIncapacidades do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FSubsidioIncapacidades, 'Empleado', iEmpleado );
               with DatosPeriodo do
               begin
                    ParamAsInteger( FSubsidioIncapacidades, 'Year', Year );
                    ParamAsInteger( FSubsidioIncapacidades, 'Tipo', Ord( Tipo ) );
                    ParamAsInteger( FSubsidioIncapacidades, 'Numero', Numero );
               end;
          end;
          Active := True;
          if Eof then
             Result := 0
          else
              Result := FieldByName( 'SUBSIDIO' ).AsFloat;
          Active := False;
     end;
end;

{ ******* D�as de incapacidades anteriores reportadas esta n�mina a RH ******* }

procedure TNomina.SetDiasIncapacidadAnteriorBegin;
begin
     with oZetaProvider do
     begin
          FIncapacidadAnterior := CreateQuery( GetSQLScript( Q_INCAPACIDAD_ANTERIOR ) );
     end;
end;

procedure TNomina.SetDiasIncapacidadAnteriorEnd;
begin
     FreeAndNil( FIncapacidadAnterior );
end;

procedure TNomina.SetDiasIncapacidadAnterior( const iEmpleado: TNumEmp; const dInicial, dFinal: TDate );
var
   dStart, dEnd: TDate;
   iDiasInca, iDiasIH, iDiasID, iDiasIT, iDiasSubsidio: Integer;
begin
     with FIncapacidadAnterior do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FIncapacidadAnterior, 'Empleado', iEmpleado );
               ParamAsDate( FIncapacidadAnterior, 'FechaInicial', dInicial );
               ParamAsDate( FIncapacidadAnterior, 'FechaInicial2', dInicial );
               ParamAsDate( FIncapacidadAnterior, 'FechaFinal', dFinal );
          end;
          Active := True;
          if ( not Eof ) then
          begin
               dStart := FieldByName( 'IN_FEC_INI' ).AsDateTime;
               dEnd := ZetaServerTools.dMin( ( FieldByName( 'IN_FEC_FIN' ).AsDateTime - 1 ), dInicial - 1 );
               iDiasInca := Round( ( dEnd - dStart + 1 ) );
               if ( iDiasInca > 0 ) then
               begin
                    with FDiasHoras do
                    begin
                         DiasIncapacidad := ( DiasIncapacidad + iDiasInca );
                    end;
                    if ( FieldByName( 'IN_TIPO' ).AsString = FIncidenciaIncapGeneral ) then    // Es incapacidad general
                    begin
                         iDiasIH := 0;
                         iDiasID := 0;
                         if eMotivoIncapacidad( FieldByName( 'IN_MOTIVO' ).AsInteger ) = miInicial then         // El subsidio solo aplica en incapacidades iniciales
                         begin
                              iDiasSubsidio := ZetaCommonTools.iMin( K_DIAS_SUBSIDIO_INCAP, iDiasInca );
                              if ( iDiasSubsidio > 0 ) then
                              begin
                                   dEnd := ( dStart + iDiasSubsidio - 1 );
                                   iDiasIH := Ritmos.GetDiasHabiles( FDatosTurno, dStart, dEnd, FPrimerDia );
                                   iDiasID := Round( ( dEnd - dStart + 1 ) ) - iDiasIH;
                              end;
                         end;
                         iDiasIT := ( iDiasInca - ( iDiasIH + iDiasID ) );
                         with FDiasHoras do
                         begin
                              DiasIncapSubsidioHabil := ( DiasIncapSubsidioHabil + iDiasIH );
                              DiasIncapSubsidioDescanso := ( DiasIncapSubsidioDescanso + iDiasID );
                              DiasIncapSinSubsidio := ( DiasIncapSinSubsidio + iDiasIT );
                         end;
                    end;
               end;
               Next;
          end;
          Active := False;
     end;
end;

{$ifdef CAMBIO_TNOM}
procedure TNomina.GetDatosEmpleadoBegin;
begin
     { Se usa este cursor para investigar el status y la n�mina del empleado }
     with oZetaProvider do
     begin
          FDatosEmpleadoTemp := CreateQuery( Format( GetSQLScript( Q_DATOS_EMPLEADO_TEMP ), [ ZetaCommonTools.DateToStrSQLC( DatosPeriodo.Inicio ),
                                                                                              ZetaCommonTools.DateToStrSQLC( DatosPeriodo.Fin ) ]));
     end;
end;

procedure TNomina.GetDatosEmpleadoEnd;
begin
     FreeAndNil( FDatosEmpleadoTemp );
end;
{$else}
{ ********* Record de Informaci�n de Status del Empleado ******* }

procedure TNomina.GetStatusEmpleadoBegin;
begin
     with oZetaProvider do
     begin
          FEmpleadoStatus := CreateQuery( GetSQLScript( Q_EMPLEADO_STATUS ) );
     end;
end;

procedure TNomina.GetStatusEmpleadoEnd;
begin
     FreeAndNil( FEmpleadoStatus );
end;
{$endif}

{$ifdef CAMBIO_TNOM}
procedure TNomina.GetDatosEmpleado(const iEmpleado: TNumEmp; var FStatusEmp: TStatusEmpleado; var FNominaEmp: TNominaEmpleado; const lStatusDesFinPer: Boolean );
var
   dFechaAlta: TDate;

    procedure ValidaFechaCambioTNom;
    begin
         with FNominaEmp do
         begin
              if ( Nomina <> NominaFin ) and ( FechaCambioTNom > oZetaProvider.DatosPeriodo.Fin ) then
              begin
                   with Queries do
                   begin
                        GetFechaCambioTNomBegin;
                        try
                           GetFechaCambioTNom( iEmpleado, oZetaProvider.DatosPeriodo.Fin );
                        finally
                              GetFechaCambioTNomEnd;
                        end;
                   end;
              end;
         end;
    end;

begin
     { Se cambi� el m�todo GetStatusEmpleado por GetDatosEmpleado para regresar los dos records, FStatus y FNomina }

     with FDatosEmpleadoTemp do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FDatosEmpleadoTemp, 'Empleado', iEmpleado );

               Active := True;
               with FStatusEmp do
               begin
                    Activo := ZetaCommonTools.zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
                    FechaIngreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
                    FechaBaja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;

                    { Si la fecha de ingreso o baja es mayor a la fecha de asistencia del periodo investiga fechas
                      en base a Kardex }
                    if ( lStatusDesFinPer ) and ( FechaBaja > DatosPeriodo.Inicio ) and (  FechaIngreso > DatosPeriodo.Fin ) then
                    begin
                         with Queries do
                         begin
                              GetFechaAltaBegin;
                              GetStatusActEmpleadoBegin;
                              try
                                 dFechaAlta:= GetFechaAlta( DatosPeriodo.FinAsis, iEmpleado );
                                 if ( dFechaAlta <> FechaIngreso ) then
                                 begin
                                      { Se trae el activo para saber si va a hacer cortes o no }
                                      { Para la fecha que se pide el empleado es considerado como antes de reingreso }
                                      Activo:= not ( GetStatusActEmpleado( iEmpleado, DatosPeriodo.FinAsis ) in [ steAnterior, steBaja ] );
                                      FechaIngreso:= dFechaAlta;
                                 end;
                              finally
                                     GetFechaAltaEnd;
                                     GetStatusActEmpleadoEnd;
                              end;
                         end;
                    end;

               end;
          end;

          with FNominaEmp do
          begin
               Nomina := eTipoPeriodo( FieldByName( 'CB_NOMINA' ).AsInteger );
               NominaFin := eTipoPeriodo( FieldByName( 'NOMINA_FIN' ).AsInteger );
               FechaCambioTNom := FieldByName( 'CB_FEC_NOM' ).AsDateTime;
          end;
          Active := False;
          { Valida que realmente la fecha de cambio de tipo de n�mina sea la adecuada }
          ValidaFechaCambioTNom;
     end;
end;
{$else}
function TNomina.GetStatusEmpleado( const iEmpleado: TNumEmp): TStatusEmpleado;
begin
     with FEmpleadoStatus do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FEmpleadoStatus, 'Empleado', iEmpleado );
          end;
          Active := True;
          with Result do
          begin
               Activo := ZetaCommonTools.zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
               FechaIngreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
               FechaBaja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
          end;
          Active := False;
     end;
end;
{$endif}

{ ******** Investigar Planes de vacaciones *********** }
{$ifdef QUINCENALES}
procedure TNomina.SetPlanVacacionesBegin;
begin
     FAplicaPlanVacacion := oZetaProvider.GetGlobalBooleano( K_GLOBAL_PLAN_VACA_PRENOMINA );
     if FAplicaPlanVacacion then
     begin
          FPlanVacacion := TPlanVacacion.Create( FCreator );
          FPlanVacacion.Preparar;
     end;
end;

procedure TNomina.SetPlanVacacionesEnd;
begin
     if Assigned( FPlanVacacion ) then
        FreeAndNil( FPlanVacacion );
end;

procedure TNomina.SetPlanVacaciones( const iEmpleado: Integer; const dInicio, dFin: TDate );
begin
     if FAplicaPlanVacacion then
        FPlanVacacion.Procesar( iEmpleado, dInicio, dFin );
end;
{$endif}

{ ******** Investigar Pago de prima vacacional *********** }

procedure TNomina.SetPagoPrimaAniversarioBegin;
begin
     FPagoPrimaEnAniversario := oZetaProvider.GetGlobalBooleano( K_GLOBAL_PAGO_PRIMA_VACA_ANIV );
     if FPagoPrimaEnAniversario then
     begin
          FPrimaAniversario := TPrimaAniversario.Create( FCreator );
          FPrimaAniversario.Preparar;
     end;
end;

procedure TNomina.SetPagoPrimaAniversarioEnd;
begin
     if Assigned( FPrimaAniversario ) then
          FreeAndNil( FPrimaAniversario );
end;

procedure TNomina.SetPagoPrimaAniversario( const iEmpleado: Integer; const dAntig: TDate );
var
   dAniv: TDate;
begin
     if FPagoPrimaEnAniversario then
     begin
          with oZetaProvider.DatosPeriodo do
          begin
               if ( Uso = upOrdinaria ) then
               begin
                    dAniv := NextDate( dAntig, Fin );
                    if ( dAniv >= Inicio ) and ( dAniv <= Fin ) then    // El aniversario cae en el periodo de pago
                       FPrimaAniversario.Procesar( iEmpleado, dAntig, dAniv );
               end;
          end;
     end;
end;

{ ******** Llenado de FDias Horas *********** }

procedure TNomina.LeeDiasHorasBegin;
begin
     with oZetaProvider do
     begin
          FNominaLee := CreateQuery( GetSQLScript( Q_NOMINA_LEE ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FNominaLee, 'Year', Year );
               ParamAsInteger( FNominaLee, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FNominaLee, 'Numero', Numero );
          end;
     end;
end;

procedure TNomina.LeeDiasHorasEnd;
begin
     FreeAndNil( FNominaLee );
end;

procedure TNomina.SetDiasHorasBegin;
begin
     with oZetaProvider do
     begin
          FPermisosLee := CreateQuery( GetSQLScript( Q_PERMISOS_LEE ) );
          FIncapacidadesLee := CreateQuery( GetSQLScript( Q_INCAPACIDADES_LEE ) );
          FVacacionesLee := CreateQuery( GetSQLScript( Q_VACACIONES_LEE ) );
     end;
end;

procedure TNomina.SetDiasHorasEnd;
begin
     FreeAndNil( FVacacionesLee );
     FreeAndNil( FIncapacidadesLee );
     FreeAndNil( FPermisosLee );
end;

procedure TNomina.SetDiasHorasCeros;
begin
     with FDiasHoras do
     begin
          { Dias }
          DiasAguinaldo := 0;
          DiasAjuste := 0;
          DiasAsistencia := 0;
          DiasFaltaVacacion := 0;
          DiasIncapacidad := 0;
          DiasIncapSubsidioHabil := 0;
          DiasIncapSubsidioDescanso := 0;
          DiasIncapSinSubsidio := 0;
          DiasSinGoce := 0;
          DiasConGoce := 0;
          DiasNoTrabajados := 0;
          DiasOtroPermiso := 0;
          DiasRetardo := 0;
          DiasSuspension := 0;
          DiasVacaciones := 0;
          DiasPrimaVaca  := 0;
          DiasSubsidioIncapacidad := 0;
          FaltasJustificadas := 0;
          FaltasInjustificadas := 0;
          {$ifdef IMSS_VACA}
          DiasVacaTotales := 0;
          DiasVacaPagadosCalendario := 0;
          {$endif}
	         { Horas }
          Horas := 0;
          HorasConGoce := 0;
          HorasSinGoce := 0;
          HorasDobles := 0;
          HorasTriples := 0;
          HorasExtras := HorasDobles + HorasTriples;
          HorasTardes := 0;
{$ifdef QUINCENALES}
          HorasNoTrabajadas := 0;
          InicioAsistencia := NullDateTime;
          FinAsistencia := NullDateTime;
{$endif}
          HorasAdicionales := 0;
          HorasDescansoTrabajado := 0;
          HorasFestivoTrabajado := 0;
          HorasPrimaDominical := 0;
          HorasPrimaDominicalTotal := 0;
          HorasVacacionesTrabajadas := 0;
          HorasFaltaVacacion := 0;
          HorasExtPrePagadas := 0;
{$ifdef CAMBIO_TNOM}
          DiasPeriodoCal:= 0;
{$endif}
          DiasHabiles := 0;
          DiasDescansoTrabajado := 0;
          DiasFestivos := 0;
          DiasFestivoTrabajado := 0;
     end;
end;

procedure TNomina.SetDiasHorasCompletas( const iEmpleado: TNumEmp );
var
   dInicial, dFinal,dFecCambioTnom: TDate;
   dias_ausent, dias_nt, dias_falta, dias_fv, dias_nt_cal: Integer;
   dias_per, dias_no_hay, dias_inca, dias_cg, dias_sg, dias_fj, dias_su, dias_ot, dias_dif: Integer;
   dias_inca_subhabil, dias_inca_subdesc, dias_inca_sinsub: Integer;
   HrsPorDia, HrsFaltas: TDiasHoras;
   FStatus: TStatusEmpleado;
   {$ifdef CAMBIO_TNOM}
   FNomina: TNominaEmpleado;
   lCambioTNom,lCalculandoNomCambio,lBajaEnPer,
   lIngresoEnPerSCambio: Boolean;
   {$endif}
{$ifdef IMSS_VACA}
   dias_vt: Integer;
{$endif}

function GetDiasIncapacidades( const dInicial, dFinal: TDate; var iDiasInca, iDiasFalta, iDiasIH, iDiasID, iDiasIT: Integer ): Boolean;
var
   dStart, dEnd, dInicioInc: TDate;
   iDiasReg, iDiasSubsidio, iHabiles, iDescansos: Integer;
   sIncidencia: String;
begin
     { Aqu� se puede optimizar: }
     { Si tenemos el campo en COLABORA con la FECHA FINAL }
     { de la ultima incapacidad, evitamos el query }
     with FIncapacidadesLee do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FIncapacidadesLee, 'Empleado', iEmpleado );
               ParamAsDate( FIncapacidadesLee, 'FechaFinal', dFinal );
               ParamAsDate( FIncapacidadesLee, 'FechaInicial', dInicial );
          end;
          Active := True;
          if Eof then
             Result := False
          else
          begin
               Result := True;
               iDiasInca := 0;
               iDiasFalta := 0;
               iDiasIH := 0;
               iDiasID := 0;
               iDiasIT := 0;
               while not Eof do
               begin
                    { Ajusta l�mites de incapacidad a los l�mites }
                    { del per�odo de referencia }
                    dInicioInc := FieldByName( 'IN_FEC_INI' ).AsDateTime;
                    dStart := ZetaServerTools.dMax( dInicioInc, dInicial );
                    dEnd := ZetaServerTools.dMin( ( FieldByName( 'IN_FEC_FIN' ).AsDateTime - 1 ), dFinal );
                    sIncidencia := FieldByName( 'IN_TIPO' ).AsString;
                    if ( dEnd >= dStart ) then
                    begin
                         { D�as de Incapacidad, sin importar el Status del D�a }
                         iDiasReg := Round( ( dEnd - dStart + 1 ) );
                         iDiasInca := iDiasInca + iDiasReg;
                         { D�as de Incapacidad que caen en D�a H�bil }
                         iDiasFalta := iDiasFalta + Ritmos.GetDiasHabiles( FDatosTurno, dStart, dEnd, FPrimerDia );
                         if ( FIncidenciaIncapGeneral = sIncidencia ) then    // Es incapacidad general
                         begin
                              iHabiles := 0;
                              iDescansos := 0;
                              if eMotivoIncapacidad( FieldByName( 'IN_MOTIVO' ).AsInteger ) = miInicial then         // El subsidio solo aplica en incapacidades iniciales
                              begin
                                   iDiasSubsidio := K_DIAS_SUBSIDIO_INCAP - ZetaCommonTools.iMax( 0, Round( dInicial - dInicioInc ) );
                                   if ( iDiasSubsidio > 0 ) then
                                   begin
                                        dEnd := ZetaServerTools.dMin( dEnd, dStart + iDiasSubsidio - 1 );
                                        iHabiles := Ritmos.GetDiasHabiles( FDatosTurno, dStart, dEnd, FPrimerDia );
                                        iDescansos := Round( ( dEnd - dStart + 1 ) ) - iHabiles;
                                        iDiasIH := iDiasIH + iHabiles;
                                        iDiasID := iDiasID + iDescansos;
                                   end;
                              end;
                              iDiasIT := iDiasIT + ( iDiasReg - ( iHabiles + iDescansos ) );
                         end;
                    end;
                    Next;
               end;
          end;
          Active := False;
     end;
end;

function GetDiasPermisos( const dInicial, dFinal: TDate; var iDiasCG, iDiasSG, iDiasFJ, iDiasSU, iDiasOT: Integer ): Boolean;
var
   dStart, dEnd: TDate;
   iDiasHabiles: Integer;
begin
     { Aqu� se puede optimizar: }
     { Si tenemos el campo en COLABORA con la FECHA FINAL del ultimo permiso, }
     { evitamos el query }
     with FPermisosLee do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FPermisosLee, 'Empleado', iEmpleado );
               ParamAsDate( FPermisosLee, 'FechaFinal', dFinal );
               ParamAsDate( FPermisosLee, 'FechaInicial', dInicial );
          end;
          Active := True;
          if Eof then
             Result := False
          else
          begin
               Result := True;
	              iDiasCG := 0;
	              iDiasSG := 0;
	              iDiasFJ := 0;
	              iDiasSU := 0;
	              iDiasOT := 0;
	              while not Eof do
               begin
                    { Ajusta l�mites de PERMISO a los l�mites del }
                    { per�odo  de referencia }
                    dStart := ZetaServerTools.dMax( FieldByName( 'PM_FEC_INI' ).AsDateTime, dInicial );
                    dEnd := ZetaServerTools.dMin( ( FieldByName( 'PM_FEC_FIN' ).AsDateTime - 1 ), dFinal );
                    if ( dEnd >= dStart ) then
                    begin
                         iDiasHabiles := Ritmos.GetDiasHabiles( FDatosTurno, dStart, dEnd, FPrimerDia );
                         case eTipoPermiso( FieldByName( 'PM_CLASIFI' ).AsInteger ) of
                              tpConGoce: iDiasCG := iDiasCG + iDiasHabiles;
                              tpSinGoce: iDiasSG := iDiasSG + iDiasHabiles;
                              tpFaltaJustificada: iDiasFJ := iDiasFJ + iDiasHabiles;
                              tpSuspension: iDiasSU := iDiasSU + iDiasHabiles;
                              tpOtros: iDiasOT := iDiasOT + iDiasHabiles;
                         end;
                    end;
                    Next;
               end;
          end;
          Active := False;
     end;
end;

function GetDiasVacaciones( const dInicial, dFinal: TDate; var iDiasVaca{$ifdef IMSS_VACA}, iDiasVacaTotal{$endif}: Integer ): Boolean;
var
   dStart, dEnd: TDate;
begin
     { Aqu� se puede optimizar: }
     { Si tenemos el campo en COLABORA con la FECHA FINAL }
     { de las ultimas vacaciones, evitamos el query }
     with FVacacionesLee do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FVacacionesLee, 'Empleado', iEmpleado );
               ParamAsDate( FVacacionesLee, 'FechaFinal', dFinal );
               ParamAsDate( FVacacionesLee, 'FechaInicial', dInicial );
          end;
          Active := True;
          if Eof then
             Result := False
          else
          begin
               Result := True;
               iDiasVaca := 0;
{$ifdef IMSS_VACA}
               iDiasVacaTotal := 0;
{$endif}
               while not Eof do
               begin
                    { Ajusta l�mites de VACACIONES a los l�mites }
                    { del per�odo de referencia }
                    dStart := ZetaServerTools.dMax( FieldByName( 'VA_FEC_INI' ).AsDateTime, dInicial );
                    dEnd := ZetaServerTools.dMin( ( FieldByName( 'VA_FEC_FIN' ).AsDateTime - 1 ), dFinal );
                    if ( dEnd >= dStart ) then
                    begin
                         { D�as de Vacaciones que caen en D�a H�bil }
                         iDiasVaca := iDiasVaca + Ritmos.GetDiasHabiles( FDatosTurno, dStart, dEnd, FPrimerDia );
{$ifdef IMSS_VACA}
                         iDiasVacaTotal := iDiasVacaTotal + ZetaCommonTools.DaysBetween( dStart, dEnd );
{$endif}
                    end;
                    Next;
               end;
          end;
          Active := False;
     end;
end;

{AP(08/Feb/2010):Funci�n que determina la diferencia entre el Inicio de N�mina y Fin
(Las fechas pueden ser diferente con respecto al periodo por los cortes de altas, bajas y cambios de tipo de n�mina.
Con la correcci�n el defecto 1628 los d�as despues de la baja son considerados d�as no trabajados
independientemente si son o no h�biles y del tipo de n�mina}
procedure SetDiasNoTrabajados;
begin
     with FDiasHoras do
     begin
          { Los dias despues de la baja o el cambio }
          if ( dFinal < FinAsistencia ) then
          begin
               dias_dif:= Trunc( FinAsistencia ) - Trunc( dFinal );
               dias_nt_cal := dias_nt_cal + dias_dif;
               { Para quincenales los dias no trabajados siempre son iguales a dias calendario}
               if (oZetaProvider.DatosPeriodo.Clasifi  in [ tpQuincenal ] ) then
               begin
                    dias_nt:= dias_nt_cal;
               end
               else
               begin
                    { Cuando no hubo cambio de tipo de n�mina, los dias trabajados siguen considerando solamente
                    los d�as h�biles}
                    dias_dif:= Ritmos.GetDiasHabiles( FDatosTurno, ( dFinal + 1 ), FinAsistencia, FPrimerDia );
                    dias_nt := dias_nt + dias_dif;
                    {$ifndef DEF_1628}
                    if ( not lCambioTNom ) then
                    begin
                         dias_nt_cal:= dias_nt;
                    end;
                    {$endif}
               end;
          end;
          { Los dias antes de la alta o el cambio }
          if ( dInicial > InicioAsistencia ) then
          begin
               dias_dif:= Trunc( dInicial ) - Trunc( InicioAsistencia );
               dias_nt_cal := dias_nt_cal + dias_dif;
               if ( oZetaProvider.DatosPeriodo.Clasifi in [ tpQuincenal ] ) then
               begin
                    dias_nt:= dias_nt_cal;
               end
               else
               begin
                    { Cuando no hubo cambio de tipo de n�mina, los dias trabajados siguen considerando solamente
                    los d�as h�biles}
                    dias_dif:= Ritmos.GetDiasHabiles( FDatosTurno, InicioAsistencia, ( dInicial - 1 ), FPrimerDia );
                    dias_nt := dias_nt + dias_dif;
                    {$ifndef DEF_1628}
                    if ( not lCambioTNom ) then
                    begin
                         dias_nt_cal:= dias_nt;
                    end;
                    {$endif}
               end;
               {$ifdef antes}
               dias_dif:= Ritmos.GetDiasHabiles( FDatosTurno, Inicio, ( dInicial - 1 ), FPrimerDia );
               dias_nt := dias_nt + dias_dif;
               if ( lCambioTNom ) then
               begin
                    dias_dif:= Trunc( dInicial ) - Trunc( Inicio );
                    dias_nt_cal := dias_nt_cal + dias_dif;
               end
               else
               begin
                    dias_nt_cal:= dias_nt;
               end;
               {$endif}
          end;
     end;
end;

begin
     {$ifdef CAMBIO_TNOM}
     GetDatosEmpleado( iEmpleado, FStatus, FNomina, TRUE );
     {$else}
     FStatus:= GetStatusEmpleado( iEmpleado );
     {$endif}

     with oZetaProvider do
     begin
          with DatosPeriodo do
          begin
{$ifdef QUINCENALES}
   { ************************************************************************************
     * Este metodo se manda llamar cuando no se usa prenomina, por lo que en apariencia *
     * no deberian usarse las fechas de asistencia, pero no usarlo afecta en llamadas   *
     * como Ritmos.GetDiasNoHay() que usan las fechas de asistencia. Por este motivo    *
     * se considera mejor asegurarse que cuando no se use prenomina las fechas del      *
     * periodo y de asistencia del periodo sean las mismas, de esta forma se logra el   *
     * comportamiento anterior.                                                         *
     ************************************************************************************ }
               //dInicial := InicioAsis;
               //ZetaCommonTools.EsBajaenPeriodo( FStatus.FechaIngreso, FStatus.FechaBaja, DatosPeriodo, dFinal );
               ZetaCommonTools.EsBajaenPeriodo( FStatus.FechaIngreso, FStatus.FechaBaja, DatosPeriodo, dInicial, dFinal );
               SetPlanVacaciones( iEmpleado, dInicial, dFinal );
{$else}
               dInicial := Inicio;
               dFinal := Fin;
{$endif}
               dias_per := Dias;
               dFecCambioTnom:= FNomina.FechaCambioTNom;

          end;
          dias_no_hay := Ritmos.GetDiasNoHay( DatosPeriodo, FDatosJornada, FDatosTurno, FPrimerDia );
     end;
     SetDiasHorasCeros;
     with FDiasHoras do
     begin

{$ifdef QUINCENALES}
          { Almacena fechas de Asistencia }
          InicioAsistencia := dInicial;
          FinAsistencia := dFinal;
{$endif}

{$ifdef CAMBIO_TNOM}
          with FNomina do
          begin
               lCambioTNom:= ( EsCambioTNomPeriodo( FechaCambioTNom, oZetaProvider.DatosPeriodo ) ) and ( NominaFin <> Nomina );
          end;
{$endif}
          { Faltas de Asistencia }
          dias_nt	:= 0;
          dias_falta	:= 0;
          dias_fv	:= 0;
{$ifdef IMSS_VACA}
          dias_vt       := 0;
{$endif}
          dias_dif      := 0;
          dias_nt_cal   := 0;
          { D�as N�mina }
          DiasAsistencia := DiasTurno;
          { Horas N�mina }
          Horas := HorasJornada;
          HorasPrimaDominical := FDatosTurno.Domingo;
          if ( DiasTurno = 0 ) then
             HrsPorDia := 0
          else
              HrsPorDia := HorasJornada / DiasTurno;

          {$ifdef CAMBIO_TNOM}
          with FStatus do
          begin
               { Lo primero es determinar que n�mina se esta calculando, la anterior al cambio o la posterior al cambio. }
                lCalculandoNomCambio:= ( lCambioTNom ) and ( FNomina.NominaFin = oZetaProvider.DatosPeriodo.Tipo );
               { Estas variables son para saber si solo fue baja y reingreso
                 lBajaEnPer: El empleado no esta activo y fecha de baja es menor al rango final determinado por EsBajaEnPeriodo
                 lIngresoEnPerCambio: Fecha de Ingreso es mayor, siempre y cuando: no sea una baja y reingreso al siguiente dia  }
                lBajaEnPer:= ( not Activo and ( FechaBaja < dFinal ) ) ;
                lIngresoEnPerSCambio:= ( FechaIngreso > dInicial ) and  ( not ( FechaBaja = ( FechaIngreso - 1 ) ) )  and ( not lCambioTNom ) ;

                if ( lCambioTNom or lBajaEnPer or lIngresoEnPerSCambio ) then
                begin
                     { Calcular el final: Opciones: Baja o cambio de tipo de n�mina
                       Si no tiene cambio de tipo de n�mina y es solo baja, ese siempre va a ser el final
                       Si tiene un cambio combinado con la baja del empleado.
                       En caso que no se este calculando la n�mina del cambio se toma el m�nimo,
                       si la baja esta despues del cambio tiene que ser en la n�mina del cambio }

                     if ( lBajaEnPer ) and
                         ( ( not lCambioTNom ) or ( ( FechaBaja > dFecCambioTnom ) and ( lCalculandoNomCambio ) ) ) then
                     begin
                          dFinal:= FechaBaja;
                     end
                     else if ( FNomina.NominaFin <> oZetaProvider.DatosPeriodo.Tipo ) then
                     begin
                          if ( ( FechaBaja >= dInicial ) and ( FechaBaja < dFinal ) ) then
                          begin
                               dFinal:= dMin( FechaBaja, dFecCambioTnom - 1);
                          end
                          else
                              dFinal:= dFecCambioTnom - 1;
                     end;

                     { Calcular el inicio: Opciones: Ingreso o cambio de tipo de n�mina.
                       Cuando hay un reingreso hay un cambio de tipo de n�mina. }

                     if  ( lIngresoEnPerSCambio ) or ( lCalculandoNomCambio  ) or ( ( FechaIngreso > dInicial ) and
                                                                                    ( FechaIngreso < dFecCambioTNom ) )  then
                     begin
                          if ( FechaIngreso > dInicial ) then
                          begin
                               dInicial:= dMin( dFecCambioTnom, FechaIngreso );
                          end
                          else
                              dInicial:= dFecCambioTnom;
                     end;
                     SetDiasNoTrabajados;
              end;
              { Las fechas de asistencia solamente se modifican cuando hay un cambio de tipo de n�mina }
              if ( lCambioTNom ) then
              begin
                   InicioAsistencia := dInicial;
                   FinAsistencia := dFinal;
              end;
          end;
          {$else}
          with FStatus do
          begin
               { Baja a media semana: }
               { Descontar los d�as h�biles despues de la Baja }
               if ( not Activo and ( FechaBaja < dFinal ) ) then
               begin
                    if ( oZetaProvider.DatosPeriodo.Tipo in [ tpQuincenal, tpQuincenalA, tpQuincenalB ] ) then
                       dias_nt := dias_nt + Trunc( dFinal ) - Trunc( FechaBaja )
                    else
                        dias_nt := dias_nt + Ritmos.GetDiasHabiles( FDatosTurno, ( FechaBaja + 1 ), dFinal, FPrimerDia );
                    { EM/ER 17/Jun/2005 : En n�minas semanales se consideran no trabajados los d�as habiles posteriores a la baja,
                      en los dem�s tipos de n�mina se cuentan como no trabajados todos los posteriores a la baja
                      CM 21/Jun/2005 : Respecto a este cambio, y platicando con Veronica, sugiero que se quede solo para nominas "Quincenales".
                      Actualmente ya hay nominas catorcenales configuradas y no me gustaria que les presentara confusion el cambio.
                      Considerando que estos cambios actualmente son para sacar adelante la configuracion de quincenales, \
                      es mas facil controlarlos solo a ellos, y conforme vaya avanzando esto, y dedicando tiempo de analisis e investigacion
                      aplicar el criterio para catorcenal y mensual.
                    }
                    dFinal := FechaBaja;
               end;
               { Ingreso a media semana: }
               { Descontar los d�as h�biles antes de la Alta }
               if ( FechaIngreso > dInicial ) then
               begin
                    dias_nt := dias_nt + Ritmos.GetDiasHabiles( FDatosTurno, dInicial, ( FechaIngreso - 1 ), FPrimerDia );
                    dInicial := FechaIngreso;
               end;

          end;
          {$endif}

          { INCAPACIDADES }
          if GetDiasIncapacidades( dInicial, dFinal, dias_inca, dias_falta, dias_inca_subhabil, dias_inca_subdesc, dias_inca_sinsub ) then
          begin
               DiasIncapacidad := dias_inca;
               { Si estuvo incapacitado todo el per�odo, obliga a ceros }
               if ( dias_per > 7 ) and ( DiasIncapacidad >= dias_per ) then
                  dias_falta := DiasIncapacidad;
               DiasIncapSubsidioHabil := dias_inca_subhabil;
               DiasIncapSubsidioDescanso := dias_inca_subdesc;
               DiasIncapSinSubsidio := dias_inca_sinsub;
          end;

          { PERMISOS }
          if GetDiasPermisos( dInicial, dFinal, dias_cg, dias_sg, dias_fj, dias_su, dias_ot ) then
          begin
               DiasSuspension := dias_su;
               DiasOtroPermiso := dias_ot;
               DiasConGoce := dias_cg;
               DiasSinGoce := dias_sg;
               FaltasJustificadas := dias_fj;
               HorasConGoce := dias_cg * HrsPorDia;
               HorasSinGoce := dias_sg * HrsPorDia;
               dias_falta := dias_falta + FaltasJustificadas + DiasSuspension;
          end;

          { VACACIONES }
          if GetDiasVacaciones( dInicial, dFinal, dias_fv{$ifdef IMSS_VACA}, dias_vt {$endif}) then
          begin
               DiasFaltaVacacion :=  dias_fv;
               dias_nt := dias_nt + dias_fv;
               dias_nt_cal := dias_nt_cal + dias_fv;
               HorasFaltaVacacion := ( dias_fv * HrsPorDia );
{$ifdef IMSS_VACA}
               DiasVacaTotales := dias_vt;
{$endif}
          end;
          { Pago de VACACIONES }
          DiasVacaciones := GetPagoVacaciones( iEmpleado, DiasPrimaVaca );
{$ifdef IMSS_VACA}
          { D�as de pago de Vacaciones}
          DiasVacaPagadosCalendario := GetDiasPagoVacaciones( iEmpleado );
{$endif}
          { Pago de SUBSIDIO de Incapacidades }
          DiasSubsidioIncapacidad := GetSubsidioIncapacidad( iEmpleado );

          { Dias de Incapacidades anteriores }
          SetDiasIncapacidadAnterior( iEmpleado, dInicial, dFinal );

          { Horas y Dias }
          dias_ausent := dias_falta + dias_nt + dias_no_hay + DiasSinGoce;
          if ( dias_ausent > 0 ) then
          begin
               HrsFaltas := dias_ausent * HrsPorDia;
               Horas := ZetaCommonTools.rMax( Horas - HrsFaltas, 0 );
               HorasPrimaDominical := ZetaCommonTools.rMin( HorasPrimaDominical, Horas );
               DiasAsistencia := ZetaCommonTools.iMax( DiasTurno - dias_ausent + DiasSinGoce, 0 );
               DiasNoTrabajados := ZetaCommonTools.iMin( ( dias_nt_cal + dias_no_hay ), ( dias_per + dias_no_hay ) );
          end
          else if {$ifndef DEF_1628}( lCambioTNom ) and{$endif}( dias_nt_cal > 0 ) then   //dias_nt_cal son los dias calendario
          begin
               DiasNoTrabajados := ZetaCommonTools.iMin( ( dias_nt_cal + dias_no_hay ), ( dias_per + dias_no_hay ) );
          end;
          { Dias IMSS }
          CalculaDiasIMSS( dias_no_hay );

{$ifdef CAMBIO_TNOM}
          { Dias Calendario del periodo }
          DiasPeriodoCal:= Trunc( dFinal + 1 ) - Trunc( dInicial );
{$endif}
          DiasHabiles := Ritmos.GetDiasHabiles( FDatosTurno, dInicial, dFinal, FPrimerDia )
     end;
end;

procedure TNomina.LeeDiasHoras( const iEmpleado: TNumEmp );
begin
     with FNominaLee do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FNominaLee, 'Empleado', iEmpleado );
          end;
          Active := True;
          with FDiasHoras do
          begin
               if Eof then
               begin
                    Turno := '';
                    SetDiasHorasCeros;
               end
               else
               begin
                    Turno := FieldByName( 'CB_TURNO' ).AsString;
                    DiasTurno := FieldByName( 'NO_D_TURNO' ).AsInteger;
                    DiasAsistencia := FieldByName( 'NO_DIAS_AS' ).AsInteger;
                    DiasNoTrabajados := FieldByName( 'NO_DIAS_NT' ).AsInteger;
                    DiasFaltaVacacion := FieldByName( 'NO_DIAS_FV' ).AsInteger;
                    DiasIncapacidad := FieldByName( 'NO_DIAS_IN' ).AsInteger;
                    DiasIncapSubsidioHabil := FieldByName( 'NO_DIAS_IH' ).AsInteger;
                    DiasIncapSubsidioDescanso := FieldByName( 'NO_DIAS_ID' ).AsInteger;
                    DiasIncapSinSubsidio := FieldByName( 'NO_DIAS_IT' ).AsInteger;
                    DiasSinGoce := FieldByName( 'NO_DIAS_SG' ).AsInteger;
                    DiasConGoce := FieldByName( 'NO_DIAS_CG' ).AsInteger;
                    DiasAguinaldo := FieldByName( 'NO_DIAS_AG' ).AsFloat;
                    DiasAjuste := FieldByName( 'NO_DIAS_AJ' ).AsInteger;
                    DiasVacaciones := FieldByName( 'NO_DIAS_VA' ).AsFloat;
                    DiasPrimaVaca := FieldByName( 'NO_DIAS_PV' ).AsFloat;
                    DiasSubsidioIncapacidad := FieldByName( 'NO_DIAS_SI' ).AsFloat;
                    DiasOtroPermiso := FieldByName( 'NO_DIAS_OT' ).AsInteger;
                    DiasRetardo := FieldByName( 'NO_DIAS_RE' ).AsInteger;
                    DiasSuspension := FieldByName( 'NO_DIAS_SU' ).AsInteger;
                    {$ifdef IMSS_VACA}
                    DiasVacaTotales := FieldByName( 'NO_DIAS_VT' ).AsInteger;
                    DiasVacaPagadosCalendario := FieldByName( 'NO_DIAS_VC' ).AsInteger;
                    {$endif}
                    FaltasJustificadas := FieldByName( 'NO_DIAS_FJ' ).AsInteger;
                    FaltasInjustificadas := FieldByName( 'NO_DIAS_FI' ).AsInteger;
                    HorasJornada := FieldByName( 'NO_JORNADA' ).AsFloat;
                    Horas := FieldByName( 'NO_HORAS' ).AsFloat;
                    HorasConGoce := FieldByName( 'NO_HORA_CG' ).AsFloat;
                    HorasSinGoce := FieldByName( 'NO_HORA_SG' ).AsFloat;
                    HorasExtras := FieldByName( 'NO_EXTRAS' ).AsFloat;
                    HorasDobles := FieldByName( 'NO_DOBLES' ).AsFloat;
                    HorasTriples := FieldByName( 'NO_TRIPLES' ).AsFloat;
                    HorasTardes := FieldByName( 'NO_TARDES' ).AsFloat;
{$ifdef QUINCENALES}
                    HorasNoTrabajadas := FieldByName( 'NO_HORASNT' ).AsFloat;
                    InicioAsistencia := FieldByName( 'NO_ASI_INI' ).AsDateTime;
                    FinAsistencia := FieldByName( 'NO_ASI_FIN' ).AsDateTime;
{$endif}
                    HorasAdicionales := FieldByName( 'NO_ADICION' ).AsFloat;
                    HorasDescansoTrabajado := FieldByName( 'NO_DES_TRA' ).AsFloat;
                    HorasFestivoTrabajado := FieldByName( 'NO_FES_TRA' ).AsFloat;
                    HorasPrimaDominical := FieldByName( 'NO_HORA_PD' ).AsFloat;
                    HorasPrimaDominicalTotal := FieldByName( 'NO_HORAPDT' ).AsFloat;
                    HorasVacacionesTrabajadas := FieldByName( 'NO_VAC_TRA' ).AsFloat;
                    UsuarioReloj := FieldByName( 'NO_USER_RJ' ).AsInteger;
                    TipoLiquidacion := eLiqNomina( FieldByName( 'NO_LIQUIDA' ).AsInteger );
                    HorasExtPrepagadas:= FieldByName( 'NO_PRE_EXT' ).AsFloat;
                    DiasPeriodoCal:= FieldByName('NO_DIAS_PE').AsInteger;

                    DiasDescansoTrabajado := FieldByName('NO_DIAS_DT').AsInteger;
                    DiasFestivos := FieldByName('NO_DIAS_FS').AsInteger;
                    DiasFestivoTrabajado := FieldByName('NO_DIAS_FT').AsInteger;
               end;
          end;
          Active := False;
     end;
end;

procedure TNomina.GrabaDiasHorasBegin;
begin
     with oZetaProvider do
     begin
          FNominaDiasHoras := CreateQuery( GetSQLScript( Q_NOMINA_DIAS_HORAS ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FNominaDiasHoras, 'Year', Year );
               ParamAsInteger( FNominaDiasHoras, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FNominaDiasHoras, 'Numero', Numero );
          end;
     end;
end;

procedure TNomina.GrabaDiasHorasEnd;
begin
     FreeAndNil( FNominaDiasHoras );
end;

procedure TNomina.GrabaDiasHoras( const iEmpleado: TNumEmp );
var
   StatusSimulacion:eStatusSimFiniquitos;
   iDiasBase: TTasa;

{$ifdef QUINCENALES}
   function GetDiasBase: TTasa;
   begin
        with oZetaProvider.DatosPeriodo do
        begin
             case TipoDiasBase of
                  dbFijos : Result := DiasBase;
                  dbTurno : Result := FDatosJornada.DiasBase;
             else {dbPeriodo}
                  Result := Dias;
             end;
        end;
   end;
{$endif}
begin
     with oZetaProvider do
     begin
          if ( GetGlobalBooleano( K_GLOBAL_SIM_FINIQ_APROBACION ) )then
             StatusSimulacion := ssfSinAprobar
          else
              StatusSimulacion := ssfAprobada;

          ParamAsInteger( FNominaDiasHoras, 'Empleado', iEmpleado );
          with FDiasHoras do
          begin
               ParamAsInteger( FNominaDiasHoras, 'NO_D_TURNO', DiasTurno );
               ParamAsFloat( FNominaDiasHoras, 'NO_DIAS', DiasLaborados( Horas, HorasJornada, DatosPeriodo.Clasifi, FDatosJornada.TipoJornada ) );
               ParamAsFloat( FNominaDiasHoras, 'NO_DIAS_AG', DiasAguinaldo );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_AJ', DiasAjuste );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_AS', DiasAsistencia );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_NT', DiasNoTrabajados );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_FV', DiasFaltaVacacion );
               ParamAsFloat( FNominaDiasHoras, 'NO_DIAS_VJ', DiasLaborados( HorasFaltaVacacion, HorasJornada, DatosPeriodo.Clasifi, FDatosJornada.TipoJornada ) );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_IN', DiasIncapacidad );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_IH', DiasIncapSubsidioHabil );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_ID', DiasIncapSubsidioDescanso );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_IT', DiasIncapSinSubsidio );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_SG', DiasSinGoce );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_CG', DiasConGoce );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_SU', DiasSuspension );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_OT', DiasOtroPermiso );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_RE', DiasRetardo );
               ParamAsFloat( FNominaDiasHoras, 'NO_DIAS_VA', DiasVacaciones );
               ParamAsFloat( FNominaDiasHoras, 'NO_DIAS_PV', DiasPrimaVaca );
               ParamAsFloat( FNominaDiasHoras, 'NO_DIAS_SI', DiasSubsidioIncapacidad );
               ParamAsFloat( FNominaDiasHoras, 'NO_DIAS_SS', DiasSS );
               ParamAsFloat( FNominaDiasHoras, 'NO_DIAS_EM', DiasEM );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_FI', FaltasInjustificadas );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_FJ', FaltasJustificadas );
{$ifdef IMSS_VACA}
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_VT', DiasVacaTotales );
               ParamAsInteger( FNominaDiasHoras, 'NO_DIAS_VC', DiasVacaPagadosCalendario );
{$endif}
               ParamAsFloat( FNominaDiasHoras, 'NO_JORNADA', HorasJornada );
               ParamAsFloat( FNominaDiasHoras, 'NO_HORAS', Horas );
               ParamAsFloat( FNominaDiasHoras, 'NO_ADICION', HorasAdicionales );
               ParamAsFloat( FNominaDiasHoras, 'NO_HORA_CG', HorasConGoce );
               ParamAsFloat( FNominaDiasHoras, 'NO_HORA_SG', HorasSinGoce );
               ParamAsFloat( FNominaDiasHoras, 'NO_EXTRAS', HorasExtras );
               ParamAsFloat( FNominaDiasHoras, 'NO_DOBLES', HorasDobles );
               ParamAsFloat( FNominaDiasHoras, 'NO_TRIPLES', HorasTriples );
               ParamAsFloat( FNominaDiasHoras, 'NO_TARDES', HorasTardes );
{$ifdef QUINCENALES}
               ParamAsFloat( FNominaDiasHoras, 'NO_HORASNT', HorasNoTrabajadas );
               ParamAsDate( FNominaDiasHoras, 'NO_ASI_INI', InicioAsistencia );
               ParamAsDate( FNominaDiasHoras, 'NO_ASI_FIN', FinAsistencia );
               iDiasBase := GetDiasBase;
               ParamAsFloat( FNominaDiasHoras, 'NO_DIAS_BA', iDiasBase );
{$endif}
               ParamAsFloat( FNominaDiasHoras, 'NO_EXENTAS', HorasExentas );
               ParamAsFloat( FNominaDiasHoras, 'NO_HORA_PD', HorasPrimaDominical );
               ParamAsFloat( FNominaDiasHoras, 'NO_HORAPDT', HorasPrimaDominicalTotal );
               ParamAsFloat( FNominaDiasHoras, 'NO_VAC_TRA', HorasVacacionesTrabajadas );
               ParamAsFloat( FNominaDiasHoras, 'NO_FES_TRA', HorasFestivoTrabajado );
               ParamAsFloat( FNominaDiasHoras, 'NO_FES_PAG', HorasFestivoPagado );
               ParamAsFloat( FNominaDiasHoras, 'NO_DES_TRA', HorasDescansoTrabajado );
               ParamAsFloat( FNominaDiasHoras, 'NO_PRE_EXT', HorasExtPrePagadas );
               ParamAsInteger( FNominaDiasHoras, 'NO_STATUS', Ord( FDiasHoras.Status ) );
               ParamAsInteger( FNominaDiasHoras, 'NO_APROBA', Ord( StatusSimulacion ) );
{$ifdef CAMBIO_TNOM}
               ParamAsInteger( FNominaDiasHoras,'NO_DIAS_PE', DiasPeriodoCal );
{$endif}
               if ( iDiasBase > 0 ) and ( DiasHabiles > 0 ) then
                  ParamAsFloat( FNominaDiasHoras, 'NO_FACT_BA', iDiasBase / DiasHabiles )
               else
                   ParamAsFloat( FNominaDiasHoras, 'NO_FACT_BA', 0 );
               ParamAsInteger( FNominaDiasHoras,'NO_DIAS_DT', DiasDescansoTrabajado );
               ParamAsInteger( FNominaDiasHoras,'NO_DIAS_FS', DiasFestivos );
               ParamAsInteger( FNominaDiasHoras,'NO_DIAS_FT', DiasFestivoTrabajado );
//               ParamAsInteger( FNominaDiasHoras, 'US_CODIGO', UsuarioActivo );    No se Requiere Actualizar el Usuario
               ParamAsInteger( FNominaDiasHoras, 'NO_USER_RJ', UsuarioReloj );
          end;
          Ejecuta( FNominaDiasHoras );
     end;
end;

{$ifdef QUINCENALES}
function TNomina.HayFuncionDias: Boolean;
begin
     Result := ZetaCommonTools.strLleno( oZetaProvider.DatosPeriodo.FormulaDias );
end;

function TNomina.HayFuncionesTotales: Boolean;
begin
     Result := oZetaProvider.DatosPeriodo.HayFuncionesTotales;
end;

procedure TNomina.InitEvaluador;
begin
     FEvaluador := TNominaEvaluator.Create( FCreator );
end;

procedure TNomina.ClearEvaluador;
begin
     FreeAndNil( FEvaluador );
end;

procedure TNomina.SetNominaEvaluadorBegin;
begin
     if HayfuncionDias or HayFuncionesTotales then
     begin
          InitEvaluador;
          FEvaluador.CalcularBegin( oZetaProvider.DatosPeriodo );
     end;

     if HayfuncionDias then
     begin
          with oZetaProvider do
          begin
               FNominaDiasOrdinarios := CreateQuery( GetSQLScript( Q_NOMINA_DIAS_ORDINARIOS ) );
               with DatosPeriodo do
               begin
                    ParamAsInteger( FNominaDiasOrdinarios, 'Year', Year );
                    ParamAsInteger( FNominaDiasOrdinarios, 'Tipo', Ord( Tipo ) );
                    ParamAsInteger( FNominaDiasOrdinarios, 'Numero', Numero );
               end;
          end;
     end;
     if HayFuncionesTotales then
     begin
          with oZetaProvider do
          begin
               FNominaFormulasTotales := CreateQuery( GetSQLScript( Q_NOMINA_FORMULAS_TOTALES ) );
               with DatosPeriodo do
               begin
                    ParamAsInteger( FNominaFormulasTotales, 'Year', Year );
                    ParamAsInteger( FNominaFormulasTotales, 'Tipo', Ord( Tipo ) );
                    ParamAsInteger( FNominaFormulasTotales, 'Numero', Numero );
               end;
          end;
     end;
end;

procedure TNomina.SetNominaEvaluadorEnd;
begin
     if HayfuncionDias or HayFuncionesTotales then
     begin
          FEvaluador.CalcularEnd;
          ClearEvaluador;
     end;
     if HayfuncionDias then
        FreeAndNil(FNominaDiasOrdinarios);
     if HayFuncionesTotales then
        FreeAndNil(FNominaFormulasTotales);
end;

procedure TNomina.SetNominaEvaluador(const iEmpleado: TNumEmp);
var
   rDiasOrdinarios: TPesos;
begin
     if HayfuncionDias or HayFuncionesTotales then
     begin
          FEvaluador.Calcular( iEmpleado );
     end;
     if HayfuncionDias then
     begin
          rDiasOrdinarios := FEvaluador.ResultadosEval.DiasOrdinarios;
          with oZetaProvider do
          begin
               ParamAsInteger( FNominaDiasOrdinarios, 'Empleado', iEmpleado );
               ParamAsFloat( FNominaDiasOrdinarios, 'NO_DIAS', rDiasOrdinarios );
               Ejecuta( FNominaDiasOrdinarios );
          end;
     end;
     if HayFuncionesTotales then
     begin
          with oZetaProvider, FEvaluador.ResultadosEval do
          begin
               ParamAsInteger( FNominaFormulasTotales, 'Empleado', iEmpleado );
               ParamAsFloat( FNominaFormulasTotales, 'NO_TOTAL_1', TotalEvaluado1 );
               ParamAsFloat( FNominaFormulasTotales, 'NO_TOTAL_2', TotalEvaluado2 );
               ParamAsFloat( FNominaFormulasTotales, 'NO_TOTAL_3', TotalEvaluado3 );
               ParamAsFloat( FNominaFormulasTotales, 'NO_TOTAL_4', TotalEvaluado4 );
               ParamAsFloat( FNominaFormulasTotales, 'NO_TOTAL_5', TotalEvaluado5 );
               Ejecuta( FNominaFormulasTotales );
          end;
     end;
end;
{$endif}

{ ******* C�lculo de Pren�mina ***** }

procedure TNomina.PrenominaBegin;
begin
     InitRitmos;
     InitGlobales;
     GetPagoVacacionesBegin;
{$ifdef IMSS_VACA}
     GetDiasPagoVacacionesBegin;
{$endif}
     GetSubsidioIncapacidadBegin;
     SetDiasIncapacidadAnteriorBegin;
     LeeDiasHorasBegin;
     GetDatosEmpleadoBegin;
     CalculaTotalesBegin;
     GrabaDiasHorasBegin;
{$ifdef QUINCENALES}
     SetNominaEvaluadorBegin;
{$endif}
end;
                                         
procedure TNomina.PrenominaEnd;
begin
{$ifdef QUINCENALES}
     SetNominaEvaluadorEnd;
{$endif}
     GrabaDiasHorasEnd;
     CalculaTotalesEnd;
     LeeDiasHorasEnd;
     GetDatosEmpleadoEnd;
     GetSubsidioIncapacidadEnd;
     SetDiasIncapacidadAnteriorEnd;
     GetPagoVacacionesEnd;
{$ifdef IMSS_VACA}
     GetDiasPagoVacacionesEnd;
{$endif}
end;

procedure TNomina.RefrescaPrenominaBegin;
begin
     PrenominaBegin;
     with oZetaProvider do
     begin
          FEmpleadoDatos := CreateQuery( GetSQLScript( Q_EMPLEADO_CHECA ) );
     end;
     GetDatosJornadaBegin;
end;

procedure TNomina.RefrescaPrenominaEnd;
begin
     GetDatosJornadaEnd;
     FreeAndNil( FEmpleadoDatos );
     PrenominaEnd;
end;

procedure TNomina.RefrescaPrenomina(const iEmpleado: TNumEmp);
var
   dBaja, dIngreso: TDate;
begin
     with oZetaProvider do
     begin
          InitArregloTPeriodo;
          with FEmpleadoDatos do
          begin
               Active := False;
          end;
          ParamAsInteger( FEmpleadoDatos, 'Empleado', iEmpleado );
          EmpiezaTransaccion;
          try
             with FEmpleadoDatos do
             begin
                  Active := True;

                  dBaja:= FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
                  dIngreso:= FieldByName( 'CB_FEC_ING' ).AsDateTime;


                  { Si ha tenido una baja y la fecha de ingreso a la fecha de asistencia del periodo investiga fecha
                     en base a Kardex }
                   if  ( dBaja > DatosPeriodo.Inicio ) and ( dIngreso > DatosPeriodo.FinAsis ) then
                   begin
                        with Queries do
                        begin
                             GetFechaAltaBegin;
                             try
                                dIngreso:= GetFechaAlta( DatosPeriodo.FinAsis, iEmpleado );
                             finally
                                    GetFechaAltaEnd;
                             end;
                        end;
                   end;


                  CalculaPrenomina( iEmpleado, ZetaCommonTools.zStrToBool( FieldByName( 'CB_CHECA' ).AsString )
{$ifdef QUINCENALES}
                                    , dIngreso, dBaja
{$endif}
                                   );
                  Active := False;
             end;
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     //TerminaTransaccion( False );
                     RollBackTransaccion;
                     raise;
                end;
          end;
          if( DatosPeriodo.Status > spCalculadaParcial )then
          begin
               CalculaStatusPeriodo;
          end;
     end;
end;

{ ******* C�lculo de Pren�mina ***** }

procedure TNomina.CalculaPrenominaBegin;
begin
     CambiaStatusPeriodo( spPrenomina );
     PrenominaBegin;
     IncluyeNominaBegin;
     oZetaProvider.InitArregloTPeriodo;//acl     
end;

procedure TNomina.CalculaPrenominaEnd;
begin
     IncluyeNominaEnd;
     PrenominaEnd;
     CalculaStatusPeriodo;
end;

{$ifdef QUINCENALES}
procedure TNomina.CalculaPrenomina(const iEmpleado: TNumEmp; const lCheca: Boolean; const dIngreso, dBaja: TDate );
{$else}
procedure TNomina.CalculaPrenomina(const iEmpleado: TNumEmp; const lCheca: Boolean);
{$endif}
begin
     LeeDiasHoras( iEmpleado );
     InitDatosTurnoJornada;
{$ifdef QUINCENALES}
     CalculaTotales( iEmpleado, lCheca, dIngreso, dBaja );
{$else}
     CalculaTotales( iEmpleado, lCheca );
{$endif}
     CalculaDiasHoras( iEmpleado );
     GrabaDiasHoras( iEmpleado );
{$ifdef QUINCENALES}
     SetNominaEvaluador( iEmpleado );
{$endif}
end;

{ ***** C�lculo de Totales de Asistencia ********** }

procedure TNomina.CalculaTotalesBegin;
begin
     with oZetaProvider do
     begin
          FLeeDiasExtras := CreateQuery( GetSQLScript( Q_PRENOMINA_HORAS_EXTRAS ) );
          FUpdateAusenciaFSS := CreateQuery( Format( GetSQLScript( Q_TARJETAS_UPDATE_FSS ), [ EntreComillas( K_I_FALTA_SS ), Ord( daNormal ) ] ) );
          FRegla3x3 := eRegla3x3( GetGlobalInteger( K_GLOBAL_HRS_EXENTAS_IMSS ) );
          FDiasAplicaExento := eDiasAplicaExento( GetGlobalInteger( K_GLOBAL_DIAS_APLICA_EXENTO) );
          FAplicaBloqueoPreNomina := GetGlobalBooleano(K_GLOBAL_BLOQUEO_PRENOM_AUT );
          {$ifdef PRIMA_DOMINICAL}
          FPrimaDominicalTotal := GetGlobalBooleano(K_GLOBAL_PRIMA_DOMINICAL_TIPO_DIA);
          FTarjetasLee := CreateQuery( GetSQLScript( Q_TARJETAS_HORARIO_LEE ) );
          FPrimaDominicalCalc := TPrimaDominical.Create(oZetaProvider);
          {$else}
          if GetGlobalBooleano( K_GLOBAL_SUMAR_JORNADAS ) then
             FTarjetasLee := CreateQuery( GetSQLScript( Q_TARJETAS_HORARIO_LEE ) )
          else
              FTarjetasLee := CreateQuery( GetSQLScript( Q_TARJETAS_LEE ) );
          {$endif}
          FNoChecaUsarJornadas := GetGlobalBooleano( K_GLOBAL_NO_CHECA_USAR_JORNADAS );
          FUpdateGlobalAutPrenom := CreateQuery( Format( GetSQLScript( Q_GLOBAL_UPDATE_BLOQUEO_PRENOM ), [ K_GLOBAL_BLOQUEO_PRENOM_AUT ] ) );
          FIncidenciaIncapGeneral := strLeft( Trim( GetGlobalString( K_GLOBAL_INCAPACIDAD_GENERAL ) ), K_ANCHO_CODIGO3);
          FLeeFechaIncapGeneral := CreateQuery( Format( GetSQLScript( Q_INCAP_GENERAL_FECHA ), [ ZetaCommonTools.EntreComillas( FIncidenciaIncapGeneral ), Ord( miInicial ) ] ) );
     end;
     FListaDescansos := TStringList.Create;
     InitRitmos;
     with Ritmos do
     begin
          GetDatosTurnoBegin;
          GetDatosJornadaBegin;
          {$ifdef ANTES}
          GetEmpleadoDatosTurnoBegin;  //Ya lo hace ahora con la llamada a Preparar
          {$else}
          //  Se ocupa que prepare el primer d�a de la semana
          Preparar;
          {$endif}
     end;
{$ifndef ANTES}
     Queries.GetDatosHorarioDiarioBegin;
{$endif}
     CalculaDiasHorasBegin;
end;

procedure TNomina.CalculaTotalesEnd;
begin
     CalculaDiasHorasEnd;
{$ifndef ANTES}
     Queries.GetDatosHorarioDiarioEnd;
{$endif}
     with Ritmos do
     begin
          GetEmpleadoDatosTurnoEnd;
          GetDatosTurnoEnd;
          GetDatosJornadaEnd;
     end;
     FreeAndNil( FListaDescansos );
     FreeAndNil( FPrimaDominicalCalc);
     FreeAndNil( FTarjetasLee );
     FreeAndNil( FLeeDiasExtras );
     FreeAndNil( FUpdateAusenciaFSS );
     FreeAndNil( FUpdateGlobalAutPrenom );
     FreeAndNil( FLeeFechaIncapGeneral );
end;

{$ifdef QUINCENALES}
procedure TNomina.CalculaTotales(const iEmpleado: TNumEmp; const lCheca: Boolean; const dIngreso, dBaja: TDate );
{$else}
procedure TNomina.CalculaTotales(const iEmpleado: TNumEmp; const lCheca: Boolean );
{$endif}
const
     K_EXENTAS_DIARIO = 3;
     K_EXENTAS_SEMANAL = 3;
var
   tot_regular, tot_extras, tot_dobles, tot_tardes, tot_des_tra, tot_fes_tra: TDiasHoras;
   tot_vac_tra, tot_per_cg, tot_per_sg, tot_domingo,tot_dom_total, tot_fes_pag, tot_jornada, tot_ext_prep: TDiasHoras;
   h_ordinarias, h_extras, h_dobles, h_triples, h_per_cg, h_per_sg, h_des_tra, h_fes_tra, h_vac_tra, h_exentas: TDiasHoras;
   h_dobles_3x3, h_triples_3x3, h_horas_vj, h_ext_prep: TDiasHoras;
   faltas_in, atrasos: Word;
   permiso_cg, permiso_sg, permiso_fj, permiso_su, permiso_ot: Word;
   dias_inca, dias_nt, dias_fv, dias_asis, dias_falta, dias_no_hay, dias_extras: Word;
   dias_inca_subhabil, dias_inca_subdesc, dias_inca_sinsub: Word;
   dias_vaca, dias_primavaca: TDiasHoras; { GJ Actualizado para que sea compatible con GetPagoVaciones }
{$ifdef IMSS_VACA}
   dias_vt, dias_vcal: Word;
{$endif}
   dias_habiles, dias_festivos, d_fes_tra, d_des_tra: Word;
   rCargo: TDiasHoras;
   dCorteExtras, dAusencia: TDate;
   lConHorario, lSumaHorarios, lHayManual, lEsManual: Boolean;
   iCodigo: TNumEmp;
   TipoDia: eTipoDiaAusencia;
   StatusAusencia: eStatusAusencia;
   i : Integer;
   dinicio, dFinal: TDate;
{$ifdef QUINCENALES}
   tot_HorasNT: TDiasHoras;
{$endif}
{$ifdef CAMBIO_TNOM}
   FStatus: TStatusEmpleado;
   FNomina: TNominaEmpleado;
   lCambioTNom, lRevTurnoReingreso: Boolean;
   dFecha: TDate;
   sHorario, sIncidencia: String;
   oDatosTurno, oDatosTReingreso: TEmpleadoDatosTurno;

   function GetJornadaEmpleado( const oTurno: TEmpleadoDatosTurno; const dFechaTemp: TDate ): TDiasHoras;
   begin
        with Ritmos.GetStatusHorario( oTurno.Codigo, dFechaTemp, False ) do
        begin
             sHorario := Horario;
             StatusAusencia := Status;
             Result:= 0;
             if ( Status = auHabil ) then
             begin
                  Result:= Queries.GetDatosHorarioDiario(sHorario).Jornada;
             end;
        end;

   end;
{$endif}
   Procedure RevisarBloqueoPrenomina(const Encender:Boolean);
   begin
        if FAplicaBloqueoPreNomina then
        begin
             with oZetaProvider do
             begin
                  ParamAsString( FUpdateGlobalAutPrenom , 'Formula', zBoolToStr( Encender ) );
                  Ejecuta( FUpdateGlobalAutPrenom );
             end;
        end;
   end;

   function TieneSubsidioIncapGeneral: Boolean;
   var
      iDias: Integer;
   begin
        with oZetaProvider do
        begin
             FLeeFechaIncapGeneral.Active := FALSE;
             ParamAsInteger( FLeeFechaIncapGeneral, 'Empleado', iEmpleado );
             ParamAsDate( FLeeFechaIncapGeneral, 'Fecha', dAusencia );
             ParamAsDate( FLeeFechaIncapGeneral, 'Fecha2', dAusencia );
             with FLeeFechaIncapGeneral do
             begin
                  Active := TRUE;
                  Result := ( not EOF );
                  if Result then
                  begin
                       iDias := Round( dAusencia - FieldByName( 'IN_FEC_INI' ).AsDateTime ) + 1;
                       Result := ( iDias > 0 ) and ( iDias <= K_DIAS_SUBSIDIO_INCAP );
                  end;
                  Active := FALSE;
             end;
        end;
   end;

begin
     tot_regular := 0;
     tot_extras := 0;
     tot_dobles := 0;
     tot_tardes := 0;
{$ifdef QUINCENALES}
     tot_HorasNT := 0;
{$endif}
     tot_des_tra := 0;
     tot_fes_tra := 0;
     tot_vac_tra := 0;
     tot_per_cg := 0;
     tot_per_sg := 0;
     tot_domingo := 0;
     tot_dom_total := 0;
     tot_fes_pag := 0;
     tot_ext_prep:= 0;
     faltas_in := 0;
     atrasos := 0;
     permiso_cg := 0;
     permiso_sg := 0;
     permiso_fj := 0;
     permiso_su := 0;
     permiso_ot := 0;
     dias_inca := 0;
     dias_inca_subhabil := 0;
     dias_inca_subdesc := 0;
     dias_inca_sinsub := 0;
     dias_nt := 0;
     dias_fv := 0;
     h_horas_vj := 0;
     dias_asis := 0;
     dias_falta := 0;
     dias_extras := 0;
     dias_vaca := GetPagoVacaciones( iEmpleado, dias_primavaca );
{$ifdef IMSS_VACA}
     dias_vt := 0;   
     dias_vcal := GetDiasPagoVacaciones( iEmpleado );
{$endif}
     dias_habiles := 0;
     dias_festivos := 0;
     d_fes_tra := 0;
     d_des_tra := 0;
     h_exentas := 0;
     tot_jornada := 0;
     lHayManual	:= False;
     oDatosTReingreso.Codigo:= VACIO;

     with oZetaProvider do
     begin
          with FTarjetasLee do
          begin
               Active := False;
          end;

          with DatosPeriodo do
          begin
{$ifdef QUINCENALES}
               //dinicio := InicioAsis;
               ZetaCommonTools.EsBajaenPeriodo( dIngreso, dBaja, DatosPeriodo, dinicio, dFinal );

               lSumaHorarios := ( Clasifi in [ tpSemanal, tpCatorcenal, tpQuincenal ] );
               if lCheca then
                  lSumaHorarios := lSumaHorarios and DatosPeriodo.SumarJornadaHorarios
               else
                  lSumaHorarios := lSumaHorarios and FNoChecaUsarJornadas; { Global }

{$ifdef CAMBIO_TNOM}
               { Se manda FStatus por compatibilidad}
               GetDatosEmpleado( iEmpleado, FStatus, FNomina, FALSE );

               with FNomina do
               begin
                    lCambioTNom:= EsCambioTNomPeriodo( FechaCambioTNom, oZetaProvider.DatosPeriodo ) and ( NominaFin <> Nomina );


                    if ( lCambioTNom ) then
                    begin

                         { Si se esta calculando la n�mina a la que cambio en el inicio se hace el corte }
                         if ( FNomina.NominaFin = oZetaProvider.DatosPeriodo.Tipo ) then
                         begin
                              dInicio:= dMax( FechaCambioTNom, dInicio );
                         end
                         else
                             { Si se esta calculando la n�mina con el cambio, al final se hace el corte }
                             dFinal:= dMin( FechaCambioTNom - 1, dFinal );

                         { Solamente cuando existan tarjetas IGR se revisa el turno en base al reingreso, para no irnos al status del empleado
                           se realiza esta condici�n }
                         lRevTurnoReingreso:= ( dBaja <> NullDateTime ) and ( dIngreso > ( dBaja + 1 ) );

                         { Los dias no trabajados son igual a la diferencia entre el inicio y el cambio de tipo de n�mina o entre el fin o el cambio }
                         if ( dFinal < FinAsis ) then
                         begin
                              dias_nt:= Trunc( FinAsis ) - Trunc( dFinal );

                              { Determina los d�as de jornada en el corte al final de la n�mina }
                              if ( lSumaHorarios ) then
                              begin
                                   dFecha:= dFinal + 1;
                                   oDatosTurno:= Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFecha );
                                   repeat
                                         tot_jornada:= tot_jornada + GetJornadaEmpleado( oDatosTurno, dFecha );
                                         dFecha:= dFecha + 1;
                                   until ( dFecha > FinAsis );
                              end;
                         end
                         else if ( dInicio > InicioAsis ) then
                         begin
                              dias_nt:= Trunc( dInicio ) - Trunc( InicioAsis );
                              { Determina los d�as de jornada en el corte al principio de la n�mina }
                              if ( lSumaHorarios ) then
                              begin
                                   dFecha:= InicioAsis;
                                   oDatosTurno:= Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFecha );

                                   repeat
                                         if ( lRevTurnoReingreso ) and ( dFecha < dIngreso ) then
                                         begin
                                              if ( StrVacio( oDatosTReingreso.Codigo ) ) then
                                              begin
                                                   oDatosTReingreso:= Ritmos.GetEmpleadoDatosTurno( iEmpleado, dIngreso );
                                              end;
                                              tot_jornada:= tot_jornada + GetJornadaEmpleado( oDatosTReingreso,
                                                                                              dFecha );
                                         end
                                         else
                                             tot_jornada:= tot_jornada + GetJornadaEmpleado( oDatosTurno, dFecha );

                                         dFecha:= dFecha + 1;
                                   until ( dFecha >= dInicio );
                              end;
                         end;

                    end;

               end;

{$endif}


{$else}

               dInicio := Inicio;
               dFinal := Fin;

               lSumaHorarios := lCheca and FSumaHorarios and ( Clasifi in [ tpSemanal, tpCatorcenal, tpQuincenal ] ); { function SumaHorarios: Boolean Es un Registro de GLOBAL }
{$endif}
               dCorteExtras := dInicio;


               if ( Trunc( dFinal - dInicio ) > 6 ) or ( lCambioTNom ) then { Fecha de corte para N�minas mayores a una Semana �No es mejor usar Periodo.Tipo ? }
               begin
                    { Nos vamos a la fecha de corte de la semana anterior }
                    { Contamos cuantos d�as de horas extras hubo en esa parte }
{$ifdef QUINCENALES}
                    dCorteExtras := ZetaCommonTools.LastCorte( dCorteExtras, FDOWCorte );
{$else}
                    while ( DayOfWeek( dCorteExtras ) <> FDOWCorte ) do
                    begin
                         dCorteExtras := ( dCorteExtras - 1 );
                    end;
{$endif}
                    with FLeeDiasExtras do
                    begin
                         Active := False;
                         ParamAsInteger( FLeeDiasExtras, 'Empleado', iEmpleado );
                         ParamAsDate( FLeeDiasExtras, 'Fecha', dCorteExtras );
                         ParamAsDate( FLeeDiasExtras, 'FechaInicial', dInicio );
                         Active := True;
                         dias_extras := Fields[ 0 ].AsInteger;
                         Active := False;
                    end;
                    dCorteExtras := ( dCorteExtras + 7 );
               end
               else
                   dCorteExtras := ( dFinal + 1 );

               ParamAsInteger( FTarjetasLee, 'Empleado', iEmpleado );
               ParamAsDate( FTarjetasLee, 'FechaInicial', dInicio );
               ParamAsDate( FTarjetasLee, 'FechaFinal', dFinal );
          end;
          lConHorario := ( FDatosJornada.TipoTurno = ttNormal );
          FListaDescansos.Clear;
          with FTarjetasLee do
          begin
               Active := True;
               while not Eof do
               begin
                    dAusencia := FieldByName( 'AU_FECHA' ).AsDateTime;
                    TipoDia := eTipoDiaAusencia( FieldByName( 'AU_TIPODIA' ).AsInteger );
                    StatusAusencia := eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger );
                    sIncidencia:= FieldByName('AU_TIPO').AsString;

                    if ( StatusAusencia <> auHabil ) then                                // Se suman los dias de descanso: auSabado y auDescanso
                       FListaDescansos.Add( IntToStr( Trunc( dAusencia ) ) )  // Se guarda la fecha como numero entero
                    else
                        dias_habiles := dias_habiles + 1;                     // Se almacenan los dias habiles independientemente de las incidencias
                    h_ordinarias := FieldByName( 'AU_HORAS' ).AsFloat;
                    h_extras := FieldByName( 'AU_EXTRAS' ).AsFloat;
                    h_dobles := FieldByName( 'AU_DOBLES' ).AsFloat;
                    h_per_cg := FieldByName( 'AU_PER_CG' ).AsFloat;
                    h_per_sg := FieldByName( 'AU_PER_SG' ).AsFloat;
                    h_des_tra := 0;  { Inicializar SIEMPRE en cero }
                    h_fes_tra := 0;
                    h_vac_tra := 0;

                     { Horas Extras Prepagadas }
                    h_ext_prep:= FieldByName( 'AU_PRE_EXT' ).AsFloat;

                    lEsManual := ( FieldByName( 'US_CODIGO' ).AsInteger > 0 );
                    lHayManual := lHayManual or lEsManual;  { Al menos una tarjeta diaria modificada por el usuario }
                    rCargo := FieldByName( 'AU_TARDES' ).AsFloat;
                    if ( rCargo > 0 ) then { DiasRetardo }
                    begin
                         atrasos := atrasos + 1;
                         tot_tardes := tot_tardes + rCargo;
                    end;
                    rCargo := FieldByName( 'AU_DES_TRA' ).AsFloat;
                    if ( rCargo > 0 ) then { Horas de Descanso Trabajado }
                    begin
                         case TipoDia of
                              daFestivo:
                              begin
                                   h_fes_tra := rCargo;
                                   d_fes_tra := d_fes_tra + 1;
                              end;

                              // US #13484: Error con global de �Calcular d�as cotizados sin faltas x vac�
                              // cuando est� activado cambia �descanso trabajado� a tiempo extra las horas de ese d�a.

                              // Se hace cambio para contar las horas de descanso trabajado en d�as con incidencia de vacaciones
                              // si el global "Calcular d�as cotizados sin faltas x vacaciones" est� activo.
                              // Esto para hacer consistente los datos de la tarjeta con el c�lculo de n�mina.
                              daVacaciones:
                              begin
                                   // Correcci�n de bug #13575: La vacaci�n trabajada la est� considerando
                                   // como Descanso trabajado cuando el global est� prendido.
                                   // Agregar como condici�n que el d�a sea distinto a un d�a h�bil.
                                   // Para un d�a h�bil se deben sumar las horas como Vacaci�n trabajada.
                                   if (FDiasCotizaSinFV) and (StatusAusencia <> auHabil) then
                                   begin
                                        h_des_tra := rCargo;
                                        d_des_tra := d_des_tra + 1;
                                   end
                                   else
                                       h_vac_tra := rCargo;
                              end;

                         else
                             begin
                                  h_des_tra := rCargo;
                                  d_des_tra := d_des_tra + 1;
                             end;
                         end;
                    end;
                    if ( TipoDia = daFestivo ) then { Festivo Pagado }
                       tot_fes_pag := tot_fes_pag + h_ordinarias;
                    tot_per_cg := tot_per_cg + h_per_cg;	{ Permisos con goce y sin goce }
                    tot_per_sg := tot_per_sg + h_per_sg;
{$ifdef QUINCENALES}
                    tot_HorasNT := tot_HorasNT + FieldByName( 'AU_HORASNT' ).AsFloat;   // Acumular Horas no Trabajadas
{$endif}
                    case TipoDia of
                         daSinGoce: permiso_sg := permiso_sg + 1;
                         daConGoce: permiso_cg := permiso_cg + 1;
                         daFestivo: dias_festivos := dias_festivos + 1;
                    end;
                    case TipoDia of { Calculo de dias }
                         daNoTrabajado: dias_nt := dias_nt + 1;
                         daVacaciones:
                         begin
{$ifdef IMSS_VACA}
                              dias_vt := dias_vt + 1;
                              if ( not FDiasCotizaSinFV ) or ( StatusAusencia = auHabil ) then
                              begin
{$endif}
                                   dias_nt := dias_nt + 1;
                                   dias_fv := dias_fv + 1;
                                   h_horas_vj := h_horas_vj + FieldByName( 'HO_JORNADA' ).AsFloat;   // Acumula las horas de jornada en faltas por vacaciones
{$ifdef IMSS_VACA}
                              end;
{$endif}
                         end;
                         daIncapacidad:
                         begin
                              dias_inca := dias_inca + 1;
                              if ( StatusAusencia = auHabil ) then
                                 dias_falta := dias_falta + 1;
                              if ( FIncidenciaIncapGeneral = sIncidencia ) then    // Es incapacidad general
                              begin
                                   if TieneSubsidioIncapGeneral then
                                   begin
                                        if ( StatusAusencia = auHabil ) then
                                           dias_inca_subhabil := dias_inca_subhabil + 1
                                        else
                                            dias_inca_subdesc := dias_inca_subdesc + 1;
                                   end
                                   else
                                       dias_inca_sinsub := dias_inca_sinsub + 1;
                              end;
                         end;
                         daJustificada:
                         begin
                              permiso_fj := permiso_fj + 1;
                              if ( StatusAusencia = auHabil ) then
                                 dias_falta := dias_falta + 1;
                         end;
                         daSuspension:
                         begin
                              permiso_su := permiso_su + 1;
                              if ( StatusAusencia = auHabil ) then
                                 dias_falta := dias_falta + 1;
                         end;
                         daOtroPermiso:
                         begin
                              permiso_ot := permiso_ot + 1;
                              if ( StatusAusencia = auHabil ) then
                                 dias_falta := dias_falta + 1;
                         end;
                         else
                         begin                         
                              if lCheca or lEsManual or ( oZetaProvider.DatosPeriodo.Clasifi <> tpDecenal ) then { Si CHECA, o se modifico la tarjeta o periodo Decenal }
                              begin
                                   if lConHorario then
                                   begin
                                        if ( ( h_ordinarias + h_per_sg + h_extras ) > 0 ) then
                                           dias_asis := dias_asis + 1
                                        else
                                            if ( TipoDia = daNormal ) and ( StatusAusencia = auHabil ) then
                                               faltas_in := faltas_in + 1;
                                   end
                                   else { Sin Horario }
                                        if ( ( h_ordinarias + h_per_sg + h_extras + h_fes_tra + h_des_tra ) > 0 ) then
                                           dias_asis := dias_asis + 1;
                              end
                              else { NO CHECA }
                              begin
                                   if ( TipoDia = daNormal ) and ( StatusAusencia = auHabil ) then
                                      if ( ( h_ordinarias + h_per_sg + h_extras ) = 0 ) then
                                      begin
                                           dias_falta := dias_falta + 1;
                                           faltas_in := faltas_in + 1;
                                      end
                                      else
                                          dias_asis := dias_asis + 1;
                              end;
                         end;
                    end; { case TipoDia }
                    {$ifdef PRIMA_DOMINICAL}
                    if ( StatusAusencia = auHabil ) then
                    begin
                         with FPrimaDominicalCalc.CalcularPrimaDominical( iEmpleado, dAusencia, eTipoHorario( FieldByName( 'HO_TIPO' ).AsInteger ), h_ordinarias, h_ordinarias + h_fes_tra + h_extras ) do
                         begin
                              tot_domingo := tot_domingo + Ordinarias;
                              tot_dom_total := tot_dom_total + Totales;
                         end;
                    end
                    else if FPrimaDominicalTotal then
                    begin
                         with FPrimaDominicalCalc.CalcularPrimaDominical( iEmpleado, dAusencia, eTipoHorario( FieldByName( 'HO_TIPO' ).AsInteger ), h_des_tra, h_ordinarias + h_extras + h_des_tra ) do
                         begin
                              //Cuando el dia NO es habil, la prima dominical no se calcula
                              //Solo se va a calcular la prima dominical total.
                              tot_dom_total := tot_dom_total + Totales;
                         end;
                    end;
                    {$else}
                    if ( StatusAusencia = auHabil ) and ( DayOfWeek( FieldByName( 'AU_FECHA' ).AsDateTime ) = 1 ) then { Prima Dominical, solo si el dia es domingo }
                       tot_domingo := tot_domingo + h_ordinarias;
                    {$endif}
                    tot_regular := tot_regular + h_ordinarias; { Totales }
                    tot_extras  := tot_extras + h_extras;
                    tot_dobles  := tot_dobles + h_dobles;
                    tot_des_tra := tot_des_tra + h_des_tra;
                    tot_fes_tra := tot_fes_tra + h_fes_tra;
                    tot_vac_tra := tot_vac_tra + h_vac_tra;
                    tot_ext_prep := tot_ext_prep + h_ext_prep;
                    if lSumaHorarios and ( StatusAusencia = auHabil ) then  { Opcional: Sumar jornadas de cada dia }
{$ifdef ANTES}
                       tot_jornada := tot_jornada + FieldByName( 'HO_JORNADA' ).AsFloat;
{$else}
                    begin
                         if ( not lEsManual ) and ( TipoDia = daFestivo ) and lConHorario and ( FDatosJornada.TieneHorarioFestivo ) then
                            tot_jornada := tot_jornada + Queries.GetDatosHorarioDiario( Ritmos.GetStatusHorario( FDatosJornada.Codigo, dAusencia, False ).Horario ).Jornada
                         { En tarjetas ingreso toma el turno del reingreso para el sumahorarios, el ingreso debe ser en el periodo para que esto aplique }
                         else if ( sIncidencia = K_I_INGRESO ) and ( dBaja <> NullDateTime ) and ( dIngreso < DatosPeriodo.Fin ) then
                         begin
                              if ( StrVacio( oDatosTReingreso.Codigo ) ) then
                              begin
                                   oDatosTReingreso:= Ritmos.GetEmpleadoDatosTurno( iEmpleado, dIngreso );
                              end;
                              tot_jornada:=  tot_jornada + GetJornadaEmpleado( oDatosTReingreso, dAusencia )
                         end
                         else
                            tot_jornada := tot_jornada + FieldByName( 'HO_JORNADA' ).AsFloat;
                    end;
{$endif}
                    if ( dAusencia >= dCorteExtras ) then  { Semana nueva, tiene otros 3 d�as frescos }
                    begin
                         dCorteExtras := ( dCorteExtras + 7 );
                         dias_extras := 0;
                    end;
                    if ( AplicaRegla3X3( FDiasAplicaExento,StatusAusencia,TipoDia ) )then
                    begin
                         if ( h_extras > 0 ) then
                         begin
                              {$ifdef ANTES}
                              dias_extras := ( dias_extras + 1 );
                              if ( dias_extras <= K_EXENTAS_SEMANAL ) then
                                 h_exentas := h_exentas + rMin( h_extras, K_EXENTAS_DIARIO );
                              {$else}
                              {cv: 26-nov-2001
                              Esta misma logica esta repetida en
                              DNominaClass.PAS  TNomina.CalculaTotales
                              ZFuncsImss.CalculaRegla3x3
                              ZFuncsImss.CalculaHorasExentas
                              si cambia algo en este algoritmo hay que
                              verificarlo en los otros 2 lugares}
                              case FRegla3x3 of
                                   r3x3_Proporcional:
                                   begin
                                        { Cada horas doble vale 1, Cada hora triples vale 1.5  ( 3 / 2 = 1.5 ) }
                                        { Esto facilita multiplicaci�n: NO_EXENTAS * SAL_HORA * 2 }
                                        dias_extras := ( dias_extras + 1 );
                                        if ( dias_extras <= K_EXENTAS_SEMANAL ) then
                                        begin
                                             h_triples := h_extras - h_dobles;
                                             h_dobles_3x3 := ZetaCommonTools.rMin( h_dobles, K_EXENTAS_DIARIO );
                                             h_triples_3x3 := ZetaCommonTools.rMin( h_triples, ( K_EXENTAS_DIARIO - h_dobles_3x3 ) );
                                             h_exentas := h_exentas + h_dobles_3x3 + h_triples_3x3 * 1.5;
                                        end;
                                   end;
                                   r3x3_Cuantas:
                                   begin
                                        { Considera dobles y triples. Tanto dobles como triples valen 1 }
                                        dias_extras := ( dias_extras + 1 );
                                        if ( dias_extras <= K_EXENTAS_SEMANAL ) then
                                           h_exentas := h_exentas + ZetaCommonTools.rMin( h_extras, K_EXENTAS_DIARIO );
                                   end;
                                   else { SoloDobles }
                                   begin
                                        if ( h_dobles > 0 ) then { Solo considera horas dobles como exentas }
                                        begin
                                             dias_extras := ( dias_extras + 1 );
                                             if ( dias_extras <= K_EXENTAS_SEMANAL ) then
                                                h_exentas := h_exentas + ZetaCommonTools.rMin( h_dobles, K_EXENTAS_DIARIO );
                                        end;
                                   end;
                              end;
                              {$endif}
                         end;
                    end;

                    Next;
               end;
               Active := False;
          end; { with AUSENCIA }
          if not lSumaHorarios then
             tot_jornada := FDatosJornada.Jornada;
          if lConHorario and ( lCheca or lHayManual ) then { Topa asistencia a Dias del Turno }
             dias_asis := iMin( dias_asis, FDatosJornada.Dias );
          dias_no_hay := Ritmos.GetDiasNoHay( DatosPeriodo, FDatosJornada, FDatosTurno, FPrimerDia );
          { GJ: Se Actualiz� la llamada a GetDiasNohay antes no llevaba par�metros }
          if not lCheca and not lHayManual and not FNoChecaUsarJornadas and ( DatosPeriodo.Clasifi <> tpDecenal  ) then { CUANDO SON EMPLEADOS QUE NO CHECAN EN NOMINAS SEMANALES/QUINCENALES }
          begin
               if ( dias_inca >= DatosPeriodo.Dias ) then { Estuvo incapacitado todos los dias habiles }
               begin
                    tot_regular := 0;
                    dias_asis := 0;
                    tot_domingo := 0;
               end
               else
               begin
                    with FDatosJornada do
                    begin
                         if ( Dias = 0 ) then
                            tot_regular := 0
                         else
                             tot_regular := ZetaCommonTools.rMax( ( tot_jornada - ( ( dias_nt + dias_falta + dias_no_hay + permiso_sg ) * tot_jornada / Dias ) ), 0 );
                         dias_asis := ZetaCommonTools.iMax( ( Dias - ( dias_nt + dias_falta + dias_no_hay ) ), 0 );
                         tot_domingo := ZetaCommonTools.rMin( Domingo, tot_regular );
                    end;
               end;
          end;
          { Se inicializan igual que en SetAsistenciaCompleta }
          with FDiasHoras do
          begin
               HorasJornada := tot_jornada;
               DiasTurno := FDatosJornada.Dias;
               DiasAsistencia := dias_asis;
               DiasSinGoce := permiso_sg;
               DiasConGoce := permiso_cg;
               DiasNoTrabajados := ZetaCommonTools.iMin( ( dias_nt + dias_no_hay ), ( DatosPeriodo.Dias + dias_no_hay ) );
               DiasIncapacidad := dias_inca;
               DiasIncapSubsidioHabil := dias_inca_subhabil;
               DiasIncapSubsidioDescanso := dias_inca_subdesc;
               DiasIncapSinSubsidio := dias_inca_sinsub;
               DiasFaltaVacacion := dias_fv;
               DiasRetardo := atrasos;
               FaltasInjustificadas := faltas_in;
               FaltasJustificadas := permiso_fj;
               DiasSuspension := permiso_su;
               DiasOtroPermiso := permiso_ot;
               DiasVacaciones := dias_vaca;
               DiasPrimaVaca  := dias_primavaca;
{$ifdef IMSS_VACA}
               DiasVacaTotales := dias_vt;
               DiasVacaPagadosCalendario := dias_vcal;
{$endif}
               DiasSubsidioIncapacidad := GetSubsidioIncapacidad( iEmpleado );
               SetDiasIncapacidadAnterior( iEmpleado, dInicio, dFinal );
               DiasAguinaldo := 0;
               DiasAjuste := 0;
               Horas := tot_regular;
               HorasAdicionales := 0;
               { GA: 06/Sep/2000 }
               { Si la n�mina NO es Liquidaci�n ni indemnizaci�n }
               { entonces si se deben de tomar en cuenta las horas }
               { dobles, triples y retardos de Asistencia }
               { Esto dificulta que se pueda cambiar el campo NOMINA.NO_LIQUIDA }
               { mediante la interfase de usuario SIN borrar las excepciones }
               if ( TipoLiquidacion = lnNormal ) then
               begin
                    HorasTardes := tot_tardes;
{$ifdef QUINCENALES}
                    HorasNoTrabajadas := tot_HorasNT;
{$endif}
                    HorasDobles := tot_dobles;
                    HorasTriples := ZetaCommonTools.rMax( ( tot_extras - tot_dobles ), 0 );
               end
               else { En caso contrario, lo que manda son las excepciones }
               begin
                    HorasTardes := 0;
{$ifdef QUINCENALES}
                    HorasNoTrabajadas := tot_HorasNT;    // Cuando se implemente en la liquidaci�n cambiar esta asignaci�n a cero
{$endif}
                    HorasDobles := 0;
                    HorasTriples := 0;
               end;
               HorasExtras := HorasDobles + HorasTriples;
               HorasDescansoTrabajado := tot_des_tra;
               HorasFestivoTrabajado := tot_fes_tra;
               HorasFestivoPagado := tot_fes_pag;
               HorasPrimaDominical := tot_domingo;
               HorasPrimaDominicalTotal := tot_dom_total;
               HorasVacacionesTrabajadas := tot_vac_tra;
               HorasFaltaVacacion := h_horas_vj;
               HorasConGoce := tot_per_cg;
               HorasSinGoce := tot_per_sg;
               HorasExentas := h_exentas;
               Status := spPreNomina;
               UsuarioReloj := UsuarioActivo;
{$ifdef QUINCENALES}
               InicioAsistencia := dInicio;
               FinAsistencia := dFinal;
{$endif}
               HorasExtPrePagadas := tot_ext_prep;

{$ifdef CAMBIO_TNOM}
               DiasPeriodoCal:= Trunc(dFinal + 1) - Trunc(dInicio);
{$endif}
               DiasHabiles := dias_habiles;
               DiasFestivos := dias_festivos;
               DiasDescansoTrabajado := d_des_tra;
               DiasFestivoTrabajado := d_fes_tra;
          end;
          if not lCheca or lConHorario then
             CalculaDiasIMSS( dias_no_hay )  { Dias para Seguro Social }
          else
          begin
               with FDiasHoras do
               begin
                    DiasSS := dias_asis;
                    DiasEM := dias_asis;
               end;
          end;
{$ifdef ANTES}
          if ( FDiasHoras.DiasSS = 0 ) and ( dias_fv < ( FDiasHoras.DiasTurno - dias_no_hay ) ) then
{$else}
{
	Cuando por lo menos hay un d�a de vacaci�n no se debe de aplicar el FSS dado
	Que el empleado si cotiz� para el IMSS durante el per�odo.
     Los DiasSS se encargan de asegurar que otro tipo de incidencias sean consideradas para la asignaci�n de FSS (incapacidades, permisos, d�as no trabajados, etc.)
     Esta solucion no considera que el efecto de FSS se da por semana pero que este algoritmo trabaja para todos los d�as del per�odo, lo cual puede traer problemas para per�odos quincenales. Sin embargo, este tipo de per�odos har�an mucho m�s dif�cil y costoso desarrollar esta soluci�n a fondo
}
          if ( FDiasHoras.DiasSS = 0 ) and ( dias_fv <= 0 ) then
{$endif}

          begin
               with FListaDescansos do
               begin
                    if ( Count > 0 ) then     // Hubo descansos   - Se debe invocar Clear antes de recorrer el query de ausencia
                    begin
                         try
                            RevisarBloqueoPrenomina(False);
                            ParamAsInteger( FUpdateAusenciaFSS, 'Empleado', iEmpleado );    // Prepara Query para indicar el Empleado
                            for i := 0 to ( Count - 1 ) do    // Recorre la lista
                            begin
                                 ParamAsDate( FUpdateAusenciaFSS, 'Fecha', StrToFloat( Strings[i] ) );
                                 Ejecuta( FUpdateAusenciaFSS );
                            end;
                         Finally
                                RevisarBloqueoPrenomina(True);
                         end;
                    end;
               end;
          end;
     end;
end;


{ ******** Preparaci�n de D�as / Horas de N�mina ********* }

{ La ejecuci�n de este m�todo debe ser precedida }
{ por una llamada a VerificaRegistro }

procedure TNomina.PreparaDiasHoras( const iEmpleado: TNumEmp );
var
   lCheca: Boolean;
{$ifdef QUINCENALES}
   dIngreso, dBaja: TDate;
{$endif}
begin
     with FEmpleadoDatos do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FEmpleadoDatos, 'Empleado', iEmpleado );
          end;
          Active := True;
          lCheca := ZetaCommonTools.zStrToBool( FieldByName( 'CB_CHECA' ).AsString );
{$ifdef QUINCENALES}
          dIngreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
          dBaja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
{$endif}
          Active := False;
     end;
     LeeDiasHoras( iEmpleado );
{$ifdef QUINCENALES}
     SumaDiasHoras( iEmpleado, lCheca, dIngreso, dBaja );
{$else}
     SumaDiasHoras( iEmpleado, lCheca );
{$endif}
     Totaliza( iEmpleado, False );
end;

procedure TNomina.PreparaDiasHorasBegin;
begin
     InitQueries;
     InitGlobales;
     {$ifdef CAMBIO_TNOM}
     GetDatosEmpleadoBegin;
     {$else}
     GetStatusEmpleadoBegin;
     {$endif}
     GetPagoVacacionesBegin;
{$ifdef IMSS_VACA}
     GetDiasPagoVacacionesBegin;
{$endif}
     GetSubsidioIncapacidadBegin;
     SetDiasIncapacidadAnteriorBegin;
     CalculaTotalesBegin;
     LeeDiasHorasBegin;
     SetDiasHorasBegin;
     GrabaDiasHorasBegin;
{$ifdef QUINCENALES}
     SetNominaEvaluadorBegin;
     SetPlanVacacionesBegin;
{$endif}
     with oZetaProvider do
     begin
{$ifdef Quincenales}
   { *****************************************************************************
     * No se ocupa usar el inicio de asistencia por que TU_NOMINA se investigar� *
     * de la n�mina que se calcula                                               *
     ***************************************************************************** }
{$endif}
          FEmpleadoDatos := CreateQuery( FOrmat( GetSQLScript( Q_EMPLEADO_DATOS ), [ ZetaCommonTools.DateToStrSQLC( DatosPeriodo.Inicio ) ] ) );
          FPeriodoSimulacion := GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS );
     end;
end;

procedure TNomina.PreparaDiasHorasEnd;
begin
     FreeAndNil( FEmpleadoDatos );
{$ifdef QUINCENALES}
     SetNominaEvaluadorEnd;
     SetPlanVacacionesEnd;
{$endif}
     GrabaDiasHorasEnd;
     SetDiasHorasEnd;
     LeeDiasHorasEnd;
     CalculaTotalesEnd;
     GetSubsidioIncapacidadEnd;
     SetDiasIncapacidadAnteriorEnd;
     GetPagoVacacionesEnd;
{$ifdef IMSS_VACA}
     GetDiasPagoVacacionesEnd;
{$endif}
{$ifdef CAMBIO_TNOM}
     GetDatosEmpleadoEnd;
{$else}
     GetStatusEmpleadoEnd;
{$endif}
end;

{ ********* Preparaci�n de N�mina ************ }

procedure TNomina.NominaPreparaBegin;
begin
     PreparaDiasHorasBegin;
     IncluyeNominaBegin;
     oZetaProvider.InitArregloTPeriodo;//acl     
     {$ifdef CAMBIO_TNOM}
     FEmpleadoTipoNomina := oZetaProvider.CreateQuery( Format( GetSQLScript( Q_DATOS_EMPLEADO_TEMP ), [ DateToStrSQLC( oZetaProvider.DatosPeriodo.Inicio ),
                                                                                                        DateToStrSQLC( oZetaProvider.DatosPeriodo.Fin ) ] ) );
     {$else}
     FEmpleadoTipoNomina := oZetaProvider.CreateQuery( Format( GetSQLScript( Q_EMPLEADO_TIPO_NOMINA ), [ DateToStrSQLC( oZetaProvider.DatosPeriodo.Inicio ) ] ) );
     {$endif}
end;

procedure TNomina.NominaPreparaEnd;
begin
     FreeAndNil( FEmpleadoTipoNomina );
     IncluyeNominaEnd;
     PreparaDiasHorasEnd;
     CalculaStatusPeriodo;
end;

{ Este m�todo siempre debe ser seguido por la }
{ ejecuci�n de NominaTotaliza para el mismo empleado y Per�odo }
function TNomina.NominaPrepara(const iEmpleado: TNumEmp): Boolean;
var
   dIngreso, dAntig, dBaja, dCambioTNom: TDate;
   TipoNomina: eTipoPeriodo;
   lActivo, lCheca: Boolean;
   PeriodoBaja: TDatosPeriodo;
   sTurno: String;

   procedure ValidaFechaCambioTNom;     
   begin
        { Si la fecha que cambio la n�mina es mayor a la fecha final del periodo pero SI cambio la n�mina
          significa que esa fecha no es realmente la fecha de cambio }
        with oZetaProvider do
        begin
             if ( dCambioTNom > DatosPeriodo.Fin ) then
             begin
                  with Queries do
                  begin
                       GetFechaCambioTNomBegin;
                       try
                          dCambioTNom:= GetFechaCambioTNom( iEmpleado, DatosPeriodo.Fin );
                       finally
                             GetFechaCambioTNomEnd;
                       end;
                  end;
             end;
        end;
   end;

begin
     with FEmpleadoDatos do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FEmpleadoDatos, 'Empleado', iEmpleado );
          end;
          Active := True;
          lCheca := ZetaCommonTools.zStrToBool( FieldByName( 'CB_CHECA' ).AsString );
          lActivo := ZetaCommonTools.zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
          dIngreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
          dAntig := FieldByName( 'CB_FEC_ANT' ).AsDateTime;
          dBaja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
          TipoNomina := eTipoPeriodo( FieldByName( 'TU_NOMINA' ).AsInteger );
          sTurno := FieldByName( 'TU_CODIGO' ).AsString;
          dCambioTNom:= FieldByName( 'CB_FEC_NOM').AsDateTime;
          with PeriodoBaja do
          begin
               Year := FieldByName( 'CB_NOMYEAR' ).AsInteger;
               Tipo := eTipoPeriodo( FieldByName( 'CB_NOMTIPO' ).AsInteger );
               Numero := FieldByName( 'CB_NOMNUME' ).AsInteger;
          end;

          with oZetaProvider do
          begin
               { Si la fecha de ingreso o baja es mayor a la fecha de asistencia del periodo investiga fechas
                 en base a Kardex }
               if ( dBaja > DatosPeriodo.Inicio ) and ( dIngreso > DatosPeriodo.Fin )  then
               begin
                    with Queries do
                    begin
                         GetFechaAltaBegin;
                         try
                            dIngreso:= GetFechaAlta( DatosPeriodo.FinAsis, iEmpleado );
                         finally
                                GetFechaAltaEnd;
                         end;
                    end;
               end;

               if ( dIngreso > DatosPeriodo.Inicio ) and ( dIngreso <= DatosPeriodo.Fin )
               {$ifdef CAMBIO_TNOM}
                   or EsCambioTNomPeriodo( dCambioTNom, DatosPeriodo )
               {$endif} then
               begin
                    ParamAsInteger( FEmpleadoTipoNomina, 'Empleado', iEmpleado );
                    FEmpleadoTipoNomina.Active := True;

                    ValidaFechaCambioTNom;
                    { En un reingreso o un cambio de tipo de n�mina a medio periodo,
                    al menos que en esa n�mina este la baja se incluye y que la n�mina al principio
                    del periodo sea igual a la del c�lculo }
                    {$ifdef CAMBIO_TNOM}
                    if ( ( ( dBaja < DatosPeriodo.Inicio ) and ( dCambioTNom = dIngreso ) ) or
                       ( DatosPeriodo.Tipo <> TipoNomina ) ) then
                    begin
                         TipoNomina := eTipoPeriodo( FEmpleadoTipoNomina.FieldByName( 'NOMINA_FIN' ).AsInteger );
                    end;
                    {$else}
                    if (DatosPeriodo.Tipo <> TipoNomina) then
                    begin
                         TipoNomina := eTipoPeriodo( FEmpleadoTipoNomina.FieldByName( 'TU_NOMINA' ).AsInteger );
                         Log.Advertencia( iEmpleado, Format( 'Tipos de N�mina diferentes antes ( %d ) y despu�s ( %d ) del ingreso', [ Ord(TipoNomina), Ord(DatosPeriodo.Tipo) ] ) );
                    end;
                    {$endif}

                    FEmpleadoTipoNomina.Active := FALSE;
               end;
          end;
          Active := False;
     end;
     { Se cambi� para validar los cambios de tipo de n�mina a medio periodo }
     Result := IncluyeNomina( iEmpleado, dIngreso, dAntig, dBaja, TipoNomina, lActivo, PeriodoBaja, dCambioTNom );


     if  Result then  { El empleado SI pertenece a esta N�mina, Incluir N�mina }
     begin
{$ifdef QUINCENALES}
          SumaDiasHoras( iEmpleado, lCheca, dIngreso, dBaja );
{$else}
          SumaDiasHoras( iEmpleado, lCheca );
{$endif}
     end;                                         
end;

{$ifdef QUINCENALES}
procedure TNomina.SumaDiasHoras( const iEmpleado: TNumEmp; const lCheca: Boolean; const dIngreso, dBaja: TDate );
{$else}
procedure TNomina.SumaDiasHoras( const iEmpleado: TNumEmp; const lCheca: Boolean );
{$endif}
begin
     InitDatosTurnoJornada;
     with FDiasHoras do
     begin
          { Era RevisaJornada() en 1.3 }
          HorasJornada := FDatosJornada.Jornada;
          DiasTurno := FDatosJornada.Dias;
          { La Nomina NO proviene de PreN�mina de Asistencia }
          if ( UsuarioReloj = 0 ) then
          begin
               if ( oZetaProvider.DatosPeriodo.Uso = upOrdinaria ) or
                  ( oZetaProvider.DatosPeriodo.Numero = FPeriodoSimulacion ) then
                  SetDiasHorasCompletas( iEmpleado )
               else
               begin
                    SetDiasHorasCeros;
                    DiasVacaciones := GetPagoVacaciones( iEmpleado, DiasPrimaVaca );
                    DiasSubsidioIncapacidad := GetSubsidioIncapacidad( iEmpleado );
{$ifdef IMSS_VACA}
                    DiasVacaPagadosCalendario := GetDiasPagoVacaciones( iEmpleado );
{$endif}
               end;
          end
          else { La N�mina procede de Pren�mina de Asistencia }
          begin
{$ifdef QUINCENALES}
               CalculaTotales( iEmpleado, lCheca, dIngreso, dBaja );
{$else}
               CalculaTotales( iEmpleado, lCheca );
{$endif}
          end;
          { � En donde se modifica el NO_STATUS y PE_STATUS ? }
          Status := spSinCalcular;
     end;
end;

{ Este m�todo siempre debe ser precedido por la }
{ ejecuci�n de NominaPrepara para el mismo empleado y Per�odo }
procedure TNomina.Totaliza( const iEmpleado: TNumEmp; const lVerificaJornada: Boolean );
begin
     { Recalcula Excepciones de Dias/Horas }
     CalculaDiasHoras( iEmpleado );
     { EZM: 6/Dic/99, para evitar N�minas Especiales con Jornada=0 en caso de R�tmicos }
     with FDiasHoras do
     begin
          //if ( HorasJornada = 0 ) and ( oZetaProvider.DatosPeriodo.Numero > K_LIMITE_NOM_NORMAL ) then
          with oZetaProvider do
          begin
               InitGlobales;
               {AP(23/Oct/2008): faltaba asignar las fechas de asistencia en n�minas especiales }
               if ( DatosPeriodo.Numero > GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS )  ) then
               begin
                    InicioAsistencia:= FechaDef( InicioAsistencia, DatosPeriodo.InicioAsis );
                    FinAsistencia:=  FechaDef( FinAsistencia, DatosPeriodo.FinAsis );

                    if ( HorasJornada = 0 ) then
                       HorasJornada := 1;
               end;
          end;
          if lVerificaJornada and ( HorasJornada <= 0 ) then
             raise EDataBaseError.Create( 'Jornada No Puede Ser CERO' );
     end;
     GrabaDiasHoras( iEmpleado );
{$ifdef QUINCENALES}
     SetNominaEvaluador( iEmpleado );
{$endif}
end;

{ Este m�todo siempre debe ser precedido por la }
{ ejecuci�n de NominaPreparaf para el mismo empleado y Per�odo }
procedure TNomina.NominaTotaliza( const iEmpleado: TNumEmp );
begin
     Totaliza( iEmpleado, True );
end;

{ ****** Preparaci�n de N�mina ************* }

procedure TNomina.PreparaNominaBegin;
begin
     with oZetaProvider do
     begin
          FNominaAgrega := CreateQuery( GetSQLScript( Q_NOMINA_AGREGA ) );
          FNominaExiste := CreateQuery( GetSQLScript( Q_NOMINA_EXISTE ) );
          FNominaJornada := CreateQuery( GetSQLScript( Q_NOMINA_JORNADA ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FNominaAgrega, 'Year', Year );
               ParamAsInteger( FNominaAgrega, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FNominaAgrega, 'Numero', Numero );
               ParamAsInteger( FNominaExiste, 'Year', Year );
               ParamAsInteger( FNominaExiste, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FNominaExiste, 'Numero', Numero );
               ParamAsInteger( FNominaJornada, 'Year', Year );
               ParamAsInteger( FNominaJornada, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FNominaJornada, 'Numero', Numero );
{$ifdef MSSQL}
               ParamSalida( FNominaAgrega, 'Rotativo' );
{$endif}
          end;
     end;
     GetDatosJornadaBegin;
end;

procedure TNomina.PreparaNominaEnd;
begin
     GetDatosJornadaEnd;
     FreeAndNil( FNominaJornada );
     FreeAndNil( FNominaExiste );
     FreeAndNil( FNominaAgrega );
end;

function TNomina.ExisteNomina( const iEmpleado: TNumEmp; var NomPorFuera: Boolean ): Boolean;
begin
     with FNominaExiste do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FNominaExiste, 'Empleado', iEmpleado );
          end;
          Active := True;
          Result := not Eof;          //( Fields[ 0 ].AsInteger > 0 );
          if Result then
             NomPorFuera:= zStrToBool( FieldByName( 'NO_FUERA' ).AsString )
          else
             NomPorFuera:= FALSE;
          Active := False;
     end;
end;

function TNomina.AgregaNomina( const iEmpleado: TNumEmp ): String;
begin
     with FNominaAgrega do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FNominaAgrega, 'Empleado', iEmpleado );
          end;
{$ifdef INTERBASE}
          Active := True;
          Result := FieldByName( 'ROTATIVO' ).AsString;
          Active := False;
{$else}
          oZetaProvider.Ejecuta( FNominaAgrega );
          Result := oZetaProvider.GetParametro( FNominaAgrega, 'Rotativo' ).AsString;
{$endif}
     end;
end;

function TNomina.ClasificaNomina( const iEmpleado: TNumEmp ): String;
begin
{$ifdef INTERBASE}
     with FNominaClasifica do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FNominaClasifica, 'Empleado', iEmpleado );
          end;
          Active := True;
          Result := FieldByName( 'ROTATIVO' ).AsString;
          Active := False;
     end;
{$else}
    with oZetaProvider do
    begin
        ParamAsInteger( FNominaClasifica, 'Empleado', iEmpleado );
        
        // RQ #19441 FANOSA - Error Deadlock en el c�lculo de pre-nomina/n�mina
        // DES Bug #19654 FANOSA - Error Deadlock en el c�lculo de pre-nomina/n�mina (bug)
        EmpiezaTransaccion;
        try
          Ejecuta( FNominaClasifica );
          TerminaTransaccion( True );
          Result := GetParametro( FNominaClasifica, 'Rotativo' ).AsString;
        except
             on Error: Exception do
             begin
                  TerminaTransaccion( False );
                  Result := VACIO;
                  Log.Excepcion( 0, 'Error al clasificar la n�mina', Error );
             end;

        end;
    end;
{$endif}
end;

procedure TNomina.ActualizaJornada( const iEmpleado: TNumEmp; const sRotativo: String );
begin
     if ZetaCommonTools.StrLleno( sRotativo ) then
     begin
          with oZetaProvider do
          begin
               ParamAsInteger( FNominaJornada, 'Empleado', iEmpleado );
               with DatosPeriodo do
               begin
{$ifdef QUINCENALES}
                    with Ritmos.GetDatosJornada( sRotativo, InicioAsis, FinAsis ) do
{$else}
                    with Ritmos.GetDatosJornada( sRotativo, Inicio, Fin ) do
{$endif}
                    begin
                         ParamAsInteger( FNominaJornada, 'Dias', Dias );
                         ParamAsFloat( FNominaJornada, 'Jornada', Jornada );
                    end;
               end;
               Ejecuta( FNominaJornada );
          end;
     end;
end;

{ ****** Verificaci�n de N�mina ************** }

procedure TNomina.VerificaStatus( const iEmpleado: TNumEmp );
begin
     with oZetaProvider do
     begin
          ParamAsInteger( FNominaStatus, 'Empleado', iEmpleado );
          Ejecuta( FNominaStatus );
     end;
end;

procedure TNomina.VerificaRegistro( const iEmpleado: TNumEmp; const lMontos: Boolean = FALSE );
var
   lNomPorFuera: Boolean;
begin
     if ExisteNomina( iEmpleado, lNomPorFuera ) then
     begin
          if lMontos and lNomPorFuera then
             DataBaseError( 'No Se Permite Agregar Excepciones A Una N�mina Pagada Por Fuera' )
          else
             VerificaStatus( iEmpleado );
     end
     else
     begin
          ActualizaJornada( iEmpleado, AgregaNomina( iEmpleado ) );
          VerificaStatus( iEmpleado );                                // Indica Status spSinCalcular y Coloca el Usuario
     end;
end;

procedure TNomina.VerificaNominaBegin;
begin
     PreparaNominaBegin;
     with oZetaProvider do
     begin
          FNominaStatus := CreateQuery( GetSQLScript( Q_NOMINA_CAMBIA_STATUS ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FNominaStatus, 'Year', Year );
               ParamAsInteger( FNominaStatus, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FNominaStatus, 'Numero', Numero );
          end;

          ParamAsInteger( FNominaStatus, 'NO_STATUS', Ord( spSinCalcular ) );

          if oZetaProvider.GetGlobalBooleano( K_GLOBAL_SIM_FINIQ_APROBACION  )then
             ParamAsInteger( FNominaStatus, 'NO_APROBA', Ord( ssfSinAprobar ) )
          else
              ParamAsInteger( FNominaStatus, 'NO_APROBA', Ord( ssfAprobada ) );

          ParamAsInteger( FNominaStatus, 'US_CODIGO', UsuarioActivo );
     end;
end;

procedure TNomina.VerificaNominaEnd;
begin
     FreeAndNil( FNominaStatus );
     PreparaNominaEnd;
end;

{ ****** Inclusi�n de N�mina ************** }

procedure TNomina.IncluyeNominaBegin;
begin
     PreparaNominaBegin;
     with oZetaProvider do
     begin
          FNominaBorra := CreateQuery( GetSQLScript( Q_NOMINA_BORRA ) );
          FNominaClasifica := CreateQuery( GetSQLScript( Q_NOMINA_CLASIFICA ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FNominaBorra, 'Year', Year );
               ParamAsInteger( FNominaBorra, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FNominaBorra, 'Numero', Numero );
               ParamAsInteger( FNominaClasifica, 'Year', Year );
               ParamAsInteger( FNominaClasifica, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FNominaClasifica, 'Numero', Numero );
{$ifdef MSSQL}
               ParamSalida( FNominaClasifica, 'Rotativo' );
{$endif}
          end;
     end;
     SetPagoPrimaAniversarioBegin;
end;

procedure TNomina.IncluyeNominaEnd;
begin
     SetPagoPrimaAniversarioEnd;
     FreeAndNil( FNominaClasifica );
     FreeAndNil( FNominaBorra );
     PreparaNominaEnd;
end;

function TNomina.InspeccionaNomina( const iEmpleado: TNumEmp; const lExiste: Boolean ): Boolean;
var
   sRotativo: String;
   rDiasPrima, rPagoVaca : TDiasHoras;
   lAgregaNomina : boolean;
begin
     Result := True;
     if lExiste then
        sRotativo := ClasificaNomina( iEmpleado )
     else
     begin
          with oZetaProvider.DatosPeriodo do
          begin
               lAgregaNomina := ( Uso = upOrdinaria ) or ( not SoloExcepciones );
               if ( not lAgregaNomina ) then
               begin
                    rPagoVaca := GetPagoVacaciones( iEmpleado , rDiasPrima );
                    lAgregaNomina := ( rPagoVaca > 0 ) or ( rDiasPrima > 0 ) or ( GetSubsidioIncapacidad( iEmpleado ) > 0 );
               end;
               if lAgregaNomina then
                  sRotativo := AgregaNomina( iEmpleado )
               else
                   Result := False;
          end;
     end;
     if Result then
     begin
          ActualizaJornada( iEmpleado, sRotativo );
          LeeDiasHoras( iEmpleado ); { Llena FDiasHoras }
     end;
end;

function TNomina.IncluyeNomina(const iEmpleado: TNumEmp; const dIngreso, dAntig, dBaja: TDate; const TipoNomina: eTipoPeriodo; const lActivo: Boolean; const PeriodoBaja: TDatosPeriodo; const dCambioTNom: TDate ): Boolean;
var
   lExiste, lNomPorFuera: Boolean;
begin
     { Si el empleado no pertenece a esta N�mina o no hubo cambio de tipo de n�mina en el periodo, Borrarlo }
     lExiste := ExisteNomina( iEmpleado, lNomPorFuera );
     oZetaProvider.InitGlobales;
     if ZetaCommonTools.RequiereBorrar( oZetaProvider.DatosPeriodo,
                                        PeriodoBaja,
                                        dIngreso,
                                        dBaja,
                                        lActivo,
                                        TipoNomina,
                                        oZetaProvider.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ),
                                        FStatus,
                                        dCambioTNom ) then
     begin
          Result := False;
          { Si existe su n�mina, borrarla }
          if lExiste then
          begin
               with oZetaProvider do
               begin
                    ParamAsInteger( FNominaBorra, 'Empleado', iEmpleado );
                    Ejecuta( FNominaBorra );
               end;
          end;
     end
     else
     begin	{ El empleado SI pertenece a esta N�mina, Incluir N�mina }
          SetPagoPrimaAniversario( iEmpleado, dAntig );
          Result := InspeccionaNomina( iEmpleado, lExiste );
     end;
end;

{ ******** C�lculo de D�as / Horas ********** }

procedure TNomina.CalculaDiasHorasBegin;
begin
     with oZetaProvider do
     begin
          FFaltasLee := CreateQuery( GetSQLScript( Q_FALTAS_LEE ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FFaltasLee, 'Year', Year );
               ParamAsInteger( FFaltasLee, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FFaltasLee, 'Numero', Numero );
          end;
     end;
end;

procedure TNomina.CalculaDiasHorasEnd;
begin
     FreeAndNil( FFaltasLee );
end;

procedure TNomina.CalculaDiasHoras(const iEmpleado: TNumEmp);
var
   eMotivoHoras: eMotivoFaltaHoras;
   eMotivoDias: eMotivoFaltaDias;
   rDias, rHoras: TDiasHoras;
   lHuboOrdinarias: Boolean;

procedure AplicaFaltaDias;
begin
     rHoras := 0;
     with FDiasHoras do
     begin
          if eMotivoDias in [ mfdSinGoce, mfdConGoce, mfdInjustificada, mfdJustificada, mfdIncapacidad, mfdNoTrabajados, mfdSuspension ] then
          begin
               if ( DiasTurno = 0 ) then
                  rHoras := 0
               else
                   rHoras := rDias * ( HorasJornada / DiasTurno );
               Horas := ZetaCommonTools.rMax( ZetaCommonTools.MiRound( ( Horas - rHoras ), 2 ), 0 );
          end;
          case eMotivoDias of
               mfdInjustificada:	FaltasInjustificadas := FaltasInjustificadas + Round( rDias );
               mfdJustificada:	FaltasJustificadas := FaltasJustificadas + Round( rDias );
               mfdSinGoce:
               begin
                    DiasSinGoce := DiasSinGoce + Round( rDias );
                    HorasSinGoce := HorasSinGoce + rHoras;
               end;
               mfdConGoce:
               begin
                    DiasConGoce := DiasConGoce + Round( rDias );
                    HorasConGoce := HorasConGoce + rHoras;
               end;
               mfdSuspension: DiasSuspension := DiasSuspension + Round( rDias );
               mfdOtrosPermisos: DiasOtroPermiso := DiasOtroPermiso + Round( rDias );
               mfdVacaciones:
               begin
                    DiasVacaciones := DiasVacaciones + rDias;
                    DiasVacaPagadosCalendario := DiasVacaPagadosCalendario + Round( rDias );
               end;
               mfdPrimaVacacional: DiasPrimaVaca := DiasPrimaVaca + rDias;
               mfdIncapacidad: DiasIncapacidad := DiasIncapacidad + Round( rDias );
               mfdAguinaldo: DiasAguinaldo := DiasAguinaldo + rDias;
               mfdNoTrabajados: DiasNoTrabajados := DiasNoTrabajados + Round( rDias );
               mfdRetardo: DiasRetardo := DiasRetardo + Round( rDias );
               mfdAjuste: DiasAjuste := DiasAjuste + Round( rDias );
               mfdAsistencia: DiasAsistencia := Round( rDias ); { Este no se suma pues el default es Asistencia Completa }
               mfdIMSS: DiasSS := Round( rDias );               { No se suma }
               mfdEM: DiasEM := Round( rDias );                 { No se suma }
          end;
          { Excepciones que afectan Asistencia, D�as IMSS, D�as EyM }
          if eMotivoDias in [ mfdInjustificada, mfdJustificada, mfdIncapacidad, mfdNoTrabajados, mfdSuspension,mfdSingoce ] then
          begin
               DiasAsistencia := Round( ZetaCommonTools.rMax( DiasAsistencia - rDias, 0 ) );
               CalculaDiasIMSS( Ritmos.GetDiasNoHay( oZetaProvider.DatosPeriodo, FDatosJornada, FDatosTurno, FPrimerDia ) );
          end;
     end;
end;

procedure AplicaFaltaHoras;
begin
     with FDiasHoras do
     begin
          case eMotivoHoras of
               mfhDobles:
               begin
                    HorasDobles := HorasDobles + rHoras;
                    HorasExtras := HorasDobles + HorasTriples;
               end;
               mfhTriples:
               begin
                    HorasTriples := HorasTriples + rHoras;
                    HorasExtras := HorasDobles + HorasTriples;
               end;
               mfhExtras:
               begin
                    HorasExtras := HorasExtras + rHoras;
                    HorasDobles := ZetaCommonTools.rMin( HorasExtras, FDatosTurno.Dobles );
                    HorasTriples := HorasExtras - HorasDobles;
               end;
               mfhOrdinarias:
               begin
                    Horas := rHoras;
                    lHuboOrdinarias := True;
               end;
               mfhRetardo: HorasTardes := HorasTardes + rHoras;
               mfhDomingo: HorasPrimaDominical := rHoras;
               { EZM: 6/Dic/99; mfhAdicionales : Ya se suman Adicionales }
               { EZM: 13/Dic/99: Ya se suman: Dobles, Triples, Extras, Tardes, Fes, Des y Vac_Tra }
               { En el caso de Ordinarias y PrimaDominical el default es <> 0 }
               mfhAdicionales: HorasAdicionales := HorasAdicionales + rHoras;
               mfhFestivo: HorasFestivoTrabajado := HorasFestivoTrabajado + rHoras;
               mfhDescanso: HorasDescansoTrabajado := HorasDescansoTrabajado + rHoras;
               mfhVacaciones: HorasVacacionesTrabajadas := HorasVacacionesTrabajadas + rHoras;
               mfhConGoce: HorasConGoce := HorasConGoce + rHoras;
               mfhSinGoce: HorasSinGoce := HorasSinGoce + rHoras;
          end;
          { Motivos que afectan horas Ordinarias }
          if ( not lHuboOrdinarias ) and ( eMotivoHoras in [ mfhRetardo, mfhSinGoce ] ) then
             Horas := ZetaCommonTools.rMax( Horas - rHoras, 0 );
     end;
end;

begin
     lHuboOrdinarias := False;
     with FFaltasLee do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FFaltasLee, 'Empleado', iEmpleado );
          end;
          Active := True;
          while not Eof do
          begin
               if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) then
               begin
                    eMotivoDias := eMotivoFaltaDias( FieldByName( 'FA_MOTIVO' ).AsInteger );
                    rDias := FieldByName( 'FA_DIAS' ).AsFloat;
                    AplicaFaltaDias;
               end
               else
               begin
                    eMotivoHoras := eMotivoFaltaHoras( FieldByName( 'FA_MOTIVO' ).AsInteger );
                    rHoras := FieldByName( 'FA_HORAS' ).AsFloat;
                    AplicaFaltaHoras;
               end;
               Next;
          end;
          Active := False;
     end;
end;

{ ******* Agregar / Modificar Excepciones de D�a / Hora / Monto ********* }

procedure TNomina.ExcepcionDiaHoraBegin;
begin
     ExcepcionDiaHoraAgregaBegin;
     with oZetaProvider do
     begin
          InitArregloTPeriodo;//acl
          FFaltasExiste := CreateQuery( GetSQLScript( Q_FALTAS_EXISTE ) );
          FFaltasEdit := CreateQuery( GetSQLScript( Q_FALTAS_EDIT ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FFaltasExiste, 'Year', Year );
               ParamAsInteger( FFaltasExiste, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FFaltasExiste, 'Numero', Numero );
               ParamAsInteger( FFaltasEdit, 'Year', Year );
               ParamAsInteger( FFaltasEdit, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FFaltasEdit, 'Numero', Numero );
          end;
     end;
end;

procedure TNomina.ExcepcionDiaHoraEnd;
begin
     FreeAndNil( FFaltasEdit );
     FreeAndNil( FFaltasExiste );
     ExcepcionDiaHoraAgregaEnd;
end;

{ ******* Agregar / Editar Excepciones de D�a / Hora ********* }

procedure TNomina.ExcepcionDiaHora( const iEmpleado: TNumEmp; const dValue: TDate; const sDiaHora: String; const iMotivo: Integer; const rValue: TDiasHoras; const eOperacion: eOperacionMontos );
var
   rDias, rHoras, rMontoAnterior: TDiasHoras;
   lExiste: Boolean;
   sField : string;
begin
     with oZetaProvider do
     begin
          with FFaltasExiste do
          begin
               Active := False;
               ParamAsInteger( FFaltasExiste, 'Empleado', iEmpleado );
               ParamAsChar( FFaltasExiste, 'DiaHora', sDiaHora, K_ANCHO_DIA_HORA );
               ParamAsDate( FFaltasExiste, 'Fecha', dValue );
               ParamAsInteger( FFaltasExiste, 'Motivo', iMotivo );
               Active := True;
               if Eof then
               begin
                    lExiste := False;
                    rDias := 0;
                    rHoras := 0;
               end
               else
               begin
                    lExiste := True;
                    rDias := FieldByName( 'FA_DIAS' ).AsFloat;
                    rHoras := FieldByName( 'FA_HORAS' ).AsFloat;
               end;
               Active := False;
          end;
          if lExiste then
          begin
               if ( sDiaHora = K_TIPO_DIA ) then
               begin
                    rMontoAnterior := rDias;
                    sField := 'FA_DIAS';

                    case eOperacion of
                         omSustituir: rDias := rValue;
                         omSumar: rDias := rDias + rValue;
                         omRestar: rDias := rDias - rValue;
                    end;
                    rHoras := 0;
               end
               else
               begin
                    rMontoAnterior := rHoras;
                    sField := 'FA_HORAS';

                    rDias := 0;
                    case eOperacion of
                         omSustituir: rHoras := rValue;
                         omSumar: rHoras := rHoras + rValue;
                         omRestar: rHoras := rHoras - rValue;
                    end;
               end;
               ParamAsInteger( FFaltasEdit, 'Empleado', iEmpleado );
               ParamAsChar( FFaltasEdit, 'DiaHora', sDiaHora, K_ANCHO_DIA_HORA );
               ParamAsDate( FFaltasEdit, 'Fecha', dValue );
               ParamAsInteger( FFaltasEdit, 'Motivo', iMotivo );
               ParamAsFloat( FFaltasEdit, 'FA_DIAS', rDias );
               ParamAsFloat( FFaltasEdit, 'FA_HORAS', rHoras );
               Ejecuta( FFaltasEdit );

               with DatosPeriodo do
                    Log.Evento( clbExcepcionDias,
                                iEmpleado,
                                Date,
                                'Modificaci�n de ' + GetBitacoraMensaje(clbExcepcionDias) + ' '+ GetPeriodoInfo( Year, Numero, Tipo ),
                                GetFaltasInfo( sDiaHora, dValue,iMotivo ) + CR_LF + CR_LF +
                                sField + CR_LF + ' De: ' + FloatToStr( rMontoAnterior ) + CR_LF +
                                                 ' A : ' + FloatToStr( rDias + rHoras ) );

          end
          else
          begin
               if ( sDiaHora = K_TIPO_DIA ) then
                  ExcepcionDiaAgrega( iEmpleado, eMotivoFaltaDias( iMotivo ), rValue, dValue )
               else
                   ExcepcionHoraAgrega( iEmpleado, eMotivoFaltaHoras( iMotivo ), rValue, dValue );
          end;
     end;
end;

procedure TNomina.ExcepcionDiaHoraAgregaBegin;
begin
     with oZetaProvider do
     begin
          FFaltasAgrega := CreateQuery( GetSQLScript( Q_FALTAS_AGREGA ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FFaltasAgrega, 'PE_YEAR', Year );
               ParamAsInteger( FFaltasAgrega, 'PE_TIPO', Ord( Tipo ) );
               ParamAsInteger( FFaltasAgrega, 'PE_NUMERO', Numero );
          end;
     end;
end;

procedure TNomina.ExcepcionDiaHoraAgregaEnd;
begin
     FreeAndNil( FFaltasAgrega );
end;

procedure TNomina.ExcepcionDiaAgrega( const iEmpleado: TNumEmp; const eMotivo: eMotivoFaltaDias; const rDias: TDiasHoras; const dValue: TDate );
begin
     with oZetaProvider do
     begin
          ParamAsInteger( FFaltasAgrega, 'CB_CODIGO', iEmpleado );
          ParamAsInteger( FFaltasAgrega, 'FA_MOTIVO', Ord( eMotivo ) );
          ParamAsFloat( FFaltasAgrega, 'FA_DIAS', rDias );
          ParamAsFloat( FFaltasAgrega, 'FA_HORAS', 0 );
          ParamAsDate( FFaltasAgrega, 'FA_FEC_INI', dValue );
          ParamAsChar( FFaltasAgrega, 'FA_DIA_HOR', K_TIPO_DIA, K_ANCHO_DIA_HORA );
          Ejecuta( FFaltasAgrega );
     end;
end;

procedure TNomina.ExcepcionHoraAgrega( const iEmpleado: TNumEmp; const eMotivo: eMotivoFaltaHoras; const rHoras: TDiasHoras; const dValue: TDate );
begin
     with oZetaProvider do
     begin
          ParamAsInteger( FFaltasAgrega, 'CB_CODIGO', iEmpleado );
          ParamAsInteger( FFaltasAgrega, 'FA_MOTIVO', Ord( eMotivo ) );
          ParamAsFloat( FFaltasAgrega, 'FA_DIAS', 0 );
          ParamAsFloat( FFaltasAgrega, 'FA_HORAS', rHoras );
          ParamAsDate( FFaltasAgrega, 'FA_FEC_INI', dValue );
          ParamAsChar( FFaltasAgrega, 'FA_DIA_HOR', K_TIPO_HORA, K_ANCHO_DIA_HORA );
          Ejecuta( FFaltasAgrega );
     end;
end;

{ ******* Agregar / Editar Excepciones de Monto ********* }

procedure TNomina.ExcepcionMontoBegin;
begin
     ExcepcionMontoAgregaBegin;
     with oZetaProvider do
     begin
          InitArregloTPeriodo;//acl     
          FMovimienExiste := CreateQuery( GetSQLScript( Q_MOVIMIEN_EXISTE ) );
          FMovimienEdit := CreateQuery( GetSQLScript( Q_MOVIMIEN_EDIT ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FMovimienExiste, 'Year', Year );
               ParamAsInteger( FMovimienExiste, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FMovimienExiste, 'Numero', Numero );
               ParamAsInteger( FMovimienEdit, 'Year', Year );
               ParamAsInteger( FMovimienEdit, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FMovimienEdit, 'Numero', Numero );
               ParamAsInteger( FMovimienEdit, 'US_CODIGO', UsuarioActivo );
          end;
     end;
end;

procedure TNomina.ExcepcionMontoEnd;
begin
     FreeAndNil( FMovimienEdit );
     FreeAndNil( FMovimienExiste );
     ExcepcionMontoAgregaEnd;
end;


function TNomina.GetBitacoraMensaje(eClase: eClaseBitacora) : string;
begin
     case eClase of
          clbExcepcionMonto: Result:= 'Excep Monto';
          clbExcepcionDias: Result := 'Excep D�as/Hrs';
          clbExcepcionGlobal: Result := 'Excep Global';
          clbMontoNomina: Result := 'Monto N�mina';
          else Result := 'Desconocido';
     end;
end;

{function TNomina.GetBitacoraMasInfo(DataSet : TZetaCursor; eClase: eClaseBitacora) : string;
begin
     with Dataset do
          case eClase of
               clbExcepcionMonto, clbMontoNomina: Result := GetMontosInfo( FieldByName( 'CO_NUMERO' ).AsInteger,
                                                                 FieldByName( 'MO_REFEREN' ).AsString  );
               clbExcepcionDias: Result := GetFaltasInfo( FieldByName( 'FA_DIA_HOR' ).AsString ,
                                                          FieldByName( 'FA_FEC_INI' ).AsDateTime,
                                                          FieldByName( 'FA_MOTIVO' ).AsInteger );
               clbExcepcionGlobal: Result := GetMontosInfo( FieldByName( 'CO_NUMERO' ).AsInteger, VACIO );
               else Result := VACIO;
          end;

     if StrLleno(Result) then
        Result := Result + CR_LF + CR_LF;
end;}


procedure TNomina.ExcepcionMonto( const iEmpleado: TNumEmp; const iConcepto: Integer; const sReferencia: String; const rValue: TPesos; const eOperacion: eOperacionMontos; const lPercepcion: Boolean );
var
   rPercepcion, rDeduccion, rMontoAnterior: TPesos;
   lExiste: Boolean;
   sField : string;
begin
     with oZetaProvider do
     begin
          with FMovimienExiste do
          begin
               Active := False;
               ParamAsInteger( FMovimienExiste, 'Empleado', iEmpleado );
               ParamAsInteger( FMovimienExiste, 'Concepto', iConcepto );
               ParamAsVarChar( FMovimienExiste, 'Referencia', sReferencia, K_ANCHO_REFERENCIA );
               Active := True;
               if Eof then
               begin
                    lExiste := False;
                    rPercepcion := 0;
                    rDeduccion := 0;
               end
               else
               begin
                    lExiste := True;
                    rPercepcion := FieldByName( 'MO_PERCEPC' ).AsFloat;
                    rDeduccion := FieldByName( 'MO_DEDUCCI' ).AsFloat;
               end;
               Active := False;
          end;

          rMontoAnterior := rPercepcion + rDeduccion;

          if lExiste then
          begin
               if lPercepcion then
               begin
                    rDeduccion := 0;
                    sField := 'MO_PERCEPC';

                    case eOperacion of
                         omSumar: rPercepcion := rPercepcion + rValue;
                         omRestar: rPercepcion := rPercepcion - rValue;
                         omSustituir: rPercepcion := rValue;
                    end;
               end
               else
               begin
                    rPercepcion := 0;
                    sField := 'MO_DEDUCCI';
                    case eOperacion of
                         omSumar: rDeduccion := rDeduccion + rValue;
                         omRestar: rDeduccion := rDeduccion - rValue;
                         omSustituir: rDeduccion := rValue;
                    end;
               end;

               ParamAsInteger( FMovimienEdit, 'Empleado', iEmpleado );
               ParamAsInteger( FMovimienEdit, 'Concepto', iConcepto );
               ParamAsVarChar( FMovimienEdit, 'Referencia', sReferencia, K_ANCHO_REFERENCIA );
               ParamAsFloat( FMovimienEdit, 'MO_PERCEPC', rPercepcion );
               ParamAsFloat( FMovimienEdit, 'MO_DEDUCCI', rDeduccion );
               Ejecuta( FMovimienEdit );

               with DatosPeriodo do
                    Log.Evento( clbExcepcionMonto,
                                iEmpleado,
                                Date,
                                'Modificaci�n de ' + GetBitacoraMensaje(clbExcepcionMonto) + ' '+ GetPeriodoInfo( Year, Numero, Tipo ),
                                GetMontosInfo( iConcepto, sReferencia ) + CR_LF + CR_LF +
                                sField + CR_LF + ' De: ' + FloatToStr( rMontoAnterior ) + CR_LF +
                                                 ' A : ' + FloatToStr( rPercepcion + rDeduccion ) );

          end
          else
              ExcepcionMontoAgrega( iEmpleado, iConcepto, rValue, sReferencia, lPercepcion );
     end;
end;

procedure TNomina.ExcepcionMontoAgregaBegin;
begin
     with oZetaProvider do
     begin
          FMovimienAgrega := CreateQuery( GetSQLScript( Q_MOVIMIEN_AGREGA ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FMovimienAgrega, 'PE_YEAR', Year );
               ParamAsInteger( FMovimienAgrega, 'PE_TIPO', Ord( Tipo ) );
               ParamAsInteger( FMovimienAgrega, 'PE_NUMERO', Numero );
               ParamAsInteger( FMovimienAgrega, 'US_CODIGO', UsuarioActivo );
               ParamAsBoolean( FMovimienAgrega, 'MO_ACTIVO', True );
          end;
     end;
end;

procedure TNomina.ExcepcionMontoAgregaEnd;
begin
     FreeAndNil( FMovimienAgrega );
end;

procedure TNomina.ExcepcionMontoAgrega( const iEmpleado: TNumEmp; const iConcepto: Integer; const rValor: TPesos; const sReferencia: String; const lPercepcion: Boolean );
begin
     with oZetaProvider do
     begin
          ParamAsInteger( FMovimienAgrega, 'CB_CODIGO', iEmpleado );
          ParamAsInteger( FMovimienAgrega, 'CO_NUMERO', iConcepto );
          ParamAsVarChar( FMovimienAgrega, 'MO_REFEREN', sReferencia, K_ANCHO_REFERENCIA );
          if lPercepcion then
          begin
               ParamAsFloat( FMovimienAgrega, 'MO_PERCEPC', rValor );
               ParamAsFloat( FMovimienAgrega, 'MO_DEDUCCI', 0 );
          end
          else
          begin
               ParamAsFloat( FMovimienAgrega, 'MO_PERCEPC', 0 );
               ParamAsFloat( FMovimienAgrega, 'MO_DEDUCCI', rValor );
          end;
          Ejecuta( FMovimienAgrega );
     end;
end;

{ ********** Liquidacion de Empleados *********** }

procedure TNomina.LiquidacionBegin;
begin
     VerificaNominaBegin;
     PreparaDiasHorasBegin;
     ExcepcionDiaHoraAgregaBegin;
     ExcepcionMontoAgregaBegin;
     with oZetaProvider do
     begin
          FAcumulados := CreateQuery( GetSQLScript( Q_ACUMULADO_AHORROS ) );
          FFaltasBorra := CreateQuery( GetSQLScript( Q_FALTAS_LIQUIDACION_BORRA ) );
          FMovimienBorraAhorrosPrestamos := CreateQuery( GetSQLScript( Q_MOVIMIEN_BORRA_AHORROS_PRESTAMOS ) );
          FNominaLiquidacion := CreateQuery( GetSQLScript( Q_NOMINA_LIQUIDACION ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FAcumulados, 'Year', Year );
               ParamAsInteger( FFaltasBorra, 'Year', Year );
               ParamAsInteger( FFaltasBorra, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FFaltasBorra, 'Numero', Numero );
               ParamAsInteger( FMovimienBorraAhorrosPrestamos, 'Year', Year );
               ParamAsInteger( FMovimienBorraAhorrosPrestamos, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FMovimienBorraAhorrosPrestamos, 'Numero', Numero );
               ParamAsInteger( FNominaLiquidacion, 'Year', Year );
               ParamAsInteger( FNominaLiquidacion, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FNominaLiquidacion, 'Numero', Numero );
          end;
     end;
end;

procedure TNomina.LiquidacionEnd;
begin
     FreeAndNil( FNominaLiquidacion );
     FreeAndNil( FMovimienBorraAhorrosPrestamos );
     FreeAndNil( FFaltasBorra );
     FreeAndNil( FAcumulados );
     ExcepcionMontoAgregaEnd;
     ExcepcionDiaHoraAgregaEnd;
     PreparaDiasHorasEnd;
     VerificaNominaEnd;
end;

procedure TNomina.Liquidacion(const Datos: TDatosLiquidacion; Ahorros, Prestamos: TDataset );
var
   iRelativo: Integer;
begin
     with Datos do
     begin
          with oZetaProvider do
          begin
               { Verificar si Nomina Existe }
               VerificaRegistro( Empleado );
               { Borrar Excepciones de Dia/Hora }
               ParamAsInteger( FFaltasBorra, 'Empleado', Empleado );
               Ejecuta( FFaltasBorra );
               { Agregar Excepciones de Dia/Hora }
               with DatosPeriodo do
               begin
                    if ( Ordinarias >= 0 ) then
                       ExcepcionHoraAgrega( Empleado, mfhOrdinarias, Ordinarias, Inicio );
                    if ( Dobles <> 0 ) then
                       ExcepcionHoraAgrega( Empleado, mfhDobles, Dobles, Inicio );
                    if ( Triples <> 0 ) then
                       ExcepcionHoraAgrega( Empleado, mfhTriples, Triples, Inicio );
                    if ( Tardes <> 0 ) then
                       ExcepcionHoraAgrega( Empleado, mfhRetardo, Tardes, Inicio );
                    if ( PrimaDominical >= 0 ) then
                       ExcepcionHoraAgrega( Empleado, mfhDomingo, PrimaDominical, Inicio );
                    if ( Adicionales <> 0 ) then
                       ExcepcionHoraAgrega( Empleado, mfhAdicionales, Adicionales, Inicio );
                    if ( Vacaciones <> 0 ) then
                       ExcepcionDiaAgrega( Empleado, mfdVacaciones, Vacaciones, Inicio );
                    if ( PrimaVaca <> 0 ) then
                       ExcepcionDiaAgrega( Empleado, mfdPrimaVacacional, PrimaVaca, Inicio );
                    if ( Aguinaldo <> 0 ) then
                       ExcepcionDiaAgrega( Empleado, mfdAguinaldo, Aguinaldo, Inicio );
               end;
               { Borrar Ahorros / Prestamos }
               ParamAsInteger( FMovimienBorraAhorrosPrestamos, 'Empleado', Empleado );
               Ejecuta( FMovimienBorraAhorrosPrestamos );
               { Saldar Ahorros }
               with Ahorros do
               begin
                    //First;
                    while not Eof do
                    begin
                         if ( FieldByName( 'TB_CONCEPT' ).AsInteger = FieldByName( 'TB_RELATIV' ).AsInteger ) then
                            DB.DatabaseError( Format( 'Conceptos Deducci�n y Relativo De %s Son Los Mismos', [ FieldByName( 'TB_ELEMENT' ).AsString ] ) );
                         if Global or
                            ( eTipoAhorro( FieldByName( 'TB_LIQUIDA' ).AsInteger ) = taAutomaticamente ) or
                            ZetaCommonTools.zStrToBool( FieldByName( 'SALDAR' ).AsString ) then
                         begin
                              try
                                 ExcepcionMontoAgrega( Empleado, FieldByName( 'TB_CONCEPT' ).AsInteger, -1 * FieldByName( 'AH_SALDO' ).AsFloat, '', False );
                                 iRelativo := FieldByName( 'TB_RELATIV' ).AsInteger;
                                 if ( iRelativo <> 0 ) then
                                 begin
                                      with FAcumulados do
                                      begin
                                           Active := False;
                                           ParamAsInteger( FAcumulados, 'Empleado', Empleado );
                                           ParamAsInteger( FAcumulados, 'Concepto', iRelativo );
                                           Active := True;
                                           if not Eof then
                                              ExcepcionMontoAgrega( Empleado, iRelativo, -1 * FieldByName( 'AC_ANUAL' ).AsFloat, '', False );
                                           Active := False;
                                      end;
                                 end;
                              except
                                    on Error: Exception do
                                    begin
                                         if DZetaServerProvider.PK_Violation( Error ) then
                                            DB.DatabaseError( 'Conceptos Deducci�n o Relativo Est�n Siendo Usados Por Otro Ahorro' )
                                         else
                                             DB.DatabaseError( format( 'Error: %s', [ Error.Message ] ) );
                                    end;
                              end;
                         end;
                         Next;
                    end;
               end;
               { Saldar Prestamos }
               with Prestamos do
               begin
                    First;
                    while not Eof do
                    begin
                         if Global or
                            ( eTipoAhorro( FieldByName( 'TB_LIQUIDA' ).AsInteger ) = taAutomaticamente ) or
                            ZetaCommonTools.zStrToBool( FieldByName( 'SALDAR' ).AsString ) then
                         begin
                              try
                                 ExcepcionMontoAgrega( Empleado, FieldByName( 'TB_CONCEPT' ).AsInteger, FieldByName( 'PR_SALDO' ).AsFloat, FieldByName( 'PR_REFEREN' ).AsString, False );
                              except
                                    on Error: Exception do
                                    begin
                                         if DZetaServerProvider.PK_Violation( Error ) then
                                            DB.DatabaseError( 'Concepto Deducci�n Est� Siendo Usado Por Otro Pr�stamo' )
                                         else
                                             DB.DatabaseError( format( 'Error: %s', [ Error.Message ] ) );
                                    end;
                              end;
                         end;
                         Next;
                    end;
               end;
               ParamAsInteger( FNominaLiquidacion, 'Empleado', Empleado );
               ParamAsInteger( FNominaLiquidacion, 'NO_LIQUIDA', Ord( Tipo ) );
               ParamAsVarChar( FNominaLiquidacion, 'NO_OBSERVA', Observaciones, K_ANCHO_OBSERVACIONES );
               ParamAsDate( FNominaLiquidacion, 'NO_FEC_LIQ', Baja );
               ParamAsVarChar( FNominaLiquidacion, 'NO_GLOBAL', ZetaCommonTools.zBoolToStr(Global),1);

               Ejecuta( FNominaLiquidacion );
               { Se requiere Invocar el Recalculo de D�as y Horas despu�s de agregar la liquidaci�n,
                 ya que afecta al calculo al darse un tratamiento especial cuando el Tipo de NO_LIQUIDA
                 es diferente a lnNormal }
               PreparaDiasHoras( Empleado );
          end;
     end;
end;

end.


