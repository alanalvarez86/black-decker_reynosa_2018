library Recursos;

uses
  ComServ,
  DServerRecursos in 'DServerRecursos.pas' {dmServerRecursos: TMtsDataModule},
  Recursos_TLB in 'Recursos_TLB.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
