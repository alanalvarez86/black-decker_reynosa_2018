unit DVacaciones;

interface

{.$define FLEXIBLES}

uses Controls, SysUtils, DB,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZCreator,
     DPrestaciones,
     DZetaServerProvider;

type
  TEmpleadoVaca = record
    Empleado: TNumEmp;
    FechaAntig: TDate;
    FechaLastCierre: TDate;
    TablaPrestaciones: TCodigo;
    Salario: TPesos;
  end;
  TInfoPlanVaca = record
    Empleado: TNumEmp;
    Inicio: TDate;
    Regreso: TDate;
    DiasGozo: TDiasHoras;
    DiasPago: TDiasHoras;
    DiasPrima: TDiasHoras;
    Status: eStatusPlanVacacion;
  end;

  TRecalculaVaca = class( TObject )
  private
    { Private declarations }
    FCreator: TZetaCreator;
    FQryEmpVaca: TZetaCursor;
    FQryVacaciones: TZetaCursor;
    FUpdVacaciones: TZetaCursor;
    FUpdEmpVaca: TZetaCursor;
    FInsertCierre: TZetaCursor;
    FInsAjuste: TZetaCursor;
    FBorraAjusteReingreso: TZetaCursor;
    FDataSet: TZetaCursor;
    FBorrarCierres : TZetaCursor;
    FActualizaEmpleado: TZetaCursor;
    FKardexTabla: TZetaCursor;
    function oZetaProvider: TdmZetaServerProvider;
    //procedure InitQueries;
    //procedure QueriesStatusActBegin;
    //procedure QueriesStatusActEnd;
  public
    { Public declarations }
    constructor Create( oCreator: TZetaCreator );
    function GetEmpVaca(const iEmpleado: Integer): TEmpleadoVaca;
    procedure RecalculaVacaBegin;
    procedure RecalculaVaca( const iEmpleado: Integer; const dAntig: TDate );
    procedure RecalculaVacaEnd;
    procedure RevisaCierreBegin;
    procedure RevisaCierre(const iEmpleado: Integer; const dFecha: TDate; const sGlobal: String = 'N'; const sObserva: String = '' ); overload;
    procedure RevisaCierre(const InfoEmpleado: TEmpleadoVaca; const dFecha: TDate; const sGlobal: String = 'N'; const sObserva: String = '' ); overload;
    procedure InsertaCierre(const iEmpleado: Integer; const dFecha: TDate; const sTabla: String; const nPropGozo, nPropPago, nPropPrima: Currency; const sGlobal: String = 'N'; const sObserva: String = '');
    procedure RevisaCierreEnd;
    procedure BorraAjusteReingresoBegin;
    procedure BorraAjusteReingreso( const iEmpleado: Integer; const dFecha: TDate );
    procedure BorraAjusteReingresoEnd;
    procedure AjustaVacaBegin;
    procedure AjustaVacaEnd;
    procedure InsertaAjuste(const iEmpleado: Integer; const dFecha: TDate; const sTabla: String; const rGozo, rPago, rPrima: Currency; const sGlobal: String = 'N'; const sObserva: String = '' );
    procedure RecalculaSaldosBegin;
    procedure RecalculaSaldos(const iEmpleado: Integer; dAntig, dFecha: TDate; sTabla: String; var iAgregarCierres, iBorrarCierres: Integer; const dTopeCierre: TDate = 0 );
    procedure RecalculaSaldosEnd;
  end;

  TPlanVacacion = class( TObject )
  private
    { Private declarations }
    FCreator: TZetaCreator;
    FQryVacaPlan: TZetaCursor;
    FUpdVacaPlan: TZetaCursor;
    FInsVacacion: TZetaCursor;
    FConflictoVac: TZetaCursor;
    FQryPlanVacUS: TZetaCursor;
    FRecalculoVacaciones: TRecalculaVaca;
    FDiasDePago: ePagoPlanVacacion;
    FPagoPrimaAniv: Boolean;
    FInfoPlanVaca: TInfoPlanVaca;
    FInicio:TDate;
    FFin:TDate;
    FPeriodoAntc:Integer;
    function oZetaProvider: TdmZetaServerProvider;
    function HayConflictoExpedientes( var sConflicto : String ) : Boolean;
    procedure IncluirAdvertencia(const sMensaje: TBitacoraTexto; const sNota: String );
    procedure IncluirError(const sTexto: String);
    procedure Despreparar;
    procedure AsignaParametrosVacacion( const InfoEmpVaca: TEmpleadoVaca );
    procedure ProcesaPlanVacacion;
    procedure ActualizaPlanVaca;
    procedure AgregaBitacoraVaca;
    procedure SetFechasPlan;
    procedure LlenaInfoPlanVacacion( const iEmpleado:Integer; const oZCursor: TZetaCursor );
    function  ExistePeriodoPago:Boolean;
    function  ProcesaFechasPlan( var dFecIni,dFecFin:TDate ):Boolean;
  public
    { Public declarations }
    constructor Create( oCreator: TZetaCreator );
    destructor Destroy; override;
    procedure Preparar;
    procedure Procesar( const iEmpleado: Integer; const dInicial, dFinal: TDate );
    procedure ProcesarPagoEspecUs( const iEmpleado: Integer );
  end;
  TPrimaAniversario = class( TObject )
  private
    { Private declarations }
    FCreator: TZetaCreator;
    FQryVacacion: TZetaCursor;
    FDelVacacion: TZetaCursor;
    FInsVacacion: TZetaCursor;
    FSaldoVacacion: TZetaCursor;
    FFaltasLeePrimaV: TZetaCursor;
    FEmpleado: TNumEmp;
    FAntiguedad: TDate;
    FAniversario: TDate;
    FRecalculoVacaciones: TRecalculaVaca;
    FDiasExcepcion:TDiasHoras;
    function oZetaProvider: TdmZetaServerProvider;
    procedure IncluirAdvertencia(const sMensaje: TBitacoraTexto; const sNota: String );
    procedure IncluirError(const sTexto: String);
    procedure Despreparar;
    procedure AsignaParametrosVacacion(const InfoEmpVaca: TEmpleadoVaca; const rDiasPrima: Currency );
    procedure ProcesaPrimaVacacional( const lExisteVaca: Boolean );
    procedure AgregaBitacoraVaca( const lExisteVaca: Boolean; const rDiasPrima: Currency );
    function ExisteExcepcionPrimaVaca(const iEmpleado: Integer): Boolean;
  public
    { Public declarations }
    constructor Create( oCreator: TZetaCreator );
    destructor Destroy; override;
    procedure Preparar;
    procedure Procesar( const iEmpleado: Integer; const dAntig, dAniv: TDate );
  end;

implementation

uses ZGlobalTress,
     ZetaCommontools,
     ZetaServerTools;

const
     K_QRY_EMP_VACA = 0;
     K_QRY_VACACIONES = 1;
     K_UPD_VACACIONES = 2;
     K_INS_CIERRE = 3;
     K_UPD_EMP_VACA = 4;
     K_QRY_PLAN_VACA = 5;
     K_UPD_PLAN_VACA = 6;
     K_QRY_INSERT_VACA = 7;
     K_QRY_STATUS_CONFLICTO = 8;
     K_QRY_VACA_AUTOMATICA = 9;
     K_DEL_VACA_AUTOMATICA = 10;
     K_SALDO_PRIMA_VACA = 11;
     K_QRY_PLAN_PAGO_US = 12;
     K_FECHAS_POR_PERIODO = 13;
     K_MAXIMO_PERIODO = 14;
     K_DEL_AJUSTE_REINGRESO = 15;
     K_INS_AJUSTE_VACA = 16;
     K_BORRAR_CIERRES = 17;
     K_INICIA_SALDOSVACA = 18;
     K_KARDEX_EMPLEADO = 19;
     K_KARDEX_TABLA = 20;
     Q_FALTAS_LEE_PRIMAV = 21;

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          K_QRY_EMP_VACA: Result := 'select CB_FEC_ANT, CB_DER_FEC, CB_TABLASS, CB_SALARIO ' +
                                    'from COLABORA ' +
                                    'where CB_CODIGO = :Empleado ';
          K_QRY_VACACIONES: Result := 'select VA_FEC_INI, VA_TIPO, VA_GOZO, VA_D_GOZO, VA_PAGO, VA_D_PAGO, ' +
                                      'VA_P_PRIMA, VA_D_PRIMA ' +
                                      'from VACACION  ' +
                                      'where VACACION.CB_CODIGO = :Empleado and VA_FEC_INI >= :Fecha ' +
                                      'order by VA_FEC_INI, VA_TIPO';
          K_UPD_VACACIONES: Result := 'update VACACION set '+
                                      'VA_S_GOZO = :SaldoGozo, VA_S_PAGO = :SaldoPago, VA_S_PRIMA = :SaldoPrima, VA_YEAR = :Year '+
                                      'where CB_CODIGO = :Empleado and VA_FEC_INI = :Fecha and VA_TIPO = :Tipo';

          K_INS_CIERRE: Result := 'insert into VACACION ' +
                                  '( CB_CODIGO, VA_FEC_INI, VA_TIPO, CB_TABLASS, VA_D_GOZO, VA_D_PAGO, VA_D_PRIMA, VA_GLOBAL, VA_COMENTA ) ' +
                                  'values ' +
                                  '( :Empleado, :Fecha, :Tipo, :Tabla, :Gozo, :Pago, :Prima, :Global, :Comenta )';
{$ifdef INTERBASE}
          K_UPD_EMP_VACA: Result := 'EXECUTE PROCEDURE UPDATE_EMP_VACA( :Empleado, :Ultima, :Cierre, :TotGozo, :TotPago, :TotPrima, :DerGozo, :DerPago, :DerPrima )';
{$endif}
{$ifdef MSSQL}
          K_UPD_EMP_VACA: Result := 'update COLABORA set '+
                                    'CB_FEC_VAC = :Ultima, CB_DER_FEC = :Cierre, ' +
                                    'CB_V_GOZO = :TotGozo, CB_V_PAGO = :TotPago, CB_V_PRIMA = :TotPrima,' +
                                    'CB_DER_GOZ = :DerGozo, CB_DER_PAG = :DerPago, CB_DER_PV = :DerPrima ' +
                                    'where CB_CODIGO = :Empleado';
{$endif}
          K_QRY_PLAN_VACA: Result := 'select VP_FEC_INI, VP_FEC_FIN, VP_DIAS, VP_STATUS ' +
                                     'from VACAPLAN ' +
                                     'where ( CB_CODIGO = :Empleado ) and ( VP_FEC_INI between :FechaInicial and :FechaFinal ) and ( VP_STATUS in ( %d, %d ) and ( VP_PAGO_US = %s ) )';
          K_UPD_PLAN_VACA: Result := 'update VACAPLAN set ' +
                                     'VP_STATUS = %d, VP_NOMYEAR = :Year, VP_NOMTIPO = :Tipo, VP_NOMNUME = :Numero ' +
                                     'where CB_CODIGO = :Empleado and VP_FEC_INI = :FechaInicio';
          K_QRY_INSERT_VACA: Result := 'insert into VACACION ' +
                                       '( CB_CODIGO,VA_FEC_INI,VA_TIPO,VA_FEC_FIN,VA_CAPTURA,' +
                                       'US_CODIGO,VA_PAGO,VA_GOZO,VA_P_PRIMA,CB_SALARIO,CB_TABLASS,VA_NOMYEAR,' +
                                       'VA_NOMTIPO,VA_NOMNUME,VA_MONTO,VA_SEVEN,VA_TASA_PR,VA_PRIMA,' +
                                       'VA_TOTAL,VA_GLOBAL ) ' +
                                       'values ( :CB_CODIGO,:VA_FEC_INI,:VA_TIPO,:VA_FEC_FIN,' +
                                       ':VA_CAPTURA,:US_CODIGO,:VA_PAGO,:VA_GOZO,:VA_P_PRIMA,:CB_SALARIO,:CB_TABLASS,' +
                                       ':VA_NOMYEAR,:VA_NOMTIPO,:VA_NOMNUME,:VA_MONTO,:VA_SEVEN,:VA_TASA_PR,' +
                                       ':VA_PRIMA,:VA_TOTAL,:VA_GLOBAL )';
          K_QRY_STATUS_CONFLICTO: Result:= 'select RESULTADO, INICIO, FIN ' +
                                           'from SP_STATUS_CONFLICTO( :Empleado, :FechaOriginal, :FechaInicio, :FechaRegreso, :Entidad )';
          K_QRY_VACA_AUTOMATICA: Result := 'select US_CODIGO, VA_PAGO, VA_GOZO, VA_P_PRIMA from VACACION ' +
                                           'where ( CB_CODIGO = :Empleado ) and ( VA_FEC_INI = :Fecha ) and ' +
                                           '( VA_TIPO = %d )';
          K_DEL_VACA_AUTOMATICA: Result := 'delete from VACACION where ( CB_CODIGO = :Empleado ) and ' +
                                               '( VA_FEC_INI = :Fecha ) and ( VA_TIPO = %d ) and ( US_CODIGO = %d )';
          K_SALDO_PRIMA_VACA: Result := 'select CB_DER_FEC, CB_TABLASS, CB_SALARIO, CB_DER_PV, CB_V_PRIMA ' +
                                        'from COLABORA where ( CB_CODIGO = :Empleado )';
          K_QRY_PLAN_PAGO_US: Result := 'select VP_FEC_INI, VP_FEC_FIN, VP_DIAS, VP_STATUS ' +
                                        'from VACAPLAN ' +
                                        'where ( CB_CODIGO = :Empleado ) and ( VP_STATUS = %d ) and ( VP_PAGO_US = %s ) and ( VP_NOMYEAR = %d and VP_NOMTIPO = %d and VP_NOMNUME = %d ) ';
          K_FECHAS_POR_PERIODO: Result := 'select PE_ASI_INI,PE_ASI_FIN from PERIODO where PE_CAL = %s and PE_NUMERO = %d and PE_YEAR = %d and PE_TIPO = %d';
          K_MAXIMO_PERIODO:     Result := 'select MAX( PE_NUMERO ) MAX_PERIODO from PERIODO where PE_CAL = %s and PE_YEAR = %d and PE_TIPO = %d';

          K_DEL_AJUSTE_REINGRESO: Result:= 'delete from VACACION where ( CB_CODIGO = :Empleado ) and ' +
                                           '( VA_FEC_INI = :Fecha ) and ( VA_TIPO = :Tipo ) and ( VA_AJUSTE = ''S'' )';

          K_INS_AJUSTE_VACA: Result := 'insert into VACACION ' +
                                       '( CB_CODIGO,VA_FEC_INI,VA_TIPO,VA_FEC_FIN,' +
                                       'VA_PAGO,VA_GOZO,VA_P_PRIMA,CB_TABLASS,VA_GLOBAL,VA_COMENTA,US_CODIGO,VA_CAPTURA, VA_AJUSTE ) ' +
                                       'values ( :CB_CODIGO,:VA_FEC_INI,:VA_TIPO,:VA_FEC_FIN,' +
                                       ':VA_PAGO,:VA_GOZO,:VA_P_PRIMA,:CB_TABLASS,:VA_GLOBAL,:VA_COMENTA,:US_CODIGO,:VA_CAPTURA, ''S'' )';
          K_BORRAR_CIERRES:  Result  := 'delete from VACACION where CB_CODIGO = :Empleado and VA_TIPO = 0 and VA_FEC_INI >= :Fecha';
          K_INICIA_SALDOSVACA: Result  := 'update COLABORA set CB_DER_FEC = ' + DateToStrSQLC( NullDateTime ) + ', CB_DER_PAG = 0, CB_DER_GOZ = 0, CB_DER_PV = 0 where CB_CODIGO = :Empleado';
          K_KARDEX_EMPLEADO:  Result  := 'Select CB_FECHA, CB_TABLASS from KARDEX where CB_CODIGO = :Empleado and CB_TIPO in ( %s ) and CB_FECHA > :Fecha order by CB_FECHA';
{$ifdef MSSQL}
          K_KARDEX_TABLA: Result:= 'select DBO.SP_KARDEX_CB_TABLASS( :Fecha, :Empleado ) CB_TABLASS';
{$else}
          K_KARDEX_TABLA: Result:= 'select CB_TABLASS from SP_FECHA_KARDEX( :Fecha , :Empleado ) CB_TABLASS';
{$endif}
          Q_FALTAS_LEE_PRIMAV: Result := 'select FA_DIAS from FALTAS where '+
                                         '( CB_CODIGO = :Empleado ) and '+
                                         '( PE_YEAR = :Year ) and '+
                                         '( PE_TIPO = :Tipo ) and '+
                                         '( PE_NUMERO = :Numero ) and '+
                                         '( FA_MOTIVO = :Motivo ) ';
     end;
end;

{ TRecalculaVaca }

constructor TRecalculaVaca.Create(oCreator: TZetaCreator);
begin
     FCreator := oCreator;
end;

function TRecalculaVaca.oZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

procedure TRecalculaVaca.RecalculaVacaBegin;
begin
     with oZetaProvider do
     begin
          FQryVacaciones := CreateQuery( GetSQLScript( K_QRY_VACACIONES ) );
          FUpdVacaciones := CreateQuery( GetSQLScript( K_UPD_VACACIONES ) );
          FUpdEmpVaca := CreateQuery( GetSQLScript( K_UPD_EMP_VACA ) );
     end;
end;

procedure TRecalculaVaca.RecalculaVacaEnd;
begin
     FreeAndNil( FQryVacaciones );
     FreeAndNil( FUpdVacaciones );
     FreeAndNil( FUpdEmpVaca );
end;

procedure TRecalculaVaca.RecalculaVaca( const iEmpleado: Integer; const dAntig: TDate );
var
    eTipo    : eTipoVacacion;
    dVaca, dCierre, dUltima : TDate;
    DerGozo, DerPago, DerPrima, TotGozo, TotPago, TotPrima : Currency;
    lAntig : Boolean;
begin
     dCierre := 0;
     dUltima := 0;
     DerGozo := 0;
     TotGozo := 0;
     DerPago := 0;
     TotPago := 0;
     DerPrima := 0;
     TotPrima := 0;

     with oZetaProvider, FQryVacaciones do
     begin
          InitGlobales;
          Active := FALSE;
          ParamAsInteger( FQryVacaciones, 'Empleado', iEmpleado );
          ParamAsDate(    FQryVacaciones, 'Fecha', dAntig );
          Active := TRUE;
          // Parametros que no cambian en el ciclo
          ParamAsInteger( FUpdVacaciones, 'Empleado', iEmpleado );

          while not Eof do
          begin
               eTipo  := eTipoVacacion( FieldByName( 'VA_TIPO' ).AsInteger );
               dVaca := FieldByName( 'VA_FEC_INI' ).AsDateTime;
               if ( eTipo = tvCierre ) then
               begin
                    DerGozo := DerGozo + FieldByName( 'VA_D_GOZO' ).AsFloat;
                    DerPago := DerPago + FieldByName( 'VA_D_PAGO' ).AsFloat;
                    DerPrima := DerPrima + FieldByName( 'VA_D_PRIMA' ).AsFloat;
                    dCierre := dVaca;
               end
               else
               begin
                    TotGozo := TotGozo + FieldByName( 'VA_GOZO' ).AsFloat;
                    TotPago := TotPago + FieldByName( 'VA_PAGO' ).AsFloat;
                    TotPrima := TotPrima + FieldByName( 'VA_P_PRIMA' ).AsFloat;
                    dUltima := dVaca;
               end;

               { Actualiza Saldo Corriente y Year de Vacaciones }
               ParamAsFloat(   FUpdVacaciones, 'SaldoGozo', ( DerGozo - TotGozo ) );
               ParamAsFloat(   FUpdVacaciones, 'SaldoPago', ( DerPago - TotPago ) );
               ParamAsFloat(   FUpdVacaciones, 'SaldoPrima', ( DerPrima - TotPrima ) );
               ParamAsInteger( FUpdVacaciones, 'Year', Trunc( YearsCompletos( dAntig, dVaca, lAntig, GetGlobalBooleano( K_GLOBAL_AVENT ) ) ));
               ParamAsDate(    FUpdVacaciones, 'Fecha', dVaca );
               ParamAsInteger( FUpdVacaciones, 'Tipo', Ord( eTipo ));
               Ejecuta( FUpdVacaciones );
               Next;
         end;

         { Actualiza Colabora }
         ParamAsInteger( FUpdEmpVaca, 'Empleado', iEmpleado );
         ParamAsDate(    FUpdEmpVaca, 'Ultima', dUltima );
         ParamAsDate(    FUpdEmpVaca, 'Cierre', dCierre );
         ParamAsFloat(   FUpdEmpVaca, 'TotGozo', TotGozo );
         ParamAsFloat(   FUpdEmpVaca, 'TotPago', TotPago );
         ParamAsFloat(   FUpdEmpVaca, 'TotPrima', TotPrima );
         ParamAsFloat(   FUpdEmpVaca, 'DerGozo', DerGozo );
         ParamAsFloat(   FUpdEmpVaca, 'DerPago', DerPago );
         ParamAsFloat(   FUpdEmpVaca, 'DerPrima', DerPrima );
         Ejecuta( FUpdEmpVaca );
     end;
end;

procedure TRecalculaVaca.RevisaCierreBegin;
begin
     FCreator.PreparaPrestaciones;
     with oZetaProvider do
     begin
          FQryEmpVaca := CreateQuery( GetSQLScript( K_QRY_EMP_VACA ) );
          FInsertCierre := CreateQuery( GetSQLScript( K_INS_CIERRE ) );
     end;
end;

procedure TRecalculaVaca.RevisaCierreEnd;
begin
     FreeAndNil( FQryEmpVaca );
     FreeAndNil( FInsertCierre );
end;

procedure TRecalculaVaca.AjustaVacaBegin;
begin
     with oZetaProvider do
          FInsAjuste := CreateQuery( GetSQLScript( K_INS_AJUSTE_VACA ) );
end;

procedure TRecalculaVaca.AjustaVacaEnd;
begin
     FreeAndNil( FInsAjuste );
end;

function TRecalculaVaca.GetEmpVaca(const iEmpleado: Integer): TEmpleadoVaca;
begin
     with FQryEmpVaca do
     begin
          Active := FALSE;
          oZetaProvider.ParamAsInteger( FQryEmpVaca, 'Empleado', iEmpleado );
          Active := TRUE;
          Result.Empleado := iEmpleado;
          if ( not IsEmpty ) then
          begin
               with Result do
               begin
                    FechaAntig := FieldByName( 'CB_FEC_ANT' ).AsDateTime;
                    FechaLastCierre := FieldByName( 'CB_DER_FEC' ).AsDateTime;
                    TablaPrestaciones := FieldByName( 'CB_TABLASS' ).AsString;
                    Salario := FieldByName( 'CB_SALARIO' ).AsFloat;
               end;
          end
          else
          begin
               with Result do
               begin
                    FechaAntig := NullDateTime;
                    FechaLastCierre := NullDateTime;
                    TablaPrestaciones := ZetaCommonClasses.VACIO;
                    Salario := 0;
               end;
          end;
     end;
end;

procedure TRecalculaVaca.RevisaCierre(const iEmpleado: Integer; const dFecha: TDate; const sGlobal: String = 'N'; const sObserva: String = '' );
begin
     RevisaCierre( GetEmpVaca( iEmpleado ), dFecha, sGlobal, sObserva );
end;

procedure TRecalculaVaca.RevisaCierre(const InfoEmpleado: TEmpleadoVaca; const dFecha: TDate; const sGlobal: String = 'N'; const sObserva: String = '' );
var
   dProximoCierre : TDate;
   nProp, nPropPrima, rDiasTabla, rDiasBase : Currency;
begin
     with InfoEmpleado do
     begin
          dProximoCierre := NextDate( FechaAntig, dFecha );
          if ( dProximoCierre > FechaLastCierre ) and ( dProximoCierre > FechaAntig ) then
          begin
               nProp := FCreator.dmPrestaciones.VacaProporcional( FechaAntig, FechaLastCierre, dProximoCierre, TablaPrestaciones, rDiasTabla, rDiasBase, nPropPrima{$ifdef FLEXIBLES} ,Empleado {$endif} );
               InsertaCierre( Empleado, dProximoCierre, TablaPrestaciones, nProp, nProp, nPropPrima, sGlobal, sObserva );
          end;
     end;
end;

procedure TRecalculaVaca.InsertaCierre(const iEmpleado: Integer; const dFecha: TDate; const sTabla: String; const nPropGozo, nPropPago, nPropPrima: Currency; const sGlobal: String = 'N'; const sObserva: String = '');
begin
     with oZetaProvider do
     begin
          ParamAsInteger( FInsertCierre, 'Empleado', iEmpleado );
          ParamAsDate( FInsertCierre, 'Fecha', dFecha );
          ParamAsInteger( FInsertCierre, 'Tipo', Ord( tvCierre ));
          ParamAsChar( FInsertCierre, 'Tabla', sTabla, 1 );
          ParamAsFloat( FInsertCierre, 'Gozo', nPropGozo );
          ParamAsFloat( FInsertCierre, 'Pago', nPropPago );
          ParamAsFloat( FInsertCierre, 'Prima', nPropPrima );
          ParamAsChar( FInsertCierre, 'Global', sGlobal, 1 );
          ParamAsVarChar( FInsertCierre, 'Comenta', sObserva, K_ANCHO_TITULO );
          Ejecuta( FInsertCierre );
     end;
end;

procedure TRecalculaVaca.InsertaAjuste(const iEmpleado: Integer; const dFecha: TDate; const sTabla: String; const rGozo, rPago, rPrima: Currency; const sGlobal: String = 'N'; const sObserva: String = '' );
begin
     with oZetaProvider do
     begin
          ParamAsInteger( FInsAjuste, 'CB_CODIGO', iEmpleado );
          ParamAsDate( FInsAjuste, 'VA_FEC_INI', dFecha );
          ParamAsDate( FInsAjuste, 'VA_FEC_FIN', dFecha );
          ParamAsInteger( FInsAjuste, 'VA_TIPO', Ord( tvVacaciones ));
          ParamAsFloat( FInsAjuste, 'VA_GOZO', rGozo );
          ParamAsFloat( FInsAjuste, 'VA_PAGO', rPago );
          ParamAsFloat( FInsAjuste, 'VA_P_PRIMA', rPrima );
          ParamAsChar( FInsAjuste, 'CB_TABLASS', sTabla, 1 );
          ParamAsChar( FInsAjuste, 'VA_GLOBAL', sGlobal, 1 );
          ParamAsVarChar( FInsAjuste, 'VA_COMENTA', sObserva, K_ANCHO_TITULO );
          ParamAsInteger(FInsAjuste,'US_CODIGO', UsuarioActivo );
          ParamAsDate(FInsAjuste,'VA_CAPTURA', FechaDefault );
          Ejecuta( FInsAjuste );
     end;
end;



procedure TRecalculaVaca.BorraAjusteReingresoEnd;
begin
     with oZetaProvider do
     begin
          FreeAndNil( FBorraAjusteReingreso );
     end;
end;

procedure TRecalculaVaca.BorraAjusteReingreso(const iEmpleado: Integer; const dFecha: TDate);
begin
     with oZetaProvider do
     begin
          ParamAsInteger( FBorraAjusteReingreso, 'Empleado', iEmpleado );
          ParamAsDate( FBorraAjusteReingreso, 'Fecha', dFecha );
          Ejecuta( FBorraAjusteReingreso );
     end;
end;

procedure TRecalculaVaca.BorraAjusteReingresoBegin;
begin
     with oZetaProvider do
     begin
          FBorraAjusteReingreso:= CreateQuery( GetSQLScript( K_DEL_AJUSTE_REINGRESO ) );
          ParamAsInteger( FBorraAjusteReingreso, 'Tipo', Ord( tvVacaciones ));
     end;
end;

procedure TRecalculaVaca.RecalculaSaldosBegin;
begin
     FBorrarCierres := oZetaProvider.CreateQuery( GetSQLScript( K_BORRAR_CIERRES ) );
     FActualizaEmpleado := oZetaProvider.CreateQuery( GetSQLScript( K_INICIA_SALDOSVACA ) );
     FDataSet := oZetaProvider.CreateQuery( Format ( GetSQLScript( K_KARDEX_EMPLEADO ), [EntreComillas( K_T_PRESTA ) + ',' + EntreComillas( K_T_ALTA ) ] ) );
     FKardexTabla:= oZetaProvider.CreateQuery( GetSQLScript( K_KARDEX_TABLA ) );
     {Se ocupa un QueriesStatusActBegin siempre en una llamada a RecalculaSaldosBegin
	  Se quito debido a que, donde se usa el recalculasaldos ya hacia una llamada previa 
      QueriesStatusActBegin;}
end;

{procedure TRecalculaVaca.QueriesStatusActBegin;
begin
     InitQueries;
     FCreator.Queries.GetStatusActEmpleadoBegin;
end; }

procedure TRecalculaVaca.RecalculaSaldos(const iEmpleado: Integer; dAntig, dFecha: TDate; sTabla: String; var iAgregarCierres, iBorrarCierres: Integer; const dTopeCierre: TDate );
var
   dAniv, dUltimoCierre,dKardex: TDate;

   procedure AgregaCierre (dCierre : TDate);
   var StatusEmp : eStatusEmpleado;
       rPrima, rProp, rDiasTabla, rDiasBase : Currency;
   begin
        if ( dUltimoCierre <> dCierre )  then  //Para los casos en que hay un cambio de prestaciones en el mismo d�a que el aniv.
        begin                                  //Nota: Se espera que los registros vengan ordenados.
             StatusEmp := FCreator.Queries.GetStatusActEmpleado( iEmpleado, dCierre );
             if ( StatusEmp = steEmpleado ) then
             begin
                  rProp := FCreator.dmPrestaciones.VacaProporcional( dAntig, dUltimoCierre, dCierre,
                                                               sTabla, rDiasTabla, rDiasBase, rPrima{$ifdef FLEXIBLES}, iEmpleado{$endif} );
                  InsertaCierre( iEmpleado, dCierre, sTabla,
                                 rProp, rProp, rPrima,
                                 K_GLOBAL_SI, VACIO );
                  RecalculaVaca( iEmpleado, dAntig );
                  Inc( iAgregarCierres );
                  dUltimoCierre := dCierre;
             end;
        end;
   end;

begin
     dAniv := DateTheYear ( dAntig, TheYear( dAntig ) + 1 );
     dUltimoCierre := NullDateTime;
     with oZetaProvider do
     begin
          // Borrar Cierres posteriores a la fecha de antig�edad
          ParamAsInteger( FBorrarCierres, 'Empleado', iEmpleado );
          ParamAsDate( FBorrarCierres, 'Fecha', dAntig );
          iBorrarCierres := Ejecuta( FBorrarCierres );
          //Poner en 0 Saldos de vacaciones en Colabora
          ParamAsInteger( FActualizaEmpleado, 'Empleado', iEmpleado );
          Ejecuta( FActualizaEmpleado );
          //Obtener registros de alta y cambio de prestaciones
          ParamAsInteger( FDataSet, 'Empleado', iEmpleado );
          ParamAsDate( FDataSet, 'Fecha', dAntig );
          FDataSet.Active := true;
          if StrVacio(sTabla) then //En el recalculo de vacacaciones global se manda la tabla desde el buildDataset
          begin
               ParamAsInteger( FKardexTabla, 'Empleado', iEmpleado );
               ParamAsDate( FKardexTabla, 'Fecha', dAntig );
               FKardexTabla.Active := true;
               sTabla := FKardexTabla.FieldByName( 'CB_TABLASS' ).AsString;
          end;

          while ( not FDataSet.EOF ) and ( ( dTopeCierre = NullDateTime ) or
                                           ( FDataSet.FieldByName( 'CB_FECHA' ).AsDateTime < dTopeCierre ) ) do
          begin
               dKardex := FDataSet.FieldByName( 'CB_FECHA' ).AsDateTime;
               while ( dAniv < dKardex ) do
               begin
                    AgregaCierre( dAniv );
                    dAniv := DateTheYear( dAntig, TheYear( dAniv ) + 1 );
               end;
               AgregaCierre( dKardex );
               sTabla := FDataSet.FieldByName( 'CB_TABLASS' ).AsString;
               FDataSet.Next;
          end;

          while ( dAniv < dFecha) do
          begin
               AgregaCierre( dAniv );
               dAniv := DateTheYear( dAntig, TheYear( dAniv ) + 1 );
          end;
          FDataSet.Active := false;
     end;
end;

procedure TRecalculaVaca.RecalculaSaldosEnd;
begin
     //QueriesStatusActEnd;
     FreeAndNil( FDataSet );
     FreeAndNil( FBorrarCierres );
     FreeAndNil( FActualizaEmpleado );
     FreeAndNil(FKardexTabla);
end;

{procedure TRecalculaVaca.InitQueries;
begin
     FCreator.PreparaQueries;
end;

procedure TRecalculaVaca.QueriesStatusActEnd;
begin
     FCreator.Queries.GetStatusActEmpleadoEnd;
end;}

{ TPlanVacacion }

constructor TPlanVacacion.Create(oCreator: TZetaCreator);
begin
     FCreator := oCreator;
end;

destructor TPlanVacacion.Destroy;
begin
     Despreparar;
     inherited Destroy;
end;

function TPlanVacacion.oZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

procedure TPlanVacacion.Preparar;
begin
     FCreator.PreparaPrestaciones;
     FRecalculoVacaciones := TRecalculaVaca.Create( FCreator );
     with FRecalculoVacaciones do
     begin
          RecalculaVacaBegin;
          RevisaCierreBegin;
     end;
     with oZetaProvider do
     begin
          InitGlobales;
          InitArregloTPeriodo;  //acl   
          FPeriodoAntc := GetGlobalInteger( ZGlobalTress.K_GLOBAL_PAGO_PERIODO_ANTC );
          if ( FPeriodoAntc > 0 )then
          begin
               SetFechasPlan;
          end;
          FQryVacaPlan := CreateQuery( Format( GetSQLScript( K_QRY_PLAN_VACA ), [ Ord( spvPendiente ), Ord( spvAutorizada ),EntreComillas( K_GLOBAL_NO ) ] ) );
          FUpdVacaPlan := CreateQuery( Format( GetSQLScript( K_UPD_PLAN_VACA ), [ Ord( spvProcesada ) ] ) );
          FInsVacacion := CreateQuery( GetSQLScript( K_QRY_INSERT_VACA ) );
          FConflictoVac := CreateQuery( GetSQLScript( K_QRY_STATUS_CONFLICTO ) );
          with DatosPeriodo do
          begin
               FQryPlanVacUS := CreateQuery( Format( GetSQLScript( K_QRY_PLAN_PAGO_US ), [ Ord( spvAutorizada ),EntreComillas( K_GLOBAL_SI ) ,Year,Ord(Tipo),Numero ] ) );
          end;
          FDiasDePago := ePagoPlanVacacion( GetGlobalInteger( K_GLOBAL_PLAN_VACA_DIAS_PAGO ) );
          FPagoPrimaAniv := GetGlobalBooleano( K_GLOBAL_PAGO_PRIMA_VACA_ANIV );
     end;
end;

procedure TPlanVacacion.SetFechasPlan;
const
     K_MENSAJE_PER_NULL = 'No se tiene definido el per�odo %s; requerido para revisar planes de vacaciones a pagar en �ste per�odo';
var
   iPeriodo,iYear,iMaxPeriodo :Integer;
   oQryPeriodo:TZetaCursor;
   sNota :string;
begin
     with oZetaProvider do
     begin
          oQryPeriodo:= CreateQuery;
          try
             iYear := DatosPeriodo.Year;
             if( DatosPeriodo.EsCalendario ) then
             begin
                  AbreQueryScript( oQryPeriodo, Format( GetSQLScript( K_MAXIMO_PERIODO ),[EntreComillas( K_GLOBAL_SI ),iYear,Ord( DatosPeriodo.Tipo ) ] ) );
                  iMaxPeriodo := oQryPeriodo.FieldByName( 'MAX_PERIODO' ).AsInteger;
                  iPeriodo := DatosPeriodo.Numero + FPeriodoAntc;
                  if( iPeriodo > iMaxPeriodo )then
                  begin
                       iPeriodo := iPeriodo - iMaxPeriodo;
                       iYear := iYear + 1;
                  end;
                  AbreQueryScript( oQryPeriodo, Format( GetSQLScript( K_FECHAS_POR_PERIODO ) ,[EntreComillas( K_GLOBAL_SI ),iPeriodo,iYear,Ord( DatosPeriodo.Tipo ) ] ) );
                  FInicio := oQryPeriodo.FieldByName('PE_ASI_INI').AsDateTime;
                  FFin := oQryPeriodo.FieldByName('PE_ASI_FIN').AsDateTime;
                  if( not ExistePeriodoPago )then
                  begin
                       sNota := Format( K_MENSAJE_PER_NULL ,[ZetacommonTools.ShowNomina( iYear, Ord( DatosPeriodo.Tipo ),iPeriodo ) ] ) + CR_LF;
                       sNota := sNota + ' Per�odo: ' + ZetacommonTools.ShowNomina( DatosPeriodo.Year, Ord( DatosPeriodo.Tipo ), DatosPeriodo.Numero ) + CR_LF;
                       sNota := sNota + ' N�mero de periodos anticipados: ' + IntToStr( FPeriodoAntc ) + CR_LF;
                       sNota := sNota + ' Per�odo a pagar: ' + IntToStr( iPeriodo ) + CR_LF;
                       sNota := sNota + ' A�o en el que se paga: ' + IntToStr( iYear );
                       IncluirError ( sNota );
                  end;
             end;
          finally
               FreeAndNil( oQryPeriodo );
          end;
     end;
end;

procedure TPlanVacacion.Despreparar;
begin
     FreeAndNil( FConflictoVac );
     FreeAndNil( FInsVacacion );
     FreeAndNil( FUpdVacaPlan );
     FreeAndNil( FQryVacaPlan );
     FreeAndNil( FQryPlanVacUS );
     with FRecalculoVacaciones do
     begin
          RecalculaVacaEnd;
          RevisaCierreEnd;
     end;
     if Assigned( FRecalculoVacaciones ) then
        FreeAndNil( FRecalculoVacaciones );
end;

procedure TPlanVacacion.Procesar(const iEmpleado: Integer; const dInicial, dFinal: TDate);
var
   dFecIni,dFecFin:TDate;
begin
     dFecIni := dInicial;
     dFecFin := dFinal;
     with oZetaProvider do
     begin
          if ( ProcesaFechasPlan( dFecIni,dFecFin ) )then
          begin
               with FQryVacaPlan do
               begin
                    Active := FALSE;
                    ParamAsInteger( FQryVacaPlan, 'Empleado', iEmpleado );
                    ParamAsDate( FQryVacaPlan, 'FechaInicial', dFecIni );
                    ParamAsDate( FQryVacaPlan, 'FechaFinal', dFecFin );
                    Active := TRUE;
                    if ( not IsEmpty ) then
                    begin
                         LlenaInfoPlanVacacion( iEmpleado,FQryVacaPlan );
                    end;
               end;
          end;
     end;
end;

function TPlanVacacion.ExistePeriodoPago;
begin
     Result := ( FInicio <> NullDateTime ) and ( FFin <> NullDateTime );
end;

function TPlanVacacion.ProcesaFechasPlan( var dFecIni,dFecFin:TDate):Boolean;
begin
     with oZetaProvider do
     begin
          Result := False;
          if( DatosPeriodo.EsCalendario )then
          begin
               Result := ( FPeriodoAntc <= 0 );
               if( not Result )then
               begin
                    Result := ExistePeriodoPago;
                    if( Result )then
                    begin
                         dFecIni := FInicio;
                         dFecFin := FFin;
                    end;
               end;
          end;
     end;
end;

procedure TPlanVacacion.ProcesarPagoEspecUs( const iEmpleado: Integer );
begin
     with FQryPlanVacUS do
     begin
          Active := FALSE;
          with oZetaProvider do
          begin
               ParamAsInteger( FQryPlanVacUS, 'Empleado', iEmpleado );
          end;
          Active := TRUE;
          if ( not IsEmpty ) then
          begin
               LlenaInfoPlanVacacion( iEmpleado,FQryPlanVacUS );
          end;
     end;
end;

procedure TPlanVacacion.LlenaInfoPlanVacacion( const iEmpleado:Integer; const oZCursor: TZetaCursor );
begin
     with oZCursor do
     begin
          while ( not EOF ) do
          begin
               with FInfoPlanVaca do
               begin
                    Empleado := iEmpleado;
                    Inicio := FieldByName( 'VP_FEC_INI' ).AsDateTime;
                    Regreso := FieldByName( 'VP_FEC_FIN' ).AsDateTime;
                    DiasGozo := FieldByName( 'VP_DIAS' ).AsFloat;
                    if ( FDiasDePago = pvGozo ) then
                       DiasPago := DiasGozo
                    else
                        DiasPago := 0;
                    Status := eStatusPlanVacacion( FieldByName( 'VP_STATUS' ).AsInteger );
               end;
               ProcesaPlanVacacion;
               Next;
          end;
     end;
end;

procedure TPlanVacacion.ProcesaPlanVacacion;
var
   sConflicto: String;
   InfoEmpVaca : TEmpleadoVaca;
begin
     with FInfoPlanVaca do
     begin
          if ( Status = spvPendiente ) then
             IncluirAdvertencia( 'Solicitud de vacaciones pendiente de autorizaci�n',
                                 'D�as Gozados: ' + CampoTexto( DiasGozo, ftFloat ) + CR_LF +
                                 'D�as Pagados: ' + CampoTexto( DiasPago, ftFloat ) + CR_LF )
          else
          begin
               try
                  if HayConflictoExpedientes( sConflicto ) then
                     DataBaseError( sConflicto );

                  InfoEmpVaca := FRecalculoVacaciones.GetEmpVaca( Empleado );

                  AsignaParametrosVacacion( InfoEmpVaca );
                  oZetaProvider.Ejecuta( FInsVacacion );
                  with FRecalculoVacaciones do
                  begin
                       RevisaCierre( InfoEmpVaca, Inicio, K_GLOBAL_SI );
                       RecalculaVaca( Empleado, InfoEmpVaca.FechaAntig );
                  end;
                  AgregaBitacoraVaca;

                  ActualizaPlanVaca;
               except
                     on Error: Exception do
                     begin
                          IncluirError( Error.Message );
                     end;
               end;
          end;
     end;
end;

procedure TPlanVacacion.AsignaParametrosVacacion( const InfoEmpVaca: TEmpleadoVaca );
var
   rMonto, rSeptimoDia, rPrima: TPesos;
   rTasaprima: TTasa;
   iYearsAntig: Integer;
begin
     with oZetaProvider, FInfoPlanVaca do
     begin
          ParamAsInteger( FInsVacacion, 'CB_CODIGO', Empleado );
          ParamAsDate( FInsVacacion, 'VA_FEC_INI', Inicio );
          ParamAsDate( FInsVacacion, 'VA_FEC_FIN', Regreso );
          ParamAsInteger( FInsVacacion, 'VA_TIPO', ord( tvVacaciones ) );
          ParamAsDate( FInsVacacion, 'VA_CAPTURA', Date );
          ParamAsInteger( FInsVacacion, 'US_CODIGO', UsuarioActivo );
          ParamAsChar( FInsVacacion, 'VA_GLOBAL', K_GLOBAL_SI, K_ANCHO_BOOLEANO );
          ParamAsFloat( FInsVacacion, 'VA_GOZO', DiasGozo );
          ParamAsFloat( FInsVacacion, 'VA_PAGO', DiasPago );
          // Per�odo de pago
          with DatosPeriodo do
          begin
               ParamAsInteger( FInsVacacion, 'VA_NOMTIPO', Ord( Tipo ) );
               ParamAsInteger( FInsVacacion, 'VA_NOMNUME', Numero );
               ParamAsInteger( FInsVacacion, 'VA_NOMYEAR', Year );
          end;
          // Datos informativos, Salario y montos
          with InfoEmpVaca do
          begin
               ParamAsFloat( FInsVacacion, 'CB_SALARIO', Salario );
               ParamAsChar( FInsVacacion, 'CB_TABLASS', TablaPrestaciones, K_ANCHO_CODIGO1 );

               iYearsAntig := Trunc( rMax( Years( FechaAntig, Inicio ), 0 )) + 1;
               rMonto := Redondea( FInfoPlanVaca.DiasPago * Salario );

               with FCreator.dmPrestaciones.GetRenglonPresta( TablaPrestaciones, iYearsAntig{$ifdef FLEXIBLES}, Empleado{$endif} ) do
               begin
                    rTasaPrima := ( FieldByName( 'PT_PRIMAVA' ).AsFloat / 100 );
                    // Monto de septimo d�a
                    if ( zStrToBool( FieldByName( 'PT_PAGO_7' ).AsString ) ) then { Se paga el 7o. Dia? }
                       rSeptimoDia := ( rMonto / 6 )
                    else
                       rSeptimoDia := 0;
                    // D�as y monto de Prima
                    if ( not FPagoPrimaAniv ) then                              // Si paga la prima en aniv. no se paga en este registro
                       FInfoPlanVaca.DiasPrima := ( FInfoPlanVaca.DiasPago * rTasaPrima )
                    else
                        FInfoPlanVaca.DiasPrima := 0;
                    rPrima := ( FInfoPlanVaca.DiasPrima * Salario );
                    if ( zStrToBool( FieldByName( 'PT_PRIMA_7' ).AsString ) ) then { Se paga prima del 7o. Dia? }
                       rPrima := Redondea( rPrima * ( 7 / 6 ) );
               end;
          end;
          ParamAsFloat( FInsVacacion, 'VA_TASA_PR', rTasaPrima );
          ParamAsFloat( FInsVacacion, 'VA_MONTO', rMonto );
          ParamAsFloat( FInsVacacion, 'VA_SEVEN', rSeptimoDia );
          ParamAsFloat( FInsVacacion, 'VA_P_PRIMA', FInfoPlanVaca.DiasPrima );
          ParamAsFloat( FInsVacacion, 'VA_PRIMA', rPrima );
          ParamAsFloat( FInsVacacion, 'VA_TOTAL', rMonto + rSeptimoDia + rPrima );
{
          // Estos campos no se requieren
          ParamAsFloat( FInsVacacion, 'VA_OTROS', rOtros );
          ParamAsVarChar( FInsVacacion, 'VA_COMENTA', VACIO, K_ANCHO_TITULO );
          ParamAsVarChar( FInsVacacion, 'VA_PERIODO', VACIO, K_ANCHO_DESCRIPCION );
}
     end;
end;

function TPlanVacacion.HayConflictoExpedientes( var sConflicto : String ) : Boolean;
begin
     with FConflictoVac do
     begin
          Active := FALSE;
          with oZetaProvider do
          begin
               with FInfoPlanVaca do
               begin
                    ParamAsInteger( FConflictoVac, 'Empleado', Empleado );
                    ParamAsDate( FConflictoVac, 'FechaInicio', Inicio );
                    ParamAsDate( FConflictoVac, 'FechaRegreso', Regreso - 1 );
                    ParamAsDate( FConflictoVac, 'FechaOriginal', Inicio );   // Para que no choque con el plan autorizado
               end;
               ParamAsInteger( FConflictoVac, 'Entidad', E_PLAN_VAC );
          end;
          Active := TRUE;
          if ( not IsEmpty ) then
          begin
               Result := ZetaServerTools.EvaluaResultadoConflicto( eStatusConflictoExp( FieldByName( 'RESULTADO' ).AsInteger ),
                                                                   FInfoPlanVaca.Empleado,
                                                                   FieldByName( 'INICIO' ).AsDateTime,
                                                                   FieldByName( 'FIN' ).AsDateTime,
                                                                   sConflicto );
          end
          else
          begin
               Result:= TRUE;
               sConflicto := 'Error al revisar expedientes';
          end;
     end;
end;

procedure TPlanVacacion.AgregaBitacoraVaca;
const
     K_MESS_AGREGA_VACA = 'Agreg� vacaciones del %s al %s';
var
   sMensaje, sNota : String;
begin
     sNota:= VACIO;
     with FInfoPlanVaca do
     begin
          sMensaje := Format( K_MESS_AGREGA_VACA, [ CampoTexto( Inicio, ftDate ), CampoTexto( Regreso - 1, ftDate ) ] );
          sNota := 'D�as Gozados: ' + CampoTexto( DiasGozo, ftFloat ) + CR_LF;
          sNota := sNota + 'D�as Pagados: ' + CampoTexto( DiasPago, ftFloat ) + CR_LF;
          sNota := sNota + 'D�as Prima Vacacional: ' + CampoTexto( DiasPrima, ftFloat ) + CR_LF;
          with oZetaProvider do
          begin
               if ( Log = nil ) then
                  EscribeBitacora( tbNormal, clbVacacion, Empleado, Inicio, sMensaje, sNota )
               else
                   Log.Evento( clbVacacion, Empleado, Inicio, sMensaje, sNota );
          end;
     end;
end;

procedure TPlanVacacion.ActualizaPlanVaca;
begin
     with oZetaProvider do
     begin
          with DatosPeriodo do
          begin
               ParamAsInteger( FUpdVacaPlan, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FUpdVacaPlan, 'Numero', Numero );
               ParamAsInteger( FUpdVacaPlan, 'Year', Year );
          end;
          with FInfoPlanVaca do
          begin
               ParamAsInteger( FUpdVacaPlan, 'Empleado', Empleado );
               ParamAsDate( FUpdVacaPlan, 'FechaInicio', Inicio );
          end;
          Ejecuta( FUpdVacaPlan );
     end;
end;

procedure TPlanVacacion.IncluirAdvertencia( const sMensaje: TBitacoraTexto; const sNota: String );
begin
     with oZetaProvider do
     begin
          if ( Log = nil ) then
             db.DataBaseError( sMensaje )   // Si se invoca fuera de un proceso se levanta la excepci�n
          else
          begin
               with FInfoPlanVaca do
                    Log.Advertencia( clbVacacion, Empleado, Inicio, sMensaje, sNota );
          end;
     end;
end;

procedure TPlanVacacion.IncluirError( const sTexto: String );
const
     K_MESS_ERROR_PLAN_VACA = 'Error al aplicar plan de vacaciones';
begin
     with oZetaProvider do
     begin
          if ( Log = nil ) then
             db.DataBaseError( sTexto )   // Si se invoca fuera de un proceso se levanta la excepci�n
          else
          begin
               with FInfoPlanVaca do
                    Log.Error( Empleado, K_MESS_ERROR_PLAN_VACA, sTexto );
          end;
     end;
end;

{ TPrimaAniversario }

constructor TPrimaAniversario.Create(oCreator: TZetaCreator);
begin
     FCreator := oCreator;
end;

destructor TPrimaAniversario.Destroy;
begin
     Despreparar;
     inherited Destroy;
end;

function TPrimaAniversario.oZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

procedure TPrimaAniversario.Preparar;
begin
     FCreator.PreparaPrestaciones;
     FRecalculoVacaciones := TRecalculaVaca.Create( FCreator );
     with FRecalculoVacaciones do
     begin
          RecalculaVacaBegin;
          RevisaCierreBegin;
     end;
     with oZetaProvider do
     begin
          FQryVacacion := CreateQuery( Format( GetSQLScript( K_QRY_VACA_AUTOMATICA ), [ Ord( tvVacaciones ) ] ) );
          FDelVacacion := CreateQuery( Format( GetSQLScript( K_DEL_VACA_AUTOMATICA ), [ Ord( tvVacaciones ), 0 ] ) );
          FInsVacacion := CreateQuery( GetSQLScript( K_QRY_INSERT_VACA ) );
          FSaldoVacacion := CreateQuery( GetSQLScript( K_SALDO_PRIMA_VACA ) );
          FFaltasLeePrimaV := CreateQuery( GetSQLScript( Q_FALTAS_LEE_PRIMAV  ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FFaltasLeePrimaV, 'Year', Year );
               ParamAsInteger( FFaltasLeePrimaV, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FFaltasLeePrimaV, 'Numero', Numero );
               ParamAsInteger( FFaltasLeePrimaV, 'Motivo', Ord(mfdPrimaVacacional) );
          end;
     end;
end;

procedure TPrimaAniversario.Despreparar;
begin
     FreeAndNil( FSaldoVacacion );
     FreeAndNil( FInsVacacion );
     FreeAndNil( FDelVacacion );
     FreeAndNil( FQryVacacion );
     FreeAndNil( FFaltasLeePrimaV );
     with FRecalculoVacaciones do
     begin
          RecalculaVacaEnd;
          RevisaCierreEnd;
     end;
     if Assigned( FRecalculoVacaciones ) then
        FreeAndNil( FRecalculoVacaciones );
end;

function TPrimaAniversario.ExisteExcepcionPrimaVaca(const iEmpleado:Integer):Boolean;
var
   sMensaje, sNota : String;
begin
     sNota:= VACIO;
     sMensaje := 'No se aplic� pago de prima vac. al aniversario ';
     sNota := 'Existe una excepci�n de pago de prima vacacional en la n�mina '+CR_LF;
     sNota := sNota + Format('se deja el valor de la excepci�n: %s d�as',[ FloatToStr( FDiasExcepcion ) ] );
     with FFaltasLeePrimaV do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FFaltasLeePrimaV, 'Empleado', iEmpleado );
          end;
          Active := True;
          Result := not IsEmpty;
          if Result then
          begin
               FDiasExcepcion := FieldByName('FA_DIAS').AsFloat;
               IncluirAdvertencia(sMensaje,sNota );
          end;
     end;
end;

procedure TPrimaAniversario.Procesar(const iEmpleado: Integer; const dAntig, dAniv: TDate);
var
   lExisteVaca: Boolean;
   sNota: String;
begin
     FEmpleado := iEmpleado;
     FAntiguedad := dAntig;
     FAniversario := dAniv;
     if not ExisteExcepcionPrimaVaca(iEmpleado)then
     begin
          with FQryVacacion do
          begin
               Active := FALSE;
               with oZetaProvider do
               begin
                    ParamAsInteger( FQryVacacion, 'Empleado', iEmpleado );
                    ParamAsDate( FQryVacacion, 'Fecha', dAniv );
               end;
               Active := TRUE;
               lExisteVaca := ( not IsEmpty );
               if ( lExisteVaca ) and ( FieldByName( 'US_CODIGO' ).AsInteger <> 0 ) then  // No se puede procesar la prima vacacional automatica
               begin
                    sNota := 'Se tiene configurado el pago de prima vacacional en aniversario, ' + CR_LF +
                             'pero ya existe un movimiento de vacaciones a la misma fecha.' + CR_LF +
                             'Se dej� el registro existente que tiene los siguientes datos:' + CR_LF;
                    sNota := sNota + 'D�as Gozados: ' + CampoTexto( FieldByName( 'VA_GOZO' ).AsFloat, ftFloat ) + CR_LF;
                    sNota := sNota + 'D�as Pagados: ' + CampoTexto( FieldByName( 'VA_PAGO' ).AsFloat, ftFloat ) + CR_LF;
                    sNota := sNota + 'D�as Prima Vacacional: ' + CampoTexto( FieldByName( 'VA_P_PRIMA' ).AsFloat, ftFloat ) + CR_LF;
                    IncluirAdvertencia( 'Existe un registro de vacaciones en aniversario', sNota );
               end
               else
                   ProcesaPrimaVacacional( lExisteVaca );
          end;
     end;
end;

procedure TPrimaAniversario.ProcesaPrimaVacacional( const lExisteVaca: Boolean );
var
   InfoEmpVaca: TEmpleadoVaca;
   rSaldoPrima, rPropPrima: Currency;
   rDiasTabla, rDiasBase: Currency;

   function GetSaldoPrimaVacacional: Currency;
   begin
        Result := 0;
        with FSaldoVacacion do
        begin
             Active := FALSE;
             oZetaProvider.ParamAsInteger( FSaldoVacacion, 'Empleado', FEmpleado );
             Active := TRUE;
             with InfoEmpVaca do
             begin
                  Empleado := FEmpleado;
                  FechaAntig := FAntiguedad;
                  if ( not IsEmpty ) then
                  begin
                       FechaLastCierre := FieldByName( 'CB_DER_FEC' ).AsDateTime;
                       TablaPrestaciones := FieldByName( 'CB_TABLASS' ).AsString;
                       Salario := FieldByName( 'CB_SALARIO' ).AsFloat;
                       Result := ( FieldByName( 'CB_DER_PV' ).AsFloat - FieldByName( 'CB_V_PRIMA' ).AsFloat );
                  end
                  else
                  begin
                       FechaLastCierre := NullDateTime;
                       TablaPrestaciones := ZetaCommonClasses.VACIO;
                       Salario := 0;
                  end;
             end;
        end;
   end;

begin
     try
        if lExisteVaca then       // Borrar vacaci�n autom�tica, no es necesario notificar en bit�cora, ya que se reemplaza
        begin
             with oZetaProvider do
             begin
                  ParamAsInteger( FDelVacacion, 'Empleado', FEmpleado );
                  ParamAsDate( FDelVacacion, 'Fecha', FAniversario );
                  Ejecuta( FDelVacacion );
             end;
             with FRecalculoVacaciones do
             begin
                  //RevisaCierre( InfoEmpVaca, FAniversario, K_GLOBAL_SI ); No es necesario por que se revisar� al agregar el nuevo registro
                  RecalculaVaca( FEmpleado, FAntiguedad );
             end;
        end;
        rSaldoPrima := GetSaldoPrimaVacacional;   // Obtiene el saldo y llena el record InfoEmpVaca
        with InfoEmpVaca do
        begin
             if ( FAniversario > FechaLastCierre ) and ( FAniversario > FechaAntig ) then
             begin
                  FCreator.dmPrestaciones.VacaProporcional( FechaAntig, FechaLastCierre, FAniversario, TablaPrestaciones,
                                                            rDiasTabla, rDiasBase, rPropPrima{$ifdef FLEXIBLES} ,Empleado {$endif} );
                  rSaldoPrima := ( rSaldoPrima + rPropPrima );
             end;
        end;
        if ( rSaldoPrima > 0 ) then    // Solo si hay prima pendiente de pagar
        begin
             AsignaParametrosVacacion( InfoEmpVaca, rSaldoPrima );
             oZetaProvider.Ejecuta( FInsVacacion );
             with FRecalculoVacaciones do
             begin
                  RevisaCierre( InfoEmpVaca, FAniversario, K_GLOBAL_SI );
                  RecalculaVaca( FEmpleado, FAntiguedad );
             end;
             AgregaBitacoraVaca( lExisteVaca, rSaldoPrima );
        end;
     except
           on Error: Exception do
           begin
                IncluirError( Error.Message );
           end;
     end;
end;

procedure TPrimaAniversario.AsignaParametrosVacacion(const InfoEmpVaca: TEmpleadoVaca; const rDiasPrima: Currency );
var
   rPrima: TPesos;
   rTasaprima: TTasa;
   iYearsAntig: Integer;
begin
     with oZetaProvider do
     begin
          ParamAsInteger( FInsVacacion, 'CB_CODIGO', FEmpleado );
          ParamAsDate( FInsVacacion, 'VA_FEC_INI', FAniversario );
          ParamAsDate( FInsVacacion, 'VA_FEC_FIN', FAniversario );
          ParamAsInteger( FInsVacacion, 'VA_TIPO', ord( tvVacaciones ) );
          ParamAsDate( FInsVacacion, 'VA_CAPTURA', Date );
          ParamAsInteger( FInsVacacion, 'US_CODIGO', 0 );   // Movimiento autom�tico
          ParamAsChar( FInsVacacion, 'VA_GLOBAL', K_GLOBAL_SI, K_ANCHO_BOOLEANO );
          ParamAsFloat( FInsVacacion, 'VA_GOZO', 0 );
          ParamAsFloat( FInsVacacion, 'VA_PAGO', 0 );
          // Per�odo de pago
          with DatosPeriodo do
          begin
               ParamAsInteger( FInsVacacion, 'VA_NOMTIPO', Ord( Tipo ) );
               ParamAsInteger( FInsVacacion, 'VA_NOMNUME', Numero );
               ParamAsInteger( FInsVacacion, 'VA_NOMYEAR', Year );
          end;
          // Datos informativos, Salario y montos
          with InfoEmpVaca do
          begin
               ParamAsFloat( FInsVacacion, 'CB_SALARIO', Salario );
               ParamAsChar( FInsVacacion, 'CB_TABLASS', TablaPrestaciones, K_ANCHO_CODIGO1 );
               rPrima := ( rDiasPrima * Salario );

               iYearsAntig := Trunc( rMax( Years( FechaAntig, FAniversario ), 0 )) + 1;
               with FCreator.dmPrestaciones.GetRenglonPresta( TablaPrestaciones, iYearsAntig{$ifdef FLEXIBLES}, FEmpleado{$endif} ) do
               begin
                    rTasaPrima := ( FieldByName( 'PT_PRIMAVA' ).AsFloat / 100 );
                    if ( zStrToBool( FieldByName( 'PT_PRIMA_7' ).AsString ) ) then { Se paga prima del 7o. Dia? }
                       rPrima := Redondea( rPrima * ( 7 / 6 ) );
               end;
          end;
          ParamAsFloat( FInsVacacion, 'VA_TASA_PR', rTasaPrima );
          ParamAsFloat( FInsVacacion, 'VA_MONTO', 0 );
          ParamAsFloat( FInsVacacion, 'VA_SEVEN', 0 );
          ParamAsFloat( FInsVacacion, 'VA_P_PRIMA', rDiasPrima );
          ParamAsFloat( FInsVacacion, 'VA_PRIMA', rPrima );
          ParamAsFloat( FInsVacacion, 'VA_TOTAL', rPrima );
{
          // Estos campos no se requieren
          ParamAsFloat( FInsVacacion, 'VA_OTROS', rOtros );
          ParamAsVarChar( FInsVacacion, 'VA_COMENTA', VACIO, K_ANCHO_TITULO );
          ParamAsVarChar( FInsVacacion, 'VA_PERIODO', VACIO, K_ANCHO_DESCRIPCION );
}
     end;
end;

procedure TPrimaAniversario.AgregaBitacoraVaca( const lExisteVaca: Boolean; const rDiasPrima: Currency );
const
     aAccion: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Agreg�', 'Recalcul�' );
     K_MESS_AGREGA_VACA = '%s pago de prima vacacional al %s';
var
   sMensaje, sNota : String;
begin
     sNota:= VACIO;
     sMensaje := Format( K_MESS_AGREGA_VACA, [ aAccion[lExisteVaca], FechaCorta( FAniversario ) ] );
     sNota := 'D�as Gozados: ' + CampoTexto( 0, ftFloat ) + CR_LF;
     sNota := sNota + 'D�as Pagados: ' + CampoTexto( 0, ftFloat ) + CR_LF;
     sNota := sNota + 'D�as Prima Vacacional: ' + CampoTexto( rDiasPrima, ftFloat ) + CR_LF;
     with oZetaProvider do
     begin
          if ( Log = nil ) then
             EscribeBitacora( tbNormal, clbVacacion, FEmpleado, FAniversario, sMensaje, sNota )
          else
              Log.Evento( clbVacacion, FEmpleado, FAniversario, sMensaje, sNota );
     end;
end;


procedure TPrimaAniversario.IncluirAdvertencia(const sMensaje: TBitacoraTexto; const sNota: String);
begin
     with oZetaProvider do
     begin
          if ( Log = nil ) then
             db.DataBaseError( sMensaje )   // Si se invoca fuera de un proceso se levanta la excepci�n
          else
          begin
               Log.Advertencia( clbVacacion, FEmpleado, FAniversario, sMensaje, sNota );
          end;
     end;
end;

procedure TPrimaAniversario.IncluirError(const sTexto: String);
const
     K_MESS_ERROR_VACA_ANIV = 'Error al agregar pago de prima vacacional';
begin
     with oZetaProvider do
     begin
          if ( Log = nil ) then
             db.DataBaseError( sTexto )   // Si se invoca fuera de un proceso se levanta la excepci�n
          else
          begin
               Log.Error( FEmpleado, K_MESS_ERROR_VACA_ANIV, sTexto );
          end;
     end;
end;

end.
