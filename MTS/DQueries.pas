unit DQueries;

interface

uses SysUtils, Classes, DB,
     {$ifndef DOTNET}Windows, Messages, Graphics, Controls,Forms, Dialogs,{$endif}
     ZetaCommonLists,
     ZetaCommonClasses,
     DZetaServerProvider;

{$define QUINCENALES}
{.$undefine QUINCENALES}

const
     { Incidencias }
     K_ANCHO_HORA = 4;
     K_ANCHO_RELOJ = 4;
     K_ANCHO_TIPO = 3;
     K_ANCHO_TURNO = 6;
{    K_ANCHO_HORARIO = 6;             // Se pasaron a ZetaCommonClasses
     K_ANCHO_BOOLEANO = 1;
     K_ANCHO_CODIGO1 = 1;
     K_ANCHO_CODIGO2 = 2;
     K_ANCHO_CODIGO = 6;
}
     //K_ANCHO_PATRON = 2;
     K_ANCHO_PATRON = 1;
     K_I_INGRESO = 'IGR';
     K_I_BAJA = 'BAJ';
     K_I_VACACION = 'VAC';
     K_I_FALTA_INJUSTIFICADA = 'FI';
     K_I_FALTA_JUSTIFICADA = 'FJ';
     K_I_FALTA_SS = 'FSS';
     K_I_RETARDO = 'RE';
     { Constante para incidencia de d�as de fondo }
     K_I_FONDO   = 'FON';

type
  TDatosClasifi = record
    Zona_GE: TCodigo;
    Puesto: TCodigo;
    Clasifi: TCodigo;
    Turno: TCodigo;
    Patron: TCodigo;
    Nivel1: TCodigo;
    Nivel2: TCodigo;
    Nivel3: TCodigo;
    Nivel4: TCodigo;
    Nivel5: TCodigo;
    Nivel6: TCodigo;
    Nivel7: TCodigo;
    Nivel8: TCodigo;
    Nivel9: TCodigo;
    {$ifdef ACS}
    Nivel10: TCodigo;
    Nivel11: TCodigo;
    Nivel12: TCodigo;
    {$endif}
  end;
  TDatosEmpleado = record
   oClasifi: TDatosClasifi;
   AUTOSAL : String[1];
   CONTRAT : TCodigo;
   MOT_BAJ : TCodigo;
   SALARIO : TPesos;
   PER_VAR : TPesos;
   TABLASS : TCodigo;      { Hasta aqui son de consulta }
   ACTIVO  : String[1] ;   { De aqui para abajo, recalculo de Kardex }
   FEC_BAJ : TDateTime;
   FEC_BSS : TDateTime;
   NOMYEAR : Integer;
   NOMTIPO : Integer;
   NOMNUME : Integer;
   SAL_INT : TPesos;
   FEC_REV : TDateTime;
   FEC_INT : TDateTime;
   TIP_REV : TCodigo;
   FEC_SAL : TDateTime;
   FEC_CON : TDateTime;
   FEC_ING : TDateTime;
   FEC_ANT : TDateTime;
   OLD_SAL : TPesos;
   OLD_INT : TPesos;
   PRE_INT : TPesos;
   SAL_TOT : TPesos;
   TOT_GRA : TPesos;
   FAC_INT : TTasa;
   RANGO_S : TTasa;
   NumEmp  : TNumEmp;
  end;
  TEmpleadoDatosTurno = record
    Codigo: TCodigo;
    TipoTurno: eTipoTurno;
    Credencial: TCodigo;
    HorarioFestivo: TCodigo;
    StatusHorario: TStatusHorario;
    VacacionHabiles: TDiasHoras;
    VacacionSabados: TDiasHoras;
    VacacionDescansos: TDiasHoras;
    Ritmico: Boolean;
  end;
  TDatosTurno = record
    Codigo: TCodigo;
    TipoTurno: eTipoTurno;
    Dobles: TDiasHoras;
    Domingo: TDiasHoras;
    TipoDia1: eStatusAusencia;
    TipoDia2: eStatusAusencia;
    TipoDia3: eStatusAusencia;
    TipoDia4: eStatusAusencia;
    TipoDia5: eStatusAusencia;
    TipoDia6: eStatusAusencia;
    TipoDia7: eStatusAusencia;
    VacacionHabiles: TDiasHoras;
    VacacionSabados: TDiasHoras;
    VacacionDescansos: TDiasHoras;
    Ritmo: TDatosRitmo;
  end;
  TDatosHorario = record
    Tipo: eTipoHorario;
    Entrada: TCodigo;
    Entra24: Word;
    Salida: TCodigo;
    Sale24: Word;
    GraciaEntradaAntes: Integer;
    UltimaSalidaMadrugada: String;
    ChecadasPares: Boolean;
  end;
  TDatosHorarioDiario = record
    Tipo: eTipoHorario;
    ConHorario: Boolean;
    Jornada: TDiasHoras;
    Dobles: TDiasHoras;
    ExtrasFijas: TDiasHoras;
    MinimoComer: Word;
    ChecaComer: Boolean;
    AgregaComer: Boolean;
    IgnoraComer: Boolean;
    IgnoraCompletamente: Boolean;
    TiempoComer: Word;
    SaleAComer: Word;
    RegresaDeComer: Word;
    ComidaExtras: Word;
    MinComExtras: Word;
    Entrada: Word;
    Entra24: Word;
    GraciaInTemprano: Word;
    GraciaInTarde: Word;
    Salida: Word;
    Sale24: Word;
    GraciaOutTemprano: Word;
    GraciaOutTarde: Word;
    SaleADescanso: Word;
    RegresaDeDescanso: Word;
  end;
  TCommonQueries = class( TObject )
  private
    { Private declarations }
    FProvider: TdmZetaServerProvider;
    FDatosEmpleado: TZetaCursor;
    FDatosHorario: TZetaCursor;
    FDatosHorarioDiario: TZetaCursor;
    FStatusEmpleado: TZetaCursor;
    FStatusActEmpleado: TZetaCursor;
    FFechaCambioTNom: TZetaCursor; 
    FFechaAlta: TZetaCursor;
    FStatusNomina :TZetaCursor;
    FTipoNomEmp :TZetaCursor;
    FStatusLimite:Integer;
    FBloqueoStatus:Boolean;
    property oZetaProvider: TdmZetaServerProvider read FProvider;
  public
    { Public declarations }
    constructor Create( oProvider: TdmZetaServerProvider );
    function GetDatosEmpleado( const iEmpleado: TNumEmp ): TDatosEmpleado;
    function GetDatosHorario( const sHorario: String ): TDatosHorario;
    function GetDatosHorarioDiario( const Codigo: String ): TDatosHorarioDiario;
    function GetStatusEmpleado( const iEmpleado: TNumEmp; const dFecha: TDate ): eStatusEmpleado;
    function GetStatusActEmpleado( const iEmpleado: TNumEmp; const dFecha: TDate ): eStatusEmpleado;
    function GetFechaCambioTNom( const iEmpleado: TNumEmp; dFecha: TDate ):  TDate; 
    function GetFechaAlta( const dFecha: TDate; const iEmpleado: Integer ): TDate; 
    function RevisaStatusBloqueo(const dFecha:TDate;const iEmpleado:Integer;var sTitulo,sDetalle:string ):Boolean;
    procedure GetDatosEmpleadoBegin;
    procedure GetDatosEmpleadoEnd;
    procedure GetDatosHorarioBegin;
    procedure GetDatosHorarioEnd;
    procedure GetDatosHorarioDiarioBegin;
    procedure GetDatosHorarioDiarioEnd;
    procedure GetStatusEmpleadoBegin;
    procedure GetStatusEmpleadoEnd;
    procedure GetStatusActEmpleadoBegin;
    procedure GetStatusActEmpleadoEnd;
    procedure GetFechaCambioTNomBegin; 
    procedure GetFechaCambioTNomEnd; 
    procedure GetFechaAltaBegin; 
    procedure GetFechaAltaEnd;
    procedure RevisaStatusBloqueoBegin;
    procedure RevisaStatusBloqueoEnd;
  end;
  TFestivoIntercambio = class( TObject )
  private
    { Private declarations }
    FDia: Word;
    FMes: Word;
    FOwner: TList;
    FIntercambio: TDate;
  public
    { Public declarations }
    constructor Create( Owner: TList );
    destructor Destroy; override;
    property Mes: Word read FMes write FMes;
    property Dia: Word read FDia write FDia;
    property Intercambio: TDate read FIntercambio write FIntercambio;
  end;
  TTurnoFestivo = class( TObject )
  private
    { Private declarations }
    FOwner: TList;
    FFechas: TList;
    FTurno: TCodigo;
    function GetFechas( Index: Integer ): TFestivoIntercambio;
  public
    { Public declarations }
    constructor Create( Owner: TList );
    destructor Destroy; override;
    property Fechas[ Index: Integer ]: TFestivoIntercambio read GetFechas;
    property Turno: TCodigo read FTurno write FTurno;
    function AgregaFecha( const iMes, iDia: Word; const dIntercambio: TDate ): Integer;
    function Count: Integer;
    function GetIntercambio( const iMes, iDia: Word; const dDefault: TDate ): TDate;
    function IndexOf( const iMes, iDia: Word ): Integer;
    procedure Clear;
  end;
  TListaFestivos = class( TObject )
  private
    { Private declarations }
    FProvider: TdmZetaServerProvider;
    FTurnos: TList;
    FGeneral: Integer;
    FFestivosLee: TZetaCursor;
    function GetTurnos( Index: Integer ): TTurnoFestivo;
    property oZetaProvider: TdmZetaServerProvider read FProvider;
  public
    { Public declarations }
    constructor Create( oProvider: TdmZetaServerProvider );
    destructor Destroy; override;
    property Turnos[ Index: Integer ]: TTurnoFestivo read GetTurnos;
    function AgregaTurno( const sTurno: TCodigo ): Integer;
    function CheckFestivo( const sTurno: String; const dValor: TDate ): Boolean;
    function CheckIntercambio( const sTurno: String; const dValor: TDate ): TDate;
    function Count: Integer;
    function IndexOf( sTurno: TCodigo ): Integer;
    procedure Cargar( const dInicial, dFinal: TDate; const Tipo: eTipoFestivo );
    procedure Clear;
    procedure Despreparar;
    procedure Preparar;
  end;
  TRitmos = class(TObject)
  private
    {Private Declarations}
    FProvider: TdmZetaServerProvider;
    FQueries: TCommonQueries;
    FIntercambios: TListaFestivos;
    FFestivos: TListaFestivos;
    FPrimerDia: Integer;
    FHayTemporal: Boolean;
    FItems: TList;
    FHorarioStatus: TZetaCursor;
    FTurnoLee: TZetaCursor;
    FTarjetaLee: TZetaCursor;
    FDatosTurno: TZetaCursor;
    FChecadasPares: TZetaCursor;
    property oZetaProvider: TdmZetaServerProvider read FProvider;
    function CreaRitmo( const sTurno: TCodigo; const Ritmo: TDatosRitmo ): TRitmo;
    function DeterminaHorario( const sTurno: TCodigo; const Fecha: TDate; Dataset: TZetaCursor ): TStatusHorario;
    function DeterminaStatusHorario(const iEmpleado: TNumEmp; const sTurno: String; const dReferencia: TDate): TStatusHorario;
    function ChecadasImpares( const iEmpleado: TNumEmp; const dFecha: TDate ): Boolean;
    function FindRitmo( const sCodigo: TCodigo ): TRitmo;
    function FindCreateRitmo(const sTurno: TCodigo; const Ritmo: TDatosRitmo ): TRitmo;
    function GetCount: Integer;
    function GetItems( Index: Integer ): TRitmo;
    procedure Clear;
    procedure Delete( const Index: Integer );
    procedure DestruyeFestivos;
    procedure GetStatusHorarioBegin;
    procedure GetStatusHorarioEnd;
  protected
    { Protected declarations }
    property Count: Integer read GetCount;
    property Items[ Index: Integer ]: TRitmo read GetItems;
  public
    {Public Declarations}
    constructor Create( oProvider: TdmZetaServerProvider );
    destructor Destroy; override;
    property Intercambios: TListaFestivos read FIntercambios;
    property Festivos: TListaFestivos read FFestivos;
    property Queries: TCommonQueries read FQueries write FQueries;
    function CreaFestivos(const dInicial, dFinal: TDate; const eTipoLista: eTipoFestivo ): TListaFestivos;
    function GetDatosJornada(const sTurno: TCodigo; const dInicial, dFinal: TDate): TDatosJornada;
    function GetDatosTurno(const sTurno: String): TDatosTurno;
    function GetDiasHabiles(Turno: TDatosTurno; const dInicial, dFinal: TDate; const iPrimerDia: Integer): Integer;
    function GetDiasNoHay(Periodo: TDatosPeriodo; Jornada: TDatosJornada; Turno: TDatosTurno; const iPrimerDia: Integer): Integer;
    function GetEmpleadoDatosTurno( const iEmpleado: TNumEmp; const dFecha: TDate; const lCheckFestivo: Boolean = FALSE ): TEmpleadoDatosTurno;
    function GetStatusHorario( const sTurno: String; const dReferencia: TDate; const lCheckFestivo: Boolean = TRUE ): TStatusHorario;
    procedure DeterminaDia( const iEmpleado: TNumEmp; var dFecha: TDate; var sHora: String; var oDatosTurno: TEmpleadoDatosTurno );
    procedure GetDatosJornadaBegin;
    procedure GetDatosJornadaEnd;
    procedure GetDatosTurnoBegin;
    procedure GetDatosTurnoEnd;
    procedure RitmosBegin( const dInicial, dFinal: TDate );
    procedure RitmosEnd;
    procedure Preparar;
    procedure Despreparar;
    procedure GetEmpleadoDatosTurnoBegin;
    procedure GetEmpleadoDatosTurnoEnd;
  end;

  THorasPrimaDominical = record
   Ordinarias: TDiasHoras;
   Totales: TDiasHoras; //Ordinarias + Extras
  end;

  TPrimaDominical = class(TObject)
  private
    {Private Declarations}
    FProvider: TdmZetaServerProvider;
    FPrimaDominicalCompleta :Boolean;
    FChecadasLee: TZetaCursor;
    property oZetaProvider: TdmZetaServerProvider read FProvider;
  protected
    { Protected declarations }
  public
    {Public Declarations}
    constructor Create( oProvider: TdmZetaServerProvider );
    destructor Destroy; override;
    property PrimaDominicalCompleta:Boolean read FPrimaDominicalCompleta write FPrimaDominicalCompleta;
    function CalcularOrdinariasMadrugada  ( const iEmpleado: TNumEmp; const dValue: TDate; const lAntesMediaNoche: Boolean; const rTotales: TDiasHoras): THorasPrimaDominical;
    function CalcularPrimaDominical( const iEmpleado: TNumEmp; const dValue: TDate; const eTipo: eTipoHorario; const rOrdinarias, rTotales: TDiasHoras; bPrimaDominicalTopada: Boolean = FALSE): THorasPrimaDominical;

  end;



{ Se pasaron a ZetaCommonTools para usarse en los clientes
function GetEntrada24( const eTipo: eTipoHorario; const sEntrada: TCodigo ): TCodigo;
function GetSalida24( const eTipo: eTipoHorario; const sEntrada, sSalida: TCodigo ): TCodigo;
}
function EscribeFechaLimite( const sDescripcion: string; dGlobal, dFecha: TDate; iUsuario: integer; Provider: TdmZetaServerProvider ): boolean;
procedure ValidaLimiteBloqueo( oDatosPeriodo: TDatosPeriodo; Provider: TdmZetaServerProvider; ePeriodoNuevo: eStatusPeriodo );

implementation

uses ZetaCommonTools,
     ZGlobalTress;

const
{     K_OFFSET_VECTOR_RITMO = 32;      Se Pas� a ZetaCommonClasses }
     Q_EMPLEADO_STATUS = 0;
     Q_EMPLEADO_DATOS = 1;
     Q_GET_DATOS_HORARIO = 2;
     Q_GET_DATOS_HORARIO_DIARIO = 3;
     Q_FESTIVOS_LISTA = 4;
     Q_TURNO_GET_DATOS = 5;
     Q_TURNO_GET_STATUS_HORARIO = 6;
     Q_TURNO_LEE_DATOS = 7;
     Q_BUSCA_TEMPORALES = 8;
     Q_TARJETA_LEE = 9;
     Q_EMPLEADO_STATUS_ACT = 10;
     Q_UPDATE_FECHA_LIMITE = 11;
     Q_INSERT_FECHA_LIMITE = 12;
     Q_QTY_CHECADAS_IN_OUT = 13;
     Q_FECHA_CAMBIO_TIP_NOM = 14; 
     Q_FECHA_ALTA      = 15; 
     Q_TIPO_NOM_EMP    = 16;
     Q_STATUS_X_BLOQUEO = 17;
     Q_CHECADAS_LEE = 18;

function NumeroToStr( const iValor, iLength: Integer ): String;
begin
     Result := Format( '%' + Format( '%d.%d', [ iLength, iLength ] ) + 'd', [ iValor ] );
end;

function ConvierteFestivo( const sTurno: String; const iMes, iDia: Word ): String;
begin
     Result := sTurno + NumeroToStr( iMes, 2 ) + NumeroToStr( iDia, 2 );
end;

function DiaSemanaPos( const dFecha: TDate; const iPrimerDiaSemana: Integer ): Integer;
begin
     Result := ( ( DayOfWeek( dFecha ) + 7 - iPrimerDiaSemana ) mod 7 ) + 1;
end;

{ Se pasaron a ZetaCommonTools para usarse en los clientes
function GetEntrada24( const eTipo: eTipoHorario; const sEntrada: TCodigo ): TCodigo;
begin
     Result := sEntrada;
     if ( eTipo = thMediaNoche ) then
        Result := aHoraString( aMinutos( Result ) + K_24HORAS );
end;

function GetSalida24( const eTipo: eTipoHorario; const sEntrada, sSalida: TCodigo ): TCodigo;
begin
     Result := sSalida;
     if ( eTipo = thMediaNoche ) or ( sSalida < sEntrada ) then
        Result := aHoraString( aMinutos( Result ) + K_24HORAS );
end;
}

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          {$ifdef INTERBASE}
          Q_EMPLEADO_STATUS: Result := 'select RESULTADO from SP_STATUS_EMP( :Fecha, :Empleado )';
          Q_EMPLEADO_STATUS_ACT: Result := 'select RESULTADO from SP_STATUS_ACT( :Fecha, :Empleado )';
          {$endif}
          {$ifdef MSSQL}
          Q_EMPLEADO_STATUS: Result := 'select DBO.SP_STATUS_EMP( :Fecha, :Empleado ) AS RESULTADO';
          Q_EMPLEADO_STATUS_ACT: Result := 'select dbo.SP_STATUS_ACT( :Fecha, :Empleado ) as RESULTADO';
          {$endif}
          Q_EMPLEADO_DATOS: Result := 'select CB_AUTOSAL, CB_CLASIFI, CB_CONTRAT, CB_TURNO, CB_MOT_BAJ, '+
                                      'CB_PATRON, CB_PUESTO, CB_SALARIO, CB_TABLASS, CB_ZONA_GE, '+
                                      'CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, '+
                                      'CB_NIVEL4,CB_NIVEL5, CB_NIVEL6, '+
                                      'CB_NIVEL7, CB_NIVEL8, CB_NIVEL9, '+
                                      {$ifdef ACS}'CB_NIVEL10, CB_NIVEL11, CB_NIVEL12, '+{$endif}                                      
                                      'CB_FEC_ANT, CB_FEC_SAL, CB_TIP_REV, CB_FEC_ING, '+
                                      'CB_OLD_SAL , CB_OLD_INT, CB_PRE_INT, CB_PER_VAR, '+
                                      'CB_SAL_TOT,CB_TOT_GRA, CB_FAC_INT, CB_RANGO_S, CB_FEC_BAJ '+
                                      'from COLABORA where ( CB_CODIGO = :Empleado )';
          Q_GET_DATOS_HORARIO: Result := 'select HO_TIPO, HO_INTIME, HO_IN_TEMP, HO_LASTOUT, HO_OUTTIME, HO_PARES '+
                                         'from HORARIO where ( HO_CODIGO = :Codigo )';
          Q_GET_DATOS_HORARIO_DIARIO: Result := 'select HO_TIPO, HO_JORNADA, HO_DOBLES, HO_MIN_EAT,'+
                                                'HO_CHK_EAT, HO_ADD_EAT, HO_IGN_EAT, HO_COMER, HO_EXT_FIJ, HO_IN_TEMP,'+
                                                'HO_IN_TARD, HO_OU_TEMP, HO_OU_TARD, HO_OUT_EAT, HO_IN_EAT, HO_EXT_COM, HO_EXT_MIN,'+
                                                'HO_INTIME, HO_OUTTIME, HO_OUT_BRK, HO_IN_BRK '+
                                                'from HORARIO where ( HO_CODIGO = :Codigo )';
          Q_FESTIVOS_LISTA: Result := 'select TU_CODIGO, FE_MES, FE_DIA, FE_CAMBIO, FE_YEAR from FESTIVO where '+
                                      '( FE_TIPO = :Tipo and FE_YEAR in( 0, :YearIni, :YearFin ) ) order by TU_CODIGO, FE_MES, FE_DIA';
          Q_TURNO_GET_DATOS: Result := 'select TU_RIT_PAT, '+
                                       'TU_HOR_1, TU_HOR_2, TU_HOR_3, TU_HOR_4, TU_HOR_5, TU_HOR_6, TU_HOR_7, '+
                                       'TU_TIP_1, TU_TIP_2, TU_TIP_3, TU_TIP_4, TU_TIP_5, TU_TIP_6, TU_TIP_7, '+
                                       'TU_CODIGO, TU_RIT_INI, TU_HORARIO, TU_HOR_FES, TU_VACA_HA, TU_VACA_SA, TU_VACA_DE, '+
                                       {$ifdef INTERBASE}
                                       'COLABORA.CB_CREDENC '+
                                       'from TURNO, SP_FECHA_KARDEX( :Fecha, :Empleado ) FK, COLABORA where '+
                                       '( TURNO.TU_CODIGO = FK.CB_TURNO ) and ( COLABORA.CB_CODIGO = :Employee )';
                                       {$endif}
                                       {$ifdef MSSQL}
                                       'CB_CREDENC from DBO.TURNO_EMPLEADO( :Fecha, :Empleado )';
                                       {$endif}
          Q_TURNO_GET_STATUS_HORARIO: Result := 'select TU_RIT_PAT, '+
                                                'TU_HOR_1, TU_HOR_2, TU_HOR_3, TU_HOR_4, TU_HOR_5, TU_HOR_6, TU_HOR_7, '+
                                                'TU_TIP_1, TU_TIP_2, TU_TIP_3, TU_TIP_4, TU_TIP_5, TU_TIP_6, TU_TIP_7, '+
                                                'TU_RIT_INI, TU_DIAS, TU_JORNADA, TU_DOBLES, TU_DOMINGO, TU_TIP_JOR, ' +
                                                'TU_HORARIO, TU_HOR_FES ' +
{$ifdef TJORNADA_FESTIVOS}
                                                ', TU_TIP_JT '+
{$endif}
{$ifdef QUINCENALES}
                                                ', TU_DIAS_BA ' +
{$endif}
                                                'from TURNO where ( TU_CODIGO = :Codigo )';
          Q_TURNO_LEE_DATOS: Result := 'select TU_CODIGO, TU_JORNADA, TU_DIAS, TU_HORARIO, TU_DOBLES, TU_DOMINGO, '+
                                       'TU_TIP_1, TU_TIP_2, TU_TIP_3, TU_TIP_4, TU_TIP_5, TU_TIP_6, TU_TIP_7, '+
                                       'TU_RIT_INI, TU_RIT_PAT, TU_HOR_1, TU_HOR_2, TU_HOR_3, TU_VACA_HA, TU_VACA_SA, TU_VACA_DE '+
                                       'from TURNO where ( TU_CODIGO = :Codigo )';
          Q_BUSCA_TEMPORALES: Result := 'select COUNT(*) CUANTOS from AUSENCIA where '+
                                        '( AU_HOR_MAN = ''%s'' ) and '+
                                        '( AU_FECHA >= %s ) and '+
                                        '( AU_FECHA <= %s )';
          Q_TARJETA_LEE: Result := 'select HO_CODIGO, AU_STATUS from AUSENCIA where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha )';
          Q_UPDATE_FECHA_LIMITE: Result := 'update GLOBAL set GL_DESCRIP = %s, GL_FORMULA = %s, '+
                                           'US_CODIGO = %d, GL_CAPTURA = %s '+
                                           'where ( GL_CODIGO = %d )';
          Q_INSERT_FECHA_LIMITE: Result := 'insert into GLOBAL (GL_CODIGO, GL_DESCRIP, GL_FORMULA, US_CODIGO, GL_CAPTURA) '+
                                           'values (%d, %s, %s, %d, %s)';
          Q_QTY_CHECADAS_IN_OUT: Result := 'select COUNT(*) CUANTOS from CHECADAS where '+
                                           '( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha ) and ' +
                                           Format( '( CH_TIPO in ( %d, %d ) ) and ', [  Ord( chEntrada ), Ord( chSalida ) ] ) +
                                           '( CH_SISTEMA <> ' + ZetaCommonTools.EntreComillas( K_GLOBAL_SI ) + ' )';
          Q_FECHA_CAMBIO_TIP_NOM: Result := 'select ' +
          {$ifdef MSSQL} 
                                           ' TOP 1 ' + 
          {$endif} 
                                           'CB_NOMINA,CB_FECHA, CB_TIPO from KARDEX WHERE ( CB_CODIGO = :Empleado ) and '+ 
                                            '( CB_FECHA <= :Fecha ) AND ( CB_TIPO = ''TIPNOM'' OR CB_TIPO = ''ALTA'') order by CB_FECHA desc'; 
 
          Q_FECHA_ALTA: Result:=  'select ' + 
 
          {$ifdef MSSQL} 
                                       'dbo.SP_KARDEX_CB_FEC_ING( :Fecha, :Empleado ) CB_FEC_ING '; 
 
          {$else} 
                                       'CB_FEC_ING from SP_FECHA_KARDEX( :Fecha, :Empleado )'; 
          {$endif} 

          Q_TIPO_NOM_EMP: Result := 'select '+
          {$ifdef MSSQL}
                                        ' dbo.SP_KARDEX_CB_NOMINA( :Fecha , :Empleado ) CB_NOMINA ';
          {$else}
                                        ' CB_NOMINA from SP_FECHA_KARDEX ( :Fecha,:Empleado ) ';
          {$endif}
          Q_STATUS_X_BLOQUEO: Result := 'Select PE_STATUS from PERIODO where PE_TIPO = :TipoPeriodo and :Fecha between PE_FEC_INI and PE_FEC_FIN ';

          Q_CHECADAS_LEE: Result := 'select CH_TIPO, CH_DESCRIP, CH_H_AJUS from CHECADAS where '+
                                    '( CB_CODIGO = :Empleado ) and '+
                                    '( AU_FECHA = :Fecha ) and '+
                                    '( CH_TIPO in ( %d, %d, %d, %d ) ) and '+ { chEntrada,chSalida,chInicio,chFin }
                                    '( CH_DESCRIP in ( %d, %d, %d, %d ) ) '+      { dcOrdinaria,dcRetardo,dcPuntual, dcExtras }
                                    ' order by CH_H_AJUS';

     end;
end;

function EscribeFechaLimite( const sDescripcion: string; dGlobal, dFecha: TDate; iUsuario: integer; Provider: TdmZetaServerProvider ): boolean;
var
   FUpdate, FInsert: TZetaCursor;
begin
     Result := FALSE;
     with Provider do
     begin
          FUpdate := CreateQuery( Format( GetSQLScript( Q_UPDATE_FECHA_LIMITE ), [ Entrecomillas( Copy( sDescripcion, 1, K_ANCHO_DESCRIPCION ) ),
                                                                                   FechaToStr( dGlobal ),
                                                                                   iUsuario,
                                                                                   DateToStrSQLC( dFecha ),
                                                                                   ZGlobalTress.K_GLOBAL_FECHA_LIMITE ] ) );
          try
             EmpiezaTransaccion;
             try
                Result := ( Ejecuta( FUpdate ) > 0 );
                if not Result then
                begin
                     FInsert := CreateQuery( Format( GetSQLScript( Q_INSERT_FECHA_LIMITE ), [ ZGlobalTress.K_GLOBAL_FECHA_LIMITE,
                                                                                              Entrecomillas( Copy( sDescripcion, 1, K_ANCHO_DESCRIPCION ) ),
                                                                                              FechaToStr( dGlobal ),
                                                                                              iUsuario,
                                                                                              DateToStrSQLC( dFecha ) ] ) );
                     try
                        try
                           Result := ( Ejecuta( FInsert ) > 0 );
                        except
                              on Error: Exception do
                              begin
                                   TerminaTransaccion( FALSE );
                                   raise;
                              end;
                        end;
                     finally
                            FreeAndNil( FInsert );
                     end;
                end;
                TerminaTransaccion( TRUE );
             except
                   on Error: Exception do
                   begin
                        TerminaTransaccion( FALSE );
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FUpdate );
          end;
     end;
end;

procedure ValidaLimiteBloqueo( oDatosPeriodo: TDatosPeriodo; Provider: TdmZetaServerProvider; ePeriodoNuevo: eStatusPeriodo );
begin
     Provider.InitGlobales;
     with oDatosPeriodo do
     begin
          //if ( Numero < ZetaCommonClasses.K_LIMITE_NOM_NORMAL ) and   //Nomina Ordinaria
          if ( Numero < Provider.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) ) and   //Nomina Ordinaria
             ( ePeriodoNuevo > eStatusPeriodo( Provider.GetGlobalInteger( K_LIMITE_MODIFICAR_ASISTENCIA ) ) ) and//( Status >= eStatusPeriodo( Provider.GetGlobalInteger( K_LIMITE_MODIFICAR_ASISTENCIA ) ) ) and
             ( Tipo = etipoPeriodo( Provider.GetGlobalInteger( K_GLOBAL_TIPO_NOMINA ) ) ) and
             {$ifdef QUINCENALES}
             ( FinAsis > Provider.GetGlobalDate( K_GLOBAL_FECHA_LIMITE ) ) then
             {$else}
             ( Fin > Provider.GetGlobalDate( K_GLOBAL_FECHA_LIMITE ) ) then
             {$endif}
          begin
               {$ifdef QUINCENALES}
               EscribeFechaLimite( ZetaCommonTools.ShowNomina( Year, Ord( Tipo ), Numero ),
                                   FinAsis,
                                   Date,
                                   0,   //Se manda Usuario = 0 cuando es la Actualizaci�n Autom�tica
                                   Provider );
               {$else}
               EscribeFechaLimite( ZetaCommonTools.ShowNomina( Year, Ord( Tipo ), Numero ),
                                   Fin,
                                   Date,
                                   0,   //Se manda Usuario = 0 cuando es la Actualizaci�n Autom�tica
                                   Provider );
               {$endif}
          end;
     end;
end;

{ ****** TFestivoIntercambio ********** }

constructor TFestivoIntercambio.Create( Owner: TList );
begin
     {$ifdef DOTNET}
     inherited Create;
     {$endif}
     FOwner := Owner;
     FDia := 0;
     FMes := 0;
     FIntercambio := NullDateTime;
     FOwner.Add( Self );
end;

destructor TFestivoIntercambio.Destroy;
begin
     FOwner.Remove( Self );
     inherited Destroy;
end;

{ ***** TTurnoFestivo ******* }

constructor TTurnoFestivo.Create( Owner: TList );
begin
     {$ifdef DOTNET}
     inherited Create;
     {$endif}
     FFechas := TList.Create;
     FOwner := Owner;
     FOwner.Add( Self );
end;

destructor TTurnoFestivo.Destroy;
begin
     Clear;
     FFechas.Free;
     FOwner.Remove( Self );
     inherited Destroy;
end;

function TTurnoFestivo.GetFechas( Index: Integer ): TFestivoIntercambio;
begin
     Result := TFestivoIntercambio( FFechas.Items[ Index ] );
end;

function TTurnoFestivo.Count: Integer;
begin
     Result := FFechas.Count;
end;

procedure TTurnoFestivo.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Self.Fechas[ i ].Free;
     end;
end;

function TTurnoFestivo.IndexOf( const iMes, iDia: Word ): Integer;
var
   i: Integer;
begin
     Result := -1;
     for i := 0 to ( Count - 1 ) do
     begin
          with Self.Fechas[ i ] do
          begin
               if ( Mes = iMes ) and ( Dia = iDia ) then
               begin
                    Result := i;
                    Break;
               end;
          end;
     end;
end;

function TTurnoFestivo.AgregaFecha( const iMes, iDia: Word; const dIntercambio: TDate ): Integer;
begin
     Result := IndexOf( iMes, iDia );
     if ( Result < 0 ) then
     begin
          with TFestivoIntercambio.Create( FFechas ) do
          begin
               Mes := iMes;
               Dia := iDia;
               Intercambio := dIntercambio;
          end;
          Result := Count - 1;
     end;
end;

function TTurnoFestivo.GetIntercambio( const iMes, iDia: Word; const dDefault: TDate ): TDate;
var
   iPtr: Integer;
begin
     iPtr := IndexOf( iMes, iDia );
     if ( iPtr < 0 ) then
        Result := dDefault
     else
         Result := Fechas[ iPtr ].Intercambio;
end;

{ ****** TListaFestivos ********** }

constructor TListaFestivos.Create( oProvider: TdmZetaServerProvider );
begin
     {$ifdef DOTNET}
     inherited Create;
     {$endif}
     FProvider := oProvider;
     FTurnos := TList.Create;
     FGeneral := -1;
end;

destructor TListaFestivos.Destroy;
begin
     Clear;
     FTurnos.Free;
     Despreparar;
     inherited Destroy;
end;

function TListaFestivos.Count: Integer;
begin
     Result := FTurnos.Count;
end;

function TListaFestivos.GetTurnos( Index: Integer ): TTurnoFestivo;
begin
     Result := TTurnoFestivo( FTurnos.Items[ Index ] );
end;

procedure TListaFestivos.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Turnos[ i ].Free;
     end;
     FGeneral := -1;
end;

function TListaFestivos.IndexOf( sTurno: TCodigo ): Integer;
var
   i: Integer;
begin
     Result := -1;
     sTurno := Trim( sTurno );
     for i := 0 to ( Count - 1 ) do
     begin
          with Self.Turnos[ i ] do
          begin
               if ( Turno = sTurno ) then
               begin
                    Result := i;
                    Break;
               end;
          end;
     end;
end;

function TListaFestivos.AgregaTurno( const sTurno: TCodigo ): Integer;
begin
     Result := IndexOf( sTurno );
     if ( Result < 0 ) then
     begin
          with TTurnoFestivo.Create( FTurnos ) do
          begin
               Turno := sTurno;
          end;
          Result := Count - 1;
          if ( sTurno = TURNO_VACIO ) then
             FGeneral := Result;
     end;
end;

procedure TListaFestivos.Preparar;
begin
     with oZetaProvider do
     begin
          FFestivosLee := CreateQuery( GetSQLScript( Q_FESTIVOS_LISTA ) );
     end;
end;

procedure TListaFestivos.Despreparar;
begin
     FreeAndNil( FFestivosLee );
end;

procedure TListaFestivos.Cargar( const dInicial, dFinal: TDate; const Tipo: eTipoFestivo );
var
   iTurno: Integer;
   iDay, iMesIni, iMesFin, iYearIni, iYearFin: Word;
   function EsRangoFechasValida: Boolean;
   begin
        with FFestivosLee do
        begin
             { Si las fechas son de a�os distintos }
             if ( iYearIni <> iYearFin ) then
             begin
                  if ( FieldByName('FE_YEAR').AsInteger <> 0 ) then  { Cuando es vigencia especifica } 
                  begin
                  Result:= ( EncodeDate( FieldByName( 'FE_YEAR' ).AsInteger, FieldByName( 'FE_MES' ).AsInteger, FieldByName( 'FE_DIA' ).AsInteger ) >= FirstDayOfMonth(dInicial) )
                            and ( EncodeDate( FieldByName( 'FE_YEAR' ).AsInteger, FieldByName( 'FE_MES' ).AsInteger, FieldByName( 'FE_DIA' ).AsInteger ) <= LastDayOfMonth(dFinal) );
                  end
                  else { Cuando es vigencia Fija } 
                  begin
                      iMesIni := 1;
                      iMesFin := 12;
                      Result := ( FieldByName( 'FE_MES' ).AsInteger >= iMesIni ) and  ( FieldByName( 'FE_MES' ).AsInteger <= iMesFin );
                 end;
             end
             else { Fechas a�os iguales sin importar Vigencia } 
                 Result := ( FieldByName( 'FE_MES' ).AsInteger >= iMesIni ) and  ( FieldByName( 'FE_MES' ).AsInteger <= iMesFin );
        end;
   end;

begin
     Clear;
     DecodeDate( dInicial, iYearIni, iMesIni, iDay );
     DecodeDate( dFinal, iYearFin, iMesFin, iDay );

     with FFestivosLee do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FFestivosLee, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FFestivosLee, 'YearIni', iYearIni );
               ParamAsInteger( FFestivosLee, 'YearFin', iYearFin );
          end;

          Active := True;
          while not Eof do
          begin
               { Se agregan todos los Turnos que aparecen en tabla FESTIVO }
               iTurno := AgregaTurno( FieldByName( 'TU_CODIGO' ).AsString );

               if EsRangoFechasValida then  
               begin
                    { Se agregan �nicamente las fechas que caen dentro del filtro }
                    with Turnos[ iTurno ] do
                    begin
                         AgregaFecha( FieldByName( 'FE_MES' ).AsInteger,
                                      FieldByName( 'FE_DIA' ).AsInteger,
                                      FieldByName( 'FE_CAMBIO' ).AsDateTime );
                    end;
               end;
               Next;
          end;
          Active := False;
     end;
end;

function TListaFestivos.CheckFestivo( const sTurno: String; const dValor: TDate ): Boolean;
var
   iTurno: Integer;
   iDia, iMes, iYear: Word;
begin
     if ( Count > 0 ) then
     begin
          DecodeDate( dValor, iYear, iMes, iDia );
          iTurno := IndexOf( sTurno );
          if ( iTurno < 0 ) then
          begin
               if ( FGeneral < 0 ) then
                  Result := False
               else
                   Result := ( Turnos[ FGeneral ].IndexOf( iMes, iDia ) >= 0 );
          end
          else
              Result := ( Turnos[ iTurno ].IndexOf( iMes, iDia ) >= 0 );
     end
     else
         Result := False;
end;

function TListaFestivos.CheckIntercambio( const sTurno: String; const dValor: TDate ): TDate;
var
   iTurno: Integer;
   iDia, iMes, iYear: Word;
begin
     if ( Count > 0 ) then
     begin
          DecodeDate( dValor, iYear, iMes, iDia );
          iTurno := IndexOf( sTurno );
          if ( iTurno < 0 ) then
          begin
               if ( FGeneral < 0 ) then
                  Result := dValor
               else
                   Result := Turnos[ FGeneral ].GetIntercambio( iMes, iDia, dValor );
          end
          else
              Result := Turnos[ iTurno ].GetIntercambio( iMes, iDia, dValor );
     end
     else
         Result := dValor;
end;

{ ************ TRitmos ********* }

constructor TRitmos.Create( oProvider: TdmZetaServerProvider );
begin
     {$ifdef DOTNET}
     inherited Create;
     {$endif}
     FProvider := oProvider;
     FItems := TList.Create;
     FHayTemporal := False;
end;

destructor TRitmos.Destroy;
begin
     if Assigned( FItems ) then
        Clear;
     FItems.Free;
     DestruyeFestivos;
     inherited Destroy;
end;

procedure TRitmos.Delete( const Index: Integer );
begin
     Items[ Index ].Free;
     FItems.Delete( Index );
end;

procedure TRitmos.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Delete( i );
     end;
end;

function TRitmos.GetCount: Integer;
begin
     Result := FItems.Count;
end;

function TRitmos.GetItems( Index: Integer ): TRitmo;
begin
     Result := TRitmo( FItems[ Index ] );
end;

function TRitmos.FindRitmo( const sCodigo: TCodigo ): TRitmo;
var
   i, iPos: Integer;
begin
     iPos := -1;
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Items[ i ].Codigo = sCodigo ) then
          begin
               iPos := i;
               Break;
          end;
     end;
     if ( iPos >= 0 ) then
        Result := Items[ iPos ]
     else
         Result := nil;
end;

function TRitmos.CreaRitmo( const sTurno: TCodigo; const Ritmo: TDatosRitmo ): TRitmo;
begin
     Result := TRitmo.Create( sTurno );
     with Ritmo do
     begin
          Result.SetPatron( Inicio, Patron, Horario1, Horario2, Horario3 );
     end;
     FItems.Add( Result );
end;

function TRitmos.FindCreateRitmo( const sTurno: TCodigo; const Ritmo: TDatosRitmo ): TRitmo;
begin
     Result := FindRitmo( sTurno );
     if not Assigned( Result ) then
        Result := CreaRitmo( sTurno, Ritmo );
end;

procedure TRitmos.Preparar;
begin
     Clear;
     with oZetaProvider do
     begin
          FPrimerDia := GetGlobalInteger( K_GLOBAL_PRIMER_DIA );
     end;
     GetStatusHorarioBegin;
     GetEmpleadoDatosTurnoBegin;
     with Queries do
     begin
          GetDatosHorarioBegin;
     end;
end;

procedure TRitmos.Despreparar;
begin
     with Queries do
     begin
          GetDatosHorarioEnd;
     end;
     GetEmpleadoDatosTurnoEnd;
     GetStatusHorarioEnd;
end;

procedure TRitmos.GetStatusHorarioBegin;
begin
     Clear;
     with oZetaProvider do
     begin
          FHorarioStatus := CreateQuery( GetSQLScript( Q_TURNO_GET_STATUS_HORARIO ) );
     end;
end;

procedure TRitmos.GetStatusHorarioEnd;
begin
     FreeAndNil( FHorarioStatus );
end;

function TRitmos.GetStatusHorario( const sTurno: String; const dReferencia: TDate; const lCheckFestivo: Boolean = TRUE ): TStatusHorario;
var
   sHorarioFest : String;
begin
     with FHorarioStatus do
     begin
          Active := False;
          oZetaProvider.ParamAsChar( FHorarioStatus, 'Codigo', sTurno, K_ANCHO_TURNO );
          Active := True;
          Result := DeterminaHorario( sTurno, dReferencia, FHorarioStatus );
          if lCheckFestivo and Festivos.CheckFestivo( sTurno, dReferencia ) then   // Si se pide checar festivos y el d�a es un festivo se procede a asignar el Horario Festivo asignado para el Turno
          begin
               sHorarioFest := FHorarioStatus.FieldByName( 'TU_HOR_FES' ).AsString;
               if strLleno( sHorarioFest ) then                                    // Solo si est� definido
                  Result.Horario := sHorarioFest;
          end;
          Active := False;
     end;
end;

procedure TRitmos.GetEmpleadoDatosTurnoBegin;
begin
     Clear;
     with oZetaProvider do
     begin
          FTurnoLee := CreateQuery( GetSQLScript( Q_TURNO_GET_DATOS ) );
     end;
end;

procedure TRitmos.GetEmpleadoDatosTurnoEnd;
begin
     FreeAndNil( FTurnoLee );
end;

function TRitmos.GetEmpleadoDatosTurno( const iEmpleado: TNumEmp; const dFecha: TDate; const lCheckFestivo: Boolean = FALSE ): TEmpleadoDatosTurno;
begin
     with FTurnoLee do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsDate( FTurnoLee, 'Fecha', dFecha );
               ParamAsInteger( FTurnoLee, 'Empleado', iEmpleado );
               {$ifdef INTERBASE}
               ParamAsInteger( FTurnoLee, 'Employee', iEmpleado );
               {$endif}
          end;
          Active := True;
          with Result do
          begin
               if Eof then
               begin
                    Codigo := VACIO;
                    TipoTurno := eTipoTurno( 0 );
                    Credencial := VACIO;
                    HorarioFestivo := VACIO;
                    VacacionHabiles := K_DIAS_VACACION_DEF;
                    VacacionSabados := 0;
                    VacacionDescansos := 0;
                    Ritmico := FALSE;
               end
               else
               begin
                    Codigo := FieldByName( 'TU_CODIGO' ).AsString;
                    TipoTurno := eTipoTurno( FieldByName( 'TU_HORARIO' ).AsInteger );
                    Credencial := FieldByName( 'CB_CREDENC' ).AsString;
                    HorarioFestivo := FieldByName( 'TU_HOR_FES' ).AsString;
                    VacacionHabiles := FieldByName( 'TU_VACA_HA' ).AsCurrency;
                    VacacionSabados := FieldByName( 'TU_VACA_SA' ).AsCurrency;
                    VacacionDescansos := FieldByName( 'TU_VACA_DE' ).AsCurrency;
                    Ritmico := StrLleno( FieldByName( 'TU_RIT_PAT' ).AsString );
               end;
               StatusHorario := DeterminaHorario( Codigo, dFecha, FTurnoLee );
               {
                 Si se pide checar festivos y Existe Horario Festivo asignado al Turno y el d�a es un festivo
                 se procede a asignar el Horario Festivo asignado para el Turno
               }
               if lCheckFestivo and strLleno( HorarioFestivo ) and Festivos.CheckFestivo( Codigo, dFecha ) then
               begin
                    StatusHorario.Horario := HorarioFestivo;
               end;
          end;
          Active := False;
     end;
end;

function TRitmos.DeterminaHorario( const sTurno: TCodigo; const Fecha: TDate; Dataset: TZetaCursor ): TStatusHorario;
var
   iPos: Integer;
   DatosRitmo: TDatosRitmo;
begin
     with Dataset do
     begin
          if Eof then
          begin
               with Result do
               begin
                    Horario := '';
                    Status := eStatusAusencia( 0 );
               end;
          end
          else
          begin
               if StrVacio( FieldByName( 'TU_RIT_PAT' ).AsString ) then
               begin
                    iPos := DiaSemanaPos( Fecha, FPrimerDia );
                    { � La posici�n de los TFields es VITAL ! }
                    with Result do
                    begin
                         Horario := Fields[ iPos ].AsString;
                         Status := eStatusAusencia( Fields[ iPos + 7 ].AsInteger );
                    end;
               end
               else
               begin
                    with Dataset do
                    begin
                         with DatosRitmo do
                         begin
                              Inicio := FieldByName( 'TU_RIT_INI' ).AsDateTime;
                              Patron := FieldByName( 'TU_RIT_PAT' ).AsString;
                              Horario1 := FieldByName( 'TU_HOR_1' ).AsString;
                              Horario2 := FieldByName( 'TU_HOR_2' ).AsString;
                              Horario3 := FieldByName( 'TU_HOR_3' ).AsString;
                         end;
                    end;
                    Result := FindCreateRitmo( sTurno, DatosRitmo ).GetStatusHorario( Fecha );
               end;
          end;
     end;
end;

function TRitmos.DeterminaStatusHorario( const iEmpleado: TNumEmp; const sTurno: String; const dReferencia: TDate ): TStatusHorario;
begin
     { Solo se manda llamar de Poll, el cual puede intercambiar fechas y no pide investigar el horario con chequeo de Festivos }
     if FHayTemporal then
     begin
          with FTarjetaLee do
          begin
               Active := False;
               with oZetaProvider do
               begin
                    ParamAsInteger( FTarjetaLee, 'Empleado', iEmpleado );
                    ParamAsDate( FTarjetaLee, 'Fecha', dReferencia );
               end;
               Active := True;
               if Eof then
                  Result := GetStatusHorario( sTurno, dReferencia, FALSE )         // No regresar Horario Festivo para Fecha ( El Poll puede intercambiar la Fecha )
               else
               begin
                    with Result do
                    begin
                         Horario := FieldByName( 'HO_CODIGO' ).AsString;
                         Status := eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger );
                    end;
               end;
               Active := False;
          end;
     end
     else
         Result := GetStatusHorario( sTurno, dReferencia, FALSE );                 // No regresar Horario Festivo para Fecha ( El Poll puede intercambiar la Fecha )
end;

function TRitmos.ChecadasImpares( const iEmpleado: TNumEmp; const dFecha: TDate ): Boolean;
begin
     with FChecadasPares do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FChecadasPares, 'Empleado', iEmpleado );
               ParamAsDate( FChecadasPares, 'Fecha', dFecha );
          end;
          Active := True;
          Result := ( ( FieldByName( 'CUANTOS' ).AsInteger mod 2 ) > 0 );
          Active := False;
     end;
end;

procedure TRitmos.DeterminaDia( const iEmpleado: TNumEmp; var dFecha: TDate; var sHora: String; var oDatosTurno: TEmpleadoDatosTurno );
var
   iHora, iMinEntrada, iMaxSalida: Integer;
   oDatosHorario, oDatosHorarioAnterior: TDatosHorario;
   oStatusHorario, oStatusHorarioTemp: TStatusHorario;
   dFecTemporal: TDate;
   lEsChecadaDiaAnterior, lEsChecadaDiaSiguiente: Boolean;
begin
     { Averigua el Horario y Status para la fecha de la checada }
     oStatusHorario := DeterminaStatusHorario( iEmpleado, oDatosTurno.Codigo, dFecha );
     { Averigua los datos del horario }
     oDatosHorario := Queries.GetDatosHorario( oStatusHorario.Horario );
     iHora := aMinutos( sHora );
     iMinEntrada := aMinutos( oDatosHorario.Entrada ) - oDatosHorario.GraciaEntradaAntes;
     iMaxSalida := aMinutos( oDatosHorario.UltimaSalidaMadrugada );
     lEsChecadaDiaAnterior := oDatosHorario.ChecadasPares and ChecadasImpares( iEmpleado, dFecha - 1 );
     lEsChecadaDiaSiguiente := FALSE;
     if ( oDatosTurno.TipoTurno <> ttNormal ) then		{ Sin Horario }
     begin
          { Si parece 'Desvelado', la checada corresponde al d�a anterior }
          if ( iHora < iMaxSalida ) or lEsChecadaDiaAnterior then
          begin
               dFecha := dFecha - 1;
               iHora := iHora + K_24HORAS;
          end;
     end
     else
     begin
          if ( iHora > iMaxSalida ) and ( iMaxSalida > AMinutos( oDatosHorario.Salida ) ) and
             ( not lEsChecadaDiaAnterior ) then  // Revisar si el siguiente dia es thMadrugadaSalida o thMediaNoche
          begin
               { Si ese mismo dia empieza la jornada de un horario de madrugada con salida al dia siguiente }
               oStatusHorarioTemp := DeterminaStatusHorario( iEmpleado, oDatosTurno.Codigo, ( dFecha + 1 ) );
               oDatosHorarioAnterior := Queries.GetDatosHorario( oStatusHorarioTemp.Horario );
               lEsChecadaDiaSiguiente := ( oDatosHorarioAnterior.Tipo in [ thMadrugadaSalida, thMediaNoche ] );
               if lEsChecadaDiaSiguiente then
               begin
                    dFecha := dFecha + 1;
                    with oDatosTurno do
                    begin
                         StatusHorario := oStatusHorarioTemp;
                    end;
               end;
          end;
          if ( not lEsChecadaDiaSiguiente ) then
          begin
               { Si es horario de 'Madrugada' que pasan al d�a siguiente }
               if oDatosHorario.Tipo in [ thMadrugadaSalida, thMediaNoche ] then
               begin
                    { Si es posterior a la Ultima hora de Salida, entonces }
                    { se determina que es una ENTRADA temprana del d�a }
                    { siguiente ( Ej. 23:50 ) }
                    if ( iHora > iMaxSalida ) then
                    begin
                         dFecha := dFecha + 1;
                         with oDatosTurno do
                         begin
                              StatusHorario := DeterminaStatusHorario( iEmpleado, Codigo, dFecha );
                         end;
                    end
                    else
                        iHora := iHora + K_24HORAS;
               end
               else
                   begin
                        { Si es anterior al horario NORMAL de entrada, }
                        { revisar la posibilidad de que sea una salida }
                        { tarde del d�a anterior (De madrugada) }
                        if (iHora < iMinEntrada ) or lEsChecadaDiaAnterior then
                        begin
                             oStatusHorario := DeterminaStatusHorario( iEmpleado, oDatosTurno.Codigo, ( dFecha - 1 ) );
                             oDatosHorarioAnterior := Queries.GetDatosHorario( oStatusHorario.Horario );
                             iMaxSalida := aMinutos( oDatosHorarioAnterior.UltimaSalidaMadrugada );
                             { S� es del d�a anterior }
                             if ( iHora <= iMaxSalida ) or lEsChecadaDiaAnterior then
                             begin
                                  dFecha := dFecha - 1;
                                  iHora := iHora + K_24HORAS;
                                  oDatosTurno.StatusHorario := oStatusHorario;
                             end;
                        end;
                   end;

          end;
     end;
     dFecTemporal := Intercambios.CheckIntercambio( oDatosTurno.Codigo, dFecha );
     if ( dFecTemporal <> dFecha ) then
        dFecha := dFecTemporal                                                     // Si hay intercambio de d�a se toma esa fecha
     else
     begin
          if Festivos.CheckFestivo( oDatosTurno.Codigo, dFecha ) and               // Si no hay intercambio investiga si es un d�a Festivo
             strLleno( oDatosTurno.HorarioFestivo ) then                           // Siempre y cuando tenga definido un Horario Festivo
          begin
               oDatosTurno.StatusHorario.Horario := oDatosTurno.HorarioFestivo;    // Asigna a StatusHorario el Horario designado como festivo
               //oDatosTurno.StatusHorario.Status := Se queda con el valor asignado por DeterminaStatusHorario()
          end;
     end;

     sHora := aHoraString( iHora );
end;

procedure TRitmos.GetDatosJornadaBegin;
begin
     GetStatusHorarioBegin;
end;

procedure TRitmos.GetDatosJornadaEnd;
begin
     GetStatusHorarioEnd;
end;

function TRitmos.GetDatosJornada( const sTurno: TCodigo; const dInicial, dFinal: TDate ): TDatosJornada;
var
   DatosRitmo: TDatosRitmo;
begin
     with FHorarioStatus do
     begin
          Active := False;
          oZetaProvider.ParamAsChar( FHorarioStatus, 'Codigo', sTurno, K_ANCHO_TURNO );
          Active := True;
          with Result do
          begin
               Codigo := sTurno;
               if Eof then
               begin
                    TipoTurno := eTipoTurno( 0 );
                    TipoJornada := eTipoJornada( 0 );
                    Dobles := 0;
                    Domingo := 0;
                    Dias := 0;
                    Jornada := 0;
                    HorarioFestivo := VACIO;
{$ifdef QUINCENALES}
                    DiasBase := 0;
{$endif}
{$ifdef TJORNADA_FESTIVOS}
                    TipoJorTrabajo:= eTipoJornadaTrabajo ( 0 );
{$endif}
               end
               else
               begin
                    TipoTurno := eTipoTurno( FieldByName( 'TU_HORARIO' ).AsInteger );
                    TipoJornada := eTipoJornada( FieldByName( 'TU_TIP_JOR' ).AsInteger );
                    Dobles := FieldByName( 'TU_DOBLES' ).AsFloat;
                    Domingo := FieldByName( 'TU_DOMINGO' ).AsFloat;
                    HorarioFestivo := FieldByName( 'TU_HOR_FES' ).AsString;
{$ifdef QUINCENALES}
                    DiasBase := FieldByName( 'TU_DIAS_BA' ).AsFloat;
{$endif}
{$ifdef TJORNADA_FESTIVOS}
                    TipoJorTrabajo := eTipoJornadaTrabajo ( FieldByName('TU_TIP_JT').AsInteger );
{$endif}
                    if ZetaCommonTools.StrVacio( FieldByName( 'TU_RIT_PAT' ).AsString ) then
                    begin
                         Dias := FieldByName( 'TU_DIAS' ).AsInteger;
                         Jornada := FieldByName( 'TU_JORNADA' ).AsFloat;
                    end
                    else
                    begin
                         with DatosRitmo do
                         begin
                              with FHorarioStatus do
                              begin
                                   Inicio := FieldByName( 'TU_RIT_INI' ).AsDateTime;
                                   Patron := FieldByName( 'TU_RIT_PAT' ).AsString;
                                   Horario1 := FieldByName( 'TU_HOR_1' ).AsString;
                                   Horario2 := FieldByName( 'TU_HOR_2' ).AsString;
                                   Horario3 := FieldByName( 'TU_HOR_3' ).AsString;
                              end;
                         end;
                         { GA: Debe regresar siempre por lo menos un 1 d�a }
                         Dias := ZetaCommonTools.iMax( 1, FindCreateRitmo( sTurno, DatosRitmo ).GetDiasStatus( dInicial, dFinal, auHabil ) );
                         //Dias := FindCreateRitmo( sTurno, DatosRitmo ).GetDiasStatus( dInicial, dFinal, auHabil );
                         with FieldByName( 'TU_DIAS' ) do
                         begin
                              if ( AsInteger = 0 ) then
                                 Jornada := 0
                              else
                                  Jornada := Dias * ( FieldByName( 'TU_JORNADA' ).AsFloat / AsInteger );
                         end;
                    end;
               end;
               TieneHorarioFestivo := ZetaCommonTools.StrLleno( HorarioFestivo );
          end;
          Active := False;
     end;
end;

procedure TRitmos.GetDatosTurnoBegin;
begin
     Clear;
     with oZetaProvider do
     begin
          FDatosTurno := CreateQuery( GetSQLScript( Q_TURNO_LEE_DATOS ) );
     end;
end;

procedure TRitmos.GetDatosTurnoEnd;
begin
     FreeAndNil( FDatosTurno );
end;

function TRitmos.GetDatosTurno( const sTurno: String ): TDatosTurno;
begin
     with FDatosTurno do
     begin
          Active := False;
	      with oZetaProvider do
          begin
               ParamAsChar( FDatosTurno, 'Codigo', sTurno, K_ANCHO_TURNO );
          end;
          Active := True;
          with Result do
          begin
               if Eof then
               begin
                    Codigo := '';
                    TipoTurno := eTipoTurno( 0 );
                    TipoDia1 := eStatusAusencia( 0 );
                    TipoDia2 := eStatusAusencia( 0 );
                    TipoDia3 := eStatusAusencia( 0 );
                    TipoDia4 := eStatusAusencia( 0 );
                    TipoDia5 := eStatusAusencia( 0 );
                    TipoDia6 := eStatusAusencia( 0 );
                    TipoDia7 := eStatusAusencia( 0 );
                    Dobles := 0;
                    Domingo := 0;
                    VacacionHabiles := K_DIAS_VACACION_DEF;
                    VacacionSabados := 0;
                    VacacionDescansos := 0;
                    with Ritmo do
                    begin
                         Inicio := NullDateTime;
                         Patron := '';
                         Horario1 := '';
                         Horario2 := '';
                         Horario3 := '';
                    end;
               end
               else
               begin
                    Codigo := FieldByName( 'TU_CODIGO' ).AsString;
                    TipoTurno := eTipoTurno( FieldByName( 'TU_HORARIO' ).AsInteger );
                    TipoDia1 := eStatusAusencia( FieldByName( 'TU_TIP_1' ).AsInteger );
                    TipoDia2 := eStatusAusencia( FieldByName( 'TU_TIP_2' ).AsInteger );
                    TipoDia3 := eStatusAusencia( FieldByName( 'TU_TIP_3' ).AsInteger );
                    TipoDia4 := eStatusAusencia( FieldByName( 'TU_TIP_4' ).AsInteger );
                    TipoDia5 := eStatusAusencia( FieldByName( 'TU_TIP_5' ).AsInteger );
                    TipoDia6 := eStatusAusencia( FieldByName( 'TU_TIP_6' ).AsInteger );
                    TipoDia7 := eStatusAusencia( FieldByName( 'TU_TIP_7' ).AsInteger );
                    Dobles := FieldByName( 'TU_DOBLES' ).AsCurrency;
                    Domingo := FieldByName( 'TU_DOMINGO' ).AsCurrency;
                    VacacionHabiles := FieldByName( 'TU_VACA_HA' ).AsCurrency;
                    VacacionSabados := FieldByName( 'TU_VACA_SA' ).AsCurrency;
                    VacacionDescansos := FieldByName( 'TU_VACA_DE' ).AsCurrency;
                    with Ritmo do
                    begin
                         Inicio := FieldByName( 'TU_RIT_INI' ).AsDateTime;
                         Patron := FieldByName( 'TU_RIT_PAT' ).AsString;
                         Horario1 := FieldByName( 'TU_HOR_1' ).AsString;
                         Horario2 := FieldByName( 'TU_HOR_2' ).AsString;
                         Horario3 := FieldByName( 'TU_HOR_3' ).AsString;
                    end;
               end;
          end;
          Active := False;
     end;
end;

function TRitmos.GetDiasNoHay( Periodo: TDatosPeriodo; Jornada: TDatosJornada; Turno: TDatosTurno; const iPrimerDia: Integer ): Integer;
begin
     with Periodo do
     begin
          if ( Dias < 7 ) and ( Uso <> upEspecial ) then
{$ifdef QUINCENALES}
             Result := ZetaCommonTools.iMax( Jornada.Dias - GetDiasHabiles( Turno, InicioAsis, FinAsis, iPrimerDia ), 0 )
{$else}
             Result := ZetaCommonTools.iMax( Jornada.Dias - GetDiasHabiles( Turno, Inicio, Fin, iPrimerDia ), 0 )
{$endif}
          else
	             Result := 0;
     end;
end;

function TRitmos.GetDiasHabiles( Turno: TDatosTurno; const dInicial, dFinal: TDate; const iPrimerDia: Integer ): Integer;
var
   dFecha: TDate;

function GetTipoDia( const iPosicion: Byte ): eStatusAusencia;
begin
     with Turno do
     begin
          case iPosicion of
               1: Result := TipoDia1;
               2: Result := TipoDia2;
               3: Result := TipoDia3;
               4: Result := TipoDia4;
               5: Result := TipoDia5;
               6: Result := TipoDia6;
          else
               Result := TipoDia7;
          end;
     end;
end;

begin
     Result := 0;
     if ZetaCommonTools.StrVacio( Turno.Ritmo.Patron ) then
     begin
          dFecha := dInicial;
          repeat
                if ( GetTipoDia( DiaSemanaPos( dFecha, iPrimerDia ) )= auHabil ) then
                   Result := Result + 1;
                dFecha := dFecha + 1;
          until ( dFecha > dFinal );
     end
     else
     begin
          Result := FindCreateRitmo( Turno.Codigo, Turno.Ritmo ).GetDiasStatus( dInicial, dFinal, auHabil );
     end;
end;

function TRitmos.CreaFestivos( const dInicial, dFinal: TDate; const eTipoLista: eTipoFestivo ): TListaFestivos;
begin
     Result := TListaFestivos.Create( oZetaProvider );
     with Result do
     begin
          Preparar;
          Cargar( dInicial, dFinal, eTipoLista );
     end;
end;

procedure TRitmos.DestruyeFestivos;
begin
     if Assigned( FIntercambios ) then
        FreeAndNil( FIntercambios );
     if Assigned( FFestivos ) then
        FreeAndNil( FFestivos );
end;

procedure TRitmos.RitmosBegin( const dInicial, dFinal: TDate );
var
   FQuery: TZetaCursor;
begin
     Preparar;
     FIntercambios := CreaFestivos( dInicial, dFinal, tfIntercambio );
     FFestivos := CreaFestivos( dInicial, dFinal, tfFestivo );
     with oZetaProvider do
     begin
          FQuery := CreateQuery( Format( GetSQLScript( Q_BUSCA_TEMPORALES ), [ K_GLOBAL_SI, ZetaCommonTools.DateToStrSQLC( dInicial - 1 ), ZetaCommonTools.DateToStrSQLC( dFinal + 1 ) ] ) );
          try
             with FQuery do
             begin
                  Active := True;
                  FHayTemporal := FieldByName( 'CUANTOS' ).AsInteger > 0;
                  Active := False;
             end;
          finally
                 FreeAndNil( FQuery );
          end;
          if FHayTemporal then
          begin
               FTarjetaLee := CreateQuery( GetSQLScript( Q_TARJETA_LEE ) );
          end;
          FChecadasPares := CreateQuery( GetSQLScript( Q_QTY_CHECADAS_IN_OUT ) );
     end;
end;

procedure TRitmos.RitmosEnd;
begin
     FreeAndNil( FChecadasPares );
     if FHayTemporal then
     begin
          FreeAndNil( FTarjetaLee );
          FHayTemporal := False;
     end;
     Despreparar;
     DestruyeFestivos;
end;

{ ************* TCommonQueries ************ }

constructor TCommonQueries.Create( oProvider: TdmZetaServerProvider );
begin
     {$ifdef DOTNET}
     inherited Create;
     {$endif}
     FProvider := oProvider;
end;

procedure TCommonQueries.GetStatusEmpleadoBegin;
begin
     with oZetaProvider do
     begin
          FStatusEmpleado := CreateQuery( GetSQLScript( Q_EMPLEADO_STATUS ) );
     end;
end;

procedure TCommonQueries.GetStatusEmpleadoEnd;
begin
     FreeAndNil( FStatusEmpleado );
end;

function TCommonQueries.GetStatusEmpleado( const iEmpleado: TNumEmp; const dFecha: TDate ): eStatusEmpleado;
begin
     with oZetaProvider do
     begin
          with FStatusEmpleado do
          begin
               Active := False;
               ParamAsInteger( FStatusEmpleado, 'Empleado', iEmpleado );
               ParamAsDate( FStatusEmpleado, 'Fecha', dFecha );
               Active := True;
               if Eof then
                  Result := eStatusEmpleado( 0 ) { � Es correcto suponer esto ? }
               else
                   Result := eStatusEmpleado( FieldByName( 'RESULTADO' ).AsInteger );
               Active := False;
          end;
     end;
end;

procedure TCommonQueries.GetDatosHorarioBegin;
begin
     with oZetaProvider do
     begin
          FDatosHorario := CreateQuery( GetSQLScript( Q_GET_DATOS_HORARIO ) );
     end;
end;

procedure TCommonQueries.GetDatosHorarioEnd;
begin
     FreeAndNil( FDatosHorario );
end;

function TCommonQueries.GetDatosHorario(const sHorario: String): TDatosHorario;
begin
     with FDatosHorario do
     begin
          Active := False;
	      with oZetaProvider do
          begin
               ParamAsChar( FDatosHorario, 'Codigo', sHorario, K_ANCHO_HORARIO );
          end;
          Active := True;
          with Result do
          begin
               if Eof then
               begin
                    Tipo := eTipoHorario( 0 );
                    Entrada := '';
                    GraciaEntradaAntes := 0;
                    UltimaSalidaMadrugada := '';
                    Salida := '';
                    ChecadasPares := False;
               end
               else
               begin
                    Tipo := eTipoHorario( FieldByName( 'HO_TIPO' ).AsInteger );
                    Entrada := FieldByName( 'HO_INTIME' ).AsString;
                    GraciaEntradaAntes := FieldByName( 'HO_IN_TEMP' ).AsInteger;
                    UltimaSalidaMadrugada := FieldByName( 'HO_LASTOUT' ).AsString;
                    Salida := FieldByName( 'HO_OUTTIME' ).AsString;
                    ChecadasPares := zStrToBool( FieldByName( 'HO_PARES' ).AsString );
               end;
          end;
          Active := False;
     end;
end;

procedure TCommonQueries.GetDatosHorarioDiarioBegin;
begin
     with oZetaProvider do
     begin
          FDatosHorarioDiario := CreateQuery( GetSQLScript( Q_GET_DATOS_HORARIO_DIARIO ) );
     end;
end;

procedure TCommonQueries.GetDatosHorarioDiarioEnd;
begin
     FreeAndNil( FDatosHorarioDiario );
end;

function TCommonQueries.GetDatosHorarioDiario( const Codigo: String ): TDatosHorarioDiario;
begin
     with FDatosHorarioDiario do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsChar( FDatosHorarioDiario, 'Codigo', Codigo, K_ANCHO_HORARIO );
          end;
          Active := True;
          with Result do
          begin
               if Eof then
               begin
                    Tipo := eTipoHorario( 0 );
                    Jornada := 0;
                    Dobles := 0;
                    MinimoComer := 0;
                    ChecaComer := False;
                    AgregaComer := False;
                    IgnoraComer := False;
                    TiempoComer := 0;
                    ExtrasFijas := 0;
                    GraciaInTemprano := 0;
                    GraciaInTarde := 0;
                    GraciaOutTemprano := 0;
                    GraciaOutTarde := 0;
                    SaleAComer := 0;
                    RegresaDeComer := 0;
                    ComidaExtras := 0;
                    MinComExtras := 0;
                    Entrada := 0;
                    Entra24 := 0;
                    Salida := 0;
                    Sale24 := 0;
                    SaleADescanso := 0;
                    RegresaDeDescanso := 0;
               end
               else
               begin
                    Tipo := eTipoHorario( FieldByName( 'HO_TIPO' ).AsInteger );
                    Jornada := FieldByName( 'HO_JORNADA' ).AsFloat;
                    Dobles := FieldByName( 'HO_DOBLES' ).AsFloat;
                    MinimoComer := FieldByName( 'HO_MIN_EAT' ).AsInteger;
                    ChecaComer := zStrToBool( FieldByName( 'HO_CHK_EAT' ).AsString );
                    AgregaComer := zStrToBool( FieldByName( 'HO_ADD_EAT' ).AsString );
                    IgnoraComer := zStrToBool( FieldByName( 'HO_IGN_EAT' ).AsString );
                    TiempoComer := FieldByName( 'HO_COMER' ).AsInteger;
                    ExtrasFijas := FieldByName( 'HO_EXT_FIJ' ).AsFloat;
                    GraciaInTemprano := FieldByName( 'HO_IN_TEMP' ).AsInteger;
                    GraciaInTarde := FieldByName( 'HO_IN_TARD' ).AsInteger;
                    GraciaOutTemprano := FieldByName( 'HO_OU_TEMP' ).AsInteger;
                    GraciaOutTarde := FieldByName( 'HO_OU_TARD' ).AsInteger;
                    SaleAComer := aMinutos( FieldByName( 'HO_OUT_EAT' ).AsString );
                    RegresaDeComer := aMinutos( FieldByName( 'HO_IN_EAT' ).AsString );
                    ComidaExtras := FieldByName( 'HO_EXT_COM' ).AsInteger;
                    MinComExtras := FieldByName( 'HO_EXT_MIN' ).AsInteger;
                    Entrada := aMinutos( FieldByName( 'HO_INTIME' ).AsString );
                    Entra24 := aMinutos( GetEntrada24( Tipo, FieldByName( 'HO_INTIME' ).AsString ) );
                    Salida := aMinutos( FieldByName( 'HO_OUTTIME' ).AsString );
                    Sale24 := aMinutos( GetSalida24( Tipo, FieldByName( 'HO_INTIME' ).AsString, FieldByName( 'HO_OUTTIME' ).AsString ) );
                    SaleADescanso := aMinutos( FieldByName( 'HO_OUT_BRK' ).AsString );
                    RegresaDeDescanso := aMinutos( FieldByName( 'HO_IN_BRK' ).AsString );
               end;
               ConHorario := ( Tipo <> thSinHorario );
               IgnoraCompletamente := IgnoraComer and ( SaleAComer = RegresaDeComer );
          end;
          Active := False;
     end;
end;

procedure TCommonQueries.GetDatosEmpleadoBegin;
begin
     with oZetaProvider do
     begin
          FDatosEmpleado := CreateQuery( GetSQLScript( Q_EMPLEADO_DATOS ) );
     end;
end;

procedure TCommonQueries.GetDatosEmpleadoEnd;
begin
     FreeAndNil( FDatosEmpleado );
end;

function TCommonQueries.GetDatosEmpleado( const iEmpleado: TNumEmp ): TDatosEmpleado;
const
     K_CB_AUTOSAL = 0;
     K_CB_CLASIFI = 1;
     K_CB_CONTRAT = 2;
     K_CB_TURNO = 3;
     K_CB_MOT_BAJ = 4;
     K_CB_PATRON = 5;
     K_CB_PUESTO = 6;
     K_CB_SALARIO = 7;
     K_CB_TABLASS = 8;
     K_CB_ZONA_GE = 9;
     K_CB_NIVEL1 = 10;
     K_CB_NIVEL2 = 11;
     K_CB_NIVEL3 = 12;
     K_CB_NIVEL4 = 13;
     K_CB_NIVEL5 = 14;
     K_CB_NIVEL6 = 15;
     K_CB_NIVEL7 = 16;
     K_CB_NIVEL8 = 17;
     K_CB_NIVEL9 = 18;
     {$ifdef ACS}
     K_CB_NIVEL10 = 32;
     K_CB_NIVEL11 = 33;
     K_CB_NIVEL12 = 34;
     {$endif}
     K_CB_FEC_ANT = 19;
     K_CB_FEC_SAL = 20;
     K_CB_TIP_REV = 21;
     K_CB_FEC_ING = 22;
     K_CB_OLD_SAL = 23;
     K_CB_OLD_INT = 24;
     K_CB_PRE_INT = 25;
     K_CB_PER_VAR = 26;
     K_CB_SAL_TOT = 27;
     K_CB_TOT_GRA = 28;
     K_CB_FAC_INT = 29;
     K_CB_RANGO_S = 30;
     K_CB_FEC_BAJ = 31;
begin
     with FDatosEmpleado do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FDatosEmpleado, 'Empleado', iEmpleado );
          end;
          Active := True;
          with Result do
          begin
               NumEmp := iEmpleado;
               if Eof then
               begin
                    AUTOSAL := K_GLOBAL_NO;
                    CONTRAT :=  '';
                    MOT_BAJ :=  '';
                    SALARIO :=  0;
                    TABLASS :=  '';
                    FEC_ANT :=  NullDateTime;
                    FEC_SAL :=  NullDateTime;
                    TIP_REV :=  '';
                    FEC_ING :=  NullDateTime;
                    OLD_SAL :=  0;
                    OLD_INT :=  0;
                    PRE_INT :=  0;
                    PER_VAR :=  0;
                    SAL_TOT :=  0;
                    TOT_GRA :=  0;
                    FAC_INT :=  0;
                    RANGO_S :=  0;
                    FEC_BAJ :=  NullDateTime;
                    with oClasifi do
                    begin
                         CLASIFI := '';
                         TURNO :=  '';
                         PATRON := '';
                         PUESTO := '';
                         ZONA_GE := '';
                         NIVEL1 := '';
                         NIVEL2 := '';
                         NIVEL3 := '';
                         NIVEL4 := '';
                         NIVEL5 := '';
                         NIVEL6 := '';
                         NIVEL7 := '';
                         NIVEL8 := '';
                         NIVEL9 := '';
                         {$ifdef ACS}
                         NIVEL10 := '';
                         NIVEL11 := '';
                         NIVEL12 := '';
		         {$endif}
                    end;
               end
               else
               begin
                    AUTOSAL := Fields[ K_CB_AUTOSAL ].AsString;
                    CONTRAT :=  Fields[ K_CB_CONTRAT ].AsString;
                    MOT_BAJ :=  Fields[ K_CB_MOT_BAJ ].AsString;
                    SALARIO :=  Fields[ K_CB_SALARIO ].AsFloat;
                    TABLASS :=  Fields[ K_CB_TABLASS ].AsString;
                    FEC_ANT :=  Fields[ K_CB_FEC_ANT ].AsDateTime;
                    FEC_SAL :=  Fields[ K_CB_FEC_SAL ].AsDateTime;
                    TIP_REV :=  Fields[ K_CB_TIP_REV ].AsString;
                    FEC_ING :=  Fields[ K_CB_FEC_ING ].AsDateTime;
                    OLD_SAL :=  Fields[ K_CB_OLD_SAL ].AsFloat;
                    OLD_INT :=  Fields[ K_CB_OLD_INT ].AsFloat;
                    PRE_INT :=  Fields[ K_CB_PRE_INT ].AsFloat;
                    PER_VAR :=  Fields[ K_CB_PER_VAR ].AsFloat;
                    SAL_TOT :=  Fields[ K_CB_SAL_TOT ].AsFloat;
                    TOT_GRA :=  Fields[ K_CB_TOT_GRA ].AsFloat;
                    FAC_INT :=  Fields[ K_CB_FAC_INT ].AsFloat;
                    RANGO_S :=  Fields[ K_CB_RANGO_S ].AsFloat;
                    FEC_BAJ :=  Fields[ K_CB_FEC_BAJ ].AsDateTime;
                    with oClasifi do
                    begin
                         CLASIFI := Fields[ K_CB_CLASIFI ].AsString;
                         TURNO :=  Fields[ K_CB_TURNO ].AsString;
                         PATRON := Fields[ K_CB_PATRON ].AsString;
                         PUESTO := Fields[ K_CB_PUESTO ].AsString;
                         ZONA_GE := Fields[ K_CB_ZONA_GE ].AsString;
                         NIVEL1 := Fields[ K_CB_NIVEL1 ].AsString;
                         NIVEL2 := Fields[ K_CB_NIVEL2 ].AsString;
                         NIVEL3 := Fields[ K_CB_NIVEL3 ].AsString;
                         NIVEL4 := Fields[ K_CB_NIVEL4 ].AsString;
                         NIVEL5 := Fields[ K_CB_NIVEL5 ].AsString;
                         NIVEL6 := Fields[ K_CB_NIVEL6 ].AsString;
                         NIVEL7 := Fields[ K_CB_NIVEL7 ].AsString;
                         NIVEL8 := Fields[ K_CB_NIVEL8 ].AsString;
                         NIVEL9 := Fields[ K_CB_NIVEL9 ].AsString;
		         {$ifdef ACS}
                         NIVEL10 := Fields[ K_CB_NIVEL10 ].AsString;
                         NIVEL11 := Fields[ K_CB_NIVEL11 ].AsString;
                         NIVEL12 := Fields[ K_CB_NIVEL12 ].AsString;
		         {$endif}
                    end;
               end;
          end;
          Active := False;
     end;
end;

procedure TCommonQueries.GetStatusActEmpleadoBegin;
begin
     with oZetaProvider do
     begin
          FStatusActEmpleado := CreateQuery( GetSQLScript( Q_EMPLEADO_STATUS_ACT ) );
     end;
end;

procedure TCommonQueries.GetStatusActEmpleadoEnd;
begin
     FreeAndNil( FStatusActEmpleado );
end;

function TCommonQueries.GetStatusActEmpleado( const iEmpleado: TNumEmp; const dFecha: TDate ): eStatusEmpleado;
begin
     with oZetaProvider do
     begin
          FStatusActEmpleado.Active := FALSE;
          ParamAsInteger( FStatusActEmpleado, 'Empleado', iEmpleado );
          ParamAsDate( FStatusActEmpleado, 'Fecha', dFecha );
          with FStatusActEmpleado do
          begin
               Active := TRUE;
               if IsEmpty then
                  Result := eStatusEmpleado( 0 ) { � Es correcto suponer esto ? }
               else
                   Result := eStatusEmpleado( FieldByName( 'RESULTADO' ).AsInteger );
               Active := FALSE;
          end;
     end;
end;

function TCommonQueries.GetFechaCambioTNom(const iEmpleado: TNumEmp;dFecha: TDate): TDate; 
begin 
     with oZetaProvider do 
     begin 
          with FFechaCambioTNom do 
          begin 
               Active:= False; 
               ParamAsInteger( FFechaCambioTNom, 'Empleado', iEmpleado ); 
               ParamAsDate( FFechaCambioTNom, 'Fecha', dFecha ); 
               Active:= True; 
               if ( IsEmpty ) then 
                    Result:= NullDateTime 
               else 
                   Result:= FieldByName('CB_FECHA').AsDateTime; 
               Active:= False; 
          end; 
     end; 
end; 
 
procedure TCommonQueries.GetFechaCambioTNomBegin; 
begin 
     with oZetaProvider do 
     begin 
          FFechaCambioTNom:= CreateQuery( GetSQLScript( Q_FECHA_CAMBIO_TIP_NOM ) ); 
     end; 
end; 
 
procedure TCommonQueries.GetFechaCambioTNomEnd; 
begin 
     FreeAndNil( FFechaCambioTNom ); 
end; 
 
procedure TCommonQueries.GetFechaAltaBegin; 
begin 
     with oZetaProvider do 
     begin 
          FFechaAlta:= CreateQuery( GetSQLScript( Q_FECHA_ALTA ) ); 
     end; 
end; 
 
function TCommonQueries.GetFechaAlta( const dFecha: TDate; const iEmpleado: Integer): TDate; 
begin 
     { GetFechaAlta: Revisa fecha de ingreso a cierta fecha de kardex } 
     with oZetaProvider do 
     begin 
          Result:= NullDateTime; 
          with FFechaAlta do 
          begin 
               Active:= False; 
                { Par�metros Kardex dAlta } 
               ParamAsInteger( FFechaAlta, 'Empleado', iEmpleado ); 
               ParamAsDate( FFechaAlta, 'Fecha', dFecha ); 
               Active:= True; 
               if not ( IsEmpty ) then 
               begin 
                    Result:= FieldByName('CB_FEC_ING').AsDateTime; 
               end; 
               Active:= False; 
          end; 
     end; 
end; 
 
 
procedure TCommonQueries.GetFechaAltaEnd; 
begin 
     with oZetaProvider do 
     begin 
          FreeAndNil( FFechaAlta ); 
     end; 
end; 


procedure TCommonQueries.RevisaStatusBloqueoBegin;
begin
     with oZetaProvider do
     begin
          FStatusNomina := CreateQuery( GetSQLScript( Q_STATUS_X_BLOQUEO ) );
          FTipoNomEmp := CreateQuery( GetSQLScript( Q_TIPO_NOM_EMP ) );
          InitGlobales;
          FStatusLimite := GetGlobalInteger(K_LIMITE_MODIFICAR_ASISTENCIA);
          FBloqueoStatus := GetGlobalBooleano(K_GLOBAL_BLOQUEO_X_STATUS);
     end;
end;

procedure TCommonQueries.RevisaStatusBloqueoEnd;
begin
     FreeAndNil(FStatusNomina);
     FreeAndNil(FTipoNomEmp);
end;

function TCommonQueries.RevisaStatusBloqueo(const dFecha:TDate;const iEmpleado:Integer;var sTitulo,sDetalle:string ):Boolean;
var
   iStatusActual,iTipoNomEmp:Integer;
begin
     sTitulo := VACIO;
     sDetalle := VACIO;
     with oZetaProvider do
     begin
          Result := not FBloqueoStatus;
          if not Result then
          begin
               with FTipoNomEmp do
               begin
                    Active := False;
                    ParamAsDate( FTipoNomEmp, 'Fecha', dFecha );
                    ParamAsInteger( FTipoNomEmp,'Empleado',iEmpleado);
                    Active := True;
                    iTipoNomEmp := FieldByName('CB_NOMINA').AsInteger;
               end;

               with FStatusNomina do
               begin
                    Active := False;
                    ParamAsDate( FStatusNomina, 'Fecha', dFecha );
                    ParamAsInteger( FStatusNomina,'TipoPeriodo',iTipoNomEmp );
                    Active := True;
                    iStatusActual := FieldByName('PE_STATUS').AsInteger;
               end;
               Result := not ( iStatusActual > FStatusLimite );
               if not Result then
               begin
                    sTitulo :=  'No Puede Cambiar Tarjetas por L�mite de Bloqueo';
                    sDetalle := Format('Fecha de Tarjeta: %s '+CR_LF+'Status del Periodo: %s',[DateToStr(dFecha),ObtieneElemento( lfStatusPeriodo,iStatusActual ) ] );
               end;
          end;
     end;
end;

{ TPrimaDominical }

function TPrimaDominical.CalcularOrdinariasMadrugada(const iEmpleado: TNumEmp; const dValue: TDate;const lAntesMediaNoche: Boolean;const rTotales: TDiasHoras): THorasPrimaDominical;
var
   lEntrada, lSalida, lOrdinarias: Boolean;
   iHora, iEntrada, iSalida, iAntes, iDespues: Integer;
   iTotalesAntes, iTotalesDespues, iDeltaAntes, iDeltaDespues: Integer;
   rTotalesAplicadas: TDiasHoras;
begin
     { Inicializa acumulados en Minutos de tiempo anterior a medianoche }
     iAntes := 0;
     iTotalesAntes := 0;
     { Inicializa acumulados en Minutos de tiempo posterior a medianoche }
     iDespues := 0;

     iTotalesDespues := 0;

     
         with FProvider do
         begin
              ParamAsInteger( FChecadasLee, 'Empleado', iEmpleado );
              ParamAsDate( FChecadasLee, 'Fecha', dValue );
         end;
         { Barrer las checadas ordinarias }
         with FChecadasLee do
         begin
              Active := True;
              lEntrada := False;
              lSalida := False;
              iEntrada := 0;
              iSalida := 0;
              while not Eof do
              begin
                   iHora := ZetaCommonTools.aMinutos( FieldByName( 'CH_H_AJUS' ).AsString );
                   lOrdinarias := ( eDescripcionChecadas( FieldByName( 'CH_DESCRIP' ).AsInteger ) <> dcExtras );
                   case eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger ) of
                        chEntrada, chInicio:
                        begin
                             if lEntrada then
                             begin
                                  iSalida := iHora;
                                  { Validar que la entrada suceda antes de la salida }
                                  if ( iEntrada < iSalida ) then
                                  begin
                                       { Tanto Entrada como Salida suceden antes de Medianoche }
                                       if ( ( iEntrada < K_24HORAS ) and ( iSalida <= K_24HORAS ) ) then
                                       begin
                                            iDeltaAntes := ZetaCommonTools.iMax( 0, ( iSalida - iEntrada ) );
                                            iTotalesAntes := iTotalesAntes + iDeltaAntes;
                                       end
                                       else
                                           { Tanto Entrada como Salida suceden despu�s de Medianoche }
                                           if ( ( iEntrada >= K_24HORAS ) and ( iSalida > K_24HORAS ) ) then
                                           begin
                                               iDeltaDespues := ZetaCommonTools.iMax( 0, ( iSalida - iEntrada ) );
                                               iTotalesDespues := iTotalesDespues + iDeltaDespues;
                                           end
                                           else
                                               { La entrada sucede antes de MediaNoche y la Salida despu�s de Medianoche }
                                               if ( ( iEntrada < K_24HORAS ) and ( iSalida > K_24HORAS ) ) then
                                               begin
                                                    iDeltaAntes := ZetaCommonTools.iMax( 0, ( K_24HORAS - iEntrada ) );
                                                    iDeltaDespues := ZetaCommonTools.iMax( 0, ( iSalida - K_24HORAS ) );
                                                    iTotalesAntes := iTotalesAntes + iDeltaAntes;
                                                    iTotalesDespues := iTotalesDespues + iDeltaDespues;
                                               end;
                                  end;
                             end;
                             lEntrada := True;
                             iEntrada := iHora;
                        end;
                        chSalida, chFin:
                        begin
                             { Ya se encontr� una entrada previa a esta salida }
                             if lEntrada then
                                iSalida := iHora
                             else if lSalida then
                             begin
                                  iEntrada := iSalida;
                                  iSalida := iHora;
                             end;

                             { Validar que la entrada suceda antes de la salida }
                             if ( iEntrada < iSalida ) then
                             begin
                                  { Tanto Entrada como Salida suceden antes de Medianoche }
                                  if ( ( iEntrada < K_24HORAS ) and ( iSalida <= K_24HORAS ) ) then
                                  begin
                                       iDeltaAntes := ZetaCommonTools.iMax( 0, ( iSalida - iEntrada ) );
                                       iTotalesAntes := iTotalesAntes + iDeltaAntes;
                                       if lOrdinarias then
                                          iAntes := iAntes + iDeltaAntes;
                                  end
                                  else
                                      { Tanto Entrada como Salida suceden despu�s de Medianoche }
                                      if ( ( iEntrada >= K_24HORAS ) and ( iSalida > K_24HORAS ) ) then
                                      begin
                                          iDeltaDespues := ZetaCommonTools.iMax( 0, ( iSalida - iEntrada ) );
                                          iTotalesDespues := iTotalesDespues + iDeltaDespues;
                                          if lOrdinarias then
                                             iDespues := iDespues + iDeltaDespues;
                                      end
                                      else
                                          { La entrada sucede antes de MediaNoche y la Salida despu�s de Medianoche }
                                          if ( ( iEntrada < K_24HORAS ) and ( iSalida > K_24HORAS ) ) then
                                          begin
                                               iDeltaAntes := ZetaCommonTools.iMax( 0, ( K_24HORAS - iEntrada ) );
                                               iDeltaDespues := ZetaCommonTools.iMax( 0, ( iSalida - K_24HORAS ) );
                                               iTotalesAntes := iTotalesAntes + iDeltaAntes;
                                               iTotalesDespues := iTotalesDespues + iDeltaDespues;
                                               if lOrdinarias then
                                               begin
                                                    iAntes := iAntes +iDeltaAntes;
                                                    iDespues := iDespues + iDeltaDespues;
                                               end;
                                          end;
                             end;
                             lSalida := TRUE;
                             lEntrada := False;
                             iEntrada := 0;
                        end;
                   end;
                   Next;
              end;
              Active := False;
         end;

         with Result do
         begin
             { Las horas totales aplicadas son las que suceden antes de la medianoche }
             rTotalesAplicadas := ZetaCommonTools.rMin( rTotales, ZetaCommonTools.aHoras( iTotalesAntes ) );

             if lAntesMediaNoche then
             begin
                  Ordinarias := ZetaCommonTools.aHoras( iAntes );
                  { Topar a las horas  totales autorizadas }
                  Totales := rTotalesAplicadas;
             end
             else
             begin
                  Ordinarias := ZetaCommonTools.aHoras( iDespues );
                  { Topar a las autorizadas, pero descontando primero las horas totales antes de la medianoche }
                  Totales := ZetaCommonTools.rMin( rTotales , ZetaCommonTools.aHoras( iTotalesDespues ) );
             end;
         end;

end;

function TPrimaDominical.CalcularPrimaDominical(const iEmpleado: TNumEmp;const dValue: TDate; const eTipo: eTipoHorario; const rOrdinarias,rTotales: TDiasHoras; bPrimaDominicalTopada: Boolean = FALSE): THorasPrimaDominical;
begin
      { Inicializar Prima Dominical en Cero }
     FPrimaDominicalCompleta := FProvider.GetGlobalBooleano( K_GLOBAL_PRIMA_DOMINICAL_COMPLETA );

     with Result do
     begin
          Ordinarias := 0;
          Totales := 0;
     end;
      //FPrimaDominicalTotal := GetGlobalBooleano(K_GLOBAL_PRIMA_DOMINICAL_TIPO_DIA);

     if FPrimaDominicalCompleta then
     begin
          case DayOfWeek( dValue ) of
               1:   { D�a h�bil es domingo }
               begin
                    if not bPrimaDominicalTopada then
                    begin
                         case eTipo of
                              { Considerar �nicamente el tiempo ordinario que cae }
                              { antes de las 24:00 horas }
                              thMadrugadaEntrada: Result := CalcularOrdinariasMadrugada( iEmpleado, dValue, True, rTotales );
                              { Considerar �nicamente el tiempo ordinario que cae }
                              { despu�s de las 24:00 horas }
                              thMadrugadaSalida: Result := CalcularOrdinariasMadrugada( iEmpleado, dValue, False, rTotales );
                         else
                             { Se toman todas las horas ordinarias ( Comportamiento anterior a este cambio ) }
                             with Result do
                             begin
                                  Ordinarias := rOrdinarias;
                                  Totales := rTotales;
                             end;
                         end;
                    end
                    else
                    begin
                         Result := CalcularOrdinariasMadrugada( iEmpleado, dValue, True, rTotales );
                    end;
               end;
               2:   { D�a h�bil es Lunes pero empez� en Domingo }
               begin
                    { Unicamente considerar cuando las checadas }
                    { empiezan el Domingo pero llevan fecha del Lunes }
                    case eTipo of
                         { Considerar �nicamente el tiempo ordinario que cae }
                         { antes de las 24:00 horas }
                         thMadrugadaSalida: Result := CalcularOrdinariasMadrugada( iEmpleado, dValue, True, rTotales );
                    end;
               end;
               7:  { D�a h�bil es S�bado pero termin� en Domingo }
               begin
                    { Unicamente considerar cuando las checadas }
                    { terminan el Domingo pero llevan fecha del S�bado }
                    if not bPrimaDominicalTopada then
                    begin
                         case eTipo of
                              { Considerar �nicamente el tiempo ordinario que cae }
                              { despu�s de las 24:00 horas }
                              thMadrugadaEntrada: Result := CalcularOrdinariasMadrugada( iEmpleado, dValue, False, rTotales );
                         end;
                    end
                    else
                    begin
                         Result := CalcularOrdinariasMadrugada( iEmpleado, dValue, False, rTotales );
                    end;
               end;
          end;
     end
     else
     begin
          case DayOfWeek( dValue ) of
               { D�a h�bil es domingo: se toman todas las horas ordinarias ( Comportamiento anterior a este cambio ) }
               1:
               with Result do
               begin
                    Ordinarias := rOrdinarias;
                    Totales := rTotales;
               end;
          end;
     end;
end;

constructor TPrimaDominical.Create(oProvider: TdmZetaServerProvider);
begin
     {$ifdef DOTNET}
     inherited Create;
     {$endif}
     FProvider := oProvider;
     FChecadasLee := oZetaProvider.CreateQuery( Format( GetSQLScript( Q_CHECADAS_LEE ) , [ Ord( chEntrada ),
                                                                                           Ord( chSalida ),
                                                                                           Ord( chInicio ),
                                                                                           Ord( chFin ),
                                                                                           Ord( dcOrdinaria ),
                                                                                           Ord( dcRetardo ),
                                                                                           Ord( dcPuntual ),
                                                                                           Ord( dcExtras ) ] ) );
end;

destructor TPrimaDominical.Destroy;
begin
     inherited;
     FreeAndNil(FChecadasLee);
end;

end.

