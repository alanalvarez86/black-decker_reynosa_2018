unit Global_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 2/24/2017 10:38:22 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2017_Mejoras_Operativas_1y2\MTS\Global.tlb (1)
// LIBID: {FC480961-88D5-11D3-8BBE-0050DA04EAC5}
// LCID: 0
// Helpfile: 
// HelpString: Tress Global
// DepndLst: 
//   (1) v1.0 Midas, (C:\Program Files (x86)\Embarcadero\RAD Studio\12.0\bin64\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  GlobalMajorVersion = 1;
  GlobalMinorVersion = 0;

  LIBID_Global: TGUID = '{FC480961-88D5-11D3-8BBE-0050DA04EAC5}';

  IID_IdmServerGlobal: TGUID = '{FC480962-88D5-11D3-8BBE-0050DA04EAC5}';
  CLASS_dmServerGlobal: TGUID = '{FC480964-88D5-11D3-8BBE-0050DA04EAC5}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerGlobal = interface;
  IdmServerGlobalDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerGlobal = IdmServerGlobal;


// *********************************************************************//
// Interface: IdmServerGlobal
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FC480962-88D5-11D3-8BBE-0050DA04EAC5}
// *********************************************************************//
  IdmServerGlobal = interface(IAppServer)
    ['{FC480962-88D5-11D3-8BBE-0050DA04EAC5}']
    function GetGlobales(Empresa: OleVariant): OleVariant; safecall;
    function GrabaGlobales(Empresa: OleVariant; oDelta: OleVariant; lActualizaDiccion: WordBool; 
                           out ErrorCount: Integer): OleVariant; safecall;
    function GetFechaLimite(Empresa: OleVariant): OleVariant; safecall;
    function SetFechaLimite(Empresa: OleVariant; Fecha: TDateTime): WordBool; safecall;
    function GrabaDescripcionesGV(Empresa: OleVariant; oDelta: OleVariant; 
                                  lActualizaDiccion: WordBool; out ErrorCount: Integer): OleVariant; safecall;
    function GetDescripcionesGV(Empresa: OleVariant): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerGlobalDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FC480962-88D5-11D3-8BBE-0050DA04EAC5}
// *********************************************************************//
  IdmServerGlobalDisp = dispinterface
    ['{FC480962-88D5-11D3-8BBE-0050DA04EAC5}']
    function GetGlobales(Empresa: OleVariant): OleVariant; dispid 1;
    function GrabaGlobales(Empresa: OleVariant; oDelta: OleVariant; lActualizaDiccion: WordBool; 
                           out ErrorCount: Integer): OleVariant; dispid 2;
    function GetFechaLimite(Empresa: OleVariant): OleVariant; dispid 3;
    function SetFechaLimite(Empresa: OleVariant; Fecha: TDateTime): WordBool; dispid 4;
    function GrabaDescripcionesGV(Empresa: OleVariant; oDelta: OleVariant; 
                                  lActualizaDiccion: WordBool; out ErrorCount: Integer): OleVariant; dispid 301;
    function GetDescripcionesGV(Empresa: OleVariant): OleVariant; dispid 302;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerGlobal provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerGlobal exposed by              
// the CoClass dmServerGlobal. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerGlobal = class
    class function Create: IdmServerGlobal;
    class function CreateRemote(const MachineName: string): IdmServerGlobal;
  end;

implementation

uses ComObj;

class function CodmServerGlobal.Create: IdmServerGlobal;
begin
  Result := CreateComObject(CLASS_dmServerGlobal) as IdmServerGlobal;
end;

class function CodmServerGlobal.CreateRemote(const MachineName: string): IdmServerGlobal;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerGlobal) as IdmServerGlobal;
end;

end.
