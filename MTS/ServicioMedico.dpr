library ServicioMedico;

uses
  ComServ,
  ServerMedico_TLB in 'ServerMedico_TLB.pas',
  DServerMedico in 'DServerMedico.pas' {dmServerMedico: TMtsDataModule} {dmServerMedico: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
