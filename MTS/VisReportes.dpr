library VisReportes;

uses
  ComServ,
  VisReportes_TLB in 'VisReportes_TLB.pas',
  DServerVisReportes in 'DServerVisReportes.pas' {dmServerVisReportes: TMtsDataModule} {dmServerVisReportes: CoClass},
  DEntidadesTress in '..\VisitantesMGR\DEntidadesTress.pas' {dmEntidadesTress: TDataModule},
  ZCreator in '..\VisitantesMGR\ZCreator.pas',
  EditorDResumen in '..\Medicos\EditorDResumen.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
