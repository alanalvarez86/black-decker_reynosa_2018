unit DServerPlanCarrera;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, {BdeMts,} DataBkr, DBClient, DB,
  MtsRdm, Mtx, Mask,
  {$ifndef VER130}Variants,{$endif}
{$ifndef DOS_CAPAS}
  PlanCarrera_TLB,
{$endif}
  DZetaServerProvider,
  ZetaCommonLists,
  ZetaCommonClasses,
  ZetaSQLBroker,
  ZetaServerDataSet;

type
  eTipoTabla = ( eGeneral,
                 eTCompete,
                 eCalificacion,
                 eCompetencia,
                 eAccion,
                 ePuesto,
                 eFamilia,
                 eNivel,
                 eDimension,
                 eCompetenciaFamilia,
                 eNivelDimension,
                 eCompetenciasPuesto,
                 ePuestoDimensiones,
                 eCompetenciaMapa,
                 eCompetenciaCalifica );

  TdmServerPlanCarrera = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerPlanCarrera {$endif})
    cdsLista: TServerDataSet;
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    oZetaComparte: TdmZetaServerProvider;
    FFiltro: string;
    Fcodigo: string;
    FOrden: integer;
    function GetTabla( const eTabla: eTipoTabla; const Empresa: OleVariant ): OleVariant;
    function GrabaTabla(const eTabla: eTipoTabla; const Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
    procedure SetTablaInfo( eTabla : eTipoTabla );
    procedure AfterUpdateCalifica( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
    procedure BeforeUpdateCompetencia( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean );
    procedure BeforeUpdateCompetenciaMapa( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean );
    procedure BeforeUpdateCompetenciaCalifica( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean );
    procedure BeforeUpdateNivelDim(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateNiveles(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateFamilia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateCompetenciaFamilia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateAccion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateCompetenciaAccion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    {$ifndef DOS_CAPAS}
    procedure ServerActivate(Sender: TObject);
    procedure ServerDeactivate(Sender: TObject);
    {$endif}
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    {$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
    {$endif}
    function GetCatCompetencias(Empresa: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCatCalificaciones(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCatDimensiones(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCatAcciones(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCatNiveles(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCatFamilias(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCatPuestos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCatHabilidades(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCatCompetencias(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCatCalificaciones(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCatDimensiones(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCatAcciones(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetNiveles(Empresa: OleVariant; const Nivel: WideString; out oNivelDime: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCatNiveles(Empresa, oDelta, oNivelDim: OleVariant; out ErrorCount: Integer;  out oResultNivelDim: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function BorraNivel(Empresa: OleVariant; const Nivel: WideString; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetFamilia(Empresa: OleVariant; const Familia: WideString; out oCompFam: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCatFamilias(Empresa, oDelta, oCompFam: OleVariant; out ErrorCount: Integer; out oResultCompFam: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function BorraFamilia(Empresa: OleVariant; const Familia: WideString; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaNiveles(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaFamilias(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetPuestos(Empresa: OleVariant; const Puesto: WideString; out oCompetenciaPuesto, oPuestoDimension: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCatPuestos(Empresa, oDelta, oCompetenciaPuesto, oPuestoDimension: OleVariant; out ErrorCount: Integer; out oResultCompPto, oResultPtoDim: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCompetencia(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCatCompetencia(Empresa, oDelta, oCompetenciaMapa, oCompetenciaCalifica: OleVariant; out ErrorCount: Integer; out oResultMapa, oResultCalifica: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCatCompetencia(Empresa: OleVariant; const Competencia: WideString; out oCompetenciaMapa,   oCompetenciaCalifica: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function BorraCompetencia(Empresa: OleVariant; const Competencia: WideString; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetAccionesComp(Empresa: OleVariant; const Accion: WideString; out oCompAcc: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaAccionesComp(Empresa, oDelta, oCompAcc: OleVariant; out ErrorCount: Integer; out oResultCompAcc: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function BorraAccion(Empresa: OleVariant; const Accion: WideString; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
  end;

const
     K_CODIGO = 6;
     K_DESCRIPCION = 30;
     K_OPERACION = 10;
     K_BOOLEANO = 1;
     K_CODIGO_PARTE = 15;
     K_OBSERVACIONES = 50;
     K_NOMBRE = 60;
     K_TITULO = 100;
     K_FORMULA = 255;
     K_NOMBRE_MODULO = 'Plan de Carrera';
     K_CHECK_IMAGEN = 'Check.gif';
     K_UNCHECK_IMAGEN = 'NotCheck.gif';
     K_ASTERISCO = '*';
     K_PORCENTAJE = '%';
     aStatusFiltro: array[eStatusCursos] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( VACIO, 'and KC_FEC_TOM is NULL','and KC_FEC_TOM is not NULL' );
var
  dmServerPlanCarrera: TdmServerPlanCarrera;

implementation

uses ZetaServerTools,
     ZetaCommonTools;

{$R *.DFM}

type
    eScripts = (
                 qBorraNivel,
                 qCompetenciaFamilia,
                 qBorraFamilia,
                 qBorraCompetencia,
                 qGetMaximoCompMap,
                 qGetEscalaCalificacion,
                 qCompetenciaAccion,
                 qBorraAccion
                  );

function GetScript( const eSQLNumero: eScripts ): String;
begin
     case eSQLNumero of
         qBorraNivel: Result := 'Delete from NIV_PTO where NP_CODIGO = %s';
         qCompetenciaFamilia: Result := 'Select COMP_FAM.FP_CODIGO, COMP_FAM.CM_CODIGO, COMP_FAM.CF_OBSERVA, COMPETEN.CM_DESCRIP, COMPETEN.TC_CODIGO '+
                                        'from COMP_FAM left outer join COMPETEN on COMPETEN.CM_CODIGO = COMP_FAM.CM_CODIGO '+
                                        'where COMP_FAM.FP_CODIGO = %s';
         qBorraFamilia: Result := 'Delete from FAM_PTO where FP_CODIGO = %s';
         qBorraCompetencia: Result := 'Delete from COMPETEN where CM_CODIGO = %s';
         qGetMaximoCompMap: Result := 'select MAX(CM_ORDEN) + 1 MAXIMO from COMP_MAP where ( CM_CODIGO = %s )';
         qGetEscalaCalificacion: Result := 'Select COUNT(*) CUANTOS from CALIFICA where UPPER( CA_GRUPO ) = %s and ( CA_ORDEN = %d )';
         qCompetenciaAccion: Result :=  'select COMP_MAP.AN_CODIGO, COMP_MAP.CM_CODIGO, COMPETEN.CM_DESCRIP, COMP_MAP.CM_OBSERVA, COMP_MAP.CM_ORDEN '+
                                        'from COMP_MAP left outer join COMPETEN on (COMPETEN.CM_CODIGO = COMP_MAP.CM_CODIGO) '+
                                        'where COMP_MAP.AN_CODIGO = %s';
         qBorraAccion: Result := 'Delete from ACCION where AN_CODIGO = %s';
     else
         Result := VACIO;
     end;
end;

class procedure TdmServerPlanCarrera.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerPlanCarrera.MtsDataModuleCreate(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     {$ifdef antes}
     EnablePooling := ZetaServerTools.GetEnablePooling;
     {$endif}
     OnActivate := ServerActivate;
     OnDeactivate := ServerDeactivate;
     {$endif}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     oZetaComparte := DZetaServerProvider.GetZetaProvider( Self );
     FFiltro := VACIO;
     FCodigo := VACIO;
     FOrden := 0;
end;

procedure TdmServerPlanCarrera.MtsDataModuleDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
     DZetaServerProvider.FreeZetaProvider( oZetaComparte );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerPlanCarrera.CierraEmpresa;
begin
end;
{$else}
procedure TdmServerPlanCarrera.ServerActivate(Sender: TObject);
begin
     oZetaProvider.Activate;
     oZetaComparte.Activate;
end;

procedure TdmServerPlanCarrera.ServerDeactivate(Sender: TObject);
begin
     oZetaProvider.Deactivate;
     oZetaComparte.Deactivate;
end;
{$endif}

procedure TdmServerPlanCarrera.SetTablaInfo( eTabla: eTipoTabla );
begin
  with oZetaProvider.TablaInfo do
     case eTabla of
          eTCompete            : SetInfo( 'TCOMPETE', 'TC_CODIGO,TC_DESCRIP,TC_INGLES,TC_NUMERO,TC_TEXTO,TC_TIPO', 'TC_CODIGO' );
          eCalificacion        : SetInfo( 'CALIFICA', 'CA_CODIGO,CA_DESCRIP,CA_INGLES,CA_NUMERO,CA_TEXTO,CA_ORDEN,CA_GRUPO', 'CA_CODIGO' );
          eDimension           : SetInfo( 'DIMENSIO', 'DM_CODIGO,DM_DESCRIP,DM_INGLES,DM_NUMERO,DM_TEXTO', 'DM_CODIGO' );
          eAccion              : SetInfo( 'ACCION', 'AN_CODIGO,CU_CODIGO,AN_NOMBRE,AN_INGLES,AN_NUMERO,AN_TEXTO,AN_CLASE,AN_TIP_MAT,AN_DETALLE,AN_DIAS,AN_URL', 'AN_CODIGO' );
          eNivel               : SetInfo( 'NIV_PTO', 'NP_CODIGO,NP_DESCRIP,NP_INGLES,NP_NUMERO,NP_TEXTO,NP_ACTITUD,NP_DETALLE', 'NP_CODIGO' );
          eFamilia             : SetInfo( 'FAM_PTO', 'FP_CODIGO,FP_DESCRIP,FP_INGLES,FP_NUMERO,FP_TEXTO', 'FP_CODIGO' );
          ePuesto              : SetInfo( 'PUESTO', 'PU_CODIGO,PU_DESCRIP,PU_DETALLE,FP_CODIGO,NP_CODIGO', 'PU_CODIGO' );
          eCompetencia         : SetInfo( 'COMPETEN', 'CM_CODIGO,CM_DESCRIP,CM_INGLES,CM_NUMERO,CM_TEXTO,CM_DETALLE,TC_CODIGO', 'CM_CODIGO' );
          eNivelDimension      : SetInfo( 'NIV_DIME', 'DM_CODIGO,NP_CODIGO,ND_RESUMEN,ND_DESCRIP', 'DM_CODIGO,NP_CODIGO' );
          eCompetenciaFamilia  : SetInfo( 'COMP_FAM', 'FP_CODIGO,CM_CODIGO,CF_OBSERVA', 'FP_CODIGO,CM_CODIGO' );
          eCompetenciasPuesto  : SetInfo( 'COMP_PTO', 'PU_CODIGO,CM_CODIGO,CA_CODIGO,MP_DESCRIP', 'PU_CODIGO,CM_CODIGO' );
          ePuestoDimensiones   : SetInfo( 'PTO_DIME', 'PU_CODIGO,DM_CODIGO,PD_DESCRIP', 'PU_CODIGO,DM_CODIGO' );
          eCompetenciaMapa     : SetInfo( 'COMP_MAP', 'CM_CODIGO,AN_CODIGO,CM_OBSERVA,CM_ORDEN', 'CM_CODIGO,AN_CODIGO' );
          eCompetenciaCalifica : SetInfo( 'COMP_CAL', 'CM_CODIGO,CA_CODIGO,MC_DESCRIP', 'CM_CODIGO,CA_CODIGO' );
     end;
end;

function TdmServerPlanCarrera.GetTabla( const eTabla: eTipoTabla; const Empresa: OleVariant ): OleVariant;
begin
     SetTablaInfo( eTabla );
     with oZetaProvider do
     begin
          Result := oZetaProvider.GetTabla( Empresa );
     end;
end;

function TdmServerPlanCarrera.GrabaTabla(const eTabla: eTipoTabla; const Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eTabla );
     with oZetaProvider do
     begin
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
end;

function TdmServerPlanCarrera.GetCatCompetencias( Empresa: OleVariant ): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GetTabla( eTCompete, Empresa );
     SetComplete;
end;

function TdmServerPlanCarrera.GetCatCalificaciones( Empresa: OleVariant ): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.SetOrder( 'CA_GRUPO,CA_ORDEN' );
     end;
     Result := GetTabla( eCalificacion, Empresa );
     SetComplete;
end;

function TdmServerPlanCarrera.GetCatDimensiones( Empresa: OleVariant ): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GetTabla( eDimension, Empresa );
     SetComplete;
end;

function TdmServerPlanCarrera.GetCatAcciones( Empresa: OleVariant ): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GetTabla( eAccion, Empresa );
     SetComplete;
end;

function TdmServerPlanCarrera.GetCatNiveles( Empresa: OleVariant ): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GetTabla( eNivel, Empresa );
     SetComplete;
end;

function TdmServerPlanCarrera.GetCatFamilias( Empresa: OleVariant ): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GetTabla( eFamilia, Empresa );
     SetComplete;
end;

function TdmServerPlanCarrera.GetCatPuestos( Empresa: OleVariant ): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GetTabla( ePuesto, Empresa );
     SetComplete;
end;

function TdmServerPlanCarrera.GetCatHabilidades( Empresa: OleVariant ): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GetTabla( eCompetencia, Empresa );
     SetComplete;
end;

function TdmServerPlanCarrera.GrabaCatCompetencias(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GrabaTabla( eTCompete, Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerPlanCarrera.GrabaCatCalificaciones(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( eCalificacion );
          TablaInfo.AfterUpdateRecord := AfterUpdateCalifica;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

procedure TdmServerPlanCarrera.AfterUpdateCalifica( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
var
   FQuery: TZetaCursor;
   iOrden, iCuantos: integer;
   sGrupo: string;
begin
     if ( UpdateKind in [ ukModify, ukInsert ] ) then
     begin
          with DeltaDS do
          begin
               if ( CambiaCampo( FieldByName( 'CA_GRUPO' ) ) or CambiaCampo( FieldByName( 'CA_ORDEN' ) ) ) then
               begin
                    sGrupo := CampoAsVar( FieldByName( 'CA_GRUPO' ) );
                    iOrden := CAmpoAsVar( FieldByName( 'CA_ORDEN' ) );
                    sGrupo := UpperCase( sGrupo );
                    FQuery := oZetaProvider.CreateQuery( Format( GetScript( qGetEscalaCalificacion ), [ EntreComillas( sGrupo ), iOrden ] ) );
                    FQuery.Active := TRUE;
                    iCuantos := FQuery.Fields[ 0 ].AsInteger;
                    FQuery.Active := FALSE;
                    if ( iCuantos > 1 ) then
                    begin
                         DatabaseError( Format( 'ˇ Ya Existe una Calificación con la Misma Posición en la Escala: %s !', [ sGrupo ] ) );
                    end;
               end;
          end;
     end;
end;

function TdmServerPlanCarrera.GrabaCatDimensiones(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GrabaTabla( eDimension, Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerPlanCarrera.GrabaCatAcciones(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GrabaTabla( eAccion, Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerPlanCarrera.GetNiveles(Empresa: OleVariant; const Nivel: WideString; out oNivelDime: OleVariant): OleVariant;
begin
     FFiltro := Format( 'NP_CODIGO=%s', [ EntreComillas( Nivel ) ] );
     SetTablaInfo( eNivel );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.Filtro := FFiltro;
          Result := GetTabla( Empresa );
          SetTablaInfo( eNivelDimension );
          TablaInfo.Filtro := FFiltro;
          oNivelDime := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerPlanCarrera.GrabaCatNiveles(Empresa, oDelta, oNivelDim: OleVariant; out ErrorCount: Integer;  out oResultNivelDim: OleVariant): OleVariant;
var
   iErrorCountNivelDim: Integer;
begin
     SetOLEVariantToNull( Result );
     SetOLEVariantToNull( oResultNivelDim );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( oDelta ) then
          begin
               SetTablaInfo( eNivel );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateNiveles;
               Result := GrabaTabla( Empresa, oDelta, ErrorCOunt );
          end;
          if ( ErrorCount = 0 ) and not VarIsNull( oNivelDim ) then
          begin
               SetTablaInfo( eNivelDimension );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateNivelDim;
               oResultNivelDim := GrabaTabla( Empresa, oNivelDim, iErrorCountNivelDim );
          end;
     end;
     SetComplete;
end;

procedure TdmServerPlanCarrera.BeforeUpdateNiveles(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          with DeltaDS do
          begin
               FCodigo := CampoAsVar( FieldByName( 'NP_CODIGO' ) );
          end;
     end;
end;

procedure TdmServerPlanCarrera.BeforeUpdateNivelDim(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          with DeltaDS do
          begin
               if strLleno( FCodigo ) then
               begin
                    Edit;
                    FieldByName( 'NP_CODIGO' ).AsString := FCodigo;
                    Post;
               end;
          end;
     end;
end;

function TdmServerPlanCarrera.BorraNivel(Empresa: OleVariant; const Nivel: WideString; out ErrorCount: Integer): OleVariant;
begin
     Result := NULL;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          try
             EmpiezaTransaccion;
             ExecSQL( Empresa, Format( GetScript( qBorraNivel ), [ EntreComillas( Nivel ) ] ) );
             TerminaTransaccion(True);
          except
                TerminaTransaccion(False);
          end;
     end;
     SetComplete;
end;

function TdmServerPlanCarrera.GetFamilia(Empresa: OleVariant; const Familia: WideString; out oCompFam: OleVariant): OleVariant;
begin
     FFiltro := Format( 'FP_CODIGO=%s', [ EntreComillas( Familia ) ] );
     SetTablaInfo( eFamilia );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.Filtro := FFiltro;
          Result := GetTabla( Empresa );
          oCompFam := OpenSQL( Empresa, Format( GetScript( qCompetenciaFamilia ), [ EntreComillas( Familia ) ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerPlanCarrera.GrabaCatFamilias(Empresa, oDelta, oCompFam: OleVariant; out ErrorCount: Integer; out oResultCompFam: OleVariant): OleVariant;
var
   iErrorCountCompFam: Integer;
begin
     SetOLEVariantToNull( Result );
     SetOLEVariantToNull( oResultCompFam );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( oDelta ) then
          begin
               SetTablaInfo( eFamilia );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateFamilia;
               Result := GrabaTabla( Empresa, oDelta, ErrorCOunt );
          end;
          if ( ErrorCount = 0 ) and not VarIsNull( oCompFam ) then
          begin
               SetTablaInfo( eCompetenciaFamilia );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateCompetenciaFamilia;
               oResultCompFam := GrabaTabla( Empresa, oCompFam, iErrorCountCompFam );
          end;
     end;
     SetComplete;
end;

procedure TdmServerPlanCarrera.BeforeUpdateFamilia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          with DeltaDS do
          begin
               FCodigo := CampoAsVar( FieldByName( 'FP_CODIGO' ) );
          end;
     end;
end;

procedure TdmServerPlanCarrera.BeforeUpdateCompetenciaFamilia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          with DeltaDS do
          begin
               if strLleno( FCodigo ) then
               begin
                    Edit;
                    FieldByName( 'FP_CODIGO' ).AsString := FCodigo;
                    Post;
               end;
          end;
     end;
end;

function TdmServerPlanCarrera.BorraFamilia(Empresa: OleVariant; const Familia: WideString; out ErrorCount: Integer): OleVariant;
begin
     Result := NULL;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          try
             EmpiezaTransaccion;
             ExecSQL( Empresa, Format( GetScript( qBorraFamilia ), [ EntreComillas( Familia ) ] ) );
             TerminaTransaccion(True);
          except
                TerminaTransaccion(False);
          end;
     end;
     SetComplete;
end;

function TdmServerPlanCarrera.GrabaNiveles(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GrabaTabla( eNivel, Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerPlanCarrera.GrabaFamilias(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GrabaTabla( eFamilia, Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerPlanCarrera.GetPuestos(Empresa: OleVariant; const Puesto: WideString; out oCompetenciaPuesto, oPuestoDimension: OleVariant): OleVariant;
begin
     FFiltro := Format( 'PU_CODIGO=%s', [ EntreComillas( Puesto ) ] );
     SetTablaInfo( ePuesto );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.Filtro := FFiltro;
          Result := GetTabla( Empresa );

          SetTablaInfo( eCompetenciasPuesto );
          TablaInfo.Filtro := FFiltro;
          oCompetenciaPuesto := GetTabla( Empresa );

          SetTablaInfo( ePuestoDimensiones );
          TablaInfo.Filtro := FFiltro;
          oPuestoDimension := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerPlanCarrera.GrabaCatPuestos(Empresa, oDelta, oCompetenciaPuesto, oPuestoDimension: OleVariant; out ErrorCount: Integer; out oResultCompPto,
                                              oResultPtoDim: OleVariant): OleVariant;
var
   ErrorCountPto, ErrorCountDim : Integer;
begin
     SetOLEVariantToNull( Result );
     SetOLEVariantToNull( oResultCompPto );
     SetOLEVariantToNull( oResultPtoDim );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( oDelta ) then
          begin
               SetTablaInfo( ePuesto );
               Result := GrabaTabla( Empresa, oDelta, ErrorCOunt );
          end;
          if ( ErrorCount = 0 ) and not VarIsNull( oCompetenciaPuesto ) then
          begin
               SetTablaInfo( eCompetenciasPuesto );
               {cdsLista.Data := oCompetenciaPuesto;
               cdsLista.Savetofile('d:\marcos.cds');}
               oResultCompPto := GrabaTabla( Empresa, oCompetenciaPuesto, ErrorCountPto );
          end;
          if ( ErrorCount = 0 ) and not VarIsNull( oPuestoDimension ) then
          begin
               SetTablaInfo( ePuestoDimensiones );
               oResultPtoDim := GrabaTabla( Empresa, oPuestoDimension, ErrorCountDim );
          end;
     end;
     SetComplete;
end;

function TdmServerPlanCarrera.GrabaCompetencia(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GrabaTabla( eCompetencia, Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerPlanCarrera.GrabaCatCompetencia(Empresa, oDelta, oCompetenciaMapa, oCompetenciaCalifica: OleVariant;
                                                           out ErrorCount: Integer; out oResultMapa, oResultCalifica: OleVariant): OleVariant;
var
   ErrorCountMapa, ErrorCountCalif : Integer;
begin
     SetOLEVariantToNull( Result );
     SetOLEVariantToNull( oResultMapa );
     SetOLEVariantToNull( oResultCalifica );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( oDelta ) then
          begin
               SetTablaInfo( eCompetencia );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateCompetencia;
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          end;
          if ( ErrorCount = 0 ) and not VarIsNull( oCompetenciaMapa ) then
          begin
               SetTablaInfo( eCompetenciaMapa );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateCompetenciaMapa;
               oResultMapa := GrabaTabla( Empresa, oCompetenciaMapa, ErrorCountMapa );
          end;
          if ( ErrorCount = 0 ) and not VarIsNull( oCompetenciaCalifica ) then
          begin
               SetTablaInfo( ecompetenciaCalifica );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateCompetenciaCalifica;
               oResultCalifica := GrabaTabla( Empresa, oCompetenciaCalifica, ErrorCountCalif );
          end;
     end;
     SetComplete;
end;

procedure TdmServerPlanCarrera.BeforeUpdateCompetencia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          with DeltaDS do
          begin
               FCodigo := CampoAsVar( FieldByName( 'CM_CODIGO' ) );
          end;
     end;
end;

procedure TdmServerPlanCarrera.BeforeUpdateCompetenciaMapa( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean );
var
   FQuery: TZetaCursor;
   sCodigo, sOrden: string;
begin
     if ( UpdateKind in [ ukInsert ] ) then
     begin
          with DeltaDS do
          begin
               sOrden := CampoAsVar( FieldByName( 'CM_ORDEN' ) );
               sCodigo := CampoAsVar( FieldByName( 'CM_CODIGO' ) );

               if( strLleno( FCodigo ) )then
               begin
                    Edit;
                    FieldByName( 'CM_CODIGO' ).AsString := FCodigo;
                    Post;
               end;

               if ( sOrden = '0' ) or strVacio( sOrden ) then
               begin
                    FQuery := oZetaProvider.CreateQuery( Format( GetScript( qGetMaximoCompMap ), [ EntreComillas( sCodigo ) ] ) );
                    FQuery.Active := TRUE;
                    sOrden := FQuery.Fields[ 0 ].AsString;
                    FQuery.Active := FALSE;
                    if strVacio( sOrden ) then
                       sOrden := '0';
                    if ( strToInt( sOrden ) > FOrden ) Then
                       FOrden := StrToInt( sOrden )
                    else
                        FOrden := FOrden + 1;
                    Edit;
                    FieldByName( 'CM_ORDEN' ).AsString := IntToStr( FOrden );
                    Post;
               end;
          end;
     end;
end;

procedure TdmServerPlanCarrera.BeforeUpdateCompetenciaCalifica( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean );
begin
     if ( UpdateKind in [ ukInsert ] ) then
     begin
          if( strLleno( FCodigo ) )then
          begin
               with DeltaDS do
               begin
                    Edit;
                    FieldByName( 'CM_CODIGO' ).AsString := FCodigo;
                    Post;
               end;
          end;
     end;
end;


function TdmServerPlanCarrera.GetCatCompetencia(Empresa: OleVariant;
  const Competencia: WideString; out oCompetenciaMApa,
  oCompetenciaCalifica: OleVariant): OleVariant;
begin
     FFiltro := Format( 'CM_CODIGO=%s', [ EntreComillas( Competencia ) ] );
     SetTablaInfo( eCompetencia );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.Filtro := FFiltro;
          Result := GetTabla( Empresa );

          SetTablaInfo( eCompetenciaMapa );
          with TablaInfo do
          begin
               Filtro := FFiltro;
               SetOrder( 'CM_ORDEN' );
          end;
          oCompetenciaMApa := GetTabla( Empresa );

          SetTablaInfo( eCompetenciaCalifica );
          TablaInfo.Filtro := FFiltro;
          oCompetenciaCalifica := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerPlanCarrera.BorraCompetencia(Empresa: OleVariant; const Competencia: WideString; out ErrorCount: Integer): OleVariant;
begin
     Result := NULL;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          try
             EmpiezaTransaccion;
             ExecSQL( Empresa, Format( GetScript( qBorraCompetencia ), [ EntreComillas( Competencia ) ] ) );
             TerminaTransaccion(True);
          except
                TerminaTransaccion(False);
          end;
     end;
     SetComplete;
end;

function TdmServerPlanCarrera.GetAccionesComp(Empresa: OleVariant; const Accion: WideString; out oCompAcc: OleVariant): OleVariant;
begin
     FFiltro := Format( 'AN_CODIGO=%s', [ EntreComillas( Accion ) ] );
     SetTablaInfo( eAccion );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.Filtro := FFiltro;
          Result := GetTabla( Empresa );
          oCompAcc := OpenSQL( Empresa, Format( GetScript( qCompetenciaAccion ), [ EntreComillas( Accion ) ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerPlanCarrera.GrabaAccionesComp(Empresa, oDelta, oCompAcc: OleVariant; out ErrorCount: Integer; out oResultCompAcc: OleVariant): OleVariant;
var
   iErrorCountCompAcc: Integer;
begin
     SetOLEVariantToNull( Result );
     SetOLEVariantToNull( oResultCompAcc );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( oDelta ) then
          begin
               SetTablaInfo( eAccion );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateAccion;
               Result := GrabaTabla( Empresa, oDelta, ErrorCOunt );
          end;
          if ( ErrorCount = 0 ) and not VarIsNull( oCompAcc ) then
          begin
               SetTablaInfo( eCompetenciaMapa );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateCompetenciaAccion;
               oResultCompAcc := GrabaTabla( Empresa, oCompAcc, iErrorCountCompAcc );
          end;
     end;
     SetComplete;
end;

procedure TdmServerPlanCarrera.BeforeUpdateAccion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          with DeltaDS do
          begin
               FCodigo := CampoAsVar( FieldByName( 'AN_CODIGO' ) );
          end;
     end;
end;

procedure TdmServerPlanCarrera.BeforeUpdateCompetenciaAccion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          with DeltaDS do
          begin
               if strLleno( FCodigo ) then
               begin
                    Edit;
                    FieldByName( 'AN_CODIGO' ).AsString := FCodigo;
                    Post;
               end;
          end;
     end;
end;

function TdmServerPlanCarrera.BorraAccion(Empresa: OleVariant; const Accion: WideString; out ErrorCount: Integer): OleVariant;
begin
     SetOLEVariantToNull( Result );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          try
             EmpiezaTransaccion;
             ExecSQL( Empresa, Format( GetScript( qBorraAccion ), [ EntreComillas( Accion ) ] ) );
             TerminaTransaccion(True);
          except
                TerminaTransaccion(False);
          end;
     end;
     SetComplete;
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerPlanCarrera,
    Class_dmServerPlanCarrera, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}

end.

