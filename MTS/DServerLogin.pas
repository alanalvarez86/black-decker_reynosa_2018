unit DServerLogin;

interface
{$define VALIDAEMPLEADOSGLOBAL}

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, ComServ, ComObj, VCLCom,
     StdVcl, DataBkr, DBClient, MtsRdm, Mtx, Db, FAutoServer,
     Variants, ZetaServerDataSet,
     {$ifndef DOS_CAPAS}
     Login_TLB,
     {$endif}
     DZetaServerProvider,
     ZetaSQLBroker;

{$define QUINCENALES}

{.$undefine QUINCENALES}

type
  TdmServerLogin = class( TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerLogin {$endif} )
    cdsTempo: TServerDataSet;
    procedure dmServerLoginCreate(Sender: TObject);
    procedure dmServerLoginDestroy(Sender: TObject);
  private
    { Private declarations }
    {$ifdef BITACORA_DLLS}
    FListaLog : TAsciiLog;
    FPaletita : string;
    FArchivoLog: string;
    FEmpresa:string;
    {$endif}
    oZetaProvider: TdmZetaServerProvider;
    {$ifndef DOS_CAPAS}
    FAuto: OleVariant;
    {$endif}
    function GetNominaEmpleado(Empresa, Parametros: OleVariant; var Datos: OleVariant; const iQuery: Integer): WordBool;
    function GetDiasExpiracion: Integer;
    function GetIntentos: Integer;
    function GetDiasInactivo: Integer;
    function GetTiempoInactivo(const lActiveDirectory:Boolean = FALSE): Integer;
    function GetLimitePassword: Integer;
    function GetMinLetrasPwd: Integer;
    function GetMinDigitosPwd: Integer;
    function GetMaxHistoriaPwd: Integer;
    function GetUsuarioTressAutomatiza: Integer; // (JB) Se agrega Configuracion de Tress Automatiza
    function GetMostrarAdvertenciaLicencia: Boolean; // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
    function GetServidorCorreos: String;             // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
    function GetPuertoSMTP: Integer;                 // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
    function GetAutentificacionCorreo: Integer;      // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
    function GetUserID: String;                      // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
    function GetEmailPSWD: String;                   // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
    function GetRecibirAdvertenciaPorEmail: Boolean;  // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
    function GetGrupoUsuariosEmail: Integer;          // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
    // US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
    function GetDirectorioServicioReportes: String;
    function GetDirectorioImagenes: String;
    function GetURLImagenes: String;
    function GetExpiracionImagenes: Integer;
    function GetRecuperarClaveServicioCorreos: Boolean;

    function LeerClaves( const iNumero: Integer ): String;
    function ReadAutoServer: OleVariant;
    function EscribeProximidad( oEmpresa: OleVariant; SQL: string; var oDatos: OleVariant ): Boolean;
    function GetSistemaBloqueo: Boolean;
    procedure AutoGetData(Sender: TObject; var StringData: String);
    procedure AutoSetData(Sender: TObject; var StringData: String);
    procedure Ejecuta(const sSQL: String);
    procedure EscribeClaves( Empresa: OleVariant; const iNumero: Integer; const sTexto: String);
    procedure EscribeClavesTx(const iNumero: Integer; const sTexto: String);
    procedure BloqueaUsuario(const sUser, sComputerName: String; const iIntentos: Integer);
    procedure EscribeBitacora(Empresa: OleVariant; const iNumero: Integer;const sGlobalNuevo: string);
    procedure InitLog( Empresa:Olevariant; const sPaletita: string );
    procedure EndLog;
    {$ifndef DOS_CAPAS}
    procedure ServerActivate(Sender: TObject);
    procedure ServerDeactivate(Sender: TObject);
    {$endif}
    function UsuarioCargar(lActiveDirectory: Boolean; const NombreCorto, Clave, ComputerName: WideString;
             AllowReentry: WordBool; var Datos, Autorizacion: OleVariant; var Intentos: Integer): Integer;
    function GetNavegacion( Empresa: OleVariant; sNavegacion: String ): String;
    function ValidarServerDB(Empresa:OleVariant): Boolean;

    function SQL_Injection (sNombre_Corto: String): Boolean;

  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;



{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetCompanys(Usuario, Tipo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPatrones(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetAccesos(Grupo: Integer; const Company: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetAuto: OleVariant; safecall;
    function GetEmpleado(Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpleadoAnterior(Empresa: OleVariant; Empleado: Integer; const sNavega: WideString; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpleadoSiguiente(Empresa: OleVariant; Empleado: Integer; const sNavega: WideString; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpleadosBuscados(Empresa: OleVariant; const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpleadosBuscados_DevEx(Empresa: OleVariant; const sPista: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    {$ifndef SELECCION}
    {$ifndef VISITANTES}
    function GetPeriodoInicial(Empresa: OleVariant; Year, Tipo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    {$endif}
    {$endif}
    function GetPeriodo(Empresa: OleVariant; Year, Tipo, Numero: Integer; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPeriodoAnterior(Empresa: OleVariant; Year, Tipo, Numero: Integer; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPeriodoSiguiente(Empresa: OleVariant; Year, Tipo, Numero: Integer; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function UsuarioLogin(const NombreCorto, Clave, ComputerName: WideString; AllowReentry: WordBool; out Datos, Autorizacion: OleVariant; out Intentos: Integer): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function UsuarioLogout(Usuario: Integer): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetLookupEmpleado(Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant; TipoLookup: Integer): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetNominaSiguiente(Empresa, Parametros: OleVariant; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetNominaAnterior(Empresa, Parametros: OleVariant; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSeguridad: OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetSeguridadEmpresa(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure SetSeguridad(Empresa, Valores: OleVariant); {$ifndef DOS_CAPAS} safecall; {$endif}
    procedure UsuarioCambiaPswd(Usuario: Integer; const ClaveActual, ClaveNueva: WideString); {$ifndef DOS_CAPAS} safecall; {$endif}
    procedure UsuarioBloquea(const NombreCorto, ComputerName: WideString; Intentos: Integer); {$ifndef DOS_CAPAS} safecall;  {$endif}
    procedure GrabaBloqueoSistema(lValorBloqueo: WordBool); {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetClave( Clave: Integer): WideString;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetTipoLogin: Integer; {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure GrabaTipoLogin(TipoLogin: Integer); {$ifndef DOS_CAPAS} safecall;{$endif}
    function LoginAD(const ComputerName: WideString; AllowReentry: WordBool;out Tipo: Integer; out Datos, Autorizacion: OleVariant; const sDominioUsuario: WideString): Integer; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetTipoPeriodo(Empresa: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}//acl
    function UsuarioLoginConsola(const Usuario, Clave: WideString; ActiveDir: WordBool; Intentos: Integer; const EquipoLocal: WideString): WideString;{$ifndef DOS_CAPAS} safecall;{$endif}
    function CheckDBServer(Empresa: OleVariant): Integer; {$ifndef DOS_CAPAS} safecall;{$endif}
    function CheckVersionDatos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;
    function GetCompanysList(Usuario: Integer; Lista: OleVariant): OleVariant; safecall;
          {$endif}
    function GetNotificacionAdvertencia: OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    // US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
    procedure SetNotificacionAdvertencia(Empresa, Valores: OleVariant); {$ifndef DOS_CAPAS} safecall;
    function GetServicioReportesCorreos: OleVariant; safecall;
    procedure SetServicioReportesCorreos(Empresa, Valores: OleVariant); safecall;
    function EnviaClaveTemporal(const sUsuario, sClaveCifrada,
      sCorreoRemitente, sDescripRemitente,
      sCorreoValidar: WideString): WordBool; safecall;{$endif}
    function GetStatusTimbrado(Empresa, Parametros: OleVariant): OleVariant; safecall;
    procedure AnaliticaContabilizarUso(Empresa: OleVariant;
      const sModulo: WideString; HelpContext, Conteo: Integer); safecall;
  end;

implementation

{$R *.DFM}

uses
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaServerTools,
     ZAccesosTress,
     ZGlobalTress,
     DSeguridad,
     ZetaRegistryServer,
{$ifdef VALIDAEMPLEADOSGLOBAL}
     ZetaLicenseClasses,
     ZetaLicenseMgr,
{$endif}
     ZEvaluador,
     ZFiltroSQLTools,
     MotorPatchUtils,
     FAutoClasses;

const
     Q_VA_EMPLEADO              = 1;
     Q_VA_EMPLEADO_ANTERIOR     = 2;
     Q_VA_EMPLEADO_SIGUIENTE    = 3;
     Q_VA_PATRONES              = 4;
     Q_VA_PERIODO               = 5;
     Q_VA_PERIODO_INICIAL       = 6;
     Q_VA_PERIODO_ANTERIOR      = 7;
     Q_VA_PERIODO_SIGUIENTE     = 8;
     Q_VA_USUARIO               = 9;
     Q_VA_USUARIO_LOG_IN        = 10;
     Q_VA_USUARIO_LOG_OUT       = 11;
     Q_VA_USUARIO_CAMBIA_PSWD   = 12;
     Q_VA_USUARIO_BLOQUEA       = 13;
     Q_VA_COMPANYS              = 14;
     Q_VA_COMPANYS_TODAS        = 15;
     Q_VA_ACCESOS               = 16;
     Q_EMPLEADOS_BUSCADOS       = 17;
     Q_LOOKUP_EMPLEADO          = 18;
     Q_VA_NOMINA_ANTERIOR       = 19;
     Q_VA_NOMINA_SIGUIENTE      = 20;
     Q_VA_LEER_CLAVES           = 21;
     Q_VA_BORRA_CLAVES          = 22;
     Q_VA_INSERTA_CLAVES        = 23;
     Q_VA_ACTUAL_VENCIMIENTO    = 24;
     Q_VA_ACTUAL_PASSWORD       = 25;
     Q_VA_USUARIO_LOG_IN_PORTAL = 26;
     Q_VA_NAVEGACION            = 27;
     Q_VA_USUARIO_CONSOLA       = 28;
     //DevEx: Constante agregada para el Query de la nueva Busqueda de Empleados
     Q_EMPLEADOS_BUSCADOS_DEV_EX = 29;
     Q_ANALITICA_CONTABILIDAD_USO = 30;

     CAMPOS_COLABORA = 'select ' + K_PRETTYNAME + ' as PrettyName, ' +
                       'CB_CODIGO,CB_ACTIVO,CB_APE_MAT,CB_APE_PAT,CB_AUTOSAL,CB_BAN_ELE,CB_CARRERA,CB_CHECA,CB_CIUDAD,CB_CLASIFI,CB_CODPOST,CB_CONTRAT,CB_CREDENC,CB_CURP,CB_CALLE,'+
                       'CB_COLONIA,CB_EDO_CIV,CB_FEC_RES,CB_EST_HOR,CB_EST_HOY,CB_ESTADO,CB_ESTUDIO,CB_EVALUA,CB_EXPERIE,CB_FEC_ANT,CB_FEC_BAJ,CB_FEC_BSS,CB_FEC_CON,CB_FEC_ING,CB_FEC_INT,'+
                       'CB_FEC_NAC,CB_FEC_REV,CB_FEC_VAC,CB_G_FEC_1,CB_G_FEC_2,CB_G_FEC_3,CB_G_LOG_1,CB_G_LOG_2,CB_G_LOG_3,CB_G_NUM_1,CB_G_NUM_2,CB_G_NUM_3,CB_G_TAB_1,CB_G_TAB_2,CB_G_TAB_3,'+
                       'CB_G_TAB_4,CB_G_TEX_1,CB_G_TEX_2,CB_G_TEX_3,CB_G_TEX_4,CB_HABLA,CB_TURNO,CB_IDIOMA,CB_INFCRED,CB_INFMANT,CB_INFTASA,CB_LA_MAT,CB_LAST_EV,CB_LUG_NAC,CB_MAQUINA,'+
                       'CB_MED_TRA,CB_PASAPOR,CB_FEC_INC,CB_FEC_PER,CB_NOMYEAR,CB_NACION,CB_NOMTIPO,CB_NEXT_EV,CB_NOMNUME,CB_NOMBRES,CB_PATRON,CB_PUESTO,CB_RFC,CB_SAL_INT,CB_SALARIO,'+
                       'CB_SEGSOC,CB_SEXO,CB_TABLASS,CB_TEL,CB_VIVECON,CB_VIVEEN,CB_ZONA,CB_ZONA_GE,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_INFTIPO,'+
                       'CB_NIVEL8,CB_NIVEL9,'+
                       {$ifdef ACS}'CB_NIVEL10, CB_NIVEL11, CB_NIVEL12,'+{$endif}
                       'CB_DER_FEC,CB_DER_PAG,CB_V_PAGO,CB_DER_GOZ,CB_V_GOZO,CB_TIP_REV,CB_MOT_BAJ,CB_FEC_SAL,CB_INF_INI,CB_INF_OLD,CB_OLD_SAL,CB_OLD_INT,CB_PRE_INT,'+
                       'CB_PER_VAR, CB_SAL_TOT, CB_TOT_GRA, CB_FAC_INT, CB_RANGO_S, CB_CLINICA, CB_NIVEL0, CB_AREA, CB_CANDIDA, CB_ID_NUM, CB_ENT_NAC, CB_COD_COL,CB_G_FEC_4,CB_G_FEC_5,'+
                       {$ifndef DOS_CAPAS}'CB_ID_BIO, CB_GP_COD, '+{$endif} // SYNERGY
                       'CB_G_FEC_6,CB_G_FEC_7,CB_G_FEC_8,CB_G_LOG_4,CB_G_LOG_5,CB_G_LOG_6,CB_G_LOG_7,CB_G_LOG_8,CB_G_NUM_4,CB_G_NUM_5,CB_G_NUM_6,CB_G_NUM_7,CB_G_NUM_8,CB_G_NUM_9,CB_G_NUM10,'+
                       'CB_G_NUM11,CB_G_NUM12,CB_G_NUM13,CB_G_NUM14,CB_G_NUM15,CB_G_NUM16,CB_G_NUM17,CB_G_NUM18,CB_G_TAB_5,CB_G_TAB_6,CB_G_TAB_7,CB_G_TAB_8,CB_G_TAB_9,CB_G_TAB10,CB_G_TAB11,'+
                       'CB_G_TAB12,CB_G_TAB13,CB_G_TAB14,CB_G_TEX_5,CB_G_TEX_6,CB_G_TEX_7,CB_G_TEX_8,CB_G_TEX_9,CB_G_TEX10,CB_G_TEX11,CB_G_TEX12,CB_G_TEX13,CB_G_TEX14,CB_G_TEX15,CB_G_TEX16,'+
                       'CB_G_TEX17,CB_G_TEX18,CB_G_TEX19,CB_SUB_CTA,CB_NETO, CB_DER_PV, CB_V_PRIMA, CB_PLAZA, CB_NOMINA,CB_RECONTR,CB_DISCAPA,CB_INDIGE,CB_FONACOT,CB_EMPLEO,US_CODIGO, CB_FEC_COV, ' +
                       'CB_G_TEX20,CB_G_TEX21,CB_G_TEX22,CB_G_TEX23,CB_G_TEX24,CB_INFACT,CB_INFDISM,CB_FEC_NOM,CB_NUM_INT, CB_NUM_EXT,CB_INF_ANT,CB_TDISCAP,CB_ESCUELA,CB_TESCUEL,CB_TITULO,CB_YTITULO,'+
                       'CB_CTA_GAS,CB_CTA_VAL,CB_MUNICIP,CB_TSANGRE,CB_ALERGIA,CB_BRG_ACT,CB_BRG_TIP,CB_BRG_ROL,CB_BRG_JEF,CB_BRG_CON,CB_BRG_PRA,CB_BRG_NOP,CB_E_MAIL,CB_BANCO,CB_REGIMEN, CB_MIFARE from COLABORA ';



var
    aSQL_INJECTION: array[ 0..52 ] of String =
       ('ADD','ALTER','AND','ASC','BETWEEN','BY','CASE','COALESCE','COLLATE','CONTAINS','CREATE','CURRENT_DATE',
       'CURRENT_TIME','CURRENT_TIMESTAMP','CURRENT_USER','CURSOR','DEFAULT','DELETE','DESC','DISTINCT','DROP','ELSE','END',
       'EXCEPT','EXEC','EXECUTE','EXISTS','FROM','FUNCTION','GROUP','HAVING','IF','INNER','INSERT','INTERSECT','JOIN','LEFT',
       'LIKE','NOT','NULL','OR','ORDER','OUTER','RETURN','ROWCOUNT','SELECT','SET','TABLE','THEN','UNION','UPDATE','WHEN','WHERE');

function GetSQLTextCase( const iSQLNumero: Integer ): String;
begin
     case iSQLNumero of
          Q_VA_EMPLEADO: Result := CAMPOS_COLABORA + 'where ( CB_CODIGO = %d ) %s';
          Q_VA_EMPLEADO_ANTERIOR: Result := CAMPOS_COLABORA + 'where ( CB_CODIGO = ( select MAX( C2.CB_CODIGO ) from COLABORA C2 where ( C2.CB_CODIGO < %d ) %s %s ) )';
          Q_VA_EMPLEADO_SIGUIENTE: Result := CAMPOS_COLABORA + 'where ( CB_CODIGO = ( select MIN( C2.CB_CODIGO ) from COLABORA C2 where ( C2.CB_CODIGO > %d ) %s %s ) )';
          Q_VA_PATRONES: Result := 'select TB_CODIGO, TB_ELEMENT, TB_NIVEL0 from RPATRON order by TB_CODIGO';
          Q_VA_PERIODO: Result := 'select PE_YEAR, PE_TIPO, PE_NUMERO, PE_USO, PE_FEC_INI, PE_FEC_FIN, PE_NUM_EMP, '+
{$ifdef QUINCENALES}
                                  'PE_ASI_INI, PE_ASI_FIN, '+
{$endif}
                                  'PE_FEC_PAG, PE_SOLO_EX, PE_INC_BAJ, PE_DIAS, PE_MES, PE_STATUS, PE_TIMBRO, PE_DESCRIP from PERIODO where ' +
                                  '( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d )';
          Q_VA_PERIODO_INICIAL: Result := 'select PE_YEAR, PE_TIPO, PE_NUMERO, PE_USO, PE_FEC_INI, PE_FEC_FIN, PE_NUM_EMP, '+
{$ifdef QUINCENALES}
                                          'PE_ASI_INI, PE_ASI_FIN, '+
{$endif}
                                          'PE_FEC_PAG, PE_SOLO_EX, PE_INC_BAJ, PE_DIAS, PE_MES, PE_STATUS, PE_TIMBRO, PE_DESCRIP from PERIODO ' +
                                          'where ( PE_YEAR = %0:d ) and ( PE_TIPO = %1:d ) and ' +
                                          '( PE_NUMERO = ( select MAX( PE_NUMERO ) from PERIODO where ' +
                                          '( PE_YEAR = %0:d ) and ( PE_TIPO = %1:d ) and ( PE_NUMERO < %2:d ) and ( PE_STATUS > 0 ) ) )';

          Q_VA_PERIODO_ANTERIOR: Result := 'select PE_YEAR, PE_TIPO, PE_NUMERO, PE_USO, PE_FEC_INI, PE_FEC_FIN, PE_NUM_EMP, ' +
{$ifdef QUINCENALES}
                                           'PE_ASI_INI, PE_ASI_FIN, '+
{$endif}
                                           'PE_FEC_PAG, PE_SOLO_EX, PE_INC_BAJ, PE_DIAS, PE_MES, PE_STATUS, PE_TIMBRO, PE_DESCRIP from PERIODO where ' +
                                           '( PE_YEAR = %0:d ) and ( PE_TIPO = %1:d ) and ( PE_NUMERO = ' +
                                           '( select MAX( P1.PE_NUMERO ) from PERIODO P1 where ' +
                                           '( P1.PE_YEAR = %0:d ) and ( P1.PE_TIPO = %1:d ) and ( P1.PE_NUMERO < %2:d ) ) )';
          Q_VA_PERIODO_SIGUIENTE: Result := 'select PE_YEAR, PE_TIPO, PE_NUMERO, PE_USO, PE_FEC_INI, PE_FEC_FIN, PE_NUM_EMP, ' +
{$ifdef QUINCENALES}
                                            'PE_ASI_INI, PE_ASI_FIN, '+
{$endif}
                                            'PE_FEC_PAG, PE_SOLO_EX, PE_INC_BAJ, PE_DIAS, PE_MES, PE_STATUS,PE_TIMBRO,  PE_DESCRIP from PERIODO where ' +
                                            '( PE_YEAR = %0:d ) and ( PE_TIPO = %1:d ) and ( PE_NUMERO = ' +
                                            '( select MIN( P1.PE_NUMERO ) from PERIODO P1 where ' +
                                            '( P1.PE_YEAR = %0:d ) and ( P1.PE_TIPO = %1:d ) and ( P1.PE_NUMERO > %2:d ) ) )';
          Q_VA_USUARIO: Result := 'select US_CODIGO, US_NOMBRE, US_CORTO, GRUPO.GR_CODIGO, US_NIVEL, US_PASSWRD, US_BLOQUEA, US_CAMBIA, US_PORTAL,US_DOMAIN,' +
                                  'US_FEC_IN, US_FEC_OUT, US_DENTRO, US_ARBOL, US_FORMA, US_BIT_LST, GR_DESCRIP, US_MAQUINA, CM_CODIGO, CB_CODIGO,'+
                                  'US_FEC_PWD, US_EMAIL, US_FORMATO, US_LUGAR, %d as TIMEOUT, %d as LIMITEPASS, %d as LETRASPASS, %d as DIGITOPASS %s , US_ACTIVO, '+
                                  'US_CLASICA , GRUPO.GR_DESCRIP GRUPODESCRIP '+
                                  'from USUARIO left join GRUPO on ( GRUPO.GR_CODIGO = USUARIO.GR_CODIGO ) ' +
                                  'where ( %s )';
          Q_VA_USUARIO_LOG_IN_PORTAL: Result := 'update USUARIO set US_MAQUINA = ''%s'' where ( US_CODIGO = %d )';
          Q_VA_USUARIO_LOG_IN: Result := 'update USUARIO set US_FEC_IN = ''%s'', US_DENTRO = ''S'', US_MAQUINA = ''%s'' where ( US_CODIGO = %d )';
          Q_VA_USUARIO_LOG_OUT: Result := 'update USUARIO set US_FEC_OUT = ''%s'', US_DENTRO = ''N'',US_MAQUINA = '''' where ( US_CODIGO = %d )';
          Q_VA_USUARIO_CAMBIA_PSWD: Result := 'update USUARIO set US_PASSWRD = ''%s'', US_CAMBIA = ''%s'', US_FEC_PWD = ''%s'' where ( US_CODIGO = %d )';
          Q_VA_USUARIO_BLOQUEA: Result := 'update USUARIO set US_FEC_IN = ''%s'', '+
                                                             'US_FEC_SUS = ''%s'', '+
                                                             'US_BLOQUEA = ''S'', '+
                                                             'US_DENTRO = ''N'', '+
                                                             'US_MAQUINA = ''%s'', '+
                                                             'US_FALLAS = %d '+
                                                             'where ( UPPER( US_CORTO ) = UPPER( ''%s'' ) )';
          Q_VA_COMPANYS: Result := 'select C.CM_CODIGO, C.CM_NOMBRE, C.CM_ALIAS, C.CM_USRNAME, C.CM_PASSWRD, C.CM_NIVEL0, C.CM_DATOS, C.CM_DIGITO, C.CM_KCLASIFI, C.CM_KCONFI, C.CM_KUSERS,C.DB_CODIGO, ' + ' C.CM_CTRL_RL '+
                                   'from COMPANY C, USUARIO U where ( %s ) and ( ' +
                                   '( U.US_CODIGO = %d ) and ' +
                                   '( ( U.GR_CODIGO = 1 ) or ' +
                                   '( select COUNT(*) from ACCESO A where '+
                                   '( A.CM_CODIGO = C.CM_CODIGO ) and '+
                                   '( A.GR_CODIGO = U.GR_CODIGO ) and '+
                                   '( A.AX_DERECHO > 0 )) > 0 ) ) '+
                                   'order by C.CM_CODIGO';
          Q_VA_COMPANYS_TODAS: Result := 'select C.CM_CODIGO, C.CM_NOMBRE, C.CM_ALIAS, '+
                                         'C.CM_USRNAME, C.CM_PASSWRD, C.CM_NIVEL0, C.CM_DATOS, C.CM_DIGITO, C.CM_KCLASIFI, C.CM_KUSERS, C.CM_KCONFI,C.DB_CODIGO, ' +' C.CM_CTRL_RL '+
                                         'from COMPANY C ' +
                                         'where ( %s ) '+
                                         'order by C.CM_CODIGO';
          Q_VA_ACCESOS: Result :=  'select AX_NUMERO, AX_DERECHO from ACCESO ' +
                                   'where ( GR_CODIGO = %d ) and ( CM_CODIGO = ''%s'' ) ' +
                                   'order by AX_NUMERO';
          Q_EMPLEADOS_BUSCADOS: Result := 'select CB_CODIGO, CB_APE_PAT, CB_APE_MAT, CB_NOMBRES, CB_RFC, CB_SEGSOC,CB_ACTIVO, CB_BAN_ELE,CB_CTA_GAS,CB_CTA_VAL, CB_PLAZA, %s '+
                                          K_PRETTYNAME + ' as PrettyName from COLABORA where '+
                                          '( UPPER( CB_APE_PAT ) like ''%s'' ) and '+
                                          '( UPPER( CB_APE_MAT ) like ''%s'' ) and '+
                                          '( UPPER( CB_NOMBRES ) like ''%s'' ) and '+
                                          '( UPPER( CB_RFC ) like ''%s'' ) and '+
                                          '( UPPER( CB_SEGSOC ) like ''%s'' ) and '+
                                          '( UPPER( CB_BAN_ELE ) like ''%s'' ) %s '+
                                          'order by CB_APE_PAT, CB_APE_MAT, CB_NOMBRES';
          Q_LOOKUP_EMPLEADO : Result := 'select CB_CODIGO,CB_ACTIVO,' + K_PRETTYNAME + ' as PrettyName %s '+
                                        'from COLABORA where ( CB_CODIGO = %d ) %s';
          Q_VA_NOMINA_ANTERIOR : Result := CAMPOS_COLABORA + 'where ( CB_CODIGO = ( select MAX( N.CB_CODIGO ) from NOMINA N, COLABORA C where ( ( N.CB_CODIGO < %d ) and ( N.PE_YEAR = %d ) and ( N.PE_TIPO = %d ) and ( N.PE_NUMERO = %d ) and ( N.CB_CODIGO = C.CB_CODIGO ) %s ) ) )';
          Q_VA_NOMINA_SIGUIENTE: Result := CAMPOS_COLABORA + 'where ( CB_CODIGO = ( select MIN( N.CB_CODIGO ) from NOMINA N, COLABORA C where ( ( N.CB_CODIGO > %d ) and ( N.PE_YEAR = %d ) and ( N.PE_TIPO = %d ) and ( N.PE_NUMERO = %d ) and ( N.CB_CODIGO = C.CB_CODIGO ) %s ) ) )';
          Q_VA_LEER_CLAVES: Result := 'select CL_TEXTO from CLAVES where ( CL_NUMERO = %d )';
          Q_VA_BORRA_CLAVES: Result := 'delete from CLAVES where ( CL_NUMERO = %d )';
          Q_VA_INSERTA_CLAVES: Result := 'insert into CLAVES ( CL_NUMERO, CL_TEXTO ) ' +
                                         'values ( %d, ''%s'' )';
          Q_VA_ACTUAL_VENCIMIENTO: Result := 'update USUARIO set US_FEC_PWD = ''%s'' ' +
                                             'where ( US_FEC_PWD = ''12/30/1899'' )';
          Q_VA_ACTUAL_PASSWORD: Result := 'select US_PASSWRD from USUARIO where ( US_CODIGO = %d )';
          Q_VA_NAVEGACION : Result := 'select QU_FILTRO from QUERYS where QU_CODIGO= ''%s'' ';
          Q_VA_USUARIO_CONSOLA : Result := 'select US_CODIGO, US_NOMBRE, US_CORTO, US_BLOQUEA, US_PASSWRD, US_ACTIVO, US_FEC_IN, US_FEC_PWD, GRUPO.GR_CODIGO from USUARIO left join GRUPO on ( GRUPO.GR_CODIGO = USUARIO.GR_CODIGO ) where ( %s )';
          Q_EMPLEADOS_BUSCADOS_DEV_EX: Result := 'select CB_CODIGO, CB_APE_PAT, CB_APE_MAT, CB_NOMBRES, CB_RFC, CB_SEGSOC,CB_ACTIVO, CB_BAN_ELE,CB_CTA_GAS,CB_CTA_VAL, CB_PLAZA, %s '+
                                          K_PRETTYNAME + ' as PrettyName from COLABORA where ('+
                                          '( UPPER( CB_APE_PAT ) like ''%s'' ) or '+
                                          '( UPPER( CB_APE_MAT ) like ''%s'' ) or '+
                                          '( UPPER( CB_NOMBRES ) like ''%s'' ) or '+
                                          '( UPPER( CB_RFC ) like ''%s'' ) or '+
                                          '( UPPER( CB_SEGSOC ) like ''%s'' ) or '+
                                          ' CB_CODIGO like ''%s'' or '+
                                          '( UPPER( CB_BAN_ELE ) like ''%s'' )) %s '+
                                          'order by CB_APE_PAT, CB_APE_MAT, CB_NOMBRES';
          // Q_ANALITICA_CONTABILIDAD_USO: Result := 'execute procedure Analitica_ContabilizarUso( :Modulo, :HelpContext, :Cuantos )';
          Q_ANALITICA_CONTABILIDAD_USO: Result := 'EXEC Analitica_ContabilizarUso  %s, %d, %d';
     else
         Result := '';
     end;
end;

{ TdmServerLogin }

procedure TdmServerLogin.InitLog( Empresa: Olevariant; const sPaletita: string );
begin
    {$ifdef BITACORA_DLLS}
    FPaletita := sPaletita;
     FArchivoLog := 'c:\BitacoraTress '+ FormatDateTime('dd_mmm_yy', Date()) +'.txt';

     if FListaLog = NIL then
     begin
          try
             FListaLog :=  TAsciiLog.Create;
             FListaLog.Init(FArchivoLog);
             try
                if NOT varisNull( Empresa ) then
                   try
                      FEmpresa :=  ' -- Alias: ' + Empresa[P_CODIGO] +
                                   ' Usuario: ' + InTToStr(Empresa[P_USUARIO])
                   except
                         FEmpresa := ' Error al leer Empresa[]';
                   end
                else
                    FEmpresa := '';
             except
                   FEmpresa := ' Error al leer Empresa[]';
             end;

             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) +
                                   ' DServerLogin :: INICIA :: '+  FPaletita +
                                   FEmpresa );
             FListaLog.Close;
          except
          end;
     end;
     {$endif}
end;

procedure TdmServerLogin.EndLog;
begin
     {$ifdef BITACORA_DLLS}
     try
        if FArchivoLog= '' then
           FArchivoLog := 'c:\BitacoraTress '+ FormatDateTime('dd_mmm_yy', Date()) +'.txt';
        if FListaLog = NIL then
        begin
             FListaLog :=  TAsciiLog.Create;
             FListaLog.Init(FArchivoLog);
             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) + ' DServerLogin :: TERMINA :: '+ FPaletita +' '+ FEmpresa);
        end
        else
        begin
             //FListaLog.Open(FArchivoLog);
             FListaLog.Init(FArchivoLog);
             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) + ' DServerLogin :: TERMINA :: '+ FPaletita +' '+ FEmpresa  );
             //FListaLog.Close;
        end;
        FListaLog.Close;
        FreeAndNil(FListaLog);
     except

     end;
     {$endif}
end;


class procedure TdmServerLogin.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerLogin.dmServerLoginCreate(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     OnActivate := ServerActivate;
     OnDeactivate := ServerDeactivate;
     ZetaCommonTools.SetOLEVariantToNull( FAuto );
     {$endif}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerLogin.dmServerLoginDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerLogin.CierraEmpresa;
begin
end;
{$else}
procedure TdmServerLogin.ServerActivate(Sender: TObject);
begin
     oZetaProvider.Activate;
end;

procedure TdmServerLogin.ServerDeactivate(Sender: TObject);
begin
     oZetaProvider.Deactivate;
end;
{$endif}

function TdmServerLogin.ReadAutoServer: OleVariant;
{$ifndef DOS_CAPAS}
var
   oAutoServer: TAutoServer;
{$endif}
begin
     {$ifndef DOS_CAPAS}
     if VarIsNull( FAuto ) then
     begin
     {$endif}
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     try
        ZetaSQLBroker.ReadAutoServer( oAutoServer, AutoGetData, AutoSetData );
        Result := oAutoServer.Exportar( VERSION_ARCHIVO );
     finally
            ZetaSQLBroker.FreeAutoServer( oAutoServer );
     end;
     {$ifndef DOS_CAPAS}
     FAuto := Result;
     end
     else
     begin
          Result := FAuto;
     end;
     {$endif}
end;

procedure TdmServerLogin.AutoGetData( Sender: TObject; var StringData: String );
begin
     StringData := LeerClaves( CLAVE_AUTORIZACION );
end;

procedure TdmServerLogin.AutoSetData( Sender: TObject; var StringData: String );
begin
     EscribeClavesTx( CLAVE_AUTORIZACION, StringData );
end;

procedure TdmServerLogin.Ejecuta( const sSQL: String );
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( Comparte, sSQL );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( False );
                     raise;
                end;
          end;
     end;
end;

{ ********** Interfases ( Paletitas ) ************ }

function TdmServerLogin.GetCompanys(Usuario, Tipo: Integer): OleVariant;
var
   sSQL: String;
begin
     InitLog(NULL,'GetCompanys');
     if ( Usuario = 0 ) then
        sSQL := Format( GetSQLTextCase( Q_VA_COMPANYS_TODAS) , [ GetTipoCompany( Tipo ) ] )
     else
        sSQL := Format( GetSQLTextCase( Q_VA_COMPANYS ), [ GetTipoCompany( Tipo ), Usuario ] );
     with oZetaProvider do
     begin
          Result := OpenSQL( Comparte, sSQL, True );
     end;
     EndLog;SetComplete;
end;

function TdmServerLogin.GetPatrones(Empresa: OleVariant): OleVariant;
begin
     InitLog(Empresa,'GetPatrones');
     Result := oZetaProvider.OpenSQL( Empresa, GetSQLTextCase( Q_VA_PATRONES ), True );
     EndLog;SetComplete;
end;

{ Regresa un arreglo por posici�n, con los derechos de acceso
  Si un registro no existe, regresa 0 en esa posici�n }
function TdmServerLogin.GetAccesos(Grupo: Integer; const Company: WideString): OleVariant;
var
   i, iCodigo: Integer;
   FDataset: TZetaCursor;
begin
     InitLog(NULL,'GetAccesos');
     Result := VarArrayCreate( [ 0, K_MAX_DERECHOS ], varSmallInt );
     for i := 0 to K_MAX_DERECHOS do
     begin
          Result[ i ] := 0;      { K_SIN_DERECHOS }
     end;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FDataset := CreateQuery( Format( GetSQLTextCase( Q_VA_ACCESOS ), [ Grupo, Company ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  if EOF then
                     Result := NULL
                  else
                  begin
                       while not Eof do
                       begin
                            iCodigo := Fields[ 0 ].AsInteger;
                            if ( iCodigo >= 1 ) and ( iCodigo <= K_MAX_DERECHOS ) then
                               Result[ iCodigo ] := Fields[ 1 ].AsInteger;
                            Next;
                       end
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
     EndLog;SetComplete;
end;

function TdmServerLogin.EscribeProximidad( oEmpresa: OleVariant; SQL: string; var oDatos: OleVariant ): Boolean;
var
   sFacilityCode: string;
begin
     with cdsTempo, oZetaProvider do
     begin
          EmpresaActiva := oEmpresa;
          InitGlobales;
          InitTempDataset;
          Data := OpenSQL( oEmpresa, SQL, True );
{$ifdef TRESS}
          if not ( State in [dsEdit, dsInsert] ) then
             Edit;
          sFacilityCode := GetGlobalString( K_GLOBAL_PROXIMITY_FACILITY_CODE );
          if ( FieldByName( 'CB_MIFARE' ).AsString = K_GLOBAL_SI ) then
             sFacilityCode := VACIO;
          FieldByName( 'CB_ID_NUM' ).AsString := ZetaServerTools.ProximidadHexToDec( FieldByName( 'CB_ID_NUM' ).AsString,
                                                                                     sFacilityCode,
                                                                                     GetGlobalInteger( K_GLOBAL_PROXIMITY_TOTAL_BYTES ) );
          Post;
          MergeChangeLog;
{$endif}
          oDatos := Data;
          Result := ( RecsOut > 0 );
     end;
end;

function TdmServerLogin.GetEmpleado(Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant): WordBool;
var
   sSQL: String;
begin
     InitLog(Empresa,'GetEmpleado');
     sSQL := Format( GetSQLTextCase( Q_VA_EMPLEADO ), [ Empleado, Nivel0( Empresa ) ] );
     Result := EscribeProximidad( Empresa, sSQL, Datos );
     EndLog;SetComplete;
end;

function TdmServerLogin.GetEmpleadoAnterior(Empresa: OleVariant; Empleado: Integer; const sNavega: WideString; out Datos: OleVariant): WordBool;
var
   sSQL, sResult: String;
begin
     InitLog(Empresa,'GetEmpleadoAnterior');
     sResult := GetNavegacion( Empresa, sNavega );

     sSQL := Format( GetSQLTextCase( Q_VA_EMPLEADO_ANTERIOR ), [ Empleado, Nivel0( Empresa ),  ( sResult )  ] );

     Result := EscribeProximidad( Empresa, sSQL, Datos );
     EndLog;SetComplete;
end;

function TdmServerLogin.GetEmpleadoSiguiente(Empresa: OleVariant; Empleado: Integer; const sNavega: WideString; out Datos: OleVariant): WordBool;
var
   sSQL, sResult: String;
begin
     InitLog(Empresa,'GetEmpleadoSiguiente');
     sResult := GetNavegacion( Empresa, sNavega );

     sSQL := Format( GetSQLTextCase( Q_VA_EMPLEADO_SIGUIENTE ), [ Empleado, Nivel0( Empresa ), ( sResult ) ] );
     Result := EscribeProximidad( Empresa, sSQL, Datos );
     EndLog;SetComplete;
end;

function TdmServerLogin.GetNavegacion( Empresa: OleVariant; sNavegacion: String ): String;
var
   sSQL: String;
begin
     Result := VACIO;
     if StrLleno( sNavegacion ) then
     begin
          sSQL := Format( GetSQLTextCase( Q_VA_NAVEGACION ), [ sNavegacion ] );
          with oZetaProvider do
          begin
               cdsTempo.Lista := OpenSQL( Empresa, sSQL, True );
               Result:= StrTransAll( cdsTempo.FieldByName( 'QU_FILTRO' ).AsString, 'COLABORA', 'C2' );
               if StrLleno(Result) then
               begin
                    if EsFormulaSQL( Result ) then
                       Result := Copy( Result, 2, Length(Result) );
                  Result:= 'AND ' + Result;
               end
          end;
     end;
     EndLog;
     SetComplete;
end;

function TdmServerLogin.GetLookupEmpleado(Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant; TipoLookup: Integer): WordBool;
var
   sSQL: String;

   function GetCampos: String;
   begin
        Result := VACIO;
        case TTipoLookupEmpleado( TipoLookup ) of
             eLookEmpNominas:    Result := ',CB_NOMYEAR,CB_NOMTIPO,CB_NOMNUME,CB_FEC_BAJ,CB_TURNO,CB_NOMINA,CB_FEC_NOM';
             eLookEmpCursos:     Result := ',CB_TURNO,CB_PUESTO,CB_CLASIFI,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3' +
                                           ',CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,CB_NIVEL9'
                                           {$ifdef ACS}+',CB_NIVEL10, CB_NIVEL11, CB_NIVEL12'{$endif};
             eLookEmpCedulas:    Result := ',CB_PUESTO';
             eLookEmpComidas:    Result := ',CB_FEC_BAJ';
             eLookEmpAsignaArea: Result := ',CB_AREA,CB_AR_FEC,CB_AR_HOR';
             eLookEmpMedicos:    Result := ',CB_NOMBRES,CB_APE_PAT,CB_APE_MAT,CB_SEXO,CB_FEC_NAC,CB_CALLE,CB_COLONIA,'+
                                           'CB_CIUDAD,CB_CODPOST,CB_ESTADO,CB_TEL,CB_NOMINA, CB_NUM_EXT, CB_NUM_INT,CB_TSANGRE,CB_ALERGIA';
             eLookEmpBancaElec:  Result := ',CB_BAN_ELE,CB_CTA_GAS,CB_CTA_VAL';
             eLookEmpInfonavit:  Result := ',CB_INFTIPO, CB_INFCRED, CB_INFTASA, CB_INF_OLD, CB_INFMANT, CB_INF_INI,CB_INFACT';
             eLookEmpPlaza:      Result := ',CB_PLAZA,CB_AUTOSAL';
             eLookEmpTransfer:   Result := ',CB_FEC_NIV,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3' +
                                           ',CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,CB_NIVEL9'
                                           {$ifdef ACS}+',CB_NIVEL10, CB_NIVEL11, CB_NIVEL12'{$endif}
        end;
   end;

begin
     InitLog(Empresa,'GetLookupEmpleado');
     sSQL := Format( GetSQLTextCase( Q_LOOKUP_EMPLEADO ), [ GetCampos, Empleado, Nivel0( Empresa ) ] );
     Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     Result := ( oZetaProvider.RecsOut > 0 );
     EndLog;SetComplete;
end;

function TdmServerLogin.GetEmpleadosBuscados(Empresa: OleVariant; const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: WideString): OleVariant;

         function filtrarParametro( sParam : string ) : string;
         begin
                Result := Trim( FiltrarTextoSQL( sParam ) ) ;
         end;

const
      {$ifdef MSSQL}
     QRY_STATUS_ACT = ' ( dbo.SP_STATUS_ACT( %s ,CB_CODIGO ) )as STATUS,';
      {$endif}
      {$ifdef INTERBASE}
     QRY_STATUS_ACT = ' ( Select Resultado from SP_STATUS_ACT( %s ,COLABORA.CB_CODIGO ) )as STATUS,';
      {$endif}
var
   sSQL: String;
   sStatusEmpleado :string;
begin
     InitLog(Empresa,'GetEmpleadosBuscados');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     // PENDIENTE OPTIMIZAR EL QUERY
     // SI algunos criterios vienen VACIOS no tiene caso incluir el WHERE LIKE
     sStatusEmpleado := VACIO;
     {$ifndef SELECCION}
     {$ifndef VISITANTES}

     if oZetaProvider.GetGlobalBooleano(K_GLOBAL_CB_ACTIVO_AL_DIA )then
             sStatusEmpleado := Format(QRY_STATUS_ACT,[ DateToStrSQLC(Now)]);
     {$endif}
     {$endif}
     sSQL := Format( GetSQLTextCase( Q_EMPLEADOS_BUSCADOS ),
                                     [ sStatusEmpleado,
                                       '%' + filtrarParametro( sPaterno ) + '%',
                                       '%' + filtrarParametro( sMaterno ) + '%',
                                       '%' + filtrarParametro( sNombre )  + '%',
                                       '%' + filtrarParametro( sRFC ) + '%',
                                       '%' + filtrarParametro( sNSS ) + '%',
                                       '%' + filtrarParametro( sBanca ) + '%',
                                       Nivel0( Empresa ) ] );

     Result := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     EndLog;SetComplete;
end;

function TdmServerLogin.GetEmpleadosBuscados_DevEx(Empresa: OleVariant; const sPista: WideString): OleVariant;

         function filtrarParametro( sParam : string ) : string;
         begin
                Result := Trim( FiltrarTextoSQL( sParam ) ) ;
         end;

const
      {$ifdef MSSQL}
     QRY_STATUS_ACT = ' ( dbo.SP_STATUS_ACT( %s ,CB_CODIGO ) )as STATUS,';
      {$endif}
      {$ifdef INTERBASE}
     QRY_STATUS_ACT = ' ( Select Resultado from SP_STATUS_ACT( %s ,COLABORA.CB_CODIGO ) )as STATUS,';
      {$endif}
var
   sSQL: String;
   sStatusEmpleado :string;
begin
     InitLog(Empresa,'GetEmpleadosBuscados');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     sStatusEmpleado := VACIO;
     {$ifndef SELECCION}
     {$ifndef VISITANTES}

     if oZetaProvider.GetGlobalBooleano(K_GLOBAL_CB_ACTIVO_AL_DIA )then
             sStatusEmpleado := Format(QRY_STATUS_ACT,[ DateToStrSQLC(Now)]);
     {$endif}
     {$endif}
     sSQL := Format( GetSQLTextCase( Q_EMPLEADOS_BUSCADOS_DEV_EX ),
                                     [ sStatusEmpleado,
                                       '%' + filtrarParametro( sPista ) + '%',
                                       '%' + filtrarParametro( sPista ) + '%',
                                       '%' + filtrarParametro( sPista )  + '%',
                                       '%' + filtrarParametro( sPista ) + '%',
                                       '%' + filtrarParametro( sPista ) + '%',
                                       '%' + filtrarParametro( sPista ) + '%',
                                       '%' + filtrarParametro( sPista ) + '%',
                                       Nivel0( Empresa ) ] );

     Result := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     EndLog;SetComplete;
end;

{$ifndef SELECCION}
{$ifndef VISITANTES}
function TdmServerLogin.GetPeriodoInicial(Empresa: OleVariant; Year, Tipo: Integer): OleVariant;
var
   sSQL: String;
begin
     InitLog(Empresa,'GetPeriodoInicial');
     oZetaProvider.EmpresaActiva := Empresa;
     oZetaProvider.InitGlobales;
     sSQL := Format( GetSQLTextCase( Q_VA_PERIODO_INICIAL ), [ Year, Tipo , oZetaProvider.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) ] );
     Result := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     if ( oZetaProvider.RecsOut <= 0 ) then
        GetPeriodoSiguiente( Empresa, Year, Tipo, 0, Result );
     EndLog;SetComplete;
end;
{$endif}
{$endif}

function TdmServerLogin.GetPeriodo(Empresa: OleVariant; Year, Tipo, Numero: Integer; out Datos: OleVariant): WordBool;
var
   sSQL: String;
begin
     InitLog(Empresa,'GetPeriodo');
     sSQL := Format( GetSQLTextCase( Q_VA_PERIODO ), [ Year, Tipo, Numero ] );
     Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     Result := ( oZetaProvider.RecsOut > 0 );
     EndLog;SetComplete;
end;

function TdmServerLogin.GetPeriodoAnterior(Empresa: OleVariant; Year, Tipo, Numero: Integer; out Datos: OleVariant): WordBool;
var
   sSQL: String;
begin
     InitLog(Empresa,'GetPeriodoAnterior');
     sSQL := Format( GetSQLTextCase( Q_VA_PERIODO_ANTERIOR ), [ Year, Tipo, Numero ] );
     Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     Result := ( oZetaProvider.RecsOut > 0 );
     EndLog;SetComplete;
end;

function TdmServerLogin.GetPeriodoSiguiente(Empresa: OleVariant; Year, Tipo, Numero: Integer; out Datos: OleVariant): WordBool;
var
   sSQL: String;
begin
     InitLog(Empresa,'GetPeriodoSiguiente');
     sSQL := Format( GetSQLTextCase( Q_VA_PERIODO_SIGUIENTE ), [ Year, Tipo, Numero ] );
     Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     Result := ( oZetaProvider.RecsOut > 0 );
     EndLog;SetComplete;
end;

function TdmServerLogin.UsuarioLogin(const NombreCorto, Clave, ComputerName: WideString;
         AllowReentry: WordBool; out Datos, Autorizacion: OleVariant; out Intentos: Integer): Integer;
begin
     Result := UsuarioCargar(False, NombreCorto, Decrypt (Clave), ComputerName, AllowReentry, Datos, Autorizacion, Intentos);
end;

function TdmServerLogin.SQL_Injection (sNombre_Corto: String): Boolean;
var
   i, j: Integer;
   slNombreCorto: TStringList;

   function MantenerAlfanumericos(const str: string): string;
   var
      i, Count: Integer;
   begin
        SetLength(Result, Length(str));
        Count := 0;
        for i := 1 to Length(str) do
        begin
             if (str[i] in ['a'..'z', 'A'..'Z', '0'..'9']) then
             begin
                  inc(Count);
                  Result[Count] := str[i];
             end
             else
             begin
                  inc(Count);
                  Result[Count] := ' ';
             end;
        end;
        SetLength(Result, Count);
   end;

begin
     Result := FALSE;

     slNombreCorto := TStringList.Create;
     try
        slNombreCorto.Delimiter := ' ';
        // slNombreCorto.DelimitedText := sNombre_Corto;
        slNombreCorto.DelimitedText := MantenerAlfanumericos (sNombre_Corto);

        for i := ( Low( aSQL_INJECTION ) ) to High( aSQL_INJECTION ) do
        begin
             for j := 0 to (slNombreCorto.Count - 1) do
             begin
                  // slNombreCorto[j] :=  MantenerAlfanumericos (slNombreCorto[j] );
                  if UpperCase (aSQL_INJECTION[i]) = UpperCase (slNombreCorto[j] )then
                  begin
                       RESULT := TRUE;
                       break;
                  end;
             end;
        end;
     finally
            FreeAndNil (slNombreCorto);
     end;
end;

function TdmServerLogin.UsuarioCargar ( lActiveDirectory:Boolean ; const NombreCorto, Clave, ComputerName: WideString;
         AllowReentry: WordBool; var Datos, Autorizacion: OleVariant; var Intentos: Integer):Integer;
const
     K_PORTAL_TOKEN = 'PORTAL::';
var
   dValue: TDate;
   iUsuario, iExpiracion, iDiasInactivo: Integer;
   lEsComputerPortal: Boolean;

function GetCondicionUsuario:string;
begin
     if (lActiveDirectory)then
        Result := Format( 'UPPER( US_DOMAIN ) = UPPER( ''%s'' )',[NombreCorto])
     else
         Result := Format('UPPER( US_CORTO ) = UPPER( ''%s'' ) ',[NombreCorto]);
end;

function GetLoginAd :string ;
begin
     if (lActiveDirectory)then
        Result := Format( ', ''%s'' as LOGIN_AD ',['S'])
     else
         Result := Format( ', ''%s'' as LOGIN_AD ',['N'])

end;

function GetComputerName: string;
begin
     if lEsComputerPortal then
        Result := Copy( ComputerName, Length(K_PORTAL_TOKEN) +1, MaxInt )
     else
         Result := Copy( ComputerName,1, MaxInt );
end;

begin
     InitLog(NULL,'GetComputerName');
     with cdsTempo do
     begin
          Intentos := GetIntentos;
          lEsComputerPortal := Copy( ComputerName, 1, Length(K_PORTAL_TOKEN) )= K_PORTAL_TOKEN;

          if SQL_Injection (NombreCorto) then
          begin
               raise Exception.Create( 'No se pueden utilizar palabras reservadas de base de datos.' );
          end
          else
          begin
               Data := oZetaProvider.OpenSQL( oZetaProvider.Comparte, Format( GetSQLTextCase( Q_VA_USUARIO ), [ GetTiempoInactivo(lActiveDirectory), GetLimitePassword, GetMinLetrasPwd, GetMinDigitosPwd,GetLoginAd, GetCondicionUsuario ] ), True );
               if IsEmpty then
                  Result := Ord( lrNotFound )
               else
               begin
                    if ( not zStrToBool(FieldByName( 'US_ACTIVO' ).AsString )) then
                         Result := Ord( lrUserInactive )
                    else
                        if ( ( not lActiveDirectory ) and ( ZetaServerTools.Decrypt( FieldByName( 'US_PASSWRD' ).AsString ) <> Clave ) ) then
                           Result := Ord( lrAccessDenied )
                        else
                            if ZetaCommonTools.zStrToBool( FieldByName( 'US_BLOQUEA' ).AsString ) then
                               Result := Ord( lrLockedOut )
                            else
                                if ( FieldByName( 'GR_CODIGO' ).AsInteger <> K_GRUPO_SISTEMA ) and
                                   ( GetSistemaBloqueo ) then
                                   Result:= Ord( lrSystemBlock )
                                else
                                if not AllowReentry and
                                   ZetaCommonTools.zStrToBool( FieldByName( 'US_DENTRO' ).AsString ) and
                                   ( FieldByName( 'GR_CODIGO' ).AsInteger <> K_GRUPO_SISTEMA ) and
                                   ( FieldByName( 'US_MAQUINA' ).AsString <> GetComputerName ) then
                                begin
                                     Result := Ord( lrLoggedIn );
                                end
                                else
                                begin
                                     iDiasInactivo := GetDiasInactivo;
                                     dValue := FieldByName( 'US_FEC_IN' ).AsDateTime;
                                     if NOT lEsComputerPortal and ( iDiasInactivo > 0 ) and ( dValue > NullDateTime ) and ( ( dValue + iDiasInactivo ) < Date ) then
                                     begin
                                          BloqueaUsuario( FieldByName( 'US_CORTO' ).AsString, GetComputerName, 0 );




                                          Result := Ord( lrInactiveBlock );
                                     end
                                     else
                                     begin
                                          if ( not lActiveDirectory ) and ZetaCommonTools.zStrToBool( FieldByName( 'US_CAMBIA' ).AsString ) then
                                             Result := Ord( lrChangePassword )
                                          else
                                          begin
                                               iExpiracion := GetDiasExpiracion;
                                               if ( not lActiveDirectory ) and ( iExpiracion > 0 ) and ( ( FieldByName( 'US_FEC_PWD' ).AsDateTime + iExpiracion ) <= Date ) then
                                                  Result := Ord( lrExpiredPassword )
                                               else
                                                   Result := Ord( lrOK );
                                          end;
                                          iUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
                                          Datos := Data;
                                          { Registra LOGIN }
                                           //13/02/2003 MA: Se valida que el nombre de la computadora venga vacio para cuando se hace Login por medio de Web
                                          if lEsComputerPortal then
                                          begin
                                               Ejecuta( Format( GetSQLTextCase( Q_VA_USUARIO_LOG_IN_PORTAL ), [ GetComputerName, iUsuario ] ) );
                                          end
                                          else
                                          begin
                                               Ejecuta( Format( GetSQLTextCase( Q_VA_USUARIO_LOG_IN ), [ ZetaCommonTools.DateToStrSQL( Now ), GetComputerName, iUsuario ] ) );
                                          end;
                                     end;
                                end;
                    Active := False;
               end;
          end;
     end;
     Autorizacion := ReadAutoServer;
     EndLog;SetComplete;
end;

function TdmServerLogin.GetAuto: OleVariant;
begin
     InitLog(NULL,'GetAuto');
     Result := ReadAutoServer;
     EndLog;SetComplete;
end;

procedure TdmServerLogin.BloqueaUsuario( const sUser, sComputerName: String;  const iIntentos: Integer );
var
   sNow: String;
begin
{     if ( iIntentos = 0 ) or ( GetDiasInactivo > 0 ) then
     begin }
          sNow := ZetaCommonTools.DateToStrSQL( Now );
          Ejecuta( Format( GetSQLTextCase( Q_VA_USUARIO_BLOQUEA ), [ sNow, sNow, sComputerName, iIntentos, sUser ] ) );
//     end;
end;

procedure TdmServerLogin.UsuarioBloquea(const NombreCorto, ComputerName: WideString; Intentos: Integer);
begin
     InitLog(NULL,'UsuarioBloquea');
     BloqueaUsuario( NombreCorto, ComputerName, Intentos );
     EndLog;SetComplete;
end;

function TdmServerLogin.UsuarioLogout(Usuario: Integer): Integer;
var
   sSQL: String;
begin
     InitLog(NULL,'UsuarioLogout');
     Result := 0;
     sSQL := Format( GetSQLTextCase( Q_VA_USUARIO_LOG_OUT ), [ ZetaCommonTools.DateToStrSQL( Now ), Usuario ] );
     with oZetaProvider do
     begin
          EmpresaActiva:= Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( Comparte, sSQL );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( False );
                end;
          end;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerLogin.UsuarioCambiaPswd(Usuario: Integer; const ClaveActual, ClaveNueva: WideString);
var
   FSeguridadInfo: TSeguridadInfo;
   FDataset: TZetaCursor;
   sClave: String;
   sMensaje: String;
begin
     InitLog(NULL,'UsuarioCambiaPswd');
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FDataset := CreateQuery( Format( GetSQLTextCase( Q_VA_ACTUAL_PASSWORD ), [ Usuario ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  sClave := FieldByName( 'US_PASSWRD' ).AsString;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          { Valida que Haya Proporcionado la Clave Anterior }
          if ( Decrypt (ClaveActual) <> ZetaServerTools.Decrypt( sClave ) ) then
             raise Exception.Create( 'Clave Actual Incorrecta' );
          { Valida Que sea diferente a la Actual }
          if ( Decrypt (ClaveNueva) = ZetaServerTools.Decrypt( sClave ) ) then
             raise Exception.Create( 'La Clave Nueva Debe Ser Diferente A La Anterior' +
                                      CR_LF + 'Por Favor Teclee Clave De Nuevo' )
          else
          begin
               FSeguridadInfo := TSeguridadInfo.Create( oZetaProvider );
               try
                  with FSeguridadInfo do
                  begin
                       PasswordLogBegin;
                       try
                          CambiaPasswordLog := TRUE;
                          if ValidaPasswordLog( ClaveNueva, Usuario, sMensaje ) then
                          begin
                               EmpiezaTransaccion;
                               try
                                  ExecSQL( Comparte, Format( GetSQLTextCase( Q_VA_USUARIO_CAMBIA_PSWD ),
                                                     [ ClaveNueva, K_GLOBAL_NO,
                                                     DateToStrSQL( Date ), Usuario ] ) );
                                  GrabaPasswordLog;
                                  TerminaTransaccion( True );
                               except
                                     on Error: Exception do
                                     begin
                                          TerminaTransaccion( False );
                                          raise;
                                     end;
                               end;
                          end
                          else
                          begin
                               raise Exception.Create( sMensaje );
                          end;
                       finally
                          PasswordLogEnd;
                       end;
                  end;
               finally
                  FreeAndNil( FSeguridadInfo );
               end;
          end;
     end;
     EndLog;SetComplete;
end;

function TdmServerLogin.GetNominaEmpleado( Empresa, Parametros: OleVariant; var Datos: OleVariant; const iQuery : Integer ): WordBool;
var
   sSQL: String;
begin
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          GetEncabPeriodo;
          with DatosPeriodo do
          begin
               sSQL := Format( GetSQLTextCase( iQuery ),
                       [ ParamList.ParamByName( 'Empleado' ).AsInteger, Year, Ord( Tipo ), Numero, Nivel0( Empresa, 'C' ) ] );
          end;
          Datos := OpenSQL( Empresa, sSQL, True );
          Result := ( RecsOut > 0 );
     end;
end;

function TdmServerLogin.GetNominaSiguiente(Empresa, Parametros: OleVariant; out Datos: OleVariant): WordBool;
begin
     InitLog(Empresa,'GetNominaSiguiente');
     Result := GetNominaEmpleado( Empresa, Parametros, Datos, Q_VA_NOMINA_SIGUIENTE );
     EndLog;SetComplete;
end;

function TdmServerLogin.GetNominaAnterior(Empresa, Parametros: OleVariant; out Datos: OleVariant): WordBool;
begin
     InitLog(Empresa,'GetNominaAnterior');
     Result := GetNominaEmpleado( Empresa, Parametros, Datos, Q_VA_NOMINA_ANTERIOR );
     EndLog;SetComplete;
end;

function TdmServerLogin.GetDiasExpiracion: Integer;
begin
     Result := StrToIntDef( LeerClaves( CLAVE_VENCIMIENTO ), 0 );
end;

function TdmServerLogin.GetIntentos: Integer;
begin
     Result := StrToIntDef( LeerClaves( CLAVE_INTENTOS ), 0 );
end;

function TdmServerLogin.GetDiasInactivo: Integer;
begin
     Result := StrToIntDef( LeerClaves( CLAVE_DIAS_INACTIVOS ), 0 );
end;

function TdmServerLogin.GetTiempoInactivo(const lActiveDirectory:Boolean): Integer;
begin
     if (lActiveDirectory)then
        Result:= 0
     else
         Result := StrToIntDef( LeerClaves( CLAVE_TIEMPO_INACTIVO ), 0 );
end;

function TdmServerLogin.GetLimitePassword: Integer;
begin
     Result := StrToIntDef( LeerClaves( CLAVE_LIMITE_PASSWORD ), 0 );
end;

function TdmServerLogin.GetMinLetrasPwd: Integer;
begin
     Result := StrToIntDef( LeerClaves( CLAVE_MIN_LETRAS_PASSWORD ), 0 );
end;

function TdmServerLogin.GetMinDigitosPwd: Integer;
begin
     Result := StrToIntDef( LeerClaves( CLAVE_MIN_DIGITOS_PASSWORD ), 0 );
end;

function TdmServerLogin.GetMaxHistoriaPwd: Integer;
begin
     Result := StrToIntDef( LeerClaves( CLAVE_MAX_LOG_PASSWORD ), 0 );
end;

// (JB) Se agrega Configuracion de Tress Automatiza
function TdmServerLogin.GetUsuarioTressAutomatiza: Integer;
begin
     Result := StrToIntDef( LeerClaves( CLAVE_USUARIO_TRESSAUTOMATIZA ), 0 );
end;

// US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
function TdmServerLogin.GetMostrarAdvertenciaLicencia: Boolean;
begin
     Result := StrToBoolDef( LeerClaves( CLAVE_MOSTRAR_ADV_LICENCIA ), TRUE );
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
function TdmServerLogin.GetServidorCorreos: String;
begin
     Result := LeerClaves( CLAVE_SERVIDOR_CORREOS );
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
function TdmServerLogin.GetPuertoSMTP: Integer;
begin
     Result := StrToIntDef( LeerClaves( CLAVE_PUERTO_SMTP ), 0 );
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
function TdmServerLogin.GetAutentificacionCorreo: Integer;
begin
     Result := StrToIntDef( LeerClaves( CLAVE_AUTENTIFICACION_CORREO ), 0 );
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
function TdmServerLogin.GetUserID: String;
begin
     Result := LeerClaves( CLAVE_USER_ID );
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
function TdmServerLogin.GetEmailPSWD: String;
begin
     Result := LeerClaves( CLAVE_EMAIL_PSWD );
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
function TdmServerLogin.GetRecibirAdvertenciaPorEmail: Boolean;
begin
     Result := StrToBoolDef( LeerClaves( CLAVE_RECIBIR_ADV_CORREO ), TRUE );
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
function TdmServerLogin.GetGrupoUsuariosEmail: Integer;
begin
     Result := StrToIntDef( LeerClaves( CLAVE_GRUPO_USUARIO_EMAIL ), 0 );
end;

 // US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
function TdmServerLogin.GetDirectorioServicioReportes: String;
begin
     Result := LeerClaves( CLAVE_DIRECTORIO_SERVICIO_REPORTES );
end;

// US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
function TdmServerLogin.GetDirectorioImagenes: String;
begin
     Result := LeerClaves( CLAVE_DIRECTORIO_IMAGENES );
end;

// US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
function TdmServerLogin.GetURLImagenes: String;
begin
     Result := LeerClaves( CLAVE_URL_IMAGENES );
end;

// US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
function TdmServerLogin.GetExpiracionImagenes: Integer;
begin
     Result := StrToIntDef (LeerClaves( CLAVE_EXPIRACION_IMAGENES ), 30);
end;

function TdmServerLogin.GetRecuperarClaveServicioCorreos: Boolean;
begin
     Result := StrToBoolDef( LeerClaves( CLAVE_RECUPERAR_PWD_SERVICIO_CORREOS ), FALSE );
end;


function TdmServerLogin.LeerClaves( const iNumero: Integer ): String;
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FDataset := CreateQuery( Format( GetSQLTextCase( Q_VA_LEER_CLAVES ), [ iNumero ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  if EOF then
                     Result := ''
                  else
                      Result := FieldByName( 'CL_TEXTO' ).AsString;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
end;

procedure TdmServerLogin.EscribeClavesTx( const iNumero: Integer; const sTexto: String );
 var Empresa: OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             SetOLEVariantToNull( Empresa );
             EscribeClaves( Empresa, iNumero, sTexto );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     //TerminaTransaccion( False );
                     RollBackTransaccion;
                     raise;
                end;
          end;
     end;
end;


procedure TdmServerLogin.EscribeBitacora( Empresa: OleVariant; const iNumero: Integer; const sGlobalNuevo : string );

 function GetDescripcion :  string;
  const
       CLAVE_D_VENCIMIENTO             = 'Expiraci�n En X D�as';
       CLAVE_D_LIMITE_PASSWORD         = 'Longitud M�nima';
       CLAVE_D_INTENTOS                = 'Bloqueo A Los X Intentos';
       CLAVE_D_DIAS_INACTIVOS          = 'Bloqueo A X D�as Inactividad';
       CLAVE_D_TIEMPO_INACTIVO         = 'L�mite de Inactividad';
       CLAVE_D_MIN_LETRAS_PASSWORD     = 'Cantidad M�nima de Letras';
       CLAVE_D_MIN_DIGITOS_PASSWORD    = 'Cantidad M�nima de D�gitos';
       CLAVE_D_MAX_LOG_PASSWORD        = 'Cantidad de Claves a Almacenar';
       CLAVE_D_SISTEMA_BLOQUEADO       = 'Bloqueo de Sistema por Administrador';
       CLAVE_D_USUARIO_TRESSAUTOMATIZA = 'Usuario de Tareas Autom�ticas'; // (JB) Se agrega Configuracion de Tress Automatiza
       CLAVE_D_MOSTRAR_ADV_LICENCIA    = 'Mostrar advertencias de licencia'; // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
       CLAVE_D_SERVIDOR_CORREOS        = 'Servidor de correos'; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
       CLAVE_D_PUERTO_SMTP             = 'Puerto'; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
       CLAVE_D_AUTENTIFICACION_CORREO  = 'Autenticaci�n'; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
       CLAVE_D_USER_ID                 = 'ID. Usuario'; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
       CLAVE_D_EMAIL_PSWD              = 'Clave'; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
       CLAVE_D_RECIBIR_ADV_CORREO      = 'Recibir advertencias de licencia de uso por correo'; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
       CLAVE_D_GRUPO_USUARIO_EMAIL     = 'Grupo Usuario Adicional'; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
 begin
      case iNumero of
           CLAVE_VENCIMIENTO            : Result := CLAVE_D_VENCIMIENTO;
           CLAVE_LIMITE_PASSWORD        : Result := CLAVE_D_LIMITE_PASSWORD;
           CLAVE_INTENTOS               : Result := CLAVE_D_INTENTOS;
           CLAVE_DIAS_INACTIVOS         : Result := CLAVE_D_DIAS_INACTIVOS;
           CLAVE_TIEMPO_INACTIVO        : Result := CLAVE_D_TIEMPO_INACTIVO;
           CLAVE_MIN_LETRAS_PASSWORD    : Result := CLAVE_D_MIN_LETRAS_PASSWORD;
           CLAVE_MIN_DIGITOS_PASSWORD   : Result := CLAVE_D_MIN_DIGITOS_PASSWORD;
           CLAVE_MAX_LOG_PASSWORD       : Result := CLAVE_D_MAX_LOG_PASSWORD;
           CLAVE_SISTEMA_BLOQUEADO      : Result := CLAVE_D_SISTEMA_BLOQUEADO;
           CLAVE_USUARIO_TRESSAUTOMATIZA: Result := CLAVE_D_USUARIO_TRESSAUTOMATIZA; // (JB) Se agrega Configuracion de Tress Automatiza
           CLAVE_MOSTRAR_ADV_LICENCIA   : Result := CLAVE_D_MOSTRAR_ADV_LICENCIA; // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
           CLAVE_SERVIDOR_CORREOS       : Result := CLAVE_D_SERVIDOR_CORREOS; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
           CLAVE_PUERTO_SMTP            : Result := CLAVE_D_PUERTO_SMTP; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
           CLAVE_AUTENTIFICACION_CORREO : Result := CLAVE_D_AUTENTIFICACION_CORREO; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
           CLAVE_USER_ID                : Result := CLAVE_D_USER_ID; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
           CLAVE_EMAIL_PSWD             : Result := CLAVE_D_EMAIL_PSWD; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
           CLAVE_RECIBIR_ADV_CORREO     : Result := CLAVE_D_RECIBIR_ADV_CORREO; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
           CLAVE_GRUPO_USUARIO_EMAIL    : Result := CLAVE_D_GRUPO_USUARIO_EMAIL; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
      end;
 end;

 const
      K_BASE_SEGURIDAD = 10000;

 var
    sGlobalViejo : string;

begin
     if NOT VarIsNull(Empresa) then
     begin
          sGlobalViejo:= LeerClaves( iNumero );

          if ( sGlobalViejo <> sGlobalNuevo ) then
          begin
               with oZetaProvider do
               begin
                    EmpresaActiva := Empresa;

                    EscribeBitacora( tbNormal, clbGlobales, K_BASE_SEGURIDAD + iNumero, NullDateTime,
                                     Format( 'Seguridad #%d: %s', [iNumero, GetDescripcion]),
                                     Format( 'De "%s" A "%s"', [sGlobalViejo, sGlobalNuevo] ) );
               end;
          end;
     end;
end;

procedure TdmServerLogin.EscribeClaves( Empresa:OleVariant; const iNumero: Integer; const sTexto: String );
begin
     EscribeBitacora( Empresa, iNumero, sTexto );
     with oZetaProvider do
     begin
          ExecSQL( Comparte, Format( GetSQLTextCase( Q_VA_BORRA_CLAVES ), [ iNumero ] ) );
          ExecSQL( Comparte, Format( GetSQLTextCase( Q_VA_INSERTA_CLAVES ), [ iNumero, sTexto ] ) );
     end;
end;

function TdmServerLogin.GetSeguridad: OleVariant;
begin
     InitLog(NULL,'GetSeguridad');
     Result := VarArrayOf( [ GetDiasExpiracion,             //SEG_VENCIMIENTO
                             GetLimitePassword,             //SEG_LIMITE_PASSWORD
                             GetIntentos,                   //SEG_INTENTOS
                             GetDiasInactivo,               //SEG_DIAS_INACTIVOS
                             GetTiempoInactivo,             //SEG_TIEMPO_INACTIVO
                             GetMinLetrasPwd,               //SEG_MIN_LETRAS_PASSWORD
                             GetMinDigitosPwd,              //SEG_MIN_DIGITOS_PASSWORD
                             GetMaxHistoriaPwd,             //SEG_MAX_LOG_PASSWORD
                             GetUsuarioTressAutomatiza,     //SEG_USUARIO_TRESSAUTOMATIZA // (JB) Se agrega Configuracion de Tress Automatiza ] ); //SEG_USUARIO_TRESSAUTOMATIZA // (JB) Se agrega Configuracion de Tress Automatiza
                             // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
                             GetMostrarAdvertenciaLicencia //SEG_MOSTRAR_ADV_LICENCIA
                              ] );
     EndLog;SetComplete;
end;

procedure TdmServerLogin.SetSeguridad(Empresa, Valores: OleVariant);
begin
     InitLog(Empresa,'SetSeguridad');
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;

          try
             EscribeClaves( Empresa, CLAVE_VENCIMIENTO            , IntToStr( Valores[ SEG_VENCIMIENTO ] ) );                 //EscribeValor( CLAVE_VENCIMIENTO );
             EscribeClaves( Empresa, CLAVE_LIMITE_PASSWORD        , IntToStr( Valores[ SEG_LIMITE_PASSWORD ] ) );             //EscribeValor( CLAVE_LIMITE_PASSWORD );
             EscribeClaves( Empresa, CLAVE_INTENTOS               , IntToStr( Valores[ SEG_INTENTOS ] ) );                    //EscribeValor( CLAVE_INTENTOS );
             EscribeClaves( Empresa, CLAVE_DIAS_INACTIVOS         , IntToStr( Valores[ SEG_DIAS_INACTIVOS ] ) );              //EscribeValor( CLAVE_DIAS_INACTIVOS );
             EscribeClaves( Empresa, CLAVE_TIEMPO_INACTIVO        , IntToStr( Valores[ SEG_TIEMPO_INACTIVO ] ) );             //EscribeValor( CLAVE_TIEMPO_INACTIVO );
             EscribeClaves( Empresa, CLAVE_MIN_LETRAS_PASSWORD    , IntToStr( Valores[ SEG_MIN_LETRAS_PASSWORD ] ) );         //EscribeValor( CLAVE_MIN_LETRAS_PASSWORD );
             EscribeClaves( Empresa, CLAVE_MIN_DIGITOS_PASSWORD   , IntToStr( Valores[ SEG_MIN_DIGITOS_PASSWORD ] ) );        //EscribeValor( CLAVE_MIN_DIGITOS_PASSWORD );
             EscribeClaves( Empresa, CLAVE_MAX_LOG_PASSWORD       , IntToStr( Valores[ SEG_MAX_LOG_PASSWORD ] ) );            //EscribeValor( CLAVE_MAX_LOG_PASSWORD );
             EscribeClaves( Empresa, CLAVE_USUARIO_TRESSAUTOMATIZA, IntToStr( Valores[ SEG_USUARIO_TRESSAUTOMATIZA ] ) );     // (JB) Se agrega Configuracion de Tress Automatiza

             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     // ToDo: Excepci�n silenciosa?
                     TerminaTransaccion( False );
                end;
          end;
     end;
     EndLog;SetComplete;
end;


procedure TdmServerLogin.GrabaBloqueoSistema(lValorBloqueo: WordBool);
begin
     InitLog(NULL,'GrabaBloqueoSistema');
     EscribeClavesTx( CLAVE_SISTEMA_BLOQUEADO, zBooltoStr(lValorBloqueo));
     EndLog;SetComplete;
end;

function TdmServerLogin.GetSistemaBloqueo: Boolean;
begin
     Result := zStrToBool( LeerClaves( CLAVE_SISTEMA_BLOQUEADO ) );
end;

function TdmServerLogin.GetClave(Clave: Integer): WideString;
begin
     InitLog(NULL,'GetClave');
     Result:= LeerClaves(Clave);
     EndLog;SetComplete;
end;

function TdmServerLogin.GetTipoLogin: Integer;
var
   oRegistry : TZetaRegistryServer;
begin
     oRegistry := TZetaRegistryServer.Create();
     try
        Result := oRegistry.TipoLogin;
     finally
            oRegistry.Free;
     end;
end;

procedure TdmServerLogin.GrabaTipoLogin(TipoLogin: Integer);
var
   oRegistry : TZetaRegistryServer;
begin
     oRegistry := TZetaRegistryServer.Create();
     oRegistry.TipoLogin := TipoLogin;
     oRegistry.Free;
end;


function TdmServerLogin.ValidarServerDB(Empresa:OleVariant): Boolean;
var
   oAutoServer: TAutoServer;
   oLicenseMgr : TLicenseMgr;
   procedure InitValues;
   begin
        oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
        oAutoServer := ZetaSQLBroker.GetAutoServer;
        try
           ZetaSQLBroker.ReadAutoServer( oAutoServer, AutoGetData, AutoSetData );
        finally
        end;

        //oAutoServer := TAutoServer.Create;
        //oAutoServer.Load;
        //oAutoServer.Cargar;

   end;
   procedure FreeValues;
   begin
        FreeAndNil( oLicenseMgr );
   end;
begin
     {SELECT SERVERPROPERTY (''edition'') as Resultado}
     InitValues;
     try
         if  (not oAutoServer.EsDemo) and( ( oAutoServer.Plataforma = ptProfesional) and (oAutoServer.SQLType = engMSSQL) ) then
         begin
              with oZetaProvider do
              begin
                   cdsTempo.InitTempDataSet;
                   cdsTempo.Data := OpenSQL(Empresa,'select @@version as Resultado',True);
                   Result := Pos( UpperCase('Express'),UpperCase(cdsTempo.FieldByName('Resultado').AsString) ) > 0 ;
              end;
         end
         else
             Result := True;
     finally
            FreeValues;
     end;
end;

function TdmServerLogin.LoginAD(const ComputerName: WideString;
  AllowReentry: WordBool; out Tipo: Integer; out Datos,
  Autorizacion: OleVariant; const sDominioUsuario: WideString): Integer;
var
   eLogin:eTipoLogin;
   Intentos :Integer;
begin
     //sDomainUser := LoggedOnUserNameEx(NameSamCompatible);
     if ValidarServerDB(oZetaProvider.Comparte) then
     begin
          eLogin := eTipoLogin(GetTipoLogin);
          case eLogin of
          tlAD:
               begin
                    Tipo := 1;
                    Result := UsuarioCargar(True,sDominioUsuario,VACIO,ComputerName,AllowReEntry,Datos,Autorizacion,Intentos);
               end;
          tlLoginTress:
                       begin
                            Tipo := 0;//Regresar login a Tress
                            Result := Ord(lrNotFound);
                       end;
          tlADLoginTress:
                         begin
                              Tipo := 2;
                              Result := UsuarioCargar(True,sDominioUsuario,VACIO,ComputerName,AllowReEntry,Datos,Autorizacion,Intentos);
                              if (eLogRePly(Result) = lrNotFound )then
                              begin
                                   Tipo := 0;
                              end
                         end;
          else
              Tipo := 0;
              Result := Ord(lrNotFound);
          end;
     end
     else
     begin
          Tipo := 0;
          Result := Ord(lrDBServerInvalid);
     end;
end;

function TdmServerLogin.GetTipoPeriodo(Empresa: OleVariant): OleVariant;
begin
     Result:= oZetaProvider.GetTiposPeriodo(Empresa);
end;

function TdmServerLogin.GetSeguridadEmpresa(
  Empresa: OleVariant): OleVariant;
{$ifdef VALIDAEMPLEADOSGLOBAL}
var
   oAutoServer: TAutoServer;
   oGlobalLicense : TGlobalLicenseValues;
   oLicenseMgr : TLicenseMgr;

   procedure InitValues;
   begin
     oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
     oAutoServer := ZetaSQLBroker.GetAutoServer;

     try
        ZetaSQLBroker.ReadAutoServer( oAutoServer, AutoGetData, AutoSetData );
     finally
     end;

     oGlobalLicense := TGlobalLicenseValues.Create( oAutoServer );
     oZetaProvider.EmpresaActiva := Empresa;

   end;

   procedure FreeValues;
   begin
     FreeAndNil( oGlobalLicense );
     FreeAndNil( oLicenseMgr );
   end;
begin
     InitLog(NULL,'GetSeguridadEmpresa');
     InitValues;
     oGlobalLicense.TotalGlobal := oAutoServer.EmpleadosConteo;
     oGlobalLicense.TipoCompany := oLicenseMgr.GetTipoValidoCompany;
     Result := oGlobalLicense.VarValues;
     FreeValues;
     EndLog;SetComplete;
{$else}
begin
   SetComplete;
{$endif}
end;

function TdmServerLogin.UsuarioLoginConsola(const Usuario, Clave: WideString; ActiveDir: WordBool; Intentos: Integer; const EquipoLocal: WideString): WideString;
var
   sFiltro, sSalida, sCorto, sNombre: string;
   iCodigo, iStatus, iDiasInactivo, iExpiracion, iMaxIntentos: integer;
   dValue: TDateTime;
begin
     //Result := Ord( lrNotFound );
     sCorto := VACIO;
     sNombre := VACIO;
     iCodigo := 0;
     iStatus := Ord( lrNotFound );
     sSalida := VACIO;

     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             if ActiveDir then
                sFiltro := Format( 'UPPER( US_DOMAIN ) = UPPER( ''%s'' )', [ Usuario ] )
             else
                 sFiltro := Format( 'UPPER( US_CORTO ) = UPPER( ''%s'' ) ', [ Usuario ] );

             cdsTempo.Data := OpenSQL( EmpresaActiva, Format( GetSQLTextCase( Q_VA_USUARIO_CONSOLA ), [ sFiltro ] ), TRUE );
             if not cdsTempo.Eof then
             begin
                  if zStrToBool( cdsTempo.FieldByName( 'US_ACTIVO' ).AsString ) then
                  begin
                       if( ( ActiveDir ) or ( ZetaServerTools.Decrypt( cdsTempo.FieldByName( 'US_PASSWRD' ).AsString ) = Clave ) )then
                       begin
                            if not zStrToBool( cdsTempo.FieldByName( 'US_BLOQUEA' ).AsString ) then
                            begin
                                 if ( not ( ( cdsTempo.FieldByName( 'GR_CODIGO' ).AsInteger <> K_GRUPO_SISTEMA ) and ( GetSistemaBloqueo ) ) ) then
                                 begin
                                      iDiasInactivo := GetDiasInactivo;
                                      dValue := cdsTempo.FieldByName( 'US_FEC_IN' ).AsDateTime;
                                      if ( not ( ( iDiasInactivo > 0 ) and ( dValue > NullDateTime ) and ( ( dValue + iDiasInactivo ) < Date ) ) )then
                                      begin
                                           iExpiracion := GetDiasExpiracion;
                                           if ( not ( ( not ActiveDir ) and ( iExpiracion > 0 ) and ( ( cdsTempo.FieldByName( 'US_FEC_PWD' ).AsDateTime + iExpiracion ) <= Date ) ) )then
                                           begin
                                                iCodigo := cdsTempo.FieldByName( 'US_CODIGO' ).AsInteger;
                                                sCorto := cdsTempo.FieldByName( 'US_CORTO' ).AsString;
                                                sNombre := cdsTempo.FieldByName( 'US_NOMBRE' ).AsString;
                                                iStatus := Ord( lrOK );
                                                //Result := Ord( lrOK );
                                           end
                                           else
                                               iStatus := Ord( lrExpiredPassword );
                                      end
                                      else
                                      begin
                                           BloqueaUsuario( cdsTempo.FieldByName( 'US_CORTO' ).AsString, EquipoLocal, 0 );
                                           iStatus := Ord( lrInactiveBlock );
                                      end;
                                 end
                                 else
                                     iStatus := Ord( lrSystemBlock );
                            end
                            else
                                iStatus := Ord( lrLockedOut ); //Result := Ord( lrLockedOut )
                       end
                       else
                       begin
                            iStatus := Ord( lrAccessDenied ); //Result := Ord( lrAccessDenied )
                            iMaxIntentos := GetIntentos;
                            if (iMaxIntentos > 0 )then
                            begin
                                 if (Intentos >= iMaxIntentos )then
                                 begin
                                      iStatus := Ord( lrLockedOut );
                                      Self.BloqueaUsuario( Usuario, EquipoLocal, Intentos );
                                 end;
                            end;
                       end;
                  end
                  else
                      iStatus := Ord( lrUserInactive ); //Result := Ord( lrUserInactive );
             end;
             TerminaTransaccion( TRUE );
          except
                TerminaTransaccion( FALSE );
          end;

          sSalida := Format( '%d|%d|%s|%s', [ iStatus, iCodigo, sCorto, sNombre ] ); // estatus|us_codigo|us_corto|us_nombre
     end;

     Result := sSalida;

     SetComplete;
end;

{* Regresa el numero de version y la version mas actual de Patch en disco para una Empresa dada. @autor Ricardo Carrillo Morales}
function TdmServerLogin.CheckDBServer(Empresa: OleVariant): Integer;
begin
      if ValidarServerDB(Empresa) then
         Result := 1
      else
          Result := 0;
end;

function TdmServerLogin.CheckVersionDatos(Empresa: OleVariant): OleVariant;
var
  Parametros: TZetaParams;
  DetallesBD: TDetallesBD;
begin
  oZetaProvider.EmpresaActiva := Empresa;
  DetallesBD := TDetallesBD.Create(oZetaProvider, oZetaProvider.EmpresaActiva[P_ALIAS]);
  Parametros := TZetaParams.Create;
  with Parametros do begin
    AddInteger('VersionActual', DetallesBD.DBVersion);
    AddInteger('VersionUltima', DetallesBD.DBMaxVersion);
  end;
  FreeAndNil(DetallesBD);
  Result := Parametros.VarValues;
  SetComplete;
end;

function TdmServerLogin.GetCompanysList(Usuario: Integer; Lista: OleVariant): OleVariant;
var
   sSQL, sListaTipos: String;
   eTipos :array of Integer;

   function ListaTipos( eTipos : Array of Integer ): String;
   var i: integer;
      eTipo: eTipoCompany;
   begin
       for i:=  Low(eTipos) to High(eTipos) do
          Result := EntreComillas (ObtieneElemento (lfTipoCompany, eTipos[i])) + ',' + Result;
   end;

begin
     InitLog(NULL,'GetCompanysList');
     eTipos := Lista;
     sListaTipos := CortaUltimo (ListaTipos(eTipos));

     if ( Usuario = 0 ) then
        sSQL := Format( GetSQLTextCase( Q_VA_COMPANYS_TODAS) , ['CM_CONTROL in (' + sListaTipos + ')' ] )
     else
        sSQL := Format( GetSQLTextCase( Q_VA_COMPANYS ), [ 'CM_CONTROL in (' + sListaTipos + ')', Usuario ] );

     with oZetaProvider do
     begin
          Result := OpenSQL( Comparte, sSQL, True );
     end;
     EndLog;SetComplete;
end;

function TdmServerLogin.GetNotificacionAdvertencia: OleVariant;
begin
     InitLog(NULL,'GetNotificacionAdvertencia');
     Result := VarArrayOf( [
                             // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
                             GetMostrarAdvertenciaLicencia, //SEG_MOSTRAR_ADV_LICENCIA
                             // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
                             GetServidorCorreos,            //SEG_SERVIDOR_CORREOS
                             GetPuertoSMTP,                 //SEG_PUERTO_SMTP
                             GetAutentificacionCorreo,      //SEG_AUTENTIFICACION_CORREO
                             GetUserID,                     //SEG_USER_ID
                             GetEmailPSWD,                  //SEG_EMAIL_PSWD
                             GetRecibirAdvertenciaPorEmail, //SEG_RECIBIR_ADV_CORREO
                             GetGrupoUsuariosEmail          //SEG_GRUPO_USUARIO_EMAIL
                              ] );
     EndLog;SetComplete;
end;

procedure TdmServerLogin.SetNotificacionAdvertencia(Empresa, Valores: OleVariant);
begin
     InitLog(Empresa,'SetNotificacionAdvertencia');
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             EscribeClaves( Empresa, CLAVE_MOSTRAR_ADV_LICENCIA   , BoolToStr( Valores[ SEG_MOSTRAR_ADV_LICENCIA ] ) );       // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
             EscribeClaves( Empresa, CLAVE_SERVIDOR_CORREOS       , Valores[ SEG_SERVIDOR_CORREOS ]);                         // US #11820 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
             EscribeClaves( Empresa, CLAVE_PUERTO_SMTP            , IntToStr( Valores[ SEG_PUERTO_SMTP ] ) );                 // US #11820 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
             EscribeClaves( Empresa, CLAVE_AUTENTIFICACION_CORREO , IntToStr( Valores[ SEG_AUTENTIFICACION_CORREO ] ) );      // US #11820 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
             EscribeClaves( Empresa, CLAVE_USER_ID                , Valores[ SEG_USER_ID ] );                                 // US #11820 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
             EscribeClaves( Empresa, CLAVE_EMAIL_PSWD             , Valores[ SEG_EMAIL_PSWD ] );                              // US #11820 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
             EscribeClaves( Empresa, CLAVE_RECIBIR_ADV_CORREO     , Valores[ SEG_RECIBIR_ADV_CORREO ] );                      // US #11820 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
             EscribeClaves( Empresa, CLAVE_GRUPO_USUARIO_EMAIL    , Valores[ SEG_GRUPO_USUARIO_EMAIL ] );                     // US #11820 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.

             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     // ToDo: Excepci�n silenciosa?
                     TerminaTransaccion( False );
                end;
          end;
     end;
     EndLog;SetComplete;
end;
{$IFNDEF DOS_CAPAS}

function TdmServerLogin.GetServicioReportesCorreos: OleVariant;
begin
     InitLog(NULL,'GetServicioReportesCorreos');
     Result := VarArrayOf( [ GetDirectorioServicioReportes,
                             GetDirectorioImagenes,
                             GetURLImagenes,
                             GetExpiracionImagenes,
                             GetRecuperarClaveServicioCorreos ] );

     EndLog;
     SetComplete;
end;

procedure TdmServerLogin.SetServicioReportesCorreos(Empresa, Valores: OleVariant);
begin
     InitLog(Empresa,'SetServicioReportesCorreos');
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             EscribeClaves( Empresa, CLAVE_DIRECTORIO_SERVICIO_REPORTES   ,  Valores[ K_DIRECTORIO_SERVICIO_REPORTES ] );
             EscribeClaves( Empresa, CLAVE_DIRECTORIO_IMAGENES   ,  Valores[ K_DIRECTORIO_IMAGENES ] );
             EscribeClaves( Empresa, CLAVE_URL_IMAGENES   ,  Valores[ K_URL_IMAGENES ] );
             EscribeClaves( Empresa, CLAVE_EXPIRACION_IMAGENES   ,  Valores[ K_EXPIRACION_IMAGENES ] );
             EscribeClaves( Empresa, CLAVE_RECUPERAR_PWD_SERVICIO_CORREOS     , Valores[ K_RECUPERAR_PWD_SERVICIO_CORREOS ] );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( False );
                end;
          end;
     end;
     EndLog;SetComplete;
end;

function ReemplazaVariablesCorreo( var sMensaje, sAttach, sURLImagen :string):string;
const
    K_IMAGEN = '/LOGOCORREO.JPG';
begin
    try
        if sURLImagen = VACIO then
        BEGIN
             sURLImagen :='http://www.tress.com.mx/imagesgti/'+ K_IMAGEN;
        END;

        sMensaje := StringReplace( sMensaje, '##IMAGEN', sURLImagen , [rfReplaceAll] );
        sMensaje := StringReplace( sMensaje, '##NOMBRE', sAttach, [rfReplaceAll] );
        Result := sMensaje;
    Except
        Result := VACIO;
    end;
end;

function TdmServerLogin.EnviaClaveTemporal(const sUsuario, sClaveCifrada,
  sCorreoRemitente, sDescripRemitente,
  sCorreoValidar: WideString): WordBool;
const
    NO_REPLY = 'noreply@';
    K_IMAGEN = '/LOGOCORREO.JPG';
    EXISTE_CORREO = 'select US_NOMBRE, US_EMAIL, US_CODIGO FROM USUARIO WHERE ( US_CORTO = %s )';
    CORREO_FORMATO = 'INSERT INTO ENVIOEMAIL (EE_TO, EE_CC, EE_SUBJECT, EE_BODY, EE_FROM_NA, EE_FROM_AD, EE_ERR_AD, EE_SUBTYPE, EE_STATUS, EE_ENVIADO,'+
    ' EE_MOD_ADJ, EE_FEC_IN, EE_FEC_OUT, EE_ATTACH, EE_BORRAR, EE_LOG, EE_ENCRIPT)'+
'VALUES (''%s'', '''', ''Sistema TRESS - Cambiar Clave de Acceso'', %s, '+
'''%s'', ''%s'',  '''', 1, 0, ''N'', ''N'', GETDATE(), '''', '''', ''N'', '''', ''S'')';
  SOLICITAR_NUEVA_CLAVE =  'update USUARIO SET US_CAMBIA = ''S'' where US_CODIGO = %d';
  CAMBIO_DE_CLAVE =  'update USUARIO SET US_PASSWRD = %s where US_CODIGO = %d';

var
   FDataset: TZetaCursor;
   sCorreo, sUsuarioNombre : string;
   iCodigoUsuario : integer;
   sClaveSinCifrar : string;
   sSQL: string;
   sBodyEncriptado, sBody : string;
   sURLImagen : string;
begin
     Result := True;
     try
         with oZetaProvider do
         begin
              EmpresaActiva := Comparte;
              FDataset := CreateQuery( Format(  EXISTE_CORREO , [ entrecomillas(sUsuario) ] ) );
              try
                 with FDataset do
                 begin
                      Active := True;
                      if EOF then
                         sCorreo := ''
                      else
                          sCorreo := FieldByName( 'US_EMAIL' ).AsString;
                          sUsuarioNombre := FieldByName( 'US_NOMBRE' ).AsString;
                          iCodigoUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
                      Active := False;
                 end;
              finally
                     FreeAndNil( FDataset );
              end;
         end;

         sBodyEncriptado := VACIO;

         if sCorreo <> VACIO then
         begin
              if AnsiUpperCase(sCorreoValidar) = AnsiUpperCase(sCorreo) then
              begin
                    sClaveSinCifrar := ZetaServerTools.Decrypt(sClaveCifrada);

                    sURLImagen := VACIO;
                    if FileExists(GetDirectorioImagenes + K_IMAGEN) then
                        sURLImagen := GetURLImagenes + K_IMAGEN;

                    sBody :=  GeneraMensajeHTMLLogin(sClaveSinCifrar,sUrlImagen, ReemplazaVariablesCorreo);

                    sBodyEncriptado := ZetaServerTools.Encrypt(sBody);

                    sSQL := Format(  CORREO_FORMATO , [ sCorreo, entrecomillas(sBodyEncriptado), sDescripRemitente, NO_REPLY+GetServidorCorreos]);
                    Ejecuta( sSQL );

                    sSQL := Format(  CAMBIO_DE_CLAVE , [entrecomillas(sClaveCifrada) , iCodigoUsuario ]);
                    Ejecuta( sSQL );

                    sSQL := Format(  SOLICITAR_NUEVA_CLAVE , [ iCodigoUsuario ]);
                    Ejecuta( sSQL );
              end
              else
              begin
                   Result := False;
              end;
         end
         else
         begin
              Result := False;
         end;
     Except
            Result := False;
     end;
end;

function TdmServerLogin.GetStatusTimbrado(Empresa, Parametros: OleVariant): OleVariant;
const
     K_CONTEO_TIMBRADO = 'select pendientes,timbradas,PendientesTotal, TimbradoPendientes, CancelacionPendiente ,TimbradasTotal from dbo.Periodo_GetStatusTimbrado( :Anio, :Tipo, :Numero, :Nivel0 )';
var
   FConteoTimbrado: TZetaCursor;
   oParametros: TZetaParams;
begin
     oParametros:= TZetaParams.Create;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          Result := VarArrayCreate( [0,1], varVariant );
          FConteoTimbrado := CreateQuery(K_CONTEO_TIMBRADO);
          try
             AsignaParamList( Parametros );
             with ParamList do
             begin
                  ParamAsInteger(FConteoTimbrado, 'Anio', ParamByName( 'Anio' ).AsInteger);
                  ParamAsInteger(FConteoTimbrado, 'Tipo', ParamByName( 'Tipo' ).AsInteger);
                  ParamAsInteger(FConteoTimbrado, 'Numero', ParamByName( 'Numero' ).AsInteger);
                  ParamAsString(FConteoTimbrado, 'Nivel0', ParamByName( 'Nivel0' ).AsString);
             end;
             FConteoTimbrado.Active := True;

             oParametros.AddInteger('Pendientes', FConteoTimbrado.FieldByName('Pendientes').AsInteger );
             oParametros.AddInteger('Timbradas', FConteoTimbrado.FieldByName('Timbradas').AsInteger );
             //Pendiente de timbrado
             oParametros.AddInteger('TimbradoPendientes', FConteoTimbrado.FieldByName('TimbradoPendientes').AsInteger );
             oParametros.AddInteger('CancelacionPendiente', FConteoTimbrado.FieldByName('CancelacionPendiente').AsInteger );
             oParametros.AddInteger('PendientesTotal', FConteoTimbrado.FieldByName('PendientesTotal').AsInteger );
             oParametros.AddInteger('TimbradasTotal', FConteoTimbrado.FieldByName('TimbradasTotal').AsInteger );

             Result := oParametros.VarValues;
          finally
                 FreeAndNil(FConteoTimbrado);
          end;
     end;
end;

procedure TdmServerLogin.AnaliticaContabilizarUso(Empresa: OleVariant;
  const sModulo: WideString; HelpContext, Conteo: Integer);
var
   oZetaProviderCompany: TdmZetaServerProvider;

begin
        oZetaProviderCompany := TdmZetaServerProvider.Create( nil );
        try

             with oZetaProviderCompany do
             begin
                  EmpresaActiva := Empresa;
                  ExecSQL( EmpresaActiva, Format( GetSQLTextCase( Q_ANALITICA_CONTABILIDAD_USO ), [EntreComillas (sModulo), HelpContext, Conteo] ) );
             end;
        Finally
               FreeAndNil( oZetaProviderCompany );
        end;
end;

initialization
  TComponentFactory.Create(ComServer, TdmServerLogin, Class_dmServerLogin, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}

end.
