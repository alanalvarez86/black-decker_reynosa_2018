unit DServerEvaluacion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, {BdeMts,} DataBkr, DBClient,
     MtsRdm, Mtx, Db,
     {$ifndef VER130}Variants,{$endif}
     {$ifndef DOS_CAPAS}
     Evaluacion_TLB,
     {$Endif}
     DXMLTools,
     DBuzon,
     ZetaCommonLists,
     ZetaSQLBroker,
     ZAgenteSQL,
     ZCreator,
     FAutoServer,
     FAutoClasses,
     ZetaCommonClasses,
     DZetaServerProvider,
     ZetaServerDataSet;

type
  eTipoTabla = ( eGeneral,
                 eCompetencia,
                 eTCompete,
                 ePreguntas,
                 eEscalas,
                 eEscalasNiveles,
                 eCriteriosEval,
                 eEscalasEncuesta,
                 ePerilEvaluadores,
                 eEncuesta,
                 eNivelesEnc,
                 ePreguntaCriterios,
                 eEvalua,
                 eSujeto );

  eAllScripts = ( eGetEncuesta,
                  eEncuestaAnterior,
                  eEncuestaSiguiente,
                  eCambiaPregunta,
                  eBorraPregunta,
                  eSiguientePregunta,
                  eNivelesEscala,
                  eBorraEscala,
                  eCriteriosEvaluar,
                  eCambiaCriterio,
                  eBorraCriterio,
                  ePreguntasCriterio,
                  eCambiaPreguntaCriterio,
                  eBorraPreguntaCriterio,
                  ePerfilEvaluador,
                  eEmpEvaluar,
                  eMaxEncuesta,
                  eCreaEscala,
                  eCopiaEncuesta,
                  eAgregaCriterios,
                  eContadoresEncuesta,
                  eNumeraPreguntas,
                  eAgregaEvaluador,
                  eEscalaEnc,
                  eNivelesEscalaEnc,
                  eMaxTipoEvaluador,
                  eEvaluados,
                  eAgregaEvaluaciones,
                  eBorraEvaluaciones,
                  eModificaEvaluaciones,
                  eObtieneEvaluadores,
                  eObtieneEvaluaciones,
                  eAnalisisCriterio,
                  eAnalisisPreguntas,
                  eFrecuenciaResp,
                  eResultadoCriterio,
                  eResultadoPregunta,
                  eObtieneSujetos,
                  eBorraEmpEvaluar,
                  eModificaStatusSujeto,
                  eAgregaSujetos,
                  eSpCuentaSujetos,
                  ePrepararEvaluaciones,
                  eCalcularResultados,
                  eRecalcularPromedios,
                  ePreguntasContestadas,
                  eTotalizaEncuesta,
                  eTotalesEncuesta,
                  eInicializaEncuesta,
                  eCuentaEvaluaciones,
                  eEvaluaciones,
                  e_Va_Empleado,
                  e_Va_Empleado_Siguiente,
                  e_Va_Empleado_Anterior,
                  eEmpleadosBuscados,
                  eDatosEmpleadoXML,
                  eCreaEncuestaSimple,
                  eAgregaEncEsca,
                  eAgregaNivelesEscala,
                  eAgregaPreguntas,
                  eAgregaCriterio,
                  eVerificaSujeto,
                  eNivelesEncuesta );

  { Herencia Temporal De Provider }
  TdmZetaWorkFlowProvider = class(TdmZetaServerProvider)
  private
    { Private Declarations }
    FGlobalUpdate: TZetaCursor;
    FGlobalInsert: TZetaCursor;
    function GlobalWriteExecute(Cursor: TZetaCursor; const iCodigo: Integer; const sFormula: String): Boolean;
    procedure GlobalWrite(const iCodigo: Integer; const sFormula: String);
  public
    { Public Declarations }
    procedure GlobalWriteInteger(const iCodigo, iValor: Integer);
    procedure GlobalWriteString(const iCodigo: Integer; const sValor: String);
  end;
  TXSLTMethod = function( const iEncuesta: Integer; Dataset: TDataset; const sPlantilla: String; var sHTML: String ): Boolean of object;
  TDatosAdministrador = record
    Nombre: String;
    EMail: String;
  end;
  TdmServerEvaluacion = class(TMtsDataModule{$ifndef DOS_CAPAS}, IdmServerEvaluacion{$endif} )
    cdsAgregarCriterios: TServerDataSet;
    cdsAgregarEvaluador: TServerDataSet;
    cdsSujetos: TServerDataSet;
    cdsLista: TServerDataSet;
    cdsListaUsuarios: TServerDataSet;
    cdsDatos: TServerDataSet;
    cdsCorreos: TServerDataSet;
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
    procedure BeforeUpdateEscalas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateEscNiveles(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateEncuesta( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateObtieneFEncuesta( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateEscalasEnc(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateEscNivelesEnc(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
  private
    {$ifndef DOS_CAPAS}
    //oAutoServer: TAutoServer;
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
    {$endif}
    FXMLTools: TdmXMLTools;
    FBuzon: TBuzon;
    oZetaProvider: {$ifdef ANTES}TdmZetaServerProvider{$else}TdmZetaWorkFlowProvider{$endif};
    FNumEncuesta: Integer;
    FEscalaEvaluacion: TCodigo;
    FEscalaImportancia: TCodigo;
    FEscalaCalificacion: TCodigo;
    FPregunta: TZetaCursor;
    FEncuesta: TZetaCursor;
    FCriterios: TZetaCursor;
    FEvaluador: TZetaCursor;
    FEvaluacion: TZetaCursor;
    FEscalas: TZetaCursor;
    FEmpleado: TZetaCursor;
    FSujetoDataSet: TZetaCursor;
    FEvaluaDataSet: TZetaCursor;
    FVerificaEmpleado: TZetaCursor;
    FCodigo: string;
    FListaParametros: string;
    FDatosAdministrador: TDatosAdministrador;
    function CreaEscalasEncuesta( const iEncuesta: Smallint; const sEscala: WideString): Smallint;
    function GetScript( const eScript: eAllScripts ): String;
    function GetTabla( const eTabla: eTipoTabla; const Empresa: OleVariant; const sFiltro: string = VACIO ): OleVariant;
    function GrabaTabla(const eTabla: eTipoTabla; const Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
    function GeneraXMLParaCorreos( const sTag: String ): WideString;
    function GetMaxEncuesta: Integer;
    function GetSQLBroker: TSQLBroker;
    procedure EscribeCorreoEnBuzon( const iEncuesta: Integer; DataSet: TDataSet; Metodo: TXSLTMethod );
    procedure InitBroker;
    procedure InitCreator;
    procedure ClearBroker;
    procedure ContadoresSujeto( Empresa: OleVariant; const iEncuesta, iEmpleado: Integer );
    procedure SetTablaInfo( eTabla : eTipoTabla );
    procedure ContadoresEncuesta( Empresa: OleVariant; const iEncuesta: Integer);
    procedure EvaluaCriterios( const iEncuesta, iOrden: Integer; const oData: OleVariant );
    procedure EvaluaTipoEvaluador( const iEncuesta, iOrden: Integer; const oData: OleVariant );
    procedure NumeraPreguntas( Empresa: OleVariant; const iEncuesta: Integer);
    procedure CreaMatrizEmpleadosEvaluarBegin;
    procedure CreaMatrizEmpleadosEvaluarEnd;
    procedure CreaMatrizEmpleadosEvaluar( const iEncuesta, iEvaluador, iEmpleado: Integer; const eRelacion: eTipoEvaluaEnc; const lContadoresSujeto: Boolean = True );
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    property SQLBroker: TSQLBroker read GetSQLBroker;
    property Buzon: TBuzon read FBuzon;
  {$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
  {$endif}
    procedure CambiaPregunta(Empresa: OleVariant; const CM_CODIGO: WideString; iActual, iNuevo: Smallint);{$ifndef DOS_CAPAS} safecall;{$endif}
    procedure BorraPregunta(Empresa: OleVariant; const CM_CODIGO: WideString; iActual: Smallint); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure CambiaCriterios(Empresa: OleVariant; iEncuesta: Integer; iActual, iNueva: Smallint); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure BorraCriterio(Empresa: OleVariant; iEncuesta: Integer; iPosicion: Smallint); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure CambiaPreguntaCriterio(Empresa: OleVariant; iEncuesta: Integer;  iCriterio, iActual, iNueva: Smallint); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure BorraPreguntaCriterio(Empresa: OleVariant; iEncuesta: Integer; iCriterio, iPregunta: Smallint); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure CopiaEncuesta(Empresa: OleVariant; iEncuesta: Integer; const sNombre: WideString; lSujetos, lEvaluadores: WordBool); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure AgregaCriterios(Empresa: OleVariant; iEncuesta: Integer; iCriterios: Smallint; oData: OleVariant); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure ContadoresDEncuesta(Empresa: OleVariant; iEncuesta: Integer); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure AgregaEvalua(Empresa: OleVariant; iEncuesta, iSujeto, iRelacion, iEmpleado: Integer; iStatus: Smallint; lInserta: WordBool); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure BorraEvalua(Empresa: OleVariant; iEncuesta, iEmpleado, iSujeto: Integer); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure BorraEmpEvaluar(Empresa: OleVariant; iEncuesta, iEmpleado: Integer); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure CambiaStatusSujeto(Empresa: OleVariant; iEncuesta,  iEmpleado: Integer; lPonerListo: WordBool); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure ImportarEncuesta(Empresa: OleVariant; const sArchivo: WideString); {$ifndef DOS_CAPAS} safecall;{$endif}
    procedure GetParametrosURL(Empresa_: OleVariant; const sEmpresa: WideString; Usuario: Integer; out RutaVirtual: WideString; out Empleado: Integer; out Nombre: WideString); {$ifndef DOS_CAPAS} safecall;{$endif}
    //WizEvalAgregaEmpEvaluarGlobal
    procedure AgregaEmpEvaluarGlobalParametros;
    procedure AgregaEmpEvaluarGlobalBuildDataset;
    function AgregaEmpEvaluarGlobalDataset(Dataset: TDataset): OleVariant;
    //WizCierraEvaluaciones
    procedure CierraEvaluacionesParametros;
    procedure CierraEvaluacionesBuildDataset;
    function CierraEvaluacionesDataset(Dataset: TDataset): OleVariant;
    procedure InicializaEncuesta(Empresa: OleVariant; iEncuesta: Integer); {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpleado(Empresa: OleVariant; Empleado, Encuesta: Integer): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpleadoSiguiente(Empresa: OleVariant; Empleado, Encuesta: Integer): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpleadoAnterior(Empresa: OleVariant; Empleado, Encuesta: Integer): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpleadosBuscados(Empresa: OleVariant; const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: WideString; iEncuesta: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function EncuestaSiguiente(Empresa: OleVariant; iEncuesta: Integer; out oDelta: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function EncuestaAnterior(Empresa: OleVariant; iEncuesta: Integer; out oDelta: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCatCompetencias(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCatHabilidades(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCatPreguntas(Empresa: OleVariant; const sCompetencia: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCatNivelesEscala(Empresa: OleVariant; const sEscala: WideString; out oEscNiv: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;
    function GetCatNivelesEnc(Empresa: OleVariant; const sFiltro: WideString): OleVariant;
          safecall;{$endif}
    function GrabaCompetencia(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaTCompetencia(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCatEscalas(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function SiguientePregunta(Empresa: OleVariant): Smallint; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaPregunta(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEscalasNiveles(Empresa: OleVariant; const sEscala: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaEscalaNiveles(Empresa, oDelta, oEscNiv: OleVariant; out ErrorCount: Integer; out oResultEscNiv: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCatEscalas(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function BorraEscala(Empresa: OleVariant; const sEscala: WideString; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCriteriosEvaluar(Empresa: OleVariant; iEncuesta: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCriteriosEval(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetPreguntasCriterio(Empresa: OleVariant; iEncuesta: Integer; iCriterio: Smallint): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEscalasEncuesta(Empresa: OleVariant;iEncuesta: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetPerfilEvaluador(Empresa: OleVariant; iEncuesta: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEmpEvaluar(Empresa: OleVariant; iEncuesta, iEmpleado: Integer; iStatus: Smallint): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEncuestas(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaEncuesta(Empresa, oDelta, oDataCriterio, oDataEval: OleVariant; lGrabaCriterios: WordBool; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEncuesta(Empresa: OleVariant; iEncuesta: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaPreguntasCriterio(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEscalaEncuestaNiv(Empresa: OleVariant; iEncuesta: Integer; const sEscala: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaEscalaNivelesEnc(Empresa, oDelta, oEscNivEnc: OleVariant; out ErrorCount: Integer; out oResultEscNivEnc: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaPerfilEvaluador(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetMaxTipoEvaluador(Empresa: OleVariant;iEncuesta: Integer): Smallint; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEvalua(Empresa: OleVariant; iEncuesta, iEmpleado: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetDatosEncuesta(Empresa: OleVariant; iEncuesta: Integer; out ParamEvaluacion, ParamEvaluador, ParamEvaluado: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEvaluadores(Empresa: OleVariant; iEncuesta: Integer; const sFiltro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEvaluaciones(Empresa: OleVariant; iEncuesta: Integer; const sFiltro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetAnalisisCriterio(Empresa: OleVariant; iEncuesta: Integer; iMostrar, iRelacion, iOrden: Smallint): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetAnalisisPreguntas(Empresa: OleVariant; iEncuesta: Integer; iMostrar, iRelacion, iCriterio, iOrden: Smallint): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetPreguntasEnc(Empresa: OleVariant; iEncuesta: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetFrecuenciaResp(Empresa: OleVariant; iEncuesta: Integer; iPregunta, iRelacion: Smallint): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetResultadoCriterio(Empresa: OleVariant; iEncuesta, iEmpleado: Integer; iMostrar, iOrden: Smallint): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetResultadoPregunta(Empresa: OleVariant; iEncuesta, iEmpleado: Integer; iMostrar, iOrden: Smallint): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetSujetos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function AgregaEmpEvaluar(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    //WizEvalAgregaEmpEvaluarGlobal
    function AgregaEmpEvaluarGlobal(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function AgregaEmpEvaluarGlobalGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function AgregaEmpEvaluarGlobalLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    //WizPrepararEvaluaciones
    procedure PrepararEvaluacionesParametros;
    function PrepararEvaluaciones(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    //WizCalcularResultados
    procedure CalcularResultadosParametros;
    function CalcularResultados(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    //WizRecalcularPromedios
    procedure RecalcularPromediosParametros;
    function RecalcularPromedios(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetPregContestadas(Empresa: OleVariant; iUsuario, iEmpleado, iEncuesta: Integer): Integer; {$ifndef DOS_CAPAS} safecall;{$endif}
    //WizCierraEvaluaciones
    function CierraEvaluaciones(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function CierraEvaluacionesGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function CierraEvaluacionesLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    //WizCorreosInvitacion
    procedure CorreosInvitacionParametros;
    procedure CorreosInvitacionBuildDataset;
    function CorreosInvitacionConstruyeDataset( const iEncuesta: Integer; Dataset: TDataset; const sPlantilla: String; var sHTML: String ): Boolean;
    function CorreosInvitacionDataset(Dataset: TDataset): OleVariant;
    function CorreosInvitacionGetLista(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function CorreosInvitacion(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function CorreosInvitacionLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function CopiaEscalasEncuesta(Empresa: OleVariant; iEncuesta: Smallint; const sEscala: WideString): Smallint;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetDatosXEvaluado(Empresa: OleVariant; iEncuesta, iSujeto: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    //WizCorreosNotificacion
    function CorreosNotificacionConstruyeDataset( const iEncuesta: Integer; Dataset: TDataset; const sPlantilla: String; var sHTML: String ): Boolean;
    function CorreosNotificacion(Empresa, Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCorreos(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;
    function GetGlobales(Empresa: OleVariant): OleVariant; safecall;
    procedure GrabaGlobales(Empresa, Valores: OleVariant); safecall;{$endif}
    function GetUsuarios(Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetRolesUsuario(Usuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaUsuarios(Usuario: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    //WizAsignaRelaciones
    procedure AsignaRelacionesParametros;
    procedure AsignaRelacionesBuildDataset;
    function AsignaRelacionesDataset(Dataset: TDataset): OleVariant;
    function AsignaRelaciones(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function AsignaRelacionesGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function AsignaRelacionesLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function ExportarEncuesta(Empresa: OleVariant; iEncuesta, iUsuario: Integer): WideString; {$ifndef DOS_CAPAS} safecall;{$endif}
    //WizCreaEncuestaSimple
    procedure CreaEncuestaSimpleParametros;
    procedure SetAgregarEncuestaParametros;
    procedure CreaEncuestaSimpleBuildDataset;
    function CreaEncuestaSimpleDataset( Dataset: TDataset ): OleVariant;
    function CreaEncuestaSimple(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function CreaEncuestaSimpleGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function CreaEncuestaSimpleLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function AgregarEncuestaParametros(Empresa,Parametros: OleVariant): OleVariant; safecall; {$ifndef DOS_CAPAS} safecall;{$endif}
  end;

implementation

uses ZetaServerTools,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaEvaluacionTools,
     ZetaTipoEntidad,
     ZetaTipoEntidadTools;

const
     K_UNKNOWN = '???';
     K_TIPO_PLANTILLA = 0;
     K_FORMATO_TEXTO = 0;
     K_CAMPOS_ENCUESTA = 'select ET_CODIGO, ET_NOMBRE, ET_STATUS, ET_GET_COM, ET_ESC_PES, ET_ESC_DEF, ET_USR_ADM  ';
     K_CAMPOS_ENCUESTA_ADICIONALES = ' ET_DESCRIP, ET_FEC_INI, ET_FEC_FIN, ET_NIVELES, ET_PESOS, ET_TOT_PES, ET_NUM_COM, ET_NUM_SUJ,'+
                                     ' ET_FIN_SUJ, ET_NUM_PRE, ET_ESCALA, ET_MSG_INI, ET_MSG_FIN, ET_TXT_COM, ET_NUM_EVA, ET_FIN_EVA,'+
                                     ' ET_ESTILO, ET_DOCUMEN, ET_ESC_CAL, ET_PTS_CAL, ET_CONSOLA ';

{$R *.DFM}

function SQLSniffer( const sValue: String ): String;
var
    oArchivo: TStrings;
begin
     oArchivo := TStringList.Create;
     try
        with oArchivo do
        begin
             Add( sValue );
             SaveToFile( 'D:\Temp\SQLSniffer.txt' );
        end;
     finally
            FreeAndNil( oArchivo );
     end;
end;

{ ******** DZetaWorkFlowProvider ******** }

function TdmZetaWorkFlowProvider.GlobalWriteExecute( Cursor: TZetaCursor; const iCodigo: Integer; const sFormula: String): Boolean;
const
     K_ANCHO_FORMULA = 255;
begin
     ParamAsInteger( Cursor, 'Codigo', iCodigo );
     ParamAsVarChar( Cursor, 'Formula', sFormula, K_ANCHO_FORMULA );
     { Para que el resultado sea TRUE, tiene que existir el Registro }
     { Si no existe, entonces se tiene que agregar; }
     Result := ( Ejecuta( Cursor ) = 1 );
end;

procedure TdmZetaWorkFlowProvider.GlobalWrite( const iCodigo: Integer; const sFormula: String );
const
     K_QUERY_UPDATE = 'update GLOBAL set GL_FORMULA = :Formula where ( GL_CODIGO = :Codigo )';
     K_QUERY_INSERT = 'insert into GLOBAL ( GL_CODIGO, GL_FORMULA ) values ( :Codigo, :Formula )';
begin
     if not Assigned( FGlobalUpdate ) then
     begin
          FGlobalUpdate := CreateQuery( K_QUERY_UPDATE );
     end;
     EmpiezaTransaccion;
     try
        if not GlobalWriteExecute( FGlobalUpdate, iCodigo, sFormula ) then
        begin
             { Si no existe el registro, lo Agrega }
             { Como no es muy comun que suceda esto, no se crea el Query }
             { sino hasta que se necesita }
             if not Assigned( FGlobalInsert ) then
                FGlobalInsert := CreateQuery( K_QUERY_INSERT );
             GlobalWriteExecute( FGlobalInsert, iCodigo, sFormula );
        end;
        TerminaTransaccion( True );
     except
           on Error: Exception do
           begin
                TerminaTransaccion( False );
                raise;
           end;
     end;
end;

procedure TdmZetaWorkFlowProvider.GlobalWriteString( const iCodigo: Integer; const sValor: String );
begin
     GlobalWrite( iCodigo, sValor );
end;

procedure TdmZetaWorkFlowProvider.GlobalWriteInteger( const iCodigo, iValor: Integer );
begin
     GlobalWrite( iCodigo, IntToStr( iValor ) );
end;

{ ************* TdmServerEvaluacion ******** }

class procedure TdmServerEvaluacion.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerEvaluacion.MtsDataModuleCreate(Sender: TObject);
begin
     {$ifdef ANTES}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     {$else}
     oZetaProvider := TdmZetaWorkFlowProvider.Create( Self );
     {$endif}
     FBuzon := TBuzon.Create;
     FXMLTools := TdmXMLTools.Create( Self );
end;

procedure TdmServerEvaluacion.MtsDataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FXMLTools );
     FreeAndNil( FBuzon );
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     {$ifdef ANTES}
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
     {$else}
     FreeAndNil( oZetaProvider );
     {$endif}
end;

function TdmServerEvaluacion.GetScript( const eScript: eAllScripts ): String;
begin
     case eScript of
          eGetEncuesta:      Result := K_CAMPOS_ENCUESTA + '%s from ENCUESTA where ( ET_CODIGO = %d )';
          eEncuestaAnterior: Result := K_CAMPOS_ENCUESTA + ' from ENCUESTA where( ET_CODIGO = ( select MAX( ENC2.ET_CODIGO ) from ENCUESTA ENC2 where( ENC2.ET_CODIGO < %d ) ) )';
          eEncuestaSiguiente:Result := K_CAMPOS_ENCUESTA + ' from ENCUESTA where( ET_CODIGO = ( select MIN( ENC2.ET_CODIGO ) from ENCUESTA ENC2 where( ENC2.ET_CODIGO > %d ) ) )';
          eCambiaPregunta:   Result := 'execute procedure DBO.SP_CAMBIA_PREGUNTA( :CM_CODIGO, :Actual, :Nuevo )';
          eBorraPregunta:    Result := 'execute procedure DBO.SP_BORRA_PREGUNTA( :CM_CODIGO, :Actual )';
          eSiguientePregunta:Result := 'select MAX( PG_FOLIO ) MAXIMO from PREGUNTA';
          eNivelesEscala:    Result := 'select SC_CODIGO,SN_ORDEN,SN_NOMBRE,SN_CORTO,SN_DESCRIP,SN_VALOR from ESCNIVEL where( SC_CODIGO = %s )';
          eBorraEscala:      Result := 'delete from ESCALA where( SC_CODIGO = %s )';
          eCriteriosEvaluar: Result := 'select ENC_COMP.ET_CODIGO, EC_ORDEN, EC_NOMBRE, CM_CODIGO, EC_PESO, ENC_NIVE.EL_NOMBRE, EC_NUM_PRE, EC_GET_COM, EC_DETALLE, EC_POS_PES, EC_DOCUMEN '+
                                       'from ENC_COMP '+
                                       'left outer join ENCUESTA on ( ENC_COMP.ET_CODIGO = ENCUESTA.ET_CODIGO ) '+
                                       'left outer join ENC_NIVE on ( ENC_COMP.ET_CODIGO = ENC_NIVE.ET_CODIGO ) and ( ENC_NIVE.EE_CODIGO = ENCUESTA.ET_ESC_PES ) and ( ENC_COMP.EC_POS_PES = ENC_NIVE.EL_ORDEN ) '+
                                       'where ( ENC_COMP.ET_CODIGO = %d ) order by ENC_COMP.EC_ORDEN';
          eCambiaCriterio:   Result := 'execute procedure DBO.SP_CAMBIA_ENC_COMP( :Encuesta, :Actual, :Nuevo )';
          eBorraCriterio:    Result := 'execute procedure DBO.SP_BORRA_ENC_COMP( :Encuesta, :Posicion )';
          ePreguntasCriterio:Result := 'select ENC_PREG.ET_CODIGO, EP_ORDEN, ENC_PREG.EC_ORDEN, EP_DESCRIP, EP_PESO, EL_NOMBRE, EP_NUMERO, EP_GET_COM, EC_NOMBRE, ENC_PREG.EE_CODIGO, '+
                                       'EP_POS_PES, EP_GET_NA, EP_TIPO, ENC_COMP.CM_CODIGO '+
                                       'from ENC_PREG '+
                                       'left outer join ENCUESTA on ( ENC_PREG.ET_CODIGO = ENCUESTA.ET_CODIGO ) '+
                                       'left outer join ENC_NIVE on ( ENC_PREG.ET_CODIGO = ENC_NIVE.ET_CODIGO ) and ( ENC_NIVE.EE_CODIGO = ENCUESTA.ET_ESC_PES ) and ( ENC_PREG.EP_POS_PES = ENC_NIVE.EL_ORDEN ) '+
                                       'left outer join ENC_COMP on ( ENC_PREG.ET_CODIGO = ENC_COMP.ET_CODIGO ) and ( ENC_COMP.EC_ORDEN = %1:d ) ' +
                                       'where( ENC_PREG.ET_CODIGO = %0:d ) and ( ENC_PREG.EC_ORDEN = %1:d ) '+
                                       'order by EP_ORDEN';
          eCambiaPreguntaCriterio: Result := 'execute procedure DBO.SP_CAMBIA_ENC_PREG( :Encuesta, :Criterio, :Actual, :Nuevo )';
          eBorraPreguntaCriterio:  Result := 'execute procedure DBO.SP_BORRA_ENC_PREG( :Encuesta, :Criterio, :Pregunta )';
          ePerfilEvaluador:  Result := 'select ENC_RELA.ET_CODIGO, ER_NOMBRE, ER_TIPO, ER_NUM_MIN, ER_NUM_MAX, ER_PESO, EL_NOMBRE, ER_POS_PES '+
                                       'from ENC_RELA '+
                                       'left outer join ENCUESTA on ( ENC_RELA.ET_CODIGO = ENCUESTA.ET_CODIGO ) '+
                                       'left outer join ENC_NIVE on ( ENC_RELA.ET_CODIGO = ENC_NIVE.ET_CODIGO ) and ( ENC_NIVE.EE_CODIGO = ENCUESTA.ET_ESC_PES ) and ( ENC_RELA.ER_POS_PES = ENC_NIVE.EL_ORDEN ) '+
                                       'where( ENC_RELA.ET_CODIGO = %d ) '+
                                       'order by ER_TIPO';
          eEmpEvaluar:       Result := 'select SUJETO.ET_CODIGO, SUJETO.CB_CODIGO, PRETTYNAME, PU_DESCRIP, SJ_STATUS, SJ_NUM_EVA, SJ_FIN_EVA '+
                                       'from SUJETO '+
                                       'left outer join COLABORA on ( SUJETO.CB_CODIGO = COLABORA.CB_CODIGO ) '+
                                       'left outer join PUESTO on ( COLABORA.CB_PUESTO = PUESTO.PU_CODIGO ) '+
                                       'where ( SUJETO.ET_CODIGO = %d ) %s '+
                                       'order by SUJETO.CB_CODIGO';
          eMaxEncuesta:      Result := 'select MAX(ET_CODIGO) MAXIMO from ENCUESTA';
          eCreaEscala:       Result := 'execute procedure DBO.SP_CREA_ENC_ESCA( :Encuesta, :Escala )';
          eCopiaEncuesta:    Result := 'execute procedure DBO.SP_COPIA_ENCUESTA( :Enc_Anterior, :Enc_Nueva, :Nombre, :Sujetos, :Evaluadores )';
          eAgregaCriterios:  Result := 'execute procedure DBO.SP_CREA_ENC_COMP( :Encuesta, :Orden, :Codigo )';
          eContadoresEncuesta:Result:= 'execute procedure DBO.SP_CONTADORES_ENCUESTA( :Encuesta )';
          eNumeraPreguntas:  Result := 'execute procedure DBO.SP_NUMERA_PREGUNTAS( :Encuesta )';
          eAgregaEvaluador:  Result := 'execute procedure DBO.SP_CREA_ENC_RELA( :Encuesta, :Orden, :Tipo, :DescTipo )';
          eEscalaEnc:        Result := 'select ET_CODIGO, EE_CODIGO, EE_NOMBRE, EE_NIVELES from ENC_ESCA where( ET_CODIGO = %d ) %s';
          eNivelesEscalaEnc: Result := 'select ET_CODIGO, EE_CODIGO, EL_ORDEN, EL_NOMBRE, EL_CORTO, EL_DESCRIP, EL_VALOR from ENC_NIVE where( ET_CODIGO = %d ) and ( EE_CODIGO = ''%s'' )';
          eMaxTipoEvaluador: Result := 'select MAX(ER_TIPO) as MAXIMO from ENC_RELA where( ET_CODIGO = %d )';
          eEvaluados:        Result := 'select EV_FOLIO, EVALUA.US_CODIGO, EVALUA.CB_CODIGO, EV_RELACIO, EV_STATUS, EV_TOTAL, EV_COMENTA, EV_FEC_INI, EV_FEC_FIN, EV_FIN_PRE, ET_CODIGO, '+
                                       'COLABORA.PRETTYNAME from EVALUA '+
                                       'left outer join COLABORA on( EVALUA.CB_CODIGO = COLABORA.CB_CODIGO ) '+
                                       'where( ET_CODIGO = %d )and( EVALUA.CB_CODIGO = %d )';
          eAgregaEvaluaciones:Result:= 'insert into EVALUA( EV_FOLIO, US_CODIGO, CB_CODIGO, EV_RELACIO, EV_STATUS, ET_CODIGO ) '+
	                               'values( NewId(), :US_CODIGO, :CB_CODIGO, :EV_RELACIO, :EV_STATUS, :ET_CODIGO )';
          eBorraEvaluaciones: Result:= 'delete from EVALUA where( ET_CODIGO = :ET_CODIGO ) and ( CB_CODIGO = :CB_CODIGO ) and ( US_CODIGO = :US_CODIGO )';
          eModificaEvaluaciones:Result:= 'update EVALUA set EV_RELACIO = :EV_RELACIO where( ET_CODIGO = :ET_CODIGO ) and (CB_CODIGO = :CB_CODIGO ) and ( US_CODIGO = :US_CODIGO )';
          eObtieneEvaluadores: Result := 'select * from DBO.SP_STATUS_EVALUADORES( %d ) %s order by US_CODIGO';
          eObtieneEvaluaciones: Result := 'select EVALUA.CB_CODIGO, PRETTYNAME, EV_RELACIO, EVALUA.US_CODIGO, EV_STATUS, EV_FIN_PRE '+
                                          'from EVALUA '+
                                          'left outer join COLABORA on( EVALUA.CB_CODIGO = COLABORA.CB_CODIGO ) '+
                                          'where( ET_CODIGO = %d ) %s '+
                                          'order by EVALUA.CB_CODIGO, EVALUA.EV_RELACIO, EVALUA.US_CODIGO';
          {OP: 6.Enero.2009}
          eAnalisisCriterio:  Result := 'select %s EC_ORDEN, EC_NOMBRE, COUNT(*) CUANTAS, MIN( VC_TOTAL * EC_TOT_PES ) MINIMO, '+
                                        'AVG( VC_TOTAL * EC_TOT_PES ) PROMEDIO, MAX( VC_TOTAL * EC_TOT_PES ) MAXIMO '+
                                        'from V_EVA_COMP '+
                                        'where( ET_CODIGO = %d ) and ( EV_STATUS = 4 ) and ( VC_TOTAL > 0 ) %s '+
                                        'group by EC_ORDEN, EC_NOMBRE';
          {OP: 6.Enero.2009}
          eAnalisisPreguntas: Result := 'select %s EP_NUMERO, EP_DESCRIP, COUNT(*) CUANTAS, MIN( VP_RESPUES * EP_PESO ) MINIMO, AVG( VP_RESPUES * EP_PESO ) PROMEDIO,'+
                                        'MAX( VP_RESPUES * EP_PESO ) MAXIMO '+
                                        'from V_EVA_PREG '+
                                        'where( ET_CODIGO = %d ) and ( VP_RESPUES > 0 ) %s '+
                                        'group by EP_NUMERO, EP_DESCRIP';
          eFrecuenciaResp:   Result :=  'select EL_NOMBRE, COUNT(*) CUANTOS from V_EVA_PREG '+
                                        'where( ET_CODIGO = %d ) and ( EP_NUMERO = %d ) and ( EL_NOMBRE <> '''' ) %s '+
                                        'group by EL_NOMBRE '+
                                        'order by 2 DESC';
          eResultadoCriterio:Result :=  'select %s EC_NOMBRE, SO_NUM_TOT, SO_TOTAL, SO_COMENTA '+
                                        'from SUJ_COMP join ENC_COMP on( SUJ_COMP.ET_CODIGO = ENC_COMP.ET_CODIGO ) '+
                                        'and (SUJ_COMP.EC_ORDEN = ENC_COMP.EC_ORDEN ) '+
                                        'where( SUJ_COMP.ET_CODIGO = %d ) and ( SUJ_COMP.CB_CODIGO =%d )';
          eResultadoPregunta: Result := 'select %s SUJ_PREG.EP_NUMERO, EP_DESCRIP, SP_NUM_TOT, SP_TOTAL, EC_NOMBRE, SP_COMENTA '+
                                        'from SUJ_PREG '+
                                        'join ENC_PREG on( SUJ_PREG.ET_CODIGO = ENC_PREG.ET_CODIGO ) and ( SUJ_PREG.EC_ORDEN = ENC_PREG.EC_ORDEN ) '+
                                        'and ( SUJ_PREG.EP_ORDEN = ENC_PREG.EP_ORDEN ) '+
                                        'join ENC_COMP on( SUJ_PREG.ET_CODIGO = ENC_COMP.ET_CODIGO ) and (SUJ_PREG.EC_ORDEN = ENC_COMP.EC_ORDEN ) '+
                                        'where( SUJ_PREG.ET_CODIGO = %d ) and ( SUJ_PREG.CB_CODIGO = %d )';
          eObtieneSujetos  : Result :=  'select S.ET_CODIGO, S.CB_CODIGO, C.PRETTYNAME from SUJETO S '+
                                        'left outer join COLABORA C on ( C.CB_CODIGO = S.CB_CODIGO ) '+
                                        'order by S.CB_CODIGO';
          eBorraEmpEvaluar : Result :=  'delete from SUJETO where( ET_CODIGO = :ET_CODIGO ) and ( CB_CODIGO = :CB_CODIGO )';
          eModificaStatusSujeto:Result:='update SUJETO set SJ_STATUS = :STATUS where( ET_CODIGO = :ET_CODIGO ) and ( CB_CODIGO = :CB_CODIGO )';
          eAgregaSujetos : Result :=    'insert into SUJETO( ET_CODIGO, CB_CODIGO, SJ_STATUS )values( :ET_CODIGO, :CB_CODIGO, :SJ_STATUS )';
          eSpCuentaSujetos : Result :=  'execute procedure DBO.SP_CONTADORES_SUJETO( :Encuesta, :Empleado )';
          ePrepararEvaluaciones: Result := 'execute procedure DBO.SP_PREPARA_SUJETOS( :Encuesta )';
          eCalcularResultados: Result := 'execute procedure DBO.SP_RESUMEN_SUJETO( :Encuesta, :CB_CODIGO )';
          eRecalcularPromedios: Result := 'execute procedure DBO.SP_TOTALIZA_EVALUACION( :Evaluacion )';
          ePreguntasContestadas: Result := 'select EV_FIN_PRE from EVALUA where ( ET_CODIGO = %d ) and ( US_CODIGO = %d ) and ( CB_CODIGO = %d )';
          eTotalizaEncuesta: Result := 'execute procedure DBO.SP_TOTALIZA_ENCUESTA( :Encuesta )';
          eTotalesEncuesta: Result := 'select %0:s, COUNT(*) CUANTOS from %1:s %2:s group by %0:s';
          eInicializaEncuesta: Result := 'execute procedure DBO.SP_BORRA_RESPUESTAS( :Encuesta )';
          eCuentaEvaluaciones: Result := 'execute procedure DBO.SP_CUENTA_EVALUACIONES( :Encuesta )';
          eEvaluaciones: Result := 'select CAST( EV_FOLIO as VARCHAR( 255 ) ) EV_FOLIO, CB_CODIGO from EVALUA where ( ET_CODIGO = %d ) %s';
          e_Va_Empleado : Result := 'select CB_CODIGO from SUJETO where ( CB_CODIGO = %d ) and ( ET_CODIGO = %d )';
          e_Va_Empleado_Anterior: Result := 'select CB_CODIGO from SUJETO where( CB_CODIGO = ( select MAX( C2.CB_CODIGO ) from SUJETO C2 where ( C2.CB_CODIGO < %d ) and ( ET_CODIGO = %d ) ) )';
          e_Va_Empleado_Siguiente: Result := 'select CB_CODIGO from SUJETO where ( CB_CODIGO = ( select MIN( C2.CB_CODIGO ) from SUJETO C2 where ( C2.CB_CODIGO > %d ) and ( ET_CODIGO = %d ) ) )';
          eEmpleadosBuscados: Result := 'select S.CB_CODIGO, C.CB_APE_PAT, C.CB_APE_MAT, C.CB_NOMBRES, C.CB_RFC, C.CB_SEGSOC, C.CB_ACTIVO, C.CB_BAN_ELE,C.CB_CTA_GAS,C.CB_CTA_VAL '+
                                        K_PRETTYNAME + ' as PrettyName from SUJETO S '+
                                        'left outer join COLABORA C on( C.CB_CODIGO = S.CB_CODIGO ) '+
                                        'where ( UPPER( C.CB_APE_PAT ) like ''%s'' ) and '+
                                        '( UPPER( C.CB_APE_MAT ) like ''%s'' ) and '+
                                        '( UPPER( C.CB_NOMBRES ) like ''%s'' ) and '+
                                        '( UPPER( C.CB_RFC ) like ''%s'' ) and '+
                                        '( UPPER( C.CB_SEGSOC ) like ''%s'' ) and '+
                                        '( UPPER( C.CB_BAN_ELE ) like ''%s'' ) and '+
					'( S.ET_CODIGO = %d ) %s '+
                                        'order by C.CB_APE_PAT, C.CB_APE_MAT, C.CB_NOMBRES';
          eDatosEmpleadoXML: Result := 'select V_EVALUA.*, ''Usuario no identificado      '' as US_NOMBRE from V_EVALUA where ( ET_CODIGO = %d ) and ( CB_CODIGO = %d ) and ( US_CODIGO = %d )';
          eCreaEncuestaSimple: Result := 'insert into ENCUESTA( ET_CODIGO, ET_NOMBRE, ET_DESCRIP, ET_FEC_INI, ET_FEC_FIN, ET_STATUS, ET_NUM_PRE, '+
                                         'ET_USR_ADM, ET_ESCALA, ET_MSG_INI, ET_MSG_FIN, ET_ESC_DEF, ET_DOCUMEN, ET_ESC_CAL, ET_PTS_CAL, ET_CONSOLA ) '+
                                         'values( :ET_CODIGO, :ET_NOMBRE, :ET_DESCRIP, :ET_FEC_INI, :ET_FEC_FIN, :ET_STATUS, :ET_NUM_PRE, '+
                                         ':ET_USR_ADM, :ET_ESCALA, :ET_MSG_INI, :ET_MSG_FIN, :ET_ESC_DEF, :ET_DOCUMEN, :ET_ESC_CAL, :ET_PTS_CAL, :ET_CONSOLA )';
          eAgregaEncEsca: Result := 'insert into ENC_ESCA( ET_CODIGO, EE_CODIGO, EE_NOMBRE, EE_NIVELES )values( :ET_CODIGO, :EE_CODIGO, :EE_NOMBRE, :EE_NIVELES )';
          eAgregaNivelesEscala: Result := 'insert into ENC_NIVE( ET_CODIGO, EE_CODIGO, EL_ORDEN, EL_NOMBRE, EL_CORTO, EL_DESCRIP, EL_VALOR ) '+
                                          'values( :ET_CODIGO, :EE_CODIGO, :EL_ORDEN, :EL_NOMBRE, :EL_CORTO, :EL_DESCRIP, :EL_VALOR )';
          eAgregaPreguntas: Result := 'insert into ENC_PREG( ET_CODIGO, EC_ORDEN, EP_ORDEN, EE_CODIGO, EP_DESCRIP, EP_NUMERO, EP_POS_PES ) '+
                                      'values( :ET_CODIGO, :EC_ORDEN, :EP_ORDEN, :EE_CODIGO, :EP_DESCRIP, :EP_NUMERO, :EP_POS_PES )';
          eAgregaCriterio: Result := 'insert into ENC_COMP( ET_CODIGO, EC_ORDEN, CM_CODIGO, EC_NOMBRE, EC_POS_PES ) '+
                                     'values( :ET_CODIGO, :EC_ORDEN, :CM_CODIGO, :EC_NOMBRE, :EC_POS_PES )';
          eVerificaSujeto: Result := 'select COUNT(*) as CUANTOS from SUJETO where ( ET_CODIGO = :Encuesta ) and ( CB_CODIGO = :Empleado )';
          eNivelesEncuesta : Result := 'SELECT ET_CODIGO,EE_CODIGO,EL_ORDEN,EL_NOMBRE,EL_CORTO,EL_DESCRIP,EL_VALOR FROM ENC_NIVE WHERE %s ORDER BY ET_CODIGO,EE_CODIGO,EL_ORDEN ';
     else
         Result := VACIO;
     end
end;

procedure TdmServerEvaluacion.SetTablaInfo( eTabla: eTipoTabla );
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTabla of
               eCompetencia     : SetInfo( 'COMPETEN', 'CM_CODIGO,CM_DESCRIP,CM_INGLES,CM_NUMERO,CM_TEXTO,CM_DETALLE,TC_CODIGO', 'CM_CODIGO' );
               eTCompete        : SetInfo( 'TCOMPETE', 'TC_CODIGO,TC_DESCRIP,TC_INGLES,TC_NUMERO,TC_TEXTO,TC_TIPO', 'TC_CODIGO' );
               ePreguntas       : SetInfo( 'PREGUNTA', 'PG_FOLIO,CM_CODIGO,PG_ORDEN,PG_DESCRIP,PG_TIPO,PG_CONSEJO', 'PG_FOLIO' );
               eEscalas         : SetInfo( 'ESCALA',   'SC_CODIGO,SC_NOMBRE,SC_NIVELES', 'SC_CODIGO' );
               eEscalasNiveles  : SetInfo( 'ESCNIVEL', 'SC_CODIGO,SN_ORDEN,SN_NOMBRE,SN_CORTO,SN_DESCRIP,SN_VALOR', 'SC_CODIGO,SN_ORDEN' );
               eCriteriosEval   : SetInfo( 'ENC_COMP', 'ET_CODIGO, EC_ORDEN, CM_CODIGO, EC_NOMBRE, EC_DETALLE, EC_PESO, EC_TOT_PES, EC_NUM_PRE, EC_GET_COM, EC_POS_PES, EC_DOCUMEN',
                                                       'ET_CODIGO, EC_ORDEN' );
               eEscalasEncuesta : SetInfo( 'ENC_ESCA', 'ET_CODIGO, EE_CODIGO, EE_NOMBRE, EE_NIVELES', 'ET_CODIGO, EE_CODIGO' );
               ePerilEvaluadores: SetInfo( 'ENC_RELA', 'ET_CODIGO, ER_TIPO, ER_NOMBRE, ER_NUM_MIN, ER_NUM_MAX, ER_PESO, ER_POS_PES', 'ET_CODIGO, ER_TIPO' );
               eEncuesta        : SetInfo( 'ENCUESTA', 'ET_CODIGO, ET_NOMBRE, ET_FEC_INI, ET_FEC_FIN, ET_STATUS, ET_NUM_COM, ET_NUM_PRE, ET_NUM_SUJ, '+
                                                       'ET_NUM_EVA, ET_USR_ADM, ET_PESOS, ET_DESCRIP, ET_ESC_DEF, ET_ESCALA, ET_ESTILO, ET_ESC_PES, '+
                                                       'ET_GET_COM, ET_TXT_COM, ET_MSG_INI, ET_MSG_FIN, ET_NIVELES, ET_FIN_SUJ, ET_FIN_EVA, '+
                                                       'ET_DOCUMEN, ET_ESC_CAL, ET_PTS_CAL, ET_CONSOLA',
                                                       'ET_CODIGO' );
               eNivelesEnc      : SetInfo( 'ENC_NIVE', 'ET_CODIGO, EE_CODIGO, EL_ORDEN, EL_NOMBRE, EL_CORTO, EL_DESCRIP, EL_VALOR', 'ET_CODIGO, EE_CODIGO, EL_ORDEN' );
               ePreguntaCriterios:SetInfo( 'ENC_PREG', 'ET_CODIGO, EC_ORDEN, EP_ORDEN, EE_CODIGO, EP_NUMERO, PG_FOLIO, EP_DESCRIP, EP_TIPO, EP_PESO, EP_GET_COM, EP_GET_NA, EP_POS_PES', 'ET_CODIGO, EC_ORDEN, EP_ORDEN' );
               eEvalua          : SetInfo( 'EVALUA',   'EV_FOLIO, US_CODIGO, CB_CODIGO, EV_RELACIO, EV_STATUS, EV_TOTAL, EV_COMENTA, EV_FEC_INI, EV_FEC_FIN, EV_FIN_PRE, ET_CODIGO', 'EV_FOLIO' );
               eSujeto          : SetInfo( 'SUJETO', 'ET_CODIGO, CB_CODIGO, SJ_STATUS, SJ_NUM_EVA, SJ_FIN_EVA, SJ_TOTAL', 'ET_CODIGO, CB_CODIGO' );
          end;
     end;
end;

function TdmServerEvaluacion.GetTabla( const eTabla: eTipoTabla; const Empresa: OleVariant; const sFiltro: string = VACIO ): OleVariant;
begin
     SetTablaInfo( eTabla );
     with oZetaProvider do
     begin
          if StrLleno( sFiltro ) then
             TablaInfo.Filtro := sFiltro;
          Result := GetTabla( Empresa );
     end;
end;

function TdmServerEvaluacion.GrabaTabla(const eTabla: eTipoTabla; const Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eTabla );
     with oZetaProvider do
     begin
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
end;

function TdmServerEvaluacion.GetSQLBroker: TSQLBroker;
begin
{$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
{$else}
     Result := Self.oZetaSQLBroker;
{$endif}
end;

{$ifdef DOS_CAPAS}
procedure TdmServerEvaluacion.CierraEmpresa;
begin
end;
{$endif}

procedure TdmServerEvaluacion.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerEvaluacion.InitBroker;
begin
     InitCreator;
     oZetaCreator.RegistraFunciones( [ efComunes ] );
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TdmServerEvaluacion.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oZetaSQLBroker );
end;

function TdmServerEvaluacion.EncuestaSiguiente(Empresa: OleVariant; iEncuesta: Integer; out oDelta: OleVariant): WordBool;
var
   sSQL: String;
begin
     sSQL := Format( GetScript( eEncuestaSiguiente ), [ iEncuesta ] );
     oDelta := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     Result := ( oZetaProvider.RecsOut > 0 );
     SetComplete;
end;

function TdmServerEvaluacion.EncuestaAnterior(Empresa: OleVariant; iEncuesta: Integer; out oDelta: OleVariant): WordBool;
var
   sSQL: String;
begin
     sSQL := Format( GetScript( eEncuestaAnterior ), [ iEncuesta ] );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          oDelta := OpenSQL( Empresa, sSQL, True );
          Result := ( RecsOut > 0 );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetCatCompetencias(Empresa: OleVariant): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GetTabla( eTCompete, Empresa );
     SetComplete;
end;

function TdmServerEvaluacion.GetCatHabilidades(Empresa: OleVariant): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GetTabla( eCompetencia, Empresa );
     SetComplete;
end;

function TdmServerEvaluacion.GetCatPreguntas(Empresa: OleVariant; const sCompetencia: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( ePreguntas );
          TablaInfo.SetOrder( 'PG_ORDEN' );
          TablaInfo.Filtro := ' CM_CODIGO = ' + ZetaCommonTools.EntreComillas( sCompetencia );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetCatNivelesEscala(Empresa: OleVariant; const sEscala: WideString; out oEscNiv: OleVariant): OleVariant;
begin
     SetTablaInfo( eEscalas );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.Filtro := Format( 'SC_CODIGO = %s', [ EntreComillas( sEscala ) ] );
          Result := GetTabla( Empresa );
          oEscNiv := OpenSQL( Empresa, Format( GetScript( eNivelesEscala ), [ EntreComillas( sEscala ) ] ), TRUE );
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.ContadoresSujeto( Empresa: OleVariant; const iEncuesta, iEmpleado: Integer );
begin
     //************ Cuenta y Totaliza Sujetos y Evaluaciones **************//
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEvaluador := CreateQuery( GetScript( eSpCuentaSujetos ) );
          try
             //MA: Ejecuta SP para contar los sujetos.
             ParamAsInteger( FEvaluador, 'Encuesta', iEncuesta );
             ParamAsInteger( FEvaluador, 'Empleado', iEmpleado );
             EmpiezaTransaccion;
             try
                Ejecuta( FEvaluador );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FEvaluador );
          end;
     end;
end;

procedure TdmServerEvaluacion.CambiaPregunta(Empresa: OleVariant; const CM_CODIGO: WideString; iActual, iNuevo: Smallint);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FPregunta := CreateQuery( GetScript( eCambiaPregunta ) );
          try
             ParamAsChar( FPregunta, 'CM_CODIGO', CM_CODIGO, K_ANCHO_CODIGO );
             ParamAsInteger( FPregunta, 'Actual', iActual );
             ParamAsInteger( FPregunta, 'Nuevo', iNuevo );
             EmpiezaTransaccion;
             try
                Ejecuta( FPregunta );
                TerminaTransaccion( True );
             except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                     raise;
                end;
             end;
          finally
                 FreeAndNil( FPregunta );
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GrabaCompetencia(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GrabaTabla( eCompetencia, Empresa, oDelta, ErrorCount );
     SetComplete;
end;

procedure TdmServerEvaluacion.BorraPregunta(Empresa: OleVariant; const CM_CODIGO: WideString; iActual: Smallint);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FPregunta := CreateQuery( GetScript( eBorraPregunta ) );
          try
             ParamAsChar( FPregunta, 'CM_CODIGO', CM_CODIGO, K_ANCHO_CODIGO );
             ParamAsInteger( FPregunta, 'Actual', iActual );
             EmpiezaTransaccion;
             try
                Ejecuta( FPregunta );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FPregunta );
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GrabaTCompetencia(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GrabaTabla( eTCompete, Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerEvaluacion.GrabaCatEscalas(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GrabaTabla( eEscalas, Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerEvaluacion.SiguientePregunta(Empresa: OleVariant): Smallint;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FPregunta := oZetaProvider.CreateQuery( GetScript( eSiguientePregunta ) );
          try
             FPregunta.Open;
             Result := FPregunta.FieldByName('MAXIMO').AsInteger + 1;
             FPregunta.Close;
          finally
                 FreeAndNil(FPregunta);
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GrabaPregunta(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GrabaTabla( ePreguntas, Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerEvaluacion.GetEscalasNiveles(Empresa: OleVariant; const sEscala: WideString): OleVariant;
var
   sFiltro: String;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     sFiltro := ' SC_CODIGO = ' + ZetaCommonTools.EntreComillas( sEscala );
     Result := GetTabla( eEscalasNiveles, Empresa, sFiltro );
     SetComplete;
end;

function TdmServerEvaluacion.GrabaEscalaNiveles( Empresa, oDelta,oEscNiv: OleVariant; out ErrorCount: Integer;
                                                 out oResultEscNiv: OleVariant): OleVariant;
var
   iErrorCountEscNiv: Integer;
begin
     SetOLEVariantToNull( Result );
     SetOLEVariantToNull( oResultEscNiv );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( oDelta ) then
          begin
               SetTablaInfo( eEscalas );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateEscalas;
               Result := GrabaTabla( Empresa, oDelta, ErrorCOunt );
          end;
          if ( ErrorCount = 0 ) and not VarIsNull( oEscNiv ) then
          begin
               SetTablaInfo( eEscalasNiveles );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateEscNiveles;
               oResultEscNiv := GrabaTabla( Empresa, oEscNiv, iErrorCountEscNiv );
          end;
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.BeforeUpdateEscalas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          with DeltaDS do
          begin
               FCodigo := CampoAsVar( FieldByName( 'SC_CODIGO' ) );
          end;
     end;
end;

procedure TdmServerEvaluacion.BeforeUpdateEscNiveles(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          with DeltaDS do
          begin
               if StrLleno( FCodigo ) then
               begin
                    Edit;
                    FieldByName( 'SC_CODIGO' ).AsString := FCodigo;
                    Post;
               end;
          end;
     end;
end;

function TdmServerEvaluacion.GetCatEscalas(Empresa: OleVariant): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GetTabla( eEscalas, Empresa );
     SetComplete;
end;

function TdmServerEvaluacion.BorraEscala(Empresa: OleVariant; const sEscala: WideString; out ErrorCount: Integer): OleVariant;
begin
     SetOLEVariantToNull( Result );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;
          try
             ExecSQL( Empresa, Format( GetScript( eBorraEscala ), [ EntreComillas( sEscala ) ] ) );
             TerminaTransaccion( True );
          except
                TerminaTransaccion( False );
          end;
     end;
     SetComplete;
end;


function TdmServerEvaluacion.GetCriteriosEvaluar(Empresa: OleVariant; iEncuesta: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, Format( GetScript( eCriteriosEvaluar ), [ iEncuesta ] ), TRUE );
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.CambiaCriterios(Empresa: OleVariant; iEncuesta: Integer; iActual, iNueva: Smallint);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEncuesta := CreateQuery( GetScript( eCambiaCriterio ) );
          try
             ParamAsInteger( FEncuesta, 'Encuesta', iEncuesta );
             ParamAsInteger( FEncuesta, 'Actual', iActual );
             ParamAsInteger( FEncuesta, 'Nuevo', iNueva );
             EmpiezaTransaccion;
             try
                Ejecuta( FEncuesta );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
         finally
                FreeAndNil( FEncuesta );
         end;
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.BorraCriterio(Empresa: OleVariant; iEncuesta: Integer; iPosicion: Smallint);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEncuesta := CreateQuery( GetScript( eBorraCriterio ) );
          try
             ParamAsInteger( FEncuesta, 'Encuesta', iEncuesta );
             ParamAsInteger( FEncuesta, 'Posicion', iPosicion );
             EmpiezaTransaccion;
             try
                Ejecuta( FEncuesta );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FEncuesta );
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GrabaCriteriosEval(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( eCriteriosEval );
          TablaInfo.BeforeUpdateRecord := BeforeUpdateObtieneFEncuesta;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     ContadoresEncuesta( Empresa, FNumEncuesta );
     SetComplete;
end;

function TdmServerEvaluacion.GetPreguntasCriterio(Empresa: OleVariant; iEncuesta: Integer; iCriterio: Smallint): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, Format( GetScript( ePreguntasCriterio ), [ iEncuesta, iCriterio ] ), TRUE );
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.CambiaPreguntaCriterio(Empresa: OleVariant; iEncuesta: Integer; iCriterio, iActual, iNueva: Smallint);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEncuesta := CreateQuery( GetScript( eCambiaPreguntaCriterio ) );
          try
             ParamAsInteger( FEncuesta, 'Encuesta', iEncuesta );
             ParamAsInteger( FEncuesta, 'Criterio', iCriterio );
             ParamAsInteger( FEncuesta, 'Actual', iActual );
             ParamAsInteger( FEncuesta, 'Nuevo', iNueva );
             EmpiezaTransaccion;
             try
                Ejecuta( FEncuesta );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
         finally
                FreeAndNil( FEncuesta );
         end;
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.BorraPreguntaCriterio(Empresa: OleVariant; iEncuesta: Integer; iCriterio, iPregunta: Smallint);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEncuesta := CreateQuery( GetScript( eBorraPreguntaCriterio ) );
          try
             ParamAsInteger( FEncuesta, 'Encuesta', iEncuesta );
             ParamAsInteger( FEncuesta, 'Criterio', iCriterio );
             ParamAsInteger( FEncuesta, 'Pregunta', iPregunta );
             EmpiezaTransaccion;
             try
                Ejecuta( FEncuesta );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FEncuesta );
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetEscalasEncuesta(Empresa: OleVariant; iEncuesta: Integer): OleVariant;
var
   sFiltro: String;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     sFiltro := ' ET_CODIGO = ' + inttostr( iEncuesta );
     Result := GetTabla( eEscalasEncuesta, Empresa, sFiltro );
     SetComplete;
end;

function TdmServerEvaluacion.GetPerfilEvaluador(Empresa: OleVariant; iEncuesta: Integer): OleVariant;
begin
    with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, Format( GetScript( ePerfilEvaluador ), [ iEncuesta ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetEmpEvaluar(Empresa: OleVariant; iEncuesta, iEmpleado: Integer; iStatus: Smallint): OleVariant;
var
   sFiltro, sFiltroEmp, sFiltroStatus: String;
begin
     sFiltroEmp := VACIO;
     sFiltroStatus := VACIO;
     if( iEmpleado > 0 )then
     begin
          sFiltroEmp := ' and SUJETO.CB_CODIGO = %d';
          sFiltroEmp := Format( sFiltroEmp,[ iEmpleado ] );
     end;
     if( iStatus >= 0 )then
     begin
          sFiltroStatus := ' and SJ_STATUS = %d';
          sFiltroStatus := Format( sFiltroStatus, [ iStatus ] );
     end;
     sFiltro := sFiltroEmp + sFiltroStatus;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, Format( GetScript( eEmpEvaluar ), [ iEncuesta, sFiltro ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetEncuestas(Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( eEncuesta );
          TablaInfo.SetOrder( 'ET_CODIGO' );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GrabaEncuesta( Empresa, oDelta, oDataCriterio, oDataEval: OleVariant; lGrabaCriterios: WordBool; out ErrorCount: Integer): OleVariant;
  {$ifdef FALSE}
  procedure CreaEscalaPeso;
  begin
       if StrLleno( FEscalaImportancia ) and ( FEscalaImportancia <> FEscalaEvaluacion ) then
       begin
            with oZetaProvider do
            begin
                 FEncuesta := CreateQuery( GetScript( eCreaEscala ) );
                 try
                    ParamAsInteger( FEncuesta, 'Encuesta', FNumEncuesta );
                    ParamAsChar( FEncuesta, 'Escala', FEscalaImportancia, K_ANCHO_CODIGO );
                    EmpiezaTransaccion;
                    try
                       Ejecuta( FEncuesta );
                       TerminaTransaccion( True );
                    except
                          on Error: Exception do
                          begin
                               RollBackTransaccion;
                               raise;
                          end;
                    end;
                finally
                       FreeAndNil( FEncuesta );
                end;
            end;
       end;
  end;
  {$endif}

begin
     SetTablaInfo( eEncuesta );

           with oZetaProvider do
           begin
                EmpresaActiva := Empresa;
                if lGrabaCriterios then
                begin
                     TablaInfo.BeforeUpdateRecord := BeforeUpdateEncuesta;
                end;
                Result := GrabaTabla( Empresa, oDelta, ErrorCount );
                if lGrabaCriterios then
                begin
                     {$ifdef FALSE}
                     FEncuesta := CreateQuery( GetScript( eCreaEscala ) );
                     try
                        ParamAsInteger( FEncuesta, 'Encuesta', FNumEncuesta );
                        ParamAsChar( FEncuesta, 'Escala', FEscalaEvaluacion, K_ANCHO_CODIGO );
                        EmpiezaTransaccion;
                        try
                           Ejecuta( FEncuesta );
                           TerminaTransaccion( True );
                        except
                              on Error: Exception do
                              begin
                                   RollBackTransaccion;
                                   raise;
                              end;
                        end;
                     finally
                           FreeAndNil( FEncuesta );
                     end;
                     {$endif}
                     CreaEscalasEncuesta( FNumEncuesta, FEscalaEvaluacion );
                     if StrLleno( FEscalaImportancia ) and ( FEscalaImportancia <> FEscalaEvaluacion ) then
                     begin
                          CreaEscalasEncuesta( FNumEncuesta, FEscalaImportancia );
                     end;
                     if StrLleno( FEscalaCalificacion ) and ( FEscalaCalificacion <> FEscalaEvaluacion ) then
                     begin
                          CreaEscalasEncuesta( FNumEncuesta, FEscalaCalificacion );
                     end;
                     EvaluaCriterios( FNumEncuesta, 0, oDataCriterio );
                     EvaluaTipoEvaluador( FNumEncuesta, 0, oDataEval );
                     ContadoresEncuesta( Empresa, FNumEncuesta );
                     NumeraPreguntas( Empresa, FNumEncuesta );
                end;
           end;
     SetComplete;
end;

procedure TdmServerEvaluacion.EvaluaCriterios( const iEncuesta, iOrden: Integer; const oData: OleVariant );
var
   i: Integer;
begin
     i := 1;
     cdsAgregarCriterios.Data := oData;
     with oZetaProvider do
     begin
          with cdsAgregarCriterios do
          begin
               FEncuesta := CreateQuery( GetScript( eAgregaCriterios ) );
               try
                  while not EOF do
                  begin
                       ParamAsInteger( FEncuesta, 'Encuesta', iEncuesta );
                       ParamAsInteger( FEncuesta, 'Orden', ( iOrden + i ) );
                       ParamAsChar( FEncuesta, 'Codigo', FieldByName( 'CM_CODIGO' ).AsString, K_ANCHO_CODIGO );
                       EmpiezaTransaccion;
                       try
                          Ejecuta( FEncuesta );
                          TerminaTransaccion( True );
                       except
                             on Error: Exception do
                             begin
                                  RollBackTransaccion;
                                  raise;
                             end;
                       end;
                       i := i + 1;
                       Next;
                  end;
               finally
                      FreeAndNil( FEncuesta );
               end;
          end;
     end;
end;

procedure TdmServerEvaluacion.EvaluaTipoEvaluador( const iEncuesta, iOrden: Integer; const oData: OleVariant );
var
   i: Integer;
begin
     i := 1;
     cdsAgregarEvaluador.Data := oData;
     with oZetaProvider do
     begin
          with cdsAgregarEvaluador do
          begin
               FEncuesta := CreateQuery( GetScript( eAgregaEvaluador ) );
               try
                  while not EOF do
                  begin
                       ParamAsInteger( FEncuesta, 'Encuesta', iEncuesta );
                       ParamAsInteger( FEncuesta, 'Orden', ( iOrden + i ) );
                       ParamAsInteger( FEncuesta, 'Tipo', FieldByName( 'ER_TIPO' ).AsInteger );
                       ParamAsChar( FEncuesta, 'DescTipo', FieldByName( 'ER_DESCRIP' ).AsString, K_ANCHO_DESCRIPCION );
                       EmpiezaTransaccion;
                       try
                          Ejecuta( FEncuesta );
                          TerminaTransaccion( True );
                       except
                             on Error: Exception do
                             begin
                                  RollBackTransaccion;
                                  raise;
                             end;
                       end;
                       i := i + 1;
                       Next;
                  end;
               finally
                      FreeAndNil( FEncuesta );
               end;
          end;
     end;
end;

procedure TdmServerEvaluacion.BeforeUpdateEncuesta( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     //Si es Alta, tengo que obtener el m�ximo Folio
     FEscalaEvaluacion := CampoAsVar( DeltaDS.FieldByName( 'ET_ESC_DEF' ) );
     FEscalaImportancia := CampoAsVar( DeltaDS.FieldByName( 'ET_ESC_PES' ) );
     FEscalaCalificacion := CampoAsVar( DeltaDS.FieldByName( 'ET_ESC_CAL' ) );
     if ( UpdateKind = ukInsert ) then
     begin
          FNumEncuesta := GetMaxEncuesta;
          DeltaDS.Edit;
          DeltaDS.FieldByName( 'ET_CODIGO' ).AsInteger := FNumEncuesta;
          DeltaDS.Post;
     end;
end;

function TdmServerEvaluacion.GetMaxEncuesta: Integer;
 var
    FDataSet : TZetaCursor;
begin
     FDataset := oZetaProvider.CreateQuery( GetScript( eMaxEncuesta ) );
     try
        FDataSet.Open;
        Result := FDataSet.FieldByName('MAXIMO').AsInteger + 1;
        FDataSet.Close;
     finally
            FreeAndNil(FDataSet);
     end;
end;

procedure TdmServerEvaluacion.CopiaEncuesta( Empresa: OleVariant; iEncuesta: Integer; const sNombre: WideString; lSujetos,
                                             lEvaluadores: WordBool);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEncuesta := CreateQuery( GetScript( eCopiaEncuesta ) );
          try
             ParamAsInteger( FEncuesta, 'Enc_Anterior', iEncuesta );
             ParamAsInteger( FEncuesta, 'Enc_Nueva', GetMaxEncuesta );
             ParamAsString( FEncuesta, 'Nombre', sNombre );
             ParamAsBoolean( FEncuesta, 'Sujetos', lSujetos );
             ParamAsBoolean( FEncuesta, 'Evaluadores', lEvaluadores );
             EmpiezaTransaccion;
             try
                Ejecuta( FEncuesta );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                        //Log.Excepcion( iEncuesta, 'Error al copiar encuesta', Error );
                   end;
             end;
         finally
                FreeAndNil( FEncuesta );
         end;
     end;
end;


procedure TdmServerEvaluacion.AgregaCriterios(Empresa: OleVariant; iEncuesta: Integer; iCriterios: Smallint; oData: OleVariant);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EvaluaCriterios( iEncuesta, iCriterios, oData );
     end;
     ContadoresEncuesta( Empresa, iEncuesta );
     NumeraPreguntas( Empresa, iEncuesta );
     SetComplete;
end;

procedure TdmServerEvaluacion.ContadoresEncuesta( Empresa: OleVariant; const iEncuesta: Integer);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FCriterios := CreateQuery( GetScript( eContadoresEncuesta ) );
          try
             ParamAsInteger( FCriterios, 'Encuesta', iEncuesta );
             EmpiezaTransaccion;
             try
                Ejecuta( FCriterios );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FCriterios );
          end;
     end;
end;

procedure TdmServerEvaluacion.NumeraPreguntas( Empresa: OleVariant; const iEncuesta: Integer);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FCriterios := CreateQuery( GetScript( eNumeraPreguntas ) );
          try
             ParamAsInteger( FCriterios, 'Encuesta', iEncuesta );
             EmpiezaTransaccion;
             try
                Ejecuta( FCriterios );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FCriterios );
          end;
     end;
end;

function TdmServerEvaluacion.GetEncuesta(Empresa: OleVariant; iEncuesta: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( GetScript( eGetEncuesta ), [ VACIO, iEncuesta ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetCatNivelesEnc(Empresa: OleVariant; const sFiltro: WideString): OleVariant;

begin
     with oZetaProvider do
     begin
        EmpresaActiva := Empresa;
        Result := OpenSQL( Empresa, Format( GetScript( eNivelesEncuesta ), [ sFiltro ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GrabaPreguntasCriterio(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( ePreguntaCriterios );
          TablaInfo.BeforeUpdateRecord := BeforeUpdateObtieneFEncuesta;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     ContadoresEncuesta( Empresa, FNumEncuesta );
     NumeraPreguntas( Empresa, FNumEncuesta );
     SetComplete;
end;

procedure TdmServerEvaluacion.BeforeUpdateObtieneFEncuesta( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     FNumEncuesta := DeltaDS.FieldByName( 'ET_CODIGO' ).AsInteger;
end;

function TdmServerEvaluacion.GetEscalaEncuestaNiv(Empresa: OleVariant; iEncuesta: Integer; const sEscala: WideString): OleVariant;
begin
     SetTablaInfo( eNivelesEnc );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.Filtro := Format( 'ET_CODIGO = %d and EE_CODIGO = %s', [ iEncuesta, EntreComillas( sEscala ) ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GrabaEscalaNivelesEnc( Empresa, oDelta, oEscNivEnc: OleVariant; out ErrorCount: Integer;
                                                    out oResultEscNivEnc: OleVariant): OleVariant;
var
   iErrorCountEscNivEnc: Integer;
begin
     SetOLEVariantToNull( Result );
     SetOLEVariantToNull( oResultEscNivEnc );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( oDelta ) then
          begin
               SetTablaInfo( eEscalasEncuesta );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateEscalasEnc;
               Result := GrabaTabla( Empresa, oDelta, ErrorCOunt );
          end;
          if ( ErrorCount = 0 ) and not VarIsNull( oEscNivEnc ) then
          begin
               SetTablaInfo( eNivelesEnc );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateEscNivelesEnc;
               oResultEscNivEnc := GrabaTabla( Empresa, oEscNivEnc, iErrorCountEscNivEnc );
          end;
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.BeforeUpdateEscalasEnc(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          with DeltaDS do
          begin
               FNumEncuesta := CampoAsVar( FieldByName( 'ET_CODIGO' ) );
               FEscalaEvaluacion := CampoAsVar( FieldByName( 'EE_CODIGO' ) );
          end;
     end;
end;

procedure TdmServerEvaluacion.BeforeUpdateEscNivelesEnc(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          with DeltaDS do
          begin
               if ( FNumEncuesta > 0 ) then
               begin
                    Edit;
                    FieldByName( 'ET_CODIGO' ).AsInteger := FNumEncuesta;
                    FieldByName( 'EE_CODIGO' ).AsString := FEscalaEvaluacion;
                    Post;
               end;
          end;
     end;
end;


procedure TdmServerEvaluacion.ContadoresDEncuesta(Empresa: OleVariant; iEncuesta: Integer);
begin
     ContadoresEncuesta( Empresa, iEncuesta );
     SetComplete;
end;


function TdmServerEvaluacion.GrabaPerfilEvaluador(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := GrabaTabla( ePerilEvaluadores, Empresa, oDelta, ErrorCount );
     SetComplete;
end;


function TdmServerEvaluacion.GetMaxTipoEvaluador(Empresa: OleVariant; iEncuesta: Integer): Smallint;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEvaluador := oZetaProvider.CreateQuery( Format( GetScript( eMaxTipoEvaluador ), [ iEncuesta ]  ) );
          try
             FEvaluador.Open;
             Result := FEvaluador.FieldByName('MAXIMO').AsInteger;
             FEvaluador.Close;
          finally
                 FreeAndNil(FEvaluador);
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetEvalua(Empresa: OleVariant; iEncuesta, iEmpleado: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, Format( GetScript( eEvaluados ), [ iEncuesta, iEmpleado ] ), TRUE );
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.AgregaEvalua( Empresa: OleVariant; iEncuesta, iSujeto, iRelacion, iEmpleado: Integer; iStatus: Smallint;
                                            lInserta: WordBool);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if( lInserta )then
          begin
               FEvaluacion := CreateQuery( GetScript( eAgregaEvaluaciones ) );
               ParamAsInteger( FEvaluacion, 'EV_STATUS', iStatus );
          end
          else
              FEvaluacion := CreateQuery( GetScript( eModificaEvaluaciones ) );
          try
             ParamAsInteger( FEvaluacion, 'US_CODIGO', iSujeto );
             ParamAsInteger( FEvaluacion, 'CB_CODIGO', iEmpleado );
             ParamAsInteger( FEvaluacion, 'EV_RELACIO', iRelacion );
             ParamAsInteger( FEvaluacion, 'ET_CODIGO', iEncuesta );
             EmpiezaTransaccion;
             try
                Ejecuta( FEvaluacion );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FEvaluacion );
          end;
          if( lInserta )then
          begin
               ContadoresSujeto( Empresa, iEncuesta, iEmpleado );
          end;
          //Cualquier modificacion a la matriz de evaluaci�n hace que se ponga en NUEVO el SJ_STATUS
          CambiaStatusSujeto( Empresa, iEncuesta, iEmpleado, FALSE );
     end;
     SetComplete;
end;

{procedure TdmServerEvaluacion.TotalizaEncuesta( Empresa: OleVariant; const iEncuesta: Integer );
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEncuesta := CreateQuery( GetScript( eTotalizaEncuesta ) );
          EmpiezaTransaccion;
          try
             try
                ParamAsInteger( FEncuesta, 'Encuesta', iEncuesta );
                Ejecuta( FEncuesta );
             finally
                    FreeAndNil( FEncuesta );
             end;
             TerminaTransaccion( True );
           except
                  on Error: Exception do
                  begin
                       RollBackTransaccion;
                  end;
            end;
     end;
end;}

procedure TdmServerEvaluacion.BorraEvalua(Empresa: OleVariant; iEncuesta, iEmpleado, iSujeto: Integer);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEvaluacion := CreateQuery( GetScript( eBorraEvaluaciones ) );
          FEncuesta := CreateQuery( GetScript( eCuentaEvaluaciones ) );
          try
             ParamAsInteger( FEvaluacion, 'ET_CODIGO', iEncuesta );
             ParamAsInteger( FEvaluacion, 'CB_CODIGO', iEmpleado );
             ParamAsInteger( FEvaluacion, 'US_CODIGO', iSujeto );
             EmpiezaTransaccion;
             try
                Ejecuta( FEvaluacion );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
             ContadoresSujeto( Empresa, iEncuesta, iEmpleado );
             //MA:Parametros para ejecutar el SP_Cuenta_Evaluaciones
             ParamAsInteger( FEncuesta, 'Encuesta', iEncuesta );
             EmpiezaTransaccion;
             try
                Ejecuta( FEncuesta );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
             //Cualquier modificacion a la matriz de evaluaci�n hace que se ponga en NUEVO el SJ_STATUS
             CambiaStatusSujeto( Empresa, iEncuesta, iEmpleado, FALSE );
          finally
                 FreeAndNil( FEncuesta );
                 FreeAndNil( FEvaluacion );
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetDatosEncuesta( Empresa: OleVariant; iEncuesta: Integer; out ParamEvaluacion, ParamEvaluador,
                                               ParamEvaluado: OleVariant): OleVariant;
const
     K_FILTRO_ENCUESTA = 'where ( ET_CODIGO = %d )';
     K_FILTRO_EVALUADORES = 'DBO.SP_STATUS_EVALUADORES( %d )';
var
   sFiltro: String;
begin
     SetOleVariantToNull( ParamEvaluacion );
     SetOleVariantToNull( ParamEvaluador );
     SetOleVariantToNull( ParamEvaluado );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          //***** Obtiene status de evaluaciones ***********//
          sFiltro := Format( K_FILTRO_ENCUESTA, [ iEncuesta ] );
          ParamEvaluacion := OpenSQL( EmpresaActiva, Format( GetScript( eTotalesEncuesta ), [ 'EV_STATUS', 'EVALUA', sFiltro ] ), TRUE );
          //***** Obtiene status evaluadores **************//
          sFiltro := Format( K_FILTRO_EVALUADORES, [ iEncuesta ] );
          ParamEvaluador := OpenSQL( EmpresaActiva, Format( GetScript( eTotalesEncuesta ), [ 'SE_STATUS', sFiltro, VACIO ] ), TRUE );
          //***** Obtiene status evaluados *************//
          sFiltro := Format( K_FILTRO_ENCUESTA, [ iEncuesta ] );
          ParamEvaluado := OpenSQL( EmpresaActiva, Format( GetScript( eTotalesEncuesta ), [ 'SJ_STATUS', 'SUJETO', sFiltro ] ), TRUE );
     end;
     Result := GetTabla( eEncuesta, Empresa, Format( 'ET_CODIGO = %d',[ iEncuesta ] ) );
     SetComplete;
end;

function TdmServerEvaluacion.GetEvaluadores(Empresa: OleVariant; iEncuesta: Integer; const sFiltro: WideString): OleVariant;
var
   sWhere: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if StrLleno( sFiltro ) then
              sWhere := 'where( ' + sFiltro + ')';
          Result := OpenSQL( Empresa, Format( GetScript( eObtieneEvaluadores ), [ iEncuesta, sWhere ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetEvaluaciones(Empresa: OleVariant; iEncuesta: Integer; const sFiltro: WideString): OleVariant;
var
   sFiltros: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if StrLleno( sFiltro ) then
             sFiltros := ' and ' + sFiltro;
          Result := OpenSQL( Empresa, Format( GetScript( eObtieneEvaluaciones ), [ iEncuesta, sFiltros ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetAnalisisCriterio(Empresa: OleVariant; iEncuesta: Integer; iMostrar, iRelacion, iOrden: Smallint): OleVariant;
const
     Q_TOP = 'TOP %d';
     Q_EC_ORDEN = ' order by EC_ORDEN';
     Q_PROMEDIO_DESC = ' order by PROMEDIO DESC';
     Q_PROMEDIO_ASC = ' order by PROMEDIO';
var
   sFiltro, sMostrar, sOrdenar, sQuery: String;
begin
     sFiltro := VACIO;
     sMostrar := VACIO;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if( iRelacion >= 0 )then
              sFiltro := Format( ' and ( EV_RELACIO = %d ) ', [ iRelacion ] );
          if( iMostrar > 0 )then
              sMostrar := Format( Q_TOP, [ iMostrar ] );
          case( iOrden )of
                0: sOrdenar := Q_EC_ORDEN;
                1: sOrdenar := Q_PROMEDIO_DESC;
                2: sOrdenar := Q_PROMEDIO_ASC;
          else
              sOrdenar := VACIO;
          end;
          sQuery := Format( GetScript( eAnalisisCriterio ), [ sMostrar, iEncuesta, sFiltro ] ) + sOrdenar;
          Result := OpenSQL( Empresa, sQuery, TRUE );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetAnalisisPreguntas( Empresa: OleVariant; iEncuesta: Integer; iMostrar, iRelacion, iCriterio,
                                                   iOrden: Smallint): OleVariant;
const
     Q_TOP = 'TOP %d';
     Q_PG_FOLIO = ' order by EP_NUMERO'; // Cambio a EP_NUMERO bug de Dise�o
     Q_PROMEDIO_DESC = ' order by PROMEDIO DESC';
     Q_PROMEDIO_ASC = ' order by PROMEDIO';
var
   sFiltro, sMostrar, sQuery, sOrdenar: String;
begin
     sFiltro := VACIO;
     sMostrar := VACIO;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if( iRelacion >= 0 )then
              sFiltro := Format( ' and ( EV_RELACIO = %d ) ', [ iRelacion ] );
          if( iCriterio > 0 )then
              sFiltro := sFiltro + Format( ' and ( EC_ORDEN = %d ) ', [ iCriterio ] );
          if( iMostrar > 0 )then
              sMostrar := Format( Q_TOP, [ iMostrar ] );
          case( iOrden )of
                0: sOrdenar := Q_PG_FOLIO;
                1: sOrdenar := Q_PROMEDIO_DESC;
                2: sOrdenar := Q_PROMEDIO_ASC;
          else
              sOrdenar := VACIO;
          end;
          sQuery := Format( GetScript( eAnalisisPreguntas ), [ sMostrar, iEncuesta, sFiltro ] ) + sOrdenar;
          Result := OpenSQL( Empresa, sQuery, TRUE );
     end;
     SetComplete;
end;


function TdmServerEvaluacion.GetPreguntasEnc(Empresa: OleVariant; iEncuesta: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( ePreguntaCriterios );
          with TablaInfo do
          begin
               Filtro := Format( '( ET_CODIGO = %d )', [ iEncuesta ] );
               SetOrder( 'PG_FOLIO' );
          end;
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetFrecuenciaResp(Empresa: OleVariant; iEncuesta: Integer; iPregunta, iRelacion: Smallint): OleVariant;
var
   sFiltro, sScript: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if( iRelacion >= 0 )then
              sFiltro := Format( ' and ( EV_RELACIO = %d ) ', [ iRelacion ] );
          sScript := Format( GetScript( eFrecuenciaResp ), [ iEncuesta, iPregunta, sFiltro ] );
          Result := OpenSQL( Empresa, sScript, TRUE );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetResultadoCriterio(Empresa: OleVariant; iEncuesta, iEmpleado: Integer; iMostrar, iOrden: Smallint): OleVariant;
const
     Q_TOP = 'TOP %d';
     Q_EC_ORDEN = ' order by SUJ_COMP.EC_ORDEN';
     Q_SO_TOTAL_DESC = ' order by SO_TOTAL DESC';
     Q_SO_TOTAL_ASC = ' order by SO_TOTAL';
var
   sMostrar, sOrdenar, sQuery: String;
begin
     sMostrar := VACIO;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if( iMostrar > 0 )then
              sMostrar := Format( Q_TOP, [ iMostrar ] );
          case( iOrden )of
                0: sOrdenar := Q_EC_ORDEN;
                1: sOrdenar := Q_SO_TOTAL_DESC;
                2: sOrdenar := Q_SO_TOTAL_ASC;
          else
              sOrdenar := VACIO;
          end;
          sQuery := Format( GetScript( eResultadoCriterio ), [ sMostrar, iEncuesta, iEmpleado ] ) + sOrdenar;
          Result := OpenSQL( Empresa, sQuery, TRUE );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetResultadoPregunta( Empresa: OleVariant; iEncuesta, iEmpleado: Integer; iMostrar, iOrden: Smallint): OleVariant;
const
     Q_TOP = 'TOP %d';
     Q_EC_ORDEN = ' order by SUJ_PREG.EC_ORDEN';
     Q_SP_TOTAL_DESC = ' order by SP_TOTAL DESC';
     Q_SP_TOTAL_ASC = ' order by SP_TOTAL';
var
   sMostrar, sOrdenar, sQuery: String;
begin
     sMostrar := VACIO;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if( iMostrar > 0 )then
              sMostrar := Format( Q_TOP, [ iMostrar ] );
          case( iOrden )of
                0: sOrdenar := Q_EC_ORDEN;
                1: sOrdenar := Q_SP_TOTAL_DESC;
                2: sOrdenar := Q_SP_TOTAL_ASC;
          else
              sOrdenar := VACIO;
          end;
          sQuery := Format( GetScript( eResultadoPregunta ), [ sMostrar, iEncuesta, iEmpleado ] ) + sOrdenar;
          Result := OpenSQL( Empresa, sQuery, TRUE );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetSujetos(Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, GetScript( eObtieneSujetos ), FALSE );
     end;
     SetComplete;
end;

function TdmServerEvaluacion.AgregaEmpEvaluar(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eSujeto );
     with oZetaProvider do
     begin
          Result := GrabaTablaGrid( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.BorraEmpEvaluar(Empresa: OleVariant; iEncuesta, iEmpleado: Integer);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEvaluacion := CreateQuery( GetScript( eBorraEmpEvaluar ) );
          try
             ParamAsInteger( FEvaluacion, 'ET_CODIGO', iEncuesta );
             ParamAsInteger( FEvaluacion, 'CB_CODIGO', iEmpleado );
             EmpiezaTransaccion;
             try
                Ejecuta( FEvaluacion );
                TerminaTransaccion( TRUE );
             except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                     raise;
                end;
             end;
          finally
                 FreeAndNil( FEvaluacion );
          end;
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.CambiaStatusSujeto(Empresa: OleVariant; iEncuesta, iEmpleado: Integer; lPonerListo: WordBool);
var
   iStatus: Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEvaluador := CreateQuery( GetScript( eModificaStatusSujeto ) );
          try
             ParamAsInteger( FEvaluador, 'ET_CODIGO', iEncuesta );
             ParamAsInteger( FEvaluador, 'CB_CODIGO', iEmpleado );
             if( lPonerListo )then
                 iStatus := Ord( ssListo )
             else
                 iStatus := Ord( ssNuevo );
             ParamAsInteger( FEvaluador, 'STATUS', iStatus );
             EmpiezaTransaccion;
             try
                Ejecuta( FEvaluador );
                TerminaTransaccion( True );
            except
                  on Error: Exception do
                  begin
                       RollBackTransaccion;
                       raise;
                  end;
            end;
          finally
                 FreeAndNil( FEvaluador );
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.AgregaEmpEvaluarGlobal(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     AgregaEmpEvaluarGlobalParametros;
     InitBroker;
     try
        AgregaEmpEvaluarGlobalBuildDataset;
        Result := AgregaEmpEvaluarGlobalDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.AgregaEmpEvaluarGlobalParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Encuesta: ' + ParamByName( 'Encuesta' ).AsString +
                              ZetaCommonClasses.K_PIPE + 'Evaluador: ' + ParamByName( 'Evaluador' ).AsString +
                              ZetaCommonClasses.K_PIPE + 'Relaci�n: ' + ParamByName( 'Relacion' ).AsString;
     end;
end;

procedure TdmServerEvaluacion.AgregaEmpEvaluarGlobalBuildDataset;
var
   iEncuesta, iEvaluador: Integer;
   eRelacion: eTipoEvaluaEnc;
begin
     with oZetaProvider.ParamList do
     begin
          iEncuesta := ParamByName( 'Encuesta' ).AsInteger;
          iEvaluador := ParamByName( 'Evaluador' ).AsInteger;
          eRelacion := eTipoEvaluaEnc( ParamByName( 'Relacion' ).AsInteger );
     end;
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', TRUE, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'PRETTYNAME' );
               AgregaColumna( IntToStr( iEncuesta ), TRUE, enNinguno, tgNumero, 0, 'ET_CODIGO' );
               AgregaColumna( IntToStr( iEvaluador ), TRUE, enNinguno, tgNumero, 0, 'US_CODIGO' );
               AgregaColumna( IntToStr( Ord( eRelacion ) ), TRUE, enNinguno, tgNumero, 0, 'EV_RELACIO' );

               AgregaFiltro( Format( '( CB_ACTIVO = %s )', [ EntreComillas('S') ] ), True, Entidad );

               AgregaOrden( 'CB_CODIGO', TRUE, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

function TdmServerEvaluacion.AgregaEmpEvaluarGlobalDataset(Dataset: TDataset): OleVariant;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prDEAgregaEmpEvaluarGlobal, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  CreaMatrizempleadosEvaluarBegin;
                  try
                     with Dataset do
                     begin
                          First;
                          while not Eof and CanContinue( FieldByName( 'ET_CODIGO' ).AsInteger ) do
                          begin
                               CreaMatrizEmpleadosEvaluar( FieldByName( 'ET_CODIGO' ).AsInteger, { Encuesta }
                                                           FieldByName( 'US_CODIGO' ).AsInteger, { Evaluador }
                                                           FieldByName( 'CB_CODIGO' ).AsInteger, { Empleado }
                                                           eTipoEvaluaEnc( FieldByName( 'EV_RELACIO' ).AsInteger ) );
                               Next;
                          end;
                     end;
                  finally
                         CreaMatrizEmpleadosEvaluarEnd;
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error agregar evaluadores globales', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerEvaluacion.CreaMatrizEmpleadosEvaluarBegin;
begin
     with oZetaProvider do
     begin
          FSujetoDataSet := CreateQuery( GetScript( eAgregaSujetos ) );
          FEvaluaDataSet := CreateQuery( GetScript( eAgregaEvaluaciones ) );
          FVerificaEmpleado := CreateQuery( GetScript( eVerificaSujeto ) );

     end;
end;

procedure TdmServerEvaluacion.CreaMatrizEmpleadosEvaluarEnd;
begin
     FreeAndNil( FVerificaEmpleado );
     FreeAndNil( FEvaluaDataSet );
     FreeAndNil( FSujetoDataSet );
end;

procedure TdmServerEvaluacion.CreaMatrizEmpleadosEvaluar( const iEncuesta, iEvaluador, iEmpleado: Integer; const eRelacion: eTipoEvaluaEnc; const lContadoresSujeto: Boolean = True );
var
   lNoHuboError, lExisteSujeto: Boolean;
begin
     lNoHuboError := True;
     with oZetaProvider do
     begin
          ParamAsInteger( FVerificaEmpleado, 'Encuesta', iEncuesta );
          ParamAsInteger( FVerificaEmpleado, 'Empleado', iEmpleado );
          with FVerificaEmpleado do
          begin
               Active := True;
               lExisteSujeto := ( FieldByName( 'CUANTOS' ).AsInteger > 0 );
               Active := False;
          end;
          if not lExisteSujeto then
          begin
               EmpiezaTransaccion;
               try
                  //MA: Agrega en Sujeto
                  ParamAsInteger( FSujetoDataSet, 'ET_CODIGO', iEncuesta );
                  ParamAsInteger( FSujetoDataSet, 'CB_CODIGO', iEmpleado );
                  ParamAsInteger( FSujetoDataSet, 'SJ_STATUS', Ord( ssListo ) );
                  Ejecuta( FSujetoDataSet );
                  TerminaTransaccion( TRUE );
               except
                     on Error: Exception do
                     begin
                          lNoHuboError := False;
                          RollBackTransaccion;
                          if DZetaServerProvider.PK_Violation( Error ) then
                             Log.Advertencia( iEmpleado, Format( 'El Empleado a evaluar : "%d" ya existe "',[ iEmpleado ] ) )
                          else
                              Log.Excepcion( iEmpleado, Format( 'Error al agregar empleado a evaluar  "%d" ', [ iEmpleado ] ), Error );
                     end;
               end;
          end;
          if lNoHuboError then
          begin
               if ( iEvaluador > 0 ) then
               begin
                    EmpiezaTransaccion;
                    try
                       //MA: Agrega en Evalua
                       ParamAsInteger( FEvaluaDataSet, 'ET_CODIGO', iEncuesta );
                       ParamAsInteger( FEvaluaDataSet, 'US_CODIGO', iEvaluador );
                       ParamAsInteger( FEvaluaDataSet, 'CB_CODIGO', iEmpleado );
                       ParamAsInteger( FEvaluaDataSet, 'EV_RELACIO', Ord( eRelacion ) );
                       ParamAsInteger( FEvaluaDataSet, 'EV_STATUS', Ord( sevNueva ) );
                       Ejecuta( FEvaluaDataSet );
                       TerminaTransaccion( TRUE );
                    except
                          on Error: Exception do
                          begin
                               RollBackTransaccion;
                               if ( Pos( 'unique', Error.Message ) > 0 ) then
                                  Log.Advertencia( iEmpleado, Format( 'La evaluaci�n del empleado "%d" ya existe "',[ iEvaluador ] ) )
                               else
                                   Log.Excepcion( iEmpleado, Format( 'Error al agregar evaluador "%d" ', [ iEvaluador ] ), Error )
                          end;
                    end;
               end;
          end;
          if lContadoresSujeto then
             ContadoresSujeto( EmpresaActiva, iEncuesta, iEmpleado );
     end;
end;

function TdmServerEvaluacion.AgregaEmpEvaluarGlobalGetLista( Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        AgregaEmpEvaluarGlobalBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.AgregaEmpEvaluarGlobalLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     AgregaEmpEvaluarGlobalParametros;
     cdsLista.Lista := Lista;
     Result := AgregaEmpEvaluarGlobalDataset( cdsLista );
     SetComplete;
end;

procedure TdmServerEvaluacion.PrepararEvaluacionesParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Encuesta: ' + ParamByName('Encuesta').AsString;
     end;
end;

function TdmServerEvaluacion.PrepararEvaluaciones(Empresa, Parametros: OleVariant): OleVariant;
var
   iEncuesta: Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          PrepararEvaluacionesParametros;
          if OpenProcess( prDEPrepararEvaluaciones, 0, FListaParametros ) then
          begin
               with ParamList do
               begin
                    iEncuesta := ParamByName('Encuesta').AsInteger;
               end;
               FEvaluacion := CreateQuery( Format( GetScript( ePrepararEvaluaciones ), [ iEncuesta ] ) );
               try
                  ParamAsInteger( FEvaluacion, 'Encuesta', iEncuesta );
                  EmpiezaTransaccion;
                  try
                     Ejecuta( FEvaluacion );
                     TerminaTransaccion(TRUE);
                  except
                        on Error: Exception do
                        begin
                             RollBackTransaccion;
                             Log.Excepcion( 0, 'Error al preparar evaluaciones', Error );
                        end;
                  end;
               finally
                      FreeAndNil( FEvaluacion );
               end;
               Result := CloseProcess;
          end;
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.CalcularResultadosParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Encuesta: ' + ParamByName('Encuesta').AsString;
     end;
end;

function TdmServerEvaluacion.CalcularResultados(Empresa, Parametros: OleVariant): OleVariant;
const
     Q_SUJETOS = 'Select CB_CODIGO from SUJETO where ET_CODIGO = %d order by CB_CODIGO';
var
   iEncuesta, iEmpleado: Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          CalcularResultadosParametros;
          iEncuesta := ParamList.ParamByName('Encuesta').AsInteger;
          cdsSujetos.Data := OpenSQL( Empresa, Format( Q_SUJETOS, [ iEncuesta ] ), TRUE );
          if OpenProcess( prDECalcularResultados, cdsSujetos.RecordCount, FListaParametros ) then
          begin
               FEvaluacion := CreateQuery( GetScript( eCalcularResultados ) );
               try
                  try
                     with cdsSujetos do
                     begin
                          First;
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               ParamAsInteger( FEvaluacion, 'Encuesta', iEncuesta );
                               ParamAsInteger( FEvaluacion, 'CB_CODIGO', iEmpleado );
                               EmpiezaTransaccion;
                               try
                                  Ejecuta( FEvaluacion );
                                  TerminaTransaccion( TRUE );
                                  //Log.Evento( clbNinguno, iEncuesta, NullDateTime, Format( 'Empleado a evaluar "%d" agregado.', [ iEmpleado ] ) );
                               except
                                    on Error: Exception do
                                    begin
                                         RollBackTransaccion;
                                         Log.Excepcion( iEmpleado, 'Error al calcular resultados', Error );
                                    end;
                               end;
                               Next;
                          end;
                     end;
                  finally
                      FreeAndNil( FEvaluacion );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error agregar evaluadores globales', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.RecalcularPromediosParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Encuesta: ' + ParamByName('Encuesta').AsString;
     end;
end;

function TdmServerEvaluacion.RecalcularPromedios(Empresa, Parametros: OleVariant): OleVariant;
var
   iEncuesta, iEmpleado: Integer;
   EV_FOLIO: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          RecalcularPromediosParametros;
          iEncuesta := ParamList.ParamByName('Encuesta').AsInteger;
          cdsDatos.Data := OpenSQL( Empresa, Format( GetScript( eEvaluaciones ), [ iEncuesta, '' ] ), TRUE );
          if OpenProcess( prDERecalcularPromedios, cdsDatos.RecordCount, FListaParametros ) then
          begin
               with ParamList do
               begin
                    FEvaluacion := CreateQuery( GetScript( eRecalcularPromedios ) );
                    try
                       with cdsDatos do
                       begin
                            First;
                            while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                            begin
                                 EV_FOLIO := FieldByName( 'EV_FOLIO' ).AsString;
                                 iEmpleado := FieldByName('CB_CODIGO').AsInteger;
                                 ParamAsString( FEvaluacion, 'Evaluacion', EV_FOLIO );
                                 EmpiezaTransaccion;
                                 try
                                    Ejecuta( FEvaluacion );
                                    TerminaTransaccion( TRUE );
                                    //Log.Evento( clbNinguno, iEncuesta, NullDateTime, Format( 'Empleado a evaluar "%d" agregado.', [ iEmpleado ] ) );
                                 except
                                      on Error: Exception do
                                      begin
                                           RollBackTransaccion;
                                           Log.Excepcion( iEmpleado, 'Error al recalcular promedios', Error );
                                      end;
                                 end;
                                 Next;
                            end;
                       end;
                   finally
                          FreeAndNil( FEvaluacion );
                   end;
               end;
               Result := CloseProcess;
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetPregContestadas(Empresa: OleVariant; iUsuario, iEmpleado, iEncuesta: Integer): Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FPregunta := CreateQuery( Format( GetScript( ePreguntasContestadas ), [ iEncuesta, iUsuario, iEmpleado ] ) );
          try
             FPregunta.Open;
             Result := FPregunta.FieldByName('EV_FIN_PRE').AsInteger;
             FPregunta.Close;
          finally
                 FreeAndNil(FPregunta);
          end;
     end;
     SetComplete;
end;


function TdmServerEvaluacion.CierraEvaluaciones(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     CierraEvaluacionesParametros;
     InitBroker;
     try
        CierraEvaluacionesBuildDataset;
        Result := CierraEvaluacionesDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.CierraEvaluacionesParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Encuesta: ' + ParamByName( 'Encuesta' ).AsString; {
                              ZetaCommonClasses.K_PIPE + 'Evaluador: ' + ParamByName( 'Evaluador' ).AsString +
                              ZetaCommonClasses.K_PIPE + 'Relaci�n: ' + ParamByName( 'Relacion' ).AsString;}
     end;
end;

procedure TdmServerEvaluacion.CierraEvaluacionesBuildDataset;
var
   iEncuesta: Integer;
begin
     with oZetaProvider.ParamList do
     begin
          iEncuesta := ParamByName( 'Encuesta' ).AsInteger;
     end;
     with SQLBroker do
     begin
          Init( enEncuesta );
          with Agente do
          begin
               AgregaColumna( 'ET_CODIGO', TRUE, Entidad, tgNumero, 0, 'ET_CODIGO' );
               AgregaColumna( 'ET_NOMBRE', TRUE, enEncuesta, tgTexto, K_ANCHO_PRETTYNAME, 'ET_NOMBRE' );
               AgregaFiltro( Format( '( ET_CODIGO = %d )', [ iEncuesta ] ), True, Entidad );

               AgregaOrden( 'ET_CODIGO', TRUE, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

function TdmServerEvaluacion.CierraEvaluacionesDataset(Dataset: TDataset): OleVariant;
begin
     SetComplete;
end;


function TdmServerEvaluacion.CierraEvaluacionesGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     SetComplete;
end;

function TdmServerEvaluacion.CierraEvaluacionesLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     SetComplete;
end;

function TdmServerEvaluacion.CreaEscalasEncuesta( const iEncuesta: Smallint; const sEscala: WideString): Smallint;
var
   FEscalas: TZetaCursor;
begin
     Result := 0;
     with oZetaProvider do
     begin
          FEscalas := CreateQuery( GetScript( eCreaEscala) );
          try
             ParamAsInteger( FEscalas, 'Encuesta', iEncuesta );
             ParamAsChar( FEscalas, 'Escala', sEscala, K_ANCHO_CODIGO );
             EmpiezaTransaccion;
             try
                Result := Ejecuta( FEscalas );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
         finally
                FreeAndNil( FEscalas );
         end;
     end;
end;

function TdmServerEvaluacion.CopiaEscalasEncuesta(Empresa: OleVariant; iEncuesta: Smallint; const sEscala: WideString): Smallint;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
     end;
     Result := CreaEscalasEncuesta( iEncuesta, sEscala );
     SetComplete;
end;

procedure TdmServerEvaluacion.InicializaEncuesta(Empresa: OleVariant; iEncuesta: Integer);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEncuesta := CreateQuery( GetScript( eInicializaEncuesta ) );
          try
              ParamAsInteger( FEncuesta, 'Encuesta', iEncuesta );
              EmpiezaTransaccion;
              try
                 Ejecuta( FEncuesta );
                 TerminaTransaccion( True );
              except
                    on Error: Exception do
                    begin
                         RollBackTransaccion;
                         raise;
                    end;
              end;
          finally
                 FreeAndNil( FEncuesta );
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetDatosXEvaluado( Empresa: OleVariant; iEncuesta, iSujeto: Integer ): OleVariant;
const
     K_FILTRO_EVALUADOS = 'where( ET_CODIGO = %d ) and ( CB_CODIGO = %d )';
var
   sFiltro: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          sFiltro := Format( K_FILTRO_EVALUADOS, [ iEncuesta, iSujeto ] );
          Result := OpenSQL( Empresa, Format( GetScript( eTotalesEncuesta ), [ 'EV_STATUS', 'EVALUA', sFiltro  ] ), TRUE );
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.CorreosInvitacionParametros;
var
   sEvaluadores, sFormato: String;
begin
     with oZetaProvider.ParamList do
     begin
          sEvaluadores := ParamByName( 'Evaluadores' ).AsString;
          if StrLleno( sEvaluadores ) then
             sEvaluadores := ZetaCommonClasses.K_PIPE + 'Evaluadores: ' + sEvaluadores;
          FListaParametros := VACIO;
          FListaParametros := 'Encuesta: ' + ParamByName( 'Encuesta' ).AsString +
                              ZetaCommonClasses.K_PIPE + 'Relacion: ' + ParamByName( 'RelacionDesc' ).AsString +
                              ZetaCommonclasses.K_PIPE + 'Evaluadores:' + sEvaluadores;
                              if ( ParamByName( 'TipoCorreo' ).AsInteger = K_TIPO_PLANTILLA ) then
                              begin
                                   FListaParametros := FListaParametros + ZetaCommonClasses.K_PIPE + 'Tipo de Correo: Plantilla' +
                                   ZetaCommonClasses.K_PIPE + 'Tema: ' + ParamByName( 'TemaPlantilla' ).AsString +
                                   ZetaCommonClasses.K_PIPE + 'Plantilla: ' + ParamByName( 'Plantilla' ).AsString;
                              end
                              else
                              begin
                                   FListaParametros := FListaParametros + ZetaCommonClasses.K_PIPE + 'Tipo de Correo: Texto fijo' +
                                   ZetaCommonClasses.K_PIPE + 'Tema: ' + ParamByName( 'TemaTexto' ).AsString;
                                   if ( ParamByName( 'FormatoCorreo' ).AsInteger = K_FORMATO_TEXTO ) then
                                       sFormato := 'Texto fijo'
                                   else
                                       sFormato := 'HTML';
                                   FListaParametros := FListaParametros + ZetaCommonClasses.K_PIPE + 'Formato: ' + sFormato;
                              end;
     end;
end;

procedure TdmServerEvaluacion.CorreosInvitacionBuildDataset;
var
   sEvaluadores, sFiltroEvaluadores, sFiltroRelacion : String;
   iEncuesta, iRelacion: Integer;
begin
     sFiltroEvaluadores := VACIO;
     sFiltroRelacion := VACIO;
     with oZetaProvider.ParamList do
     begin
          iEncuesta := ParamByName( 'Encuesta' ).AsInteger;
          iRelacion := ParamByName( 'Relacion' ).AsInteger;
          sEvaluadores := ParamByName( 'Evaluadores' ).AsString;
     end;
     with SQLBroker do
     begin
          Init( enEvalua );
          with Agente do
          begin
               AgregaColumna( 'ET_CODIGO', TRUE, Entidad, tgNumero, 0, 'ET_CODIGO' );
               AgregaColumna( 'PRETTYNAME', TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'PRETTYNAME' );
               AgregaColumna( 'US_CODIGO', TRUE, Entidad, tgNumero, 0, 'US_CODIGO' );
               AgregaColumna( 'Usuario( US_CODIGO )', FALSE, Entidad, tgTexto, K_ANCHO_PRETTYNAME, 'US_NOMBRE' );
               AgregaColumna( 'CB_CODIGO', TRUE, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaFiltro( Format( '( EVALUA.ET_CODIGO = %d )', [ iEncuesta ] ), True, Entidad );
               if StrLleno( sEvaluadores ) then
                   AgregaFiltro( Format( '( EVALUA.US_CODIGO in ( %s ) )', [ sEvaluadores ] ), True, Entidad );
               if ( iRelacion >= 0 ) then
                   AgregaFiltro( Format( '( EVALUA.EV_RELACIO = %d )', [ iRelacion ] ), True, Entidad );
               AgregaOrden( 'CB_CODIGO', TRUE, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

function TdmServerEvaluacion.CorreosInvitacion( Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     CorreosInvitacionParametros;
     InitBroker;
     try
        CorreosInvitacionBuildDataset;
        Result := CorreosInvitacionDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.CorreosInvitacionDataset( Dataset: TDataset ): OleVariant;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prDECorreosInvitacion, Dataset.RecordCount, FListaParametros ) then
          begin
               with Dataset do
               begin
                    First;
                    while not EOF do
                    begin
                         EscribeCorreoEnBuzon( FieldByName( 'ET_CODIGO' ).AsInteger, DataSet, CorreosInvitacionConstruyeDataset );
                         Next;
                    end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerEvaluacion.CorreosInvitacionConstruyeDataset( const iEncuesta: Integer; Dataset: TDataset; const sPlantilla: String; var sHTML: String ): Boolean;
var
   iUsuario, iEmpleado: Integer;
begin
     with Dataset do
     begin
          iUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
     end;
     with cdsCorreos do
     begin
          with oZetaProvider do
          begin
               Lista := OpenSQL( EmpresaActiva, Format( GetScript( eDatosEmpleadoXML ), [ iEncuesta, iEmpleado, iUsuario ] ), TRUE );
          end;
          if cdsListaUsuarios.Locate( 'US_CODIGO', cdsCorreos.FieldByName( 'ET_USR_ADM' ).AsInteger, [] ) then
          begin
               with FDatosAdministrador do
               begin
                    Nombre := cdsListaUsuarios.FieldByName( 'US_NOMBRE' ).AsString;
                    EMail := cdsListaUsuarios.FieldByName( 'US_EMAIL' ).AsString;
               end;
          end;
          First;
          while not Eof do
          begin
               if cdsListaUsuarios.Locate( 'US_CODIGO', FieldByName( 'US_CODIGO' ).AsInteger, [] ) then
               begin
                    Edit;
                    FieldByName( 'US_NOMBRE' ).AsString := cdsListaUsuarios.FieldByName( 'US_NOMBRE' ).AsString;
                    Post
               end;
               Next;
          end;
     end;
     Result := FXMLTools.XMLTransform( GeneraXMLParaCorreos( 'EMPLEADO' ), sPlantilla, sHTML );
end;

{.$define TEST_XML}
function TdmServerEvaluacion.GeneraXMLParaCorreos( const sTag: String ): WideString;
{$ifdef TEST_XML}
var
    oArchivo: TStrings;
{$endif}
begin
     {$ifdef TEST_XML}
     oArchivo := TStringList.Create;
     try
     {$endif}
         with FXMLTools do
         begin
              XMLInit( 'DATOS' );
              XMLBuildDataRow( cdsCorreos, sTag, True );
              WriteAttributeString( 'US_ET_ADM', FDatosAdministrador.Nombre );
              with oZetaProvider do
              begin
                   InitGlobales;
                   WriteAttributeString( 'Empresa', EmpresaActiva[ P_CODIGO ] );
                   WriteAttributeString( 'RutaVirtual', GetGlobalString( K_GLOBAL_RUTA_VIRTUAL ) );
              end;
              XMLEnd;
              {$ifdef TEST_XML}
              with oArchivo do
              begin
                   Add( XMLAsText );
                   SaveToFile( 'D:\Temp\DatosCorreos.xml' );
              end;
              {$endif}
              Result := XMLAsText;
         end;
     {$ifdef TEST_XML}
     finally
            FreeAndNil( oArchivo );
     end;
     {$endif}
end;

function TdmServerEvaluacion.CorreosInvitacionGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        CorreosInvitacionBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.CorreosInvitacionLista( Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     CorreosInvitacionParametros;
     cdsLista.Lista := Lista;
     Result := CorreosInvitacionDataset( cdsLista );
     SetComplete;
end;

function TdmServerEvaluacion.CorreosNotificacion( Empresa, Parametros: OleVariant): OleVariant;
const
     Q_EVALUADORES_POR_STATUS = 'select DISTINCT( US_CODIGO ) from EVALUA where ( ET_CODIGO = %0:d ) %1:s '+
                                'and ( US_CODIGO in ( select US_CODIGO from DBO.SP_STATUS_EVALUADORES( %0:d ) where %2:s ) )';
     Q_EVALUADORES_POR_LISTA = 'select DISTINCT( US_CODIGO ) from EVALUA where ( ET_CODIGO = %0:d ) %1:s';
var
   iEncuesta, iRelacion: Integer;
   sFiltro, sEvaluadores, sRelacion, sQuery: String;
   lPorRelacion: Boolean;
begin
     sFiltro := VACIO;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          CorreosInvitacionParametros;
          with ParamList do
          begin
               iEncuesta := ParamByName( 'Encuesta' ).AsInteger;
               lPorRelacion := ParamByName( 'PorRelacion' ).AsBoolean;
               iRelacion := ParamByName( 'Relacion' ).AsInteger;
               case eStatusEvaluador( ParamByName( 'Status' ).AsInteger ) of
                    esteNoHaComenzado: sFiltro := '( SE_NUEVA = SE_TOTAL )';
                    esteEnProceso: sFiltro := '( SE_TERMINA <> SE_TOTAL ) and ( SE_NUEVA <> SE_TOTAL )';
                    esteTerminado: sFiltro := '( SE_TERMINA = SE_TOTAL )';
               else
                   sFiltro := 'STATUS INVALIDO';
               end;
               sEvaluadores :=  ParamByName( 'Evaluadores' ).AsString;
          end;
          if lPorRelacion then
          begin
               if ( iRelacion >= 0 )then
                  sRelacion := Format( 'and ( EV_RELACIO = %d ) ', [  iRelacion  ] )
               else
                   sRelacion := '';
               sQuery := Format( Q_EVALUADORES_POR_STATUS, [ iEncuesta, sRelacion, sFiltro ] );
          end
          else
          begin
               if StrLleno( sEvaluadores ) then
               begin
                    sEvaluadores := Format( 'and ( US_CODIGO in ( %s ) )', [ sEvaluadores ]  );
               end;
               sQuery := Format( Q_EVALUADORES_POR_LISTA, [ iEncuesta, sEvaluadores ] );
          end;
          cdsDatos.Lista := OpenSQL( Empresa, sQuery, TRUE );
          if OpenProcess( prDECorreosNotificacion, cdsDatos.RecordCount, FListaParametros ) then
          begin
               EscribeCorreoEnBuzon( iEncuesta, cdsDatos, CorreosNotificacionConstruyeDataset );
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerEvaluacion.CorreosNotificacionConstruyeDataset( const iEncuesta: Integer; Dataset: TDataset; const sPlantilla: String; var sHTML: String ): Boolean;
const
     K_CAMPOS_USUARIO = ', %s as US_NOMBRE, %s as US_EMAIL';
var
   iUsuario: Integer;
   sUserName, sUserEMail: String;
begin
     with Dataset do
     begin
          iUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
     end;
     if cdsListaUsuarios.Locate( 'US_CODIGO', iUsuario, [] ) then
     begin
          sUserName := cdsListaUsuarios.FieldByName( 'US_NOMBRE' ).AsString;
          sUserEMail := cdsListaUsuarios.FieldByName( 'US_EMAIL' ).AsString;
     end
     else
     begin
          sUserName := K_UNKNOWN;
          sUserEMail := K_UNKNOWN;
     end;
     with oZetaProvider do
     begin
          cdsCorreos.Lista := OpenSQL( EmpresaActiva, Format( GetScript( eGetEncuesta ), [ Format( K_CAMPOS_USUARIO, [ EntreComillas( sUserName ), EntreComillas( sUserEMail ) ] ), iEncuesta ] ), TRUE );
     end;
     if cdsListaUsuarios.Locate( 'US_CODIGO', cdsCorreos.FieldByName( 'ET_USR_ADM' ).AsInteger, [] ) then
     begin
          with FDatosAdministrador do
          begin
               Nombre := cdsListaUsuarios.FieldByName( 'US_NOMBRE' ).AsString;
               EMail := cdsListaUsuarios.FieldByName( 'US_EMAIL' ).AsString;
          end;
     end;
     Result := FXMLTools.XMLTransform( GeneraXMLParaCorreos( 'EVALUADOR' ), sPlantilla, sHTML );
end;

procedure TdmServerEvaluacion.EscribeCorreoEnBuzon( const iEncuesta: Integer; DataSet: TDataSet; Metodo: TXSLTMethod );
var
   sHTML: String;
   FParametrosCorreo: TZetaParams;
   iUsuario: Integer;
begin
     with FDatosAdministrador do
     begin
          Nombre := K_UNKNOWN;
          EMail := K_UNKNOWN;
     end;
     //Procesar registros para WOUTBOX
     with DataSet do
     begin
          if IsEmpty then
          begin
               oZetaProvider.Log.Error( 0, 'No hay datos para generar correos', VACIO );
          end
          else
          begin
               cdsListaUsuarios.Lista := Buzon.ListaUsuarios;
               FParametrosCorreo := TZetaParams.Create;
               try
                  First;
                  with oZetaProvider do
                  begin
                       while not Eof and CanContinue( FieldByName( 'US_CODIGO' ).AsInteger ) do
                       begin
                            iUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
                            if cdsListaUsuarios.Locate( 'US_CODIGO', iUsuario, [] ) then
                            begin
                                 with FParametrosCorreo do
                                 begin
                                      Clear;
                                      AddString( 'WO_TO', cdsListaUsuarios.FieldByName( 'US_EMAIL' ).AsString );
                                      AddString( 'WO_CC', VACIO );
                                      if ( ParamList.ParamByName( 'TipoCorreo' ).AsInteger = K_TIPO_PLANTILLA ) then
                                      begin
                                           AddString( 'WO_SUBJECT', ParamList.ParamByName( 'TemaPlantilla' ).AsString );
                                           AddBlob( 'WO_BODY', nil );
                                           try
                                              if Assigned( Metodo ) then
                                              begin
                                                   if Metodo( iEncuesta, Dataset, ParamList.ParamByName( 'Plantilla' ).AsString, sHTML ) then
                                                  AddString( 'WO_BODY', sHTML )
                                                   else
                                                       Log.Error( 0, 'No se pudo hacer el merge entre el XML - XSL', sHTML );
                                              end
                                              else
                                                  Log.Error( 0, 'No hay m�todo para el merge entre el XML - XSL', VACIO );
                                           except
                                                 on Error: Exception do
                                                 begin
                                                      Log.Excepcion( 0, 'No se pudo hacer el merge entre el XML-XSL', Error );
                                                 end;
                                           end;
                                      end
                                      else
                                      begin
                                           cdsCorreos.Lista := OpenSQL( EmpresaActiva, Format( GetScript( eGetEncuesta ), [ VACIO, iEncuesta ] ), TRUE );
                                           if cdsListaUsuarios.Locate( 'US_CODIGO', cdsCorreos.FieldByName( 'ET_USR_ADM' ).AsInteger, [] ) then
                                           begin
                                                with FDatosAdministrador do
                                                begin
                                                     Nombre := cdsListaUsuarios.FieldByName( 'US_NOMBRE' ).AsString;
                                                     Email := cdsListaUsuarios.FieldByName( 'US_EMAIL' ).AsString;
                                                end;
                                           end;
                                           AddString( 'WO_SUBJECT', ParamList.ParamByName( 'TemaTexto' ).AsString );
                                           AddBlob( 'WO_BODY', ParamList.ParamByName( 'Mensaje' ).AsBlob )
                                      end;
                                      with FDatosAdministrador do
                                      begin
                                           AddString( 'WO_FROM_NA', Nombre );
                                           AddString( 'WO_FROM_AD', EMail );
                                      end;
                                      if ( ParamList.ParamByName( 'FormatoCorreo' ).AsInteger = K_FORMATO_TEXTO ) then
                                          AddInteger( 'WO_SUBTYPE', Ord( emtTexto ) )
                                      else
                                          AddInteger( 'WO_SUBTYPE', Ord( emtHTML ) );
                                      AddDate( 'WO_FEC_IN', Date );
                                      AddInteger( 'US_ENVIA', ParamList.ParamByName( 'Responsable' ).AsInteger );
                                      AddInteger( 'US_RECIBE', cdsListaUsuarios.FieldByName( 'US_CODIGO' ).AsInteger );
                                 end;
                                 EmpiezaTransaccion;
                                 try
                                    Buzon.GrabaBuzon( FParametrosCorreo );
                                    TerminaTransaccion( TRUE );
                                 except
                                       on Error: Exception do
                                       begin
                                            RollBackTransaccion;
                                            Log.Excepcion( 0, 'Error al preparar env�o de correo', Error );
                                       end;
                                 end;
                            end
                            else
                                Log.Advertencia( iUsuario, Format( 'El usuario %d no existe', [ iUsuario ] ) );
                            Next;
                       end;
                  end;
               finally
                      FreeAndNil( FParametrosCorreo );
               end;
          end;
     end;
end;

function TdmServerEvaluacion.GetEmpleado(Empresa: OleVariant; Empleado, Encuesta: Integer): Integer;
var
   sSQL: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          sSQL := Format( GetScript( e_Va_Empleado ), [ Empleado, Encuesta ] );
          FEmpleado := CreateQuery( sSQL );
          try
             FEmpleado.Open;
             Result := FEmpleado.FieldByName('CB_CODIGO').AsInteger;
             FEmpleado.Close;
          finally
                 FreeAndNil(FEmpleado);
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetEmpleadoSiguiente(Empresa: OleVariant; Empleado, Encuesta: Integer): Integer;
var
   sSQL: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          sSQL := Format( GetScript( e_Va_Empleado_Siguiente ), [ Empleado, Encuesta ] );
          FEmpleado := CreateQuery( sSQL );
          try
             FEmpleado.Open;
             Result := FEmpleado.FieldByName('CB_CODIGO').AsInteger - 1;
             if( Result = -1 )then
                 Result := 0;
             FEmpleado.Close;
          finally
                 FreeAndNil(FEmpleado);
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetEmpleadoAnterior(Empresa: OleVariant; Empleado, Encuesta: Integer): Integer;
var
   sSQL: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          sSQL := Format( GetScript( e_Va_Empleado_Anterior ), [ Empleado, Encuesta ] );
          FEmpleado := CreateQuery( sSQL );
          try
             FEmpleado.Open;
             Result := FEmpleado.FieldByName('CB_CODIGO').AsInteger;
             if( Result > 0 )then
                 Result := Result + 1;
             FEmpleado.Close;
          finally
                 FreeAndNil(FEmpleado);
          end;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.GetEmpleadosBuscados( Empresa: OleVariant;
                                                   const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: WideString;
                                                   iEncuesta: Integer): OleVariant;
var
   sSQL: String;
begin
     sSQL := Format( GetScript( eEmpleadosBuscados ), [ Trim( sPaterno ) + '%',
                                                        Trim( sMaterno ) + '%',
                                                        Trim( sNombre )  + '%',
                                                        Trim( sRFC ) + '%',
                                                        Trim( sNSS ) + '%',
                                                        Trim( sBanca ) + '%',
                                                        iEncuesta,
                                                        Nivel0( Empresa ) ] );
     Result := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     SetComplete;
end;

function TdmServerEvaluacion.GetCorreos( Parametros: OleVariant ): OleVariant;
begin
     Result := Buzon.ConsultaBitacoraCorreos( Parametros );
     SetComplete;
end;

function TdmServerEvaluacion.GetGlobales(Empresa: OleVariant): OleVariant;
var
   FParametros: TZetaParams;
begin
     FParametros := TZetaParams.Create;
     try
        try
           {
           Result := Buzon.GetGlobalesEvaluacion;
           }
           with oZetaProvider do
           begin
                EmpresaActiva := Empresa;
                InitGlobales;
                with FParametros do
                begin
                     Clear;
                     AddString( Format( '%d', [ K_GLOBAL_RUTA_VIRTUAL ] ), GetGlobalString( K_GLOBAL_RUTA_VIRTUAL ) );
                     Result := VarValues;
                end;
           end;
           SetComplete;
        except
              on Error: Exception do
              begin
                   SetAbort;
                   raise;
              end;
        end;
     finally
            FreeAndNil( FParametros );
     end;
end;

procedure TdmServerEvaluacion.GrabaGlobales(Empresa, Valores: OleVariant);
var
   FParametros: TZetaParams;
begin
     FParametros := TZetaParams.Create;
     try
        try
           {
           Buzon.GrabaGlobalesEvaluacion( Valores );
           }
           with oZetaProvider do
           begin
                EmpresaActiva := Empresa;
                InitGlobales;
                with FParametros do
                begin
                     VarValues := Valores;
                     GlobalWriteString( K_GLOBAL_RUTA_VIRTUAL, ParamByName( Format( '%d', [ K_GLOBAL_RUTA_VIRTUAL ] ) ).AsString );
                end;
           end;
           SetComplete;
        except
              on Error: Exception do
              begin
                   SetAbort;
                   raise;
              end;
        end;
     finally
            FreeAndNil( FParametros );
     end;
end;

function TdmServerEvaluacion.GetRolesUsuario(Usuario: Integer): OleVariant;
begin
     try
        Result := Buzon.GetRolesUsuario( Usuario );
        SetComplete;
     except
           on Error: Exception do
           begin
                SetAbort;
                raise;
           end;
     end;
end;

function TdmServerEvaluacion.GrabaUsuarios(Usuario: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     try
        Result := Buzon.GrabaUsuarios( Usuario, Delta, ErrorCount, cdsLista );
        SetComplete;
     except
           on Error: Exception do
           begin
                SetAbort;
                raise;
           end;
     end;
end;

function TdmServerEvaluacion.GetUsuarios(Grupo: Integer): OleVariant;
begin
     try
        Result := Buzon.GetUsuarios( Grupo );
        SetComplete;
     except
           on Error: Exception do
           begin
                SetAbort;
                raise;
           end;
     end;
end;

procedure TdmServerEvaluacion.AsignaRelacionesParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Encuesta: ' + ParamByName( 'Encuesta' ).AsString +
                              ZetaCommonClasses.K_PIPE + 'Relaciones: ' + ParamByName( 'Relaciones' ).AsString;
     end;
end;

procedure TdmServerEvaluacion.AsignaRelacionesBuildDataset;
begin
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', TRUE, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'PRETTYNAME' );
               AgregaFiltro( Format( '( CB_ACTIVO = %s )', [ EntreComillas('S') ] ), True, Entidad );

               AgregaOrden( 'CB_CODIGO', TRUE, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

function TdmServerEvaluacion.AsignaRelaciones(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     AsignaRelacionesParametros;
     InitBroker;
     try
        AsignaRelacionesBuildDataset;
        Result := AsignaRelacionesDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.AsignaRelacionesGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        AsignaRelacionesBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.AsignaRelacionesLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     AsignaRelacionesParametros;
     cdsLista.Lista := Lista;
     Result := AsignaRelacionesDataset( cdsLista );
     SetComplete;
end;

function TdmServerEvaluacion.AsignaRelacionesDataset(Dataset: TDataset): OleVariant;
         function GetLimitesRelacion( const oRelacionTemp: eTipoEvaluaEnc; const lMaximo: Boolean; const iEncuesta: Integer ): Integer;
         const
              Q_LIMITES = 'select ER_NUM_MAX, ER_NUM_MIN from ENC_RELA where( ET_CODIGO = %d ) and ( ER_TIPO = %d )';
         var
            FLimitesPerfil: TZetaCursor;
         begin
              with oZetaProvider do
              begin
                   FLimitesPerfil := CreateQuery( Format( Q_LIMITES, [  iEncuesta, Ord( oRelacionTemp ) ]  ) );
                   try
                      FLimitesPerfil.Open;
                      if( lMaximo )then
                          Result := FLimitesPerfil.FieldByName( 'ER_NUM_MAX' ).AsInteger
                      else
                          Result := FLimitesPerfil.FieldByName( 'ER_NUM_MIN' ).AsInteger;
                      FLimitesPerfil.Close;
                   finally
                          FreeAndNil( FLimitesPerfil );
                   end;
              end;
         end;

         function GetTotalEvaluadoresRelacion( const oRelacionTemp: eTipoEvaluaEnc; const iEncuesta, iEmpleado: Integer ): Integer;
         const
              Q_LIMITES = 'select count(US_CODIGO) as CUANTOS from EVALUA where( ET_CODIGO = %d ) and ( EV_RELACIO = %d ) and ( CB_CODIGO = %d )';
         var
            FTotalEvaluadores: TZetaCursor;
         begin
              with oZetaProvider do
              begin
                   FTotalEvaluadores := CreateQuery( Format( Q_LIMITES, [  iEncuesta, Ord( oRelacionTemp ), iEmpleado ]  ) );
                   try
                      FTotalEvaluadores.Open;
                      Result := FTotalEvaluadores.FieldByName( 'CUANTOS' ).AsInteger;
                      FTotalEvaluadores.Close;
                   finally
                          FreeAndNil( FTotalEvaluadores );
                   end;
              end;
         end;

         procedure ObtenerSubordinados( const iUsuario: Integer; const oDataSetInfo, oDataSetResult: TServerDataSet );
         begin
              with oDataSetResult do
              begin
                   Lista := oDataSetInfo.Data;
                   Filtered := False;
                   Filter := Format( 'US_JEFE = %d', [ iUsuario ] );
                   Filtered := True;
              end;
         end;
var
   sEmpresa: String;
   i, iEncuesta, iEmpleado, iMaximoRelacion, iContador, iJefeInmediato: Integer;
   iMinimoRelacion, iTotalEvaluadoresRelacion, iFaltanEvaluadores, iUsuario: Integer;
   eRelacion: eTipoEvaluaEnc;
   oRelaciones: TStrings;
begin
     with oZetaProvider do
     begin
          sEmpresa := EmpresaActiva[ P_CODIGO ];
          iEncuesta := ParamList.ParamByName( 'Encuesta' ).AsInteger;
          cdsListaUsuarios.Lista := Buzon.ListaUsuarios;
          if OpenProcess( prDEAsignaRelaciones, Dataset.RecordCount, FListaParametros ) then
          begin
               CreaMatrizEmpleadosEvaluarBegin;
               try
                  with Dataset do
                  begin
                       oRelaciones := TStringList.Create;
                       try
                          oRelaciones.CommaText := ParamList.ParamByName( 'Relaciones' ).AsString;
                          while not Eof and CanContinue( iEncuesta ) do
                          begin
                               for i := 0 to ( oRelaciones.Count - 1 ) do
                               begin
                                    eRelacion := eTipoEvaluaEnc( StrToInt( oRelaciones.Strings[ i ] ) );
                                    iMaximoRelacion := GetLimitesRelacion( eRelacion, True, iEncuesta );
                                    iMinimoRelacion := GetLimitesRelacion( eRelacion, False, iEncuesta );
                                    iEmpleado := FieldByName('CB_CODIGO').AsInteger;
                                    iTotalEvaluadoresRelacion := GetTotalEvaluadoresRelacion( eRelacion, iEncuesta, iEmpleado );
                                    if cdsListaUsuarios.Locate( 'CM_CODIGO;CB_CODIGO', VarArrayOf( [ sEmpresa, iEmpleado ] ), [] ) then
                                    begin
                                         if ( iMaximoRelacion > 0 ) then
                                         begin
                                              if ( iTotalEvaluadoresRelacion <= iMaximoRelacion ) then
                                              begin
                                                   case eRelacion of
                                                        tevMismo:
                                                        begin
                                                             CreaMatrizEmpleadosEvaluar( iEncuesta,
                                                                                         cdsListaUsuarios.FieldByName('US_CODIGO').AsInteger,
                                                                                         iEmpleado,
                                                                                         eRelacion );
                                                        end;
                                                        tevJefe:
                                                        begin
                                                             if ( cdsListaUsuarios.FieldByName('US_JEFE').AsInteger > 0 ) then
                                                             begin
                                                                  CreaMatrizEmpleadosEvaluar( iEncuesta,
                                                                                              cdsListaUsuarios.FieldByName('US_JEFE').AsInteger,
                                                                                              iEmpleado,
                                                                                              eRelacion );
                                                             end
                                                             else
                                                                 Log.Advertencia( iEmpleado, Format( 'El empleado %d no cuenta con un jefe asignado',[ iEmpleado ] ) );
                                                        end;
                                                        tevSubordinado:
                                                        begin
                                                             iFaltanEvaluadores := ( iMaximoRelacion - iTotalEvaluadoresRelacion );
                                                             ObtenerSubordinados( cdsListaUsuarios.FieldByName( 'US_CODIGO' ).AsInteger, cdsListaUsuarios, cdsSujetos );
                                                             if ( iMinimoRelacion > 0 ) and cdsSujetos.IsEmpty then
                                                                Log.Advertencia( iEmpleado, Format( 'No se cuenta con al menos 1 %s para el empleado %d' , [ ObtieneElemento( lfTipoEvaluaEnc, Ord( eRelacion ) ), iEmpleado ] ) )
                                                             else
                                                             begin
                                                                  iContador := 1;
                                                                  if cdsSujetos.IsEmpty then
                                                                  begin
                                                                       CreaMatrizEmpleadosEvaluar( iEncuesta,
                                                                                                   0,
                                                                                                   iEmpleado,
                                                                                                   eRelacion )
                                                                  end
                                                                  else
                                                                  begin
                                                                       while not cdsSujetos.EOF do
                                                                       begin
                                                                            if ( iFaltanEvaluadores >= iContador ) then
                                                                            begin
                                                                                 CreaMatrizEmpleadosEvaluar( iEncuesta,
                                                                                                             cdsSujetos.FieldByName('US_CODIGO').AsInteger,
                                                                                                             iEmpleado,
                                                                                                             eRelacion );
                                                                                 iContador := iContador + 1;
                                                                            end
                                                                            else
                                                                            begin
                                                                                 Log.Advertencia( iEmpleado, 'El l�mite m�ximo de evaluadores se excedi�' );
                                                                                 Break;
                                                                            end;
                                                                            cdsSujetos.Next;
                                                                       end;
                                                                  end;
                                                             end;
                                                        end;
                                                        tevColega:
                                                        begin
                                                             iFaltanEvaluadores := ( iMaximoRelacion - iTotalEvaluadoresRelacion );
                                                             iJefeInmediato := cdsListaUsuarios.FieldByName( 'US_JEFE' ).AsInteger;
                                                             iUsuario := cdsListaUsuarios.FieldByName( 'US_CODIGO' ).AsInteger;
                                                             if cdsListaUsuarios.Locate( 'US_CODIGO', iJefeInmediato, [] ) then
                                                             begin
                                                                  iContador := 1;
                                                                  ObtenerSubordinados( iJefeInmediato, cdsListaUsuarios, cdsSujetos );
                                                                  while not cdsSujetos.Eof do
                                                                  begin
                                                                       if ( iFaltanEvaluadores >= iContador ) then
                                                                       begin
                                                                            if ( iUsuario <> cdsSujetos.FieldByName( 'US_CODIGO' ).AsInteger ) then
                                                                            begin
                                                                                 CreaMatrizEmpleadosEvaluar( iEncuesta,
                                                                                                             cdsSujetos.FieldByName('US_CODIGO').AsInteger,
                                                                                                             iEmpleado,
                                                                                                             eRelacion );
                                                                                 iContador := iContador + 1;
                                                                            end;
                                                                       end
                                                                       else
                                                                       begin
                                                                            Log.Advertencia( iEmpleado, 'El l�mite m�ximo de evaluadores se excedi�' );
                                                                            Break;
                                                                       end;
                                                                       cdsSujetos.Next;
                                                                  end;
                                                             end
                                                             else //No tiene Jefe inmediato
                                                             begin
                                                                  iContador := 1;
                                                                  ObtenerSubordinados( 0, cdsListaUsuarios, cdsSujetos );
                                                                  if cdsSujetos.IsEmpty then
                                                                  begin
                                                                       CreaMatrizEmpleadosEvaluar( iEncuesta,
                                                                                                   0,
                                                                                                   iEmpleado,
                                                                                                   eRelacion );
                                                                  end
                                                                  else
                                                                  begin
                                                                       while not cdsSujetos.EOF do
                                                                       begin
                                                                            if ( iFaltanEvaluadores >= iContador ) then
                                                                            begin
                                                                                 if ( iUsuario <> cdsListaUsuarios.FieldByName( 'US_CODIGO' ).AsInteger ) then
                                                                                 begin
                                                                                      CreaMatrizEmpleadosEvaluar( iEncuesta,
                                                                                                                  iUsuario,
                                                                                                                  iEmpleado,
                                                                                                                  eRelacion );
                                                                                      iContador := iContador + 1;
                                                                                 end;
                                                                            end
                                                                            else
                                                                            begin
                                                                                 Log.Advertencia( iEmpleado, 'El l�mite m�ximo de evaluadores se excedi�' );
                                                                                 Break;
                                                                            end;
                                                                            cdsSujetos.Next;
                                                                       end;
                                                                  end;
                                                             end;
                                                        end; //tevColega
                                                   else
                                                       Log.Error( iEmpleado, 'La relaci�n que se quiere asignar no es v�lida', 'Relaci�n inv�lida' );
                                                       Break;
                                                   end;
                                              end
                                              else
                                              begin
                                                   Log.Advertencia( iEmpleado, 'El l�mite m�ximo de evaluadores se excedi�' );
                                                   Break;
                                              end;
                                         end
                                         else
                                         begin
                                              Log.Advertencia( iEmpleado, 'El m�ximo de evaluadores para la relaci�n es 0' );
                                              Break;
                                         end;
                                    end
                                    else
                                        Log.Advertencia( iEmpleado, Format( 'Empleado %d no tiene un usuario asignado', [ iEmpleado ] ) );
                               end;
                               Next;
                          end;
                       finally
                              FreeAndNil( oRelaciones );
                       end;
                  end;
               finally
                      CreaMatrizEmpleadosEvaluarEnd;
               end;
          end;
          Result := CloseProcess;
     end;
end;


function TdmServerEvaluacion.ExportarEncuesta(Empresa: OleVariant; iEncuesta, iUsuario: Integer): WideString;
const
      Q_CRITERIOS = 'select * from ENC_COMP where ( ET_CODIGO = %d ) %s';
      Q_PREGUNTAS = 'select * from ENC_PREG where ( ET_CODIGO = %d ) and ( EC_ORDEN = %d )';
      Q_PERFILES = 'select * from ENC_RELA where ( ET_CODIGO = %d )';
var
  sSQL, sFiltro: String;
begin
     sSQL := Format( GetScript( eGetEncuesta ), [ ','+ K_CAMPOS_ENCUESTA_ADICIONALES , iEncuesta ] );
     with oZetaProvider do
     begin
           EmpresaActiva := Empresa;
           with FXMLTools do
           begin
                XMLInit( 'DATOS' );
                cdsDatos.Lista := OpenSQL( Empresa, sSQL, TRUE );
                XMLBuildDataRow( cdsDatos, 'ENCUESTA', True, False );
                {******************** Generales ********************}
                WriteStartElement('GENERALES');
                WriteAttributeString( 'CM_CODIGO', EmpresaActiva[ P_CODIGO ] );
                WriteAttributeString( 'US_CODIGO', InttoStr( iUsuario ) );
                WriteEndElement;//Generales
                {******************** Escalas ********************}
                WriteStartElement('ESCALAS');
                cdsDatos.Lista := OpenSQL( Empresa, Format( GetScript( eEscalaEnc ), [ iEncuesta, VACIO ] ), TRUE );
                cdsDatos.First;
                while not cdsDatos.EOF do
                begin
                     sFiltro := Format( ' and ( EE_CODIGO = ''%s'' )', [ cdsDatos.FieldByName('EE_CODIGO').AsString ] );
                     cdsSujetos.Lista := OpenSQL( Empresa, Format( GetScript( eEscalaEnc ), [ iEncuesta, sFiltro ] ), TRUE );
                     XMLBuildDataRow( cdsSujetos, 'ENC_ESCA', True, False );
                     cdsSujetos.Lista := OpenSQL( Empresa, Format( GetScript( eNivelesEscalaEnc ), [ iEncuesta, cdsDatos.FieldByName('EE_CODIGO').AsString ] ), TRUE );
                     XMLBuildDataRow( cdsSujetos, 'ENC_NIVE', True );
                     WriteEndElement;//Enc_Esca
                     cdsDatos.Next;
                end;
                WriteEndElement;//Escalas
                {******************** Criterios ********************}
                WriteStartElement('CRITERIOS');
                cdsDatos.Lista := OpenSQL( Empresa, Format( Q_CRITERIOS, [ iEncuesta, VACIO ] ), TRUE );
                while not cdsDatos.EOF do
                begin
                     sFiltro := Format( ' and EC_ORDEN = %d', [ cdsDatos.FieldByName('EC_ORDEN').AsInteger ]  );
                     cdsSujetos.Lista := OpenSQL( Empresa, Format( Q_CRITERIOS, [ iEncuesta, sFiltro ] ), TRUE );
                     XMLBuildDataRow( cdsSujetos, 'ENC_COMP', True, False );
                     cdsSujetos.Lista := OpenSQL( Empresa, Format( Q_PREGUNTAS, [ iEncuesta, cdsDatos.FieldbyName('EC_ORDEN').AsInteger ] ), TRUE );
                     XMLBuildDataRow( cdsSujetos, 'ENC_PREG', True );
                     WriteEndElement;//Enc_Comp
                     cdsDatos.Next;
                end;
                WriteEndElement;//Criterios
                {******************** Criterios ********************}
                WriteStartElement('PERFILES');
                cdsDatos.Lista := OpenSQL( Empresa, Format( Q_PERFILES, [ iEncuesta ] ), TRUE );
                XMLBuildDataRow( cdsDatos, 'ENC_RELA', True );
                WriteEndElement;//Perfiles
                WriteEndElement;//Encuesta
                XMLEnd;//Datos
                Result := XMLAsText;
           end;
     end; 
     SetComplete;
end;

procedure TdmServerEvaluacion.ImportarEncuesta(Empresa: OleVariant; const sArchivo: WideString);

    function GrabaEncuestaImportada( const oNodo: TZetaXMLNode ): Integer;
    const
         Q_AGREGA_ENCUESTA = ' insert into ENCUESTA( ET_CODIGO, ET_NOMBRE, ET_STATUS, ET_GET_COM, ET_ESC_PES, ET_ESC_DEF, ET_USR_ADM, ' + K_CAMPOS_ENCUESTA_ADICIONALES + ')'+
                    ' values( :ET_CODIGO, :ET_NOMBRE, :ET_STATUS, :ET_GET_COM, :ET_ESC_PES, :ET_ESC_DEF, :ET_USR_ADM, '+
                    ' :ET_DESCRIP, :ET_FEC_INI, :ET_FEC_FIN, :ET_NIVELES, :ET_PESOS, :ET_TOT_PES, :ET_NUM_COM, :ET_NUM_SUJ, '+
                    ' :ET_FIN_SUJ, :ET_NUM_PRE, :ET_ESCALA, :ET_MSG_INI, :ET_MSG_FIN, :ET_TXT_COM, :ET_NUM_EVA, :ET_FIN_EVA, :ET_ESTILO, :ET_DOCUMEN, :ET_ESC_CAL, :ET_PTS_CAL, :ET_CONSOLA )';
    var
       i: Integer;
       sNombreAtributo, sValor: String;
    begin
         with oZetaProvider do
         begin
               Result := GetMaxEncuesta;
               FEncuesta := CreateQuery( Q_AGREGA_ENCUESTA );
               try
                  for i := 0 to ( oNodo.AttributeNodes.Count - 1 ) do
                  begin
                       with oNodo.AttributeNodes[ i ] do
                       begin
                            sNombreAtributo := LocalName;
                            sValor := Text;
                       end;
                       if ( sNombreAtributo = 'ET_CODIGO' ) then
                          ParamAsInteger( FEncuesta, 'ET_CODIGO', Result );
                       if ( sNombreAtributo = 'ET_STATUS') or
                          ( sNombreAtributo = 'ET_NIVELES' ) or
                          ( sNombreAtributo = 'ET_PESOS' ) or
                          ( sNombreAtributo = 'ET_NUM_COM' ) or
                          ( sNombreAtributo = 'ET_FIN_SUJ' ) or
                          ( sNombreAtributo = 'ET_NUM_PRE' ) or
                          ( sNombreAtributo = 'ET_USR_ADM' ) or
                          ( sNombreAtributo = 'ET_ESTILO' ) or
                          ( sNombreAtributo = 'ET_FIN_EVA' ) or
                          ( sNombreAtributo = 'ET_PTS_CAL' ) then
                       begin
                            ParamAsInteger( FEncuesta, sNombreAtributo, StrToInt( sValor ) );
                       end;
                       if ( sNombreAtributo = 'ET_NUM_SUJ' ) or ( sNombreAtributo = 'ET_NUM_EVA') then
                       begin
                            ParamAsInteger( FEncuesta, sNombreAtributo, 0 );
                       end;
                       if ( sNombreAtributo = 'ET_NOMBRE' ) or
                          ( sNombreAtributo = 'ET_DESCRIP' ) or
                          ( sNombreAtributo = 'ET_ESCALA' ) or
                          ( sNombreAtributo = 'ET_TXT_COM' ) or
                          ( sNombreAtributo = 'ET_ESC_PES' ) or
                          ( sNombreAtributo = 'ET_ESC_DEF' ) or
                          ( sNombreAtributo = 'ET_DOCUMEN' ) or
                          ( sNombreAtributo = 'ET_ESC_CAL' ) then
                       begin
                            ParamAsString( FEncuesta, sNombreAtributo, sValor );
                       end;
                       if ( sNombreAtributo = 'ET_FEC_INI' ) or ( sNombreAtributo = 'ET_FEC_FIN' ) then
                       begin
                            ParamAsDate( FEncuesta, sNombreAtributo, Date );
                       end;
                       if ( sNombreAtributo = 'ET_TOT_PES' ) then
                       begin
                            if StrVacio( sValor ) then
                               sValor := '0';
                            ParamAsFloat( FEncuesta, sNombreAtributo, StrToFloat( sValor ) );
                       end;
                       if ( sNombreAtributo = 'ET_MSG_INI' ) or ( sNombreAtributo = 'ET_MSG_FIN' ) then
                       begin
                            ParamAsBlob( FEncuesta, sNombreAtributo, sValor );
                       end;
                       if ( sNombreAtributo = 'ET_GET_COM' ) or ( sNombreAtributo = 'ET_CONSOLA' ) then
                       begin
                            ParamAsBoolean( FEncuesta, sNombreAtributo, zStrToBool( sValor ) );
                       end;
                  end;
                  EmpiezaTransaccion;
                  try
                     Ejecuta( FEncuesta );
                     TerminaTransaccion( True );
                  except
                        on Error: Exception do
                        begin
                             RollBackTransaccion;
                             raise;
                        end;
                  end;
               finally
                      FreeAndNil( FEncuesta );
               end;
         end;
    end;

    procedure GrabaNivelesEscala( const oNodoHijo: TZetaXMLNode; const iEncuesta: Integer );
    const
         Q_AGREGA_NIVELES = ' insert into ENC_NIVE( ET_CODIGO, EE_CODIGO, EL_ORDEN, EL_NOMBRE, EL_CORTO, EL_DESCRIP, EL_VALOR ) '+
                              'values( :ET_CODIGO, :EE_CODIGO, :EL_ORDEN, :EL_NOMBRE, :EL_CORTO, :EL_DESCRIP, :EL_VALOR )';
    var
       i, j: Integer;
       oNodoNiveles: TZetaXMLNode;
       sNombreAtributo, sValor: String;
       FNiveles: TZetaCursor;
    begin
         with oZetaProvider do
         begin
              FNiveles := CreateQuery( Q_AGREGA_NIVELES );
              try
                  for i:= 0 to ( oNodoHijo.ChildNodes.Count - 1 ) do
                  begin
                       oNodoNiveles := oNodoHijo.ChildNodes[ i ];
                       for j := 0 to ( oNodoNiveles.AttributeNodes.Count - 1 ) do
                       begin
                            with oNodoNiveles.AttributeNodes[ j ] do
                            begin
                                 sNombreAtributo := LocalName;
                                 sValor := Text;
                            end;
                            if ( sNombreAtributo = 'ET_CODIGO' ) then
                            begin
                                 ParamAsInteger( FNiveles, 'ET_CODIGO', iEncuesta );
                            end;
                            if ( sNombreAtributo = 'EL_ORDEN') then
                            begin
                                 ParamAsInteger( FNiveles, sNombreAtributo, StrToInt( sValor ) );
                            end;
                            if ( ( sNombreAtributo = 'EL_DESCRIP' ) or
                                 ( sNombreAtributo = 'EE_CODIGO' ) or
                                 ( sNombreAtributo = 'EL_NOMBRE' ) or
                                 ( sNombreAtributo = 'EL_CORTO' ) ) then
                            begin
                                 ParamAsString( FNiveles, sNombreAtributo, sValor );
                            end;
                            if ( sNombreAtributo = 'EL_VALOR'  )then
                            begin
                                 if StrVacio( sValor ) then
                                    sValor := '0';
                                 ParamAsFloat( FNiveles, sNombreAtributo, StrToFloat( sValor ) );
                            end;
                       end;
                       EmpiezaTransaccion;
                       try
                          Ejecuta( FNiveles );
                          TerminaTransaccion( True );
                       except
                             on Error: Exception do
                             begin
                                  RollBackTransaccion;
                                  raise;
                             end;
                       end;
                   end;
              finally
                     FreeAndNil( FNiveles );
              end;
         end;
    end;

    procedure GrabaEscalas( const oNodo: TZetaXMLNode; const iEncuesta: Integer );
    const
         Q_AGREGA_ESCALAS = 'insert into ENC_ESCA( ET_CODIGO, EE_CODIGO, EE_NOMBRE, EE_NIVELES ) '+
                            'values( :ET_CODIGO, :EE_CODIGO, :EE_NOMBRE, :EE_NIVELES )';
    var
       i, j: Integer;
       oNodoHijo: TZetaXMLNode;
       sNombreAtributo, sValor: String;
    begin
         with oZetaProvider do
         begin
              FEscalas := CreateQuery( Q_AGREGA_ESCALAS );
              try
                  for i := 0 to ( oNodo.ChildNodes.Count - 1 ) do
                  begin
                       oNodoHijo := oNodo.ChildNodes[ i ];
                       for j := 0 to ( oNodoHijo.AttributeNodes.Count - 1 ) do
                       begin
                            with oNodoHijo.AttributeNodes[ j ] do
                            begin
                                 sNombreAtributo := LocalName;
                                 sValor := Text;
                            end;
                            if ( sNombreAtributo = 'ET_CODIGO' ) then
                            begin
                                 ParamAsInteger( FEscalas, 'ET_CODIGO', iEncuesta );
                            end;
                            if ( sNombreAtributo = 'EE_NIVELES' ) then
                            begin
                                 ParamAsInteger( FEscalas, sNombreAtributo, StrToInt( sValor ) );
                            end;
                            if ( ( sNombreAtributo = 'EE_CODIGO' ) or
                                 ( sNombreAtributo = 'EE_NOMBRE' ) or
                                 ( sNombreAtributo = 'EE_NA_NOMB' ) or
                                 ( sNombreAtributo = 'EE_NA_CORT' ) ) then
                            begin
                                 ParamAsString( FEscalas, sNombreAtributo, sValor );
                            end;
                       end;
                       EmpiezaTransaccion;
                       try
                          Ejecuta( FEscalas );
                          TerminaTransaccion( True );
                       except
                             on Error: Exception do
                             begin
                                  RollBackTransaccion;
                                  raise;
                             end;
                       end;
                       GrabaNivelesEscala( oNodoHijo, iEncuesta );
                  end;
              finally
                     FreeAndNil( FEscalas );
              end;
         end;
    end;

    procedure GrabaPreguntas( const oNodoHijo: TZetaXMLNode; const iEncuesta: Integer );
    const
         Q_AGREGA_PREGUNTAS = 'insert into ENC_PREG( ET_CODIGO, EC_ORDEN, EP_ORDEN, EE_CODIGO, EP_NUMERO, PG_FOLIO, '+
                              'EP_DESCRIP, EP_TIPO, EP_PESO, EP_GET_COM, EP_GET_NA, EP_POS_PES ) ' +
                              'values( :ET_CODIGO, :EC_ORDEN, :EP_ORDEN, :EE_CODIGO, :EP_NUMERO, :PG_FOLIO, '+
                              ':EP_DESCRIP, :EP_TIPO, :EP_PESO, :EP_GET_COM, :EP_GET_NA, :EP_POS_PES )';
    var
       i, j: Integer;
       oNodoPregunta: TZetaXMLNode;
       sNombreAtributo, sValor: String;
    begin
         with oZetaProvider do
         begin
              FPregunta := CreateQuery( Q_AGREGA_PREGUNTAS );
              try
                  for i := 0 to ( oNodoHijo.ChildNodes.Count - 1 ) do
                  begin
                       oNodoPregunta := oNodoHijo.ChildNodes[ i ];
                       for j := 0 to ( oNodoPregunta.AttributeNodes.Count - 1 ) do
                       begin
                            with oNodoPregunta.AttributeNodes[ j ] do
                            begin
                                 sNombreAtributo := LocalName;
                                 sValor := Text;
                            end;
                            if ( sNombreAtributo = 'ET_CODIGO' ) then
                            begin
                                 ParamAsInteger( FPregunta, 'ET_CODIGO', iEncuesta );
                            end;
                            if ( ( sNombreAtributo = 'EC_ORDEN') or
                                 ( sNombreAtributo = 'EP_ORDEN' ) or
                                 ( sNombreAtributo = 'EP_TIPO' ) or
                                 ( sNombreAtributo = 'EP_GET_NA' ) or
                                 ( sNombreAtributo = 'EP_POS_PES' ) or
                                 ( sNombreAtributo = 'EP_NUMERO' ) or
                                 ( sNombreAtributo = 'PG_FOLIO' ) or
                                 ( sNombreAtributo = 'EP_GET_COM' ) ) then
                            begin
                                 ParamAsInteger( FPregunta, sNombreAtributo, StrToInt( sValor ) );
                            end;
                            if ( ( sNombreAtributo = 'EP_DESCRIP' ) or
                                 ( sNombreAtributo = 'EE_CODIGO' ) ) then
                            begin
                                 ParamAsString( FPregunta, sNombreAtributo, sValor );
                            end;
                            if ( sNombreAtributo = 'EP_PESO' ) then
                            begin
                                 if StrVacio( sValor ) then
                                    sValor := '0';
                                 ParamAsFloat( FPregunta, sNombreAtributo, StrToFloat( sValor ) );
                            end;
                       end;
                       EmpiezaTransaccion;
                       try
                          Ejecuta( FPregunta );
                          TerminaTransaccion( True );
                       except
                             on Error: Exception do
                             begin
                                  RollBackTransaccion;
                                  raise;
                             end;
                       end;
                   end;
              finally
                     FreeAndNil( FPregunta );
              end;
         end;
    end;

    procedure GrabaCriterios( const oNodo: TZetaXMLNode; const iEncuesta: Integer );
    const
         Q_AGREGA_CRITERIOS = 'insert into ENC_COMP( ET_CODIGO, EC_ORDEN, CM_CODIGO, EC_NOMBRE, EC_DETALLE, EC_PESO, EC_TOT_PES, EC_NUM_PRE, EC_GET_COM, EC_POS_PES ) '+
                    'values( :ET_CODIGO, :EC_ORDEN, :CM_CODIGO, :EC_NOMBRE, :EC_DETALLE, :EC_PESO, :EC_TOT_PES, :EC_NUM_PRE, :EC_GET_COM, :EC_POS_PES )';
    var
       i, j: Integer;
       oNodoHijo: TZetaXMLNode;
       sNombreAtributo, sValor: String;
    begin
         with oZetaProvider do
         begin
              FCriterios := CreateQuery( Q_AGREGA_CRITERIOS );
              try
                  for i := 0 to ( oNodo.ChildNodes.Count - 1 ) do
                  begin
                       oNodoHijo := oNodo.ChildNodes[ i ];
                       for j := 0 to ( oNodoHijo.AttributeNodes.Count - 1 ) do
                       begin
                            with oNodoHijo.AttributeNodes[ j ] do
                            begin
                                 sNombreAtributo := LocalName;
                                 sValor := Text;
                            end;
                            if ( sNombreAtributo = 'ET_CODIGO' ) then
                            begin
                                 ParamAsInteger( FCriterios, 'ET_CODIGO', iEncuesta );
                            end;
                            if ( ( sNombreAtributo = 'EC_ORDEN') or
                                 ( sNombreAtributo = 'EC_NUM_PRE' ) or
                                 ( sNombreAtributo = 'EC_GET_COM' ) or
                                 ( sNombreAtributo = 'EC_POS_PES' ) ) then
                            begin
                                 ParamAsInteger( FCriterios, sNombreAtributo, StrToInt( sValor ) );
                            end;
                            if ( ( sNombreAtributo = 'CM_CODIGO' ) or
                                 ( sNombreAtributo = 'EC_NOMBRE' ) ) then
                            begin
                                 ParamAsString( FCriterios, sNombreAtributo, sValor );
                            end;
                            if ( ( sNombreAtributo = 'EC_TOT_PES' ) or
                                 ( sNombreAtributo = 'EC_PESO' ) ) then
                            begin
                                 if StrVacio( sValor ) then
                                    sValor := '0';
                                 ParamAsFloat( FCriterios, sNombreAtributo, StrToFloat( sValor ) );
                            end;
                            if ( sNombreAtributo = 'EC_DETALLE' ) then
                            begin
                                 ParamAsBlob( FCriterios, sNombreAtributo, sValor );
                            end;
                       end;
                       EmpiezaTransaccion;
                       try
                          Ejecuta( FCriterios );
                          TerminaTransaccion( True );
                       except
                             on Error: Exception do
                             begin
                                  RollBackTransaccion;
                                  raise;
                             end;
                       end;
                       GrabaPreguntas( oNodoHijo, iEncuesta );
                  end;
              finally
                     FreeAndNil( FCriterios );
              end;
         end;
    end;

    procedure GrabaPerfiles( const oNodo: TZetaXMLNode; const iEncuesta: Integer );
    const
         Q_AGREGA_PERFILES = 'insert into ENC_RELA( ET_CODIGO, ER_TIPO, ER_NOMBRE, ER_NUM_MIN, ER_NUM_MAX, ER_PESO, ER_POS_PES ) '+
                             'values( :ET_CODIGO, :ER_TIPO, :ER_NOMBRE, :ER_NUM_MIN, :ER_NUM_MAX, :ER_PESO, :ER_POS_PES )';
    var
       i, j: Integer;
       oNodoHijo: TZetaXMLNode;
       sNombreAtributo, sValor: String;
    begin
         with oZetaProvider do
         begin
              FEvaluador := CreateQuery( Q_AGREGA_PERFILES );
              try
                  for i := 0 to ( oNodo.ChildNodes.Count - 1 ) do
                  begin
                       oNodoHijo := oNodo.ChildNodes[ i ];
                       for j := 0 to ( oNodoHijo.AttributeNodes.Count - 1 ) do
                       begin
                            with oNodoHijo.AttributeNodes[ j ] do
                            begin
                                 sNombreAtributo := LocalName;
                                 sValor := Text;
                            end;
                            if ( sNombreAtributo = 'ET_CODIGO' ) then
                            begin
                                 ParamAsInteger( FEvaluador, 'ET_CODIGO', iEncuesta );
                            end;
                            if ( ( sNombreAtributo = 'ER_TIPO') or
                                 ( sNombreAtributo = 'ER_NUM_MIN' ) or
                                 ( sNombreAtributo = 'ER_NUM_MAX' ) or
                                 ( sNombreAtributo = 'ER_POS_PES' ) ) then
                            begin
                                 ParamAsInteger( FEvaluador, sNombreAtributo, StrToInt( sValor ) );
                            end;
                            if ( sNombreAtributo = 'ER_NOMBRE' ) then
                            begin
                                 ParamAsString( FEvaluador, sNombreAtributo, sValor );
                            end;
                            if ( sNombreAtributo = 'ER_PESO' ) then
                            begin
                                 if StrVacio( sValor ) then
                                    sValor := '0';
                                 ParamAsFloat( FEvaluador, sNombreAtributo, StrToFloat( sValor ) );
                            end;
                       end;
                       EmpiezaTransaccion;
                       try
                          Ejecuta( FEvaluador );
                          TerminaTransaccion( True );
                       except
                             on Error: Exception do
                             begin
                                  RollBackTransaccion;
                                  raise;
                             end;
                       end;
                  end;
              finally
                     FreeAndNil( FEvaluador );
              end;
         end;
    end;

var
   iEncuesta: Integer;
   oNodo: TZetaXMLNode;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          with FXMLTools do
          begin
               XMLAsText := sArchivo;
               try
                  oNodo := GetNode( 'ENCUESTA' );
                  EmpiezaTransaccion;
                  try
                      iEncuesta := GrabaEncuestaImportada( oNodo );
                      oNodo := GetNode( 'ESCALAS', oNodo );
                      GrabaEscalas( oNodo, iEncuesta );
                      oNodo := oNodo.ParentNode;
                      oNodo := GetNode( 'CRITERIOS', oNodo );
                      GrabaCriterios( oNodo, iEncuesta );
                      oNodo := oNodo.ParentNode;
                      oNodo := GetNode( 'PERFILES', oNodo );
                      GrabaPerfiles( oNodo, iEncuesta );
                      TerminaTransaccion( True );
                  except
                        on Error: Exception do
                        begin
                             RollBackTransaccion;
                             raise;
                        end;
                  end;
               finally
                     FXMLTools.XMLDocument.Active := False;
               end;
          end;
     end;
     SetComplete;
end;

procedure TdmServerEvaluacion.GetParametrosURL( Empresa_: OleVariant; const sEmpresa: WideString; Usuario: Integer;
                                                out RutaVirtual: WideString; out Empleado: Integer; out Nombre: WideString);
begin
     Buzon.GetParametrosURL( sEmpresa, Usuario, Empleado, Nombre );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa_;
          InitGlobales;
          RutaVirtual := GetGlobalString( K_GLOBAL_RUTA_VIRTUAL );
     end;
end;

procedure TdmServerEvaluacion.CreaEncuestaSimpleParametros;
{$ifdef FALSE}
const
     K_TODOS = 0;
     K_RANGO = 1;
     K_LISTA = 2;
var
   iTipoEvaluadores: Integer;
   sSeleccEvaluadores: String;
{$endif}
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          {$ifdef FALSE}
          iTipoEvaluadores := ParamByName('Evaluadores').AsInteger;
          case( iTipoEvaluadores ) of
              K_TODOS: sSeleccEvaluadores := 'Todos';
              K_RANGO: sSeleccEvaluadores := 'Del: ' + InttoStr( ParamByName('RANGO_INI').AsInteger ) +
                                            ' al: ' + InttoStr( ParamByName('RANGO_FIN').AsInteger );
              K_LISTA: sSeleccEvaluadores := ParamByName('EVAL_LISTA').AsString;
          else
              sSeleccEvaluadores := VACIO;
          end;
          {$endif}
          FListaParametros := 'Encuesta: ' + ParamByName('ET_NOMBRE').AsString + K_PIPE +
                              'Fecha Inicial: ' + FechaCorta( ParamByName('ET_FEC_INI').AsDateTime) + K_PIPE +
                              'Fecha Final: ' + FechaCorta(  ParamByName('ET_FEC_FIN').AsDatetime ) + K_PIPE +
                              'No. Preguntas: ' + InttoStr( ParamByName('ET_NUM_PRE').AsInteger ) + K_PIPE +
                              'Administrador: ' + InttoStr( ParamByName('ET_USR_ADM').AsInteger ) + K_PIPE{$ifdef FALSE} +
                              'Evaluadores: ' + sSeleccEvaluadores{$endif};
     end;
end;

procedure TdmServerEvaluacion.CreaEncuestaSimpleBuildDataset;
begin
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', TRUE, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'PRETTYNAME' );
               AgregaColumna( '0', TRUE, Entidad, tgNumero, 0, 'US_CODIGO' );
               AgregaFiltro( Format( '( CB_ACTIVO = %s )', [ EntreComillas('S') ] ), True, Entidad );
               AgregaOrden( 'CB_CODIGO', TRUE, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

function TdmServerEvaluacion.CreaEncuestaSimpleDataset( Dataset: TDataset ): OleVariant;

    procedure AgregaEncuesta( const oParametros: TZetaParams; const iEncuesta: Integer );
    begin
         with oZetaProvider do
         begin
              with oParametros do
              begin
                   FEncuesta := CreateQuery( GetScript( eCreaEncuestaSimple ) );
                   try
                      ParamAsInteger( FEncuesta, 'ET_CODIGO', iEncuesta );
                      ParamAsString( FEncuesta, 'ET_NOMBRE', ParamByName( 'ET_NOMBRE' ).AsString );
                      ParamAsString( FEncuesta, 'ET_DESCRIP', ParamByName( 'ET_DESCRIP' ).AsString );
                      ParamAsDate( FEncuesta, 'ET_FEC_INI', ParamByName( 'ET_FEC_INI' ).AsDate );
                      ParamAsDate( FEncuesta, 'ET_FEC_FIN', ParamByName( 'ET_FEC_FIN' ).AsDate );
                      ParamAsInteger( FEncuesta, 'ET_USR_ADM', ParamByName( 'ET_USR_ADM' ).AsInteger );
                      ParamAsString( FEncuesta, 'ET_ESCALA', 'Escala #1' );
                      ParamAsString( FEncuesta, 'ET_ESC_DEF', 'ESC1' );
                      ParamAsInteger( FEncuesta, 'ET_STATUS', Ord( stenDiseno ) );
                      ParamAsInteger( FEncuesta, 'ET_NUM_PRE', ParamByName( 'ET_NUM_PRE' ).AsInteger );
                      ParamAsString( FEncuesta, 'ET_MSG_INI', ParamByName( 'ET_MSG_INI' ).AsString );
                      ParamAsString( FEncuesta, 'ET_MSG_FIN', ParamByName( 'ET_MSG_FIN' ).AsString );
                      ParamAsString( FEncuesta, 'ET_DOCUMEN', ParamByName( 'ET_DOCUMEN' ).AsString );
                      ParamAsInteger( FEncuesta, 'ET_PTS_CAL', Ord( eccaNoUsar ) );
                      ParamAsString( FEncuesta, 'ET_ESC_CAL', VACIO );
                      ParamAsString( FEncuesta, 'ET_CONSOLA', K_GLOBAL_SI );
                      EmpiezaTransaccion;
                      try
                         Ejecuta( FEncuesta );
                         TerminaTransaccion( True );
                      except
                            on Error: Exception do
                            begin
                                 RollBackTransaccion;
                                 Log.Excepcion( 0, 'Error al crear encuesta simple', Error );
                            end;
                      end;
                   finally
                          FreeAndNil( FEncuesta );
                   end;
              end;
         end;
    end;

    function CreaEscalas( const oParametros: TZetaParams; const iEncuesta: Integer; const oEscalas: TStrings ): TStrings;
    var
       i, j, iNiveles, iLineas: Integer;
       oRespuestas: TStrings;
       sCodEscala, sNombre, sNombreCorto: String;
       FNivelesEscala: TZetaCursor;
    begin
         with oZetaProvider do
         begin
              with oParametros do
              begin
                   i := 0;
                   j := 1;
                   FEscalas := CreateQuery( GetScript( eAgregaEncEsca ) );
                   FNivelesEscala := CreateQuery( GetScript( eAgregaNivelesEscala ) );
                   oRespuestas := TStringList.Create;
                   try
                       oEscalas.Clear;
                       oRespuestas.Clear;
                       while ( i < Count ) do
                       begin
                            if ( Pos( 'Respuestas', Items[ i ].Name ) > 0 ) then
                            begin
                                 oRespuestas.Text := Items[ i ].AsMemo;
                                 iNiveles := oRespuestas.Count;
                                 if ( iNiveles > 0 ) then
                                 begin
                                      sCodEscala := Format( 'ESC%d', [ j ] );
                                      oEscalas.Add( sCodEscala );
                                      { ***********  Agregar Escala ( ENC_ESCA ) ************** }
                                      ParamAsInteger( FEscalas, 'ET_CODIGO', iEncuesta );
                                      ParamAsString( FEscalas, 'EE_CODIGO', sCodEscala );
                                      ParamAsString( FEscalas, 'EE_NOMBRE', Format( 'Escala #%d', [ j ] ) );
                                      ParamAsInteger( FEscalas, 'EE_NIVELES', iNiveles );
                                      EmpiezaTransaccion;
                                      try
                                         Ejecuta( FEscalas );
                                         TerminaTransaccion( True );
                                      except
                                            on Error: Exception do
                                            begin
                                                 RollBackTransaccion;
                                                 Log.Excepcion( 0, 'Error al agregar escalas de encuesta', Error );
                                            end;
                                      end;
                                      { ********** Agrega Niveles a la escala ( ENC_NIVE )************** }
                                      iLineas := 0;
                                      while ( iLineas < iNiveles ) do
                                      begin
                                           sNombre := Copy( oRespuestas.Strings[ iLineas ], 1, K_ANCHO_DESCRIPCION );
                                           sNombreCorto := Copy( oRespuestas.Strings[ iLineas ], 1, K_ANCHO_REFERENCIA );
                                           ParamAsInteger( FNivelesEscala, 'ET_CODIGO', iEncuesta );
                                           ParamAsString( FNivelesEscala, 'EE_CODIGO', sCodEscala );
                                           ParamAsInteger( FNivelesEscala, 'EL_ORDEN', ( iLineas + 1 ) );
                                           ParamAsString( FNivelesEscala, 'EL_NOMBRE', sNombre );
                                           ParamAsString( FNivelesEscala, 'EL_CORTO', sNombreCorto );
                                           ParamAsString( FNivelesEscala, 'EL_DESCRIP', sNombre );
                                           ParamAsInteger( FNivelesEscala, 'EL_VALOR', ( iLineas + 1 ) );
                                           EmpiezaTransaccion;
                                           try
                                              Ejecuta( FNivelesEscala );
                                              TerminaTransaccion( True );
                                           except
                                                 on Error: Exception do
                                                 begin
                                                      RollBackTransaccion;
                                                      Log.Excepcion( 0, 'Error al agregar niveles de escala', Error );
                                                 end;
                                           end;
                                           iLineas := iLineas + 1 ;
                                      end;
                                      j := j + 1;
                                 end;
                            end;
                            i := i + 1 ;
                       end;
                       Result := oEscalas;
                   finally
                          FreeAndNil( oRespuestas );
                          FreeAndNil( FNivelesEscala );
                          FreeAndNil( FEscalas );
                   end;
              end;
         end;
    end;

    procedure CreaPreguntas( const oParametros: TZetaParams; const iEncuesta: Integer; const ListaEscalas: TStrings );
    var
       i, j: Integer;
       oEscalas: TStrings;
       sTextoPregunta: String;
    begin
         oEscalas := ListaEscalas;
         with oZetaProvider do
         begin
              with oParametros do
              begin
                   i := 0;
                   j := 0;
                   FCriterios := CreateQuery( GetScript( eAgregaCriterio ) );
                   FPregunta := CreateQuery( GetScript( eAgregaPreguntas ) );
                   try
                      { ***********  Agregar criterio ( ENC_COMP ) ************** }
                      ParamAsInteger( FCriterios, 'ET_CODIGO', iEncuesta );
                      ParamAsInteger( FCriterios, 'EC_ORDEN', 1 );
                      ParamAsString( FCriterios, 'CM_CODIGO', VACIO );
                      ParamAsInteger( FCriterios, 'EC_POS_PES', 1 );
                      ParamAsString( FCriterios, 'EC_NOMBRE', 'Preguntas - encuesta' );
                      EmpiezaTransaccion;
                      try
                         Ejecuta( FCriterios );
                         TerminaTransaccion( True );
                      except
                            on Error: Exception do
                            begin
                                 RollBackTransaccion;
                                 Log.Excepcion( 0, 'Error al agregar criterios a la encuesta', Error );
                            end;
                      end;
                      while ( i < Count ) do
                      begin
                           if ( Pos( 'EP_DESCRIP', Items[ i ].Name ) > 0 ) then
                           begin
                                { ********** Agrega preguntas a la encuesta ( ENC_PREG )************** }
                                sTextoPregunta := Items[ i ].AsMemo;
                                if StrLleno( sTextoPregunta ) then
                                begin
                                     ParamAsInteger( FPregunta, 'ET_CODIGO', iEncuesta );
                                     ParamAsInteger( FPregunta, 'EC_ORDEN', 1 );
                                     ParamAsInteger( FPregunta, 'EP_NUMERO', ( j + 1 ) );
                                     ParamAsInteger( FPregunta, 'EP_ORDEN', ( j + 1 ) );
                                     if ( ( Assigned( oEscalas ) ) and ( oEscalas.Count > 0 ) ) then
                                        ParamAsString( FPregunta, 'EE_CODIGO', oEscalas[ j ] );
                                     ParamAsString( FPregunta, 'EP_DESCRIP', sTextoPregunta );
                                     ParamAsInteger( FPregunta, 'EP_POS_PES', 1 );
                                     EmpiezaTransaccion;
                                     try
                                        Ejecuta( FPregunta );
                                        TerminaTransaccion( True );
                                     except
                                           on Error: Exception do
                                           begin
                                               RollBackTransaccion;
                                               Log.Excepcion( 0, 'Error al agregar preguntas a la encuesta', Error );
                                           end;
                                     end;
                                     j := j + 1;
                                end;
                           end;
                           i := i + 1 ;
                      end;
                   finally
                          FreeAndNil( FPregunta );
                          FreeAndNil( FCriterios );
                   end;
              end;
         end;
    end;

    procedure CreaRelacion( const iEncuesta: Integer );
    const
         K_AUTOEVALUACION = 0;
         K_DESC_AUTOEVAL = 'Autoevaluaci�n';
    begin
         with oZetaProvider do
         begin
              FEvaluador := CreateQuery( GetScript( eAgregaEvaluador ) );
              try
                 ParamAsInteger( FEvaluador, 'Encuesta', iEncuesta );
                 ParamAsInteger( FEvaluador, 'Orden', 1 );
                 ParamAsInteger( FEvaluador, 'Tipo', K_AUTOEVALUACION );
                 ParamAsChar( FEvaluador, 'DescTipo', K_DESC_AUTOEVAL, K_ANCHO_DESCRIPCION );
                 EmpiezaTransaccion;
                 try
                    Ejecuta( FEvaluador );
                    TerminaTransaccion( True );
                 except
                       on Error: Exception do
                       begin
                            RollBackTransaccion;
                            raise;
                       end;
                 end;
              finally
                     FreeAndNil( FEvaluador );
              end;
         end;
    end;

    function EstaEnListaEmpleados( const iEmpleado, iUsuario: Integer ): Boolean;
    begin
         Result := False;
         with Dataset do
         begin
              if ( iEmpleado > 0 ) and Locate( 'CB_CODIGO', iEmpleado, [] ) then
              begin
                   Edit;
                   FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                   Post;
                   Result := True;
              end;
         end;
    end;

    procedure CreaMatrizEvaluaciones( const oParametros: TZetaParams; const iEncuesta: Integer );
    const
         K_TODOS = 0;
         K_RANGO = 1;
         K_LISTA = 2;
    var
       lReportaError: Boolean;
       iAdministrador, iEvaluado: Integer;
    begin
         iAdministrador := 0;
         cdsListaUsuarios.Lista := Buzon.ListaUsuarios;
         with oZetaProvider do
         begin
              with oParametros do
              begin
                   if ( cdsListaUsuarios.Locate( 'US_CODIGO', ParamByName( 'ET_USR_ADM' ).AsInteger, [] ) ) then
                      iAdministrador := cdsListaUsuarios.FieldByName( 'CB_CODIGO' ).AsInteger;
                   { Borrar usuarios que no aparecen en lista de empleados }
                   with cdsListaUsuarios do
                   begin
                        First;
                        while not Eof do
                        begin
                             if EstaEnListaEmpleados( FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'US_CODIGO' ).AsInteger ) then
                                Next
                             else
                                 Delete;
                        end;
                   end;
                   {
                   Reportar en bit�cora a empleados quienes
                   no tienen asignado un usuario
                   }
                   with Dataset do
                   begin
                        First;
                        while not Eof do
                        begin
                             if ( FieldByName( 'US_CODIGO' ).AsInteger <= 0 ) then
                                Log.Evento( clbNinguno, FieldByName( 'CB_CODIGO' ).AsInteger, Now, 'Empleado No Es Usuario' );
                             Next;
                        end;
                   end;
                   {$ifdef FALSE}
                   case( ParamByName('Evaluadores').AsInteger ) of
                       K_TODOS: begin
                                     cdsListaUsuarios.Filtered := False;
                                end;
                        K_RANGO: begin
                                     with cdsListaUsuarios do
                                     begin
                                          Filtered := False;
                                           Filter := Format( 'US_CODIGO >= %d and US_CODIGO <= %d', [ ParamByName('RANGO_INI').AsInteger,
                                                                                                                 ParamByName('RANGO_FIN').AsInteger ] );
                                          Filtered := True;
                                     end;
                                end;
                       K_LISTA: begin
                                     with cdsListaUsuarios do
                                     begin
                                          Filtered := False;
                                          Filter := Format( 'US_CODIGO in ( %s )', [ ParamByName('EVAL_LISTA').AsString ] );
                                          Filtered := True;
                                     end;
                                end;
                   else
                       with cdsListaUsuarios do
                       begin
                             Filtered := False;
                            Filter := 'US_CODIGO = -1';
                            Filtered := True;
                       end;
                   end;
                   {$endif}
                   CreaMatrizEmpleadosEvaluarBegin;
                   try
                      with cdsListaUsuarios do
                      begin
                           First;
                           while not EOF do
                           begin
                                lReportaError := False;
                                iEvaluado := FieldbyName( 'CB_CODIGO' ).AsInteger;
                                if ( iEvaluado = 0 ) then
                                begin
                                     if ( iAdministrador <> 0 ) then
                                         iEvaluado := iAdministrador
                                     else
                                         lReportaError := True;
                                end;
                                if lReportaError then
                                    Log.Error( 0, 'Empleado no encontrado', 'El evaluado no tiene un empleado asignado � el administrador de la encuesta no tiene un empleado asignado' )
                                else
                                    CreaMatrizEmpleadosEvaluar( iEncuesta,
                                                                FieldByName( 'US_CODIGO' ).AsInteger,
                                                                iEvaluado,
                                                                tevMismo,
                                                                False );
                                Next;
                           end;
                      end;
                   finally
                          CreaMatrizEmpleadosEvaluarEnd;
                   end;
              end;
         end;
    end;

    procedure IniciaPrepararEvaluaciones( const oParametros: TZetaParams; const iEncuesta: Integer );
    begin
         with oZetaProvider do
         begin
              with oParametros do
              begin
                   FEvaluacion := CreateQuery( GetScript( ePrepararEvaluaciones ) );
                   try
                      ParamAsInteger( FEvaluacion, 'Encuesta', iEncuesta );
                      EmpiezaTransaccion;
                      try
                         Ejecuta( FEvaluacion );
                         TerminaTransaccion(TRUE);
                      except
                            on Error: Exception do
                            begin
                                 RollBackTransaccion;
                                 Log.Excepcion( 0, 'Error al preparar evaluaciones', Error );
                            end;
                      end;
                   finally
                          FreeAndNil( FEvaluacion );
                   end;
              end;
         end;
    end;

var
   iEncuesta: Integer;
   oEscalas: TStrings;
begin
     with oZetaProvider do
     begin
          iEncuesta := GetMaxEncuesta;
          if OpenProcess( prDECreaEncuestaSimple, Dataset.RecordCount, FListaParametros ) then
          begin
               oEscalas := TStringList.Create;
               try
                  AgregaEncuesta( ParamList, iEncuesta );
                  CreaEscalas( ParamList, iEncuesta, oEscalas );
                  CreaPreguntas( ParamList, iEncuesta, oEscalas );
                  CreaRelacion( iEncuesta );
                  CreaMatrizEvaluaciones( ParamList, iEncuesta );
                  IniciaPrepararEvaluaciones( ParamList, iEncuesta  );
                  ContadoresEncuesta( EmpresaActiva, iEncuesta );
                  //GA: Creo que no se necesita..
                  //TerminaTransaccion( True );
               finally
                      FreeAndNil( oEscalas );
               end;
               Result := CloseProcess;
          end;
     end;
end;

function TdmServerEvaluacion.CreaEncuestaSimple( Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     CreaEncuestaSimpleParametros;
     InitBroker;
     try
        CreaEncuestaSimpleBuildDataset;
        Result := CreaEncuestaSimpleDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.CreaEncuestaSimpleGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        CreaEncuestaSimpleBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerEvaluacion.CreaEncuestaSimpleLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;

     CreaEncuestaSimpleParametros;
     cdsLista.Lista := Lista;
     Result := CreaEncuestaSimpleDataset( cdsLista );
     SetComplete;
end;


function TdmServerEvaluacion.AgregarEncuestaParametros(Empresa, Parametros: OleVariant): OleVariant;
begin
    with oZetaProvider do
    begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
    end;

    SetAgregarEncuestaParametros;
    InitBroker;
    try
        with oZetaProvider do
        begin
            if OpenProcess( prDEAgregarEncuesta, 0, FListaParametros ) then
            begin
                Result := CloseProcess; //Modificar el proceso de como se guarda la encuesta
            end;
        end;

    finally
        ClearBroker;
    end;

    SetComplete;
end;

procedure TdmServerEvaluacion.SetAgregarEncuestaParametros;
begin
with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;

          FListaParametros := 'Encuesta: ' + ParamByName('ET_NOMBRE').AsString + K_PIPE +
                              'Fecha Inicial: ' + FechaCorta( ParamByName('ET_FEC_INI').AsDateTime) + K_PIPE +
                              'Fecha Final: ' + FechaCorta(  ParamByName('ET_FEC_FIN').AsDatetime ) + K_PIPE +
                              'Administrador: ' + InttoStr( ParamByName('ET_USR_ADM').AsInteger );
     end;
end;


{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerEvaluacion, Class_dmServerEvaluacion, ciMultiInstance, ZetaServerTools.GetThreadingModel );
{$endif}
end.
