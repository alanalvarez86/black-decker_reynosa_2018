unit GlobalVisitantes_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision: 5$
// File generated on 04/07/2006 05:28:23 p.m. from Type Library described below.

// ************************************************************************ //
// Type Lib: D:\3win_20\MTS\GlobalVisitantes.tlb (1)
// IID\LCID: {23A250A4-66D2-4B54-B4C9-9251E7C0CC61}\0
// Helpfile: 
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (3) v4.0 StdVCL, (C:\WINDOWS\system32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL, 
  MIDAS;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  GlobalVisitantesMajorVersion = 1;
  GlobalVisitantesMinorVersion = 0;

  LIBID_GlobalVisitantes: TGUID = '{23A250A4-66D2-4B54-B4C9-9251E7C0CC61}';

  IID_IdmServerGlobalVisitantes: TGUID = '{8892CDA2-A462-4A32-BB00-329039A39286}';
  CLASS_dmServerGlobalVisitantes: TGUID = '{4000D2C6-F326-4CB6-9409-4E0B1A16750B}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerGlobalVisitantes = interface;
  IdmServerGlobalVisitantesDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerGlobalVisitantes = IdmServerGlobalVisitantes;


// *********************************************************************//
// Interface: IdmServerGlobalVisitantes
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8892CDA2-A462-4A32-BB00-329039A39286}
// *********************************************************************//
  IdmServerGlobalVisitantes = interface(IAppServer)
    ['{8892CDA2-A462-4A32-BB00-329039A39286}']
    function  GetGlobales(Empresa: OleVariant): OleVariant; safecall;
    function  GrabaGlobales(Empresa: OleVariant; oDelta: OleVariant; lActualizaDiccion: WordBool; 
                            out ErrorCount: Integer): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerGlobalVisitantesDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8892CDA2-A462-4A32-BB00-329039A39286}
// *********************************************************************//
  IdmServerGlobalVisitantesDisp = dispinterface
    ['{8892CDA2-A462-4A32-BB00-329039A39286}']
    function  GetGlobales(Empresa: OleVariant): OleVariant; dispid 1;
    function  GrabaGlobales(Empresa: OleVariant; oDelta: OleVariant; lActualizaDiccion: WordBool; 
                            out ErrorCount: Integer): OleVariant; dispid 2;
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                              MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                            Options: Integer; const CommandText: WideString; 
                            var Params: OleVariant; var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerGlobalVisitantes provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerGlobalVisitantes exposed by              
// the CoClass dmServerGlobalVisitantes. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerGlobalVisitantes = class
    class function Create: IdmServerGlobalVisitantes;
    class function CreateRemote(const MachineName: string): IdmServerGlobalVisitantes;
  end;

implementation

uses ComObj;

class function CodmServerGlobalVisitantes.Create: IdmServerGlobalVisitantes;
begin
  Result := CreateComObject(CLASS_dmServerGlobalVisitantes) as IdmServerGlobalVisitantes;
end;

class function CodmServerGlobalVisitantes.CreateRemote(const MachineName: string): IdmServerGlobalVisitantes;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerGlobalVisitantes) as IdmServerGlobalVisitantes;
end;

end.
