library TimbradoNomina;

{%ToDo 'TimbradoNomina.todo'}

uses
  ComServ,
  TimbradoNomina_TLB in 'TimbradoNomina_TLB.pas',
  DServerNominaTimbrado in 'DServerNominaTimbrado.pas' {dmServerNominaTimbrado: TMtsDataModule} {dmServerNominaTimbrado: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
