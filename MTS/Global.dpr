library Global;

uses
  ComServ,
  Global_TLB in 'Global_TLB.pas',
  DServerGlobal in 'DServerGlobal.pas' {dmServerGlobal: TMtsDataModule} {dmServerGlobal: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
