library Catalogos;

uses
  ComServ,
  Catalogos_TLB in 'Catalogos_TLB.pas',
  DServerCatalogos in 'DServerCatalogos.pas' {dmServerCatalogos: TMtsDataModule} {dmServerCatalogos: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
