unit Recursos_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 08-Jan-18 12:31:24 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2018\MTS\Recursos.tlb (1)
// LIBID: {390ED40B-88B3-11D3-A265-0050DA04EC66}
// LCID: 0
// Helpfile: 
// HelpString: Tress Recursos
// DepndLst: 
//   (1) v1.0 Midas, (C:\Program Files (x86)\Embarcadero\RAD Studio\12.0\bin64\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  RecursosMajorVersion = 1;
  RecursosMinorVersion = 0;

  LIBID_Recursos: TGUID = '{390ED40B-88B3-11D3-A265-0050DA04EC66}';

  IID_IdmServerRecursos: TGUID = '{390ED40C-88B3-11D3-A265-0050DA04EC66}';
  CLASS_dmServerRecursos: TGUID = '{390ED40E-88B3-11D3-A265-0050DA04EC66}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerRecursos = interface;
  IdmServerRecursosDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerRecursos = IdmServerRecursos;


// *********************************************************************//
// Interface: IdmServerRecursos
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {390ED40C-88B3-11D3-A265-0050DA04EC66}
// *********************************************************************//
  IdmServerRecursos = interface(IAppServer)
    ['{390ED40C-88B3-11D3-A265-0050DA04EC66}']
    function GetDatosEmpleado(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetEmpPercepcion(Empresa: OleVariant; EmpleadoNumero: Integer; 
                              const sTipo: WideString; dFechaMov: TDateTime): OleVariant; safecall;
    function GetIdentFoto(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetEmpAntesCur(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetEmpAntesPto(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetEmpFoto(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetEmpParientes(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetHisAcumulados(Empresa: OleVariant; EmpleadoNumero: Integer; Year: Integer): OleVariant; safecall;
    function GetHisAhorros(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetHisCursos(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetHisCursosProg(Empresa: OleVariant; EmpleadoNumero: Integer; lFiltraCurso: WordBool): OleVariant; safecall;
    function GetHisIncapaci(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetHisKardex(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetHisNominas(Empresa: OleVariant; EmpleadoNumero: Integer; Year: Integer; 
                           Tipo: Integer): OleVariant; safecall;
    function GetHisPagosIMSS(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetHisPermiso(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetHisPrestamos(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GetHisVacacion(Empresa: OleVariant; EmpleadoNumero: Integer; FechaRefe: TDateTime; 
                            out DatosEmpleado: OleVariant): OleVariant; safecall;
    function GetEditHisKardex(Empresa: OleVariant; EmpleadoNumero: Integer; dFecha: TDateTime; 
                              const sTipo: WideString): OleVariant; safecall;
    function GrabaHistorial(Empresa: OleVariant; iTipo: Integer; oDelta: OleVariant; 
                            out ErrorCount: Integer): OleVariant; safecall;
    function GrabaHisVacacion(Empresa: OleVariant; oDelta: OleVariant; ValidaInicio: WordBool; 
                              out ErrorCount: Integer): OleVariant; safecall;
    function GrabaHisKardex(Empresa: OleVariant; oDelta: OleVariant; const sFijas: WideString; 
                            out ErrorCount: Integer): OleVariant; safecall;
    function GrabaDatosEmpleado(Empresa: OleVariant; oDelta: OleVariant; OtrosDatos: OleVariant; 
                                const sFijas: WideString; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaEmpParientes(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaEmpAntesCur(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaEmpAntesPto(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaEmpFoto(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetLastNumeroEmpleado(Empresa: OleVariant): Integer; safecall;
    function BorrarBajas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AplicarTabuladorGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AplicarTabuladorLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AplicarTabulador(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function SalarioIntegradoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function SalarioIntegrado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function SalarioIntegradoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CambioSalario(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CambioSalarioLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CambioSalarioGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function Vacaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function VacacionesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function VacacionesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function Eventos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function EventosLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function EventosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ImportarKardexGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                    ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function ImportarKardexLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ImportarKardexGetLista(Empresa: OleVariant; Parametros: OleVariant; Lista: OleVariant): OleVariant; safecall;
    function CancelarKardex(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarKardexLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarKardexGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarVacaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarVacacionesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarVacacionesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PromediarVariables(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PromediarVariablesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PromediarVariablesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetStatus(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): Integer; safecall;
    function GetEditKarFijas(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; safecall;
    function GrabaCambiosMultiples(Empresa: OleVariant; oDelta: OleVariant; OtrosDatos: OleVariant; 
                                   out ErrorCount: Integer): OleVariant; safecall;
    function GrabaBorrarKardex(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetSaldosVacacion(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; safecall;
    function GetDatosClasificacion(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; safecall;
    function GetDatosSalario(Empresa: OleVariant; Empleado: Integer; FechaInicial: TDateTime; 
                             FechaFinal: TDateTime): OleVariant; safecall;
    function GetGridCursos(Empresa: OleVariant): OleVariant; safecall;
    function GrabaGridCursos(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ExisteBancaElectronica(Empresa: OleVariant; const Banca: WideString; 
                                    Empleado: Integer; out Mensaje: WideString; 
                                    const Campo: WideString): WordBool; safecall;
    function CierreGlobalVacaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CierreGlobalVacacionesLista(Empresa: OleVariant; Lista: OleVariant; 
                                         Parametros: OleVariant): OleVariant; safecall;
    function CierreGlobalVacacionesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetHisTools(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function GrabaGridTools(Empresa: OleVariant; Parametros: OleVariant; oDelta: OleVariant; 
                            out ErrorCount: Integer): OleVariant; safecall;
    function GetGridTools(Empresa: OleVariant; Global: WordBool): OleVariant; safecall;
    function GetGridToolsBack(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; safecall;
    function EntregarHerramienta(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function EntregarHerramientaLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function EntregarHerramientaGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function BorrarHerramientas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RegresarHerramienta(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RegresarHerramientaLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RegresarHerramientaGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CursoTomado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CursoTomadoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CursoTomadoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetGridIncapaci(Empresa: OleVariant; SoloAltas: WordBool): OleVariant; safecall;
    function ValidaInformacion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ImportarAltasGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                   ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ImportarAltasValidaCatalogos(Empresa: OleVariant; Parametros: OleVariant; 
                                          ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ImportarAltasLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function Renumera(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RenumeraLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RenumeraGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RenumeraValidaASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                 ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetBancaElectronica(Empresa: OleVariant; lSoloAltas: WordBool; const Campo: WideString): OleVariant; safecall;
    function GrabaBancaElectronica(Empresa: OleVariant; oDelta: OleVariant; 
                                   out ErrorCount: Integer; const Campo: WideString): OleVariant; safecall;
    function GetCreditoInfonavit(Empresa: OleVariant; lSoloAltas: WordBool): OleVariant; safecall;
    function GrabaCreditoInfonavit(Empresa: OleVariant; oDelta: OleVariant; ErrorCount: Integer): OleVariant; safecall;
    function PermisoGlobal(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PermisoGlobalLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PermisoGlobalGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetGridPrestamos(Empresa: OleVariant; SoloAltas: WordBool): OleVariant; safecall;
    function GrabaGridPrestamos(Empresa: OleVariant; oDelta: OleVariant; Parametros: OleVariant; 
                                out ErrorCount: Integer; ForzarGrabarPrestamos: WordBool): OleVariant; safecall;
    function GetQtyTitularesPuesto(Empresa: OleVariant; const Puesto: WideString): Integer; safecall;
    function GetEmpOrganigrama(Empresa: OleVariant; iPlaza: Integer): OleVariant; safecall;
    function BorrarCursoTomado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function BorrarCursoTomadoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function BorrarCursoTomadoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function BuscaCandidato(Empresa: OleVariant; EmpresaSeleccion: OleVariant; 
                            Parametros: OleVariant; out Encontrado: WordBool): OleVariant; safecall;
    function GrabaFotoCandidato(Empresa: OleVariant; EmpresaSeleccion: OleVariant; 
                                Empleado: Integer; Folio: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function GetEmpProg(Empresa: OleVariant; iEmpleado: Integer; const sCurso: WideString): OleVariant; safecall;
    function GrabaEmpProg(Empresa: OleVariant; oDelta: OleVariant; ErrorCount: Integer): OleVariant; safecall;
    procedure BorraCurIndivid(Empresa: OleVariant; iEmpleado: Integer; const sCurso: WideString); safecall;
    function GetSaldosVacaAniv(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CambioMasivoTurnoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CambioMasivoTurnoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CambioMasivoTurno(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ImportarAsistenciaSesionesGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                                ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function ImportarAsistenciaSesionesLista(Empresa: OleVariant; Lista: OleVariant; 
                                             Parametros: OleVariant): OleVariant; safecall;
    function ImportarAsistenciaSesionesGetLista(Empresa: OleVariant; Parametros: OleVariant; 
                                                Lista: OleVariant): OleVariant; safecall;
    function CancelaCierreVacaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelaCierreVacacionesLista(Empresa: OleVariant; Lista: OleVariant; 
                                          Parametros: OleVariant): OleVariant; safecall;
    function CancelarCierreVacacionesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetSesiones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GrabaSesiones(Empresa: OleVariant; oDelta: OleVariant; out iFolio: Integer; 
                           out ErrorCount: Integer): OleVariant; safecall;
    function GetAprobados(Empresa: OleVariant; Sesion: Integer): OleVariant; safecall;
    function GetInscritos(Empresa: OleVariant; Sesion: Integer): OleVariant; safecall;
    function GetListaAsistencia(Empresa: OleVariant; Sesion: Integer; Reserva: Integer; 
                                lRefrescar: WordBool; out sMensaje: WideString): OleVariant; safecall;
    function GetReservasSesion(Empresa: OleVariant; Sesion: Integer): OleVariant; safecall;
    function GetReservas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GrabaReservas(Empresa: OleVariant; oDelta: OleVariant; out iFolioReserva: Integer; 
                           out ErrorCount: Integer): OleVariant; safecall;
    function GrabaInscritos(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaListaAsistencia(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetKarCertificaciones(Empresa: OleVariant; iEmpleado: Integer): OleVariant; safecall;
    function GetPlanVacacion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GrabaPlanVacacion(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function CancelarPermisosGlobales(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarPermisosGlobalesLista(Empresa: OleVariant; Parametros: OleVariant; 
                                           Lista: OleVariant): OleVariant; safecall;
    function CancelarPermisosGlobalesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function MostrarEmpleadosAInscribir(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ImportarAhorrosPrestamosGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                              ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ImportarAhorrosPrestamos(Empresa: OleVariant; Parametros: OleVariant; Datos: OleVariant): OleVariant; safecall;
    function GetMaxOrdenPlazas(Empresa: OleVariant; const sPuesto: WideString): Integer; safecall;
    function GetPlazas(Empresa: OleVariant): OleVariant; safecall;
    function GrabaPlazas(Empresa: OleVariant; Delta: OleVariant; dFechaPlaza: TDateTime; 
                         const Observaciones: WideString; out ErrorCount: Integer): OleVariant; safecall;
    function RecalculaSaldosVaca(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RecalculaSaldosVacaLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RecalculaSaldosVacaGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetLookupPlazas(Empresa: OleVariant; Plaza: Integer; const Filtro: WideString; 
                             out Datos: OleVariant): WordBool; safecall;
    function GetSearchPlazas(Empresa: OleVariant; const Filtro: WideString): OleVariant; safecall;
    function GetArbolPlazas(Empresa: OleVariant; Plaza: Integer): OleVariant; safecall;
    function GetPlaza(Empresa: OleVariant; Plaza: Integer): OleVariant; safecall;
    function BorrarPlazas(Empresa: OleVariant; const Plazas: WideString): Integer; safecall;
    function GetGridCertificaciones(Empresa: OleVariant; Global: WordBool): OleVariant; safecall;
    function GrabaGridCertificaciones(Empresa: OleVariant; oDelta: OleVariant; 
                                      out iErrorCount: Integer): OleVariant; safecall;
    function GetPeriodoPlanVacacion(Empresa: OleVariant; iTipo: Integer; dFechaPlan: TDateTime; 
                                    out iYear: Integer; out iPeriodo: Integer): WordBool; safecall;
    function GetEmpleadoPlaza(Empresa: OleVariant; iEmpleado: Integer): OleVariant; safecall;
    function UpdPlazaSalTab(Empresa: OleVariant): WideString; safecall;
    function GetPlazasFiltro(Empresa: OleVariant; const sFiltro: WideString): OleVariant; safecall;
    function GetHisCertificaProg(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function GetCertificacionesGlobal(Empresa: OleVariant; Fecha: TDateTime; 
                                      const Certificacion: WideString): OleVariant; safecall;
    function GetAsistenciaCurso(Empresa: OleVariant; Asistencia: Integer; Grupo: Integer): OleVariant; safecall;
    function GetHisCreInfonavit(Empresa: OleVariant; iEmpleado: Integer): OleVariant; safecall;
    function GrabaPrestamos(Empresa: OleVariant; Parametros: OleVariant; oDelta: OleVariant; 
                            GrabaPrestamo: WordBool; var Mensaje: WideString; 
                            out ErrorCount: Integer): OleVariant; safecall;
    function CursoProgGlobal(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CursoProgGlobalLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CursoProgGlobalGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarCursoProgGlobal(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarCursoProgGlobalLista(Empresa: OleVariant; Lista: OleVariant; 
                                          Parametros: OleVariant): OleVariant; safecall;
    function CancelarCursoProgGlobalGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ImportarInfonavitLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function FoliarCapacitaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetDocumentos(Empresa: OleVariant; iEmpleado: Integer): OleVariant; safecall;
    function GrabaDocumento(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetListaEmpleadosZK(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ConsultaUnEmpleadoZK(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    procedure BorraDatoZK(Empresa: OleVariant; Empleado: Integer); safecall;
    procedure AgregaDatoZK(Empresa: OleVariant; Empleado: Integer; Huella: Integer; 
                           const Plantilla: WideString); safecall;
    function ConsultaZK(Empresa: OleVariant; Fecha: TDateTime; const FiltroEmpleado: WideString): OleVariant; safecall;
    function GetPensionesAlimenticias(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function GrabaPensionesAlimenticias(Empresa: OleVariant; Delta: OleVariant; 
                                        out ErrorCount: Integer): OleVariant; safecall;
    function GetSegurosGastosMedicos(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function GrabaSegurosGastosMedicos(Empresa: OleVariant; Delta: OleVariant; 
                                       out ErrorCount: Integer): OleVariant; safecall;
    function ImportarSGMGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                 ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ImportarSGMLista(Empresa: OleVariant; Parametros: OleVariant; Lista: OleVariant): OleVariant; safecall;
    function ImportarSGMGetLista(Empresa: OleVariant; Parametros: OleVariant; Lista: OleVariant): OleVariant; safecall;
    function RenovarSGMLista(Empresa: OleVariant; Parametros: OleVariant; Lista: OleVariant): OleVariant; safecall;
    function RenovarSGMGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function BorrarSGMLista(Empresa: OleVariant; Parametros: OleVariant; Lista: OleVariant): OleVariant; safecall;
    function BorrarSGMGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetSaldosVacaciones(Empresa: OleVariant; Empleado: Integer; out Totales: OleVariant): OleVariant; safecall;
    function GrabaIncapacidad(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                              ConflictoParams: OleVariant): OleVariant; safecall;
    function ImportarOrganigramaGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                         ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ImportarOrganigramaGetLista(Empresa: OleVariant; Parametros: OleVariant; 
                                         Lista: OleVariant): OleVariant; safecall;
    function ImportarOrganigramaLista(Empresa: OleVariant; Parametros: OleVariant; 
                                      Employees: OleVariant): OleVariant; safecall;
    function GetMaxOrden(Empresa: OleVariant; const Poliza: WideString; const Vigencia: WideString; 
                         Empleado: Integer): Integer; safecall;
    function FoliarCapacitacionesSTPSGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function FoliarCapacitacionesSTPS(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function FoliarCapacitacionesSTPSLista(Empresa: OleVariant; Lista: OleVariant; 
                                           Parametros: OleVariant): OleVariant; safecall;
    function RevisarDatosSTPS(Empresa: OleVariant; Parametros: OleVariant; out Errores: WideString): OleVariant; safecall;
    function GetCompetenciasEmp(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function GrabaCompetenciasEmp(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetEvaluaCompEmp(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function GrabaEvaluaCompEmp(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetPlanCapacitacion(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function GetMatrizHabilidades(Empresa: OleVariant; Params: OleVariant): OleVariant; safecall;
    function GetEmpleadosImagenes(Empresa: OleVariant): OleVariant; safecall;
    function ImportarImagenesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetEmpFonacotGetLista(Empresa: OleVariant; Parametros: OleVariant; Lista: OleVariant): OleVariant; safecall;
    function GetEmpFonacotLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetHisAcumuladosRS(Empresa: OleVariant; EmpleadoNumero: Integer; Year: Integer; 
                                const RazonSocial: WideString): OleVariant; safecall;
    function GetMatrizCursos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetSugiereEmpleado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetHisHistorialFonacot(Empresa: OleVariant; Empleado: Integer; 
                                    const Referencia: WideString; var Suma: Double; 
                                    var SumaRetencion: Double): OleVariant; safecall;
    function GetHisVacacionAFecha(Empresa: OleVariant; EmpleadoNumero: Integer; FechaRefe: TDateTime): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerRecursosDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {390ED40C-88B3-11D3-A265-0050DA04EC66}
// *********************************************************************//
  IdmServerRecursosDisp = dispinterface
    ['{390ED40C-88B3-11D3-A265-0050DA04EC66}']
    function GetDatosEmpleado(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 1610809344;
    function GetEmpPercepcion(Empresa: OleVariant; EmpleadoNumero: Integer; 
                              const sTipo: WideString; dFechaMov: TDateTime): OleVariant; dispid 2;
    function GetIdentFoto(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 3;
    function GetEmpAntesCur(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 4;
    function GetEmpAntesPto(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 5;
    function GetEmpFoto(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 6;
    function GetEmpParientes(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 7;
    function GetHisAcumulados(Empresa: OleVariant; EmpleadoNumero: Integer; Year: Integer): OleVariant; dispid 8;
    function GetHisAhorros(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 9;
    function GetHisCursos(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 10;
    function GetHisCursosProg(Empresa: OleVariant; EmpleadoNumero: Integer; lFiltraCurso: WordBool): OleVariant; dispid 11;
    function GetHisIncapaci(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 12;
    function GetHisKardex(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 13;
    function GetHisNominas(Empresa: OleVariant; EmpleadoNumero: Integer; Year: Integer; 
                           Tipo: Integer): OleVariant; dispid 14;
    function GetHisPagosIMSS(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 15;
    function GetHisPermiso(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 16;
    function GetHisPrestamos(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 17;
    function GetHisVacacion(Empresa: OleVariant; EmpleadoNumero: Integer; FechaRefe: TDateTime; 
                            out DatosEmpleado: OleVariant): OleVariant; dispid 18;
    function GetEditHisKardex(Empresa: OleVariant; EmpleadoNumero: Integer; dFecha: TDateTime; 
                              const sTipo: WideString): OleVariant; dispid 19;
    function GrabaHistorial(Empresa: OleVariant; iTipo: Integer; oDelta: OleVariant; 
                            out ErrorCount: Integer): OleVariant; dispid 20;
    function GrabaHisVacacion(Empresa: OleVariant; oDelta: OleVariant; ValidaInicio: WordBool; 
                              out ErrorCount: Integer): OleVariant; dispid 21;
    function GrabaHisKardex(Empresa: OleVariant; oDelta: OleVariant; const sFijas: WideString; 
                            out ErrorCount: Integer): OleVariant; dispid 22;
    function GrabaDatosEmpleado(Empresa: OleVariant; oDelta: OleVariant; OtrosDatos: OleVariant; 
                                const sFijas: WideString; out ErrorCount: Integer): OleVariant; dispid 23;
    function GrabaEmpParientes(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 24;
    function GrabaEmpAntesCur(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 25;
    function GrabaEmpAntesPto(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 26;
    function GrabaEmpFoto(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 27;
    function GetLastNumeroEmpleado(Empresa: OleVariant): Integer; dispid 28;
    function BorrarBajas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 29;
    function AplicarTabuladorGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 30;
    function AplicarTabuladorLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 31;
    function AplicarTabulador(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 32;
    function SalarioIntegradoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 33;
    function SalarioIntegrado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 34;
    function SalarioIntegradoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 35;
    function CambioSalario(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 36;
    function CambioSalarioLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 37;
    function CambioSalarioGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 38;
    function Vacaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 39;
    function VacacionesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 41;
    function VacacionesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 42;
    function Eventos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 43;
    function EventosLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 44;
    function EventosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 45;
    function ImportarKardexGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                    ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant; dispid 46;
    function ImportarKardexLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 47;
    function ImportarKardexGetLista(Empresa: OleVariant; Parametros: OleVariant; Lista: OleVariant): OleVariant; dispid 48;
    function CancelarKardex(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 49;
    function CancelarKardexLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 50;
    function CancelarKardexGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 51;
    function CancelarVacaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 55;
    function CancelarVacacionesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 56;
    function CancelarVacacionesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 57;
    function PromediarVariables(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 40;
    function PromediarVariablesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 60;
    function PromediarVariablesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 61;
    function GetStatus(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): Integer; dispid 59;
    function GetEditKarFijas(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; dispid 64;
    function GrabaCambiosMultiples(Empresa: OleVariant; oDelta: OleVariant; OtrosDatos: OleVariant; 
                                   out ErrorCount: Integer): OleVariant; dispid 65;
    function GrabaBorrarKardex(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 62;
    function GetSaldosVacacion(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; dispid 63;
    function GetDatosClasificacion(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; dispid 66;
    function GetDatosSalario(Empresa: OleVariant; Empleado: Integer; FechaInicial: TDateTime; 
                             FechaFinal: TDateTime): OleVariant; dispid 67;
    function GetGridCursos(Empresa: OleVariant): OleVariant; dispid 58;
    function GrabaGridCursos(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 68;
    function ExisteBancaElectronica(Empresa: OleVariant; const Banca: WideString; 
                                    Empleado: Integer; out Mensaje: WideString; 
                                    const Campo: WideString): WordBool; dispid 69;
    function CierreGlobalVacaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 52;
    function CierreGlobalVacacionesLista(Empresa: OleVariant; Lista: OleVariant; 
                                         Parametros: OleVariant): OleVariant; dispid 53;
    function CierreGlobalVacacionesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 54;
    function GetHisTools(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 70;
    function GrabaGridTools(Empresa: OleVariant; Parametros: OleVariant; oDelta: OleVariant; 
                            out ErrorCount: Integer): OleVariant; dispid 71;
    function GetGridTools(Empresa: OleVariant; Global: WordBool): OleVariant; dispid 72;
    function GetGridToolsBack(Empresa: OleVariant; EmpleadoNumero: Integer): OleVariant; dispid 73;
    function EntregarHerramienta(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 74;
    function EntregarHerramientaLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 75;
    function EntregarHerramientaGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 76;
    function BorrarHerramientas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 77;
    function RegresarHerramienta(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 78;
    function RegresarHerramientaLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 79;
    function RegresarHerramientaGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 80;
    function CursoTomado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 81;
    function CursoTomadoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 82;
    function CursoTomadoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 83;
    function GetGridIncapaci(Empresa: OleVariant; SoloAltas: WordBool): OleVariant; dispid 84;
    function ValidaInformacion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 85;
    function ImportarAltasGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                   ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; dispid 86;
    function ImportarAltasValidaCatalogos(Empresa: OleVariant; Parametros: OleVariant; 
                                          ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; dispid 87;
    function ImportarAltasLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 88;
    function Renumera(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 89;
    function RenumeraLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 90;
    function RenumeraGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 91;
    function RenumeraValidaASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                 ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; dispid 92;
    function GetBancaElectronica(Empresa: OleVariant; lSoloAltas: WordBool; const Campo: WideString): OleVariant; dispid 93;
    function GrabaBancaElectronica(Empresa: OleVariant; oDelta: OleVariant; 
                                   out ErrorCount: Integer; const Campo: WideString): OleVariant; dispid 94;
    function GetCreditoInfonavit(Empresa: OleVariant; lSoloAltas: WordBool): OleVariant; dispid 95;
    function GrabaCreditoInfonavit(Empresa: OleVariant; oDelta: OleVariant; ErrorCount: Integer): OleVariant; dispid 96;
    function PermisoGlobal(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 97;
    function PermisoGlobalLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 98;
    function PermisoGlobalGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 99;
    function GetGridPrestamos(Empresa: OleVariant; SoloAltas: WordBool): OleVariant; dispid 100;
    function GrabaGridPrestamos(Empresa: OleVariant; oDelta: OleVariant; Parametros: OleVariant; 
                                out ErrorCount: Integer; ForzarGrabarPrestamos: WordBool): OleVariant; dispid 101;
    function GetQtyTitularesPuesto(Empresa: OleVariant; const Puesto: WideString): Integer; dispid 102;
    function GetEmpOrganigrama(Empresa: OleVariant; iPlaza: Integer): OleVariant; dispid 103;
    function BorrarCursoTomado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 104;
    function BorrarCursoTomadoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 105;
    function BorrarCursoTomadoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 106;
    function BuscaCandidato(Empresa: OleVariant; EmpresaSeleccion: OleVariant; 
                            Parametros: OleVariant; out Encontrado: WordBool): OleVariant; dispid 107;
    function GrabaFotoCandidato(Empresa: OleVariant; EmpresaSeleccion: OleVariant; 
                                Empleado: Integer; Folio: Integer; out ErrorCount: Integer): OleVariant; dispid 108;
    function GetEmpProg(Empresa: OleVariant; iEmpleado: Integer; const sCurso: WideString): OleVariant; dispid 109;
    function GrabaEmpProg(Empresa: OleVariant; oDelta: OleVariant; ErrorCount: Integer): OleVariant; dispid 110;
    procedure BorraCurIndivid(Empresa: OleVariant; iEmpleado: Integer; const sCurso: WideString); dispid 111;
    function GetSaldosVacaAniv(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 112;
    function CambioMasivoTurnoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 113;
    function CambioMasivoTurnoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 114;
    function CambioMasivoTurno(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 115;
    function ImportarAsistenciaSesionesGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                                ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant; dispid 116;
    function ImportarAsistenciaSesionesLista(Empresa: OleVariant; Lista: OleVariant; 
                                             Parametros: OleVariant): OleVariant; dispid 117;
    function ImportarAsistenciaSesionesGetLista(Empresa: OleVariant; Parametros: OleVariant; 
                                                Lista: OleVariant): OleVariant; dispid 118;
    function CancelaCierreVacaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 119;
    function CancelaCierreVacacionesLista(Empresa: OleVariant; Lista: OleVariant; 
                                          Parametros: OleVariant): OleVariant; dispid 120;
    function CancelarCierreVacacionesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 121;
    function GetSesiones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 122;
    function GrabaSesiones(Empresa: OleVariant; oDelta: OleVariant; out iFolio: Integer; 
                           out ErrorCount: Integer): OleVariant; dispid 123;
    function GetAprobados(Empresa: OleVariant; Sesion: Integer): OleVariant; dispid 301;
    function GetInscritos(Empresa: OleVariant; Sesion: Integer): OleVariant; dispid 302;
    function GetListaAsistencia(Empresa: OleVariant; Sesion: Integer; Reserva: Integer; 
                                lRefrescar: WordBool; out sMensaje: WideString): OleVariant; dispid 303;
    function GetReservasSesion(Empresa: OleVariant; Sesion: Integer): OleVariant; dispid 304;
    function GetReservas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 124;
    function GrabaReservas(Empresa: OleVariant; oDelta: OleVariant; out iFolioReserva: Integer; 
                           out ErrorCount: Integer): OleVariant; dispid 125;
    function GrabaInscritos(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 305;
    function GrabaListaAsistencia(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 306;
    function GetKarCertificaciones(Empresa: OleVariant; iEmpleado: Integer): OleVariant; dispid 307;
    function GetPlanVacacion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 308;
    function GrabaPlanVacacion(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 309;
    function CancelarPermisosGlobales(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 310;
    function CancelarPermisosGlobalesLista(Empresa: OleVariant; Parametros: OleVariant; 
                                           Lista: OleVariant): OleVariant; dispid 311;
    function CancelarPermisosGlobalesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 312;
    function MostrarEmpleadosAInscribir(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 313;
    function ImportarAhorrosPrestamosGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                              ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; dispid 314;
    function ImportarAhorrosPrestamos(Empresa: OleVariant; Parametros: OleVariant; Datos: OleVariant): OleVariant; dispid 315;
    function GetMaxOrdenPlazas(Empresa: OleVariant; const sPuesto: WideString): Integer; dispid 316;
    function GetPlazas(Empresa: OleVariant): OleVariant; dispid 317;
    function GrabaPlazas(Empresa: OleVariant; Delta: OleVariant; dFechaPlaza: TDateTime; 
                         const Observaciones: WideString; out ErrorCount: Integer): OleVariant; dispid 318;
    function RecalculaSaldosVaca(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 319;
    function RecalculaSaldosVacaLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 320;
    function RecalculaSaldosVacaGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 321;
    function GetLookupPlazas(Empresa: OleVariant; Plaza: Integer; const Filtro: WideString; 
                             out Datos: OleVariant): WordBool; dispid 1;
    function GetSearchPlazas(Empresa: OleVariant; const Filtro: WideString): OleVariant; dispid 127;
    function GetArbolPlazas(Empresa: OleVariant; Plaza: Integer): OleVariant; dispid 126;
    function GetPlaza(Empresa: OleVariant; Plaza: Integer): OleVariant; dispid 128;
    function BorrarPlazas(Empresa: OleVariant; const Plazas: WideString): Integer; dispid 129;
    function GetGridCertificaciones(Empresa: OleVariant; Global: WordBool): OleVariant; dispid 322;
    function GrabaGridCertificaciones(Empresa: OleVariant; oDelta: OleVariant; 
                                      out iErrorCount: Integer): OleVariant; dispid 323;
    function GetPeriodoPlanVacacion(Empresa: OleVariant; iTipo: Integer; dFechaPlan: TDateTime; 
                                    out iYear: Integer; out iPeriodo: Integer): WordBool; dispid 324;
    function GetEmpleadoPlaza(Empresa: OleVariant; iEmpleado: Integer): OleVariant; dispid 325;
    function UpdPlazaSalTab(Empresa: OleVariant): WideString; dispid 326;
    function GetPlazasFiltro(Empresa: OleVariant; const sFiltro: WideString): OleVariant; dispid 327;
    function GetHisCertificaProg(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 328;
    function GetCertificacionesGlobal(Empresa: OleVariant; Fecha: TDateTime; 
                                      const Certificacion: WideString): OleVariant; dispid 329;
    function GetAsistenciaCurso(Empresa: OleVariant; Asistencia: Integer; Grupo: Integer): OleVariant; dispid 330;
    function GetHisCreInfonavit(Empresa: OleVariant; iEmpleado: Integer): OleVariant; dispid 331;
    function GrabaPrestamos(Empresa: OleVariant; Parametros: OleVariant; oDelta: OleVariant; 
                            GrabaPrestamo: WordBool; var Mensaje: WideString; 
                            out ErrorCount: Integer): OleVariant; dispid 332;
    function CursoProgGlobal(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 333;
    function CursoProgGlobalLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 334;
    function CursoProgGlobalGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 335;
    function CancelarCursoProgGlobal(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 336;
    function CancelarCursoProgGlobalLista(Empresa: OleVariant; Lista: OleVariant; 
                                          Parametros: OleVariant): OleVariant; dispid 337;
    function CancelarCursoProgGlobalGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 338;
    function ImportarInfonavitLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 339;
    function FoliarCapacitaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 340;
    function GetDocumentos(Empresa: OleVariant; iEmpleado: Integer): OleVariant; dispid 341;
    function GrabaDocumento(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 343;
    function GetListaEmpleadosZK(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 344;
    function ConsultaUnEmpleadoZK(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 345;
    procedure BorraDatoZK(Empresa: OleVariant; Empleado: Integer); dispid 346;
    procedure AgregaDatoZK(Empresa: OleVariant; Empleado: Integer; Huella: Integer; 
                           const Plantilla: WideString); dispid 347;
    function ConsultaZK(Empresa: OleVariant; Fecha: TDateTime; const FiltroEmpleado: WideString): OleVariant; dispid 348;
    function GetPensionesAlimenticias(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 342;
    function GrabaPensionesAlimenticias(Empresa: OleVariant; Delta: OleVariant; 
                                        out ErrorCount: Integer): OleVariant; dispid 349;
    function GetSegurosGastosMedicos(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 350;
    function GrabaSegurosGastosMedicos(Empresa: OleVariant; Delta: OleVariant; 
                                       out ErrorCount: Integer): OleVariant; dispid 351;
    function ImportarSGMGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                 ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; dispid 352;
    function ImportarSGMLista(Empresa: OleVariant; Parametros: OleVariant; Lista: OleVariant): OleVariant; dispid 353;
    function ImportarSGMGetLista(Empresa: OleVariant; Parametros: OleVariant; Lista: OleVariant): OleVariant; dispid 354;
    function RenovarSGMLista(Empresa: OleVariant; Parametros: OleVariant; Lista: OleVariant): OleVariant; dispid 355;
    function RenovarSGMGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 356;
    function BorrarSGMLista(Empresa: OleVariant; Parametros: OleVariant; Lista: OleVariant): OleVariant; dispid 357;
    function BorrarSGMGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 358;
    function GetSaldosVacaciones(Empresa: OleVariant; Empleado: Integer; out Totales: OleVariant): OleVariant; dispid 359;
    function GrabaIncapacidad(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                              ConflictoParams: OleVariant): OleVariant; dispid 360;
    function ImportarOrganigramaGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                         ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; dispid 361;
    function ImportarOrganigramaGetLista(Empresa: OleVariant; Parametros: OleVariant; 
                                         Lista: OleVariant): OleVariant; dispid 362;
    function ImportarOrganigramaLista(Empresa: OleVariant; Parametros: OleVariant; 
                                      Employees: OleVariant): OleVariant; dispid 363;
    function GetMaxOrden(Empresa: OleVariant; const Poliza: WideString; const Vigencia: WideString; 
                         Empleado: Integer): Integer; dispid 364;
    function FoliarCapacitacionesSTPSGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 365;
    function FoliarCapacitacionesSTPS(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 366;
    function FoliarCapacitacionesSTPSLista(Empresa: OleVariant; Lista: OleVariant; 
                                           Parametros: OleVariant): OleVariant; dispid 367;
    function RevisarDatosSTPS(Empresa: OleVariant; Parametros: OleVariant; out Errores: WideString): OleVariant; dispid 368;
    function GetCompetenciasEmp(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 369;
    function GrabaCompetenciasEmp(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 370;
    function GetEvaluaCompEmp(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 371;
    function GrabaEvaluaCompEmp(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 372;
    function GetPlanCapacitacion(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 373;
    function GetMatrizHabilidades(Empresa: OleVariant; Params: OleVariant): OleVariant; dispid 374;
    function GetEmpleadosImagenes(Empresa: OleVariant): OleVariant; dispid 375;
    function ImportarImagenesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 378;
    function GetEmpFonacotGetLista(Empresa: OleVariant; Parametros: OleVariant; Lista: OleVariant): OleVariant; dispid 376;
    function GetEmpFonacotLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 377;
    function GetHisAcumuladosRS(Empresa: OleVariant; EmpleadoNumero: Integer; Year: Integer; 
                                const RazonSocial: WideString): OleVariant; dispid 379;
    function GetMatrizCursos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 380;
    function GetSugiereEmpleado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 381;
    function GetHisHistorialFonacot(Empresa: OleVariant; Empleado: Integer; 
                                    const Referencia: WideString; var Suma: Double; 
                                    var SumaRetencion: Double): OleVariant; dispid 382;
    function GetHisVacacionAFecha(Empresa: OleVariant; EmpleadoNumero: Integer; FechaRefe: TDateTime): OleVariant; dispid 383;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerRecursos provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerRecursos exposed by              
// the CoClass dmServerRecursos. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerRecursos = class
    class function Create: IdmServerRecursos;
    class function CreateRemote(const MachineName: string): IdmServerRecursos;
  end;

implementation

uses ComObj;

class function CodmServerRecursos.Create: IdmServerRecursos;
begin
  Result := CreateComObject(CLASS_dmServerRecursos) as IdmServerRecursos;
end;

class function CodmServerRecursos.CreateRemote(const MachineName: string): IdmServerRecursos;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerRecursos) as IdmServerRecursos;
end;

end.
