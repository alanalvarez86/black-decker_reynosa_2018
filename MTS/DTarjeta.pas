unit DTarjeta;

interface

{$define QUINCENALES}
{$define CAMBIO_TNOM}
{$define DEF_1628}  //Registro incorrecto de incidencias 'FSS' cuando existe una baja
{$define CR2575}    //Se est� considerando como asistencia cuando hay permisos sin goce entrada y no hay checadas, debiera ser FI
{$define IMSS_VACA} //Corregir d�as IMSS cuando hay vacaciones
{.$undefine QUINCENALES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DB,
     DQueries,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZCreator,
{$ifdef QUINCENALES}
     DVacaciones,
{$endif}
     DZetaServerProvider;

type
  eSustitucion = ( tsNoSustituir, tsSumar, tsSustituir );
  TDatosTarjeta = record
    Horario: TCodigo;
    Status: eStatusAusencia;
    EsManual: Boolean;
    Usuario: Integer;
  end;
  TLlaveChecada = class(TObject)
    Empleado: TNumEmp;
    Fecha: TDate;
    Hora: String;
    Tipo: eTipoChecadas;
  end;
  TRenglonNumerica = record
    Inferior: TPesos;
    Cuota: TPesos;
    Tasa: TTasa;
  end;
  TDatosJornadaDiaria = record
    Jornada: TDiasHoras;
    Dobles: TDiasHoras;
  end;
  THorarioData = class( TObject )
  private
    { Private declarations }
    FJornada: TDiasHoras;
    FDobles: TDiasHoras;
  public
    { Public declarations }
    constructor Create( const rJornada, rDobles: TDiasHoras );
    property Jornada: TDiasHoras read FJornada;
    property Dobles: TDiasHoras read FDobles;
  end;
  THorarioList = class( TStringList )
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FPtr: Integer;
    FCodigo: TCodigo;
    function GetElemento(Index: Integer): THorarioData;
  public
    { Public declarations }
    property Elemento[ Index: Integer ]: THorarioData read GetElemento;
    constructor Create( oProvider: TdmZetaServerProvider );
    destructor Destroy; override;
    function Locate( const sCodigo: TCodigo; var iPtr: Integer ): Boolean;
    procedure Cargar;
    procedure Clear; override;
  end;
  TChecada = class( TObject )
  private
    { Private Declarations }
    oZetaProvider: TdmZetaServerProvider;
    FEmpleado: TNumEmp;
    FFecha: TDate;
    FTipo: eTipoChecadas;
    FDescripcion: eDescripcionChecadas;
    FHoraReal: String;
    FHoraAjustada: String;
    FGlobal: Boolean;
    FIgnorar: Boolean;
    FSistema: Boolean;
    FOrdinarias: Real;
    FExtras: Real;
    FDescanso: Real;
    FReloj: String;
    FPosicion: Word;
    FUsuario: Word;
    FAprobadaPor: Word;
    FAprobadas: Real;
    FBorrada: Boolean;
    FLlave: TLlaveChecada;
    FMotivo: String;
    procedure Preparar;
  public
    { Public Declarations }
    property Llave: TLlaveChecada read FLlave write FLlave;
    property Empleado: TNumEmp read FEmpleado write FEmpleado;
    property Fecha: TDate read FFecha write FFecha;
    property Tipo: eTipoChecadas read FTipo write FTipo;
    property Descripcion: eDescripcionChecadas read FDescripcion write FDescripcion;
    property HoraReal: String read FHoraReal write FHoraReal;
    property HoraAjustada: String read FHoraAjustada write FHoraAjustada;
    property Global: Boolean read FGlobal write FGlobal;
    property Ignorar: Boolean read FIgnorar write FIgnorar;
    property Sistema: Boolean read FSistema write FSistema;
    property Ordinarias: Real read FOrdinarias write FOrdinarias;
    property Extras: Real read FExtras write FExtras;
    property Descanso: Real read FDescanso write FDescanso;
    property Reloj: String read FReloj write FReloj;
    property Posicion: Word read FPosicion write FPosicion;
    property Usuario: Word read FUsuario write FUsuario;
    property AprobadaPor: Word read FAprobadaPor write FAprobadaPor;
    property Aprobadas: Real read FAprobadas write FAprobadas;
    property Borrada: Boolean read FBorrada write FBorrada;
    property Motivo: String read FMotivo write FMotivo;
    constructor Create(oProvider: TdmZetaServerProvider);
    destructor Destroy;
    procedure SetAutorizacion( const iEmpleado: TNumEmp;
                               const dFecha: TDate;
                               const eTipo: eTipoChecadas;
                               const sMotivo: String;
                               const rValue: TDiasHoras;
                               const rHorasAprobadas: TDiasHoras;
                               const iAprobadasPor: integer );
    procedure Agregar( Dataset: TZetaCursor );
    procedure Cargar( Dataset: TZetaCursor );
    procedure Modificar( Dataset: TZetaCursor );
  end;
  TChecadas = class( TObject )
  private
    { Private Declarations }
    oZetaProvider: TdmZetaServerProvider;
    FQRead: TZetaCursor;
    FQWrite: TZetaCursor;
    FQDelete: TZetaCursor;
    FItems: TList;
    FChecadas: Word;
    function GetCount: Integer;
    function GetItem( Index: Integer ): TChecada;
    procedure AgregarNormal;
    procedure BorrarAnteriores( const iEmpleado: TNumEmp; const dFecha: TDate );
  public
    { Public Declarations }
    property Count: Integer read GetCount;
    property Items[ Index: Integer ]: TChecada read GetItem;
    property Checadas: Word read FChecadas;
    constructor Create( oProvider: TdmZetaServerProvider );
    destructor Destroy; override;
    function Add: TChecada;
    function EsValida: Boolean;
    procedure Cargar( const iEmpleado: TNumEmp; const dFecha: TDate; const iGraciaRepetida: Word );
    procedure Descargar( const iEmpleado: TNumEmp; const dFecha: TDate );
    procedure Delete( const Index: Integer );
    procedure Clear;
    procedure Despreparar;
    procedure Preparar;
    procedure Reordena;
    procedure AgregarSistema( const iEmpleado: TNumEmp; const dFecha: TDate; const sHora: String; const eTipo: eTipoChecadas; const eDescripcion: eDescripcionChecadas );
    procedure Ignorar( const Index: Integer );
  end;
  TTarjeta = class( TObject )
  private
    { Private declarations }
    FCreator: TZetaCreator;
{
    FFestivos: TListaFestivos;
}
    FChecada: TChecada;
    FChecadas: TChecadas;
    FHorarios: THorarioList;
    FStatusCheck: TZetaCursor;
{$ifdef QUINCENALES}
    FLeeDiasHorasExtras: TZetaCursor;
{$endif}
    FStatusActivo: TZetaCursor;
    FChecadaExiste: TZetaCursor;
    FChecadaBorra: TZetaCursor;
    FChecadaModifica: TZetaCursor;
    FChecadaAgrega: TZetaCursor;
    FTarjetaLee: TZetaCursor;
    FTarjetaEscribe: TZetaCursor;
    FTarjetaCambia: TZetaCursor;
    FTarjetaExiste: TZetaCursor;
    FQTurno: TZetaCursor;
    FQTNom: TZetaCursor;
    //FQArt80: TZetaCursor;
    FQGrabaTarjeta: TZetaCursor;
    FQGrabaGracias: TZetaCursor;
    FLeeOut2Eat: TZetaCursor;
    FGraciaRepetida: Integer;
    FTablaRedondeoOrdinaria: Integer;
    FTablaRedondeoExtras: Integer;
    FExtraSabado: eHorasExtras;
    FExtraDescanso: eHorasExtras;
    FExtraFestivo: eHorasExtras;
    FExtraConfianza: Boolean;
    FDescuentaComida: Boolean;
    FAprobarAutorizaciones: Boolean;
    FAutorizarFestivos: Boolean;
    FAutorizaExtrasYDescansos: Boolean;
    FAutExtYDesSabado: Boolean;
    FAutExtYDesDescanso: Boolean;
{$ifdef IMSS_VACA}
    FDiasCotizaSinFV: Boolean;
{$endif}
    FFestivoEnDescanso: Boolean;
{$ifdef QUINCENALES}
    FTopaPorSemana: Boolean;
    FTratamientoExtras: eTratamientoExtras;
    FDOWCorte: Word;                  { Cache de dmGlobal.D�a de Corte de Horas Extras en Pren�mina }
    FAplicaPlanVacacion: Boolean;
    FPlanVacacion: TPlanVacacion;
{$endif}
    FAutorizarExtras: Boolean;
    FDatosHorario: TDatosHorarioDiario;
    FEmpleado: Integer;
    FFecha: TDateTime;
    FMinTarde: Integer;
    FSumaOrd: Integer;
    FSumaExt: Integer;
    FSumaDes: Integer;
    FTotAutExt, FTotPreExt: Real;
    function CheckFestivo(const sTurno: String; const dFestivo: TDate): Boolean;
    //function GetArt80Renglon( const iTabla: Integer; const rValor: Real ): TRenglonNumerica;
    function GetJornadaDiaria( const sCodigo: TCodigo ): TDatosJornadaDiaria;
    function GetRitmos: TRitmos;
    function GetQueries: TCommonQueries;
    function oZetaProvider: TdmZetaServerProvider;
    function GetStatus(const iEmpleado: TNumEmp; const dReferencia: TDate; var Incidencia: TCodigo): eStatusEmpleado;
    function RedondeoExtras( const iMinutos: Integer; const dFecha: TDate ): Extended;
    function RedondeoHorasExtras( const rHoras: Extended; const dFecha: TDate ): Extended;
    function RedondeoOrdinarias( const iMinutos: Integer; const dFecha: TDate ): Extended;
    function Redondeo( const iMinutos, iTabla: Integer; const dFecha: TDate ): Extended;
    procedure CalcularHoras;
    procedure ClearHorarios;
    procedure GetJornadaDiariaBegin;
    procedure GetJornadaDiariaEnd;
    procedure GrabarTotales( eTipoDia: eStatusAusencia );
    procedure InitGlobalesPrenomina;
    procedure InitGlobalesRedondeo;
    procedure InitGlobalesTarjeta;
    procedure LeeUnaTarjetaBegin;
    procedure LeeUnaTarjetaEnd;
    procedure ProcesarGracias( const iEmpleado: TNumEmp; const dFecha: TDate );
    procedure RedondeoBegin;
    procedure RedondeoEnd;
    procedure TarjetaExisteBegin;
    procedure TarjetaExisteEnd;
    procedure SetPlanVacacionesBegin;
    procedure SetPlanVacacionesEnd;
    procedure SetPlanVacaciones( const iEmpleado: Integer; const dInicio, dFin: TDate );

  public
    { Public declarations }
    constructor Create( oCreator: TZetaCreator );
    destructor Destroy; override;
    property Ritmos: TRitmos read GetRitmos;
    property Queries: TCommonQueries read GetQueries;
    function AgregaTarjetaDiaria( const iEmpleado: TNumEmp; const dFecha: TDate ): TDatosTarjeta;
    function AgregaTarjetaDiariaExiste(const iEmpleado: TNumEmp; const dFecha: TDate; var DatosTarjeta: TDatosTarjeta): Boolean;
    function DoPoll(const iEmpleado: TNumEmp; dFecha: TDate; const sReloj, sCredencial, sHora: String; const lManejaTrans: Boolean = TRUE ): Boolean;
    function GetTurno(const iEmpleado: TNumEmp; const dReferencia: TDate): TCodigo;
    function GetTurnoHorario( const iEmpleado: TNumEmp; const dFecha: TDate ): TStatusHorario;
    procedure AgregaTarjetaDiariaEscribe(const iEmpleado: TNumEmp; const dFecha: TDate; var DatosTarjeta: TDatosTarjeta);
    procedure GetTurnoHorarioBegin(const dInicial, dFinal: TDate);
    procedure GetTurnoHorarioEnd;
    procedure AgregaAutorizacionBegin;
    function AgregaAutorizacion(const iEmpleado: TNumEmp;
                                 const dFecha: TDate;
                                 const eTipo: eTipoChecadas;
                                 const sMotivo: String;
                                 const rValue: Extended;
                                 const eModo: eSustitucion;
                                 const lTieneDerecho: boolean): boolean;
    procedure AgregaAutorizacionEnd;
    procedure AutorizacionAgregar;
    procedure AutorizacionBorrar;
    procedure AutorizacionModificar;
    procedure AutorizacionSet( const iEmpleado: TNumEmp;
                               const dFecha: TDate;
                               const eTipo: eTipoChecadas;
                               const sMotivo: String;
                               const rValue: TDiasHoras;
                               const rHorasAprobadas: TDiasHoras;
                               const iAprobadasPor: integer );
    procedure AutorizacionSetKey( const iEmpleado: TNumEmp;
                                  const dFecha: TDate;
                                  const eTipo: eTipoChecadas);
    procedure AgregaChecadasBegin;
    procedure AgregaChecada(const iEmpleado: TNumEmp;
                            const dFecha: TDate;
                            const sHora, sReloj, sMotivo: String;
                            const eTipo: eTipoChecadas;
                            const eDescripcion: eDescripcionChecadas;
                            const rHoras: Real;
                            const iUsuario: Integer;
                            const lGlobal: Boolean);
    procedure AgregaChecadasEnd;
    procedure AgregaTarjetaDiariaBegin( const dInicial, dFinal: TDate );
    procedure AgregaTarjetaDiariaEnd;
    procedure AutorizacionBegin( const dInicial, dFinal: TDate );
    procedure AutorizacionEnd;
    procedure CambiarTarjeta(const iEmpleado: TNumEmp; const dFecha: TDate; const sHorario: String; const eStatus: eStatusAusencia; const lCambioHorario, lCambioStatus: Boolean );
    procedure CambiarTarjetaBegin;
    procedure CambiarTarjetaEnd;
    procedure DoPollBegin( const dInicial, dFinal: TDate );
    procedure DoPollEnd;
{$ifdef QUINCENALES}
    procedure CalculaPrenominaBegin;
{$ifdef CAMBIO_TNOM}
    procedure CalculaPrenomina( const iEmpleado: TNumEmp; const lCheca: Boolean; const sTurno: TCodigo; const dIngreso, dBaja, dCambioTNom: TDate );
{$else}
    procedure CalculaPrenomina( const iEmpleado: TNumEmp; const lCheca: Boolean; const sTurno: TCodigo; const dIngreso, dBaja: TDate );
{$else}
    procedure CalculaPrenominaBegin( const dInicial, dFinal: TDate );
    procedure CalculaPrenomina( const iEmpleado: TNumEmp; const lCheca: Boolean; const sTurno: TCodigo );
{$endif}
{$endif}
    procedure CalculaPrenominaEnd;
    procedure CalculaTarjetaBegin;
    procedure CalculaTarjeta( const iEmpleado: TNumEmp; const dFecha: TDate; const sHorario: TCodigo; const eTipoDia: eStatusAusencia; const iUsuario: Integer );
    procedure CalculaTarjetaEnd;
    procedure CalculaUnaTarjeta( const iEmpleado: TNumEmp; const dFecha: TDate );
    procedure CalculaUnaTarjetaBegin;
    procedure CalculaUnaTarjetaEnd;
{$ifdef CAMBIO_TNOM}
    procedure GetTipoNomBegin;
    function GetTipoNomina(const iEmpleado: TNumEmp; const dReferencia: TDate): eTipoPeriodo;
    procedure GetTipoNomEnd;
{$endif}
  end;

implementation

uses ZetaCommonTools,
     ZGlobalTress,
     ZetaServerTools;

const
     K_HORA_DUMMY = '----';
     Q_POLL_AGREGA_CHECADA = 0;
     Q_POLL_BORRA_TODO = 1;
     Q_CHECADAS_BORRA = 2;
     Q_CHECADAS_BORRA_UNA = 3;
     Q_CHECADAS_ESCRIBE = 4;
     Q_CHECADAS_EXISTE = 5;
     Q_CHECADAS_MODIFICA = 6;
     Q_CHECADAS_LEE = 7;
     Q_TARJETA_AGREGA = 8;
     Q_TARJETAS_BORRA = 9;
     Q_TARJETA_CAMBIA = 10;
     Q_TARJETA_EXISTE = 11;
     Q_TARJETA_GRABA = 12;
     Q_TARJETA_GRABA_GRACIAS = 13;
     Q_TARJETA_LEE = 14;
     Q_TARJETA_PRENOMINA_LEE = 15;
     Q_TARJETA_PRENOMINA_ACTUALIZA = 16;
     //Q_ART_80 = 17;
     Q_LEE_TURNO = 18;
     Q_EMPLEADO_STATUS_ACTIVO = 19;
     Q_EMPLEADO_STATUS_CHECK = 20;
     Q_JORNADA_DIARIA = 21;
     Q_LEE_OUT2EAT = 22;
{$ifdef QUINCENALES}
     Q_TARJETA_PRENOMINA_HORAS_DOBLES = 23;
     Q_TARJETA_PRENOMINA_DIAS_EXTRAS = 24;
{$endif}
{$ifdef CAMBIO_TNOM}
     Q_LEE_TIPO_NOMINA               = 25;
{$endif}

function HorasAMinutos( const rHoras: Real ): Integer;
begin
     Result := Round( 60 * rHoras );
end;

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_POLL_BORRA_TODO: Result := 'delete from POLL';
          Q_CHECADAS_BORRA: Result := 'delete from CHECADAS where ( CB_CODIGO = :CB_CODIGO ) and ( AU_FECHA = :AU_FECHA )';
          Q_CHECADAS_BORRA_UNA: Result := 'delete from CHECADAS where '+
                                          '( CB_CODIGO = :Empleado ) and '+
                                          '( AU_FECHA = :Fecha ) and '+
                                          '( CH_H_REAL = :Hora ) and '+
                                          '( CH_TIPO = :Tipo )';
          Q_CHECADAS_ESCRIBE: Result := 'insert into CHECADAS( CB_CODIGO, AU_FECHA, CH_H_REAL, CH_H_AJUS, CH_TIPO, CH_DESCRIP, CH_GLOBAL, CH_IGNORAR, CH_SISTEMA, CH_HOR_ORD, CH_HOR_EXT, CH_HOR_DES, CH_RELOJ, CH_POSICIO, US_CODIGO, US_COD_OK, CH_MOTIVO ) '+
                                        'values( :CB_CODIGO, :AU_FECHA, :CH_H_REAL, :CH_H_AJUS, :CH_TIPO, :CH_DESCRIP, :CH_GLOBAL, :CH_IGNORAR, :CH_SISTEMA, :CH_HOR_ORD, :CH_HOR_EXT, :CH_HOR_DES, :CH_RELOJ, :CH_POSICIO, :US_CODIGO, :US_COD_OK, :CH_MOTIVO )';
          Q_CHECADAS_EXISTE: Result := 'select CH_TIPO, CH_H_REAL, CH_HOR_EXT, CH_HOR_DES, CH_HOR_ORD, US_COD_OK from CHECADAS where ' +
                                      '( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha ) and ' +
                                      '( CH_H_REAL = :Hora ) and ( CH_TIPO = :Tipo )';
          Q_CHECADAS_LEE: Result := 'select CB_CODIGO, AU_FECHA, CH_TIPO, CH_DESCRIP, CH_H_REAL, CH_H_AJUS, CH_GLOBAL, CH_IGNORAR, CH_SISTEMA, '+
                                    'CH_HOR_ORD, CH_HOR_EXT, CH_HOR_DES, CH_RELOJ, CH_POSICIO, US_CODIGO, US_COD_OK, CH_MOTIVO '+
                                    'from CHECADAS where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha ) order by CH_H_REAL';
          Q_CHECADAS_MODIFICA: Result := 'update CHECADAS set '+
                                         'CB_CODIGO = :CB_CODIGO, '+
                                         'AU_FECHA = :AU_FECHA, '+
                                         'CH_H_REAL = :CH_H_REAL, '+
                                         'CH_TIPO = :CH_TIPO, '+
                                         'CH_DESCRIP = :CH_DESCRIP, '+
                                         'CH_IGNORAR = :CH_IGNORAR, '+
                                         'CH_HOR_ORD = :CH_HOR_ORD, '+
                                         'CH_HOR_EXT = :CH_HOR_EXT, '+
                                         'CH_RELOJ = :CH_RELOJ, '+
                                         'CH_HOR_DES = :CH_HOR_DES, '+
                                         'US_COD_OK = :US_COD_OK, '+
                                         'CH_MOTIVO = :CH_MOTIVO, ' +
                                         'US_CODIGO = :US_CODIGO where '+
                                         '( CB_CODIGO = :Empleado ) and '+
                                         '( AU_FECHA = :Fecha ) and '+
                                         '( CH_H_REAL = :Hora ) and '+
                                         '( CH_TIPO = :Tipo )';
          Q_TARJETAS_BORRA: Result := 'delete from AUSENCIA where ( AU_FECHA >= :FechaInicial ) and ( AU_FECHA <= :FechaFinal )';
          Q_TARJETA_CAMBIA: Result := 'update AUSENCIA set '+
                                      'AU_HOR_MAN = :EsManual, '+
                                      'AU_STATUS = :Status, '+
                                      'HO_CODIGO = :Horario '+
                                      'where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha )';
          Q_TARJETA_EXISTE: Result := 'select HO_CODIGO, AU_STATUS, AU_HOR_MAN, US_CODIGO from AUSENCIA where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha )';
          Q_TARJETA_GRABA: Result := 'update AUSENCIA set '+
                                     'AU_HORASCK = :AU_HORASCK, AU_NUM_EXT = :AU_NUM_EXT, AU_EXTRAS = :AU_EXTRAS, '+
                                     'AU_DOBLES = :AU_DOBLES, AU_TRIPLES = :AU_TRIPLES, AU_TARDES = :AU_TARDES '+
                                     'where ( CB_CODIGO = :CB_CODIGO ) and ( AU_FECHA = :AU_FECHA )';
          Q_TARJETA_GRABA_GRACIAS: Result := 'update AUSENCIA set '+
                                             'AU_AUT_EXT = :AU_AUT_EXT, AU_AUT_TRA = :AU_AUT_TRA, AU_PER_CG = :AU_PER_CG, AU_PER_SG = :AU_PER_SG, AU_PRE_EXT = :AU_PRE_EXT '+
                                             'where ( CB_CODIGO = :CB_CODIGO ) and ( AU_FECHA = :AU_FECHA )';
          Q_TARJETA_LEE: Result := 'select HO_CODIGO, AU_STATUS, US_CODIGO from AUSENCIA where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha )';
          Q_TARJETA_PRENOMINA_LEE: Result := 'select AU_FECHA, HO_CODIGO, AU_STATUS, AU_NUM_EXT, AU_AUT_EXT, AU_AUT_TRA, AU_HORASCK, AU_PER_CG, '+
                                             'AU_PER_SG, AU_TARDES, AU_EXTRAS, US_CODIGO, AU_TIPODIA, AU_STA_FES, AU_PRE_EXT, CB_NOMINA ' +
                                             // DES US #13377: TPI Composites- Calculo de horas ordinarias en dia festivo es incorrecta cuando hay cambio de turno
                                             ', CB_TURNO ' +
                                             // DES US #13383: Festivos - Modifica el horario en la pre-nomina y no se considera cuando es festivo y tiene horario de festivo trabajado
                                             ', AU_HOR_MAN ' +
                                             'from AUSENCIA '+
{$ifdef QUINCENALES}
                                             'where ( CB_CODIGO = :Empleado ) and ( AU_FECHA >= :FechaIni ) and ( AU_FECHA <= :FechaFin ) '+
{$else}
                                             'where ( CB_CODIGO = :Empleado ) and ( AU_FECHA >= %s ) and ( AU_FECHA <= %s ) '+
{$endif}
                                             'order by AU_FECHA';
          Q_TARJETA_PRENOMINA_ACTUALIZA: Result := 'execute procedure SP_UPDATE_AUSENCIA( '+
                                                   ':EMPLEADO, '+
                                                   ':FECHA, '+
                                                   ':HORARIO, '+
                                                   ':STATUS, '+
                                                   ':ANNO, '+
                                                   ':TIPO, '+
                                                   ':PERIODO, '+
                                                   ':POSICION, '+
                                                   ':INCIDENCIA, '+
                                                   ':TIPODIA, '+
                                                   ':ORDINARIAS, '+
                                                   ':DOBLES, '+
                                                   ':TRIPLES, '+
                                                   ':DESCANSO, '+
                                                   ':PERMISOCG, '+
                                                   ':PERMISOSG' +
{$ifdef QUINCENALES}
                                                   ', :HORAS_NT' +
{$endif}
                                                   ' )';
{
          Q_ART_80: Result := 'select A80_LI, A80_TASA, A80_CUOTA from ART_80 '+
                              'where ( NU_CODIGO = :Tabla1 ) and '+
                              '( A80_LI = ( select MAX( A80_LI ) from ART_80 '+
                              'where ( NU_CODIGO = :Tabla2 ) and ( A80_LI <= :Valor ) ) )';
}
{$ifdef INTERBASE}
          Q_POLL_AGREGA_CHECADA: Result := 'select CB_CODIGO, AU_FECHA, HO_CODIGO, AU_STATUS, AU_AUT_EXT, US_CODIGO, STATUS_EMP '+
                                           'from SP_ADD_CHECADA( :Empleado, :Fecha, :Horario, :Status, :Hora, :Reloj )';
          Q_TARJETA_AGREGA: Result := 'select CB_CODIGO, AU_FECHA, HO_CODIGO, AU_STATUS, AU_AUT_EXT, US_CODIGO from SP_ADD_AUSENCIA( :EMPLEADO, :FECHA, :HORARIO, :STATUS )';
          Q_LEE_TURNO: Result := 'select CB_TURNO from SP_FECHA_TURNO( :Fecha, :Empleado )';
          Q_EMPLEADO_STATUS_ACTIVO: Result := 'select RESULTADO, INCIDENCIA from SP_STATUS_INCIDENCIA( :Fecha, :Empleado )';
{$ifdef QUINCENALES}
          Q_EMPLEADO_STATUS_CHECK: Result := 'select RESULTADO from SP_RANGO_ACTIVO( :Empleado, :FechaIni, :FechaFin )';
{$else}
          Q_EMPLEADO_STATUS_CHECK: Result := 'select RESULTADO from SP_RANGO_ACTIVO( :Empleado, %s, %s )';
{$endif}
{$endif}
{$ifdef MSSQL}
          Q_POLL_AGREGA_CHECADA: Result := 'execute procedure SP_ADD_CHECADA( :Empleado, :Fecha, :Horario, :Status, :Hora, :Reloj, '+
                                           ':CB_CODIGO, :AU_FECHA, :HO_CODIGO, :AU_STATUS, :AU_AUT_EXT, :US_CODIGO, :STATUS_EMP )';
          Q_TARJETA_AGREGA: Result := 'EXECUTE PROCEDURE SP_ADD_AUSENCIA( :EMPLEADO, :FECHA, :HORARIO, :STATUS, :CB_CODIGO, :AU_FECHA, :HO_CODIGO, :AU_STATUS, :AU_AUT_EXT, :US_CODIGO )';
          Q_LEE_TURNO: Result := 'select CB_TURNO = dbo.SP_KARDEX_CB_TURNO( :Fecha, :Empleado )';
          Q_EMPLEADO_STATUS_ACTIVO: Result := 'EXECUTE PROCEDURE SP_STATUS_INCIDENCIA( :Fecha, :Empleado, :Resultado, :Incidencia )';
{$ifdef QUINCENALES}
          Q_EMPLEADO_STATUS_CHECK: Result := 'select DBO.SP_RANGO_ACTIVO( :Empleado, :FechaIni, :FechaFin ) as RESULTADO';
{$else}
          Q_EMPLEADO_STATUS_CHECK: Result := 'select DBO.SP_RANGO_ACTIVO( :Empleado, %s, %s ) as RESULTADO';
{$endif}
{$endif}
          Q_JORNADA_DIARIA : Result := 'select HO_CODIGO, HO_JORNADA, HO_DOBLES from HORARIO order by HO_CODIGO';
          Q_LEE_OUT2EAT: Result := 'select AU_OUT2EAT from AUSENCIA where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha )';
{$ifdef QUINCENALES}
          Q_TARJETA_PRENOMINA_HORAS_DOBLES: Result := 'select SUM( AU_DOBLES ) as CUANTOS from AUSENCIA where '+
                                                      '( CB_CODIGO = :Empleado ) and '+
                                                      '( AU_FECHA >= :Fecha ) and '+
                                                      '( AU_FECHA < :FechaInicial )';
          Q_TARJETA_PRENOMINA_DIAS_EXTRAS: Result := 'select COUNT(*) as CUANTOS from AUSENCIA where '+
                                                     '( CB_CODIGO = :Empleado ) and '+
                                                     '( AU_FECHA >= :Fecha ) and '+
                                                     '( AU_FECHA < :FechaInicial ) and '+
                                                     '( AU_EXTRAS > 0 )';
{$endif}
{$ifdef CAMBIO_TNOM}
        {$ifdef MSSQL}
          Q_LEE_TIPO_NOMINA: Result:=  'select CB_NOMINA = dbo.SP_KARDEX_CB_NOMINA( :Fecha, :Empleado )';
        {$endif}
        {$ifdef INTERBASE}
          Q_LEE_TIPO_NOMINA: Result:=  'select TIPNOM AS CB_NOMINA from SP_GET_NOMTIPO( :Fecha, :Empleado )';
        {$endif}
{$endif}
     end;
end;

{ ********* THorarioData ********* }

constructor THorarioData.Create( const rJornada, rDobles: TDiasHoras );
begin
     FJornada := rJornada;
     FDobles := rDobles;
end;

{ ********* THorarioList ********* }

constructor THorarioList.Create(oProvider: TdmZetaServerProvider);
begin
     oZetaProvider := oProvider;
     FPtr := -1;
     FCodigo := '';
     Sorted := True;
end;

destructor THorarioList.Destroy;
begin
     Clear;
     inherited Destroy;
end;

procedure THorarioList.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Objects[ i ].Free;
     end;
     inherited Clear;
end;

function THorarioList.GetElemento(Index: Integer): THorarioData;
begin
     if ( Index < Count ) then
        Result := THorarioData( Objects[ Index ] )
     else
         Result := nil;
end;

function THorarioList.Locate(const sCodigo: TCodigo; var iPtr: Integer): Boolean;
begin
     if ( sCodigo = FCodigo ) then
     begin
          Result := True;
          iPtr := FPtr;
     end
     else
     begin
          Result := Find( sCodigo, iPtr );
          if Result then
          begin
               FPtr := iPtr;
               FCodigo := sCodigo;
          end;
     end;
end;

procedure THorarioList.Cargar;
begin
     with oZetaProvider do
     begin
          with CreateQuery( GetSQLScript( Q_JORNADA_DIARIA ) ) do
          begin
               try
                  Active := True;
                  while not Eof do
                  begin
                       AddObject( FieldByName( 'HO_CODIGO' ).AsString,
                                  THorarioData.Create( FieldByName( 'HO_JORNADA' ).AsFloat,
                                                       FieldByName( 'HO_DOBLES' ).AsFloat ) );
                       Next;
                  end;
                  Active := False;
               finally
                      Free;
               end;
          end;
     end;
end;

{ ************ TChecada ************* }

constructor TChecada.Create( oProvider: TdmZetaServerProvider );
begin
  FLlave := TLlaveChecada.Create;
     oZetaProvider := oProvider;
end;

destructor TChecada.Destroy;
begin
  FreeAndNil(FLlave)
end;

procedure TChecada.Preparar;
begin
     case Tipo of
          chConGoce:
          begin
               Ignorar := True;
               Descripcion := dcOrdinaria;
               Extras := 0;
          end;
          chSinGoce:
          begin
               Ignorar := True;
               Descripcion := dcOrdinaria;
               Extras := 0;
          end;
          chExtras:
          begin
               Ignorar := True;
               Descripcion := dcExtras;
               Ordinarias := 0;
          end;
          chDescanso:
          begin
               Ignorar := True;
               Descripcion := dcDescanso;
               Extras := 0;
          end;
          chConGoceEntrada:
          begin
               Ignorar := True;
               Descripcion := dcEntrada;
               Extras := 0;
          end;
          chSinGoceEntrada:
          begin
               Ignorar := True;
               Descripcion := dcEntrada;
               Extras := 0;
          end;
          chPrepagFueraJornada:
          begin
               Ignorar := True;
               Descripcion := dcExtras;
               Ordinarias:= 0;
          end;
          chPrepagDentroJornada:
          begin
               Ignorar := True;
               Descripcion := dcExtras;
               Ordinarias:= 0;
          end;
     else
         begin
              Ignorar := False;
         end;
     end;
end;

procedure TChecada.SetAutorizacion( const iEmpleado: TNumEmp;
                                    const dFecha: TDate;
                                    const eTipo: eTipoChecadas;
                                    const sMotivo: String;
                                    const rValue: TDiasHoras;
                                    const rHorasAprobadas: TDiasHoras;
                                    const iAprobadasPor: integer );
begin
     Empleado := iEmpleado;
     Fecha := dFecha;
     Tipo := eTipo;
     HoraReal := K_HORA_DUMMY;
     HoraAjustada := K_HORA_DUMMY;
     Sistema := False;
     Ordinarias := rValue; { M�todo Agregar / Modificar se encarga de separarlas }
     Extras := rValue;     { M�todo Agregar / Modificar se encarga de separarlas }
     Descanso := 0;
     Reloj := sMotivo;
     Posicion := 0;
     Usuario := oZetaProvider.UsuarioActivo;
     Borrada := False;
     Global := False;
     Aprobadas := rHorasAprobadas;   { Horas Aprobadas }
     AprobadaPor := iAprobadasPor    { Usuario que Aprobo las Horas}
end;

procedure TChecada.Cargar( Dataset: TZetaCursor );
begin
     with Dataset do
     begin
          Empleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          Fecha := FieldByName( 'AU_FECHA' ).AsDateTime;
          Tipo := eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger );
          Descripcion := eDescripcionChecadas( FieldByName( 'CH_DESCRIP' ).AsInteger );
          HoraReal := FieldByName( 'CH_H_REAL' ).AsString;
          {
          HoraAjustada := FieldByName( 'CH_H_AJUS' ).AsString;
          }
          { ProcesarGracias se encarga de cambiarle el valor }
          { cuando hay gracias en el horario de la tarjeta }
          HoraAjustada := HoraReal;
          Global := zStrToBool( FieldByName( 'CH_GLOBAL' ).AsString );
          Ignorar := zStrToBool( FieldByName( 'CH_IGNORAR' ).AsString );
          Sistema := zStrToBool( FieldByName( 'CH_SISTEMA' ).AsString );
          Ordinarias := FieldByName( 'CH_HOR_ORD' ).AsFloat;
          Extras := FieldByName( 'CH_HOR_EXT' ).AsFloat;
          Descanso := FieldByName( 'CH_HOR_DES' ).AsFloat;
          Reloj := FieldByName( 'CH_RELOJ' ).AsString;
          Posicion := FieldByName( 'CH_POSICIO' ).AsInteger;
          Usuario := FieldByName( 'US_CODIGO' ).AsInteger;
          {
          Se usa el mismo campo debido a que CHECADAS.CH_HOR_DES no
          se usa cuando el registro es una autorizaci�n
          }
          Aprobadas := Descanso;
          AprobadaPor := FieldByName( 'US_COD_OK' ).AsInteger;
          Motivo := FieldByName('CH_MOTIVO').AsString;
     end;
end;

procedure TChecada.Agregar( Dataset: TZetaCursor );
begin
     Preparar;
     with oZetaProvider do
     begin
          ParamAsInteger( Dataset, 'CB_CODIGO', Empleado );
          ParamAsDate( Dataset, 'AU_FECHA', Fecha );
          ParamAsChar( Dataset, 'CH_H_REAL', HoraReal, K_ANCHO_HORA );
          ParamAsChar( Dataset, 'CH_H_AJUS', HoraAjustada, K_ANCHO_HORA );
          ParamAsInteger( Dataset, 'CH_TIPO', Ord( Tipo ) );
          ParamAsInteger( Dataset, 'CH_DESCRIP', Ord( Descripcion ) );
          ParamAsBoolean( Dataset, 'CH_GLOBAL', Global );
          ParamAsBoolean( Dataset, 'CH_IGNORAR', Ignorar );
          ParamAsBoolean( Dataset, 'CH_SISTEMA', Sistema );
          ParamAsFloat( Dataset, 'CH_HOR_ORD', MiRound( Ordinarias, 2 ) );
          ParamAsFloat( Dataset, 'CH_HOR_EXT', MiRound( Extras, 2 ) );
          if ( AprobadaPor = 0 ) then
             ParamAsFloat( Dataset, 'CH_HOR_DES', MiRound( Descanso, 2 ) )
          else
              ParamAsFloat( Dataset, 'CH_HOR_DES', MiRound( Aprobadas, 2 ) );
          ParamAsVarChar( Dataset, 'CH_RELOJ', Copy( Reloj, 1, 4 ), K_ANCHO_RELOJ );
          ParamAsInteger( Dataset, 'CH_POSICIO', Posicion );
          ParamAsInteger( Dataset, 'US_CODIGO', Usuario );
          ParamAsInteger( Dataset, 'US_COD_OK', AprobadaPor );
          ParamAsVarChar( Dataset, 'CH_MOTIVO', Motivo, K_ANCHO_RELOJ );          
          Ejecuta( Dataset );
     end;
end;

procedure TChecada.Modificar( Dataset: TZetaCursor );
begin
     Preparar;
     with oZetaProvider do
     begin
          with Llave do
          begin
               ParamAsInteger( Dataset, 'Empleado', Empleado );
               ParamAsDate( Dataset, 'Fecha', Fecha );
               ParamAsChar( Dataset, 'Hora', Hora, K_ANCHO_HORA );
               ParamAsInteger( Dataset, 'Tipo', Ord( Tipo ) );
          end;
          ParamAsInteger( Dataset, 'CB_CODIGO', Empleado );
          ParamAsDate( Dataset, 'AU_FECHA', Fecha );
          ParamAsChar( Dataset, 'CH_H_REAL', HoraReal, K_ANCHO_HORA );
          ParamAsInteger( Dataset, 'CH_TIPO', Ord( Tipo ) );
          ParamAsInteger( Dataset, 'CH_DESCRIP', Ord( Descripcion ) );
          ParamAsBoolean( Dataset, 'CH_IGNORAR', Ignorar );
          ParamAsFloat( Dataset, 'CH_HOR_ORD', MiRound( Ordinarias, 2 ) );
          ParamAsFloat( Dataset, 'CH_HOR_EXT', MiRound( Extras, 2 ) );
          ParamAsInteger( Dataset, 'US_CODIGO', Usuario );
          ParamAsVarChar( Dataset, 'CH_RELOJ', Copy( Reloj, 1, 4 ), K_ANCHO_RELOJ );
          //Campos Agregados para la sugerencia de Horas Aprobadas
          if ( AprobadaPor = 0 ) then
             ParamAsFloat( Dataset, 'CH_HOR_DES', MiRound( Descanso, 2 ) )
          else
              ParamAsFloat( Dataset, 'CH_HOR_DES', MiRound( Aprobadas, 2 ) );
          ParamAsInteger( Dataset, 'US_COD_OK', AprobadaPor );
          ParamAsVarChar( Dataset, 'CH_MOTIVO', Motivo, K_ANCHO_RELOJ );            
          Ejecuta( Dataset );
     end;
end;

{ ************ TChecadas ************ }

constructor TChecadas.Create( oProvider: TdmZetaServerProvider );
begin
     FItems := TList.Create;
     oZetaProvider := oProvider;
end;

destructor TChecadas.Destroy;
begin
     if Assigned( FItems ) then
        Clear;
     FItems.Free;
     Despreparar;
     inherited Destroy;
end;

procedure TChecadas.Preparar;
begin
     with oZetaProvider do
     begin
          FQRead := CreateQuery( GetSQLScript( Q_CHECADAS_LEE ) );
          FQDelete := CreateQuery( GetSQLScript( Q_CHECADAS_BORRA ) );
          FQWrite := CreateQuery( GetSQLScript( Q_CHECADAS_ESCRIBE ) );
     end;
end;

procedure TChecadas.Despreparar;
begin
     FreeAndNil( FQRead );
     FreeAndNil( FQWrite );
     FreeAndNil( FQDelete );
end;

function TChecadas.Add: TChecada;
begin
     Result := TChecada.Create( oZetaProvider );
     FItems.Add( Result );
end;

function TChecadas.EsValida: Boolean;  { Tiene que ser un n�mero PAR de checadas }
begin
     Result := ( FChecadas > 0 ) and ( ( FChecadas mod 2 ) = 0 );
end;

procedure TChecadas.Delete( const Index: Integer );
begin
     Items[ Index ].Free;
     FItems.Delete( Index );
end;

procedure TChecadas.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
         Delete( i );
     FChecadas := 0;
end;

function TChecadas.GetCount: Integer;
begin
     Result := FItems.Count;
end;

function TChecadas.GetItem( Index: Integer ): TChecada;
begin
     Result := TChecada( FItems[ Index ] );
end;

procedure TChecadas.Ignorar( const Index: Integer );
begin
     FChecadas := FChecadas - 1;
     Items[ Index ].Borrada := True;
end;

function Compara( Item1, Item2: Pointer ): Integer;
begin
     if ( TChecada( Item1 ).HoraAjustada < TChecada( Item2 ).HoraAjustada ) then
        Result := -1
     else
         if ( TChecada( Item1 ).HoraAjustada > TChecada( Item2 ).HoraAjustada ) then
            Result := 1
         else
             Result := 0;
end;

procedure TChecadas.Reordena;
begin
     FItems.Sort( Compara );
end;

procedure TChecadas.AgregarSistema( const iEmpleado: TNumEmp; const dFecha: TDate; const sHora: String; const eTipo: eTipoChecadas; const eDescripcion: eDescripcionChecadas );
begin
     with Add do
     begin
          Empleado := iEmpleado;
          Fecha := dFecha;
          HoraReal := sHora;
          HoraAjustada := sHora;
          Tipo := eTipo;
          Descripcion := eDescripcion;
          Sistema := True;
     end;
end;

procedure TChecadas.Cargar( const iEmpleado: TNumEmp; const dFecha: TDate; const iGraciaRepetida: Word );
var
   iHora, iAnterior :Integer;
begin
     with FQRead do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FQRead, 'Empleado', iEmpleado );
               ParamAsDate( FQRead, 'Fecha', dFecha );
          end;
          Active := True;
          iAnterior := -1;
          Clear;
          while not Eof do
          begin
               if zStrToBool( FieldByName( 'CH_IGNORAR' ).AsString ) then
                  AgregarNormal
               else
               begin
                    iHora := aMinutos( FieldByName( 'CH_H_REAL' ).AsString ); { Elimina supuestos y Duplicados }
                    if not zStrToBool( FieldByName( 'CH_SISTEMA' ).AsString ) and
                       ( Abs( iHora - iAnterior ) > iGraciaRepetida ) then
                    begin
                         AgregarNormal;
                         FChecadas := FChecadas + 1;
                         iAnterior := iHora;
                    end;
               end;
               Next;
          end;
          Active := False;
     end;
end;

procedure TChecadas.AgregarNormal;
begin
     with Add do
     begin
          Cargar( FQRead );
          Borrada := False;
     end;
end;

procedure TChecadas.BorrarAnteriores( const iEmpleado: TNumEmp; const dFecha: TDate );
begin
     { Lee las Checadas de un Empleado en una Fecha }
     with oZetaProvider do
     begin
          ParamAsInteger( FQDelete, 'CB_CODIGO', iEmpleado );
          ParamAsDate( FQDelete, 'AU_FECHA', dFecha );
          Ejecuta( FQDelete );
     end;
end;

procedure TChecadas.Descargar( const iEmpleado: TNumEmp; const dFecha: TDate );
var
   i: Integer;
begin
     BorrarAnteriores( iEmpleado, dFecha ); { Borra las checadas ya existentes }
     for i := 0 to ( Count - 1 ) do
     begin
          with Items[ i ] do
          begin
               if not Borrada then
               begin
                    Agregar( FQWrite );
               end;
          end;
     end;
end;

{ ************ TTarjeta *********** }

constructor TTarjeta.Create( oCreator: TZetaCreator );
begin
     FCreator := oCreator;
end;

destructor TTarjeta.Destroy;
begin
{
     if Assigned( FFestivos ) then
        FreeAndNil( FFestivos );
}
     if Assigned( FChecadas ) then
        FreeAndNil( FChecadas );
     if Assigned( FChecada ) then
        FreeAndNil( FChecada );
     ClearHorarios;
     inherited Destroy;
end;

procedure TTarjeta.ClearHorarios;
begin
     if Assigned( FHorarios ) then
        FreeAndNil( FHorarios );
end;

function TTarjeta.GetQueries: TCommonQueries;
begin
     Result := FCreator.Queries;
end;

function TTarjeta.GetRitmos: TRitmos;
begin
     Result := FCreator.Ritmos;
end;

function TTarjeta.oZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

function TTarjeta.GetTurno(const iEmpleado: TNumEmp; const dReferencia: TDate): TCodigo;
begin
     with FQTurno do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsDate( FQTurno, 'FECHA', dReferencia );
               ParamAsInteger( FQTurno, 'EMPLEADO', iEmpleado );
          end;
          Active := True;
          if Eof then
             Result := ''
          else
              Result := FieldByName( 'CB_TURNO' ).AsString;
          Active := False;
     end;
end;

{$ifdef CAMBIO_TNOM}
function TTarjeta.GetTipoNomina(const iEmpleado: TNumEmp; const dReferencia: TDate): eTipoPeriodo;
begin
     with FQTNom do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsDate( FQTNom, 'FECHA', dReferencia );
               ParamAsInteger( FQTNom, 'EMPLEADO', iEmpleado );
          end;
          Active := True;
          if Eof then
             Result := Ord(tpDiario)
          else
              Result := eTipoPeriodo( FieldByName( 'CB_NOMINA' ).AsInteger );
          Active := False;
     end;
end;
{$endif}


procedure TTarjeta.InitGlobalesRedondeo;
begin
     with oZetaProvider do
     begin
          FTablaRedondeoOrdinaria := GetGlobalInteger( K_GLOBAL_TAB_REDON_ORD );
          FTablaRedondeoExtras := GetGlobalInteger( K_GLOBAL_TAB_REDON_X );
     end;
end;

procedure TTarjeta.InitGlobalesTarjeta;
begin
     with oZetaProvider do
     begin
          FGraciaRepetida := GetGlobalInteger( K_GLOBAL_GRACIA_2CHECK );
          FExtraSabado:= eHorasExtras( GetGlobalInteger( K_GLOBAL_TRATO_X_SAB ) );
          FExtraDescanso:= eHorasExtras( GetGlobalInteger( K_GLOBAL_TRATO_X_DESC ) );
          FAutorizarExtras := GetGlobalBooleano( K_GLOBAL_AUT_X );
          FDescuentaComida := GetGlobalBooleano( K_GLOBAL_DESCONTAR_COMIDAS );
          FAprobarAutorizaciones := GetGlobalBooleano( K_GLOBAL_APROBAR_AUTORIZACIONES );
     end;
     InitGlobalesRedondeo;
end;

procedure TTarjeta.InitGlobalesPrenomina;
begin
     with oZetaProvider do
     begin
          FExtraConfianza := GetGlobalBooleano( K_EXTRAS_NO_CHECAN );
          FExtraFestivo:= eHorasExtras( GetGlobalInteger( K_GLOBAL_TRATO_X_FESTIVO ) );
          FAutorizarFestivos := GetGlobalBooleano( K_GLOBAL_AUT_FESTIVO_TRAB );
          FAutorizaExtrasYDescansos := GetGlobalBooleano( K_GLOBAL_EXTRA_DESCANSO );
{$ifdef QUINCENALES}
          FTopaPorSemana := GetGlobalBooleano( K_GLOBAL_TOPE_SEMANAL_DOBLES );
          FDOWCorte := GetGlobalInteger( K_GLOBAL_PRIMER_DIA );
          FTratamientoExtras := eTratamientoExtras( GetGlobalInteger( K_GLOBAL_TRATAMIENTO_TIEMPO_EXTRA ) );
//          if ( FDOWCorte = 7 ) then { Offset porque antes Domingo = 7 (En Delphi Domingo = 1 ) }
//             FDOWCorte := 1
//          else
//              FDOWCorte := FDOWCorte + 1;
{$endif}
          FAutExtYDesSabado := GetGlobalBooleano( K_GLOBAL_EXTRA_DESCANSO_SAB );
          FAutExtYDesDescanso := GetGlobalBooleano( K_GLOBAL_EXTRA_DESCANSO_DES );
{$ifdef IMSS_VACA}
          FDiasCotizaSinFV := GetGlobalBooleano( K_GLOBAL_DIAS_COTIZADOS_FV );
{$endif}
          FFestivoEnDescanso := GetGlobalBooleano( K_GLOBAL_FESTIVO_EN_DESCANSOS );
     end;
     InitGlobalesRedondeo;
end;

procedure TTarjeta.GetTurnoHorarioBegin( const dInicial, dFinal: TDate );
begin
     Ritmos.RitmosBegin( dInicial, dFinal );
     with oZetaProvider do
     begin
          FQTurno := CreateQuery( GetSQLScript( Q_LEE_TURNO ) );
     end;
end;

procedure TTarjeta.GetTurnoHorarioEnd;
begin
     FreeAndNil( FQTurno );
     Ritmos.RitmosEnd;
end;

function TTarjeta.GetTurnoHorario( const iEmpleado: TNumEmp; const dFecha: TDate ): TStatusHorario;
begin
     Result := Ritmos.GetStatusHorario( GetTurno( iEmpleado, dFecha ), dFecha );
end;

procedure TTarjeta.GetJornadaDiariaBegin;
begin
     FHorarios := THorarioList.Create( oZetaProvider );
     FHorarios.Cargar;
end;

procedure TTarjeta.GetJornadaDiariaEnd;
begin
     ClearHorarios;
end;

function TTarjeta.GetJornadaDiaria( const sCodigo: TCodigo ): TDatosJornadaDiaria;
var
   iPtr: Integer;
begin
     with FHorarios do
     begin
          if Locate( sCodigo, iPtr ) then
          begin
               with Elemento[ iPtr ] do
               begin
                    Result.Jornada := Jornada;
                    Result.Dobles := Dobles;
               end;
          end
          else
          begin
               with Result do
               begin
                    Jornada := 0;
                    Dobles := 0;
               end;
          end;
     end;
end;

{$ifdef CAMBIO_TNOM}
procedure TTarjeta.GetTipoNomBegin;
begin
     with oZetaProvider do
     begin
          FQTNom := CreateQuery( GetSQLScript( Q_LEE_TIPO_NOMINA ) );
     end;
end;

procedure TTarjeta.GetTipoNomEnd;
begin
     with oZetaProvider do
     begin
          FreeAndNil( FQTNom );
     end;
end;
{$endif}

{ ******** Investigar Planes de vacaciones *********** }
{$ifdef QUINCENALES}
procedure TTarjeta.SetPlanVacacionesBegin;
begin
     FAplicaPlanVacacion := (  oZetaProvider.GetGlobalBooleano( K_GLOBAL_PLAN_VACA_PRENOMINA ) );
     if FAplicaPlanVacacion then
     begin
          FPlanVacacion := TPlanVacacion.Create( FCreator );
          FPlanVacacion.Preparar;
     end;
end;

procedure TTarjeta.SetPlanVacacionesEnd;
begin
     if Assigned( FPlanVacacion ) then
        FreeAndNil( FPlanVacacion );
end;

procedure TTarjeta.SetPlanVacaciones( const iEmpleado: Integer; const dInicio, dFin: TDate );
begin
     if FAplicaPlanVacacion then
     begin
          with FPlanVacacion do
          begin
               Procesar( iEmpleado,dInicio ,dFin );
               ProcesarPagoEspecUs(iEmpleado);
          end;
     end;
end;
{$endif}

{ ********* Calcular Una Tarjeta ********* }

procedure TTarjeta.LeeUnaTarjetaBegin;
begin
     with oZetaProvider do
     begin
          FTarjetaLee := CreateQuery( GetSQLScript( Q_TARJETA_LEE ) );
     end;
end;

procedure TTarjeta.LeeUnaTarjetaEnd;
begin
     FreeAndNil( FTarjetaLee );
end;

procedure TTarjeta.CalculaUnaTarjetaBegin;
begin
     LeeUnaTarjetaBegin;
     CalculaTarjetaBegin;
end;

procedure TTarjeta.CalculaUnaTarjetaEnd;
begin
     CalculaTarjetaEnd;
     LeeUnaTarjetaEnd;
end;

procedure TTarjeta.CalculaUnaTarjeta( const iEmpleado: TNumEmp; const dFecha: TDate );
begin
     with FTarjetaLee do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FTarjetaLee, 'Empleado', iEmpleado );
               ParamAsDate( FTarjetaLee, 'Fecha', dFecha );
          end;
          Active := True;
          if not Eof then
          begin
               CalculaTarjeta( iEmpleado,
                               dFecha,
                               FieldByName( 'HO_CODIGO' ).AsString,
                               eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger ),
                               FieldByName( 'US_CODIGO' ).AsInteger );
          end;
          Active := False;
     end;
end;

procedure TTarjeta.RedondeoBegin;
begin
     FCreator.PreparaTablasISPT;        // Prepara Tablas ISPT del Creator que le pasaron por Param�tro
{
     with oZetaProvider do
     begin
          FQArt80 := CreateQuery( GetSQLScript( Q_ART_80 ) );
     end;
}
end;

procedure TTarjeta.RedondeoEnd;         // No se destruye TablasISPT - Lo hace el Destroy del Creator
begin
{
     FreeAndNil( FQArt80 );
}
end;

procedure TTarjeta.CalculaTarjetaBegin;
begin
     FChecadas := TChecadas.Create( oZetaProvider );
     with FChecadas do
     begin
          Preparar;
     end;
     with oZetaProvider do
     begin
          FQGrabaTarjeta := CreateQuery( GetSQLScript( Q_TARJETA_GRABA ) );
          FQGrabaGracias := CreateQuery( GetSQLScript( Q_TARJETA_GRABA_GRACIAS ) );
     end;
     Queries.GetDatosHorarioDiarioBegin;
     RedondeoBegin;
     InitGlobalesTarjeta;

     with oZetaProvider do
     begin
          if FDescuentaComida then
             FLeeOut2Eat := CreateQuery( GetSQLScript( Q_LEE_OUT2EAT ) );
     end;

end;

procedure TTarjeta.CalculaTarjetaEnd;
begin
     RedondeoEnd;
     Queries.GetDatosHorarioDiarioEnd;

      if FDescuentaComida then
         FreeAndNil( FLeeOut2Eat );

     FreeAndNil( FQGrabaTarjeta );
     FreeAndNil( FQGrabaGracias );
     FreeAndNil( FChecadas );
end;

procedure TTarjeta.CalculaTarjeta( const iEmpleado: TNumEmp; const dFecha: TDate; const sHorario: TCodigo; const eTipoDia: eStatusAusencia; const iUsuario: Integer );
begin
     if ( iUsuario = 0 ) then { La tarjetas que ya han sido modificadas por el usuario no pueden ser recalculadas }
     begin
          FEmpleado := iEmpleado;
          FFecha := dFecha;
          FSumaOrd := 0;
          FSumaExt := 0;
          FSumaDes := 0;
          FMinTarde := 0;
          FDatosHorario := Queries.GetDatosHorarioDiario( sHorario );
          with FChecadas do
          begin
               Cargar( FEmpleado, FFecha, FGraciaRepetida );
          end;
          ProcesarGracias( FEmpleado, FFecha );
          FChecadas.Reordena;
          CalcularHoras;
          GrabarTotales( eTipoDia );
          with FChecadas do
          begin
               Descargar( FEmpleado, FFecha );
          end;
     end;
end;

procedure TTarjeta.CalcularHoras;
var
   i, iPosEntrada, iPosSalida, iAnterior, iActual, iCalculados: Integer;
   rHoras: Real;
begin
     iAnterior := -1;
     iCalculados := 0;
     iPosEntrada := 1;
     iPosSalida	:= 1;
     for i := 0 to ( FChecadas.Count - 1 ) do
     begin
          with FChecadas.Items[ i ] do
          begin
               if not Borrada and not Ignorar then
               begin
                    { Calcula la diferencia entre este movimiento y el anterior }
                    iActual := aMinutos( HoraAjustada );
                    if ( iAnterior >= 0 ) then
                       iCalculados := ( iActual - iAnterior );
                    rHoras := aHoras( iCalculados );
                    Ordinarias := 0;
                    Extras := 0;
                    Descanso := 0;
                    Posicion := 0;
                    case Tipo of
                         chEntrada:
                         begin
                              if ( Descripcion <> dcExtras ) then { Ej. E-7 a S-17 y regresa a las 19 }
                              begin
                                   Descanso := rHoras;
                                   FSumaDes := FSumaDes + iCalculados;
                              end;
                              Posicion := iPosEntrada;
                              iPosEntrada := iPosEntrada + 1;
                         end;
                         chSalida:
                         begin
                              if ( Descripcion = dcExtras ) then
                              begin
                                   Extras := rHoras;
                                   FSumaExt := FSumaExt + iCalculados;
                              end
                              else
                              begin
                                   Ordinarias := rHoras;
                                   FSumaOrd := FSumaOrd + iCalculados;
                              end;
                              Posicion := iPosSalida;
                              iPosSalida := iPosSalida + 1;
                         end;
                         chInicio:
                         begin
                              if ( Descripcion = dcOrdinaria ) then
                              begin
                                   Extras := rHoras;
                                   FSumaExt := FSumaExt + iCalculados;
                              end
                              else
                              begin
                                   Ordinarias := rHoras;
                                   FSumaOrd := FSumaOrd + iCalculados;
                              end;
                         end;
                         chFin:
                         begin
                              if ( Descripcion = dcOrdinaria ) then
                              begin
                                   Ordinarias := rHoras;
                                   FSumaOrd := FSumaOrd + iCalculados;
                              end
                              else
                              begin
                                   Descanso := rHoras;
                                   FSumaDes := FSumaDes + iCalculados;
                              end;
                         end;
                    end;
                    iAnterior := iActual; { �Por Qu� Hasta Abajo? }
               end; { NOT Borrada }
          end; { with }
     end; { for }
end;

procedure TTarjeta.GrabarTotales( eTipoDia: eStatusAusencia );

 procedure DescontarComida(const iComer: Integer; const lValidar: Boolean );
 begin
      if ( not lValidar ) or
         ( FSumaOrd >= FDatosHorario.MinimoComer ) then
      begin
           FSumaOrd := iMax( ( FSumaOrd - iComer ), 0 );
      end;
 end;

var
   iComer: Integer;
   rHoras, rJornada, rNumExt: Real;
   eDescuentaComida : eOut2Eat;
const
     K_AU_HORASCK = 0;
     K_AU_NUM_EXT = 1;
     K_AU_EXTRAS  = 2;
     K_AU_DOBLES  = 3;
     K_AU_TRIPLES = 4;
     K_AU_TARDES  = 5;
     K_EMPLEADO   = 6;
     K_AU_FECHA   = 7;
begin
     iComer := FDatosHorario.TiempoComer;   { Hora de comida }
     if FDatosHorario.ChecaComer or FDatosHorario.AgregaComer then
     begin
          if FDatosHorario.IgnoraComer then { Checan, pero se ignora las checadas de comida }
          begin
               if ( iComer > 0 ) then
               begin
                    { Si quisieramos realmente ignorar, habr�a que distinguir entre
                    estas dos l�neas}
                    if FDatosHorario.IgnoraCompletamente then
                    begin
                         { CASO Abierto: No Topar }
                         FSumaOrd := FSumaOrd + FSumaDes;
                    end
                    else
                    begin
                         { CASO Estricto: Topar a HORARIO.HO_COMER el tiempo de comida ( CALIMAX ) }
                         FSumaOrd := FSumaOrd + ZetaCommonTools.iMin( iComer, FSumaDes );
                    end;
                    { GA: Esta l�nea sale sobrando }
                    //iComer := 0;
               end
               else
                   FSumaOrd := FSumaOrd + FSumaDes;
          end
          else
              iComer := iMax( ( iComer - FSumaDes ), 0 ); { Asegura que se descuente la hora completa }
     end;
     { -------------------------------------------------- }

     if FDescuentaComida then
     begin
          with FLeeOut2Eat do
          begin
               Active := False;
               with oZetaProvider do
               begin
                    ParamAsInteger( FLeeOut2Eat, 'Empleado', FEmpleado );
                    ParamAsDate( FLeeOut2Eat, 'Fecha', FFecha );
               end;
               Active := True;

               eDescuentaComida := eOut2Eat( FieldByName( 'AU_OUT2EAT' ).AsInteger );

               Active := False;
          end;

          case eDescuentaComida of
               e2Sistema: DescontarComida( iComer, True );
               e2SiComio: DescontarComida( iComer, False );
             { e2NoComio: No se Hace Nada }
          end;
     end
     else
     begin
          DescontarComida( iComer, True);
     end;

     if ( FDatosHorario.MinComExtras > 0 ) and ( FSumaExt >= FDatosHorario.MinComExtras ) then
        FSumaExt := iMax( FSumaExt - FDatosHorario.ComidaExtras, 0 );
     { -------------------------------------------------- }
     { DIAS DE DESCANSO, HORAS SON EXTRAS }
     { A menos que permita pagar 'Descanso Trabajado + Extras' }
     if NoEsHabil( eTipoDia, FExtraSabado, FExtraDescanso ) then
     begin
          FSumaExt := FSumaExt + FSumaOrd;
	  FSumaOrd := 0;
     end;
     { Totales de ese dia }
     rJornada := FDatosHorario.Jornada;
     rHoras := RedondeoOrdinarias( FSumaOrd, FFecha );
     if FDatosHorario.ConHorario then { Horarios Normales, topado a jornada }
        rHoras := rMin( rHoras, rJornada )
     else        { Si no tienen horario, pero se define una Jornada diaria }
     begin       { el excedente a la jornada se convierte en tiempo extra }
          if ( rHoras > rJornada ) and ( rJornada > 0 ) then
          begin
	       FSumaExt := HorasAMinutos( rHoras - rJornada );
	       rHoras := rJornada;
          end;
     end;
     with oZetaProvider do     { Graba los totales diarios en Ausencia }
     begin
          if ( FChecadas.EsValida ) or ( FTotPreExt > 0 ) then
          begin
               ParamAsFloat( FQGrabaTarjeta, 'AU_HORASCK', MiRound( rHoras, 2 ) );
               rNumExt := RedondeoExtras( FSumaExt, FFecha );
               ParamAsFloat( FQGrabaTarjeta, 'AU_NUM_EXT', rNumExt );
               if FAutorizarExtras then
               begin
                    rHoras := FTotAutExt;
                    if ( rHoras < rNumExt ) then
                       rNumExt := rHoras;
               end;
               rNumExt:= FTotPreExt + rNumExt; { Extras prepagadas + Horas extras}
               ParamAsFloat( FQGrabaTarjeta, 'AU_EXTRAS', rNumExt );
               if ( rNumExt > FDatosHorario.Dobles ) then
               begin
                    rHoras := MiRound( FDatosHorario.Dobles, 2 );
                    ParamAsFloat( FQGrabaTarjeta, 'AU_DOBLES', rHoras );
                    ParamAsFloat( FQGrabaTarjeta, 'AU_TRIPLES', MiRound( rNumExt - rHoras, 2 ) );
               end
               else
               begin
                    ParamAsFloat( FQGrabaTarjeta, 'AU_DOBLES', rNumExt );
                    ParamAsFloat( FQGrabaTarjeta, 'AU_TRIPLES', 0 );
               end;
               if ( eTipoDia = auHABIL ) then
                  ParamAsFloat( FQGrabaTarjeta, 'AU_TARDES', aHoras( FMinTarde ) )
               else
                   ParamAsFloat( FQGrabaTarjeta, 'AU_TARDES', 0 );
          end
          else
          begin
               ParamAsFloat( FQGrabaTarjeta, 'AU_HORASCK', 0 );
               ParamAsFloat( FQGrabaTarjeta, 'AU_NUM_EXT', 0 );
               ParamAsFloat( FQGrabaTarjeta, 'AU_EXTRAS', 0 );
               ParamAsFloat( FQGrabaTarjeta, 'AU_DOBLES', 0 );
               ParamAsFloat( FQGrabaTarjeta, 'AU_TRIPLES', 0 );
               ParamAsFloat( FQGrabaTarjeta, 'AU_TARDES', 0 );
          end;
          ParamAsInteger( FQGrabaTarjeta, 'CB_CODIGO', FEmpleado );
          ParamAsDate( FQGrabaTarjeta, 'AU_FECHA', FFecha );
          Ejecuta( FQGrabaTarjeta );
     end;
end;

procedure TTarjeta.ProcesarGracias( const iEmpleado: TNumEmp; const dFecha: TDate );
const
     K_AU_AUT_EXT = 0;
     K_AU_AUT_TRA = 1;
     K_AU_PER_CG = 2;
     K_AU_PER_SG = 3;
     K_CB_CODIGO = 4;
     K_AU_FECHA = 5;
var
   i, iHora: Integer;
   sHora: String;
   lExtraAntes, lEntrada, lHuboEntrada, lHuboSalida: Boolean;
   wEntrada, wMinEntrada, wMaxEntrada, wSalida, wMinSalida, wMaxSalida: Word;
   wPermisoIn, wEntPuntual:{$ifdef ANTES}Word{$else}Integer{$endif};
   wPrimeraHora, wUltimaHora: Integer;
   rTotPerCG, rTotPerSG, rTotAutDes, rAprobadas: Extended;
   Checada: TChecada;

function Topa99( const rValor: Real ): Real;
begin
     if ( rValor > 99 ) then
        Result := 99
     else
         Result := MiRound( rValor, 2 );
end;

function MasQueCero( const iValor: Integer ): Integer;
begin
     if ( iValor < 0 ) then
        Result := 0
     else
         Result := iValor;
end;

procedure AgregaComidas( wComeOut, wComeIn: Word );
begin
     if ( wComeOut = 0 ) and ( wComeIn = 0 ) then
        Exit;
     if ( wComeOut < wEntrada ) then
        wComeOut := wComeOut + K_24HORAS;
     if ( wComeIn < wComeOut ) then
        wComeIn := wComeIn + K_24HORAS;
     if ( wPrimeraHora < wComeIn ) and ( wUltimaHora > wComeIn ) then
     begin
          if ( wPrimeraHora <= wComeOut ) then
	     FChecadas.AgregarSistema( iEmpleado, dFecha, aHoraString( wComeOut ), chInicio, dcComida );
          FChecadas.AgregarSistema( iEmpleado, dFecha, aHoraString( wComeIn ), chFin, dcComida );
     end;
end;

function GetAutorizadas: Real;
begin
     with Checada do
     begin
          case Tipo of
               chExtras, chPrepagFueraJornada, chPrepagDentroJornada: Result := Extras;
          else
              Result := Ordinarias;
          end;
          if FAprobarAutorizaciones then
          begin
               Result := ZetaCommonTools.rMin( Result, Aprobadas );
          end;
     end;
end;

begin
     wPermisoIn:= 0;
     wEntPuntual := {$ifdef ANTES}0{$else}-1{$endif}; { No hay ninguna entrada puntual identificada }
     lEntrada := True;
     lHuboEntrada := False;
     lHuboSalida := False;
     lExtraAntes := False;
     rTotPerCG := 0;
     rTotPerSG := 0;
     rTotAutDes := 0;
     FTotPreExt := 0;
     { ANTERIORMENTE ESTO ESTABA EN UN WITH, PERO CON LOS PROYECTOS DE SUPERVISORES Y DISENADOR }
     { MARCABA UNINTERNAL ERROR C186 -FAVOR DE DEJARLO ASI. -CV }
     FTotAutExt := FDatosHorario.ExtrasFijas;
     wEntrada := FDatosHorario.Entra24;
     wMinEntrada := wEntrada - FDatosHorario.GraciaInTemprano;
     wMaxEntrada := wEntrada + FDatosHorario.GraciaInTarde;
     wSalida := FDatosHorario.Sale24;
     wMinSalida := wSalida - FDatosHorario.GraciaOutTemprano;
     wMaxSalida := wSalida + FDatosHorario.GraciaOutTarde;
     if FDatosHorario.AgregaComer then 	{ Estos datos s�lo se usan para agregar comidas/descanso }
     begin
          with FChecadas do
          begin
               if ( Count > 0 ) then	{ Si hay checadas, las tomo de la lista ordenada via query }
               begin
                    wPrimeraHora := aMinutos( Items[ 0 ].HoraReal ); 	{ H.Real de 1a. Checada }
                    wUltimaHora :=  aMinutos( Items[ Count - 1 ].HoraReal );	{ y de la Ultima }
               end
               else	{ Si no hay, pongo valores extremos para que no se cumplan }
               begin
	            wPrimeraHora := K_24HORAS * 2;
		    wUltimaHora := -K_24HORAS;	{ ( -1 * K_24_HORAS ); }
               end;
          end;
     end;
     for i := 0 to ( FChecadas.Count - 1 ) do
     begin
          Checada := FChecadas.Items[ i ];
          with Checada do
          begin
               if Ignorar then
               begin
                    rAprobadas := GetAutorizadas;
                    case Tipo of
                         chExtras: FTotAutExt := FTotAutExt + rAprobadas;      { Autorizaciones de Tiempo Extra  }
                         chConGoce, chConGoceEntrada: { Permisos con Goce Ordinarios y de Entrada }
                         begin
                              rTotPerCG := rTotPerCG + rAprobadas;
                              if ( Tipo = chConGoceEntrada ) then { Perdonan el Retardo }
                                 wPermisoIn := wPermisoIn + HorasAMinutos( rAprobadas );
                         end;
                         chSinGoce, chSinGoceEntrada: { Permisos sin Goce Ordinarios y de Entrada }
                         begin
                              rTotPerSG := rTotPerSG + rAprobadas;
                              if ( Tipo = chSinGoceEntrada ) then { Perdonan el Retardo }
                                 wPermisoIn := wPermisoIn + HorasAMinutos( rAprobadas );
                         end;
                         chDescanso: rTotAutDes := rTotAutDes + rAprobadas;    { Autorizaciones de Descanso Trabajado }
                         chPrepagFueraJornada, chPrepagDentroJornada:   { Autorizaciones de Extras Prepagadas}
                         begin
                              FTotPreExt := FTotPreExt + rAprobadas;  {Se suma a las autorizadas}
                         end;
                    end;
               end
               else { No ignorar }
               begin
                    sHora := HoraReal;
                    iHora := aMinutos( sHora );
                    if not FDatosHorario.ConHorario then	{ No tienen horario diario }
                    begin
                         if lEntrada then { Marca como Entrada Ordinario }
                         begin
                              Tipo := chEntrada;
                              Descripcion := dcOrdinaria;
                              lHuboEntrada := True;
                              lHuboSalida := False;
                         end
                         else             { Marca como Salida Ordinario }
                         begin
                              Tipo := chSalida;
                              Descripcion := dcOrdinaria;
                              lHuboSalida := True;
                         end;
                    end
                    else
                    if ( iHora < wMinEntrada ) then	{ Se dobla antes de la entrada }
                    begin
                         if lEntrada then    { Marcar como Entrada Extra }
                         begin
                              Tipo := chEntrada;
                              Descripcion := dcExtras;
                         end
                         else             { Marcar como Salida Extra }
                         begin
                              Tipo := chSalida;
                              Descripcion := dcExtras;
                         end;
                         lExtraAntes := not lExtraAntes;
                    end
                    else
                    if ( iHora <= wMaxEntrada ) then	{ Entrada Puntual }
                    begin
                         if lEntrada then     { Marca como Entrada Ordinario }
                         begin
                              Tipo := chEntrada;
                              Descripcion := dcPuntual;
                              HoraAjustada := aHoraString( wEntrada );
                              lHuboEntrada := True;
                              wEntPuntual := i;
                         end
                         else { Otra Entrada Puntual }
{$ifndef ANTES}
                             { No hay una entrada puntual anterior a esta checada }
                             if ( wEntPuntual < 0 ) then
                             begin
                                  { GA: La checada, aunque tiene la misma hora que}
                                  { la entrada puntual del horario, no es entrada, }
                                  { es una salida Extras }
                                  Tipo := chSalida;
                                  Descripcion := dcExtras;
                                  lExtraAntes := not lExtraAntes;
                             end
                             else
{$endif}
                             begin
                                  lEntrada := True;
                                  { GA: Hay que resetear cualquier suposici�n de checadas }
                                  { anteriores a esta, ya que la anterior se borra }
                                  { 21/Sep/2000 }
                                  lExtraAntes := False;
                                  Tipo := chEntrada;
                                  Descripcion := dcPuntual;
                                  HoraAjustada := aHoraString( wEntrada );
                                  FChecadas.Ignorar( wEntPuntual );  { Borro la anterior; esta es m�s tarde }
                                  {
                                  GA - 9/Jun/2005: oZetaProvider.Log.Advertencia( CONVENDRIA HACERLO, PERO �EXISTE UN LOG ABIERTO EN TODOS LOS CASOS? );
                                  }
                                  wEntPuntual := i;  { Ahora esta va a ser la puntual }
                             end
                    end
                    else	{ Ya se brincaron la hora de entrada }
                    begin
                         if lExtraAntes then    { Agrega Inicio Ordinaria }
                         begin
                              sHora := aHoraString( wEntrada );
                              FChecadas.AgregarSistema( iEmpleado, dFecha, sHora, chInicio, dcOrdinaria );
                              lExtraAntes := False;
                              lHuboEntrada := True;
                         end;
                         if ( iHora < wMinSalida ) then { Entrada o Salida en Jornada }
                         begin
                              if lEntrada then   { Marca como Entrada Ordinario }
                              begin
                                   Tipo := chEntrada;
                                   { Si es la primera entrada, retardo }
                                   if not lHuboEntrada then
                                   begin
                                        { A menos que tenga permiso de Ma�ana }
                                        FMinTarde := MasQueCero( MasQueCero( iHora - wEntrada ) - wPermisoIn );
                                        if ( FMinTarde = 0 ) then
                                           Descripcion := dcOrdinaria
                                        else
                                            Descripcion := dcRetardo;
                                   end
                                   else
                                       Descripcion := dcOrdinaria;
                                   lHuboEntrada := True;
                                   lHuboSalida := False;
                              end
                              else { Marca como Salida Ordinario }
                              begin
                                   Tipo := chSalida;
                                   Descripcion := dcOrdinaria;
                                   lHuboSalida := True;
                              end;
                         end
                         else
                         begin
                              if ( iHora <= wMaxSalida ) then   { Salida Puntual }
                              begin
                                   if not lEntrada then { Marca como Salida Ordinario }
                                   begin
                                        Tipo := chSalida;
                                        Descripcion := dcPuntual;
                                        HoraAjustada := aHoraString( wSalida );
                                        lHuboSalida := True;
                                   end
                                   else { Marca como Entrada Extra }
                                   begin
                                        Tipo := chEntrada;
                                        Descripcion := dcExtras;
                                        // DES US #17003:
										// SKYWORKS- Problema con c�lculo de horas en pre-n�mina (Integrar a 2018)                                        
                                        wMaxSalida := iHora;     // Si hay entrada extra se anula la gracia despues
                                   end;
                              end
                              else	{ Ya se brincaron la hora de salida }
                              begin
                                   if lHuboEntrada and not lHuboSalida then { Agrega Fin Ordinaria }
                                   begin
                                        sHora := aHoraString( wSalida );
                                        FChecadas.AgregarSistema( iEmpleado, dFecha, sHora, chFin, dcOrdinaria );
                                        lHuboEntrada := False;
                                   end;
                                   if lEntrada then  { Marca como Entrada Extra }
                                   begin
                                        Tipo := chEntrada;
                                        Descripcion := dcExtras;
                                   end
                                   else { Marca como Salida Extra }
                                   begin
                                        Tipo := chSalida;
                                        Descripcion := dcExtras;
                                   end;
                              end;
                         end;
                    end;
                    lEntrada := not lEntrada;
               end;  { No ignorar }
          end; { with }
     end; { for i }
     with FDatosHorario do  { Agregar HORAS DE COMIDA solo despues }
     begin                  { de que hay un movimiento posterior }
          if AgregaComer then
          begin
               AgregaComidas( SaleAComer, RegresaDeComer );
               AgregaComidas( SaleADescanso, RegresaDeDescanso );
          end;
     end;
     with oZetaProvider do
     begin
          ParamAsFloat( FQGrabaGracias, 'AU_AUT_EXT', Topa99( FTotAutExt ) );
          ParamAsFloat( FQGrabaGracias, 'AU_AUT_TRA', Topa99( rTotAutDes ) );
          ParamAsFloat( FQGrabaGracias, 'AU_PER_CG', Topa99( rTotPerCG ) );
          ParamAsFloat( FQGrabaGracias, 'AU_PER_SG', Topa99( rTotPerSG ) );
          ParamAsFloat( FQGrabaGracias, 'AU_PRE_EXT', Topa99( FTotPreExt ) );
          ParamAsInteger( FQGrabaGracias, 'CB_CODIGO', FEmpleado );
          ParamAsDate( FQGrabaGracias, 'AU_FECHA', FFecha );
          Ejecuta( FQGrabaGracias );
     end;
end;

function TTarjeta.RedondeoOrdinarias( const iMinutos: Integer; const dFecha: TDate ): Extended;
begin
     Result := Redondeo( iMinutos, FTablaRedondeoOrdinaria, dFecha );
end;

function TTarjeta.RedondeoExtras( const iMinutos: Integer; const dFecha: TDate ): Extended;
begin
     Result := Redondeo( iMinutos, FTablaRedondeoExtras, dFecha );
end;

function TTarjeta.RedondeoHorasExtras( const rHoras: Extended; const dFecha: TDate ): Extended;
begin
     Result := RedondeoExtras( Trunc( ZetaCommonTools.MiRound( 60 * rHoras, 0 ) ), dFecha );
end;

function TTarjeta.Redondeo( const iMinutos, iTabla: Integer; const dFecha: TDate ): Extended;
begin
     if ( iTabla <= 0 ) then
        Result := ZetaCommonTools.aHoras( iMinutos )	{ Sin redondeo }
     else
	 Result := ZetaCommonTools.MiRound( FCreator.dmTablasISPT.ConsultaTablaNumerica( iTabla, iMinutos, dFecha ).Cuota, 2 );
{
	 Result := ZetaCommonTools.MiRound( GetArt80Renglon( iTabla, iMinutos ).Cuota, 2 );
}
     if ( Result < 0 ) then
        Result := 0;
     if ( Result > 99 ) then
        Result := 99;
end;

{
function TTarjeta.GetArt80Renglon( const iTabla: Integer; const rValor: Real ): TRenglonNumerica;
begin
     with FQArt80 do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FQArt80, 'Tabla1', iTabla );
               ParamAsInteger( FQArt80, 'Tabla2', iTabla );
               ParamAsFloat( FQArt80, 'Valor', rValor );
          end;
          Active := True;
          with Result do
          begin
               Inferior := FieldByName( 'A80_LI' ).AsFloat;
               Cuota := FieldByName( 'A80_CUOTA' ).AsFloat;
               Tasa := FieldByName( 'A80_TASA' ).AsFloat;
          end;
          Active := False;
     end;
end;
}

{ *********** Poll de Asistencia ************ }

procedure TTarjeta.DoPollBegin( const dInicial, dFinal: TDate );
begin
     GetTurnoHorarioBegin( dInicial, dFinal );
     CalculaTarjetaBegin;
     with oZetaProvider do
     begin
          FChecadaAgrega := CreateQuery( GetSQLScript( Q_POLL_AGREGA_CHECADA ) );
          {$ifdef MSSQL}
          ParamSalida( FChecadaAgrega, 'CB_CODIGO' );
          ParamSalida( FChecadaAgrega, 'AU_FECHA' );
          ParamSalida( FChecadaAgrega, 'HO_CODIGO' );
          ParamSalida( FChecadaAgrega, 'AU_STATUS' );
          ParamSalida( FChecadaAgrega, 'AU_AUT_EXT' );
          ParamSalida( FChecadaAgrega, 'US_CODIGO' );
          ParamSalida( FChecadaAgrega, 'STATUS_EMP' );
          {$endif}
     end;
end;

procedure TTarjeta.DoPollEnd;
begin
     FreeAndNil( FChecadaAgrega );
     CalculaTarjetaEnd;
     GetTurnoHorarioEnd;
end;

function TTarjeta.DoPoll( const iEmpleado: TNumEmp; dFecha: TDate; const sReloj, sCredencial, sHora: String; const lManejaTrans: Boolean ): Boolean;
var
   oDatosTurno: TEmpleadoDatosTurno;
   eStatus: eStatusEmpleado;
   sAjuste: String;
   dAjuste: TDate;
   bTarjetaValida, bProcesaTarjetasInvalidas : Boolean;

   procedure IncluirAdvertencia(const eClase: eClaseBitacora; const iEmpleado: Integer;
             const dFecha: TDate; const sMensaje: TBitacoraTexto; const sTexto: String);
   begin
        with oZetaProvider do
        begin
             if Assigned( Log ) then
                Log.Advertencia( eClase, iEmpleado, dFecha, sMensaje, sTexto )
             else
                 EscribeBitacora( tbAdvertencia, eClase, iEmpleado, dFecha, sMensaje, sTexto );
        end;
   end;

begin
     Result := False;
     eStatus := steEmpleado;
     dAjuste := dFecha;
     sAjuste := sHora;
     oDatosTurno := Ritmos.GetEmpleadoDatosTurno( iEmpleado, dAjuste );
     { Aqu� si puede suceder que el empleado no existe pues el n�mero de }
     { empleado viene de un archivo ASCII externo
     { En caso de no existir, el GetDatosTurno puede }
     { asignar el Codigo de Turno como vac�o }
     { Aqu� detectamos eso y generamos un error de bit�cora }
     with oZetaProvider do
     begin
          if StrLleno( oDatosTurno.Codigo ) then
          begin
               if StrLleno( oDatosTurno.StatusHorario.Horario ) then
               begin
                    // (MV) Si la credencial viene vacio, ya es una tarjeta Valida.
                    bTarjetaValida := StrVacio( sCredencial );

                    if not bTarjetaValida then
                    begin
                         // (JB) Valida letra de la tarjeta
                         bTarjetaValida := ( ( Pos( TOKEN_ZK + TOKEN_ASISTENCIA, sReloj ) > 0 ) or ( sCredencial = oDatosTurno.Credencial ) );
                    end;

                    // (JB) Se permite procesar tarjetas invalidas
                    bProcesaTarjetasInvalidas := GetGlobalBooleano( K_GLOBAL_PROCESA_CRED_INVALIDAS );

                    Ritmos.DeterminaDia( iEmpleado, dAjuste, sAjuste, oDatosTurno );

                    {$ifdef INTERBASE}
                    with FChecadaAgrega do
                    begin
                         Active := False;
                    end;
                    {$endif}
                    ParamAsInteger( FChecadaAgrega, 'Empleado', iEmpleado );
                    ParamAsDate( FChecadaAgrega, 'Fecha', dAjuste );
                    ParamAsChar( FChecadaAgrega, 'Hora', sAjuste, K_ANCHO_HORA );
                    ParamAsChar( FChecadaAgrega, 'Reloj', Copy( sReloj, 1, 4 ), K_ANCHO_RELOJ );
                    with oDatosTurno.StatusHorario do
                    begin
                         ParamAsChar( FChecadaAgrega, 'Horario', Horario, K_ANCHO_HORARIO );
                         ParamAsInteger( FChecadaAgrega, 'Status', Ord( Status ) );
                    end;
                    if lManejaTrans then
                       EmpiezaTransaccion;
                    try
                         // (JB) Segun la regla de la doble negacion !(!A y !B) = (A o B)
                         if ( bTarjetaValida or bProcesaTarjetasInvalidas ) then
                         begin
                              {$ifdef INTERBASE}
                              with FChecadaAgrega do
                              begin
                                   Active := True;
                                   CalculaTarjeta( FieldByName( 'CB_CODIGO' ).AsInteger,
                                                   FieldByName( 'AU_FECHA' ).AsDateTime,
                                                   FieldByName( 'HO_CODIGO' ).AsString,
                                                   eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger ),
                                                   FieldByName( 'US_CODIGO' ).AsInteger );
                                   eStatus := eStatusEmpleado( FieldByName( 'STATUS_EMP' ).AsInteger );
                                   Active := False;
                              end;
                              {$endif}
                              {$ifdef MSSQL}
                              Ejecuta( FChecadaAgrega );
                              CalculaTarjeta( GetParametro( FChecadaAgrega, 'CB_CODIGO' ).AsInteger,
                                              GetParametro( FChecadaAgrega, 'AU_FECHA' ).AsDateTime,
                                              GetParametro( FChecadaAgrega, 'HO_CODIGO' ).AsString,
                                              eStatusAusencia( GetParametro( FChecadaAgrega, 'AU_STATUS' ).AsInteger ),
                                              GetParametro( FChecadaAgrega, 'US_CODIGO' ).AsInteger );
                              eStatus := eStatusEmpleado( GetParametro( FChecadaAgrega, 'STATUS_EMP' ).AsInteger );
                              {$endif}
                         end;
                         
                         if lManejaTrans then
                            TerminaTransaccion( True );
                         Result := True;
                    except
                          on Error: Exception do
                          begin
                               //TerminaTransaccion( False );
                               if lManejaTrans then
                                  RollBackTransaccion;
                               if Assigned( Log ) then
                                  Log.Excepcion( iEmpleado, 'Error Al Grabar Checada Del ' + FechaCorta( dAjuste ) + ' Hora: ' + sHora, Error )
                               else
                                   EscribeBitacora( tbErrorGrave, clbTarjeta, iEmpleado, dAjuste, 'Error Al Grabar Checada Del ' + FechaCorta( dAjuste ) + ' Hora: ' + sHora, Error.Message );
                          end;
                    end;


                    if Result then
                    begin
                         if not bTarjetaValida then
                         begin
                              Result := bProcesaTarjetasInvalidas;
                              // (JB) CR 1776 T 994 - Se agrega la advertencia en Log nuevamente para ligar con el tipo de bitacora.
                              IncluirAdvertencia( clbCredencialInvalida, iEmpleado, dFecha, 'Credencial ' + sCredencial + ' Inv�lida D�a ' + FechaCorta( dFecha ) + ' Hora ' + sHora, VACIO );
                         end;
                         if ( eStatus <> steEmpleado ) then
                            IncluirAdvertencia( clbTarjeta, iEmpleado, dFecha, 'D�a ' + FechaCorta( dAjuste ) + ' Irregular: ' + ObtieneElemento( lfStatusEmpleado, Ord( eStatus ) ), VACIO );
                    end;
               end
               else
                   IncluirAdvertencia( clbTarjeta, iEmpleado, dFecha, 'Horario Vac�o Para Turno ' + oDatosTurno.Codigo + ' En ' + FechaCorta( dAjuste ), VACIO );
          end
          else
              IncluirAdvertencia( clbTarjeta, iEmpleado, dFecha, 'Empleado No Existe / Sin Turno En ' + FechaCorta( dAjuste ) , VACIO );
     end;
end;

{ ****** Agregar Tarjeta Diaria *********** }

procedure TTarjeta.TarjetaExisteBegin;
begin
     with oZetaProvider do
     begin
          FTarjetaExiste := CreateQuery( GetSQLScript( Q_TARJETA_EXISTE ) );
     end;
end;

procedure TTarjeta.TarjetaExisteEnd;
begin
     FreeAndNil( FTarjetaExiste );
end;

procedure TTarjeta.AgregaTarjetaDiariaBegin( const dInicial, dFinal: TDate );
begin
     with oZetaProvider do
     begin
          FTarjetaEscribe := CreateQuery( GetSQLScript( Q_TARJETA_AGREGA ) );
     end;
     TarjetaExisteBegin;
     GetTurnoHorarioBegin( dInicial, dFinal );
     CalculaTarjetaBegin;
end;

procedure TTarjeta.AgregaTarjetaDiariaEnd;
begin
     CalculaTarjetaEnd;
     GetTurnoHorarioEnd;
     TarjetaExisteEnd;
     FreeAndNil( FTarjetaEscribe );
end;

function TTarjeta.AgregaTarjetaDiariaExiste( const iEmpleado: TNumEmp; const dFecha: TDate; var DatosTarjeta: TDatosTarjeta ):Boolean;
begin
     with oZetaProvider do
     begin
          with FTarjetaExiste do
          begin
               Active := False;
               ParamAsInteger( FTarjetaExiste, 'Empleado', iEmpleado );
               ParamAsDate( FTarjetaExiste, 'Fecha', dFecha );
               Active := True;
               Result := not Eof;
               if Result then
               begin
                    with DatosTarjeta do
                    begin
                         Horario := FieldByName( 'HO_CODIGO' ).AsString;
                         Status := eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger );
                         EsManual := ZetaCommonTools.zStrToBool( FieldByName( 'AU_HOR_MAN' ).AsString );
                         Usuario := FieldByName( 'US_CODIGO' ).AsInteger;
                    end;
               end
               else //Si no lo encuentra regresa los datos del horario para la fecha
               begin
                    with GetTurnoHorario( iEmpleado, dFecha ) do
                    begin
                         DatosTarjeta.Horario := Horario;
                         DatosTarjeta.Status := Status;
                    end;

                    DatosTarjeta.EsManual := False;
               end;
               Active := False;
          end;
     end;
end;

procedure TTarjeta.AgregaTarjetaDiariaEscribe( const iEmpleado: TNumEmp; const dFecha: TDate;var DatosTarjeta: TDatosTarjeta );
begin
     with oZetaProvider do
     begin
          {$ifdef INTERBASE}
          with FTarjetaEscribe do
          begin
               Active := False;
          end;
          {$endif}
          ParamAsInteger( FTarjetaEscribe, 'Empleado', iEmpleado );
          ParamAsDate( FTarjetaEscribe, 'Fecha', dFecha );
          with DatosTarjeta do
          begin
               ParamAsChar( FTarjetaEscribe, 'Horario', Horario, K_ANCHO_HORARIO );
               ParamAsInteger( FTarjetaEscribe, 'Status', Ord( Status ) );
          end;
          {$ifdef INTERBASE}
          with FTarjetaEscribe do
          begin
               Active := True;  { Agrega y Clasifica Tarjeta }
               with DatosTarjeta do
               begin
                    Usuario := FieldByName( 'US_CODIGO' ).AsInteger;
               end;
               Active := False; { � No se usa el resultado para nada ? }
          end;
          {$endif}
          {$ifdef MSSQL}
          ParamSalida( FTarjetaEscribe, 'CB_CODIGO' );
          ParamSalida( FTarjetaEscribe, 'AU_FECHA' );
          ParamSalida( FTarjetaEscribe, 'HO_CODIGO' );
          ParamSalida( FTarjetaEscribe, 'AU_STATUS' );
          ParamSalida( FTarjetaEscribe, 'AU_AUT_EXT' );
          ParamSalida( FTarjetaEscribe, 'US_CODIGO' );
          Ejecuta( FTarjetaEscribe );
          DatosTarjeta.Usuario := GetParametro( FTarjetaEscribe, 'US_CODIGO' ).AsInteger;
          {$endif}
     end;
end;

function TTarjeta.AgregaTarjetaDiaria( const iEmpleado: TNumEmp; const dFecha: TDate ): TDatosTarjeta;
begin
     if not AgregaTarjetaDiariaExiste( iEmpleado, dFecha, Result ) then
        AgregaTarjetaDiariaEscribe( iEmpleado, dFecha, Result );
end;

{ ************** Cambiar Datos de Tarjeta ( Pantallas de Registro ) ********** }

procedure TTarjeta.CambiarTarjetaBegin;
begin
     with oZetaProvider do
     begin
          FTarjetaCambia := CreateQuery( GetSQLScript( Q_TARJETA_CAMBIA ) );
          FTarjetaEscribe := CreateQuery( GetSQLScript( Q_TARJETA_AGREGA ) );
     end;
     {
     TarjetaExisteBegin;
     }
     CalculaTarjetaBegin;
end;

procedure TTarjeta.CambiarTarjetaEnd;
begin
     CalculaTarjetaEnd;
     {
     TarjetaExisteEnd;
     }
     FreeAndNil( FTarjetaCambia );
     FreeAndNil( FTarjetaEscribe );
end;

procedure TTarjeta.CambiarTarjeta( const iEmpleado: TNumEmp; const dFecha: TDate; const sHorario: String; const eStatus: eStatusAusencia; const lCambioHorario, lCambioStatus: Boolean);
var
   iUsuario: Integer;
begin
     with oZetaProvider do
     begin
          with FTarjetaEscribe do
          begin
{$ifdef INTERBASE}
               Active := False;  { Agrega y Clasifica Tarjeta }
{$endif}
               ParamAsInteger( FTarjetaEscribe, 'Empleado', iEmpleado );
               ParamAsDate( FTarjetaEscribe, 'Fecha', dFecha );
               ParamAsChar( FTarjetaEscribe, 'Horario', sHorario, K_ANCHO_HORARIO );
               ParamAsInteger( FTarjetaEscribe, 'Status', Ord( eStatus ) );
{$ifdef INTERBASE}
               Active := True;  { Agrega y Clasifica Tarjeta }
               iUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
               Active := False;
{$endif}
          end;
{$ifdef MSSQL}
          ParamSalida( FTarjetaEscribe, 'CB_CODIGO' );
          ParamSalida( FTarjetaEscribe, 'AU_FECHA' );
          ParamSalida( FTarjetaEscribe, 'HO_CODIGO' );
          ParamSalida( FTarjetaEscribe, 'AU_STATUS' );
          ParamSalida( FTarjetaEscribe, 'AU_AUT_EXT' );
          ParamSalida( FTarjetaEscribe, 'US_CODIGO' );
          Ejecuta( FTarjetaEscribe );
          iUsuario := GetParametro( FTarjetaEscribe, 'US_CODIGO' ).AsInteger;
{$endif}
          if lCambioHorario or lCambioStatus then
          begin
               ParamAsDate( FTarjetaCambia, 'Fecha', dFecha );
               ParamAsInteger( FTarjetaCambia, 'Empleado', iEmpleado );
               ParamAsInteger( FTarjetaCambia, 'Status', Ord( eStatus ) );
               ParamAsChar( FTarjetaCambia, 'EsManual', K_GLOBAL_SI, K_ANCHO_BOOLEANO );
               ParamAsChar( FTarjetaCambia, 'Horario', sHorario, K_ANCHO_HORARIO );
               Ejecuta( FTarjetaCambia );
          end;
     end;
     CalculaTarjeta( iEmpleado, dFecha, sHorario, eStatus, iUsuario );
end;

{ ************* Agregar Checadas ************ }

procedure TTarjeta.AgregaChecadasBegin;
begin
     FChecada := TChecada.Create( oZetaProvider );
     with oZetaProvider do
     begin
          FChecadaAgrega := CreateQuery( GetSQLScript( Q_CHECADAS_ESCRIBE ) );
     end;
end;

procedure TTarjeta.AgregaChecadasEnd;
begin
     FreeAndNil( FChecadaAgrega );
     FreeAndNil( FChecada );
end;

procedure TTarjeta.AgregaChecada( const iEmpleado: TNumEmp; const dFecha: TDate; const sHora, sReloj, sMotivo: String;
		                  const eTipo: eTipoChecadas; const eDescripcion: eDescripcionChecadas;
                                  const rHoras: Real; const iUsuario: Integer;
                                  const lGlobal: Boolean );
begin
     with FChecada do
     begin
          Empleado := iEmpleado;
          Fecha := dFecha;
          Tipo := eTipo;
          Descripcion := eDescripcion;
          HoraReal := sHora;
          HoraAjustada := sHora;
          Global := lGlobal;
          Sistema := False;
          Ordinarias := rHoras; { M�todo Agregar se encarga de separarlas }
          Extras := rHoras;     { M�todo Agregar se encarga de separarlas }
          Descanso := 0;
          Reloj := sReloj;
          Posicion := 0;
          Usuario := iUsuario;
          Borrada := False;
          Motivo := sMotivo;          
          Agregar( FChecadaAgrega );
     end;
end;

{ ************ Agregar Autorizaciones ************ }

procedure TTarjeta.AgregaAutorizacionBegin;
begin
     with oZetaProvider do
     begin
          FChecadaExiste := CreateQuery( GetSQLScript( Q_CHECADAS_EXISTE ) );
          FChecadaModifica := CreateQuery( GetSQLScript( Q_CHECADAS_MODIFICA ) );
     end;
     AgregaChecadasBegin;
end;

procedure TTarjeta.AgregaAutorizacionEnd;
begin
     AgregaChecadasEnd;
     FreeAndNil( FChecadaModifica );
     FreeAndNil( FChecadaExiste );
end;

function TTarjeta.AgregaAutorizacion( const iEmpleado: TNumEmp;
                                       const dFecha: TDate;
                                       const eTipo: eTipoChecadas;
                                       const sMotivo: String;
                                       const rValue: Extended;
                                       const eModo: eSustitucion;
                                       const lTieneDerecho: boolean ): boolean;
var
   lExiste, lCambia: Boolean;
   rHoras, rHorasAprobadas: TDiasHoras;
   iAprobadasPor: integer;
begin
     rHorasAprobadas := 0;
     iAprobadasPor := 0;
     with FChecadaExiste do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FChecadaExiste, 'Empleado', iEmpleado );
               ParamAsDate( FChecadaExiste, 'Fecha', dFecha  );
               ParamAsChar( FChecadaExiste, 'Hora', K_HORA_DUMMY, K_ANCHO_HORA );
               ParamAsInteger( FChecadaExiste, 'Tipo', Ord( eTipo ) );
          end;
          Active := True;
          if Eof then
          begin
               rHoras := rValue;
               lExiste := False;
               lCambia := True;
          end
          else
          begin
               lCambia := True;
               lExiste := True;
               if ( eTipo in [ chConGoce, chSinGoce, chExtras, chDescanso, chConGoceEntrada, chSinGoceEntrada, chPrepagFueraJornada, chPrepagDentroJornada ] ) then
               begin
                    if ( eTipo in [ chExtras, chPrepagFueraJornada, chPrepagDentroJornada ]  ) then
                       rHoras:= FieldByName( 'CH_HOR_EXT' ).AsFloat
                    else
                        rHoras:= FieldByName( 'CH_HOR_ORD' ).AsFloat;
               end
               else
                   rHoras:= FieldByName( 'CH_HOR_ORD' ).AsFloat;
               case eModo of
                    tsNoSustituir: lCambia := False;
                    tsSumar: rHoras := rHoras + rValue;
                    tsSustituir: rHoras := rValue;
               end;
               rHorasAprobadas := FieldByName( 'CH_HOR_DES' ).AsFloat;
               iAprobadasPor := FieldByName( 'US_COD_OK' ).AsInteger;
               lCambia := lCambia and ( lTieneDerecho or ( iAprobadasPor = 0 ) );
          end;
          Active := False;
     end;
     if lCambia then
     begin
          AutorizacionSet( iEmpleado, dFecha, eTipo, sMotivo, rHoras, rHorasAprobadas, iAprobadasPor );
          with FChecada do
          begin
               Global := True;
               if lExiste then
               begin
                    AutorizacionSetKey( iEmpleado, dFecha, eTipo );
                    Modificar( FChecadaModifica )
               end
               else
               begin
                    Agregar( FChecadaAgrega );
               end;
          end;
     end;
     Result := lCambia;
end;

{ *********** Grabar Autorizaciones ( Pantallas de Edici�n ) *********** }

procedure TTarjeta.AutorizacionBegin( const dInicial, dFinal: TDate );
begin
     AgregaTarjetaDiariaBegin( dInicial, dFinal );
     with oZetaProvider do
     begin
          FChecadaBorra := CreateQuery( GetSQLScript( Q_CHECADAS_BORRA_UNA ) );
          FChecadaModifica := CreateQuery( GetSQLScript( Q_CHECADAS_MODIFICA ) );
     end;
     AgregaChecadasBegin;
     LeeUnaTarjetaBegin;
end;

procedure TTarjeta.AutorizacionEnd;
begin
     LeeUnaTarjetaEnd;
     AgregaChecadasEnd;
     FreeAndNil( FChecadaModifica );
     FreeAndNil( FChecadaBorra );
     AgregaTarjetaDiariaEnd;
end;

procedure TTarjeta.AutorizacionSet( const iEmpleado: TNumEmp;
                                    const dFecha: TDate;
                                    const eTipo: eTipoChecadas;
                                    const sMotivo: String;
                                    const rValue: TDiasHoras;
                                    const rHorasAprobadas: TDiasHoras;
                                    const iAprobadasPor: integer );

begin
     with FChecada do
     begin
          SetAutorizacion( iEmpleado, dFecha, eTipo, sMotivo, rValue, rHorasAprobadas, iAprobadasPor );
     end;
end;

procedure TTarjeta.AutorizacionSetKey( const iEmpleado: TNumEmp; const dFecha: TDate; const eTipo: eTipoChecadas );
begin
     with FChecada.Llave do
     begin
          Empleado := iEmpleado;
          Fecha := dFecha;
          Hora := K_HORA_DUMMY;
          Tipo := eTipo;
     end;
end;

procedure TTarjeta.AutorizacionAgregar;
begin
     with FChecada do
     begin
          with AgregaTarjetaDiaria( Empleado, Fecha ) do
          begin
               Agregar( FChecadaAgrega );
               CalculaTarjeta( Empleado, Fecha, Horario, Status, Usuario );
          end;
     end;
end;

procedure TTarjeta.AutorizacionBorrar;
begin
     with FChecada.Llave do
     begin
          with oZetaProvider do
          begin
               ParamAsInteger( FChecadaBorra, 'Empleado', Empleado );
               ParamAsDate( FChecadaBorra, 'Fecha', Fecha  );
               ParamAsChar( FChecadaBorra, 'Hora', K_HORA_DUMMY, K_ANCHO_HORA );
               ParamAsInteger( FChecadaBorra, 'Tipo', Ord( Tipo ) );
               Ejecuta( FChecadaBorra );
          end;
          CalculaUnaTarjeta( Empleado, Fecha );
     end;
end;

procedure TTarjeta.AutorizacionModificar;
begin
     with FChecada do
     begin
          with AgregaTarjetaDiaria( Empleado, Fecha ) do
          begin
               Modificar( FChecadaModifica );
               CalculaTarjeta( Empleado, Fecha, Horario, Status, Usuario );
          end;
          if ( Empleado <> Llave.Empleado ) or ( Fecha <> Llave.Fecha ) then
          begin
               CalculaUnaTarjeta( Llave.Empleado, Llave.Fecha );
          end;
     end;
end;

{ ************** Calcular Pren�mina ************* }

{$ifdef QUINCENALES}
procedure TTarjeta.CalculaPrenominaBegin;
{$else}
procedure TTarjeta.CalculaPrenominaBegin(const dInicial, dFinal: TDate);
{$endif}
begin
     InitGlobalesTarjeta;
     InitGlobalesPrenomina;
{
     FFestivos := Ritmos.CreaFestivos( dInicial, dFinal, tfFestivo );  // Ya no se ocupa
}
     with oZetaProvider do
     begin
          FStatusActivo := CreateQuery( GetSQLScript( Q_EMPLEADO_STATUS_ACTIVO ) );
{$ifdef MSSQL}
          ParamSalida( FStatusActivo, 'Resultado' );
          ParamSalida( FStatusActivo, 'Incidencia' );
{$endif}
{$ifdef QUINCENALES}
          with DatosPeriodo do
          begin
               FStatusCheck := CreateQuery( GetSQLScript( Q_EMPLEADO_STATUS_CHECK ) );
               FTarjetaLee := CreateQuery( GetSQLScript( Q_TARJETA_PRENOMINA_LEE ) );
          end;
          if ( FTratamientoExtras = eteHorasExtrasTriples ) then
             FLeeDiasHorasExtras := CreateQuery( GetSQLScript( Q_TARJETA_PRENOMINA_HORAS_DOBLES ) )
          else
              FLeeDiasHorasExtras := CreateQuery( GetSQLScript( Q_TARJETA_PRENOMINA_DIAS_EXTRAS ) );
{$else}
          FStatusCheck := CreateQuery( Format( GetSQLScript( Q_EMPLEADO_STATUS_CHECK ), [ ZetaCommonTools.DateToStrSQLC( dInicial ), ZetaCommonTools.DateToStrSQLC( dFinal ) ] ) );
          FTarjetaLee := CreateQuery( Format( GetSQLScript( Q_TARJETA_PRENOMINA_LEE ), [ ZetaCommonTools.DateToStrSQLC( dInicial ), ZetaCommonTools.DateToStrSQLC( dFinal ) ] ) );
{$endif}
          with DatosPeriodo do
          begin
               FTarjetaEscribe := CreateQuery( GetSQLScript( Q_TARJETA_PRENOMINA_ACTUALIZA ) );
               ParamAsInteger( FTarjetaEscribe, 'ANNO', Year );              { PE_YEAR }
               ParamAsInteger( FTarjetaEscribe, 'TIPO', Ord( Tipo ) );       { PE_TIPO }
               ParamAsInteger( FTarjetaEscribe, 'PERIODO', Numero );         { PE_NUMERO }
          end;
     end;
     RedondeoBegin;
{$ifdef QUINCENALES}
     with oZetaProvider.DatosPeriodo do
     begin
          Ritmos.RitmosBegin( InicioAsis, Fin );    // Con desfase se ocupa revisar rangos mayores de ritmo - Ritmos.Preparar - Se ocupa que tambien prepare los Festivos
     end;
     SetPlanVacacionesBegin;
{$else}
     Ritmos.RitmosBegin( dInicial, dFinal );    // Ritmos.Preparar - Se ocupa que tambien prepare los Festivos
{$endif}
     GetJornadaDiariaBegin;
{$ifdef CAMBIO_TNOM}
     GetTipoNomBegin;
{$endif}
end;

procedure TTarjeta.CalculaPrenominaEnd;
begin
{$ifdef CAMBIO_TNOM}
     GetTipoNomEnd;
{$endif}
     GetJornadaDiariaEnd;
     Ritmos.RitmosEnd;                          // Ritmos.Despreparar; - Debe Despreparar tambien los Festivos
     RedondeoEnd;
     FreeAndNil( FTarjetaEscribe );
{$ifdef QUINCENALES}
     SetPlanVacacionesEnd;
     FreeAndNil( FLeeDiasHorasExtras );
{$endif}
     FreeAndNil( FTarjetaLee );
     FreeAndNil( FStatusCheck );
     FreeAndNil( FStatusActivo );
{
     FreeAndNil( FFestivos );
}
end;

{$ifdef CAMBIO_TNOM}
procedure TTarjeta.CalculaPrenomina( const iEmpleado: TNumEmp; const lCheca: Boolean; const sTurno: TCodigo; const dIngreso, dBaja, dCambioTNom: TDate );
{$else}
{$ifdef QUINCENALES}
procedure TTarjeta.CalculaPrenomina( const iEmpleado: TNumEmp; const lCheca: Boolean; const sTurno: TCodigo; const dIngreso, dBaja: TDate );
{$else}
procedure TTarjeta.CalculaPrenomina( const iEmpleado: TNumEmp; const lCheca: Boolean; const sTurno: TCodigo );
{$endif}
{$endif}
const
     K_EXENTAS_DIARIO = 3;
     K_EXENTAS_SEMANAL = 3;
{$ifdef TJORNADA_FESTIVOS}
     aJornadaNum: array[ eTipoJornadaTrabajo ] of TDiasHoras = ( 8, 7.5, 7 );
{$endif}
var
   dAusencia: TDate;
   iPosicion: Cardinal;
   sIncidencia: TCodigo;
   lCheckStatus, lFestivo, lConHorario, lActivo: Boolean;
   StatusEmpleado: eStatusEmpleado;
   StatusDia: eStatusAusencia;
   TipoDia: eTipoDiaAusencia;
   h_ordinarias, h_dobles, h_extras, h_per_cg, h_per_sg, h_des_tra{$ifdef TJORNADA_FESTIVOS}, h_jornada_tra{$endif}: TDiasHoras;
   h_s_dobles, tot_dobles, tot_dobles_semana, tot_dias_extras_sem : TDiasHoras;
   mck_horas, mck_extras, mck_aut_tra: TDiasHoras;
   sHorario: String;
   iUsuario: Integer;
   rNumExt, rAutTra, rHorasCK, rAutExt, rPerCG, rPerSG, rTardes, rExtras, rPreExtras{$ifdef TJORNADA_FESTIVOS}, rNumExtFestivo{$endif}: TDiasHoras;
   // DES US #13377: TPI Composites- Calculo de horas ordinarias en dia festivo es incorrecta cuando hay cambio de turno
   sTurnoTarjeta: String;
   FDatosTurnoTarjeta: TDatosJornada;
   // DES US #13383: Festivos - Modifica el horario en la pre-nomina y no se considera cuando es festivo y tiene horario de festivo trabajado
   lHorarioManual: boolean;

   FDatosJornada: TDatosJornada;
   FDatosJornadaDiaria : TDatosJornadaDiaria;
   FExtra: eHorasExtras;
   lCrearTarjeta: Boolean;
{$ifdef QUINCENALES}
   rHorasNT: TDiasHoras;
   iDiasFondo : Integer;
   dInicioFondo : TDate;
   lEsBaja : Boolean;
   dFechaLimite: TDate;
   dCorteExtras: TDate;
{$endif}
   oStatusFestivo: eStatusFestivo;
{$ifdef CAMBIO_TNOM}
   eTNominaIni, eTNominaFin: eTipoPeriodo;
   lCambioTNom: Boolean;
{$endif}
   lAutExtraYDes: Boolean;

function TopaDiario: Boolean;
begin
     Result :=  ( ( ( TipoDia = daNormal ) and
                    ( StatusDia = auSabado ) and
                    ( FExtraSabado in [ heExtrasTopeDiario, heDescansoExtrasTopeDiario ] ) ) or
                  ( ( TipoDia = daFestivo ) and
                    ( FExtraFestivo in [ heExtrasTopeDiario, heDescansoExtrasTopeDiario ] ) ) or
                  ( ( TipoDia = daNormal ) and
                    ( StatusDia = auDescanso ) and
                    ( FExtraDescanso in [ heExtrasTopeDiario, heDescansoExtrasTopeDiario ] ) ) );
end;

begin
     iPosicion := 0;
     tot_dobles := 0;
     tot_dobles_semana := 0;
     tot_dias_extras_sem := 0;
     StatusEmpleado := steEmpleado; { Default }
     sIncidencia := '';
     with oZetaProvider do
     begin
{$ifdef QUINCENALES}
          lEsBaja := ZetaCommonTools.EsBajaenPeriodo( dIngreso, dBaja, DatosPeriodo, dAusencia, dFechaLimite );

          lCambioTNom:= FALSE;

          if ( EsCambioTNomPeriodo( dCambioTNom, oZetaProvider.DatosPeriodo ) ) then
          begin
               eTNominaIni:= GetTipoNomina( iEmpleado, dAusencia );
               eTNominaFin:= GetTipoNomina( iEmpleado, dFechaLimite );

               lCambioTNom:= ( eTNominaIni <> eTNominaFin );
          end;

          if ( lCambioTNom ) then
          begin
               { Si se esta calculando la n�mina a la que cambio el inicio cambia}
               if ( eTNominaFin = oZetaProvider.DatosPeriodo.Tipo ) then
               begin
                    dAusencia:= dMax( dCambioTNom, dAusencia );
               end
               else
                   { Si se esta calculando la n�mina con el cambio el final cambia }
                   dFechaLimite:= dMin( dCambioTNom - 1, dFechaLimite );
          end;

          with DatosPeriodo do
          begin
               //dAusencia := DatosPeriodo.InicioAsis;
               //FDatosJornada := Ritmos.GetDatosJornada( sTurno, DatosPeriodo.InicioAsis, dFechaLimite );
               FDatosJornada := Ritmos.GetDatosJornada( sTurno, dAusencia, dFechaLimite );
          end;
          SetPlanVacaciones( iEmpleado, dAusencia, dFechaLimite );
          if FTopaPorSemana then
          begin
               {$ifndef CAMBIO_TNOM}
               if ( Trunc( dFechaLimite - dAusencia ) > 6 )  then { Fecha de corte para N�minas mayores a una Semana �No es mejor usar Periodo.Tipo ? }
               {$else}
               {Con la programaci�n de Cambio de Tipo de N�mina tambien se revisan las semanales si se tiene el global prendido.}
               if ( Trunc( dFechaLimite - dAusencia ) >= 6 ) or ( lCambioTNom ) then
               {$endif}
               begin
                    { Nos vamos a la fecha de corte de la semana anterior }
                    dCorteExtras := ZetaCommonTools.LastCorte( dAusencia, FDOWCorte );
                    if ( dCorteExtras < dAusencia ) then    // Corte anterior a fecha de inicio
                    begin
                         FLeeDiasHorasExtras.Active := FALSE;
                         ParamAsInteger( FLeeDiasHorasExtras, 'Empleado', iEmpleado );
                         ParamAsDate( FLeeDiasHorasExtras, 'Fecha', dCorteExtras );
                         ParamAsDate( FLeeDiasHorasExtras, 'FechaInicial', dAusencia );
                         with FLeeDiasHorasExtras do
                         begin
                              Active := TRUE;
                              if ( FTratamientoExtras = eteHorasExtrasTriples ) then
                                 tot_dobles_semana := FieldByName( 'CUANTOS' ).AsFloat
                              else  // eteRegla3X3
                                  tot_dias_extras_sem := FieldByName( 'CUANTOS' ).AsFloat;
                              Active := FALSE;
                         end;
                    end;
                    dCorteExtras := ( dCorteExtras + 7 );
               end
               else
                   dCorteExtras := ( dFechaLimite + 1 );
          end
          else
              dCorteExtras := ( dAusencia + 7 ); // Equivalente a iPosicion = 7
{$else}
          with DatosPeriodo do
          begin
               dAusencia := Inicio;
               FDatosJornada := Ritmos.GetDatosJornada( sTurno, Inicio, Fin );
          end;
{$endif}
          with FTarjetaLee do
          begin
               Active := False;
               ParamAsInteger( FTarjetaLee, 'Empleado', iEmpleado );
{$ifdef QUINCENALES}
               ParamAsDate( FTarjetaLee, 'FechaIni', dAusencia );
               ParamAsDate( FTarjetaLee, 'FechaFin', dFechaLimite );
{$endif}
               Active := True;
          end;
          with FStatusCheck do
          begin
               Active := False;
               ParamAsInteger( FStatusCheck, 'Empleado', iEmpleado );
{$ifdef QUINCENALES}
               ParamAsDate( FStatusCheck, 'FechaIni', dAusencia );
               ParamAsDate( FStatusCheck, 'FechaFin', dFechaLimite );
{$endif}
               Active := True;
               lCheckStatus := ( FieldByName( 'RESULTADO' ).AsInteger <> 1 );
               Active := False;
          end;
     end;
     lConHorario := ( FDatosJornada.TipoTurno = ttNormal );
     { Inicializa suponiendo que el empleado no est� Activo }
     lActivo := False;
{$ifdef QUINCENALES}
     { Revision de Dias de Fondo para ingresos }
     { ******************************************************************************************************************
       *  Dependiendo de la ubicaci�n de la fecha de ingreso se tienen los siguientes dos comportamientos:              *
       *                                                                                                                *
       * 1.- Si el ingreso est� entre la fecha de inicio pago y fin pago se debe aplicar el criterio azul               *
       * 2.- Si el ingreso no entra en el caso #1 se evalua si est� entre la fecha de inicio de asistencia y fin        *
       *     de asistencia y si es as� aplica el criterio verde                                                         *
       *                                                                                                                *
       * Criterio Azul                                                                                                  *
       * 1.- Dias de Fondo = MIN( PE_FEC_FIN - ( MAX( PE_ASI_FIN + 1, CB_FEC_ING ) ) + 1, PE_ASI_FIN - PE_ASI_INI + 1 ) *
       * 2.- Inicio Fondo = MIN( CB_FEC_ING, PE_ASI_FIN + 1) - Dias de Fondo                                            *
       *                                                                                                                *
       * Criterio Verde                                                                                                 *
       * 1.- Dias de Fondo = CB_FEC_ING - PE_ASI_INI                                                                    *
       * 2.- Inicio de Fondo = CB_FEC_ING - Dias de Fondo                                                               *
       *                                                                                                                *
       * Cambio al Algoritmo: 12/Abr/2005:                                                                              *
       * Criterio Azul                                                                                                  *
       * 1.- Dias de Fondo = MAX( 0, ( ( MIN( PE_ASI_FIN, CB_FEC_ING - 1 ) - PE_ASI_INI + 1 ) - ( CB_FEC_ING - PE_FEC_INI ) ) )
       * 2.- Inicio Fondo =  MIN( CB_FEC_ING, PE_ASI_FIN + 1) - Dias de Fondo                                           *
       *                                                                                                                *
       * Criterio Verde                                                                                                 *
       * 1.- Dias de Fondo = CB_FEC_ING - PE_ASI_INI                                                                    *
       * 2.- Inicio de Fondo = CB_FEC_ING - Dias de Fondo
       ****************************************************************************************************************** }
     with oZetaProvider.DatosPeriodo do
     begin
          iDiasFondo := 0;
          dInicioFondo := NullDateTime;
          if ( not lEsBaja ) then                    // Si hay baja en mismo periodo de ingreso no se crean tarjetas de fondo
          begin
               { Criterio Azul }
               if ( dIngreso >= Inicio ) and ( dIngreso <= Fin ) then
               begin
                    //iDiasFondo := iMin( Trunc( Fin ) - iMax( Trunc( FinAsis ) + 1, Trunc( dIngreso ) ), Trunc( FinAsis ) - Trunc( InicioAsis ) ) + 1;
                    iDiasFondo := iMax( 0, ( ( ( iMin( Trunc( FinAsis ), Trunc( dIngreso ) - 1 ) - Trunc( InicioAsis ) ) + 1 ) - ( Trunc( dIngreso ) - Trunc( Inicio ) ) ) );
                    dInicioFondo := iMin( Trunc( dIngreso ), Trunc( FinAsis ) + 1 ) - iDiasFondo;
               end
                    { Criterio Verde }
               else if ( dIngreso >= InicioAsis ) and ( dIngreso <= FinAsis ) then
               begin
                    iDiasFondo := Trunc( dIngreso ) - Trunc( InicioAsis );
                    dInicioFondo := ( dIngreso - iDiasFondo );
               end;
          end;
     end;
{$endif}
     repeat
           with FTarjetaLee do
           begin
                lCrearTarjeta := Eof or ( FieldByName( 'AU_FECHA' ).AsDateTime <> dAusencia );
                if lCrearTarjeta then
                begin
                     iUsuario := 0;
                     with Ritmos.GetEmpleadoDatosTurno( iEmpleado, dAusencia, TRUE ).StatusHorario do    // Para agregar la Tarjeta considera los Intercambios de Horario en Dias Festivos
                     begin
                          sHorario := Horario;
                          StatusDia := Status;
                     end;

                     TipoDia := daNormal; { Default }
                     rHorasCK := 0;
                     rNumExt := 0;
                     rAutTra := 0;
                     rAutExt := 0;
                     rPerCG := 0;
                     rPerSG := 0;
                     rTardes := 0;
                     {
                     rDobles := 0;
                     }
                     rExtras := 0;
                     rPreExtras:= 0;
                     oStatusFestivo := esfAutomatico;

                     // DES US #13377: TPI Composites- Calculo de horas ordinarias en dia festivo es incorrecta cuando hay cambio de turno
                     sTurnoTarjeta := Ritmos.GetEmpleadoDatosTurno( iEmpleado, dAusencia, TRUE ).Codigo;
                     // DES US #13383: Festivos - Modifica el horario en la pre-nomina y no se considera cuando es festivo y tiene horario de festivo trabajado
                     lHorarioManual := FALSE;
                end
                else
                begin
                     iUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
                     sHorario := FieldByName( 'HO_CODIGO' ).AsString;
                     StatusDia := eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger );
                     TipoDia := eTipoDiaAusencia( FieldByName( 'AU_TIPODIA' ).AsInteger );
                     rHorasCK := FieldByName( 'AU_HORASCK' ).AsFloat;	{ Campo nuevo }
                     rNumExt := FieldByName( 'AU_NUM_EXT' ).AsFloat;
                     rAutTra := FieldByName( 'AU_AUT_TRA' ).AsFloat;
                     rAutExt := FieldByName( 'AU_AUT_EXT' ).AsFloat;
                     rPerCG := FieldByName( 'AU_PER_CG' ).AsFloat;
                     rPerSG := FieldByName( 'AU_PER_SG' ).AsFloat;
                     rTardes := FieldByName( 'AU_TARDES' ).AsFloat;
                     {
                     rDobles := FieldByName( 'AU_DOBLES' ).AsFloat;
                     }
                     rExtras := FieldByName( 'AU_EXTRAS' ).AsFloat;
                     rPreExtras := FieldByName('AU_PRE_EXT').AsFloat;

                     oStatusFestivo := eStatusFestivo( FieldByName('AU_STA_FES').AsInteger );

                     // DES US #13377: TPI Composites- Calculo de horas ordinarias en dia festivo es incorrecta cuando hay cambio de turno
                     sTurnoTarjeta := FieldByName ('CB_TURNO').AsString;
                     // DES US #13383: Festivos - Modifica el horario en la pre-nomina y no se considera cuando es festivo y tiene horario de festivo trabajado
                     lHorarioManual := zStrToBool (FieldByName( 'AU_HOR_MAN' ).AsString);

                     Next; { Avanza Apuntador Para Analizar Siguiente Fecha }
                end;
           end;

           iPosicion := iPosicion + 1;
           sIncidencia := '';
           h_ordinarias := 0;
           h_extras := 0;
           h_per_cg := 0;
           h_per_sg := 0;
           h_des_tra := 0;

{$ifdef QUINCENALES}
           rHorasNT := 0;
{$endif}
{$ifdef TJORNADA_FESTIVOS}
           rNumExtFestivo:= 0;
           h_jornada_tra:= aJornadaNum[FDatosJornada.TipoJorTrabajo];
{$endif}
           if strVacio( sHorario ) then
              DB.DatabaseError( 'El Turno Asignado Al Empleado No Tiene Horarios Definidos' );
           FDatosJornadaDiaria := GetJornadaDiaria( sHorario  );
           if ( iUsuario = 0 ) then { Si no fue modificada por el usuario, se reprocesa }
           begin
                 case oStatusFestivo of
                      esfFestivo: lFestivo := True;
                      esfFestivoTransferido: lFestivo := False;
                 else
                     lFestivo := CheckFestivo(sTurno, dAusencia);
                 end;
                //lFestivo := CheckFestivo( sTurno, dAusencia );
                if lCheckStatus then
                   StatusEmpleado := GetStatus( iEmpleado, dAusencia, sIncidencia );
                TipoDia := GetTipoDia( StatusEmpleado, lFestivo, FFestivoEnDescanso{$ifdef IMSS_VACA}, FDiasCotizaSinFV{$endif}, StatusDia );
{$ifdef QUINCENALES}
                if ( iDiasFondo > 0 ) and ( StatusEmpleado in [ steAnterior, steBaja ] ) then
                begin
                     // De momento no se reporta error si ya existe, solo se reprocesa como Tarjeta de Fondo
                     if ( dAusencia >= dInicioFondo ) then          // Solo es necesario revisar mayor o igual a dInicioFondo, por que est�n consecutivos los d�as
                     begin
                          TipoDia := daNormal;  // Sobreescribe a GetTipoDia() para que agregue las tarjetas de fondo
                          sIncidencia := K_I_FONDO;
                          if ( StatusDia = auHabil ) then
                             h_ordinarias := FDatosJornadaDiaria.Jornada;
                     end;
                end
                else
                begin
{$endif}
                     {$ifdef TJORNADA_FESTIVOS}
                     if ( lFestivo ) and ( not FDatosJornada.TieneHorarioFestivo ) then
                     begin
                          { Independientemente del tipo de d�a, si el d�a es festivo se sique el criterio de topar de
                            acuerdo al tipo de jornada.
                            Si no tiene horario festivo, desglosa lo que le corresponde a extras y descanso trabajado.
                            El criterio de globales se determina cuando se topan las extras.
                            Solamente aplica para empleados que tienen horas calculadas en tarjeta. }

                          if ( rHorasCK > h_jornada_tra )  then
                          begin
                               rNumExtFestivo:= rHorasCK - h_jornada_tra;
                               rNumExt:= RedondeoHorasExtras( rNumExtFestivo, dAusencia ) + rNumExt;
                               rHorasCK:= h_jornada_tra;
                          end;
                     end;
                     {$endif}

                     case TipoDia of
                          daNormal:
                          begin
                               sIncidencia := '';
                               if ( StatusDia = auHabil ) and not lCheca then { Si no checa y es Dia Habil, recibe el pago de dia completo }
                                  h_ordinarias := FDatosJornadaDiaria.Jornada;
                          end;
                          daFestivo:
                          begin
                               // DES US #13377: TPI Composites- Calculo de horas ordinarias en dia festivo es incorrecta cuando hay cambio de turno
                               // Si es festivo, obtener los datos del turno sTurnoTarjeta.
                               FDatosTurnoTarjeta :=  Ritmos.GetDatosJornada( sTurnoTarjeta, dAusencia, dAusencia );

                               sIncidencia := '';
                               {$ifdef ANTES}
                               if lConHorario and ( StatusDia = auHabil ) then { Festivo en dia habil se paga el dia completo a todos }
                               {$else}
                               { Defecto #1278: No estaba tomando valor en festivo trabajado. Aun cuando sea un tipo de turno
                               de con jornada sin horario o sin jornada sin horario, al empleado se le paga el festivo como si hubiera
                               trabajado su jornada completa }
                               if ( StatusDia = auHabil ) then { Festivo en dia habil se paga el dia completo a todos }
                               {$endif}
                               begin
                                    // DES US #13377: TPI Composites- Calculo de horas ordinarias en dia festivo es incorrecta cuando hay cambio de turno
                                    // DES US #13383: Festivos - Modifica el horario en la pre-nomina y no se considera cuando es festivo y tiene horario de festivo trabajado                                    
                                    if FDatosTurnoTarjeta.TieneHorarioFestivo  and (not lHorarioManual)  then
                                    begin
                                         { GA (15/Jun/03): Averiguar cual es el horario que le corresponde al empleado }
                                         { en lugar del horario asignado al d�a festivo
                                         with Ritmos.GetStatusHorario( sTurno, dAusencia, False ) do
                                         begin
                                              sHorario := Horario;
                                         end; }
                                         { GA (15/Jun/03): La jornada es definida por el horario que le corresponde }
                                         { al empleado normalmente, no por el horario del d�a festivo }

                                         // DES US #13377: TPI Composites- Calculo de horas ordinarias en dia festivo
                                         // es incorrecta cuando hay cambio de turno
                                         with GetJornadaDiaria( Ritmos.GetStatusHorario( sTurnoTarjeta, dAusencia, False ).Horario ) do
                                         begin
                                              h_ordinarias := Jornada;
                                         end;
                                    end
                                    else
                                    begin
                                         { GA (15/Jun/03): Se aplica el comportamiento existente cuando }
                                         { no hay un horario para festivos asignado al turno }
                                         h_ordinarias := FDatosJornadaDiaria.Jornada;
                                    end;
                               end;
                          end;
                          daConGoce: h_per_cg := FDatosJornadaDiaria.Jornada; { Permiso con Goce de 1 dia completo }
                          daSinGoce: h_per_sg := FDatosJornadaDiaria.Jornada; { Permiso sin Goce de 1 dia completo }
                     end;
{$ifdef QUINCENALES}
                end;
{$endif}
                { Si checa, o bien, }
                { No checa pero se permiten extras para los que no checan y empleado tiene extras/descanso }
                mck_extras := rNumExt;
                mck_aut_tra := rAutTra;
                mck_horas := rHorasCK;
{$ifdef QUINCENALES}
                if ( sIncidencia <> K_I_FONDO ) then      // Solo aplica para tarjetas que no sean de fondo - Reales de Asistencia
                begin
{$endif}
                     // Correcci�n de Bug #12057:
                     // Cardinal Brands (Matamoros): Descanso trabajado para empleados que no checan.
                     // if ( lCheca ) or ( FExtraConfianza and ( ( mck_extras > 0 ) or ( mck_aut_tra > 0 ) or ( rPreExtras > 0 ) ) ) then
                     if ( lCheca ) or ( FExtraConfianza ) then
                     begin
                          if FAutorizarExtras then { Es necesario volver a toparlo para el criterio de tiempo extra en descansos }
                             mck_extras := ZetaCommonTools.rMin( mck_extras, rAutExt );
                          mck_extras:= mck_extras + rPreExtras;
                          case TipoDia of
                               daNormal:
                               begin
                                    case StatusDia of
                                         auHabil:
                                         begin
                                              if ( lCheca ) then
                                                 h_ordinarias := mck_horas;
                                              h_extras := mck_extras;
                                              h_per_cg := rPerCG;
                                              h_per_sg := rPerSG;
                                         end;
                                         auSabado, auDescanso:
                                         begin
                                              if ( lFestivo ) then
                                              begin
                                                   FExtra := FExtraFestivo;
                                                   lAutExtraYDes:= FAutorizaExtrasYDescansos;
                                              end
                                              else
                                                  if ( StatusDia = auSabado ) then
                                                  begin
                                                       FExtra := FExtraSabado;
                                                       lAutExtraYDes:= FAutExtYDesSabado;
                                                  end
                                                  else
                                                  begin
                                                       FExtra := FExtraDescanso;
                                                       lAutExtraYDes:= FAutExtYDesDescanso;
                                                  end;
                                              { Define si se puede autorizar como extras }
                                              if lAutExtraYDes then
                                                 mck_aut_tra := ( rAutTra + rAutExt );

                                              if FExtra in [ heDescansoExtrasTopeSemanal, heDescansoExtrasTopeDiario ] then
                                              begin
                                                   if lAutExtraYDes and ( rAutExt > 0 ) then
                                                   begin
                                                        //db.databaseerror( FloatToStr( mck_extras ) + '|' + FloatToStr( mck_aut_tra ) + '|' + FloatToStr( mck_horas ) + '|' + FloatToStr( rHorasCK ) );
                                                        mck_extras := ZetaCommonTools.rMin( mck_extras, ZetaCommonTools.rMax( ( mck_aut_tra - mck_horas ), 0 ) );
                                                        mck_aut_tra := ( mck_aut_tra - mck_extras );               // Quitar la parte de extras ya usada por las extras
                                                   end;

                                                   {$ifndef ANTES}
                                                   if FAutorizarExtras then         // Para no requerir autorizaciones
                                                   {$endif}
                                                      mck_horas := ZetaCommonTools.rMin( mck_horas, mck_aut_tra );
                                                   h_des_tra := mck_horas;
                                                   h_extras := mck_extras;
                                              end
                                              else
                                                  if ( FExtra = heDescansoTrabajado ) then
                                                     h_des_tra := mck_extras
                                                  else {$ifndef ANTES} if FExtra in [ heExtrasTopeSemanal, heExtrasTopeDiario ] then {$endif}  // Para evitar el defecto que el NoPagar, paga las extras
                                                      h_extras := mck_extras;
                                         end;
                                    end;
                               end;
                               daIncapacidad:
                               begin
                                    if ( lCheca ) then
                                       h_ordinarias := mck_horas;
                                    h_extras := mck_extras;
                                    if ( mck_horas + mck_extras ) > 0 then
                                       oZetaProvider.Log.Advertencia( iEmpleado, 'Horas en D�a de Incapacidad: ' + FechaCorta( dAusencia ) );
                               end;
                               daVacaciones:
                               begin
                                    {$ifdef ANTES}
                                    h_des_tra := ZetaCommonTools.rMin( mck_horas, mck_aut_tra );
                                    h_extras  := mck_extras;
                                    {$else}
                                    case StatusDia of
                                         auHabil:
                                         begin
                                              if FAutorizarExtras then         // Para no requerir autorizaciones
                                                 mck_horas := ZetaCommonTools.rMin( mck_horas, mck_aut_tra );
                                         end;
                                         auSabado, auDescanso:
                                         begin
                                              if ( StatusDia = auSabado ) then
                                              begin
                                                   FExtra := FExtraSabado;
                                                   lAutExtraYDes:= FAutExtYDesSabado;
                                              end
                                              else
                                              begin
                                                   FExtra := FExtraDescanso;
                                                   lAutExtraYDes:= FAutExtYDesDescanso;
                                              end;

                                              if lAutExtraYDes then
                                                 mck_aut_tra := ( rAutTra + rAutExt );

                                              if ( FExtra = heNoPagar ) then
                                              begin
                                                   mck_horas := 0;
                                                   mck_extras := 0;
                                              end
                                              else if FExtra in [ heDescansoExtrasTopeSemanal, heDescansoExtrasTopeDiario ] then
                                              begin
                                                   if lAutExtraYDes and ( rAutExt > 0 ) then
                                                   begin
                                                        mck_extras := ZetaCommonTools.rMin( mck_extras, ZetaCommonTools.rMax( ( mck_aut_tra - mck_horas ), 0 ) );
                                                        mck_aut_tra := ( mck_aut_tra - mck_extras );               // Quitar la parte de extras ya usada por las extras
                                                   end;
                                              end
                                              else
                                              begin
                                                   mck_horas := ( mck_horas + rNumExt );   // No se usa mck_extras por que est� topada por aut. de extras y todas las extras se consideran descanso trabajado
                                                   mck_extras := 0;
                                              end;

                                              if FAutorizarExtras then
                                                 mck_horas := ZetaCommonTools.rMin( mck_horas, mck_aut_tra );

                                              if  ( FExtra in [ heExtrasTopeSemanal, heExtrasTopeDiario, heDescansoTrabajado ] ) then
                                                 mck_horas := mck_horas + rPreExtras;

                                              if FExtra in [ heExtrasTopeSemanal, heExtrasTopeDiario ] then
                                              begin
                                                   mck_extras := mck_horas;
                                                   mck_horas := 0;
                                              end;
                                         end;
                                    end;
                                    // Asigna valores de descanso trabajado y extras
                                    h_des_tra := mck_horas;
                                    h_extras := RedondeoHorasExtras( mck_extras, dAusencia );
                                    {$endif}
                               end;
                               daConGoce, daSinGoce, daJustificada, daSuspension, daOtroPermiso:
                               begin
                                    if ( lCheca ) then
                                       h_ordinarias := mck_horas;
                                    h_extras := mck_extras;
                                    if ( mck_horas + mck_extras ) > 0 then
                                       oZetaProvider.Log.Advertencia( iEmpleado, 'Horas en D�a de Permiso/Suspensi�n: ' + FechaCorta( dAusencia ) );
                               end;
                               daFestivo:
                               begin
                                    {$ifndef ANTES}
                                    if FAutorizaExtrasYDescansos then
                                       mck_aut_tra := ( rAutTra + rAutExt );
                                    if ( FExtraFestivo = heNoPagar ) then
                                    begin
                                         mck_horas := 0;
                                         mck_extras := 0;
                                    end
                                    else if FExtraFestivo in [ heDescansoExtrasTopeSemanal, heDescansoExtrasTopeDiario ] then
                                    begin
                                         if FAutorizaExtrasYDescansos and ( rAutExt > 0 ) then
                                         begin
                                              //db.databaseerror( FloatToStr( mck_extras ) + '|' + FloatToStr( mck_aut_tra ) + '|' + FloatToStr( mck_horas ) );
                                              mck_extras := ZetaCommonTools.rMin( mck_extras, ZetaCommonTools.rMax( ( mck_aut_tra - mck_horas ), 0 ) );
                                              mck_aut_tra := ( mck_aut_tra - mck_extras );               // Quitar la parte de extras ya usada por las extras
                                         end;
                                    end
                                    else
                                    begin
                                         mck_horas := ( mck_horas + rNumExt );   // No se usa mck_extras por que est� topada por aut. de extras y todas las extras se consideran descanso trabajado
                                         mck_extras := 0;
                                    end;
                                    if FAutorizarFestivos then
                                       mck_horas := ZetaCommonTools.rMin( mck_horas, mck_aut_tra );

                                    { Solamente en estas condiciones las horas extras prepagadas se suman a  mck_horas de lo contrario ya esta considerado
                                     en mck_extras }
                                    if  ( FExtraFestivo in [ heExtrasTopeSemanal, heExtrasTopeDiario, heDescansoTrabajado ] ) then
                                       mck_horas := mck_horas + rPreExtras;

                                    if FExtraFestivo in [ heExtrasTopeSemanal, heExtrasTopeDiario ] then
                                    begin
                                         mck_extras := mck_horas;
                                         mck_horas := 0;
                                    end;
                                    // Asigna valores de descanso trabajado y extras
                                    h_des_tra := mck_horas;
                                    h_extras := RedondeoHorasExtras( mck_extras, dAusencia );
                                    {$else}
                                    if FExtraFestivo in [ heDescansoExtrasTopeSemanal, heDescansoExtrasTopeDiario ] then
                                    begin
                                         if FAutorizarFestivos then
                                            mck_horas := ZetaCommonTools.rMin( mck_horas, mck_aut_tra );
                                         h_des_tra := mck_horas;
                                         { � Es Necesario Llamar RedondeoExtras ? }
                                         h_extras := RedondeoHorasExtras( mck_extras, dAusencia );
                                    end
                                    else
                                    begin
                                         mck_extras := RedondeoHorasExtras( mck_extras + mck_horas, dAusencia );
                                         if FAutorizarFestivos then
                                            mck_extras := ZetaCommonTools.rMin( mck_extras, mck_aut_tra );
                                         if ( FExtraFestivo = heDescansoTrabajado ) then
                                            h_des_tra := mck_extras
                                         else
                                             h_extras := mck_extras;
                                    end;
                                    {$endif}
                               end;
                               daNoTrabajado:  { Se ignorar las horas }
                               begin
                                    if ( mck_horas + mck_extras ) > 0 then
                                       oZetaProvider.Log.Advertencia( iEmpleado, 'Horas en D�a No Trabajado: ' + FechaCorta( dAusencia ) );
                               end;
                          end;
                     end;
{$ifdef QUINCENALES}
                end;
{$endif}
                if ( h_per_cg > 0 ) then { Las horas de permiso c/Goce se pasan a h_ordinarias, hasta el tope de jornada }
                begin
                     h_per_cg := ZetaCommonTools.rMin( ZetaCommonTools.rMax( FDatosJornadaDiaria.Jornada - h_ordinarias, 0 ), h_per_cg );
                     h_ordinarias := h_ordinarias + h_per_cg;
                end;
                if ( h_per_sg > 0 ) then { Las horas de permiso s/Goce s�lo se topan al complemento de la jornada }
                begin
                     h_per_sg := ZetaCommonTools.rMin( ZetaCommonTools.rMax( FDatosJornadaDiaria.Jornada - h_ordinarias, 0 ), h_per_sg );
                end;
                if ( TipoDia = daNormal ) and { Si no hubo horas en un dia Habil/Normal }
                   ( StatusDia = auHabil ) and
                   lConHorario and
                   ( ( h_ordinarias + {$ifndef CR2575}h_per_sg +{$endif} h_extras ) <= 0 ) then
                   sIncidencia := K_I_FALTA_INJUSTIFICADA;
                if ( rTardes > 0 ) and ( sIncidencia = '' ) then { Si hubo horas de retardo, asigna incidencia RE=RETARDO }
                   sIncidencia := K_I_RETARDO;
{$ifdef QUINCENALES}
                if ( TipoDia = daNormal ) and
                   ( StatusDia = auHabil ) and
                   ( ( h_ordinarias + {$ifndef CR2575}h_per_sg +{$endif} h_extras ) > 0 ) then                   // Solo acumula horas no trabajadas si el empleado asisti� a trabajar
                   rHorasNT := ZetaCommonTools.rMax( FDatosJornadaDiaria.Jornada - h_ordinarias, 0 );
{$endif}
           end  { if ( iUsuario > 0 ) then }
           else
           begin
                { Las lee de AUSENCIA: AU_EXTRAS }
                h_extras := rExtras;
           end;
           if ( FTratamientoExtras = eteHorasExtrasTriples ) then
           begin
                { Siempre se aplica el TOPE Diario }
                h_dobles := ZetaCommonTools.rMin( h_extras, FDatosJornadaDiaria.Dobles );
                { EM: 4/Oct/99. Se decidi� que aunque la tarjeta haya }
                { sido capturada manual, se aplica la regla semanal de }
                { dobles y triples }
                if TopaDiario then
                begin
                     h_s_dobles := h_dobles;
                     h_dobles   := 0;
                end
                else
                begin
                     h_s_dobles := 0;
                     { EZM: Esto cambi� a partir del Build 332 }
                     { Antes: El tope de horas era por PERIODO de NOMINA (Ej. Toda la quincena) }
                     { Ahora: El tope de horas extras dobles es POR SEMANA y }
                     { no por per�odo de n�mina }
                     { En caso de quincenal, se tiene un tope por los 7 primeros d�as }
                     { y otro tope por el resto }
                     { Para per�odos semanales, esto no afecta. }
                     h_dobles := ZetaCommonTools.rMax( ZetaCommonTools.rMin( h_dobles, FDatosJornada.Dobles - tot_dobles_semana ), 0 );
                end;
           end
           else //eteRegla3X3
           begin
                h_s_dobles := 0;
                if ( h_extras > 0 ) and ( tot_dias_extras_sem < K_EXENTAS_SEMANAL ) then
                   h_dobles := ZetaCommonTools.rMin( h_extras, K_EXENTAS_DIARIO )
                else
                    h_dobles := 0;
           end;

           with oZetaProvider do
           begin
                ParamAsInteger( FTarjetaEscribe, 'EMPLEADO', iEmpleado );                   { CB_CODIGO }
                ParamAsDate( FTarjetaEscribe, 'FECHA', dAusencia );                         { AU_FECHA }
                ParamAsChar( FTarjetaEscribe, 'HORARIO', sHorario, K_ANCHO_HORARIO );    { HO_CODIGO }
                ParamAsInteger( FTarjetaEscribe, 'STATUS', Ord( StatusDia ) );              { AU_STATUS }
                ParamAsInteger( FTarjetaEscribe, 'POSICION', iPosicion );                   { AU_POSICIO }
                ParamAsChar( FTarjetaEscribe, 'INCIDENCIA', sIncidencia, K_ANCHO_TIPO ); { AU_TIPO }
                ParamAsInteger( FTarjetaEscribe, 'TIPODIA', Ord( TipoDia ) );               { AU_TIPODIA }
                ParamAsFloat( FTarjetaEscribe, 'ORDINARIAS', h_ordinarias );                { AU_HORAS }
                ParamAsFloat( FTarjetaEscribe, 'DOBLES', h_dobles + h_s_dobles );           { AU_DOBLES }
                ParamAsFloat( FTarjetaEscribe, 'TRIPLES', ( h_extras - ( h_dobles + h_s_dobles ) ) ); { AU_TRIPLES }
                ParamAsFloat( FTarjetaEscribe, 'DESCANSO', h_des_tra );                     { AU_DES_TRA }
                ParamAsFloat( FTarjetaEscribe, 'PERMISOCG', h_per_cg );                     { AU_PER_CG }
                ParamAsFloat( FTarjetaEscribe, 'PERMISOSG', h_per_sg );                     { AU_PER_SG }

{$ifdef QUINCENALES}
                ParamAsFloat( FTarjetaEscribe, 'HORAS_NT', rHorasNT );                     { AU_HORASNT }
{$endif}
                Ejecuta( FTarjetaEscribe );
           end;
           tot_dobles := tot_dobles + h_dobles;
           dAusencia := dAusencia + 1;
{$ifdef QUINCENALES}
           if ( dAusencia >= dCorteExtras ) then  { Semana nueva }
           begin
                if FTopaPorSemana then
                   dCorteExtras := ( dCorteExtras + 7 )
                else
                    { Para Quincenales, se topan las horas extras dobles }
                    { de los primeros 7 d�as }
                    { Ej. Si la quincena tiene 15 d�as, son 7 d�as con un tope semanal y }
                    { 8 d�as con otro tope semanal }
                    dCorteExtras := dFechaLimite + 1;
                tot_dobles_semana := 0;
                tot_dias_extras_sem := 0;
           end
{$else}
           if ( iPosicion = 7 ) then
           begin
                tot_dobles_semana := 0;
                tot_dias_extras_sem := 0;
           end
{$endif}
           else
           begin
                tot_dobles_semana := tot_dobles_semana + h_dobles;
                if ( h_extras > 0 ) then
                   tot_dias_extras_sem := tot_dias_extras_sem + 1;
           end;
{$ifdef QUINCENALES}
           if ( iDiasFondo > 0 ) and ( sIncidencia = K_I_FONDO ) then
                iDiasFondo := iDiasFondo - 1
           else
           if not ( oZetaProvider.DatosPeriodo.Clasifi in [ tpQuincenal ] ) then
           { EM/ER 17/Jun/2005 : La Funci�n que clasifica los tipos de dia ( GetTipoDia ) recibe la variable lActivo la cual es inicializada
             como FALSE antes de empezar a revisar las tarjetas, solo debe y cambiar� a TRUE despues de revisar cada tarjeta con el primer d�a
             diferente de 'No Trabajado', esto permite configurar correctamente los dias no trabajados anteriores al ingreso.
             Sin embargo esta l�gica solo es deseable en n�minas semanales. Para los dem�s tipos de n�mina la variable debe quedarse con el
             valor FALSE, para que la funci�n GetTipoDia(), clasifica como daNoTrabajado todos aquellos en que el status del empleado
             sea 'steBaja'. Cuando el status del empleado es 'steAnterior' no hay problema por que en estos casos tambien llegaba como FALSE.
             CM 21/Jun/2005 : Respecto a este cambio, y platicando con Veronica, sugiero que se quede solo para nominas "Quincenales".
             Actualmente ya hay nominas catorcenales configuradas y no me gustaria que les presentara confusion el cambio.
             Considerando que estos cambios actualmente son para sacar adelante la configuracion de quincenales, \
             es mas facil controlarlos solo a ellos, y conforme vaya avanzando esto, y dedicando tiempo de analisis e investigacion
             aplicar el criterio para catorcenal y mensual.
           }
{$endif}
               lActivo := ( TipoDia <> daNoTrabajado );

{$ifdef QUINCENALES}
     until ( dAusencia > dFechaLimite );
{$else}
     until ( dAusencia > oZetaProvider.DatosPeriodo.Fin );
{$endif}
end;

function TTarjeta.CheckFestivo( const sTurno: String; const dFestivo: TDate ): Boolean; { Supone que ya fu� ejecutado CreaListaFestivos }
begin
     Result := Ritmos.Festivos.CheckFestivo( sTurno, dFestivo );
end;

function TTarjeta.GetStatus( const iEmpleado: TNumEmp; const dReferencia: TDate; var Incidencia: TCodigo ): eStatusEmpleado;
begin
{$ifdef INTERBASE}
     with FStatusActivo do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FStatusActivo, 'Empleado', iEmpleado );
               ParamAsDate( FStatusActivo, 'Fecha', dReferencia );
          end;
          Active := True;
          if Eof then
          begin
               Result := steAnterior;
               Incidencia := '';
          end
          else
          begin
               Result := eStatusEmpleado( FieldByName( 'RESULTADO' ).AsInteger );
               Incidencia := FieldByName( 'INCIDENCIA' ).AsString;
          end;
          Active := False;
     end;
{$else}
    with oZetaProvider do
    begin
        ParamAsInteger( FStatusActivo, 'Empleado', iEmpleado );
        ParamAsDate( FStatusActivo, 'Fecha', dReferencia );
        Ejecuta( FStatusActivo );
        Result := eStatusEmpleado( GetParametro( FStatusActivo, 'Resultado' ).AsInteger );
        Incidencia := GetParametro( FStatusActivo, 'Incidencia' ).AsString;
    end;
{$endif}
end;

end.
