library Cafeteria;

uses
  ComServ,
  Cafeteria_TLB in 'Cafeteria_TLB.pas',
  DServerCafeteria in 'DServerCafeteria.pas' {dmServerCafeteria: TMtsDataModule} {dmServerCafeteria: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
