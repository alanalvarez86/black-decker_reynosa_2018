unit DServerAsistencia;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DServerAsistencia.pas                      ::
  :: Descripci�n: Programa principal de Asistencia.dll       ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, DataBkr, DBClient, Db,
     MtsRdm, Mtx, Provider,
     Variants,
     {$ifndef DOS_CAPAS}
     {$ifdef ASISTENCIA_DLL}
     Asistencia_TLB,
     {$endif}
     {$endif}
     DTarjeta, DQueries,
     ZetaCommonClasses, 
     ZetaCommonLists,
     ZetaServerDataSet,
     ZCreator,
     ZetaSQLBroker,
     DZetaServerProvider;
     {$define VALIDA_ASIST_BD}
type
  eTipoAsistencia = ( eAsisTarjeta,
                      eAutorizaciones,
                      eAsisCalendario,
                      eHorarioTemp,
                      eAsisChecadas,
                      eClasifTemp,
                      eTransferencias,
                      eSynelAlarma );
  TdmServerAsistencia = class(TMtsDataModule {$ifndef DOS_CAPAS}{$ifdef ASISTENCIA_DLL}, IdmServerAsistencia {$endif}{$endif} )
    cdsLista: TServerDataSet;
    cdsErroresAuto: TServerDataSet;
    cdsErroresAutoCB_CODIGO: TIntegerField;
    cdsErroresAutoAU_FECHA: TDateTimeField;
    cdsErroresAutoHORAS: TFloatField;
    cdsErroresAutoCH_TIPO: TSmallintField;
    cdsErroresAutoCH_HOR_DES: TFloatField;
    cdsErroresAutoUS_COD_OK: TIntegerField;
    cdsASCII: TServerDataSet;
    procedure dmServerAsistenciaCreate(Sender: TObject);
    procedure dmServerAsistenciaDestroy(Sender: TObject);
  private
    { Private declarations }
    {$ifdef BITACORA_DLLS}
    FListaLog : TAsciiLog;
    FPaletita : string;
    FArchivoLog: string;
    FEmpresa:string;
    {$endif}

    oZetaProvider: TdmZetaServerProvider;
    FTarjeta: TTarjeta;
    FEmpleado: TNumEmp;
    FFecha: TDate;
    FLogData: String;
    FLogOperation: String;
    FOperacion: eOperacionConflicto;
    FQryActual: TZetaCursor;
    FQryValida: TZetaCursor;
{$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
{$endif}
    FListaParametros: string;
    FQueryAusenciaValida: TZetaCursor;
    function GetSQLBroker: TSQLBroker;
    function AutorizarHorasDataset(Dataset: TDataset): OleVariant;
    function BorrarTarjetasDataset( Dataset: TDataset ): OleVariant;
    function CalcularTarjeta: Boolean;
    function ConfiguraBorrarAuto(var sLista: String): Boolean;
    function CorregirFechasDataset( Dataset: TDataset ): OleVariant;
    function LeeUnaTarjeta( const Empresa: OleVariant; const iEmpleado: TNumEmp; const dFecha: TDate; var Checadas: OleVariant): OleVariant;
    function ReadVacaDiasGozados( const iEmpleado: Integer; const dFechaIni, dFechaFin: TDate ): TPesos;
    function ReadVacaFechaRegreso( const iEmpleado: Integer; const dFechaIni: TDate; const rGozo: TPesos; out rRango: Double ): TDate;
    function GetValorVacaciones( oDatosTurno: TEmpleadoDatosTurno; oCursor: TZetaCursor; const dFecha: TDate ): TPesos;

    function GetValorDiaPermiso( oDatosTurno: TEmpleadoDatosTurno; oCursor: TZetaCursor; const dFecha: TDate ): TPesos;
    function ReadPermisoDiasHabiles( const iEmpleado: Integer; const dFechaIni, dFechaFin: TDate ): TPesos;
    function ReadPermisoFechaRegreso( const iEmpleado: Integer; const dFechaIni: TDate; const rDias: TPesos; out rRango: Double ): TDate;
    function ReadEstatusDia( const iEmpleado: Integer; const dFecha: TDate): Currency;

    procedure SetTablaInfo( const eTipo: eTipoAsistencia );
    procedure AutorizarHorasBuildDataset;
    procedure BorrarTarjetasBuildDataset;
    procedure BorrarTarjetasParametros;
    procedure CorregirFechasBuildDataset;
    procedure CorregirFechaParametros;
    {$ifdef VALIDA_ASIST_BD}
    procedure GetTarjetaInfo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure GetChecadaInfo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    {$else}
    procedure SetTarjetaInfo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure GetTarjetaInfo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure GetChecadaInfo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    {$endif}
    procedure GetAutorizacionPeriodo(Sender: TObject; DataSet: TzProviderClientDataSet);
    procedure BeforeUpdateAutorizacion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateTransferencia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure AfterUpdateTransferencia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure EscribeHorarioTemporal(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure SetAutorizacion(DataSet: TzProviderClientDataSet);
    procedure SetAutorizacionKey(DataSet: TzProviderClientDataSet);
    procedure BeforeUpdateClasifiTemp(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure AfterUpdateClasifiTemp(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure AfterUpdateAutoXAprobar(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure ClearBroker;
    procedure ClearTarjeta;
    procedure InitCreator;
    procedure InitBroker;
    procedure InitTarjeta;
    procedure InitTarjetaInfo;
    procedure InitQueries;
    procedure InitRitmos;
    procedure CargaParametrosAutorizar;
    procedure ClasificacionTempValidaASCII(DataSet: TDataset; var nProblemas: Integer; var sErrorMsg: String);
    procedure PreparaCalendarioEmpleado( const iEmpleado: Integer; const dInicio, dFinal: TDate );
    function IntercambioFestivoDataset( Dataset: TDataset ): OleVariant;
    procedure CargaParametrosIntercambioFestivo;
    procedure IntercambioFestivoBuildDataset( sFecha: string );
    function  CancelarExcepcionesFestivosDataset( Dataset: TDataset ): OleVariant;
    procedure CargaParametrosCancelarExcepcionesFestivos;
    procedure CancelarExcepcionesFestivosBuildDataset( sFecha: string );
    {Ajustar Incapacidades en Calendario}
    procedure AjustarIncapaCalParametros;
    function AjustarIncapaCalDataset( Dataset: TDataset ): OleVariant;
    function AjustarIncapaCalBuildDataset: OleVariant;
    procedure SetCondiciones(var sCondicion, sFiltro: String);
    procedure InitLog(Empresa:Olevariant; const sPaletita: string);
    procedure EndLog;
    procedure AutorizacionPreNominaBuildDataset;
    procedure AutorizacionPreNominaParametros;
    function  AutorizacionPreNominaDataset( Dataset: TDataset ): OleVariant;
    procedure CargaParametrosRegistrarExcepcionesFestivos;
    function RegistrarExcepcionesFestivosDataset( Dataset: TDataset ): OleVariant;
    procedure ValidaEnPrenominas(const iEmpleado:Integer;const dFecha:TDate);

    procedure TransferenciasCosteoParametros;
    procedure TransferenciasCosteoBuildDataset;
    function TransferenciasCosteoDataset(Dataset: TDataset): OleVariant;
    procedure ImportarClasificacionTempCrearDataSet;
    procedure ImportarClasificacionTempBuildDataSet;
    procedure ImportarClasificacionesTempParametros;
    
    procedure AvisarTarjetas( const iEmpleado : Integer; const dFecha : TDateTime );

    procedure CancelaTransferenciasCosteoParametros;
    procedure CancelaTransferenciasCosteoBuildDataset;
    function CancelaTransferenciasCosteoDataset(Dataset: TDataset): OleVariant;

    procedure CalculaCosteoParametros;
    procedure IncluirBitacora(const eClase: eClaseBitacora; const iEmpleado: Integer; const dFecha: TDate; const sMensaje: TBitacoraTexto; const sTexto: String);

    procedure BitacoraAprobacion( const eClase: eClaseBitacora; const iEmpleado: Integer;  const fUsuario: TField; const dFecha: TDate; const sMensaje: String; const HorasAutorizadas: Currency; const fHorasDes: TField; const UpdateKind: TUpdateKind);

  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    property SQLBroker: TSQLBroker read GetSQLBroker;
    function GetEstatusDia(Empresa: OleVariant; Empleado: Integer;
      Fecha: TDateTime): Currency; safecall;

{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetAutorizaciones(Empresa: OleVariant; Fecha: TDateTime;lSoloAltas: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCalendario(Empresa: OleVariant; Empleado: Integer; FechaIni, FechaFin: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetHorarioTemp(Empresa: OleVariant; Empleado: Integer; FechaIni, FechaFin: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTarjeta(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; out Checadas: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaAutorizaciones(Empresa, oDelta: OleVariant; out ErrorCount: Integer; out ErrorData: OleVariant; Operacion: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCalendario(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaHorarioTemp(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaTarjeta(Empresa, oDelta, oChecadas: OleVariant; var ErrorCount, ErrorChCount: Integer; out Valor, oNewTarjeta, oNewChecadas: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function BorrarPoll(Usuario: Integer; Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function BorrarTarjetas(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CorregirFechas(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AutorizarHoras(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AutorizarHorasGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AutorizarHorasLista(Empresa, Lista,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetHorarioStatus(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetVacaDiasGozar(Empresa: OleVariant; Empleado: Integer; FechaIni, FechaFin: TDateTime): Double; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetVacaFechaRegreso(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; DiasGozo: Double; out DiasRango: Double): TDateTime; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetAutoXAprobar(Empresa: OleVariant; Fecha: TDateTime; Todos: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaAutoXAprobar(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetClasificacionTemp(Empresa: OleVariant; Fecha: TDateTime; const Empleados: WideString; SoloAltas: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaClasificacionTemp(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCalendarioEmpleado(Empresa: OleVariant; Empleado: Integer; FechaIni, FechaFin: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PuedeCambiarTarjetaStatus(Empresa: OleVariant; Fecha: TDateTime; Empleado, StatusLimite: Integer): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CancelarExcepcionesFestivos(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function CancelarExcepcionesFestivosGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function CancelarExcepcionesFestivosLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function IntercambioFestivo(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function IntercambioFestivoGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function IntercambioFestivoLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    {Ajustar Incapacidades en Calendario}    
    function AjustarIncapaCal(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function AjustarIncapaCalGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function AjustarIncapaCalLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function AutorizacionPreNomina(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function AutorizacionPreNominaGetLista(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function AutorizacionPreNominaLista(Empresa, Lista,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetCosteoTransferencias(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;{$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaCosteoTransferencias(Empresa, Delta, Parametros: OleVariant; out Resultdata: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}

    function RegistrarExcepcionesFestivo(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function RegistrarExcepcionesFestivoGetLista(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function RegistrarExcepcionesFestivoLista(Empresa, Parametros,Lista: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}

    function TransferenciasCosteo(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function TransferenciasCosteoGetLista(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function TransferenciasCosteoLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetClasificacionTempLista(Empresa, Parametros, Lista: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetClasificacionTempGetLista(Empresa, Parametros, Lista: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ImportarClasificacionesTemp(Empresa, Parametros, Lista: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CalculaCosteo(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSuperXCentroCosto(Empresa: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}

    function CancelaTransferenciasCosteo(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CancelaTransferenciasCosteoGetLista(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CancelaTransferenciasCosteoLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}

    function GetAlarmasSynel(Empresa: OleVariant; const Filtro: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaAlarmaSynel(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetRutaReporteSynel(Empresa: OleVariant; Reporte: Integer): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetListaReloj(Empresa: OleVariant): OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}
    function GetHorarioTurno(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}

{$ifndef DOS_CAPAS}
{$ifndef ASISTENCIA_DLL}
 public
{$endif}
{$endif}
    function GetPermisoDiasHabiles(Empresa: OleVariant; Empleado: Integer; FechaIni, FechaFin: TDateTime): Double;  {$ifndef DOS_CAPAS}{$ifdef ASISTENCIA_DLL}safecall;{$endif}{$endif}
    function GetPermisoFechaRegreso(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; Dias: Double; out DiasRango: Double): TDateTime;     {$ifndef DOS_CAPAS}{$ifdef ASISTENCIA_DLL}safecall;{$endif}{$endif}
  end;

implementation

{$R *.DFM}

uses ZetaSQL,
     ZetaCommonTools,
     ZetaServerTools,
     ZetaTipoEntidad,
     ZGlobalTress,
     ZEvaluador,
     DSuperASCII, Math;

procedure TdmServerAsistencia.InitLog( Empresa: Olevariant; const sPaletita: string );
begin
     {$ifdef BITACORA_DLLS}
     FPaletita := sPaletita;
     FArchivoLog := 'c:\BitacoraTress '+ FormatDateTime('dd_mmm_yy', Date()) +'.txt';

     if FListaLog = NIL then
     begin
          try
             FListaLog :=  TAsciiLog.Create;
             FListaLog.Init(FArchivoLog);
             try
                if NOT varisNull( Empresa ) then
                   try
                      FEmpresa :=  ' -- Alias: ' + Empresa[P_CODIGO] +
                                   ' Usuario: ' + InTToStr(Empresa[P_USUARIO])
                   except
                         FEmpresa := ' Error al leer Empresa[]';
                   end
                else
                    FEmpresa := '';
             except
                   FEmpresa := ' Error al leer Empresa[]';
             end;

             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) +
                                   ' DServerAsistencia :: INICIA :: '+  FPaletita +
                                   FEmpresa );
             FListaLog.Close;
          except
          end;
     end;
     {$endif}
end;

procedure TdmServerAsistencia.EndLog;
begin
     {$ifdef BITACORA_DLLS}
     try
        if FArchivoLog= '' then
           FArchivoLog := 'c:\BitacoraTress '+ FormatDateTime('dd_mmm_yy', Date()) +'.txt';
        if FListaLog = NIL then
        begin
             FListaLog :=  TAsciiLog.Create;
             FListaLog.Init(FArchivoLog);
             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) + ' DServerAsistencia :: TERMINA :: '+ FPaletita +' '+ FEmpresa);
        end
        else
        begin
             //FListaLog.Open(FArchivoLog);
             FListaLog.Init(FArchivoLog);
             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',NOw) + ' DServerAsistencia :: TERMINA :: '+ FPaletita +' '+ FEmpresa  );
             //FListaLog.Close;
        end;
        FListaLog.Close;
        FreeAndNil(FListaLog);
     except

     end;
     {$endif}
end;


{$define QUINCENALES}
const
     Q_POLL_BORRA_TODO = 0;
     Q_POLL_BORRA_RANGO = 1;
     Q_BORRAR_TARJETAS_TODA = 2;
     Q_BORRAR_TARJETAS_CHECADAS = 3;
     Q_RECALCULAR_TARJETAS_BORRA_AUTO = 4;
     Q_RECALCULAR_TARJETAS_REVISA_HORARIO = 5;
     Q_CORREGIR_FECHAS = 6;
     Q_LEE_AUTORIZACIONES = 7;
     Q_LEE_HORARIO = 8;
     Q_LEE_TARJETA = 9;
     Q_LEE_ACTUALES = 10;
     Q_LEE_CHECADAS = 11;
     Q_CHECADAS = 12;
     Q_LEE_TARJETA_VACA = 13;
     Q_LEE_AUTO_X_APROBAR_SUPER = 14;
     Q_LEE_AUTO_X_APROBAR_TODOS = 15;
     Q_LEE_CLASIFI_TEMP = 16;
     Q_AVISAR_TARJETAS = 17;
     Q_LEE_TARJETAS_RANGO = 18;
     Q_SP_STATUS_TARJETA = 19;
     Q_SELECT_TARJETAS_TIPO_FESTIVO = 20;
     Q_UPDATE_TARJETAS_TIPO_FESTIVO = 21;
     Q_SELECT_TARJETAS_CANCELAR_EXCEP_FESTIVO = 22;
     Q_UPDATE_TARJETAS_CANCELAR_EXCEP_FESTIVO = 23;
     Q_UPDATE_AJUST_INCAPA_CAL = 24;
     Q_UPDATE_AUTORIZACION_PRENOMINA = 25;
     Q_STATUS_X_BLOQUEO = 26;
     Q_SELECT_TARJETAS_REGISTRAR_EXCEP_FESTIVO = 27;
     Q_UPDATE_TARJETAS_REGISTRAR_EXCEP_FESTIVO = 28;
     Q_LEE_COSTEO_TRANSFERS = 29;
     Q_INSERT_COSTEO_TRANSFERS = 30;
     Q_EXISTE_COSTEO_TRANSFERS = 31;
     Q_SELECT_ASISTENCIA_CLASIFICACIONES_TEMPORALES = 32;
     Q_SELECT_ASISTENCIA_CLASIFITMP = 33;
     Q_INSERT_ASISTENCIA_CLASIFITMP = 34;
     Q_UPDATE_ASISTENCIA_CLASIFITMP = 35;
     Q_SELECT_ASISTENCIA_CLASIFICACIONES_TEMPORALES_EXISTECATALOGO =36;
     Q_COSTEO_CUENTA_NOMINAS = 37;
     Q_COSTEO_CALCULA = 38;
     Q_LEE_SUPER_X_CENTRO_COSTO = 39;
     Q_CANCELAR_TRANSFERENCIA = 40;    
     Q_GET_RUTA_ARCHIVO_REPORTE = 41;
     Q_GET_LISTA_RELOJ = 42;

     K_HORA_DUMMY = '----';

     D_TEXT_NIVEL_PUESTO = 'Puesto';
     D_TEXT_NIVEL_CLASIFICACION = 'Clasificaci�n';
     D_TEXT_NIVEL_TURNO = 'Turno';     
     K_INDICE_TODAS_COSTEO  = 0;

     K_VALOR_DIA_PERMISO_HABIL = 1 ;
     K_VALOR_DIA_PERMISO_SABADO = 0;
     K_VALOR_DIA_PERMISO_DESCANSO = 0;

     Q_TURNO_FESTIVO = 43;
     Q_UPDATE_HORARIO_TARJETAS_TIPO_FESTIVO = 44;


function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_POLL_BORRA_TODO: Result := 'delete from POLL';
          Q_POLL_BORRA_RANGO: Result := 'delete from POLL where( PO_FECHA >= ''%s'' ) and ( PO_FECHA <= ''%s'' )';
          Q_BORRAR_TARJETAS_TODA: Result := 'delete from AUSENCIA where '+
                                            '( CB_CODIGO = :Empleado ) and '+
                                            '( AU_FECHA = :Fecha )';
          Q_BORRAR_TARJETAS_CHECADAS: Result := 'delete from CHECADAS where '+
                                                '( CB_CODIGO = :Empleado ) and '+
                                                '( AU_FECHA = :Fecha ) '+
                                                ' %s ';
          Q_RECALCULAR_TARJETAS_BORRA_AUTO: Result := 'delete from CHECADAS where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha ) and ( CH_TIPO in ( %s ) ) %s';
          Q_RECALCULAR_TARJETAS_REVISA_HORARIO: Result := 'update AUSENCIA set '+
                                                          'AU_HOR_MAN = :EsManual, '+
                                                          'AU_STATUS = :Status, '+
                                                          'HO_CODIGO = :Horario '+
                                                          'where ( AU_FECHA = :Fecha ) and '+
                                                          '( CB_CODIGO = :Empleado )';
          Q_CORREGIR_FECHAS: Result := 'update AUSENCIA set '+
                                       'AU_FECHA = :FechaNueva, '+
                                       'AU_STATUS = :Status, '+
                                       'HO_CODIGO = :Horario, '+
                                       'PE_YEAR = 0, ' +
                                       'PE_TIPO = 0, ' +
                                       'PE_NUMERO = 0 ' +
                                       'where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha )';
          Q_LEE_AUTORIZACIONES: Result := 'select C.CB_CODIGO, C.AU_FECHA, C.CH_DESCRIP, C.CH_GLOBAL, C.CH_H_AJUS, ' +
                                          'C.CH_H_REAL, C.CH_HOR_DES, C.CH_HOR_EXT, C.CH_HOR_ORD, C.CH_IGNORAR, ' +
                                          'C.CH_RELOJ, C.CH_SISTEMA, C.CH_TIPO, C.US_CODIGO, C.CH_POSICIO, C.US_COD_OK, ' +
                                          'C.CH_HOR_EXT + C.CH_HOR_ORD as HORAS, 0 as OPERACION, ' + K_PRETTYNAME + ' as PrettyName, ' +
                                          'M.TB_ELEMENT as MOTIVOAUTO, 0 as CAMBIO ' +
                                          'from CHECADAS C ' +
                                          'left outer join COLABORA E on ( E.CB_CODIGO = C.CB_CODIGO ) ' +
                                          'left outer join MOT_AUTO M on ( M.TB_CODIGO = C.CH_RELOJ ) ' +
                                          'where ( C.AU_FECHA = ''%s'' ) and ( C.CH_TIPO > ' + IntToStr( Ord( chFin ) ) + ' ) %s ' +
                                          'order by C.CB_CODIGO';
          Q_LEE_HORARIO: Result := 'select HO_DESCRIP from HORARIO where ( HO_CODIGO = :Horario )';
          Q_LEE_TARJETA: Result := 'select HO_CODIGO, AU_HOR_MAN, AU_STATUS, AU_HORASCK ' +
                                    'from  AUSENCIA where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha )';
          Q_LEE_ACTUALES: Result := 'select ( CH_HOR_ORD + CH_HOR_EXT ) as HORAS, CH_HOR_DES, US_COD_OK from CHECADAS ' +
                                    'where ( AU_FECHA = :Fecha ) and ( CB_CODIGO = :Empleado ) and ( CH_H_REAL = :Hora ) and ( CH_TIPO = :Tipo )';
          Q_LEE_CHECADAS: Result := 'select CH_H_AJUS from CHECADAS ' +
                                    'where ( CB_CODIGO = %d ) and ( AU_FECHA = ''%s'' ) and ( CH_SISTEMA <> ''%s'' )';
          Q_CHECADAS: Result := 'select CB_CODIGO, AU_FECHA, CH_DESCRIP, CH_GLOBAL, CH_H_AJUS, CH_H_REAL, '+
                                'CH_HOR_DES, CH_HOR_DES as CH_APRUEBA, CH_HOR_EXT, CH_HOR_ORD, CH_IGNORAR, CH_RELOJ, CH_SISTEMA, '+
                                'CH_TIPO, US_CODIGO, CH_POSICIO, CH_MOTIVO from CHECADAS where '+
                                '( CB_CODIGO = %d ) and ( AU_FECHA = ''%s'' ) '+
                                'order by CH_H_REAL, CH_TIPO'; {ACL150409}
          Q_LEE_TARJETA_VACA: Result := 'select AU_FECHA, AU_STATUS, AU_TIPODIA, AU_POSICIO ' +
                                        'from AUSENCIA ' +
                                        'where ( CB_CODIGO = %d ) and ( AU_FECHA = :Fecha )';
          Q_LEE_AUTO_X_APROBAR_SUPER : Result := 'select LISTA.CB_CODIGO, '  + K_PRETTYNAME + ' as PrettyName, '+
                                                 'LISTA.CB_NIVEL, LISTA.CH_TIPO, LISTA.CH_HORAS, ' + 
                                                 'LISTA.CH_MOTIVO, LISTA.CH_MOT_DES, LISTA.CH_HOR_OK CH_HOR_DES, LISTA.US_CODIGO, ' +
                                                 'LISTA.US_COD_OK,  %2:s AU_FECHA, ' + EntreComillas( K_HORA_DUMMY ) + ' CH_H_REAL ' +
{$ifdef INTERBASE}
                                                 'from SP_AUTO_X_APROBAR( %0:s, %1:d ) LISTA ' +
{$endif}
{$ifdef MSSQL}
                                                 'from dbo.SP_AUTO_X_APROBAR( %0:s, %1:d ) LISTA ' +
{$endif}
                                                 'left outer join COLABORA on ( COLABORA.CB_CODIGO = LISTA.CB_CODIGO ) %3:s';
          Q_LEE_AUTO_X_APROBAR_TODOS : Result := 'select LISTA.CB_CODIGO, '  + K_PRETTYNAME + ' as PrettyName, '+
                                                 'LISTA.CB_NIVEL, LISTA.CH_TIPO, LISTA.CH_HORAS, ' +
                                                 'LISTA.CH_MOTIVO, LISTA.CH_MOT_DES, LISTA.CH_HOR_OK CH_HOR_DES, LISTA.US_CODIGO, ' +
                                                 'LISTA.US_COD_OK, %1:s AU_FECHA, ' + EntreComillas( K_HORA_DUMMY ) + ' CH_H_REAL ' +
{$ifdef INTERBASE}
                                                 'from SP_AUTO_X_APROBAR_TODOS( %0:s ) LISTA ' +
{$endif}
{$ifdef MSSQL}
                                                 'from dbo.SP_AUTO_X_APROBAR_TODOS( %0:s ) LISTA ' +
{$endif}
                                                 'left outer join COLABORA on ( COLABORA.CB_CODIGO = LISTA.CB_CODIGO ) %2:s';
          Q_LEE_CLASIFI_TEMP : Result := 'select A.CB_CODIGO,A.AU_FECHA,A.CB_CLASIFI,A.CB_TURNO,A.CB_PUESTO,A.CB_NIVEL1,A.CB_NIVEL2,A.CB_NIVEL3,A.CB_NIVEL4,' +
                                         'A.CB_NIVEL5,A.CB_NIVEL6,A.CB_NIVEL7,A.CB_NIVEL8,A.CB_NIVEL9,'+
                                         {$ifdef ACS}'A.CB_NIVEL10,A.CB_NIVEL11,A.CB_NIVEL12,'+{$endif}
                                         'A.US_CODIGO,A.CT_FECHA,A.US_COD_OK,A.CT_FEC_OK,' +
                                         K_PRETTYNAME + ' as PrettyName '+
                                         'from CLASITMP A ' +
                                         'left outer join COLABORA B on ( A.CB_CODIGO = B.CB_CODIGO ) '+
                                         'where %s( A.AU_FECHA = %s ) %s order by A.CB_CODIGO, A.AU_FECHA';
          Q_AVISAR_TARJETAS: Result := 'execute procedure SP_CLAS_AUSENCIA( :Empleado, :Fecha )';
          Q_LEE_TARJETAS_RANGO: Result := 'select A.AU_FECHA, A.CB_TURNO, A.HO_CODIGO, A.AU_STATUS, T.TU_VACA_HA, T.TU_VACA_SA,T.TU_VACA_DE,' +
{$ifdef INTERBASE}
                                          '( select RESULTADO from SP_STATUS_ACT( A.AU_FECHA, A.CB_CODIGO ) ) STATUS_ACT ' +
{$endif}
{$ifdef MSSQL}
                                          '( dbo.SP_STATUS_ACT( A.AU_FECHA, A.CB_CODIGO ) ) STATUS_ACT ' +
{$endif}
                                          'from AUSENCIA A left outer join TURNO T on T.TU_CODIGO = A.CB_TURNO where ( A.CB_CODIGO = %d ) and ( A.AU_FECHA between %s and %s )';{OP: 11/06/08}
          Q_SP_STATUS_TARJETA:
{$ifdef INTERBASE}
                 Result := 'Select Resultado FROM SP_STATUS_TARJETA (%d, %s)';
{$ELSE}
                 Result := 'Select Resultado = dbo.SP_STATUS_TARJETA (%d, %s)';
{$ENDIF}
        Q_SELECT_TARJETAS_TIPO_FESTIVO : Result:= 'Select AU_STA_FES, PE_YEAR, PE_TIPO, PE_NUMERO from AUSENCIA where CB_CODIGO = :Empleado and AU_FECHA = :Fecha';
        Q_UPDATE_TARJETAS_TIPO_FESTIVO : Result := 'Update AUSENCIA set AU_STA_FES = :StatusFestivo where CB_CODIGO = :Empleado and AU_FECHA = :Fecha';
        Q_SELECT_TARJETAS_CANCELAR_EXCEP_FESTIVO : Result := 'Select PE_YEAR, PE_TIPO, PE_NUMERO,AU_FECHA from AUSENCIA where CB_CODIGO = :Empleado and AU_FECHA between %s and %s and AU_STA_FES <> %d ';
        Q_UPDATE_TARJETAS_CANCELAR_EXCEP_FESTIVO : Result := 'Update AUSENCIA set AU_STA_FES = %2:d where CB_CODIGO = :Empleado and AU_FECHA between %0:s and %1:s and AU_STA_FES <> %2:d';
        Q_UPDATE_AJUST_INCAPA_CAL: Result  := 'update AUSENCIA set AU_TIPO =:IN_TIPO,  AU_TIPODIA =:AU_TIPODIA where CB_CODIGO=:CB_CODIGO AND ( AU_FECHA = :AU_FECHA )';
        Q_UPDATE_AUTORIZACION_PRENOMINA: Result := 'update NOMINA set NO_SUP_OK = :USUARIO, NO_FEC_OK = :Fecha, NO_HOR_OK = :HORA Where PE_YEAR = :PE_YEAR and PE_TIPO = :PE_TIPO and PE_NUMERO = :PE_NUMERO and CB_CODIGO = :CB_CODIGO';
        Q_LEE_COSTEO_TRANSFERS: Result := 'select	TRANSFER.LLAVE, '+
                                                 'TRANSFER.CB_CODIGO, '+
                                                 'COLABORA.PRETTYNAME, '+
                                                 'TRANSFER.AU_FECHA, '+
                                                  //--Datos de la tarjeta
                                                 'AUSENCIA.%s CC_ORIGEN, '+
                                                 'case TR_TIPO WHEN 0 THEN AUSENCIA.AU_HORAS ELSE AUSENCIA.AU_DES_TRA + AUSENCIA.AU_EXTRAS END ASIS_HORAS, '+
                                                  //--Datos de la transferencia
                                                 'CC_CODIGO, '+
                                                 'TR_HORAS, '+
                                                 'TR_TIPO, '+
                                                 'TR_MOTIVO, '+
                                                 'TR_NUMERO, '+
                                                 'TR_TEXTO, '+
                                                 'TRANSFER.US_CODIGO, '+
                                                 'TR_FECHA, '+
                                                 'TR_GLOBAL, '+
                                                 'TR_APRUEBA, '+
                                                 'TR_FEC_APR, '+
                                                 'TR_STATUS, '+
                                                 'TR_TXT_APR '+
                                          'from TRANSFER '+
                                          'left outer join COLABORA on COLABORA.CB_CODIGO = TRANSFER.CB_CODIGO '+
                                          'left outer join AUSENCIA on AUSENCIA.CB_CODIGO = TRANSFER.CB_CODIGO '+
                                          'and AUSENCIA.AU_FECHA = TRANSFER.AU_FECHA %s ';
        Q_INSERT_COSTEO_TRANSFERS: Result := 'insert INTO TRANSFER(CB_CODIGO,AU_FECHA,TR_HORAS,TR_TIPO,TR_MOTIVO,CC_CODIGO, '+
                                                          'TR_NUMERO,TR_TEXTO,US_CODIGO,TR_FECHA,TR_GLOBAL,TR_APRUEBA,TR_FEC_APR,TR_STATUS) '+
                                                          'VALUES(:CB_CODIGO,:AU_FECHA,:TR_HORAS,:TR_TIPO,:TR_MOTIVO,:CC_CODIGO, '+
                                                          ':TR_NUMERO,:TR_TEXTO,:US_CODIGO,:TR_FECHA,:TR_GLOBAL,:TR_APRUEBA,:TR_FEC_APR,:TR_STATUS)';
        Q_EXISTE_COSTEO_TRANSFERS: Result := 'select CB_CODIGO, AU_FECHA from TRANSFER where CB_CODIGO=:CB_CODIGO and  AU_FECHA = :AU_FECHA and CC_CODIGO = :CC_CODIGO';
        Q_SELECT_TARJETAS_REGISTRAR_EXCEP_FESTIVO : Result := 'Select PE_YEAR, PE_TIPO, PE_NUMERO from AUSENCIA where CB_CODIGO = :Empleado and AU_FECHA = :Fecha and AU_STA_FES <> %d group by PE_YEAR, PE_TIPO, PE_NUMERO';
        Q_UPDATE_TARJETAS_REGISTRAR_EXCEP_FESTIVO : Result := 'Update AUSENCIA set AU_STA_FES = %1:d where CB_CODIGO = :Empleado and AU_FECHA = %0:s and AU_STA_FES <> %1:d';
        Q_COSTEO_CUENTA_NOMINAS: Result := 'select COUNT(*) Cuantos from NOMINA where  ( PE_YEAR = :Year ) and '+
                                                                                      '( PE_TIPO = :Tipo ) and '+
                                                                                      '( PE_NUMERO = :Numero ) ';
        Q_COSTEO_CALCULA: Result := 'execute procedure dbo.Costos_CalculaPeriodo( :Year, :Tipo, :Numero )';
        Q_LEE_SUPER_X_CENTRO_COSTO : Result := 'select * from SUP_COSTEO';
        Q_CANCELAR_TRANSFERENCIA: Result := 'delete from TRANSFER where ( LLAVE = :LLAVE )';
        Q_GET_RUTA_ARCHIVO_REPORTE: Result := 'select RE_ARCHIVO from REPORTE where RE_CODIGO=%d';
        Q_GET_LISTA_RELOJ: Result := 'select RELOJ from SY_ALARMA group by RELOJ order by RELOJ';

        {$IFDEF DOS_CAPAS}
        Q_SELECT_ASISTENCIA_CLASIFICACIONES_TEMPORALES : Result := 'SELECT CB_CODIGO, CB_APE_MAT || '' '' || CB_APE_MAT || '', '' || CB_NOMBRES as PRETTYNAME from COLABORA where CB_CODIGO=:CB_CODIGO  %s';
        {$ELSE}
        Q_SELECT_ASISTENCIA_CLASIFICACIONES_TEMPORALES : Result := 'SELECT CB_CODIGO, PRETTYNAME from COLABORA where CB_CODIGO=:CB_CODIGO  %s';
        {$ENDIF}
        Q_SELECT_ASISTENCIA_CLASIFITMP : Result := ' SELECT * FROM CLASITMP WHERE CB_CODIGO = :Empleado and AU_FECHA = :Fecha';
        Q_INSERT_ASISTENCIA_CLASIFITMP : Result := ' insert into CLASITMP (CB_CODIGO, AU_FECHA, CB_CLASIFI, CB_TURNO, CB_PUESTO, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9, US_CODIGO, CT_FECHA, US_COD_OK) ' +
                                                   ' Values (:CB_CODIGO, :AU_FECHA, :CB_CLASIFI, :CB_TURNO, :CB_PUESTO, :CB_NIVEL1, :CB_NIVEL2, :CB_NIVEL3, :CB_NIVEL4, :CB_NIVEL5, :CB_NIVEL6, :CB_NIVEL7, :CB_NIVEL8, :CB_NIVEL9, :US_CODIGO, :CT_FECHA, :US_COD_OK)';
        Q_UPDATE_ASISTENCIA_CLASIFITMP : Result := ' UPDATE CLASITMP SET '+
                                                          ' CB_CLASIFI = :CB_CLASIFI, CB_TURNO = :CB_TURNO, CB_PUESTO = :CB_PUESTO, '+
                                                          ' CB_NIVEL1 = :CB_NIVEL1, CB_NIVEL2 = :CB_NIVEL2, CB_NIVEL3 = :CB_NIVEL3, CB_NIVEL4 = :CB_NIVEL4, CB_NIVEL5 = :CB_NIVEL5, '+
                                                          'CB_NIVEL6 = :CB_NIVEL6, CB_NIVEL7 = :CB_NIVEL7, CB_NIVEL8 = :CB_NIVEL8, CB_NIVEL9 = :CB_NIVEL9, '+
                                                          {$IFDEF ACS}'CB_NIVEL10 = :CB_NIVEL10, CB_NIVEL11 = :CB_NIVEL11, CB_NIVEL12 = :CB_NIVEL12, ' + {$ENDIF}
                                                          'US_CODIGO = :US_CODIGO, CT_FECHA = :CT_FECHA, US_COD_OK = :US_COD_OK WHERE CB_CODIGO = :CB_CODIGO AND AU_FECHA = :AU_FECHA';
        Q_SELECT_ASISTENCIA_CLASIFICACIONES_TEMPORALES_EXISTECATALOGO : Result := 'Select %0:s from %1:s where %0:s = %2:s';

        Q_TURNO_FESTIVO: Result:= 'SELECT TU_HOR_FES FROM TURNO WHERE TU_CODIGO = :horario';
        Q_UPDATE_HORARIO_TARJETAS_TIPO_FESTIVO : Result := 'Update AUSENCIA set HO_CODIGO = :Horario where CB_CODIGO = :Empleado and AU_FECHA = :Fecha and AU_STA_FES = :Festivo ';


    end;
end;

{ ************ TdmServerAsistencia ************* }

class procedure TdmServerAsistencia.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerAsistencia.dmServerAsistenciaCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerAsistencia.dmServerAsistenciaDestroy(Sender: TObject);
begin
{$ifndef DOS_CAPAS}
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
{$endif}
end;

{ *********** Clases Auxiliares ************ }

{$ifdef DOS_CAPAS}
procedure TdmServerAsistencia.CierraEmpresa;
begin
end;
{$endif}

function TdmServerAsistencia.GetSQLBroker: TSQLBroker;
begin
{$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
{$else}
     Result := Self.oZetaSQLBroker;
{$endif}
end;

procedure TdmServerAsistencia.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerAsistencia.InitBroker;
begin
     InitCreator;
     oZetaCreator.RegistraFunciones([efComunes]);
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TdmServerAsistencia.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oZetaSQLBroker );
end;

procedure TdmServerAsistencia.InitQueries;
begin
     InitCreator;
     oZetaCreator.PreparaQueries;
end;

procedure TdmServerAsistencia.InitRitmos;
begin
     InitCreator;
     InitQueries;
     oZetaCreator.PreparaRitmos;
end;

procedure TdmServerAsistencia.InitTarjeta;
begin
     InitCreator;
     InitQueries;
     FTarjeta := TTarjeta.Create( oZetaCreator );
end;

procedure TdmServerAsistencia.ClearTarjeta;
begin
     FreeAndNil( FTarjeta );
end;

{ *********** Private ************ }

procedure TdmServerAsistencia.SetTablaInfo(const eTipo: ETipoAsistencia);
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTipo of
               eAutorizaciones, eAsisChecadas:
               begin
                    SetInfo( 'CHECADAS',
                             'CB_CODIGO, AU_FECHA, CH_DESCRIP, CH_GLOBAL, CH_H_AJUS, CH_H_REAL, CH_HOR_DES, CH_HOR_EXT, CH_HOR_ORD, CH_IGNORAR, CH_RELOJ, CH_SISTEMA, CH_TIPO, US_CODIGO, CH_POSICIO, US_COD_OK, CH_MOTIVO',
                             'CB_CODIGO, AU_FECHA, CH_H_REAL, CH_TIPO' );
               end;
               eAsisCalendario, eAsisTarjeta:
               begin
                    SetInfo( 'AUSENCIA',
                             'CB_CODIGO, AU_DES_TRA, AU_DOBLES, AU_EXTRAS, AU_FECHA, AU_HORAS, AU_PER_CG, AU_PER_SG, AU_TARDES, AU_TIPO, AU_TIPODIA,'+
                             'CB_CLASIFI, CB_TURNO, CB_PUESTO, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9, '+
                             {$ifdef ACS}'CB_NIVEL10, CB_NIVEL11, CB_NIVEL12, '+{$endif}                             
                             'AU_AUT_EXT, AU_AUT_TRA, AU_HOR_MAN, HO_CODIGO, AU_STATUS,AU_NUM_EXT, AU_TRIPLES, US_CODIGO, AU_POSICIO, AU_HORASCK, PE_YEAR, PE_TIPO, PE_NUMERO, AU_OUT2EAT, AU_STA_FES, AU_PRE_EXT'
{$ifdef QUINCENALES}
                             + ', CB_SALARIO'
{$endif}

                             +', CB_NOMINA' 
                             ,
                             'CB_CODIGO, AU_FECHA' );
               end;
               eHorarioTemp:
               begin
                    SetInfo( 'AUSENCIA',
                             'HO_CODIGO, AU_HOR_MAN, AU_STATUS',
                             'CB_CODIGO, AU_FECHA' );
               end;
               eClasifTemp:
               begin
                    SetInfo( 'CLASITMP', 'CB_CODIGO,AU_FECHA,CB_CLASIFI,CB_TURNO,CB_PUESTO,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,' +
                             'CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,CB_NIVEL9,'+
                             {$ifdef ACS}'CB_NIVEL10,CB_NIVEL11,CB_NIVEL12,'+{$endif}
                             'US_CODIGO,CT_FECHA,US_COD_OK,CT_FEC_OK',
                             'CB_CODIGO,AU_FECHA' );
               end;
               eTransferencias:
               begin
                    SetInfo( 'TRANSFER',
                             'CB_CODIGO,AU_FECHA,TR_HORAS,TR_TIPO,TR_MOTIVO,CC_CODIGO,TR_NUMERO,TR_TEXTO,US_CODIGO,TR_FECHA,LLAVE,'+
                             'TR_GLOBAL,TR_APRUEBA,TR_FEC_APR,TR_STATUS,TR_TXT_APR', 'LLAVE' );
               end;
			   eSynelAlarma:
               begin
                    SetInfo( 'SY_ALARMA', // Nombre de tabla
                             'RELOJ, HORA, LUNES, MARTES, MIERCOLES, JUEVES, VIERNES, SABADO, DOMINGO, TPO_OPERA, RELAY, ACTIVO, INACTIVO, TOTAL',  // Campos
                             'RELOJ, HORA' ); // Llave primaria
                    SetOrder( 'RELOJ, HORA' );
               end;
          end;
          if ( eTipo = eAsisCalendario ) then
             SetOrder( 'AU_FECHA' ); { PENDIENTE: Se necesita ordenar DESCENDENTEMENTE }
     end;
end;

function TdmServerAsistencia.ConfiguraBorrarAuto( var sLista: String ): Boolean;
begin
     with oZetaProvider.ParamList do
     begin
          Result := ParamByName( 'CancelarExtras' ).AsBoolean or
                    ParamByName( 'CancelarDescansos' ).AsBoolean or
                    ParamByName( 'CancelarPermisosConGoce' ).AsBoolean or
                    ParamByName( 'CancelarPermisosSinGoce' ).AsBoolean or
                    ParamByName( 'CancelarHorasPrepagadas' ).AsBoolean;
          sLista := '';
          if ParamByName( 'CancelarExtras' ).AsBoolean then
          begin
               sLista := sLista + IntToStr( Ord( chExtras ) ) + ',';
          end;
          if ParamByName( 'CancelarDescansos' ).AsBoolean then
          begin
               sLista := sLista + IntToStr( Ord( chDescanso ) ) + ',';
          end;
          if ParamByName( 'CancelarPermisosConGoce' ).AsBoolean then
          begin
               sLista := sLista + IntToStr( Ord( chConGoceEntrada ) ) + ',' + IntToStr( Ord( chConGoce ) ) + ',';
          end;
          if ParamByName( 'CancelarPermisosSinGoce' ).AsBoolean then
          begin
               sLista := sLista + IntToStr( Ord( chSinGoceEntrada ) ) + ',' + IntToStr( Ord( chSinGoce ) ) + ',';
          end;
          if ParamByName( 'CancelarHorasPrepagadas' ).AsBoolean then
          begin
               sLista := sLista + IntToStr( Ord( chPrepagFueraJornada ) ) + ',' + IntToStr( Ord( chPrepagDentroJornada ) ) + ',';
          end;
     end;
end;

procedure TdmServerAsistencia.InitTarjetaInfo;
begin
     FEmpleado := 0;
     FFecha := NullDateTime;
     FLogData := '';
     FLogOperation := '';
end;

function TdmServerAsistencia.CalcularTarjeta: Boolean;
begin
     Result := ( FEmpleado <> 0 ) and ( FFecha <> NullDateTime );
     if Result then
     begin
          with oZetaProvider do
          begin
               InitGlobales;
               InitTarjeta;
               try
                  if ZetaCommonTools.StrLleno( FLogOperation ) and ZetaCommonTools.StrLleno( FLogData ) then
                     EscribeBitacora( tbNormal, clbTarjeta, FEmpleado, FFecha, FLogOperation, FLogData );
                  with FTarjeta do
                  begin
                       CalculaUnaTarjetaBegin;
                       try
                          EmpiezaTransaccion;
                          try
                             CalculaUnaTarjeta( FEmpleado, FFecha );
                             TerminaTransaccion( True );
                          except
                                on Error: Exception do
                                begin
                                     //TerminaTransaccion( False );
                                     RollBackTransaccion;
                                     raise;
                                end;
                          end;
                       finally
                              CalculaUnaTarjetaEnd;
                       end;
                  end;
               finally
                      ClearTarjeta;
               end;
          end;
     end;
end;

{$ifdef VALIDA_ASIST_BD}
procedure TdmServerAsistencia.GetTarjetaInfo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
{$else}
procedure TdmServerAsistencia.GetTarjetaInfo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
{$endif}
var
   FChecadas: TZetaCursor;
begin
     with DeltaDS do
     begin
          case UpdateKind of
               ukModify:
               begin
                    FEmpleado := ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' ) );
                    FFecha := ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) );
                    {$ifdef VALIDA_ASIST_BD}
                    with oZetaProvider do { Bit�cora }
                    begin
                         if CambiaCampo(FieldByName('HO_CODIGO'))then
                         begin
                              EscribeBitacora( tbNormal,
                                               clbCambioHorario,
                                               FEmpleado,
                                               FFecha,
                                               Format( 'Cambi� Horario de %s a %s', [ FieldByName( 'HO_CODIGO' ).OldValue, FieldByName('HO_CODIGO').AsString ] ),
                                               ' ' );
                         end;
                         if CambiaCampo(FieldByName('AU_STATUS'))then
                         begin
                              EscribeBitacora( tbNormal,
                                               clbCambioTipoDia,
                                               FEmpleado,
                                               FFecha,
                                               Format( 'Cambi� Tipo de D�a de %s a %s', [ ObtieneElemento( lfStatusAusencia, FieldByName( 'AU_STATUS' ).OldValue ), ObtieneElemento( lfStatusAusencia, FieldByName( 'AU_STATUS' ).AsInteger  ) ] ),
                                               ' ' );
                         end;
                         if CambiaCampo(FieldByName('AU_STA_FES'))then
                         begin
                              EscribeBitacora( tbNormal,
                                               clbCambioFestivo,
                                               FEmpleado,
                                               FFecha,
                                               'Cambi� criterio para indicar si es festivo', Format ( 'Antes: %s %sNuevo: %s',[ ObtieneElemento( lfStatusFestivos, FieldByName('AU_STA_FES').OldValue ),
                                                                                                                                CR_LF,
                                                                                                                                ObtieneElemento( lfStatusFestivos, CampoAsVar( FieldByName('AU_STA_FES' ) ) ) ] ) );
                         end;
                    end;
                    FLogOperation := 'Tarjeta Modificada';
                    {$endif}
               end;
               ukInsert:
               begin
                    FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                    FFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
                    {$ifdef INTERMEX}
                    FLogOperation := 'Checada Agregada';
                    {$endif}
               end;
               ukDelete:
               begin
                    FEmpleado := ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' ) );
                    FFecha := ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) );
                    with oZetaProvider do
                    begin
                         FLogData := FLogData + Format( 'Checadas del %s del Empleado %d: ', [ FormatDateTime( 'dd/mmm/yyyy', FFecha ), FEmpleado ] ) + CR_LF;
                         FChecadas := CreateQuery( Format( GetSQLScript( Q_LEE_CHECADAS ), [ FEmpleado, ZetaCommonTools.DateToStrSQL( FFecha ), K_GLOBAL_SI ] ) );
                         try
                            with FChecadas do
                            begin
                                 Active := True;
                                 while not Eof do
                                 begin
                                      FLogData := FLogData + FieldByName( 'CH_H_AJUS' ).AsString + CR_LF;
                                      Next;
                                 end;
                                 Active := False;
                            end;
                         finally
                                FreeAndNil( FChecadas );
                         end;
                    end;
                    {$ifdef VALIDA_ASIST_BD}
                    FLogOperation := 'Tarjeta Borrada';
                    {$endif}
               end;
          end;
     end;
end;

{$ifdef VALIDA_ASIST_BD}
procedure TdmServerAsistencia.GetChecadaInfo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
{$else}
procedure TdmServerAsistencia.GetChecadaInfo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
{$endif}
var
   oCampo: TField;
begin
     with DeltaDS do
     begin
          case UpdateKind of
               ukModify:
               begin
                    FEmpleado := ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' ) );
                    FFecha := ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) );
                    oCampo := FieldByName( 'CH_H_AJUS' );
                    if ZetaServerTools.CambiaCampo( oCampo ) then
                    begin
                         with oCampo do
                         begin
                              FLogData := FLogData + Format( 'Cambio %s a %s', [ OldValue, NewValue ] );
                         end;
                    end;
               end;
               ukInsert:
               begin
                    FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                    FFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
                    {$ifdef INTERMEX}
                    if ( oZetaProvider.UsuarioActivo <> 0 ) then
                    begin
                         FLogOperation := 'Checada Agregada';
                         FLogData := FLogData + Format( 'Checada agregada con hora %s.', [ FieldByName( 'CH_H_REAL' ).AsString ] ) + CR_LF;
                    end
                    else
                    begin
                         FLogOperation := VACIO;
                    end;
                    {$endif}
               end;
               ukDelete:
               begin
                    FEmpleado := ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' ) );
                    FFecha := ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) );
                    FLogData := FLogData + Format( '%s Borrada', [ ZetaServerTools.CampoAsVar( FieldByName( 'CH_H_AJUS' ) ) ] );
               end;
          end;
     end;
end;

{$ifndef VALIDA_ASIST_BD}
procedure TdmServerAsistencia.SetTarjetaInfo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
begin
     case UpdateKind of
          ukModify: FLogOperation := 'Tarjeta Modificada';
          ukDelete: FLogOperation := 'Tarjeta Borrada';
          {$ifdef INTERMEX}
          ukInsert: FLogOperation := 'Checada Agregada';
          {$endif}
     end;
end;
{$endif}

procedure TdmServerAsistencia.GetAutorizacionPeriodo(Sender: TObject; DataSet: TzProviderClientDataSet);
var
   dInicial, dFinal: TDate;
begin
     dInicial := EncodeDate( 2999, 12, 31 );
     dFinal := NullDateTime;
     with Dataset do
     begin
          First;
          while not Eof do
          begin
               with FieldByName( 'AU_FECHA' ) do
               begin
                    if not IsNull then
                    begin
                         if not VarIsNull( OldValue ) then
                         begin
                              dInicial := ZetaServerTools.dMin( OldValue, dInicial );
                              dFinal := ZetaServerTools.dMax( OldValue, dFinal );
                         end;
                         if not VarIsNull( NewValue ) then
                         begin
                              dInicial := ZetaServerTools.dMin( NewValue, dInicial );
                              dFinal := ZetaServerTools.dMax( NewValue, dFinal );
                         end;
                    end;
               end;
               Next;
          end;
     end;
     FTarjeta.AutorizacionBegin( dInicial, dFinal );
end;

procedure TdmServerAsistencia.SetAutorizacion( DataSet : TzProviderClientDataSet );
begin
     with DataSet do
     begin
          FTarjeta.AutorizacionSet( CampoAsVar( FieldByName( 'CB_CODIGO' ) ),
                                    CampoAsVar( FieldByName( 'AU_FECHA' ) ),
                                    eTipoChecadas( CampoAsVar( FieldByName( 'CH_TIPO' ) ) ),
                                    CampoAsVar( FieldByName( 'CH_RELOJ' ) ),
                                    CampoAsVar( FieldByName( 'HORAS' ) ),
                                    CampoAsVar( FieldByName( 'CH_HOR_DES' ) ),
                                    CampoAsVar( FieldByName( 'US_COD_OK' ) ) );
     end;
end;

procedure TdmServerAsistencia.SetAutorizacionKey( DataSet : TzProviderClientDataSet );
var
   iEmpleado: TNumEmp;
   dFecha: TDate;
   eTipo: eTipoChecadas;
begin
     with DataSet do
     begin
          iEmpleado := FieldByName( 'CB_CODIGO' ).OldValue;
          dFecha := FieldByName( 'AU_FECHA' ).OldValue;
          eTipo := eTipoChecadas( FieldByName( 'CH_TIPO' ).OldValue );
     end;
     with FTarjeta do
     begin
          AutorizacionSetKey( iEmpleado, dFecha, eTipo );
     end;
end;

procedure TdmServerAsistencia.BeforeUpdateAutorizacion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   BorrarRegistro: Boolean;
   rHoras, rHorasAprobadas: TDiasHoras;
   iAprobadasPor: integer;

   function ExisteChecada: Boolean;
   begin
        with FQryActual do
        begin
             Active := False;
             with oZetaProvider do
             begin
                  ParamAsDate( FQryActual, 'Fecha', CampoAsVar( DeltaDS.FieldByName( 'AU_FECHA' ) ) );
                  ParamAsInteger( FQryActual, 'Empleado', CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ) );
                  ParamAsChar( FQryActual, 'Hora', '----', K_ANCHO_HORA );
                  ParamAsInteger( FQryActual, 'Tipo', CampoAsVar( DeltaDS.FieldByName( 'CH_TIPO' ) ) );
             end;
             Active := True;
             Result := not (EOF);
             rHoras := FieldByName( 'HORAS' ).AsFloat;
             rHorasAprobadas := FieldByName( 'CH_HOR_DES' ).AsFloat;
             iAprobadasPor := FieldByName( 'US_COD_OK' ).AsInteger;
             Active := False;
        end;
   end;

   procedure ModificaUnaAutorizacion;
   begin
        //SetAutorizacionKey( DeltaDS );
        with DeltaDS do
        begin
             FTarjeta.AutorizacionSetKey( CampoAsVar( FieldByName( 'CB_CODIGO' ) ),
                                          CampoAsVar( FieldByName( 'AU_FECHA' ) ),
                                          eTipoChecadas( CampoAsVar( FieldByName( 'CH_TIPO' ) ) ) );
        end;
        SetAutorizacion( DeltaDS );
        FTarjeta.AutorizacionModificar;
   end;

   procedure AgregaUnaAutorizacion;
   begin
        SetAutorizacion( DeltaDS );
        FTarjeta.AutorizacionAgregar;
   end;

   procedure CancelaUnaAutorizacion;
   begin
        SetAutorizacionKey( DeltaDS );
        FTarjeta.AutorizacionBorrar;
   end;

begin
     BorrarRegistro:= FALSE;
     try
        with DeltaDS do
        begin
             if ( UpdateKind = ukModify ) then
             begin
                  if ( CambiaCampo( FieldByName( 'CH_TIPO' ) ) or CambiaCampo( FieldByName( 'CB_CODIGO' ) ) or
                       CambiaCampo( FieldByName( 'AU_FECHA' ) ) ) then
                     BorrarRegistro:= TRUE
                  else
                     ModificaUnaAutorizacion;
             end;
             if ( UpdateKind = ukInsert ) or BorrarRegistro then
             begin
                  if ( CampoAsVar( FieldByName( 'OPERACION' ) ) > 0 ) or    // Ya eligi� el usuario
                     ( ( ExisteChecada ) and ( FOperacion <> ocReportar ) ) then
                  begin
                       if FOperacion = ocIgnorar then
                          BorrarRegistro:= FALSE
                       else
                       begin
                            if FOperacion = ocSumar then
                            begin
                                 with DeltaDS do
                                 begin
                                      Edit;
                                      FieldByName( 'HORAS' ).AsFloat := CampoAsVar( FieldByName( 'HORAS' ) ) + rHoras;
                                      Post;
                                 end;
                            end;
                            ModificaUnaAutorizacion;
                       end;
                  end
                  else
                      AgregaUnaAutorizacion;
             end;
             if ( UpdateKind = ukDelete ) or BorrarRegistro then
                CancelaUnaAutorizacion;
        end;
     except
           on Error: Exception do
           begin
                if (  Pos( 'VIOLATION OF PRIMARY', UpperCase( Error.Message )) > 0 ) and ExisteChecada then
                begin
                     with DeltaDS do
                     begin
                          cdsErroresAuto.AppendRecord( [ CampoAsVar( FieldByName( 'CB_CODIGO' ) ),
                                                         CampoAsVar( FieldByName( 'AU_FECHA' ) ),
                                                         CampoAsVar( FieldByName( 'CH_TIPO' ) ),
                                                         rHoras,
                                                         rHorasAprobadas,
                                                         iAprobadasPor ] );
                     end;
                end;
                raise;
           end;
     end;
     Applied := True;
end;

procedure TdmServerAsistencia.EscribeHorarioTemporal(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   iEmpleado: TNumEmp;
   dFecha   : TDate;
   sHorario : String;
   eStatus  : eStatusAusencia;
   lCambiaHorario, lCambiaTipoDia: Boolean;
begin
     case UpdateKind of
          ukModify:
          begin
               with DeltaDS do
               begin
                    iEmpleado := CampoAsVar( FieldByName( 'CB_CODIGO' ) );
                    dFecha    := CampoAsVar( FieldByName( 'AU_FECHA' ) );
                    sHorario  := CampoAsVar( FieldByName( 'HO_CODIGO' ) );
                    eStatus   := eStatusAusencia( CampoAsVar( FieldByName( 'AU_STATUS' ) ) );
                    lCambiaHorario:= CambiaCampo( FieldByName( 'HO_CODIGO' ) );
                    lCambiaTipoDia:= CambiaCampo( FieldByName( 'AU_STATUS' ) );

                    FTarjeta.CambiarTarjeta( iEmpleado,
                                             dFecha,
                                             sHorario,
                                             eStatus,
                                             lCambiaHorario,
                                             lCambiaTipoDia );
                    with oZetaProvider do { Bit�cora }
                    begin
                         if lCambiaHorario then
                         begin
                              EscribeBitacora( tbNormal,
                                               clbCambioHorario,
                                               iEmpleado,
                                               dFecha,
                                               Format( 'Cambi� Horario de %s a %s', [ FieldByName( 'HO_CODIGO' ).OldValue, sHorario ] ),
                                               ' ' );
                         end;
                         if lCambiaTipoDia then
                         begin
                              EscribeBitacora( tbNormal,
                                               clbCambioTipoDia,
                                               iEmpleado,
                                               dFecha,
                                               Format( 'Cambi� Tipo de D�a de %s a %s', [ ObtieneElemento( lfStatusAusencia, FieldByName( 'AU_STATUS' ).OldValue ), ObtieneElemento( lfStatusAusencia, Ord( eStatus ) ) ] ),
                                               ' ' );
                         end;
                    end;
               end;
          end;
     end;
     Applied := True;
end;

{ ************ Interfases ( Paletitas ) ************** }

function TdmServerAsistencia.GetAutorizaciones(Empresa: OleVariant; Fecha: TDateTime; lSoloAltas: WordBool): OleVariant;
begin
     InitLog(Empresa,'GetAutorizaciones');
     Result:= oZetaProvider.OpenSQL( Empresa,
                                     Format( GetSQLScript( Q_LEE_AUTORIZACIONES ), [ ZetaCommonTools.DateToStrSQL( Fecha ), Nivel0( Empresa ) ] ),
                                     not lSoloAltas );
     EndLog;SetComplete;
end;

function TdmServerAsistencia.GetCalendario(Empresa: OleVariant; Empleado: Integer; FechaIni, FechaFin: TDateTime): OleVariant;
begin
     InitLog(Empresa,'GetCalendario');
     SetTablaInfo( eAsisCalendario );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '( CB_CODIGO = %d ) and ( AU_FECHA >= ''%s'' ) and ( AU_FECHA <= ''%s'' )',
                              [ Empleado,
                                DateToStrSQL( FechaIni ),
                                DateToStrSQL( FechaFin ) ] );
          Result := GetTabla( Empresa );
    end;
    EndLog;SetComplete;
end;

function TdmServerAsistencia.GetHorarioTemp(Empresa: OleVariant; Empleado: Integer; FechaIni, FechaFin: TDateTime): OleVariant;
var
   dValue: TDate;
   FLeeTarjeta, FLeeHorario: TZetaCursor;
   sHorario: String;

function GetDescripcionHorario: String;
begin
     if ZetaCommonTools.StrLleno( sHorario ) then
     begin
          with oZetaProvider do
          begin
               ParamAsChar( FLeeHorario, 'Horario', sHorario, K_ANCHO_HORARIO );
          end;
          with FLeeHorario do
          begin
               Active := True;
               if (EOF) then
                  Result := ''
               else
                   Result := FieldByName( 'HO_DESCRIP' ).AsString;
               Active := False;
          end;
     end
     else
         Result := '';
end;

begin
     InitLog(Empresa,'GetHorarioTemp');
     with cdsLista do
     begin
          InitTempDataset;
          AddIntegerField( 'CB_CODIGO' );
          AddDateField( 'AU_FECHA' );
          AddIntegerField( 'AU_STATUS' );
          AddFloatField( 'AU_HORASCK' );
          AddStringField( 'AU_HOR_MAN', K_ANCHO_BOOLEANO );
          AddStringField( 'HO_CODIGO', K_ANCHO_HORARIO );
          AddStringField( 'HO_DESCRIP', 30 );
          CreateTempDataset;
     end;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          InitRitmos;
          try
             InitTarjeta;
             with FTarjeta do
             begin
                  GetTurnoHorarioBegin( FechaIni, FechaFin );
             end;
             FLeeTarjeta := CreateQuery( GetSQLScript( Q_LEE_TARJETA ) );
             try
                ParamAsInteger( FLeeTarjeta, 'Empleado', Empleado );
                FLeeHorario := CreateQuery( GetSQLScript( Q_LEE_HORARIO ) );
                try
                   dValue := FechaIni;
                   while ( dValue <= FechaFin ) do
                   begin
                        with cdsLista do
                        begin
                             Append;
                             FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
                             FieldByName( 'AU_FECHA' ).AsDateTime := dValue;
                        end;
                        ParamAsDate( FLeeTarjeta, 'Fecha', dValue );
                        with FLeeTarjeta do
                        begin
                             Active := True;
                             if Eof then
                             begin
                                  with cdsLista do
                                  begin
                                       FieldByName( 'AU_HORASCK' ).AsFloat := 0;
                                       FieldByName( 'AU_HOR_MAN' ).AsString := K_GLOBAL_NO;
                                       with FTarjeta do
                                       begin
                                            with GetTurnoHorario( Empleado, dValue ) do
                                            begin
                                                 sHorario := Horario;
                                                 FieldByName( 'AU_STATUS' ).AsInteger := Ord( Status );
                                                 FieldByName( 'HO_CODIGO' ).AsString := sHorario;
                                            end;
                                       end;
                                  end;
                             end
                             else
                             begin
                                  sHorario := FieldByName( 'HO_CODIGO' ).AsString;
                                  cdsLista.FieldByName( 'AU_STATUS' ).AsInteger := FieldByName( 'AU_STATUS' ).AsInteger;
                                  cdsLista.FieldByName( 'AU_HORASCK' ).AsFloat := FieldByName( 'AU_HORASCK' ).AsFloat;
                                  cdsLista.FieldByName( 'AU_HOR_MAN' ).AsString := FieldByName( 'AU_HOR_MAN' ).AsString;
                                  cdsLista.FieldByName( 'HO_CODIGO' ).AsString := sHorario;
                             end;
                             Active := False;
                        end;
                        with cdsLista do
                        begin
                             FieldByName( 'HO_DESCRIP' ).AsString := GetDescripcionHorario;
                             Post;
                        end;
                        dValue := dValue + 1;
                   end;
                finally
                       FreeAndNil( FLeeHorario );
                end;
             finally
                    FreeAndNil( FLeeTarjeta );
             end;
             with FTarjeta do
             begin
                  GetTurnoHorarioEnd;
             end;
          finally
                 ClearTarjeta;
          end;
     end;
     with cdsLista do
     begin
          MergeChangeLog; { Elimina los ukInsert's del UpdateStatus }
          Result := Data;
          Active := False;
     end;
     EndLog;SetComplete;
end;

function TdmServerAsistencia.LeeUnaTarjeta( const Empresa: OleVariant; const iEmpleado: TNumEmp; const dFecha: TDate; var Checadas: OleVariant): OleVariant;
var
   sFecha: String;
begin
     sFecha := ZetaCommonTools.DateToStrSQL( dFecha );
     SetTablaInfo( eAsisTarjeta );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '( CB_CODIGO = %d ) and ( AU_FECHA = ''%s'' )', [ iEmpleado, sFecha ] );
          Result := GetTabla( Empresa );
          Checadas := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_CHECADAS ), [ iEmpleado, sFecha ] ), True );
     end;
end;

function TdmServerAsistencia.GetTarjeta(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; out Checadas: OleVariant): OleVariant;
begin
     InitLog(Empresa,'GetTarjeta');
     Result := LeeUnaTarjeta( Empresa, Empleado, Fecha, Checadas );
     EndLog;SetComplete;
end;

function TdmServerAsistencia.GrabaAutorizaciones(Empresa, oDelta: OleVariant; out ErrorCount: Integer; out ErrorData: OleVariant; Operacion: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaAutorizaciones');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FOperacion := eOperacionConflicto( Operacion );
          InitGlobales;
          InitRitmos;
          InitTarjeta;
          try
             FQryActual := CreateQuery( GetSQLScript( Q_LEE_ACTUALES ) );
             try
                with cdsErroresAuto do
                begin
                     Active := False;
                     CreateDataSet;
                end;
                SetTablaInfo( eAutorizaciones );
                with TablaInfo do
                begin
                     OnUpdateData := GetAutorizacionPeriodo;
                     BeforeUpdateRecord := BeforeUpdateAutorizacion;
                     // V2013-4985 [SOP-4299] Correccion bit�cora de aprobaciones no esta aplicada a tress.exe                     
                     AfterUpdateRecord := AfterUpdateAutoXAprobar;                     
                     {
                     BeforeUpdateRecord := EscribeAutorizacion;
                     OnUpdateError := ErrorAutorizacion;
                     }
                end;
                { AutorizacionBegin se hace en GetAutorizacionPeriodo }
                Result := GrabaTablaGrid( Empresa, oDelta, ErrorCount );
                with FTarjeta do
                begin
                     AutorizacionEnd;
                end;
                with cdsErroresAuto do
                begin
                     ErrorData := Data;
                     Active := False;
                end;
             finally
                    FreeAndNil( FQryActual );
             end;
          finally
                 ClearTarjeta;
          end;
     end;
     EndLog;SetComplete;
end;

function TdmServerAsistencia.GrabaCalendario(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaCalendario');
     InitTarjetaInfo;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( eAsisCalendario );
          with TablaInfo do
          begin
               BeforeUpdateRecord := {$ifdef VALIDA_ASIST_BD}nil{$else}GetTarjetaInfo{$endif};
               AfterUpdateRecord := {$ifdef VALIDA_ASIST_BD}GetTarjetaInfo;{$else}SetTarjetaInfo{$endif};
          end;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          CalcularTarjeta;
     end;
     EndLog;SetComplete;
end;

function TdmServerAsistencia.GrabaHorarioTemp(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaHorarioTemp');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          InitTarjeta;
          try
             with FTarjeta do
             begin
                  CambiarTarjetaBegin;
                  try
                     SetTablaInfo( eHorarioTemp );
                     with TablaInfo do
                     begin
                          BeforeUpdateRecord := EscribeHorarioTemporal;
                     end;
                     Result := GrabaTablaGrid( Empresa, oDelta, ErrorCount );
                  finally
                         CambiarTarjetaEnd;
                  end;
             end;
          finally
                 ClearTarjeta;
          end;
     end;
     EndLog;SetComplete;
end;

function TdmServerAsistencia.GrabaTarjeta(Empresa, oDelta, oChecadas: OleVariant; var ErrorCount,  ErrorChCount: Integer; out Valor, oNewTarjeta, oNewChecadas: OleVariant): OleVariant;
var
   iTarjeta, iChecadas: Integer;
begin
     InitLog(Empresa,'GrabaTarjeta');
     iTarjeta := ErrorCount;
     iChecadas := ErrorChCount;
     InitTarjetaInfo;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if ( iTarjeta = 0 ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( eAsisTarjeta );
               with TablaInfo do
               begin
                    BeforeUpdateRecord := {$ifdef VALIDA_ASIST_BD}nil{$else}GetTarjetaInfo{$endif};
                    AfterUpdateRecord := {$ifdef VALIDA_ASIST_BD}GetTarjetaInfo;{$else}SetTarjetaInfo{$endif};
               end;
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          end;
          if ( ( iTarjeta = 0 ) or ( ErrorCount = 0 ) ) and ( iChecadas > 0 ) then
          begin
               SetTablaInfo( eAsisChecadas );
               with TablaInfo do
               begin
                    BeforeUpdateRecord := {$ifdef VALIDA_ASIST_BD}nil{$else}GetChecadaInfo{$endif};
                    AfterUpdateRecord := {$ifdef VALIDA_ASIST_BD}GetChecadaInfo{$else}nil{$endif};
               end;
               Valor := GrabaTabla( Empresa, oChecadas, ErrorChCount );
          end;
          if CalcularTarjeta then
          begin
               { Obtiene nuevos Datos de Tarjeta }
               oNewTarjeta := LeeUnaTarjeta( Empresa, FEmpleado, FFecha, oNewChecadas );
          end;
     end;
     EndLog;SetComplete;
end;

{ ************** Borrar Poll *************** }

function TdmServerAsistencia.BorrarPoll(Usuario: Integer; Parametros: OleVariant): OleVariant;
type
    eTipoBorraPoll = ( tbpTodo, tbpPorFechas );
var
   sScript: String;
begin
     InitLog(NUll,'BorrarPoll');
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               case eTipoBorraPoll( ParamByName( 'Operacion' ).AsInteger ) of
                    tbpTodo: sScript := GetSQLScript( Q_POLL_BORRA_TODO );
                    tbpPorFechas: sScript := Format( GetSQLScript( Q_POLL_BORRA_RANGO ), [ DateToStrSQL( ParamByName( 'FechaInicial' ).AsDate ), DateToStrSQL( ParamByName( 'FechaFinal' ).AsDate ) ] );
               end;
          end;
          Result := DZetaServerProvider.GetEmptyProcessResult( prSISTBorrarPOLL );
          EmpiezaTransaccion;
          try
             ExecSQL( Comparte, sScript );
             TerminaTransaccion( True );
             DZetaServerProvider.SetProcessOK( Result );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( False );
                     DZetaServerProvider.SetProcessError( Result );
                end;
          end;
     end;
     EndLog;SetComplete;
end;

{ ************** Borrar Tarjetas *************** }

procedure TdmServerAsistencia.BorrarTarjetasBuildDataset;
var
   dInicial, dFinal: TDate;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dInicial := ParamByName( 'FechaInicial' ).AsDate;
               dFinal := ParamByName( 'FechaFinal' ).AsDate;
          end;
          with SQLBroker do
          begin
               Init( enAusencia );
               with Agente do
               begin
                    AgregaColumna( 'AU_FECHA', True, Entidad, tgFecha, 0, 'AU_FECHA' );
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( 'HO_CODIGO', True, Entidad, tgTexto, 6, 'HO_CODIGO' );
                    AgregaColumna( 'AU_STATUS', True, Entidad, tgNumero, 0, 'AU_STATUS' );
                    AgregaColumna( 'US_CODIGO', True, Entidad, tgNumero, 0, 'US_CODIGO' );
                    AgregaFiltro( Format( '( AU_FECHA >= ''%s'' )', [ DateToStrSQL( dInicial ) ] ), True, Entidad );
                    AgregaFiltro( Format( '( AU_FECHA <= ''%s'' )', [ DateToStrSQL( dFinal ) ] ), True, Entidad );
                    AgregaOrden( 'AU_FECHA', True, Entidad );
                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAsistencia.BorrarTarjetasDataset( Dataset: TDataset ): OleVariant;
type
    eTipoOperacion = ( eBorrarToda, eBorrarEntradas, eBorrarSalidas );
    eTipoChecada =   ( eTodas, eReloj, eAutomatica, eManual);
const
     K_FILTRO_TIPO = ' and ( CH_TIPO = %d ) ';
     K_FILTRO_RELOJ = ' and ( CH_RELOJ%s ) ';
     K_FILTRO_GLOBAL = ' and ( CH_GLOBAL%s ) ';
var
   eOperacion: eTipoOperacion;
   iEmpleado: TNumEmp;
   dFecha: TDate;
   FDataset: TZetaCursor;
   lUsaTarjeta: Boolean;
   eChecada: eTipoChecada;

   function GetFiltroTipo: String;
   begin
        Result := VACIO;
        if ( eOperacion = eBorrarEntradas )  then
           Result := Format( K_FILTRO_TIPO, [ Ord( chEntrada ) ] )
        else if ( eOperacion = eBorrarSalidas ) then
             Result := Format( K_FILTRO_TIPO, [ Ord( chSalida ) ] );
   end;
   function GetFiltroReloj: String;
   begin
        Result := VACIO;
        if ( eChecada = eReloj ) then
           Result := Format( K_FILTRO_RELOJ, [ ' <> ' + ZetaCommonTools.EntreComillas(VACIO) ])
        else if ( eChecada <> eTodas)  then
             Result:= Format( K_FILTRO_RELOJ, [ ' = ' + ZetaCommonTools.EntreComillas(VACIO) ]);
   end;
   function GetFiltroGlobal: String;
   begin
        Result := VACIO;
        if ( eChecada = eAutomatica )  then
           Result := Format( K_FILTRO_GLOBAL, [ ' = ' + ZetaCommonTools.EntreComillas(ZetaCommonClasses.K_GLOBAL_SI ) ] )
        else if ( eChecada = eManual )  then
             Result := Format( K_FILTRO_GLOBAL, [ ' = ' + ZetaCommonTools.EntreComillas(ZetaCommonClasses.K_GLOBAL_NO ) ] );
   end;

begin
     with oZetaProvider do
     begin
          eOperacion := eTipoOperacion( ParamList.ParamByName( 'Operacion' ).AsInteger );
          eChecada := eTipoChecada( ParamList.ParamByName('TipoChecada').asInteger ) ;
          lUsaTarjeta := ( eOperacion in [ eBorrarEntradas, eBorrarSalidas ] ) ;
          InitGlobales;
          if OpenProcess( prSISTBorrarTarjetas, Dataset.RecordCount, FListaParametros ) then
          begin
               if lUsaTarjeta then
               begin
                    InitTarjeta;
               end;
               try
                  FDataset := CreateQuery;
                  try
                     try
                        { Antes de empezar }
                        if lUsaTarjeta then
                        begin
                             PreparaQuery( FDataset, Format( GetSQLScript( Q_BORRAR_TARJETAS_CHECADAS ), [ GetFiltroTipo + GetFiltroReloj + GetFiltroGlobal ] ) );
                             FTarjeta.CalculaTarjetaBegin;
                        end
                        else
                            PreparaQuery( FDataset, GetSQLScript( Q_BORRAR_TARJETAS_TODA ) );
                            {
                             case eOperacion of
                                 eBorrarToda:     // Borrar Toda La Tarjeta //
                                 begin
                                      PreparaQuery( FDataset, GetSQLScript( Q_BORRAR_TARJETAS_TODA ) );
                                 end;
                                 eBorrarEntradas: // Borrar Solo Las Entradas //
                                 begin
                                      PreparaQuery( FDataset, GetSQLScript( Q_BORRAR_TARJETAS_CHECADAS ) );
                                      ParamAsInteger( FDataset, 'Tipo', Ord( chEntrada ) );
                                      FTarjeta.CalculaTarjetaBegin;
                                 end;
                                 eBorrarSalidas:  // Borrar Solo Las Salidas //
                                 begin
                                      PreparaQuery( FDataset, GetSQLScript( Q_BORRAR_TARJETAS_CHECADAS ) );
                                      ParamAsInteger( FDataset, 'Tipo', Ord( chSalida ) );
                                      FTarjeta.CalculaTarjetaBegin;
                                 end;
                             end;
                            }
                        { Ejecuci�n }
                        with Dataset do
                        begin
                             while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                             begin
                                  iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                  dFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
                                  EmpiezaTransaccion;
                                  try
                                     ParamAsInteger( FDataset, 'Empleado', iEmpleado );
                                     ParamAsDate( FDataset, 'Fecha', dFecha );
                                     Ejecuta( FDataset );
                                     if lUsaTarjeta then
                                     begin
                                          FTarjeta.CalculaTarjeta( iEmpleado,
                                                                   dFecha,
                                                                   FieldByName( 'HO_CODIGO' ).AsString,
                                                                   eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger ),
                                                                   FieldByName( 'US_CODIGO' ).AsInteger );
                                     end;
                                     TerminaTransaccion( True );
                                     if ( eOperacion = eBorrarToda ) then
                                        Log.Evento( clbTarjeta, iEmpleado, FFecha,'Proceso Borrar Tarjetas Con Fecha: '+ DateToStr( dFecha ) );
                                  except
                                        on Error: Exception do
                                        begin
                                             //TerminaTransaccion( False );
                                             RollBackTransaccion;
                                             Log.Excepcion( iEmpleado, 'Error Al Borrar Tarjeta ' + ZetaCommonTools.FechaCorta( dFecha ), Error );
                                        end;
                                  end;
                                  Next;
                             end;
                        end;
                        { Al terminar }
                        if lUsaTarjeta then
                        begin
                             FTarjeta.CalculaTarjetaEnd;
                        end;
                     except
                           on Error: Exception do
                           begin
                                Log.Excepcion( 0, 'Error Al Borrar Tarjetas', Error );
                           end;
                     end;
                  finally
                         FreeAndNil( FDataset );
                  end;
               finally
                      if lUsaTarjeta then
                      begin
                           ClearTarjeta;
                      end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerAsistencia.BorrarTarjetasParametros;
const
     aOperacion: array[ 0..2 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('Toda La Tarjeta','Solo Las Entradas','Solo Las Salidas');
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Fecha Inicial: ' + FechaCorta(ParamByName('FechaInicial').AsDateTime) +
                         K_PIPE + 'Fecha Final: ' + FechaCorta(ParamByName('FechaFinal').AsDateTime) +
                         K_PIPE + 'Datos a Borrar: ' + aOperacion[ParamByName('Operacion').AsInteger];
     end;
end;

function TdmServerAsistencia.BorrarTarjetas(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'BorrarTarjetas');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             BorrarTarjetasParametros;
             BorrarTarjetasBuildDataset;
             Result := BorrarTarjetasDataset( SQLBroker.SuperReporte.DataSetReporte );
        end;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

{ ************** Corregir Fechas *************** }

procedure TdmServerAsistencia.CorregirFechasBuildDataset;
var
   dActual: TDate;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dActual := ParamByName( 'FechaActual' ).AsDate;
          end;
          with SQLBroker do
          begin
               Init( enAusencia );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( 'AU_FECHA', True, Entidad, tgFecha, 0, 'AU_FECHA' );
                    AgregaColumna( 'US_CODIGO', True, Entidad, tgNumero, 0, 'US_CODIGO' );
                    AgregaColumna( 'AU_HOR_MAN', True, Entidad, tgBooleano, 0, 'AU_HOR_MAN' );
                    AgregaColumna( 'HO_CODIGO', True, Entidad, tgTexto, 6, 'HO_CODIGO' );
                    AgregaColumna( 'AU_STATUS', True, Entidad, tgNumero, 0, 'AU_STATUS' );
                    AgregaFiltro( Format( '( AU_FECHA = ''%s'' )', [ DateToStrSQL( dActual ) ] ), True, Entidad );
                    AgregaOrden( 'CB_CODIGO', True, Entidad );
                    AgregaOrden( 'AU_FECHA', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAsistencia.CorregirFechasDataset( Dataset: TDataset ): OleVariant;
type
    eTipoCambioFecha = ( tcfTarjeta, tcfEntrada, tcfSalida );
var
   eCambio: eTipoCambioFecha;
   dFecha, dActual, dNueva: TDate;
   lRevisaHorario, lEsManual,lPuedeCambiarTarjetas : Boolean;
   FCorregir: TZetaCursor;
   iEmpleado: TNumEmp;
   eTipoDia: eStatusAusencia;
   sHorario: TCodigo;
   iUsuario: Integer;
   sTitulo,sDetalle :string;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dActual := ParamByName( 'FechaActual' ).AsDate;
               dNueva := ParamByName( 'FechaNueva' ).AsDate;
               eCambio := eTipoCambioFecha( ParamByName( 'Cambio' ).AsInteger );
               lRevisaHorario := ParamByName( 'RevisarHorario' ).AsBoolean;
               lPuedeCambiarTarjetas := ParamByName( 'PuedeCambiarTarjeta' ).AsBoolean;
          end;
          InitGlobales;
          if OpenProcess( prASISCorregirFechas, Dataset.RecordCount, FListaParametros ) then
          begin
               InitRitmos;
               try
                  InitTarjeta;
                  FTarjeta.Queries.RevisaStatusBloqueoBegin;
                  try
                     { Antes de empezar }
                     case eCambio of
                          tcfTarjeta: { Cambiar Toda La Tarjeta }
                          begin
                               FCorregir := CreateQuery( GetSQLScript( Q_CORREGIR_FECHAS ) );
                               ParamAsDate( FCorregir, 'FechaNueva', dNueva );
                               ParamAsDate( FCorregir, 'Fecha', dActual );
                               if lRevisaHorario then
                               begin
                                    with FTarjeta do
                                    begin
                                         GetTurnoHorarioBegin( dNueva, dNueva );
                                         CalculaTarjetaBegin;
                                    end;
                               end;
                          end;
                     else
                         FCorregir := nil;
                     end;
                     { Ejecuci�n }
                     with Dataset do
                     begin
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               dFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
                               sHorario := FieldByName( 'HO_CODIGO' ).AsString;
                               eTipoDia := eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger );
                               lEsManual := zStrToBool( FieldByName( 'AU_HOR_MAN' ).AsString );
                               iUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
                               if ( lPuedeCambiarTarjetas or FTarjeta.Queries.RevisaStatusBloqueo( dFecha, iEmpleado, sTitulo,sDetalle ) or FTarjeta.Queries.RevisaStatusBloqueo( dNueva, iEmpleado, sTitulo,sDetalle ) )then
                               begin
                                     EmpiezaTransaccion;
                                     try
                                        case eCambio of
                                             tcfTarjeta: { Cambiar Toda La Tarjeta }
                                             begin
                                                  ParamAsInteger( FCorregir, 'Empleado', iEmpleado );
                                                  if lRevisaHorario and not lEsManual then
                                                  begin
                                                       with FTarjeta do
                                                       begin
                                                            with GetTurnoHorario( iEmpleado, dNueva ) do
                                                            begin
                                                                 eTipoDia := Status;
                                                                 sHorario := Horario;
                                                            end;
                                                       end;
                                                  end;
                                                  ParamAsInteger( FCorregir, 'Status', Ord( eTipoDia ) );
                                                  ParamAsChar( FCorregir, 'Horario', sHorario, K_ANCHO_HORARIO );
                                                  Ejecuta( FCorregir );
                                                  if lRevisaHorario and not lEsManual then
                                                  begin
                                                       FTarjeta.CalculaTarjeta( iEmpleado, dNueva, sHorario, eTipoDia, iUsuario );
                                                  end;
                                                  Log.Evento( clbNinguno, iEmpleado, dFecha, Format( 'Tarjeta de %s Fue Cambiada A %s', [ FormatDateTime( 'dd/mmm/yy', dFecha ), FormatDateTime( 'dd/mmm/yy', dNueva ) ] ) );
                                             end;
                                        end;
                                        TerminaTransaccion( True );
                                     except
                                           on Error: Exception do
                                           begin
                                                //TerminaTransaccion( False );
                                                RollBackTransaccion;
                                                Log.Excepcion( iEmpleado, 'Error Al Corregir Fecha de Tarjeta ' + FechaCorta( dFecha ), Error );
                                           end;
                                     end;
                               end
                               else
                               begin
                                     if strLleno(sTitulo)then
                                     begin
                                          Log.Error(iEmpleado,sTitulo,sDetalle);
                                     end;
                               end;
                               Next;
                          end;
                     end;
                     case eCambio of
                          tcfTarjeta: { Cambiar Toda La Tarjeta }
                          begin
                               if lRevisaHorario then
                               begin
                                    with FTarjeta do
                                    begin
                                         CalculaTarjetaEnd;
                                         GetTurnoHorarioEnd;
                                    end;
                               end;
                               FreeAndNil( FCorregir );
                          end;
                     end;
                  except
                        on Error: Exception do
                        begin
                             Log.Excepcion( 0, 'Error Al Corregir Fecha de Tarjetas', Error );
                        end;
                  end;
               finally
                      FTarjeta.Queries.RevisaStatusBloqueoEnd;
                      ClearTarjeta;
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerAsistencia.CorregirFechaParametros;
const
     aCorregir: array[ 0..2 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('Toda La Tarjeta','Primera Entrada','Ultima Salida');
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Fecha Actual: ' + FechaCorta(ParamByName( 'FechaActual' ).AsDateTime) +
                         K_PIPE + 'Fecha Nueva: ' + FechaCorta(ParamByName( 'FechaNueva' ).AsDateTime);
          if ( ParamByName( 'RevisarHorario' ).AsBoolean ) then
              FListaParametros := FListaParametros + K_PIPE + 'Revisar Horario: ' + K_GLOBAL_SI
          else
              FListaParametros := FListaParametros + K_PIPE + 'Revisar Horario: ' + K_GLOBAL_NO;
          FListaParametros := FListaParametros + K_PIPE + 'Cambiar: ' + aCorregir[ParamByName( 'Cambio' ).AsInteger];
     end;
end;

function TdmServerAsistencia.CorregirFechas(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'CorregirFechas');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             CorregirFechasBuildDataset;
             CorregirFechaParametros;
             Result := CorregirFechasDataset( SQLBroker.SuperReporte.DataSetReporte );
        end;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

{ ************** Autorizar Horas *************** }

procedure TdmServerAsistencia.AutorizarHorasBuildDataset;
begin
     with oZetaProvider do
     begin
          with SQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( K_PRETTYNAME, True, enEmpleado, tgTexto, 50, 'PrettyName' );
                    AgregaFiltro( '( CB_ACTIVO = ''S'' )', True, Entidad );
                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAsistencia.AutorizarHorasDataset( Dataset: TDataset ): OleVariant;
const
     K_NO_AUTORIZACION = 'No Se Agreg� Autorizaci�n';
     K_MENSAJE_APROBADAS = 'Fecha: %s' + CR_LF +
                           'No Se Puede Agregar/Modificar Autorizaciones De %s Ya Aprobadas';
var
   dFecha, dInicial, dFinal: TDate;
   iEmpleado: TNumEmp;
   iUsuario: Integer;
   sHorario, sHorarioNew: TCodigo;
   eTipoDia, eTipoDiaNew: eStatusAusencia;
   eCambio: eSustitucion;
   lEsManual, lBorraAuto, lSetHorario, lCambiarHorario, lSetStatus, lCambiarStatus, lCancelarHorario ,lPuedeCambiarTarjetas: Boolean;
   lAutoExtras, lAutoDescanso, lAutoConGoce, lAutoSinGoce, lAutoConGoceE, lAutoSinGoceE, lAutoPrepFueraJor, lAutoPrepDentroJor: Boolean;
   sLista, sMotivoExtras, sMotivoDescanso, sMotivoConGoce, sMotivoSinGoce, sMotivoConGoceE, sMotivoSinGoceE, sMotivoPrepFueraJor, sMotivoPrepDentroJor,sTitulo,sDetalle: String;
   rExtras, rDescanso, rConGoce, rSinGoce, rConGoceE, rSinGoceE, rPrepFueraJor, rPrepDentroJor: Extended;
   FBorraAuto, FRevisaHorario: TZetaCursor;
   lAprobarAutorizaciones, lGlobalAutorizaciones, lAplicaCambios: boolean;

   function GetFiltroAutorizacion: string;
   const
        K_FILTRO_AUTORIZACIONES_EXISTENTES = 'and ( US_COD_OK = 0 )';
   begin
        Result := VACIO;
        if not lAplicaCambios then
           Result := K_FILTRO_AUTORIZACIONES_EXISTENTES;
   end;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dInicial := ParamByName( 'FechaInicial' ).AsDate;
               dFinal := ParamByName( 'FechaFinal' ).AsDate;

               lCambiarHorario := ParamByName( 'CambiarHorario' ).AsBoolean;
               sHorarioNew := ParamByName( 'Horario' ).AsString;

               lCambiarStatus := ParamByName( 'CambiarTipoDia' ).AsBoolean;
               eTipoDiaNew := eStatusAusencia( ParamByName( 'TipoDia' ).AsInteger );

               lAutoExtras := ParamByName( 'AutorizarExtras' ).AsBoolean;
               rExtras := ParamByName( 'Extras' ).AsFloat;
               sMotivoExtras := ParamByName( 'MotivoExtras' ).AsString;

               lAutoDescanso := ParamByName( 'AutorizarDescanso' ).AsBoolean;
               rDescanso := ParamByName( 'Descanso' ).AsFloat;
               sMotivoDescanso := ParamByName( 'MotivoDescanso' ).AsString;

               lAutoConGoce := ParamByName( 'AutorizarConGoce' ).AsBoolean;
               rConGoce := ParamByName( 'ConGoce' ).AsFloat;
               sMotivoConGoce := ParamByName( 'MotivoConGoce' ).AsString;

               lAutoSinGoce := ParamByName( 'AutorizarSinGoce' ).AsBoolean;
               rSinGoce := ParamByName( 'SinGoce' ).AsFloat;
               sMotivoSinGoce := ParamByName( 'MotivoSinGoce' ).AsString;

               lAutoConGoceE := ParamByName( 'AutorizarConGoceE' ).AsBoolean;
               rConGoceE := ParamByName( 'ConGoceE' ).AsFloat;
               sMotivoConGoceE := ParamByName( 'MotivoConGoceE' ).AsString;

               lAutoSinGoceE := ParamByName( 'AutorizarSinGoceE' ).AsBoolean;
               rSinGoceE := ParamByName( 'SinGoceE' ).AsFloat;
               sMotivoSinGoceE := ParamByName( 'MotivoSinGoceE' ).AsString;

               lAutoPrepFueraJor := ParamByName( 'AutorizarPrepFueraJor' ).AsBoolean;
               rPrepFueraJor := ParamByName('PrepFueraJor').AsFloat;
               sMotivoPrepFueraJor := ParamByName( 'MotivoPrepFueraJor' ).AsString;

               lAutoPrepDentroJor:= ParamByName( 'AutorizarPrepDentroJor' ).AsBoolean;
               rPrepDentroJor:= ParamByName('PrepDentroJor').AsFloat;
               sMotivoPrepDentroJor:= ParamByName( 'MotivoPrepDentroJor' ).AsString;

               eCambio := eSustitucion( ParamByName( 'ExisteAnterior' ).AsInteger );

               lCancelarHorario := ParamByName( 'CancelarHorarioTemporal' ).AsBoolean;

               //Derecho de Aprobar Autorizaciones
               lAprobarAutorizaciones := ParamByName( 'AprobarAutorizaciones' ).AsBoolean;

               lPuedeCambiarTarjetas := ParamByName( 'PuedeCambiarTarjeta' ).AsBoolean;

               lBorraAuto := ConfiguraBorrarAuto( sLista );
          end;
          InitGlobales;
          //Derecho de Aprobar Autorizaciones
          lGlobalAutorizaciones := oZetaProvider.GetGlobalBooleano( K_GLOBAL_APROBAR_AUTORIZACIONES );

          //Se determina si se Aplicaran los cambios con Respecto al Global Aprobar Autorizaciones
          lAplicaCambios := lAprobarAutorizaciones;
          if not lGlobalAutorizaciones then
             lAplicaCambios := TRUE;

          if OpenProcess( prASISExtrasPer, Dataset.RecordCount, FListaParametros ) then
          begin
               InitRitmos;
               try
                  InitTarjeta;
                  try
                     { Antes de empezar }
                     with oZetaCreator.Queries do
                     begin
                          GetDatosHorarioBegin;
                     end;
                     with FTarjeta do
                     begin
                          AgregaTarjetaDiariaBegin( dInicial, dFinal );
                          AgregaAutorizacionBegin;
                          Queries.RevisaStatusBloqueoBegin;
                     end;
                     if ZetaCommonTools.StrLleno( sLista ) then
                        FBorraAuto := CreateQuery( Format( GetSQLScript( Q_RECALCULAR_TARJETAS_BORRA_AUTO ), [ ZetaCommonTools.CortaUltimo( sLista ), GetFiltroAutorizacion ] ) )
                     else
                         FBorraAuto := nil;
                     try
                        FRevisaHorario := CreateQuery( GetSQLScript( Q_RECALCULAR_TARJETAS_REVISA_HORARIO ) );
                        try
                           {$ifdef ANTES}
                           ParamAsBoolean( FRevisaHorario, 'EsManual', False );
                           {$else}
                           ParamAsBoolean( FRevisaHorario, 'EsManual', TRUE );
                           {$endif}
                           { Ejecuci�n }
                           with Dataset do
                           begin
                                while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                                begin
                                     iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                     dFecha := dInicial;
                                     while ( dFecha <= dFinal ) do
                                     begin
                                          if ( lPuedeCambiarTarjetas or FTarjeta.Queries.RevisaStatusBloqueo(dFecha,iEmpleado,sTitulo,sDetalle) )then
                                          begin
                                                EmpiezaTransaccion;
                                                try
                                                   with FTarjeta do
                                                   begin
                                                        with AgregaTarjetaDiaria( iEmpleado, dFecha ) do
                                                        begin
                                                             sHorario := Horario;
                                                             eTipoDia := Status;
                                                             lEsManual := EsManual;
                                                             iUsuario := Usuario;
                                                        end;
                                                   end;
                                                   { Borrado de Autorizaciones Preexistentes }
                                                   if lBorraAuto then
                                                   begin
                                                        ParamAsInteger( FBorraAuto, 'Empleado', iEmpleado );
                                                        ParamAsDate( FBorraAuto, 'Fecha', dFecha );
                                                        Ejecuta( FBorraAuto );
                                                   end;
                                                   { Revisi�n de Autorizaciones / Cancelaci�n de Horarios Temporales}
                                                   if lEsManual and lCancelarHorario then
                                                   begin
                                                        lSetHorario := True;
                                                        lSetStatus := True;
                                                        with FTarjeta do
                                                        begin
                                                             with GetTurnoHorario( iEmpleado, dFecha ) do
                                                             begin
                                                                  sHorario := Horario;
                                                                  eTipoDia := Status;
                                                             end;
                                                        end;
                                                   end
                                                   else
                                                   begin
                                                        if ( lCambiarHorario and ( sHorario <> sHorarioNew ) ) then
                                                        begin
                                                             lSetHorario := True;
                                                             sHorario := sHorarioNew;
                                                        end
                                                        else
                                                            lSetHorario := False;
                                                        if ( lCambiarStatus and ( eTipoDia <> eTipoDiaNew ) ) then
                                                        begin
                                                             lSetStatus := True;
                                                             eTipoDia := eTipoDiaNew;
                                                        end
                                                        else
                                                            lSetStatus := False;
                                                   end;
                                                   if lSetHorario or lSetStatus then
                                                   begin
                                                        ParamAsDate( FRevisaHorario, 'FECHA', dFecha );
                                                        ParamAsInteger( FRevisaHorario, 'EMPLEADO', iEmpleado );
                                                        ParamAsChar( FRevisaHorario, 'Horario', sHorario, K_ANCHO_HORARIO );
                                                        ParamAsInteger( FRevisaHorario, 'Status', Ord( eTipoDia ) );
                                                        Ejecuta( FRevisaHorario );
                                                   end;
                                                   { Agregar Autorizaciones }
                                                   with FTarjeta do
                                                   begin
                                                        if lAutoExtras then
                                                        begin
                                                             if not AgregaAutorizacion( iEmpleado, dFecha, chExtras, sMotivoExtras, rExtras, eCambio, lAplicaCambios ) and lGlobalAutorizaciones then
                                                                Log.Advertencia( iEmpleado,
                                                                                 K_NO_AUTORIZACION,
                                                                                 Format( K_MENSAJE_APROBADAS, [ FechaCorta( dFecha ),
                                                                                                                ZetaCommonLists.ObtieneElemento( lfAutorizaChecadas, ORD( acExtras ) ) ] ) );
                                                        end;
                                                        if lAutoDescanso then
                                                        begin
                                                             if not AgregaAutorizacion( iEmpleado, dFecha, chDescanso, sMotivoDescanso, rDescanso, eCambio, lAplicaCambios ) and lGlobalAutorizaciones then
                                                                Log.Advertencia( iEmpleado,
                                                                                 K_NO_AUTORIZACION,
                                                                                 Format( K_MENSAJE_APROBADAS, [ FechaCorta( dFecha ),
                                                                                                                ZetaCommonLists.ObtieneElemento( lfAutorizaChecadas, ORD( acDescanso ) ) ] ) );
                                                        end;
                                                        if lAutoConGoce then
                                                        begin
                                                             if not AgregaAutorizacion( iEmpleado, dFecha, chConGoce, sMotivoConGoce, rConGoce, eCambio, lAplicaCambios ) and lGlobalAutorizaciones then
                                                                Log.Advertencia( iEmpleado,
                                                                                 K_NO_AUTORIZACION,
                                                                                 Format( K_MENSAJE_APROBADAS, [ FechaCorta( dFecha ),
                                                                                                                ZetaCommonLists.ObtieneElemento( lfAutorizaChecadas, ORD( acConGoce ) ) ] ) );
                                                        end;
                                                        if lAutoSinGoce then
                                                        begin
                                                             if not AgregaAutorizacion( iEmpleado, dFecha, chSinGoce, sMotivoSinGoce, rSinGoce, eCambio, lAplicaCambios ) and lGlobalAutorizaciones then
                                                                Log.Advertencia( iEmpleado,
                                                                                 K_NO_AUTORIZACION,
                                                                                 Format( K_MENSAJE_APROBADAS, [ FechaCorta( dFecha ),
                                                                                                                ZetaCommonLists.ObtieneElemento( lfAutorizaChecadas, ORD( acSinGoce ) ) ] ) );
                                                        end;
                                                        if lAutoConGoceE then
                                                        begin
                                                             if not AgregaAutorizacion( iEmpleado, dFecha, chConGoceEntrada, sMotivoConGoceE, rConGoceE, eCambio, lAplicaCambios ) and lGlobalAutorizaciones then
                                                                Log.Advertencia( iEmpleado,
                                                                                 K_NO_AUTORIZACION,
                                                                                 Format( K_MENSAJE_APROBADAS, [ FechaCorta( dFecha ),
                                                                                                                ZetaCommonLists.ObtieneElemento( lfAutorizaChecadas, ORD( acConGoceEntrada ) ) ] ) );
                                                        end;
                                                        if lAutoSinGoceE then
                                                        begin
                                                             if not AgregaAutorizacion( iEmpleado, dFecha, chSinGoceEntrada, sMotivoSinGoceE, rSinGoceE, eCambio, lAplicaCambios ) and lGlobalAutorizaciones then
                                                                Log.Advertencia( iEmpleado,
                                                                                 K_NO_AUTORIZACION,
                                                                                 Format( K_MENSAJE_APROBADAS, [ FechaCorta( dFecha ),
                                                                                                                ZetaCommonLists.ObtieneElemento( lfAutorizaChecadas, ORD( acSinGoceEntrada ) ) ] ) );
                                                        end;
                                                        if lAutoPrepFueraJor then
                                                        begin
                                                             if not AgregaAutorizacion( iEmpleado, dFecha, chPrepagFueraJornada, sMotivoPrepFueraJor, rPrepFueraJor, eCambio, lAplicaCambios ) and lGlobalAutorizaciones then
                                                                Log.Advertencia( iEmpleado,
                                                                                 K_NO_AUTORIZACION,
                                                                                 Format( K_MENSAJE_APROBADAS, [ FechaCorta( dFecha ),
                                                                                                                ZetaCommonLists.ObtieneElemento( lfAutorizaChecadas, ORD( acPrepagFueraJornada ) ) ] ) );
                                                        end;
                                                        if lAutoPrepDentroJor then
                                                        begin
                                                             if not AgregaAutorizacion( iEmpleado, dFecha, chPrepagDentroJornada, sMotivoPrepDentroJor, rPrepDentroJor, eCambio, lAplicaCambios ) and lGlobalAutorizaciones then
                                                                Log.Advertencia( iEmpleado,
                                                                                 K_NO_AUTORIZACION,
                                                                                 Format( K_MENSAJE_APROBADAS, [ FechaCorta( dFecha ),
                                                                                                                ZetaCommonLists.ObtieneElemento( lfAutorizaChecadas, ORD( acPrepagDentroJornada ) ) ] ) );
                                                        end;
                                                   end;
                                                   { Calcular Tarjeta }
                                                   FTarjeta.CalculaTarjeta( iEmpleado, dFecha, sHorario, eTipoDia, iUsuario );
                                                   TerminaTransaccion( True );
                                                except
                                                      on Error: Exception do
                                                      begin
                                                           //TerminaTransaccion( False );
                                                           RollBackTransaccion;
                                                           Log.Excepcion( iEmpleado, 'Error Al Agregar Tarjeta Autom�tica ' + FechaCorta( dFecha ), Error );
                                                      end;
                                                end;
                                          end
                                          else
                                          begin
                                               if strLleno(sTitulo)then
                                               begin
                                                    Log.Error(iEmpleado,sTitulo,sDetalle);
                                               end;
                                          end;
                                          dFecha := dFecha + 1;
                                     end;
                                     Next;
                                end;
                                {
                                Checa que el Global este Prendido, que se tenga derecho de Acceso y
                                que se haya seleccionado alguno de los checkboxes del Wizard para
                                Borrar Autorizaciones Existentes
                                }
                                if lGlobalAutorizaciones and not lAprobarAutorizaciones and lBorraAuto then
                                   Log.Advertencia( 0, 'No se Borraron Autorizaciones Aprobadas Existentes' );
                           end;
                        finally
                               FreeAndNil( FRevisaHorario );
                               FTarjeta.Queries.RevisaStatusBloqueoEnd; 
                        end;
                     finally
                            FreeAndNil( FBorraAuto );
                     end;
                     { Al terminar }
                     with FTarjeta do
                     begin
                          AgregaAutorizacionEnd;
                          AgregaTarjetaDiariaEnd;
                     end;
                  except
                        on Error: Exception do
                        begin
                             Log.Excepcion( 0, 'Error Al Agregar Tarjetas Autom�ticas', Error );
                        end;
                  end;
               finally
                      ClearTarjeta;
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerAsistencia.AutorizarHorasGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'AutorizarHorasGetLista');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             CargaParametrosAutorizar;
             AutorizarHorasBuildDataset;
             Result := SQLBroker.SuperReporte.GetReporte;
        end;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerAsistencia.CargaParametrosAutorizar;
const
     K_TEMPORALES='Horarios Temporales';
     K_EXTRAS = 'Horas Extras';
     K_DESCANSO = 'Descanso Trabajado';
     K_CON_GOCE = 'Permisos Con Goce';
     K_SIN_GOCE = 'Permisos Sin Goce';
     K_HORAS_PREP = 'Horas prepagadas';
     aSiExiste: array[ 0..2 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('Dejar Anterior','Sumar','Sustituir');
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Fecha Inicial: ' + FechaCorta(ParamByName( 'FechaInicial' ).AsDateTime) +
                         K_PIPE + 'Fecha Final: ' + FechaCorta(ParamByName( 'FechaFinal' ).AsDateTime);
         if ( ParamByName( 'CambiarHorario' ).AsBoolean ) then
              FListaParametros := FListaParametros + K_PIPE + 'Horario: ' + ParamByName( 'Horario' ).AsString;
         if ( ParamByName( 'CambiarTipoDia' ).AsBoolean ) then
              FListaParametros := FListaParametros + K_PIPE + 'Horario: ' + ParamByName( 'TipoDia' ).AsString;
         if ( ParamByName( 'AutorizarExtras' ).AsBoolean ) then
              FListaParametros := FListaParametros + K_PIPE + Format( 'Horas Extras: %2.2n', [ ParamByName( 'Extras' ).AsFloat ] ) +
                             K_PIPE + 'Motivo: ' + ParamByName( 'MotivoExtras' ).AsString;
         if ( ParamByName( 'AutorizarDescanso' ).AsBoolean ) then
              FListaParametros := FListaParametros + K_PIPE + Format( 'Descanso Trabajado: %2.2n', [ ParamByName( 'Descanso' ).AsFloat ] ) +
                             K_PIPE + 'Motivo: ' + ParamByName( 'MotivoDescanso' ).AsString;
         if ( ParamByName( 'AutorizarConGoce' ).AsBoolean ) then
              FListaParametros := FListaParametros + K_PIPE + Format( 'Permiso Con Goce: %2.2n', [ ParamByName( 'ConGoce' ).AsFloat ] ) +
                             K_PIPE + 'Motivo: ' + ParamByName( 'MotivoConGoce' ).AsString;
         if ( ParamByName( 'AutorizarSinGoce' ).AsBoolean ) then
              FListaParametros := FListaParametros + K_PIPE + Format( 'Permiso Sin Goce: %2.2n', [ ParamByName( 'SinGoce' ).AsFloat ] ) +
                             K_PIPE + 'Motivo: ' + ParamByName( 'MotivoSinGoce' ).AsString;
         if ( ParamByName( 'AutorizarConGoceE' ).AsBoolean ) then
              FListaParametros := FListaParametros + K_PIPE + Format( 'Permiso c/Goce Entrada: %2.2n', [ ParamByName( 'ConGoceE' ).AsFloat ] ) +
                             K_PIPE + 'Motivo: ' + ParamByName( 'MotivoConGoceE' ).AsString;
         if ( ParamByName( 'AutorizarSinGoceE' ).AsBoolean ) then
              FListaParametros := FListaParametros + K_PIPE + Format( 'Permiso s/Goce Entrada: %2.2n', [ ParamByName( 'SinGoceE' ).AsFloat ] ) +
                             K_PIPE + 'Motivo: ' + ParamByName( 'MotivoSinGoceE' ).AsString;

          if ( ParamByName( 'AutorizarPrepFueraJor' ).AsBoolean ) then
              FListaParametros := FListaParametros + K_PIPE + Format( 'Prepagadas fuera jornada: %2.2n', [ ParamByName( 'PrepFueraJor' ).AsFloat ] ) +
                             K_PIPE + 'Motivo: ' + ParamByName( 'MotivoPrepFueraJor' ).AsString;

           if ( ParamByName( 'AutorizarPrepDentroJor' ).AsBoolean ) then
              FListaParametros := FListaParametros + K_PIPE + Format( 'Prepagadas dentro jornada: %2.2n', [ ParamByName( 'PrepDentroJor' ).AsFloat ] ) +
                             K_PIPE + 'Motivo: ' + ParamByName( 'MotivoPrepDentroJor' ).AsString;

         FListaParametros := FListaParametros + K_PIPE + 'En Caso De Existir Autorizaci�n: ' + aSiExiste[ParamByName( 'ExisteAnterior' ).AsInteger];
         if ( ParamByName('CancelarHorarioTemporal').AsBoolean or ParamByName('CancelarExtras').AsBoolean or ParamByName('CancelarDescansos').AsBoolean or
              ParamByName('CancelarPermisosConGoce').AsBoolean or ParamByName('CancelarPermisosSinGoce').AsBoolean ) then
         begin
              FListaParametros := FListaParametros + K_PIPE + 'Cancelar: ';
              if ( ParamByName('CancelarHorarioTemporal').AsBoolean )then
                   FListaParametros := FListaParametros + K_PIPE + K_TEMPORALES;
              if ( ParamByName('CancelarExtras').AsBoolean )then
                   FListaParametros := FListaParametros + K_PIPE + K_EXTRAS;
              if ( ParamByName('CancelarDescansos').AsBoolean )then
                     FListaParametros := FListaParametros + K_PIPE + K_DESCANSO;
              if ( ParamByName('CancelarPermisosConGoce').AsBoolean )then
                     FListaParametros := FListaParametros + K_PIPE + K_CON_GOCE;
              if ( ParamByName('CancelarPermisosSinGoce').AsBoolean )then
                     FListaParametros := FListaParametros + K_PIPE + K_SIN_GOCE;
              if ( ParamByName('CancelarHorasPrepagadas').AsBoolean )then
                     FListaParametros := FListaParametros + K_PIPE + K_HORAS_PREP;
         end;
     end;
end;


function TdmServerAsistencia.AutorizarHorasLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'AutorizarHorasLista');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
     end;
     CargaParametrosAutorizar;
     cdsLista.Lista := Lista;
     Result := AutorizarHorasDataset( cdsLista );
     cdsLista.Active := False;
     EndLog;SetComplete;
end;

function TdmServerAsistencia.AutorizarHoras(Empresa,Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'AutorizarHoras');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
        end;
        AutorizarHorasBuildDataset;
        CargaParametrosAutorizar;
        Result := AutorizarHorasDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

{ ********** GetHorarioStatus *************** }

function TdmServerAsistencia.GetHorarioStatus(Empresa,Parametros: OleVariant): OleVariant;
var
   dFecha: TDate;
   sTurno: String;
begin
     InitLog(Empresa,'GetHorarioStatus');
     sTurno := Parametros[ 0 ];
     dFecha := Parametros[ 1 ];
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          InitRitmos;
          with oZetaCreator.Ritmos do
          begin
               RitmosBegin( dFecha, dFecha );
               with GetStatusHorario( sTurno, dFecha ) do
               begin
                    Result := VarArrayOf( [ Horario, Ord( Status ) ] );
               end;
               RitmosEnd;
          end;
     end;
     EndLog;SetComplete;
end;

{ Funciones de D�as de Vacaciones }

function TdmServerAsistencia.GetValorVacaciones( oDatosTurno: TEmpleadoDatosTurno; oCursor: TZetaCursor;
         const dFecha: TDate ): TPesos;
var
   eStatusDia : eStatusAusencia;
   lExisteTarjeta, lEsFestivo : Boolean;

   function AbreTarjeta: Boolean;
   { Indica si existe la tarjeta y deja el Cursor Abierto para consultar los campos del registro }
   begin
        with oCursor do
        begin
             Active := FALSE;
             oZetaProvider.ParamAsDate( oCursor, 'Fecha', dFecha );
             Active := TRUE;
             Result := ( not EOF );
        end;
   end;

begin
     lExisteTarjeta := AbreTarjeta;
     lEsFestivo := FALSE;
     with oZetaCreator.Ritmos do
     begin
          // Obtiene el Status Correspondiente ( H�bil, Sabado o Descanso )
          if lExisteTarjeta then
             eStatusDia := eStatusAusencia( oCursor.FieldByName( 'AU_STATUS' ).AsInteger )
          else
             eStatusDia := GetStatusHorario( oDatosTurno.Codigo, dFecha ).Status;
          // Si no es Dia de Descanso, revisa si es D�a Festivo ( Los Festivos cuentan como Descansos )
          if ( eStatusDia <> auDescanso ) then
          begin
               if lExisteTarjeta and ( oCursor.FieldByName( 'AU_POSICIO' ).AsInteger > 0 ) then    // Existe Tarjeta y Pren�mina Calculada - AU_POSICIO toma valor cuando se Calcula la Pren�mina
                  lEsFestivo := ( eTipoDiaAusencia( oCursor.FieldByName( 'AU_TIPODIA' ).AsInteger ) = daFestivo )
               else
                  lEsFestivo := Festivos.CheckFestivo( oDatosTurno.Codigo, dFecha );   // Si no se ha calculado la Pren�mina se debe investigar la Lista de Festivos Generales o del Turno

               if lEsFestivo then
                  eStatusDia := auDescanso;
          end;
          // Ya Determinado el Status se investiga el Valor del D�a para Vacaciones
          with oDatosTurno do
          begin
               if ( Ritmico ) and ( StatusHorario.DiaVacacion >= 0 ) then    // Tiene asignado un valor para el ritmo
               begin
                    Result := StatusHorario.DiaVacacion;
                    if ( eStatusDia = auDescanso ) and lEsFestivo then                     //Si es ritmico y es festivo se regresa cero, no hay valor de descanso equivalente en este caso
                       Result := 0;
               end
               else
               begin
                    case eStatusDia of
                         auHabil : Result := VacacionHabiles;
                         auSabado : Result := VacacionSabados;
                         auDescanso : Result := VacacionDescansos;
                    else
                         Result := 0;
                    end;
               end;
          end;
     end;
end;

function TdmServerAsistencia.GetVacaDiasGozar(Empresa: OleVariant; Empleado: Integer; FechaIni, FechaFin: TDateTime): Double;
begin
     InitLog(Empresa,'GetVacaDiasGozar');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     Result := ReadVacaDiasGozados( Empleado, FechaIni, FechaFin );
     EndLog;SetComplete;
end;

function TdmServerAsistencia.ReadVacaDiasGozados( const iEmpleado: Integer; const dFechaIni, dFechaFin: TDate ): TPesos;
var
   FQryTarjeta : TZetaCursor;
   oDatosTurno : TEmpleadoDatosTurno;
   dFecha : TDate;
   eValorDias : eValorDiasVacaciones;
begin
     eValorDias := eValorDiasVacaciones( oZetaProvider.GetGlobalInteger( K_GLOBAL_DIAS_VACACIONES ) );
     if ( eValorDias = dvNoUsar ) then             // Se valida el global, pero es preferible que esto lo haga el cliente ( ya tiene el Global )
     begin
          Result := ( dFechaFin - dFechaIni );
     end
     else
     begin
          Result := 0;
          FQryTarjeta:= oZetaProvider.CreateQuery( Format( GetSQLScript( Q_LEE_TARJETA_VACA ), [ iEmpleado ] ) );
          try
             InitRitmos;
             with oZetaCreator.Ritmos do
             begin
                  RitmosBegin( dFechaIni, dFechaFin );
                  try
                     dFecha := dFechaIni;
                     while ( dFecha < dFechaFin ) do
                     begin
                          oDatosTurno := GetEmpleadoDatosTurno( iEmpleado, dFecha );
                          Result := Result + GetValorVacaciones( oDatosTurno, FQryTarjeta, dFecha );
                          dFecha := ( dFecha + 1 );
                     end;
                     case eValorDias of
                          dvRedondearArriba : Result := Round( Result );
                          dvRedondearAbajo  : Result := Trunc( Result );
                          // el "Else" no se necesita por que No se hace nada, se reqresa lo investigado
                     end;
                  finally
                         RitmosEnd;
                  end;
             end;
          finally
                 FreeAndNil( FQryTarjeta );
          end;
     end;
end;

function TdmServerAsistencia.GetVacaFechaRegreso(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime;
         DiasGozo: Double; out DiasRango: Double): TDateTime;
begin
     InitLog(Empresa,'GetVacaFechaRegreso');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     Result := ReadVacaFechaRegreso( Empleado, FechaIni, DiasGozo, DiasRango );
     EndLog;SetComplete;
end;

function TdmServerAsistencia.ReadVacaFechaRegreso( const iEmpleado: Integer; const dFechaIni: TDate;
         const rGozo: TPesos; out rRango: Double ): TDate;
var
   FQryTarjeta : TZetaCursor;
   dFecha, dFechaRitmo : TDate;
   rSumaDias : TPesos;
   oDatosTurno : TEmpleadoDatosTurno;
   eValorDias : eValorDiasVacaciones;

   function GetFinalRitmo: TDate;
   const
        K_FACTOR_X_SEMANA = 2;
   var
      iDias: Integer;
   begin
        { Algoritmo para obtener una rango de preparaci�n de Ritmos
          suficiente para la mayoria de los casos }
        iDias := Trunc( rGozo ) + 1;
        Result := dFechaIni + iDias + ( ( ( iDias div ZetaCommonClasses.K_DIAS_SEMANA ) + 1 ) * K_FACTOR_X_SEMANA );    // Suma 2 d�as por cada 7 dias contenidos en iDias
   end;

begin
     eValorDias := eValorDiasVacaciones( oZetaProvider.GetGlobalInteger( K_GLOBAL_DIAS_VACACIONES ) );
     if ( eValorDias = dvNoUsar ) then             // Se valida el global, pero es preferible que esto lo haga el cliente ( ya tiene el Global )
     begin
          Result := ( dFechaIni + rGozo );
          rRango := rGozo;
     end
     else
     begin
          dFecha := dFechaIni;
          dFechaRitmo := GetFinalRitmo;
          rSumaDias := 0;
          FQryTarjeta:= oZetaProvider.CreateQuery( Format( GetSQLScript( Q_LEE_TARJETA_VACA ), [ iEmpleado ] ) );
          try
             InitRitmos;
             with oZetaCreator.Ritmos do
             begin
                  RitmosBegin( dFecha, dFechaRitmo );
                  try
                     while ( rSumaDias <= rGozo ) do
                     begin
                          oDatosTurno := GetEmpleadoDatosTurno( iEmpleado, dFecha );
                          if ( dFecha > dFechaRitmo ) then     // Si el rango del Ritmo sugerido no es suficiente se deber� volver a preparar dia con dia
                          begin
                               RitmosEnd;                      // Se desprepara el original o bien el Anterior
                               RitmosBegin( dFecha, dFecha );  // Se prepara para el siguiente dia
                               dFechaRitmo := dFecha;          // Para volver a checar si continua dentro del ciclo
                          end;
                          rSumaDias := rSumaDias + GetValorVacaciones( oDatosTurno, FQryTarjeta, dFecha );
                          if ( rSumaDias <= rGozo ) then
                          begin
                               dFecha := ( dFecha + 1 );
                               rRango := rSumaDias;
                          end;
                     end;
                  finally
                         RitmosEnd;
                  end;
             end;
          finally
                 FreeAndNil( FQryTarjeta );
          end;
          Result := dFecha;
     end;
end;

function TdmServerAsistencia.GetAutoXAprobar(Empresa: OleVariant; Fecha: TDateTime; Todos: WordBool): OleVariant;
var
   sFecha, sFechaCast, sNivel0: String;
begin
     InitLog(Empresa,'GetAutoXAprobar');
     sFecha := ZetaCommonTools.DateToStrSQLC( Fecha );

     {$ifdef INTERBASE}
     sFechaCast := GetCastFormula( sFecha, tsqlFecha );
     {$else}
     sFechaCast := Format( 'cast( ( %s ) as Datetime ) ', [ sFecha ] );
     {$endif}

     sNivel0 := GetFiltroNivel0( Empresa, 'COLABORA' );
     if strLleno( sNivel0 ) then
        sNivel0 := ' where ' + sNivel0;

     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if ( Todos ) then
             Result := OpenSQL( Empresa, Format( GetSQLScript( Q_LEE_AUTO_X_APROBAR_TODOS ), [ sFecha, sFechaCast, sNivel0 ] ), True )
          else
              Result := OpenSQL( Empresa, Format( GetSQLScript( Q_LEE_AUTO_X_APROBAR_SUPER ), [ sFecha, UsuarioActivo, sFechaCast, sNivel0 ] ), True );
     end;
     EndLog;SetComplete;
end;

function TdmServerAsistencia.GrabaAutoXAprobar(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaAutoXAprobar');
     SetTablaInfo( eAsisChecadas );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          InitTarjeta;
          try
             FTarjeta.CalculaUnaTarjetaBegin;
             try
                TablaInfo.AfterUpdateRecord := AfterUpdateAutoXAprobar;
                Result := GrabaTablaGrid( Empresa, oDelta, ErrorCount );
             finally
                    FTarjeta.CalculaUnaTarjetaEnd;
             end;
          finally
                 ClearTarjeta;
          end;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerAsistencia.AfterUpdateAutoXAprobar(Sender: TObject; SourceDS: TDataSet;
          DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   chTipo : integer;
   HorasAutorizadas :  Currency;

begin
     if UpdateKind <> ukDelete then
     begin
         with DeltaDS do
         begin
              FTarjeta.CalculaUnaTarjeta( ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' ) ),
                                          ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) ) );
              chTipo := ZetaServerTools.CampoAsVar( FieldByName( 'CH_TIPO' ));

              if ( FindField('HORAS') <> NIL ) then
                 HorasAutorizadas := CampoAsVar (FieldByName('HORAS'))
              else
                  HorasAutorizadas :=  CampoAsVar (FieldByName('CH_HORAS'));

              case  eAutorizaChecadas(  chTipo - K_OFFSET_AUTORIZACION   )  of
                   acExtras :
                            BitacoraAprobacion( clbApruebHorasExtras, ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' )), FieldByName( 'US_COD_OK' ),
                                              (ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) )), '%s %s Hr. Extras',
                                              HorasAutorizadas,
                                              FieldByName( 'CH_HOR_DES' ), UpdateKind);
                   acConGoce :
                             BitacoraAprobacion( clbApruebPermisoCG, ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' )), FieldByName( 'US_COD_OK' ),
                                              (ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) )), '%s %s Hr. de Permiso CG',
                                              HorasAutorizadas,
                                              FieldByName( 'CH_HOR_DES' ), UpdateKind);
                   acSinGoce :
                             BitacoraAprobacion( clbApruebPermisoSG, ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' )), FieldByName( 'US_COD_OK' ),
                                              (ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) )), '%s %s Hr. de Permiso SG',
                                              HorasAutorizadas,
                                              FieldByName( 'CH_HOR_DES' ), UpdateKind);
                   acDescanso :
                             BitacoraAprobacion( clbApruebDescanso, ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' )), FieldByName( 'US_COD_OK' ),
                                              (ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) )), '%s %s Hr. de Descanso Trab.',
                                              HorasAutorizadas,
                                              FieldByName( 'CH_HOR_DES' ), UpdateKind);
                   acConGoceEntrada :
                             BitacoraAprobacion( clbApruebPermisoCGEnt, ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' )), FieldByName( 'US_COD_OK' ),
                                              (ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) )), '%s %s Hr. de Permiso CG Ent.',
                                              HorasAutorizadas,
                                              FieldByName( 'CH_HOR_DES' ), UpdateKind);
                   acSinGoceEntrada :
                             BitacoraAprobacion( clbApruebPermisoSGEnt, ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' )), FieldByName( 'US_COD_OK' ),
                                              (ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) )), '%s %s Hr. de Permiso SG Ent.',
                                              HorasAutorizadas,
                                              FieldByName( 'CH_HOR_DES' ), UpdateKind);
                   acPrepagFueraJornada :
                             BitacoraAprobacion( clbApruebPrepFueraJor, ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' )), FieldByName( 'US_COD_OK' ),
                                              (ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) )), '%s %s Hr. Prep. fuera de jornada',
                                              HorasAutorizadas,
                                              FieldByName( 'CH_HOR_DES' ), UpdateKind);
                   acPrepagDentroJornada :
                             BitacoraAprobacion( clbApruebPrepDentroJor, ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' )), FieldByName( 'US_COD_OK' ),
                                              (ZetaServerTools.CampoAsVar( FieldByName( 'AU_FECHA' ) )), '%s %s Hr. Prep. dentro de jornada',
                                              HorasAutorizadas,
                                              FieldByName( 'CH_HOR_DES' ), UpdateKind);
              end;
         end;
     end;
end;

procedure TdmServerAsistencia.BitacoraAprobacion( const eClase: eClaseBitacora; const iEmpleado: Integer;
          const fUsuario: TField; const dFecha: TDate; const sMensaje: String; const HorasAutorizadas: Currency; const fHorasDes: TField; const UpdateKind: TUpdateKind);
var
   sAccion, sHoras: String;
begin
     sAccion := '';
     sHoras := '';
     if UpdateKind = ukInsert then
     begin
          if CampoAsVar(fUsuario) <> 0 then
          begin
               sAccion := 'Aprob� ' + FormatFloat('###,##0.00', CampoAsVar (fHorasDes)) + ' de';
               sHoras := FormatFloat('###,##0.00', HorasAutorizadas);
          end;
     end
     else if (CambiaCampo(fHorasDes)) or (CambiaCampo(fUsuario))  then
     begin
          if CampoAsVar(fUsuario) <> 0 then
          begin 
               sAccion := 'Aprob� ' + FormatFloat('###,##0.00', CampoAsVar (fHorasDes) ) + ' de';
               sHoras := FormatFloat('###,##0.00', HorasAutorizadas);
          end
          else
          begin
               sAccion := 'Desaprob� Aut. de';
               sHoras := FormatFloat('###,##0.00', HorasAutorizadas);
          end;
     end;
     if sAccion <> '' then
        IncluirBitacora( eClase, iEmpleado, dFecha, Format( sMensaje, [ sAccion, sHoras ] ), '' );
end;

function TdmServerAsistencia.GetClasificacionTemp(Empresa: OleVariant; Fecha: TDateTime;
         const Empleados: WideString; SoloAltas: WordBool): OleVariant;
var
   sEmpleados: String;
begin
     InitLog(Empresa,'GetClasificacionTemp');
     sEmpleados := Empleados;
     if strLleno( sEmpleados ) then
        sEmpleados := Format( '(%s) and ', [ sEmpleados ] );
     Result:= oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_LEE_CLASIFI_TEMP ),
                                     [ sEmpleados, DateToStrSQLC( Fecha ),
                                       Nivel0( Empresa ) ] ),
                                     ( not SoloAltas ) );
     EndLog;SetComplete;
end;

function TdmServerAsistencia.GrabaClasificacionTemp(Empresa, oDelta: OleVariant;
         out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaClasificacionTemp');
     SetTablaInfo( eClasifTemp );
     // PENDIENTE - Como avisarle a la tarjeta que hubo cambio para esta fecha:
     // probablemente con stored procedure SP_CLAS_AUSENCIA
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.BeforeUpdateRecord := BeforeUpdateClasifiTemp;
          TablaInfo.AfterUpdateRecord := AfterUpdateClasifiTemp;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     EndLog;SetComplete;
end;

procedure TdmServerAsistencia.BeforeUpdateClasifiTemp(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind in [ ukModify, ukInsert ] ) then
     begin
          with DeltaDS do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CT_FECHA' ).AsDateTime := Date;
          end;
     end;
end;

procedure TdmServerAsistencia.AfterUpdateClasifiTemp(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
begin
     with DeltaDS do
     begin
          AvisarTarjetas(  CampoAsVar( FieldByName( 'CB_CODIGO' ) ) , CampoAsVar( FieldByName( 'AU_FECHA' ) ) );
     end;
end;

procedure TdmServerAsistencia.AvisarTarjetas( const iEmpleado : Integer; const dFecha : TDateTime );
var
   FTarjetas: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FTarjetas := CreateQuery( GetSQLScript( Q_AVISAR_TARJETAS ) );
          try
             ParamAsInteger( FTarjetas, 'Empleado', iEmpleado );
             ParamAsDate( FTarjetas, 'Fecha', dFecha );
             Ejecuta( FTarjetas );
          finally
                 FreeAndNil( FTarjetas );
          end;
     end;
end;

function TdmServerAsistencia.GetCalendarioEmpleado(Empresa: OleVariant; Empleado: Integer;
         FechaIni, FechaFin: TDateTime): OleVariant;
begin
     InitLog(Empresa,'GetCalendarioEmpleado');
     oZetaProvider.EmpresaActiva := Empresa;
     PreparaCalendarioEmpleado( Empleado, FechaIni, FechaFin ); { Utiliza cdsLista }
     Result := cdsLista.Data;
     cdsLista.Active := FALSE;
     EndLog;SetComplete;
end;

procedure TdmServerAsistencia.PreparaCalendarioEmpleado( const iEmpleado: Integer;
          const dInicio, dFinal: TDate );
const
     K_STATUS_NINGUNO = -1;
var
   dFecha : TDate;
   StatusEmp : eStatusEmpleado;
begin
     InitRitmos;
     with oZetaProvider do       // Obtiene tarjetas y status del empleado a esa fecha
     begin
          InitGlobales;
          cdsLista.Lista := OpenSQL( EmpresaActiva, Format( GetSQLScript( Q_LEE_TARJETAS_RANGO ), [
                                                           iEmpleado, DateToStrSQLC( dInicio ),
                                                           DateToStrSQLC( dFinal ) ] ), TRUE );
     end;
     with oZetaCreator do
     begin
          Queries.GetStatusActEmpleadoBegin;
          Ritmos.RitmosBegin( dInicio, dFinal );
          try
             dFecha := dInicio;
             while ( dFecha <= dFinal ) do
             begin
                  if not ( cdsLista.Locate( 'AU_FECHA', VarArrayOf([dFecha]), [] ) ) then   // Si No existe se investiga Status e Informaci�n del Dia
                  begin
                       StatusEmp := Queries.GetStatusActEmpleado( iEmpleado, dFecha );
                       if ( StatusEmp = steEmpleado ) then         // Si est� activo se investiga la informaci�n del d�a
                       begin
                            with Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFecha ) do
                            begin
                                 if Ritmico and ( StatusHorario.DiaVacacion >= 0 ) then
                                 begin
                                      cdsLista.AppendRecord( [ dFecha, Codigo, StatusHorario.Horario,
                                                               Ord( StatusHorario.Status ),
                                                               StatusHorario.DiaVacacion,StatusHorario.DiaVacacion,StatusHorario.DiaVacacion,   //Se repite el valor de Vacacion para ser compatible con funcionalidad en el cliente (No importar� el status, se asigna el valor indicado)
                                                               Ord( StatusEmp ) ] );
                                 end
                                 else
                                 begin
                                      cdsLista.AppendRecord( [ dFecha, Codigo, StatusHorario.Horario,
                                                               Ord( StatusHorario.Status ),
                                                               VacacionHabiles,VacacionSabados,VacacionDescansos,{OP: 11/06/08}
                                                               Ord( StatusEmp ) ] );
                                 end;
                            end;
                       end
                       else
                       begin
                            cdsLista.AppendRecord( [ dFecha, VACIO, VACIO, K_STATUS_NINGUNO, Ord( StatusEmp ) ] );
                       end;
                  end;
                  dFecha := dFecha + 1;
             end;
          finally
                 Ritmos.RitmosEnd;
                 Queries.GetStatusActEmpleadoEnd;
          end;
     end;
end;

function TdmServerAsistencia.PuedeCambiarTarjetaStatus(Empresa: OleVariant;  Fecha: TDateTime; Empleado, StatusLimite: Integer): WordBool;
begin
     cdsLista.InitTempDataset;
     with oZetaProvider do
          cdsLista.Data := OpenSQL( Empresa, Format( GetSQLScript( Q_SP_STATUS_TARJETA ),
                                                     [ Empleado, DateToStrSQLC( Fecha ) ] ), True );
     Result := ZetaCommonTools.ValidaStatusNomina( eStatusPeriodo(cdsLista.FieldByName('RESULTADO').AsInteger), eStatusPeriodo(StatusLimite) );
end;

procedure TdmServerAsistencia.CancelarExcepcionesFestivosBuildDataset(sFecha : string);
const
  K_FILTRO_ACTIVO = '( DBO.SP_STATUS_ACT ( %s, COLABORA.CB_CODIGO ) = %d )';
begin
     with oZetaProvider do
     begin
          with SQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( K_PRETTYNAME, True, Entidad, tgTexto, K_ANCHO_PRETTYNAME, 'PrettyName' );
                    AgregaFiltro( Format( K_FILTRO_ACTIVO, [ sFecha, Ord( steEmpleado ) ] ), TRUE, Entidad );
                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;

end;

function TdmServerAsistencia.CancelarExcepcionesFestivos(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'CancelarExcepcionesFestivos');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;
     end;
     InitBroker;
     try
        CargaParametrosCancelarExcepcionesFestivos;
        CancelarExcepcionesFestivosBuildDataset(DateToStrSQLC(oZetaProvider.ParamList.ParamByName( 'FechaFinal' ).AsDateTime));
        Result := CancelarExcepcionesFestivosDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerAsistencia.CancelarExcepcionesFestivosGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'CancelarExcepcionesFestivosGetLista');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        with oZetaProvider do
        begin
             CargaParametrosCancelarExcepcionesFestivos;
             CancelarExcepcionesFestivosBuildDataset(DateToStrSQLC(ParamList.ParamByName( 'FechaFinal' ).AsDateTime));
             Result := SQLBroker.SuperReporte.GetReporte;
        end;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerAsistencia.CancelarExcepcionesFestivosLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'CancelarExcepcionesFestivosLista');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;        
     end;
     CargaParametrosCancelarExcepcionesFestivos;
     cdsLista.Lista := Lista;
     Result := CancelarExcepcionesFestivosDataset( cdsLista );
     cdsLista.Active := False;
     EndLog;SetComplete;
end;

procedure TdmServerAsistencia.CargaParametrosCancelarExcepcionesFestivos;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Fecha inicial: ' + FechaCorta(ParamByName( 'FechaInicial' ).AsDateTime) +
                              K_PIPE + 'Fecha final: ' + FechaCorta(ParamByName( 'FechaFinal' ).AsDateTime)  +
                              K_PIPE + 'Asignar horario de festivo trabajado del turno: ';
          if  ParamByName( 'FestivoTrabajado' ).AsBoolean = True then
              FListaParametros := FListaParametros + 'S'
          else
              FListaParametros := FListaParametros + 'N';
     end;
end;

procedure TdmServerAsistencia.ValidaEnPrenominas(const iEmpleado:Integer;const dFecha:TDate);
var
   iYear, iTipo, iNumero : Integer;
begin
     with FQueryAusenciaValida do
     begin
          with oZetaProvider do
          begin
               Active := false;
               ParamAsInteger( FQueryAusenciaValida, 'Empleado', iEmpleado );
               ParamAsDate( FQueryAusenciaValida, 'Fecha', dFecha );
               Active := true;
               while (not EOF) do
               begin
                    iYear := FieldByName('PE_YEAR').AsInteger;
                    iTipo := FieldByName('PE_TIPO').AsInteger;
                    iNumero := FieldByName('PE_NUMERO').AsInteger;
                    if (iYear > 0) and (iNumero > 0) then
                         Log.Advertencia( iEmpleado, Format( 'Se debe recalcular la pren�mina: %s', [ ShowNomina( iYear, iTipo, iNumero ) ] ));
                    next;
               End;
               Active:= False;
          end;
     end;
end;


function TdmServerAsistencia.CancelarExcepcionesFestivosDataset( Dataset: TDataset ): OleVariant;
var
   dInicial, dFinal,dFecha: TDate;
   FUpdateAusencia,FSelectAusencia, FUpdateHorario: TZetaCursor;
   iEmpleado : Integer;
   lPuedeCambiarTarjetas, lFestivosTrabajados :Boolean;
   sTitulo,sDetalle, sHorarioOriginal: string;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dInicial := ParamByName( 'FechaInicial' ).AsDate;
               dFinal := ParamByName( 'FechaFinal' ).AsDate;
               lPuedeCambiarTarjetas := ParamByName( 'PuedeCambiarTarjetas' ).AsBoolean;
               lFestivosTrabajados := ParamByName( 'FestivoTrabajado' ).AsBoolean;
          end;

          if OpenProcess( prASISCancelarExcepcionesFestivos, Dataset.RecordCount, FListaParametros ) then
          begin
               InitRitmos;
               try
                  InitTarjeta;
                  InitGlobales;
                  try
                    { Antes de empezar }
                    with FTarjeta do
                    begin
                         AgregaTarjetaDiariaBegin( dInicial, dFinal );
                         Queries.RevisaStatusBloqueoBegin;
                    end;
                    FSelectAusencia := CreateQuery( Format ( GetSQLScript( Q_SELECT_TARJETAS_CANCELAR_EXCEP_FESTIVO ), [ DatetoStrSQLC(dInicial), DatetoStrSQLC(dFinal), Ord(esfAutomatico) ]  ) );
                    FQueryAusenciaValida := CreateQuery( Format ( GetSQLScript( Q_SELECT_TARJETAS_REGISTRAR_EXCEP_FESTIVO ), [Ord(esfAutomatico) ]  ) );
                    FUpdateAusencia := CreateQuery( Format ( GetSQLScript( Q_UPDATE_TARJETAS_CANCELAR_EXCEP_FESTIVO ), [DatetoStrSQLC(dInicial), DatetoStrSQLC(dFinal), Ord(esfAutomatico) ]  ) );
                    FUpdateHorario := CreateQuery( GetSQLScript( Q_UPDATE_HORARIO_TARJETAS_TIPO_FESTIVO ) );

                    try
                       { Ejecuci�n }
                       with Dataset do
                       begin
                            while ( not EOF ) and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                            begin
                                 iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                 FSelectAusencia.Active := false;
                                 ParamAsInteger( FSelectAusencia, 'Empleado', iEmpleado );
                                 FSelectAusencia.Active := true;
                                 while (not FSelectAusencia.EOF) do
                                 begin
                                      dFecha := FSelectAusencia.FieldByName('AU_FECHA').AsDateTime;
                                      if ( lPuedeCambiarTarjetas or FTarjeta.Queries.RevisaStatusBloqueo( dFecha , iEmpleado, sTitulo,sDetalle ) )then
                                      begin
                                           try
                                              EmpiezaTransaccion;
                                              ValidaEnPrenominas(iEmpleado,dFecha);
                                              ParamAsInteger( FUpdateAusencia, 'Empleado', iEmpleado );
                                              Ejecuta( FUpdateAusencia );

                                              {US: 13644} //Excepcion de estivo no determina el horario
                                              if lFestivosTrabajados = true then
                                              begin
                                                   sHorarioOriginal := oZetaCreator.Ritmos.GetStatusHorario (oZetaCreator.Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFecha ).Codigo, dFecha ).Horario;
                                                   ParamAsString( FUpdateHorario, 'Horario', sHorarioOriginal);
                                                   ParamAsInteger( FUpdateHorario, 'Empleado', iEmpleado );
                                                   ParamAsDate( FUpdateHorario, 'Fecha', dFecha );
                                                   ParamAsInteger( FUpdateHorario, 'Festivo', Ord(esfAutomatico) );

                                                   Ejecuta( FUpdateHorario );
                                                   FTarjeta.CalculaTarjetaBegin;

                                                   try
                                                   begin
                                                        FTarjeta.CalculaTarjeta( iEmpleado,
                                                                                   dFecha,
                                                                                   sHorarioOriginal,
                                                                                   FTarjeta.GetTurnoHorario(iEmpleado, dFecha).Status,
                                                                                   0 );
                                                   end;

                                                   finally
                                                       FTarjeta.CalculaTarjetaEnd;
                                                   end;
                                              end;

                                              TerminaTransaccion( True );
                                          except
                                                on Error: Exception do
                                                begin
                                                     RollBackTransaccion;
                                                     Log.Excepcion( iEmpleado, 'Error al cancelar excepciones de festivos', Error );
                                                end;
                                          end;
                                      end
                                      else
                                      begin
                                           if strLleno(sTitulo)then
                                           begin
                                                Log.Error(iEmpleado,sTitulo,sDetalle);
                                           end;
                                      end;
                                      FSelectAusencia.Next;
                                 end;
                                 Next;
                            end;
                       end;
                    finally
                           FreeAndNil( FUpdateAusencia );
                           FreeAndNil( FQueryAusenciaValida );
                           FreeAndNil( FSelectAusencia);
                           FreeAndNil( FUpdateHorario );
                    end;
                 except
                       on Error: Exception do
                       begin
                            Log.Excepcion( 0, 'Error al cancelar excepciones de festivos', Error );
                       end;
                 end;
               finally

               end;
          end;
          Result := CloseProcess;
     end;

end;

procedure TdmServerAsistencia.IntercambioFestivoBuildDataset(sFecha : string);
const
  K_FILTRO_ACTIVO = '( DBO.SP_STATUS_ACT ( %s, COLABORA.CB_CODIGO ) = %d )';
var
   dFechaOriginal, dFechaNueva : TDate;
begin
     with oZetaProvider do
     begin
         with ParamList do
         begin
              dFechaOriginal := ParamByName( 'FechaOriginal' ).AsDate;
              dFechaNueva := ParamByName( 'FechaNueva' ).AsDate;
         end;
          with SQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( K_PRETTYNAME, True, Entidad, tgTexto, K_ANCHO_PRETTYNAME, 'PrettyName' );
                    AgregaColumna( Format( 'FECHA_KARDEX( %s, %s )', [ EntreComillas( 'CB_TURNO' ), EntreComillas( FechaAsStr( dFechaOriginal ) ) ] ), FALSE, Entidad, tgTexto, ZetaCommonClasses.K_ANCHO_CODIGO, 'TURNO_ORIG' );
                    AgregaColumna( Format( 'FECHA_KARDEX( %s, %s )', [ EntreComillas( 'CB_TURNO' ), EntreComillas( FechaAsStr( dFechaNueva ) ) ] ), FALSE, Entidad, tgTexto, ZetaCommonClasses.K_ANCHO_CODIGO, 'TURNO_NEW' );
                    //AgregaFiltro( Format('( CB_ACTIVO = %s )', [EntreComillas(K_GLOBAL_SI)]), True, Entidad );
                    AgregaFiltro( Format( K_FILTRO_ACTIVO, [ sFecha, Ord( steEmpleado ) ] ), TRUE, Entidad );
                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAsistencia.IntercambioFestivo(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'IntercambioFestivo');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo; 
     end;
     InitBroker;
     try
        IntercambioFestivoBuildDataset(DateToStrSQLC(oZetaProvider.ParamList.ParamByName( 'FechaNueva' ).AsDateTime));
        CargaParametrosIntercambioFestivo;
        Result := IntercambioFestivoDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerAsistencia.IntercambioFestivoGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'IntercambioFestivoGetLista');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             IntercambioFestivoBuildDataset(DateToStrSQLC(oZetaProvider.ParamList.ParamByName( 'FechaNueva' ).AsDateTime));
             Result := SQLBroker.SuperReporte.GetReporte;
        end;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerAsistencia.IntercambioFestivoLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'IntercambioFestivoLista');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;
     end;
     CargaParametrosIntercambioFestivo;
     cdsLista.Lista := Lista;
     Result := IntercambioFestivoDataset( cdsLista );
     cdsLista.Active := False;
     EndLog;SetComplete;
end;

procedure TdmServerAsistencia.CargaParametrosIntercambioFestivo;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Fecha Original: ' + FechaCorta(ParamByName( 'FechaOriginal' ).AsDateTime) +
                              K_PIPE + 'Fecha Nueva: ' + FechaCorta(ParamByName( 'FechaNueva' ).AsDateTime) +
                              K_PIPE + 'Intercambiar horarios de las tarjetas: ';

          if  ParamByName( 'FestivoTrabajado' ).AsBoolean = True then
              FListaParametros := FListaParametros + 'S'
          else
              FListaParametros := FListaParametros + 'N';
     end;
end;

function TdmServerAsistencia.IntercambioFestivoDataset( Dataset: TDataset ): OleVariant;
var
   dFechaOriginal, dFechaNueva, dInicial, dFinal: TDate;
   lCrearTarjetaFechaOriginal, lCrearTarjetaFechaNueva, lProcesaFechaOriginal, lProcesaFechaNueva ,lPuedeCambiarTarjetas, lFestivosTrabajados: Boolean;
   sAdvertenciaOriginal, sAdvertenciaNueva,sTitulo,sDetalle, sNuevoHorario, sNuevoHorarioOriginal : string;
   DatosTarjetaOriginal, DatosTarjetaNueva : TDatosTarjeta;
   FQueryAusencia, FUpdateAusencia, FUpdateHorario: TZetaCursor;
   iEmpleado : Integer;

   function ValidaTarjetaFestivo( const iEmpleado: Integer; const dFecha:TDate; var sMensaje : String): Boolean;
   var
      iYear, iTipo, iNumero : Integer;
   begin
         with FQueryAusencia do
         begin
              with oZetaProvider do
              begin
                   Active := false;
                   ParamAsInteger( FQueryAusencia, 'Empleado', iEmpleado );
                   ParamAsDate( FQueryAusencia, 'Fecha', dFecha );
                   Active := true;
              end;
              if (not IsEmpty) then
              begin
                   Result := (eStatusFestivo(FieldByName('AU_STA_FES').AsInteger) = esfAutomatico ) OR (eStatusFestivo(FieldByName('AU_STA_FES').AsInteger) = esfFestivo);
                   iYear := FieldByName('PE_YEAR').AsInteger;
                   iTipo := FieldByName('PE_TIPO').AsInteger;
                   iNumero := FieldByName('PE_NUMERO').AsInteger;
                   if (iYear > 0) and (iNumero > 0) then
                        sMensaje := Format('Se debe recalcular la pren�mina: %s', [ ShowNomina(iYear, iTipo, iNumero) ])
                   else
                       sMensaje := VACIO;
              End
              Else
                  Result := true;
              Active := false;
         end;
   end;

   function EsFestivoExcepcion( const dFecha:TDate ):Boolean;
   begin
         with FQueryAusencia do
         begin
              with oZetaProvider do
              begin
                   Active := false;
                   ParamAsInteger( FQueryAusencia, 'Empleado', iEmpleado );
                   ParamAsDate( FQueryAusencia, 'Fecha', dFecha );
                   Active := true;
              end;
              Result := eStatusFestivo(FieldByName('AU_STA_FES').AsInteger) = esfFestivo;
         end;
   end;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dFechaOriginal := ParamByName( 'FechaOriginal' ).AsDate;
               dFechaNueva := ParamByName( 'FechaNueva' ).AsDate;
               lPuedeCambiarTarjetas := ParamByName( 'PuedeCambiarTarjeta' ).AsBoolean;
               lFestivosTrabajados := ParamByName( 'FestivoTrabajado' ).AsBoolean;
          end;
          dInicial := rMin( dFechaOriginal, dFechaNueva );
          dFinal := rMax( dFechaOriginal, dFechaNueva );
          if OpenProcess( prASISIntercambioFestivo, Dataset.RecordCount, FListaParametros ) then
          begin
               InitRitmos;
               try
                  InitTarjeta;
                  InitGlobales;
                  try
                     { Antes de empezar }
                     with FTarjeta do
                     begin
                          AgregaTarjetaDiariaBegin( dInicial, dFinal );
                          Queries.RevisaStatusBloqueoBegin;
                     end;
                     FQueryAusencia := CreateQuery( GetSQLScript( Q_SELECT_TARJETAS_TIPO_FESTIVO ) );
                     FUpdateAusencia := CreateQuery( GetSQLScript( Q_UPDATE_TARJETAS_TIPO_FESTIVO ) );
                     FUpdateHorario := CreateQuery( GetSQLScript( Q_UPDATE_HORARIO_TARJETAS_TIPO_FESTIVO ) );
                     try
                        { Ejecuci�n }
                        with Dataset do
                        begin
                             while ( not EOF ) and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                             begin
                                  iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;

                                  lProcesaFechaOriginal :=  ( oZetaCreator.Ritmos.Festivos.CheckFestivo( FieldByName( 'TURNO_ORIG' ).AsString, dFechaOriginal ) ) or
                                                            ( EsFestivoExcepcion( dFechaOriginal ) );
                                  if lProcesaFechaOriginal then
                                  begin
                                       lProcesaFechaNueva := ( not oZetaCreator.Ritmos.Festivos.CheckFestivo( FieldByName( 'TURNO_NEW' ).AsString, dFechaNueva ) ) and
                                                             ( not EsFestivoExcepcion( dFechaNueva ) );
                                       if lProcesaFechaNueva then
                                       begin
                                            lCrearTarjetaFechaOriginal := ( not FTarjeta.AgregaTarjetaDiariaExiste( iEmpleado, dFechaOriginal, DatosTarjetaOriginal ) );
                                            lCrearTarjetaFechaNueva := ( not FTarjeta.AgregaTarjetaDiariaExiste( iEmpleado, dFechaNueva, DatosTarjetaNueva ) );
                                            if ( lCrearTarjetaFechaOriginal or ValidaTarjetaFestivo( iEmpleado, dFechaOriginal, sAdvertenciaOriginal ) ) and
                                               ( lCrearTarjetaFechaNueva or ValidaTarjetaFestivo( iEmpleado, dFechaNueva, sAdvertenciaNueva ) ) then
                                            begin
                                                 if ( lPuedeCambiarTarjetas or FTarjeta.Queries.RevisaStatusBloqueo( dFechaOriginal, iEmpleado, sTitulo,sDetalle ) or FTarjeta.Queries.RevisaStatusBloqueo( dFechaNueva, iEmpleado, sTitulo,sDetalle ) )then
                                                 begin

                                                       EmpiezaTransaccion;
                                                       try
                                                          with FTarjeta do
                                                          begin
                                                               if lCrearTarjetaFechaOriginal then
                                                                  AgregaTarjetaDiariaEscribe( iEmpleado, dFechaOriginal, DatosTarjetaOriginal );
                                                               if lCrearTarjetaFechaNueva then
                                                                  AgregaTarjetaDiariaEscribe( iEmpleado, dFechaNueva, DatosTarjetaNueva );
                                                          end;
                                                          { Ajustar AU_STA_FES de fecha original }
                                                          ParamAsInteger( FUpdateAusencia, 'Empleado', iEmpleado );
                                                          ParamAsDate( FUpdateAusencia, 'Fecha', dFechaOriginal );
                                                          ParamAsInteger( FUpdateAusencia, 'StatusFestivo', Ord( esfFestivoTransferido ) );
                                                          Ejecuta( FUpdateAusencia );
                                                          { Ajustar AU_STA_FES de fecha nueva }
                                                          ParamAsDate( FUpdateAusencia, 'Fecha', dFechaNueva );
                                                          ParamAsInteger( FUpdateAusencia, 'StatusFestivo', Ord( esfFestivo ) );
                                                          Ejecuta( FUpdateAusencia );

                                                          //{US 13646}
                                                          if lFestivosTrabajados = true then
                                                          begin
                                                               sNuevoHorarioOriginal := oZetaCreator.Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFechaOriginal ).StatusHorario.Horario;
                                                               ParamAsString( FUpdateHorario, 'Horario', sNuevoHorarioOriginal );
                                                               ParamAsInteger( FUpdateHorario, 'Empleado', iEmpleado );
                                                               ParamAsDate( FUpdateHorario, 'Fecha', dFechaOriginal );
                                                               ParamAsInteger( FUpdateHorario, 'Festivo', Ord(esfFestivoTransferido) );

                                                               Ejecuta( FUpdateHorario );

                                                               if (oZetaCreator.Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFechaNueva ).HorarioFestivo <> VACIO) then
                                                                  sNuevoHorario := oZetaCreator.Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFechaNueva ).HorarioFestivo
                                                               else
                                                                   sNuevoHorario := oZetaCreator.Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFechaNueva ).StatusHorario.Horario;

                                                               ParamAsString( FUpdateHorario, 'Horario', sNuevoHorario );
                                                               ParamAsInteger( FUpdateHorario, 'Empleado', iEmpleado );
                                                               ParamAsDate( FUpdateHorario, 'Fecha', dFechaNueva );
                                                               ParamAsInteger( FUpdateHorario, 'Festivo', Ord(esfFestivo) );

                                                               Ejecuta( FUpdateHorario );

                                                               //Recalculando Tarjetas
                                                               FTarjeta.CalculaTarjetaBegin;
                                                               try
                                                               begin
                                                                     FTarjeta.CalculaTarjeta( iEmpleado,
                                                                                                dFechaOriginal,
                                                                                                sNuevoHorarioOriginal,                                                                                                
                                                                                                FTarjeta.GetTurnoHorario(iEmpleado, dFechaOriginal).Status, // Obtiene si el status de la fecha habil, sabado, descanso
                                                                                                0 );
                                                                                               
                                                                     FTarjeta.CalculaTarjeta( iEmpleado,
                                                                                                dFechaNueva,
                                                                                                sNuevoHorario,                                                                                                
                                                                                                FTarjeta.GetTurnoHorario(iEmpleado, dFechaNueva).Status, // Obtiene si el status de la fecha habil, sabado, descanso
                                                                                                0 );
                                                               end;

                                                               finally
                                                                   FTarjeta.CalculaTarjetaEnd;
                                                               end;
                                                          end;
                                                          

                                                          { Advertencias de periodos calculados }
                                                          if strLleno( sAdvertenciaOriginal ) then
                                                             Log.Advertencia( iEmpleado, sAdvertenciaOriginal, Format('Se cambio el tipo del d�a %s a no festivo', [FechaCorta( dFechaOriginal ) ] ) );
                                                          if strLleno( sAdvertenciaNueva ) then
                                                             Log.Advertencia( iEmpleado, sAdvertenciaNueva , Format('Se cambio el tipo del d�a %s a festivo', [FechaCorta( dFechaNueva ) ] ) );

                                                          TerminaTransaccion( True );
                                                       except
                                                             on Error: Exception do
                                                             begin
                                                                  RollBackTransaccion;
                                                                  Log.Excepcion( iEmpleado, Format( 'Error al cambiar festivo:%s a %s', [ FechaCorta( dFechaOriginal ), FechaCorta( dFechaNueva ) ] ), Error );
                                                             end;
                                                       end;
                                                 end
                                                 else
                                                 begin
                                                      if strLleno(sTitulo)then
                                                      begin
                                                           Log.Error(iEmpleado,sTitulo,sDetalle);
                                                      end;
                                                 end;
                                            end;
                                       end
                                       else
                                           Log.Error( iEmpleado, Format( 'El d�a %s ya es festivo para el emp. #%d', [ FechaCorta( dFechaNueva ), iEmpleado ] ), VACIO );
                                  end
                                  else
                                      Log.Error( iEmpleado, Format( 'El d�a %s no es festivo para el emp. #%d', [ FechaCorta( dFechaOriginal ), iEmpleado ] ), VACIO );
                                  Next;
                             end;
                        end;
                     finally
                            FreeAndNil( FUpdateAusencia );
                            FreeAndNil( FUpdateHorario );
                            FreeAndNil( FQueryAusencia );
                     end;
                     { Al terminar }
                     with FTarjeta do
                     begin
                          Queries.RevisaStatusBloqueoEnd; 
                          AgregaTarjetaDiariaEnd;
                     end;
                  except
                        on Error: Exception do
                        begin
                             Log.Excepcion( 0, Format( 'Error al cambiar festivo:%s a %s', [ FechaCorta( dFechaOriginal ), FechaCorta( dFechaNueva ) ] ), Error );
                        end;
                  end;
               finally
                      ClearTarjeta;
               end;
          end;
          Result := CloseProcess;
     end;
end;

{FWizAsistAjustIncapaCal ACL.09.Abril.09}
procedure TdmServerAsistencia.AjustarIncapaCalParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := 'Fecha Inicio: ' + FechaCorta( ParamByName( 'dInicial' ).AsDateTime ) + K_PIPE +
                              'Fecha Final: ' + FechaCorta( ParamByName( 'dFinal' ).AsDateTime ) + K_PIPE +
                              'Incluir Bajas: ' + ParamByName( 'Bajas' ).AsString;
     end;
end;

function TdmServerAsistencia.AjustarIncapaCalDataset( Dataset: TDataset): OleVariant;
var
   FUpdateIncapa: TZetaCursor;
   iEmpleado : Integer;
   sTipoInciden, sNota: String;
   dFecha: TDate;
begin
     with oZetaProvider do
     begin
          sNota := VACIO;     
          if OpenProcess( prASIAjustIncapaCal, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  FUpdateIncapa := CreateQuery( GetSQLScript( Q_UPDATE_AJUST_INCAPA_CAL ) );
                  try
                     with ParamList do
                     begin
                          ParamAsInteger( FUpdateIncapa, 'AU_TIPODIA', Ord ( ZetaCommonLists.daIncapacidad ) );
                     end;
                     with Dataset do
                     begin
                          First;
                          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                          sTipoInciden := FieldByName( 'IN_TIPO' ).AsString;
                          while not Eof and CanContinue( iEmpleado ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               dFecha := FieldByName('AU_FECHA').AsDateTime;
                               sTipoInciden := FieldByName( 'IN_TIPO' ).AsString;
                               EmpiezaTransaccion;
                               try
                                  ParamAsInteger( FUpdateIncapa, 'CB_CODIGO', iEmpleado );
                                  ParamAsDate( FUpdateIncapa, 'AU_FECHA', dFecha );
                                  ParamAsString( FUpdateIncapa, 'IN_TIPO', sTipoInciden );
                                  Ejecuta( FUpdateIncapa );
                                  if sTipoInciden <> FieldByName('AU_TIPO').AsString then
                                     sNota := 'Incidencia:' + ' -> ' + FieldByName('AU_TIPO').AsString + CR_LF;
                                  sNota := sNota + ConcatString( 'Tipo de D�a: '+ ZetaCommonLists.ObtieneElemento( lfTipoDiaAusencia, Ord( daNormal ) ), 'Incapacidad' , ' -> ' );
                                  Log.Evento( clbNinguno, iEmpleado, dFecha, 'Se ajust� Incapacidad en Tarjeta', sNota );
                                  TerminaTransaccion( True );
                               except
                                     on Error: Exception do
                                     begin
                                          RollBackTransaccion;
                                          Log.Excepcion( iEmpleado, 'Error Al Ajustar Incapacidad En Tarjeta' + '0' , Error );
                                     end;
                               end;
                               Next;    // Siguiente Empleado
                          end;
                     end;
                  finally
                     FreeAndNil( FUpdateIncapa );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Ajustar Incapacidad En Tarjeta', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerAsistencia.AjustarIncapaCal(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     AjustarIncapaCalParametros;

     cdsLista.Lista := AjustarIncapaCalBuildDataset;

     Result := AjustarIncapaCalDataset( cdsLista );
     SetComplete;
end;

function TdmServerAsistencia.AjustarIncapaCalLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     AjustarIncapaCalParametros;
     cdsLista.Lista := Lista;
     Result := AjustarIncapaCalDataset( cdsLista );
     SetComplete;
end;

function TdmServerAsistencia.AjustarIncapaCalGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'AjustarIncapaCalGetLista');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          Result := AjustarIncapaCalBuildDataset;          
     end;
     EndLog;
     SetComplete;
end;

procedure TdmServerAsistencia.SetCondiciones( var sCondicion, sFiltro: String );
var
   oEvaluador : TZetaEvaluador;
   sError : String;
begin
     InitCreator;
     with oZetaCreator do
     begin
          PreparaEntidades;
          RegistraFunciones([efComunes]);
     end;
     oEvaluador := TZetaEvaluador.Create( oZetaCreator );
     with oEvaluador do
     begin
          oEvaluador.Entidad := enEmpleado;	// LA TABLA PRINCIPAL
          // Condicion
          if ( strLleno( sCondicion ) ) and ( not FiltroSQLValido( sCondicion, sError ) ) then
             DataBaseError( sError );
          // Filtro
          if ( strLleno( sFiltro ) ) and ( not FiltroSQLValido( sFiltro, sError ) ) then
             DataBaseError( sError );
     end;
end;

function TdmServerAsistencia.AjustarIncapaCalBuildDataset: OleVariant;
var
   sSQL: String;

   procedure AgregaFiltroAusencia( sFiltro: String );
   begin
        if( Pos('WHERE', sSQL ) > 0 ) then
           sSQL:= sSQL+ ' AND' + sFiltro
        else
            sSQL:= sSQL + 'WHERE '+ sFiltro;
   end;
   
   procedure AgregaColumna( sCampo: String );
   begin
        sSQL:= ConcatString( sSQL,sCampo,' ') + ',';
   end;

   procedure AgregaOrdenAusencia( sOrden: String );
   begin
        sSQL:= sSQL + 'ORDER BY ' + sOrden;
   end;

   procedure AgregaTablaAnidada( sTabla, sTabla2, sCampo, sCampo2: String );
   begin
        sSQL:= sSQL + ' right outer join '+ sTabla + ' on ( ' +sTabla+'.'+sCampo+ ' =  '+sTabla2+'.'+sCampo2+' ) ';
   end;

   function GetFiltroAsistencia : String;
   var
      Condicion, Filtro: String;
   begin
        Result := VACIO;
        with oZetaProvider.ParamList do
        begin
             Result := ConcatFiltros( Result, ParamByName( 'RangoLista' ).AsString );
             if( StrLleno( ParamByName( 'RangoLista' ).AsString ) ) then
                 Result := StrTransAll( ParamByName( 'RangoLista' ).AsString, '@TABLA', 'COLABORA' );
             // Condicion y Filtro
             Condicion := ParamByName( 'Condicion' ).AsString;
             Filtro := ParamByName( 'Filtro' ).AsString;
             if strLleno( Condicion ) or strLleno( Filtro ) then
             begin
                  SetCondiciones( Condicion, Filtro );
                  Result := ConcatFiltros( Result, Condicion );
                  Result := ConcatFiltros( Result, Filtro );
             end;
             // Nivel 0
             Result := ConcatFiltros( Result, GetFiltroNivel0( oZetaProvider.EmpresaActiva ) );
        end;
        if StrLleno( Result ) then
           sSQL:= sSQL + 'WHERE '+ Result;
           Result := sSQL;
   end;   
            
begin
     sSQL:= 'SELECT ';
     AgregaColumna( 'AUSENCIA.CB_CODIGO');
     AgregaColumna( '' + K_PRETTYNAME + ' as PrettyName');
     AgregaColumna( 'AUSENCIA.AU_FECHA');
     AgregaColumna( 'AUSENCIA.AU_TIPO');
     AgregaColumna( 'INCIDEN.TB_ELEMENT');
     AgregaColumna( 'INCAPACI.IN_SUA_INI');
     AgregaColumna( 'INCAPACI.IN_SUA_FIN');
     AgregaColumna( 'INCAPACI.IN_NUMERO');
     AgregaColumna( 'AUSENCIA.AU_STATUS');
     AgregaColumna( 'AUSENCIA.AU_TIPODIA');
     AgregaColumna( 'INCAPACI.IN_TIPO');
     AgregaColumna( 'INCAPACI.IN_DIAS');     
     sSQL:= CortaUltimo( sSQL ) + ' FROM AUSENCIA ';
     AgregaTablaAnidada( 'INCAPACI', 'AUSENCIA', 'CB_CODIGO', 'CB_CODIGO');
     sSQL := ConcatString( sSQL, 'AUSENCIA.AU_FECHA between INCAPACI.IN_FEC_INI and ( INCAPACI.IN_FEC_FIN - 1 )' , ' AND ' );
     AgregaTablaAnidada( 'INCIDEN', 'INCAPACI', 'TB_CODIGO', 'IN_TIPO');
     AgregaTablaAnidada( 'COLABORA', 'AUSENCIA', 'CB_CODIGO', 'CB_CODIGO');
     GetFiltroAsistencia;     
     AgregaFiltroAusencia( Format( '( AU_FECHA between %0:s and %1:s )', [ DateToStrSQLC( oZetaProvider.ParamList.ParamByName( 'dInicial' ).AsDate ), DateToStrSQLC( oZetaProvider.ParamList.ParamByName( 'dFinal' ).AsDate ) ] ) );
     AgregaFiltroAusencia( Format( '( AU_TIPODIA <> %d )', [ Ord ( ZetaCommonLists.daIncapacidad ) ] ) );
     if ( oZetaProvider.ParamList.ParamByName( 'Bajas' ).AsString = 'N' ) then
        AgregaFiltroAusencia( Format('( CB_ACTIVO = %s )', [EntreComillas(K_GLOBAL_SI)]) );
     AgregaOrdenAusencia( 'AUSENCIA.CB_CODIGO ' );
     Result:= oZetaProvider.OpenSQL( oZetaProvider.EmpresaActiva, sSQL, True );
end;

procedure TdmServerAsistencia.AutorizacionPreNominaBuildDataset;
var
   sFiltroAutoriza : string;
begin
     with oZetaProvider do
     begin
          if ParamList.ParamByName('Autorizacion').AsInteger = 1 then
          begin
               sFiltroAutoriza := 'NO_SUP_OK > 0'
          end
          else
              sFiltroAutoriza := 'NO_SUP_OK = 0';
          with SQLBroker do
          begin
               Init( enNomina );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( K_PRETTYNAME, True, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'PrettyName' );
                    AgregaColumna( 'NOMINA.NO_HORAS', True, Entidad, tgFloat , 0, 'NO_HORAS' );
                    AgregaColumna( 'NOMINA.NO_DOBLES', True, Entidad, tgFloat, 0, 'NO_DOBLES' );
                    AgregaColumna( 'NOMINA.NO_TRIPLES', True, Entidad, tgFloat, 0, 'NO_TRIPLES' );
                    AgregaColumna( 'NOMINA.NO_SUP_OK', True, Entidad, tgNumero, 0, 'NO_SUP_OK' );
                    AgregaColumna( 'NOMINA.NO_HORA_CG', True, Entidad, tgFloat, 0, 'NO_HORA_CG' );
                    AgregaColumna( 'NOMINA.NO_HORA_SG', True, Entidad, tgFloat, 0, 'NO_HORA_SG' );
                    AgregaColumna( 'NOMINA.NO_DIAS_IN', True, Entidad, tgFloat, 0, 'NO_DIAS_IN' );
                    AgregaColumna( 'NOMINA.NO_DIAS_VA', True, Entidad, tgFloat, 0, 'NO_DIAS_VA' );
                    AgregaColumna( 'NOMINA.NO_DES_TRA', True, Entidad, tgFloat, 0, 'NO_DES_TRA' );
                    
                    with ParamList do
                    begin
                         AgregaFiltro( sFiltroAutoriza , True, Entidad );
                         AgregaFiltro( Format( 'PE_YEAR = %d', [ ParamByName( 'Year' ).AsInteger ] ), True, Entidad );
                         AgregaFiltro( Format( 'PE_TIPO = %d', [ ParamByName( 'Tipo' ).AsInteger ] ), True, Entidad );
                         AgregaFiltro( Format( 'PE_NUMERO = %d', [ ParamByName( 'Periodo' ).AsInteger ] ), True, Entidad );
                    end;

                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAsistencia.AutorizacionPreNomina(Empresa,Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;//Para poder mostrar correctamente la descripcion de Tipo de nommina
     end;
     AutorizacionPreNominaParametros;
     InitBroker;
     AutorizacionPreNominaBuildDataset;
     cdsLista.Lista := SQLBroker.SuperReporte.GetReporte ;
     Result := AutorizacionPreNominaDataset( cdsLista );
     SetComplete;
end;

function TdmServerAsistencia.AutorizacionPreNominaLista(Empresa, Lista,Parametros: OleVariant): OleVariant;
begin
      with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     AutorizacionPreNominaParametros;
     cdsLista.Lista := Lista;
     Result := AutorizacionPreNominaDataset( cdsLista );
     SetComplete;
end;


function TdmServerAsistencia.AutorizacionPreNominaGetLista(Empresa,Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        AutorizacionPreNominaBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

procedure TdmServerAsistencia.AutorizacionPreNominaParametros;
const
     aAccion: array[ 0..1 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Autorizar ', ' Desautorizar' );
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := 'Accion: ' + aAccion[ParamByName('Autorizacion').AsInteger] + K_PIPE +
                              'Periodo: '+ ZetacommonTools.ShowNomina( ParamByName('Anio').AsInteger, ParamByName('Tipo').AsInteger, ParamByName('Periodo').AsInteger );
     end;
end;

function TdmServerAsistencia.AutorizacionPreNominaDataset(Dataset: TDataset): OleVariant;
var
   sAutorizacion,sDescripPeriodo,sText:string;
   FUpdateAutorizacion: TZetaCursor;
   iEmpleado:Integer;
begin
      with oZetaProvider do
     begin
          InitGlobales;
          if OpenProcess( prASISAutorizacionPreNomina, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  FUpdateAutorizacion := CreateQuery( GetSQLScript( Q_UPDATE_AUTORIZACION_PRENOMINA ) );
                  try
                     with ParamList do
                     begin
                          if ParamByName('Autorizacion').AsInteger = 1 then
                          begin
                               sAutorizacion := 'Desautoriz�';
                               ParamAsInteger( FUpdateAutorizacion, 'USUARIO',0);
                               ParamAsDateTime( FUpdateAutorizacion, 'FECHA', NullDateTime );
                               ParamAsString( FUpdateAutorizacion, 'HORA', VACIO );
                          end
                          else
                          begin
                               ParamAsDateTime( FUpdateAutorizacion, 'FECHA', Date() );
                               ParamAsInteger( FUpdateAutorizacion, 'USUARIO',UsuarioActivo);
                               ParamAsString( FUpdateAutorizacion, 'HORA', FormatDateTime( 'hhmm', Now ) );
                               sAutorizacion := 'Autoriz�';
                          end;

                          sDescripPeriodo :=  IntToStr( ParamByName('Numero').AsInteger ) +' '+ ObtieneElemento( lfTipoPeriodo, ParamByName('Tipo').AsInteger ) +' del '+IntToStr( ParamByName('Anio').AsInteger);
                          sText := 'El usuario : '+IntToStr( UsuarioActivo)+' '+sAutorizacion+' la Pre-N�mina a las:'+ FormatDateTime( 'hh:nn', Now )+' del D�a:'+DateToStr(Now);
                          ParamAsInteger( FUpdateAutorizacion, 'PE_YEAR',ParamByName('Anio').AsInteger );
                          ParamAsInteger( FUpdateAutorizacion, 'PE_TIPO',ParamByName('Tipo').AsInteger );
                          ParamAsInteger( FUpdateAutorizacion, 'PE_NUMERO',ParamByName('Periodo').AsInteger );

                     end;
                     with Dataset do
                     begin
                          First;
                          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                          while not Eof and CanContinue( iEmpleado ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               EmpiezaTransaccion;
                               try
                                  with ParamList do
                                  begin
                                       ParamAsInteger( FUpdateAutorizacion, 'CB_CODIGO', iEmpleado );
                                       Ejecuta( FUpdateAutorizacion );
                                       if ZetaServerTools.RegistrarBitacora( clbAsisAutorizaPrenom, GetGlobalString( K_GLOBAL_GRABA_BITACORA ) ) then
                                          Log.Evento(clbAsisAutorizaPrenom,iEmpleado,Date(),Format('Se %s la Pre-N�mina %s ', [sAutorizacion,sDescripPeriodo] ),sText );
                                  end;
                                  TerminaTransaccion( True );
                               except
                                     on Error: Exception do
                                     begin
                                          RollBackTransaccion;
                                          Log.Excepcion( iEmpleado, 'Error en La Autorizaci�n de Pre-N�mina '  , Error );
                                     end;
                               end;
                               Next;    // Siguiente Empleado
                          end;
                     end;
                  finally
                     FreeAndNil( FUpdateAutorizacion );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error En la Autorizaci�n de la Pre-N�mina', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerAsistencia.CargaParametrosRegistrarExcepcionesFestivos;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Fecha de Tarjeta: ' + FechaCorta(ParamByName( 'Fecha' ).AsDateTime) +
                              K_PIPE + 'Asignar horario de festivo trabajado del turno: ';

          if ParamByName( 'FestivoTrabajado' ).AsBoolean = True then
              FListaParametros := FListaParametros + 'S'
          else
              FListaParametros := FListaParametros + 'N';
     end;
end;

function TdmServerAsistencia.RegistrarExcepcionesFestivo(Empresa,Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'RegistrarExcepcionesFestivos');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;
     end;
     InitBroker;
     try
        CargaParametrosRegistrarExcepcionesFestivos;
        CancelarExcepcionesFestivosBuildDataset(DateToStrSQLC(oZetaProvider.ParamList.ParamByName( 'Fecha' ).AsDateTime)); //Misma Lista que Registrar Excepciones
        Result := RegistrarExcepcionesFestivosDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     EndLog;
     SetComplete;
end;

function TdmServerAsistencia.RegistrarExcepcionesFestivosDataset( Dataset: TDataset ): OleVariant;
var
   dFecha:TDateTime;
   FUpdateAusencia, FUpdateHorario : TZetaCursor;
   DatosTarjeta:TDatosTarjeta;
   iEmpleado : Integer;
   sTitulo,sDetalle, sNuevoHorario:string;
   lCrearTarjetaFecha,lPuedeCambiarTarjetas, lFestivosTrabajados:Boolean;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dFecha := ParamByName( 'Fecha' ).AsDate;
               lPuedeCambiarTarjetas := ParamByName( 'PuedeCambiarTarjeta' ).AsBoolean;
               lFestivosTrabajados := ParamByName( 'FestivoTrabajado' ).AsBoolean;
          end;

          if OpenProcess( prASISRegistrarExcepcionesFestivos, Dataset.RecordCount, FListaParametros ) then
          begin
               InitRitmos;
               try
                  InitTarjeta;
                  InitGlobales;
                  with FTarjeta do
                  begin
                       AgregaTarjetaDiariaBegin( dFecha, dFecha );
                       Queries.RevisaStatusBloqueoBegin;
                  end;
                  { Antes de empezar }
                  FQueryAusenciaValida := CreateQuery( Format ( GetSQLScript( Q_SELECT_TARJETAS_REGISTRAR_EXCEP_FESTIVO ), [ Ord(esfFestivo) ]  ) );
                  FUpdateAusencia := CreateQuery( Format ( GetSQLScript( Q_UPDATE_TARJETAS_REGISTRAR_EXCEP_FESTIVO ), [DatetoStrSQLC(dFecha), Ord(esfFestivo) ]  ) );
                  FUpdateHorario := CreateQuery( GetSQLScript( Q_UPDATE_HORARIO_TARJETAS_TIPO_FESTIVO ) );
                  try
                     { Ejecuci�n }
                     with Dataset do
                     begin
                          while ( not EOF ) and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                                iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                if ( lPuedeCambiarTarjetas or FTarjeta.Queries.RevisaStatusBloqueo(dFecha,iEmpleado,sTitulo,sDetalle) )then
                                begin
                                     EmpiezaTransaccion;
                                     try
                                         with FTarjeta do
                                         begin
                                              lCrearTarjetaFecha := ( not AgregaTarjetaDiariaExiste( iEmpleado, dFecha, DatosTarjeta ) );
                                              if lCrearTarjetaFecha then
                                              begin                                                
                                                 AgregaTarjetaDiariaEscribe( iEmpleado, dFecha, DatosTarjeta );
                                              end
                                         end;
                                         ValidaEnPrenominas(iEmpleado,dFecha);
                                         ParamAsInteger( FUpdateAusencia, 'Empleado', iEmpleado );
                                         Ejecuta( FUpdateAusencia );

                                         {US: 13643} //Excepcion de estivo no determina el horario

                                         if oZetaCreator.Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFecha ).HorarioFestivo <> VACIO then
                                         begin
                                              sNuevoHorario := oZetaCreator.Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFecha ).HorarioFestivo;
                                         end
                                         else
                                         begin
                                              sNuevoHorario := oZetaCreator.Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFecha ).StatusHorario.Horario;
                                         end;

                                         if (lFestivosTrabajados = true) then
                                         begin
                                              // sNuevoHorario := oZetaCreator.Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFecha ).HorarioFestivo;

                                              ParamAsString( FUpdateHorario, 'Horario', sNuevoHorario );
                                              ParamAsInteger( FUpdateHorario, 'Empleado', iEmpleado );
                                              ParamAsDate( FUpdateHorario, 'Fecha', dFecha );
                                              ParamAsInteger( FUpdateHorario, 'Festivo', Ord(esfFestivo) );

                                              Ejecuta( FUpdateHorario );

                                              // RECALCULAR TARJETAS
                                              FTarjeta.CalculaUnaTarjetaBegin;
                                              try
                                              begin
                                                   FTarjeta.CalculaUnaTarjeta (iEmpleado, dFecha);
                                              end;
                                              finally
                                                  FTarjeta.CalculaUnaTarjetaEnd;
                                              end;
                                         end;
                                                 
                                         if ( ZetaServerTools.RegistrarBitacora( clbCambioFestivo, GetGlobalString( K_GLOBAL_GRABA_BITACORA ) ) ) then
                                              Log.Evento(clbCambioFestivo,iEmpleado,dFecha,'Se Registro Tarjeta como Festivo ','');
                                         TerminaTransaccion( True );
                                     except
                                           on Error: Exception do
                                           begin
                                                RollBackTransaccion;
                                                Log.Excepcion( iEmpleado, 'Error al registrar excepciones de festivos', Error );
                                           end;
                                     end;
                                end
                                else
                                begin
                                     if strLleno(sTitulo)then
                                     begin
                                          Log.Error(iEmpleado,sTitulo,sDetalle);
                                     end;
                                end;
                                Next;
                          end;
                     end;
                  finally
                         FreeAndNil( FUpdateAusencia );
                         FreeAndNil( FQueryAusenciaValida );
                         FreeAndNil( FUpdateHorario );
                         with FTarjeta do
                         begin
                              AgregaTarjetaDiariaEnd;
                              Queries.RevisaStatusBloqueoEnd;
                         end;
                         ClearTarjeta;
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error al registrar excepciones de festivos', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerAsistencia.RegistrarExcepcionesFestivoGetLista(Empresa,Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'RegistrarExcepcionesFestivosGetLista');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;
     end;
     InitBroker;
     try
        with oZetaProvider do
        begin
             CargaParametrosRegistrarExcepcionesFestivos;
             CancelarExcepcionesFestivosBuildDataset(DateToStrSQLC(ParamList.ParamByName( 'Fecha' ).AsDateTime)); //Misma Lista que Registrar Excepciones
             Result := SQLBroker.SuperReporte.GetReporte;
        end;
     finally
            ClearBroker;
     end;
     EndLog;
     SetComplete;
end;

function TdmServerAsistencia.RegistrarExcepcionesFestivoLista(Empresa,Parametros, Lista: OleVariant): OleVariant;
begin
     InitLog(Empresa,'RegistrarExcepcionesFestivosLista');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;
     end;
     CargaParametrosRegistrarExcepcionesFestivos;
     cdsLista.Lista := Lista;
     Result := RegistrarExcepcionesFestivosDataset( cdsLista );
     cdsLista.Active := False;
     EndLog;
     SetComplete;
end;

function TdmServerAsistencia.GetCosteoTransferencias(Empresa: OleVariant; Parametros: OleVariant): OleVariant;
const
     K_MANUALES = 0;
var
   sFiltro : string;
   iNivel: integer;

   procedure ConcatFiltroT( const sValor, sCampo: string );
   begin
        if StrLleno(sValor) then
           sFiltro := ConcatFiltros( sCampo + '=' +sValor, sFiltro );
   end;

begin
     InitLog(Empresa,'GetCosteoTransferencias');
     sFiltro := VACIO;

     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          iNivel := GetGlobalInteger(K_GLOBAL_NIVEL_COSTEO);
          if iNivel = 0 then
             raise Exception.Create( 'El Nivel de Organigrama para Costeo no ha sido definido.' + CR_LF +
                                     'No se podr� Agregar o Modificar las Transferencias.' );

          AsignaParamList( Parametros );
          with ParamList do
          begin
               ConcatFiltroT( Comillas( ParamByName( 'CCOrigen').AsString ) , 'AUSENCIA.CB_NIVEL' + IntToStr(iNivel) );
               ConcatFiltroT( Comillas( ParamByName( 'CCDestino').AsString ), 'TRANSFER.CC_CODIGO' );
               ConcatFiltroT( Comillas( ParamByName( 'Motivo').AsString ), 'TRANSFER.TR_MOTIVO' );
               sFiltro := ConcatFiltros( Format( 'TRANSFER.AU_FECHA BETWEEN %s and %s',
                                                 [ ZetaCommonTools.DateToStrSQLC( ParamByName('FechaIni').AsDate ),
                                                   ZetaCommonTools.DateToStrSQLC( ParamByName('FechaFin').AsDate ) ] ), sFiltro );
               if ( FindParam( 'Tipo') <> NIL ) and (ParamByName( 'Tipo').AsInteger>=0) then
                  ConcatFiltroT(ParamByName( 'Tipo').AsString, 'TRANSFER.TR_TIPO' );
               with ParamByName('Globales') do
               begin
                    if (AsInteger>=0) then
                    begin
                         if ( AsInteger = K_MANUALES ) then
                           ConcatFiltroT(Comillas(K_GLOBAL_NO), 'TRANSFER.TR_GLOBAL' )
                         else
                             ConcatFiltroT(Comillas(K_GLOBAL_SI), 'TRANSFER.TR_GLOBAL' );
                    end;
               end;

               if ParamByName( 'SoloNegativos').AsBoolean then
                  sFiltro := ConcatFiltros( '(TR_HORAS - (case TR_TIPO WHEN 0 THEN AUSENCIA.AU_HORAS ELSE AUSENCIA.AU_EXTRAS END)>0)', sFiltro );


               with ParamByName('Status') do
               begin
                    if (AsInteger>=0) then
                       ConcatFiltroT( AsString, 'TRANSFER.TR_STATUS' );
               end;

               if ParamByName( 'ModuloSupervisores').AsBoolean then //Si viene de Tress no se aplica el filtro o bien se debe de aplicar de forma distinta.
               begin
                    if ParamByName( 'MisPendientes' ).AsBoolean then
                       ConcatFiltroT( IntToStr( UsuarioActivo ), 'TRANSFER.TR_APRUEBA' )
                    else if ParamByName('Status').AsInteger >= 0 then
                        ConcatFiltroT( IntToStr( UsuarioActivo ), '(TRANSFER.TR_APRUEBA <> TRANSFER.US_CODIGO) and TRANSFER.US_CODIGO' )
                    else
                        sFiltro := ConcatFiltros( Format( '( TRANSFER.US_CODIGO = %0:s or TRANSFER.TR_APRUEBA = %0:s )', [ IntToStr( UsuarioActivo ) ] ), sFiltro );
               end;

               sFiltro := sFiltro + Nivel0( Empresa ); //El filtro de confidencialidad siempre se pone, dependiendo de la empresa seleccionada.
               if StrLleno( sFiltro ) then sFiltro := 'WHERE ' + sFiltro;
               Result:= OpenSQL( Empresa,
                                     Format( GetSQLScript( Q_LEE_COSTEO_TRANSFERS ),[ 'CB_NIVEL' + IntToStr( iNivel ), sFiltro ] ),
                                               True );
          end;
     end;
     EndLog;SetComplete;
end;



procedure TdmServerAsistencia.AfterUpdateTransferencia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   sMensaje: string;
begin
     sMensaje := 'Transferencia';
     with oZetaPRovider do
     begin
          case UpdateKind of
               ukModify :
               begin
                    CambioCatalogo( sMensaje, clbTransferencias, DeltaDS, ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ) );
               end;
               ukInsert :
               begin
                    IncluirBitacora( clbTransferencias, ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ), Now, 'Agreg� Transferencia', GetDatasetInfo( DeltaDS, TRUE )  );
               end;
               ukDelete :
               begin
                    BorraCatalogo(sMensaje, clbTransferencias, DeltaDS, ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ) );
               end;
          end;
     end;
end;

procedure TdmServerAsistencia.BeforeUpdateTransferencia(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
 var
    sMensaje: string;
    FDatosTarjeta : TDatosTarjeta;
begin
     if ( UpdateKind in [ ukModify, ukInsert ] ) then
     begin
          FFecha:= CampoAsVar( DeltaDS.FieldByName( 'AU_FECHA' ) );
          FTarjeta.AgregaTarjetaDiariaBegin( FFecha, FFecha );
          try
             if not FTarjeta.AgregaTarjetaDiariaExiste( CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' )), FFecha, FDatosTarjeta ) then
             begin
                  with oZetaProvider do
                  begin
                       if ( PuedeCambiarTarjeta( FFecha, FFecha, GetGlobalDate( K_GLOBAL_FECHA_LIMITE ), ParamList.ParamByName('PuedeCambiarTarjeta').AsBoolean, sMensaje ) ) then
                       begin
                            FTarjeta.AgregaTarjetaDiaria( CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ),
                                                        FFecha );
                       end
                       else
                           DatabaseError( sMensaje );
                  end;
             end;
          finally
                 FTarjeta.AgregaTarjetaDiariaEnd;
          end;
     end;
end;

function TdmServerAsistencia.GrabaCosteoTransferencias(Empresa, Delta, Parametros: OleVariant; out Resultdata: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaCosteoTransferencias');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitGlobales;
          InitRitmos;
          try
             InitTarjeta;

             SetTablaInfo( eTransferencias );
             TablaInfo.BeforeUpdateRecord := BeforeUpdateTransferencia;
             TablaInfo.AfterUpdateRecord := AfterUpdateTransferencia;

             Result := GrabaTabla( Empresa, Delta, ErrorCount );

             if ( ErrorCount=0 ) then
                ResultData:= GetCosteoTransferencias(Empresa, Parametros);
          finally
                 ClearTarjeta;
          end;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerAsistencia.TransferenciasCosteoParametros;
 var
    sNombreNivel: string;
begin
     with oZetaProvider, ParamList do
begin
          sNombreNivel := NombreNivel( GetGlobalInteger(K_GLOBAL_NIVEL_COSTEO) );
          FListaParametros := VACIO;
          FListaParametros := 'Fecha Inicial: ' + FechaCorta( ParamByName( 'FechaInicio' ).AsDateTime ) +
                              K_PIPE + Format( 'Duraci�n %s horas ', [ ParamByName( 'Horas').AsString ] ) +
                              K_PIPE + Format( '%s a Transferir %s',  [ sNombreNivel, ParamByName( 'CCDestino' ).AsString] ) +
                              K_PIPE + Format( '%s Original %s',  [ sNombreNivel, StrDef(ParamByName( 'CCOriginal' ).AsString, '< Todos(as) >')] ) +
                              K_PIPE + 'Motivo: ' +  ParamByName( 'Motivo' ).AsString +
                              K_PIPE + 'Observaciones: ' + ParamByName( 'Observa' ).AsString +
                              K_PIPE + 'Tipo de Transferencia: ' + ObtieneElemento( lfTipoHoraTransferenciaCosteo, ParamByName('Tipo' ).AsInteger ) +
                              K_PIPE + Format( 'Validar que no exista transferencia anterior a mismo(a) %s: %s', [sNombreNivel, zBooltostr( ParamByName( 'SoloUnaTransferencia' ).AsBoolean) ] );
     end;
end;

function TdmServerAsistencia.TransferenciasCosteo(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'TransferenciasCosteo');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             InitGlobales;
             NumNiveles;
        end;
        TransferenciasCosteoParametros;
        TransferenciasCosteoBuildDataset;
        Result := TransferenciasCosteoDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;

     EndLog;SetComplete;
end;

function TdmServerAsistencia.TransferenciasCosteoGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'TransferenciasCosteoGetLista');
     try
        InitBroker;
        with oZetaProvider do
        begin
             EmpresaActiva:= Empresa;
             AsignaParamList( Parametros );
             InitGlobales;
        end;
        TransferenciasCosteoBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerAsistencia.TransferenciasCosteoBuildDataset;
 const
     K_COLUMNA_NIVEL_COSTEO =
       '@(CASE '+
       'WHEN (select CB_NIVEL%0:d from AUSENCIA A where AU_FECHA = %1:s AND A.CB_CODIGO= COLABORA.CB_CODIGO) IS NULL  '+
	     'then dbo.SP_KARDEX_CB_NIVEL%0:d(%1:s, COLABORA.CB_CODIGO)  '+
	     'else (select CB_NIVEL%0:d from AUSENCIA A where AU_FECHA = %1:s AND A.CB_CODIGO= COLABORA.CB_CODIGO) end )';

 var
    iNivel: integer;

begin
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'PrettyName' );
               with oZetaProvider, ParamList do
               begin
                    iNivel := GetGlobalInteger(K_GLOBAL_NIVEL_COSTEO);
                    AgregaColumna( Format( K_COLUMNA_NIVEL_COSTEO,[ iNivel, Comillas(FechaAsStr( ParamByName('FechaInicio').AsDateTime )) ] ), FALSE, enEmpleado, tgTexto, 10, 'CC_CODIGO' );
                    if StrLleno(ParamByName('CCOriginal').AsString) then
                       AgregaFiltro( Format( 'CB_NIVEL%d= %s', [iNivel,Comillas(ParamByName('CCOriginal').AsString)] ), TRUE, Entidad );
               end;
               AgregaOrden( 'CB_CODIGO', TRUE, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

function TdmServerAsistencia.TransferenciasCosteoLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'TransferenciasCosteoLista');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitGlobales;
          NumNiveles; // Para que cargue las descripciones de
     end;
     TransferenciasCosteoParametros;
     cdsLista.Lista := Lista;
     Result := TransferenciasCosteoDataset( cdsLista );
     cdsLista.Close;
     EndLog;SetComplete;
end;


function TdmServerAsistencia.TransferenciasCosteoDataset(Dataset: TDataset): OleVariant;
var
   dFecha, dLimite: TDateTime;
   iEmpleado: integer;
   FInsert, FExiste :TZetaCursor;
   rHoras : TDiasHoras;
   sNombreNivel, sMotivo, sObserva, sCCDestino, sDetalle: string;
   iTipo : integer;
   lSoloUnaTransferencia, lPuedeCambiarTarjeta, lExiste : Boolean;
   FDatosTarjeta : TDatosTarjeta;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dFecha := ParamByName( 'FechaInicio' ).AsDateTime;
               rHoras := ParamByName( 'Horas' ).AsFloat;
               sMotivo := ParamByName( 'Motivo' ).AsString;
               sObserva := ParamByName( 'Observa' ).AsString;
               sCCDestino := ParamByName( 'CCDestino' ).AsString;
               iTipo := ParamByName( 'Tipo' ).AsInteger;
               lSoloUnaTransferencia := ParamByName( 'SoloUnaTransferencia' ).AsBoolean;
               lPuedeCambiarTarjeta := ParamList.ParamByName('PuedeCambiarTarjeta').AsBoolean;
               sNombreNivel := NombreNivel( GetGlobalInteger(K_GLOBAL_NIVEL_COSTEO) );
               dLimite := GetGlobalDate( K_GLOBAL_FECHA_LIMITE );
          end;


          if OpenProcess( prNoTransferenciasCosteo, Dataset.RecordCount, FListaParametros ) then
          begin
               FInsert := CreateQuery( GetSQLScript( Q_INSERT_COSTEO_TRANSFERS ) );
               FExiste := CreateQuery( GetSQLScript( Q_EXISTE_COSTEO_TRANSFERS ) );

               InitRitmos;
               try
                  InitTarjeta;
                  try
                     // Antes de empezar
                     with FTarjeta do
                     begin
                          AgregaTarjetaDiariaBegin( dFecha, dFecha );
                          Queries.RevisaStatusBloqueoBegin;
                     end;
                     with oZetaCreator.Queries do
                     begin
                          GetStatusEmpleadoBegin;
                          GetDatosHorarioBegin;
                     end;

                     // Ejecuci�n
                     with Dataset do
                     begin
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               lExiste :=  FTarjeta.AgregaTarjetaDiariaExiste( iEmpleado, dFecha, FDatosTarjeta );

                               if not lExiste then
                               begin
                                    if ( PuedeCambiarTarjeta( dFecha, dFecha, dLimite, lPuedeCambiarTarjeta, sDetalle ) ) then
                                    begin
                                         FTarjeta.AgregaTarjetaDiariaEscribe( iEmpleado, dFecha, FDatosTarjeta );
                                         lExiste := TRUE;
                                    end
                                    else
                                         Log.Error(iEmpleado,'No se Puede Agregar Transferencias',sDetalle);
                               end;

                               if lExiste then
                               begin
                                    FExiste.Active := FALSE;
                                    ParamAsInteger( FExiste, 'CB_CODIGO', iEmpleado );
                                    ParamAsDateTime( FExiste, 'AU_FECHA', dFecha );
                                    ParamAsString( FExiste, 'CC_CODIGO', sCCDestino );
                                    FExiste.Active := TRUE;

                                    if (lSoloUnaTransferencia and FExiste.Fields[0].IsNull) or ( NOT lSoloUnaTransferencia) then
                                    begin
                                         ParamAsInteger( FInsert, 'CB_CODIGO', iEmpleado );
                                         ParamAsDateTime( FInsert, 'AU_FECHA', dFecha );

                                         ParamAsFloat( FInsert, 'TR_HORAS', rHoras );
                                         ParamAsInteger( FInsert, 'TR_TIPO', iTipo );
                                         ParamAsString( FInsert, 'TR_MOTIVO', sMotivo );
                                         ParamAsString( FInsert, 'CC_CODIGO', sCCDestino );
                                         ParamAsFloat( FInsert, 'TR_NUMERO', 0 );
                                         ParamAsString( FInsert, 'TR_TEXTO', sObserva );
                                         ParamAsInteger( FInsert, 'US_CODIGO', UsuarioActivo );
                                         ParamAsDateTime( FInsert, 'TR_FECHA', Date );
                                         ParamAsString( FInsert, 'TR_GLOBAL', K_GLOBAL_SI );
                                         ParamAsInteger( FInsert, 'TR_APRUEBA', UsuarioActivo );
                                         ParamAsDateTime( FInsert, 'TR_FEC_APR', Date );
                                         ParamAsInteger( FInsert, 'TR_STATUS', Ord(stcAprobada) );

                                         EmpiezaTransaccion;
                                         try
                                            Ejecuta( FInsert );
                                            TerminaTransaccion( True );
                                         except
                                               on Error: Exception do
                                               begin
                                                    RollBackTransaccion;
                                                    Log.Excepcion( iEmpleado, 'Error al Agregar Transferencia ' + FechaCorta( dFecha ), Error );
                                               end;
                                         end;
                                    end
                                    else
                                    begin
                                         Log.Advertencia( iEmpleado, 'Transferencia Repetida no fue Agregada',
                                                                     Format( 'Empleado: %d ' +CR_LF +
                                                                             'Fecha: %s ' +CR_LF +
                                                                             '%s: %s ', [iEmpleado, FechaCorta( dFecha ), sNombreNivel, sCCDestino ] ) );
                                    end;
                               end;
                               Next;
                          end;
                     end;//Dataset

                     with oZetaCreator.Queries do
                     begin
                          GetDatosHorarioEnd;
                          GetStatusEmpleadoEnd;
                     end;
                     { Al terminar }
                     with FTarjeta do
                     begin
                          AgregaTarjetaDiariaEnd;
                          Queries.RevisaStatusBloqueoEnd;
                     end;
                  except
                        on Error: Exception do
                        begin
                             Log.Excepcion( 0, 'Error Al Grabar las Transferencias', Error );
                        end;
                  end;
               finally
                      FreeAndNil( FInsert );
                      FreeAndNil( FExiste );
                      ClearTarjeta;
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerAsistencia.GetClasificacionTempLista(Empresa, Parametros, Lista : OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GetClasificacionTempLista');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitGlobales;
          NumNiveles;
          oSuperASCII := TdmSuperASCII.Create( Self );
          try
             with oSuperASCII do
             begin
                  OnValidar := ClasificacionTempValidaASCII;
                  Formato := eFormatoASCII( oZetaProvider.ParamList.ParamByName( 'Formato' ).AsInteger );
                  FormatoImpFecha := eFormatoImpFecha( oZetaProvider.ParamList.ParamByName( 'FormatoImpFecha' ).AsInteger );
                  AgregaColumna( 'CB_CODIGO', tgNumero, 9, TRUE );
                  AgregaColumna( 'AU_FECHA', tgFecha, 10, TRUE );
                  AgregaColumna( 'CB_PUESTO', tgTexto, 6, FALSE );
                  AgregaColumna( 'CB_CLASIFI', tgTexto, 6, FALSE );
                  AgregaColumna( 'CB_TURNO', tgTexto, 6, FALSE );
                  AgregaColumna( 'CB_NIVEL1', tgTexto, K_ANCHO_CODIGO_NIVELES, FALSE );
                  AgregaColumna( 'CB_NIVEL2', tgTexto, K_ANCHO_CODIGO_NIVELES, FALSE );
                  AgregaColumna( 'CB_NIVEL3', tgTexto, K_ANCHO_CODIGO_NIVELES, FALSE );
                  AgregaColumna( 'CB_NIVEL4', tgTexto, K_ANCHO_CODIGO_NIVELES, FALSE );
                  AgregaColumna( 'CB_NIVEL5', tgTexto, K_ANCHO_CODIGO_NIVELES, FALSE );
                  AgregaColumna( 'CB_NIVEL6', tgTexto, K_ANCHO_CODIGO_NIVELES, FALSE );
                  AgregaColumna( 'CB_NIVEL7', tgTexto, K_ANCHO_CODIGO_NIVELES, FALSE );
                  AgregaColumna( 'CB_NIVEL8', tgTexto, K_ANCHO_CODIGO_NIVELES, FALSE );
                  AgregaColumna( 'CB_NIVEL9', tgTexto, K_ANCHO_CODIGO_NIVELES, FALSE );
     {$IFDEF ACS}
                  AgregaColumna( 'CB_NIVEL10', tgTexto, K_ANCHO_CODIGO_NIVELES, FALSE );
                  AgregaColumna( 'CB_NIVEL11', tgTexto, K_ANCHO_CODIGO_NIVELES, FALSE );
                  AgregaColumna( 'CB_NIVEL12', tgTexto, K_ANCHO_CODIGO_NIVELES, FALSE );
     {$ENDIF}
                  FQryActual := CreateQuery( Format(  GetSQLScript( Q_SELECT_ASISTENCIA_CLASIFICACIONES_TEMPORALES ), [ Nivel0( EmpresaActiva ) ] ) );
                  Result := Procesa( Lista );
                  ErrorCount := Errores;
             end;
          finally
                 oSuperASCII.Free;
                 FreeAndNil( FQryActual );
          end;
     end;
     EndLog; SetComplete;
end;

procedure TdmServerAsistencia.ClasificacionTempValidaASCII(DataSet: TDataset; var nProblemas: Integer; var sErrorMsg: String);
//var
   //i:Integer;
   procedure ValidaCampo( sCampo, sTabla, sValor, sNombreTabla : String );
   begin
        try
           if StrLleno( sCampo ) and StrLleno( sTabla ) and StrLleno( sValor ) then
                with oZetaProvider do
                begin
                     FQryValida := CreateQuery( Format( GetSQLScript( Q_SELECT_ASISTENCIA_CLASIFICACIONES_TEMPORALES_EXISTECATALOGO ), [ sCampo, sTabla, UpperCase( Comillas( Trim( sValor ) ) ) ] ) );
                     Ejecuta( FQryValida );
                     if ( FQryValida.IsEmpty ) then
                     begin
                          Inc( nProblemas );
                          sErrorMsg := sErrorMsg + ' ' + sNombreTabla + ' [ ' + sValor + ' ] es inv�lido. ';
                     end;
                end
        finally
           FreeAndNil( FQryValida )
        end;
   end;

begin

     with Dataset do
     begin
          if (  FieldByName( 'CB_CODIGO' ).AsInteger <= 0 ) then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + ' El N�mero de Empleado es inv�lido. ';
          end;

          ValidaCampo( 'PU_CODIGO', 'PUESTO', FieldByName( 'CB_PUESTO' ).AsString, D_TEXT_NIVEL_PUESTO );
          ValidaCampo( 'TB_CODIGO', 'CLASIFI', FieldByName( 'CB_CLASIFI' ).AsString, D_TEXT_NIVEL_CLASIFICACION );
          ValidaCampo( 'TU_CODIGO', 'TURNO', FieldByName( 'CB_TURNO' ).AsString, D_TEXT_NIVEL_TURNO );
          //for i := 1 to K_GLOBAL_NIVEL_MAX do
            //   ValidaCampo( 'TB_CODIGO', 'NIVEL' + IntToStr( i ), FieldByName( 'CB_NIVEL' + IntToStr( i ) ).AsString, oZetaProvider.NombreNivel( i ) );

          if ( StrVacio( FieldByName( 'CB_PUESTO' ).AsString )
               and StrVacio( FieldByName( 'CB_CLASIFI' ).AsString )
               and StrVacio( FieldByName( 'CB_TURNO' ).AsString )
               and StrVacio( FieldByName( 'CB_NIVEL1' ).AsString )
               and StrVacio( FieldByName( 'CB_NIVEL2' ).AsString )
               and StrVacio( FieldByName( 'CB_NIVEL3' ).AsString )
               and StrVacio( FieldByName( 'CB_NIVEL4' ).AsString )
               and StrVacio( FieldByName( 'CB_NIVEL5' ).AsString )
               and StrVacio( FieldByName( 'CB_NIVEL6' ).AsString )
               and StrVacio( FieldByName( 'CB_NIVEL7' ).AsString )
               and StrVacio( FieldByName( 'CB_NIVEL8' ).AsString )
               and StrVacio( FieldByName( 'CB_NIVEL9' ).AsString )
{$IFDEF ACS}
               and StrVacio( FieldByName( 'CB_NIVEL10' ).AsString )
               and StrVacio( FieldByName( 'CB_NIVEL11' ).AsString )
               and StrVacio( FieldByName( 'CB_NIVEL12' ).AsString )
{$ENDIF}
               ) then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + ' No hay informaci�n v�lida para el rengl�n. ';
          end;

          //Valida Si EXISTE
          with oZetaProvider do
          begin
               FQryActual.Active := False;
               ParamAsInteger( FQryActual, 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger);
               FQryActual.Active := True;
               if ( FQryActual.IsEmpty ) then
               begin
                    Inc( nProblemas );
                    sErrorMsg := sErrorMsg + ' Empleado No Existe. ';
               end;
          end;
     end;
end;

function TdmServerAsistencia.GetClasificacionTempGetLista(Empresa, Parametros, Lista: OleVariant): OleVariant;
begin
     InitLog(Empresa,'GetClasificacionTempGetLista');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          cdsASCII.Lista := Lista;

          ImportarClasificacionTempCrearDataSet;
          ImportarClasificacionTempBuildDataSet;

          Result := cdsLista.Data;
          EndLog; SetComplete;
     end;
end;

procedure TdmServerAsistencia.ImportarClasificacionTempCrearDataSet;
var
   i : Integer;
begin
     with cdsLista do
     begin
          InitTempDataSet;

          AddIntegerField( 'CB_CODIGO' );
          AddDateField( 'AU_FECHA' );
          AddStringField( 'CB_PUESTO', K_ANCHO_CODIGO );
          AddStringField( 'PRETTYNAME', K_ANCHO_PRETTYNAME );
          AddStringField( 'CB_CLASIFI', K_ANCHO_CODIGO );
          AddStringField( 'CB_TURNO', K_ANCHO_CODIGO );
          for i := 1 to K_GLOBAL_NIVEL_MAX do
              AddStringField( 'CB_NIVEL' + IntToStr( i ), K_ANCHO_CODIGO );

          IndexFieldNames:= 'CB_CODIGO;AU_FECHA';
          CreateTempDataset;
     end;
end;

procedure TdmServerAsistencia.ImportarClasificacionTempBuildDataset;
var
     iEmpleado, i : Integer;
     dFecha : TdateTime;
     FDatosEmpleado : TZetaCursor;

     procedure AsignaCampo( sCampo : String );
     begin
          with cdsLista do
               if StrLleno( cdsASCII.FieldByName( sCampo ).AsString ) then
                    FieldByName( sCampo ).AsString := cdsASCII.FieldByName( sCampo ).AsString;
     end;

     function GetPrettyName( const iEmpleado : Integer ) : String;
     begin
          oZetaProvider.ParamAsInteger( fDatosEmpleado, 'CB_CODIGO', iEmpleado);
          with FDatosEmpleado do
          begin
               Close;
               Open;
               If not FDatosEmpleado.IsEmpty then
                   Result := FieldByName( 'PRETTYNAME' ).AsString
               else
                   Result := VACIO;
          end;
     end;
begin
     try
        with oZetaProvider do
        begin
             FDatosEmpleado := CreateQuery( Format( GetSQLScript( Q_SELECT_ASISTENCIA_CLASIFICACIONES_TEMPORALES ), [ Nivel0( EmpresaActiva ) ] ) );
             with cdsASCII do
             begin
                  First;
                  while not EOF do
                  begin
                       iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                       dFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
                       if ( cdsLista.FindKey( [ iEmpleado, dFecha ] ) ) then
                            Edit
                       else
                       begin
                            cdsLista.Append;
                            cdsLista.FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                            cdsLista.FieldByName( 'AU_FECHA' ).AsDateTime := dFecha;
                            cdsLista.FieldByName( 'PRETTYNAME' ).AsString := GetPrettyName( iEmpleado );

                            AsignaCampo('CB_CLASIFI');
                            AsignaCampo('CB_TURNO');
                            AsignaCampo('CB_PUESTO');
                            for i := 1 to K_GLOBAL_NIVEL_MAX do
                                  AsignaCampo('CB_NIVEL' + IntToStr( i ) );
                       end;
                       Next;
                  end;
             end;
        end;
     finally
            FreeAndNil( FDatosEmpleado );
     end;
end;

procedure TdmServerAsistencia.ImportarClasificacionesTempParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := FListaParametros + K_PIPE + 'Archivo: ' + ParamByName('Archivo').AsString;
          FListaParametros := FListaParametros + K_PIPE + 'Formato Fecha: ' + ObtieneElemento( lfFormatoImpFecha, ParamByName('FormatoImpFecha').AsInteger);
          FListaParametros := FListaParametros + K_PIPE + 'Observaciones: ' + ParamByName('Observaciones').AsString;
     end;
end;


function TdmServerAsistencia.ImportarClasificacionesTemp(Empresa, Parametros, Lista: OleVariant): OleVariant;
var
   FQueryClasifiTmp, FQueryInsertClasifiTmp, FQueryUpdateClasifiTmp, FQuerySelectClasifiTmp : TZetaCursor;
   iEmpleado : Integer;
   dFecha : TDateTime;
   sDetalle : String;

   function ExisteClasificacionTemporal( const iNumEmp : Integer; const dFecha : TdateTime ): Boolean;
   begin
        with oZetaProvider do
        begin
             ParamAsInteger(FQueryClasifiTmp, 'Empleado', iNumEmp);
             ParamAsDate(FQueryClasifiTmp, 'Fecha', dFecha);
        end;
        FQueryClasifiTmp.Active := TRUE;
        Result := ( FQueryClasifiTmp.RecordCount > 0 );
        FQueryClasifiTmp.Active := FALSE;
   end;

   procedure InsertClasificacionTemporal( const Data : TServerDataSet );
   var
      iEmpleado, i : Integer;
      dFecha : TDateTime;
   begin
        with oZetaProvider do
        begin
             InitGlobales;
             NumNiveles;
             try
                FQueryInsertClasifiTmp := CreateQuery( GetSQLScript( Q_INSERT_ASISTENCIA_CLASIFITMP ) );
                sDetalle := ' Fecha: ' + DateToStr( Data.FieldByName('AU_FECHA').AsDateTime ) + CR_LF;
                iEmpleado := Data.FieldByName('CB_CODIGO').AsInteger;
                dFecha := Data.FieldByName('AU_FECHA').AsDateTime;
                ParamAsInteger( FQueryInsertClasifiTmp, 'CB_CODIGO', iEmpleado );
                ParamAsDateTime( FQueryInsertClasifiTmp, 'AU_FECHA', dFecha );

                ParamAsString( FQueryInsertClasifiTmp, 'CB_PUESTO', UpperCase( Data.FieldByName('CB_PUESTO').AsString ) );
                if ( StrLleno( Data.FieldByName('CB_PUESTO').AsString ) ) then
                     sDetalle := sDetalle + ' ' + D_TEXT_NIVEL_PUESTO + ': ' + UpperCase( Data.FieldByName('CB_PUESTO').AsString ) + CR_LF;

                ParamAsString( FQueryInsertClasifiTmp, 'CB_CLASIFI', UpperCase( Data.FieldByName('CB_CLASIFI').AsString ) );
                if ( StrLleno( Data.FieldByName('CB_CLASIFI').AsString ) ) then
                     sDetalle := sDetalle + ' ' + D_TEXT_NIVEL_CLASIFICACION + ': ' + UpperCase( Data.FieldByName('CB_CLASIFI').AsString ) + CR_LF;
                ParamAsString( FQueryInsertClasifiTmp, 'CB_TURNO', UpperCase( Data.FieldByName('CB_TURNO').AsString ) );
                if ( StrLleno( Data.FieldByName('CB_TURNO').AsString ) ) then
                     sDetalle := sDetalle + ' ' + D_TEXT_NIVEL_TURNO + ': ' + UpperCase( Data.FieldByName('CB_TURNO').AsString ) + CR_LF;

                for i := 1 to K_GLOBAL_NIVEL_MAX do
                begin
                     ParamAsString( FQueryInsertClasifiTmp, 'CB_NIVEL' + IntToStr( i ), UpperCase( Data.FieldByName( 'CB_NIVEL' + IntToStr( i ) ).AsString ) );
                     if ( StrLleno( Data.FieldByName( 'CB_NIVEL' + IntToStr( i ) ).AsString ) ) then
                          sDetalle := sDetalle + ' ' + oZetaProvider.NombreNivel( i ) + ': ' + UpperCase(  Data.FieldByName( 'CB_NIVEL' + IntToStr( i ) ).AsString ) + CR_LF;
                end;

                ParamAsInteger( FQueryInsertClasifiTmp, 'US_CODIGO', UsuarioActivo );
                ParamAsDateTime( FQueryInsertClasifiTmp, 'CT_FECHA', FechaDefault );
                ParamAsInteger( FQueryInsertClasifiTmp, 'US_COD_OK', -1 );

                oZetaProvider.Ejecuta( FQueryInsertClasifiTmp );
                AvisarTarjetas( iEmpleado, dFecha );
                if ZetaServerTools.RegistrarBitacora( clbAsisImportaClasifTemp, GetGlobalString( K_GLOBAL_GRABA_BITACORA ) ) then
                     Log.Evento(clbAsisImportaClasifTemp,iEmpleado,dFecha,'Insert� registro de Clasificaci�n Temporal',sDetalle);
             finally
                    FreeAndNil( FQueryInsertClasifiTmp );
             end;
        end;
   end;

   function AsignaCampo( const sCampoAnterior, sCampoActual, sNombreCampo : String ): String;
   begin
        if ( StrVacio( sCampoActual ) ) then
             Result := sCampoAnterior
        else
        begin
             if ( StrLleno( sCampoActual ) ) then
                  sDetalle := sDetalle + ' ' + sNombreCampo + ': ' + UpperCase( sCampoActual ) + CR_LF;
             Result := UpperCase( sCampoActual );
        end;
   end;

   procedure UpdateClasificacionTemporal( const Data : TServerDataSet );
   var
        sClasificacion, sTurno, sPuesto : String;
        sNiveles : array[1..K_GLOBAL_NIVEL_MAX] of string;
        iEmpleado, i : Integer;
        dFecha : TDateTime;
   begin
        with oZetaProvider do
        begin
             InitGlobales;
             NumNiveles;
             try
                FQuerySelectClasifiTmp := CreateQuery( GetSQLScript( Q_SELECT_ASISTENCIA_CLASIFITMP ) );
                with FQuerySelectClasifiTmp do
                begin
                     Active := False;
                     iEmpleado := Data.FieldByName('CB_CODIGO').AsInteger;
                     dFecha := Data.FieldByName('AU_FECHA').AsDateTime;
                     ParamAsInteger( FQuerySelectClasifiTmp, 'Empleado', iEmpleado );
                     ParamAsDateTime( FQuerySelectClasifiTmp, 'Fecha', dFecha );
                     Active := True;
                     sClasificacion := FieldByName( 'CB_CLASIFI' ).AsString;
                     sTurno := FieldByName( 'CB_TURNO' ).AsString;
                     sPuesto := FieldByName( 'CB_PUESTO' ).AsString;

                     for i := 1 to K_GLOBAL_NIVEL_MAX do
                          sNiveles[ i ] := FieldByName( 'CB_NIVEL' + IntToStr( i ) ).AsString;

                end;
             finally
               FreeAndNil( FQuerySelectClasifiTmp );
             end;
             try
                FQueryUpdateClasifiTmp := CreateQuery( GetSQLScript( Q_UPDATE_ASISTENCIA_CLASIFITMP ) );
                sDetalle := ' Fecha: ' + DateToStr( Data.FieldByName('AU_FECHA').AsDateTime ) + CR_LF;

                ParamAsInteger( FQueryUpdateClasifiTmp, 'CB_CODIGO', iEmpleado );
                ParamAsDateTime( FQueryUpdateClasifiTmp, 'AU_FECHA', dFecha );

                ParamAsString( FQueryUpdateClasifiTmp, 'CB_PUESTO', AsignaCampo( sPuesto, Data.FieldByName('CB_PUESTO').AsString, D_TEXT_NIVEL_PUESTO ) );
                ParamAsString( FQueryUpdateClasifiTmp, 'CB_CLASIFI', AsignaCampo( sClasificacion, Data.FieldByName('CB_CLASIFI').AsString, D_TEXT_NIVEL_CLASIFICACION ) );
                ParamAsString( FQueryUpdateClasifiTmp, 'CB_TURNO', AsignaCampo( sTurno, Data.FieldByName('CB_TURNO').AsString, D_TEXT_NIVEL_TURNO ) );

                for i := 1 to K_GLOBAL_NIVEL_MAX do
                     ParamAsString( FQueryUpdateClasifiTmp, 'CB_NIVEL' + IntToStr( i ), AsignaCampo( sNiveles[ i ], Data.FieldByName('CB_NIVEL' + IntToStr( i ) ).AsString, NombreNivel( i ) ) );

                ParamAsInteger( FQueryUpdateClasifiTmp, 'US_CODIGO', UsuarioActivo );
                ParamAsDateTime( FQueryUpdateClasifiTmp, 'CT_FECHA', FechaDefault );
                ParamAsInteger( FQueryUpdateClasifiTmp, 'US_COD_OK', -1 );
                Ejecuta( FQueryUpdateClasifiTmp );
                AvisarTarjetas( iEmpleado, dFecha );
                if ZetaServerTools.RegistrarBitacora( clbAsisImportaClasifTemp, GetGlobalString( K_GLOBAL_GRABA_BITACORA ) ) then
                     Log.Evento(clbAsisImportaClasifTemp,iEmpleado,dFecha,'Actualiz� registro de Clasificaci�n Temporal',sDetalle);
             finally
               FreeAndNil( FQueryUpdateClasifiTmp );
             end;
        end;
   end;

begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          ImportarClasificacionesTempParametros;
          cdsLista.Lista := Lista;
          if OpenProcess( prASISImportacionClasificaciones, cdsLista.RecordCount, FListaParametros ) then
          begin
               with cdsLista do
               begin
                    cdsLista.First;
                    while not EOF do
                    begin
                         EmpiezaTransaccion;
                         iEmpleado := FieldByName('CB_CODIGO').AsInteger;
                         dFecha := FieldByName('AU_FECHA').AsDateTime;
                         try
                            FQueryClasifiTmp := CreateQuery( GetSQLScript( Q_SELECT_ASISTENCIA_CLASIFITMP ) );
                            if ExisteClasificacionTemporal( iEmpleado, FieldByName('AU_FECHA').AsDateTime ) then
                            begin
                                 try
                                    UpdateClasificacionTemporal( cdsLista );
                                    TerminaTransaccion( True );
                                 except
                                       On Error: Exception do
                                       begin
                                            RollBackTransaccion;
                                            Log.Excepcion( iEmpleado, 'Error Al Importar Clasificaciones Temporales ' + FechaCorta( dFecha ), Error );
                                       end;
                                 end;
                            end
                            else
                            begin
                                 try
                                    InsertClasificacionTemporal( cdsLista );
                                    TerminaTransaccion( True );
                                 except
                                       On Error: Exception do
                                       begin
                                            RollBackTransaccion;
                                            Log.Excepcion( iEmpleado, 'Error Al Importar Clasificaciones Temporales ' + FechaCorta( dFecha ), Error );
                                       end;
                                 end;
                            end;
                            cdsLista.Next;
                         finally
                                FreeAndNil( FQueryClasifiTmp );
                         end;
                    end;
               end;
          end;
          Result := CloseProcess;
     end;
     SetComplete;     
end;

procedure TdmServerAsistencia.CalculaCosteoParametros;
begin
     with oZetaProvider.DatosPeriodo do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo: ' + ZetacommonTools.ShowNomina( Year, Ord( Tipo ), Numero );
     end;
end;

function TdmServerAsistencia.CalculaCosteo(Empresa,  Parametros: OleVariant): OleVariant;
 var
    FCuenta, FCalcula : TZetaCursor;
    iCuantos: integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;
          GetDatosPeriodo;

          CalculaCosteoParametros;

          FCuenta := CreateQuery( GetSQLScript( Q_COSTEO_CUENTA_NOMINAS ) );
          try
             AsignaDataSetParams( FCuenta );
             with FCuenta do
             begin
                  Active := True;
                  iCuantos := Fields[ 0 ].AsInteger;
                  Active := False;
             end;

             if OpenProcess( prNoCalculaCosteo, iCuantos, FListaParametros) then
             begin
                  FCalcula := CreateQuery( GetSQLScript( Q_COSTEO_CALCULA ) );
                  try
                     EmpiezaTransaccion;
                     try
                        AsignaDataSetParams( FCalcula );
                        Ejecuta( FCalcula );
                        TerminaTransaccion( True );
                     except
                           on Error: Exception do
                           begin
                                RollBackTransaccion;
                                Log.Excepcion( 0, 'Error Al Calcular Costeo', Error );
                           end;
                     end;
                  finally
                         FreeAndNil(FCalcula);
                  end;
                  Result := CloseProcess;
             end;
          finally
                 FreeAndNil(FCuenta);
          end;
     end;
end;

function TdmServerAsistencia.GetSuperXCentroCosto( Empresa: OleVariant ): OleVariant;
begin
     InitLog(Empresa,'GetSuperXCentroCosto');
     Result:= oZetaProvider.OpenSQL( Empresa, GetSQLScript( Q_LEE_SUPER_X_CENTRO_COSTO ), TRUE );
     EndLog;SetComplete;
end;


procedure TdmServerAsistencia.IncluirBitacora(const eClase: eClaseBitacora; const iEmpleado: Integer; const dFecha: TDate; const sMensaje: TBitacoraTexto; const sTexto: String);
begin
     with oZetaProvider do
     begin
          if Log = nil then
             EscribeBitacora( tbNormal, eClase, iEmpleado, dFecha, sMensaje, sTexto )
          else
             Log.Evento( eClase, iEmpleado, dFecha, sMensaje, sTexto );
     end;
end;

//Procesdo de Cancelacion de Transferencias, manuales y globales
//CancelaTransferencias
function TdmServerAsistencia.CancelaTransferenciasCosteo(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'CancelaTransferenciasCosteo');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             InitGlobales;
             NumNiveles;
        end;
        CancelaTransferenciasCosteoParametros;
        CancelaTransferenciasCosteoBuildDataset;
        Result := CancelaTransferenciasCosteoDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;

     EndLog;SetComplete;

end;

function TdmServerAsistencia.CancelaTransferenciasCosteoGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'CancelaTransferenciasCosteoGetLista');
     try
        InitBroker;
        with oZetaProvider do
        begin
             EmpresaActiva:= Empresa;
             AsignaParamList( Parametros );
             InitGlobales;
        end;
        CancelaTransferenciasCosteoBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerAsistencia.CancelaTransferenciasCosteoLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'CancelaTransferenciasCosteoLista');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitGlobales;
          NumNiveles; // Para que cargue las descripciones de
     end;
     CancelaTransferenciasCosteoParametros;
     cdsLista.Lista := Lista;
     Result := CancelaTransferenciasCosteoDataset( cdsLista );
     cdsLista.Close;
     EndLog;SetComplete;
end;

procedure TdmServerAsistencia.CancelaTransferenciasCosteoParametros;
 const
      K_TEXTO_TODAS = 'Todos';
 var
    sNombreNivel: string;

 function ObtieneElementoeSpecial( const eLista: ListasFijas; const Index: integer ): string;
 begin
      if ( Index = K_INDICE_TODAS_COSTEO  ) then
         Result := K_TEXTO_TODAS
      else
          Result := ObtieneElemento( eLista, Index-1 )

 end;

 function GetTextoCaptura( const Index: string ): string;
 begin
      if StrVacio( Index ) then
         Result := K_TEXTO_TODAS
      else if ( Index =  K_GLOBAL_NO ) then
           Result := 'Individuales'
      else
           Result := 'Globales';
 end;
begin
     with oZetaProvider, ParamList do
     begin
          sNombreNivel := NombreNivel( GetGlobalInteger(K_GLOBAL_NIVEL_COSTEO) );
          FListaParametros := VACIO;
          FListaParametros := 'Fecha Inicial: ' + FechaCorta( ParamByName( 'FechaInicial' ).AsDateTime ) +
                              K_PIPE + Format( '%s Original: %s',  [ sNombreNivel, StrDef(ParamByName( 'CCOriginal' ).AsString, K_TEXTO_TODAS )] ) +
                              K_PIPE + 'Transferido hacia: '+  StrDef( ParamByName( 'CCDestino' ).AsString, K_TEXTO_TODAS ) +
                              K_PIPE + 'Tipo: ' + ObtieneElementoeSpecial( lfTipoHoraTransferenciaCosteo, ParamByName('Tipo' ).AsInteger ) +
                              K_PIPE + 'Motivo: ' +  StrDef( ParamByName( 'Motivo' ).AsString, K_TEXTO_TODAS ) +
                              K_PIPE + 'Status: ' + ObtieneElementoeSpecial( lfStatusTransCosteo, ParamByName('Status' ).AsInteger ) +
                              K_PIPE + 'Captura: ' + GetTextoCaptura( ParamByName('Captura' ).AsString ) +
                              K_PIPE + 'Observaciones o comentarios: ' + StrDef( ParamByName( 'Observa' ).AsString, K_TEXTO_TODAS  ) ;
     end;
end;

procedure TdmServerAsistencia.CancelaTransferenciasCosteoBuildDataset;
begin
     with SQLBroker do
     begin
          Init( enTransferencias );
          with Agente do
          begin
               AgregaColumna( 'V_TRANSFER.CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'PrettyName' );
               AgregaColumna( 'CC_DESTINO', TRUE, Entidad, tgTexto, 6, 'CC_DESTINO' );
               AgregaColumna( 'CC_ORIGEN', TRUE, Entidad, tgTexto, 6, 'CC_ORIGEN' );
               AgregaColumna( 'TR_MOTIVO', TRUE, Entidad, tgTexto, 6, 'TR_MOTIVO' );
               AgregaColumna( 'TR_TEXTO', TRUE, Entidad, tgTexto, 30, 'TR_TEXTO' );
               AgregaColumna( 'TR_TXT_APR', TRUE, Entidad, tgTexto, 50, 'TR_TXT_APR' );
               AgregaColumna( 'TR_TIPO', TRUE, Entidad, tgNumero, 0, 'TR_TIPO' );
               AgregaColumna( 'TR_STATUS', TRUE, Entidad, tgNumero, 0, 'TR_STATUS' );
               AgregaColumna( 'TR_GLOBAL', TRUE, Entidad, tgTexto, 1, 'TR_GLOBAL' );
               AgregaColumna( 'US_CODIGO', TRUE, Entidad, tgNumero, 0, 'US_CODIGO' );
               AgregaColumna( 'TR_APRUEBA', TRUE, Entidad, tgNumero, 0, 'TR_APRUEBA' );
               AgregaColumna( 'TR_HORAS', TRUE, Entidad, tgFloat, 0, 'TR_HORAS' );
               AgregaColumna( 'LLAVE', TRUE, Entidad, tgNumero, 0, 'LLAVE' );

               //Filtros que vienen del cliente
               with oZetaProvider, ParamList do
               begin
                    with ParamByName('CCDestino') do
                    begin
                         if StrLleno(AsString) then
                            AgregaFiltro( Format( 'CC_DESTINO= %s', [Comillas(AsString)] ), TRUE, Entidad );
                    end;

                    with ParamByName('CCOriginal') do
                    begin
                         if StrLleno(AsString) then
                            AgregaFiltro( Format( 'CC_ORIGEN= %s', [Comillas(AsString)] ), TRUE, Entidad );
                    end;

                    AgregaFiltro( Format( 'V_TRANSFER.AU_FECHA = %s', [ DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDateTime ) ] ), TRUE, Entidad );

                    with ParamByName('Tipo') do
                    begin
                         if ( AsInteger > K_INDICE_TODAS_COSTEO  ) then
                            AgregaFiltro( Format( 'TR_TIPO = %d', [ AsInteger -1 ] ), TRUE, Entidad );
                    end;

                    with ParamByName('Status') do
                    begin
                         if ( AsInteger > K_INDICE_TODAS_COSTEO  ) then
                              AgregaFiltro( Format( 'TR_STATUS = %d', [ AsInteger -1 ] ), TRUE, Entidad );
                    end;

                    if StrLleno(ParamByName('Motivo').AsString) then
                         AgregaFiltro( Format( 'TR_MOTIVO= %s', [ Comillas(ParamByName('Motivo').AsString ) ] ), TRUE, Entidad );

{$ifndef DOS_CAPAS}
                    if StrLLeno( ParamByName('Captura').AsString ) then
                         AgregaFiltro( Format( 'TR_GLOBAL = %s', [ Comillas( ParamByName('Captura').AsString ) ] ), TRUE, Entidad );
{$endif}
                    with ParamByName( 'Observa' ) do
                    begin
                         if StrLleno( AsString ) then
                         begin
                              //que pasa con los acentos ya capturados?
                              AgregaFiltro( Format( 'dbo.QuitaAcentos(TR_TXT_APR) like ''%0:s'' or dbo.QuitaAcentos(TR_TEXTO) like ''%0:s'' ', [ '%'+ UpperCase(QuitaAcentos(AsString)) + '%' ] ) , TRUE, Entidad );
                         end;
                    end;
               end;
               AgregaOrden( 'CB_CODIGO', TRUE, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

function TdmServerAsistencia.CancelaTransferenciasCosteoDataset(Dataset: TDataset): OleVariant;
 var
    FDataset: TZetaCursor;
    iLLave, iEmpleado : integer;
    sFecha,sMensaje :string;

 function GetMensaje: string;
  var
     sNombreNivel: string;
 begin
      with oZetaProvider do
           sNombreNivel := NombreNivel( GetGlobalInteger(K_GLOBAL_NIVEL_COSTEO) );
      with DataSet do
      begin
           Result := 
           Format( 'Fecha: %s',        [ sFecha ] ) + CR_LF +
           Format( 'Empleado: %d',     [ iEmpleado ] ) + CR_LF +
           Format( '%s Origen: %s',    [ sNombreNivel, FieldByName( 'CC_ORIGEN' ).AsString ] ) + CR_LF +
           Format( '%s Destino: %s',   [ sNombreNivel, FieldByName( 'CC_DESTINO' ).AsString ] ) + CR_LF +
           Format( 'Duraci�n: %s',     [ FieldByName( 'TR_HORAS' ).AsString ] ) + CR_LF +
           Format( 'Tipo: %s',         [ FieldByName( 'TR_TIPO' ).AsString ] ) + CR_LF +
           Format( 'Motivo: %s',       [ FieldByName( 'TR_MOTIVO' ).AsString ] ) + CR_LF +
           Format( 'Status: %s',       [ FieldByName( 'TR_STATUS' ).AsString ] ) + CR_LF +
           Format( 'Transfiri�: %d',   [ FieldByName( 'US_CODIGO' ).AsInteger ] ) + CR_LF +
           Format( 'Recibi�: %d',      [ FieldByName( 'TR_APRUEBA' ).AsInteger ] ) + CR_LF +
           Format( '�Captura Global?: %s',[ FieldByName( 'TR_GLOBAL' ).AsString ] ) + CR_LF +
           Format( 'Observaciones: %s',[ FieldByName( 'TR_TEXTO' ).AsString ] ) + CR_LF +
           Format( 'Comentarios: %s',  [ FieldByName( 'TR_TXT_APR' ).AsString ] );
      end;
 end;

begin
     with oZetaProvider do
     begin
          if OpenProcess( prNoCancelaTransferencias, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  FDataset := CreateQuery( GetSQLScript( Q_CANCELAR_TRANSFERENCIA ) );
                  try
                     with Dataset do
                     begin
                          First;
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iLlave := FieldByName( 'LLAVE' ).AsInteger;
                               iEmpleado := FieldByName('CB_CODIGO').AsInteger;
                               sFecha := FechaCorta( oZetaProvider.ParamList.ParamByName( 'FechaInicial' ).AsDateTime ) ;
                               EmpiezaTransaccion;
                               try
                                  sMensaje := GetMensaje;
                                  ParamAsInteger( FDataset, 'LLAVE', iLlave );
                                  Ejecuta( FDataset );
                                  TerminaTransaccion( TRUE );
                                  Log.Evento( clbNinguno, iEmpleado, NullDateTime, Format( 'Transferencia Eliminada, Emp. %d Fecha %s', [ iEmpleado, sFecha ] ), sMensaje );
                               except
                                     on Error: Exception do
                                     begin
                                          RollBackTransaccion;
                                          Log.Excepcion( iEmpleado, Format( 'Error Al Eliminar Transferencia del Emp. %d Fecha %s ', [  iEmpleado, sFecha ] ), Error, sMensaje );
                                     end;
                               end;
                               Next;
                          end;
                     end;
                  finally
                         FreeAndNil( FDataSet );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Eliminar Transferencias', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerAsistencia.GetAlarmasSynel(Empresa: OleVariant; const Filtro: WideString): OleVariant;
begin
     SetTablaInfo( eSynelAlarma );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Filtro;
          Result := GetTabla( Empresa );
     end;
end;

function TdmServerAsistencia.GrabaAlarmaSynel(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eSynelAlarma );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerAsistencia.GetRutaReporteSynel(Empresa: OleVariant; Reporte: Integer): WideString;
var
   oCursor: TZetaCursor;
begin
     Result := VACIO;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;

          oCursor := CreateQuery( Format( GetSQLScript( Q_GET_RUTA_ARCHIVO_REPORTE ), [ Reporte ]  ) );
          try
             oCursor.Active := TRUE;
             if Not oCursor.IsEmpty then
                Result := oCursor.FieldByName( 'RE_ARCHIVO' ).AsString;
          finally
                 FreeAndNil( oCursor );
          end;
          TerminaTransaccion( TRUE );
     end;
     SetComplete;
end;

function TdmServerAsistencia.GetListaReloj( Empresa: OleVariant): OleVariant;
begin
     Result := oZetaProvider.OpenSQL( Empresa, GetSQLScript( Q_GET_LISTA_RELOJ ), TRUE );
end;



//Logica basada en la funci�n de Vacaciones
function TdmServerAsistencia.GetValorDiaPermiso( oDatosTurno: TEmpleadoDatosTurno; oCursor: TZetaCursor;
         const dFecha: TDate ): TPesos;
var
   eStatusDia : eStatusAusencia;
   lExisteTarjeta, lEsFestivo : Boolean;

   function AbreTarjeta: Boolean;
   { Indica si existe la tarjeta y deja el Cursor Abierto para consultar los campos del registro }
   begin
        with oCursor do
        begin
             Active := FALSE;
             oZetaProvider.ParamAsDate( oCursor, 'Fecha', dFecha );
             Active := TRUE;
             Result := ( not EOF );
        end;
   end;

begin
     lExisteTarjeta := AbreTarjeta;
     with oZetaCreator.Ritmos do
     begin
          // Obtiene el Status Correspondiente ( H�bil, Sabado o Descanso )
          if lExisteTarjeta then
             eStatusDia := eStatusAusencia( oCursor.FieldByName( 'AU_STATUS' ).AsInteger )
          else
             eStatusDia := GetStatusHorario( oDatosTurno.Codigo, dFecha ).Status;
          // Si no es Dia de Descanso, revisa si es D�a Festivo ( Los Festivos cuentan como Descansos )
          if ( eStatusDia <> auDescanso ) then
          begin
               if lExisteTarjeta and ( oCursor.FieldByName( 'AU_POSICIO' ).AsInteger > 0 ) then    // Existe Tarjeta y Pren�mina Calculada - AU_POSICIO toma valor cuando se Calcula la Pren�mina
                  lEsFestivo := ( eTipoDiaAusencia( oCursor.FieldByName( 'AU_TIPODIA' ).AsInteger ) = daFestivo )
               else
                  lEsFestivo := Festivos.CheckFestivo( oDatosTurno.Codigo, dFecha );   // Si no se ha calculado la Pren�mina se debe investigar la Lista de Festivos Generales o del Turno

               if lEsFestivo then
                  eStatusDia := auDescanso;
          end;

          // Ya Determinado el Status se investiga el Valor del D�a para Permiso
          // TO-DO: Hacerloque lea globales?, investigar el Turno???
          with oDatosTurno do
          begin
               case eStatusDia of
                    auHabil : Result := K_VALOR_DIA_PERMISO_HABIL;
                    auSabado : Result := K_VALOR_DIA_PERMISO_SABADO;
                    auDescanso : Result := K_VALOR_DIA_PERMISO_DESCANSO;
               else
                    Result := 0;
               end;
          end;
     end;
end;

function TdmServerAsistencia.GetPermisoDiasHabiles(Empresa: OleVariant;
  Empleado: Integer; FechaIni, FechaFin: TDateTime): Double;
begin
     InitLog(Empresa,'GetPermisoDiasHabiles');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     Result := ReadPermisoDiasHabiles( Empleado, FechaIni, FechaFin );
     EndLog;SetComplete;
end;

function TdmServerAsistencia.ReadPermisoDiasHabiles( const iEmpleado: Integer; const dFechaIni, dFechaFin: TDate ): TPesos;
var
   FQryTarjeta : TZetaCursor;
   oDatosTurno : TEmpleadoDatosTurno;
   dFecha : TDate;
begin
     {{ TO-DO:  Condicionar leeyendo el global nuevo de permiso ??}
    { if ( FALSE ) then             // Se valida el global, pero es preferible que esto lo haga el cliente ( ya tiene el Global )
     begin

     end
     else
     begin}
     Result := 0;

     //reutiliza la lectura de tarjeta de Vacaciones
     FQryTarjeta:= oZetaProvider.CreateQuery( Format( GetSQLScript( Q_LEE_TARJETA_VACA ), [ iEmpleado ] ) );
     try
        InitRitmos;
        with oZetaCreator.Ritmos do
        begin
             RitmosBegin( dFechaIni, dFechaFin );
             try
                oDatosTurno := GetEmpleadoDatosTurno( iEmpleado, dFechaIni );
                // Se investigan los datos del Primer d�a del Permiso el cual se aplicar� para todo el rango del Permiso
                // {Logica heredada de vacaciones)
                dFecha := dFechaIni;
                while ( dFecha < dFechaFin ) do
                begin
                     Result := Result + GetValorDiaPermiso( oDatosTurno, FQryTarjeta, dFecha );
                     dFecha := ( dFecha + 1 );
                end;

                // En este caso el redondeo no se usa , como codigo defensivo se Trunca. No deben existir decimales.
                Result := Trunc( Result );

             finally
                    RitmosEnd;
             end;
        end;
     finally
            FreeAndNil( FQryTarjeta );
     end;
 {{    end;}
end;


function TdmServerAsistencia.GetPermisoFechaRegreso(Empresa: OleVariant;
  Empleado: Integer; FechaIni: TDateTime; Dias: Double;
  out DiasRango: Double): TDateTime;
begin
     InitLog(Empresa,'GetPermisoFechaRegreso');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     Result := ReadPermisoFechaRegreso( Empleado, FechaIni, Dias, DiasRango );
     EndLog;SetComplete;
end;


function TdmServerAsistencia.ReadPermisoFechaRegreso( const iEmpleado: Integer; const dFechaIni: TDate;
         const rDias: TPesos; out rRango: Double ): TDate;
var
   FQryTarjeta : TZetaCursor;
   dFecha, dFechaRitmo : TDate;
   rSumaDias : TPesos;
   oDatosTurno : TEmpleadoDatosTurno;

   function GetFinalRitmo: TDate;
   const
        K_FACTOR_X_SEMANA = 2;
   var
      iDias: Integer;
   begin
        { Algoritmo para obtener una rango de preparaci�n de Ritmos
          suficiente para la mayoria de los casos }
        iDias := Trunc( rDias ) + 1;
        Result := dFechaIni + iDias + ( ( ( iDias div ZetaCommonClasses.K_DIAS_SEMANA ) + 1 ) * K_FACTOR_X_SEMANA );    // Suma 2 d�as por cada 7 dias contenidos en iDias
   end;

begin
     if not oZetaProvider.GetGlobalBooleano( K_GLOBAL_PERMISOS_DIAS_HABILES ) then
     begin
          Result := ( dFechaIni + rDias );
          rRango := rDias;
     end
     else
     begin
          dFecha := dFechaIni;
          dFechaRitmo := GetFinalRitmo;
          rSumaDias := 0;
          //reutiliza la lectura de tarjeta de Vacaciones
          FQryTarjeta:= oZetaProvider.CreateQuery( Format( GetSQLScript( Q_LEE_TARJETA_VACA ), [ iEmpleado ] ) );
          try
             InitRitmos;
             with oZetaCreator.Ritmos do
             begin
                  RitmosBegin( dFecha, dFechaRitmo );
                  try
                     oDatosTurno := GetEmpleadoDatosTurno( iEmpleado, dFecha );  // Se investigan los datos del Primer d�a de Vacaciones el cual se aplicar� para todo el rango de vacaciones
                     while ( rSumaDias <= rDias ) do
                     begin
                          if ( dFecha > dFechaRitmo ) then     // Si el rango del Ritmo sugerido no es suficiente se deber� volver a preparar dia con dia
                          begin
                               RitmosEnd;                      // Se desprepara el original o bien el Anterior
                               RitmosBegin( dFecha, dFecha );  // Se prepara para el siguiente dia
                               dFechaRitmo := dFecha;          // Para volver a checar si continua dentro del ciclo
                          end;
                          rSumaDias := rSumaDias + GetValorDiaPermiso( oDatosTurno, FQryTarjeta, dFecha );
                          if ( rSumaDias <= rDias ) then
                          begin
                               dFecha := ( dFecha + 1 );
                               rRango := rSumaDias;
                          end;
                     end;
                  finally
                         RitmosEnd;
                  end;
             end;
          finally
                 FreeAndNil( FQryTarjeta );
          end;
          Result := dFecha;
     end;
end;

function TdmServerAsistencia.GetHorarioTurno(Empresa, Parametros: OleVariant): OleVariant;
var
    dFecha: TDate;
    iEmpleado, iTipo : Integer;
begin
    InitLog(Empresa,'GetHorarioTurno');
    with oZetaProvider do
    begin
          iEmpleado := Parametros[ 0 ];
          dFecha := Parametros[ 1 ];
          iTipo := Parametros[ 2 ];

          EmpresaActiva := Empresa;
          InitGlobales;
          InitRitmos;
          with oZetaCreator.Ritmos do
          begin
                RitmosBegin( dFecha, dFecha );
                case iTipo of
                    ord (esfFestivo): Result := VarArrayOf ( [oZetaCreator.Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFecha ).HorarioFestivo ]); //Festivo
                    ord (esfFestivoTransferido) : Result := VarArrayOf ( [oZetaCreator.Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFecha ).StatusHorario.Horario  ]); //Ordinario
                end;
               RitmosEnd;
          end;
     end;
     //EndLog;
     SetComplete;
end;
       
function TdmServerAsistencia.GetEstatusDia(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): Currency;
begin
     InitLog(Empresa,'GetEstatusDia');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     Result := ReadEstatusDia( Empleado, Fecha);
     EndLog;SetComplete;
end;

function TdmServerAsistencia.ReadEstatusDia( const iEmpleado: Integer; const dFecha: TDate): Currency;
var
   FQryTarjeta : TZetaCursor;
   pEstatusDia : TPesos;
   oDatosTurno : TEmpleadoDatosTurno;
begin
    FQryTarjeta:= oZetaProvider.CreateQuery( Format( GetSQLScript( Q_LEE_TARJETA_VACA ), [ iEmpleado ] ) );
    try
       InitRitmos;
       with oZetaCreator.Ritmos do
       begin
            RitmosBegin( dFecha, dFecha );
            try
               oDatosTurno := GetEmpleadoDatosTurno( iEmpleado, dFecha );
               pEstatusDia :=  GetValorDiaPermiso ( oDatosTurno, FQryTarjeta, dFecha );
            finally
                   RitmosEnd;
            end;
       end;
    finally
           FreeAndNil( FQryTarjeta );
    end;
    Result := pEstatusDia;
end;


{$ifndef DOS_CAPAS}
{$ifdef ASISTENCIA_DLL}


initialization
  TComponentFactory.Create(ComServer, TdmServerAsistencia, Class_dmServerAsistencia, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
{$endif}
end.
