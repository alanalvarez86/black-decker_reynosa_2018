unit Consultas_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 5/11/2017 1:07:37 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2017_Inicio2\MTS\Consultas.tlb (1)
// LIBID: {FC480966-88D5-11D3-8BBE-0050DA04EAC5}
// LCID: 0
// Helpfile: 
// HelpString: Tress Consultas
// DepndLst: 
//   (1) v1.0 Midas, (C:\Program Files (x86)\Embarcadero\RAD Studio\12.0\bin64\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ConsultasMajorVersion = 1;
  ConsultasMinorVersion = 0;

  LIBID_Consultas: TGUID = '{FC480966-88D5-11D3-8BBE-0050DA04EAC5}';

  IID_IdmServerConsultas: TGUID = '{FC480967-88D5-11D3-8BBE-0050DA04EAC5}';
  CLASS_dmServerConsultas: TGUID = '{FC480969-88D5-11D3-8BBE-0050DA04EAC5}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerConsultas = interface;
  IdmServerConsultasDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerConsultas = IdmServerConsultas;


// *********************************************************************//
// Interface: IdmServerConsultas
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FC480967-88D5-11D3-8BBE-0050DA04EAC5}
// *********************************************************************//
  IdmServerConsultas = interface(IAppServer)
    ['{FC480967-88D5-11D3-8BBE-0050DA04EAC5}']
    function GetQueryGral(Empresa: OleVariant; const sSQL: WideString; bDerechoCampoConfi: WordBool): OleVariant; safecall;
    function GetProcessLog(Empresa: OleVariant; Folio: Integer): OleVariant; safecall;
    function GetBitacora(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetProcessList(Empresa: OleVariant; Status: Integer; Proceso: Integer; 
                            Usuario: Integer; FechaInicial: TDateTime; FechaFinal: TDateTime): OleVariant; safecall;
    procedure CancelaProceso(Empresa: OleVariant; Folio: Integer); safecall;
    function GetProcess(Empresa: OleVariant; Folio: Integer): OleVariant; safecall;
    function GetConteo(Empresa: OleVariant): OleVariant; safecall;
    function GrabaConteo(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; 
                         out oNewConteo: OleVariant): OleVariant; safecall;
    function GetQueryGralTodos(Empresa: OleVariant; const sSQL: WideString): OleVariant; safecall;
    function GetQueryGralLimite(Empresa: OleVariant; const sSQL: WideString; 
                                bDerechoConfid: WordBool; LimiteReg: Integer): OleVariant; safecall;
    function SetSesionGraficas(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): Integer; safecall;
    function CreateTablerosCompany(Empresa: OleVariant; Parametros: OleVariant): Integer; safecall;
    function SetRolTableros(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function GetListaTableros(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function GetConsultaDashletsTablero(Empresa: OleVariant; Parametros: OleVariant; 
                                        var ErrorCount: Integer): OleVariant; safecall;
    function EjecutarDashlet(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function GetRolTablerosConsulta(Empresa: OleVariant; Parametros: OleVariant; 
                                    var ErrorCount: Integer): OleVariant; safecall;
    function GetRolEmpresas(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function GetRolTableros(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function GrabaRolTablero(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerConsultasDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FC480967-88D5-11D3-8BBE-0050DA04EAC5}
// *********************************************************************//
  IdmServerConsultasDisp = dispinterface
    ['{FC480967-88D5-11D3-8BBE-0050DA04EAC5}']
    function GetQueryGral(Empresa: OleVariant; const sSQL: WideString; bDerechoCampoConfi: WordBool): OleVariant; dispid 1;
    function GetProcessLog(Empresa: OleVariant; Folio: Integer): OleVariant; dispid 2;
    function GetBitacora(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 3;
    function GetProcessList(Empresa: OleVariant; Status: Integer; Proceso: Integer; 
                            Usuario: Integer; FechaInicial: TDateTime; FechaFinal: TDateTime): OleVariant; dispid 4;
    procedure CancelaProceso(Empresa: OleVariant; Folio: Integer); dispid 5;
    function GetProcess(Empresa: OleVariant; Folio: Integer): OleVariant; dispid 6;
    function GetConteo(Empresa: OleVariant): OleVariant; dispid 7;
    function GrabaConteo(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; 
                         out oNewConteo: OleVariant): OleVariant; dispid 8;
    function GetQueryGralTodos(Empresa: OleVariant; const sSQL: WideString): OleVariant; dispid 9;
    function GetQueryGralLimite(Empresa: OleVariant; const sSQL: WideString; 
                                bDerechoConfid: WordBool; LimiteReg: Integer): OleVariant; dispid 301;
    function SetSesionGraficas(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): Integer; dispid 302;
    function CreateTablerosCompany(Empresa: OleVariant; Parametros: OleVariant): Integer; dispid 303;
    function SetRolTableros(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): OleVariant; dispid 304;
    function GetListaTableros(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): OleVariant; dispid 305;
    function GetConsultaDashletsTablero(Empresa: OleVariant; Parametros: OleVariant; 
                                        var ErrorCount: Integer): OleVariant; dispid 306;
    function EjecutarDashlet(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): OleVariant; dispid 307;
    function GetRolTablerosConsulta(Empresa: OleVariant; Parametros: OleVariant; 
                                    var ErrorCount: Integer): OleVariant; dispid 308;
    function GetRolEmpresas(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): OleVariant; dispid 309;
    function GetRolTableros(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): OleVariant; dispid 310;
    function GrabaRolTablero(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): OleVariant; dispid 311;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerConsultas provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerConsultas exposed by              
// the CoClass dmServerConsultas. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerConsultas = class
    class function Create: IdmServerConsultas;
    class function CreateRemote(const MachineName: string): IdmServerConsultas;
  end;

implementation

uses ComObj;

class function CodmServerConsultas.Create: IdmServerConsultas;
begin
  Result := CreateComObject(CLASS_dmServerConsultas) as IdmServerConsultas;
end;

class function CodmServerConsultas.CreateRemote(const MachineName: string): IdmServerConsultas;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerConsultas) as IdmServerConsultas;
end;

end.
