unit VisReportes_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision: 5$
// File generated on 04/07/2006 04:23:05 p.m. from Type Library described below.

// ************************************************************************ //
// Type Lib: D:\3win_20\MTS\VisReportes.tlb (1)
// IID\LCID: {F54F3866-8392-43F4-9CAD-034E8B72FFE6}\0
// Helpfile: 
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (3) v4.0 StdVCL, (C:\WINDOWS\system32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL, 
  MIDAS;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  VisReportesMajorVersion = 1;
  VisReportesMinorVersion = 0;

  LIBID_VisReportes: TGUID = '{F54F3866-8392-43F4-9CAD-034E8B72FFE6}';

  IID_IdmServerVisReportes: TGUID = '{A4DEA538-8C9A-4C5C-8E4E-214717D31E54}';
  CLASS_dmServerVisReportes: TGUID = '{ACC0C49E-A664-40C9-9813-06FC29442B87}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerVisReportes = interface;
  IdmServerVisReportesDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerVisReportes = IdmServerVisReportes;


// *********************************************************************//
// Interface: IdmServerVisReportes
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A4DEA538-8C9A-4C5C-8E4E-214717D31E54}
// *********************************************************************//
  IdmServerVisReportes = interface(IAppServer)
    ['{A4DEA538-8C9A-4C5C-8E4E-214717D31E54}']
    function  GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                        out Error: WideString): OleVariant; safecall;
    function  EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                          out Error: WideString): WordBool; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerVisReportesDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A4DEA538-8C9A-4C5C-8E4E-214717D31E54}
// *********************************************************************//
  IdmServerVisReportesDisp = dispinterface
    ['{A4DEA538-8C9A-4C5C-8E4E-214717D31E54}']
    function  GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                        out Error: WideString): OleVariant; dispid 1;
    function  EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                          out Error: WideString): WordBool; dispid 2;
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                              MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                            Options: Integer; const CommandText: WideString; 
                            var Params: OleVariant; var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerVisReportes provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerVisReportes exposed by              
// the CoClass dmServerVisReportes. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerVisReportes = class
    class function Create: IdmServerVisReportes;
    class function CreateRemote(const MachineName: string): IdmServerVisReportes;
  end;

implementation

uses ComObj;

class function CodmServerVisReportes.Create: IdmServerVisReportes;
begin
  Result := CreateComObject(CLASS_dmServerVisReportes) as IdmServerVisReportes;
end;

class function CodmServerVisReportes.CreateRemote(const MachineName: string): IdmServerVisReportes;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerVisReportes) as IdmServerVisReportes;
end;

end.
