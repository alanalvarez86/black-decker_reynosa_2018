unit DServerConsultas;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DServerConsultas.pas                       ::
  :: Descripci�n: Programa principal de Consultas.dll        ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface
{$define CONFIDENCIALIDAD_MULTIPLE}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, DataBkr, DBClient, MtsRdm, Mtx,DB,
{$ifndef DOS_CAPAS}
     Consultas_TLB,
{$endif}
     DZetaServerProvider;

type
  TdmServerConsultas = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerConsultas {$endif} )
    procedure dmServerConsultasCreate(Sender: TObject);
    procedure dmServerConsultasDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    procedure SetTablaInfo;
    {$ifndef DOS_CAPAS}
    procedure ServerActivate(Sender: TObject);
    procedure ServerDeactivate(Sender: TObject);
    {$endif}
    function ExistenCamposConfi(oEmpresa:OleVariant;sScript:string):Boolean;
    function HayTablasCamposConfidenciales(oEmpresa:OleVariant;const Query,sField,sScript:string;var iPos:Integer;var sTabla:string):Boolean;
    function ExistenTablasConCamposConfidenciales( oEmpresa:OleVariant; const sScript:string ):Boolean;
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetBitacora(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetProcess(Empresa: OleVariant; Folio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetProcessList(Empresa: OleVariant; Status, Proceso, Usuario: Integer; FechaInicial, FechaFinal: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetProcessLog(Empresa: OleVariant; Folio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetQueryGral(Empresa: OleVariant; const sSQL: WideString;bDerechoCampoConfi: WordBool): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function GetQueryGralTodos(Empresa: OleVariant; const sSQL: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetConteo(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaConteo(Empresa, oDelta: OleVariant; var ErrorCount: Integer; out oNewConteo: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    procedure CancelaProceso(Empresa: OleVariant; Folio: Integer); {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetQueryGralLimite(Empresa: OleVariant; const sSQL: WideString; bDerechoConfid: WordBool; LimiteReg: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CreateTablerosCompany(Empresa, Parametros: OleVariant): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetConsultaDashletsTablero(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetListaTableros(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function SetRolTableros(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function SetSesionGraficas(Empresa, Parametros: OleVariant; var ErrorCount: Integer): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function EjecutarDashlet(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetRolTablerosConsulta(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetRolEmpresas(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetRolTableros(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaRolTablero(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
  end;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaSQL,
     ZetaServerTools,
     DConteoPersonal, StrUtils;

const
     Q_BITACORA = 0;
     Q_BITACORA_PROCESO = 1;
     Q_PROCESO = 2;
     Q_PROCESO_LISTA = 3;
     Q_CONFIDENCIALIDAD = 4;
     Q_TABLAS_CONFI = 5;
     Q_CAMPOS_CONFI = 6;
     Q_TABLAS_CAMP_CONFI = 7;

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_BITACORA: Result := 'select BI_FECHA, BI_HORA, BI_TIPO, BI_NUMERO, '+
                                'BI_TEXTO, BI_PROC_ID, US_CODIGO, CB_CODIGO, '+
                                'BI_DATA, BI_CLASE, BI_FEC_MOV ' +
                                'from BITACORA where ' +
                                '( %0:s >= %1:s and %0:s < %2:s ) and '+
                                '( ( %3:d = -1 ) or ( BI_TIPO = %3:d ) ) and '+
                                '( ( %4:d <= 0 ) or ( BI_CLASE = %4:d  ) ) and '+
                                '( ( %5:d = 0 ) or ( CB_CODIGO = %5:d ) ) and '+
                                '( ( %6:d = 0 ) or ( US_CODIGO = %6:d ) ) and '+
                                '( ( %7:d = -1 ) or ( BI_NUMERO = %7:d ) ) '+
                                'order by BI_FECHA DESC, BI_HORA DESC';
           {
                  Q_BITACORA: Result := 'select BI_FECHA, BI_HORA, BI_TIPO, BI_NUMERO, '+
                                'BI_TEXTO, BI_PROC_ID, US_CODIGO, CB_CODIGO, '+
                                'BI_DATA, BI_CLASE, BI_FEC_MOV ' +
                                'from BITACORA where ' +
                                '( %s between ''%s'' and ''%s'' ) and '+
                                '( ( %d = -1 ) or ( BI_TIPO = %d ) ) and '+
                                '( ( %d <= 0 ) or ( BI_CLASE = %d ) ) and '+
                                '( ( %d = 0 ) or ( CB_CODIGO = %d ) ) and '+
                                '( ( %d = 0 ) or ( US_CODIGO = %d ) ) and '+
                                '( ( %d = -1 ) or ( BI_NUMERO = %d ) ) '+
                                'order by BI_FECHA DESC, BI_HORA DESC';  }

          Q_BITACORA_PROCESO: Result := 'select BI_FECHA, BI_HORA, BI_TIPO, BI_NUMERO, '+
                                        'BI_TEXTO, BI_PROC_ID, US_CODIGO, CB_CODIGO, '+
                                        'BI_DATA, BI_CLASE, BI_FEC_MOV '+
                                        'from BITACORA where ' +
                                        '( BI_NUMERO = %d ) order by BI_TIPO, CB_CODIGO, BI_FEC_MOV';
          Q_PROCESO: Result := 'select PC_PROC_ID, PC_NUMERO, US_CODIGO, US_CANCELA, '+
                               'PC_FEC_INI, PC_FEC_FIN, PC_HOR_INI, PC_HOR_FIN, '+
                               'PC_ERROR, CB_CODIGO, PC_MAXIMO, PC_PASO, PC_PARAMS '+
                               'from PROCESO where ( PC_NUMERO = %d )';
          Q_PROCESO_LISTA: Result := 'select PC_PROC_ID, PC_NUMERO, US_CODIGO, US_CANCELA, '+
                                     'PC_FEC_INI, PC_FEC_FIN, PC_HOR_INI, PC_HOR_FIN, '+
                                     'PC_ERROR, CB_CODIGO, PC_MAXIMO, PC_PASO, PC_PARAMS  '+
                                     'from PROCESO where ' +
                                     '( PC_ERROR in ( %s ) ) and '+
                                     '( ( %d = 0 ) or ( PC_PROC_ID = %d ) ) and '+
                                     '( PC_FEC_INI between ''%s'' and ''%s'' ) and '+
                                     '( ( %d = 0 ) or ( US_CODIGO = %d ) ) '+
                                     'order by PC_NUMERO desc';
          Q_CONFIDENCIALIDAD : Result := 'select CM_NIVEL0 from COMPANY where CM_CODIGO = ''%s'' ';
          Q_TABLAS_CONFI : Result := 'select EN_TABLA from R_ENTIDAD where EN_NIVEL0 = ''%s''';
          Q_CAMPOS_CONFI : Result := 'select AT_CAMPO from R_ATRIBUTO where AT_CONFI  = ''%s''';
          Q_TABLAS_CAMP_CONFI : Result := ' SELECT DISTINCT(R.EN_CODIGO),E.EN_TABLA from R_ATRIBUTO R '+
                                          ' left outer join R_ENTIDAD E ON R.EN_CODIGO = E.EN_CODIGO  where AT_CONFI= ''%s''';
     end;
end;

{$R *.DFM}

function StrToList( const sLista: String ): String;
var
   i: Integer;
begin
     Result := '';
     for i := 1 to Length( sLista ) do
     begin
          if ( i > 1 ) then
             Result := Result + ', ';
          Result := Result + ZetaCommonTools.EntreComillas( Copy( sLista, i, 1 ) );
     end;
end;

{ *********** TdmServerConsultas *********** }

class procedure TdmServerConsultas.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerConsultas.dmServerConsultasCreate(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     OnActivate := ServerActivate;
     OnDeactivate := ServerDeactivate;
     {$endif}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerConsultas.dmServerConsultasDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerConsultas.CierraEmpresa;
begin
end;
{$else}
procedure TdmServerConsultas.ServerActivate(Sender: TObject);
begin
     oZetaProvider.Activate;
end;

procedure TdmServerConsultas.ServerDeactivate(Sender: TObject);
begin
     oZetaProvider.Deactivate;
end;
{$endif}

function TdmServerConsultas.HayTablasCamposConfidenciales( oEmpresa:OleVariant; const Query,sField,sScript:string ;var iPos:Integer;var sTabla:string):Boolean;
var
   FDataset:TZetaCursor;
begin
     with oZetaProvider do
     begin
          sTabla := VACIO;
          EmpresaActiva := oEmpresa;
          Result := False;
          FDataset := CreateQuery( Format( Query, [K_GLOBAL_SI] ) );
          if ( FDataSet <> nil )then
          begin
               try
                  with FDataSet do
                  begin
                       Open;
                       First;
                       while ( ( not eof ) and ( not Result ) )do
                       begin
                             iPos := Pos( FieldByName(sField).AsString,sScript );
                             Result := iPos > 0;
                             if Result then
                                sTabla := FieldByName(sField).AsString;
                             Next;
                       end;
                       Close;
                  end;
               finally
                      FreeAndNil(FDataSet);
               end;
          end;
     end;
end;

function TdmServerConsultas.ExistenTablasConCamposConfidenciales( oEmpresa:OleVariant; const sScript:string ):Boolean;
var
   FDataset:TZetaCursor;
   iPos:Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := oEmpresa;
          Result := False;
          FDataset := CreateQuery( Format( GetSQLScript(Q_TABLAS_CAMP_CONFI),[K_GLOBAL_SI] ) );
          if ( FDataSet <> nil )then
          begin
               try
                  with FDataSet do
                  begin
                       Open;
                       First;
                       while ( ( not eof ) and ( not Result ) )do
                       begin
                             iPos := Pos( FieldByName('EN_TABLA').AsString,sScript );
                             Result := iPos > 0;
                             Next;
                       end;
                       Close;
                  end;
               finally
                      FreeAndNil(FDataSet);
               end;
          end;
     end;
end;

function TdmServerConsultas.ExistenCamposConfi(oEmpresa:OleVariant;sScript:string):Boolean;
var
   iPos:Integer;
   sTabla:string;
begin
     Result :=  HayTablasCamposConfidenciales(oEmpresa,GetSQLScript( Q_CAMPOS_CONFI ),'AT_CAMPO',sScript,iPos,sTabla);
end;

{ ********** Interfases ( Paletitas ) *********** }

function TdmServerConsultas.GetQueryGral(Empresa: OleVariant;const sSQL: WideString; bDerechoCampoConfi: WordBool): OleVariant;
begin
     Result := GetQueryGralLimite(Empresa,sSQL,bDerechoCampoConfi,1000 );
end;

function TdmServerConsultas.GetQueryGralTodos(Empresa: OleVariant; const sSQL: WideString): OleVariant;
begin
     // No est� limitado como GetQueryGral()
     // PENDIENTE: A Futuro juntar ambas paletitas para que por parametro se indique si quieren un limite de registros
     Result := oZetaProvider.OpenSQL( Empresa, ConvierteFechas( sSQL ), -1 );
     SetComplete;
end;

function TdmServerConsultas.GetProcessLog(Empresa: OleVariant; Folio: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_BITACORA_PROCESO ), [ Folio ] ), True );
     end;
     SetComplete;
end;

function TdmServerConsultas.GetBitacora(Empresa, Parametros: OleVariant): OleVariant;
var
   sSQL, CampoFecha: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );

          // US #9701: Aplicar mejora de cursores del provider a pantallas de Sistema TRESS con mucho volumen de informaci�n
          OptimizeCursor := TRUE;

          with ParamList do
          begin
               if ParamByName('FechaCaptura').AsBoolean then
                  CampoFecha := 'BI_FECHA'
               else
                  CampoFecha := 'BI_FEC_MOV';


               sSQL := Format( GetSQLScript( Q_BITACORA ),
                             [ CampoFecha,
                               DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDate ),
                               DateToStrSQLC( ParamByName( 'FechaFinal' ).AsDate + 1 ),
                               ParamByName( 'Tipo' ).AsInteger,
                               ParamByName( 'Clase' ).AsInteger,
                               ParamByName( 'Empleado' ).AsInteger,
                               ParamByName( 'Usuario' ).AsInteger,
                               ParamByName( 'NumProceso' ).AsInteger ] );
                {

               sSQL := Format( GetSQLScript( Q_BITACORA ),
                             [ CampoFecha,
                               DateToStrSQL( ParamByName( 'FechaInicial' ).AsDate ),
                               DateToStrSQL( ParamByName( 'FechaFinal' ).AsDate ),
                               ParamByName( 'Tipo' ).AsInteger,
                               ParamByName( 'Tipo' ).AsInteger,
                               ParamByName( 'Clase' ).AsInteger,
                               ParamByName( 'Clase' ).AsInteger,
                               ParamByName( 'Empleado' ).AsInteger,
                               ParamByName( 'Empleado' ).AsInteger,
                               ParamByName( 'Usuario' ).AsInteger,
                               ParamByName( 'Usuario' ).AsInteger,
                               ParamByName( 'NumProceso' ).AsInteger,
                               ParamByName( 'NumProceso' ).AsInteger ] );   }
          end;
          Result := OpenSQL( Empresa, sSQL, True );
     end;
     SetComplete;
end;

function TdmServerConsultas.GetProcessList(Empresa: OleVariant; Status, Proceso, Usuario: Integer;
  FechaInicial, FechaFinal: TDateTime): OleVariant;
var
  sSQL   : String;
  sStatus: String;
begin
     case Status of
                   0: sStatus := StrToList(B_PROCESO_ABIERTO);
                   1: sStatus := StrToList(B_PROCESO_CANCELADO + B_PROCESO_OK + B_PROCESO_ERROR);
                   2: sStatus := StrToList(B_PROCESO_OK);
                   3: sStatus := StrToList(B_PROCESO_ERROR);
                   4: sStatus := StrToList(B_PROCESO_CANCELADO);
                   else
                     sStatus := StrToList(B_PROCESO_ABIERTO + B_PROCESO_CANCELADO + B_PROCESO_OK + B_PROCESO_ERROR);
     end;

	 // US #9701: Aplicar mejora de cursores del provider a pantallas de Sistema TRESS con mucho volumen de informaci�n
     oZetaProvider.OptimizeCursor := TRUE;

     sSQL := Format(GetSQLScript(Q_PROCESO_LISTA), [sStatus, Proceso, Proceso,
         DateToStrSQL(FechaInicial), DateToStrSQL(FechaFinal), Usuario, Usuario]);
     Result := oZetaProvider.OpenSQL(Empresa, sSQL, True);
     SetComplete;
end;

procedure TdmServerConsultas.CancelaProceso(Empresa: OleVariant; Folio: Integer);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          CancelProcess( Folio );
     end;
     SetComplete;
end;

function TdmServerConsultas.GetProcess(Empresa: OleVariant; Folio: Integer): OleVariant;
begin
     Result := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_PROCESO ), [ Folio ] ), True );
     SetComplete;
end;

procedure TdmServerConsultas.SetTablaInfo;
begin
     oZetaProvider.TablaInfo.SetInfo( 'CONTEO', 'CT_NIVEL_1, CT_NIVEL_2, CT_NIVEL_3, CT_NIVEL_4,'+
                                      'CT_NIVEL_5, CT_CUANTOS, CT_REAL, CT_NUMERO1, CT_NUMERO2,'+
                                      'CT_NUMERO3, CT_TEXTO1, CT_TEXTO2, CT_FECHA','CT_FECHA,'+
                                      'CT_NIVEL_1, CT_NIVEL_2, CT_NIVEL_3, CT_NIVEL_4, CT_NIVEL_5' );
end;

function TdmServerConsultas.GetConteo(Empresa: OleVariant): OleVariant;
const
     K_CONTEO = 'select CT_NIVEL_1, CT_NIVEL_2, CT_NIVEL_3, CT_NIVEL_4, CT_NIVEL_5, '+
                'CT_CUANTOS, cast( (CT_REAL) AS INTEGER ) as CT_REAL, CT_NUMERO1, CT_NUMERO2,'+
                'CT_NUMERO3, CT_TEXTO1, CT_TEXTO2, 0 as CT_VACANTES, 0 as CT_EXCEDENTE '+
                'from CONTEO order by CT_NIVEL_1, CT_NIVEL_2, CT_NIVEL_3, CT_NIVEL_4, CT_NIVEL_5';
begin
     oZetaProvider.EmpresaActiva := Empresa;
     with TConteoPersonal.Create(oZetaProvider) do
     begin
          try
             try
                ActualizaConteo;
             except
                on Error: Exception do
                begin
                     Error.Message := CR_LF + 'Error al Recalcular Presupuesto de Personal' +
                                      CR_LF + Error.Message;
                     raise;
                end;
             end;
          finally
             Free;
          end;
     end;
     Result := oZetaProvider.OpenSQL( Empresa, K_CONTEO, True );
     SetComplete;
end;

function TdmServerConsultas.GrabaConteo(Empresa, oDelta: OleVariant; var ErrorCount: Integer;
         out oNewConteo: OleVariant): OleVariant;
begin
     SetTablaInfo;
     Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     oNewConteo := GetConteo( Empresa );
     SetComplete;
end;

function TdmServerConsultas.GetQueryGralLimite(Empresa: OleVariant;const sSQL: WideString; bDerechoConfid: WordBool;LimiteReg: Integer): OleVariant;
var
   sScript,sNivel :string;

    function TieneSoloSelect :Boolean;
    begin
         Result := StrCount(sScript,'SELECT') = 1;
    end;

    function ModificarScript:string;
    var
       iPos :Integer;
       iPosWhere,iFin,iWhere,iAlias :Integer;
       sScriptNext,sScriptAlias,sTabla,sAlias :string;

       procedure FindTablaConfid ;
       begin
            sScript := StrTransAll(sScript,CR_LF,' '+CR_LF+' ');
            sScriptNext := Copy (sScript,iPos,( Length(sScript) - iPos ) );
            iFin := Pos (' ',sScriptNext);
            sScriptAlias := Copy( sScriptNext,iFin+1,( Length(sScriptNext) - iFin ) );
            iAlias := Pos (' ',sScriptAlias);
            sAlias := Copy (sScriptAlias,0, iAlias );
       end;

    begin
         if HayTablasCamposConfidenciales(Empresa,GetSQLScript( Q_TABLAS_CONFI ),'EN_TABLA',sScript,iPos,sTabla)then
         begin
              FindTablaConfid;
              if ( StrLleno( sAlias ) and ( not ( Pos( sAlias,' ON , LEFT , JOIN , GROUP , WHERE , SELECT , FROM , COUNT , RIGHT , OUTER , AND , OR , LIKE , DISTINCT , SUM ') > 0 ) ) )then
              begin
                   sAlias := sAlias+'.';
                   sTabla := sAlias;
              end
              else
                  sTabla := sTabla +'.';

              iWhere := Pos('WHERE',sScript);

              iPosWhere := Pos('GROUP',sScript);
              If iPosWhere > 0 then
              begin
                   iPosWhere := iPosWhere - 1;
              end
              else
              begin
                   iPosWhere := Pos('ORDER',sScript);
                   If iPosWhere > 0 then
                   begin
                        iPosWhere := iPosWhere - 1;
                   end
              end;


              if iWhere = 0 then
              begin

    {$ifdef CONFIDENCIALIDAD_MULTIPLE}
                         if iPosWhere > 0 then
                         begin
                              Insert( Format( ' WHERE ( %sCB_CODIGO in (select CCC.CB_CODIGO from COLABORA CCC WHERE CCC.CB_NIVEL0 IN %s ) ) ' ,[ sTabla,sNivel] ),
                                    sScript,
                                    iPosWhere );
                              Result := sScript;
                         end
                         else
                             Result := sScript +
                                       Format( ' WHERE ( %sCB_CODIGO in (select CCC.CB_CODIGO from COLABORA CCC WHERE CCC.CB_NIVEL0 IN %s ) ) ' ,[ sTabla,sNivel] );

    {$else}
                         if iPosWhere > 0 then
                         begin
                              Insert( Format( ' WHERE ( %sCB_CODIGO in (select CCC.CB_CODIGO from COLABORA CCC WHERE CCC.CB_NIVEL0 = ''%s'' ) ) ' ,[ sTabla,sNivel] ),
                                    sScript,
                                    iPosWhere );
                              Result := sScript;
                         end
                         else
                             Result := sScript +
                                       Format( ' WHERE ( %sCB_CODIGO in (select CCC.CB_CODIGO from COLABORA CCC WHERE CCC.CB_NIVEL0 = ''%s'' ) ) ' ,[ sTabla,sNivel] );
    {$endif}

              end
              else
              begin
                   if iPosWhere > 0 then
                      Insert (')',sScript,iPosWhere - 1)
                   else
                       sScript:= sScript +')';

    {$ifdef CONFIDENCIALIDAD_MULTIPLE}
                   Insert( Format( ' ( %sCB_CODIGO in (select CCC.CB_CODIGO from COLABORA CCC WHERE CCC.CB_NIVEL0 IN %s ) ) AND (' ,[ sTabla,sNivel ]),
                           sScript,
                           iWhere + 5);

    {$else}
                   Insert( Format( ' ( %sCB_CODIGO in (select CCC.CB_CODIGO from COLABORA CCC WHERE CCC.CB_NIVEL0 = ''%s'' ) ) AND (' ,[ sTabla,sNivel ]),
                           sScript,
                           iWhere + 5);
    {$endif}
                   Result := sScript;

              end;
         end
         else
             Result := sScript;
    end;

    function EmpresaConfidencial :Boolean;
    begin
    {$ifdef CONFIDENCIALIDAD_MULTIPLE}
         sNivel := ListaComas2InQueryList( Empresa[ P_NIVEL_0 ] ) ;
    {$else}
         sNivel := Empresa[P_NIVEL_0];
    {$endif}
         Result := sNivel <> VACIO;
    end;

begin
     sScript := UPPERCASE(sSQL);

     if not bDerechoConfid and ExistenCamposConfi( Empresa,sScript )  then
     begin
          DataBaseError('Script Inv�lido: No tiene Derecho a ver campos confidenciales');
     end
     else
     begin
          if ( not bDerechoConfid )and ( Pos('*',sScript) > 0 ) and ( ExistenTablasConCamposConfidenciales(Empresa,sScript))then
          begin
               DataBaseError('Script Inv�lido: No se puede utilizar * con tablas que tienen campos confidenciales');
          end
          else
          if EmpresaConfidencial then
          begin
               if TieneSoloSelect then
                    sScript := ModificarScript
               else
                   DataBaseError( 'Script Inv�lido: Solo puede utilizar un comando SELECT');
          end;
     end;

     Result := oZetaProvider.OpenSQL( Empresa, ConvierteFechas( sScript ), LimiteReg );
     SetComplete;

end;

function TdmServerConsultas.CreateTablerosCompany(Empresa, Parametros: OleVariant): Integer;
const
   Q_GET_TABLEROS = '{CALL CompanyTableros_Create( :CompanytableroID, :CM_CODIGO, :TableroID, :CompanyTableroOrden )}';
var
   FTableros: TZetaCursor;
begin
      with oZetaProvider do
      begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          FTableros:= CreateQuery( Q_GET_TABLEROS );
          try
             EmpiezaTransaccion;
             try
                ParamAsChar( FTableros, 'CM_CODIGO', ParamList.ParamByName('CM_CODIGO').AsString, ZetaCommonClasses.K_ANCHO_CODIGO );
                ParamAsInteger( FTableros, 'TableroID', ParamList.ParamByName('TableroID').AsInteger );
                ParamAsInteger( FTableros, 'CompanyTableroOrden', ParamList.ParamByName('CompanyTableroOrden').AsInteger );

                ParamSalida( FTableros, 'CompanytableroID');
                Ejecuta( FTableros );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        Result := 0;
                        RollBackTransaccion;
                        if ( Pos( 'UNIQUE INDEX', UpperCase( Error.Message ) ) > 0 ) then
                        begin
                        end;
                        raise;
                   end;
             end;
             Result := GetParametro( FTableros, 'CompanytableroID').AsInteger;

          finally
                 FreeAndNil( FTableros );
          end;
      end;
     SetComplete;
end;

function TdmServerConsultas.GetConsultaDashletsTablero(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant;
const
     Q_CONSULTA_DASHLET = 'exec dbo.Tablero_ConsultaDashlets %d, %d';
var
   FConsultaDashlet: TZetaCursor;
begin
     with oZetaProvider do
     begin
         EmpresaActiva := Comparte;
         AsignaParamList( Parametros );
         Result := oZetaProvider.OpenSQL( EmpresaActiva, Format( Q_CONSULTA_DASHLET, [ ParamList.ParamByName('SesionID').AsInteger, ParamList.ParamByName('TableroID').AsInteger ] ), True );
         SetComplete;
         EmpresaActiva := Empresa;
     end;
end;

function TdmServerConsultas.GetListaTableros(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant;
const
     Q_LISTA_TABLEROS = 'exec dbo.Tableros_GetListaTableros %d';
var
   FListaTableros: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          Result := OpenSQL( EmpresaActiva, Format( Q_LISTA_TABLEROS, [ ParamList.ParamByName('SesionID').AsInteger ] ), True );
          SetComplete;
          EmpresaActiva := Empresa;
     end;
end;

function TdmServerConsultas.SetRolTableros(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant;
const
     Q_ROL_TABLEROS = '{CALL RolTableros_Set( :CM_CODIGO, :CompanyTableroID, :RO_CODIGO, :Activo )}';
var
   FRolTableros: TZetaCursor;
begin
      with oZetaProvider do
      begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          FRolTableros:= CreateQuery( Q_ROL_TABLEROS );
          try
             EmpiezaTransaccion;
             try
                ParamAsChar( FRolTableros, 'CM_CODIGO', ParamList.ParamByName('CM_CODIGO').AsString, ZetaCommonClasses.K_ANCHO_CODIGO );
                ParamAsInteger( FRolTableros, 'CompanyTableroID', ParamList.ParamByName('CompanyTableroID').AsInteger );
                ParamAsChar( FRolTableros, 'RO_CODIGO', ParamList.ParamByName('RO_CODIGO').AsString, ZetaCommonClasses.K_ANCHO_CODIGO );
                ParamAsBoolean( FRolTableros, 'Activo', ParamList.ParamByName('Activo').AsBoolean );
                Ejecuta( FRolTableros );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        ErrorCount := 1;
                        RollBackTransaccion;
                        if ( Pos( 'UNIQUE INDEX', UpperCase( Error.Message ) ) > 0 ) then
                        begin
                        end;
                        raise;
                   end;
             end;
             ErrorCount := 0;

          finally
                 FreeAndNil( FRolTableros );
          end;
      end;
     SetComplete;
end;

function TdmServerConsultas.SetSesionGraficas(Empresa, Parametros: OleVariant; var ErrorCount: Integer): Integer;
const
     Q_SET_SESION = 'execute procedure DBO.Sesion_SetValues( :SesionIDActual, :SesionID, :US_CODIGO, :CM_CODIGO, :PE_TIPO, :PE_YEAR, :PE_NUMERO, :FechaActiva)';
var
   FSesion: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          FSesion:= CreateQuery( Q_SET_SESION );
          try
             EmpiezaTransaccion;
             try
                ParamAsInteger( FSesion, 'SesionIDActual', ParamList.ParamByName('SesionIDActual').AsInteger );
                ParamAsInteger( FSesion, 'US_CODIGO', ParamList.ParamByName('US_CODIGO').AsInteger );
                ParamAsChar( FSesion, 'CM_CODIGO', ParamList.ParamByName('CM_CODIGO').AsString, ZetaCommonClasses.K_ANCHO_CODIGO );
                ParamAsInteger( FSesion, 'PE_TIPO', ParamList.ParamByName('PE_TIPO').AsInteger );
                ParamAsInteger( FSesion, 'PE_YEAR', ParamList.ParamByName('PE_YEAR').AsInteger );
                ParamAsInteger( FSesion, 'PE_NUMERO', ParamList.ParamByName('PE_NUMERO').AsInteger );
                ParamAsDate( FSesion, 'FechaActiva', ParamList.ParamByName('FechaActiva').AsDate );

                ParamSalida( FSesion, 'SesionID' );

                Ejecuta( FSesion );
                Result := GetParametro( FSesion, 'SesionID').AsInteger;
                TerminaTransaccion( True );
                ErrorCount := 0;
             except
                   on Error: Exception do
                   begin
                        Result := 0;
                        ErrorCount := 1;
                        RollBackTransaccion;
                        if ( Pos( 'UNIQUE INDEX', UpperCase( Error.Message ) ) > 0 ) then
                        begin
                        end;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FSesion );
          end;
     end;
     SetComplete;
end;

function TdmServerConsultas.EjecutarDashlet(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant;
const
     Q_DASHLET_RUN = 'exec dbo.SP_DASHLET_RUN %d, %d';
var
   FDashletRun: TZetaCursor;
begin

     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          Result := OpenSQL( Empresa, Format( Q_DASHLET_RUN, [ ParamList.ParamByName('SesionID').AsInteger, ParamList.ParamByName('DashletID').AsInteger ] ), True );
          SetComplete;
     end;
end;

function TdmServerConsultas.GetRolTablerosConsulta(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant;
const
     Q_LISTA_TABLEROS = 'exec dbo.RolTableros_Consulta %d, %s, %d, %s, %s';
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          Result := OpenSQL( EmpresaActiva, Format( Q_LISTA_TABLEROS, [
                 ParamList.ParamByName('SesionID').AsInteger,
                 EntreComillas(ParamList.ParamByName('Empresa').AsString),
                 ParamList.ParamByName('Tablero').AsInteger,
                 EntreComillas(ParamList.ParamByName('Rol').AsString),
                 EntreComillas(ParamList.ParamByName('Pista').AsString)
           ] ), True );
          SetComplete;
          EmpresaActiva := Empresa;
     end;
end;

function TdmServerConsultas.GetRolEmpresas(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant;
const
     Q_LISTA_TABLEROS = 'exec dbo.CompanyTableros_ConsultaEmpresasCombo %d ';
     Q_TODOS = ', %s';
var sScript : String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          if ParamList.ParamByName('Todos').AsString = K_GLOBAL_SI then
             sScript := Format( Q_LISTA_TABLEROS, [
                 ParamList.ParamByName('SesionID').AsInteger
                 ] )
          else
              sScript := Format( Q_LISTA_TABLEROS + Q_TODOS, [
                 ParamList.ParamByName('SesionID').AsInteger,
                 EntreComillas(K_GLOBAL_SI)
                 ] );

          Result := OpenSQL( EmpresaActiva, sScript , True );
          SetComplete;
          EmpresaActiva := Empresa;
     end;
end;

function TdmServerConsultas.GetRolTableros(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant;
const
     Q_LISTA_TABLEROS = 'exec dbo.Tableros_ConsultaCombo %d';
     Q_TODOS = ', %s';
var sScript : String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          if ParamList.ParamByName('Todos').AsString = K_GLOBAL_SI then
             sScript := Format( Q_LISTA_TABLEROS, [
                 ParamList.ParamByName('SesionID').AsInteger
                 ] )
          else
              sScript := Format( Q_LISTA_TABLEROS + Q_TODOS, [
                 ParamList.ParamByName('SesionID').AsInteger,
                 EntreComillas(K_GLOBAL_SI)
                 ] );

          Result := OpenSQL( EmpresaActiva, sScript , True );
          SetComplete;
          EmpresaActiva := Empresa;
     end;
end;

function TdmServerConsultas.GrabaRolTablero(Empresa, Parametros: OleVariant; var ErrorCount: Integer): OleVariant;
const
   Q_GET_TABLEROS = '{CALL RolTableros_Set( :CM_CODIGO, :TableroID, :RO_CODIGO, :Activo )}';
var
   FTableros: TZetaCursor;
begin
      with oZetaProvider do
      begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          FTableros:= CreateQuery( Q_GET_TABLEROS );
          try
             EmpiezaTransaccion;
             try
                ParamAsString( FTableros, 'CM_CODIGO', ParamList.ParamByName('Empresa').AsString );
                ParamAsInteger( FTableros, 'TableroID', ParamList.ParamByName('Tablero').AsInteger );
                ParamAsString( FTableros, 'RO_CODIGO', ParamList.ParamByName('Rol').AsString );
                ParamAsString( FTableros, 'Activo', ParamList.ParamByName('Activo').AsString );

                Ejecuta( FTableros );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        Result := 0;
                        RollBackTransaccion;
                        if ( Pos( 'UNIQUE INDEX', UpperCase( Error.Message ) ) > 0 ) then
                        begin
                        end;
                        raise;
                   end;
             end;

             Result := 1;

          finally
                 FreeAndNil( FTableros );
          end;
      end;
     SetComplete;
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerConsultas, Class_dmServerConsultas, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.
