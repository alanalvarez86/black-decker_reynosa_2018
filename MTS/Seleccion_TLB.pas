unit Seleccion_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 4/18/2008 4:23:46 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20\MTS\Seleccion.tlb (1)
// LIBID: {991F7BC1-B293-11D5-9249-0050DA04EAA0}
// LCID: 0
// Helpfile: 
// HelpString: Seleccion Library
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SeleccionMajorVersion = 1;
  SeleccionMinorVersion = 0;

  LIBID_Seleccion: TGUID = '{991F7BC1-B293-11D5-9249-0050DA04EAA0}';

  IID_IdmServerSeleccion: TGUID = '{991F7BC2-B293-11D5-9249-0050DA04EAA0}';
  CLASS_dmServerSeleccion: TGUID = '{991F7BC4-B293-11D5-9249-0050DA04EAA0}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerSeleccion = interface;
  IdmServerSeleccionDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerSeleccion = IdmServerSeleccion;


// *********************************************************************//
// Interface: IdmServerSeleccion
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {991F7BC2-B293-11D5-9249-0050DA04EAA0}
// *********************************************************************//
  IdmServerSeleccion = interface(IAppServer)
    ['{991F7BC2-B293-11D5-9249-0050DA04EAA0}']
    function GetPuestos(Empresa: OleVariant): OleVariant; safecall;
    function GetCondiciones(Empresa: OleVariant): OleVariant; safecall;
    function GetTabla(Empresa: OleVariant; iTabla: Integer): OleVariant; safecall;
    function GetRequiere(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetSolicitud(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetEditRequiere(Empresa: OleVariant; Folio: Integer; out Candidatos: OleVariant; 
                             out Gastos: OleVariant): OleVariant; safecall;
    function GetEditSolicitud(Empresa: OleVariant; Folio: Integer; out Candidatos: OleVariant; 
                              out Examenes: OleVariant; out RefLaboral: OleVariant; 
                              out Documentos: OleVariant): OleVariant; safecall;
    function GetEntrevista(Empresa: OleVariant; SolFolio: Integer; ReqFolio: Integer): OleVariant; safecall;
    function GrabaTabla(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; 
                        out ErrorCount: Integer): OleVariant; safecall;
    function GrabaSolicitud(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                            out Folio: Integer): OleVariant; safecall;
    function BorraSolicitud(Empresa: OleVariant; oDelta: OleVariant; Operacion: Integer; 
                            out ErrorCount: Integer; out ErrorData: OleVariant): OleVariant; safecall;
    function GrabaRequisicion(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                              out Folio: Integer): OleVariant; safecall;
    function BorraRequisicion(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ObtenerCandidatos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GrabaGasto(Empresa: OleVariant; oDelta: OleVariant; iFolioReq: Integer; 
                        out ErrorCount: Integer): OleVariant; safecall;
    function GrabaCandidato(Empresa: OleVariant; oDelta: OleVariant; iFolioReq: Integer; 
                            out iStatusSol: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function Depuracion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetDocumento(Empresa: OleVariant; Folio: Integer; const Tipo: WideString): OleVariant; safecall;
    function GrabaDocumento(Empresa: OleVariant; lReemplaza: WordBool; oDelta: OleVariant; 
                            out ErrorCount: Integer): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerSeleccionDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {991F7BC2-B293-11D5-9249-0050DA04EAA0}
// *********************************************************************//
  IdmServerSeleccionDisp = dispinterface
    ['{991F7BC2-B293-11D5-9249-0050DA04EAA0}']
    function GetPuestos(Empresa: OleVariant): OleVariant; dispid 1;
    function GetCondiciones(Empresa: OleVariant): OleVariant; dispid 2;
    function GetTabla(Empresa: OleVariant; iTabla: Integer): OleVariant; dispid 3;
    function GetRequiere(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 4;
    function GetSolicitud(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 5;
    function GetEditRequiere(Empresa: OleVariant; Folio: Integer; out Candidatos: OleVariant; 
                             out Gastos: OleVariant): OleVariant; dispid 6;
    function GetEditSolicitud(Empresa: OleVariant; Folio: Integer; out Candidatos: OleVariant; 
                              out Examenes: OleVariant; out RefLaboral: OleVariant; 
                              out Documentos: OleVariant): OleVariant; dispid 7;
    function GetEntrevista(Empresa: OleVariant; SolFolio: Integer; ReqFolio: Integer): OleVariant; dispid 8;
    function GrabaTabla(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; 
                        out ErrorCount: Integer): OleVariant; dispid 9;
    function GrabaSolicitud(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                            out Folio: Integer): OleVariant; dispid 10;
    function BorraSolicitud(Empresa: OleVariant; oDelta: OleVariant; Operacion: Integer; 
                            out ErrorCount: Integer; out ErrorData: OleVariant): OleVariant; dispid 11;
    function GrabaRequisicion(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                              out Folio: Integer): OleVariant; dispid 12;
    function BorraRequisicion(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 13;
    function ObtenerCandidatos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 14;
    function GrabaGasto(Empresa: OleVariant; oDelta: OleVariant; iFolioReq: Integer; 
                        out ErrorCount: Integer): OleVariant; dispid 15;
    function GrabaCandidato(Empresa: OleVariant; oDelta: OleVariant; iFolioReq: Integer; 
                            out iStatusSol: Integer; out ErrorCount: Integer): OleVariant; dispid 16;
    function Depuracion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 17;
    function GetDocumento(Empresa: OleVariant; Folio: Integer; const Tipo: WideString): OleVariant; dispid 18;
    function GrabaDocumento(Empresa: OleVariant; lReemplaza: WordBool; oDelta: OleVariant; 
                            out ErrorCount: Integer): OleVariant; dispid 19;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerSeleccion provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerSeleccion exposed by              
// the CoClass dmServerSeleccion. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerSeleccion = class
    class function Create: IdmServerSeleccion;
    class function CreateRemote(const MachineName: string): IdmServerSeleccion;
  end;

implementation

uses ComObj;

class function CodmServerSeleccion.Create: IdmServerSeleccion;
begin
  Result := CreateComObject(CLASS_dmServerSeleccion) as IdmServerSeleccion;
end;

class function CodmServerSeleccion.CreateRemote(const MachineName: string): IdmServerSeleccion;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerSeleccion) as IdmServerSeleccion;
end;

end.
