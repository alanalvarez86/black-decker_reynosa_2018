unit PortalTress_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision: 17$
// File generated on 03/08/2006 08:42:40 a.m. from Type Library described below.

// ************************************************************************ //
// Type Lib: D:\3win_20\MTS\PortalTress.tlb (1)
// IID\LCID: {CEE0510F-5516-4CC6-8143-BD4FF6EAE320}\0
// Helpfile: 
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (3) v4.0 StdVCL, (C:\WINDOWS\system32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL, 
  MIDAS;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  PortalTressMajorVersion = 1;
  PortalTressMinorVersion = 0;

  LIBID_PortalTress: TGUID = '{CEE0510F-5516-4CC6-8143-BD4FF6EAE320}';

  IID_IdmPortalTress: TGUID = '{5F4F1FD8-74F3-44BF-81CD-B2E16CB314A6}';
  CLASS_dmPortalTress: TGUID = '{5A97D224-257B-4588-A6A5-9A4DCCD46698}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmPortalTress = interface;
  IdmPortalTressDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmPortalTress = IdmPortalTress;


// *********************************************************************//
// Interface: IdmPortalTress
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5F4F1FD8-74F3-44BF-81CD-B2E16CB314A6}
// *********************************************************************//
  IdmPortalTress = interface(IAppServer)
    ['{5F4F1FD8-74F3-44BF-81CD-B2E16CB314A6}']
    function  GetEmpleado(const Empresa: WideString; Empleado: Integer; const Archivo: WideString): WideString; safecall;
    function  GetFoto(const Empresa: WideString; Empleado: Integer; const Archivo: WideString): WideString; safecall;
    function  BuscaEmpleados(const Texto: WideString; Tipo: Integer; const Archivo: WideString): WideString; safecall;
    function  SetEmpresas(const Empresas: WideString): Integer; safecall;
    function  GetEmpresas: WideString; safecall;
    function  KioskoEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function  KioskoPrestamos(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function  KioskoAhorros(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function  KioskoTarjeta(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; 
                            out Checadas: OleVariant): OleVariant; safecall;
    function  KioskoNominaInit(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                               Tipo: Integer; out Conceptos: OleVariant): OleVariant; safecall;
    function  KioskoNominaAnterior(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                   Tipo: Integer; Numero: Integer; out Conceptos: OleVariant): OleVariant; safecall;
    function  KioskoNominaSiguiente(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                    Tipo: Integer; Numero: Integer; out Conceptos: OleVariant): OleVariant; safecall;
    function  KioskoPrenominaInit(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                  Tipo: Integer; out Tarjetas: OleVariant): OleVariant; safecall;
    function  KioskoPrenominaAnterior(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                      Tipo: Integer; Numero: Integer; out Tarjetas: OleVariant): OleVariant; safecall;
    function  KioskoPrenominaSiguiente(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                       Tipo: Integer; Numero: Integer; out Tarjetas: OleVariant): OleVariant; safecall;
    function  KioskoComidasInit(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                Tipo: Integer; out Periodo: OleVariant; out Totales: OleVariant): OleVariant; safecall;
    function  KioskoComidasAnterior(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                    Tipo: Integer; Numero: Integer; out Periodo: OleVariant; 
                                    out Totales: OleVariant): OleVariant; safecall;
    function  KioskoComidasSiguiente(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                     Tipo: Integer; Numero: Integer; out Periodo: OleVariant; 
                                     out Totales: OleVariant): OleVariant; safecall;
    function  GetEmpleado2(const Parametros: WideString): WideString; safecall;
    function  KioscoCursosTomados(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function  SetClaveEmpleadoKiosko(Empresa: OleVariant; Empleado: Integer; const Clave: WideString): Integer; safecall;
    function  ReseteaPassword(Empresa: OleVariant; Empleados: OleVariant): Integer; safecall;
    function  EnrolDataGet(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function  EnrolDataSet(Empresa: OleVariant; Empleado: Integer; Valores: OleVariant): Smallint; safecall;
    function  GetServerData: OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmPortalTressDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5F4F1FD8-74F3-44BF-81CD-B2E16CB314A6}
// *********************************************************************//
  IdmPortalTressDisp = dispinterface
    ['{5F4F1FD8-74F3-44BF-81CD-B2E16CB314A6}']
    function  GetEmpleado(const Empresa: WideString; Empleado: Integer; const Archivo: WideString): WideString; dispid 1;
    function  GetFoto(const Empresa: WideString; Empleado: Integer; const Archivo: WideString): WideString; dispid 2;
    function  BuscaEmpleados(const Texto: WideString; Tipo: Integer; const Archivo: WideString): WideString; dispid 3;
    function  SetEmpresas(const Empresas: WideString): Integer; dispid 4;
    function  GetEmpresas: WideString; dispid 5;
    function  KioskoEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 6;
    function  KioskoPrestamos(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 7;
    function  KioskoAhorros(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 8;
    function  KioskoTarjeta(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; 
                            out Checadas: OleVariant): OleVariant; dispid 9;
    function  KioskoNominaInit(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                               Tipo: Integer; out Conceptos: OleVariant): OleVariant; dispid 10;
    function  KioskoNominaAnterior(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                   Tipo: Integer; Numero: Integer; out Conceptos: OleVariant): OleVariant; dispid 11;
    function  KioskoNominaSiguiente(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                    Tipo: Integer; Numero: Integer; out Conceptos: OleVariant): OleVariant; dispid 12;
    function  KioskoPrenominaInit(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                  Tipo: Integer; out Tarjetas: OleVariant): OleVariant; dispid 13;
    function  KioskoPrenominaAnterior(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                      Tipo: Integer; Numero: Integer; out Tarjetas: OleVariant): OleVariant; dispid 14;
    function  KioskoPrenominaSiguiente(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                       Tipo: Integer; Numero: Integer; out Tarjetas: OleVariant): OleVariant; dispid 15;
    function  KioskoComidasInit(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                Tipo: Integer; out Periodo: OleVariant; out Totales: OleVariant): OleVariant; dispid 16;
    function  KioskoComidasAnterior(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                    Tipo: Integer; Numero: Integer; out Periodo: OleVariant; 
                                    out Totales: OleVariant): OleVariant; dispid 17;
    function  KioskoComidasSiguiente(Empresa: OleVariant; Empleado: Integer; Year: Integer; 
                                     Tipo: Integer; Numero: Integer; out Periodo: OleVariant; 
                                     out Totales: OleVariant): OleVariant; dispid 18;
    function  GetEmpleado2(const Parametros: WideString): WideString; dispid 19;
    function  KioscoCursosTomados(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 20;
    function  SetClaveEmpleadoKiosko(Empresa: OleVariant; Empleado: Integer; const Clave: WideString): Integer; dispid 21;
    function  ReseteaPassword(Empresa: OleVariant; Empleados: OleVariant): Integer; dispid 301;
    function  EnrolDataGet(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 302;
    function  EnrolDataSet(Empresa: OleVariant; Empleado: Integer; Valores: OleVariant): Smallint; dispid 303;
    function  GetServerData: OleVariant; dispid 22;
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                              MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                            Options: Integer; const CommandText: WideString; 
                            var Params: OleVariant; var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmPortalTress provides a Create and CreateRemote method to          
// create instances of the default interface IdmPortalTress exposed by              
// the CoClass dmPortalTress. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmPortalTress = class
    class function Create: IdmPortalTress;
    class function CreateRemote(const MachineName: string): IdmPortalTress;
  end;

implementation

uses ComObj;

class function CodmPortalTress.Create: IdmPortalTress;
begin
  Result := CreateComObject(CLASS_dmPortalTress) as IdmPortalTress;
end;

class function CodmPortalTress.CreateRemote(const MachineName: string): IdmPortalTress;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmPortalTress) as IdmPortalTress;
end;

end.
