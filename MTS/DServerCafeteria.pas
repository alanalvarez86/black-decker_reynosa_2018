unit DServerCafeteria;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DServerCafeteria.pas                       ::
  :: Descripci�n: Programa principal de Cafeteria.dll        ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, DataBkr, DBClient, MtsRdm, Mtx, Db,
     MaskUtils, ZetaCommonLists,
{$ifndef DOS_CAPAS}
     Cafeteria_TLB,
{$endif}
     ZCreator,
     DZetaServerProvider,
     ZetaSQLBroker,   
     ZetaServerDataSet;

{$define MULT_TIPO_COMIDA}  //Permitir multiples registros de cafeteria de diferente tipo en un mismo minuto
type
  eTipoCafeteria = ( ecCafDiarias,
                     ecCafInvita,
                     ecCafPeriodo,
                     ecComidas,
                     ecCafPeriodoTotales,
                     ecComiGrupalesCheca,
                     ecEscribeComiGrupalesEmp,
                     ecEscribeComiGrupalesInv,
                     ecAutorizaComidas,
                     ecAutorizaInvitacion,
                     ecCorrigeComidas,
                     ecCorrigeInvitaciones,
                     ecCaseta );


  TdmServerCafeteria = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerCafeteria {$endif} )
    cdsLista: TServerDataSet;
    cdsLog: TServerDataSet;
    procedure dmServerCafeteriaCreate(Sender: TObject);
    procedure dmServerCafeteriaDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
{$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
{$endif}
    FCursor3: TZetaCursor;
    FCursor4: TZetaCursor;
    FListaParametros: string;
    function GetSQLBroker: TSQLBroker;
    function GetScript( const eTipo: eTipoCafeteria ): String;
    procedure ClearBroker;
    procedure InitBroker;
    procedure InitCreator;
    procedure InitQueries;
    procedure InitRitmos;
    procedure QueriesStatusActBegin;
    procedure QueriesStatusActEnd;
    procedure CheckEmpleadoBaja( const iEmpleado: Integer; const dFecha: TDate );
    procedure SetTablaInfo(const eTabla: eTipoCafeteria );
    procedure BeforeUpdateComidas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure AfterUpdateComidas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    {$ifndef DOS_CAPAS}
    procedure ServerActivate(Sender: TObject);
    procedure ServerDeactivate(Sender: TObject);
    {$endif}
    function ComidasGrupalesDataset( Dataset: TDataset ): OleVariant;
    function ComidasGrupalesASCII( const Datos: OleVariant; const sCampo: String ): Integer;
    function AutorizaEmpleado( iEmpleado: Integer; dFecha: TDateTime; sHora, sCredencial, sTipoComida: String; iNumComidas: Integer;var sDatos: String ): Boolean;
    function AutorizaInvitacion( const iEmpleado: Integer; dFecha: TDateTime; sHora, sTipoComida: String; const iNumComidas: Integer; var sDatos: String ): Boolean;
    function CorregirFechasCafeDataset: OleVariant;
    //procedure InitLog;
    procedure ComidasGrupalesParametros;
    procedure ComidasGrupalesBuildDataset;
    procedure CorregirFechasCafeBuildDataset;
    procedure CorregirFechasCafeParametros;
    function ChecaCambio(const sCampo: String; oCampo: TField; var sNota: String): Boolean;
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    property SQLBroker: TSQLBroker read GetSQLBroker;
{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetCafDiarias(Empresa: OleVariant; Empleado: Integer; dFecha: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCafInvita(Empresa: OleVariant; Invitador: Integer; dFecha: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCafPeriodo(Empresa: OleVariant; Empleado: Integer; dFechaIni, dFechaFin: TDateTime; out Totales: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetComidas(Empresa: OleVariant; Fecha: TDateTime; lSoloAltas: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaComidas(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCafDiarias(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCafInvita(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ComidasGrupales(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ComidasGrupalesGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ComidasGrupalesLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CorregirFechasCafe(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCasDiarias(Empresa: OleVariant; Empleado: Integer; dFecha: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCasDiarias(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCaseta(Empresa: OleVariant; Fecha: TDateTime; lSoloAltas: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCaseta(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
  end;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaTipoEntidad,
     ZetaServerTools,
     ZGlobalTress,
     ZetaQRExpr,
     ZFuncsGlobal,
     Mask,
     DSuperASCII,
     DQueries;

{$R *.DFM}
const
     K_ANCHO_HORA = 4;
     K_ANCHO_TIPO_COMIDA = 1;
     K_POST_ERROR_INV = 'Invitador: %d ' + CR_LF +
                        'Fecha: %s '+ CR_LF +
                        'Hora: %s '+ CR_LF +
                        'Tipo: %s '+ CR_LF +
                        'Comidas: %d ' + CR_LF +
                        'Error: %s ';
     K_POST_ERROR_EMP = 'Empleado: %d ' + CR_LF +
                        'Fecha: %s '+ CR_LF +
                        'Hora: %s '+ CR_LF +
                        'Tipo: %s '+ CR_LF +
                        'Comidas: %d '+ CR_LF +
                        'Letra Credencial: %s  ' + CR_LF +
                        'Error: %s ';

     aTipoRegistro: array[ TUpdateKind ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Modific�','Agreg�','Borr�');

{----------------------------------------------------------------}

class procedure TdmServerCafeteria.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerCafeteria.dmServerCafeteriaCreate(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     OnActivate := ServerActivate;
     OnDeactivate := ServerDeactivate;
     {$endif}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerCafeteria.dmServerCafeteriaDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

function TdmServerCafeteria.GetSQLBroker: TSQLBroker;
begin
{$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
{$else}
     Result := Self.oZetaSQLBroker;
{$endif}
end;

{$ifdef DOS_CAPAS}
procedure TdmServerCafeteria.CierraEmpresa;
begin
end;
{$else}
procedure TdmServerCafeteria.ServerActivate(Sender: TObject);
begin
     oZetaProvider.Activate;
end;

procedure TdmServerCafeteria.ServerDeactivate(Sender: TObject);
begin
     oZetaProvider.Deactivate;
end;
{$endif}

{ ***************** PARTE PRIVADA *******************************}

function TdmServerCafeteria.GetScript( const eTipo: eTipoCafeteria ): String;
begin
     case eTipo of
          ecComidas:
          Result := 'select A.CB_CODIGO, A.CF_FECHA, A.CF_HORA, A.CF_TIPO, A.CF_COMIDAS, ' +
                    'A.CF_EXTRAS, A.CF_RELOJ, A.US_CODIGO, A.CL_CODIGO, A.CF_REG_EXT, ' +
                    'A.CF_COMIDAS + A.CF_EXTRAS as Comidas, ' +
                    K_PRETTYNAME + ' as PrettyName '+
                    'from CAF_COME A ' +
                    'left outer join COLABORA B on ( A.CB_CODIGO = B.CB_CODIGO ) ' +
                    'where ( A.CF_FECHA = ''%s'' ) %s ' +
                    'order by A.CB_CODIGO';
          ecCafPeriodoTotales:
          Result  := 'select CF_TIPO, SUM( CF_COMIDAS ) as CF_COMIDAS, SUM( CF_EXTRAS ) as CF_EXTRAS ' +
                     'from CAF_COME where ( CB_CODIGO = %d ) and ( CF_FECHA between ''%s'' and ''%s'' ) ' +
                     'group by CF_TIPO order by CF_TIPO';
          ecComiGrupalesCheca: Result := 'select CB_CODIGO, '+ K_PRETTYNAME + ' as PrettyName from COLABORA where ( CB_CODIGO = :Empleado )';
          ecEscribeComiGrupalesEmp:
          Result := 'insert into CAF_COME ( CB_CODIGO, '+
                                           'CF_FECHA, '+
                                           'CF_HORA, '+
                                           'CF_TIPO, '+
                                           'CF_RELOJ, '+
                                           'CF_COMIDAS )values( :CB_CODIGO, '+
                                                               ':CF_FECHA, '+
                                                               ':CF_HORA, '+
                                                               ':CF_TIPO, '+
                                                               ':CF_RELOJ, '+
                                                               ':CF_COMIDAS )';
          ecEscribeComiGrupalesInv:
          Result := 'insert into CAF_INV ( IV_CODIGO, '+
                                           'CF_FECHA, '+
                                           'CF_HORA, '+
                                           'CF_TIPO, '+
                                           'CF_RELOJ, '+
                                           'CF_COMIDAS )values( :CB_CODIGO, '+
                                                               ':CF_FECHA, '+
                                                               ':CF_HORA, '+
                                                               ':CF_TIPO, '+
                                                               ':CF_RELOJ, '+
                                                               ':CF_COMIDAS )';
          {$ifdef MULT_TIPO_COMIDA}
          ecAutorizaComidas: Result := 'select AUTORIZACION, FECHABAJA from SP_CAFE_AUTORIZA_COMIDA( :EMPLEADO, :FECHA, :HORA, :CREDENCIAL, :TIPOCOMIDA )';
          {$else}
          ecAutorizaComidas: Result := 'select AUTORIZACION, FECHABAJA from SP_CAFE_AUTORIZA_COMIDA( :EMPLEADO, :FECHA, :HORA, :CREDENCIAL )';
          {$endif}
          ecAutorizaInvitacion: Result := 'select AUTORIZACION, EMPLEADO from SP_CAFE_AUTORIZA_INVITACION( :INVITADOR, :FECHA, :HORA, :TIPOCOMIDA, :NUMCOMIDAS )';
          {$ifdef MULT_TIPO_COMIDA}
          ecCorrigeComidas: Result := 'update CAF_COME set CF_FECHA = %s where ( CB_CODIGO = :CB_CODIGO ) '+
                                      'and ( CF_FECHA = :CF_FECHA ) and ( CF_HORA = :CF_HORA ) and ( CF_TIPO = :CF_TIPO )';
          {$else}
          ecCorrigeComidas: Result := 'update CAF_COME set CF_FECHA = %s where ( CB_CODIGO = :CB_CODIGO ) '+
                                      'and ( CF_FECHA = :CF_FECHA ) and ( CF_HORA = :CF_HORA )';
          {$endif}
          ecCorrigeInvitaciones: Result := 'update CAF_INV set CF_FECHA = %s where ( CF_FECHA = %s )';
          ecCaseta:
          Result := 'select A.CB_CODIGO, A.AL_FECHA, A.AL_HORA, AL_ENTRADA, AL_OK_SIST, ' +
                    ' AL_OK_MAN, AL_OK_FEC,AL_OK_HOR,AL_COMENT, AE_CODIGO, AL_CASETA, A.US_CODIGO,' +
                    K_PRETTYNAME + ' as PrettyName '+
                    'from ACCESLOG A ' +
                    'left outer join COLABORA B on ( A.CB_CODIGO = B.CB_CODIGO ) ' +
                    'where ( A.AL_FECHA = ''%s'' ) %s ' +
                    'order by A.CB_CODIGO';          
     end;
end;

procedure TdmServerCafeteria.SetTablaInfo( const eTabla: ETipoCafeteria );
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTabla of
               ecCafDiarias, ecCafPeriodo: SetInfo( 'CAF_COME', 'CB_CODIGO,CF_FECHA,CF_HORA,CF_TIPO,CF_COMIDAS,CF_EXTRAS,CF_RELOJ,US_CODIGO,CL_CODIGO,CF_REG_EXT', 'CB_CODIGO,CF_FECHA,CF_HORA'{$ifdef MULT_TIPO_COMIDA}+',CF_TIPO'{$endif} );
               ecComidas: SetInfo( 'CAF_COME', 'CB_CODIGO,CF_FECHA,CF_HORA,CF_TIPO,CF_COMIDAS,CF_EXTRAS,CF_RELOJ,US_CODIGO,CL_CODIGO,CF_REG_EXT', 'CB_CODIGO,CF_FECHA,CF_HORA'{$ifdef MULT_TIPO_COMIDA}+',CF_TIPO'{$endif} );
               ecCafInvita: SetInfo( 'CAF_INV', 'IV_CODIGO,CF_FECHA,CF_HORA,CF_TIPO,CF_COMIDAS,CF_EXTRAS,CF_RELOJ,US_CODIGO,CL_CODIGO,CF_REG_EXT','IV_CODIGO,CF_FECHA,CF_HORA'{$ifdef MULT_TIPO_COMIDA}+',CF_TIPO'{$endif} );
               ecCaseta: SetInfo( 'ACCESLOG', 'CB_CODIGO,AL_FECHA,AL_HORA,AL_ENTRADA,AL_OK_SIST,AL_OK_MAN,AL_OK_FEC,AL_OK_HOR,AL_COMENT,AE_CODIGO, AL_CASETA,US_CODIGO', 'CB_CODIGO,AL_FECHA,AL_HORA' ); {acl}
          end
     end;
end;

procedure TdmServerCafeteria.InitBroker;
begin
     InitCreator;
     oZetaCreator.RegistraFunciones([efComunes]);
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TdmServerCafeteria.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oZetaSQLBroker );
end;

procedure TdmServerCafeteria.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerCafeteria.InitQueries;
begin
     InitCreator;
     oZetaCreator.PreparaQueries;
end;

procedure TdmServerCafeteria.InitRitmos;
begin
     InitQueries; { Llama a InitCreator }
     oZetaCreator.PreparaRitmos;
end;

procedure TdmServerCafeteria.QueriesStatusActBegin;
begin
     InitQueries;
     oZetaCreator.Queries.GetStatusActEmpleadoBegin;
end;

procedure TdmServerCafeteria.QueriesStatusActEnd;
begin
     oZetaCreator.Queries.GetStatusActEmpleadoEnd;
end;

procedure TdmServerCafeteria.CheckEmpleadoBaja( const iEmpleado: Integer; const dFecha: TDate );
var
   StatusEmp : eStatusEmpleado;
begin
     StatusEmp := oZetaCreator.Queries.GetStatusActEmpleado( iEmpleado, dFecha );
     if ( StatusEmp <> steEmpleado ) then
        DataBaseError( ZetaServerTools.GetMsgStatusEmpleado( iEmpleado, dFecha, StatusEmp ) );
end;

{ **************** Interfases ( Paletitas ) ************ }

function TdmServerCafeteria.GetCafDiarias(Empresa: OleVariant; Empleado: Integer; dFecha: TDateTime): OleVariant;
const
     K_FILTRO = '( ( CB_CODIGO = %d ) and ( CF_FECHA = ''%s'' ) )';
begin
     SetTablaInfo( ecCafDiarias );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ Empleado, DateToStrSQL( dFecha ) ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCafeteria.GetCafInvita(Empresa: OleVariant; Invitador: Integer; dFecha: TDateTime): OleVariant;
const
     K_FILTRO = '( ( IV_CODIGO = %d ) and ( CF_FECHA = ''%s'' ) )';
begin
     SetTablaInfo( ecCafInvita );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ Invitador, DateToStrSQL( dFecha ) ] );
          Result := GetTabla(Empresa);
     end;
     SetComplete;
end;

function TdmServerCafeteria.GetCafPeriodo(Empresa: OleVariant; Empleado: Integer; dFechaIni, dFechaFin: TDateTime; out Totales: OleVariant): OleVariant;
const
     K_FILTRO = '( ( CB_CODIGO = %d ) and ( CF_FECHA between ''%s'' and ''%s'' ) )';
var
   sScript: String;
begin
     SetTablaInfo( ecCafPeriodo );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ Empleado, DateToStrSQL( dFechaIni ), DateToStrSQL( dFechaFin ) ] );
          Result := GetTabla(Empresa);
          { Aprovechando el viaje, se trae los Totales agrupados por Tipo }
          sScript := Format( GetScript( ecCafPeriodoTotales ), [ Empleado, DateToStrSQL( dFechaIni ), DateToStrSQL( dFechaFin ) ] );
          Totales := OpenSQL( Empresa, sScript, TRUE );
     end;
     SetComplete;
end;

function TdmServerCafeteria.GetComidas(Empresa: OleVariant; Fecha: TDateTime; lSoloAltas: WordBool): OleVariant;
var
   sScript: String;
begin
     sScript := Format( GetScript( ecComidas ), [ ZetaCommonTools.DateToStrSQL( Fecha ), Nivel0( Empresa ) ] );
     Result := oZetaProvider.OpenSQL( Empresa, sScript, not lSoloAltas );
     SetComplete;
end;

function TdmServerCafeteria.GrabaComidas(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( ecComidas );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.BeforeUpdateRecord := BeforeUpdateComidas;
          TablaInfo.AfterUpdateRecord := AfterUpdateComidas;
     end;
     QueriesStatusActBegin;
     try
        Result := oZetaProvider.GrabaTablaGrid( Empresa, oDelta, ErrorCount );
     finally
            QueriesStatusActEnd;
     end;
     SetComplete;
end;

function TdmServerCafeteria.GrabaCafDiarias(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( ecCafDiarias );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.BeforeUpdateRecord := BeforeUpdateComidas;
          TablaInfo.AfterUpdateRecord := AfterUpdateComidas;
     end;
     QueriesStatusActBegin;
     try
        Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     finally
            QueriesStatusActEnd;
     end;
     SetComplete;
end;

function TdmServerCafeteria.GrabaCafInvita(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( ecCafInvita );
     Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     SetComplete;
end;

procedure TdmServerCafeteria.BeforeUpdateComidas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if UpdateKind in [ ukModify, ukInsert ] then
     begin
          with DeltaDS do
          begin
               CheckEmpleadoBaja( CampoAsVar( FieldByName( 'CB_CODIGO' ) ),
                                  CampoAsVar( FieldByName( 'CF_FECHA' ) ) );
          end;
     end;
end;

procedure TdmServerCafeteria.AfterUpdateComidas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   sNota: String;
   BITACORA_COMIDA : string;

   procedure EnlistaCambios;
   begin
        sNota:= VACIO;
        with DeltaDS do
        begin
             ChecaCambio( 'Hora', FieldByName( 'CF_HORA' ), sNota );
             ChecaCambio( 'Tipo', FieldByName( 'CF_TIPO' ), sNota );
             ChecaCambio( 'Comidas', FieldByName( 'CF_COMIDAS' ), sNota );
             ChecaCambio( 'Extras', FieldByName( 'CF_EXTRAS' ), sNota );
             ChecaCambio( 'Reloj', FieldByName( 'CF_RELOJ' ), sNota );
             ChecaCambio( 'Regla', FieldByName( 'CL_CODIGO' ), sNota );
             ChecaCambio( 'T. Extra', FieldByName( 'CF_REG_EXT' ), sNota );
        end;
   end;

begin
     BITACORA_COMIDA := '%s consumos de cafeteria';

     with DeltaDS do
     begin
          sNota := ' ';   // Para que no de problemas al insertar campo Blob
          if UpdateKind <> ukInsert then
               EnlistaCambios;

          with oZetaProvider do
          begin
               if UpdateKind = ukDelete then
                   BorraCatalogo( 'consumos de cafeteria', clbCafeHistorial, DeltaDS, ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ) )
               else if UpdateKind = ukModify then
                    EscribeBitacora( tbNormal, clbCafeHistorial, CampoAsVar( FieldByName( 'CB_CODIGO' )), CampoAsVar( FieldByName( 'CF_FECHA' ) ), Format( BITACORA_COMIDA, [ aTipoRegistro[ UpdateKind ] ]), sNota )
          end;
     end;
end;

function TdmServerCafeteria.ChecaCambio( const sCampo: String; oCampo: TField; var sNota: String ): Boolean;
begin
     Result := CambiaCampo( oCampo );
     if Result then
        sNota := sNota + sCampo + ': ' + CampoTexto( oCampo.OldValue, oCampo.DataType ) +
                 ' -> ' + CampoTexto( oCampo.NewValue, oCampo.DataType ) + CR_LF;
end;


{WizEmpComidasGrupales}
procedure TdmServerCafeteria.ComidasGrupalesParametros;
var
   lGenerar: Boolean;
begin
     with oZetaProvider.ParamList do
     begin
          lGenerar := ParamByName( 'GenerarChecadas' ).AsBoolean;
          FListaParametros := VACIO;
          FListaParametros :=  ConcatString( FListaParametros, 'Generar Checadas: ' + BoolToSiNo( lGenerar ), K_PIPE );
          if( lGenerar )then
          begin
               FListaParametros :=  ConcatString( FListaParametros, 'Fecha: ' + FechaCorta( ParamByName( 'Fecha' ).AsDateTime ), K_PIPE );
               FListaParametros :=  ConcatString( FListaParametros, 'Hora: ' + FormatMaskText( '99:99;0', ParamByName( 'Hora' ).AsString ), K_PIPE );
               FListaParametros :=  ConcatString( FListaParametros, 'Tipo de Comida: ' + IntToStr( ParamByName( 'TipoComida' ).AsInteger ), K_PIPE );
               FListaParametros :=  ConcatString( FListaParametros, 'Cantidad: ' + InttoStr( ParamByName( 'Cantidad' ).AsInteger ), K_PIPE );
               if ParamByName( 'Reloj' ).AsString <> VACIO then
                  FListaParametros :=  ConcatString( FListaParametros, 'Estaci�n: ' + ParamByName( 'Reloj' ).AsString, K_PIPE )
          end
          else
          begin
               if strLleno( ParamByName( 'Archivo' ).AsString ) then
                  FListaParametros :=  ConcatString( FListaParametros, 'Archivo: ' + ParamByName( 'Archivo' ).AsString, K_PIPE ); 
          end;
     end;
end;

function TdmServerCafeteria.ComidasGrupales(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          ComidasGrupalesParametros;
          InitBroker;
          try
             ComidasGrupalesBuildDataset;
             Result := ComidasGrupalesDataset( SQLBroker.SuperReporte.DataSetReporte );
          finally
                 ClearBroker;
          end;
     end;
     SetComplete;
end;

function TdmServerCafeteria.ComidasGrupalesLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
var
   lGeneraChecada: Boolean;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          lGeneraChecada := ParamList.ParamByName('GenerarChecadas').AsBoolean;
     end;
     ComidasGrupalesParametros;
     if( lGeneraChecada )then
         cdsLista.Lista := Lista
     else
         ComidasGrupalesASCII( Lista, VACIO );
     Result := ComidasGrupalesDataset( cdsLista );
     SetComplete;
end;

function TdmServerCafeteria.ComidasGrupalesGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        ComidasGrupalesBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

procedure TdmServerCafeteria.ComidasGrupalesBuildDataset;
const
     K_FILTRO_ACTIVO = '( STATUS_ACTIVO( %s ) = %d )';
var
   dFecha: TDateTime;
   sHora, sFecha: String;
   iTipoComida, iCantidad: Integer;
begin
     with oZetaProvider.ParamList do
     begin
               dFecha := ParamByName( 'Fecha' ).AsDateTime;
               sHora := ParamByName( 'Hora' ).AsString;
               iTipoComida := ParamByName( 'TipoComida' ).AsInteger;
               iCantidad := ParamByName( 'Cantidad' ).AsInteger;
     end;
     sFecha := FechaAsStr( dFecha );
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', True, Entidad, tgTexto, 9, 'CB_CODIGO' );
               AgregaColumna( K_PRETTYNAME, TRUE, Entidad, tgTexto, 35, 'PRETTYNAME' );
               AgregaColumna( Format( 'CTOD( %s )', [ EntreComillas( FechaAsStr( dFecha ) ) ] ), FALSE, enNinguno, tgFecha, 0, 'CF_FECHA' );
               AgregaColumna( sHora, TRUE, Entidad, tgTexto, K_ANCHO_HORA, 'CF_HORA' );
               AgregaColumna( InttoStr(iTipoComida), TRUE, Entidad, tgNumero, 0, 'CF_TIPO' );
               AgregaColumna( InttoStr(iCantidad), TRUE, Entidad, tgNumero, 0, 'CF_COMIDAS' );
               AgregaColumna( 'CB_CREDENC', True, Entidad, tgTexto, 1, 'CB_CREDENC' );
               AgregaColumna( '0000', True, Entidad, tgTexto, 4, 'RELOJ' );
               AgregaFiltro( Format( K_FILTRO_ACTIVO, [ EntreComillas( sFecha ), Ord( steEmpleado ) ] ), FALSE, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

function TdmServerCafeteria.ComidasGrupalesDataset( Dataset: TDataset ): OleVariant;
const
     K_INVITACION_PREFIX = 'IV';
     K_DIGIT_EMPRESA = 'Digito Empresa: %s ' +  CR_LF +
                       'Digito Empresa Empleado: %s ';
     K_ERROR_REGISTRO = 'Error Al Registrar Comidas Grupales';
var
   sDatos, sMensaje, sHora, sCredencial: String;
   FDatasetEmp, FDataSetInv: TZetaCursor;
   iMes, iDia, iEmpleado, iInvitador, iPos, iCantidad: Integer;
   { Compatibilidad con el evento determinadia
   dFecComida: TDateTime;}
   dFecComida: TDate;
   lGeneraChecadas, lDigitoEmpresa: Boolean;
   sDigitoEmpresa, sMensajeInv, sEmpleado, sInvitador, sReloj: String;
   sTipoComida: String;
   oDatosTurno: TEmpleadoDatosTurno;

   function GetDigitoEmpresa( const EmpresaActual: OleVariant): String;
   var
      oGetGlobal: TGetGlobal;
      oEvaluador: TQREvaluator;
   begin
        oEvaluador := TQREvaluator.Create( );
        oGetGlobal := TGetGlobal.Create( oEvaluador );
        try
           Result := oGetGlobal.GetDigitoEmpresa( EmpresaActual[P_CODIGO] );
        finally
               FreeAndNil( oGetGlobal );
               FreeAndNil( oEvaluador );
        end;
   end;

   procedure CargaHoraFecha;
   begin
        with oZetaProvider do
        begin
             with DataSet do
             begin
                  lDigitoEmpresa := ( sDigitoEmpresa = FieldByName('ID_EMPRESA').AsString );
                  iMes := StrtoInt( Copy(FieldByName( 'CF_FECHA' ).AsString,0,2) );
                  iDia := StrToInt( Copy(FieldByName( 'CF_FECHA' ).AsString,3,2) );
                  dFecComida := EncodeDate( theYear(Date), iMes, iDia );
                  sHora := FieldByName('CF_HORA').AsString;
             end;
        end;
   end;

   procedure GrabaEmpleado( iEmpleado: Integer );
   var
      dFechaAjuste: TDate;
      sHoraAjuste: String;
   begin
        { Se hace el ritmosbegin por cada empleado debido a que cuando se importa x archivo pueden venir fechas variables }
        oZetaCreator.Ritmos.RitmosBegin(dFecComida, dFecComida);
        try
           with oZetaProvider do
           begin
                oDatosTurno := oZetaCreator.Ritmos.GetEmpleadoDatosTurno( iEmpleado, dFecComida );
                dFechaAjuste:= dFecComida;
                sHoraAjuste:= sHora;
                with DataSet do
                begin
                     { Defecto #1087: Si tiene turno y horario convierte la fecha y hora }
                     if StrLleno( oDatosTurno.Codigo ) and StrLleno( oDatosTurno.StatusHorario.Horario ) then
                        oZetaCreator.Ritmos.DeterminaDia( iEmpleado, dFechaAjuste, sHoraAjuste, oDatosTurno );
                     ParamAsInteger( FDatasetEmp, 'CB_CODIGO', iEmpleado );
                     ParamAsDate( FDatasetEmp, 'CF_FECHA', dFechaAjuste );
                     ParamAsChar( FDatasetEmp, 'CF_HORA', sHoraAjuste, K_ANCHO_HORA );
                     ParamAsChar( FDatasetEmp, 'CF_TIPO', FieldByName( 'CF_TIPO' ).AsString, K_ANCHO_TIPO_COMIDA );
                     ParamAsInteger( FDatasetEmp, 'CF_COMIDAS', FieldByName( 'CF_COMIDAS' ).AsInteger );
                     ParamAsChar( FDatasetEmp, 'CF_RELOJ', sReloj, 4 );
                     try
                        Ejecuta( FDatasetEmp );
                     except
                           on Error: Exception do
                           begin
                                sDatos := Format( K_POST_ERROR_EMP, [ iEmpleado,
                                                                      FechaCorta( dFechaAjuste ),
                                                                      sHoraAjuste,
                                                                      FieldByName( 'CF_TIPO' ).AsString,
                                                                      FieldByName( 'CF_COMIDAS' ).AsInteger,
                                                                      FieldByName( 'CB_CREDENC').AsString,
                                                                      FieldByName( 'RELOJ').AsString,
                                                                      VACIO ] );
                                Log.Excepcion( iEmpleado, K_ERROR_REGISTRO, Error, sDatos );
                           end;
                     end;
                end;
           end;
        finally
               oZetaCreator.Ritmos.RitmosEnd;
        end;
   end;

   procedure GrabaInvitador( iInvitador: Integer );
   begin
        with oZetaProvider do
        begin
             with DataSet do
             begin
                  ParamAsInteger( FDatasetInv, 'CB_CODIGO', iInvitador );
                  ParamAsDate( FDatasetInv, 'CF_FECHA', dFecComida );
                  ParamAsChar( FDatasetInv, 'CF_HORA', sHora, K_ANCHO_HORA );
                  ParamAsChar( FDatasetInv, 'CF_TIPO', FieldByName( 'CF_TIPO' ).AsString, K_ANCHO_TIPO_COMIDA );
                  ParamAsInteger( FDatasetInv, 'CF_COMIDAS', FieldByName( 'CF_COMIDAS' ).AsInteger );
                  ParamAsChar( FDatasetInv, 'CF_RELOJ', sReloj, 4 );
                  try
                     Ejecuta( FDatasetInv );
                  except
                        on Error: Exception do
                        begin
                             sDatos := Format( K_POST_ERROR_INV, [ iInvitador,
                                                                   FechaCorta( dFecComida ),
                                                                   sHora,
                                                                   FieldByName( 'CF_TIPO' ).AsString,
                                                                   FieldByName( 'CF_COMIDAS' ).AsInteger,
                                                                   FieldByName( 'CB_CREDENC').AsString,
                                                                   FieldByName( 'RELOJ').AsString,
                                                                   VACIO ] );

                             Log.Excepcion( 0, K_ERROR_REGISTRO, Error, sDatos );
                        end;
                  end;
             end;
        end;
   end;

begin
     with oZetaProvider do
     begin
          lGeneraChecadas := ParamList.ParamByName('GenerarChecadas').AsBoolean;
          InitGlobales;
          InitRitmos;

          if OpenProcess( prCAFEComidasGrupales, Dataset.RecordCount, FListaParametros ) then
          begin
               FCursor3 := CreateQuery( GetScript( ecAutorizaComidas ) );
               FCursor4 := CreateQuery( GetScript( ecAutorizaInvitacion ) );
               FDatasetEmp := CreateQuery( GetScript( ecEscribeComiGrupalesEmp ) );
               FDatasetInv := CreateQuery( GetScript( ecEscribeComiGrupalesInv ) );
               EmpiezaTransaccion;
               try
                  with DataSet do
                  begin
                       sEmpleado := FieldByName('CB_CODIGO').AsString;
                       iEmpleado := StrtoIntDef( sEmpleado, 0 );
                       if ( lGeneraChecadas ) then
                       begin
                            lDigitoEmpresa := True;
                            dFecComida := FieldByName('CF_FECHA').AsDateTime;
                            sHora := ParamList.ParamByName('Hora').AsString;
                       end
                       else
                           sDigitoEmpresa := GetDigitoEmpresa( EmpresaActiva );
                       while not Eof and CanContinue( iEmpleado )  do
                       begin
                            sCredencial := FieldByName('CB_CREDENC').AsString;
                            sTipoComida := FieldByName('CF_TIPO').AsString;

                            if lGeneraChecadas then
                               sReloj := ParamList.ParamByName('Reloj').AsString
                            else
                              sReloj := Copy( FieldByName('RELOJ').AsString, 1, 4 );

                            iCantidad := FieldByName('CF_COMIDAS').AsInteger;
                            if ( not lGeneraChecadas ) then
                               CargaHoraFecha;
                            if( lDigitoEmpresa )then
                            begin
                                 iPos := Pos( K_INVITACION_PREFIX, sEmpleado );
                                 if ( iPos > 0 ) then   //Para Saber si es un Invitador
                                 begin
                                      sInvitador := Copy( sEmpleado, (iPos + 2), ( Length(sEmpleado) - (iPos + 2) + 1 ) );
                                      iInvitador := ZetaCommonTools.StrAsInteger(sInvitador);
                                      if( AutorizaInvitacion( iInvitador, dFecComida, sHora, sTipoComida, iCantidad, sMensajeInv ) )then
                                           GrabaInvitador( iInvitador )
                                      else
                                           Log.Error( 0, K_ERROR_REGISTRO, sMensajeInv );
                                 end
                                 else
                                 begin
                                      if( AutorizaEmpleado( iEmpleado, dFecComida, sHora, sCredencial, sTipoComida, iCantidad, sMensaje ) )then
                                      begin
                                           GrabaEmpleado( iEmpleado );
                                           Log.Evento(clbCafeHistorial,iEmpleado,Now,Format('Se agrego %d consumos de cafeteria #%d ',[iCantidad, iEmpleado] ) );
                                      end
                                      else
                                           Log.Error( iEmpleado, K_ERROR_REGISTRO, sMensaje );
                                 end;
                            end
                            else
                                 Log.Advertencia( iEmpleado, K_ERROR_REGISTRO, Format( K_DIGIT_EMPRESA, [ sDigitoEmpresa, FieldByName('ID_EMPRESA').AsString ] ) );
                            Next;
                            sEmpleado := FieldByName('CB_CODIGO').AsString;
                            iEmpleado := StrtoIntDef( sEmpleado, 0 );
                       end;
                  end;
               finally
                      TerminaTransaccion( True );
                      FreeAndNil( FDatasetInv );
                      FreeAndNil( FDatasetEmp );
                      FreeAndNil( FCursor4 );
                      FreeAndNil( FCursor3 );
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerCafeteria.ComidasGrupalesASCII( const Datos: OleVariant; const sCampo: String ): Integer;
begin
     oSuperASCII := TdmSuperASCII.Create( Self );
     try
        with oSuperASCII do
        begin
             Formato := faASCIIFijo;
             AgregaColumna( 'RELOJ', tgTexto, 11, True );
             AgregaColumna( 'ID_EMPRESA', tgTexto, 1, True );
             AgregaColumna( 'CB_CODIGO', tgTexto, 9, True );
             AgregaColumna( 'CB_CREDENC', tgTexto, 1, True );
             AgregaColumna( 'CF_FECHA', tgTexto, 4, True );
             AgregaColumna( 'CF_HORA', tgTexto, K_ANCHO_HORA, True );
             AgregaColumna( 'CF_COMIDAS', tgTexto, 2, True );
             AgregaColumna( 'CF_TIPO', tgTexto, K_ANCHO_TIPO_COMIDA, True );
             with cdsLista do
             begin
                  try
                     Lista := Procesa( Datos );
                     Result := RecordCount - Errores;
                  except
                     on Error: Exception do
                     begin
                          oZetaProvider.Log.Excepcion( FieldByName('CB_CODIGO').AsInteger, 'Error Al Leer Archivo de Comidas', Error );
                          Result := 0;
                     end;
                  end;
             end;
        end;
     finally
            oSuperASCII.Free;
     end;
end;

function TdmServerCafeteria.AutorizaEmpleado( iEmpleado: Integer; dFecha: TDateTime; sHora, sCredencial, sTipoComida: String; iNumComidas: Integer;var sDatos: String ): Boolean;
const
     R_RECHAZADA = 'COMIDA RECHAZADA';
     R_EMPLEADO_NO_EXISTE = 'Empleado No Existe';
     R_EMPLEADO_ES_BAJA = 'Empleado Es Baja (%s)';
     R_EMPLEADO_CREDENCIAL_INCORRECTA = 'Letra Credencial Incorrecta';
     R_COMIDA_YA_EXISTE = 'Ya Est� Registrada Su Comida';
var
   sMensaje: String;
begin
     sDatos := VACIO;
     Result := False;
     with oZetaProvider do
     begin
          ParamAsInteger( FCursor3, 'Empleado', iEmpleado );
          ParamAsDate( FCursor3, 'Fecha', dFecha );
          ParamAsString( FCursor3, 'Hora', sHora );
          ParamAsString( FCursor3, 'Credencial', sCredencial );
          {$ifdef MULT_TIPO_COMIDA}
          ParamAsString( FCursor3, 'TipoComida', sTipoComida );
          {$endif}
     end;
     with FCursor3 do
     begin
          Active := True;
          if Eof then
             sMensaje := R_RECHAZADA
          else
          begin
               case FieldByName( 'Autorizacion' ).AsInteger of
                    0: Result := True;
                    1: sMensaje := R_EMPLEADO_NO_EXISTE;
                    2: sMensaje := Format( R_EMPLEADO_ES_BAJA, [ FechaCorta( FieldByName( 'FechaBaja' ).AsDateTime ) ] );
                    3: sMensaje := R_EMPLEADO_CREDENCIAL_INCORRECTA;
                    4:
                    begin
                         Result := TRUE;
                         sMensaje := R_COMIDA_YA_EXISTE;
                    end;
               else
                   sMensaje := R_RECHAZADA;
               end;
          end;
          Active := False;
          if not Result then
          begin
               sDatos := Format( K_POST_ERROR_EMP, [ iEmpleado,
                                                     FechaCorta( dFecha ),
                                                     //FormatDateTime( 'dd/mmm/yyyy', dFecha ),
                                                     sHora,
                                                     sTipoComida,
                                                     iNumComidas,
                                                     sCredencial,
                                                     sMensaje ] );
          end;
     end;
end;

function TdmServerCafeteria.AutorizaInvitacion( const iEmpleado: Integer; dFecha: TDateTime; sHora, sTipoComida: String; const iNumComidas: Integer; var sDatos: String ): Boolean;
const
     R_INVITADOR_NO_EXISTE = 'Invitador No Existe';
     R_INVITADOR_SUSPENDIDO = 'Invitador Suspendido';
     R_INVITACION_RECHAZADA = 'No Autoriza Invitaci�n';
     R_INVITACION_YA_EXISTE = 'Ya Est� Registrada Su Invitaci�n';
var
   sMensajeInv: String;
begin
     sDatos := VACIO;
     Result := False;
     with oZetaProvider do
     begin
          ParamAsInteger( FCursor4, 'Invitador', iEmpleado );
          ParamAsDate( FCursor4, 'Fecha', dFecha );
          ParamAsString( FCursor4, 'Hora', sHora );
          ParamAsString( FCursor4, 'TipoComida', sTipoComida );
          ParamAsInteger( FCursor4, 'NumComidas', iNumComidas );
     end;
     with FCursor4 do
     begin
          Active := True;
          if Eof then
          begin
               sMensajeInv := R_INVITACION_RECHAZADA;
          end
          else
          begin
               case FieldByName( 'Autorizacion' ).AsInteger of
                    0: Result := True;
                    1: sMensajeInv := R_INVITADOR_NO_EXISTE;
                    2: sMensajeInv := R_INVITADOR_SUSPENDIDO;
                    3: sMensajeInv := R_INVITACION_RECHAZADA;
                    4: sMensajeInv := R_INVITACION_YA_EXISTE;
               else
                   sMensajeInv := R_INVITACION_RECHAZADA;
               end;
          end;
          Active := False;
          if not Result then
          begin
               sDatos := Format( K_POST_ERROR_INV, [ iEmpleado,
                                                     FechaCorta( dFecha ),
                                                     //FormatDateTime( 'dd/mmm/yyyy', dFecha ),
                                                     sHora,
                                                     sTipoComida,
                                                     iNumComidas,
                                                     sMensajeInv ] );
          end;
     end;
end;

procedure TdmServerCafeteria.CorregirFechasCafeParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Fecha Actual: ' + FechaCorta( ParamByName( 'FechaActual' ).AsDateTime ) + K_PIPE +
                              'Fecha Nueva: ' + FechaCorta( ParamByName( 'FechaNueva' ).AsDateTime ) + K_PIPE +
                              'Corregir: ' + ZetaCommonLists.ObtieneElemento( lfTipoCorregirFechas, ParamByName( 'Corregir' ).AsInteger );
     end;
end;

procedure TdmServerCafeteria.CorregirFechasCafeBuildDataset;
var
   dActual: TDate;
   eTipo: eTipoCorregirFechas;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dActual := ParamByName( 'FechaActual' ).AsDate;
               eTipo   := eTipoCorregirFechas( ParamByName( 'Corregir' ).AsInteger );
          end;
          if ( eTipo <> tcfInvitaciones ) then
          begin
               with SQLBroker do
               begin
                    Init( enComida );
                    with Agente do
                    begin
                         AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                         AgregaColumna( 'CF_FECHA', True, Entidad, tgFecha, 0, 'CF_FECHA' );
                         AgregaColumna( 'CF_HORA', True, Entidad, tgTexto, 4, 'CF_HORA' );
                         {$ifdef MULT_TIPO_COMIDA}
                         AgregaColumna( 'CF_TIPO', True, Entidad, tgTexto, 1, 'CF_TIPO' );
                         {$endif}
                         AgregaFiltro( Format( '( CF_FECHA = %s )', [ DateToStrSQLC( dActual ) ] ), True, Entidad );
                         AgregaOrden( 'CB_CODIGO', True, Entidad );
                    end;
                    AgregaRangoCondicion( ParamList );
                    FiltroConfidencial( EmpresaActiva );
                    BuildDataset( EmpresaActiva );
               end;
          end;
     end;
end;

function TdmServerCafeteria.CorregirFechasCafeDataset: OleVariant;
var
   eCorregir: eTipoCorregirFechas;
   FInvitaciones, FComidas: TZetaCursor;
   sNueva, sActual: string;
   iCuantos: integer;
   dNueva: TDate;

   procedure CorregirInvitaciones;
   begin
        with oZetaProvider do
        begin
             EmpiezaTransaccion;
             try
                if ( Ejecuta( FInvitaciones ) > 0 ) then
                   Log.Evento( clbCafeHistorial, 0, Date, 'Se Corrigieron Fechas Globales de Invitaciones' );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        Log.Excepcion( 0, 'Error Al Corregir Fechas Globales de Invitaciones', Error );
                   end;
             end;
        end;
   end;

   procedure CorregirComidas;
   var
      dFecha: TDate;
      sHora: string;
      iEmpleado, iEmpleadoAnt: TNumEmp;
   begin
        with oZetaProvider do
        begin
             with SQLBroker.SuperReporte.DataSetReporte do
             begin
                  iEmpleadoAnt := 0;
                  while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                  begin
                       iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                       dFecha := FieldByName( 'CF_FECHA' ).AsDateTime;
                       sHora := FieldByName( 'CF_HORA' ).AsString;
                       EmpiezaTransaccion;
                       try
                          ParamAsInteger( FComidas, 'CB_CODIGO', iEmpleado );
                          ParamAsDate( FComidas, 'CF_FECHA', dFecha );
                          ParamAsString( FComidas, 'CF_HORA', sHora );
                          {$ifdef MULT_TIPO_COMIDA}
                          ParamAsChar( FComidas, 'CF_TIPO', FieldByName( 'CF_TIPO' ).AsString, K_ANCHO_CODIGO1 );
                          {$endif}
                          Ejecuta( FComidas );
                          TerminaTransaccion( True );
                          if ( iEmpleadoAnt <> iEmpleado ) then
                             Log.Evento( clbCafeHistorial,
                                         iEmpleado,
                                         Date,
                                         'Se Corrigi� Fecha Global de Comidas',
                                         Format( 'Empleado: %d ' + CR_LF +
                                                 'Cambi� Fecha de  %s  a  %s', [ iEmpleado,
                                                                                 FechaCorta( dFecha ),
                                                                                 FechaCorta( dNueva ) ] ) );
                          iEmpleadoAnt := iEmpleado;
                       except
                             on Error: Exception do
                             begin
                                  RollBackTransaccion;
                                  Log.Excepcion( iEmpleado, 'Error Al Corregir Fechas Globales de Comidas', Error );
                             end;
                       end;
                       Next;
                  end;
             end;
        end;
   end;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               sActual := DateToStrSQLC( ParamByName( 'FechaActual' ).AsDate );
               dNueva := ParamByName( 'FechaNueva' ).AsDate;
               sNueva := DateToStrSQLC( dNueva );
               eCorregir := eTipoCorregirFechas( ParamByName( 'Corregir' ).AsInteger );
          end;
          //Se Determina si es Unicamente Invitaciones
          if ( eCorregir <> tcfInvitaciones ) then
             iCuantos := SQLBroker.SuperReporte.DataSetReporte.RecordCount
          else
              iCuantos := 1;
          if OpenProcess( prCAFECorregirFechas, iCuantos, FListaParametros ) then
          begin
               FInvitaciones := CreateQuery( Format( GetScript( ecCorrigeInvitaciones ), [ sNueva, sActual ] ) );
               FComidas := CreateQuery( Format( GetScript( ecCorrigeComidas ), [ sNueva ] ) );
                try
                   case eCorregir of
                        //Comidas e Invitaciones
                        tcfAmbos:
                        begin
                             CorregirInvitaciones;
                             CorregirComidas;
                        end;
                        //Unicamente Comidas
                        tcfComidas: CorregirComidas;

                        //Unicamente Invitaciones
                        tcfInvitaciones: CorregirInvitaciones;
                   end;
               finally
                      FreeAndNil( FComidas );
                      FreeAndNil( FInvitaciones );
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerCafeteria.CorregirFechasCafe(Empresa,Parametros: OleVariant): OleVariant;
begin
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
        end;
        CorregirFechasCafeParametros;
        CorregirFechasCafeBuildDataset;
        Result := CorregirFechasCafeDataset;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ServerCafeteria contendra la logica necesaria para Caseta}
function TdmServerCafeteria.GetCasDiarias(Empresa: OleVariant; Empleado: Integer; dFecha: TDateTime): OleVariant;
const
     K_FILTRO = '( ( CB_CODIGO = %d ) and ( AL_FECHA = %s ) )';
begin
     SetTablaInfo( ecCaseta );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO , [ Empleado, DateToStrSQLC( dFecha ) ] ) ;
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCafeteria.GrabaCasDiarias(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( ecCaseta );
     oZetaProvider.EmpresaActiva := Empresa;
     Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerCafeteria.GetCaseta(Empresa: OleVariant; Fecha: TDateTime; lSoloAltas: WordBool): OleVariant;
var
   sScript: String;
begin
     sScript := Format( GetScript( ecCaseta ), [ ZetaCommonTools.DateToStrSQL( Fecha ), Nivel0( Empresa ) ] );
     Result := oZetaProvider.OpenSQL( Empresa, sScript, not lSoloAltas );
     SetComplete;
end;

function TdmServerCafeteria.GrabaCaseta(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( ecCaseta );
     oZetaProvider.EmpresaActiva := Empresa;
     Result := oZetaProvider.GrabaTablaGrid( Empresa, oDelta, ErrorCount );
     SetComplete;
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerCafeteria, Class_dmServerCafeteria, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.
