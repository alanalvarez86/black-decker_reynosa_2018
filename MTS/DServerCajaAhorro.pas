unit dServerCajaAhorro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, {BdeMts,} DataBkr, DBClient,
  MtsRdm, Mtx,
  Variants,
  {$ifndef DOS_CAPAS}
  CajaAhorro_TLB,
  {$endif}
  DZetaServerProvider, DB, ZetaSQLBroker,ZCreator, ZetaClientDataSet, ZetaServerDataSet,DPrestamosServer;

type
  eTipoTabla = ( eTAhorro,           //0
                 eTPresta,           //1
                 eCtasBancarias,     //2
                 eTiposDeposito,     //3
                 eAhorro,            //4
                 eRegPrestamos,      //5
                 eCtasMovimientos,   //6
                 eACarAbo,           //7
                 ePCarAbo,
                 ePeriodos );
type
  TdmServerCajaAhorro = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerCajaAhorro {$endif} )
    cdsTotales: TServerDataSet;
    cdsTemporal: TZetaClientDataSet;
    cdsDataset: TZetaClientDataSet;
    cdsAhorro: TServerDataSet;
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FCursorGral: TZetaCursor;
      // Cursor de uso general
    {$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    {$endif}
    //procedure InitCreator;
    //procedure InitQueries;

    procedure SetTablaInfo(const eTabla: eTipoTabla);
    procedure AfterUpdateAhorros(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure AfterUpdatePrestamos(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure BeforeUpdatePrestamos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    function ValidaPrestamosCargosAbonos( oEmpresa: Olevariant; sTipo: string; iCodigo: integer; dRegistro: TDate ): TDate;
    function ValidaAhorrosCargosAbonos( oEmpresa: Olevariant; sTipo: string; iCodigo: integer; dRegistro: TDate ): TDate;
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    //procedure AfterUpdateCuentas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetTiposAhorro(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;  {$endif}
    function GetCatalogo(Empresa: OleVariant; iTabla: Integer; const Filtro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCatalogo(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetNominaEmpleado(Empresa: OleVariant; Empleado: Integer): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function InitRegPrestamos(Empresa: OleVariant;out CtasMovs: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ValidaCheque(Empresa: OleVariant; const CtaBancaria: WideString; Cheque: Integer): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSiguienteCheque(Empresa: OleVariant;const CtaBancaria: WideString): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaRegPrestamo(Empresa, oDelta, oDeltaMov, Parametros: OleVariant; out ErrorCount, ErrorCountMov: Integer; out ResultsMov: OleVariant; ForzarGrabarPrestamos: WordBool; var MensajePrestamo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetLiquidaAhorro(Empresa: OleVariant; iEmpleado: Integer; const sTipoAhorro: WideString; out CtasMovs: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    //procedure RegistrarLiquidacion(Empresa, LiquidaAhorro, CtasMovs: OleVariant); {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCuentasMovtos(Empresa: OleVariant; const sFiltro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCuentasMovtos(Empresa, Delta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetAhorro(Empresa: OleVariant; Empleado: Integer;  const TipoAhorro: WideString; out Prestamos,  ACarAbo: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaSaldosAhorro(Empresa, Delta, DeltaACarAbo: OleVariant; var ErrorCount, ErrorCountCarAbo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaSaldosPrestamo(Empresa, Delta, DeltaPCarAbo,Parametros: OleVariant; out ErrorCount, ErrorCountCarAbo: Integer; ForzarGrabarPrestamos: WordBool; var MensajePrestamo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTotalesCajaFondo(Empresa: OleVariant; const TipoAhorro: WideString; var Cuentas: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCuentasBancarias(Empresa, Parametros: OleVariant; var SaldoInicial: Double): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTotalesRetenciones(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;
    function GetPeriodos(Empresa: OleVariant; Year, Tipo,
      Filtro: Integer): OleVariant; safecall; {$endif}
    function GetCtasBancarias(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTotalAhorro(Empresa: OleVariant; Empleado: Integer; const CajaAhorro, FondoAhorro: WideString; out Prestamos, ACarAbo: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTipoPrestamo(Empresa: OleVariant; Tipo: Integer): WideString;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTipoInteres(Empresa: OleVariant; Tipo: Integer): WideString; {$ifndef DOS_CAPAS} safecall;
    function GetTotalRepartoInteres(Empresa,
      Parametros: OleVariant): OleVariant; safecall; {$endif}
    function GetDatosRepartoIntereses(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CancelaGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;
    function GetEmpleadosAval(Empresa: OleVariant;
      const TipoAhorro: WideString): OleVariant; safecall;
    procedure RegistrarLiquidacion(Empresa, LiquidaAhorro,
      CtasMovs: OleVariant; Tipo: Integer); safecall; {$endif}
    procedure GrabarIntereses(Empresa, Datos, Parametros: OleVariant); {$ifndef DOS_CAPAS} safecall;
    procedure CancelaIntereses(Empresa, Parametros: OleVariant); safecall; {$endif}
  end;

var
  dmServerPrestamos : TPrestamosServer;   { Es un var en un dll, �esta bien? }

implementation

{$R *.DFM}

uses ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     DQueries,
     ZetaServerTools;


const
     K_FILTRO_NO_CTAS_MOVS = '( CM_FOLIO = 0 )';

type
    eScript=( Q_TIPOAHORRO,
              Q_SIGCHEQUE,
              Q_AHORRO_LIQUIDA,
              Q_PRESTAMO_LIQUIDA,
              Q_CTA_MOVS_LIQUIDA,
              Q_INSERT_CTAMOVS_LIQUIDA,
              Q_INSERT_ACAR_ABO,
              Q_INSERT_PCAR_ABO,
              Q_UPDATE_AHORRO,
              Q_UPDATE_PRESTAMO,
              Q_AHORRO,
              Q_AFTER_UPDATE_AHORRO,
              Q_PRESTAMO,
              Q_TOTAL_PRESTAMO,
              Q_AFTER_UPDATE_PRESTAMO,
              Q_TOTAL_AHORRADO,
              Q_SALDO_PRESTAMOS,
              Q_SALDO_BANCOS,
              Q_CUENTAS,
              Q_SALDO_CTA,
              Q_MOVIMIENTOS,
              Q_TOTALES_RENTENCION_AHORRO,
              Q_TOTALES_RENTENCION_PRESTAMO,
              Q_NOMINA_EMPLEADO,
              Q_VALIDA_CHEQUE,             
              Q_AFTER_UPDATE_PRESTAMO_CTA_MOVS,
              Q_TOTAL_AHORRO,
              Q_GET_TIPO_AHORRO,
              Q_GET_TIPO_INTERES,
              Q_TIPOS_NOMINA,
              Q_TOTAL_INTERESXPRESTAMOS,
              Q_TOTAL_AHORRO_SEMANAL,
              Q_AHORRO_ACUMULADO,
              Q_INTERESES_ACUMULADO,
              Q_DATOS_REPARTO_AHORRO,
              Q_FECHA_INICIO_PERIODO,
              Q_DATOS_AHORRO,
              Q_UPDATE_ACAR_ABO,
              Q_DATOS_ACAR_ABO,
              Q_GET_LISTA_CANCELACION,
              Q_DELETE_ACAR_ABO,
              Q_EMPLEADOS_AVAL,
              Q_PRESTAMO_LIQUIDA_CAPITAL,
              Q_PRESTAMO_LIQUIDA_INTERES,
              Q_AHORRO_LIQUIDA_MONTOS,
              Q_INTERESES_COMISION,
              Q_UPDATE_PRESTAMO2,
              Q_CAPITAL_TOTAL,
              Q_GET_SALDO_PRESTAMO,
              Q_AFTER_UPDATE_AHORRO_CAJA,
              Q_UPDATE_AHORRO2,
              Q_DATOS_PCAR_ABO,
              Q_UPDATE_PRESTAMO_ESTATUS
            );

function GetSQLScript( const Script: eScript ): String;
begin
     case Script of
          {$ifdef B&D}
          Q_TIPOAHORRO: Result := 'select TB_CODIGO, TB_ELEMENT, TB_PRESTA, TB_CONCEPT, TB_RELATIV, TB_NUMERO, TB_INTERES, TB_PAGO, TB_ACTIVO, TB_NIVEL0 from TAHORRO where TB_NUMERO in( 1, 2 ) order by TB_CODIGO';
          {$else}
         Q_TIPOAHORRO: Result := 'select TB_CODIGO, TB_ELEMENT, TB_PRESTA, TB_CONCEPT, TB_RELATIV, TB_ACTIVO, TB_NIVEL0 from TAHORRO where TB_ACTIVO = ''S'' order by TB_CODIGO';
          {$endif}
          Q_SIGCHEQUE: Result :=
                                {$ifdef INTERBASE}
                                'select RESULTADO FROM SIGUIENTE_CHEQUE( %s )';
                                {$ELSE}
                                'select RESULTADO = dbo.SIGUIENTE_CHEQUE( %s )';
                                {$ENDIF}
          Q_AHORRO_LIQUIDA: Result := 'select CB_CODIGO, AH_TIPO, AH_SALDO, AH_FECHA, AH_STATUS, TAHORRO.TB_PRESTA, TAHORRO.TB_RELATIV,'+
                                      'AH_ABONOS, AH_CARGOS, AH_SALDO_I, AH_TOTAL, 0.0 as TASA, 0.0 as DURACION, ' +
                                      '(SELECT SUM(PR_SALDO)  '+
                                      'FROM PRESTAMO '+
                                      'WHERE PRESTAMO.CB_CODIGO=AHORRO.CB_CODIGO '+
                                      'AND PRESTAMO.PR_TIPO = TAHORRO.TB_PRESTA '+
                                      'AND PR_STATUS = 0) PR_SALDO, '+
                                      '( select AC_ANUAL from ACUMULA where '+
                                         '( AC_YEAR = %d ) and '+
                                         '( CB_CODIGO = %d ) and '+
                                         '( CO_NUMERO = TAHORRO.TB_RELATIV ) ) C_RELATIVO ' +
                                      'from AHORRO '+
                                      'left outer join TAHORRO on AHORRO.AH_TIPO = TAHORRO.TB_CODIGO  '+
                                      'where AHORRO.CB_CODIGO = %d and AHORRO.AH_TIPO = %s ';
                                        {PE_YEAR,PE_TIPO,PE_NUMERO,}
          Q_CTA_MOVS_LIQUIDA: Result := 'select CB_CODIGO,CM_FOLIO,CM_FECHA,CM_TOT_AHO,CM_INTERES,CM_SAL_PRE, '+
                                        'CM_MONTO,CT_CODIGO,CM_CHEQUE,CM_DESCRIP, CM_TIPO, '+
                                        'CM_DEP_RET,CM_BENEFI,CM_STATUS,CM_DEPOSIT,CM_RETIRO,'+
                                        '0.0 as CM_AFAVOR, 0.0 as CM_DISPONIBLE, '' '' as PRETTYNAME, '+
                                        '0.0 as CM_TOT_AHO_EMP, 0.0 as CM_INTERES_EMP, 0.0 as CM_AFAVOR_EMP, '+
                                        '0.0 as CM_DISPONIBLE_EMP, 0.0 as CM_TOTAL_DISPONIBLE '+
                                        'from CTA_MOVS '+
                                        'WHERE CM_FOLIO = 0 ';
          Q_PRESTAMO_LIQUIDA: Result := 'select CB_CODIGO, PR_TIPO, PR_REFEREN, PR_FECHA, PR_MONTO, PR_SALDO, PR_PAGOS '+
                                        'from PRESTAMO '+
                                        'left outer join TPRESTA on PRESTAMO.PR_TIPO = TPRESTA.TB_CODIGO '+
                                        'where PRESTAMO.CB_CODIGO = %d and PR_TIPO = %s and PR_STATUS = 0 '+
                                        'order by PR_FECHA' ;
          Q_INSERT_PCAR_ABO: Result := 'insert into PCAR_ABO ( CB_CODIGO, CR_CAPTURA,PR_TIPO, '+
                                       'CR_FECHA,PR_REFEREN,CR_OBSERVA, '+
                                       'CR_ABONO,CR_CARGO,US_CODIGO ) '+
                                       'VALUES( :CB_CODIGO,:CR_CAPTURA,:PR_TIPO, '+
                                       ':CR_FECHA,:PR_REFEREN,:CR_OBSERVA, '+
                                       ':CR_ABONO,:CR_CARGO,:US_CODIGO )';
          Q_INSERT_CTAMOVS_LIQUIDA: Result := 'insert into CTA_MOVS ( CB_CODIGO, CM_FECHA,CM_TOT_AHO,CM_INTERES,CM_SAL_PRE, '+
                                                  'CM_MONTO,CT_CODIGO,CM_CHEQUE,CM_DESCRIP, CM_TIPO, '+
                                                  'CM_DEP_RET,CM_BENEFI,CM_STATUS ) '+
                                          'VALUES( :CB_CODIGO, :CM_FECHA,:CM_TOT_AHO,:CM_INTERES,:CM_SAL_PRE, '+
                                                  ':CM_MONTO,:CT_CODIGO,:CM_CHEQUE,:CM_DESCRIP, :CM_TIPO, '+
                                                  ':CM_DEP_RET,:CM_BENEFI,:CM_STATUS )';
          Q_INSERT_ACAR_ABO: Result := 'insert into ACAR_ABO( AH_TIPO,	CB_CODIGO,	CR_CAPTURA,	CR_ABONO, '+
                  	                                 'CR_FECHA,	CR_CARGO,	CR_OBSERVA,	US_CODIGO ) '+
                                           'VALUES( :AH_TIPO,	:CB_CODIGO,	:CR_CAPTURA,	:CR_ABONO, '+
                                                   ':CR_FECHA,	:CR_CARGO,	:CR_OBSERVA,	:US_CODIGO ) ';
          {Q_UPDATE_AHORRO: Result :=  'update AHORRO set ' +
                                      'AH_STATUS = :AH_STATUS, '+
                                      'AH_ABONOS  = :AH_ABONOS, '+
                                      'AH_CARGOS  = :AH_CARGOS, '+
                                      'AH_SALDO   = :AH_SALDO '+
                                      'where CB_CODIGO = :CB_CODIGO AND AH_TIPO = :AH_TIPO ';
          }
          Q_UPDATE_AHORRO: Result := 'update AHORRO set '+
                                     'AH_CARGOS = ISNULL( ( SELECT SUM( CR_CARGO ) FROM ACAR_ABO C  WHERE ( C.CB_CODIGO = AHORRO.CB_CODIGO ) and ( C.AH_TIPO = AHORRO.AH_TIPO ) ),0 ), '+
                                     'AH_ABONOS = ISNULL( ( SELECT SUM( CR_ABONO ) FROM ACAR_ABO C  WHERE ( C.CB_CODIGO = AHORRO.CB_CODIGO ) and ( C.AH_TIPO = AHORRO.AH_TIPO ) ),0 ), '+
                                     'AH_SALDO = ( AH_SALDO_I + AH_TOTAL + '+
                                     'ISNULL( ( SELECT SUM( CR_ABONO ) FROM ACAR_ABO C WHERE ( C.CB_CODIGO = AHORRO.CB_CODIGO ) and ( C.AH_TIPO = AHORRO.AH_TIPO ) ),0 ) - '+
                                     'ISNULL( ( SELECT SUM( CR_CARGO ) FROM ACAR_ABO C WHERE ( C.CB_CODIGO = AHORRO.CB_CODIGO ) and ( C.AH_TIPO = AHORRO.AH_TIPO ) ), 0 ) ) '+
                                     'where ( CB_CODIGO = :CB_CODIGO ) and ( AH_TIPO = :AH_TIPO )';
          Q_AFTER_UPDATE_AHORRO: Result :=  'update AHORRO set ' +
                                            'AH_SALDO = AH_SALDO_I + AH_TOTAL + AH_ABONOS-AH_CARGOS ' +
                                            'where CB_CODIGO = %s AND AH_TIPO = %s ';

          Q_UPDATE_PRESTAMO: Result :=
                                       {$IFDEF INTERBASE}
                                       'update PRESTAMO set '+
                                       'PR_CARGOS = ( SELECT RESULTADO FROM ES_NULO_NUMERICO((SELECT SUM( CR_CARGO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0)), '+
                                       'PR_ABONOS = ( SELECT RESULTADO FROM ES_NULO_NUMERICO((SELECT SUM( CR_ABONO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0)), '+
                                       'PR_SALDO = ( PR_MONTO - PR_SALDO_I - PR_TOTAL - ( SELECT RESULTADO FROM ES_NULO_NUMERICO((SELECT SUM( CR_ABONO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0)) + (SELECT RESULTADO FROM ES_NULO_NUMERICO((SELECT SUM( CR_CARGO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0)) ) '+
                                       'where ( CB_CODIGO = :Empleado ) and ( PR_TIPO = :Tipo ) and ( PR_REFEREN = :Referencia )';
                                       {$ELSE}
                                       'update PRESTAMO set '+
                                       'PR_CARGOS = ISNULL((SELECT SUM( CR_CARGO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0), '+
                                       'PR_ABONOS = ISNULL((SELECT SUM( CR_ABONO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0), '+
                                       'PR_SALDO = ( PR_MONTO - PR_SALDO_I - PR_TOTAL - ISNULL((SELECT SUM( CR_ABONO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0) + ISNULL((SELECT SUM( CR_CARGO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0) ), '+
                                       'PR_STATUS = 2 '+
                                       'where ( CB_CODIGO = :Empleado ) and ( PR_TIPO = :Tipo ) and ( PR_REFEREN = :Referencia )';
                                       {$ENDIF}
          Q_AFTER_UPDATE_PRESTAMO: Result := 'update PRESTAMO set '+
                                       'PR_SALDO = ( PR_MONTO - PR_SALDO_I - PR_TOTAL - PR_ABONOS + PR_CARGOS ) '+
                                       'where ( CB_CODIGO = %s ) and ( PR_TIPO = %s ) and ( PR_REFEREN = %s )';


          Q_UPDATE_PRESTAMO_ESTATUS: Result := 'update PRESTAMO set '+
                                                      'PR_STATUS = %d '+
                                                      'where ( CB_CODIGO = %d ) and ( PR_TIPO = %s ) and ( PR_REFEREN = %s )';

          Q_AHORRO: Result := 'select TAHORRO.TB_PRESTA, '+
                              'AH_ABONOS,AH_CARGOS,AH_FECHA,AH_FORMULA,AH_NUMERO,AH_SALDO_I,AH_STATUS, '+
                              'AH_SALDO,AH_TIPO,AH_TOTAL,US_CODIGO,CB_CODIGO, AH_SUB_CTA, '+
                              '(SELECT SUM(PR_SALDO)  '+
                              'FROM PRESTAMO '+
                              'WHERE PRESTAMO.CB_CODIGO=AHORRO.CB_CODIGO '+
                              'AND PRESTAMO.PR_TIPO = TAHORRO.TB_PRESTA '+
                              'AND PR_STATUS = 0) PR_SALDO, '+
                              {$ifdef INTERBASE}
                              '(AH_SALDO - ( select Resultado from ES_NULO_NUMERICO(  '+
                                          '( SELECT SUM(PR_SALDO) FROM PRESTAMO  '+
                                            'WHERE PRESTAMO.CB_CODIGO=AHORRO.CB_CODIGO '+
                                            'AND PRESTAMO.PR_TIPO = TAHORRO.TB_PRESTA '+
                                            'AND PR_STATUS = 0),0) ) ) AH_NETO '+
                              {$ELSE}
                              '(AH_SALDO - isnull((SELECT SUM(PR_SALDO) '+
                                           'FROM PRESTAMO '+
                                           'WHERE PRESTAMO.CB_CODIGO=AHORRO.CB_CODIGO AND '+
                                           'PRESTAMO.PR_TIPO = TAHORRO.TB_PRESTA AND PR_STATUS = 0),0) ) AH_NETO '+
                              {$ENDIF}
                              'from AHORRO '+
                              'left outer join TAHORRO on AHORRO.AH_TIPO = TAHORRO.TB_CODIGO  '+
                              'where AHORRO.CB_CODIGO = %d and AHORRO.AH_TIPO = %s ';
          {
          Q_PRESTAMO: Result := 'select PR_ABONOS,PR_CARGOS,PR_FECHA,PR_FORMULA,PR_MONTO,PR_NUMERO,PR_REFEREN,PR_SALDO_I,PR_STATUS,PR_TIPO,PR_TOTAL,PR_SALDO,US_CODIGO,CB_CODIGO,PR_MONTO_S,PR_INTERES,PR_TASA,PR_MESES,PR_PAGOS,PR_PAG_PER, '+
                                'TPRESTA.TB_ELEMENT,TAHORRO.TB_CODIGO AH_TIPO '+
                                'from PRESTAMO '+
                                'left outer join TAHORRO on TAHORRO.TB_CODIGO = %s '+
                                'left outer join TPRESTA on TPRESTA.TB_CODIGO = TAHORRO.TB_PRESTA '+
                                'where PRESTAMO.CB_CODIGO = %d and PR_STATUS = 0 AND PR_TIPO = TAHORRO.TB_PRESTA ' +
                                'order by PR_FECHA';
          }
          Q_PRESTAMO: Result := 'select PRESTAMO.PR_ABONOS, PRESTAMO.PR_CARGOS, PRESTAMO.PR_FECHA, PRESTAMO.PR_FORMULA, PRESTAMO.PR_MONTO, PRESTAMO.PR_NUMERO, PRESTAMO.PR_REFEREN, '+
                                'PRESTAMO.PR_SALDO_I, PRESTAMO.PR_STATUS, PRESTAMO.PR_TIPO, PRESTAMO.PR_TOTAL, PRESTAMO.PR_SALDO, PRESTAMO.US_CODIGO, PRESTAMO.CB_CODIGO, '+
                                'PRESTAMO.PR_MONTO_S, PRESTAMO.PR_INTERES, PRESTAMO.PR_TASA, PRESTAMO.PR_MESES, PRESTAMO.PR_PAGOS, PRESTAMO.PR_PAG_PER, PRESTAMO.PR_AVAL, PRESTAMO.PR_MONTO_A, '+
                                'PRESTAMO.PR_FEC_AVA, PRESTAMO.PR_OBSERVA, '+
                                'TPRESTA.TB_ELEMENT,TAHORRO.TB_CODIGO AH_TIPO, PRESTAMO.PR_SUB_CTA, CTA_MOVS.CM_PRESTA '+
                                'from PRESTAMO '+
                                'left outer join TAHORRO on TAHORRO.TB_CODIGO = %s '+
                                'left outer join TPRESTA on TPRESTA.TB_CODIGO = TAHORRO.TB_PRESTA '+
                                'left outer join CTA_MOVS on PRESTAMO.PR_TIPO = CTA_MOVS.PR_TIPO and PRESTAMO.PR_REFEREN = CTA_MOVS.PR_REFEREN and PRESTAMO.PR_FECHA = CTA_MOVS.CM_FECHA and CTA_MOVS.CB_CODIGO = %d '+
                                'where PRESTAMO.CB_CODIGO = %d and PRESTAMO.PR_STATUS = 0 AND PRESTAMO.PR_TIPO = TAHORRO.TB_PRESTA '+
                                'order by PRESTAMO.PR_FECHA';

          Q_TOTAL_PRESTAMO: Result :=
                                'select PRESTAMO.PR_ABONOS, PRESTAMO.PR_CARGOS, PRESTAMO.PR_FECHA, PRESTAMO.PR_FORMULA, PRESTAMO.PR_MONTO, PRESTAMO.PR_NUMERO, PRESTAMO.PR_REFEREN, '+
                                'PRESTAMO.PR_SALDO_I, PRESTAMO.PR_STATUS, PRESTAMO.PR_TIPO, PRESTAMO.PR_TOTAL, PRESTAMO.PR_SALDO, PRESTAMO.US_CODIGO, PRESTAMO.CB_CODIGO, '+
                                'PRESTAMO.PR_MONTO_S, PRESTAMO.PR_INTERES, PRESTAMO.PR_TASA, PRESTAMO.PR_MESES, PRESTAMO.PR_PAGOS, PRESTAMO.PR_PAG_PER, PRESTAMO.PR_AVAL, PRESTAMO.PR_MONTO_A, '+
                                'PRESTAMO.PR_FEC_AVA, PRESTAMO.PR_OBSERVA, '+
                                'TPRESTA.TB_ELEMENT,TAHORRO.TB_CODIGO AH_TIPO, PRESTAMO.PR_SUB_CTA '+ // ,CTA_MOVS.CM_PRESTA '+
                                'from PRESTAMO '+
                                'left outer join TAHORRO on TAHORRO.TB_NUMERO = %s or TAHORRO.TB_NUMERO = %s '+
                                'left outer join TPRESTA on TPRESTA.TB_CODIGO = TAHORRO.TB_PRESTA or TPRESTA.TB_CODIGO = TAHORRO.TB_INTERES '+
                                'where PRESTAMO.CB_CODIGO = %d '+
                                'and PRESTAMO.PR_TIPO = TPRESTA.TB_CODIGO '+
                                'order by PRESTAMO.PR_FECHA';

          //Folio 2:Totales de caja/fondo
          //1)
          Q_TOTAL_AHORRADO:    Result := 'select COUNT(*) CUANTOS_SOCIOS, SUM (AH_SALDO) TOTAL_AHORRADO '+
                                        'from AHORRO '+
                                        'left outer join TAHORRO on AHORRO.AH_TIPO= TAHORRO.TB_CODIGO '+
                                        'where TAHORRO.TB_CODIGO = %s and AHORRO.AH_STATUS = 0 ';
          //2)
          Q_SALDO_PRESTAMOS:   Result := 'select COUNT(*) NUM_PRESTAMOS, SUM (PR_SALDO) SALDO_PRESTAMOS '+
                                        'from PRESTAMO '+
                                        'left outer join TAHORRO on PRESTAMO.PR_TIPO = TAHORRO.TB_PRESTA '+
                                        'where TAHORRO.TB_CODIGO = %s and PR_STATUS = 0 ';
          //3)
          Q_SALDO_BANCOS:      Result :=
                                        {$ifdef INTERBASE}
                                        'select SUM( ( select Resultado from SALDO_CTA( CTABANCO.CT_CODIGO, %s )) ) SALDO_BANCOS '+
                                        'from CTABANCO '+
                                        'where AH_TIPO = %s and CT_STATUS = 0 ';
                                        {$ELSE}
                                        'select SUM( dbo.SALDO_CTA( CT_CODIGO, %s )) SALDO_BANCOS '+
                                        'from CTABANCO '+
                                        'where AH_TIPO = %s and CT_STATUS = 0 ';
                                        {$ENDIF}
          //4)
          Q_CUENTAS:           Result :=
                                         {$ifdef INTERBASE}
                                         'select CT_CODIGO,CT_NUM_CTA,CT_NOMBRE,CT_BANCO,CT_NUMERO,CT_TEXTO, '+
                                         'AH_TIPO,CT_STATUS,CT_REP_CHK,CT_REP_LIQ, '+
                                         '( select Resultado from SALDO_CTA(CTABANCO.CT_CODIGO, %s ) ) SALDO_HOY '+
                                         'from CTABANCO '+
                                         'where AH_TIPO = %s and CT_STATUS = 0 order by CT_CODIGO ';

                                         {$ELSE}
                                         'select CT_CODIGO,CT_NUM_CTA,CT_NOMBRE,CT_BANCO,CT_NUMERO,CT_TEXTO,AH_TIPO,CT_STATUS,CT_REP_CHK,CT_REP_LIQ, dbo.SALDO_CTA(CT_CODIGO, %s ) SALDO_HOY  '+
                                         'from CTABANCO '+
                                         'where AH_TIPO = %s and CT_STATUS = 0 order by CT_CODIGO ';
                                         {$ENDIF}

          //Folio 3: Estado de cuenta bancaria
          //1)
          Q_SALDO_CTA:         Result :=
                                        {$ifdef INTERBASE}
                                        'select Resultado from SALDO_CTA(%s,%s)';
                                        {$ELSE}
                                        'select dbo.SALDO_CTA(%s,%s) SALDO_INI';
                                        {$ENDIF}

          //2)
          Q_MOVIMIENTOS:        Result := 'select CM_FOLIO,CT_CODIGO,CM_TIPO,CM_DEP_RET,CM_MONTO,CM_DESCRIP,CM_CHEQUE,CM_FECHA,CM_BENEFI,CM_STATUS,CM_TOT_AHO,CM_INTERES,CM_SAL_PRE,CB_CODIGO,PE_YEAR,PE_TIPO,PE_NUMERO,CM_DEPOSIT,CM_RETIRO, '+
                                          {$ifdef INTERBASE}
                                          'cast((CM_DESCRIP ||'' ''|| CM_BENEFI) as VarChar(255)) DESCRIPCION, '+
                                          {$ELSE}
                                          'lTrim(CM_DESCRIP +'' ''+ CM_BENEFI) DESCRIPCION, '+
                                          {$ENDIF}
                                          '0.0 SALDO, 0.0 DEPOSITOS, 0.0 RETIROS '+
                                          'from CTA_MOVS '+
                                          'left outer join TCTAMOVS on TCTAMOVS.TB_CODIGO = CM_TIPO '+
                                          'where (CT_CODIGO = %s ) and (CM_FECHA between %s and %s ) and ( CM_STATUS = %d ) %s '+
                                          'order by CM_FECHA, CM_DEP_RET ';
          Q_TOTALES_RENTENCION_AHORRO: Result := 'select COUNT(*) Num_Ahorro, SUM(MO_DEDUCCI) Suma_Ahorro '+
                                                 'from MOVIMIEN '+
                                                 'where ( PE_YEAR = %d ) and ( %s ) and ( PE_NUMERO = %d ) '+
                                                 'and ( CO_NUMERO = %d ) and ( MO_ACTIVO = ''S'' )';
          Q_TOTALES_RENTENCION_PRESTAMO: Result := 'select COUNT(*) Num_Prestamos, SUM(MO_DEDUCCI) Suma_Prestamos '+
                                                   'from MOVIMIEN '+
                                                   'where ( PE_YEAR = %d ) and ( %s ) and ( PE_NUMERO = %d ) '+
                                                   'and ( ( CO_NUMERO = %d ) or ( CO_NUMERO = %d ) ) and ( MO_ACTIVO = ''S'' )';
          Q_NOMINA_EMPLEADO: Result := 'select TU_NOMINA from TURNO where TU_CODIGO = ( select CB_TURNO from COLABORA where CB_CODIGO = %d )';
          Q_VALIDA_CHEQUE: Result := 'select count(*) TOTAL from CTA_MOVS where ( CT_CODIGO = %s ) and ( CM_CHEQUE = %d )';
          Q_AFTER_UPDATE_PRESTAMO_CTA_MOVS: Result := 'update CTA_MOVS set '+
                                                      'CM_MONTO = %s '+
                                                      'where ( CB_CODIGO = %s ) and ( PR_TIPO = %s ) and ( PR_REFEREN = %s )';
          Q_TOTAL_AHORRO: Result := 'select * from dbo.CalcularAhorro( %d )';
          Q_GET_TIPO_AHORRO: Result := 'select TB_PRESTA from TAHORRO where ( TB_NUMERO = %d )';
          Q_GET_TIPO_INTERES: Result := 'select TB_INTERES from TAHORRO where ( TB_NUMERO = %d )';
          Q_TIPOS_NOMINA: Result := 'select distinct( cb_nomina ) from colabora ' +
                                    'where cb_codigo in( select cb_codigo from AHORRO where AH_TIPO = %s and AH_STATUS = 0 ) and cb_nomina is not null';
          Q_TOTAL_INTERESXPRESTAMOS: Result := 'select COALESCE( SUM(MO_DEDUCCI), 0 ) TOTAL from MOVIMIEN where CO_NUMERO = '+
                                               '( select TB_CONCEPT from TPRESTA where TB_CODIGO = ( select TB_INTERES from TAHORRO where TB_CODIGO = %s ) ) '+
                                               'and ( PE_YEAR = %d ) and ( %s ) and ( PE_NUMERO = %d )';
          Q_TOTAL_AHORRO_SEMANAL: Result := 'select COALESCE( SUM(MO_DEDUCCI), 0 ) TOTAL from MOVIMIEN where CO_NUMERO = '+
                                            '( select TB_CONCEPT from TAHORRO where TB_CODIGO = %s ) '+
                                            'and ( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d )';
          {$ifdef FALSE}
          Q_AHORRO_ACUMULADO: Result := 'select COALESCE( SUM(AC_ANUAL), 0 ) TOTAL from ACUMULA where ( AC_YEAR = %d ) and CO_NUMERO = '+
                                        '( select TB_CONCEPT from TAHORRO where TB_CODIGO = %s )';
          {$else}
          Q_AHORRO_ACUMULADO: Result := 'select SUM( AH_TOTAL ) TOTAL from AHORRO where ( AH_TIPO = %s ) and ( AH_STATUS = 0 ) and ( AH_TOTAL > 0 )';
          {$endif}
          Q_INTERESES_ACUMULADO: Result := 'select COALESCE( SUM(CR_ABONO), 0 ) TOTAL from ACAR_ABO where AH_TIPO = %s and ( CR_OBSERVA like %s or CR_OBSERVA like %s ) '+
                                           'and CR_FECHA between %s and %s';
          Q_DATOS_REPARTO_AHORRO: Result := 'select * from DBO.PE_REPARTO_INTERESES( %s, %d, %d, %d, %s, %s, %s, %s, %s, %s, %s )';
          Q_FECHA_INICIO_PERIODO: Result := 'select PE_ASI_INI from PERIODO where ( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d )';
          Q_DATOS_AHORRO: Result := 'select * from AHORRO where ( AH_TIPO = %s ) and ( CB_CODIGO = %d )';
          Q_UPDATE_ACAR_ABO: Result := 'update ACAR_ABO set CR_CAPTURA = :CR_CAPTURA, CR_ABONO=:CR_ABONO,CR_CARGO=:CR_CARGO,CR_OBSERVA=:CR_OBSERVA,US_CODIGO=:US_CODIGO '+
                                       'where ( AH_TIPO = :AH_TIPO ) and ( CB_CODIGO = :CB_CODIGO ) and ( CR_FECHA = :CR_FECHA )';
          Q_DATOS_ACAR_ABO: Result := 'select count(*) TOTAL from ACAR_ABO where ( AH_TIPO = %s ) and ( CB_CODIGO = %d ) and ( CR_FECHA = %s )';
          Q_GET_LISTA_CANCELACION: Result := 'select ACAR_ABO.*, COLABORA.PRETTYNAME, AHORRO.AH_STATUS, AHORRO.AH_CARGOS, AHORRO.AH_SALDO_I, AHORRO.AH_TOTAL from ACAR_ABO '+
                                              'left outer join COLABORA on COLABORA.CB_CODIGO = ACAR_ABO.CB_CODIGO '+
					      'left outer join AHORRO on AHORRO.AH_TIPO = ACAR_ABO.AH_TIPO and AHORRO.CB_CODIGO = ACAR_ABO.CB_CODIGO ' +
                                              'where ACAR_ABO.CR_FECHA = ( select PE_ASI_INI from PERIODO where ( PE_YEAR = %d ) '+
                                              'and ( PE_TIPO = %d ) and ( PE_NUMERO = %d ) ) '+
                                              'and ( ACAR_ABO.AH_TIPO = %s ) '+
                                              'and ( ( ACAR_ABO.CR_OBSERVA = ''INVERSION'' ) or ( ACAR_ABO.CR_OBSERVA = ''PRESTAMOS'' ) )';
          Q_DELETE_ACAR_ABO: Result := 'delete from ACAR_ABO where ( AH_TIPO = :AH_TIPO ) and ( CB_CODIGO = :CB_CODIGO ) and ( CR_FECHA = :CR_FECHA )';
          Q_EMPLEADOS_AVAL: Result := 'select COLABORA.CB_CODIGO, COLABORA.PRETTYNAME, AHORRO.AH_SALDO '+
                                      'from COLABORA '+
                                      'left outer join AHORRO on AHORRO.CB_CODIGO = COLABORA.CB_CODIGO '+
                                      'where ( COLABORA.CB_ACTIVO = ''S'' ) and ( AHORRO.AH_TIPO = %s ) and ( AHORRO.AH_STATUS = 0 ) ' +
                                      'and COLABORA.CB_CODIGO not in( select COALESCE( PRESTAMO.PR_AVAL , 0 ) from PRESTAMO where ( PRESTAMO.PR_STATUS = 0 ) )';
          Q_PRESTAMO_LIQUIDA_CAPITAL: Result := 'select CB_CODIGO, PR_TIPO, PR_REFEREN, PR_FECHA, PR_MONTO, PR_SALDO, PR_PAGOS, PR_STATUS '+
                                                'from PRESTAMO '+
                                                'left outer join TPRESTA on PRESTAMO.PR_TIPO = TPRESTA.TB_CODIGO '+
                                                'where ( PRESTAMO.CB_CODIGO = %d ) and PR_TIPO = ( select TB_PRESTA from TAHORRO where TB_CODIGO = %s ) '+
                                                'and ( PR_STATUS = 0 ) '+
                                                'order by PR_FECHA';
          Q_PRESTAMO_LIQUIDA_INTERES: Result := 'select CB_CODIGO, PR_TIPO, PR_REFEREN, PR_FECHA, PR_MONTO, PR_SALDO, PR_PAGOS, PR_STATUS '+
                                                'from PRESTAMO '+
                                                'left outer join TPRESTA on PRESTAMO.PR_TIPO = TPRESTA.TB_CODIGO '+
                                                'where ( PRESTAMO.CB_CODIGO = %d ) and PR_TIPO = ( select TB_INTERES from TAHORRO where TB_CODIGO = %s ) '+
                                                'and ( PR_STATUS = 0 ) '+
                                                'order by PR_FECHA' ;
          Q_AHORRO_LIQUIDA_MONTOS: Result := 'select * from AHORRO where ( AH_TIPO = %s ) and ( CB_CODIGO = %d )';
          Q_INTERESES_COMISION: Result := 'select COALESCE( SUM(CR_CARGO), 0 ) TOTAL from ACAR_ABO where AH_TIPO = %s and CR_OBSERVA like %s '+
                                           'and CR_FECHA between %s and %s';
          Q_UPDATE_PRESTAMO2: Result := 'update PRESTAMO set '+
                                        'PR_CARGOS = ISNULL((SELECT SUM( CR_CARGO ) FROM PCAR_ABO C '+
                                        'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0), '+
                                        'PR_ABONOS = ISNULL((SELECT SUM( CR_ABONO ) FROM PCAR_ABO C '+
                                        'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0), '+
                                        'PR_SALDO = ( PR_MONTO - PR_SALDO_I - PR_TOTAL - ISNULL((SELECT SUM( CR_ABONO ) FROM PCAR_ABO C '+
                                        'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0) + ISNULL((SELECT SUM( CR_CARGO ) FROM PCAR_ABO C '+
                                        'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0) ), PR_STATUS = :PR_STATUS  '+
                                        'where ( CB_CODIGO = :Empleado ) and ( PR_TIPO = :Tipo ) and ( PR_REFEREN = :Referencia )';
          Q_CAPITAL_TOTAL: Result := 'select SUM( CAPITAL ) AS TOTAL_CAPITAL_TOTAL from DBO.PE_REPARTO_INTERESES( %s, %d, %d, %d, %d, %d, %d, %n, %n, %n, %n )';
          Q_GET_SALDO_PRESTAMO: Result := 'select ( PR_MONTO - PR_SALDO_I - PR_TOTAL - ( select coalesce( SUM( CR_ABONO ), 0 ) '+
                                          'from PCAR_ABO C WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) '+
                                          'and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ) + ( SELECT coalesce( SUM( CR_CARGO ), 0 ) FROM PCAR_ABO C WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) '+
                                          'and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ) ) SALDO '+
                                          'from PRESTAMO where ( CB_CODIGO = %d ) and ( PR_TIPO = %s ) and ( PR_REFEREN = %s )';
          Q_AFTER_UPDATE_AHORRO_CAJA: Result := 'update AHORRO set ' +
                                                'AH_FECREIN = AH_FECHA, AH_TOTREIN = AH_TOTAL ' +
                                                'where CB_CODIGO = %s AND AH_TIPO = %s ';
          Q_UPDATE_AHORRO2: Result :=  'update AHORRO set ' +
                                       'AH_STATUS = :AH_STATUS, '+
                                       'AH_ABONOS  = :AH_ABONOS, '+
                                       'AH_CARGOS  = :AH_CARGOS, '+
                                       'AH_SALDO   = :AH_SALDO, '+
                                       'AH_FECREIN = :AH_FECREIN, '+
                                       'AH_TOTREIN = :AH_TOTREIN '+
                                       'where CB_CODIGO = :CB_CODIGO AND AH_TIPO = :AH_TIPO ';
          Q_DATOS_PCAR_ABO: Result := 'select count( * ) TOTAL from PCAR_ABO where ( PR_TIPO = %s ) and ( CB_CODIGO = %d ) and ( CR_FECHA = %s )';
     end;
end;

class procedure TdmServerCajaAhorro.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerCajaAhorro.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     dmServerPrestamos:= TPrestamosServer.Create( Self );
end;

procedure TdmServerCajaAhorro.MtsDataModuleDestroy(Sender: TObject);
begin
{$ifndef DOS_CAPAS}
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
     FreeAndNil(dmServerPrestamos);
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
{$endif}

end;

{ *********** Clases Auxiliares ************ }

{$ifdef DOS_CAPAS}
procedure TdmServerCajaAhorro.CierraEmpresa;
begin
end;
{$endif}

procedure TdmServerCajaAhorro.SetTablaInfo( const eTabla: eTipoTabla );
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTabla of
              {$ifdef B&D}
              eTAhorro : SetInfo('TAHORRO', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_CONCEPT,TB_RELATIV,TB_LIQUIDA,TB_ALTA,TB_PRESTA,TB_TASA1,TB_TASA2,TB_TASA3,TB_INTERES,TB_PAGO,TB_REP_INS,TB_REP_PRE,TB_REP_LIQ,TB_VAL_RAN,TB_FEC_FIN,TB_FEC_INI,TB_ACTIVO,TB_NIVEL0', 'TB_CODIGO' );
              eTPresta : SetInfo('TPRESTA', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_CONCEPT,TB_RELATIV,TB_LIQUIDA,TB_ALTA,TB_TASA1,TB_TASA2,TB_TASA3,TB_PAGO,TB_ACTIVO,TB_NIVEL0', 'TB_CODIGO' );
              {$else}
              eTAhorro : SetInfo('TAHORRO', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_CONCEPT,TB_RELATIV,TB_LIQUIDA,TB_ALTA,TB_PRESTA,TB_TASA1,TB_TASA2,TB_TASA3,TB_VAL_RAN,TB_FEC_FIN,TB_FEC_INI', 'TB_CODIGO' );
              eTPresta : SetInfo('TPRESTA', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_CONCEPT,TB_RELATIV,TB_LIQUIDA,TB_ALTA,TB_TASA1,TB_TASA2,TB_TASA3', 'TB_CODIGO' );
              {$endif}
              eCtasBancarias : SetInfo( 'CTABANCO', 'CT_CODIGO,CT_NUM_CTA,CT_NOMBRE,CT_BANCO,CT_NUMERO,CT_TEXTO,AH_TIPO,CT_STATUS,CT_REP_CHK,CT_REP_LIQ', 'CT_CODIGO' );
              eTiposDeposito : SetInfo( 'TCTAMOVS', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_SISTEMA', 'TB_CODIGO' );
              eRegPrestamos  :
              begin
                   SetInfo('PRESTAMO','PR_ABONOS,PR_CARGOS,PR_FECHA,PR_FORMULA,PR_MONTO,PR_NUMERO,PR_REFEREN,PR_SALDO_I,PR_STATUS,PR_TIPO,PR_TOTAL,PR_SALDO,US_CODIGO,CB_CODIGO,PR_MONTO_S,PR_INTERES,PR_TASA,PR_MESES,PR_PAGOS,PR_PAG_PER,PR_SUB_CTA,PR_AVAL,PR_MONTO_A,PR_FEC_AVA,PR_OBSERVA', 'CB_CODIGO,PR_TIPO,PR_REFEREN' );
                   AfterUpdateRecord := AfterUpdatePrestamos;
              end;
              eAhorro :
              begin
                   SetInfo( 'AHORRO','AH_ABONOS,AH_CARGOS,AH_FECHA,AH_FORMULA,AH_NUMERO,AH_SALDO_I,AH_STATUS,AH_SALDO,AH_TIPO,AH_TOTAL,US_CODIGO,CB_CODIGO,AH_SUB_CTA,AH_FECREIN,AH_TOTREIN', 'CB_CODIGO,AH_TIPO' );
                   AfterUpdateRecord := AfterUpdateAhorros;
              end;
              eCtasMovimientos: SetInfo( 'CTA_MOVS', 'CM_FOLIO,CT_CODIGO,CM_TIPO,CM_DEP_RET,CM_MONTO,CM_DESCRIP,CM_CHEQUE,CM_FECHA,CM_BENEFI,CM_STATUS,CM_TOT_AHO,CM_INTERES,CM_SAL_PRE,CB_CODIGO,PE_YEAR,PE_TIPO,PE_NUMERO,CM_DEPOSIT,CM_RETIRO,PR_TIPO,PR_REFEREN,CM_PRESTA', 'CM_FOLIO' );
              eACarAbo : SetInfo( 'ACAR_ABO', 'AH_TIPO,CB_CODIGO,CR_CAPTURA,CR_ABONO,CR_FECHA,CR_CARGO,CR_OBSERVA,US_CODIGO', 'CB_CODIGO,AH_TIPO,CR_FECHA' );
              ePCarAbo : SetInfo( 'PCAR_ABO', 'CB_CODIGO,CR_CAPTURA,PR_TIPO,CR_FECHA,PR_REFEREN,CR_OBSERVA,CR_ABONO,CR_CARGO,US_CODIGO', 'CB_CODIGO,PR_TIPO,PR_REFEREN,CR_FECHA' );
              ePeriodos    : SetInfo( 'PERIODO', 'PE_YEAR,PE_TIPO,PE_NUMERO,PE_DESCRIP,PE_USO,PE_STATUS,PE_INC_BAJ, '+
                                      'PE_SOLO_EX,PE_FEC_INI,PE_FEC_FIN,PE_FEC_PAG,PE_MES,PE_DIAS,PE_DIAS_AC, '+
{$ifdef QUINCENALES}
                                      'PE_ASI_INI, PE_ASI_FIN, '+
{$endif}
                                      'PE_DIA_MES,PE_POS_MES,PE_PER_MES,PE_PER_TOT,PE_FEC_MOD,PE_AHORRO,PE_PRESTAM, '+
                                      'PE_NUM_EMP,PE_TOT_PER,PE_TOT_NET,PE_TOT_DED,US_CODIGO', 'PE_YEAR,PE_TIPO,PE_NUMERO' );

          end;
     end;
end;

function TdmServerCajaAhorro.GetTiposAhorro(Empresa: OleVariant): OleVariant;
begin
     Result := oZetaProvider.OpenSQL( Empresa, GetSQLScript( Q_TIPOAHORRO ), True );
     SetComplete;
end;

function TdmServerCajaAhorro.GetCatalogo(Empresa: OleVariant; iTabla: Integer; const Filtro: WideString): OleVariant;
begin
     try
        SetTablaInfo( eTipoTabla( iTabla ) );
        with oZetaProvider do
        begin
             TablaInfo.Filtro := Filtro;
             Result := GetTabla( Empresa );
        end;
     except
        SetOLEVariantToNull( Result );
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.GrabaCatalogo(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant;
         out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eTipoTabla( iTabla ) );
     Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerCajaAhorro.GetSiguienteCheque(Empresa: OleVariant; const CtaBancaria: WideString): Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FCursorGral:= CreateQuery;
          try
             AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_SIGCHEQUE ), [ EntreComillas( CtaBancaria ) ] ) );
             Result := FCursorGral.FieldByName( 'RESULTADO' ).AsInteger;
          finally
             FreeAndNil( FCursorGral );
          end;
     end;
     SetComplete;
end;


function TdmServerCajaAhorro.InitRegPrestamos(Empresa: OleVariant; out CtasMovs: OleVariant): OleVariant;
const
     K_FILTRO_NO_PRESTAMOS = '( CB_CODIGO = 0 ) and ( PR_TIPO = '''' ) and ( PR_REFEREN = '''' )';
begin
     try
        SetTablaInfo( eRegPrestamos );
        with oZetaProvider do
        begin
             TablaInfo.Filtro := K_FILTRO_NO_PRESTAMOS;
             Result := GetTabla( Empresa );
        end;
        SetTablaInfo( eCtasMovimientos );
        with oZetaProvider do
        begin
             TablaInfo.Filtro := K_FILTRO_NO_CTAS_MOVS;
             CtasMovs := GetTabla( Empresa );
        end;
     except
        SetOLEVariantToNull( Result );
        SetOLEVariantToNull( CtasMovs );
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.GrabaRegPrestamo(Empresa, oDelta, oDeltaMov, Parametros: OleVariant;
         out ErrorCount, ErrorCountMov: Integer; out ResultsMov: OleVariant; ForzarGrabarPrestamos: WordBool;
         var MensajePrestamo: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );

          SetTablaInfo( eRegPrestamos );
          try
             TablaInfo.BeforeUpdateRecord := BeforeUpdatePrestamos;
             dmServerPrestamos.ZetaProvider.EmpresaActiva := EmpresaActiva;
             dmServerPrestamos.ZetaProvider.AsignaParamList( oZetaProvider.ParamList.VarValues );

             dmServerPrestamos.ForzarGrabaPrestamo := ForzarGrabarPrestamos;

             //TablaInfo.AfterUpdateRecord := AfterUpdateCuentas;
             Result := GrabaTabla( Empresa, oDelta, ErrorCount );
             if ( ErrorCount = 0 ) and ( not VarIsNull( oDeltaMov ) ) then
             begin
                  SetTablaInfo( eCtasMovimientos );
                  ResultsMov := GrabaTabla( Empresa, oDeltaMov, ErrorCountMov );
             end
             else
                 ResultsMov := oDeltaMov;
          finally
                 MensajePrestamo := dmServerPrestamos.MensajePrestamo;
          end;
     end;
end;
{
procedure TdmServerCajaAhorro.AfterUpdateCuentas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
begin
     if ( UpdateKind <> ukDelete ) then
     begin
          with oZetaProvider do
          begin
               FCursorGral := CreateQuery;

               //Se agrega un registro en CTA_MOVS
               PreparaQuery( FCursorGral, GetSQLScript( Q_INSERT_CTAMOVS_LIQUIDA ) );
               with DeltaDS do
               begin
                    ParamAsInteger( FCursorGral, 'CB_CODIGO', FieldByName('CB_CODIGO').AsInteger );
                    ParamAsDate( FCursorGral, 'CM_FECHA',     FieldByName('PR_FECHA').AsDateTime );
                    ParamAsFloat( FCursorGral, 'CM_TOT_AHO',  0  );
                    ParamAsFloat( FCursorGral, 'CM_INTERES',  FieldByName('CM_INTERES').AsFloat  );
                    ParamAsFloat( FCursorGral, 'CM_SAL_PRE',  FieldByName('CM_SAL_PRE').AsFloat  );
                    ParamAsFloat( FCursorGral, 'CM_MONTO',    FieldByName('CM_MONTO').AsFloat    );
                    ParamAsString( FCursorGral, 'CT_CODIGO',  FieldByName('CT_CODIGO').AsString  );
                    ParamAsInteger( FCursorGral, 'CM_CHEQUE', FieldByName('CM_CHEQUE').AsInteger );
                    ParamAsString( FCursorGral, 'CM_DESCRIP', FieldByName('CM_DESCRIP').AsString );
                    ParamAsString( FCursorGral, 'CM_TIPO',    FieldByName('CM_TIPO').AsString    );
                    ParamAsString( FCursorGral, 'CM_DEP_RET', FieldByName('CM_DEP_RET').AsString );
                    ParamAsString( FCursorGral, 'CM_BENEFI',  FieldByName('CM_BENEFI').AsString  );
                    ParamAsInteger( FCursorGral, 'CM_STATUS', FieldByName('CM_STATUS').AsInteger );
               end;
               try
                  Ejecuta( FCursorGral );
               except
                     on Error: Exception do
                     begin
                          if ( pos( 'XAK1', Error.Message ) > 0 ) then
                             DatabaseError( 'MARCO' );
                     end;

               end;
          end;
     end;
end;
}
function TdmServerCajaAhorro.GetLiquidaAhorro(Empresa: OleVariant;iEmpleado: Integer; const sTipoAhorro: WideString; out CtasMovs: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FCursorGral:= CreateQuery;

          {Result := OpenSQL( Empresa, Format( GetSQLScript( Q_AHORRO_LIQUIDA ), [ TheYear( Now ),
                                                                                  iEmpleado,
                                                                                  iEmpleado,
                                                                                  EntreComillas( sTipoAhorro ) ] ), True );}
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_AHORRO_LIQUIDA ), [ iEmpleado ] ), True );
          CtasMovs := OpenSQL( Empresa, GetSQLScript( Q_CTA_MOVS_LIQUIDA ), TRUE );
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.ValidaAhorrosCargosAbonos( oEmpresa: Olevariant; sTipo: string; iCodigo: integer; dRegistro: TDate ): TDate;
var
   iRegistros: integer;
   dResultado: TDate;
   FACargosAbonos: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := oEmpresa;
          dResultado := dRegistro;
          repeat
            FACargosAbonos := CreateQuery;
            try
               AbreQueryScript( FACargosAbonos, Format( GetSQLScript ( Q_DATOS_ACAR_ABO ), [ EntreComillas( sTipo ), iCodigo, DatetostrSQLC( dResultado ) ] ) );
               iRegistros := FACargosAbonos.FieldByName( 'TOTAL' ).AsInteger;
            finally
                   FreeAndNil( FACargosAbonos );
            end;
            if ( iRegistros > 0 ) then
               dResultado := dResultado + 1;
          until ( iRegistros = 0 );
          Result := dResultado;
     end;
end;

function TdmServerCajaAhorro.ValidaPrestamosCargosAbonos( oEmpresa: Olevariant; sTipo: string; iCodigo: integer; dRegistro: TDate ): TDate;
var
   iRegistros: integer;
   dResultado: TDate;
   FPCargosAbonos: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := oEmpresa;
          dResultado := dRegistro;
          repeat
            FPCargosAbonos := CreateQuery;
            try
               AbreQueryScript( FPCargosAbonos, Format( GetSQLScript ( Q_DATOS_PCAR_ABO ), [ EntreComillas( sTipo ), iCodigo, DatetostrSQLC( dResultado ) ] ) );
               iRegistros := FPCargosAbonos.FieldByName( 'TOTAL' ).AsInteger;
            finally
                   FreeAndNil( FPCargosAbonos );
            end;
            if ( iRegistros > 0 ) then
               dResultado := dResultado + 1;
          until ( iRegistros = 0 );
          Result := dResultado;
     end;
end;

procedure TdmServerCajaAhorro.RegistrarLiquidacion(Empresa, LiquidaAhorro,
  CtasMovs: OleVariant; Tipo: Integer);
var
   dAbonos, dCargos, dCapital, dInteres, rSaldo: TPesos;
   sObservaciones : string;
   FPrestamo, FPrestamoUpdate, FAhorro, FSaldo: TZetaCursor;
   dReingreso: Tdate;

begin
     cdsDataset.Data := LiquidaAhorro;
     cdsTemporal.Data := CtasMovs;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FCursorGral := CreateQuery;
          try
             if ( Tipo = 1 ) then //El monto que se presto esta avalado por el Fondo.
             begin
                  //FSaldo := CreateQuery;
                  //try
                     FPrestamo := CReateQuery;
                     try
                        AbreQueryScript( FPrestamo, Format( GetSQLScript( Q_PRESTAMO_LIQUIDA_CAPITAL ), [ cdsDataSet.FieldByName( 'CB_CODIGO' ).AsInteger,
                                                                                                          EntreComillas( cdsDataset.FieldByName( 'AH_TIPO' ).AsString ) ] ) );
                        { Prestamos de Capital }
                        FPrestamoUpdate := CreateQuery;
                        try
                           PreparaQuery( FPrestamoUpdate, GetSQLScript( Q_UPDATE_PRESTAMO2 ) );
                           //dCapital := 0;
                           dinteres := 0;
                           with FPrestamo do
                           begin
                                while not EOF do
                                begin
                                     PreparaQuery( FCursorGral, GetSQLScript( Q_INSERT_PCAR_ABO ) );
                                     with cdsTemporal do
                                     begin
                                          ParamAsString( FCursorGral, 'PR_TIPO', FPrestamo.FieldByName( 'PR_TIPO' ).AsString );
                                          ParamAsInteger( FCursorGral, 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                                          ParamAsDate( FCursorGral, 'CR_CAPTURA', Date );
                                          ParamAsDate( FCursorGral, 'CR_FECHA',  ValidaPrestamosCargosAbonos( Empresa, FPrestamo.FieldByName( 'PR_TIPO' ).AsString, FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'CM_FECHA' ).AsDateTime ) );  // FieldByName( 'CM_FECHA' ).AsDateTime );
                                          if ( Abs( FieldByName( 'CM_MONTO' ).AsFloat ) > FPrestamo.FieldByName( 'PR_SALDO' ).AsFloat ) then
                                          begin
                                               dCapital := FPrestamo.FieldByName( 'PR_SALDO' ).AsFloat;
                                               dInteres := Abs( FieldByName( 'CM_MONTO' ).AsFloat ) - dCapital;
                                          end
                                          else
                                          begin
                                               dCapital := Abs( FieldByName( 'CM_MONTO' ).AsFloat );
                                               dinteres := 0;
                                          end;
                                          if ( dCapital < 0 ) then
                                             ParamAsFloat( FCursorGral, 'CR_ABONO', dCapital * (-1) ) //Para no dejar negativos en Abonos
                                          else
                                              ParamAsFloat( FCursorGral, 'CR_ABONO', dCapital );
                                          ParamAsFloat( FCursorGral, 'CR_CARGO', 0 );
                                          ParamAsString( FCursorGral, 'PR_REFEREN', FPrestamo.FieldByName( 'PR_REFEREN' ).AsString );
                                          ParamAsString( FCursorGral, 'CR_OBSERVA', 'Liquidacion de Fondo Ahorro' );
                                          ParamAsInteger( FCursorGral, 'US_CODIGO', UsuarioActivo  );
                                     end;
                                     Ejecuta( FCursorGral );

                                     FSaldo := CreateQuery;
                                     try
                                        { Validar si el saldo quedo en cero }
                                        AbreQueryScript( FSaldo, Format( GetSQLScript( Q_GET_SALDO_PRESTAMO ), [ FPrestamo.FieldByName( 'CB_CODIGO' ).AsInteger,
                                                                                                                 EntreComillas( FPrestamo.FieldByName( 'PR_TIPO' ).AsString ),
                                                                                                                 EntreComillas( FPrestamo.FieldByName( 'PR_REFEREN' ).AsString ) ] ) );
                                        rSaldo := FSaldo.FieldByName( 'SALDO' ).AsFloat;
                                     finally
                                            FreeAndNil( FSaldo );
                                     end;

                                     { Si el saldo es menor a cero se cambia el estatus a Saldado }
                                     if ( rSaldo <= 0 ) then
                                        ParamAsInteger( FPrestamoUpdate, 'PR_STATUS', 2 )
                                     else
                                         ParamAsInteger( FPrestamoUpdate, 'PR_STATUS', FPrestamo.FieldByName( 'PR_STATUS' ).AsInteger );

                                     ParamAsString( FPrestamoUpdate, 'Tipo',    FPrestamo.FieldByName( 'PR_TIPO' ).AsString );
                                     ParamAsInteger( FPrestamoUpdate, 'Empleado',  FieldByName( 'CB_CODIGO' ).AsInteger );
                                     ParamAsString( FPrestamoUpdate, 'Referencia', FPrestamo.FieldByName( 'PR_REFEREN' ).AsString );

                                     Ejecuta( FPrestamoUpdate );
                                     Next;
                                end;
                           end;

                           { Sobra parte de intereses }
                           if ( dInteres > 0 ) then
                           begin
                                { Prestamos de intereses }
                                AbreQueryScript( FPrestamo, Format( GetSQLScript( Q_PRESTAMO_LIQUIDA_INTERES ), [ cdsDataSet.FieldByName('CB_CODIGO').AsInteger,
                                                                                                                  EntreComillas( cdsDataset.FieldByName( 'AH_TIPO' ).AsString ) ] ) );
                                with FPrestamo do
                                begin
                                     while not EOF do
                                     begin
                                          PreparaQuery( FCursorGral, GetSQLScript( Q_INSERT_PCAR_ABO ) );
                                          with cdsTemporal do
                                          begin
                                               ParamAsString( FCursorGral, 'PR_TIPO', FPrestamo.FieldByName( 'PR_TIPO' ).AsString );
                                               ParamAsInteger( FCursorGral, 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                                               ParamAsDate( FCursorGral, 'CR_CAPTURA', Date );
                                               ParamAsDate( FCursorGral, 'CR_FECHA', ValidaPrestamosCargosAbonos( Empresa, FPrestamo.FieldByName( 'PR_TIPO' ).AsString, FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'CM_FECHA' ).AsDateTime ) ); //FieldByName( 'CM_FECHA' ).AsDateTime  );
                                               if ( dInteres < 0 ) then
                                                  ParamAsFloat( FCursorGral, 'CR_ABONO', dInteres * (-1) )
                                               else
                                                   ParamAsFloat( FCursorGral, 'CR_ABONO', dInteres );
                                               ParamAsFloat( FCursorGral, 'CR_CARGO', 0 );
                                               ParamAsString( FCursorGral, 'PR_REFEREN', FPrestamo.FieldByName( 'PR_REFEREN' ).AsString );
                                               ParamAsString( FCursorGral, 'CR_OBSERVA', 'LIQUIDACION F.A.' );
                                               ParamAsInteger( FCursorGral, 'US_CODIGO', UsuarioActivo  );
                                          end;
                                          Ejecuta( FCursorGral );

                                          FSaldo := CreateQuery;
                                          try
                                             { Validar si el saldo quedo en cero }
                                             AbreQueryScript( FSaldo, Format( GetSQLScript( Q_GET_SALDO_PRESTAMO ), [ FPrestamo.FieldByName( 'CB_CODIGO' ).AsInteger,
                                                                                                                      EntreComillas( FPrestamo.FieldByName( 'PR_TIPO' ).AsString ),
                                                                                                                      EntreComillas( FPrestamo.FieldByName( 'PR_REFEREN' ).AsString ) ] ) );
                                             rSaldo := FSaldo.FieldByName( 'SALDO' ).AsFloat;
                                          finally
                                                 FreeAndNil( FSaldo );
                                          end;

                                          { Si el saldo es menor a cero se cambia el estatus a Saldado }
                                          if ( rSaldo <= 0 ) then
                                             ParamAsInteger( FPrestamoUpdate, 'PR_STATUS', 2 )
                                          else
                                              ParamAsInteger( FPrestamoUpdate, 'PR_STATUS', FPrestamo.FieldByName( 'PR_STATUS' ).AsInteger );

                                          ParamAsString( FPrestamoUpdate, 'Tipo', FPrestamo.FieldByName( 'PR_TIPO' ).AsString );
                                          ParamAsInteger( FPrestamoUpdate, 'Empleado',  FieldByName( 'CB_CODIGO' ).AsInteger );
                                          ParamAsString( FPrestamoUpdate, 'Referencia', FPrestamo.FieldByName( 'PR_REFEREN' ).AsString );

                                          Ejecuta(FPrestamoUpdate);

                                          Next;
                                     end;
                                end;
                           end;
                        finally
                               FreeAndNil( FPrestamoUpdate );
                        end;
                     finally
                            FreeAndNil( FPrestamo );
                     end;
                  //finally
                         //FreeAndNil( FSaldo );
                  //end;
             end
             else
             begin
                  { Se agrega un registro en CTA_MOVS }
                  PreparaQuery( FCursorGral, GetSQLScript( Q_INSERT_CTAMOVS_LIQUIDA ) );
                  with cdsTemporal do
                  begin
                       ParamAsInteger( FCursorGral, 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                       ParamAsDate( FCursorGral, 'CM_FECHA', FieldByName( 'CM_FECHA' ).AsDateTime );
                       ParamAsFloat( FCursorGral, 'CM_TOT_AHO', FieldByName( 'CM_TOT_AHO' ).AsFloat + FieldByName( 'CM_TOT_AHO_EMP' ).AsFloat );
                       ParamAsFloat( FCursorGral, 'CM_INTERES', FieldByName( 'CM_INTERES' ).AsFloat + FieldByName( 'CM_INTERES_EMP' ).AsFloat );
                       ParamAsFloat( FCursorGral, 'CM_SAL_PRE', FieldByName( 'CM_SAL_PRE' ).AsFloat );
                       ParamAsFloat( FCursorGral, 'CM_MONTO', FieldByName( 'CM_MONTO' ).AsFloat );
                       ParamAsString( FCursorGral, 'CT_CODIGO', FieldByName( 'CT_CODIGO' ).AsString );
                       ParamAsInteger( FCursorGral, 'CM_CHEQUE', FieldByName( 'CM_CHEQUE' ).AsInteger );
                       ParamAsString( FCursorGral, 'CM_DESCRIP', FieldByName( 'CM_DESCRIP' ).AsString );
                       ParamAsString( FCursorGral, 'CM_TIPO', FieldByName( 'CM_TIPO' ).AsString );
                       ParamAsString( FCursorGral, 'CM_DEP_RET', FieldByName( 'CM_DEP_RET' ).AsString );
                       ParamAsString( FCursorGral, 'CM_BENEFI', FieldByName( 'CM_BENEFI' ).AsString );
                       ParamAsInteger( FCursorGral, 'CM_STATUS', FieldByName( 'CM_STATUS' ).AsInteger );
                  end;
                  Ejecuta( FCursorGral );

                  //Si es liquidacion se agrega un registro por el prestamo descontado
                  PreparaQuery( FCursorGral, GetSQLScript( Q_INSERT_ACAR_ABO ) );
                  with cdsTemporal do
                  begin
                       if ( FieldByName( 'CM_TIPO' ).AsString = 'LIQUID' ) then
                       begin
                            if ( FieldByName( 'CM_SAL_PRE' ).AsFloat <> 0 ) then
                            begin
                                 ParamAsString( FCursorGral, 'AH_TIPO', cdsDataset.FieldByName( 'AH_TIPO' ).AsString );
                                 ParamAsInteger( FCursorGral, 'CB_CODIGO', cdsDataset.FieldByName( 'CB_CODIGO' ).AsInteger );
                                 ParamAsDate( FCursorGral, 'CR_CAPTURA', Date );
                                 ParamAsDate( FCursorGral, 'CR_FECHA', ValidaAhorrosCargosAbonos( Empresa, cdsDataset.FieldByName( 'AH_TIPO' ).AsString, cdsDataset.FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'CM_FECHA' ).AsDateTime ) ); //FieldByName( 'CM_FECHA' ).AsDateTime + 1 );
                                 ParamAsFloat( FCursorGral, 'CR_ABONO', 0 );
                                 ParamAsFloat( FCursorGral, 'CR_CARGO', FieldByName( 'CM_SAL_PRE' ).AsFloat );
                                 ParamAsString( FCursorGral, 'CR_OBSERVA', 'Pago de prestamos'  );
                                 ParamAsInteger( FCursorGral, 'US_CODIGO', UsuarioActivo );
                                 Ejecuta( FCursorGral );
                            end;
                       end;
                  end;

                   { Se agrega un registro por el monto retirado o liquidado }
                  PreparaQuery( FCursorGral, GetSQLScript( Q_INSERT_ACAR_ABO ) );
                  with cdsTemporal do
                  begin
                       if ( FieldByName('CM_TIPO').AsString = 'LIQUID' ) then
                          sObservaciones := 'Liquidacion'
                       else
                           sObservaciones := 'Retiro parcial';
           { iEmpleado :=  FieldByName('CB_CODIGO').AsInteger;
            sReferencia :=  FPrestamo.FieldByName('PR_REFEREN').AsString;
            sTipoPrestamo := FPrestamo.FieldByName('PR_TIPO').AsString ; }

                       dReingreso := ValidaAhorrosCargosAbonos( Empresa, cdsDataset.FieldByName( 'AH_TIPO' ).AsString, cdsDataset.FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'CM_FECHA' ).AsDateTime + 1 ); //FieldByName( 'CM_FECHA' ).AsDateTime  );
                       ParamAsString( FCursorGral, 'AH_TIPO', cdsDataset.FieldByName( 'AH_TIPO' ).AsString );
                       ParamAsInteger( FCursorGral,'CB_CODIGO', cdsDataset.FieldByName( 'CB_CODIGO' ).AsInteger );
                       ParamAsDate( FCursorGral, 'CR_CAPTURA', Date );
                       ParamAsDate( FCursorGral, 'CR_FECHA', dReingreso ); //ValidaAhorrosCargosAbonos( Empresa, cdsDataset.FieldByName( 'AH_TIPO' ).AsString, cdsDataset.FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'CM_FECHA' ).AsDateTime + 1 ) ); //FieldByName( 'CM_FECHA' ).AsDateTime  );
                       ParamAsFloat( FCursorGral, 'CR_ABONO', 0.0 );
                       ParamAsFloat( FCursorGral, 'CR_CARGO', FieldByName( 'CM_DISPONIBLE' ).AsFloat );
                       ParamAsString( FCursorGral, 'CR_OBSERVA', sObservaciones  );
                       ParamAsInteger( FCursorGral,'US_CODIGO', UsuarioActivo  );
                       { Fecha de reingreso desde cuando se deben ver los registros }
                       //dReingreso := FieldByName( 'CM_FECHA' ).AsDateTime + 3;
                  end;
                  Ejecuta( FCursorGral );

                  FAhorro := CReateQuery;
                  AbreQueryScript( FAhorro, Format( GetSQLScript( Q_AHORRO_LIQUIDA_MONTOS ), [ EntreComillas( cdsDataset.FieldByName( 'AH_TIPO' ).AsString ),
                                                                                               cdsDataSet.FieldByName('CB_CODIGO').AsInteger ] ) );
                  //Se actualizan los saldos del Ahorro
                  with cdsDataset do
                  begin
                       PreparaQuery ( FCursorGral, GetSQLScript( Q_UPDATE_AHORRO2 ) );
                       ParamAsInteger( FCursorGral, 'CB_CODIGO', FieldByName('CB_CODIGO').AsInteger );
                       ParamAsInteger( FCursorGral, 'AH_STATUS', FieldByName('AH_STATUS').AsInteger );
                       ParamAsString( FCursorGral, 'AH_TIPO', FieldByName('AH_TIPO').AsString );

                       dAbonos := FAhorro.FieldByName( 'AH_ABONOS' ).AsFloat;
                       ParamAsFloat( FCursorGral, 'AH_ABONOS', dAbonos );

                       dCargos := FAhorro.FieldByName( 'AH_CARGOS' ).AsFloat +
                                  cdsTemporal.FieldByName('CM_DISPONIBLE').AsFloat +
                                  cdsTemporal.FieldByName( 'CM_SAL_PRE' ).AsFloat;
                       ParamAsFloat( FCursorGral, 'AH_CARGOS', dCargos );

                       ParamAsFloat( FCursorGral, 'AH_SALDO', FAhorro.FieldByName('AH_SALDO_I').AsFloat +
                                                              FAhorro.FieldByName('AH_TOTAL').AsFloat +
                                                              dAbonos-dCargos );
                       ParamAsDate( FCursorGral, 'AH_FECREIN', dReingreso );
                       ParamAsFloat( FCursorGral, 'AH_TOTREIN', FAhorro.FieldByName('AH_TOTAL').AsFloat );
                  end;
                  Ejecuta( FCursorGral );

                  //Se saldan los prestamos.
                  if ( cdsTemporal.FieldByName('CM_TIPO').AsString = 'LIQUID' ) then
                     sObservaciones := 'Saldado por liquidacion'
                  else
                      sObservaciones := 'Saldado por Retiro Parcial';

                  //FSaldo := CReateQuery;
                  //try
                     FPrestamo := CReateQuery;
                     try
                        AbreQueryScript( FPrestamo, Format( GetSQLScript( Q_PRESTAMO_LIQUIDA_CAPITAL ), [ cdsDataSet.FieldByName('CB_CODIGO').AsInteger,
                                                                                                          EntreComillas( cdsDataset.FieldByName( 'AH_TIPO' ).AsString ) ] ) );

                        { Prestamos de Capital }
                        FPrestamoUpdate := CreateQuery;
                        try
                           PreparaQuery( FPrestamoUpdate, GetSQLScript( Q_UPDATE_PRESTAMO ) );

                           with FPrestamo do
                           begin
                                while not EOF do
                                begin
                                     PreparaQuery( FCursorGral, GetSQLScript( Q_INSERT_PCAR_ABO ) );
                                     with cdsTemporal do
                                     begin
                                          ParamAsString( FCursorGral, 'PR_TIPO', FPrestamo.FieldByName( 'PR_TIPO' ).AsString );
                                          ParamAsInteger( FCursorGral, 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                                          ParamAsDate( FCursorGral, 'CR_CAPTURA', Date  );
                                          ParamAsDate( FCursorGral, 'CR_FECHA', ValidaPrestamosCargosAbonos( Empresa, FPrestamo.FieldByName( 'PR_TIPO' ).AsString, FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'CM_FECHA' ).AsDateTime ) ); //FieldByName( 'CM_FECHA' ).AsDateTime  );
                                          ParamAsFloat( FCursorGral, 'CR_ABONO', FPrestamo.FieldByName( 'PR_SALDO' ).AsFloat );
                                          ParamAsFloat( FCursorGral, 'CR_CARGO', 0 );
                                          ParamAsString( FCursorGral, 'PR_REFEREN', FPrestamo.FieldByName( 'PR_REFEREN' ).AsString );
                                          ParamAsString( FCursorGral, 'CR_OBSERVA', sObservaciones );
                                          ParamAsInteger( FCursorGral, 'US_CODIGO', UsuarioActivo  );
                                     end;
                                     Ejecuta( FCursorGral );

                                     ParamAsString( FPrestamoUpdate, 'Tipo',    FPrestamo.FieldByName( 'PR_TIPO' ).AsString );
                                     ParamAsInteger( FPrestamoUpdate, 'Empleado',  FieldByName( 'CB_CODIGO' ).AsInteger );
                                     ParamAsString( FPrestamoUpdate, 'Referencia', FPrestamo.FieldByName( 'PR_REFEREN' ).AsString );

                                     Ejecuta(FPrestamoUpdate);

                                     Next;
                                end;
                           end;

                           { Prestamos de intereses }
                           AbreQueryScript( FPrestamo, Format( GetSQLScript( Q_PRESTAMO_LIQUIDA_INTERES ), [ cdsDataSet.FieldByName('CB_CODIGO').AsInteger,
                                                                                                             EntreComillas( cdsDataset.FieldByName( 'AH_TIPO' ).AsString ) ] ) );
                           with FPrestamo do
                           begin
                                while not EOF do
                                begin
                                     PreparaQuery( FCursorGral, GetSQLScript( Q_INSERT_PCAR_ABO ) );
                                     with cdsTemporal do
                                     begin
                                          ParamAsString( FCursorGral, 'PR_TIPO', FPrestamo.FieldByName( 'PR_TIPO' ).AsString );
                                          ParamAsInteger( FCursorGral, 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                                          ParamAsDate( FCursorGral, 'CR_CAPTURA', Date  );
                                          ParamAsDate( FCursorGral, 'CR_FECHA', ValidaPrestamosCargosAbonos( Empresa, FPrestamo.FieldByName( 'PR_TIPO' ).AsString, FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'CM_FECHA' ).AsDateTime ) ); //FieldByName( 'CM_FECHA' ).AsDateTime  );
                                          ParamAsFloat( FCursorGral, 'CR_ABONO', FPrestamo.FieldByName( 'PR_SALDO' ).AsFloat );
                                          ParamAsFloat( FCursorGral, 'CR_CARGO', 0 );
                                          ParamAsString( FCursorGral, 'PR_REFEREN', FPrestamo.FieldByName( 'PR_REFEREN' ).AsString );
                                          ParamAsString( FCursorGral, 'CR_OBSERVA', sObservaciones );
                                          ParamAsInteger( FCursorGral, 'US_CODIGO', UsuarioActivo  );
                                     end;
                                     Ejecuta( FCursorGral );

                                     ParamAsString( FPrestamoUpdate, 'Tipo',    FPrestamo.FieldByName( 'PR_TIPO' ).AsString );
                                     ParamAsInteger( FPrestamoUpdate, 'Empleado',  FieldByName( 'CB_CODIGO' ).AsInteger );
                                     ParamAsString( FPrestamoUpdate, 'Referencia', FPrestamo.FieldByName( 'PR_REFEREN' ).AsString );

                                     Ejecuta( FPrestamoUpdate );

                                     Next;
                                end;
                           end;
                        finally
                               FreeAndNil( FPrestamoUpdate );
                        end;
                     finally
                            FreeAndNil( FPrestamo );
                     end;
                  //finally
                         //FreeAndNil( FSaldo );
                  //end;
             end;
          finally
                 FreeAndNil( FCursorGral );
          end;
  end;
end;

function TdmServerCajaAhorro.GetCuentasMovtos( Empresa: OleVariant; const sFiltro: WideString ): OleVariant;
begin
     SetTablaInfo( eCtasMovimientos );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := sFiltro;
          Result := GetTabla( Empresa );
     end;
end;


function TdmServerCajaAhorro.GrabaCuentasMovtos(Empresa, Delta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          if  not VarIsNull( Delta )  then
          begin
               SetTablaInfo( eCtasMovimientos );
               Result := GrabaTabla( Empresa, Delta, ErrorCount );
          end
     end;

end;


function TdmServerCajaAhorro.GetAhorro(Empresa: OleVariant; Empleado: Integer; const TipoAhorro: WideString; out Prestamos,
  ACarAbo: OleVariant): OleVariant;
begin
     Result := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_AHORRO ),[ Empleado, EntreComillas( TipoAhorro ) ] ), True );

     Prestamos := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_PRESTAMO ),[ EntreComillas( TipoAhorro ), Empleado, Empleado ] ), True );

     with oZetaProvider do
     begin
          SetTablaInfo( eACarAbo );
          TablaInfo.Filtro := Format( 'CB_CODIGO = %d and AH_TIPO = %s',[Empleado,EntreComillas( TipoAhorro ) ] );
          ACarAbo := GetTabla( Empresa );
     end;

     SetComplete;
end;

function TdmServerCajaAhorro.GrabaSaldosAhorro(Empresa, Delta, DeltaACarAbo: OleVariant; var ErrorCount, ErrorCountCarAbo: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( Delta )  then
          begin
               SetTablaInfo( eAhorro );
               Result := GrabaTabla( Empresa, Delta, ErrorCount );
          end;

          if ( ErrorCount = 0 ) and not VarIsNull( DeltaACarAbo ) then
          begin
               SetTablaInfo( eACarAbo );
               Result := GrabaTabla( Empresa, DeltaACarAbo, ErrorCountCarAbo );
          end;
     end;
end;

procedure TdmServerCajaAhorro.AfterUpdateAhorros(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
 var
    eClase: eClaseBitacora;
    sTitulo: string;
begin
     if ( UpdateKind <> ukDelete ) then
     begin
          oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetSQLScript( Q_AFTER_UPDATE_AHORRO ), [ ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ),
                                                                                                               EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'AH_TIPO' ) ) ) ]  ) );
     end;

     if ( UpdateKind <> ukInsert ) then
     begin
          eClase  := clbHisAhorros;
          sTitulo := 'Ahorros:' +
                     ' Empleado:' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ) ) +
                     ' Tipo:' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'AH_TIPO' ) ) ;

          with oZetaProvider do
          begin
               if UpdateKind = ukModify then
                  CambioCatalogo( sTitulo, eClase, DeltaDS, ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ) )
               else
                   BorraCatalogo( sTitulo, eClase, DeltaDS, ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ) );
          end;
     end;

     if ( UpdateKind = ukInsert ) then
     begin
          oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetSQLScript( Q_AFTER_UPDATE_AHORRO_CAJA ), [ ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ),
                                                                                                                    EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'AH_TIPO' ) ) ) ]  ) );
     end;
end;

function TdmServerCajaAhorro.GrabaSaldosPrestamo(Empresa, Delta, DeltaPCarAbo, Parametros: OleVariant; out ErrorCount,
         ErrorCountCarAbo: Integer; ForzarGrabarPrestamos: WordBool;
         var MensajePrestamo: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          try
             if not VarIsNull( Delta )  then
             begin
                  SetTablaInfo( eRegPrestamos );
                  TablaInfo.BeforeUpdateRecord := BeforeUpdatePrestamos;
                  dmServerPrestamos.ZetaProvider.EmpresaActiva := EmpresaActiva;
                  dmServerPrestamos.ZetaProvider.AsignaParamList( oZetaProvider.ParamList.VarValues );
                  dmServerPrestamos.ForzarGrabaPrestamo := ForzarGrabarPrestamos;
                  Result := GrabaTabla( Empresa, Delta, ErrorCount );
             end;

             if ( ErrorCount = 0 ) and not VarIsNull( DeltaPCarAbo ) then
             begin
                  SetTablaInfo( ePCarAbo );
                  Result := GrabaTabla( Empresa, DeltaPCarAbo, ErrorCountCarAbo );
             end;
          finally
                 MensajePrestamo :=  dmServerPrestamos.MensajePrestamo; 
          end;
     end;
end;

procedure TdmServerCajaAhorro.AfterUpdatePrestamos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
 var
    eClase: eClaseBitacora;
    sTitulo: string;
begin
     if ( UpdateKind <> ukDelete ) then
     begin
          oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetSQLScript( Q_AFTER_UPDATE_PRESTAMO ), [ ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ),
                                                                                                                 EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PR_TIPO' ) ) ),
                                                                                                                 EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PR_REFEREN' ) ) ) ]  ) );

          oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetSQLScript( Q_AFTER_UPDATE_PRESTAMO_CTA_MOVS ), [ ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'PR_MONTO_S' ) ),
                                                                                                                          ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ),
                                                                                                                          EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PR_TIPO' ) ) ),
                                                                                                                          EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PR_REFEREN' ) ) ) ]  ) );

          if ( DeltaDS.FindField( 'AH_TIPO' ) <> NIL ) then
             oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetSQLScript( Q_AFTER_UPDATE_AHORRO ), [ ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ),
                                                                                                                  EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'AH_TIPO' ) ) ) ]  ) );

     end;

     if ( UpdateKind <> ukInsert ) then
     begin
          eClase  := clbHisPrestamos;
          sTitulo := 'Pr�stamo:' +
                     ' Empl.:' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ) ) +
                     ' Tipo:' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PR_TIPO' ) )+
                     ' Ref:' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PR_REFEREN' ) ) ;


          with oZetaProvider do
          begin
               if UpdateKind = ukModify then
                  CambioCatalogo( sTitulo, eClase, DeltaDS, ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ), sTitulo + CR_LF )
               else
                   BorraCatalogo( sTitulo, eClase, DeltaDS, ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ), sTitulo + CR_LF );
          end;
     end;
end;

function TdmServerCajaAhorro.GetTotalesCajaFondo(Empresa: OleVariant; const TipoAhorro: WideString; var Cuentas: OleVariant): OleVariant;
begin

     with cdsTotales do
     begin
          InitTempDataset;
          AddIntegerField( 'Cuantos_Socios' );
          AddFloatField( 'Total_Ahorrado' );
          AddIntegerField( 'Num_Prestamos' );
          AddFloatField( 'Saldo_Prestamos' );
          AddFloatField( 'Saldo_Bancos' );
          CreateTempDataset;

          with oZetaProvider do
          begin
               EmpresaActiva := Empresa;
               FCursorGral:= CreateQuery;
               AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_TOTAL_AHORRADO ), [ EntreComillas( TipoAhorro ) ] ) );

          end;
          Edit;
          FieldByName( 'Cuantos_Socios' ).AsInteger := FCursorGral.FieldByName( 'CUANTOS_SOCIOS' ).AsInteger;
          FieldByName( 'Total_Ahorrado' ).AsInteger := FCursorGral.FieldByName( 'TOTAL_AHORRADO' ).AsInteger;

          with oZetaProvider do
          begin
               AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_SALDO_PRESTAMOS ), [ EntreComillas( TipoAhorro ) ] ) );
          end;
          FieldByName( 'Num_Prestamos' ).AsFloat := FCursorGral.FieldByName( 'NUM_PRESTAMOS' ).AsFloat;
          FieldByName( 'Saldo_Prestamos' ).AsFloat := FCursorGral.FieldByName( 'SALDO_PRESTAMOS' ).AsFloat;

          with oZetaProvider do
          begin
               AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_SALDO_BANCOS ), [  DateToStrSQLC(Date),EntreComillas( TipoAhorro ) ] ) );
          end;
          FieldByName( 'Saldo_Bancos' ).AsFloat := FCursorGral.FieldByName( 'SALDO_BANCOS' ).AsFloat;
          Post;
     end;

     Cuentas := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_CUENTAS ), [ DateToStrSQLC(Date), EntreComillas( TipoAhorro ) ] ), True );


     Result := cdsTotales.Data;
end;

function TdmServerCajaAhorro.GetCuentasBancarias(Empresa, Parametros: OleVariant; var SaldoInicial: Double): OleVariant;
var
   oParams:TZetaParams;
   sFiltro:string;

begin
     oZetaProvider.EmpresaActiva := Empresa;
     oParams := TZetaParams.Create;
     try
        oParams.VarValues := Parametros;
        if(StrLleno(oParams.ParamByName('Tipo').AsString))then
        begin
             sFiltro := ' and CM_TIPO = '+EntreComillas(oParams.ParamByName('Tipo').AsString);
        end
        else sFiltro := '';

        with oZetaProvider do
        begin
             Result := OpenSQL(Empresa,Format( GetSQLScript( Q_MOVIMIENTOS), [ EntreComillas(oParams.ParamByName('Cuenta').AsString),
                                                                              EntreComillas(DateToStrSQL(oParams.ParamByName('FechaIni').AsDate)),
                                                                              EntreComillas(DateToStrSQL(oParams.ParamByName('FechaFin').AsDate)),
                                                                              oParams.ParamByName('Status').AsInteger,
                                                                              sFiltro
                                                                              ]), True );
             FCursorGral := CreateQuery;
             AbreQueryScript( FCursorGral, Format( GetSQLScript( Q_SALDO_CTA ), [ EntreComillas(oParams.ParamByName('Cuenta').AsString),
                                                                                  EntreComillas(DateToStrSQL(oParams.ParamByName('FechaIni').AsDate-1))] ) );
             SaldoInicial := FCursorGral.Fields[0].AsFloat;
             FreeAndNil(FCursorGral);
        end;
     Finally
            FreeAndNil(oParams);
     end;
end;



function TdmServerCajaAhorro.GetTotalesRetenciones(Empresa, Parametros: OleVariant): OleVariant;
var
   oParams, Resultado : TZetaParams;
   rSumaAhorro: TPesos;
   sFiltroTiposNomina: string;
   CursorTiposNomina: TZetaCursor;

begin
     try
        oParams := TZetaParams.Create;
        Resultado := TZetaParams.Create;
        oParams.VarValues := Parametros;

        //Obtener Tipos de Nomina
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             CursorTiposNomina:= CreateQuery;
             sFiltroTiposNomina := VACIO;
             try
                AbreQueryScript( CursorTiposNomina, Format( GetSQLScript ( Q_TIPOS_NOMINA ), [ EntreComillas( oParams.ParamByName( 'TipoAhorro' ).AsString ) ] ) );

                //Construir Filtro
                if ( not CursorTiposNomina.IsEmpty ) then
                begin
                     CursorTiposNomina.First;
                     sFiltroTiposNomina := 'PE_TIPO = ' + CursorTiposNomina.FieldByName( 'cb_nomina' ).AsString;
                     CursorTiposNomina.Next;
                     while not CursorTiposNomina.Eof do
                     begin
                          sFiltroTiposNomina := sFiltroTiposNomina + ' OR PE_TIPO = ' + CursorTiposNomina.FieldByName( 'cb_nomina' ).AsString;
                          CursorTiposNomina.Next;
                     end;
                end
                else
                begin
                     sFiltroTiposNomina := 'PE_TIPO = 1';
                end;
             finally
                    FreeAndNil( CursorTiposNomina );
             end;
        end;//with oZetaProvider do

        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             FCursorGral:= CreateQuery;

             with oParams do
                  AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_TOTALES_RENTENCION_AHORRO ), [ ParamByName( 'Year' ).AsInteger,
                                                                                                        sFiltroTiposNomina,
                                                                                                        ParamByName( 'Numero' ).AsInteger,
                                                                                                        ParamByName( 'ConceptoAhorro' ).AsInteger ] ) );
             with Resultado do
             begin
                  AddFloat( 'Num_Ahorro', FCursorGral.FieldByName( 'Num_Ahorro' ).AsFloat );
                  rSumaAhorro := FCursorGral.FieldByName( 'Suma_Ahorro' ).AsFloat ;
             end;

             with oParams do
             begin
                  if (ParamByName( 'ConceptoRelativo' ).AsInteger <> 0 ) then
                  begin
                       AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_TOTALES_RENTENCION_AHORRO ), [ ParamByName( 'Year' ).AsInteger,
                                                                                                             sFiltroTiposNomina,
                                                                                                             ParamByName( 'Numero' ).AsInteger,
                                                                                                             ParamByName( 'ConceptoRelativo' ).AsInteger ] ) );
                       rSumaAhorro := rSumaAhorro + FCursorGral.FieldBYName( 'Suma_Ahorro' ).AsFloat;
                  end;
             end;

             with Resultado do
                  AddFloat( 'Suma_Ahorro', rSumaAhorro  );

             with oParams do
                  AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_TOTALES_RENTENCION_PRESTAMO ), [ ParamByName( 'Year' ).AsInteger,
                                                                                                          sFiltroTiposNomina,
                                                                                                          ParamByName( 'Numero' ).AsInteger,
                                                                                                          ParamByName( 'ConceptoPrestamo' ).AsInteger,
                                                                                                          ParamByName( 'ConceptoInteres' ).AsInteger ] ) );
             with Resultado do
             begin
                  AddFloat( 'Num_Prestamos', FCursorGral.FieldByName( 'Num_Prestamos' ).AsFloat );
                  AddFloat( 'Suma_Prestamos', FCursorGral.FieldByName( 'Suma_Prestamos' ).AsFloat );
             end;

        end;
        Result := Resultado.VarValues;

     finally
            FreeAndNiL( Resultado );
            FreeAndNiL( oParams );
     end;
end;

function TdmServerCajaAhorro.GetPeriodos(Empresa: OleVariant; Year, Tipo,
  Filtro: Integer): OleVariant;
begin
     SetTablaInfo( ePeriodos );
     with oZetaProvider do
     begin
          if ( Filtro = 1 ) then //Filtrar a que no se vean las nominas afectadas
             TablaInfo.Filtro := Format( '( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_STATUS <> 6 )', [ Year, Tipo ] )
          else
              TablaInfo.Filtro := Format( '( PE_YEAR = %d ) and ( PE_TIPO = %d )', [ Year, Tipo ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;
function TdmServerCajaAhorro.GetCtasBancarias( Empresa: OleVariant): OleVariant;
begin
     SetComplete
end;

function TdmServerCajaAhorro.GetNominaEmpleado(Empresa: OleVariant; Empleado: Integer): Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FCursorGral:= CreateQuery;
          try
             AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_NOMINA_EMPLEADO ), [ Empleado ] ) );
             Result := FCursorGral.FieldByName( 'TU_NOMINA' ).AsInteger;
          finally
                 FreeAndNil( FCursorGral );
          end;
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.ValidaCheque(Empresa: OleVariant; const CtaBancaria: WideString; Cheque: Integer): Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FCursorGral:= CreateQuery;
          try
             AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_VALIDA_CHEQUE ), [ EntreComillas( CtaBancaria ), Cheque ] ) );
             Result := FCursorGral.FieldByName( 'TOTAL' ).AsInteger;
          finally
             FreeAndNil( FCursorGral );
          end;
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.GetTotalAhorro(Empresa: OleVariant; Empleado: Integer; const CajaAhorro, FondoAhorro: WideString; out Prestamos, ACarAbo: OleVariant): OleVariant;
begin
     Result := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_TOTAL_AHORRO ),[ Empleado ] ), True );
     Prestamos := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_TOTAL_PRESTAMO ),[ CajaAhorro,
                                                                                             FondoAhorro,
                                                                                             Empleado, Empleado ] ), True );
     with oZetaProvider do
     begin
          SetTablaInfo( eACarAbo );
          TablaInfo.Filtro := Format( 'CB_CODIGO = %d and AH_TIPO = %s',[ Empleado, EntreComillas( CajaAhorro ) ] );
          ACarAbo := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.GetTipoPrestamo(Empresa: OleVariant; Tipo: Integer): WideString;
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FDataset := CreateQuery( Format( GetSQLScript( Q_GET_TIPO_AHORRO ),[ Tipo ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  if EOF then
                     Result := VACIO
                  else
                      Result := FieldByName( 'TB_PRESTA' ).AsString;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.GetTipoInteres(Empresa: OleVariant;
  Tipo: Integer): WideString;
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FDataset := CreateQuery( Format( GetSQLScript( Q_GET_TIPO_INTERES ),[ Tipo ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  if EOF then
                     Result := VACIO
                  else
                      Result := FieldByName( 'TB_INTERES' ).AsString;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.GetTotalRepartoInteres(Empresa,
  Parametros: OleVariant): OleVariant;
var
   oParams, Resultado : TZetaParams;
   rCapital: TPesos;
   dInicio, dFin: TDate;
   iYear, iTipo, iNumero: integer;
   sTipoAhorro: string;
   CursorTiposNomina: TZetaCursor;
   sFiltroTiposNomina : string;
begin
     oParams := TZetaParams.Create;
     try
        Resultado := TZetaParams.Create;
        try
           //rCapital := 0;
           oParams.VarValues := Parametros;
           { Obtener los parametros enviados }
           with oParams do
           begin
                iYear := oParams.ParamByName( 'Year' ).AsInteger;
                iTipo :=  oParams.ParamByName( 'Tipo' ).AsInteger;
                iNumero :=  oParams.ParamByName( 'Numero' ).AsInteger;
                sTipoAhorro := oParams.ParamByName( 'TipoAhorro' ).AsString;
           end;
           { Fechas de Inicio y Fin }
           dInicio := FirstDayOfYear( iYear );
           dFin := LastDayOfYear( iYear );
           //Obtener Tipos de Nomina
           with oZetaProvider do
           begin
                EmpresaActiva := Empresa;
                CursorTiposNomina:= CreateQuery;
                sFiltroTiposNomina := VACIO;
                try
                   AbreQueryScript( CursorTiposNomina, Format( GetSQLScript ( Q_TIPOS_NOMINA ), [ EntreComillas( sTipoAhorro ) ] ) );

                   //Construir Filtro
                   if ( not CursorTiposNomina.IsEmpty ) then
                   begin
                        CursorTiposNomina.First;
                        sFiltroTiposNomina := 'PE_TIPO = ' + CursorTiposNomina.FieldByName('cb_nomina').AsString;
                        CursorTiposNomina.Next;
                        while not CursorTiposNomina.Eof do
                        begin
                             sFiltroTiposNomina := sFiltroTiposNomina + ' OR PE_TIPO = ' + CursorTiposNomina.FieldByName('cb_nomina').AsString;
                             CursorTiposNomina.Next;
                        end;
                   end
                   else
                   begin
                        sFiltroTiposNomina := 'PE_TIPO = ' + IntToStr( iTipo );
                   end;
                finally
                       FreeAndNil( CursorTiposNomina );
                end;
           end;//with oZetaProvider do

           with oZetaProvider do
           begin
                EmpresaActiva := Empresa;
                FCursorGral:= CreateQuery;
                try
                   //AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_TOTAL_INTERESXPRESTAMOS ), [ EntreComillas( sTipoAhorro ), iYear, iTipo, iNumero ] ) );
                   AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_TOTAL_INTERESXPRESTAMOS ), [ EntreComillas( sTipoAhorro ), iYear, sFiltroTiposNomina, iNumero ] ) );

                   Resultado.AddFloat( 'InteresesXPrestamos', FCursorGral.FieldBYName( 'TOTAL' ).AsFloat );

                   //AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_TOTAL_AHORRO_SEMANAL ), [ EntreComillas( sTipoAhorro ),iYear, iTipo, iNumero ] ) );
                   //rCapital := rCapital + FCursorGral.FieldByName('TOTAL').AsFloat;

                   AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_AHORRO_ACUMULADO ), [ EntreComillas( sTipoAhorro ) ] ) );
                   //rCapital := FCursorGral.FieldByName('TOTAL').AsFloat;

                   AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_INTERESES_ACUMULADO ), [ EntreComillas( sTipoAhorro ), EntreComillas( '%INVERSION%' ), EntreComillas( '%PRESTAMOS%' ), DateToStrSQLC( dInicio ), DateToStrSQLC( dFin ) ] ) );
                   //rCapital := rCapital + FCursorGral.FieldByName('TOTAL').AsFloat;

                   AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_INTERESES_COMISION ), [ EntreComillas( sTipoAhorro ), EntreComillas( '%COMISION%' ), DateToStrSQLC( dInicio ), DateToStrSQLC( dFin ) ] ) );
                   //rCapital := rCapital - FCursorGral.FieldByName('TOTAL').AsFloat;

                   AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_CAPITAL_TOTAL ), [ EntreComillas( sTipoAhorro ), 2008, 1, 1, 0, 0, 0, 0.0, 0.0, 0.0, 1.0 ] ) );
                   rCapital := FCursorGral.FieldByName( 'TOTAL_CAPITAL_TOTAL' ).AsFloat;

                   Resultado.AddFloat( 'Capital', rCapital );
                finally
                       FreeAndNil( FCursorGral );
                end;
           end;
           Result := Resultado.VarValues;
        finally
               FreeAndNil( Resultado );
        end;
     finally
            FreeAndNil( oParams );
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.GetDatosRepartoIntereses(Empresa, Parametros: OleVariant): OleVariant;
var
   oParams: TZetaParams;
begin
     oParams := TZetaParams.Create;
     try
        oParams.VarValues := Parametros;
        { Obtener los parametros enviados }
        with oParams do
        begin
             Result := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_DATOS_REPARTO_AHORRO ),[ EntreComillas( ParamByName( 'TipoAhorro' ).AsString ),
                                                                                                        ParamByName( 'Year' ).AsInteger,
                                                                                                        ParamByName( 'Tipo' ).AsInteger,
                                                                                                        ParamByName( 'Numero' ).AsInteger,
                                                                                                        FormatFloat( '0.#####', ParamByName( 'TasaInversion' ).AsFloat ),
                                                                                                        FormatFloat( '0.#####', ParamByName( 'TasaPrestamos' ).AsFloat ),
                                                                                                        FormatFloat( '0.#####', ParamByName( 'TasaComisiones' ).AsFloat ),
                                                                                                        FormatFloat( '0.##', ParamByName( 'MontoInversion' ).AsFloat ),
                                                                                                        FormatFloat( '0.##', ParamByName( 'MontoPrestamos' ).AsFloat ),
                                                                                                        FormatFloat( '0.##', ParamByName( 'MontoComisiones' ).AsFloat ),
                                                                                                        FormatFloat( '0.##', ParamByName( 'MontoCapital' ).AsFloat )
                                                                                                         ] ), True );
        end;
     finally
            FreeAndNil( oParams );
     end;
     SetComplete;
end;

procedure TdmServerCajaAhorro.GrabarIntereses(Empresa, Datos,
  Parametros: OleVariant);
var
   oParams: TZetaParams;
   dFechaInicio: TDate;
   FDataset: TZetaCursor;
   dAbonos, dCargos: currency;
   iInversion, iPrestamos, iComisiones: integer;
begin
     oParams := TZetaParams.Create;
     try
        oParams.VarValues := Parametros;
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             cdsTemporal.Data := Datos;
             FDataset:= CreateQuery;
             try
                AbreQueryScript( FDataset, Format( GetSQLScript ( Q_FECHA_INICIO_PERIODO ), [ oParams.ParamByName( 'Year' ).AsInteger,
                                                                                              oParams.ParamByName( 'Tipo' ).AsInteger,
                                                                                              oParams.ParamByName( 'Numero' ).AsInteger ] ) );
                dFechaInicio := FDataset.FieldBYName( 'PE_ASI_INI' ).AsDateTime;
             finally
                    FreeAndNil( FDataset );
             end;
             with cdsTemporal do
             begin
                  FCursorGral := CreateQuery;
                  First;
                  while not Eof do
                  begin
                       FDataset:= CreateQuery;
                       try
                          { Existe registro de ACAR_ABO de Inversion }
                          AbreQueryScript( FDataset, Format( GetSQLScript ( Q_DATOS_ACAR_ABO ), [ EntreComillas( oParams.ParamByName( 'TipoAhorro' ).AsString ),
                                                                                                  FieldByName( 'CB_CODIGO' ).AsInteger,
                                                                                                  DatetostrSQLC( dFechaInicio ) ] ) );
                          iInversion := FDataset.FieldBYName( 'TOTAL' ).AsInteger;

                          { Existe registro de ACAR_ABO de Prestamo }
                          AbreQueryScript( FDataset, Format( GetSQLScript ( Q_DATOS_ACAR_ABO ), [ EntreComillas( oParams.ParamByName( 'TipoAhorro' ).AsString ),
                                                                                                  FieldByName( 'CB_CODIGO' ).AsInteger,
                                                                                                  DatetostrSQLC( dFechaInicio + 1 ) ] ) );
                          iPrestamos := FDataset.FieldBYName( 'TOTAL' ).AsInteger;

                          { Existe registro de ACAR_ABO de Comisiones }
                          AbreQueryScript( FDataset, Format( GetSQLScript ( Q_DATOS_ACAR_ABO ), [ EntreComillas( oParams.ParamByName( 'TipoAhorro' ).AsString ),
                                                                                                  FieldByName( 'CB_CODIGO' ).AsInteger,
                                                                                                  DatetostrSQLC( dFechaInicio + 2 ) ] ) );
                          iComisiones := FDataset.FieldBYName( 'TOTAL' ).AsInteger;
                       finally
                              FreeAndNil( FDataset );
                       end;

                       if ( iInversion = 0 ) then
                       begin
                            { Se agregaran registros de Abono por los intereses calculados }
                            PreparaQuery( FCursorGral, GetSQLScript( Q_INSERT_ACAR_ABO ) );
                            { Intereses por INVERSION }
                            ParamAsString( FCursorGral, 'AH_TIPO', oParams.ParamByName( 'TipoAhorro' ).AsString );
                            ParamAsInteger( FCursorGral,'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                            ParamAsDate( FCursorGral, 'CR_CAPTURA', Date  );
                            ParamAsDate( FCursorGral, 'CR_FECHA', ValidaAhorrosCargosAbonos( Empresa, oParams.ParamByName( 'TipoAhorro' ).AsString, FieldByName( 'CB_CODIGO' ).AsInteger, dFechaInicio ) ); //dFechaInicio );
                            ParamAsFloat( FCursorGral, 'CR_ABONO', FieldByName( 'INTERESES_INV' ).AsFloat );
                            ParamAsFloat( FCursorGral, 'CR_CARGO', 0.0 );
                            ParamAsString( FCursorGral, 'CR_OBSERVA', 'INVERSION' );
                            ParamAsInteger( FCursorGral,'US_CODIGO', UsuarioActivo );
                            Ejecuta( FCursorGral );
                       end
                       else
                       begin
                            { Se agregaran registros de Abono por los intereses calculados }
                            PreparaQuery( FCursorGral, GetSQLScript( Q_UPDATE_ACAR_ABO ) );
                            { Intereses por INVERSION }
                            ParamAsDate( FCursorGral, 'CR_CAPTURA', Date  );
                            ParamAsFloat( FCursorGral, 'CR_ABONO', FieldByName( 'INTERESES_INV' ).AsFloat );
                            ParamAsFloat( FCursorGral, 'CR_CARGO', 0.0 );
                            ParamAsString( FCursorGral, 'CR_OBSERVA', 'INVERSION' );
                            ParamAsInteger( FCursorGral,'US_CODIGO', UsuarioActivo );
                            ParamAsString( FCursorGral, 'AH_TIPO', oParams.ParamByName( 'TipoAhorro' ).AsString );
                            ParamAsInteger( FCursorGral,'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                            ParamAsDate( FCursorGral, 'CR_FECHA', ValidaAhorrosCargosAbonos( Empresa, oParams.ParamByName( 'TipoAhorro' ).AsString, FieldByName( 'CB_CODIGO' ).AsInteger, dFechaInicio ) ); //dFechaInicio );
                            Ejecuta( FCursorGral );
                       end;

                       if ( iPrestamos = 0 ) then
                       begin
                            { Se agregaran registros de Abono por los intereses calculados }
                            PreparaQuery( FCursorGral, GetSQLScript( Q_INSERT_ACAR_ABO ) );
                            { Intereses por PRESTAMOS }
                            ParamAsString( FCursorGral, 'AH_TIPO', oParams.ParamByName( 'TipoAhorro' ).AsString );
                            ParamAsInteger( FCursorGral,'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                            ParamAsDate( FCursorGral, 'CR_CAPTURA', Date  );
                            ParamAsDate( FCursorGral, 'CR_FECHA', ValidaAhorrosCargosAbonos( Empresa, oParams.ParamByName( 'TipoAhorro' ).AsString, FieldByName( 'CB_CODIGO' ).AsInteger, dFechaInicio + 1 ) ); //dFechaInicio + 1 );
                            ParamAsFloat( FCursorGral, 'CR_ABONO', FieldByName( 'INTERESES_PRESTAMO' ).AsFloat );
                            ParamAsFloat( FCursorGral, 'CR_CARGO', 0.0 );
                            ParamAsString( FCursorGral, 'CR_OBSERVA', 'PRESTAMOS' );
                            ParamAsInteger( FCursorGral,'US_CODIGO', UsuarioActivo );
                            Ejecuta( FCursorGral );
                       end
                       else
                       begin
                            { Se agregaran registros de Abono por los intereses calculados }
                            PreparaQuery( FCursorGral, GetSQLScript( Q_UPDATE_ACAR_ABO ) );
                            { Intereses por INVERSION }
                            ParamAsDate( FCursorGral, 'CR_CAPTURA', Date  );
                            ParamAsFloat( FCursorGral, 'CR_ABONO', FieldByName( 'INTERESES_PRESTAMO' ).AsFloat );
                            ParamAsFloat( FCursorGral, 'CR_CARGO', 0.0 );
                            ParamAsString( FCursorGral, 'CR_OBSERVA', 'PRESTAMOS' );
                            ParamAsInteger( FCursorGral,'US_CODIGO', UsuarioActivo );
                            ParamAsString( FCursorGral, 'AH_TIPO', oParams.ParamByName( 'TipoAhorro' ).AsString );
                            ParamAsInteger( FCursorGral,'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                            ParamAsDate( FCursorGral, 'CR_FECHA', ValidaAhorrosCargosAbonos( Empresa, oParams.ParamByName( 'TipoAhorro' ).AsString, FieldByName( 'CB_CODIGO' ).AsInteger, dFechaInicio + 1 ) ); //dFechaInicio + 1 );
                            Ejecuta( FCursorGral );
                       end;
                       {.$ifdef FALSE}
                       if ( iComisiones = 0 ) then
                       begin
                            { Se agregaran registros de Abono por los intereses calculados }
                            PreparaQuery( FCursorGral, GetSQLScript( Q_INSERT_ACAR_ABO ) );
                            { Intereses por COMISIONES }
                            ParamAsString( FCursorGral, 'AH_TIPO', oParams.ParamByName( 'TipoAhorro' ).AsString );
                            ParamAsInteger( FCursorGral,'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                            ParamAsDate( FCursorGral, 'CR_CAPTURA', Date  );
                            ParamAsDate( FCursorGral, 'CR_FECHA', ValidaAhorrosCargosAbonos( Empresa, oParams.ParamByName( 'TipoAhorro' ).AsString, FieldByName( 'CB_CODIGO' ).AsInteger, dFechaInicio + 2 ) ); //dFechaInicio + 2 );
                            ParamAsFloat( FCursorGral, 'CR_ABONO', 0.0 );
                            ParamAsFloat( FCursorGral, 'CR_CARGO', FieldByName( 'MONTO_COMISIONES' ).AsFloat );
                            ParamAsString( FCursorGral, 'CR_OBSERVA', 'COMISIONES' );
                            ParamAsInteger( FCursorGral,'US_CODIGO', UsuarioActivo );
                            Ejecuta( FCursorGral );
                       end
                       else
                       begin
                            { Se agregaran registros de Abono por los intereses calculados }
                            PreparaQuery( FCursorGral, GetSQLScript( Q_UPDATE_ACAR_ABO ) );
                            { Intereses por INVERSION }
                            ParamAsDate( FCursorGral, 'CR_CAPTURA', Date  );
                            ParamAsFloat( FCursorGral, 'CR_ABONO', 0.0 );
                            ParamAsFloat( FCursorGral, 'CR_CARGO', FieldByName( 'MONTO_COMISIONES' ).AsFloat );
                            ParamAsString( FCursorGral, 'CR_OBSERVA', 'COMISIONES' );
                            ParamAsInteger( FCursorGral,'US_CODIGO', UsuarioActivo );
                            ParamAsString( FCursorGral, 'AH_TIPO', oParams.ParamByName( 'TipoAhorro' ).AsString );
                            ParamAsInteger( FCursorGral,'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                            ParamAsDate( FCursorGral, 'CR_FECHA', ValidaAhorrosCargosAbonos( Empresa, oParams.ParamByName( 'TipoAhorro' ).AsString, FieldByName( 'CB_CODIGO' ).AsInteger, dFechaInicio + 2 ) );//dFechaInicio + 2 );
                            Ejecuta( FCursorGral );
                       end;
                       {.$endif}
                       EscribeBitacora( tbNormal,
                                        clbMovCajaAhorro,
                                        FieldByName( 'CB_CODIGO' ).AsInteger,
                                        FechaDefault,
                                        'Intereses Registrados',
                                        Format( 'Se agreg� Intereses por inversi�n $%n Fecha: %s' + CR_LF +
                                                'Se agreg� Intereses por prestamos $%n Fecha: %s' + CR_LF +
                                                'Se agreg� Intereses por comisiones $%n Fecha: %s', [ FieldByName( 'INTERESES_INV' ).AsFloat,
                                                                                                      FechaCorta( dFechaInicio ),
                                                                                                      FieldByName( 'INTERESES_PRESTAMO' ).AsFloat,
                                                                                                      FechaCorta( dFechaInicio + 1 ),
                                                                                                      FieldByName( 'MONTO_COMISIONES' ).AsFloat,
                                                                                                      FechaCorta( dFechaInicio + 2 ) ] ) );
                       { Se actualizan los saldos del Ahorro }
                       cdsAhorro.Data := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_DATOS_AHORRO ), [ EntreComillas( oParams.ParamByName( 'TipoAhorro' ).AsString ),
                                                                                                                   FieldByName( 'CB_CODIGO' ).AsInteger ] ) , True );
                       FDataset:= CreateQuery;
                       try
                          PreparaQuery ( FDataset, GetSQLScript( Q_UPDATE_AHORRO ) );
                          ParamAsInteger( FDataset, 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                          ParamAsString( FDataset, 'AH_TIPO', oParams.ParamByName( 'TipoAhorro' ).AsString );
                          {
                          PreparaQuery ( FDataset, GetSQLScript( Q_UPDATE_AHORRO ) );
                          ParamAsInteger( FDataset, 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                          ParamAsInteger( FDataset, 'AH_STATUS', cdsAhorro.FieldByName( 'AH_STATUS' ).AsInteger );
                          ParamAsString( FDataset, 'AH_TIPO', oParams.ParamByName( 'TipoAhorro' ).AsString );
                          dAbonos := cdsAhorro.FieldByName( 'AH_ABONOS' ).AsFloat +
                                     FieldByName( 'INTERESES_INV' ).AsFloat +
                                     FieldByName( 'INTERESES_PRESTAMO' ).AsFloat;
                          ParamAsFloat( FDataset, 'AH_ABONOS', dAbonos );
                          dCargos := cdsAhorro.FieldByName( 'AH_CARGOS' ).AsFloat + FieldByName( 'MONTO_COMISIONES' ).AsFloat;
                          ParamAsFloat( FDataset, 'AH_CARGOS', dCargos );
                          ParamAsFloat( FDataset, 'AH_SALDO', cdsAhorro.FieldByName( 'AH_SALDO_I' ).AsFloat +
                                                              cdsAhorro.FieldByName( 'AH_TOTAL' ).AsFloat +
                                                              dAbonos - dCargos );
                          }
                          Ejecuta( FDataset );
                       finally
                              FreeAndNil( FDataset );
                       end;
                       Next;
                  end;
             end;
        end;
     finally
            FreeAndNil( oParams );
     end;
     Setcomplete;
end;

function TdmServerCajaAhorro.CancelaGetLista(Empresa,
  Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          Result := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_GET_LISTA_CANCELACION ), [ ParamList.ParamByName( 'Year' ).AsInteger,
                                                                                                       ParamList.ParamByName( 'Tipo' ).AsInteger,
                                                                                                       ParamList.ParamByName( 'Numero' ).AsInteger,
                                                                                                       EntreComillas( ParamList.ParamByName( 'TipoAhorro' ).AsString ) ] ), True );
     end;
     SetComplete;
end;

procedure TdmServerCajaAhorro.CancelaIntereses(Empresa,
  Parametros: OleVariant);
var
   FDataset: TZetaCursor;
begin
     cdsTemporal.Data := Parametros;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FCursorGral := CreateQuery;
          PreparaQuery( FCursorGral, GetSQLScript( Q_DELETE_ACAR_ABO ) );
          while not cdsTemporal.EOF do
          begin
               { Borrar Montos de Intereses Fecha Inicio del periodo }
               ParamAsString( FCursorGral, 'AH_TIPO', cdsTemporal.FieldByName( 'AH_TIPO' ).AsString );
               ParamAsDate( FCursorGral, 'CR_FECHA', cdsTemporal.FieldByName( 'CR_FECHA' ).AsDateTime );
               ParamAsInteger( FCursorGral,'CB_CODIGO', cdsTemporal.FieldByName( 'CB_CODIGO' ).AsInteger );
               Ejecuta( FCursorGral );

               ParamAsString( FCursorGral, 'AH_TIPO', cdsTemporal.FieldByName( 'AH_TIPO' ).AsString );
               ParamAsDate( FCursorGral, 'CR_FECHA', cdsTemporal.FieldByName( 'CR_FECHA' ).AsDateTime + 1 );
               ParamAsInteger( FCursorGral,'CB_CODIGO', cdsTemporal.FieldByName( 'CB_CODIGO' ).AsInteger );
               Ejecuta( FCursorGral );

               ParamAsString( FCursorGral, 'AH_TIPO', cdsTemporal.FieldByName( 'AH_TIPO' ).AsString );
               ParamAsDate( FCursorGral, 'CR_FECHA', cdsTemporal.FieldByName( 'CR_FECHA' ).AsDateTime + 2 );
               ParamAsInteger( FCursorGral,'CB_CODIGO', cdsTemporal.FieldByName( 'CB_CODIGO' ).AsInteger );
               Ejecuta( FCursorGral );

               FDataset:= CreateQuery;
               try
                  PreparaQuery ( FDataset, GetSQLScript( Q_UPDATE_AHORRO ) );
                  ParamAsInteger( FDataset, 'CB_CODIGO', cdsTemporal.FieldByName( 'CB_CODIGO' ).AsInteger );
                  ParamAsString( FDataset, 'AH_TIPO', cdsTemporal.FieldByName( 'AH_TIPO' ).AsString );

                  {
                  PreparaQuery ( FDataset, GetSQLScript( Q_UPDATE_AHORRO ) );
                  ParamAsInteger( FDataset, 'CB_CODIGO', cdsTemporal.FieldByName( 'CB_CODIGO' ).AsInteger );
                  ParamAsInteger( FDataset, 'AH_STATUS', cdsTemporal.FieldByName( 'AH_STATUS' ).AsInteger );
                  ParamAsString( FDataset, 'AH_TIPO', cdsTemporal.FieldByName( 'AH_TIPO' ).AsString );
                  ParamAsFloat( FDataset, 'AH_ABONOS', 0 );
                  ParamAsFloat( FDataset, 'AH_CARGOS', cdsTemporal.FieldByName( 'AH_CARGOS' ).AsFloat );
                  ParamAsFloat( FDataset, 'AH_SALDO', cdsTemporal.FieldByName( 'AH_SALDO_I' ).AsFloat +
                                                      cdsTemporal.FieldByName( 'AH_TOTAL' ).AsFloat - cdsTemporal.FieldByName( 'AH_CARGOS' ).AsFloat );
                  }
                  Ejecuta( FDataset );
               finally
                      FreeAndNil( FDataset );
               end;

               EscribeBitacora( tbNormal,
                                clbMovCajaAhorro,
                                cdsTemporal.FieldByName( 'CB_CODIGO' ).AsInteger,
                                FechaDefault,
                                'Intereses Cancelados',
                                Format( 'Se cancelaron Intereses por inversi�n y prestamo Fecha: %s', [ FechaCorta( cdsTemporal.FieldByName( 'CR_FECHA' ).AsDateTime ) ] ) );
               cdsTemporal.Next;
          end;
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.GetEmpleadosAval(Empresa: OleVariant;
  const TipoAhorro: WideString): OleVariant;
begin
     Result := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_EMPLEADOS_AVAL ), [ EntreComillas( TipoAhorro ) ] ), True );
     SetComplete;
end;

procedure TdmServerCajaAhorro.BeforeUpdatePrestamos(Sender: TObject;SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     dmServerPrestamos.ValidaReglasPrestamos(UpdateKind,DeltaDS );
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerCajaAhorro, Class_dmServerCajaAhorro, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}

end.



