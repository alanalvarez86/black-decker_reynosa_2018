unit DqueriesLAbor;

interface
uses
    SysUtils;

type
    eScripts = ( Q_LECTURAS_LEE,
                 Q_LECTURAS_BORRA,
                 Q_LECTURAS_PERIODO,
                 Q_TARJETA_AGREGA,
                 Q_TARJETA_LEE,
                 Q_WORKS_AGREGA,
                 Q_WORKS_LEE,
                 Q_WORKS_BORRA,
                 Q_WORKS_INSERTA,
                 Q_WORKS_DELETE_SIN_CEDULA,
                 Q_WORKS_DELETE_NO_APLICADOS,
                 Q_WORKS_DELETE_SISTEMA,
                 Q_WORKS_STD_TIME,
                 Q_EMPLEADO_PUESTO,
                 Q_EMPLEADO_INFO,
                 Q_CHECADAS_LEE,
                 Q_BREAKS_LEE,
                 Q_AREA_LEE,
                 Q_EMPLEADO_AREAS_LISTA,
                 Q_EMPLEADO_AREA,
                 Q_CEDULA_LEE_EMPLEADOS,
                 Q_CEDULA_WORKS_DELETE,
                 Q_CEDULA_WORKS_INSERT,
                 Q_CEDULA_LISTA,
                 Q_CEDULA_1_DIA,
                 Q_CEDULA_CUENTA,
                 Q_CEDULA_PIEZAS,
                 Q_CEDULA_SET_EMPLEADOS,
                 Q_CEDULA_LISTA_EMPLEADOS,
                 Q_BREAK_AGREGA,
                 Q_BREAK_AREA,
                 Q_BREAK_TIENE_AREAS,
                 Q_BREAK_HAY_TARJETA,
                 Q_TMPLABOR_START,
                 Q_TMPLABOR_INDIRECTOS,
                 Q_TMPLABOR_APLICAR,
                 Q_TMPLABOR_LISTAR,
                 Q_TMPLABOR_TARJETA,
                 Q_KARDEX_AREAS,
                 Q_LOTES_CEDULA,
                 Q_EXISTE_WORKS
                 );
function GetSQLScriptLabor( const eValor: eScripts ): String;

implementation
uses
    ZGlobalTress,
    ZetaCommonLists,
    ZetaCommonClasses,
    ZetaCommonTools;


function GetSQLScriptLabor( const eValor: eScripts ): String;
begin
     case eValor of
          Q_LECTURAS_LEE: Result := 'select CB_CODIGO, LX_FECHA, LX_HORA, LX_FOLIO, '+
                                           'LX_WORDER, LX_OPERA, LX_PIEZAS, '+
                                           'LX_MODULA1, LX_MODULA2, LX_MODULA3, LX_TMUERTO, '+
                                           'LX_NUMERO, LX_STATUS, LX_LINX_ID '+
                                           'from LECTURAS order by CB_CODIGO, LX_FECHA, LX_HORA, LX_FOLIO';
          Q_LECTURAS_BORRA: Result := 'delete from LECTURAS where '+
                                      '( CB_CODIGO = :Empleado) and '+
                                      '( LX_FECHA = :Fecha) and '+
                                      '( LX_HORA = :Hora ) and '+
                                      '( LX_OPERA = :Operacion ) and '+
                                      '( LX_WORDER = :Orden )';
          Q_LECTURAS_PERIODO: Result := 'select MIN( LX_FECHA ) MINIMA, MAX( LX_FECHA ) MAXIMA, COUNT(*) CUANTAS from LECTURAS';
          Q_TARJETA_LEE: Result := 'select HO_CODIGO, AU_STATUS, AU_HORASCK, AU_NUM_EXT, AU_HORAS, AU_AUT_EXT, AU_EXTRAS, AU_DOBLES, AU_TRIPLES, AU_DES_TRA from AUSENCIA where ( AU_FECHA = :Fecha ) and ( CB_CODIGO = :Empleado )';
          Q_WORKS_AGREGA: Result := 'insert into WORKS( ' +
                                    'CB_CODIGO, '+                                      'OP_NUMBER, '+                                         'WO_NUMBER, '+                                      'AR_CODIGO, '+
                                    'WK_CEDULA, '+                                      'WK_FECHA_R, '+                                        'WK_HORA_R, '+                                      'AU_FECHA, '+
                                    'WK_HORA_A, '+                                      'WK_LINX_ID, '+                                        'WK_TIPO, '+                                        'WK_STATUS, '+
                                    'WK_FOLIO, '+                                       'WK_PIEZAS, '+                                         'WK_MOD_1, '+                                       'WK_MOD_2, '+
                                    'WK_MOD_3, '+                                       'WK_TMUERTO, '+                                        'WK_MANUAL, '+                                      'CB_AREA, '+
                                    'CB_PUESTO '+                                       ') values ( '+                                         ':CB_CODIGO, '+                                     ':OP_NUMBER, '+
                                    ':WO_NUMBER, '+                                     ':AR_CODIGO, '+                                        ':WK_CEDULA, '+                                     ':WK_FECHA_R, '+
                                    ':WK_HORA_R, '+                                     ':AU_FECHA, '+                                         ':WK_HORA_A, '+                                     ':WK_LINX_ID, '+
                                    ':WK_TIPO, '+                                       ':WK_STATUS, '+                                        ':WK_FOLIO, '+                                      ':WK_PIEZAS, '+
                                    ':WK_MOD_1, '+                                      ':WK_MOD_2, '+                                         ':WK_MOD_3, '+                                      ':WK_TMUERTO, '+
                                    ':WK_MANUAL, '+                                     ':CB_AREA, '+                                          ':CB_PUESTO )';
          Q_WORKS_LEE: Result := 'select CB_CODIGO, OP_NUMBER, WO_NUMBER, AR_CODIGO, '+
                                 'AU_FECHA, WK_HORA_A, WK_FECHA_R, WK_HORA_R, WK_FOLIO, '+
                                 'WK_LINX_ID, WK_TIPO, WK_STATUS, WK_PIEZAS, WK_MANUAL, '+
                                 'WK_MOD_1, WK_MOD_2, WK_MOD_3, WK_TMUERTO, WK_TIEMPO, CB_AREA, CB_PUESTO, WK_CEDULA, WK_PRE_CAL '+
                                 'from WORKS where '+
                                 '( CB_CODIGO = :Empleado ) and '+
                                 '( AU_FECHA = :Fecha ) and '+
                                 '( ( WK_MANUAL = ' + ZetaCommonTools.EntreComillas( K_GLOBAL_SI ) + ' ) or '+
                                 '( ( WK_MANUAL <> ' + ZetaCommonTools.EntreComillas( K_GLOBAL_SI ) + ' ) and ( WK_CEDULA > 0 ) ) ) '+
                                 'order by WK_HORA_R, WK_FOLIO';
          Q_WORKS_BORRA: Result := 'delete from WORKS where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha )';
          Q_WORKS_INSERTA: Result := 'insert into WORKS( ' +
                                     'CB_CODIGO, '+
                                     'OP_NUMBER, '+
                                     'WO_NUMBER, '+
                                     'AR_CODIGO, '+
                                     'WK_FECHA_R, '+
                                     'WK_HORA_R, '+
                                     'AU_FECHA, '+
                                     'WK_HORA_A, '+
                                     'WK_HORA_I, '+
                                     'WK_LINX_ID, '+
                                     'WK_TIPO, '+
                                     'WK_STATUS, '+
                                     'WK_FOLIO, '+
                                     'WK_PIEZAS, '+
                                     'WK_MOD_1, '+
                                     'WK_MOD_2, '+
                                     'WK_MOD_3, '+
                                     'WK_TMUERTO, '+
                                     'WK_MANUAL, '+
                                     'WK_TIEMPO, '+
                                     'WK_PRE_CAL, '+
                                     'WK_HRS_ORD, '+
                                     'WK_HRS_2EX, '+
                                     'WK_HRS_3EX, '+
                                     'CB_PUESTO, '+
                                     'CB_AREA, '+
                                     'WK_CEDULA '+
                                     ') values ( '+
                                     ':CB_CODIGO, '+
                                     ':OP_NUMBER, '+
                                     ':WO_NUMBER, '+
                                     ':AR_CODIGO, '+
                                     ':WK_FECHA_R, '+
                                     ':WK_HORA_R, '+
                                     ':AU_FECHA, '+
                                     ':WK_HORA_A, '+
                                     ':WK_HORA_I, '+
                                     ':WK_LINX_ID, '+
                                     ':WK_TIPO, '+
                                     ':WK_STATUS, '+
                                     ':WK_FOLIO, '+
                                     ':WK_PIEZAS, '+
                                     ':WK_MOD_1, '+
                                     ':WK_MOD_2, '+
                                     ':WK_MOD_3, '+
                                     ':WK_TMUERTO, '+
                                     ':WK_MANUAL, '+
                                     ':WK_TIEMPO, '+
                                     ':WK_PRE_CAL, '+
                                     ':WK_HRS_ORD, '+
                                     ':WK_HRS_2EX, '+
                                     ':WK_HRS_3EX, '+
                                     ':CB_PUESTO, '+
                                     ':CB_AREA, '+
                                     ':WK_CEDULA )';
          Q_WORKS_DELETE_SIN_CEDULA: Result := 'delete from WORKS where ( AU_FECHA = :Fecha ) and ( WK_CEDULA > 0 ) and '+
                                               '( ( select COUNT(*) from CEDULA where ( CEDULA.CE_FOLIO = WORKS.WK_CEDULA ) ) = 0 )';
          Q_WORKS_DELETE_NO_APLICADOS: Result := 'delete from WORKS where ( AU_FECHA = :Fecha ) and '+
                                                 '( WK_MANUAL <> ''%0:s'' ) and '+
                                                 '( ( CB_CODIGO in ( select T.CB_CODIGO from TMPLABOR T where ( T.US_CODIGO = %1:d ) and ( ( T.CB_AREA = '''' ) or ( ( T.TL_APLICA <> ''%0:s'' ) and ( T.TL_CALCULA <> ''%0:s'' ) ) ) ) ) or '+
                                                 '( ( select COUNT(*) from TMPLABOR T where ( T.US_CODIGO = %1:d ) and ( T.CB_CODIGO = WORKS.CB_CODIGO ) ) = 0 ) )';
          Q_WORKS_DELETE_SISTEMA: Result := 'delete from WORKS where '+
                                            '( AU_FECHA = :Fecha ) and '+
                                            '( ( WK_MANUAL <> ''%0:s'' ) or ( WK_CEDULA <> 0 ) ) and '+
                                            '( CB_CODIGO in ( select T.CB_CODIGO from TMPLABOR T where ( T.US_CODIGO = %1:d ) and ( ( T.CB_AREA <> '''' ) or ( ( T.TL_APLICA = ''%0:s'' ) or ( T.TL_CALCULA = ''%0:s'' ) ) ) ) )';
          Q_WORKS_STD_TIME: Result := 'update WORKS set '+
                                      'WK_STD_HR = ( select %0:s.%2:s from %0:s where ( %0:s.%1:s = WORKS.%4:s ) ), '+
                                      'WK_STD_CST = ( select %0:s.%3:s from %0:s where ( %0:s.%1:s = WORKS.%4:s ) ) '+
                                      ' where '+
                                      '( WORKS.AU_FECHA = :Fecha ) and '+
                                      '( WORKS.%4:s <> '''' ) and '+
                                      '( ( select COUNT(*) from %0:s where ( %0:s.%1:s = WORKS.%4:s ) ) = 1 )';
          Q_CHECADAS_LEE: Result := 'select CH_TIPO, CH_H_REAL, CH_H_AJUS, CH_DESCRIP from CHECADAS where '+
                                    '( CB_CODIGO = :Empleado ) and '+
                                    '( AU_FECHA = :Fecha ) and '+
                                    Format( '( CH_TIPO in ( %d, %d ) ) and ', [  Ord( chEntrada ), Ord( chSalida ) ] ) +
                                    '( CH_SISTEMA <> ' + ZetaCommonTools.EntreComillas( K_GLOBAL_SI ) + ' ) '+
                                    'order by CH_H_AJUS';
          Q_BREAKS_LEE: Result := 'select BR_CODIGO, BH_INICIO, BH_TIEMPO, BH_MOTIVO from BRK_HORA order by BR_CODIGO, BH_INICIO';
          Q_AREA_LEE: Result := 'select TB_CODIGO, TB_BREAK_1, TB_BREAK_2, TB_BREAK_3, TB_BREAK_4, TB_BREAK_5, TB_BREAK_6, TB_BREAK_7, TB_OPERA, AR_SHIFT, AR_PRI_HOR, AR_TIPO from AREA order by TB_CODIGO';
          Q_EMPLEADO_AREAS_LISTA: Result := 'select CB_AREA, KA_FECHA, KA_HORA from SP_EMPLEADO_AREAS( :Empleado, :FechaInicial, :HoraInicial, :FechaFinal, :HoraFinal )';
          Q_EMPLEADO_AREA: Result := 'select CB_CODIGO, CB_AREA, CB_PUESTO from COLABORA where ( CB_CODIGO = :Empleado )';
          {$ifdef INTERBASE}
          Q_TARJETA_AGREGA: Result := 'select CB_CODIGO, AU_FECHA, HO_CODIGO, AU_STATUS, AU_AUT_EXT, US_CODIGO '+
                                      'from SP_ADD_AUSENCIA( :Empleado, :Fecha, :Horario, :Status )';
          Q_EMPLEADO_PUESTO: Result := 'select CB_PUESTO from SP_FECHA_KARDEX( :Fecha, :Empleado )';
          {$endif}
          {$ifdef MSSQL}
          Q_TARJETA_AGREGA: Result := 'execute procedure SP_ADD_AUSENCIA( :Empleado, :Fecha, :Horario, :Status, NULL, NULL, NULL, NULL, NULL, :US_CODIGO )';
          Q_EMPLEADO_PUESTO: Result := 'select CB_PUESTO = dbo.SP_KARDEX_CB_PUESTO( :Fecha, :Empleado )';
          {$endif}
          Q_CEDULA_LEE_EMPLEADOS: Result := 'select CB_CODIGO, CAPTURADO from SP_CEDULA_EMPLEADOS( :Cedula, :Fecha, :Hora, %d )';
          Q_CEDULA_WORKS_DELETE: Result := 'delete from WORKS where ( WK_CEDULA = :Cedula ) and ( AU_FECHA = :Fecha )';
          Q_CEDULA_WORKS_INSERT: Result := 'insert into WORKS( ' +
                                           'CB_CODIGO, '+
                                           'OP_NUMBER, '+
                                           'WO_NUMBER, '+
                                           'AR_CODIGO, '+
                                           'WK_FECHA_R, '+
                                           'WK_HORA_R, '+
                                           'AU_FECHA, '+
                                           'WK_HORA_A, '+
                                           'WK_LINX_ID, '+
                                           'WK_TIPO, '+
                                           'WK_STATUS, '+
                                           'WK_MOD_1, '+
                                           'WK_MOD_2, '+
                                           'WK_MOD_3, '+
                                           'WK_TMUERTO, '+
                                           'WK_PRE_CAL, '+
                                           'WK_MANUAL, '+
                                           'CB_AREA, '+
                                           'CB_PUESTO, '+
                                           'WK_FOLIO, '+
                                           'WK_PIEZAS, '+
                                           'WK_CEDULA '+
                                           ') values ( '+
                                           ':CB_CODIGO, '+
                                           ':OP_NUMBER, '+
                                           ':WO_NUMBER, '+
                                           ':AR_CODIGO, '+
                                           ':WK_FECHA_R, '+
                                           ':WK_HORA_R, '+
                                           ':AU_FECHA, '+
                                           ':WK_HORA_A, '+
                                           ':WK_LINX_ID, '+
                                           ':WK_TIPO, '+
                                           ':WK_STATUS, '+
                                           ':WK_MOD_1, '+
                                           ':WK_MOD_2, '+
                                           ':WK_MOD_3, '+
                                           ':WK_TMUERTO, '+
                                           ':WK_PRE_CAL, '+
                                           ':WK_MANUAL, '+
                                           ':CB_AREA, '+
                                           ':CB_PUESTO, '+
                                           ':WK_FOLIO, '+
                                           ':WK_PIEZAS, '+
                                           ':WK_CEDULA )';
          Q_CEDULA_LISTA: Result := 'select CE_FOLIO, WO_NUMBER, AR_CODIGO, OP_NUMBER, CE_AREA, CE_TIPO, '+
                                    'CE_FECHA, CE_HORA, CE_STATUS, CE_MOD_1, CE_MOD_2, CE_MOD_3, '+
                                    'CE_TMUERTO, CE_TIEMPO, CE_MULTI,CE_PIEZAS, US_CODIGO from CEDULA where '+
                                    '( CE_AREA = :Area ) and '+
                                    '( CE_FECHA >= :FechaInicial ) and '+
                                    '( CE_HORA >= :HoraInicial ) and '+
                                    '( CE_FECHA <= :FechaFinal ) and '+
                                    '( CE_HORA <= :HoraFinal ) and '+
                                    '( ( select COUNT(*) from CED_EMP where ( CED_EMP.CE_FOLIO = CEDULA.CE_FOLIO ) ) = 0 ) '+
                                    'order by CE_FECHA, CE_HORA';
          Q_CEDULA_1_DIA: Result := 'select CE_FOLIO, WO_NUMBER, AR_CODIGO, OP_NUMBER, CE_AREA, CE_TIPO, '+
                                    'CE_FECHA, CE_HORA, CE_STATUS, CE_MOD_1, CE_MOD_2, CE_MOD_3, CE_PIEZAS, '+
                                    'CE_TMUERTO, CE_TIEMPO, CE_MULTI, US_CODIGO, AR_SHIFT, AR_PRI_HOR, '+
                                    '( select COUNT(*) from CED_EMP where ( CED_EMP.CE_FOLIO = CEDULA.CE_FOLIO ) ) CE_EMPS '+
                                    'from CEDULA left outer join AREA on ( AREA.TB_CODIGO = CEDULA.CE_AREA )  where '+
                                    '( ( not ( AREA.AR_SHIFT in ( %0:d, %1:d ) ) ) and ( CE_FECHA = ''%2:s'' ) ) or '+
                                    '( ( AREA.AR_SHIFT = %0:d ) and '+
                                    '( ( ( CE_FECHA = ''%2:s'' ) and ( CE_HORA >= AREA.AR_PRI_HOR ) ) or '+
                                    '( ( CE_FECHA = ''%3:s'' ) and ( CE_HORA < AREA.AR_PRI_HOR ) ) ) ) or '+
                                    '( ( AREA.AR_SHIFT = %1:d ) and '+
                                    '( ( ( CE_FECHA = ''%2:s'' ) and ( CE_HORA < AREA.AR_PRI_HOR ) ) or '+
                                    '( ( CE_FECHA = ''%4:s'' ) and ( CE_HORA >= AREA.AR_PRI_HOR ) ) ) ) '+
                                    'order by CE_FOLIO';
          Q_CEDULA_CUENTA: Result := 'select COUNT(*) CUANTOS from CEDULA left outer join AREA on ( AREA.TB_CODIGO = CEDULA.CE_AREA ) where '+
                                     '( ( not ( AREA.AR_SHIFT in ( %0:d, %1:d ) ) ) and ( CE_FECHA >= ''%2:s'' ) and ( CE_FECHA <= ''%3:s'' ) ) or '+
                                     '( ( ( AREA.AR_SHIFT in ( %0:d, %1:d ) ) ) and ( CE_FECHA > ''%2:s'' ) and ( CE_FECHA < ''%3:s'' ) ) or '+
                                     '( ( AREA.AR_SHIFT = %0:d ) and '+
                                     '( ( ( CE_FECHA = ''%2:s'' ) and ( CE_HORA >= AREA.AR_PRI_HOR ) ) or '+
                                     '( ( CE_FECHA = ''%4:s'' ) and ( CE_HORA < AREA.AR_PRI_HOR ) ) ) ) or '+
                                     '( ( AREA.AR_SHIFT = %1:d ) and '+
                                     '( ( ( CE_FECHA = ''%3:s'' ) and ( CE_HORA < AREA.AR_PRI_HOR ) ) or '+
                                     '( ( CE_FECHA = ''%5:s'' ) and ( CE_HORA >= AREA.AR_PRI_HOR ) ) ) )';
          Q_CEDULA_PIEZAS: Result := 'execute procedure SP_CEDULA_PIEZAS( :Cedula )';
          Q_CEDULA_SET_EMPLEADOS: Result := 'update TMPLABOR set TL_CALCULA = ''%0:s'' where '+
                                            '( TL_CALCULA <> ''%0:s'' ) and '+
                                            '( TL_APLICA = ''%0:s'' ) and '+ //Que tengan checadas en Ausencia !! 
                                            '( US_CODIGO = %1:d ) and '+
                                            '( ( select COUNT(*) from CED_EMP where '+
                                            '( CED_EMP.CE_FOLIO = :Cedula ) and '+
                                            '( CED_EMP.CB_CODIGO = TMPLABOR.CB_CODIGO ) ) > 0 )';
          Q_CEDULA_LISTA_EMPLEADOS: Result := 'select CB_CODIGO from CED_EMP CE where '+
                                              '( CE.CE_FOLIO = :Cedula ) and '+
                                              '( ( select COUNT(*) from TMPLABOR T where ( T.US_CODIGO = %0:d ) and ( T.CB_CODIGO = CE.CB_CODIGO ) and ( T.CB_AREA <> '''' ) and ( T.TL_APLICA = ''%1:s'' ) ) > 0 ) '+
                                              'order by CB_CODIGO';
          Q_BREAK_AGREGA: Result := 'insert into WORKS( ' +
                                    'CB_CODIGO, '+
                                    'WK_FECHA_R, '+
                                    'WK_HORA_R, '+
                                    'AU_FECHA, '+
                                    'WK_HORA_A, '+
                                    'WK_TIPO, '+
                                    'WK_PRE_CAL, '+
                                    'WK_MANUAL '+
                                    ') values ( '+
                                    ':CB_CODIGO, '+
                                    ':WK_FECHA_R, '+
                                    ':WK_HORA_R, '+
                                    ':AU_FECHA, '+
                                    ':WK_HORA_A, '+
                                    ':WK_TIPO, '+
                                    ':WK_PRE_CAL, '+
                                    ':WK_MANUAL )';
          Q_BREAK_AREA: Result := 'select CB_AREA, AR_SHIFT, AR_PRI_HOR from SUP_AREA '+
                                  'left outer join AREA on ( AREA.TB_CODIGO = SUP_AREA.CB_AREA ) where '+
                                  '( US_CODIGO = %d ) and '+
                                  {$ifdef INTERBASE}
                                  '( CB_AREA = ( select CB_AREA from SP_FECHA_AREA( :Empleado, :Fecha, :Hora ) ) )';
                                  {$endif}
                                  {$ifdef MSSQL}
                                  '( CB_AREA = ( select DBO.SP_FECHA_AREA( :Empleado, :Fecha, :Hora ) as CB_AREA ) )';
                                  {$endif}
          Q_BREAK_TIENE_AREAS: Result := 'select COUNT(*) CUANTOS From SUP_AREA Where ( US_CODIGO = %d )';
          Q_BREAK_HAY_TARJETA: Result := 'select COUNT(*) CUANTOS from AUSENCIA where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha )';
          Q_TMPLABOR_START: Result := 'execute procedure SP_TMPLABOR_START( %d, :Fecha )';
          Q_TMPLABOR_INDIRECTOS: Result := 'select CB_CODIGO, CB_TURNO, TL_STATUS from TMPLABOR where '+
                                           '( US_CODIGO = %d ) and '+
                                           '( CB_AREA <> '''' ) and '+
                                           '( CB_CHECA <> ''%1:s'' ) and '+
                                           '( TL_APLICA <> ''%1:s'' ) order by CB_CODIGO';
          Q_TMPLABOR_APLICAR: Result := 'update TMPLABOR set TL_APLICA = ''%s'' where ( US_CODIGO = %d ) and ( CB_CODIGO = :Empleado )';
          Q_TMPLABOR_LISTAR: Result := 'select CB_CODIGO, CB_CHECA, CB_AREA, TL_APLICA, TL_CALCULA '+
                                       'from TMPLABOR where ( US_CODIGO = %d ) order by CB_CODIGO';
          Q_TMPLABOR_TARJETA: Result := 'select AU_STATUS from AUSENCIA where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha )';
          Q_KARDEX_AREAS: Result := 'select KA_FEC_MOV, KA_HOR_MOV, US_CODIGO from KAR_AREA WHERE ( CB_CODIGO = %d ) and ( KA_FECHA = :KA_FECHA ) and ( KA_HORA = :KA_HORA ) and ( CB_AREA = :CB_AREA )';
          Q_LOTES_CEDULA : Result := 'select CW.WO_NUMBER,CW.CW_PIEZAS,WO.AR_CODIGO,CW.CW_POSICIO FROM CED_WORD CW left outer join WORDER WO ON WO.WO_NUMBER = CW.WO_NUMBER where CE_FOLIO = :CE_FOLIO order by CW_POSICIO';
          Q_EXISTE_WORKS : Result := 'select W.WK_FOLIO from WORKS W '+
                                             ' where W.CB_CODIGO = :CB_CODIGO '+
                                             ' and W.AU_FECHA = :AU_FECHA '+
                                             ' and W.WK_HORA_A = :WK_HORA_A  '+
                                             ' and W.WK_TIPO = :WK_TIPO '+
                                             ' and  ( WK_CEDULA <> :WK_CEDULA ) ';
     end;
end;

end.
