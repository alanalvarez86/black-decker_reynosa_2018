library TimbradoCatalogos;

uses
  ComServ,
  TimbradoCatalogos_TLB in 'TimbradoCatalogos_TLB.pas',
  DServerCatalogosTimbrado in 'DServerCatalogosTimbrado.pas' {dmServerCatalogosTimbrado: TMtsDataModule} {dmServerCatalogosTimbrado: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
