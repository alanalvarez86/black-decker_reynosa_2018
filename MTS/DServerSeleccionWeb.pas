unit DServerSeleccionWeb;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, BdeMts, DataBkr, DBClient, MtsRdm, Mtx, DB,
     SeleccionWeb_TLB,
     Seleccion_TLB,
     DZetaServerProvider,
     ZetaXMLTools,
     ZetaCommonLists,
     ZetaSQLBroker,
     ZetaServerDataSet;

type
  TdmServerSeleccionWeb = class(TMtsDataModule, IdmServerSeleccionWeb)
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FXML: TZetaXML;
    oZetaProvider: TdmZetaServerProvider;
    FSeleccionObj: IdmServerSeleccion;
    function AgregaAdicional( Documento: TZetaXMLNode; const iGlobal: Integer; const sTipo, sCampo: String ): TZetaXMLNode;
    procedure AgregaAdicionalTabla( Documento: TZetaXMLNode; const iGlobal: Integer; const sTipo, sCampo: String );
    procedure ObtieneElementoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ListaFija( Campo: TField; const Lista: ListasFijas);
    procedure CampoRequierePuestoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure RequiereSexoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    function GetCompanys(const Parametros: WideString): WideString; safecall;
    function GetPuestosDisponibles(const Parametros: WideString): WideString; safecall;
    function GetCFGSolicitud(const Parametros: WideString): WideString; safecall;
    function GetOneCompany(const Parametros: WideString): WideString; safecall;
    function AgregaSolicitud(const Parametros: WideString): WideString; safecall;
    function GetRequisicion(const Parametros: WideString): WideString; safecall;
  public
    { Public declarations }
  end;

var
  dmServerSeleccionWeb: TdmServerSeleccionWeb;

implementation

uses ZetaServerTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZGlobalTress;

{$R *.DFM}

type
    eScripts = ( qCompanys,
                 qRequisiciones,
                 qAreasInteres,
                 qTablaAdicional,
                 qDetalleRequisicion );

function GetScript( const eSQLNumero: eScripts ): String;
begin
     case eSQLNumero of
          qCompanys: Result := 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_DATOS ' +
                               'from COMPANY where ( %s ) order by CM_CODIGO';
{
          qRequisiciones: Result := 'select R.RQ_FOLIO, R.RQ_TURNO, R.RQ_SEXO, '+
                                    'R.RQ_INGLES, R.RQ_EDADMIN, R.RQ_EDADMAX, '+
                                    'R.RQ_ESTUDIO, R.RQ_ANUNCIO, P.PU_HABILID, P.PU_ACTIVID '+
                                    'from REQUIERE R left outer join PUESTO P on ( P.PU_CODIGO = R.RQ_PUESTO ) '+
                                    'where ( R.RQ_STATUS = %d ) '+
                                    'and ( RQ_VACANTE > 0 ) '+
                                    'order by RQ_FOLIO';
}
          qRequisiciones: Result := 'select R.RQ_FOLIO, R.RQ_ANUNCIO, P.PU_DESCRIP, C.CL_NOMBRE '+
                                    'from REQUIERE R '+
                                    'left outer join PUESTO P on ( P.PU_CODIGO = R.RQ_PUESTO ) '+
                                    'left outer join CLIENTE C on ( C.CL_CODIGO = R.RQ_CLIENTE ) '+
                                    'where ( R.RQ_STATUS = %d ) and ( R.RQ_VACANTE > 0 ) '+
                                    'order by R.RQ_FOLIO';
          qAreasInteres: Result := 'select TB_CODIGO, TB_ELEMENT from AINTERES order by TB_ELEMENT';
          qTablaAdicional: Result := 'select TB_CODIGO, TB_ELEMENT from TABLA%2.2d order by TB_ELEMENT';
          qDetalleRequisicion: Result := 'select R.RQ_FOLIO, R.RQ_FEC_INI, R.RQ_VACANTE, R.RQ_TURNO,'+
                                         'R.RQ_ANUNCIO, RQ_SEXO, R.RQ_INGLES, R.RQ_EDADMIN, R.RQ_EDADMAX,'+
                                         'R.RQ_OFERTA, R.RQ_SUELDO, R.RQ_VACACI, R.RQ_AGUINAL, R.RQ_BONO, R.RQ_VALES,'+
                                         'C.CL_NOMBRE, P.PU_DESCRIP, P.PU_HABILID, P.PU_ACTIVID, A.TB_ELEMENT RQ_AREA '+
                                         'from REQUIERE R '+
                                         'left outer join PUESTO P on ( P.PU_CODIGO = R.RQ_PUESTO ) '+
                                         'left outer join CLIENTE C on ( C.CL_CODIGO = R.RQ_CLIENTE ) '+
                                         'left outer join AINTERES A on ( A.TB_CODIGO = R.RQ_AREA ) '+
                                         'where R.RQ_FOLIO = %d';
     else
         Result := VACIO;
     end;
end;

{ ***** TdmServerSeleccionWeb ******* }

class procedure TdmServerSeleccionWeb.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerSeleccionWeb.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerSeleccionWeb.MtsDataModuleDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

procedure TdmServerSeleccionWeb.ObtieneElementoGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if Sender.Dataset.IsEmpty then
             Text := ''
          else
              Text := ObtieneElemento( ListasFijas( Sender.Tag ), Sender.AsInteger );
     end
     else
         Text:= Sender.AsString;

end;

procedure TdmServerSeleccionWeb.ListaFija( Campo: TField; const Lista: ListasFijas);
begin
     with Campo do
     begin
          OnGetText := ObtieneElementoGetText;
          Alignment := taLeftJustify;
          Tag := Ord( Lista );
     end;
end;

function TdmServerSeleccionWeb.GetCompanys( const Parametros: WideString): WideString;
begin
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             ListarEmpresas( Documento, Format( GetScript( qCompanys ), [ ZetaServerTools.GetTipoCompany( Ord( tcRecluta ) ) ] ) );
             with Documento do
             begin
                  Result := GetXML;
             end;
        end;
     finally
            FreeAndNil( FXML );
     end;
     SetComplete;
end;

function TdmServerSeleccionWeb.GetOneCompany(const Parametros: WideString): WideString;
var
   sFiltro: String;
begin
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             sFiltro := Format( '( COMPANY.CM_CODIGO = ''%s'' )', [ Parametros ] );
             sFiltro := Format( GetScript( qCompanys ), [ sFiltro ] );
             ListarEmpresa( Documento.GetRoot, sFiltro );
             with Documento do
             begin
                  Result := GetXML;
             end;
        end;
     finally
            FreeAndNil( FXML );
     end;
     SetComplete;
end;

function TdmServerSeleccionWeb.GetPuestosDisponibles(const Parametros: WideString): WideString;
var
   FCursor: TZetaCursor;
begin
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if SetEmpresaActiva( Parametros ) then
             begin
                  try
                     FCursor := oZetaProvider.CreateQuery( Format( GetScript( qRequisiciones ), [ Ord( stPendiente ) ] ) );
                     try
                        try
                           with FCursor do
                           begin
                                Active := True;
                                FieldByName( 'RQ_FOLIO' ).DisplayLabel := 'Folio';
                                FieldByName( 'RQ_ANUNCIO' ).DisplayLabel := 'Anuncio';
                                with FieldByName( 'PU_DESCRIP' ) do
                                begin
                                     DisplayLabel := 'Puesto';
                                     OnGetText := CampoRequierePuestoGetText;
                                end;
{
                                FieldByName( 'RQ_TURNO' ).DisplayLabel := 'Turno';
                                FieldByName( 'RQ_SEXO' ).DisplayLabel := 'Sexo';
                                ListaFija( FieldByName( 'RQ_SEXO' ), lfSexo );
                                FieldByName( 'RQ_INGLES' ).DisplayLabel := 'Dominio Ingl�s';
                                FieldByName( 'RQ_EDADMIN' ).DisplayLabel := 'Edad M�nima';
                                FieldByName( 'RQ_EDADMAX' ).DisplayLabel := 'Edad M�xima';
                                FieldByName( 'RQ_ESTUDIO' ).DisplayLabel := 'Estudios M�nimos';
                                ListaFija( FieldByName( 'RQ_ESTUDIO' ), lfEstudios );
                                FieldByName( 'PU_HABILID' ).DisplayLabel := 'Habilidades Requeridas';
                                FieldByName( 'PU_ACTIVID' ).DisplayLabel := 'Actividades a Realizar';
                                AddDataset( Documento, 'puestos', 'puesto', FCursor );
}
                                AddDataset2( Documento, 'REQUISICIONES', FCursor );
                                Active := False;
                           end;
                        except
                              on Error: Exception do
                              begin
                                   BuildErrorXML( 'Error Al Leer Requisiciones: ' + Error.Message );
                              end;
                        end
                     finally
                            FreeAndNil( FCursor );
                     end;
                  except
                        on Error: Exception do
                        begin
                             BuildErrorXML( Error.Message );
                        end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := Documento.GetXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     SetComplete;
end;

procedure TdmServerSeleccionWeb.CampoRequierePuestoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if strVacio( Sender.AsString ) then
        Text := '[ Ver Detalle ]'
     else
        Text := Sender.AsString;
end;

function TdmServerSeleccionWeb.AgregaAdicional( Documento: TZetaXMLNode; const iGlobal: Integer; const sTipo, sCampo: String ): TZetaXMLNode;
var
   sLetrero: String;
begin
     sLetrero := oZetaProvider.GetGlobalString( iGlobal );
     if ZetaCommonTools.StrLleno( sLetrero ) then
     begin
          Result := Documento.NewChild( 'ADICIONAL', '' );
          Result.AddAttribute( K_LETRERO, sLetrero );
          Result.AddAttribute( 'Campo', sCampo );
          Result.AddAttribute( 'Tipo', sTipo );
     end
     else
         Result := nil;
end;

procedure TdmServerSeleccionWeb.AgregaAdicionalTabla( Documento: TZetaXMLNode; const iGlobal: Integer; const sTipo, sCampo: String );
const
     K_VALORES: WideString = 'VALORES';
     K_VALOR: WideString = 'VALOR';
var
   Tabla: TZetaXMLNode;
begin
     Tabla := AgregaAdicional( Documento, iGlobal, sTipo, sCampo );
     if Assigned( Tabla ) then
        FXML.AddLookup( Tabla, K_VALORES, K_VALOR, Format( GetScript( qTablaAdicional ), [ iGlobal - K_GLOBAL_TAB_BASE ] ) );
end;

function TdmServerSeleccionWeb.GetCFGSolicitud(const Parametros: WideString): WideString;
var
   eGender: eSexo;
   eEdo: eEdoCivil;
   eEscuela: eEstudios;
   Seccion, Elemento: TZetaXMLNode;
   FListaUsados: TStrings;
   i, iGlobal: Integer;
   sLetrero: String;
begin
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if SetEmpresaActiva( Parametros ) then
             begin
                  FListaUsados := TStringList.Create;
                  try
                     try
                        with oZetaProvider do
                        begin
                             InitGlobales;
                             FListaUsados.CommaText := GetGlobalString( ZGlobalTress.K_GLOBAL_POSICION_ADIC );
                        end;
                        { Sexos }
                        Seccion := Documento.NewChild( 'SEXOS', VACIO );
                        for eGender := Low( eSexo ) to High( eSexo ) do
                        begin
                             Elemento := Seccion.NewChild( 'SEXO', ZetaCommonLists.ObtieneElemento( lfSexo, Ord( eGender ) ) );
                             Elemento.AddAttribute( K_DESCRIPCION, ZetaCommonLists.ObtieneElemento( lfSexoDesc, Ord( eGender ) ) );
                        end;
                        { Estados Civiles }
                        Seccion := Documento.NewChild( 'ESTADOSCIVILES', VACIO );
                        for eEdo := Low( eEdoCivil ) to High( eEdoCivil ) do
                        begin
                             Elemento := Seccion.NewChild( 'EDOCIV', ZetaCommonLists.ObtieneElemento( lfEdoCivil, Ord( eEdo ) ) );
                             Elemento.AddAttribute( K_DESCRIPCION, ZetaCommonLists.ObtieneElemento( lfEdoCivilDesc, Ord( eEdo ) ) );
                        end;
                        { Escolaridades }
                        Seccion := Documento.NewChild( 'ESCOLARIDAD', VACIO );
                        for eEscuela := Low( eEstudios ) to High( eEstudios ) do
                        begin
                             Elemento := Seccion.NewChild( 'GRADO', Format( '%d', [ Ord( eEscuela ) ] ) );
                             Elemento.AddAttribute( K_DESCRIPCION, ZetaCommonLists.ObtieneElemento( lfEstudios, Ord( eEscuela ) ) );
                        end;
                        { Lookup de Areas de Interes }
                        AddLookup( Documento, 'INTERESES', 'AREA', GetScript( qAreasInteres ) );
                        { Adicionales }
                        Seccion := Documento.NewChild( 'ADICIONALES', VACIO );
                        with FListaUsados do
                        begin
                             for i := 0 to ( Count - 1 ) do
                             begin
                                  iGlobal := StrToIntDef( Strings[ i ], 0 );
                                  { Textos }
                                  if ( iGlobal > K_GLOBAL_TEXTO_BASE ) and ( iGlobal <= ( K_GLOBAL_TEXTO_BASE + K_GLOBAL_TEXTO_MAX ) ) then
                                  begin
                                       AgregaAdicional( Seccion, iGlobal, 'TEXTO', Format( 'SO_G_TEX_%d', [ iGlobal - K_GLOBAL_TEXTO_BASE ] ) );
                                  end
                                  else
                                  { N�meros }
                                  if ( iGlobal > K_GLOBAL_NUM_BASE ) and ( iGlobal <= ( K_GLOBAL_NUM_BASE + K_GLOBAL_NUM_MAX ) ) then
                                  begin
                                       AgregaAdicional( Seccion, iGlobal, 'NUMERO', Format( 'SO_G_NUM_%d', [ iGlobal - K_GLOBAL_NUM_BASE ] ) );
                                  end
                                  else
                                  { Booleanos }
                                  if ( iGlobal > K_GLOBAL_LOG_BASE ) and ( iGlobal <= ( K_GLOBAL_LOG_BASE + K_GLOBAL_LOG_MAX ) ) then
                                  begin
                                       AgregaAdicional( Seccion, iGlobal, 'LOGICO', Format( 'SO_G_LOG_%d', [ iGlobal - K_GLOBAL_LOG_BASE ] ) );
                                  end
                                  else
                                  { Fechas }
                                  if ( iGlobal > K_GLOBAL_FECHA_BASE ) and ( iGlobal <= ( K_GLOBAL_FECHA_BASE + K_GLOBAL_FECHA_MAX ) ) then
                                  begin
                                       AgregaAdicional( Seccion, iGlobal, 'FECHA', Format( 'SO_G_FEC_%d', [ iGlobal - K_GLOBAL_FECHA_BASE ] ) );
                                  end
                                  else
                                  { Tablas }
                                  if ( iGlobal > K_GLOBAL_TAB_BASE ) and ( iGlobal <= ( K_GLOBAL_TAB_BASE + K_GLOBAL_TAB_MAX ) ) then
                                  begin
                                       AgregaAdicionalTabla( Seccion, iGlobal, 'TABLA', Format( 'SO_G_TAB_%d', [ iGlobal - K_GLOBAL_TAB_BASE ] ) );
                                  end;
                             end;
                        end;
                        { Antecedentes }
                        Seccion := Documento.NewChild( 'ANTECEDENTES', VACIO );
                        Seccion.NewChild( 'CANTIDAD', Format( '%d', [ oZetaProvider.GetGlobalInteger( K_GLOBAL_REF_LABORAL_QTY ) ] ) );
                        for i := 1 to K_GLOBAL_REF_LABORAL_MAX do
                        begin
                             sLetrero := oZetaProvider.GetGlobalString( K_GLOBAL_REF_LABORAL_BASE + i );
                             if ZetaCommonTools.StrLleno( sLetrero ) then
                             begin
                                  Elemento := Seccion.NewChild( K_LETRERO, sLetrero );
                                  Elemento.AddAttribute( 'Campo', Format( 'RL_CAMPO%d', [ i ] ) );
                             end;
                        end;
                     except
                           on Error: Exception do
                           begin
                                BuildErrorXML( Error.Message );
                           end;
                     end;
                  finally
                         FreeAndNil( FListaUsados );
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     SetComplete;
end;

function TdmServerSeleccionWeb.AgregaSolicitud(const Parametros: WideString): WideString;
const
     K_FOLIO: WideString = 'FOLIO';
     K_TAG_REF_LABORAL = 20;
var
   oCandidatos, oExamenes, oRefLaboral, oDocumentos : OleVariant;
   cdsEditSolicita, cdsRefLaboral : TClientDataSet;
   ErrorCount, iFolio : Integer;

   function GrabaReferencias: Boolean;
   var
      i : Integer;
   begin
        Result := FALSE;
        i := 0;
        with cdsRefLaboral do
        begin
             First;
             while not EOF do
             begin
                  Inc(i);
                  if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                     Edit;
                  FieldByName( 'RL_FOLIO' ).AsInteger := i;
                  FieldByName( 'SO_FOLIO' ).AsInteger := iFolio;
                  Next;   // Se hace un Post Automatico
             end;
             if ( ChangeCount > 0 ) then   // Despues de Asignar Folios envia las referencias
                Result := VarIsNull( FSeleccionObj.GrabaTabla( oZetaProvider.EmpresaActiva, K_TAG_REF_LABORAL, Delta, ErrorCount ) );
        end;
   end;

begin
        FXML := TZetaXML.Create( oZetaProvider );
        try
           with FXML do
           begin
                if SetEmpresaActiva( Parametros ) then
                begin
                     try
                        FSeleccionObj := CreateComObject( CLASS_dmServerSeleccion ) as IdmServerSeleccion;
                        cdsEditSolicita  := TClientDataSet.Create( self );
                        cdsRefLaboral := TClientDataSet.Create( self );
                        try
                           // Obtiene Estructura de ClientDataSets
                           cdsEditSolicita.Data := FSeleccionObj.GetEditSolicitud( oZetaProvider.EmpresaActiva, 0, oCandidatos, oExamenes, oRefLaboral, oDocumentos );
                           cdsRefLaboral.Data := oRefLaboral;
                           // Traspasa XML a ClientDataSets
                           AgregaRegistro( cdsEditSolicita, 'SOLICITUD' );
                           AgregaTabla( cdsRefLaboral, 'ANTECEDENTES' );
                           with cdsEditSolicita do
                           begin
                                if ( State in [ dsEdit, dsInsert ] ) then
                                   Post;
                                if ( ChangeCount > 0 ) then
                                begin
                                     if VarIsNull( FSeleccionObj.GrabaSolicitud( oZetaProvider.EmpresaActiva, Delta,
                                                   ErrorCount, iFolio ) ) then
                                     begin
                                          if GrabaReferencias then
                                             Documento.NewChild( K_FOLIO, IntToStr( iFolio ) )
                                          else
                                             BuildErrorXML( 'No se Pudieron Grabar las Referencias Laborales' );
                                     end
                                     else
                                        BuildErrorXML( 'No se Pudo Grabar la Solicitud' );
                                end
                                else
                                     BuildErrorXML( 'No se Encontraron Cambios que Aplicar' );
                           end;
                        finally
                           FreeAndNil( cdsEditSolicita );
                           FreeAndNil( cdsRefLaboral );
                           FSeleccionObj := nil;
                        end;
                     except
                           on Error: Exception do
                           begin
                                BuildErrorXML( Error.Message );
                           end;
                     end;
                end;
                Result := GetDocXML;
           end;
        finally
           FreeAndNil( FXML );
        end;
{
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if SetEmpresaActiva( Parametros ) then
             begin
                  Documento.NewChild( K_FOLIO, IntToStr( 99999 ) );
             end;
             Result := Documento.GetXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
}
     SetComplete;
end;

function TdmServerSeleccionWeb.GetRequisicion(const Parametros: WideString): WideString;
var
   FCursor: TZetaCursor;
begin
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if SetEmpresaActiva( Parametros ) then
             begin
                  try
                     FCursor := oZetaProvider.CreateQuery( Format( GetScript( qDetalleRequisicion ), [ ParamAsInteger( 'FOLIO' ) ] ) );
                     try
                        try
                           with FCursor do
                           begin
                                Active := True;
                                FieldByName( 'RQ_SEXO' ).OnGetText := RequiereSexoGetText;
                                AddRegistro( Documento, 'REQUISICION', FCursor );
                                Active := False;
                           end;
                        except
                              on Error: Exception do
                              begin
                                   BuildErrorXML( 'Error Al Leer Detalle de Requisici�n: ' + Error.Message );
                              end;
                        end
                     finally
                            FreeAndNil( FCursor );
                     end;
                  except
                        on Error: Exception do
                        begin
                             BuildErrorXML( 'Error al Obtener Detalle de Requisici�n: ' + Error.Message );
                        end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := Documento.GetXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     SetComplete;
end;

procedure TdmServerSeleccionWeb.RequiereSexoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Trim( Sender.AsString ) = 'M' then
        Text := ObtieneElemento( lfSexoDesc, Ord( esMasculino ) )
     else
        Text := ObtieneElemento( lfSexoDesc, Ord( esFemenino ) );
end;

initialization
  TComponentFactory.Create(ComServer, TdmServerSeleccionWeb, Class_dmServerSeleccionWeb, ciMultiInstance, ZetaServerTools.GetThreadingModel);
end.