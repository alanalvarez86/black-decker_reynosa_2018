unit Tablas_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 3/31/2016 1:40:13 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2016\MTS\Tablas.tlb (1)
// LIBID: {6AA1CF60-88BC-11D3-8BBD-0050DA04EAC5}
// LCID: 0
// Helpfile: 
// HelpString: Tress Tablas
// DepndLst: 
//   (1) v1.0 Midas, (C:\Program Files (x86)\Embarcadero\RAD Studio\12.0\bin64\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  TablasMajorVersion = 1;
  TablasMinorVersion = 0;

  LIBID_Tablas: TGUID = '{6AA1CF60-88BC-11D3-8BBD-0050DA04EAC5}';

  IID_IdmServerTablas: TGUID = '{6AA1CF61-88BC-11D3-8BBD-0050DA04EAC5}';
  CLASS_dmServerTablas: TGUID = '{6AA1CF63-88BC-11D3-8BBD-0050DA04EAC5}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerTablas = interface;
  IdmServerTablasDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerTablas = IdmServerTablas;


// *********************************************************************//
// Interface: IdmServerTablas
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6AA1CF61-88BC-11D3-8BBD-0050DA04EAC5}
// *********************************************************************//
  IdmServerTablas = interface(IAppServer)
    ['{6AA1CF61-88BC-11D3-8BBD-0050DA04EAC5}']
    function GetTabla(Empresa: OleVariant; iTabla: Integer): OleVariant; safecall;
    function GrabaTabla(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; 
                        out ErrorCount: Integer): OleVariant; safecall;
    function GetNumerica(Empresa: OleVariant; Codigo: Integer; out oArt80: OleVariant): OleVariant; safecall;
    function GrabaNumerica(Empresa: OleVariant; oDelta: OleVariant; oArt80: OleVariant; 
                           out ErrorCount: Integer; out ErrorArt80: Integer; 
                           out oArt80Result: OleVariant): OleVariant; safecall;
    function GetPrioridadDescuento(Empresa: OleVariant): OleVariant; safecall;
    function GrabaPrioridadDescuento(Empresa: OleVariant; oDelta: OleVariant; 
                                     out ErrorCount: Integer): OleVariant; safecall;
    function GetUMA(Empresa: OleVariant): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerTablasDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6AA1CF61-88BC-11D3-8BBD-0050DA04EAC5}
// *********************************************************************//
  IdmServerTablasDisp = dispinterface
    ['{6AA1CF61-88BC-11D3-8BBD-0050DA04EAC5}']
    function GetTabla(Empresa: OleVariant; iTabla: Integer): OleVariant; dispid 1;
    function GrabaTabla(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; 
                        out ErrorCount: Integer): OleVariant; dispid 2;
    function GetNumerica(Empresa: OleVariant; Codigo: Integer; out oArt80: OleVariant): OleVariant; dispid 3;
    function GrabaNumerica(Empresa: OleVariant; oDelta: OleVariant; oArt80: OleVariant; 
                           out ErrorCount: Integer; out ErrorArt80: Integer; 
                           out oArt80Result: OleVariant): OleVariant; dispid 4;
    function GetPrioridadDescuento(Empresa: OleVariant): OleVariant; dispid 5;
    function GrabaPrioridadDescuento(Empresa: OleVariant; oDelta: OleVariant; 
                                     out ErrorCount: Integer): OleVariant; dispid 6;
    function GetUMA(Empresa: OleVariant): OleVariant; dispid 301;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerTablas provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerTablas exposed by              
// the CoClass dmServerTablas. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerTablas = class
    class function Create: IdmServerTablas;
    class function CreateRemote(const MachineName: string): IdmServerTablas;
  end;

implementation

uses ComObj;

class function CodmServerTablas.Create: IdmServerTablas;
begin
  Result := CreateComObject(CLASS_dmServerTablas) as IdmServerTablas;
end;

class function CodmServerTablas.CreateRemote(const MachineName: string): IdmServerTablas;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerTablas) as IdmServerTablas;
end;

end.
