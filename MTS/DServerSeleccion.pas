unit DServerSeleccion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, {BdeMts,} DataBkr, DBClient, Db,
  MtsRdm, Mtx,
  Variants,
{$ifndef DOS_CAPAS}
  Seleccion_TLB,
{$endif}
  DZetaServerProvider,
  ZetaCommonClasses,
  ZetaCommonLists,
  ZCreator,
  ZetaSQLBroker;

{$ifdef SELECCION}
{$INCLUDE ..\Seleccion\GlobalesSeleccion.inc}
{$endif}

type
  {CV:No mover los numerados, ya que en el cliente, se utiliza esta posicion}
  eTipoCatalogo = ( eAreaInteres, {0}
                    eClientes,    {1}
                    eTipoGasto,   {2}
                    ePuestos,     {3}
                    eCondiciones, {4}
                    eRequisicion, {5}
                    eSolicita,    {6}
                    eExamen,      {7}
                    eCandidato,   {8}
                    eReqGasto,    {9}
                    eEditSolicita,{10}
                    eEntrevista,  {11}
                    eReqCandidatos{12},
                    eAdicional1,  {13}
                    eAdicional2,  {14}
                    eAdicional3,  {15}
                    eAdicional4,  {16}
                    eAdicional5,  {17}
                    eMaxSolicita, {18}
                    eMaxRequiere, {19}
                    eRefLaboral,  {20}
                    eDocumentos,  {21}
                    eEstados,     {22}
                    eContratos,   {23}
                    eTransporte,  {24}
                    eReqChkRequisitos, {25}{OP:23.Abr.08}
                    eReqTextAdicional1, {26}{OP:23.Abr.08}
                    eReqTextAdicional2, {27}{OP:23.Abr.08}
                    eReqChkAdicional1, {28}{OP:23.Abr.08}
                    eReqChkAdicional2 );{OP:23.Abr.08}

  TdmServerSeleccion = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerSeleccion {$endif})
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FDataSet : TZetacursor;
    FOperacion: eOperacionConflicto;
    FHuboErrores: Boolean;
    FFolio: Integer;
    FListaReq : TStringList;
    FStatusSolCandidato: eSolStatus;{OP:18.Abr.08}
{$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
{$endif}
    function GetCatalogo(const eCatalogo: eTipoCatalogo;const Empresa: OleVariant): OLEVariant;
    function GetNombreTabla(eCatalogo: eTipoCatalogo): string;
    function GetScript( eCatalogo : eTipoCatalogo ) : string;
    function GetLastFolio( const Tipo: eTipoCatalogo ): Integer;
    function GetSQLBroker: TSQLBroker;
    function ObtenerCandidatosDataSet(Dataset: TDataset): OleVariant;
    procedure SetTablaInfo(eCatalogo: ETipoCatalogo);
    procedure BeforeUpdateSolicita(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure AfterUpdateSolicita(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure BeforeUpdateRequisicion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure AfterUpdateRequisicion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure BeforeUpdateBorraSolicitud(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateDocumento(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure AfterUpdateCandidato(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure InitCreator;
    procedure InitBroker;
    procedure ClearBroker;
    procedure GetSolicitudBuildDataset;
    procedure CalculaTotalesRequisicion(const iFolioReq: integer);
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    property SQLBroker: TSQLBroker read GetSQLBroker;
{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetRequiere(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEditRequiere(Empresa: OleVariant; Folio: Integer; out Candidatos, Gastos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEditSolicitud(Empresa: OleVariant; Folio: Integer; out Candidatos, Examenes, RefLaboral,Documentos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetSolicitud(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetCondiciones(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetPuestos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTabla(Empresa: OleVariant; iTabla: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEntrevista(Empresa: OleVariant; SolFolio, ReqFolio: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaTabla(Empresa: OleVariant; iTabla: Integer;oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaSolicitud(Empresa, oDelta: OleVariant; out ErrorCount, Folio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function BorraSolicitud(Empresa, oDelta: OleVariant; Operacion: Integer; out ErrorCount: Integer; out ErrorData: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaRequisicion(Empresa, oDelta: OleVariant; out ErrorCount, Folio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function BorraRequisicion(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function ObtenerCandidatos(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCandidato(Empresa, oDelta: OleVariant; iFolioReq: Integer; out iStatusSol, ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}{OP:18.Abr.08}
    function GrabaGasto(Empresa, oDelta: OleVariant; iFolioReq: Integer;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function Depuracion(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDocumento(Empresa: OleVariant; Folio: Integer; const Tipo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaDocumento(Empresa: OleVariant; lReemplaza: WordBool; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
  end;

const
     K_WHERE_CONFLICTO_CANDIDATO = 'and ( CD_STATUS in (2,3,5) )';
var
  dmServerSeleccion: TdmServerSeleccion;

implementation

uses ZetaCommonTools,
     ZetaServerTools,
     ZetaTipoEntidad,
     ZGlobalTress;

{$R *.DFM}

class procedure TdmServerSeleccion.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerSeleccion.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerSeleccion.MtsDataModuleDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

function TdmServerSeleccion.GetSQLBroker: TSQLBroker;
begin
{$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
{$else}
     Result := Self.oZetaSQLBroker;
{$endif}
end;

procedure TdmServerSeleccion.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerSeleccion.InitBroker;
begin
     InitCreator;
     oZetaCreator.RegistraFunciones([efComunes]);
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TdmServerSeleccion.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oZetaSQLBroker );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerSeleccion.CierraEmpresa;
begin
end;
{$endif}

function TdmServerSeleccion.GetScript( eCatalogo : eTipoCatalogo ) : string;
begin
     case eCatalogo of
          eSolicita : Result := 'select SO_FOLIO, %s as '+
                                'PrettyName,SO_ESTUDIO,SO_FEC_NAC,SO_INGLES,SO_SEXO,SO_AREA_1,SO_AREA_2,' +
                                'SO_AREA_3, SO_STATUS from SOLICITA %s order by SO_FOLIO';
          eEditSolicita : Result := 'select SO_FOLIO, SO_STATUS, SO_AREA_1, SO_AREA_2, SO_AREA_3, '+
                                    '%s AS PrettyName, ' +
                                    'SO_APE_PAT, SO_APE_MAT, SO_NOMBRES, SO_EDO_CIV, SO_ESTUDIO, '+
                                    'SO_FEC_INI, SO_FEC_NAC, SO_PUE_SOL, SO_FOTO, SO_INGLES, SO_DOMINA2, '+
                                    'SO_IDIOMA2, SO_SEXO, SO_TEL, SO_OBSERVA, '+
                                    'SO_G_FEC_1, SO_G_FEC_2, SO_G_FEC_3, SO_G_FEC_4, SO_G_FEC_5, '+
                                    'SO_G_LOG_1, SO_G_LOG_2, SO_G_LOG_3, SO_G_LOG_4, SO_G_LOG_5, '+
                                    'SO_G_NUM_1, SO_G_NUM_2, SO_G_NUM_3, SO_G_NUM_4, SO_G_NUM_5, '+
                                    'SO_G_TAB_1, SO_G_TAB_2, SO_G_TAB_3, SO_G_TAB_4, SO_G_TAB_5, '+
                                    'SO_G_TEX_1, SO_G_TEX_2, SO_G_TEX_3, SO_G_TEX_4, SO_G_TEX_5, '+
                                    'SO_REQUI_1, SO_REQUI_2, SO_REQUI_3, SO_REQUI_4, SO_REQUI_5, '+
                                    'SO_REQUI_6, SO_REQUI_7, SO_REQUI_8, SO_REQUI_9, ' +
                                    'SO_PAIS, SO_CURP, SO_RFC, SO_CALLE, SO_COLONIA, SO_CODPOST, ' +
                                    'SO_CIUDAD, SO_ESTADO, SO_NACION, SO_FEC_RES, SO_EMAIL, SO_ESCUELA, ' +
                                    'SO_G_NUM_6,SO_G_NUM_7,SO_G_NUM_8,SO_G_NUM_9,SO_G_NUM10, '+
                                    'SO_G_LOG_6,SO_G_LOG_7,SO_G_LOG_8,SO_G_LOG_9,SO_G_LOG10, '+
                                    'SO_G_TEX_6,SO_G_TEX_7,SO_G_TEX_8,SO_G_TEX_9,SO_G_TEX10, '+
                                    'SO_G_TEX11,SO_G_TEX12,SO_G_TEX13,SO_G_TEX14,SO_G_TEX15, '+
                                    'SO_G_TEX16,SO_G_TEX17,SO_G_TEX18,SO_G_TEX19,SO_G_TEX20, SO_MED_TRA, SO_NUM_EXT, SO_NUM_INT '+
                                    'from SOLICITA where ( SO_FOLIO = %d )';
          eCandidato : Result := 'select A.RQ_FOLIO, A.CD_STATUS, B.RQ_FEC_INI, B.RQ_VACANTE, B.RQ_PUESTO '+
                                 'from CANDIDAT A, REQUIERE B where '+
                                 '( A.RQ_FOLIO = B.RQ_FOLIO ) and '+
                                 '( A.SO_FOLIO = %d ) %s';
          eReqCandidatos : Result := 'select A.SO_FOLIO, A.RQ_FOLIO, A.CD_COMENT, A.CD_STATUS, ' +
                                     K_PRETTYNAME + ' as PRETTYNAME, '+
                                     'B.SO_SEXO, B.SO_ESTUDIO, B.SO_FEC_NAC, B.SO_STATUS, B.SO_INGLES ' +
                                     'from CANDIDAT A, SOLICITA B ' +
                                     'where ( A.SO_FOLIO = B.SO_FOLIO ) and ( A.RQ_FOLIO = %d )' +
                                     'order by A.SO_FOLIO';
          eMaxSolicita : Result := 'select MAX( SO_FOLIO ) RESULTADO from SOLICITA';
          eMaxRequiere : Result := 'select MAX( RQ_FOLIO ) RESULTADO from REQUIERE';
          eDocumentos : Result := 'select SO_FOLIO, DO_TIPO, DO_EXT, DO_OBSERVA from DOCUMENT where SO_FOLIO = %d';
          eReqChkRequisitos : Result := 'SELECT (GL_CODIGO - ( %0:d - 1 ) ) as GL_CODIGO FROM GLOBAL WHERE GL_CODIGO >= %0:d AND GL_CODIGO <= %d AND UPPER(GL_FORMULA) LIKE ''%s'' ';{OP:23.Abr.08}
          eReqTextAdicional1 : Result := 'SELECT ( GL_CODIGO - ( %0:d - 1 ) ) as GL_CODIGO FROM GLOBAL WHERE ( GL_FORMULA <> '''' ) AND ( GL_CODIGO >= %0:d AND GL_CODIGO <= %d )';{OP:23.Abr.08}
          eReqTextAdicional2 : Result := 'SELECT ( GL_CODIGO - ( ( %0:d - 1 ) - %1:d ) ) as GL_CODIGO FROM GLOBAL WHERE ( GL_FORMULA <> '''' ) AND ( GL_CODIGO >= %0:d AND GL_CODIGO <= %2:d )';{OP:23.Abr.08}
          eReqChkAdicional1 : Result := 'SELECT ( GL_CODIGO - ( %0:d - 1 ) ) as GL_CODIGO FROM GLOBAL WHERE ( GL_FORMULA <> '''' ) AND ( GL_CODIGO >= %0:d AND GL_CODIGO <= %d ) AND UPPER(GL_FORMULA) LIKE ''%s''';{OP:23.Abr.08}
          eReqChkAdicional2 : Result := 'SELECT ( GL_CODIGO - ( ( %d - 1 ) - %d ) ) as GL_CODIGO FROM GLOBAL WHERE ( GL_FORMULA <> '''' ) AND ( GL_CODIGO >= %d AND GL_CODIGO <= %d ) AND UPPER(GL_FORMULA) LIKE ''%s''';{OP:23.Abr.08}
     end;
end;

procedure TdmServerSeleccion.SetTablaInfo(eCatalogo: ETipoCatalogo);
begin
     with oZetaProvider.TablaInfo do
     begin
          BeforeUpdateRecord := nil;     // Por si tienen asignados estos eventos
          AfterUpdateRecord := nil;
          case eCatalogo of
               ePuestos     : SetInfo( 'PUESTO', 'PU_CODIGO,PU_DESCRIP,PU_INGLES,PU_NUMERO,PU_TEXTO,QU_CODIGO, '+
                                                 'PU_HABILID,PU_ACTIVID,PU_OFERTA,PU_AREA,PU_EDADMIN,PU_EDADMAX, '+
                                                 'PU_SEXO,PU_ESTUDIO,PU_DOMINA1,PU_SUELDO,PU_VACACI,PU_AGUINAL, '+
                                                 'PU_BONO,PU_VALES', 'PU_CODIGO' );
               eCondiciones : SetInfo( 'QUERYS', 'QU_CODIGO,QU_DESCRIP,QU_FILTRO,QU_NIVEL,US_CODIGO', 'QU_CODIGO' );
               eRequisicion : SetInfo( 'REQUIERE', 'RQ_FOLIO,RQ_FEC_INI,RQ_PUESTO,QU_CODIGO,RQ_CLIENTE,RQ_AREA,RQ_MOTIVO, '+
                                                   'RQ_TURNO,RQ_SEXO,RQ_INGLES,RQ_EDADMIN,RQ_EDADMAX,RQ_ESTUDIO, '+
                                                   'RQ_STATUS,RQ_GTOPRES,RQ_CONTRAT,RQ_FEC_PRO,RQ_FEC_FIN,RQ_OBSERVA, RQ_CANDIDA, '+
                                                   'RQ_AUTORIZ,RQ_ANUNCIO,RQ_VACANTE,RQ_PRIORID,RQ_OFERTA, RQ_GTOREAL,RQ_SUELDO, '+
                                                   'RQ_VACACI,RQ_AGUINAL,RQ_BONO,RQ_VALES,RQ_TIPO_CO,RQ_REEMPLA, RQ_PUES_AU, RQ_HABILID', 'RQ_FOLIO' );{OP:14.Abr.08}
               eSolicita    : SetInfo( 'SOLICITA', 'SO_FOLIO,SO_STATUS,SO_AREA_1,SO_AREA_2,SO_APE_MAT,SO_AREA_3, '+
                                                   K_PRETTYNAME  + ' AS PrettyName, ' +
                                                   'SO_APE_PAT,SO_EDO_CIV,SO_ESTUDIO,SO_FEC_INI,SO_FEC_NAC,SO_G_FEC_1, '+
                                                   'SO_G_FEC_2,SO_G_FEC_3,SO_G_LOG_1,SO_G_LOG_2,SO_G_LOG_3,SO_G_NUM_1, '+
                                                   'SO_G_NUM_2,SO_G_FEC_4,SO_G_FEC_5,SO_G_NUM_3,SO_G_TAB_1,SO_G_TAB_2, '+
                                                   'SO_G_TAB_3,SO_G_LOG_4,SO_G_TAB_4,SO_G_TEX_1,SO_G_LOG_5,SO_G_TEX_2, '+
                                                   'SO_G_TEX_3,SO_G_TEX_4,SO_NOMBRES,SO_G_NUM_4,SO_PUE_SOL,SO_G_NUM_5, '+
                                                   'SO_FOTO,SO_INGLES,SO_DOMINA2,SO_IDIOMA2,SO_G_TAB_5,SO_SEXO,SO_TEL, '+
                                                   'SO_EMAIL,SO_REQUI_1,SO_REQUI_2,SO_REQUI_3,SO_G_TEX_5,SO_REQUI_4,SO_REQUI_5, '+
                                                   'SO_REQUI_6,SO_REQUI_7,SO_REQUI_8,SO_REQUI_9,SO_OBSERVA,'+
                                                   'SO_PAIS,SO_CURP,SO_RFC,SO_CALLE,SO_COLONIA,SO_CODPOST,SO_CIUDAD,SO_ESTADO,'+
                                                   'SO_NACION,SO_FEC_RES,SO_ESCUELA,SO_G_NUM_6,SO_G_NUM_7,SO_G_NUM_8,'+
                                                   'SO_G_NUM_9,SO_G_NUM10,SO_G_LOG_6,SO_G_LOG_7,SO_G_LOG_8,SO_G_LOG_9,SO_G_LOG10,'+
                                                   'SO_G_TEX_6,SO_G_TEX_7,SO_G_TEX_8,SO_G_TEX_9,SO_G_TEX10,SO_G_TEX11,SO_G_TEX12,'+
                                                   'SO_G_TEX13,SO_G_TEX14,SO_G_TEX15,SO_G_TEX16,SO_G_TEX17,SO_G_TEX18,SO_G_TEX19,'+
                                                   'SO_G_TEX20, SO_MED_TRA, SO_NUM_EXT, SO_NUM_INT', 'SO_FOLIO' );
               eExamen      : SetInfo( 'EXAMEN', 'SO_FOLIO,EX_FOLIO,EX_FECHA,EX_TIPO,EX_APLICO,EX_RESUMEN,EX_EVALUA1,EX_EVALUA2, '+
                                                 'EX_EVALUA3,EX_OBSERVA,EX_APROBO','SO_FOLIO,EX_FOLIO');
               eCandidato   : SetInfo( 'CANDIDAT', 'RQ_FOLIO,SO_FOLIO,CD_COMENT,CD_STATUS','SO_FOLIO,RQ_FOLIO' );
               eReqGasto    : SetInfo( 'REQGASTO', 'RQ_FOLIO,RG_FOLIO,RG_TIPO,RG_FECHA,RG_DESCRIP,RG_COMENTA,RG_MONTO', 'RQ_FOLIO,RG_FOLIO' );
               eEntrevista  : SetInfo( 'ENTREVIS', 'RQ_FOLIO,SO_FOLIO,ER_FOLIO,ER_FECHA,ER_NOMBRE,ER_RESUMEN,ER_DETALLE,ER_RESULT','SO_FOLIO, ER_FOLIO');
               eRefLaboral  : SetInfo( 'REF_LAB', 'SO_FOLIO,RL_FOLIO,RL_CAMPO1,RL_CAMPO2,RL_CAMPO3,RL_CAMPO4,RL_CAMPO5,RL_CAMPO6,'+
                                                  'RL_CAMPO7,RL_CAMPO8,RL_CAMPO9,RL_CAMPO10, RL_CAMPO11, RL_CAMPO12, RL_CAMPO13, RL_CAMPO14, RL_CAMPO15,'+
                                                  'RL_CAMPO16, RL_CAMPO17, RL_CAMPO18, RL_CAMPO19, RL_CAMPO20, RL_EVAL1, RL_EVAL2, RL_EVAL3, RL_EVAL4, '+
                                                  'RL_EVAL5, RL_EVAL6, RL_EVAL7, RL_EVAL8, RL_EVAL9, RL_EVAL10, RL_EVAL11, RL_EVAL12, RL_EVAL13, '+
                                                  'RL_EVAL14, RL_EVAL15, RL_EVAL16, RL_EVAL17, RL_EVAL18, RL_EVAL19, RL_EVAL20','SO_FOLIO,RL_FOLIO' );{OP:22.Abr.08}
               eClientes : SetInfo( 'CLIENTE', 'CL_CODIGO,CL_NOMBRE,CL_DIRECC,CL_TEL,CL_FAX,CL_DESCRIP,CL_EMPLEAD,CL_INDUSTR,CL_NOM_C1,'+
                                               'CL_PUES_C1,CL_MAIL_C1,CL_DESC_C1,CL_NOM_C2,CL_PUES_C2,CL_MAIL_C2,CL_DESC_C2,CL_OBSERVA,CL_NOM_AU, CL_PUES_AU',{OP:14.Abr.08}
                                               'CL_CODIGO' );
               eDocumentos : SetInfo('DOCUMENT', 'SO_FOLIO,DO_TIPO,DO_EXT,DO_BLOB,DO_OBSERVA','SO_FOLIO,DO_TIPO');
          else
              { AreadeInteres, TipoGasto, Adicionales }
              SetInfo( GetNombreTabla( eCatalogo ), 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO', 'TB_CODIGO' );
          end;
     end;
end;

function TdmServerSeleccion.GetNombreTabla(eCatalogo : eTipoCatalogo) : string;
begin
     case eCatalogo of
          eAreaInteres: Result := 'AINTERES';
          eClientes: Result := 'CLIENTE';
          eTipoGasto: Result := 'TGASTO';
          eAdicional1: Result := 'TABLA01';
          eAdicional2: Result := 'TABLA02';
          eAdicional3: Result := 'TABLA03';
          eAdicional4: Result := 'TABLA04';
          eAdicional5: Result := 'TABLA05';
          eEstados:    Result := 'ENTIDAD';
          eContratos:  Result := 'CONTRATO';
          eTransporte: Result := 'TRANSPOR';
     else
         Result := '';
     end;
end;

function TdmServerSeleccion.GetLastFolio( const Tipo: eTipoCatalogo ): Integer;
var
   FGeneral: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FGeneral := CreateQuery( GetScript( Tipo ) );
          try
             with FGeneral do
             begin
                  Active := True;
                  if IsEmpty then
                     Result := 0
                  else
                     Result := FieldByName( 'RESULTADO' ).AsInteger;
                  Active := FALSE;
             end;
          finally
             FreeAndNil( FGeneral );
          end;
     end;
end;

function TdmServerSeleccion.GetCatalogo( const eCatalogo: eTipoCatalogo; const Empresa: OleVariant ): OLEVariant;
begin
     SetTablaInfo( eCatalogo );
     with oZetaProvider do
     begin
          Result := GetTabla( Empresa );
     end;
end;

function TdmServerSeleccion.GetCondiciones(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eCondiciones, Empresa );
     SetComplete;
end;

function TdmServerSeleccion.GetPuestos(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( ePuestos, Empresa );
     SetComplete;
end;

function TdmServerSeleccion.GetTabla(Empresa: OleVariant; iTabla: Integer): OleVariant;
begin
     Result := GetCatalogo( eTipoCatalogo( iTabla ), Empresa );
     SetComplete;
end;

function TdmServerSeleccion.GetSolicitud( Empresa, Parametros: OleVariant ): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        GetSolicitudBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
        ClearBroker;
     end;
     SetComplete;
end;

procedure TdmServerSeleccion.GetSolicitudBuildDataset;

   function GetFecha( const iAnno: Integer ): String;
   begin
        Result := DateToStrSQLC( DateTheYear( Date, TheYear( Date ) - iAnno ) );
   end;

   {OP:15.Abr.08}
   function ObtenChkRequisitos( const sOtrasHab : string ): string;
   var
      sFiltro : string;
      iCodigo : Integer;
   begin
        with oZetaProvider do
        begin
             sFiltro := VACIO;
             FDataSet := CreateQuery;
             try
                try
                   AbreQueryScript(FDataSet, Format( GetScript( eReqChkRequisitos ), [ K_GLOBAL_REQUISITO1, K_GLOBAL_REQUISITO9, '%' + sOtrasHab + '%' ] ));
                   with FDataSet do
                   begin
                        while not EOF do
                        begin
                             iCodigo := FieldByName( 'GL_CODIGO' ).AsInteger;
                             sFiltro := ConcatString( sFiltro, Format( 'UPPER( SO_REQUI%s ) = ''%s''', [ PadLCar( IntToStr( iCodigo ), 2, '_' ), K_GLOBAL_SI ] ), ' OR ' );
                             Next;
                        end;
                   end;
                except
                      on Error: Exception do
                      begin
                           Log.Excepcion( 0, 'Error En Consulta De Requisitos', Error );
                      end;
                end;
             finally
                    FreeAndNil( FDataSet );
             end;
             if sFiltro <> VACIO then
                sFiltro := ' OR ( ' + sFiltro + ' ) ';
             Result := sFiltro;
        end;
   end;

   {OP:15.Abr.08}
   function ObtenTextAdicionales( const sOtrasHab : string ): string;
   var
      sFiltro : string;
      iCodigo : Integer;

      procedure ObtenFiltroTextAdicional( const sQuery: String );
      begin
           with oZetaProvider do
           begin
                FDataSet := CreateQuery;
                try
                   try
                      AbreQueryScript( FDataSet, sQuery );
                      with FDataSet do
                      begin
                           while not EOF do
                           begin
                                iCodigo := FieldByName( 'GL_CODIGO' ).AsInteger;
                                sFiltro := ConcatString( sFiltro, Format('UPPER( SO_G_TEX%s ) LIKE ''%s''',[ PadLCar( IntToStr( iCodigo ), 2, '_' ), '%' + sOtrasHab + '%' ] ), ' OR ' );
                                Next;
                           end;
                      end;
                   except
                         on Error: Exception do
                         begin
                              Log.Excepcion( 0, 'Error En Consulta De Textos Adicionales', Error );
                         end;
                   end;
                finally
                       FreeAndNil( FDataSet );
                end;
           end;
      end;

   begin
     sFiltro := VACIO;
     ObtenFiltroTextAdicional( Format( GetScript( eReqTextAdicional1 ), [ K_GLOBAL_TEXTO1, K_GLOBAL_TEXTO5 ] ) );
     ObtenFiltroTextAdicional( Format( GetScript( eReqTextAdicional2 ), [ K_GLOBAL_TEXTO6, K_GLOBAL_NUM_MAX2, K_GLOBAL_TEXTO20 ] ) );
     if sFiltro <> VACIO then
        sFiltro := ' OR ( ' + sFiltro + ' ) ';
     Result := sFiltro;
   end;

   {OP:15.Abr.08}
   function ObtenChkAdicionales( const sOtrasHab : string ): string;
   var
      sFiltro : string;
      iCodigo : Integer;

      procedure ObtenFiltroChkAdicional( const sQuery: String );
      begin
           with oZetaProvider do
           begin
                FDataSet := CreateQuery;
                try
                   try
                      AbreQueryScript( FDataSet, sQuery );
                      with FDataSet do
                      begin
                           while not EOF do
                           begin
                                iCodigo := FieldByName( 'GL_CODIGO' ).AsInteger;
                                sFiltro := ConcatString( sFiltro, Format( 'UPPER( SO_G_LOG%s ) = ''%s''', [ PadLCar( IntToStr( iCodigo ), 2, '_' ), K_GLOBAL_SI ] ), ' OR ' );
                                Next;
                           end;
                      end;
                   except
                         on Error: Exception do
                         begin
                              Log.Excepcion( 0, 'Error En Consulta De Campos L�gicos Adicionales', Error );
                         end;
                   end;
                finally
                       FreeAndNil( FDataSet );
                end;
           end;
      end;

   begin
       sFiltro := VACIO;
       ObtenFiltroChkAdicional( Format( GetScript( eReqChkAdicional1 ), [ K_GLOBAL_LOG1, K_GLOBAL_LOG5, '%' + sOtrasHab + '%' ] ) );
       ObtenFiltroChkAdicional( Format( GetScript( eReqChkAdicional2 ), [ K_GLOBAL_LOG6, K_GLOBAL_LOG_MAX2, K_GLOBAL_LOG6, K_GLOBAL_LOG10, '%' + sOtrasHab + '%' ] ) );
       if sFiltro <> VACIO then
          sFiltro := ' OR ( ' + sFiltro + ' ) ';
       Result := sFiltro;
   end;

   function GetCampo( const sCampo: String ): String;
   begin
        {$ifdef DOS_CAPAS}
        Result := Format( 'UPPER( %s )', [ sCampo ] );
        {$else}
        Result := sCampo;
        {$endif}
   end;

var
   sHabilid, sNotas: String;
begin
     with SQLBroker do
     begin
          Init( enSolicitud );
          with Agente do
          begin
               AgregaColumna( 'SO_FOLIO', TRUE, Entidad, tgNumero, 0, 'SO_FOLIO' );
               AgregaColumna( K_PRETTYNAME, TRUE, Entidad, tgTexto, 50, 'PRETTYNAME' );
               AgregaColumna( 'SO_ESTUDIO', TRUE, Entidad, tgNumero, 0, 'SO_ESTUDIO' );
               AgregaColumna( 'SO_FEC_NAC', TRUE, Entidad, tgFecha, 0, 'SO_FEC_NAC' );
               AgregaColumna( 'SO_INGLES', TRUE, Entidad, tgNumero, 0, 'SO_INGLES' );
               AgregaColumna( 'SO_SEXO', TRUE, Entidad, tgTexto, 1, 'SO_SEXO' );
               AgregaColumna( 'SO_AREA_1', TRUE, Entidad, tgTexto, 6, 'SO_AREA_1' );
               AgregaColumna( 'SO_AREA_2', TRUE, Entidad, tgTexto, 6, 'SO_AREA_2' );
               AgregaColumna( 'SO_AREA_3', TRUE, Entidad, tgTexto, 6, 'SO_AREA_3' );
               AgregaColumna( 'SO_STATUS', TRUE, Entidad, tgNumero, 0, 'SO_STATUS' );

               with oZetaProvider.ParamList do
               begin
                    if ( ParamByName( 'Status' ).AsInteger >= 0 ) then
                       AgregaFiltro( Format( '( SO_STATUS = %d )',
                                     [ ParamByName( 'Status' ).AsInteger ] ),
                                     TRUE, Entidad )
                    else if ( ParamByName( 'Status' ).AsInteger = -1 ) then             // Disponible/En Tr�mite
                       AgregaFiltro( Format( '( SO_STATUS in ( %d, %d ))', [ Ord( stDisponible ),
                       Ord( stEnTramite ) ] ), TRUE, Entidad );
                    if ( ParamByName( 'EdadMinima' ).AsInteger > 0 ) then
                       AgregaFiltro( Format( '( SO_FEC_NAC <= %s )',
                                     [ GetFecha( ParamByName( 'EdadMinima' ).AsInteger ) ] ),
                                     TRUE, Entidad );
                    if ( ParamByName( 'EdadMaxima' ).AsInteger > 0 ) then
                       AgregaFiltro( Format( '( SO_FEC_NAC > %s )',
                                     [ GetFecha( ParamByName( 'EdadMaxima' ).AsInteger + 1) ] ),
                                     TRUE, Entidad );
                    if ( ParamByName( 'Sexo' ).AsInteger >= 0 ) then
                       AgregaFiltro( Format( '( SO_SEXO = ''%s'' )',
                                     [ ObtieneElemento( lfSexo, ParamByName( 'Sexo' ).AsInteger ) ] ),
                                     TRUE, Entidad );
                    if ( ParamByName( 'MinEstudios' ).AsInteger >= 0 ) then
                       AgregaFiltro( Format( '( SO_ESTUDIO >= %d )',
                                     [ ParamByName( 'MinEstudios' ).AsInteger ] ),
                                     TRUE, Entidad );
{                    if ( ParamByName( 'MaxEstudios' ).AsInteger >= 0 ) then
                       AgregaFiltro( Format( '( SO_ESTUDIO <= %d )',
                                     [ ParamByName( 'MaxEstudios' ).AsInteger ] ),
                                     TRUE, Entidad ); }
                    if ( ParamByName( 'Ingles' ).AsInteger > 0 ) then
                       AgregaFiltro( Format( '( SO_INGLES >= %d )',
                                     [ ParamByName( 'Ingles' ).AsInteger ] ),
                                     TRUE, Entidad );
                    if strLleno( ParamByName( 'ApePat' ).AsString ) then
                       AgregaFiltro( Format( '( upper( SO_APE_PAT ) LIKE ''%s'' )',
                                     [  '%' + ParamByName( 'ApePat' ).AsString + '%' ] ),
                                     TRUE, Entidad );
                    if strLleno( ParamByName( 'ApeMat' ).AsString ) then
                       AgregaFiltro( Format( '( upper( SO_APE_MAT ) LIKE ''%s'' )',
                                     [  '%' + ParamByName( 'ApeMat' ).AsString + '%' ] ),
                                     TRUE, Entidad );
                    if strLleno( ParamByName( 'Nombres' ).AsString ) then
                       AgregaFiltro( Format( '( upper( SO_NOMBRES ) LIKE ''%s'' )',
                                     [  '%' + ParamByName( 'Nombres' ).AsString + '%' ] ),
                                     TRUE, Entidad );
                    if strLleno( ParamByName( 'Area' ).AsString ) then
                       AgregaFiltro( Format( '( SO_AREA_1 = ''%0:s'' OR SO_AREA_2 = ''%0:s'' OR SO_AREA_3 = ''%0:s'' )',
                                     [ ParamByName( 'Area' ).AsString ] ),
                                     TRUE, Entidad );
                    if strLleno( ParamByName( 'Condicion' ).AsString ) then
                       AgregaFiltro( ParamByName( 'Condicion' ).AsString, FALSE, Entidad );

                    if strLleno( ParamByName( 'OtrasHab' ).AsString ) then {OP:14.Abr.08}
                    begin
                         sHabilid := ParamByName( 'OtrasHab' ).AsString;
                         {$ifdef DOS_CAPAS}
                         sNotas := 'UPPER( SUBSTRING( SO_OBSERVA FROM 1 FOR 8000 ) )';
                         {$else}
                         sNotas := 'SO_OBSERVA';
                         {$endif}
                         AgregaFiltro( Format( '( ( %0:s LIKE ''%1:s'' ) OR ( %2:s LIKE ''%1:s'' ) OR ( %3:s LIKE ''%1:s'' ) OR ( %4:s LIKE ''%1:s'' ) ' +
                                     '%5:s %6:s %7:s )',
                                     [ sNotas, '%' + sHabilid + '%',
                                       GetCampo( 'SO_ESCUELA' ),
                                       GetCampo( 'SO_IDIOMA2' ),
                                       GetCampo( 'SO_PUE_SOL' ),
                                       ObtenChkRequisitos( sHabilid ),
                                       ObtenTextAdicionales( sHabilid ),
                                       ObtenChkAdicionales( sHabilid ) ] ), TRUE, Entidad );
                    end;

               end;
               AgregaOrden( 'SO_FOLIO', TRUE, Entidad );
          end;
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

function TdmServerSeleccion.GetRequiere(Empresa,Parametros: OleVariant): OleVariant;

 function GetFStatus( const sFiltro, sParam, sCampo : string;
                      const sCompara : string = '=';
                      const nValor : integer = 0 ) : string;
 begin
      Result := sFiltro;
      with oZetaProvider.ParamList do
      begin
           if ( ParamByName( sParam ).AsInteger >= nValor ) then
              Result := ConcatFiltros( sFiltro, Format( sCampo + sCompara + '%d', [ParamByName( sParam ).AsInteger] ));
      end;
 end;

 function GetFiltro : string;
 begin
      RESULT := '';
      with oZetaProvider.ParamList do
      begin
           Result := GetFStatus(Result, 'Status', 'RQ_STATUS' );
           Result := GetFStatus(Result, 'Prioridad','RQ_PRIORID' );
           Result := GetFStatus(Result, 'FolioInicial', 'RQ_FOLIO', '>=',1 );
           Result := GetFStatus(Result, 'FolioFinal', 'RQ_FOLIO', '<=',1 );
           if StrLleno( ParamByName( 'Cliente' ).AsString ) then
              Result := ConcatFiltros( Result, 'RQ_CLIENTE=' + EntreComillas(ParamByName( 'Cliente' ).AsString ));
           if StrLleno( ParamByName( 'Puesto' ).AsString ) then
              Result := ConcatFiltros( Result, 'RQ_PUESTO=' + EntreComillas(ParamByName( 'Puesto' ).AsString ));
      end;
 end;

begin
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          with TablaInfo do
          begin
               SetInfo( 'REQUIERE', 'RQ_FOLIO,RQ_FEC_INI,RQ_PUESTO,RQ_CLIENTE,'+
                                    'RQ_STATUS,RQ_VACANTE,RQ_PRIORID,RQ_CANDIDA,RQ_CONTRAT', 'RQ_FOLIO'  );
               Filtro := GetFiltro;
          end;
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerSeleccion.GetEditRequiere(Empresa: OleVariant; Folio: Integer; out Candidatos, Gastos: OleVariant): OleVariant;

function GetTablaInfo : OleVariant;
  begin
       with oZetaProvider.TablaInfo do
       begin
            Filtro := Format('RQ_FOLIO=%d', [Folio]);
       end;
       Result := oZetaProvider.GetTabla(Empresa);
  end;

begin
     SetTablaInfo(eRequisicion);
     Result := GetTablaInfo;

     Candidatos := oZetaProvider.OpenSQL( Empresa, Format( GetScript( eReqCandidatos ), [ Folio ] ), TRUE );

     SetTablaInfo(eReqGasto);
     Gastos := GetTablaInfo;

     SetComplete;
end;

function TdmServerSeleccion.GetEditSolicitud( Empresa: OleVariant;
                                              Folio: Integer;
                                              out Candidatos, Examenes, RefLaboral, Documentos: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( GetScript( eEditSolicita ), [ K_PRETTYNAME, Folio ] ), TRUE );
          Candidatos := OpenSQL( Empresa, Format( GetScript( eCandidato ), [ Folio, VACIO ] ), TRUE );
          // Examenes
          SetTablaInfo( eExamen );
          TablaInfo.Filtro := Format( 'SO_FOLIO = %d', [ Folio ] );
          Examenes := GetTabla( Empresa );
          // Referencias Laborales
          SetTablaInfo( eRefLaboral );
          TablaInfo.Filtro := Format( 'SO_FOLIO = %d', [ Folio ] );
          RefLaboral := GetTabla( Empresa );
          //Documentos
          Documentos := OpenSQL( Empresa, Format( GetScript( eDocumentos ), [ Folio ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerSeleccion.GetEntrevista(Empresa: OleVariant; SolFolio,ReqFolio: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          SetTablaInfo( eEntrevista );
          with TablaInfo do
          begin
               //Filtro := Format( '(SO_FOLIO= %d and RQ_FOLIO = %d)', [ SolFolio, ReqFolio ] );
               Filtro := Format( '(SO_FOLIO= %d)', [ SolFolio ] );
          end;
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerSeleccion.GetDocumento(Empresa: OleVariant; Folio: Integer; const Tipo: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          SetTablaInfo( eDocumentos );
          with TablaInfo do
          begin
               Filtro := Format( '( SO_FOLIO= %d and DO_TIPO = %s )', [ Folio, ZetaCommonTools.EntreComillas( Tipo ) ] );
          end;
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerSeleccion.GrabaDocumento(Empresa: OleVariant; lReemplaza: WordBool; oDelta: OleVariant;
         out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eDocumentos );
     FHuboErrores := lReemplaza;                           // Se toma que hubo errores si pide reemplazar
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.BeforeUpdateRecord := BeforeUpdateDocumento;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

procedure TdmServerSeleccion.BeforeUpdateDocumento(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
const
     K_BORRA_DOC_ANTERIOR = 'delete from DOCUMENT where SO_FOLIO = %d and DO_TIPO = %s';
begin
     if FHuboErrores and ( UpdateKind = ukInsert ) then
        with oZetaProvider, DeltaDS do
             ExecSQL( EmpresaActiva, Format( K_BORRA_DOC_ANTERIOR, [ Integer( ZetaServerTools.CampoAsVar( FieldByName( 'SO_FOLIO' ) ) ),
                      EntreComillas( ZetaServerTools.CampoAsVar( FieldByName( 'DO_TIPO' ) ) ) ] ) );
end;

function TdmServerSeleccion.GrabaTabla(Empresa: OleVariant;iTabla: Integer; oDelta: OleVariant;out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eTipoCatalogo(iTabla) );
     Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerSeleccion.GrabaSolicitud(Empresa, oDelta: OleVariant; out ErrorCount,
         Folio: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FFolio := 0;
          SetTablaInfo( eSolicita );
          with TablaInfo do
          begin
               BeforeUpdateRecord := BeforeUpdateSolicita;
               AfterUpdateRecord := AfterUpdateSolicita;
          end;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          Folio := FFolio;
     end;
     SetComplete;
end;

procedure TdmServerSeleccion.BeforeUpdateSolicita(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   i : Integer;
   lHuboCambios: Boolean;
begin
     if ( UpdateKind = ukInsert ) then
     begin
          with DeltaDS do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'SO_FOLIO' ).AsInteger := GetLastFolio( eMaxSolicita ) + 1;
          end;
     end
     else
     begin
          lHuboCambios := FALSE;
          with DeltaDS do
               for i := 0 to FieldCount-1 do
                   if CambiaCampo( Fields[i] ) then
                   begin
                        lHuboCambios := TRUE;
                        Break;
                   end;
          Applied := ( not lHuboCambios );
     end;
end;

procedure TdmServerSeleccion.AfterUpdateSolicita(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
const
     K_UPDATE_FOTO = 'UPDATE SOLICITA SET SO_FOTO = NULL WHERE SO_FOLIO = %d';
begin
     if ( UpdateKind = ukInsert ) then
     with oZetaProvider do
     begin
          FFolio := DeltaDS.FieldByName( 'SO_FOLIO' ).AsInteger;
     end;

     if ( UpdateKind = ukModify ) then
        with DeltaDS.FieldByName('SO_FOTO') do
        begin
             if VarIsNull( NewValue ) and (OldValue <> NewValue) then
                with oZetaProvider do
                     ExecSQL( EmpresaActiva, Format( K_UPDATE_FOTO, [ Integer( CampoAsVar( DeltaDS.FieldByName('SO_FOLIO') ) ) ] ) );
        end;
end;

function TdmServerSeleccion.BorraSolicitud(Empresa, oDelta: OleVariant; Operacion: Integer;
         out ErrorCount: Integer; out ErrorData: OleVariant): OleVariant;
var
   i : Integer;
begin
     SetTablaInfo( eSolicita );
     with oZetaProvider do
     begin
          FListaReq := TStringList.Create;
          try
             EmpresaActiva := Empresa;
             FOperacion := eOperacionConflicto( Operacion );
             FHuboErrores := FALSE;
             TablaInfo.BeforeUpdateRecord := BeforeUpdateBorraSolicitud;
             Result := GrabaTabla( Empresa, oDelta, ErrorCount );
             with FListaReq do
                  if ( Count > 0 ) then
                     for i := 0 to Count - 1 do
                         CalculaTotalesRequisicion( StrToIntDef( Strings[ i ], 0 ) );
          finally
             FreeAndNil( FListaReq );
             if FHuboErrores then
                ErrorData := OpenSQL( Empresa, Format( GetScript( eCandidato ), [ FFolio, K_WHERE_CONFLICTO_CANDIDATO ] ), TRUE )
             else
                SetOLEVariantToNull( ErrorData );
          end;
     end;
     SetComplete;
end;

procedure TdmServerSeleccion.BeforeUpdateBorraSolicitud(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
const
     K_CONFLICTO_CANDIDATO = 'select COUNT(*) CUANTOS from CANDIDAT where SO_FOLIO = %d %s';
     K_LISTA_CANDIDATURAS = 'select RQ_FOLIO from CANDIDAT where SO_FOLIO = %d';

   function HayConflictoCandidato: Boolean;
   begin
        with oZetaProvider do
             AbreQueryScript( FDataSet, Format( K_CONFLICTO_CANDIDATO, [ FFolio, K_WHERE_CONFLICTO_CANDIDATO ] ) );
        Result := ( FDataSet.FieldByName( 'CUANTOS' ).AsInteger > 0 );
   end;

begin
     if ( UpdateKind = ukDelete ) then      // En teoria siempre debe ser Borrar
     begin
          with oZetaProvider, DeltaDS do
          begin
               FDataSet := CreateQuery;
               try
                  FFolio := FieldByName( 'SO_FOLIO' ).AsInteger;
                  FHuboErrores := ( FOperacion = ocReportar ) and HayConflictoCandidato;
                  if ( FHuboErrores ) then
                     DataBaseError( 'STATUS_CANDIDATURAS' )
                  else
                  begin
                       AbreQueryScript( FDataSet, Format( K_LISTA_CANDIDATURAS, [ FFolio ] ) );
                       with FDataSet do
                       begin
                            if not IsEmpty then
                               while not EOF do
                               begin
                                    FListaReq.Add( IntToStr( FieldByName( 'RQ_FOLIO' ).AsInteger ) );
                                    Next;
                               end;
                       end;
                  end;
               finally
                  FreeAndNil( FDataSet );
               end;
          end;
     end;
end;

function TdmServerSeleccion.GrabaRequisicion(Empresa, oDelta: OleVariant; out ErrorCount,
         Folio: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FFolio := 0;
          SetTablaInfo( eRequisicion );
          with TablaInfo do
          begin
               BeforeUpdateRecord := BeforeUpdateRequisicion;
               AfterUpdateRecord := AfterUpdateRequisicion;
          end;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          Folio := FFolio;
     end;
     SetComplete;
end;

procedure TdmServerSeleccion.BeforeUpdateRequisicion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if ( UpdateKind = ukInsert ) then
        with DeltaDS do
        begin
             if not ( State in [ dsEdit, dsInsert ] ) then
                Edit;
             FieldByName( 'RQ_FOLIO' ).AsInteger := GetLastFolio( eMaxRequiere ) + 1;
        end;
end;

procedure TdmServerSeleccion.AfterUpdateRequisicion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
begin
     if ( UpdateKind = ukInsert ) then
        FFolio := DeltaDS.FieldByName( 'RQ_FOLIO' ).AsInteger;
end;

function TdmServerSeleccion.BorraRequisicion(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eRequisicion );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );   // Se requieren triggers para validar el borrado y depurar Gastos y Candidatos ( El Candidatos tendr� un trigger que borre entrevistas )
     end;
     SetComplete;
end;

function TdmServerSeleccion.ObtenerCandidatos(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        GetSolicitudBuildDataset;
        Result := ObtenerCandidatosDataSet( SQLBroker.SuperReporte.DataSetReporte );
     finally
        ClearBroker;
     end;
     SetComplete;
end;

function TdmServerSeleccion.ObtenerCandidatosDataSet( Dataset: TDataset ): OleVariant;
const
     K_BORRA_CANDIDATOS = 'delete from CANDIDAT where RQ_FOLIO = %d and CD_STATUS = %d';
     K_INSERTA_CANDIDATOS = 'insert into CANDIDAT ( RQ_FOLIO, SO_FOLIO, CD_STATUS ) ' +
                            'values ( :RQ_FOLIO, :SO_FOLIO, :CD_STATUS )';
begin
     {
     with oZetaProvider do
     begin
               iFolioReq := ParamList.ParamByName( 'FolioReq' ).AsInteger;
               EmpiezaTransaccion;
               try
                  EjecutaAndFree( Format( K_BORRA_CANDIDATOS, [ iFolioReq, Ord( csPorRevisar ) ] ) );
                  FDataset := CreateQuery( K_INSERTA_CANDIDATOS );
                  try
                     with Dataset do
                          while not Eof  do
{                          begin
                               ParamAsInteger( FDataset, 'RQ_FOLIO', iFolioReq );
                               ParamAsInteger( FDataset, 'SO_FOLIO', FieldByName( 'SO_FOLIO' ).AsInteger );
                               ParamAsInteger( FDataset, 'CD_STATUS', Ord( csPorRevisar ) );
                               try
                                  Ejecuta( FDataset );
                               except                   // Si no se puede agregar se deja el registro ya existente sin marcar error
                               end;
                               //Log.Evento( clbNinguno, iEmpleado, NullDateTime, 'Empleado Dado de Baja Fu� Borrado' );
                               Next;
                          end;
                  finally
                      CalculaTotalesRequisicion(iFolioReq);
                      FreeAndNil( FDataSet );
                  end;
                  TerminaTransaccion( TRUE );
               except
                  RollBackTransaccion;
               end;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( eReqCandidatos ), [ iFolioReq ] ), TRUE );
     end;
     }
end;

procedure TdmServerSeleccion.CalculaTotalesRequisicion( const iFolioReq : integer );
const
     K_CANDIDATOS = 'SELECT COUNT(*) CUANTOS FROM CANDIDAT WHERE RQ_FOLIO = %d';
     K_CONTRAT = 'SELECT COUNT(*) CUANTOS FROM CANDIDAT WHERE RQ_FOLIO = %d AND CD_STATUS = %d';
     //K_SELECCIONADO = 'SELECT COUNT(*) CUANTOS FROM CANDIDAT WHERE RQ_FOLIO = %d AND CD_STATUS IN (2,3,5)';
     K_UPDATEREQ = 'UPDATE REQUIERE SET RQ_CONTRAT=%d,RQ_CANDIDA=%d WHERE RQ_FOLIO = %d';
var
   iCandidatos, iContratado: integer;
begin
     with oZetaProvider do
     begin
          FDataSet := CreateQuery;
          try
             AbreQueryScript(FDataSet, Format(K_CANDIDATOS, [iFolioReq] ));
             iCandidatos := FDataSet.FieldByName( 'CUANTOS' ).AsInteger;

             AbreQueryScript(FDataSet, Format(K_CONTRAT, [iFolioReq, Ord(csContratado)] ));
             iContratado := FDataSet.FieldByName( 'CUANTOS' ).AsInteger;

             {AbreQueryScript(FDataSet, Format(K_SELECCIONADO, [iFolioReq] ));
             iSeleccionados := FDataSet.FieldByName( 'CUANTOS' ).AsInteger;}
          finally
             FreeAndNil( FDataSet );
          end;
          EjecutaAndFree(Format( K_UPDATEREQ, [iContratado,iCandidatos,iFolioReq] ) );
     end;
end;

function TdmServerSeleccion.GrabaCandidato(Empresa, oDelta: OleVariant;
  iFolioReq: Integer; out iStatusSol, ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eCandidato );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          with TablaInfo do
          begin
               AfterUpdateRecord := AfterUpdateCandidato;{OP:18.Abr.08}
          end;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          iStatusSol := Ord( FStatusSolCandidato );{OP:28.Abr.08}
          CalculaTotalesRequisicion( iFolioReq );
     end;
     SetComplete;
end;

{OP:18.Abr.08}
procedure TdmServerSeleccion.AfterUpdateCandidato(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
const
     K_UPDATE_STATUS = 'UPDATE SOLICITA SET SO_STATUS = %d WHERE SO_FOLIO = %d';
begin
     with DeltaDS do
     begin
          if ( UpdateKind in [ ukModify, ukInsert ] ) then
          begin
               case eCanStatus( CampoAsVar( FieldByName( 'CD_STATUS' ) ) ) of
                    csContratado: FStatusSolCandidato := esContratado;
                    csPorContratar: FStatusSolCandidato := stEnTramite;
               else FStatusSolCandidato := stDisponible;
               end;
          end
          else if ( UpdateKind = ukDelete ) then
               FStatusSolCandidato := stDisponible;

          with oZetaProvider do
               ExecSQL( EmpresaActiva, Format( K_UPDATE_STATUS, [ Ord( FStatusSolCandidato ), Integer( CampoAsVar( FieldByName( 'SO_FOLIO' ) ) ) ] ) );
     end;
end;

function TdmServerSeleccion.GrabaGasto(Empresa, oDelta: OleVariant; iFolioReq: Integer; out ErrorCount: Integer): OleVariant;
const
     K_SUMGASTO = 'SELECT SUM(RG_MONTO) SUMA FROM REQGASTO WHERE RQ_FOLIO = %d';
     K_UPDATEREQ = 'UPDATE REQUIERE SET RQ_GTOREAL = %f WHERE RQ_FOLIO = %d';
var
   iGasto : Double;
begin
     SetTablaInfo( eReqGasto );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          FDataSet := CreateQuery(Format(K_SUMGASTO, [iFolioReq] ));
          try
              with FDataSet do
              begin
                   Active := TRUE;
                   iGasto := FieldByName( 'SUMA' ).AsFloat;
              end;
              EjecutaAndFree(Format( K_UPDATEREQ, [iGasto, iFolioReq] ) );
          finally
                 FreeAndNil(FDataSet);
          end;
     end;
     SetComplete;
end;

function TdmServerSeleccion.Depuracion(Empresa, Parametros: OleVariant): OleVariant;
const
     Q_REQUIERE_DEPURAR = 'delete from REQUIERE where ( RQ_FEC_INI < ''%s'' ) %s';
     Q_SOLICITA_DEPURAR = 'delete from SOLICITA where ( SO_FEC_INI < ''%s'' ) %s';
     K_MESS_DATA = 'Anteriores al : %s' + CR_LF + 'Con Status : <%s>';
var
   dFechaReq, dFechaSol : TDate;
   DescStatus : String;
   iCuantos : Integer;

   function GetFiltroStatus( const sCampo: String; const iValor : Integer; Tipo: ListasFijas ): String;
   begin
        Result := VACIO;
        if ( iValor >= 0 ) then
        begin
             Result := Format( 'and ( %s = %d )', [ sCampo, iValor ] );
             DescStatus := ZetaCommonLists.ObtieneElemento( Tipo, iValor );
        end
        else
             DescStatus := 'Todos';
   end;

begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          if OpenProcess( prDepuraSeleccion, 0 ) then
          begin
               with ParamList do
               begin
                    // Requisiciones
                    if ParamByName( 'BorrarRequiere' ).AsBoolean then
                    begin
                         EmpiezaTransaccion;
                         try
                            dFechaReq := ParamByName( 'FechaRequiere' ).AsDate;
                            iCuantos := EjecutaAndFree( Format( Q_REQUIERE_DEPURAR, [ DateToStrSQL( dFechaReq ),
                                                        GetFiltroStatus( 'RQ_STATUS', ParamByName( 'StatusRequiere' ).AsInteger,
                                                        lfReqStatus ) ] ) );
                            TerminaTransaccion(TRUE);
                            Log.Evento( clbNinguno, 0, FechaDefault, Format( 'Se realiz� Depuraci�n de %d Requisiciones', [ iCuantos ] ),
                                        Format( K_MESS_DATA, [ FechaCorta( dFechaReq ), DescStatus ] ) );
                         except
                            on Error: Exception do
                            begin
                                 RollBackTransaccion;
                                 Log.Excepcion( 0, 'Error Al Depurar Requisiciones', Error );
                            end;
                         end;
                    end;
                    // Solicitudes
                    if ParamByName( 'BorrarSolicita' ).AsBoolean then
                    begin
                         EmpiezaTransaccion;
                         try
                            dFechaSol := ParamByName( 'FechaSolicita' ).AsDate;
                            iCuantos := EjecutaAndFree( Format( Q_SOLICITA_DEPURAR, [ DateToStrSQL( dFechaSol ),
                                                        GetFiltroStatus( 'SO_STATUS', ParamByName( 'StatusSolicita' ).AsInteger,
                                                        lfSolStatus ) ] ) );
                            TerminaTransaccion(TRUE);
                            Log.Evento( clbNinguno, 0, FechaDefault, Format( 'Se realiz� Depuraci�n de %d Solicitudes', [ iCuantos ] ),
                                        Format( K_MESS_DATA, [ FechaCorta( dFechaSol ), DescStatus ] ) );
                         except
                            on Error: Exception do
                            begin
                                 RollBackTransaccion;
                                 Log.Excepcion( 0, 'Error Al Depurar Solicitudes', Error );
                            end;
                         end;
                    end;
               end;
               Result := CloseProcess;
          end;
     end;
     SetComplete;
end;

{$ifndef DOS_CAPAS}

initialization
  TComponentFactory.Create(ComServer, TdmServerSeleccion,
    Class_dmServerSeleccion, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.