unit Super_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 29-06-2017 10:08:05 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2017\MTS\Super (1)
// LIBID: {7E81EC62-0B1F-11D4-A33F-0050DA04EC66}
// LCID: 0
// Helpfile:
// HelpString: Tress Supervisores
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
//   (2) v1.0 Midas, (midas.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface


{$IFDEF TRESS_DELPHIXE5_UP}
uses Windows, Midas, Classes, Variants, StdVCL, Graphics, OleServer, ActiveX;
{$ELSE}
uses Windows, Midas, Classes, Variants, StdVCL, Graphics, OleServer, ActiveX;
{$ENDIF}



// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SuperMajorVersion = 1;
  SuperMinorVersion = 0;

  LIBID_Super: TGUID = '{7E81EC62-0B1F-11D4-A33F-0050DA04EC66}';

  IID_IdmServerSuper: TGUID = '{7E81EC63-0B1F-11D4-A33F-0050DA04EC66}';
  CLASS_dmServerSuper: TGUID = '{7E81EC65-0B1F-11D4-A33F-0050DA04EC66}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  IdmServerSuper = interface;
  IdmServerSuperDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  dmServerSuper = IdmServerSuper;


// *********************************************************************//
// Interface: IdmServerSuper
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7E81EC63-0B1F-11D4-A33F-0050DA04EC66}
// *********************************************************************//
  IdmServerSuper = interface(IAppServer)
    ['{7E81EC63-0B1F-11D4-A33F-0050DA04EC66}']
    function GetTarjeta(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime;
                        out Checadas: OleVariant): OleVariant; safecall;
    function GrabaTarjeta(Empresa: OleVariant; oDelta: OleVariant; const OldChecadas: WideString;
                          VarChecadas: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function GrabaListaAsistencia(Empresa: OleVariant; oDelta: OleVariant; Parametros: OleVariant;
                                  var ErrorCount: Integer): OleVariant; safecall;
    function GetEmpleados(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetHisKardex(Empresa: OleVariant; Empleado: Integer; out Otros: OleVariant): OleVariant; safecall;
    function GetMisAsignaciones(Empresa: OleVariant; Fecha: TDateTime): OleVariant; safecall;
    function GetListaSupervisores(Empresa: OleVariant): OleVariant; safecall;
    function GrabaAsignaciones(Empresa: OleVariant; oDelta: OleVariant; ErrorCount: Integer): OleVariant; safecall;
    function GetDatosEmpleado(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime;
                              ObtenerFoto: WordBool; out Datos: OleVariant): WordBool; safecall;
    function GetDatosEmpleadoAnterior(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime;
                                      ObtenerFoto: WordBool; out Datos: OleVariant): WordBool; safecall;
    function GetDatosEmpleadoSiguiente(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime;
                                       ObtenerFoto: WordBool; out Datos: OleVariant): WordBool; safecall;
    function AjusteColectivoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GrabaAutorizaPrenomina(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetAutorizaPrenomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetSesion(Empresa: OleVariant; iFolio: Integer): OleVariant; safecall;
    function GetHisCursos(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function GetTarjetasPeriodo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GrabaTarjetaPeriodo(Empresa: OleVariant; Parametros: OleVariant;
                                 out iErrorCount: Integer; oDelta: OleVariant): OleVariant; safecall;
    function GetFechaLimite(Empresa: OleVariant): TDateTime; safecall;
    function GetEvaluacionDiaria(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GrabaEvaluacionDiaria(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetFechaEvaluacionDiaria(Empresa: OleVariant): TDateTime; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerSuperDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7E81EC63-0B1F-11D4-A33F-0050DA04EC66}
// *********************************************************************//
  IdmServerSuperDisp = dispinterface
    ['{7E81EC63-0B1F-11D4-A33F-0050DA04EC66}']
    function GetTarjeta(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime;
                        out Checadas: OleVariant): OleVariant; dispid 1;
    function GrabaTarjeta(Empresa: OleVariant; oDelta: OleVariant; const OldChecadas: WideString;
                          VarChecadas: OleVariant; var ErrorCount: Integer): OleVariant; dispid 3;
    function GrabaListaAsistencia(Empresa: OleVariant; oDelta: OleVariant; Parametros: OleVariant;
                                  var ErrorCount: Integer): OleVariant; dispid 4;
    function GetEmpleados(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 6;
    function GetHisKardex(Empresa: OleVariant; Empleado: Integer; out Otros: OleVariant): OleVariant; dispid 5;
    function GetMisAsignaciones(Empresa: OleVariant; Fecha: TDateTime): OleVariant; dispid 7;
    function GetListaSupervisores(Empresa: OleVariant): OleVariant; dispid 8;
    function GrabaAsignaciones(Empresa: OleVariant; oDelta: OleVariant; ErrorCount: Integer): OleVariant; dispid 9;
    function GetDatosEmpleado(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime;
                              ObtenerFoto: WordBool; out Datos: OleVariant): WordBool; dispid 10;
    function GetDatosEmpleadoAnterior(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime;
                                      ObtenerFoto: WordBool; out Datos: OleVariant): WordBool; dispid 11;
    function GetDatosEmpleadoSiguiente(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime;
                                       ObtenerFoto: WordBool; out Datos: OleVariant): WordBool; dispid 12;
    function AjusteColectivoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 2;
    function GrabaAutorizaPrenomina(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 13;
    function GetAutorizaPrenomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 14;
    function GetSesion(Empresa: OleVariant; iFolio: Integer): OleVariant; dispid 301;
    function GetHisCursos(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 303;
    function GetTarjetasPeriodo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 302;
    function GrabaTarjetaPeriodo(Empresa: OleVariant; Parametros: OleVariant;
                                 out iErrorCount: Integer; oDelta: OleVariant): OleVariant; dispid 304;
    function GetFechaLimite(Empresa: OleVariant): TDateTime; dispid 305;
    function GetEvaluacionDiaria(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 306;
    function GrabaEvaluacionDiaria(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 307;
    function GetFechaEvaluacionDiaria(Empresa: OleVariant): TDateTime; dispid 308;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer;
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer;
                           Options: Integer; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerSuper provides a Create and CreateRemote method to
// create instances of the default interface IdmServerSuper exposed by
// the CoClass dmServerSuper. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CodmServerSuper = class
    class function Create: IdmServerSuper;
    class function CreateRemote(const MachineName: string): IdmServerSuper;
  end;

implementation

{$IFDEF TRESS_DELPHIXE5_UP}
uses System.Win.ComObj;
{$ELSE}
uses ComObj;
{$ENDIF}

class function CodmServerSuper.Create: IdmServerSuper;
begin
  Result := CreateComObject(CLASS_dmServerSuper) as IdmServerSuper;
end;

class function CodmServerSuper.CreateRemote(const MachineName: string): IdmServerSuper;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerSuper) as IdmServerSuper;
end;

end.
