unit TimbradoSistema_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 12/19/2013 1:02:10 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20_Timbrado\MTS\TimbradoSistema.tlb (1)
// LIBID: {11C17F87-DD0C-4245-8E7F-96EF65C9740A}
// LCID: 0
// Helpfile: 
// HelpString: Tress Sistema
// DepndLst: 
//   (1) v1.0 Midas, (C:\Windows\SysWOW64\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  TimbradoSistemaMajorVersion = 1;
  TimbradoSistemaMinorVersion = 0;

  LIBID_TimbradoSistema: TGUID = '{11C17F87-DD0C-4245-8E7F-96EF65C9740A}';

  IID_IdmServerSistemaTimbrado: TGUID = '{C10F69CF-474E-4D04-9FB0-1A3D27FA8EBD}';
  CLASS_dmServerSistemaTimbrado: TGUID = '{F67F4524-CDD2-4BD8-8391-AE7A292392A6}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerSistemaTimbrado = interface;
  IdmServerSistemaTimbradoDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerSistemaTimbrado = IdmServerSistemaTimbrado;


// *********************************************************************//
// Interface: IdmServerSistemaTimbrado
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C10F69CF-474E-4D04-9FB0-1A3D27FA8EBD}
// *********************************************************************//
  IdmServerSistemaTimbrado = interface(IAppServer)
    ['{C10F69CF-474E-4D04-9FB0-1A3D27FA8EBD}']
    function GetEmpresas(Tipo: Integer; GrupoActivo: Integer): OleVariant; safecall;
    function GrabaEmpresas(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetGrupos(Grupo: Integer): OleVariant; safecall;
    function GrabaGrupos(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function GetImpresoras: OleVariant; safecall;
    function GrabaImpresoras(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetUsuarios(out Longitud: Integer; Grupo: Integer; out BlockSistema: WordBool): OleVariant; safecall;
    function GrabaUsuarios(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function GetUsuarioSupervisores(Empresa: OleVariant; iUsuario: Integer): OleVariant; safecall;
    function GrabaUsuariosSupervisores(Empresa: OleVariant; oDelta: OleVariant; 
                                       out ErrorCount: Integer; iUsuario: Integer; 
                                       const sTexto: WideString): OleVariant; safecall;
    function GetAccesos(iGrupo: Integer; const sCompany: WideString): OleVariant; safecall;
    function GrabaAccesos(oDelta: OleVariant; oParams: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetUsuarioArbol(iUsuario: Integer): OleVariant; safecall;
    function GrabaArbol(iUsuario: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetPoll: OleVariant; safecall;
    function GrabaPoll(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetPollVacio: OleVariant; safecall;
    function SuspendeUsuarios(Usuario: Integer; Grupo: Integer): OleVariant; safecall;
    function ActivaUsuarios(Usuario: Integer; Grupo: Integer): OleVariant; safecall;
    function BorrarBitacora(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetNivel0: OleVariant; safecall;
    function GrabaNivel0(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetUsuarioSuscrip(Empresa: OleVariant; Usuario: Integer): OleVariant; safecall;
    function GrabaUsuarioSuscrip(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                                 Usuario: Integer): OleVariant; safecall;
    function GetGruposAccesos(iGrupo: Integer; const sCompany: WideString; iTipo: Integer): OleVariant; safecall;
    function GetUsuarioAreas(Empresa: OleVariant; Usuario: Integer): OleVariant; safecall;
    function GrabaUsuarioAreas(Empresa: OleVariant; Usuario: Integer; oDelta: OleVariant; 
                               out ErrorCount: Integer; const sTexto: WideString): OleVariant; safecall;
    function GetNuevoUsuario: Integer; safecall;
    function GetEmpresasAccesos(Tipo: Integer; Grupo: Integer): OleVariant; safecall;
    function ActualizaNumeroTarjeta(out Bitacora: OleVariant): OleVariant; safecall;
    function GetGruposAdic(Empresa: OleVariant; out oCamposAdic: OleVariant): OleVariant; safecall;
    function GrabaGruposAdic(Empresa: OleVariant; oDelta: OleVariant; oDeltaCampos: OleVariant; 
                             out ErrorCount: Integer; out ErrorCampos: Integer; 
                             out CamposResult: OleVariant): OleVariant; safecall;
    function GetBitacora(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetReporteSuscrip(Empresa: OleVariant; iReporte: Integer): OleVariant; safecall;
    function GetRoles: OleVariant; safecall;
    function GetDatosEmpleadoUsuario(Empresa: OleVariant; Datos: OleVariant): OleVariant; safecall;
    function GrabaRoles(const Rol: WideString; Delta: OleVariant; Usuarios: OleVariant; 
                        Modelos: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function GetUsuariosRol(const Rol: WideString; out Modelos: OleVariant): OleVariant; safecall;
    function GetUserRoles(Empresa: OleVariant; iUsuario: Integer): OleVariant; safecall;
    function GrabaUsuarioRoles(Empresa: OleVariant; iUsuario: Integer; Delta: OleVariant; 
                               var ErrorCount: Integer): OleVariant; safecall;
    function GrabaEmpleadoUsuario(oEmpresa: OleVariant; iUsuario: Integer; iEmpleado: Integer): OleVariant; safecall;
    function EnrolamientoMasivo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function EnrolamientoMasivoGetLista(oEmpresa: OleVariant; oParametros: OleVariant): OleVariant; safecall;
    function ImportarEnrolamientoMasivoGetASCII(oEmpresa: OleVariant; Parametros: OleVariant; 
                                                ListaASCII: OleVariant; out ErrorCount: Integer; 
                                                LoginAD: WordBool): OleVariant; safecall;
    function ImportarEnrolamientoMasivo(Empresa: OleVariant; Parametros: OleVariant; 
                                        Datos: OleVariant): OleVariant; safecall;
    function GetClasifiEmpresa(Empresa: OleVariant): OleVariant; safecall;
    function GetDerechosAdicionales(Empresa: OleVariant; Grupo: Integer): OleVariant; safecall;
    function GrabaAccesosAdicionales(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant; 
                                     out ErrorCount: Integer): OleVariant; safecall;
    function GetSistLstDispositivos(Empresa: OleVariant): OleVariant; safecall;
    function GrabaSistLstDispositivos(Empresa: OleVariant; oDelta: OleVariant; 
                                      out ErrorCount: Integer): OleVariant; safecall;
    function GetGlobalLstDispositivos(Empresa: OleVariant; var Disponibles: OleVariant; 
                                      var Asignados: OleVariant): OleVariant; safecall;
    function GrabaGlobalLstDispositivos(Empresa: OleVariant; oDelta: OleVariant; 
                                        out ErrorCount: Integer): OleVariant; safecall;
    function GetAccArbol(Empresa: OleVariant): OleVariant; safecall;
    procedure ActualizaUsuariosEmpresa(Empresa: OleVariant; Delta: OleVariant); safecall;
    function ConsultaBitacoraBiometrica(Parametros: OleVariant): OleVariant; safecall;
    function DepuraBitacoraBiometrica(Parametros: OleVariant): OleVariant; safecall;
    function ObtenIdBioMaximo: Integer; safecall;
    function ActualizaIdBio(const CodigoEmpresaLocal: WideString; Empleado: Integer; Id: Integer; 
                            const Grupo: WideString; Invitador: Integer): WordBool; safecall;
    function GetUsuarioCCosto(Empresa: OleVariant; iUsuario: Integer): OleVariant; safecall;
    function GrabaUsuarioCCosto(Empresa: OleVariant; oDelta: OleVariant; ErrorCount: Integer; 
                                iUsuario: Integer; const sTexto: WideString; 
                                const sNombreCosteo: WideString): OleVariant; safecall;
    function ConsultaListaGrupos: OleVariant; safecall;
    function GrabaSistListaGrupos(oDelta: OleVariant; UsuarioTerminal: Integer; 
                                  out ErrorCount: Integer): OleVariant; safecall;
    function ConsultaTermPorGrupo(const Grupo: WideString): OleVariant; safecall;
    function GrabaTermPorGrupo(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; safecall;
    procedure GrabaListaTermPorGrupo(const Terminales: WideString; UsuarioTerminal: Integer; 
                                     const Grupo: WideString); safecall;
    function AsignaNumBiometricos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    procedure ReiniciaSincTerminales(const Lista: WideString); safecall;
    function AsignaGrupoTerminales(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function EmpleadosBiometricosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function EmpleadosSinBiometricosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AsignaGrupoTerminalesLista(Empresa: OleVariant; Lista: OleVariant; 
                                        Parametros: OleVariant): OleVariant; safecall;
    function AsignaNumBiometricosLista(Empresa: OleVariant; Lista: OleVariant; 
                                       Parametros: OleVariant): OleVariant; safecall;
    procedure ActualizaTerminalesGTI(const Lista: WideString); safecall;
    function ConsultaMensaje(const Filtro: WideString): OleVariant; safecall;
    function GrabaMensaje(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function ConsultaMensajeTerminal(const Filtro: WideString): OleVariant; safecall;
    function GrabaMensajeTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; 
                                  out ErrorCount: Integer): OleVariant; safecall;
    function ConsultaTerminal(const Filtro: WideString): OleVariant; safecall;
    function GrabaTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function ImportaTerminales(Parametros: OleVariant; UsuarioTerminal: Integer): OleVariant; safecall;
    function ExisteHuellaRegistrada(Empleado: Integer; const Empresa: WideString): WordBool; safecall;
    procedure ReiniciaTerminal(Terminal: Integer; const Empresa: WideString); safecall;
    function ExisteEmpleadoEnGrupo(const Grupo: WideString): WordBool; safecall;
    function ObtieneTemplates(Parametros: OleVariant): OleVariant; safecall;
    procedure InsertaHuella(Parametros: OleVariant); safecall;
    procedure EliminaHuellas(const Empresa: WideString; Empleado: Integer; Invitador: Integer; 
                             Usuario: Integer); safecall;
    function ReportaHuella(Parametros: OleVariant): OleVariant; safecall;
    function ExisteHuellaInvitador(const Empresa: WideString; Invitador: Integer): WordBool; safecall;
    function GetCuentasTimbrado: OleVariant; safecall;
    function GrabaCuentasTimbrado(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ExistenEstructurasTimbradoComparte: WordBool; safecall;
    function CrearEstructurasTimbradoComparte: WordBool; safecall;
    function ExistenEstructurasTimbradoEmpresa(Empresa: OleVariant): WordBool; safecall;
    function CrearEstructurasTimbradoEmpresa(Empresa: OleVariant): WordBool; safecall;
    procedure CrearBitacora(Empresa: OleVariant; Tipo: Integer; Parametros: OleVariant); safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerSistemaTimbradoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C10F69CF-474E-4D04-9FB0-1A3D27FA8EBD}
// *********************************************************************//
  IdmServerSistemaTimbradoDisp = dispinterface
    ['{C10F69CF-474E-4D04-9FB0-1A3D27FA8EBD}']
    function GetEmpresas(Tipo: Integer; GrupoActivo: Integer): OleVariant; dispid 2;
    function GrabaEmpresas(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 3;
    function GetGrupos(Grupo: Integer): OleVariant; dispid 4;
    function GrabaGrupos(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant; dispid 5;
    function GetImpresoras: OleVariant; dispid 6;
    function GrabaImpresoras(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 7;
    function GetUsuarios(out Longitud: Integer; Grupo: Integer; out BlockSistema: WordBool): OleVariant; dispid 8;
    function GrabaUsuarios(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant; dispid 9;
    function GetUsuarioSupervisores(Empresa: OleVariant; iUsuario: Integer): OleVariant; dispid 10;
    function GrabaUsuariosSupervisores(Empresa: OleVariant; oDelta: OleVariant; 
                                       out ErrorCount: Integer; iUsuario: Integer; 
                                       const sTexto: WideString): OleVariant; dispid 11;
    function GetAccesos(iGrupo: Integer; const sCompany: WideString): OleVariant; dispid 12;
    function GrabaAccesos(oDelta: OleVariant; oParams: OleVariant; out ErrorCount: Integer): OleVariant; dispid 13;
    function GetUsuarioArbol(iUsuario: Integer): OleVariant; dispid 14;
    function GrabaArbol(iUsuario: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 15;
    function GetPoll: OleVariant; dispid 16;
    function GrabaPoll(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 17;
    function GetPollVacio: OleVariant; dispid 18;
    function SuspendeUsuarios(Usuario: Integer; Grupo: Integer): OleVariant; dispid 20;
    function ActivaUsuarios(Usuario: Integer; Grupo: Integer): OleVariant; dispid 21;
    function BorrarBitacora(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 22;
    function GetNivel0: OleVariant; dispid 19;
    function GrabaNivel0(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 23;
    function GetUsuarioSuscrip(Empresa: OleVariant; Usuario: Integer): OleVariant; dispid 24;
    function GrabaUsuarioSuscrip(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                                 Usuario: Integer): OleVariant; dispid 25;
    function GetGruposAccesos(iGrupo: Integer; const sCompany: WideString; iTipo: Integer): OleVariant; dispid 26;
    function GetUsuarioAreas(Empresa: OleVariant; Usuario: Integer): OleVariant; dispid 27;
    function GrabaUsuarioAreas(Empresa: OleVariant; Usuario: Integer; oDelta: OleVariant; 
                               out ErrorCount: Integer; const sTexto: WideString): OleVariant; dispid 28;
    function GetNuevoUsuario: Integer; dispid 29;
    function GetEmpresasAccesos(Tipo: Integer; Grupo: Integer): OleVariant; dispid 30;
    function ActualizaNumeroTarjeta(out Bitacora: OleVariant): OleVariant; dispid 31;
    function GetGruposAdic(Empresa: OleVariant; out oCamposAdic: OleVariant): OleVariant; dispid 32;
    function GrabaGruposAdic(Empresa: OleVariant; oDelta: OleVariant; oDeltaCampos: OleVariant; 
                             out ErrorCount: Integer; out ErrorCampos: Integer; 
                             out CamposResult: OleVariant): OleVariant; dispid 33;
    function GetBitacora(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 301;
    function GetReporteSuscrip(Empresa: OleVariant; iReporte: Integer): OleVariant; dispid 302;
    function GetRoles: OleVariant; dispid 303;
    function GetDatosEmpleadoUsuario(Empresa: OleVariant; Datos: OleVariant): OleVariant; dispid 304;
    function GrabaRoles(const Rol: WideString; Delta: OleVariant; Usuarios: OleVariant; 
                        Modelos: OleVariant; var ErrorCount: Integer): OleVariant; dispid 305;
    function GetUsuariosRol(const Rol: WideString; out Modelos: OleVariant): OleVariant; dispid 307;
    function GetUserRoles(Empresa: OleVariant; iUsuario: Integer): OleVariant; dispid 308;
    function GrabaUsuarioRoles(Empresa: OleVariant; iUsuario: Integer; Delta: OleVariant; 
                               var ErrorCount: Integer): OleVariant; dispid 309;
    function GrabaEmpleadoUsuario(oEmpresa: OleVariant; iUsuario: Integer; iEmpleado: Integer): OleVariant; dispid 306;
    function EnrolamientoMasivo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 310;
    function EnrolamientoMasivoGetLista(oEmpresa: OleVariant; oParametros: OleVariant): OleVariant; dispid 311;
    function ImportarEnrolamientoMasivoGetASCII(oEmpresa: OleVariant; Parametros: OleVariant; 
                                                ListaASCII: OleVariant; out ErrorCount: Integer; 
                                                LoginAD: WordBool): OleVariant; dispid 312;
    function ImportarEnrolamientoMasivo(Empresa: OleVariant; Parametros: OleVariant; 
                                        Datos: OleVariant): OleVariant; dispid 313;
    function GetClasifiEmpresa(Empresa: OleVariant): OleVariant; dispid 314;
    function GetDerechosAdicionales(Empresa: OleVariant; Grupo: Integer): OleVariant; dispid 315;
    function GrabaAccesosAdicionales(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant; 
                                     out ErrorCount: Integer): OleVariant; dispid 316;
    function GetSistLstDispositivos(Empresa: OleVariant): OleVariant; dispid 317;
    function GrabaSistLstDispositivos(Empresa: OleVariant; oDelta: OleVariant; 
                                      out ErrorCount: Integer): OleVariant; dispid 318;
    function GetGlobalLstDispositivos(Empresa: OleVariant; var Disponibles: OleVariant; 
                                      var Asignados: OleVariant): OleVariant; dispid 319;
    function GrabaGlobalLstDispositivos(Empresa: OleVariant; oDelta: OleVariant; 
                                        out ErrorCount: Integer): OleVariant; dispid 320;
    function GetAccArbol(Empresa: OleVariant): OleVariant; dispid 321;
    procedure ActualizaUsuariosEmpresa(Empresa: OleVariant; Delta: OleVariant); dispid 322;
    function ConsultaBitacoraBiometrica(Parametros: OleVariant): OleVariant; dispid 324;
    function DepuraBitacoraBiometrica(Parametros: OleVariant): OleVariant; dispid 325;
    function ObtenIdBioMaximo: Integer; dispid 323;
    function ActualizaIdBio(const CodigoEmpresaLocal: WideString; Empleado: Integer; Id: Integer; 
                            const Grupo: WideString; Invitador: Integer): WordBool; dispid 326;
    function GetUsuarioCCosto(Empresa: OleVariant; iUsuario: Integer): OleVariant; dispid 327;
    function GrabaUsuarioCCosto(Empresa: OleVariant; oDelta: OleVariant; ErrorCount: Integer; 
                                iUsuario: Integer; const sTexto: WideString; 
                                const sNombreCosteo: WideString): OleVariant; dispid 328;
    function ConsultaListaGrupos: OleVariant; dispid 329;
    function GrabaSistListaGrupos(oDelta: OleVariant; UsuarioTerminal: Integer; 
                                  out ErrorCount: Integer): OleVariant; dispid 330;
    function ConsultaTermPorGrupo(const Grupo: WideString): OleVariant; dispid 331;
    function GrabaTermPorGrupo(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; dispid 332;
    procedure GrabaListaTermPorGrupo(const Terminales: WideString; UsuarioTerminal: Integer; 
                                     const Grupo: WideString); dispid 333;
    function AsignaNumBiometricos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 334;
    procedure ReiniciaSincTerminales(const Lista: WideString); dispid 335;
    function AsignaGrupoTerminales(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 336;
    function EmpleadosBiometricosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 337;
    function EmpleadosSinBiometricosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 338;
    function AsignaGrupoTerminalesLista(Empresa: OleVariant; Lista: OleVariant; 
                                        Parametros: OleVariant): OleVariant; dispid 339;
    function AsignaNumBiometricosLista(Empresa: OleVariant; Lista: OleVariant; 
                                       Parametros: OleVariant): OleVariant; dispid 340;
    procedure ActualizaTerminalesGTI(const Lista: WideString); dispid 341;
    function ConsultaMensaje(const Filtro: WideString): OleVariant; dispid 342;
    function GrabaMensaje(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; dispid 343;
    function ConsultaMensajeTerminal(const Filtro: WideString): OleVariant; dispid 344;
    function GrabaMensajeTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; 
                                  out ErrorCount: Integer): OleVariant; dispid 345;
    function ConsultaTerminal(const Filtro: WideString): OleVariant; dispid 346;
    function GrabaTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; dispid 347;
    function ImportaTerminales(Parametros: OleVariant; UsuarioTerminal: Integer): OleVariant; dispid 348;
    function ExisteHuellaRegistrada(Empleado: Integer; const Empresa: WideString): WordBool; dispid 349;
    procedure ReiniciaTerminal(Terminal: Integer; const Empresa: WideString); dispid 350;
    function ExisteEmpleadoEnGrupo(const Grupo: WideString): WordBool; dispid 351;
    function ObtieneTemplates(Parametros: OleVariant): OleVariant; dispid 352;
    procedure InsertaHuella(Parametros: OleVariant); dispid 353;
    procedure EliminaHuellas(const Empresa: WideString; Empleado: Integer; Invitador: Integer; 
                             Usuario: Integer); dispid 354;
    function ReportaHuella(Parametros: OleVariant): OleVariant; dispid 355;
    function ExisteHuellaInvitador(const Empresa: WideString; Invitador: Integer): WordBool; dispid 356;
    function GetCuentasTimbrado: OleVariant; dispid 357;
    function GrabaCuentasTimbrado(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 358;
    function ExistenEstructurasTimbradoComparte: WordBool; dispid 359;
    function CrearEstructurasTimbradoComparte: WordBool; dispid 360;
    function ExistenEstructurasTimbradoEmpresa(Empresa: OleVariant): WordBool; dispid 361;
    function CrearEstructurasTimbradoEmpresa(Empresa: OleVariant): WordBool; dispid 362;
    procedure CrearBitacora(Empresa: OleVariant; Tipo: Integer; Parametros: OleVariant); dispid 363;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerSistemaTimbrado provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerSistemaTimbrado exposed by              
// the CoClass dmServerSistemaTimbrado. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerSistemaTimbrado = class
    class function Create: IdmServerSistemaTimbrado;
    class function CreateRemote(const MachineName: string): IdmServerSistemaTimbrado;
  end;

implementation

uses ComObj;

class function CodmServerSistemaTimbrado.Create: IdmServerSistemaTimbrado;
begin
  Result := CreateComObject(CLASS_dmServerSistemaTimbrado) as IdmServerSistemaTimbrado;
end;

class function CodmServerSistemaTimbrado.CreateRemote(const MachineName: string): IdmServerSistemaTimbrado;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerSistemaTimbrado) as IdmServerSistemaTimbrado;
end;

end.
