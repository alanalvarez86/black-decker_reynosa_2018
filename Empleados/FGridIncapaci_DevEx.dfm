inherited GridIncapaci_DevEx: TGridIncapaci_DevEx
  Left = 98
  Top = 170
  Width = 807
  Height = 354
  BorderStyle = bsSizeable
  Caption = 'Registro de Incapacidades'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ZetaDBGrid: TZetaDBGrid [0]
    Width = 791
    Height = 230
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection]
    OnColExit = ZetaDBGridColExit
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'N'#250'mero'
        Width = 65
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre Completo'
        Width = 200
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IN_FEC_INI'
        Title.Caption = 'Inicio'
        Width = 100
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IN_DIAS'
        Title.Caption = 'D'#237'as'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IN_TIPO'
        Title.Caption = 'Tipo'
        Width = 40
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IN_NUMERO'
        Title.Caption = 'N'#250'mero'
        Width = 65
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IN_MOTIVO'
        Title.Caption = 'Motivo'
        Width = 90
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IN_FIN'
        Title.Caption = 'Fin de Incapacidad'
        Width = 100
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IN_TASA_IP'
        Title.Caption = '% Permanente'
        Width = 75
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IN_FEC_RH'
        Title.Caption = 'Fecha RH'
        Width = 100
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IN_DIASSUB'
        Title.Caption = 'D'#237'as Pago'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IN_NOMNUME'
        Title.Caption = 'N'#243'mina Pago'
        Width = 72
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IN_SUA_INI'
        Title.Caption = 'Fecha IMSS'
        Width = 100
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IN_COMENTA'
        Title.Caption = 'Comentarios'
        Width = 135
        Visible = True
      end>
  end
  inherited PanelBotones: TPanel [1]
    Top = 280
    Width = 791
    inherited OK_DevEx: TcxButton
      Left = 624
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 704
    end
  end
  inherited PanelIdentifica: TPanel [2]
    Width = 791
    inherited ValorActivo2: TPanel
      Width = 465
      inherited textoValorActivo2: TLabel
        Width = 459
      end
    end
  end
  object ZFecha: TZetaDBFecha [4]
    Left = 272
    Top = 152
    Width = 105
    Height = 22
    Cursor = crArrow
    TabStop = False
    TabOrder = 4
    Text = '14/oct/99'
    Valor = 36447.000000000000000000
    Visible = False
    DataField = 'IN_FEC_INI'
    DataSource = DataSource
  end
  object zComboMot: TZetaDBKeyCombo [5]
    Left = 272
    Top = 184
    Width = 83
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    DropDownCount = 3
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 5
    TabStop = False
    Visible = False
    Items.Strings = (
      'Descanso')
    ListaFija = lfMotivoIncapacidad
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'IN_MOTIVO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object zComboFin: TZetaDBKeyCombo [6]
    Left = 272
    Top = 208
    Width = 83
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    DropDownCount = 3
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 6
    TabStop = False
    Visible = False
    Items.Strings = (
      'Descanso')
    ListaFija = lfFinIncapacidad
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'IN_FIN'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object ZFechaSUA: TZetaDBFecha [7]
    Left = 400
    Top = 152
    Width = 105
    Height = 22
    Cursor = crArrow
    TabStop = False
    TabOrder = 8
    Text = '14/oct/99'
    Valor = 36447.000000000000000000
    Visible = False
    DataField = 'IN_SUA_INI'
    DataSource = DataSource
  end
  object btnBuscaPeriodo: TcxButton [8]
    Left = 400
    Top = 184
    Width = 17
    Height = 17
    Hint = 'Buscar Periodo'
    TabOrder = 7
    Visible = False
    OnClick = btnBuscaPeriodoClick
    OptionsImage.Glyph.Data = {
      BA030000424DBA0300000000000036000000280000000F0000000F0000000100
      20000000000084030000000000000000000000000000000000008B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFCAC6
      CBFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFFB5AEB5FFFFFFFFFFFDFDFDFF9F97
      A0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF9F97A0FFFAF9FAFFFFFFFFFFBAB4BBFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF988F99FFC0BAC0FFD2CE
      D2FFBEB8BFFFEFEDEFFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFAF9FAFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFE9E7E9FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8F8590FFF4F3F4FFF0EFF1FFAFA9B0FF908791FFAFA9B0FFFBFB
      FBFFE9E7E9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFFA8A1A9FFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFFCAC6CBFFFFFF
      FFFF9D959EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFAAA3
      ABFFFFFFFFFFB7B0B7FF8B818CFF8B818CFF8B818CFFC5C0C6FFFFFFFFFFA39B
      A3FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFFBFB
      FBFFE9E7E9FF8B818CFF8B818CFF968D97FFEDEBEDFFF8F7F8FF908791FF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFBEB8BFFFFFFF
      FFFFF8F7F8FFDCD9DDFFFAF9FAFFFDFDFDFFB5AEB5FF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFAEA7AEFFE7E5
      E8FFF0EFF1FFE0DDE0FFAAA3ABFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
  end
  object ZFechaRH: TZetaDBFecha [9]
    Left = 536
    Top = 152
    Width = 105
    Height = 22
    Cursor = crArrow
    TabStop = False
    TabOrder = 9
    Text = '14/oct/99'
    Valor = 36447.000000000000000000
    Visible = False
    DataField = 'IN_FEC_RH'
    DataSource = DataSource
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 1
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
