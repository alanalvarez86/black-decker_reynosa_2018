unit FHisSeguroGastosMedicos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, Db,
  ZetaDBGrid, ExtCtrls, StdCtrls, Buttons, ZetaKeyCombo, ZetaKeyLookup,
  ZetaEdit;

type
  THisSegurosGastosMedicos = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    Panel1: TPanel;
    lkpSGMS: TZetaKeyLookup;
    Label1: TLabel;
    Label2: TLabel;
    zcbStatus: TZetaKeyCombo;
    btnRefrescar: TBitBtn;
    txtPoliza: TZetaEdit;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btnRefrescarClick(Sender: TObject);
  private
    procedure AplicaFiltro;
    { Private declarations }
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  HisSegurosGastosMedicos: THisSegurosGastosMedicos;

implementation

{$R *.DFM}

uses dRecursos, DCatalogos,dSistema, ZetaCommonLists,ZetaCommonTools, ZetaCommonClasses,ZAccesosTress,ZAccesosMgr ;

procedure THisSegurosGastosMedicos.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;

     with dmRecursos do
     begin
          cdsHisSGM.Conectar;
          DataSource.DataSet:= cdsHisSGM;
     end;
     with dmCatalogos do
     begin
          cdsSegGastosMed.Conectar;
          lkpSGMS.LookupDataset := cdsSegGastosMed;
     end;
     zcbStatus.ItemIndex := 0;
     AplicaFiltro;
end;

procedure THisSegurosGastosMedicos.Refresh;
begin
     dmRecursos.cdsHisSGM.Refrescar;
     AplicaFiltro;
end;

procedure THisSegurosGastosMedicos.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H_EMP_EXP_SGM;
     IndexDerechos := D_EMP_SEGUROS_GASTOS_MEDICOS;
     zcbStatus.Items.Insert(0,'<Todos>');
end;

procedure THisSegurosGastosMedicos.Agregar;
begin
     dmRecursos.cdsHisSGM.Agregar;
end;

procedure THisSegurosGastosMedicos.Borrar;
begin
     dmRecursos.cdsHisSGM.Borrar;
end;

procedure THisSegurosGastosMedicos.Modificar;
begin
     dmRecursos.cdsHisSGM.Modificar;
end;


procedure THisSegurosGastosMedicos.btnRefrescarClick(Sender: TObject);
begin
     inherited;
     AplicaFiltro;
end;

procedure THisSegurosGastosMedicos.AplicaFiltro;

function GetFiltro:string;
begin
     if zcbStatus.Valor >= 0 then
        Result := ConcatFiltros( Result,Format('EP_STATUS = %d',[zcbStatus.Valor] ) );

     if StrLleno(lkpSGMS.Llave)then
     begin
          Result := ConcatFiltros( Result,Format('PM_CODIGO = ''%s''',[lkpSGMS.Llave]) );
     end;

     if StrLleno( txtPoliza.Text )then
     begin
          Result := ConcatFiltros( Result,Format('PM_NUMERO = ''%s''',[txtPoliza.Text]) );
     end;
end;

begin
     with dmRecursos.cdsHisSGM do
     begin
          Filtered := False;
          Filter := GetFiltro;
          if StrLleno(Filter)then
             Filtered := True;
     end;
end;

function THisSegurosGastosMedicos.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho(D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_CONSULTA );
end;

function THisSegurosGastosMedicos.PuedeBorrar(
  var sMensaje: String): Boolean;
begin
      sMensaje := 'No Tiene Permisos para Borrar';
     IF dmRecursos.cdsHisSGM.FieldByName('EP_STATUS').AsInteger = Ord(ssVencida) then
     begin
          sMensaje := 'No Tiene Permisos para Borrar P�lizas Vencidas';
          Result := ZAccesosMgr.CheckDerecho(D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_BAJA ) and ZAccesosMgr.CheckDerecho(D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_SIST_KARDEX );
     end
     else
         Result := ZAccesosMgr.CheckDerecho(D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_BAJA );
end;

end.
