inherited EditHisSeguroGastosMedicos_DevEx: TEditHisSeguroGastosMedicos_DevEx
  Left = 295
  Top = 26
  Caption = 'Seguro de Gastos M'#233'dicos'
  ClientHeight = 674
  ClientWidth = 504
  PixelsPerInch = 96
  TextHeight = 13
  object lblDependiente: TLabel [0]
    Left = 54
    Top = 276
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pariente:'
    Enabled = False
  end
  object Label8: TLabel [1]
    Left = 43
    Top = 350
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'Certificado:'
  end
  object Label9: TLabel [2]
    Left = 68
    Top = 379
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'Inicio:'
  end
  object Label10: TLabel [3]
    Left = 272
    Top = 379
    Width = 17
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fin:'
  end
  object Label17: TLabel [4]
    Left = 63
    Top = 408
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status:'
  end
  object Label16: TLabel [5]
    Left = 228
    Top = 350
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Endoso Alta:'
  end
  inherited PanelBotones: TPanel
    Top = 638
    Width = 504
    TabOrder = 8
    inherited OK_DevEx: TcxButton
      Left = 340
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 419
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 504
    Height = 20
    TabOrder = 9
    inherited Splitter: TSplitter
      Height = 20
    end
    inherited ValorActivo1: TPanel
      Height = 20
    end
    inherited ValorActivo2: TPanel
      Width = 178
      Height = 20
      inherited textoValorActivo2: TLabel
        Width = 172
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 17
  end
  object gbxTipo: TcxRadioGroup [9]
    Left = 99
    Top = 295
    BiDiMode = bdLeftToRight
    Caption = ' Tipo '
    ParentBiDiMode = False
    Properties.Columns = 2
    Properties.Items = <
      item
        Caption = 'Titular'
        Value = '0'
      end
      item
        Caption = 'Dependiente'
        Value = '1'
      end>
    ItemIndex = 0
    TabOrder = 3
    OnClick = gbxTipoClick
    Height = 45
    Width = 319
  end
  object EP_CERTIFI: TZetaDBEdit [10]
    Left = 99
    Top = 347
    Width = 125
    Height = 21
    TabOrder = 4
    DataField = 'EP_CERTIFI'
    DataSource = DataSource
  end
  object EP_FEC_INI: TZetaDBFecha [11]
    Left = 99
    Top = 376
    Width = 125
    Height = 22
    Cursor = crArrow
    TabOrder = 5
    Text = '23/mar/12'
    Valor = 40991.000000000000000000
    DataField = 'EP_FEC_INI'
    DataSource = DataSource
  end
  object EP_FEC_FIN: TZetaDBFecha [12]
    Left = 291
    Top = 376
    Width = 125
    Height = 22
    Cursor = crArrow
    TabOrder = 6
    Text = '23/mar/12'
    Valor = 40991.000000000000000000
    DataField = 'EP_FEC_FIN'
    DataSource = DataSource
  end
  object EP_STATUS: TZetaDBKeyCombo [13]
    Left = 99
    Top = 405
    Width = 126
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 7
    OnCloseUp = EP_STATUSCloseUp
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'EP_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object cbxDependiente: TZetaKeyCombo [14]
    Left = 99
    Top = 274
    Width = 318
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 2
    OnChange = cbxDependienteChange
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  object Panel1: TPanel [15]
    Left = 0
    Top = 51
    Width = 504
    Height = 169
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 56
      Top = 39
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 61
      Top = 59
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'P'#243'liza:'
    end
    object Label3: TLabel
      Left = 37
      Top = 82
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Referencia:'
    end
    object Label4: TLabel
      Left = 47
      Top = 100
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = 'Vigencia:'
    end
    object Label5: TLabel
      Left = 31
      Top = 117
      Width = 61
      Height = 13
      Alignment = taRightJustify
      Caption = 'Condiciones:'
    end
    object txtPV_FEC_INI: TZetaTextBox
      Left = 96
      Top = 100
      Width = 137
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object txtPM_NUMERO: TZetaTextBox
      Left = 96
      Top = 59
      Width = 137
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label6: TLabel
      Left = 237
      Top = 103
      Width = 9
      Height = 13
      Alignment = taRightJustify
      Caption = 'a:'
    end
    object txtPV_FEC_FIN: TZetaTextBox
      Left = 248
      Top = 100
      Width = 137
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object txtPV_CONDIC: TZetaTextBox
      Left = 96
      Top = 119
      Width = 369
      Height = 47
      AutoSize = False
      ShowAccelChar = False
      WordWrap = True
      Brush.Color = clBtnFace
      Border = True
    end
    object Label7: TLabel
      Left = 60
      Top = 16
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = 'Orden:'
    end
    object PM_CODIGO: TZetaDBKeyLookup_DevEx
      Left = 96
      Top = 35
      Width = 369
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 1
      TabStop = True
      WidthLlave = 60
      OnExit = PM_CODIGOExit
      DataField = 'PM_CODIGO'
      DataSource = DataSource
    end
    object PV_REFEREN: TZetaDBKeyCombo
      Left = 96
      Top = 78
      Width = 137
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
      OnCloseUp = PV_REFERENCloseUp
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      LlaveNumerica = False
    end
    object EP_ORDEN: TZetaDBNumero
      Left = 96
      Top = 11
      Width = 59
      Height = 21
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
      OnChange = EP_CFIJOChange
      DataField = 'EP_ORDEN'
      DataSource = DataSource
    end
  end
  object rdgAsegurado: TcxRadioGroup [16]
    Left = 99
    Top = 224
    BiDiMode = bdLeftToRight
    Caption = ' Asegurado '
    ParentBiDiMode = False
    Properties.Columns = 2
    Properties.Items = <
      item
        Caption = 'Empleado'
        Value = '0'
      end
      item
        Caption = 'Pariente'
        Value = '1'
      end>
    ItemIndex = 0
    TabOrder = 1
    OnClick = rdgAseguradoClick
    Height = 45
    Width = 319
  end
  object pgPaginas: TcxPageControl [17]
    Left = 0
    Top = 431
    Width = 504
    Height = 173
    Align = alBottom
    TabOrder = 11
    Properties.ActivePage = TabSheet1
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 171
    ClientRectLeft = 2
    ClientRectRight = 502
    ClientRectTop = 28
    object TabSheet1: TcxTabSheet
      Caption = 'Costos'
      object Label15: TLabel
        Left = 22
        Top = 121
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Costo Empresa:'
      end
      object Label13: TLabel
        Left = 16
        Top = 95
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Costo Empleado:'
      end
      object Label14: TLabel
        Left = 29
        Top = 69
        Width = 67
        Height = 13
        Alignment = taRightJustify
        Caption = 'Total a Pagar:'
      end
      object Label12: TLabel
        Left = 35
        Top = 44
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = 'Costo P'#243'liza:'
      end
      object Label11: TLabel
        Left = 47
        Top = 20
        Width = 49
        Height = 13
        Alignment = taRightJustify
        Caption = 'Costo Fijo:'
      end
      object txtTotalPagar: TZetaTextBox
        Left = 99
        Top = 66
        Width = 125
        Height = 19
        Alignment = taRightJustify
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object lblTablaCotizacion: TLabel
        Left = 263
        Top = 44
        Width = 37
        Height = 13
        Caption = 'lblTabla'
      end
      object bbtnBuscarTablaCotizacion: TcxButton
        Left = 228
        Top = 40
        Width = 21
        Height = 21
        Hint = 'Tablas de Cotizaci'#243'n'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = bbtnBuscarTablaCotizacionClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A4050000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF968D97FFE9E7E9FFAAA3ABFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8D838EFFE2DFE2FFFFFF
          FFFFFDFDFDFF9B939CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFCAC6CBFFFFFFFFFFFFFFFFFFC8C4C9FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFB1ABB2FFFFFFFFFFFFFFFFFFE0DDE0FF8D838EFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF9B939CFFD0CCD0FFF2F1F2FFFBFBFBFFE2DFE2FFFAF9FAFFFFFFFFFFF0EF
          F1FF948B95FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF9B939CFFF4F3F4FFFFFFFFFFFBFBFBFFF2F1F2FFFFFF
          FFFFFFFFFFFFFBFBFBFFA39BA3FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFDBD7DBFFFFFFFFFFD5D2
          D6FF968D97FF8B818CFFA39BA3FFF4F3F4FFFFFFFFFFA199A2FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF968D
          97FFFFFFFFFFE9E7E9FF8B818CFF8B818CFF8B818CFF8B818CFFB5AEB5FFFFFF
          FFFFCAC6CBFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF9A919AFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B81
          8CFF8B818CFF9A919AFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFFBFBFBFFEFED
          EFFF8D838EFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFC7C2C7FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFD7D4D7FFFFFFFFFFD2CED2FF928993FF8F8590FFBEB8BFFFF6F5
          F6FFFDFDFDFF9B939CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFEDEBEDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEB8BFFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF928993FFC5C0C6FFF0EFF1FFF0EFF1FFDBD7DBFFAAA3ABFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        OptionsImage.Margin = 1
      end
      object EP_CFIJO: TZetaDBNumero
        Left = 99
        Top = 16
        Width = 125
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        OnChange = EP_CFIJOChange
        DataField = 'EP_CFIJO'
        DataSource = DataSource
      end
      object EP_CPOLIZA: TZetaDBNumero
        Left = 99
        Top = 40
        Width = 125
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
        OnChange = EP_CFIJOChange
        DataField = 'EP_CPOLIZA'
        DataSource = DataSource
      end
      object EP_CTITULA: TZetaDBNumero
        Left = 99
        Top = 91
        Width = 125
        Height = 21
        Mascara = mnPesos
        TabOrder = 3
        Text = '0.00'
        OnChange = EP_CTITULAChange
        DataField = 'EP_CTITULA'
        DataSource = DataSource
      end
      object txtCEMPRES: TZetaNumero
        Left = 99
        Top = 117
        Width = 125
        Height = 21
        Mascara = mnPesos
        TabOrder = 4
        Text = '0.00'
        OnExit = txtCEMPRESExit
      end
    end
    object TabSheet3: TcxTabSheet
      Caption = 'Observaciones'
      ImageIndex = 2
      object EP_OBSERVA: TcxDBMemo
        Left = 0
        Top = 0
        Align = alClient
        DataBinding.DataField = 'EP_OBSERVA'
        DataBinding.DataSource = DataSource
        ParentFont = False
        Properties.MaxLength = 255
        Properties.ScrollBars = ssVertical
        TabOrder = 0
        Height = 143
        Width = 500
      end
    end
    object TabCancelacion: TcxTabSheet
      Caption = 'Cancelaci'#243'n'
      ImageIndex = 1
      object lblFechaCan: TLabel
        Left = 64
        Top = 13
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
        Enabled = False
      end
      object lblMontoCan: TLabel
        Left = 11
        Top = 40
        Width = 86
        Height = 13
        Caption = 'Monto de endoso:'
        Enabled = False
      end
      object lblMotivoCan: TLabel
        Left = 62
        Top = 62
        Width = 35
        Height = 13
        Caption = 'Motivo:'
        Enabled = False
      end
      object lblFolioEndoso: TLabel
        Left = 3
        Top = 88
        Width = 93
        Height = 13
        Alignment = taRightJustify
        Caption = 'Endoso Cancelado:'
        Enabled = False
      end
      object EP_CAN_FEC: TZetaDBFecha
        Left = 99
        Top = 11
        Width = 125
        Height = 22
        Cursor = crArrow
        Enabled = False
        TabOrder = 0
        Text = '23/mar/12'
        Valor = 40991.000000000000000000
        DataField = 'EP_CAN_FEC'
        DataSource = DataSource
      end
      object EP_CAN_MON: TZetaDBNumero
        Left = 99
        Top = 36
        Width = 108
        Height = 21
        Enabled = False
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        DataField = 'EP_CAN_MON'
        DataSource = DataSource
      end
      object EP_CAN_MOT: TZetaDBEdit
        Left = 99
        Top = 60
        Width = 318
        Height = 21
        Enabled = False
        TabOrder = 2
        DataField = 'EP_CAN_MOT'
        DataSource = DataSource
      end
      object EP_END_CAN: TZetaDBEdit
        Left = 99
        Top = 84
        Width = 125
        Height = 21
        Enabled = False
        TabOrder = 3
        DataField = 'EP_END_CAN'
        DataSource = DataSource
      end
    end
  end
  object Panel2: TPanel [18]
    Left = 0
    Top = 604
    Width = 504
    Height = 34
    Align = alBottom
    TabOrder = 12
    object Label21: TLabel
      Left = 55
      Top = 12
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Modific'#243':'
    end
    object US_DESCRIP: TZetaDBTextBox
      Left = 101
      Top = 8
      Width = 166
      Height = 17
      AutoSize = False
      Caption = 'US_DESCRIP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_DESCRIP'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label22: TLabel
      Left = 270
      Top = 12
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Modificado:'
    end
    object EP_FEC_REG: TZetaDBTextBox
      Left = 328
      Top = 8
      Width = 91
      Height = 17
      AutoSize = False
      Caption = 'EP_FEC_REG'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'EP_FEC_REG'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label23: TLabel
      Left = 425
      Top = 12
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Global:'
    end
    object EP_GLOBAL: TZetaDBTextBox
      Left = 461
      Top = 8
      Width = 30
      Height = 17
      AutoSize = False
      Caption = 'EP_GLOBAL'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'EP_GLOBAL'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object EP_END_ALT: TZetaDBEdit [19]
    Left = 291
    Top = 347
    Width = 125
    Height = 21
    TabOrder = 13
    DataField = 'EP_END_ALT'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 568
    Top = 60
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
