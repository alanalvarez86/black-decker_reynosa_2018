inherited EditPlanVacacion_DevEx: TEditPlanVacacion_DevEx
  Left = 419
  Top = 126
  Caption = 'Solicitud de Vacaciones'
  ClientHeight = 447
  ClientWidth = 769
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 17
    Top = 367
    Width = 37
    Height = 13
    Alignment = taRightJustify
    Caption = 'Solicita:'
  end
  object Label4: TLabel [1]
    Left = 392
    Top = 392
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'Procesada:'
  end
  object txtProcesada: TZetaTextBox [2]
    Left = 448
    Top = 390
    Width = 312
    Height = 17
    AutoSize = False
    Caption = 'txtProcesada'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object Label6: TLabel [3]
    Left = 405
    Top = 366
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Autoriza:'
  end
  object USR_SOL_DES: TZetaDBTextBox [4]
    Left = 59
    Top = 365
    Width = 209
    Height = 17
    AutoSize = False
    Caption = 'USR_SOL_DES'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'USR_SOL_DES'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object VP_SOL_FEC: TZetaDBTextBox [5]
    Left = 276
    Top = 365
    Width = 107
    Height = 17
    AutoSize = False
    Caption = 'VP_SOL_FEC'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'VP_SOL_FEC'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object USR_AUT_DES: TZetaDBTextBox [6]
    Left = 448
    Top = 364
    Width = 193
    Height = 17
    AutoSize = False
    Caption = 'USR_AUT_DES'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'USR_AUT_DES'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object VP_AUT_FEC: TZetaDBTextBox [7]
    Left = 648
    Top = 364
    Width = 111
    Height = 17
    AutoSize = False
    Caption = 'VP_AUT_FEC'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'VP_AUT_FEC'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 411
    Width = 769
    inherited OK_DevEx: TcxButton
      Left = 601
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 680
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 769
    inherited ValorActivo2: TPanel
      Width = 443
      inherited textoValorActivo2: TLabel
        Width = 437
      end
    end
  end
  object Panel1: TPanel [11]
    Left = 0
    Top = 50
    Width = 769
    Height = 38
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 16
      Top = 10
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
      FocusControl = CB_CODIGO
    end
    object Label2: TLabel
      Left = 449
      Top = 10
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status:'
      FocusControl = VP_STATUS
    end
    object CB_CODIGO: TZetaDBKeyLookup_DevEx
      Left = 72
      Top = 8
      Width = 300
      Height = 21
      Enabled = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CB_CODIGO'
      DataSource = DataSource
    end
    object VP_STATUS: TZetaDBKeyCombo
      Left = 488
      Top = 8
      Width = 145
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 1
      OnChange = VP_STATUSChange
      ListaFija = lfStatusPlanVacacion
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'VP_STATUS'
      DataSource = DataSource
      LlaveNumerica = True
    end
  end
  object Panel3: TPanel [12]
    Left = 392
    Top = 83
    Width = 369
    Height = 273
    BevelOuter = bvNone
    TabOrder = 5
    object GroupBox2: TGroupBox
      Left = 0
      Top = 113
      Width = 369
      Height = 160
      Align = alClient
      Caption = ' Observaciones '
      TabOrder = 1
      object VP_AUT_COM: TcxDBMemo
        Left = 2
        Top = 15
        Align = alClient
        DataBinding.DataField = 'VP_AUT_COM'
        DataBinding.DataSource = DataSource
        Properties.ScrollBars = ssVertical
        TabOrder = 0
        Height = 143
        Width = 365
      end
    end
    object gpbxSeccPerPago: TGroupBox
      Left = 0
      Top = 0
      Width = 369
      Height = 113
      Align = alTop
      Caption = ' Periodo de pago '
      TabOrder = 0
      Visible = False
      object lblNumero: TLabel
        Left = 10
        Top = 83
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object lblAnio: TLabel
        Left = 28
        Top = 59
        Width = 22
        Height = 13
        Alignment = taRightJustify
        Caption = 'A'#241'o:'
      end
      object lblTipo: TLabel
        Left = 128
        Top = 59
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object RBAutomatico: TcxRadioButton
        Left = 9
        Top = 16
        Width = 209
        Height = 17
        Caption = 'Autom'#225'tico ( Calculado por el sistema )'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = RBEspecPeriodoClick
      end
      object RBEspecPeriodo: TcxRadioButton
        Left = 9
        Top = 34
        Width = 113
        Height = 17
        Caption = 'Especificar periodo'
        TabOrder = 1
        OnClick = RBEspecPeriodoClick
      end
      object VP_NOMTIPO: TZetaDBKeyCombo
        Left = 159
        Top = 55
        Width = 190
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 3
        ListaFija = lfTipoPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
        DataField = 'VP_NOMTIPO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object VP_NOMYEAR: TZetaDBNumero
        Left = 56
        Top = 55
        Width = 60
        Height = 21
        Mascara = mnDias
        TabOrder = 2
        Text = '0'
        DataField = 'VP_NOMYEAR'
        DataSource = DataSource
      end
      object VP_NOMNUME: TZetaDBKeyLookup_DevEx
        Left = 56
        Top = 80
        Width = 293
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        OnValidKey = VP_NOMNUMEValidKey
        DataField = 'VP_NOMNUME'
        DataSource = DataSource
      end
    end
  end
  object Panel2: TPanel [13]
    Left = 8
    Top = 83
    Width = 377
    Height = 273
    BevelOuter = bvNone
    TabOrder = 4
    object GroupBox1: TGroupBox
      Left = 0
      Top = 113
      Width = 377
      Height = 160
      Align = alClient
      Caption = ' Observaciones '
      TabOrder = 1
      object VP_SOL_COM: TcxDBMemo
        Left = 2
        Top = 15
        Align = alClient
        DataBinding.DataField = 'VP_SOL_COM'
        DataBinding.DataSource = DataSource
        Enabled = False
        Properties.ScrollBars = ssVertical
        TabOrder = 0
        Height = 143
        Width = 373
      end
    end
    object GroupBox4: TGroupBox
      Left = 0
      Top = 0
      Width = 377
      Height = 113
      Align = alTop
      Caption = ' Vacaciones '
      TabOrder = 0
      object VP_DIAS: TZetaDBTextBox
        Left = 63
        Top = 62
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'VP_DIAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VP_DIAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object LblGozados: TLabel
        Left = 33
        Top = 64
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as:'
      end
      object LVA_VAC_AL: TLabel
        Left = 16
        Top = 41
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Regresa:'
        FocusControl = VP_FEC_FIN
      end
      object LVA_VAC_DEL: TLabel
        Left = 27
        Top = 18
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Salida:'
        FocusControl = VP_FEC_INI
      end
      object VP_FEC_FIN: TZetaDBFecha
        Left = 63
        Top = 39
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '16/dic/97'
        Valor = 35780.000000000000000000
        DataField = 'VP_FEC_FIN'
        DataSource = DataSource
      end
      object VP_FEC_INI: TZetaDBFecha
        Left = 63
        Top = 16
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '16/dic/97'
        Valor = 35780.000000000000000000
        DataField = 'VP_FEC_INI'
        DataSource = DataSource
      end
      object gbSaldos: TGroupBox
        Left = 183
        Top = 18
        Width = 161
        Height = 62
        Caption = ' Saldo de vacaciones '
        TabOrder = 2
        object Label5: TLabel
          Left = 6
          Top = 18
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'os anteriores:'
        end
        object Label10: TLabel
          Left = 20
          Top = 38
          Width = 62
          Height = 13
          Alignment = taRightJustify
          Caption = 'Proporcional:'
        end
        object VP_SAL_ANT: TZetaDBTextBox
          Left = 88
          Top = 16
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'VP_SAL_ANT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'VP_SAL_ANT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object VP_SAL_PRO: TZetaDBTextBox
          Left = 88
          Top = 36
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'VP_SAL_PRO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'VP_SAL_PRO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 460
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
