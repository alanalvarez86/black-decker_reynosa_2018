unit FEditHisVacacion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, DBCtrls, Buttons, StdCtrls, ExtCtrls,
  Mask, ZetaDBTextBox,
  ComCtrls, ZetaKeyCombo, ZetaNumero, ZetaFecha,
  ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, ZetaKeyLookup_DevEx,
  dxSkinsDefaultPainters, cxRadioGroup, dxBarBuiltInMenu;

type
  TEditHisVacacion_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    LVA_VAC_DEL: TLabel;
    VA_FEC_INI: TZetaDBFecha;
    LblGozados: TLabel;
    VA_GOZO: TZetaDBNumero;
    LVA_VAC_AL: TLabel;
    VA_FEC_FIN: TZetaDBFecha;
    gbSaldos: TGroupBox;
    Label5: TLabel;
    ZSaldoGozo: TZetaTextBox;
    Label10: TLabel;
    ZSaldoPago: TZetaTextBox;
    ZSaldoPrima: TZetaTextBox;
    Label11: TLabel;
    RBAniversario: TRadioButton;
    RBFecha: TRadioButton;
    cxPageControl: TcxPageControl;
    TabMovimiento_DevEx: TcxTabSheet;
    TabGenerales_DevEx: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    LblPago: TLabel;
    VA_PAGO: TZetaDBNumero;
    L_VA_P_PRIMA: TLabel;
    VA_P_PRIMA: TZetaDBNumero;
    LblPeriodo: TLabel;
    VA_PERIODO: TDBEdit;
    LblObservaciones: TLabel;
    VA_COMENTA: TDBMemo;
    GBNominaPago: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    VA_NOMTIPO: TZetaDBKeyCombo;
    VA_NOMNUME: TZetaDBKeyLookup_DevEx;
    VA_NOMYEAR: TZetaDBNumero;
    Label1: TLabel;
    CB_SALARIO: TZetaDBTextBox;
    Monto: TLabel;
    VA_MONTO: TZetaDBTextBox;
    Label21: TLabel;
    VA_SEVEN: TZetaDBTextBox;
    Label22: TLabel;
    VA_PRIMA: TZetaDBTextBox;
    LblOtros: TLabel;
    VA_OTROS: TZetaDBNumero;
    TotalLbl: TLabel;
    VA_TOTAL: TZetaDBTextBox;
    VA_TASA_PR: TZetaDBTextBox;
    Label3: TLabel;
    Bevel1: TBevel;
    LTablaIMSS: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    GCapturoLbl: TLabel;
    US_DESCRIP: TZetaDBTextBox;
    GCapturaLbl: TLabel;
    VA_CAPTURA: TZetaDBTextBox;
    Label4: TLabel;
    VA_GLOBAL: TDBCheckBox;
    Label6: TLabel;
    ZetaDBTextBox3: TZetaDBTextBox;
    BtnRecalculo_DevEx: TcxButton;
    BtnRegreso_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure DataSourceStateChange(Sender: TObject);
    //procedure BtnRecalculoClick(Sender: TObject);
    //procedure BtnRegresoClick(Sender: TObject);
    procedure RBSaldosClick(Sender: TObject);
    procedure VA_NOMNUMEValidKey(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnRegreso_DevExClick(Sender: TObject);
    procedure BtnRecalculo_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetFiltroPeriodo;
    procedure SetInfoSaldos;
  protected
    procedure ImprimirForma;override;
    procedure Connect;override;
    procedure SetControls;
  public
    { Public declarations }
  end;

var
  EditHisVacacion_DevEx: TEditHisVacacion_DevEx;

implementation

uses dCatalogos,
     dSistema,
     dCliente,
{$ifdef SUPERVISORES}
     dSuper,
{$else}
     dRecursos,
{$endif}
     dGlobal,
     ZGlobalTress,
     ZImprimeForma,
     ZetaTipoEntidad,
     ZAccesosTress, ZetaCommonClasses, ZetaCommonLists,
     ZAccesosMgr;

{$R *.DFM}

procedure TEditHisVacacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     VA_NOMNUME.LookupDataset := dmCatalogos.cdsPeriodo;
     FirstControl := VA_FEC_INI;
     VA_PERIODO.MaxLength := K_ANCHO_DESCRIPCION;
     VA_COMENTA.MaxLength := K_ANCHO_TITULO;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H11510_vacaciones;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_VACA;
end;

procedure TEditHisVacacion_DevEx.FormShow(Sender: TObject);
begin
     //PageControl.ActivePage := TabMovimiento;
     cxPageControl.ActivePage := TabMovimiento_DevEx;
     {$ifndef SUPERVISORES}
              //TabGenerales.TabVisible := ZAccesosMgr.CheckDerecho( D_REPORTES_CONFIDENCIALES, K_DERECHO_CONSULTA );
              TabGenerales_DevEx.TabVisible := ZAccesosMgr.CheckDerecho( D_REPORTES_CONFIDENCIALES, K_DERECHO_CONSULTA );
     {$endif}
     VA_NOMTIPO.ListaFija:=lfTipoPeriodo; //acl
     inherited;
     SetControls;
     dmCatalogos.AplicaFiltroStatusPeriodo(VA_NOMNUME,True);
end;

procedure TEditHisVacacion_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     dmCatalogos.cdsPeriodo.Conectar;
{$ifdef SUPERVISORES}
     with dmSuper do
{$else}
     with dmRecursos do
{$endif}
     begin
          cdsHisVacacion.Conectar;
          DataSource.DataSet := cdsHisVacacion;
     end;
     SetFiltroPeriodo;         //dmCatalogos.cdsPeriodo.Conectar; .. SetFiltro se encarga de Conectar el DataSet con el a�o y Tipo apropiado seg�n el registro de Kardex que se edita
end;

procedure TEditHisVacacion_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if Assigned( Field ) then
     begin
          with Field do
          begin
               if ( FieldName = 'VA_NOMYEAR' ) or ( FieldName = 'VA_NOMTIPO' ) then
               begin
                    SetFiltroPeriodo
               end
               else if ( FieldName = 'VA_FEC_INI' ) then
                  SetInfoSaldos;
          end;
     end;
end;

procedure TEditHisVacacion_DevEx.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     with DataSource do
     begin
          if ( State = dsBrowse ) then
             SetFiltroPeriodo;
          gbSaldos.Visible := ( State = dsInsert );
          SetInfoSaldos;
     end;
end;

procedure TEditHisVacacion_DevEx.SetFiltroPeriodo;
var
   iYear : Integer;
   TipoPer : eTipoPeriodo;
begin
     VA_NOMNUME.Filtro := VACIO;
     with DataSource.DataSet do
     begin
          iYear := FieldByName( 'VA_NOMYEAR' ).AsInteger;
          TipoPer := eTipoPeriodo( FieldByName( 'VA_NOMTIPO' ).AsInteger );
     end;
     with dmCatalogos do
     begin
          with cdsPeriodo do
          begin
               Filtered := False;
               if ( IsEmpty ) or
                  ( ( iYear <> FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPer ) <> FieldByName( 'PE_TIPO' ).AsInteger ) ) then
               begin
                    GetDatosPeriodo( iYear, TipoPer );
                    // Para que se refresque el Control de Lookup:
                    with VA_NOMNUME do
                    begin
                         SetLlaveDescripcion( VACIO, VACIO );
                         Llave := DataSource.DataSet.FieldByName( 'VA_NOMNUME' ).AsString;
                    end;
               end;
          end;
          dmCatalogos.AplicaFiltroStatusPeriodo(VA_NOMNUME,True);
     end;
{
     with dmRecursos.cdsHisVacacion, dmCatalogos do
     begin
          if ( not cdsPeriodo.IsEmpty ) then
          begin
               if ( ( FieldByName( 'VA_NOMYEAR' ).AsInteger <> cdsPeriodo.FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( FieldByName( 'VA_NOMTIPO' ).AsInteger <> cdsPeriodo.FieldByName( 'PE_TIPO' ).AsInteger ) ) then
                  GetDatosPeriodo( FieldByName( 'VA_NOMYEAR' ).AsInteger, eTipoPeriodo( FieldByName( 'VA_NOMTIPO' ).AsInteger ) );
          end
          else
              GetDatosPeriodo( FieldByName( 'VA_NOMYEAR' ).AsInteger, eTipoPeriodo( FieldByName( 'VA_NOMTIPO' ).AsInteger ) );
     end;
}
end;



procedure TEditHisVacacion_DevEx.SetInfoSaldos;
var
   rGozo, rPago, rPrima : TPesos;
begin
     if ( gbSaldos.Visible ) then
     begin
{$ifdef SUPERVISORES}
          with dmSuper do
{$else}
          with dmRecursos do
{$endif}
          begin
               SetSaldosVacaciones( RBAniversario.Checked, rGozo, rPago, rPrima );
          end;
          ZSaldoGozo.Caption := FormatFloat( '#,0.00', rGozo );
          ZSaldoPago.Caption := FormatFloat( '#,0.00', rPago );
          ZSaldoPrima.Caption := FormatFloat( '#,0.00', rPrima );

          {$ifndef SUPERVISORES}
          //US 17705: INTERMEX - Al momento de capturar vacaciones deja el dato de d�as de prima vacacional en cero si solo se modifica la fecha de inicio y ya no se mueve la fecha de regreso.
          with dmRecursos do
          begin
               SetDiasPrimasVacaciones(VA_FEC_INI, rPrima);
          end;
          {$endif}
     end;
end;

procedure TEditHisVacacion_DevEx.ImprimirForma;
begin
{$ifndef SUPERVISORES}
     ZImprimeForma.ImprimeUnaForma( enVacacion, dmRecursos.cdsHisVacacion );
{$endif}
end;

procedure TEditHisVacacion_DevEx.RBSaldosClick(Sender: TObject);
begin
     inherited;
     if ( TComponent( Sender ).Tag = 0 ) then
        RBFecha.Checked := FALSE
     else
         RBAniversario.Checked := FALSE;
     SetInfoSaldos;
end;

procedure TEditHisVacacion_DevEx.VA_NOMNUMEValidKey(Sender: TObject);
begin
     dmCatalogos.AplicaFiltroStatusPeriodo(VA_NOMNUME,True);
     inherited;
end;

procedure TEditHisVacacion_DevEx.SetControls;
var
   lHabilitar:Boolean;
begin
     if ( DataSource.State <> dsInsert )then
     begin
          {$ifdef SUPERVISORES}
          if VA_NOMNUME.Valor > 0 then
             lHabilitar := False
          else
              lHabilitar := True;
          {$else}
             lHabilitar := dmRecursos.PuedePagarVacaciones( VA_NOMNUME.Valor );
          {$endif}
     end
     else
         lHabilitar := True;

     GBNominaPago.Enabled := lHabilitar;
     //VA_GOZO.Enabled := lHabilitar;
     VA_PAGO.Enabled := lHabilitar;
     VA_MONTO.Enabled := lHabilitar;
     VA_P_PRIMA.Enabled := lHabilitar;
     VA_PRIMA.Enabled := lHabilitar;
     LblPago.Enabled := lHabilitar;
     L_VA_P_PRIMA.Enabled := lHabilitar;
     Label7.Enabled := lHabilitar;
     Label8.Enabled := lHabilitar;
     Label9.Enabled := lHabilitar;
end;
procedure TEditHisVacacion_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dmCatalogos.AplicaFiltroStatusPeriodo(VA_NOMNUME,False);
     inherited;

end;

procedure TEditHisVacacion_DevEx.BtnRegreso_DevExClick(Sender: TObject);
begin
  inherited;
{$ifdef SUPERVISORES}
     with dmSuper do
{$else}
     with dmRecursos do
{$endif}
     begin
          RecalculaVacaFechaRegreso;
     end;
end;

procedure TEditHisVacacion_DevEx.BtnRecalculo_DevExClick(Sender: TObject);
begin
  inherited;
  {$ifdef SUPERVISORES}
     with dmSuper do
{$else}
     with dmRecursos do
{$endif}
     begin
          RecalculaVacaDiasGozados;
     end;
end;

end.
