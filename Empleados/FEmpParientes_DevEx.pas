unit FEmpParientes_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, Grids,
  ZetaDBGrid, DBGrids, ExtCtrls, StdCtrls, ZBaseGridLectura_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TEmpParientes_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  EmpParientes_DevEx: TEmpParientes_DevEx;

implementation

{$R *.DFM}

uses dRecursos, ZetaCommonLists, ZetaCommonClasses;

procedure TEmpParientes_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsEmpParientes.Conectar;
          DataSource.DataSet:= cdsEmpParientes;
     end;
end;

procedure TEmpParientes_DevEx.Refresh;
begin
     dmRecursos.cdsEmpParientes.Refrescar;
end;

procedure TEmpParientes_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10123_Expediente_Parientes_empleado;
end;

procedure TEmpParientes_DevEx.Agregar;
begin
     dmRecursos.cdsEmpParientes.Agregar;
end;

procedure TEmpParientes_DevEx.Borrar;
begin
     dmRecursos.cdsEmpParientes.Borrar;
end;

procedure TEmpParientes_DevEx.Modificar;
begin
     dmRecursos.cdsEmpParientes.Modificar;
end;

procedure TEmpParientes_DevEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
//CreaColumaSumatoria('PA_RELACIO',skCount);
end;

end.
