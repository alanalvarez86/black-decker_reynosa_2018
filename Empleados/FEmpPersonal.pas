unit FEmpPersonal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, ZetaDBTextBox, StdCtrls, Db, DBCtrls,
  Mask, ExtCtrls;

type
  TEmpPersonal = class(TBaseConsulta)
    LEdoCivil: TLabel;
    LCB_LA_MAT: TLabel;
    LLugNac: TLabel;
    LNacio: TLabel;
    CB_LUG_NAC: TZetaDBTextBox;
    CB_NACION: TZetaDBTextBox;
    CB_PASAPOR: TDBCheckBox;
    CB_LA_MAT: TZetaDBTextBox;
    LDireccion: TLabel;
    LColonia: TLabel;
    LCiudad: TLabel;
    LCP: TLabel;
    LEstado: TLabel;
    LZona: TLabel;
    LTelefono: TLabel;
    CB_CALLE: TZetaDBTextBox;
    CB_COLONIA: TZetaDBTextBox;
    CB_CIUDAD: TZetaDBTextBox;
    CB_CODPOST: TZetaDBTextBox;
    CB_ZONA: TZetaDBTextBox;
    CB_TEL: TZetaDBTextBox;
    LResidencia: TLabel;
    LTransporte: TLabel;
    LHabita: TLabel;
    LViveCon: TLabel;
    Label1: TLabel;
    ZCB_FEC_RES: TZetaTextBox;
    ZCB_VIVEEN: TZetaTextBox;
    ZCB_VIVECON: TZetaTextBox;
    ZCB_MED_TRA: TZetaTextBox;
    ZCB_ESTADO: TZetaTextBox;
    ZCB_EDO_CIV: TZetaTextBox;
    CB_COD_COL: TZetaDBTextBox;
    lblClinica: TLabel;
    CB_CLINICA: TZetaDBTextBox;
    CB_DISCAPA: TDBCheckBox;
    CB_INDIGE: TDBCheckBox;
    Label2: TLabel;
    Label3: TLabel;
    CB_NUM_EXT: TZetaDBTextBox;
    CB_NUM_INT: TZetaDBTextBox;
    Label4: TLabel;
    CB_TDISCAP: TZetaTextBox;
    Label5: TLabel;
    ZCB_MUNICIP: TZetaTextBox;
    Label6: TLabel;
    CB_E_MAIL: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
  end;

var
  EmpPersonal: TEmpPersonal;

implementation

uses dRecursos, dTablas, ZetaTipoEntidad,ZetaCommonLists, ZetaCommonTools, ZetaCommonClasses,
     DGlobal,ZGlobalTress,
     ZImprimeForma,
     ZBaseEdicion_DevEx, FEditEmpPersonal_DevEx, DCliente;

{$R *.DFM}

procedure TEmpPersonal.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10121_Expediente_personales_empleado;

     //Verificar constante de AVENT
     if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
     begin
          CB_DISCAPA.Visible := False;
          CB_INDIGE.Visible := False;
     end;//if
end;

procedure TEmpPersonal.Connect;
begin
     with dmRecursos, dmTablas do
     begin
          cdsDatosEmpleado.Conectar;
          cdsEstadoCivil.Conectar;
          cdsHabitacion.Conectar;
          cdsViveCon.Conectar;
          cdsTransporte.Conectar;
          cdsEstado.Conectar;
          cdsMunicipios.Conectar;
          cdsColonia.Conectar;
          DataSource.DataSet:= cdsDatosEmpleado;
     end;
end;

procedure TEmpPersonal.Refresh;
begin
     dmRecursos.cdsDatosEmpleado.Refrescar;
end;

function TEmpPersonal.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoAlta( sMensaje );
end;

procedure TEmpPersonal.Agregar;
begin
     dmRecursos.cdsDatosEmpleado.Agregar;    // Se mandar� llamar la Alta
end;

function TEmpPersonal.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoBaja( sMensaje );
end;

procedure TEmpPersonal.Borrar;
begin
     dmRecursos.cdsDatosEmpleado.Borrar;    // Se mandar� llamar la Baja
end;

procedure TEmpPersonal.Modificar;
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpPersonal_DevEx, TEditEmpPersonal_DevEx );
end;

procedure TEmpPersonal.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dmRecursos.cdsDatosEmpleado do
     begin
          ZCB_FEC_RES.Caption := TiempoDias( Date - FieldbyName('CB_FEC_RES').AsDateTime + 1 , etDias );
          ZCB_EDO_CIV.Caption := dmTablas.cdsEstadoCivil.GetDescripcion( FieldByName( 'CB_EDO_CIV' ).AsString );
          ZCB_VIVECON.Caption := dmTablas.cdsViveCon.GetDescripcion( FieldByName( 'CB_VIVECON' ).AsString );
          ZCB_VIVEEN.Caption  := dmTablas.cdsHabitacion.GetDescripcion( FieldByName( 'CB_VIVEEN' ).AsString );
          ZCB_MED_TRA.Caption := dmTablas.cdsTransporte.GetDescripcion( FieldByName( 'CB_MED_TRA' ).AsString );
          ZCB_ESTADO.Caption  := dmTablas.cdsEstado.GetDescripcion( FieldByName( 'CB_ESTADO' ).AsString );
          ZCB_MUNICIP.Caption  := dmTablas.cdsMunicipios.GetDescripcion( FieldByName( 'CB_MUNICIP' ).AsString );
          if CB_DISCAPA.Checked then
          begin
               CB_TDISCAP.Caption := ObtieneElemento(lfDiscapacidadSTPS , FieldByName('CB_TDISCAP').AsInteger);
          end
          else
          begin
               CB_TDISCAP.Caption := VACIO;

          end;
     end;
end;

procedure TEmpPersonal.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

end.
