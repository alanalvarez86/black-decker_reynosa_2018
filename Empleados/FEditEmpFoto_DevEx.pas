unit FEditEmpFoto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, ZetaDBTextBox, Db, ExtCtrls, ExtDlgs,
  Mask, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, imageenview, ieview, dbimageen,
  ieopensavedlg, hyieutils;

type
  TEditEmpFoto_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    IM_TIPO: TDBComboBox;
    IM_OBSERVA: TDBEdit;
    LeerDisco: TdxBarButton;
    GrabarDisco: TdxBarButton;
    BtnLeerTwain: TdxBarButton;
    BtnSeleccionarTwain: TdxBarButton;
    dxBarButton1: TdxBarButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    dxBarSubItem2: TdxBarSubItem;
    FOTO: TImageEnView;
    OpenImageEnDialog: TOpenImageEnDialog;
    SaveImageEnDialog: TSaveImageEnDialog;
    procedure LeerDiscoClick(Sender: TObject);
    procedure GrabarDiscoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnLeerTwainClick(Sender: TObject);
    procedure BtnSeleccionarTwainClick(Sender: TObject);
    procedure IM_TIPOKeyPress(Sender: TObject; var Key: Char);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure dxBarButton_CortarBtnClick(Sender: TObject);
    procedure dxBarButton_CopiarBtnClick(Sender: TObject);
    procedure dxBarButton_PegarBtnClick(Sender: TObject);
    procedure FOTOEnter(Sender: TObject);
    procedure FOTOExit(Sender: TObject);
    procedure FOTOClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    LimpiarImagen: Boolean;
    Borde: Boolean;
    procedure GrabaImagen;
    procedure CrearBorde;
    procedure EliminarBorde;

  protected
    procedure Connect; override;
  public
  end;

var
  EditEmpFoto_DevEx: TEditEmpFoto_DevEx;

implementation

uses dRecursos, ZetaCommonClasses, ZAccesosTress, {JPEG,} ZetaDialogo,
     ZetaCommonLists, DGlobal, ZGlobalTress, ZetaCommonTools, iexAcquire, hyiedefs, imageenio,
     FToolsImageEn, Clipbrd;

{$R *.DFM}

procedure TEditEmpFoto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_EMP_CURR_FOTO;
     FirstControl := IM_TIPO;
     HelpContext:= H10122_Expediente_Foto_empleado;
     TipoValorActivo1 := stEmpleado;
     LimpiarImagen := FALSE;
end;

procedure TEditEmpFoto_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Borde := FALSE;
end;

procedure TEditEmpFoto_DevEx.FOTOClick(Sender: TObject);
begin
    CrearBorde;
end;

procedure TEditEmpFoto_DevEx.FOTOEnter(Sender: TObject);
begin
    CrearBorde;
end;

procedure TEditEmpFoto_DevEx.FOTOExit(Sender: TObject);
begin
    EliminarBorde;
end;

procedure TEditEmpFoto_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsEmpFoto.Conectar;
          DataSource.DataSet:= cdsEmpFoto;
     end;
end;

procedure TEditEmpFoto_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;

     Borde := FALSE;
     if (dmRecursos.cdsEmpFoto.State = dsInsert) and (LimpiarImagen) then
     begin
        FOTO.Clear;
        LimpiarImagen := FALSE;
     end
     else if ( Field = nil ) and ( dmRecursos.cdsEmpFoto.State = dsBrowse ) then     // Navegando
     begin
        FToolsImageEn.AsignaBlobAImagen( FOTO, dmRecursos.cdsEmpFoto, 'IM_BLOB' );
        LimpiarImagen := TRUE;
     end;
end;

procedure TEditEmpFoto_DevEx.LeerDiscoClick(Sender: TObject);
begin
     inherited;
     if OpenImageEnDialog.Execute then
     begin
          // FOTO.Clear;
          FOTO.IO.LoadFromFile( OpenImageEnDialog.FileName );
          GrabaImagen;
     end;
end;

procedure TEditEmpFoto_DevEx.GrabarDiscoClick(Sender: TObject);
const
     aImagenExt: array[ 1..7 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'JPG','JPG','BMP', 'GIF', 'TIF', 'PCX', 'PNG' );
begin
     inherited;
     EliminarBorde;
     with SaveImageEnDialog do
     begin
          FilterIndex := 0;
          FileName := VACIO;
          if Execute then
          begin
               if strVacio( DefaultExt ) then
                  DefaultExt := aImagenExt[ FilterIndex ];
               FOTO.IO.Params.JPEG_Quality := 75;
               FOTO.IO.SaveToFile( FileName );
          end;
     end;
end;

procedure TEditEmpFoto_DevEx.BtnSeleccionarTwainClick(Sender: TObject);
begin
     inherited;
     try
        FOTO.IO.SelectAcquireSource([ieaTwain, ieaWIA, ieaDCIM]);
     except
           ZError( Self.Caption, 'No se pudo seleccionar dispositivo' ,0);
     end;
end;

procedure TEditEmpFoto_DevEx.BtnLeerTwainClick(Sender: TObject);
begin
     if ( FOTO.IO.SelectedAcquireSource.Api <> ieaNone ) then
     begin
          try
             FOTO.Clear;
             FOTO.IO.Acquire;

             GrabaImagen;
          except
                ZError( self.Caption, 'No se pudo obtener la imagen - Error al leer del dispositivo', 0 );
          end;
     end
     else
     begin
          ZError( self.Caption, 'No se ha seleccionado dispositivo', 0 );
     end;
end;

procedure TEditEmpFoto_DevEx.GrabaImagen;
var
   sMsgError: String;
begin
     if ( not Global.GetGlobalBooleano( K_GLOBAL_CONSERVAR_TAM_FOTOS ) ) then
     begin
          if ( not FToolsImageEn.ResizeImagen( FOTO, sMsgError ) ) then
             ZError( self.Caption, sMsgError, 0);
     end;
     FToolsImageEn.AsignaImagenABlob( FOTO, dmRecursos.cdsEmpFoto, 'IM_BLOB' );
end;

procedure TEditEmpFoto_DevEx.dxBarButton_CortarBtnClick(Sender: TObject);
begin
     EliminarBorde;
     if Assigned( ActiveControl ) and ( ActiveControl.Enabled ) and ( ActiveControl is TImageEnView ) then
     begin
          with TImageEnView( ActiveControl ) do
          begin
               Proc.CopyToClipboard;
               Clear;
               Update;
          end;
          GrabaImagen;
     end
     else
         inherited;
end;

procedure TEditEmpFoto_DevEx.dxBarButton_CopiarBtnClick(Sender: TObject);
begin
     EliminarBorde;
     if Assigned( ActiveControl ) and ( ActiveControl.Enabled ) and ( ActiveControl is TImageEnView ) then
     begin
          TImageEnView( ActiveControl ).Proc.CopyToClipboard;
     end
     else
         inherited;
end;

procedure TEditEmpFoto_DevEx.dxBarButton_PegarBtnClick(Sender: TObject);
begin
     EliminarBorde;
     if Assigned( ActiveControl ) and ( ActiveControl.Enabled ) and ( ActiveControl is TImageEnView ) then
     begin
          if Clipboard.HasFormat ( CF_PICTURE ) then
          begin
               with TImageEnView( ActiveControl ) do
               begin
                    Clear;
                    Proc.PasteFromClipboard;
                    Update;
               end;
               GrabaImagen;
          end;
     end
     else
         inherited;
end;

procedure TEditEmpFoto_DevEx.IM_TIPOKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key = ZetaCommonClasses.UnaCOMILLA ) then
        Key := Chr( 0 );
     inherited KeyPress( Key );
end;

procedure TEditEmpFoto_DevEx.CrearBorde;
var
  OrigWidth, origHeight: integer;
  oLeft: integer;
begin
    if (not Borde) and (dmRecursos.cdsEmpFoto.FieldByName('IM_BLOB').AsString <> VACIO) then
    begin
      Borde := TRUE;
      OrigWidth := FOTO.IEBitmap.Width;
      OrigHeight := FOTO.IEBitmap.Height;
      oLeft := 4;

      // Color para el borde.
      FOTO.BackGround := TColor($4D3B4B);

      FOTO.Proc.ImageResize(OrigWidth + oLeft * 2, OrigHeight + oLeft * 2, iehCenter, ievCenter);

      // Restaurar background color.
      FOTO.Background := clBtnFace;
    end;
end;

procedure TEditEmpFoto_DevEx.EliminarBorde;
var
  OrigWidth, origHeight: integer;
  oLeft: integer;
begin
    if Borde then
    begin
      Borde := FALSE;
      OrigWidth := FOTO.IEBitmap.Width;
      OrigHeight := FOTO.IEBitmap.Height;
      oLeft := 4;

      FOTO.Proc.ImageResize(OrigWidth - oLeft * 2, OrigHeight - oLeft * 2, iehCenter, ievCenter);

      // Restaurar background color.
      FOTO.Background := clBtnFace;
    end;
end;

end.
