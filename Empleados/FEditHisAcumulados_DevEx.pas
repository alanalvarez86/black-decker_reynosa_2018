unit FEditHisAcumulados_DevEx;

interface

{  Folio      :
   Prop�sito  :
   Creaci�n   :
   Responsable:
   Calculados : No
   Lista Calculados[] }


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, ExtCtrls, StdCtrls, Db,
  Buttons, DBCtrls, Mask,
  ZetaDBTextBox, ZetaNumero, ZetaSmartLists,  
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TEditHisAcumulados_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label14: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    Label10: TLabel;
    Label9: TLabel;
    AC_ANUAL: TZetaDBTextBox;
    CO_NUMERO: TZetaDBKeyLookup_DevEx;
    AC_MES_01: TZetaDBNumero;
    AC_MES_02: TZetaDBNumero;
    AC_MES_03: TZetaDBNumero;
    AC_MES_04: TZetaDBNumero;
    AC_MES_05: TZetaDBNumero;
    AC_MES_06: TZetaDBNumero;
    AC_MES_07: TZetaDBNumero;
    AC_MES_08: TZetaDBNumero;
    AC_MES_09: TZetaDBNumero;
    AC_MES_10: TZetaDBNumero;
    AC_MES_11: TZetaDBNumero;
    AC_MES_12: TZetaDBNumero;
    AC_MES_13: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditHisAcumulados_DevEx: TEditHisAcumulados_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZAccesosTress,
     dRecursos, dCatalogos;

{$R *.DFM}

procedure TEditHisAcumulados_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsHisAcumulados.Conectar;
          Datasource.DataSet := cdsHisAcumulados;
     end;
end;

procedure TEditHisAcumulados_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stSistema;
     HelpContext:= H10143_Acumulados;
     IndexDerechos := ZAccesosTress.D_EMP_NOM_ACUMULA;
     FirstControl := CO_NUMERO;
     CO_NUMERO.LookupDataset := dmCatalogos.cdsConceptos;
end;
end.







