unit FCandidatosGridShow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridShow, Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls;

type
  TCandidatosGridShow = class(TBaseGridShow)
    procedure OKClick(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CandidatosGridShow: TCandidatosGridShow;

implementation

uses ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

procedure TCandidatosGridShow.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_Lista_de_Candidatos;
end;

procedure TCandidatosGridShow.OKClick(Sender: TObject);
const
     K_YA_CONTRATADO = 'No Se Puede Seleccionar Este Candidato' + CR_LF +
                       'Ya Fu� Contratado Con El N�mero: %d';
var
   iEmpleado: Integer;
begin
     inherited;
     iEmpleado := Dataset.FieldByName( 'CB_CODIGO' ).AsInteger;
     if ( iEmpleado > 0 ) then
        ZetaDialogo.ZError( self.Caption, Format( K_YA_CONTRATADO, [ iEmpleado ] ), 0 )
     else
        ModalResult := mrOk;   // Cierra la forma
end;

procedure TCandidatosGridShow.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
          DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Brush.Color := clNavy;
                    Font.Color := clWhite;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    if ( DataSet.FieldByName( 'CB_CODIGO' ).AsInteger > 0 ) then
                       Font.Color := clRed
                    else
                        Font.Color := ZetaDBGrid.Font.Color;
               end;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;

end.
