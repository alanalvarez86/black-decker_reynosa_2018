inherited EditReservaAula_DevEx: TEditReservaAula_DevEx
  Left = 308
  Top = 221
  Caption = 'Reservaciones de aula'
  ClientHeight = 485
  ClientWidth = 451
  PixelsPerInch = 96
  TextHeight = 13
  object lblAula: TLabel [0]
    Left = 55
    Top = 65
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'Aula:'
  end
  object lblFecIni: TLabel [1]
    Left = 17
    Top = 114
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha inicial:'
  end
  object lblFecFin: TLabel [2]
    Left = 24
    Top = 138
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha final:'
  end
  object lblHoraIni: TLabel [3]
    Left = 24
    Top = 161
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Hora inicial:'
  end
  object lblHoraFin: TLabel [4]
    Left = 31
    Top = 185
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Hora final:'
  end
  object lblResumen: TLabel [5]
    Left = 31
    Top = 209
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Resumen:'
  end
  object lblReferencia: TLabel [6]
    Left = 24
    Top = 41
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Referencia:'
  end
  object Label5: TLabel [7]
    Left = 38
    Top = 89
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Maestro:'
  end
  inherited PanelBotones: TPanel
    Top = 449
    Width = 451
    TabOrder = 12
    DesignSize = (
      451
      36)
    inherited OK_DevEx: TcxButton
      Left = 286
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 365
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 451
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 125
      inherited textoValorActivo2: TLabel
        Width = 119
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 16
  end
  object AL_CODIGO: TZetaDBKeyLookup_DevEx [11]
    Left = 82
    Top = 61
    Width = 350
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 2
    TabStop = True
    WidthLlave = 70
    DataField = 'AL_CODIGO'
    DataSource = DataSource
  end
  object RV_FEC_INI: TZetaDBFecha [12]
    Left = 82
    Top = 109
    Width = 105
    Height = 22
    Cursor = crArrow
    TabOrder = 4
    Text = '05/may/05'
    Valor = 38477.000000000000000000
    DataField = 'RV_FEC_INI'
    DataSource = DataSource
  end
  object RV_FEC_FIN: TZetaDBFecha [13]
    Left = 82
    Top = 133
    Width = 105
    Height = 22
    Cursor = crArrow
    TabOrder = 5
    Text = '05/may/05'
    Valor = 38477.000000000000000000
    DataField = 'RV_FEC_FIN'
    DataSource = DataSource
  end
  object RV_HOR_INI: TZetaDBHora [14]
    Left = 82
    Top = 157
    Width = 40
    Height = 21
    EditMask = '99:99;0'
    TabOrder = 6
    Text = '    '
    Tope = 24
    Valor = '    '
    DataField = 'RV_HOR_INI'
    DataSource = DataSource
  end
  object RV_HOR_FIN: TZetaDBHora [15]
    Left = 82
    Top = 181
    Width = 40
    Height = 21
    EditMask = '99:99;0'
    TabOrder = 7
    Text = '    '
    Tope = 24
    Valor = '    '
    DataField = 'RV_HOR_FIN'
    DataSource = DataSource
  end
  object RV_RESUMEN: TDBEdit [16]
    Left = 82
    Top = 205
    Width = 350
    Height = 21
    DataField = 'RV_RESUMEN'
    DataSource = DataSource
    TabOrder = 8
  end
  object gbDetalle: TGroupBox [17]
    Left = 18
    Top = 230
    Width = 415
    Height = 118
    Caption = ' Detalle '
    TabOrder = 9
    object RV_DETALLE: TcxDBMemo
      Left = 2
      Top = 15
      Align = alClient
      DataBinding.DataField = 'RV_DETALLE'
      DataBinding.DataSource = DataSource
      TabOrder = 0
      Height = 101
      Width = 411
    end
  end
  object gbReserva: TGroupBox [18]
    Left = 18
    Top = 352
    Width = 415
    Height = 91
    Caption = ' Detalles de la Reservaci'#243'n: '
    TabOrder = 10
    object Label2: TLabel
      Left = 40
      Top = 40
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object Label3: TLabel
      Left = 47
      Top = 61
      Width = 26
      Height = 13
      Alignment = taRightJustify
      Caption = 'Hora:'
    end
    object Label4: TLabel
      Left = 269
      Top = 61
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object RV_FEC_RES: TZetaDBTextBox
      Left = 76
      Top = 38
      Width = 80
      Height = 17
      AutoSize = False
      Caption = 'RV_FEC_RES'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'RV_FEC_RES'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object RV_HOR_RES: TZetaDBTextBox
      Left = 76
      Top = 59
      Width = 51
      Height = 17
      AutoSize = False
      Caption = 'RV_HOR_RES'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'RV_HOR_RES'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object RV_TIPO: TZetaDBTextBox
      Left = 296
      Top = 59
      Width = 80
      Height = 17
      AutoSize = False
      Caption = 'RV_TIPO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'RV_TIPO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lblReservo: TLabel
      Left = 30
      Top = 19
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Reserv'#243':'
    end
    object US_DESCRIP: TZetaDBTextBox
      Left = 76
      Top = 17
      Width = 300
      Height = 17
      AutoSize = False
      Caption = 'US_DESCRIP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_DESCRIP'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object MA_CODIGO: TZetaDBKeyLookup_DevEx [19]
    Left = 82
    Top = 85
    Width = 350
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 3
    TabStop = True
    WidthLlave = 70
    DataField = 'MA_CODIGO'
    DataSource = DataSource
  end
  object RV_ORDEN: TZetaDBEdit [20]
    Left = 82
    Top = 37
    Width = 70
    Height = 21
    TabOrder = 1
    ConfirmEdit = True
    DataField = 'RV_ORDEN'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 412
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
