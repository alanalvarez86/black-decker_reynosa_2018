inherited HisPrestamos_DevEx: THisPrestamos_DevEx
  Left = 246
  Top = 226
  Caption = 'Historial de Pr'#233'stamos'
  ClientWidth = 798
  ExplicitWidth = 798
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 798
    ExplicitWidth = 796
    inherited Slider: TSplitter
      Left = 323
      ExplicitLeft = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      ExplicitWidth = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 472
      ExplicitLeft = 326
      ExplicitWidth = 470
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 466
        ExplicitLeft = 660
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 798
    Height = 38
    Align = alTop
    TabOrder = 2
    Visible = False
    ExplicitWidth = 796
    object lblTipo: TLabel
      Left = 7
      Top = 13
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object PR_TIPO: TZetaKeyCombo
      Left = 36
      Top = 9
      Width = 221
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      OnChange = PR_TIPOChange
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object Panel2: TPanel
      Left = 658
      Top = 1
      Width = 139
      Height = 36
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 656
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 57
    Width = 798
    Height = 167
    ExplicitTop = 57
    ExplicitWidth = 796
    ExplicitHeight = 167
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object PR_FECHA: TcxGridDBColumn
        Caption = 'Inicio'
        DataBinding.FieldName = 'PR_FECHA'
      end
      object PR_TIPO_GRID: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'PR_TIPO'
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
      end
      object PR_REFEREN: TcxGridDBColumn
        Caption = 'Referencia'
        DataBinding.FieldName = 'PR_REFEREN'
      end
      object PR_MONTO: TcxGridDBColumn
        Caption = 'Prestado'
        DataBinding.FieldName = 'PR_MONTO'
      end
      object PR_SALDO: TcxGridDBColumn
        Caption = 'Saldo'
        DataBinding.FieldName = 'PR_SALDO'
      end
      object PR_STATUS_GRID: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'PR_STATUS'
      end
      object PR_SUB_CTA: TcxGridDBColumn
        Caption = 'Subcuenta'
        DataBinding.FieldName = 'PR_SUB_CTA'
      end
      object PR_OBSERVA: TcxGridDBColumn
        Caption = 'Observaciones'
        DataBinding.FieldName = 'PR_OBSERVA'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 16
    Top = 128
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
