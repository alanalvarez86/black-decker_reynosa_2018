unit FKardexRenova_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, 
  ZetaFecha, ExtCtrls, StdCtrls, DBCtrls, ZetaDBTextBox, Mask, ComCtrls,
  Buttons, FKardexBase_DevEx, ZetaSmartLists, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls, cxContainer, cxEdit,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxTextEdit, cxMemo, cxDBEdit, cxPC, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TKardexRenova_DevEx = class(TKardexBase_DevEx)
    CB_FEC_COV: TZetaDBFecha;
    CB_CONTRAT: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    //procedure CancelarClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    procedure CalculaVencimiento;

  protected
    procedure Connect;override;
  public
    { Public declarations }
  end;

var
  KardexRenova_DevEx: TKardexRenova_DevEx;

implementation

uses ZAccesosTress,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     dCatalogos, dSistema, dRecursos;

{$R *.DFM}

procedure TKardexRenova_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10158_Cambio_contrato;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX;
     CB_CONTRAT.LookUpDataSet:= dmCatalogos.cdsContratos;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TKardexRenova_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     dmCatalogos.cdsContratos.Conectar;
     with dmRecursos do
     begin
          cdsEditHisKardex.Conectar;
          DataSource.DataSet := cdsEditHisKardex;
     end;
     CalculaVencimiento;
end;


{procedure TKardexRenova_DevEx.CancelarClick(Sender: TObject);
begin
     inherited;
     CalculaVencimiento;
end;}

procedure TKardexRenova_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field <> nil ) then
        with Field do
             if ( FieldName = 'CB_FECHA' ) or ( FieldName = 'CB_CONTRAT' ) then
                CalculaVencimiento;
end;

procedure TKardexRenova_DevEx.CalculaVencimiento;
begin
     CB_FEC_COV.Enabled := ( dmRecursos.cdsEditHisKardex.FieldByName( 'CB_FEC_COV' ).AsDateTime <> NullDateTime );
end;

procedure TKardexRenova_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
     CalculaVencimiento;
end;


procedure TKardexRenova_DevEx.SetEditarSoloActivos;
begin
     CB_CONTRAT.EditarSoloActivos := TRUE;
end;

end.
