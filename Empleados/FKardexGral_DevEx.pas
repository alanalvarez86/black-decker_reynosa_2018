unit FKardexGral_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaNumero, Db,
  ZetaFecha, ExtCtrls, StdCtrls, DBCtrls, ZetaDBTextBox,
  Mask, ComCtrls, Buttons, FKardexBase_DevEx, ZetaSmartLists, 
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls, cxContainer, cxEdit,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxTextEdit, cxMemo, cxDBEdit, cxPC, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TKardexGeneral_DevEx = class(TKardexBase_DevEx)
    CB_MONTO: TZetaDBNumero;
    CB_TIPO: TZetaDBKeyLookup_DevEx;
    MontoLbl: TLabel;
    CB_TIPOlbl: TLabel;
    procedure FormCreate(Sender: TObject);
  protected
    procedure Connect;override;
  private
  public
    { Public declarations }
  end;

var
  KardexGeneral_DevEx: TKardexGeneral_DevEx;

implementation

uses ZetaCommonLists,
     ZetaCommonClasses,
     ZAccesosTress,
     dTablas, dSistema, dRecursos;

{$R *.DFM}

procedure TKardexGeneral_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H31313_Kardex_General;
     TipoValorActivo1 := stEmpleado;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX;
     CB_TIPO.LookupDataset := dmTablas.cdsMovKardex;
end;

procedure TKardexGeneral_DevEx.Connect;
begin
     dmTablas.cdsMovKardex.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsEditHisKardex.Conectar;
          DataSource.DataSet := cdsEditHisKardex;
     end;
end;

end.






