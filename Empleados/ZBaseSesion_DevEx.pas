unit ZBaseSesion_DevEx;

interface

uses
  Windows, Messages, SysUtils,
  {$ifndef VER130}Variants,{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZetaDBTextBox, StdCtrls, DB, Grids, DBGrids,
  ZetaDBGrid, Buttons, ExtCtrls, DBCtrls, ZetaSmartLists,
  ZetaClientDataSet, ZBaseGridEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TBaseSesion_DevEx = class(TBaseGridEdicion_DevEx)
    dsSesion: TDataSource;
    Panel1: TPanel;
    Label1: TLabel;
    CU_CODIGO: TZetaDBTextBox;
    CU_NOMBRE: TZetaDBTextBox;
    Label2: TLabel;
    MA_CODIGO: TZetaDBTextBox;
    MA_NOMBRE: TZetaDBTextBox;
    lblFolio: TLabel;
    SE_FOLIO: TZetaDBTextBox;
    procedure ZetaDBGridDblClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure Modificar;override;
  public
    { Public declarations }
  end;

var
  BaseSesion_DevEx: TBaseSesion_DevEx;

implementation

uses DRecursos;

{$R *.dfm}

procedure TBaseSesion_DevEx.Connect;
begin
     with dmRecursos do
     begin
          dsSesion.DataSet := cdsSesion;
     end;
end;


procedure TBaseSesion_DevEx.ZetaDBGridDblClick(Sender: TObject);
begin
     inherited;
     Modificar;
end;

procedure TBaseSesion_DevEx.Modificar;
begin
     if PuedeModificar then
        ClientDataset.Modificar;
end;


end.
