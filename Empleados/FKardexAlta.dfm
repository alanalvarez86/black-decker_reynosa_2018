inherited KardexAlta: TKardexAlta
  Left = 599
  Top = 212
  Caption = 'Alta'
  ClientHeight = 438
  ClientWidth = 494
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 402
    Width = 494
    inherited OK: TBitBtn
      Left = 333
    end
    inherited Cancelar: TBitBtn
      Left = 410
    end
  end
  inherited PanelSuperior: TPanel
    Width = 494
  end
  inherited PageControl: TPageControl
    Width = 494
    Height = 321
    ActivePage = TabContratacion
    object TabContratacion: TTabSheet [0]
      Caption = 'Contrataci'#243'n'
      object Label34: TLabel
        Left = 9
        Top = 8
        Width = 89
        Height = 13
        Caption = 'Antig'#252'edad desde:'
      end
      object ZAntiguedad: TZetaTextBox
        Left = 240
        Top = 6
        Width = 177
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object CB_PUESTOlbl: TLabel
        Left = 62
        Top = 114
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object CB_CLASIFIlbl: TLabel
        Left = 36
        Top = 160
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
      end
      object CB_HORARIOlbl: TLabel
        Left = 67
        Top = 183
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object Label36: TLabel
        Left = 14
        Top = 206
        Width = 84
        Height = 13
        Caption = 'Registro Patronal:'
      end
      object bbMostrarCalendario: TSpeedButton
        Left = 408
        Top = 178
        Width = 23
        Height = 22
        Hint = 'Mostrar Calendario del Turno'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
          003337777777777777F330FFFFFFFFFFF03337F3333FFF3337F330FFFF000FFF
          F03337F33377733337F330FFFFF0FFFFF03337F33337F33337F330FFFF00FFFF
          F03337F33377F33337F330FFFFF0FFFFF03337F33337333337F330FFFFFFFFFF
          F03337FFF3F3F3F3F7F33000F0F0F0F0F0333777F7F7F7F7F7F330F0F000F070
          F03337F7F777F777F7F330F0F0F0F070F03337F7F7373777F7F330F0FF0FF0F0
          F03337F733733737F7F330FFFFFFFF00003337F33333337777F330FFFFFFFF0F
          F03337FFFFFFFF7F373330999999990F033337777777777F733330FFFFFFFF00
          333337FFFFFFFF77333330000000000333333777777777733333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = bbMostrarCalendarioClick
      end
      object label99: TLabel
        Left = 69
        Top = 137
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'Plaza:'
      end
      object Label11: TLabel
        Left = 20
        Top = 230
        Width = 78
        Height = 13
        Caption = 'Tipo de N'#243'mina:'
      end
      object CB_FEC_ANT: TZetaDBFecha
        Left = 106
        Top = 3
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '16/dic/97'
        Valor = 35780.000000000000000000
        DataField = 'CB_FEC_ANT'
        DataSource = DataSource
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 24
        Width = 409
        Height = 82
        Caption = 'Contrato'
        TabOrder = 1
        object CB_FEC_CONlbl: TLabel
          Left = 62
          Top = 13
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicio:'
        end
        object CB_CONTRATlbl: TLabel
          Left = 63
          Top = 36
          Width = 27
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo: '
        end
        object LVencimiento: TLabel
          Left = 29
          Top = 59
          Width = 61
          Height = 13
          Caption = 'Vencimiento:'
        end
        object CB_FEC_CON: TZetaDBFecha
          Left = 98
          Top = 8
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '16/dic/97'
          Valor = 35780.000000000000000000
          DataField = 'CB_FEC_CON'
          DataSource = DataSource
        end
        object CB_CONTRAT: TZetaDBKeyLookup
          Left = 98
          Top = 32
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsContratos
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
          OnExit = CB_CONTRATExit
          DataField = 'CB_CONTRAT'
          DataSource = DataSource
        end
        object CB_FEC_COV: TZetaDBFecha
          Left = 98
          Top = 55
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 2
          Text = '13/may/08'
          Valor = 39581.000000000000000000
          DataField = 'CB_FEC_COV'
          DataSource = DataSource
        end
      end
      object CB_PATRON: TZetaDBKeyLookup
        Left = 106
        Top = 202
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsRPatron
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_PATRON'
        DataSource = DataSource
      end
      object CB_TURNO: TZetaDBKeyLookup
        Left = 106
        Top = 179
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTurnos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TURNO'
        DataSource = DataSource
      end
      object CB_CLASIFI: TZetaDBKeyLookup
        Left = 106
        Top = 156
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        OnExit = CB_CLASIFIExit
        DataField = 'CB_CLASIFI'
        DataSource = DataSource
      end
      object CB_PUESTO: TZetaDBKeyLookup
        Left = 106
        Top = 110
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        OnExit = CB_PUESTOExit
        DataField = 'CB_PUESTO'
        DataSource = DataSource
      end
      object Panel1: TPanel
        Left = 432
        Top = 99
        Width = 50
        Height = 41
        BevelOuter = bvNone
        TabOrder = 8
        TabStop = True
        OnEnter = Panel1Enter
      end
      object CB_PLAZA: TZetaDBKeyLookup
        Left = 106
        Top = 133
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_PLAZA'
        DataSource = DataSource
      end
      object CB_NOMINA: TZetaDBKeyCombo
        Left = 106
        Top = 227
        Width = 190
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 7
        ListaFija = lfTipoPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
        DataField = 'CB_NOMINA'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
    object TabArea: TTabSheet [1]
      Caption = 'Area'
      object CB_NIVEL1lbl: TLabel
        Left = 82
        Top = 4
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object CB_NIVEL2lbl: TLabel
        Left = 82
        Top = 28
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object CB_NIVEL3lbl: TLabel
        Left = 82
        Top = 52
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object CB_NIVEL4lbl: TLabel
        Left = 82
        Top = 76
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object CB_NIVEL5lbl: TLabel
        Left = 82
        Top = 100
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object CB_NIVEL6lbl: TLabel
        Left = 82
        Top = 124
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object CB_NIVEL7lbl: TLabel
        Left = 82
        Top = 148
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object CB_NIVEL8lbl: TLabel
        Left = 82
        Top = 172
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object CB_NIVEL9lbl: TLabel
        Left = 82
        Top = 196
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object CB_NIVEL10lbl: TLabel
        Left = 76
        Top = 220
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object CB_NIVEL11lbl: TLabel
        Left = 76
        Top = 244
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object CB_NIVEL12lbl: TLabel
        Left = 76
        Top = 268
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
      object CB_NIVEL9: TZetaDBKeyLookup
        Left = 128
        Top = 192
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL9ValidLookup
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
      end
      object CB_NIVEL8: TZetaDBKeyLookup
        Left = 128
        Top = 168
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL8ValidLookup
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
      end
      object CB_NIVEL7: TZetaDBKeyLookup
        Left = 128
        Top = 144
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL7ValidLookup
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
      end
      object CB_NIVEL6: TZetaDBKeyLookup
        Left = 128
        Top = 120
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL6ValidLookup
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
      end
      object CB_NIVEL5: TZetaDBKeyLookup
        Left = 128
        Top = 96
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL5ValidLookup
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
      end
      object CB_NIVEL4: TZetaDBKeyLookup
        Left = 128
        Top = 72
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL4ValidLookup
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
      end
      object CB_NIVEL3: TZetaDBKeyLookup
        Left = 128
        Top = 48
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL3ValidLookup
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
      end
      object CB_NIVEL2: TZetaDBKeyLookup
        Left = 128
        Top = 24
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL2ValidLookup
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
      end
      object CB_NIVEL1: TZetaDBKeyLookup
        Left = 128
        Top = 0
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
      end
      object Panel2: TPanel
        Left = 432
        Top = 176
        Width = 50
        Height = 41
        BevelOuter = bvNone
        TabOrder = 13
        TabStop = True
        OnEnter = Panel2Enter
      end
      object Panel2_plazas: TPanel
        Left = 440
        Top = 117
        Width = 50
        Height = 41
        BevelOuter = bvNone
        TabOrder = 12
        TabStop = True
        OnEnter = Panel2_plazasEnter
      end
      object CB_NIVEL10: TZetaDBKeyLookup
        Left = 128
        Top = 216
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL10ValidLookup
        DataField = 'CB_NIVEL10'
      end
      object CB_NIVEL11: TZetaDBKeyLookup
        Left = 128
        Top = 240
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL11ValidLookup
        DataField = 'CB_NIVEL11'
      end
      object CB_NIVEL12: TZetaDBKeyLookup
        Left = 128
        Top = 264
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL12ValidLookup
        DataField = 'CB_NIVEL12'
      end
    end
    inherited General: TTabSheet
      inherited Label12: TLabel
        Left = 258
      end
      inherited Label13: TLabel
        Left = 274
        Top = 49
      end
      inherited Label15: TLabel
        Left = 249
        Top = 93
      end
      inherited Label16: TLabel
        Left = 279
        Top = 114
      end
      inherited CB_OTRAS_P: TZetaDBTextBox
        Left = 348
        Top = 91
      end
      inherited CB_SAL_TOT: TZetaDBTextBox
        Left = 348
        Top = 112
      end
      inherited CB_RANGO_SLbl: TLabel
        Left = 269
        Top = 72
      end
      inherited Label5: TLabel
        Left = 263
        Top = 135
      end
      inherited CB_SAL_SEM: TZetaTextBox
        Left = 348
        Top = 133
      end
      inherited Label17: TLabel
        Enabled = True
      end
      inherited CB_FECHA_2: TZetaDBFecha
        Left = 348
        Top = 44
        TabOrder = 5
      end
      object Panel3: TPanel [29]
        Left = 440
        Top = 152
        Width = 50
        Height = 41
        BevelOuter = bvNone
        TabOrder = 7
        TabStop = True
        OnEnter = Panel3Enter
      end
      inherited CB_ZONA_GE: TDBComboBox
        Left = 348
        TabOrder = 4
      end
      inherited CB_RANGO_S: TZetaDBNumero
        Left = 348
        TabOrder = 6
      end
      inherited CB_TABLASS: TZetaDBKeyLookup
        Enabled = True
        LookupDataset = nil
        TabOrder = 3
      end
    end
    inherited Otras: TTabSheet
      inherited ZetaSmartListsButton1: TZetaSmartListsButton
        Left = 220
      end
      inherited ZetaSmartListsButton2: TZetaSmartListsButton
        Left = 220
      end
      inherited Label22: TLabel
        Left = 254
      end
      inherited ZetaSmartListsButton3: TZetaSmartListsButton
        Left = 459
      end
      inherited ZetaSmartListsButton4: TZetaSmartListsButton
        Left = 459
      end
      inherited SBMas: TSpeedButton
        Left = 459
        Top = 24
      end
      inherited LBDisponibles: TZetaSmartListBox
        Top = 24
        Width = 200
        Height = 175
      end
      inherited LBSeleccion: TZetaSmartListBox
        Left = 254
        Top = 24
        Width = 200
        Height = 175
      end
      object Panel4: TPanel
        Left = 456
        Top = 176
        Width = 26
        Height = 33
        BevelOuter = bvNone
        TabOrder = 2
        TabStop = True
        OnEnter = Panel4Enter
      end
      object Panel5: TPanel
        Left = 456
        Top = 142
        Width = 26
        Height = 33
        BevelOuter = bvNone
        TabOrder = 3
        TabStop = True
        OnEnter = Panel5Enter
      end
    end
    inherited Notas: TTabSheet
      inherited CB_NOTA: TDBMemo
        Width = 486
        Height = 293
      end
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 494
    inherited ValorActivo2: TPanel
      Width = 168
      inherited textoValorActivo2: TLabel
        Width = 162
      end
    end
  end
  inherited PanelFecha: TPanel
    Width = 494
  end
  inherited DataSource: TDataSource
    Left = 380
  end
  inherited ZetaSmartLists: TZetaSmartLists
    Left = 440
    Top = 120
  end
end
