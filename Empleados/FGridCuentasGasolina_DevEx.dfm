inherited TarjetasGasolina_DevEx: TTarjetasGasolina_DevEx
  Left = 304
  Top = 194
  Caption = 'Tarjetas de Gasolina'
  ClientHeight = 348
  ClientWidth = 560
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 312
    Width = 560
    inherited OK_DevEx: TcxButton
      Left = 396
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 476
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 560
    inherited ValorActivo2: TPanel
      Width = 234
      inherited textoValorActivo2: TLabel
        Width = 228
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 560
    Height = 262
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'N'#250'mero'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre Completo'
        Width = 300
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CB_CTA_GAS'
        Title.Caption = 'Cuenta Tarjeta Gasolina'
        Width = 175
        Visible = True
      end>
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
