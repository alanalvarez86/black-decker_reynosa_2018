unit FCapturarFoto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, Videocap, TDMULTIP, StdCtrls, Mask, DBCtrls, Buttons, db,
  ExtCtrls, ZetaCommonClasses;

type
  TCapturaFoto = class(TZetaDlgModal)
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    lblDriver: TLabel;
    IM_TIPO: TDBComboBox;
    IM_OBSERVA: TDBEdit;
    cbDrivers: TComboBox;
    Panel1: TPanel;
    Documento: TPDBMultiImage;
    VideoCap1: TVideoCap;
    pnValorActivo: TPanel;
    btnCapturar: TBitBtn;
    btnVideo: TBitBtn;
    DataSource1: TDataSource;
    Timer: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCapturarClick(Sender: TObject);
    procedure cbDriversChange(Sender: TObject);
    procedure btnVideoClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TimerTimer(Sender: TObject);
  private
    { Private declarations }
    Nombre: String;
    FCierraForma: Boolean;
    function DriverManager: Boolean;
    function InicializaVideo: Boolean;
    procedure SetControls(lHabilita: Boolean);
    procedure SetDriver;
    procedure TomaFotografia;

  public
    { Public declarations }
  end;

var
  CapturaFoto: TCapturaFoto;

//function CapturarImagen( const iEmpleado: TNumEmp; const sNombre: String; var sTipo, sDescripcion: String; var Imagen: TBlobData  ): Boolean;
function CapturarImagen( const sNombre: String ): Boolean;

implementation

uses dRecursos, ZetaDialogo, ZAccesosTress, ZAccesosMgr, Clipbrd,
     FToolsFoto, DGlobal, ZGlobalTress, ZetaCommonTools;

{$R *.DFM}

function CapturarImagen( const sNombre: String ): Boolean;
begin
     try
        if not Assigned( CapturaFoto ) then
        begin
             CapturaFoto := TCapturaFoto.Create( Application );
        end;
        with CapturaFoto do
        begin
             Nombre := sNombre;
             ShowModal;
             Result := ( ModalResult = mrOk );
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
     Clipbrd.Clipboard.Clear;
end;

procedure TCapturaFoto.FormCreate(Sender: TObject);
begin
     inherited;
     FCierraForma := False;
     HelpContext:= H10122_Expediente_Foto_empleado;
     DataSource1.DataSet := dmRecursos.cdsEmpFoto;
end;

procedure TCapturaFoto.FormShow(Sender: TObject);
begin
     inherited;
     dmRecursos.cdsEmpFoto.Conectar;
     pnValorActivo.Caption := Nombre;
     if DriverManager then
     begin
          SetControls( False );
          FCierraForma :=  not InicializaVideo;
          Timer.Enabled := FCierraForma;
     end
     else
     begin
          SetControls( True );
          Timer.Enabled := True;
     end;
end;

function TCapturaFoto.InicializaVideo: Boolean;
var
   oCursor: TCursor;
begin
     Result := True;
     Ok.Enabled := True;
     with VideoCap1 do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             try
                VideoPreview := True;
                SetControls(False);
            except
                  on Error: ENotConnectException do
                  begin
                       SetControls( True );
                       Ok.Enabled := False;
                       ZetaDialogo.ZError('Error Al Capturar Imagen', 'El Dispositivo Seleccionado No Est� Disponible', 0 );
                       Result := False;
                  end;
                  on Error: ENoDriverException do
                  begin
                       Ok.Enabled := False;
                       SetControls( True );
                       ZetaDialogo.ZError('Error Al Capturar Imagen', 'El Dispositivo Seleccionado No Est� Disponible', 0 );
                       Result := False;
                  end;
            end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

function TCapturaFoto.DriverManager: Boolean;
var
   i: Integer;
   oDriverList: TStringList;
begin
     Result := True;
     oDriverList := TStringList.Create;
     with oDriverList do
     begin
          try
             oDriverList := VideoCap.GetDriverList;
             if( Count > 0 )then
             begin
                  for i:=0 to ( Count - 1 ) do
                  begin
                       cbDrivers.Items.Add( Strings[i] );
                  end;
                  cbDrivers.ItemIndex := 0;
                  cbDrivers.Visible := ( Count > 1 );
                  lblDriver.Visible := ( Count > 1 );
                  SetDriver;
             end
             else
             begin
                  Result := False;
                  cbDrivers.Visible := False;
                  lblDriver.Visible := False;
                  Ok.Enabled := False;
                  ZetaDialogo.ZError('Error Al Capturar Imagen', 'No Se Tienen Dispositivos de Video Instalados', 0 );
             end;
          finally
                 FreeAndNil( oDriverList );
          end;
     end;
end;


procedure TCapturaFoto.btnCapturarClick(Sender: TObject);
begin
     inherited;
     TomaFotografia;
end;

procedure TCapturaFoto.TomaFotografia;
begin
     SetControls( True );
     VideoCap1.SaveToClipboard;
     with Documento do
     begin
          UpdateAsJPG:=True;
          PastefromClipboard;
     end;
     //Documento.PasteFromClipboard;
end;

procedure TCapturaFoto.cbDriversChange(Sender: TObject);
begin
     inherited;
     SetDriver;
     InicializaVideo;
end;

procedure TCapturaFoto.btnVideoClick(Sender: TObject);
begin
     inherited;
     SetControls( True );
     dmRecursos.cdsEmpFoto.CancelUpdates;
     InicializaVideo;
end;

procedure TCapturaFoto.SetDriver;
begin
     VideoCap1.DriverIndex := cbDrivers.ItemIndex;
end;

procedure TCapturaFoto.SetControls( lHabilita: Boolean );
begin
     Documento.Visible := lHabilita;
     btnVideo.Enabled := lHabilita;
     btnCapturar.Enabled := not lHabilita;
     VideoCap1.Visible := not lHabilita;
end;

procedure TCapturaFoto.OKClick(Sender: TObject);
begin
     if( btnCapturar.Enabled )then
         TomaFotografia;
         //acl1
     if ( strLleno( dmRecursos.cdsEmpFoto.FieldByName('IM_BLOB').AsString ) ) then
     begin
          {OP: 25/06/08}
          with Documento do
             begin
                  CutToClipboard;
                  UpdateAsJPG:=True;
                  PastefromClipboard;
             end;
             try        
             if( Not Global.GetGlobalBooleano( K_GLOBAL_CONSERVAR_TAM_FOTOS ) )then
                 if( Documento.Picture.Bitmap.Width > 300 ) then
                     FToolsFoto.ResizeImageBestFit( Documento, 300, 300 );
          except
                on Error: Exception do
                begin
                     ZError( self.Caption,'No se pudo ajustar el tama�o de la fotograf�a. Se guardar� con su tama�o original'+CR_LF+Error.Message, 0);
                end;
          end;
     end;
     inherited;
end;

procedure TCapturaFoto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     VideoCap1.DriverOpen := False;
     VideoCap1.StopCapture;
     Documento.ResetImage;
end;


procedure TCapturaFoto.TimerTimer(Sender: TObject);
begin
     inherited;
     if FCierraForma then
     begin
          Timer.Enabled := False;
          Close;
     end;
end;


end.
