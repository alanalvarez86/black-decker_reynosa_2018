unit FEditHisPrestamos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon, ZetaNumero, ZetaDBTextBox, StdCtrls, ZetaKeyLookup,
  Mask, ZetaFecha, DBCtrls, Db,
  {$ifdef CAJAAHORRO}
  dCajaAhorro,
  {$else}
  dRecursos,
  {$endif}
  Grids, DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls, Buttons, ZetaKeyCombo,
  ZetaMessages, ZBaseEdicion, ZetaEdit, ZetaSmartLists;

type
  TEditHisPrestamos = class(TBaseEdicionRenglon)
    AH_TIPOLbl: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    PR_FORMULA: TDBMemo;
    PR_FECHA: TZetaDBFecha;
    GBTotales: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    PR_CARGOS: TZetaDBTextBox;
    Label5: TLabel;
    PR_ABONOS: TZetaDBTextBox;
    Label6: TLabel;
    PR_TOTAL: TZetaDBTextBox;
    LSigno1: TLabel;
    LSigno2: TLabel;
    LSigno3: TLabel;
    LSigno4: TLabel;
    SaldoLbl: TLabel;
    PR_SALDO: TZetaDBTextBox;
    PR_SALDO_I: TZetaDBNumero;
    DedLbl: TLabel;
    PR_NUMERO: TZetaDBTextBox;
    Label7: TLabel;
    PR_STATUS: TZetaDBKeyCombo;
    Label8: TLabel;
    PR_REFEREN: TZetaDBEdit;
    Label9: TLabel;
    PR_MONTO: TZetaDBNumero;
    Label10: TLabel;
    SBCO_FORMULA: TSpeedButton;
    US_DESCRIP: TZetaDBTextBox;
    Label11: TLabel;
    ZFecha: TZetaDBFecha;
    Label12: TLabel;
    PR_SUB_CTA: TZetaDBEdit;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label17: TLabel;
    PR_TASA: TZetaDBNumero;
    PR_MONTO_S: TZetaDBNumero;
    PR_MESES: TZetaDBNumero;
    PR_INTERES: TZetaDBNumero;
    PR_PAGOS: TZetaDBNumero;
    PR_PAG_PER: TZetaDBNumero;
    PR_MONTO_INT: TZetaDBNumero;
    PR_TIPO_TXT: TZetaDBTextBox;
    PR_TIPO_DESC: TZetaTextBox;
    PR_TIPO: TZetaDBKeyLookup;
    Label18: TLabel;
    PR_OBSERVA: TZetaDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure SBCO_FORMULAClick(Sender: TObject);
    procedure GridRenglonesColExit(Sender: TObject);
    procedure GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CancelarClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure dsRenglonStateChange(Sender: TObject);
  private
    { Private declarations }
    FPuedeRefrescar: Boolean; //Agregada para corregir Bug de navegacion en el detalle del grid Cargos/Abonos
    procedure ControlFecha;
    procedure SetError;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure KeyPress(var Key: Char); override; { TWinControl }
    procedure Agregar; override;
    {$ifdef CAJAAHORRO}
    function dmRecursos : TdmCajaAhorro;
    {$endif}
  public
    { Public declarations }
  end;

var
  EditHisPrestamos: TEditHisPrestamos;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZConstruyeFormula,
     ZAccesosTress,
     dSistema,
     dTablas,
     ZAccesosMgr;
{$R *.DFM}

procedure TEditHisPrestamos.SBCO_FORMULAClick(Sender: TObject);
begin
     with dmRecursos.cdsHisPrestamos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'PR_FORMULA').AsString := GetFormulaConst( enNomina, PR_FORMULA.Lines.Text, PR_FORMULA.SelStart, evBase );
     end;
end;

procedure TEditHisPrestamos.Connect;
begin
     dmTablas.cdsTPresta.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          {$ifdef B&D}
          cdsTotalHisPrestamos.Conectar;
          Datasource.Dataset := cdsTotalHisPrestamos;
          {$else}
          cdsHisPrestamos.Conectar;
          Datasource.Dataset := cdsHisPrestamos;
          {$endif}
          cdsPCarAbo.Conectar;
          dsRenglon.DataSet := cdsPCarAbo;
     end;
     PR_TIPO_DESC.Caption := PR_TIPO.Descripcion;
end;

procedure TEditHisPrestamos.FormCreate(Sender: TObject);
begin
     inherited;
     GridRenglones.Options := [ dgEditing,
                                dgTitles,
                                dgIndicator,
                                dgColumnResize,
                                dgColLines,
                                dgRowLines,
                                dgTabs ];

     TipoValorActivo1 := stEmpleado;
     PR_TIPO.LookupDataset := dmTablas.cdsTPresta;

     HelpContext:= ZetaCommonClasses.H10142_Prestamos;
     IndexDerechos := ZAccesosTress.D_EMP_NOM_PRESTAMOS;

     {$ifdef CAJAAHORRO}
     PR_TIPO.Visible := False;
     PR_TIPO_TXT.Visible := True;
     PR_TIPO_DESC.Visible := True;
     TipoValorActivo2 := stTipoAhorro;
     FirstControl := PR_REFEREN;
     {$else}
     PR_TIPO.Visible := True;
     PR_TIPO_TXT.Visible := False;
     PR_TIPO_DESC.Visible := False;
     FirstControl := PR_TIPO;
     {$endif}
end;


procedure TEditHisPrestamos.GridRenglonesColExit(Sender: TObject);
begin
     inherited;
     with GridRenglones do
     begin
          if ( SelectedField <> nil ) and
             ( SelectedField.FieldName = 'CR_FECHA' ) and
             ( zFecha.Visible ) then
          begin
               zFecha.Visible:= False;
          end;
     end;
end;

procedure TEditHisPrestamos.GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'CR_FECHA' ) then
          begin
               with zFecha do
               begin
                    Left := Rect.Left + GridRenglones.Left;
                    Top := Rect.Top + GridRenglones.Top;
                    Width := Column.Width + 2;
                    Visible := True;
               end;
          end;
     end;
end;

procedure TEditHisPrestamos.ControlFecha;
begin
     if PageControl.ActivePage = Tabla then
     begin
          Self.ActiveControl := GridRenglones;
          GridRenglones.SelectedField := GridRenglones.Columns[ PrimerColumna + 1 ].Field;
          GridRenglones.SelectedField := GridRenglones.Columns[ PrimerColumna ].Field;
     end;
end;

procedure TEditHisPrestamos.CancelarClick(Sender: TObject);
begin
     inherited;
     ControlFecha;
end;

procedure TEditHisPrestamos.OKClick(Sender: TObject);
begin
     inherited;
     ControlFecha;
end;

procedure TEditHisPrestamos.BBBorrarClick(Sender: TObject);
begin
     inherited;
     ControlFecha;
end;

procedure TEditHisPrestamos.BBAgregarClick(Sender: TObject);
begin
     inherited;
     SeleccionaPrimerColumna;
end;

procedure TEditHisPrestamos.KeyPress( var Key: Char );
begin
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( GridRenglones.SelectedField.FieldName = 'CR_FECHA' ) then
               begin
                    zFecha.SetFocus;
                    SendMessage( zFecha.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end
               else
               begin
                    if ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
                    begin
                         with GridRenglones do
                         begin
                              Key := #0;
                              SelectedIndex := PosicionaSiguienteColumna;
                              if ( Selectedindex = PrimerColumna ) then
                                 SetOk;
                         end;
                    end
                    else
                    begin
                         if ( Key = Chr( VK_ESCAPE ) ) then
                         begin
                              SeleccionaPrimerColumna;
                         end;
                    end;
               end;
          end;
     end
     else
     begin
          if ( ActiveControl = zFecha ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
          begin
               Key := #0;
               with GridRenglones do
               begin
                    SetFocus;
                    SelectedField := dmRecursos.cdsPCarAbo.FieldByName( 'CR_CARGO' );
               end;
          end;
     end;
     inherited;
end;

procedure TEditHisPrestamos.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          GridRenglones.SelectedIndex := PosicionaSiguienteColumna;
          if ( GridRenglones.SelectedIndex = PrimerColumna ) then
             Setok;
     end;
end;

procedure TEditHisPrestamos.PageControlChange(Sender: TObject);
begin
     with PageControl do
     begin
          if ( ActivePage = Tabla ) then
          begin
               if ( ZetaCommonTools.StrLleno( PR_TIPO.Llave ) ) then
               begin
                    SeleccionaPrimerColumna;
                    ControlFecha;
               end
               else
                   SetError;
          end;
     end;
end;

procedure TEditHisPrestamos.SetError;
begin
     PageControl.ActivePage := Datos;
     ZetaDialogo.ZError( 'Pr�stamo', 'No se Pueden Agregar Cargos o Abonos si el' + CR_LF +
                                     'C�digo del Pr�stamo est� Vacio ', 0 );
     FocusFirstControl;
end;

{$ifdef CAJAAHORRO}
function TEditHisPrestamos.dmRecursos: TdmCajaAhorro;
begin
     Result := dmCajaAhorro;
end;
{$endif}


procedure TEditHisPrestamos.Agregar;
begin
     {$ifdef CAJAAHORRO}
     ClientDataset.Agregar;
     {$else}
     inherited Agregar;
     {$endif}
end;

{***Correccion de bug (by: am): Agregado para corregir bug en VS 2013 que no actualiza el
          detalle de cargos/abonos al navegar entre los prestamos***}
procedure TEditHisPrestamos.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
    if Field = nil then
     if dmRecursos.cdsHisAhorros.State = dsBrowse then
        if dsRenglon.DataSet <> nil then //Para evitar que se realice antes de que el grid tenga datos
           if FPuedeRefrescar and ( dmRecursos.cdsPCarAbo.ChangeCount = 0 ) then
                dmRecursos.cdsPCarAbo.Refrescar;
end;

{***Correccion de bug (by: am):Agregado para corregir bug en VS 2013 que no actualiza el
          detalle de cargos/abonos al navegar entre los prestamos***}
procedure TEditHisPrestamos.dsRenglonStateChange(Sender: TObject);
begin
  inherited;
  if dsRenglon.State = dsBrowse then
      FPuedeRefrescar:= True
  else
      FPuedeRefrescar:= False;
end;

end.
