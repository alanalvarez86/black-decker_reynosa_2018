unit FEditEmpPariente_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, StdCtrls, DBCtrls, Mask, Db, ExtCtrls, Buttons,
  ZetaDBTextBox, ZetaKeyCombo,ZetaFecha, ZetaNumero, ZetaSmartLists,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TEditEmpPariente_DevEx = class(TBaseEdicion_DevEx)
    PA_RELACIONLbl: TLabel;
    Label1: TLabel;
    PA_TRABAJALbl: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    PA_RELACIO: TZetaDBKeyCombo;
    PA_NOMBRE: TDBEdit;
    PA_TEXTO: TDBEdit;
    PA_NUMERO: TZetaDBNumero;
    Label6: TLabel;
    PA_FOLIO: TZetaDBNumero;
    PA_SEXO: TZetaKeyCombo;
    PA_FEC_NAC: TZetaDBFecha;
    PA_TRABAJA: TZetaDBKeyLookup_DevEx;
    PA_APE_PAT: TDBEdit;
    Label7: TLabel;
    PA_NOMBRES: TDBEdit;
    Label8: TLabel;
    PA_APE_MAT: TDBEdit;
    Label9: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure PA_SEXOChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure CancelarClick(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    iEmpleadoAnt: Integer;
    procedure IniciaSexo;
    procedure RevisaEmpleado;
    procedure IniciaEmpleadoAnt;
  protected
    procedure Connect; override;
  public
  end;

var
  EditEmpPariente_DevEx: TEditEmpPariente_DevEx;

implementation

uses dRecursos, ZetaCommonLists, ZetaCommonClasses, ZAccesosTress, DCliente,
  ZetaClientDataSet;

{$R *.DFM}

procedure TEditEmpPariente_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_EMP_CURR_PARIENTES;
     FirstControl := PA_RELACIO;
     HelpContext:= H10123_Expediente_Parientes_empleado;
     TipoValorActivo1 := stEmpleado;
     PA_TRABAJA.LookupDataset := dmCliente.cdsEmpleadoLookUp;

end;

procedure TEditEmpPariente_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmRecursos do
     begin
          DataSource.DataSet:= cdsEmpParientes;
          IniciaSexo;
     end;
end;

procedure TEditEmpPariente_DevEx.IniciaSexo;
begin
     with dmRecursos.cdsEmpParientes, PA_SEXO do
     begin
          if ( FieldByName( 'PA_SEXO' ).AsString = 'F' ) then
             ItemIndex:= Ord( esFemenino )
          else
             ItemIndex:= Ord( esMasculino );
     end;
end;

procedure TEditEmpPariente_DevEx.PA_SEXOChange(Sender: TObject);
var
   sSexo: String;
begin
     sSexo:= ObtieneElemento( lfSexo, PA_SEXO.ItemIndex );
     with dmRecursos.cdsEmpParientes do
          if ( sSexo <> FieldByName( 'PA_SEXO' ).AsString ) then
          begin
               if not Editing then
                  Edit;
               FieldByName( 'PA_SEXO' ).AsString:= sSexo;
          end;
end;

procedure TEditEmpPariente_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then
     begin
          IniciaEmpleadoAnt;
     end
     else
     begin
          if ( Field.FieldName = 'PA_TRABAJA' ) then
          begin
               RevisaEmpleado;
          end;
     end;
     IniciaSexo;
end;

procedure TEditEmpPariente_DevEx.CancelarClick(Sender: TObject);
begin
     inherited;
     IniciaSexo;
end;

procedure TEditEmpPariente_DevEx.RevisaEmpleado;
begin
     with dmRecursos.cdsEmpParientes do
     begin
          if ( iEmpleadoAnt <> FieldByName( 'PA_TRABAJA' ).AsInteger ) then
          begin
               IniciaEmpleadoAnt;
               if ( FieldByName( 'PA_TRABAJA' ).AsInteger > 0 ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    //FieldByName('PA_NOMBRE').AsString:= dmCliente.cdsEmpleadoLookUp.GetDescription;
                    FieldByName('PA_NOMBRES').AsString:= dmCliente.cdsEmpleadoLookUp.FieldByName('CB_NOMBRES').AsString;
                    FieldByName('PA_APE_PAT').AsString:= dmCliente.cdsEmpleadoLookUp.FieldByName('CB_APE_PAT').AsString;
                    FieldByName('PA_APE_MAT').AsString:= dmCliente.cdsEmpleadoLookUp.FieldByName('CB_APE_MAT').AsString;

                    FieldByName('PA_FEC_NAC').AsDateTime:= dmCliente.cdsEmpleadoLookUp.FieldByName('CB_FEC_NAC').AsDateTime;
               end;
          end;
     end;
end;

procedure TEditEmpPariente_DevEx.IniciaEmpleadoAnt;
begin
     iEmpleadoAnt := dmRecursos.cdsEmpParientes.FieldByName( 'PA_TRABAJA' ).AsInteger;
     if iEmpleadoAnt = 0 then
        PA_TRABAJA.ResetMemory; 
end;

procedure TEditEmpPariente_DevEx.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     if ( Datasource.Dataset <> NIL ) and ( Datasource.Dataset.State = dsInsert ) then
        PA_TRABAJA.SetLlaveDescripcion( VACIO, VACIO );

end;

procedure TEditEmpPariente_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
 IniciaSexo;
end;

end.
