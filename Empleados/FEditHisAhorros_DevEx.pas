unit FEditHisAhorros_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaNumero, ZetaDBTextBox, StdCtrls, ZetaKeyLookup_DevEx,
  Mask, ZetaFecha, DBCtrls, Db,
  {$ifdef CAJAAHORRO}
  dCajaAhorro,
  {$else}
  dRecursos,
  {$endif}
  Grids, DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls, Buttons, ZetaKeyCombo,
  ZetaMessages, ZetaSmartLists, ZetaEdit, ZBaseEdicionRenglon_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
    TressMorado2013,
  dxSkinsDefaultPainters, cxControls,
  dxSkinsdxBarPainter, ImgList, dxBarExtItems, dxBar, cxClasses,
  cxNavigator, cxDBNavigator, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxPC;


type
  TEditHisAhorros_DevEx = class(TBaseEdicionRenglon_DevEx)
    AH_TIPOLbl: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    AH_FORMULA: TDBMemo;
    AH_FECHA: TZetaDBFecha;
    AH_TIPO: TZetaDBKeyLookup_DevEx;
    GBTotales: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    AH_CARGOS: TZetaDBTextBox;
    Label5: TLabel;
    AH_ABONOS: TZetaDBTextBox;
    Label6: TLabel;
    AH_TOTAL: TZetaDBTextBox;
    LSigno1: TLabel;
    LSigno2: TLabel;
    LSigno3: TLabel;
    LSigno4: TLabel;
    SaldoLbl: TLabel;
    AH_SALDO: TZetaDBTextBox;
    ZetaDBNumero1: TZetaDBNumero;
    UsuarioLbl: TLabel;
    DedLbl: TLabel;
    AH_NUMERO: TZetaDBTextBox;
    Label7: TLabel;
    AH_STATUS: TZetaDBKeyCombo;
    US_DESCRIP: TZetaDBTextBox;
    ZFecha: TZetaDBFecha;
    Label8: TLabel;
    AH_SUB_CTA: TZetaDBEdit;
    SBCO_FORMULA_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure SBCO_FORMULAClick(Sender: TObject);
    procedure GridRenglonesColExit(Sender: TObject);
    procedure GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure ControlFecha;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    procedure SetError;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    {$ifdef CAJAAHORRO}
    function dmRecursos : TdmCajaAhorro;
    {$endif}
  public
    { Public declarations }
  end;

var
  EditHisAhorros_DevEx: TEditHisAhorros_DevEx;

implementation
uses ZetaCommonClasses,
     ZetaTipoEntidad,
     ZAccesosTress,
     ZetaDialogo,
     dSistema,
     dTablas,
     ZConstruyeFormula,
     ZetaCommonTools,
     ZetaCommonLists,
     ZAccesosMgr;

{$R *.DFM}

procedure TEditHisAhorros_DevEx.Connect;
begin
     dmTablas.cdsTAhorro.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsHisAhorros.Conectar;
          Datasource.Dataset := cdsHisAhorros;
          dsRenglon.DataSet := cdsACarAbo;
     end;
end;

procedure TEditHisAhorros_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     GridRenglones.Options := [ dgEditing,
                                dgTitles,
                                dgIndicator,
                                dgColumnResize,
                                dgColLines,
                                dgRowLines,
                                dgTabs ];
     
     FirstControl := AH_TIPO;
     IndexDerechos := ZAccesosTress.D_EMP_NOM_AHORROS;
     AH_TIPO.LookupDataset := dmTablas.cdsTAhorro;
     TipoValorActivo1 := stEmpleado;
     {$ifdef CAJAAHORRO}
     TipoValorActivo2 := stTipoAhorro;
     HelpContext:= H_REG_AHORRO;
     dxBarControlContainerItem_DBNavigator.Visible := ivNever;
     {$else}
     HelpContext:= H10141_Emp_nom_ahorros;
     {$endif}
     AH_TIPO.EditarSoloActivos := TRUE; //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigne en el DFM, Bug#15743
end;

procedure TEditHisAhorros_DevEx.SBCO_FORMULAClick(Sender: TObject);
begin
  inherited;
     with dmRecursos.cdsHisAhorros do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'AH_FORMULA').AsString := GetFormulaConst( enNomina , AH_FORMULA.Lines.Text, AH_FORMULA.SelStart, evBase );
     end;
end;

procedure TEditHisAhorros_DevEx.GridRenglonesColExit(Sender: TObject);
begin
     inherited;
     with GridRenglones do
     begin
          if ( SelectedField <> nil ) and
             ( SelectedField.FieldName = 'CR_FECHA' ) and
             ( zFecha.Visible ) then
          begin
               zFecha.Visible:= False;
          end;
     end;
end;

procedure TEditHisAhorros_DevEx.GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'CR_FECHA' ) then
          begin
               with zFecha do
               begin
                    Left := Rect.Left + GridRenglones.Left;
                    Top := Rect.Top + GridRenglones.Top;
                    Width := Column.Width + 2;
                    Visible := TRUE;
               end;
          end;
     end;
end;

procedure TEditHisAhorros_DevEx.ControlFecha;
begin
     if PageControl.ActivePage = Tabla then
     begin
          Self.ActiveControl := GridRenglones;
          GridRenglones.SelectedField := GridRenglones.Columns[ PrimerColumna + 1 ].Field;
          GridRenglones.SelectedField := GridRenglones.Columns[ PrimerColumna ].Field;
     end;
end;

procedure TEditHisAhorros_DevEx.BBBorrarClick(Sender: TObject);
begin
     inherited;
     ControlFecha;
end;

procedure TEditHisAhorros_DevEx.BBAgregarClick(Sender: TObject);
begin
     inherited;
     SeleccionaPrimerColumna;
end;

procedure TEditHisAhorros_DevEx.KeyPress( var Key: Char );
begin
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( GridRenglones.SelectedField.FieldName = 'CR_FECHA' ) then
               begin
                    zFecha.SetFocus;
                    SendMessage( zFecha.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end
               else
               begin
                    if ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
                    begin
                         with GridRenglones do
                         begin
                              Key := #0;
                              SelectedIndex := PosicionaSiguienteColumna;
                              if Selectedindex = PrimerColumna then
                                 SetOk;
                         end;
                    end
                    else
                    begin
                         if ( Key = Chr( VK_ESCAPE ) ) then
                         begin
                              SeleccionaPrimerColumna;
                         end;
                    end;
               end;
          end;
     end
     else
     begin
          if ( ActiveControl = zFecha ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
          begin
               Key := #0;
               with GridRenglones do
               begin
                    SetFocus;
                    SelectedField := dmRecursos.cdsACarAbo.FieldByName( 'CR_CARGO' );
               end;
          end;
     end;
     inherited;
end;

procedure TEditHisAhorros_DevEx.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          GridRenglones.SelectedIndex := PosicionaSiguienteColumna;
          if ( GridRenglones.SelectedIndex = PrimerColumna ) then
             Setok;
     end;
end;

procedure TEditHisAhorros_DevEx.PageControlChange(Sender: TObject);
begin
     with PageControl do
     begin
          if ( ActivePage = Tabla ) then
          begin
               if ( ZetaCommonTools.StrLleno( AH_TIPO.Llave ) ) then
               begin
                    SeleccionaPrimerColumna;
                    ControlFecha;
               end
               else
                   SetError;
          end;
     end;
end;

procedure TEditHisAhorros_DevEx.SetError;
begin
     PageControl.ActivePage := Datos;
     ZetaDialogo.ZError( 'Ahorro', 'No se Pueden Agregar Cargos o Abonos si el' + CR_LF +
                                   'C�digo del Ahorro est� Vacio ', 0 );
     FocusFirstControl;
end;

{$ifdef CAJAAHORRO}
function TEditHisAhorros_DevEx.dmRecursos: TdmCajaAhorro;
begin
     Result := dmCajaAhorro;
end;
{$endif}

function TEditHisAhorros_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     {$ifdef CAJAAHORRO}
     Result:= FALSE;
     sMensaje:= 'No se permite agregar registros';
     {$else}
     Result:= inherited PuedeAgregar( sMensaje );
     {$endif}
end;

function TEditHisAhorros_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     {$ifdef CAJAAHORRO}
     Result:= FALSE;
     sMensaje:= 'No se permite borrar registros';
     {$else}
     Result:= inherited PuedeBorrar( sMensaje );
     {$endif}

end;

procedure TEditHisAhorros_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
  ControlFecha;
end;

procedure TEditHisAhorros_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
  ControlFecha;
end;

end.




