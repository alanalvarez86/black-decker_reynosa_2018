unit FEditHisEvaluaCompeten_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, StdCtrls, Mask, DBCtrls, Db, Buttons,
  ExtCtrls,ZetaDBTextBox, ZetaKeyCombo, ZetaNumero,
  ZetaFecha, ZetaSmartLists,Variants, 
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx;

type
  TEditHisEvaluaComp_DevEx = class(TBaseEdicion_DevEx)
    Label13: TLabel;
    Label5: TLabel;
    UsuarioLbl: TLabel;
    EC_COMENT: TDBEdit;
    CC_CODIGO: TZetaDBKeyLookup_DevEx;
    EC_FECHA: TZetaDBTextBox;
    Label2: TLabel;
    US_DESCRIP: TZetaDBTextBox;
    Label1: TLabel;
    NC_NIVEL: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure CC_CODIGOValidKey(Sender: TObject);

  protected
   procedure Connect;override;
  public
  end;

var
  EditHisEvaluaComp_DevEx: TEditHisEvaluaComp_DevEx;


implementation

uses DTablas,
     DCatalogos,
     {$IFNDEF SUPERVISORES}
     DRecursos,
     {$ELSE}
     DSuper,
     {$ENDIF}
     DSistema,
     ZAccesosTress,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZGlobalTress;

{$R *.DFM}



procedure TEditHisEvaluaComp_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := ZetaCommonLists.stEmpleado;
     IndexDerechos := ZAccesosTress.D_EMP_HIS_EVA_COMPETEN;
     FirstControl := CC_CODIGO;
     HelpContext:= H_Empleados_Edit_His_Competen;
     WITH dmCatalogos do
     begin
          CC_CODIGO.LookupDataset := cdsCompetencias;
          NC_NIVEL.LookupDataset := cdsCompNiveles;
     end;
end;

procedure TEditHisEvaluaComp_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     {$IFNDEF SUPERVISORES}
     with dmRecursos do
     {$ELSE}
     with dmSuper do
     {$ENDIF}
     begin
          cdsHisCompEvalua.Conectar;
          Datasource.DataSet := cdsHisCompEvalua;
     end;
     with dmCatalogos do
     begin
          cdsCompetencias.Conectar;
          if StrLleno(NC_NIVEL.Llave)then
          begin
               cdsCompNiveles.Locate('CC_CODIGO;NC_NIVEL',VarArrayOf([CC_CODIGO.Llave,NC_NIVEL.Valor]),[]);
               NC_NIVEL.SetLlaveDescripcion(NC_NIVEL.Llave,cdsCompNiveles.FieldByName('NC_DESCRIP').AsString);
          end;
     end;
end;

procedure TEditHisEvaluaComp_DevEx.CC_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     NC_NIVEL.Enabled := (CC_CODIGO.Llave <> VACIO );
     if StrLleno(CC_CODIGO.Llave) then
     begin
          NC_NIVEL.Filtro := Format(' CC_CODIGO = ''%s''',[dmCatalogos.cdsCompetencias.FieldByName('CC_CODIGO').AsString]);
     end
     else
         NC_NIVEL.Filtro := VACIO;

     with {$IFNDEF SUPERVISORES}dmRecursos{$ELSE}dmSuper{$endif}.cdsHisCompEvalua.FieldByName('CC_CODIGO') do
     begin
          if ( OldValue <> NewValue )then
          begin
               NC_NIVEL.Llave := VACIO;
          end;
     end;
end;

end.
