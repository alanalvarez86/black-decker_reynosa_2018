unit FEditHisPensionesAlimenticias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon_DevEx, ZetaNumero, ZetaDBTextBox, StdCtrls, 
  Mask, ZetaFecha, DBCtrls, Db,
  {$ifdef CAJAAHORRO}
  dCajaAhorro,
  {$else}
  dRecursos,
  {$endif}
  Grids, DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls, Buttons, ZetaKeyCombo,
  ZetaMessages, ZetaEdit, ZetaSmartLists, 
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinscxPCPainter, cxPCdxBarPopupMenu, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxPC, cxNavigator,
  cxDBNavigator, cxButtons, dxBarBuiltInMenu;

type
  TEditHisPensionAlimenticia_DevEx = class(TBaseEdicionRenglon_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    PS_OBSERVA: TDBMemo;
    PS_FECHA: TZetaDBFecha;
    DedLbl: TLabel;
    Label8: TLabel;
    PS_EXPEDIE: TDBEdit;
    US_DESCRIP: TZetaDBTextBox;
    Label11: TLabel;
    PS_ORDEN: TZetaDBTextBox;
    Label7: TLabel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    PS_CTA_1: TDBEdit;
    Label4: TLabel;
    PS_CTA_2: TDBEdit;
    Label5: TLabel;
    PS_BANCO_1: TDBEdit;
    Label6: TLabel;
    PS_BANCO_2: TDBEdit;
    PS_FIJA: TZetaDBNumero;
    Label9: TLabel;
    GroupBox2: TGroupBox;
    PS_BENEFIC: TDBEdit;
    PS_B_DOM: TDBEdit;
    Label19: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    PS_B_EMAIL: TDBEdit;
    Label13: TLabel;
    PS_B_TEL: TDBEdit;
    btnBuscarEmp_DevEx: TcxButton;
    PS_ACTIVA: TDBCheckBox;
    Label14: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure GridRenglonesDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure GridRenglonesColExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnBuscarEmp_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  public
    { Public declarations }
  end;
  TDBGridHack = class(TDBGrid);
var
  EditHisPensionAlimenticia_DevEx: TEditHisPensionAlimenticia_DevEx;

CONST
     K_COL_TP_CODIGO = 0;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZConstruyeFormula,
     ZAccesosTress,
     dSistema,
     dTablas,
     ZAccesosMgr, dCatalogos,ZetaBusqueda_DevEx;
{$R *.DFM}


procedure TEditHisPensionAlimenticia_DevEx.Connect;
begin
     dmCatalogos.cdsTiposPension.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsHisPensionesAlimenticias.Conectar;
          Datasource.Dataset := cdsHisPensionesAlimenticias;
          dsRenglon.DataSet := cdsPorcPensiones;
     end;
end;

procedure TEditHisPensionAlimenticia_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
      GridRenglones.Options := [ dgEditing,
                                dgTitles,
                                dgIndicator,
                                dgColumnResize,
                                dgColLines,
                                dgRowLines,
                                dgTabs,
                                dgCancelOnExit ];

     TipoValorActivo1 := stEmpleado;

     HelpContext:= ZetaCommonClasses.H_EMP_EXP_PENSIONES_EDIT;
     IndexDerechos := ZAccesosTress.D_EMP_NOM_PENSIONES;

     FirstControl := PS_FECHA;

end;



procedure TEditHisPensionAlimenticia_DevEx.BBAgregarClick(Sender: TObject);
begin
     inherited;
     SeleccionaPrimerColumna;
end;

procedure TEditHisPensionAlimenticia_DevEx.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          GridRenglones.SelectedIndex := PosicionaSiguienteColumna;
          if ( GridRenglones.SelectedIndex = PrimerColumna ) then
             Setok;
     end;
end;


procedure TEditHisPensionAlimenticia_DevEx.KeyDown( var Key: Word; Shift: TShiftState );
begin
     if GridEnfocado and not ( ClientDatasetHijo.State in [ dsInsert, dsEdit ] ) then
     begin
          if ( ssShift in Shift ) then { SHIFT }
          begin
               case Key of
                    VK_INSERT:    { INS = Agregar }
                    begin
                         Key := 0;
                         DoInsert;
                    end;
                    VK_DELETE:    { DEL = Borrar }
                    begin
                         Key := 0;
                         DoDelete;
                    end;
               end;
          end
          else
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        VK_INSERT:   { INS = Modificar }
                        begin
                             Key := 0;
                             DoEdit;
                        end;
                   end;
              end;
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        66:   { F }
                        begin
                             Key := 0;
                             btnBuscarEmp_DevExClick(Self);
                        end;
                   end;
              end;
     end;
     inherited KeyDown( Key, Shift );
end;


procedure TEditHisPensionAlimenticia_DevEx.GridRenglonesDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
   iCurrentRow, iRecNo: Integer;
begin
     with GridRenglones, Canvas, Brush do
    begin
         if ( State = [] ) then
         begin
              iCurrentRow := Rect.Top div (1+Rect.Bottom-Rect.Top);
              iRecNo:= TDBGridHack( GridRenglones ).Row;
              if iCurrentRow = iRecNo then
              begin
                   //Color:= clBtnShadow;
                   Color:=  RGB(237,230,249);//RGB(156,129,139); //morado row seleccionado
                   Font.Color := clWindowText;
              end;
         end
         else if gdSelected in State then
         begin
              //Color:= clHighlight;
              Color:= RGB(212,188,251);//RGB(136,174,213); //Azul de celda seleccionado
              Font.Color := clHighlightText; //Texto en blanco para celda seleccionada
         end;
         DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
    if (( gdFocused in State ) and ( dmRecursos.cdsPorcPensiones.State in [dsEdit,dsInsert] ) )then
     begin
          if ( Column.FieldName = 'TP_CODIGO' ) then
          begin
               with btnBuscarEmp_DevEx do
               begin
                    Left := Rect.Left + GridRenglones.Left + Column.Width - Width;
                    Top := Rect.Top;
                    Visible := True;
               end;
          end;
     end
     else if  (dmRecursos.cdsPorcPensiones.State in [dsBrowse ] )then
     begin
          btnBuscarEmp_DevEx.Visible := False;
     end;
end;

procedure TEditHisPensionAlimenticia_DevEx.GridRenglonesColExit(Sender: TObject);
begin
     inherited;
     with GridRenglones do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'TP_CODIGO' ) and
                  ( btnBuscarEmp_DevEx.Visible ) then
                  btnBuscarEmp_DevEx.Visible:= False;
          end;
     end;
end;

procedure TEditHisPensionAlimenticia_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Tabla;
     //PS_ACTIVA.Checked := True;
end;

procedure TEditHisPensionAlimenticia_DevEx.btnBuscarEmp_DevExClick(
  Sender: TObject);
var
   sKey,sDescription:string;
begin
     inherited;
     with dmRecursos do
     begin
               if ZetaBusqueda_DevEx.ShowSearchForm( dmCatalogos.cdsTiposPension, VACIO, sKey, sDescription, FALSE, FALSE ) then
          begin
               if ( StrLLeno ( sKey ) ) then
               begin
                    cdsPorcPensiones.Edit;
                    cdsPorcPensiones.FieldByName('TP_CODIGO').AsString := sKey;
                    cdsPorcPensiones.Post;
                    PosicionaSiguienteColumna;
               end;
          end;
     end;
end;

end.
