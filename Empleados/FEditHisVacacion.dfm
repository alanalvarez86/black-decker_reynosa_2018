inherited EditHisVacacion: TEditHisVacacion
  Left = 368
  Top = 192
  Caption = 'Vacaciones'
  ClientHeight = 444
  ClientWidth = 413
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 408
    Width = 413
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 249
    end
    inherited Cancelar: TBitBtn
      Left = 330
    end
  end
  inherited PanelSuperior: TPanel
    Width = 413
    TabOrder = 3
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 413
    TabOrder = 4
    inherited ValorActivo2: TPanel
      Width = 87
      inherited textoValorActivo2: TLabel
        Width = 81
      end
    end
  end
  object PageControl: TPageControl [3]
    Left = 0
    Top = 153
    Width = 413
    Height = 255
    ActivePage = TabMovimiento
    Align = alClient
    TabOrder = 1
    object TabMovimiento: TTabSheet
      Caption = 'D'#237'as a Pagar'
      object LblObservaciones: TLabel
        Left = 45
        Top = 80
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Observaciones:'
      end
      object LblPeriodo: TLabel
        Left = 27
        Top = 58
        Width = 92
        Height = 13
        Caption = 'Per'#237'odo Trabajado:'
      end
      object LblPago: TLabel
        Left = 53
        Top = 10
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as a Pagar:'
      end
      object L_VA_P_PRIMA: TLabel
        Left = 10
        Top = 34
        Width = 109
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as prima vacacional:'
      end
      object VA_PERIODO: TDBEdit
        Tag = 99
        Left = 124
        Top = 56
        Width = 269
        Height = 21
        DataField = 'VA_PERIODO'
        DataSource = DataSource
        MaxLength = 40
        TabOrder = 2
      end
      object VA_PAGO: TZetaDBNumero
        Left = 124
        Top = 8
        Width = 65
        Height = 21
        Mascara = mnDiasFraccion
        TabOrder = 0
        Text = '0.00'
        DataField = 'VA_PAGO'
        DataSource = DataSource
      end
      object GBNominaPago: TGroupBox
        Left = 8
        Top = 134
        Width = 385
        Height = 90
        Caption = 'N'#243'mina a Pagar '
        TabOrder = 4
        object Label7: TLabel
          Left = 88
          Top = 40
          Width = 24
          Height = 13
          Caption = 'Tipo:'
        end
        object Label8: TLabel
          Left = 72
          Top = 64
          Width = 40
          Height = 13
          Caption = 'N'#250'mero:'
        end
        object Label9: TLabel
          Left = 90
          Top = 16
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
        end
        object VA_NOMTIPO: TZetaDBKeyCombo
          Left = 117
          Top = 38
          Width = 190
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfTipoPeriodo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
          DataField = 'VA_NOMTIPO'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object VA_NOMNUME: TZetaDBKeyLookup
          Left = 117
          Top = 62
          Width = 265
          Height = 21
          LookupDataset = dmCatalogos.cdsPeriodo
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
          OnValidKey = VA_NOMNUMEValidKey
          DataField = 'VA_NOMNUME'
          DataSource = DataSource
        end
        object VA_NOMYEAR: TZetaDBNumero
          Left = 117
          Top = 14
          Width = 60
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          DataField = 'VA_NOMYEAR'
          DataSource = DataSource
        end
      end
      object VA_COMENTA: TDBMemo
        Left = 124
        Top = 80
        Width = 269
        Height = 52
        DataField = 'VA_COMENTA'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 3
      end
      object VA_P_PRIMA: TZetaDBNumero
        Left = 124
        Top = 32
        Width = 65
        Height = 21
        Mascara = mnDiasFraccion
        TabOrder = 1
        Text = '0.00'
        DataField = 'VA_P_PRIMA'
        DataSource = DataSource
      end
    end
    object TabGenerales: TTabSheet
      Caption = 'Montos a Pagar'
      object CB_SALARIO: TZetaDBTextBox
        Left = 124
        Top = 8
        Width = 110
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_SALARIO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_SALARIO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label1: TLabel
        Left = 55
        Top = 10
        Width = 65
        Height = 13
        Caption = 'Salario Diario:'
      end
      object Monto: TLabel
        Left = 61
        Top = 31
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vacaciones:'
      end
      object VA_MONTO: TZetaDBTextBox
        Left = 124
        Top = 29
        Width = 110
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'VA_MONTO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_MONTO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label21: TLabel
        Left = 58
        Top = 51
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'S'#233'ptimo D'#237'a:'
      end
      object VA_SEVEN: TZetaDBTextBox
        Left = 124
        Top = 49
        Width = 110
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'VA_SEVEN'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_SEVEN'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label22: TLabel
        Left = 35
        Top = 71
        Width = 85
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima Vacacional:'
      end
      object VA_PRIMA: TZetaDBTextBox
        Left = 124
        Top = 69
        Width = 110
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'VA_PRIMA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_PRIMA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object LblOtros: TLabel
        Left = 92
        Top = 95
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Otros:'
      end
      object Bevel1: TBevel
        Left = 120
        Top = 117
        Width = 110
        Height = 6
        Shape = bsBottomLine
      end
      object VA_TOTAL: TZetaDBTextBox
        Left = 124
        Top = 129
        Width = 110
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'VA_TOTAL'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_TOTAL'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object TotalLbl: TLabel
        Left = 65
        Top = 131
        Width = 55
        Height = 13
        Caption = 'Total Pago:'
      end
      object VA_TASA_PR: TZetaDBTextBox
        Left = 244
        Top = 69
        Width = 89
        Height = 17
        AutoSize = False
        Caption = 'VA_TASA_PR'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_TASA_PR'
        DataSource = DataSource
        FormatFloat = '%14.5n'
        FormatCurrency = '%m'
      end
      object Label3: TLabel
        Left = 339
        Top = 71
        Width = 8
        Height = 13
        Caption = '%'
        Visible = False
      end
      object VA_OTROS: TZetaDBNumero
        Left = 124
        Top = 91
        Width = 110
        Height = 21
        Mascara = mnPesos
        TabOrder = 0
        Text = '0.00'
        DataField = 'VA_OTROS'
        DataSource = DataSource
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Generales'
      object LTablaIMSS: TLabel
        Left = 26
        Top = 16
        Width = 94
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tabla Prestaciones:'
      end
      object ZetaDBTextBox1: TZetaDBTextBox
        Left = 124
        Top = 14
        Width = 46
        Height = 17
        AutoSize = False
        Caption = 'ZetaDBTextBox1'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_TABLASS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object GCapturoLbl: TLabel
        Left = 77
        Top = 36
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modific'#243':'
      end
      object GCapturaLbl: TLabel
        Left = 65
        Top = 56
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modificado:'
      end
      object VA_CAPTURA: TZetaDBTextBox
        Left = 124
        Top = 54
        Width = 80
        Height = 17
        AutoSize = False
        Caption = 'VA_CAPTURA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_CAPTURA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label6: TLabel
        Left = 26
        Top = 96
        Width = 94
        Height = 13
        Alignment = taRightJustify
        Caption = 'A'#241'o de Antig'#252'edad:'
      end
      object ZetaDBTextBox3: TZetaDBTextBox
        Left = 124
        Top = 94
        Width = 45
        Height = 17
        AutoSize = False
        Caption = 'ZetaDBTextBox3'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_YEAR'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label4: TLabel
        Left = 45
        Top = 76
        Width = 75
        Height = 13
        Caption = 'Registro Global:'
      end
      object US_DESCRIP: TZetaDBTextBox
        Left = 124
        Top = 34
        Width = 143
        Height = 17
        AutoSize = False
        Caption = 'US_DESCRIP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object VA_GLOBAL: TDBCheckBox
        Left = 124
        Top = 74
        Width = 17
        Height = 17
        DataField = 'VA_GLOBAL'
        DataSource = DataSource
        ReadOnly = True
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 51
    Width = 413
    Height = 102
    Align = alTop
    TabOrder = 0
    object LVA_VAC_DEL: TLabel
      Left = 48
      Top = 13
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha de Inicio:'
    end
    object LblGozados: TLabel
      Left = 53
      Top = 63
      Width = 71
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'as Gozados:'
    end
    object LVA_VAC_AL: TLabel
      Left = 81
      Top = 38
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Regresa:'
    end
    object BtnRecalculo: TZetaSpeedButton
      Left = 190
      Top = 61
      Width = 20
      Height = 21
      Hint = 'Forzar Rec'#225'lculo de D'#237'as Gozados'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00337000000000
        73333337777777773F333308888888880333337F3F3F3FFF7F33330808089998
        0333337F737377737F333308888888880333337F3F3F3F3F7F33330808080808
        0333337F737373737F333308888888880333337F3F3F3F3F7F33330808080808
        0333337F737373737F333308888888880333337F3F3F3F3F7F33330808080808
        0333337F737373737F333308888888880333337F3FFFFFFF7F33330800000008
        0333337F7777777F7F333308000E0E080333337F7FFFFF7F7F33330800000008
        0333337F777777737F333308888888880333337F333333337F33330888888888
        03333373FFFFFFFF733333700000000073333337777777773333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BtnRecalculoClick
    end
    object BtnRegreso: TZetaSpeedButton
      Left = 245
      Top = 36
      Width = 20
      Height = 22
      Hint = 'Forzar Rec'#225'lculo de Fecha de Regreso'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00337000000000
        73333337777777773F333308888888880333337F3F3F3FFF7F33330808089998
        0333337F737377737F333308888888880333337F3F3F3F3F7F33330808080808
        0333337F737373737F333308888888880333337F3F3F3F3F7F33330808080808
        0333337F737373737F333308888888880333337F3F3F3F3F7F33330808080808
        0333337F737373737F333308888888880333337F3FFFFFFF7F33330800000008
        0333337F7777777F7F333308000E0E080333337F7FFFFF7F7F33330800000008
        0333337F777777737F333308888888880333337F333333337F33330888888888
        03333373FFFFFFFF733333700000000073333337777777773333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BtnRegresoClick
    end
    object VA_FEC_INI: TZetaDBFecha
      Left = 128
      Top = 11
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '16/dic/97'
      Valor = 35780.000000000000000000
      DataField = 'VA_FEC_INI'
      DataSource = DataSource
    end
    object VA_GOZO: TZetaDBNumero
      Left = 128
      Top = 61
      Width = 60
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 3
      Text = '0.00'
      DataField = 'VA_GOZO'
      DataSource = DataSource
    end
    object VA_FEC_FIN: TZetaDBFecha
      Left = 128
      Top = 36
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 2
      Text = '16/dic/97'
      Valor = 35780.000000000000000000
      DataField = 'VA_FEC_FIN'
      DataSource = DataSource
    end
    object gbSaldos: TGroupBox
      Left = 280
      Top = -2
      Width = 125
      Height = 102
      TabOrder = 0
      object Label5: TLabel
        Left = 21
        Top = 45
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Gozo:'
      end
      object ZSaldoGozo: TZetaTextBox
        Left = 53
        Top = 43
        Width = 60
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ZSaldoGozo'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label10: TLabel
        Left = 21
        Top = 64
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pago:'
      end
      object ZSaldoPago: TZetaTextBox
        Left = 53
        Top = 62
        Width = 60
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ZSaldoPago'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object ZSaldoPrima: TZetaTextBox
        Left = 53
        Top = 81
        Width = 60
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ZSaldoPrima'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label11: TLabel
        Left = 20
        Top = 83
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima:'
      end
      object RBAniversario: TRadioButton
        Left = 3
        Top = 7
        Width = 113
        Height = 17
        Caption = 'Saldo al Aniversario'
        Checked = True
        TabOrder = 0
        OnClick = RBSaldosClick
      end
      object RBFecha: TRadioButton
        Tag = 1
        Left = 3
        Top = 24
        Width = 113
        Height = 17
        Caption = 'Saldo a la Fecha'
        TabOrder = 1
        OnClick = RBSaldosClick
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 57
  end
end
