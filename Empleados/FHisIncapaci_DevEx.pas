unit FHisIncapaci_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, Db,
  ZetaDBGrid, ExtCtrls, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, 
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid;

type
  THisIncapaci_DevEx = class(TBaseGridLectura_DevEx)
    IN_FEC_INI: TcxGridDBColumn;
    IN_DIAS: TcxGridDBColumn;
    IN_FEC_FIN: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    IN_NUMERO: TcxGridDBColumn;
    IN_COMENTA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
     procedure ConfigAgrupamiento;
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  HisIncapaci_DevEx: THisIncapaci_DevEx;

implementation

{$R *.DFM}

uses dRecursos, dTablas, dSistema, ZetaTipoEntidad,ZImprimeForma, ZetaCommonLists, ZetaCommonClasses ;

procedure THisIncapaci_DevEx.Connect;
begin
     dmTablas.cdsIncidencias.Conectar;
     dmSistema.cdsUsuarios.Conectar;

     with dmRecursos do
     begin
          cdsHisIncapaci.Conectar;
          DataSource.DataSet:= cdsHisIncapaci;
     end;
end;

procedure THisIncapaci_DevEx.Refresh;
begin
     dmRecursos.cdsHisIncapaci.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;


procedure THisIncapaci_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10133_Historial_incapacidades;
end;

procedure THisIncapaci_DevEx.Agregar;
begin
     dmRecursos.cdsHisIncapaci.Agregar;
end;

procedure THisIncapaci_DevEx.Borrar;
begin
     dmRecursos.cdsHisIncapaci.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisIncapaci_DevEx.Modificar;
begin
     dmRecursos.cdsHisIncapaci.Modificar;
end;

procedure THisIncapaci_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enIncapacidad, dmRecursos.cdsHisIncapaci );
end;

procedure THisIncapaci_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     IN_FEC_INI.Options.Grouping:= FALSE;
     TB_ELEMENT.Options.Grouping := TRUE;
     IN_DIAS.Options.Grouping:= FALSE;
     IN_FEC_FIN.Options.Grouping:= FALSE;
     IN_NUMERO.Options.Grouping:= FALSE;
     IN_COMENTA.Options.Grouping:= FALSE;
end;

procedure THisIncapaci_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  {***Banda Sumatoria***}
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  //CreaColumaAgrupadaSumatoria( IN_COMENTA.Name, skCount );
  inherited;
  //DevEx (by am):Configuracion Especial para agrupamiento
  ConfigAgrupamiento;

end;

end.
