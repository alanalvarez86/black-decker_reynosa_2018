inherited KardexCambio: TKardexCambio
  Left = 3106
  Top = 230
  Caption = 'Cambio de Salario'
  ClientHeight = 325
  ClientWidth = 478
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 289
    Width = 478
    DesignSize = (
      478
      36)
  end
  inherited PanelSuperior: TPanel
    Width = 478
  end
  inherited PageControl: TPageControl
    Width = 478
    Height = 208
    ActivePage = MovimientoIDSE
    inherited General: TTabSheet
      Caption = 'Salario'
      object CB_SALARIOlbl: TLabel
        Left = 64
        Top = 25
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Salario Diario:'
      end
      object CB_FAC_INTlbl: TLabel
        Left = 40
        Top = 46
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Factor Integraci'#243'n:'
      end
      object Label35: TLabel
        Left = 62
        Top = 68
        Width = 67
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pre-Integrado:'
      end
      object OPLBl: TLabel
        Left = 21
        Top = 90
        Width = 108
        Height = 13
        Caption = 'Promedio de Variables:'
      end
      object Label3: TLabel
        Left = 7
        Top = 46
        Width = 7
        Height = 13
        Caption = 'X'
      end
      object Label4: TLabel
        Left = 1
        Top = 68
        Width = 18
        Height = 13
        Caption = '( = )'
      end
      object Label6: TLabel
        Left = 1
        Top = 90
        Width = 18
        Height = 13
        Caption = '( + )'
      end
      object Label7: TLabel
        Left = 46
        Top = 113
        Width = 83
        Height = 13
        Caption = 'Gravado de Fijas:'
      end
      object Label8: TLabel
        Left = 1
        Top = 113
        Width = 18
        Height = 13
        Caption = '( + )'
      end
      object Label9: TLabel
        Left = 1
        Top = 136
        Width = 18
        Height = 13
        Caption = '( = )'
      end
      object Label10: TLabel
        Left = 46
        Top = 136
        Width = 83
        Height = 13
        Caption = 'Salario Integrado:'
      end
      object CB_FAC_INT: TZetaDBTextBox
        Left = 133
        Top = 44
        Width = 90
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_FAC_INT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_FAC_INT'
        DataSource = DataSource
        FormatFloat = '%14.4n'
        FormatCurrency = '%m'
      end
      object CB_PRE_INT: TZetaDBTextBox
        Left = 133
        Top = 66
        Width = 90
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_PRE_INT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_PRE_INT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_TOT_GRA: TZetaDBTextBox
        Left = 133
        Top = 111
        Width = 90
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_TOT_GRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_TOT_GRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_SAL_INT: TZetaDBTextBox
        Left = 133
        Top = 134
        Width = 90
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_SAL_INT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_SAL_INT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label12: TLabel
        Left = 262
        Top = 25
        Width = 83
        Height = 13
        Caption = 'Zona Geogr'#225'fica:'
      end
      object Label13: TLabel
        Left = 278
        Top = 48
        Width = 67
        Height = 13
        Caption = 'Aviso a IMSS:'
      end
      object Label15: TLabel
        Left = 253
        Top = 95
        Width = 92
        Height = 13
        Caption = 'Percepciones Fijas:'
      end
      object Label16: TLabel
        Left = 283
        Top = 116
        Width = 62
        Height = 13
        Caption = 'Salario Total:'
      end
      object CB_OTRAS_P: TZetaDBTextBox
        Left = 352
        Top = 93
        Width = 90
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_OTRAS_P'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_OTRAS_P'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_SAL_TOT: TZetaDBTextBox
        Left = 352
        Top = 114
        Width = 90
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_SAL_TOT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_SAL_TOT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_RANGO_SLbl: TLabel
        Left = 273
        Top = 73
        Width = 72
        Height = 13
        Caption = 'Rango Salarial:'
      end
      object Label5: TLabel
        Left = 267
        Top = 136
        Width = 79
        Height = 13
        Caption = 'Salario Semanal:'
      end
      object CB_SAL_SEM: TZetaTextBox
        Left = 352
        Top = 134
        Width = 90
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_SAL_SEM'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object BtnRecalculo: TZetaSpeedButton
        Left = 224
        Top = 42
        Width = 20
        Height = 20
        Hint = 'Forzar Recalculo de Salario Integrado'
        Flat = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00337000000000
          73333337777777773F333308888888880333337F3F3F3FFF7F33330808089998
          0333337F737377737F333308888888880333337F3F3F3F3F7F33330808080808
          0333337F737373737F333308888888880333337F3F3F3F3F7F33330808080808
          0333337F737373737F333308888888880333337F3F3F3F3F7F33330808080808
          0333337F737373737F333308888888880333337F3FFFFFFF7F33330800000008
          0333337F7777777F7F333308000E0E080333337F7FFFFF7F7F33330800000008
          0333337F777777737F333308888888880333337F333333337F33330888888888
          03333373FFFFFFFF733333700000000073333337777777773333}
        NumGlyphs = 2
        OnClick = BtnRecalculoClick
      end
      object Label17: TLabel
        Left = 20
        Top = 159
        Width = 109
        Height = 13
        Caption = 'Tabla de Prestaciones:'
        Enabled = False
      end
      object CB_SALARIO: TZetaDBNumero
        Left = 133
        Top = 21
        Width = 90
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        DataField = 'CB_SALARIO'
        DataSource = DataSource
      end
      object CB_PER_VAR: TZetaDBNumero
        Left = 133
        Top = 86
        Width = 90
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
        DataField = 'CB_PER_VAR'
        DataSource = DataSource
      end
      object CB_FECHA_2: TZetaDBFecha
        Left = 352
        Top = 43
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 4
        Text = '13/dic/97'
        Valor = 35777.000000000000000000
        DataField = 'CB_FECHA_2'
        DataSource = DataSource
      end
      object CB_ZONA_GE: TDBComboBox
        Left = 352
        Top = 21
        Width = 60
        Height = 21
        Style = csDropDownList
        DataField = 'CB_ZONA_GE'
        DataSource = DataSource
        ItemHeight = 13
        Items.Strings = (
          'A'
          'B'
          'C')
        TabOrder = 3
      end
      object CB_AUTOSAL: TDBCheckBox
        Left = 23
        Top = 1
        Width = 123
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Salario por Tabulador:'
        DataField = 'CB_AUTOSAL'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_AUTOSALClick
      end
      object CB_RANGO_S: TZetaDBNumero
        Left = 352
        Top = 68
        Width = 90
        Height = 21
        Mascara = mnTasa
        TabOrder = 5
        Text = '0.0 %'
        OnExit = CB_RANGO_SExit
        DataField = 'CB_RANGO_S'
        DataSource = DataSource
      end
      object CB_TABLASS: TZetaDBKeyLookup
        Left = 133
        Top = 155
        Width = 308
        Height = 21
        Enabled = False
        LookupDataset = dmCatalogos.cdsSSocial
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TABLASS'
        DataSource = DataSource
      end
    end
    object Otras: TTabSheet [1]
      Caption = 'Percepciones Fijas'
      object Label20: TLabel
        Left = 7
        Top = 6
        Width = 151
        Height = 13
        Caption = 'Percepciones Disponibles:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object ZetaSmartListsButton1: TZetaSmartListsButton
        Left = 207
        Top = 75
        Width = 26
        Height = 25
        Enabled = False
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000000080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          FFFF000000000000008080008080008080008080008080008080008080008080
          00808000808000808000808000000000FFFF00FFFF00FFFF0000000000000080
          8000808000808000808000000000000000000000000000000000000000000000
          FFFF00FFFF00FFFF00FFFF00FFFF00000000000000808000808000000000FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00000000000000000000000000000000000000000000000000000000
          FFFF00FFFF00FFFF00FFFF00FFFF000000000000008080008080008080008080
          00808000808000808000808000000000FFFF00FFFF00FFFF0000000000000080
          8000808000808000808000808000808000808000808000808000808000000000
          FFFF000000000000008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000000080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080}
        OnClick = AgregarBtnClick
        Tipo = bsEscoger
        SmartLists = ZetaSmartLists
      end
      object ZetaSmartListsButton2: TZetaSmartListsButton
        Left = 207
        Top = 100
        Width = 26
        Height = 25
        Enabled = False
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080000000000000008080008080008080008080008080008080008080008080
          00808000808000808000808000000000000000FFFF0000000080800080800080
          8000808000808000808000808000808000808000808000000000000000FFFF00
          FFFF00FFFF000000008080008080008080008080008080008080008080008080
          00000000000000FFFF00FFFF00FFFF00FFFF00FFFF0000000000000000000000
          0000000000000000000000000000000000FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000008080008080
          00000000000000FFFF00FFFF00FFFF00FFFF00FFFF0000000000000000000000
          0000000000000000000000808000808000808000808000000000000000FFFF00
          FFFF00FFFF000000008080008080008080008080008080008080008080008080
          00808000808000808000808000000000000000FFFF0000000080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080000000000000008080008080008080008080008080008080008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080}
        OnClick = AgregarBtnClick
        Tipo = bsRechazar
        SmartLists = ZetaSmartLists
      end
      object Label22: TLabel
        Left = 242
        Top = 6
        Width = 173
        Height = 13
        Caption = 'Percepciones  Seleccionadas:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object ZetaSmartListsButton3: TZetaSmartListsButton
        Left = 439
        Top = 75
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000008080008080
          0080800080800080800080800000000000000000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800000000000000000000000000000FF0000000000000000000000000080
          800080800080800080800080800080800080800000000000FF0000FF0000FF00
          00FF0000FF0000FF0000FF000000008080008080008080008080008080008080
          0080800080800000000000FF0000FF0000FF0000FF0000FF0000000080800080
          800080800080800080800080800080800080800080800000000000FF0000FF00
          00FF0000FF0000FF000000008080008080008080008080008080008080008080
          0080800080800080800000000000FF0000FF0000FF0000000080800080800080
          800080800080800080800080800080800080800080800080800000000000FF00
          00FF0000FF000000008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800080800080800080800080800000000080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          0000008080008080008080008080008080008080008080008080}
        Tipo = bsSubir
        SmartLists = ZetaSmartLists
      end
      object ZetaSmartListsButton4: TZetaSmartListsButton
        Left = 439
        Top = 100
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000008080008080
          0080800080800080800080800080800000000080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          0000008080008080008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800080800080800000000000FF0000FF0000FF0000000080800080800080
          800080800080800080800080800080800080800080800080800000000000FF00
          00FF0000FF000000008080008080008080008080008080008080008080008080
          0080800080800000000000FF0000FF0000FF0000FF0000FF0000000080800080
          800080800080800080800080800080800080800080800000000000FF0000FF00
          00FF0000FF0000FF000000008080008080008080008080008080008080008080
          0080800000000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000000080
          8000808000808000808000808000808000808000000000000000000000000000
          00FF000000000000000000000000008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          0000000000008080008080008080008080008080008080008080}
        Tipo = bsBajar
        SmartLists = ZetaSmartLists
      end
      object SBMas: TSpeedButton
        Left = 439
        Top = 21
        Width = 25
        Height = 25
        Hint = 'Consultar Detalle de Percepciones'
        Enabled = False
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333FF3333333333333C0C333333333333F777F3333333333CC0F0C3
          333333333777377F33333333C30F0F0C333333337F737377F333333C00FFF0F0
          C33333F7773337377F333CC0FFFFFF0F0C3337773F33337377F3C30F0FFFFFF0
          F0C37F7373F33337377F00FFF0FFFFFF0F0C7733373F333373770FFFFF0FFFFF
          F0F073F33373F333373730FFFFF0FFFFFF03373F33373F333F73330FFFFF0FFF
          00333373F33373FF77333330FFFFF000333333373F333777333333330FFF0333
          3333333373FF7333333333333000333333333333377733333333333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = SBMasClick
      end
      object LBDisponibles: TZetaSmartListBox
        Left = 7
        Top = 21
        Width = 190
        Height = 150
        ItemHeight = 13
        TabOrder = 0
      end
      object LBSeleccion: TZetaSmartListBox
        Left = 242
        Top = 21
        Width = 190
        Height = 150
        ItemHeight = 13
        TabOrder = 1
      end
    end
    inherited Notas: TTabSheet
      inherited CB_NOTA: TDBMemo
        Width = 470
        Height = 180
      end
    end
    object Salarios2: TTabSheet [3]
      Caption = 'Anteriores'
      object Label14: TLabel
        Left = 66
        Top = 18
        Width = 65
        Height = 13
        Caption = 'Salario Diario:'
      end
      object Label18: TLabel
        Left = 45
        Top = 40
        Width = 86
        Height = 13
        Caption = 'Fecha de Cambio:'
      end
      object Label19: TLabel
        Left = 48
        Top = 80
        Width = 83
        Height = 13
        Caption = 'Salario Integrado:'
      end
      object Label21: TLabel
        Left = 45
        Top = 104
        Width = 86
        Height = 13
        Caption = 'Fecha de Cambio:'
      end
      object CB_FEC_INT: TZetaDBTextBox
        Left = 148
        Top = 102
        Width = 90
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_FEC_INT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_FEC_INT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_OLD_INT: TZetaDBTextBox
        Left = 148
        Top = 78
        Width = 90
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_OLD_INT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_OLD_INT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_FEC_REV: TZetaDBTextBox
        Left = 148
        Top = 38
        Width = 90
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_FEC_REV'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_FEC_REV'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_OLD_SAL: TZetaDBTextBox
        Left = 148
        Top = 16
        Width = 90
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_OLD_SAL'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_OLD_SAL'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object MovimientoIDSE: TTabSheet
      Caption = 'Movimiento IDSE'
      ImageIndex = 3
      object GroupBox2: TGroupBox
        Left = 24
        Top = 24
        Width = 417
        Height = 89
        Caption = ' Validaci'#243'n IDSE '
        TabOrder = 0
        object lblLoteIDSE: TLabel
          Left = 46
          Top = 27
          Width = 79
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero de Lote:'
        end
        object lblFechaIDSE: TLabel
          Left = 15
          Top = 51
          Width = 110
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha de Transacci'#243'n:'
        end
        object CB_FEC_IDS: TZetaDBFecha
          Left = 132
          Top = 46
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '13/dic/97'
          Valor = 35777.000000000000000000
          DataField = 'CB_FEC_IDS'
          DataSource = DataSource
        end
        object CB_LOT_IDS: TDBEdit
          Left = 132
          Top = 23
          Width = 260
          Height = 21
          DataField = 'CB_LOT_IDS'
          DataSource = DataSource
          TabOrder = 0
        end
      end
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 478
    inherited ValorActivo2: TPanel
      Width = 152
      inherited textoValorActivo2: TLabel
        Width = 146
      end
    end
  end
  inherited PanelFecha: TPanel
    Width = 478
  end
  object ZetaSmartLists: TZetaSmartLists
    BorrarAlCopiar = True
    CopiarObjetos = True
    ListaDisponibles = LBDisponibles
    ListaEscogidos = LBSeleccion
    AlBajar = ZetaSmartListsAlModificar
    AlEscoger = ZetaSmartListsAlModificar
    AlRechazar = ZetaSmartListsAlRechazar
    AlSeleccionar = ZetaSmartListsAlSeleccionar
    AlSubir = ZetaSmartListsAlModificar
    Left = 128
    Top = 280
  end
end
