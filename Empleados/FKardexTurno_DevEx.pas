unit FKardexTurno_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ZetaFecha, ExtCtrls, StdCtrls, DBCtrls, ZetaDBTextBox, Mask, ComCtrls,
  Buttons, FKardexBase_DevEx, ZetaSmartLists, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls, cxContainer, cxEdit,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxTextEdit, cxMemo, cxDBEdit, cxPC, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TKardexTurno_DevEx = class(TKardexBase_DevEx)
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    HorarioLbl: TLabel;
    bbMostrarCalendario_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    //procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure bbMostrarCalendario_DevExClick(Sender: TObject);
  private
  protected
   procedure Connect;override;
  public
    { Public declarations }
  end;

var
  KardexTurno_DevEx: TKardexTurno_DevEx;

implementation

uses ZAccesosTress, ZetaCommonClasses, ZetaCommonLists, ZetaCommonTools,
     dCatalogos, dSistema, dRecursos;

{$R *.DFM}

procedure TKardexTurno_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10155_Cambio_turno;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX;
     CB_TURNO.LookupDataset := dmCatalogos.cdsTurnos;
     CB_TURNO.EditarSoloActivos := TRUE; //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigne en el DFM, Bug#15743
end;

procedure TKardexTurno_DevEx.Connect;
begin
     dmCatalogos.cdsTurnos.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsEditHisKardex.Conectar;
          DataSource.DataSet := cdsEditHisKardex;
     end;
end;

{procedure TKardexTurno_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Llave );
end; }

procedure TKardexTurno_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) or ( Field.FieldName = 'CB_TURNO' ) then
     begin
          with dmRecursos.cdsEditHisKardex do
               bbMostrarCalendario_DevEx.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
     end;
end;

procedure TKardexTurno_DevEx.bbMostrarCalendario_DevExClick(
  Sender: TObject);
begin
  inherited;
  dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Llave );
end;

end.






