unit FEditEmpOtros;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, StdCtrls, Mask, DBCtrls, ZetaDBTextBox, Db, ExtCtrls, Buttons,
  ZetaFecha, ZetaNumero, ZetaKeyCombo, ZetaKeyLookup, ZetaSmartLists,
  ComCtrls;

type
  TEditEmpOtros = class(TBaseEdicion)
    pcOtros: TPageControl;
    tsInfonavit: TTabSheet;
    tsAsistencia: TTabSheet;
    tsCuentas: TTabSheet;
    tsFonacot: TTabSheet;
    tsPPrimerEmpleo: TTabSheet;
    tsEvaluacion: TTabSheet;
    CB_INF_INILbl: TLabel;
    CB_INF_INI: TZetaDBFecha;
    CB_INF_ANTLbl: TLabel;
    CB_INF_ANT: TZetaDBFecha;
    CB_INFCREDLbl: TLabel;
    CB_INFCRED: TDBEdit;
    CB_INFTIPOlbl: TLabel;
    CB_INFTIPO: TZetaDBKeyCombo;
    CB_INFTASALbl: TLabel;
    CB_INFTASA: TZetaDBNumero;
    CB_INFDISM: TDBCheckBox;
    CB_INFACT: TDBCheckBox;
    LblDescuento: TLabel;
    CB_INFMANT: TDBCheckBox;
    CB_INF_OLDLbl: TLabel;
    zkcCB_INF_OLD: TZetaKeyCombo;
    lblPorcentaje: TLabel;
    LTipoCreden: TLabel;
    CB_CREDENC: TDBEdit;
    CB_CHECA: TDBCheckBox;
    Label1: TLabel;
    CB_ID_NUM: TDBEdit;
    lblNumeroBiometrico: TLabel;
    CB_ID_BIO: TZetaDBTextBox;
    btnAsignarBio: TSpeedButton;
    btnBorrarBio: TSpeedButton;
    lblCB_GP_COD: TLabel;
    CB_GP_COD: TZetaDBKeyLookup;
    LBanca: TLabel;
    CB_BAN_ELE: TDBEdit;
    lblTarjetasGasolina: TLabel;
    CB_CTA_GAS: TDBEdit;
    lblTarjetasDespensa: TLabel;
    CB_CTA_VAL: TDBEdit;
    BtnBAN_ELE: TSpeedButton;
    btnTarjetaGasolina: TSpeedButton;
    btnTarjetaDespensa: TSpeedButton;
    Label2: TLabel;
    CB_SUB_CTA: TDBEdit;
    lbNeto: TLabel;
    CB_NETO: TZetaDBNumero;
    Label3: TLabel;
    CB_FONACOT: TDBEdit;
    CB_EMPLEO: TDBCheckBox;
    LFechaEva: TLabel;
    CB_LAST_EV: TZetaDBFecha;
    LEvalua: TLabel;
    CB_EVALUA: TZetaDBNumero;
    LProxima: TLabel;
    CB_NEXT_EV: TZetaDBFecha;
    tsBrigada: TTabSheet;
    lblTipoBrigada: TLabel;
    lblNoPiso: TLabel;
    CB_BRG_TIP: TZetaDBKeyCombo;
    CB_BRG_NOP: TDBEdit;
    CB_BRG_ACT: TDBCheckBox;
    tsDatosMedicos: TTabSheet;
    lblTSangre: TLabel;
    lblAlerPadec: TLabel;
    CB_ALERGIA: TDBEdit;
    tsConfidencialidad: TTabSheet;
    CB_NIVEL0: TZetaDBKeyLookup;
    LblNivel0: TLabel;
    CB_TSANGRE: TDBComboBox;
    CB_BRG_JEF: TDBCheckBox;
    CB_BRG_ROL: TDBCheckBox;
    CB_BRG_CON: TDBCheckBox;
    CB_BRG_PRA: TDBCheckBox;
    lblRol: TLabel;
    lblConocimiento: TLabel;
    chkTieneHuella: TCheckBox;
    BtnEnrolar: TSpeedButton;
    btnBorrarHuellas: TSpeedButton;
    GroupBox1: TGroupBox;
    lBanco: TLabel;
    CB_BANCO: TZetaDBKeyLookup;
    tsTimbrado: TTabSheet;
    Label16: TLabel;
    ZetaDBKeyCombo1: TZetaDBKeyCombo;
    procedure CB_INFTIPOChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure BtnBAN_ELEClick(Sender: TObject);
    procedure CB_ID_NUMExit(Sender: TObject);
    procedure zkcCB_INF_OLDChange(Sender: TObject);
    procedure btnTarjetaDespensaClick(Sender: TObject);
    procedure btnTarjetaGasolinaClick(Sender: TObject);
    procedure btnBorrarBioClick(Sender: TObject);
    procedure btnAsignarBioClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rgRolClick(Sender: TObject);
    procedure rgConocimientoClick(Sender: TObject);
    procedure CB_BRG_ACTClick(Sender: TObject);
    procedure HabilitarBrigada(bandera: Boolean);
    procedure pcOtrosChange(Sender: TObject);
    procedure BtnEnrolarClick(Sender: TObject);
    procedure btnBorrarHuellasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    FConectandoTasa: Boolean;
    procedure HabilitaCredInfo;
    //procedure ClearInfonavit;
    procedure SetControlesNivel0;
    procedure HabilitaOtrosControles;
    procedure ControlesInfonavit( const lEnabled: Boolean );
    procedure AsignaValorTasa;
    procedure SetControlesNeto;
{$ifndef DOS_CAPAS}
    procedure RegistroBiometrico;
{$endif}
  protected
    procedure Connect; override;
    procedure ImprimirForma;override;
    procedure EscribirCambios; override;
    function CheckDerechos(const iDerecho: Integer): Boolean; override;
  public
  end;

const
     K_NINGUNO = 0;

var
  EditEmpOtros: TEditEmpOtros;


procedure ShowFormaEdicionOtros( iDerecho : integer );

implementation

uses dRecursos, dSistema, ZetaCommonLists, ZAccesosTress, ZetaCommonClasses,
     ZetaCommonTools, ZetaTipoEntidad,ZetaDialogo, ZImprimeForma, ZAccesosMgr, ZetaClientTools,
     ZGlobalTress, DCliente, DGlobal, FSistEditAccesos{$ifndef DOS_CAPAS}, FEnrolamiento{$endif},
  dTablas;

{$R *.DFM}

procedure ShowFormaEdicionOtros( iDerecho : integer );
begin
     if EditEmpOtros = nil then
          EditEmpOtros :=  TEditEmpOtros.Create( Application );

     EditEmpOtros.IndexDerechos := iDerecho;

     ZBaseEdicion.ShowFormaEdicion( EditEmpOtros, TEditEmpOtros );
end;



procedure TEditEmpOtros.FormCreate(Sender: TObject);
begin
     inherited;
     //IndexDerechos := iIndexDerechos;

     tsInfonavit.Tag :=  D_EMP_DATOS_OTROS_INFONAVIT;
     tsFonacot.Tag :=    D_EMP_DATOS_OTROS_FONACOT;
     tsPPrimerEmpleo.Tag :=  D_EMP_DATOS_OTROS_PRIMER_EMPLEO;
     tsAsistencia.Tag :=  D_EMP_DATOS_OTROS_ASISTENCIA;
     tsEvaluacion.Tag :=  D_EMP_DATOS_OTROS_EVALUACION;
     tsCuentas.Tag := D_EMP_DATOS_OTROS_CUENTAS;
     tsConfidencialidad.Tag :=  D_EMP_DATOS_OTROS_NIVEL0;
     tsDatosMedicos.Tag := D_EMP_DATOS_OTROS_MEDICOS;
     tsBrigada.Tag :=      D_EMP_DATOS_OTROS_BRIGADAS;
     tsTimbrado.Tag :=      D_EMP_DATOS_OTROS_TIMBRADO;

     FirstControl := CB_INFTIPO;
     HelpContext:= H10115_Otros_datos_empleado;
     CB_NIVEL0.LookupDataset := dmSistema.cdsNivel0;

     TipoValorActivo1 := stEmpleado;
     FConectandoTasa := FALSE;
     SetControlesNivel0;
     SetControlesNeto;
     zkcCB_INF_OLD.Style := csDropDown;
     ZetaClientTools.LlenaTasaAnterior( zkcCB_INF_OLD );
     
     // Verificar constante de AVENT
     // Protecci�n Civil 2013
     if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
     begin
          // GBInfonavit.Visible := False;
          tsInfonavit.TabVisible := False;
          // GBFonacot.Visible := False;
          tsFonacot.TabVisible := False;
          // GBEmpleo.Visible := False;
          tsPPrimerEmpleo.TabVisible := False;
          // GBAsistencia.Top := GBInfonavit.Top;
          // GBEvaluacion.Top := GBAsistencia.Top;
          // GBNomina.Top := GBAsistencia.Top + GBAsistencia.Height + 4;
          // Self.Height := Self.Height - GBInfonavit.Height;
     end;//if }
end;

procedure TEditEmpOtros.Connect;
begin
    {$ifndef DOS_CAPAS}
    with dmSistema do
    begin
         cdsListaGrupos.Conectar;
         CB_GP_COD.LookUpDataSet := cdsListaGrupos;
    end;
    {$endif}

     with dmTablas do
     begin
          cdsBancos.Conectar;
          CB_BANCO.LookupDataSet := cdsBancos;
     end;

     with dmRecursos do
     begin
          DataSource.DataSet:= cdsDatosEmpleado;
     end;
     HabilitaOtrosControles;
     HabilitaCredInfo;
     AsignaValorTasa;
end;

procedure TEditEmpOtros.FormShow(Sender: TObject);
var
   lAplicaTodas : boolean;
begin
     inherited;
     {$ifndef DOS_CAPAS}
     lblNumeroBiometrico.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_ASISTENCIA, K_DERECHO_ADICIONAL11 ); // SYNERGY
     lblNumeroBiometrico.Visible := True; // SYNERGY
     lblCB_GP_COD.Visible := True;
     chkTieneHuella.Visible := True;
     CB_ID_BIO.Visible := True; // SYNERGY
     CB_GP_COD.Visible := True; // SYNERGY
     
     btnAsignarBio.Visible := true;
     btnBorrarBio.Visible := true;

     CB_ID_BIO.DataField := 'CB_ID_BIO'; // SYNERGY
     CB_ID_BIO.Enabled := lblNumeroBiometrico.Enabled; // SYNERGY
     CB_GP_COD.DataField := 'CB_GP_COD'; // SYNERGY
     btnAsignarBio.Enabled := lblNumeroBiometrico.Enabled; // SYNERGY
     btnBorrarBio.Enabled := lblNumeroBiometrico.Enabled; // SYNERGY
     lblCB_GP_COD.Enabled := lblNumeroBiometrico.Enabled; // SYNERGY
     BtnEnrolar.Visible := True; // BIOMETRICO
     btnBorrarHuellas.Visible := True; // BIOMETRICO
     CB_GP_COD.Enabled := ( lblNumeroBiometrico.Enabled and ( CB_ID_BIO.Caption <> '0' ) ); // SYNERGY
     chkTieneHuella.Checked := dmSistema.TieneHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger,dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger);
     RegistroBiometrico; // BIOMETRICO
     {$endif}

    HabilitarBrigada(CB_BRG_ACT.Checked);

    // Mostrar  Tab Sheet con derecho a modificar (Consulta en el caso de tsInfonavit)
    lAplicaTodas := IndexDerechos <= D_EMP_DATOS_OTROS;

    tsInfonavit.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_INFONAVIT, K_DERECHO_CONSULTA ) and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_INFONAVIT)  ) ;
    tsFonacot.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_FONACOT, K_DERECHO_CAMBIO ) and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_FONACOT)  ) ;
    tsPPrimerEmpleo.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_PRIMER_EMPLEO, K_DERECHO_CAMBIO ) and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_PRIMER_EMPLEO)  ) ;
    tsAsistencia.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_ASISTENCIA, K_DERECHO_CAMBIO ) and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_ASISTENCIA)  ) ;
    tsEvaluacion.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_EVALUACION,  K_DERECHO_CAMBIO ) and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_EVALUACION)  ) ;
    tsCuentas.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_CAMBIO )and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_CUENTAS)  ) ;
    tsConfidencialidad.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_NIVEL0, K_DERECHO_CAMBIO ) and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_NIVEL0)  ) ;
    tsDatosMedicos.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_MEDICOS, K_DERECHO_CAMBIO )and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_MEDICOS)  ) ;
    tsBrigada.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_BRIGADAS, K_DERECHO_CAMBIO )and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_BRIGADAS)  ) ;
    tsTimbrado.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_TIMBRADO, K_DERECHO_CAMBIO )and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_TIMBRADO)  ) ;


    case  IndexDerechos of
           D_EMP_DATOS_OTROS_INFONAVIT : pcOtros.ActivePage := tsInfonavit;
           D_EMP_DATOS_OTROS_FONACOT : pcOtros.ActivePage := tsFonacot;
           D_EMP_DATOS_OTROS_PRIMER_EMPLEO : pcOtros.ActivePage := tsPPrimerEmpleo;
           D_EMP_DATOS_OTROS_ASISTENCIA : pcOtros.ActivePage := tsAsistencia;
           D_EMP_DATOS_OTROS_EVALUACION : pcOtros.ActivePage := tsEvaluacion;
           D_EMP_DATOS_OTROS_CUENTAS : pcOtros.ActivePage := tsCuentas;
           D_EMP_DATOS_OTROS_NIVEL0 : pcOtros.ActivePage := tsConfidencialidad;

    else
        begin
               if tsConfidencialidad.TabVisible then  pcOtros.ActivePage := tsConfidencialidad;
               if tsCuentas.TabVisible then  pcOtros.ActivePage := tsCuentas;
               if tsEvaluacion.TabVisible then  pcOtros.ActivePage := tsEvaluacion;
               if tsAsistencia.TabVisible then  pcOtros.ActivePage := tsAsistencia;
               if tsPPrimerEmpleo.TabVisible then  pcOtros.ActivePage := tsPPrimerEmpleo;
               if tsFonacot.TabVisible then  pcOtros.ActivePage := tsFonacot;
               IndexDerechos := pcOtros.ActivePage.Tag;
               if tsInfonavit.TabVisible then  pcOtros.ActivePage := tsInfonavit;
        end;

    end;



    if lAplicaTodas then
    begin
       // Verificar constante de AVENT
       if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
       begin
          if tsAsistencia.TabVisible then
             pcOtros.ActivePage := tsAsistencia;
       end
       else
       begin
          if tsInfonavit.TabVisible then
             pcOtros.ActivePage := tsInfonavit;
       end;
    end;



    end;

procedure TEditEmpOtros.CB_INFTIPOChange(Sender: TObject);
begin
     HabilitaCredInfo;
end;

procedure TEditEmpOtros.HabilitaCredInfo;
var
   lEnabled: Boolean;
begin
     // Si No Tiene Prestamo, deshabilitar Amortizacion
     // Si Tiene poner mascara correcta
    // lEnabled:= (( CB_INFTIPO.Valor ) <> Ord( tiNoTiene )) and (ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS, K_DERECHO_SIST_KARDEX ));
    lEnabled:= FALSE;
    { AP(20/May/2008): Sug 904 Historial de Infonavit, se cambi� la edici�n al historial de Cr�dito Infonavit }
     with CB_INFTASA do
     begin
          case CB_INFTIPO.Valor of
               Ord( tiNoTiene )   : Mascara := mnPesos;
               Ord( tiPorcentaje ): Mascara := mnTasa;
               Ord( tiCuotaFija  ): Mascara := mnPesos;
               Ord( tiVeces )     : Mascara := mnVecesSMGDF;
          end;
          Enabled:= lEnabled;
     end;
     CB_INFTASALbl.Enabled := lEnabled;
     CB_INFCRED.Enabled:= lEnabled;
     CB_INFCREDLbl.Enabled:= lEnabled;
     CB_INFMANT.Enabled:= lEnabled;
     CB_INF_INI.Enabled:= lEnabled;
     CB_INF_INILbl.Enabled:= lEnabled;
     CB_INF_ANT.Enabled:= lEnabled;
     CB_INF_ANTLbl.Enabled:= lEnabled;
     CB_INFDISM.Enabled:= lEnabled;
     CB_INFACT.Enabled:= lEnabled;     
     zkcCB_INF_OLD.Text := FloattoStr( dmRecursos.cdsDatosEmpleado.FieldByName( 'CB_INF_OLD' ).AsFloat );
     {zkcCB_INF_OLD.Enabled := (( CB_INFTIPO.Valor ) = Ord( tiPorcentaje ));}
     zkcCB_INF_OLD.Enabled:= lEnabled;
     CB_INF_OLDLbl.Enabled := zkcCB_INF_OLD.Enabled;
end;

procedure TEditEmpOtros.OKClick(Sender: TObject);
begin
     {V2013

     Protecci�n Civil}
     with dmRecursos.cdsDatosEmpleado do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;

     if ( CB_NIVEL0.Visible ) and ( strVacio( CB_NIVEL0.Llave ) )  and ( tsConfidencialidad.TabVisible )  then
     begin
          ZError('','Nivel de Confidencialidad no puede quedar Vacio',0);
          pcOtros.ActivePage := tsConfidencialidad;
          ActiveControl := CB_NIVEL0;
     end
     else if (CB_BRG_ACT.Checked ) and (CB_BRG_TIP.ItemIndex = 0) and ( tsBrigada.TabVisible ) then
     begin
          ZError('','Tipo de Brigada no puede ser Ninguna',0);
          pcOtros.ActivePage := tsBrigada;
          ActiveControl := CB_BRG_TIP;
     end
     else
     begin
          {if (( CB_INFTIPO.Valor ) = Ord( tiNoTiene )) then
             ClearInfonavit;}
          inherited;
{$ifdef DOS_CAPAS}
          Close; //Ya no se cerrara para que puedan enrolar su huella.
{$endif}
     end;
end;

procedure TEditEmpOtros.CancelarClick(Sender: TObject);
begin
     inherited;
     HabilitaCredInfo;
end;

procedure TEditEmpOtros.BtnBAN_ELEClick(Sender: TObject);
begin
     dmRecursos.ValidaBancaElectronica( Caption, Trim( CB_BAN_ELE.text ),'CB_BAN_ELE' );
end;



procedure TEditEmpOtros.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

procedure TEditEmpOtros.SetControlesNivel0;
begin
     CB_NIVEL0.Visible := dmSistema.HayNivel0;
     LblNivel0.Visible := CB_NIVEL0.Visible;
end;

procedure TEditEmpOtros.SetControlesNeto;
begin
     lbNeto.Visible := dmRecursos.PuedeVerNeto;
     CB_NETO.Visible := lbNeto.Visible;

     { V2013
     2013-03-18. AL.}
     {if NOT lbNeto.Visible then
     begin
          if CB_NIVEL0.Visible then
          begin
               LblNivel0.Top := lbNeto.Top;
               CB_NIVEL0.Top := CB_neto.Top;
          end;
     end; }
end;

procedure TEditEmpOtros.HabilitaOtrosControles;
begin
     CB_BAN_ELE.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_BANCA );
     lBanca.Enabled := CB_BAN_ELE.Enabled;
     BtnBAN_ELE.Enabled := CB_BAN_ELE.Enabled;
     lBanco.Enabled := CB_BAN_ELE.Enabled;
     CB_BANCO.Enabled :=CB_BAN_ELE.Enabled;

     if CB_NIVEL0.Visible then
     begin
          CB_NIVEL0.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_NIVEL0, K_DERECHO_CAMBIO );
          LblNivel0.Enabled := CB_NIVEL0.Enabled;
     end;
     ControlesInfonavit( FALSE );

     CB_CTA_VAL.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_ADICIONAL9 );
     lblTarjetasDespensa.Enabled := CB_CTA_VAL.Enabled;
     btnTarjetaDespensa.Enabled := CB_CTA_VAL.Enabled;

     CB_CTA_GAS.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_CONFIGURA );
     lblTarjetasGasolina.Enabled := CB_CTA_GAS.Enabled;
     btnTarjetaGasolina.Enabled := CB_CTA_GAS.Enabled;
     { AP(20/May/2008): Sug 904 Historial de Infonavit, se cambi� la edici�n al historial de Cr�dito Infonavit
     ControlesInfonavit( ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS, K_DERECHO_SIST_KARDEX ) );}
end;

procedure TEditEmpOtros.ControlesInfonavit( const lEnabled: Boolean );
begin
     CB_INFTIPO.Enabled := lEnabled;
     CB_INFTIPOlbl.Enabled := lEnabled;
     CB_INFCRED.Enabled := lEnabled;
     CB_INFCREDlbl.Enabled := lEnabled;
     CB_INFTASA.Enabled := lEnabled;
     CB_INFTASAlbl.Enabled := lEnabled;
     zkcCB_INF_OLD.Enabled := lEnabled;
     CB_INF_OLDlbl.Enabled := lEnabled;
     CB_INFMANT.Enabled := lEnabled;
     CB_INF_INI.Enabled := lEnabled;
     CB_INF_INIlbl.Enabled := lEnabled;
     CB_INFDISM.Enabled:= lEnabled;
end;

procedure TEditEmpOtros.CB_ID_NUMExit(Sender: TObject);
var
   sTarjeta: string;
begin
     sTarjeta := ZetaCommonTools.PreparaGafeteProximidad( CB_ID_NUM.Text );
     with dmRecursos.cdsDatosEmpleado do
     begin
          if ( sTarjeta <> FieldByName( 'CB_ID_NUM' ).AsString ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_ID_NUM' ).AsString := sTarjeta;
          end;
     end;
end;

procedure TEditEmpOtros.AsignaValorTasa;
var
     rTasa: TTasa;
begin
     FConectandoTasa := TRUE;
     try
     begin
          rTasa := dmRecursos.cdsDatosEmpleado.FieldByName('CB_INF_OLD').AsFloat;
          if( rTasa > 0 )then
              zkcCB_INF_OLD.Text := FloattoStr( rTasa )
          else
              zkcCB_INF_OLD. ItemIndex := K_NINGUNO;
     end
     finally
            FConectandoTasa := FALSE;
     end;
end;

procedure TEditEmpOtros.zkcCB_INF_OLDChange(Sender: TObject);
var
    rValor: TTasa;
begin
     if not( FConectandoTasa ) then
     begin
          with  zkcCB_INF_OLD do
          begin
               if( Text = ZetaCommonClasses.aArregloTasa[0] )then
                   rValor := K_NINGUNO
               else
               begin
                    try
                       rValor := StrtoFloat( Text );
                    except
                          //on E: Exception do //ErrorDialog(E.Message, E.HelpContext);
                          rValor := K_NINGUNO;
                    end;
               end;
               //with dmRecursos.cdsDatosEmpleado do
               with dmCliente.cdsEmpleado do
               begin
                    if not Editing then
                       Edit;
                    FieldByName( 'CB_INF_OLD' ).AsFloat:= rValor;
               end;
          end;
     end;
end;

procedure TEditEmpOtros.EscribirCambios;
begin
     with dmCliente.cdsEmpleado do
     begin
          if ( FieldByName( 'CB_INFTIPO' ).AsInteger = ord(tiPorcentaje) ) then
          begin
               if not( ZetaClientTools.VerificaValorTasa( FieldByName('CB_INF_OLD').AsFloat ) )then
               begin
                    if not( ZConfirm( 'Informaci�n', 'La Tasa Anterior es un valor diferente a los valores propuestos.' + CR_LF +
                                                    '�Desea continuar?',0,mbYes ) )then
                    begin
                         Abort;
                    end;
               end;
          end;
     end;
     inherited EscribirCambios;
end;

procedure TEditEmpOtros.btnTarjetaDespensaClick(Sender: TObject);
begin
     inherited;
     dmRecursos.ValidaBancaElectronica( Caption, Trim( CB_CTA_VAL.text ),'CB_CTA_VAL' );
end;

procedure TEditEmpOtros.btnTarjetaGasolinaClick(Sender: TObject);
begin
     inherited;
     dmRecursos.ValidaBancaElectronica( Caption, Trim( CB_CTA_GAS.text ),'CB_CTA_GAS' );
end;

procedure TEditEmpOtros.btnBorrarBioClick(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     with dmCliente.cdsEmpleado do
     begin
          if not( State in [ dsEdit, dsInsert ] )then
             Edit;
          FieldByName( 'CB_ID_BIO' ).AsInteger := 0;
          FieldByName( 'CB_GP_COD' ).AsString := VACIO;
     end;
     CB_GP_COD.Enabled := False;
     {$endif}
end;

procedure TEditEmpOtros.btnAsignarBioClick(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with dmCliente.cdsEmpleado do
     begin
          if not( State in [ dsEdit, dsInsert ] )then
             Edit;
          FieldByName( 'CB_ID_BIO' ).AsInteger := dmSistema.ObtenMaxIdBio;
          CB_GP_COD.Enabled := ( FieldByName( 'CB_ID_BIO' ).AsInteger <> 0 );
     end;
     {$endif}
end;

procedure TEditEmpOtros.rgRolClick(Sender: TObject);
begin
  inherited;
  OK.Enabled := true;
end;

procedure TEditEmpOtros.rgConocimientoClick(Sender: TObject);
begin
  inherited;
  OK.Enabled := TRUE;
end;

procedure TEditEmpOtros.CB_BRG_ACTClick(Sender: TObject);
begin
  inherited;   
  HabilitarBrigada(CB_BRG_ACT.Checked);

  { if not CB_BRG_ACT.Checked then
     CB_BRG_TIP.ItemIndex := 0; }
end;

procedure TEditEmpOtros.HabilitarBrigada(bandera: Boolean);
begin
  inherited;   
  CB_BRG_TIP.Enabled := bandera;
  CB_BRG_ROL.Enabled := bandera;
  CB_BRG_JEF.Enabled := bandera;
  CB_BRG_CON.Enabled := bandera;
  CB_BRG_PRA.Enabled := bandera;
  CB_BRG_NOP.Enabled := bandera;
end;

procedure TEditEmpOtros.pcOtrosChange(Sender: TObject);
begin
  inherited;
  Self.IndexDerechos :=  pcOtros.ActivePage.Tag;
end;

function TEditEmpOtros.CheckDerechos(const iDerecho: Integer): Boolean;
begin
      if iDerecho = K_DERECHO_CAMBIO then
         Result := TRUE
      else
          Result := ZAccesosMgr.CheckDerecho( IndexDerechos, iDerecho );
end;

{$ifndef DOS_CAPAS}
procedure TEditEmpOtros.RegistroBiometrico;
begin
     //Derechos de Acceso
     with dmRecursos.cdsDatosEmpleado do
     begin
          BtnEnrolar.Enabled := ( ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_ASISTENCIA, K_DERECHO_ADICIONAL12 ) and ( FieldByName( 'CB_GP_COD' ).AsString <> VACIO ) );
          btnBorrarHuellas.Enabled := ( ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_ASISTENCIA, K_DERECHO_ADICIONAL13 ) and ( FieldByName( 'CB_GP_COD' ).AsString <> VACIO ) );
     end;
end;
{$endif}

procedure TEditEmpOtros.BtnEnrolarClick(Sender: TObject);
begin
     inherited;
{$ifndef DOS_CAPAS}
     with TEnrolamiento.Create( Self ) do
     begin
          try
             with dmSistema do
             begin

                  IdBiometrico := dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger;

                  Empleado := dmCliente.cdsEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger;
                  Invitador := 0;

                  ShowModal;
                  chkTieneHuella.Checked := TieneHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger, dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger );


             end;
          finally
                 Free;
          end;
     end;
{$endif}
end;

procedure TEditEmpOtros.btnBorrarHuellasClick(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   sHeader : String;
   iEmpleado : Integer;
{$endif}
begin
     inherited;
{$ifndef DOS_CAPAS}
     iEmpleado := dmCliente.cdsEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger;
       sHeader := 'Eliminaci�n de Huellas';

      with dmSistema do
      begin
           if ( ZConfirm( sHeader, 'Se eliminar�n todas las huellas del empleado.' + CR_LF + '�Desea continuar?',0,mbNo ) ) then
           begin
                EliminaHuella( iEmpleado, 0 );
                ZInformation(sHeader, 'Se eliminaron las huellas del empleado ' + IntToStr( iEmpleado ) + '.', 0);
           end;

           //Borra archivo Local
           DeleteFile( LocalBioDbPath );

           chkTieneHuella.Checked := TieneHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger,dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger);
      end;
{$endif}
end;

procedure TEditEmpOtros.DataSourceDataChange(Sender: TObject; Field: TField);
{$ifndef DOS_CAPAS}
var
   lBiometrico : Boolean;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with dmRecursos.cdsDatosEmpleado do
     begin
          lBiometrico := ( ( State = dsBrowse ) and ( FieldByName( 'CB_GP_COD' ).AsString <> VACIO ) );
          BtnEnrolar.Enabled := ( ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_ASISTENCIA, K_DERECHO_ADICIONAL12 ) and lBiometrico );
          btnBorrarHuellas.Enabled := ( ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_ASISTENCIA, K_DERECHO_ADICIONAL13 ) and lBiometrico );
     end;
     {$endif}
end;

end.
