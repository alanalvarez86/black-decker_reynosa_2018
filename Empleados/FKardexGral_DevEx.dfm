inherited KardexGeneral_DevEx: TKardexGeneral_DevEx
  Caption = 'Kardex General'
  ClientWidth = 473
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 473
    inherited OK_DevEx: TcxButton
      Left = 309
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 388
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 473
    inherited ValorActivo2: TPanel
      Width = 147
      inherited textoValorActivo2: TLabel
        Width = 141
      end
    end
  end
  inherited PageControl_DevEx: TcxPageControl
    Width = 473
    ClientRectRight = 471
    inherited General_DevEx: TcxTabSheet
      object MontoLbl: TLabel
        Left = 44
        Top = 42
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto:'
      end
      object CB_TIPOlbl: TLabel
        Left = 53
        Top = 18
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object CB_MONTO: TZetaDBNumero
        Left = 80
        Top = 40
        Width = 115
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        DataField = 'CB_MONTO'
        DataSource = DataSource
      end
      object CB_TIPO: TZetaDBKeyLookup_DevEx
        Left = 80
        Top = 14
        Width = 353
        Height = 21
        Filtro = 
          '( TB_SISTEMA = '#39'N'#39' ) OR ( TB_CODIGO = '#39'TASINF'#39' ) OR ( TB_CODIGO ' +
          '= '#39'EVENTO'#39' )'
        LookupDataset = dmTablas.cdsMovKardex
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TIPO'
        DataSource = DataSource
      end
    end
    inherited Notas_DevEx: TcxTabSheet
      inherited CB_NOTA: TcxDBMemo
        Width = 469
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Top = 224
  end
  inherited PanelFecha: TPanel
    Width = 473
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
