inherited EditHisPrestamos: TEditHisPrestamos
  Left = 320
  Top = 193
  Caption = 'Pr'#233'stamos'
  ClientHeight = 513
  ClientWidth = 477
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 477
    Width = 477
    inherited OK: TBitBtn
      Left = 315
    end
    inherited Cancelar: TBitBtn
      Left = 394
    end
  end
  inherited PanelSuperior: TPanel
    Width = 477
  end
  inherited PanelIdentifica: TPanel
    Width = 477
    inherited ValorActivo2: TPanel
      Width = 151
    end
  end
  inherited Panel1: TPanel
    Width = 477
    Height = 214
    object AH_TIPOLbl: TLabel
      Left = 59
      Top = 12
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object Label1: TLabel
      Left = 7
      Top = 60
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha de Inicio:'
    end
    object Label2: TLabel
      Left = 43
      Top = 105
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'F'#243'rmula:'
    end
    object Label7: TLabel
      Left = 50
      Top = 83
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status:'
    end
    object Label8: TLabel
      Left = 28
      Top = 36
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Referencia:'
    end
    object SBCO_FORMULA: TSpeedButton
      Left = 432
      Top = 103
      Width = 25
      Height = 25
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      OnClick = SBCO_FORMULAClick
    end
    object US_DESCRIP: TZetaDBTextBox
      Left = 292
      Top = 34
      Width = 137
      Height = 17
      AutoSize = False
      Caption = 'US_DESCRIP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_DESCRIP'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label11: TLabel
      Left = 244
      Top = 36
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Modific'#243':'
    end
    object Label12: TLabel
      Left = 28
      Top = 157
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Subcuenta:'
    end
    object PR_TIPO_TXT: TZetaDBTextBox
      Left = 91
      Top = 11
      Width = 60
      Height = 17
      AutoSize = False
      Caption = 'PR_TIPO_TXT'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'PR_TIPO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object PR_TIPO_DESC: TZetaTextBox
      Left = 154
      Top = 11
      Width = 275
      Height = 17
      AutoSize = False
      Caption = 'PR_TIPO_DESC'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label18: TLabel
      Left = 9
      Top = 183
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = 'Observaciones:'
    end
    object PR_FORMULA: TDBMemo
      Left = 91
      Top = 103
      Width = 339
      Height = 49
      DataField = 'PR_FORMULA'
      DataSource = DataSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 255
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 4
    end
    object PR_FECHA: TZetaDBFecha
      Left = 91
      Top = 55
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 2
      Text = '17/dic/97'
      Valor = 35781.000000000000000000
      DataField = 'PR_FECHA'
      DataSource = DataSource
    end
    object PR_STATUS: TZetaDBKeyCombo
      Left = 91
      Top = 79
      Width = 145
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 3
      ListaFija = lfStatusPrestamo
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'PR_STATUS'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object PR_REFEREN: TZetaDBEdit
      Left = 91
      Top = 32
      Width = 60
      Height = 21
      TabOrder = 1
      DataField = 'PR_REFEREN'
      DataSource = DataSource
    end
    object PR_SUB_CTA: TZetaDBEdit
      Left = 91
      Top = 155
      Width = 211
      Height = 21
      TabOrder = 5
      Text = 'PR_SUB_CTA'
      DataField = 'PR_SUB_CTA'
      DataSource = DataSource
    end
    object PR_TIPO: TZetaDBKeyLookup
      Left = 91
      Top = 8
      Width = 340
      Height = 21
      LookupDataset = dmTablas.cdsTPresta
      Opcional = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'PR_TIPO'
      DataSource = DataSource
    end
    object PR_OBSERVA: TZetaDBEdit
      Left = 91
      Top = 179
      Width = 339
      Height = 21
      TabOrder = 6
      DataField = 'PR_OBSERVA'
      DataSource = DataSource
    end
  end
  inherited PageControl: TPageControl
    Top = 265
    Width = 477
    Height = 212
    OnChange = PageControlChange
    inherited Datos: TTabSheet
      Caption = 'Resumen'
      object DedLbl: TLabel
        Left = 182
        Top = 156
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = '# de Abonos:'
      end
      object PR_NUMERO: TZetaDBTextBox
        Left = 253
        Top = 154
        Width = 120
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'PR_NUMERO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PR_NUMERO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object GBTotales: TGroupBox
        Left = 58
        Top = 8
        Width = 353
        Height = 138
        Caption = 'Totales '
        TabOrder = 0
        object Label3: TLabel
          Left = 70
          Top = 36
          Width = 114
          Height = 13
          Alignment = taRightJustify
          Caption = 'Abonos a'#241'os anteriores:'
        end
        object Label4: TLabel
          Left = 117
          Top = 74
          Width = 67
          Height = 13
          Alignment = taRightJustify
          Caption = 'Otros Abonos:'
        end
        object PR_CARGOS: TZetaDBTextBox
          Left = 194
          Top = 90
          Width = 120
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PR_CARGOS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PR_CARGOS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label5: TLabel
          Left = 120
          Top = 92
          Width = 64
          Height = 13
          Alignment = taRightJustify
          Caption = 'Otros Cargos:'
        end
        object PR_ABONOS: TZetaDBTextBox
          Left = 194
          Top = 72
          Width = 120
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PR_ABONOS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PR_ABONOS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label6: TLabel
          Left = 94
          Top = 56
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = 'Abonado este a'#241'o:'
        end
        object PR_TOTAL: TZetaDBTextBox
          Left = 194
          Top = 54
          Width = 120
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PR_TOTAL'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PR_TOTAL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LSigno1: TLabel
          Left = 20
          Top = 74
          Width = 21
          Height = 13
          Caption = '( -- ) '
        end
        object LSigno2: TLabel
          Left = 20
          Top = 56
          Width = 21
          Height = 13
          Caption = '( -- ) '
        end
        object LSigno3: TLabel
          Left = 20
          Top = 92
          Width = 21
          Height = 13
          Caption = '( + ) '
        end
        object LSigno4: TLabel
          Left = 20
          Top = 110
          Width = 21
          Height = 13
          Caption = '( = ) '
        end
        object SaldoLbl: TLabel
          Left = 121
          Top = 110
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Saldo Actual:'
        end
        object PR_SALDO: TZetaDBTextBox
          Left = 194
          Top = 108
          Width = 120
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PR_SALDO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PR_SALDO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label9: TLabel
          Left = 106
          Top = 14
          Width = 78
          Height = 13
          Alignment = taRightJustify
          Caption = 'Monto Prestado:'
        end
        object Label10: TLabel
          Left = 20
          Top = 37
          Width = 21
          Height = 13
          Caption = '( -- ) '
        end
        object PR_SALDO_I: TZetaDBNumero
          Left = 194
          Top = 32
          Width = 120
          Height = 21
          Mascara = mnPesos
          TabOrder = 1
          Text = '0.00'
          DataField = 'PR_SALDO_I'
          DataSource = DataSource
        end
        object PR_MONTO: TZetaDBNumero
          Left = 194
          Top = 10
          Width = 120
          Height = 21
          Mascara = mnPesos
          TabOrder = 0
          Text = '0.00'
          DataField = 'PR_MONTO'
          DataSource = DataSource
        end
      end
    end
    inherited Tabla: TTabSheet
      Caption = 'Cargos y Abonos'
      inherited GridRenglones: TZetaDBGrid
        Width = 469
        Height = 155
        OnColExit = GridRenglonesColExit
        OnDrawColumnCell = GridRenglonesDrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'CR_FECHA'
            Title.Caption = 'Fecha'
            Width = 100
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'CR_CARGO'
            Title.Alignment = taRightJustify
            Title.Caption = 'Cargo'
            Width = 80
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'CR_ABONO'
            Title.Alignment = taRightJustify
            Title.Caption = 'Abono'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_OBSERVA'
            Title.Caption = 'Observaciones'
            Width = 166
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 469
      end
      object ZFecha: TZetaDBFecha
        Left = 136
        Top = 104
        Width = 110
        Height = 22
        Cursor = crArrow
        TabStop = False
        TabOrder = 2
        Text = '14/oct/99'
        Valor = 36447.000000000000000000
        Visible = False
        DataField = 'CR_FECHA'
        DataSource = dsRenglon
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Intereses'
      ImageIndex = 2
      object GroupBox1: TGroupBox
        Left = 58
        Top = 3
        Width = 353
        Height = 169
        TabOrder = 0
        object Label13: TLabel
          Left = 68
          Top = 36
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa de inter'#233's:'
        end
        object Label14: TLabel
          Left = 98
          Top = 80
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Intereses:'
        end
        object Label15: TLabel
          Left = 70
          Top = 124
          Width = 74
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cu'#225'ntos pagos:'
        end
        object Label16: TLabel
          Left = 98
          Top = 58
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Duraci'#243'n:'
        end
        object Label20: TLabel
          Left = 89
          Top = 146
          Width = 55
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cada pago:'
        end
        object Label21: TLabel
          Left = 64
          Top = 14
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Monto solicitado:'
        end
        object Label17: TLabel
          Left = 78
          Top = 102
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total a pagar:'
        end
        object PR_TASA: TZetaDBNumero
          Left = 147
          Top = 32
          Width = 120
          Height = 21
          Mascara = mnTasa
          TabOrder = 1
          Text = '0.0 %'
          DataField = 'PR_TASA'
          DataSource = DataSource
        end
        object PR_MONTO_S: TZetaDBNumero
          Left = 147
          Top = 10
          Width = 120
          Height = 21
          Mascara = mnPesos
          TabOrder = 0
          Text = '0.00'
          DataField = 'PR_MONTO_S'
          DataSource = DataSource
        end
        object PR_MESES: TZetaDBNumero
          Left = 147
          Top = 54
          Width = 120
          Height = 21
          Mascara = mnDias
          TabOrder = 2
          Text = '0'
          DataField = 'PR_MESES'
          DataSource = DataSource
        end
        object PR_INTERES: TZetaDBNumero
          Left = 147
          Top = 76
          Width = 120
          Height = 21
          Mascara = mnPesos
          TabOrder = 3
          Text = '0.00'
          DataField = 'PR_INTERES'
          DataSource = DataSource
        end
        object PR_PAGOS: TZetaDBNumero
          Left = 147
          Top = 120
          Width = 120
          Height = 21
          Mascara = mnDias
          TabOrder = 5
          Text = '0'
          DataField = 'PR_PAGOS'
          DataSource = DataSource
        end
        object PR_PAG_PER: TZetaDBNumero
          Left = 147
          Top = 142
          Width = 120
          Height = 21
          Mascara = mnPesos
          TabOrder = 6
          Text = '0.00'
          DataField = 'PR_PAG_PER'
          DataSource = DataSource
        end
        object PR_MONTO_INT: TZetaDBNumero
          Left = 147
          Top = 98
          Width = 120
          Height = 21
          Mascara = mnPesos
          TabOrder = 4
          Text = '0.00'
          DataField = 'PR_MONTO'
          DataSource = DataSource
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 329
  end
  inherited dsRenglon: TDataSource
    OnStateChange = dsRenglonStateChange
    Left = 389
    Top = 0
  end
end
