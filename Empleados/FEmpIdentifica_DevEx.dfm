inherited EmpIdentifica_DevEx: TEmpIdentifica_DevEx
  Left = 298
  Top = 355
  Caption = 'Identificaci'#243'n'
  ClientHeight = 302
  ClientWidth = 499
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LCB_CODIGO: TLabel [0]
    Left = 56
    Top = 7
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
    Visible = False
  end
  object LCB_APE_PAT: TLabel [1]
    Left = 16
    Top = 27
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Apellido Paterno:'
  end
  object LCB_APE_MAT: TLabel [2]
    Left = 14
    Top = 47
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Apellido Materno:'
  end
  object LCB_NOMBRES: TLabel [3]
    Left = 45
    Top = 67
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre(s):'
  end
  object LCB_FEC_NAC: TLabel [4]
    Left = 7
    Top = 107
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha Nacimiento:'
  end
  object LRFC: TLabel [5]
    Left = 63
    Top = 166
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'R.F.C.:'
  end
  object LCB_SEGSOC: TLabel [6]
    Left = 17
    Top = 186
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = '# Seguro Social:'
  end
  object LCB_CURP: TLabel [7]
    Left = 51
    Top = 226
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'C.U.R.P.:'
  end
  object CB_CODIGO: TZetaDBTextBox [8]
    Left = 104
    Top = 5
    Width = 65
    Height = 17
    AutoSize = False
    Caption = 'CB_CODIGO'
    ShowAccelChar = False
    Visible = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_APE_PAT: TZetaDBTextBox [9]
    Left = 104
    Top = 25
    Width = 190
    Height = 17
    AutoSize = False
    Caption = 'CB_APE_PAT'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_APE_PAT'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_APE_MAT: TZetaDBTextBox [10]
    Left = 104
    Top = 45
    Width = 190
    Height = 17
    AutoSize = False
    Caption = 'CB_APE_MAT'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_APE_MAT'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_CURP: TZetaDBTextBox [11]
    Left = 105
    Top = 224
    Width = 190
    Height = 17
    AutoSize = False
    Caption = 'CB_CURP'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    FormatSpecifier = 'LLLL-999999-L-LL-LLL-9-9;0'
    DataField = 'CB_CURP'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_FEC_NAC: TZetaDBTextBox [12]
    Left = 104
    Top = 105
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_FEC_NAC'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_FEC_NAC'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object LEdad: TLabel [13]
    Left = 68
    Top = 146
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'Edad:'
  end
  object lblSexo: TLabel [14]
    Left = 69
    Top = 87
    Width = 27
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sexo:'
  end
  object CB_NOMBRES: TZetaDBTextBox [15]
    Left = 104
    Top = 65
    Width = 190
    Height = 17
    AutoSize = False
    Caption = 'CB_NOMBRES'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NOMBRES'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object ZEdad: TZetaTextBox [16]
    Left = 105
    Top = 144
    Width = 145
    Height = 17
    AutoSize = False
    Caption = 'ZEdad'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object CB_RFC: TZetaDBTextBox [17]
    Left = 105
    Top = 164
    Width = 108
    Height = 17
    AutoSize = False
    Caption = 'CB_RFC'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    FormatSpecifier = 'LLLL-999999-AAA;0'
    DataField = 'CB_RFC'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_SEG_SOC: TZetaDBTextBox [18]
    Left = 105
    Top = 184
    Width = 108
    Height = 17
    AutoSize = False
    Caption = 'CB_SEG_SOC'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    FormatSpecifier = '99-99-99-9999-9;0'
    DataField = 'CB_SEGSOC'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object ZSexo: TZetaTextBox [19]
    Left = 104
    Top = 85
    Width = 190
    Height = 17
    AutoSize = False
    Caption = 'ZSexo'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object LCB_CLINICA: TLabel [20]
    Left = 60
    Top = 206
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cl'#237'nica:'
  end
  object CB_CLINICA: TZetaDBTextBox [21]
    Left = 105
    Top = 204
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_CLINICA'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_CLINICA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object LCANDIDA: TLabel [22]
    Left = 19
    Top = 246
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = '# de Solicitante:'
  end
  object CB_CANDIDA: TZetaDBTextBox [23]
    Left = 105
    Top = 244
    Width = 108
    Height = 17
    AutoSize = False
    Caption = 'CB_CANDIDA'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_CANDIDA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object lblEntidad: TLabel [24]
    Left = 1
    Top = 127
    Width = 95
    Height = 13
    Alignment = taRightJustify
    Caption = 'Entidad Nacimiento:'
  end
  object CB_ENT_NAC: TZetaDBTextBox [25]
    Left = 104
    Top = 125
    Width = 190
    Height = 17
    AutoSize = False
    Caption = 'CB_ENT_NAC'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object FotoSwitch: TcxButton [26]
    Left = 307
    Top = 24
    Width = 100
    Height = 26
    Hint = 'Mostrar Foto'
    Caption = 'Mostrar Foto'
    OptionsImage.Glyph.Data = {
      36090000424D3609000000000000360000002800000018000000180000000100
      20000000000000090000000000000000000000000000000000005A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFA2C2FFFFC1D6FFFFC1D6FFFFC1D6FFFFC1D6FFFFC1D6FFFFBED4
      FFFF6A9CFFFF679BFFFFA5C4FFFFC1D6FFFFAFCAFFFF76A5FFFF6297FFFFB9D1
      FFFFC1D6FFFFC1D6FFFFADC9FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA8C5
      FFFF7EAAFFFFEFF5FFFFB9D1FFFF98BBFFFFAFCAFFFFEFF5FFFF98BBFFFF8EB4
      FFFFFCFDFFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFF5FFFF6297
      FFFFEDF3FFFF83ADFFFF5A92FFFF5A92FFFF5A92FFFF6FA0FFFFF2F6FFFF74A3
      FFFFD0E0FFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6D9FFFF8BB2
      FFFFD3E2FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFFB9D1FFFFA5C4
      FFFFA8C5FFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADC9FFFF9BBD
      FFFFC1D6FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFFA0C0FFFFB9D1
      FFFF8BB2FFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6D9FFFF7CA8
      FFFFE5EEFFFF5D94FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFFC9DBFFFF9BBD
      FFFFA5C4FFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF1FFFF5F95
      FFFFCEDFFFFFB1CCFFFF5D94FFFF5A92FFFF5A92FFFF98BBFFFFE5EEFFFF5F95
      FFFFD6E4FFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB4CD
      FFFF6A9CFFFFCEDFFFFFEDF3FFFFCBDDFFFFE3ECFFFFE0EAFFFF74A3FFFF98BB
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFD
      FFFFB9D1FFFF6297FFFF76A5FFFF90B6FFFF79A7FFFF5F95FFFFA8C5FFFFFAFC
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFF2F6FFFFC9DBFFFFADC9FFFFC3D8FFFFEDF3FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF98BBFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFD3E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFE3ECFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF90B6FFFF98BBFFFF98BBFFFF6A9CFFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF96B9FFFFC1D6FFFFB1CCFFFF5D94FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF}
    OptionsImage.Margin = 1
    ParentShowHint = False
    ShowHint = True
    SpeedButtonOptions.GroupIndex = 1
    SpeedButtonOptions.AllowAllUp = True
    TabOrder = 2
    OnClick = FotoSwitchClick
  end
  inherited PanelIdentifica: TPanel
    Width = 499
    ExplicitWidth = 499
    inherited Slider: TSplitter
      Left = 352
      ExplicitLeft = 352
    end
    inherited ValorActivo1: TPanel
      Width = 336
      ExplicitWidth = 336
    end
    inherited ValorActivo2: TPanel
      Left = 355
      Width = 144
      ExplicitLeft = 355
      ExplicitWidth = 144
      inherited textoValorActivo2: TLabel
        Left = 139
        ExplicitLeft = 139
      end
    end
  end
  object GBBaja: TGroupBox [28]
    Left = 0
    Top = 263
    Width = 495
    Height = 51
    Caption = 'Datos de Baja'
    TabOrder = 1
    Visible = False
    object LBaja: TLabel
      Left = 26
      Top = 13
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha Baja:'
    end
    object CB_FEC_BAJ: TZetaDBTextBox
      Left = 89
      Top = 11
      Width = 80
      Height = 17
      AutoSize = False
      Caption = 'CB_FEC_BAJ'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_FEC_BAJ'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LBajaIMSS: TLabel
      Left = 5
      Top = 32
      Width = 78
      Height = 13
      Alignment = taRightJustify
      Caption = 'Baja Ante IMSS:'
    end
    object CB_FEC_BSS: TZetaDBTextBox
      Left = 89
      Top = 30
      Width = 80
      Height = 17
      AutoSize = False
      Caption = 'CB_FEC_BSS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_FEC_BSS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label3: TLabel
      Left = 195
      Top = 14
      Width = 59
      Height = 13
      Caption = 'Motivo Baja:'
    end
    object Label4: TLabel
      Left = 183
      Top = 32
      Width = 71
      Height = 13
      Caption = 'Ultima N'#243'mina:'
    end
    object ZNomina: TZetaTextBox
      Left = 257
      Top = 30
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'ZNomina'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object ZMotivoBaja: TZetaTextBox
      Left = 257
      Top = 12
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'ZMotivoBaja'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  object FOTO: TImageEnView [29]
    Left = 307
    Top = 62
    Width = 186
    Height = 155
    Cursor = crDefault
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    ParentCustomHint = False
    Background = clBtnFace
    LegacyBitmap = False
    AutoFit = True
    AutoStretch = True
    EnableInteractionHints = True
    PlayLoop = False
    ParentShowHint = False
    ShowHint = False
    Visible = False
    TabOrder = 3
    TabStop = True
  end
  inherited DataSource: TDataSource
    Left = 436
    Top = 25
  end
  object dsImagen: TDataSource
    Left = 466
    Top = 25
  end
end
