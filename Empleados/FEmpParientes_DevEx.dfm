inherited EmpParientes_DevEx: TEmpParientes_DevEx
  Left = 663
  Top = 256
  Caption = 'Parientes'
  ClientWidth = 505
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 505
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 179
      inherited textoValorActivo2: TLabel
        Width = 173
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 505
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object PA_RELACIO: TcxGridDBColumn
        Caption = 'Relaci'#243'n'
        DataBinding.FieldName = 'PA_RELACIO'
        MinWidth = 80
        Width = 102
      end
      object PA_FOLIO: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'PA_FOLIO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 32
        Width = 32
      end
      object PA_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PA_NOMBRE'
        MinWidth = 80
        Width = 231
      end
      object PA_SEXO: TcxGridDBColumn
        Caption = 'Sexo'
        DataBinding.FieldName = 'PA_SEXO'
        MinWidth = 50
        Width = 50
      end
      object PA_FEC_NAC: TcxGridDBColumn
        Caption = 'Edad'
        DataBinding.FieldName = 'PA_FEC_NAC'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 50
        Width = 50
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end