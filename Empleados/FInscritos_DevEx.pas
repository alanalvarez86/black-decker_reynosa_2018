unit FInscritos_DevEx;

interface

uses
  Windows, Messages, SysUtils,
  {$ifndef VER130}Variants,{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, DB, ZetaDBTextBox, StdCtrls, Grids, DBGrids,
  ZetaDBGrid, DBCtrls, ZetaSmartLists, Buttons, ExtCtrls,
  ZBaseSesion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TInscritos_DevEx = class(TBaseSesion_DevEx)
    lblPeriodo: TLabel;
    ztbPeriodo: TZetaTextBox;
    lblCupo: TLabel;
    SE_CUPO: TZetaDBTextBox;
    Label6: TLabel;
    ztbInscritos: TZetaTextBox;
    btnInscripciones: TcxButton;
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject); 
    procedure btnInscripcionesClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure BuscaEmpleado;
    procedure Buscar;override;
    procedure EscribirCambios; override;
    procedure RefrescaInscritos; virtual;
  public
    { Public declarations }
  end;

var
  Inscritos_DevEx: TInscritos_DevEx;

implementation

uses DRecursos,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaBuscaEmpleado_DevEx,
     FEmpInscritosGridSelect_DevEx;

{$R *.dfm}
procedure TInscritos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_INSCRITOS;
end;

procedure TInscritos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmRecursos.cdsSesion do
     begin
          if( FieldbyName('SE_FEC_INI').AsDateTime = FieldbyName('SE_FEC_FIN').AsDateTime )then
              ztbPeriodo.Caption := FechaCorta( FieldbyName('SE_FEC_INI').AsDateTime )
          else
              ztbPeriodo.Caption := Format( 'del  %s  al  %s ', [ FechaCorta( FieldbyName('SE_FEC_INI').AsDateTime ),
                                                                  FechaCorta( FieldbyName('SE_FEC_FIN').AsDateTime ) ] );
     end;
end;

procedure TInscritos_DevEx.Connect;
begin
     inherited Connect;
     with dmRecursos do
     begin
          RefrescaInscritos;
          DataSource.DataSet := cdsInscritos;
     end;
end;

procedure TInscritos_DevEx.Buscar;
begin
     with ZetaDBGrid.SelectedField do
     begin
          if ( FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado
     end;
end;

procedure TInscritos_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmRecursos.cdsInscritos do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_CODIGO' ).AsString := sKey;
          end;
     end;
end;

procedure TInscritos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
       if( Field = nil )then
         ztbInscritos.Caption := InttoStr( dmRecursos.cdsInscritos.RecordCount );
end;

procedure TInscritos_DevEx.EscribirCambios;
begin
     with dmRecursos do
     begin
          if( cdsSesion.FieldByName('SE_CUPO').AsInteger < cdsInscritos.RecordCount )then
              ZetaDialogo.ZWarning( 'Informaci�n', 'El n�mero de empleados inscritos es mayor que el cupo del grupo', 0, mbNo );
     end;
     inherited EscribirCambios;
end;

procedure TInscritos_DevEx.RefrescaInscritos;
begin
     dmRecursos.cdsInscritos.Refrescar;
end;

procedure TInscritos_DevEx.btnInscripcionesClick(Sender: TObject);
var
   EmpInscritosGridSelect_DevEx :  TEmpInscribirGridSelect_DevEx;
begin
     inherited;
     EmpInscritosGridSelect_DevEx := TEmpInscribirGridSelect_DevEx.Create(Application);
     try
        with EmpInscritosGridSelect_DevEx do
        begin
             Dataset := dmRecursos.cdsInscribirAlumnos;
             DataSet.Init;
             ShowModal;
             if ( modalResult = mrOk ) then
                dmRecursos.AgregarAlumnos(Truncar);
        end;
     finally
            FreeAndNil( EmpInscritosGridSelect_DevEx );
     end;
end;
end.
