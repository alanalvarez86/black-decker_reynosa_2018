unit FEditHisCreInfonavit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, DBCtrls, StdCtrls,
  Mask, Db, Buttons, ExtCtrls, ZetaDBTextBox,
  ZetaKeyCombo,
  ZetaKeyLookup, ZetaNumero, ZetaFecha, ComCtrls, ZetaSmartLists, ZetaCommonLists;

type
     TEditHisCreInfonavit = class(TBaseEdicion)
     Panel1: TPanel;
     lblTipoMov: TLabel;
     KI_TIPO: TZetaDBKeyCombo;
     Panel2: TPanel;
     PageControlInfonavit: TPageControl;
     TabCredito: TTabSheet;
     TabNotas: TTabSheet;
     lblFecha: TLabel;
     lblNoCredito: TLabel;
     lblTipoPresta: TLabel;
     lblAmorti: TLabel;
     Label1: TLabel;
     UsuarioLbl: TLabel;
     US_DESCRIP: TZetaDBTextBox;
     Label2: TLabel;
     KI_CAPTURA: TZetaDBTextBox;
     CB_INF_ANTLbl: TLabel;
     KI_FECHA: TZetaDBFecha;
     CB_INFCRED: TDBEdit;
     CB_INFTIPO: TZetaDBKeyCombo;
     CB_INFTASA: TZetaDBNumero;
     KI_COMENTA: TDBEdit;
     CB_INFDISM: TDBCheckBox;
     CB_INF_ANT: TZetaDBFecha;
     Label3: TLabel;
     KI_NOTA: TDBMemo;
     bgDocumento: TGroupBox;
     btnAgregarDocumento: TSpeedButton;
     btnBorraDocumento: TSpeedButton;
     btnEditarDocumento: TSpeedButton;
     btnVerDocumento: TSpeedButton;
     lblDescripcionArchivo: TLabel;
     lblTipoArchivo: TLabel;
     procedure FormCreate(Sender: TObject);
     procedure FormShow(Sender: TObject);
     //procedure CB_INF_OLDChange(Sender: TObject);
     procedure KI_TIPOChange(Sender: TObject);
     procedure CB_INFTIPOChange(Sender: TObject);
     procedure CancelarClick(Sender: TObject);
     procedure btnAgregarDocumentoClick(Sender: TObject);
     procedure btnBorraDocumentoClick(Sender: TObject);
     procedure btnEditarDocumentoClick(Sender: TObject);
     procedure btnVerDocumentoClick(Sender: TObject);
  private
     //FConectandoTasa: Boolean;
     procedure SetControls( const TipoMov: eTipoMovInfonavit );
     procedure HabilitaTasaAnterior( const TipoMov: eTipoMovInfonavit );
     procedure MaskAmortizacion;
     procedure AgregaDocumento;
     procedure DialogoAgregaDocumento(const lAgregando: Boolean);
     procedure EnabledControlDocumento;
     //procedure AsignaValorTasa;
  protected
     procedure Connect;override;
     //procedure EscribirCambios; override;
  public
     { Public declarations }
end;

const
     K_NINGUNO = 0;

var
     EditHisCreInfonavit: TEditHisCreInfonavit;

implementation

uses DRecursos,
     dSistema,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaClientTools,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaFilesTools,
     FEditDocumento;

{$R *.DFM}

procedure TEditHisCreInfonavit.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := ZetacommonLists.stEmpleado;
     HelpContext:= H_EMP_EDIT_EXP_INFONAVIT;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_INFONAVIT;
     //FConectandoTasa := FALSE;
     FirstControl := KI_TIPO;
     // CB_INF_OLD.Style := csDropDown;
     //ZetaClientTools.LlenaTasaAnterior( CB_INF_OLD );
end;

procedure TEditHisCreInfonavit.FormShow(Sender: TObject);
begin
     PageControlInfonavit.ActivePage := TabCredito;
     inherited;
     MaskAmortizacion;
     SetControls( eTipoMovInfonavit( dmRecursos.cdsHisCreInfonavit.FieldByName('KI_TIPO').AsInteger ) );
     EnabledControlDocumento;
end;

procedure TEditHisCreInfonavit.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsHisCreInfonavit.Conectar;
          DataSource.DataSet := cdsHisCreInfonavit;
     end;
end;

procedure TEditHisCreInfonavit.KI_TIPOChange(Sender: TObject);
begin
     inherited;
     dmRecursos.AsignaDefaultsInfonavit( eTipoMovInfonavit( KI_TIPO.Valor ) );
     SetControls( eTipoMovInfonavit( KI_TIPO.Valor ) );
end;

procedure TEditHisCreInfonavit.SetControls( const TipoMov: eTipoMovInfonavit);
const
     aCaptionFecha: array[FALSE..TRUE] of PChar = ('Fecha de Inicio:','Fecha Suspensi�n:');
begin
     { La fecha y tipo de movimiento, solamente estar�n habilitados cuando este en estado de dsInsert }
     KI_TIPO.Enabled:= ( dmRecursos.cdsHisCreInfonavit.State = dsInsert );
     lblTipoMov.Enabled:= KI_TIPO.Enabled;
     KI_FECHA.Enabled:= ( dmRecursos.cdsHisCreInfonavit.RecordCount = 1 ) OR ( KI_TIPO.Enabled );
     lblFecha.Enabled:= ( dmRecursos.cdsHisCreInfonavit.RecordCount = 1 ) OR ( KI_TIPO.Enabled );
     CB_INF_ANTlbl.Enabled:= TipoMov in [ infoInicio, inforeinicio];
     CB_INF_ANT.Enabled:= TipoMov in [ infoInicio, inforeinicio];
     { Fecha }
     lblFecha.Caption:= aCaptionFecha[ TipoMov in [ infosuspen ] ];
     { # de Cr�dito }     
     CB_INFCRED.Enabled:= ( TipoMov in [ infoInicio, inforeinicio, infocambionocre ] );
     lblNoCredito.Enabled:= CB_INFCRED.Enabled;
     { Tipo de Pr�stamo }
     CB_INFTIPO.Enabled:= ( TipoMov in [ infoInicio, inforeinicio, infocambiotd, infocambionocre ] );
     lblTipoPresta.Enabled:= CB_INFTIPO.Enabled;
     { Amortizaci�n }
     CB_INFTASA.Enabled:= ( TipoMov in [ infoInicio, inforeinicio, infocambiotd, infocambiovd, infocambionocre ] );
     lblAmorti.Enabled:= CB_INFTASA.Enabled;
     { Tasa Anterior y Aplica Disminuci�n % }
     HabilitaTasaAnterior( TipoMov ) ;
end;

procedure TEditHisCreInfonavit.HabilitaTasaAnterior( const TipoMov: eTipoMovInfonavit);
var
   eTipoPresta: eTipoInfonavit;
begin
     { �ste m�todo maneja los controles de tasa anterior % }
     eTipoPresta:= eTipoInfonavit ( CB_INFTIPO.Valor );
     //CB_INF_OLD.Text := FloattoStr( dmRecursos.cdsHisCreInfonavit.FieldByName( 'CB_INF_OLD' ).AsFloat );
     CB_INFDISM.Enabled := ( eTipoPresta = tiPorcentaje ) and
                           ( TipoMov in [ infoInicio, inforeinicio, infocambiotd, infocambiovd, infocambionocre ] );
    // lblTasaAnt.Enabled:= CB_INF_OLD.Enabled;
    // CB_INFDISM.Enabled:= CB_INF_OLD.Enabled;
end;

procedure TEditHisCreInfonavit.CB_INFTIPOChange(Sender: TObject);
begin
     inherited;
     MaskAmortizacion;  { cambia la m�scara de la amortizaci�n }
     HabilitaTasaAnterior( eTipoMovInfonavit( KI_TIPO.Valor ) );
     if ( eTipoInfonavit ( CB_INFTIPO.Valor ) <> tiPorcentaje ) then
     begin
          with dmRecursos.cdsHisCreInfonavit do
          begin
               if not ( State in [dsEdit, dsInsert] ) then
                  Edit;
               FieldByName('CB_INFDISM').AsString := K_GLOBAL_NO;
               if ( eTipoInfonavit ( CB_INFTIPO.Valor ) = tiCuotaFija ) then
                  FieldByName('CB_INFTASA').AsFloat := ZetaCommonTools.Redondea( FieldByName('CB_INFTASA').AsFloat );
          end;
     end;
end;

procedure TEditHisCreInfonavit.MaskAmortizacion;
var
   eTipoPresta: eTipoInfonavit;
begin
     eTipoPresta:= eTipoInfonavit ( CB_INFTIPO.Valor );
     with CB_INFTASA do
     begin
          case eTipoPresta of
               tiNoTiene : Mascara := mnPesos;
               tiPorcentaje: Mascara := mnTasa;
               tiCuotaFija: Mascara := mnPesos;
               tiVeces: Mascara := mnVecesSMGDF;
          end;
     end;
end;

procedure TEditHisCreInfonavit.CancelarClick(Sender: TObject);
begin
     inherited;
     SetControls( eTipoMovInfonavit( dmRecursos.cdsHisCreInfonavit.FieldByName('KI_TIPO').AsInteger ) );
end;

procedure TEditHisCreInfonavit.AgregaDocumento;
var
   sDocumento, sExtension : string;
begin
     with dmRecursos.cdsHisCreInfonavit do
     begin
          sDocumento := FieldByName('KI_D_NOM').AsString;
          sExtension := FieldByName('KI_D_EXT').AsString;
     end;
     if ( StrVacio( sDocumento ) or 
          ZetaDialogo.ZConfirm( Caption, '� Desea Sustituir el Documento: ' + sDocumento + ' por uno Nuevo ?', 0, mbNo ) )  then
          DialogoAgregaDocumento( TRUE );
end;

procedure TEditHisCreInfonavit.btnAgregarDocumentoClick(Sender: TObject);
begin
     AgregaDocumento;
end;

procedure TEditHisCreInfonavit.DialogoAgregaDocumento(const lAgregando: Boolean);
begin
     with dmRecursos.cdsHisCreInfonavit do
     begin
          if FEditDocumento.EditarDocumento( lAgregando, FieldByName('KI_D_NOM').AsString, H_DOCUMENTO_CONCEPTOS, dmRecursos.CargaDocumento ) then
          begin
               EnabledControlDocumento;
               if state in [ dsinsert,dsedit ] then
                  Modo:= dsEdit;
          end;
     end;
end;

procedure TEditHisCreInfonavit.EnabledControlDocumento;
var
     lEnabled : Boolean;
     sDescripcion, sTipo : string;
     oColor : TColor;
begin
     with dmRecursos.cdsHisCreInfonavit do
     begin
          lEnabled := StrLleno( FieldByName('KI_D_NOM').AsString );

          if lEnabled then
          begin
               oColor := clNavy;
               sDescripcion := 'Archivo: '+ FieldByName('KI_D_NOM').AsString;
               sTipo := ZetaFilesTools.GetTipoDocumento( FieldByName('KI_D_EXT').AsString );
          end
          else
          begin
               with lblDescripcionArchivo do
               begin
                    oColor := clGreen;
                    sDescripcion := 'No hay Ning�n Documento Almacenado';
                    sTipo := VACIO;
               end;
          end;
     end;

     btnVerDocumento.Enabled := lEnabled ;
     btnBorraDocumento.Enabled := lEnabled;
     btnEditarDocumento.Enabled := lEnabled;

     with lblDescripcionArchivo do
     begin
          Font.Color:= oColor;
          Caption := sDescripcion;
     end;

     lblTipoArchivo.Caption := sTipo;
end;

procedure TEditHisCreInfonavit.btnBorraDocumentoClick(Sender: TObject);
begin
     with dmRecursos do
     begin
          if ( ZetaDialogo.ZConfirm(Caption, '� Desea Borrar el Documento ' + cdsHisCreInfonavit.FieldByName('KI_D_NOM').AsString + ' ?', 0, mbNo ) ) then
          begin
               dmRecursos.BorraDocumento;
               EnabledControlDocumento;
          end;
     end;
end;

procedure TEditHisCreInfonavit.btnEditarDocumentoClick(Sender: TObject);
begin
     inherited;
     if StrLleno( dmRecursos.cdsHisCreInfonavit.FieldByName('KI_D_NOM').AsString ) then
     begin
          DialogoAgregaDocumento( FALSE );
     end
     else ZetaDialogo.ZError( Caption, 'El Documento No Contiene Informaci�n', 0 );
end;

procedure TEditHisCreInfonavit.btnVerDocumentoClick(Sender: TObject);
begin
     inherited;
     dmRecursos.AbreDocumento;
end;

end.
