inherited KardexBaja_DevEx: TKardexBaja_DevEx
  Left = 692
  Top = 297
  Caption = 'Baja '
  ClientHeight = 324
  ClientWidth = 472
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 288
    Width = 472
    inherited OK_DevEx: TcxButton
      Left = 306
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 384
    end
  end
  inherited PageControl_DevEx: TcxPageControl [1]
    Width = 472
    Height = 208
    ClientRectBottom = 206
    ClientRectRight = 470
    inherited General_DevEx: TcxTabSheet
      Caption = 'Baja'
      object Label4: TLabel
        Left = 41
        Top = 36
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Motivo de Baja:'
      end
      object Label3: TLabel
        Left = 38
        Top = 13
        Width = 77
        Height = 13
        Alignment = taRightJustify
        Caption = 'Baja ante IMSS:'
      end
      object CB_RECONTR: TDBCheckBox
        Left = 29
        Top = 55
        Width = 113
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Es Recontratable:'
        DataField = 'CB_RECONTR'
        DataSource = DataSource
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CB_MOT_BAJ: TZetaDBKeyLookup_DevEx
        Left = 129
        Top = 32
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsMotivoBaja
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_MOT_BAJ'
        DataSource = DataSource
      end
      object GroupBox1: TGroupBox
        Left = 33
        Top = 80
        Width = 401
        Height = 90
        Caption = 'Ultima N'#243'mina'
        TabOrder = 3
        object Label5: TLabel
          Left = 57
          Top = 44
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
        end
        object Label6: TLabel
          Left = 41
          Top = 67
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label7: TLabel
          Left = 59
          Top = 20
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
        end
        object CB_NOMTIPO: TZetaDBKeyCombo
          Left = 97
          Top = 40
          Width = 190
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfTipoPeriodo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
          DataField = 'CB_NOMTIPO'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object CB_NOMYEAR: TZetaDBNumero
          Left = 97
          Top = 16
          Width = 60
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          DataField = 'CB_NOMYEAR'
          DataSource = DataSource
        end
        object CB_NOMNUME: TZetaDBKeyLookup_DevEx
          Left = 97
          Top = 63
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsPeriodo
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_NOMNUME'
          DataSource = DataSource
        end
      end
      object CB_FECHA_2: TZetaDBFecha
        Left = 129
        Top = 8
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '13/dic/97'
        Valor = 35777.000000000000000000
        DataField = 'CB_FECHA_2'
        DataSource = DataSource
      end
    end
    inherited Notas_DevEx: TcxTabSheet
      inherited CB_NOTA: TcxDBMemo
        Height = 178
        Width = 468
      end
    end
    object MovimientoIDSE_DevEx: TcxTabSheet
      Caption = 'Movimiento IDSE'
      ImageIndex = 3
      object GroupBox2: TGroupBox
        Left = 25
        Top = 16
        Width = 417
        Height = 89
        Caption = ' Validaci'#243'n IDSE '
        TabOrder = 0
        object lblLoteIDSE: TLabel
          Left = 46
          Top = 27
          Width = 79
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero de Lote:'
        end
        object lblFechaIDSE: TLabel
          Left = 15
          Top = 51
          Width = 110
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha de Transacci'#243'n:'
        end
        object CB_FEC_IDS: TZetaDBFecha
          Left = 132
          Top = 46
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '13/dic/97'
          Valor = 35777.000000000000000000
          DataField = 'CB_FEC_IDS'
          DataSource = DataSource
        end
        object CB_LOT_IDS: TDBEdit
          Left = 132
          Top = 23
          Width = 260
          Height = 21
          DataField = 'CB_LOT_IDS'
          DataSource = DataSource
          TabOrder = 0
        end
      end
    end
  end
  inherited PanelIdentifica: TPanel [2]
    Width = 472
    inherited ValorActivo2: TPanel
      Width = 146
      inherited textoValorActivo2: TLabel
        Width = 140
      end
    end
  end
  inherited PanelFecha: TPanel [3]
    Width = 472
    inherited Label1: TLabel
      Left = 3
      Width = 67
      Caption = 'Fecha de RH:'
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator [4]
    Left = 312
    Top = 88
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
