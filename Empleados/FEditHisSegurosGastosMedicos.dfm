inherited EditHisSeguroGastosMedicos: TEditHisSeguroGastosMedicos
  Left = 295
  Top = 26
  Caption = 'Seguro de Gastos M'#233'dicos'
  ClientHeight = 642
  ClientWidth = 504
  PixelsPerInch = 96
  TextHeight = 13
  object lblDependiente: TLabel [0]
    Left = 54
    Top = 268
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pariente:'
    Enabled = False
  end
  object Label8: TLabel [1]
    Left = 43
    Top = 331
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'Certificado:'
  end
  object Label9: TLabel [2]
    Left = 68
    Top = 355
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'Inicio:'
  end
  object Label10: TLabel [3]
    Left = 272
    Top = 358
    Width = 17
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fin:'
  end
  object Label17: TLabel [4]
    Left = 63
    Top = 381
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status:'
  end
  object Label16: TLabel [5]
    Left = 228
    Top = 331
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Endoso Alta:'
  end
  inherited PanelBotones: TPanel
    Top = 606
    Width = 504
    TabOrder = 8
    inherited OK: TBitBtn
      Left = 346
    end
    inherited Cancelar: TBitBtn
      Left = 424
    end
  end
  inherited PanelSuperior: TPanel
    Width = 504
    TabOrder = 10
  end
  inherited PanelIdentifica: TPanel
    Width = 504
    Height = 20
    TabOrder = 9
    inherited Splitter: TSplitter
      Height = 20
    end
    inherited ValorActivo1: TPanel
      Height = 20
    end
    inherited ValorActivo2: TPanel
      Width = 178
      Height = 20
    end
  end
  object gbxTipo: TRadioGroup [9]
    Left = 98
    Top = 288
    Width = 319
    Height = 37
    BiDiMode = bdLeftToRight
    Caption = ' Tipo '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Titular'
      'Dependiente')
    ParentBiDiMode = False
    TabOrder = 3
    OnClick = gbxTipoClick
  end
  object EP_CERTIFI: TZetaDBEdit [10]
    Left = 99
    Top = 328
    Width = 125
    Height = 21
    TabOrder = 4
    DataField = 'EP_CERTIFI'
    DataSource = DataSource
  end
  object EP_FEC_INI: TZetaDBFecha [11]
    Left = 99
    Top = 352
    Width = 125
    Height = 22
    Cursor = crArrow
    TabOrder = 5
    Text = '23/Mar/12'
    Valor = 40991.000000000000000000
    DataField = 'EP_FEC_INI'
    DataSource = DataSource
  end
  object EP_FEC_FIN: TZetaDBFecha [12]
    Left = 291
    Top = 353
    Width = 125
    Height = 22
    Cursor = crArrow
    TabOrder = 6
    Text = '23/Mar/12'
    Valor = 40991.000000000000000000
    DataField = 'EP_FEC_FIN'
    DataSource = DataSource
  end
  object EP_STATUS: TZetaDBKeyCombo [13]
    Left = 99
    Top = 378
    Width = 126
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
    OnCloseUp = EP_STATUSCloseUp
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'EP_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object cbxDependiente: TZetaKeyCombo [14]
    Left = 99
    Top = 265
    Width = 318
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    OnChange = cbxDependienteChange
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  object Panel1: TPanel [15]
    Left = 0
    Top = 52
    Width = 504
    Height = 169
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 56
      Top = 39
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 61
      Top = 59
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'P'#243'liza:'
    end
    object Label3: TLabel
      Left = 37
      Top = 82
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Referencia:'
    end
    object Label4: TLabel
      Left = 47
      Top = 100
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = 'Vigencia:'
    end
    object Label5: TLabel
      Left = 31
      Top = 117
      Width = 61
      Height = 13
      Alignment = taRightJustify
      Caption = 'Condiciones:'
    end
    object txtPV_FEC_INI: TZetaTextBox
      Left = 96
      Top = 100
      Width = 137
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object txtPM_NUMERO: TZetaTextBox
      Left = 96
      Top = 59
      Width = 137
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label6: TLabel
      Left = 237
      Top = 103
      Width = 9
      Height = 13
      Alignment = taRightJustify
      Caption = 'a:'
    end
    object txtPV_FEC_FIN: TZetaTextBox
      Left = 248
      Top = 100
      Width = 137
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object txtPV_CONDIC: TZetaTextBox
      Left = 96
      Top = 119
      Width = 369
      Height = 47
      AutoSize = False
      ShowAccelChar = False
      WordWrap = True
      Brush.Color = clBtnFace
      Border = True
    end
    object Label7: TLabel
      Left = 60
      Top = 16
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = 'Orden:'
    end
    object PM_CODIGO: TZetaDBKeyLookup
      Left = 96
      Top = 35
      Width = 369
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 1
      TabStop = True
      WidthLlave = 60
      OnExit = PM_CODIGOExit
      DataField = 'PM_CODIGO'
      DataSource = DataSource
    end
    object PV_REFEREN: TZetaDBKeyCombo
      Left = 96
      Top = 78
      Width = 137
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnCloseUp = PV_REFERENCloseUp
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      LlaveNumerica = False
    end
    object EP_ORDEN: TZetaDBNumero
      Left = 96
      Top = 11
      Width = 59
      Height = 21
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
      OnChange = EP_CFIJOChange
      DataField = 'EP_ORDEN'
      DataSource = DataSource
    end
  end
  object rdgAsegurado: TRadioGroup [16]
    Left = 98
    Top = 224
    Width = 319
    Height = 36
    BiDiMode = bdLeftToRight
    Caption = ' Asegurado '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Empleado'
      'Pariente')
    ParentBiDiMode = False
    TabOrder = 1
    OnClick = rdgAseguradoClick
  end
  object pgPaginas: TPageControl [17]
    Left = 0
    Top = 399
    Width = 504
    Height = 173
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 11
    object TabSheet1: TTabSheet
      Caption = 'Costos'
      object Label15: TLabel
        Left = 22
        Top = 121
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Costo Empresa:'
      end
      object Label13: TLabel
        Left = 16
        Top = 95
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Costo Empleado:'
      end
      object Label14: TLabel
        Left = 29
        Top = 69
        Width = 67
        Height = 13
        Alignment = taRightJustify
        Caption = 'Total a Pagar:'
      end
      object Label12: TLabel
        Left = 35
        Top = 44
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = 'Costo P'#243'liza:'
      end
      object Label11: TLabel
        Left = 47
        Top = 20
        Width = 49
        Height = 13
        Alignment = taRightJustify
        Caption = 'Costo Fijo:'
      end
      object txtTotalPagar: TZetaTextBox
        Left = 99
        Top = 66
        Width = 125
        Height = 19
        Alignment = taRightJustify
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object lblTablaCotizacion: TLabel
        Left = 263
        Top = 44
        Width = 37
        Height = 13
        Caption = 'lblTabla'
      end
      object bbtnBuscarTablaCotizacion: TBitBtn
        Left = 224
        Top = 39
        Width = 32
        Height = 22
        Hint = 'Tablas de Cotizaci'#243'n'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = bbtnBuscarTablaCotizacionClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
      end
      object EP_CFIJO: TZetaDBNumero
        Left = 99
        Top = 16
        Width = 125
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        OnChange = EP_CFIJOChange
        DataField = 'EP_CFIJO'
        DataSource = DataSource
      end
      object EP_CPOLIZA: TZetaDBNumero
        Left = 99
        Top = 40
        Width = 125
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
        OnChange = EP_CFIJOChange
        DataField = 'EP_CPOLIZA'
        DataSource = DataSource
      end
      object EP_CTITULA: TZetaDBNumero
        Left = 99
        Top = 91
        Width = 125
        Height = 21
        Mascara = mnPesos
        TabOrder = 3
        Text = '0.00'
        OnChange = EP_CTITULAChange
        DataField = 'EP_CTITULA'
        DataSource = DataSource
      end
      object txtCEMPRES: TZetaNumero
        Left = 99
        Top = 117
        Width = 125
        Height = 21
        Mascara = mnPesos
        TabOrder = 4
        Text = '0.00'
        OnExit = txtCEMPRESExit
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Observaciones'
      ImageIndex = 2
      object EP_OBSERVA: TDBMemo
        Left = 0
        Top = 0
        Width = 496
        Height = 145
        Align = alClient
        DataField = 'EP_OBSERVA'
        DataSource = DataSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabCancelacion: TTabSheet
      Caption = 'Cancelaci'#243'n'
      ImageIndex = 1
      object lblFechaCan: TLabel
        Left = 64
        Top = 13
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
        Enabled = False
      end
      object lblMontoCan: TLabel
        Left = 11
        Top = 40
        Width = 86
        Height = 13
        Caption = 'Monto de endoso:'
        Enabled = False
      end
      object lblMotivoCan: TLabel
        Left = 62
        Top = 62
        Width = 35
        Height = 13
        Caption = 'Motivo:'
        Enabled = False
      end
      object lblFolioEndoso: TLabel
        Left = 3
        Top = 88
        Width = 93
        Height = 13
        Alignment = taRightJustify
        Caption = 'Endoso Cancelado:'
        Enabled = False
      end
      object EP_CAN_FEC: TZetaDBFecha
        Left = 99
        Top = 11
        Width = 125
        Height = 22
        Cursor = crArrow
        Enabled = False
        TabOrder = 0
        Text = '23/Mar/12'
        Valor = 40991.000000000000000000
        DataField = 'EP_CAN_FEC'
        DataSource = DataSource
      end
      object EP_CAN_MON: TZetaDBNumero
        Left = 99
        Top = 36
        Width = 108
        Height = 21
        Enabled = False
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        DataField = 'EP_CAN_MON'
        DataSource = DataSource
      end
      object EP_CAN_MOT: TZetaDBEdit
        Left = 99
        Top = 60
        Width = 318
        Height = 21
        Enabled = False
        TabOrder = 2
        DataField = 'EP_CAN_MOT'
        DataSource = DataSource
      end
      object EP_END_CAN: TZetaDBEdit
        Left = 99
        Top = 84
        Width = 125
        Height = 21
        Enabled = False
        TabOrder = 3
        DataField = 'EP_END_CAN'
        DataSource = DataSource
      end
    end
  end
  object Panel2: TPanel [18]
    Left = 0
    Top = 572
    Width = 504
    Height = 34
    Align = alBottom
    TabOrder = 12
    object Label21: TLabel
      Left = 55
      Top = 5
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Modific'#243':'
    end
    object US_DESCRIP: TZetaDBTextBox
      Left = 101
      Top = 1
      Width = 166
      Height = 17
      AutoSize = False
      Caption = 'US_DESCRIP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_DESCRIP'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label22: TLabel
      Left = 270
      Top = 5
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Modificado:'
    end
    object EP_FEC_REG: TZetaDBTextBox
      Left = 328
      Top = 1
      Width = 91
      Height = 17
      AutoSize = False
      Caption = 'EP_FEC_REG'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'EP_FEC_REG'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label23: TLabel
      Left = 425
      Top = 5
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Global:'
    end
    object EP_GLOBAL: TZetaDBTextBox
      Left = 461
      Top = 1
      Width = 30
      Height = 17
      AutoSize = False
      Caption = 'EP_GLOBAL'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'EP_GLOBAL'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object EP_END_ALT: TZetaDBEdit [19]
    Left = 291
    Top = 328
    Width = 125
    Height = 21
    TabOrder = 13
    DataField = 'EP_END_ALT'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 568
    Top = 60
  end
end
