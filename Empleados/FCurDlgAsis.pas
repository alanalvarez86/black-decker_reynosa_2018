unit FCurDlgAsis;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal, ExtCtrls, StdCtrls, Mask, ZetaNumero, Buttons,ZetaCommonTools,ZetaCommonClasses;

type
  TCurDlgAsis = class(TZetaDlgModal)
    GroupBox2: TGroupBox;
    rdAsistenciaMinima: TRadioGroup;
    chkValidarAsistencia: TCheckBox;
    NumSesiones: TZetaNumero;
    gpEvaluaciones: TGroupBox;
    lblEval1: TLabel;
    lblEval2: TLabel;
    lblEval3: TLabel;
    lblEvalFin: TLabel;
    IC_EVA_1: TZetaNumero;
    IC_EVA_2: TZetaNumero;
    IC_EVA_3: TZetaNumero;
    IC_EVA_FIN: TZetaNumero;
    Label6: TLabel;
    Label5: TLabel;
    chkValidarEvaluaciones: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure rdAsistenciaMinimaClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure chkValidarAsistenciaClick(Sender: TObject);
    procedure chkValidarEvaluacionesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FParametros :TZetaParams;
    procedure HabilitarControles;
  public
    { Public declarations }
    property Parametros :TZetaParams read FParametros write FParametros; 

  end;

var
  CurDlgAsis: TCurDlgAsis;


implementation
uses
    ZetaDialogo;
    
{$R *.dfm}

procedure TCurDlgAsis.FormCreate(Sender: TObject);
begin
  inherited;
  Parametros:= TZetaParams.Create;
  NumSesiones.Valor := 1;
end;

procedure TCurDlgAsis.OKClick(Sender: TObject);
var
   Sesiones: Integer;
begin
     Sesiones := NumSesiones.ValorEntero;
     if ( chkValidarAsistencia.Checked ) and ( rdAsistenciaMinima.ItemIndex = 0 ) then
        Sesiones := 0;
     if ( chkValidarAsistencia.Checked ) and ( rdAsistenciaMinima.ItemIndex = 1 ) and ( NumSesiones.ValorEntero = 0 ) then
     begin
        ZetaDialogo.zError( '� Error de N�mero de Sesiones !', 'La Asistencia M�nima Debe Ser Mayor a Cero', 0 );
        ModalResult := mrIgnore;
     end
     else
     begin
          inherited;
          with FParametros do
          begin
               AddBoolean('ValidarAsis',chkValidarAsistencia.Checked );
               AddBoolean('ValidarEval',chkValidarEvaluaciones.Checked );
               AddBoolean('AsisPerfecta',(rdAsistenciaMinima.ItemIndex = 0));
               AddInteger('AsisSesiones',Sesiones);
               AddFloat ('Eval1',IC_EVA_1.Valor );
               AddFloat('Eval2',IC_EVA_2.Valor );
               AddFloat('Eval3',IC_EVA_3.Valor );
               AddFloat('EvalFin',IC_EVA_FIN.Valor );
          end;
     end;
end;

procedure TCurDlgAsis.rdAsistenciaMinimaClick(Sender: TObject);
begin
     inherited;
     NumSesiones.Enabled := rdAsistenciaMinima.ItemIndex = 1;
end;

procedure TCurDlgAsis.FormCloseQuery(Sender: TObject;  var CanClose: Boolean);
begin
     inherited;
     CanClose := ModalResult <> mrIgnore;

end;

procedure  TCurDlgAsis.HabilitarControles;
begin
     rdAsistenciaMinima.Enabled := chkValidarAsistencia.Checked;
     //NumSesiones.Enabled := rdAsistenciaMinima.Enabled; //BUG
     gpEvaluaciones.Enabled  := chkValidarEvaluaciones.Checked;
     lblEval1.Enabled := gpEvaluaciones.Enabled;
     lblEval2.Enabled := gpEvaluaciones.Enabled;
     lblEval3.Enabled := gpEvaluaciones.Enabled;
     lblEvalFin.Enabled := gpEvaluaciones.Enabled;
end;

procedure TCurDlgAsis.chkValidarAsistenciaClick(Sender: TObject);
begin
     inherited;
     HabilitarControles;
end;

procedure TCurDlgAsis.chkValidarEvaluacionesClick(Sender: TObject);
begin
     inherited;
     HabilitarControles;
end;

procedure TCurDlgAsis.FormShow(Sender: TObject);
begin
     inherited;
     HabilitarControles;
end;

end.
