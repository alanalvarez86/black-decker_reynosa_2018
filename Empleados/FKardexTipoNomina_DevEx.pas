unit FKardexTipoNomina_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ZetaFecha, ExtCtrls, StdCtrls, DBCtrls, ZetaDBTextBox, Mask, ComCtrls,
  Buttons, FKardexBase_DevEx, ZetaSmartLists, ZetaKeyCombo, 
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls, cxContainer, cxEdit,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxTextEdit, cxMemo, cxDBEdit, cxPC, cxButtons;

type
  TKardexTipoNomina_DevEx = class(TKardexBase_DevEx)
    CB_NOMINA: TZetaDBKeyCombo;
    HorarioLbl: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
   procedure Connect;override;
  public
    { Public declarations }
  end;

var
  KardexTipoNomina_DevEx: TKardexTipoNomina_DevEx;

implementation

uses ZAccesosTress, ZetaCommonClasses, ZetaCommonLists, ZetaCommonTools,
     dCatalogos, dSistema, dRecursos;

{$R *.DFM}

procedure TKardexTipoNomina_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H_EMP_EDIT_TIPO_NOMINA;
     {V 2013. SOP 4738 No es posible editar un registro de cambio de tipo de n�mina desde la ventana de Kardex}
     // IndexDerechos := ZAccesosTress.D_EMP_REG_CAMBIO_TIPO_NOMINA;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX;
end;

procedure TKardexTipoNomina_DevEx.Connect;
begin
//     dmCatalogos.cdsTPeriodos.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsEditHisKardex.Conectar;
          DataSource.DataSet := cdsEditHisKardex;
     end;
end;

procedure TKardexTipoNomina_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     CB_NOMINA.ListaFija:=lfTipoPeriodo; //acl
end;

end.






