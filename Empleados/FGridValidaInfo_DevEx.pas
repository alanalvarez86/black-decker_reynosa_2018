unit FGridValidaInfo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, StdCtrls, Buttons, Db, ZetaDBGrid,
  ZetaDBTextBox, DBCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, cxControls, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxTextEdit, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons,
  dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, imageenview, ieview;

type
  TFGridInfoAlta_DevEx = class(TForm)
    PanelBotones: TPanel;
    ContinuarBtn: TcxButton;
    CancelarBtn: TcxButton;
    DataSource: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    CB_CODIGO: TZetaDBTextBox;
    CB_NOMBRES: TZetaDBTextBox;
    CB_RFC: TZetaDBTextBox;
    CB_SEGSOC: TZetaDBTextBox;
    CB_FEC_NAC: TZetaDBTextBox;
    Label6: TLabel;
    Label7: TLabel;
    CB_APE_MAT: TZetaDBTextBox;
    CB_APE_PAT: TZetaDBTextBox;
    Panel4: TPanel;
    Label8: TLabel;
    CB_CURP: TZetaDBTextBox;
    Label9: TLabel;
    CB_RECONTR: TDBCheckBox;
    ZetaCXGrid1DBTableView1: TcxGridDBTableView;
    ZetaCXGrid1: TZetaCXGrid;
    ZetaCXGrid1Level1: TcxGridLevel;
    GridEmpleados: TcxGridDBTableView;
    GridEmpleadosCB_CODIGO: TcxGridDBColumn;
    GridEmpleadosPRETTYNAME: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    FOTO: TImageEnView;
    procedure FormCreate(Sender: TObject);
    procedure CancelarBtnClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure GridEmpleadosCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure GridEmpleadosDblClick(Sender: TObject);
    procedure GridEmpleadosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    function GetEmpleadoSeleccionado: Integer;
    { Private declarations }
  public
    { Public declarations }
    property EmpleadoSeleccionado: Integer read GetEmpleadoSeleccionado;
  end;

var
  FGridInfoAlta_DevEx: TFGridInfoAlta_DevEx;

implementation

uses DRecursos,
     DCliente,
     DGlobal,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses,
     FToolsImageEn;

{$define CAMBIOS_SENDA}
{.$undefine CAMBIOS_SENDA}

{$R *.DFM}

procedure TFGridInfoAlta_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext := H10152_Tress_verifica_la_existencia_de_nuevos_empleados;
     DataSource.DataSet := dmRecursos.cdsInfoEmp;
end;

procedure TFGridInfoAlta_DevEx.FormShow(Sender: TObject);
begin
     {$ifdef CAMBIOS_SENDA}
     Continuarbtn.Visible := not ( Global.GetGlobalString( K_GLOBAL_VALIDA_ALTA_EMP ) = VALIDAR_REPETIDOS_ALTA_IMPIDIENDO );
     {$else}
     Continuarbtn.Visible := True;
     {$endif}
      ActiveControl :=  ZetaCXGrid1;
end;

procedure TFGridInfoAlta_DevEx.CancelarBtnClick(Sender: TObject);
begin
     Close;
end;

function TFGridInfoAlta_DevEx.GetEmpleadoSeleccionado: Integer;
begin
     Result := GridEmpleados.Columns[0].Databinding.Field.AsInteger;
end;

procedure TFGridInfoAlta_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     with dmRecursos.cdsInfoEmp do
     begin
          if (strVacio(FieldByName('FOTO').AsString))then
              Foto.Visible := FALSE
          else
          begin
              Foto.Visible := TRUE;

              if ( Field = nil ) then
                 FToolsImageEn.AsignaBlobAImagen( FOTO, dmRecursos.cdsInfoEmp, 'FOTO' );
          end;
     end;
end;

procedure TFGridInfoAlta_DevEx.GridEmpleadosCellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
       ModalResult :=  mrYes;
end;

procedure TFGridInfoAlta_DevEx.GridEmpleadosDblClick(Sender: TObject);
begin
     ModalResult :=  mrYes;
end;

procedure TFGridInfoAlta_DevEx.GridEmpleadosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if Key = VK_RETURN then
     begin
          ModalResult :=  mrYes;
     end;
end;
end.
