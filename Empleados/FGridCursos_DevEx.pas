unit FGridCursos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, DBCtrls, Buttons, ExtCtrls,
  StdCtrls, ZetaNumero, Mask, ZetaFecha, ZetaDBGrid,
  ZetaHora, ZetaSmartLists, ZBaseGridEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, ZetaKeyLookup_DevEx, dxBarExtItems, {$ifdef ICUMEDICAL_CURSOS}System.IOUtils,{$endif}
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TGridCursos_DevEx = class(TBaseGridEdicion_DevEx)
    PanelCurso: TPanel;
    KC_FEC_TOMLbl: TLabel;
    Label1: TLabel;
    MA_CODIGOLbl: TLabel;
    KC_HORASLbl: TLabel;
    CU_CODIGO: TZetaKeyLookup_DevEx;
    MA_CODIGO: TZetaKeyLookup_DevEx;
    KC_FEC_TOM: TZetaFecha;
    KC_HORAS: TZetaNumero;
    KC_FEC_FINLbl: TLabel;
    KC_FEC_FIN: TZetaFecha;
    lblRevision: TLabel;
    KC_REVISIO: TEdit;
    lblCupo: TLabel;
    lblLugar: TLabel;
    lblComenta: TLabel;
    lblHorFin: TLabel;
    lblHorIni: TLabel;
    SE_HOR_INI: TZetaHora;
    SE_HOR_FIN: TZetaHora;
    SE_CUPO: TZetaNumero;
    SE_LUGAR: TEdit;
    SE_COMENTA: TEdit;
    SE_COSTO1Lbl: TLabel;
    SE_COSTO1: TZetaNumero;
    SE_COSTO2Lbl: TLabel;
    SE_COSTO2: TZetaNumero;
    SE_COSTO3Lbl: TLabel;
    SE_COSTO3: TZetaNumero;
    lblEstablec: TLabel;
    KC_EST: TZetaKeyLookup_DevEx;
    GroupBox1: TGroupBox;
    lblDescripcionArchivo: TLabel;
    lblTipoArchivo: TLabel;
    btnAgregarDocumento: TcxButton;
    btnBorraDocumento: TcxButton;
    btnVerDocumento: TcxButton;
    lblUbicacionArchivo: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure CU_CODIGOValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KC_FEC_FINValidDate(Sender: TObject);
    procedure KC_HORASExit(Sender: TObject);
    procedure SetEditarSoloActivos;
    procedure btnAgregarDocumentoClick(Sender: TObject);
    procedure btnBorraDocumentoClick(Sender: TObject);
    procedure btnVerDocumentoClick(Sender: TObject);
  private
    procedure BuscaEmpleado;
    procedure AsignaDatosCurso;
    procedure ValidaDatosCurso;
    {$ifdef ICUMEDICAL_CURSOS}
    procedure EnabledControlDocumento;
    procedure AgregaDocumento;
    procedure DialogoAgregaDocumento( const lAgregando: Boolean );
    {$endif}
  protected
    procedure Connect; override;
    procedure Buscar; override;
    procedure EscribirCambios; override;
  public
  end;

var
  GridCursos_DevEx: TGridCursos_DevEx;

implementation

uses dCliente, dCatalogos, ZetaCommonClasses, ZetaCommonTools,
     ZetaDialogo, ZetaBuscaEmpleado_DevEx{$ifdef ICUMEDICAL_CURSOS }, ZetaFilesTools, FEditDocumentoICU_DevEx{$endif};

{$R *.DFM}

procedure TGridCursos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H10169_Captura_de_cursos;
     with dmCliente do
     begin
          KC_FEC_TOM.Valor := FechaDefault;
          KC_FEC_FIN.Valor := FechaDefault;
     end;
     CU_CODIGO.LookupDataset := dmCatalogos.cdsCursos;
     MA_CODIGO.LookupDataset := dmCatalogos.cdsMaestros;
     KC_EST.LookupDataset := dmCatalogos.cdsEstablecimientos;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TGridCursos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmCatalogos.Duracion_Curso := 0;
     self.ActiveControl:= CU_CODIGO;
     {$ifdef ICUMEDICAL_CURSOS}
     GroupBox1.Visible := True;
     GroupBox1.Enabled := True;
     GridCursos_DevEx.ClientHeight := 679;
     GridCursos_DevEx.ClientWidth := 481;
     PanelCurso.Height := 311;
     PanelCurso.Width := 481;
     dmCatalogos.PathICU := VACIO;
     EnabledControlDocumento;
     {$else}
     GroupBox1.Visible := False;
     GroupBox1.Enabled := False;
     {$endif}
end;

procedure TGridCursos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          cdsMaestros.Conectar;
          cdsSesiones.Conectar;
          cdsEstablecimientos.Conectar; 
          ConectacdsHisCursos( 0 );
          DataSource.DataSet:= cdsHisCursos;
     end;
     {$ifdef ICUMEDICAL_CURSOS}
     EnabledControlDocumento;
     {$endif}
end;

procedure TGridCursos_DevEx.Buscar;
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado;
     end;
end;

{$ifdef ICUMEDICAL_CURSOS}
procedure TGridCursos_DevEx.EnabledControlDocumento;
var
   lEnabled: Boolean;
   sDescripcion, sTipo, sRuta: string;
   oColor: TColor;
begin
     //with dmCatalogos.cdsSesiones do
     //begin
          lEnabled := StrLleno(dmCatalogos.PathICU);

          if lEnabled then
          begin
               oColor := clNavy;
               sDescripcion := 'Archivo: ' + TPath.GetFileName( dmCatalogos.PathICU );
               sTipo := 'Tipo: ' + ZetaFilesTools.GetTipoDocumento( UpperCase( Copy( ExtractFileExt( dmCatalogos.PathICU ), 2 , 255 ) ) );
               sRuta := 'Ruta: ' + dmCatalogos.PathICU;
          end
          else
          begin
               with lblDescripcionArchivo do
               begin
                    oColor := clGreen;
                    sDescripcion := 'No hay ning�n documento almacenado';
                    sTipo := VACIO;
                    sRuta := VACIO;
               end;
          end;
     //end;

     btnVerDocumento.Enabled := lEnabled;
     btnBorraDocumento.Enabled := lEnabled;

     with lblDescripcionArchivo do
     begin
          Font.Color := oColor;
          Caption := sDescripcion;
          Hint := TPath.GetFileName( dmCatalogos.PathICU );
     end;

     lblTipoArchivo.Caption := sTipo;
     lblUbicacionArchivo.Caption := sRuta;
     lblUbicacionArchivo.Hint := dmCatalogos.PathICU;
end;


procedure TGridCursos_DevEx.AgregaDocumento;
var
   sDocumento: string;
begin
     sDocumento := dmCatalogos.cdsSesiones.FieldByName('SE_D_NOM').AsString;
     if StrVacio( sDocumento ) or ZetaDialogo.ZConfirm( Caption, '�Desea sustituir el documento: ' + sDocumento + ' por uno nuevo?', 0, mbNo ) then
     begin
          DialogoAgregaDocumento( TRUE );
     end;
end;


procedure TGridCursos_DevEx.DialogoAgregaDocumento( const lAgregando: Boolean );
begin
     with dmCatalogos.cdsSesiones do
     begin
          if FEditDocumentoICU_DevEx.EditarDocumento( lAgregando, FIeldByName( 'SE_D_NOM' ).AsString, H_DOCUMENTO_CONCEPTOS, dmCatalogos.CargaDocumentoICU ) then
          begin
               EnabledControlDocumento;
               {if state in [ dsinsert, dsedit ] then
                  Modo:= dsEdit;}
          end;
     end;
end;
{$endif}

procedure TGridCursos_DevEx.btnAgregarDocumentoClick(Sender: TObject);
begin
     inherited;
     {$ifdef ICUMEDICAL_CURSOS}
     AgregaDocumento;
     {$endif}
end;

procedure TGridCursos_DevEx.btnBorraDocumentoClick(Sender: TObject);
begin
     inherited;
     {$ifdef ICUMEDICAL_CURSOS}
     with dmCatalogos do
     begin
          if ( ZetaDialogo.ZConfirm(Caption, '� Desea borrar el documento ' + cdsSesiones.FieldByName('SE_D_NOM').AsString + ' ?', 0, mbNo ) ) then
          begin
               dmCatalogos.BorraDocumentoICU;
               EnabledControlDocumento;
          end;
     end;
     {$endif}
end;

procedure TGridCursos_DevEx.btnVerDocumentoClick(Sender: TObject);
begin
     inherited;
     {$ifdef ICUMEDICAL_CURSOS}
     dmCatalogos.AbreDocumentoICU;
     {$endif}
end;

procedure TGridCursos_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmCatalogos.cdsHisCursos do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName('CB_CODIGO').AsString:= sKey;
          end;
     end;
end;

procedure TGridCursos_DevEx.ValidaDatosCurso;
begin
     if StrVacio( CU_CODIGO.Llave ) then
     begin
          ActiveControl := CU_CODIGO;
          DataBaseError( 'No Se Ha Especificado Un Curso' );
     end
     else
     if ( KC_HORAS.Valor <= 0 ) then
     begin
          ActiveControl := KC_HORAS;
          DataBaseError( 'La Duraci�n Debe Ser Mayor A Cero' );
     end;
end;

procedure TGridCursos_DevEx.AsignaDatosCurso;
begin
     with dmCatalogos do
     begin
          try
             with cdsSesiones do
             begin
                  Append;
                  FieldByName( 'CU_CODIGO' ).AsString := CU_CODIGO.Llave;
                  FieldByName( 'SE_REVISIO' ).AsString := KC_REVISIO.Text;
                  FieldByName( 'MA_CODIGO' ).AsString := MA_CODIGO.Llave;
                  FieldByName( 'SE_FEC_INI' ).AsDateTime := KC_FEC_TOM.Valor;
                  FieldByName( 'SE_FEC_FIN' ).AsDateTime := KC_FEC_FIN.Valor;
                  FieldByName( 'SE_HOR_INI' ).AsString := SE_HOR_INI.Valor;
                  FieldByName( 'SE_HOR_FIN' ).AsString := SE_HOR_FIN.Valor;
                  FieldByName( 'SE_HORAS' ).AsFloat := KC_HORAS.Valor;
                  FieldByName( 'SE_CUPO' ).AsInteger := SE_CUPO.ValorEntero;
                  FieldByName( 'SE_LUGAR' ).AsString := SE_LUGAR.Text;
                  FieldByName( 'SE_COMENTA' ).AsString := SE_COMENTA.Text;
                  FieldByName( 'SE_COSTO1' ).AsFloat := SE_COSTO1.Valor;
                  FieldByName( 'SE_COSTO2' ).AsFloat := SE_COSTO2.Valor;
                  FieldByName( 'SE_COSTO3' ).AsFloat := SE_COSTO3.Valor;
                  FieldByName( 'SE_EST' ).AsString := KC_EST.Llave;
                  Post;
             end;
             with cdsHisCursos do
             begin
                  if State in [ dsEdit, dsInsert ] then
                     Post;
                  DisableControls;
                  try
                     First;
                     while not EOF do
                     begin
                          Edit;
                          FieldByName( 'SE_FOLIO' ).AsInteger := MaxFolioSesion;
                          FieldByName( 'KC_REVISIO' ).AsString := cdsSesiones.FieldByName( 'SE_REVISIO' ).AsString;
                          FieldByName( 'MA_CODIGO' ).AsString := cdsSesiones.FieldByName( 'MA_CODIGO' ).AsString;
                          FieldByName( 'KC_FEC_TOM' ).AsDateTime := cdsSesiones.FieldByName('SE_FEC_INI').AsDateTime;
                          FieldByName( 'KC_FEC_FIN' ).AsDateTime := cdsSesiones.FieldByName('SE_FEC_FIN').AsDateTime;
                          FieldByName( 'KC_EST' ).AsString := cdsSesiones.FieldByName('SE_EST').AsString;
                          Next;
                     end;
                  finally
                     EnableControls;
                  end;
             end;
             cdsSesiones.Enviar;
             ZInformation( Self.Caption, 'Los cursos fueron registrados en el grupo No. ' + InttoStr( MaxFolioSesion ), 0 );
             self.ModalResult := mrOk;
          except
                on Error: Exception do
                     Application.HandleException( Error );
          end;
     end;
end;

procedure TGridCursos_DevEx.CU_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          if( strLleno( CU_CODIGO.Llave ) )then
          begin
               MA_CODIGO.Llave := cdsCursos.FieldByName( 'MA_CODIGO' ).AsString;
               KC_HORAS.Valor := cdsCursos.FieldByName( 'CU_HORAS' ).AsFloat;
               Duracion_Curso := cdsCursos.FieldByName( 'CU_HORAS' ).AsFloat;
               KC_REVISIO.Text := cdsCursos.FieldByName( 'CU_REVISIO' ).AsString;
               SE_COSTO1.Valor := cdsCursos.FieldByName( 'CU_COSTO1' ).AsFloat;
               SE_COSTO2.Valor := cdsCursos.FieldByName( 'CU_COSTO2' ).AsFloat;
               SE_COSTO3.Valor := cdsCursos.FieldByName( 'CU_COSTO3' ).AsFloat;
          end;
     end;
end;

procedure TGridCursos_DevEx.KC_FEC_FINValidDate(Sender: TObject);
begin
     inherited;
     if ( KC_FEC_FIN.Valor < KC_FEC_TOM.Valor ) then
     begin
          ActiveControl := KC_FEC_FIN;
          DataBaseError( 'Fecha de Terminaci�n debe ser Mayor o Igual a Inicio' );
     end;
end;

procedure TGridCursos_DevEx.EscribirCambios;
begin
     ValidaDatosCurso;
     AsignaDatosCurso;
end;

procedure TGridCursos_DevEx.KC_HORASExit(Sender: TObject);
begin
     inherited;
     dmCatalogos.Duracion_Curso := KC_HORAS.Valor;
end;

procedure TGridCursos_DevEx.SetEditarSoloActivos;
begin
     CU_CODIGO.EditarSoloActivos := TRUE;
     MA_CODIGO.EditarSoloActivos := TRUE;
     KC_EST.EditarSoloActivos := TRUE;
end;

end.
