unit FFiltroEmpleados_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, DBCtrls, Db,  DBGrids,
     {$ifndef ver130}
     variants,
     {$endif}
     ZetaClientDataset,
     ZetaCommonLists,ZetaCommonClasses,
     ZBaseDlgModal, ZetaKeyLookup, ZetaEdit, ZBaseDlgModal_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  ZetaKeyLookup_DevEx, ImgList, cxButtons, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMemo;

type
  TFiltroEmpleados_DevEx = class(TZetaDlgModal_DevEx)
    FiltrosGB: TGroupBox;
    sFiltroLBL: TLabel;
    sCondicionLBl: TLabel;
    sFiltro: TcxMemo;
    GBRango: TGroupBox;
    lbInicial: TLabel;
    lbFinal: TLabel;
    BFinal: TcxButton;
    BInicial: TcxButton;
    BLista: TcxButton;
    RBTodos: TRadioButton;
    RBRango: TRadioButton;
    RBLista: TRadioButton;
    EInicial: TZetaEdit;
    EFinal: TZetaEdit;
    ELista: TZetaEdit;
    ECondicion: TZetaKeyLookup_DevEx;
    BAgregaCampo: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure RBRangoClick(Sender: TObject);
    procedure RBListaClick(Sender: TObject);
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);

  private
    { Private declarations }
    FTipoRango: eTipoRangoActivo;
    FEmpleadoCodigo: String;
    FEmpleadoFiltro: String;
    FVerificacion: Boolean;
    FParameterList: TZetaParams;
    FDescripciones: TZetaParams;
    function BuscaEmpleado(const lConcatena: Boolean; const sLlave: String): String;
    procedure DescargarParametros;

  protected
    { Protected declarations }
    property EmpleadoCodigo: String read FEmpleadoCodigo write FEmpleadoCodigo;
    property EmpleadoFiltro: String read FEmpleadoFiltro write FEmpleadoFiltro;
    property Verificacion: Boolean read FVerificacion write FVerificacion;
    function GetFiltro: String;
    function GetRango: String;
    function GetCondicion: String;
    procedure EnabledBotones( const eTipo: eTipoRangoActivo );

  public
     procedure CargaParametros;
     property ParameterList: TZetaParams read FParameterList write FParameterList;
     property Descripciones: TZetaParams read FDescripciones write FDescripciones;
    { Public declarations }
    property TipoRango: eTipoRangoActivo read FTipoRango write FTipoRango;
    procedure SetRangoLista(var RangoI,RangoF, Lista,Condicion: string);
    procedure GetRangoLista(var RangoI, RangoF, Lista,Condicion: string);
  end;


var
  FiltroEmpleados_DevEx: TFiltroEmpleados_DevEx;


implementation

uses ZetaDialogo,
     DCliente, dCatalogos,ZetaClientTools,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaBuscaEmpleado_DevEx,
     ZConstruyeFormula;

{$R *.DFM}

procedure TFiltroEmpleados_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     FEmpleadoCodigo := 'COLABORA' + '.CB_CODIGO';
     FEmpleadoFiltro := '';
     ECondicion.LookupDataset := dmCatalogos.cdsCondiciones;
     FVerificacion := False;
     ECondicion.Llave := VACIO;
     sFiltro.Text := VACIO;
     ELista.Text := IntToStr( dmCliente.Empleado );
     RBTodos.Checked := True;
     dmCatalogos.cdsCondiciones.Conectar;
     DescargarParametros;
end;

{procedure TFiltroEmpleados.SetVerificacion( const Value: Boolean );
begin
     FVerificacion := FVerificacion or Value;
end;
  }
procedure TFiltroEmpleados_DevEx.EnabledBotones( const eTipo: eTipoRangoActivo );
var
   lEnabled: Boolean;
begin
     FTipoRango := eTipo;
     lEnabled := ( eTipo = raRango );
     lbInicial.Enabled := lEnabled;
     lbFinal.Enabled := lEnabled;
     EInicial.Enabled := lEnabled;
     EFinal.Enabled := lEnabled;
     bInicial.Enabled := lEnabled;
     bFinal.Enabled := lEnabled;
     lEnabled := ( eTipo = raLista );
     ELista.Enabled := lEnabled;
     bLista.Enabled := lEnabled;
end;

procedure TFiltroEmpleados_DevEx.DescargarParametros;
begin
     with FParameterList do
     begin
          EnabledBotones(FTipoRango);
          RBTodos.Checked := (FTipoRango = raTodos);
          RBRango.Checked := (FTipoRango = raRango);
          RBLista.Checked := (FTipoRango = raLista);


          if FindParam('Filtro')<>  NIL then
          begin
               if StrLleno(ParamByName('Filtro').AsString )then
               begin
                     sFiltro.Text := ParamByName('Filtro').AsString;
               end;
          end;
     end;
end;

function TFiltroEmpleados_DevEx.GetRango: String;
begin

     case TipoRango of
          raRango: Result := GetFiltroRango( FEmpleadoCodigo, Trim( EInicial.Text ), Trim( EFinal.Text ) );
          raLista: Result := '@'+GetFiltroLista( FEmpleadoCodigo, Trim( ELista.Text ) );
     else
         Result := '';
     end;
end;


function TFiltroEmpleados_DevEx.GetCondicion: String;
begin
     if StrLleno( ECondicion.Llave ) then
     begin
          Result := Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString );
     end
     else
         Result := '';
end;

function TFiltroEmpleados_DevEx.GetFiltro: String;
begin
     Result := Trim( sFiltro.Text );
end;

procedure TFiltroEmpleados_DevEx.CargaParametros;
begin

     with FParameterList do
     begin
          AddString( 'RangoLista', GetRango );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', GetFiltro );
     end;

end;

function TFiltroEmpleados_DevEx.BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
     begin
          if lConcatena and ZetaCommonTools.StrLleno( Text ) then
             Result := sLlave + ',' + sKey
          else
              Result := sKey;
     end;
end;

{ ********** Eventos ******** }

procedure TFiltroEmpleados_DevEx.RBTodosClick(Sender: TObject);
begin
     EnabledBotones( raTodos );
end;

procedure TFiltroEmpleados_DevEx.RBRangoClick(Sender: TObject);
begin
     EnabledBotones( raRango );
end;

procedure TFiltroEmpleados_DevEx.RBListaClick(Sender: TObject);
begin
     EnabledBotones( raLista );
end;

procedure TFiltroEmpleados_DevEx.BInicialClick(Sender: TObject);
begin
     with EInicial do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TFiltroEmpleados_DevEx.BFinalClick(Sender: TObject);
begin
     with EFinal do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TFiltroEmpleados_DevEx.BListaClick(Sender: TObject);
begin
     with ELista do
     begin
          Text := BuscaEmpleado( True, Text );
     end;
end;

procedure TFiltroEmpleados_DevEx.BAgregaCampoClick(Sender: TObject);
begin
     with sFiltro do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, Text, SelStart, evBase );
     end;
end;


procedure TFiltroEmpleados_DevEx.SetRangoLista(var RangoI,RangoF, Lista,Condicion: string);
begin
     Elista.Text := Lista;
     EInicial.Text := RangoI;
     EFinal.Text := RangoF;
     ECondicion.Llave := Condicion;

end;

procedure TFiltroEmpleados_DevEx.GetRangoLista(var RangoI,RangoF, Lista,Condicion: string);
begin
     Lista := Elista.Text ;
     RangoI := EInicial.Text;
     RangoF := EFinal.Text;
     Condicion := ECondicion.Llave;
end;

procedure TFiltroEmpleados_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
     inherited;
     CargaParametros;
     Close;
end;

procedure TFiltroEmpleados_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
  Close;
end;

end.
