inherited EditEmpOtros: TEditEmpOtros
  Left = 378
  Top = 332
  Caption = 'Otros Datos del Empleado'
  ClientHeight = 567
  ClientWidth = 514
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 531
    Width = 514
    inherited OK: TBitBtn
      Left = 346
    end
    inherited Cancelar: TBitBtn
      Left = 431
    end
  end
  inherited PanelSuperior: TPanel
    Width = 514
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 514
    inherited ValorActivo2: TPanel
      Width = 188
      inherited textoValorActivo2: TLabel
        Width = 182
      end
    end
  end
  object pcOtros: TPageControl [3]
    Left = 0
    Top = 51
    Width = 514
    Height = 480
    ActivePage = tsTimbrado
    Align = alClient
    MultiLine = True
    TabOrder = 3
    OnChange = pcOtrosChange
    object tsInfonavit: TTabSheet
      Caption = 'Infonavit'
      object CB_INF_INILbl: TLabel
        Left = 24
        Top = 13
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio de Descuento:'
      end
      object CB_INF_ANTLbl: TLabel
        Left = 5
        Top = 37
        Width = 117
        Height = 13
        Alignment = taRightJustify
        Caption = 'Otorgamiento de Cr'#233'dito:'
      end
      object CB_INFCREDLbl: TLabel
        Left = 61
        Top = 60
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = '# de Cr'#233'dito:'
      end
      object CB_INFTIPOlbl: TLabel
        Left = 43
        Top = 81
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo Descuento:'
      end
      object CB_INFTASALbl: TLabel
        Left = 40
        Top = 103
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Valor Descuento:'
      end
      object LblDescuento: TLabel
        Left = 215
        Top = 120
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'LblDescuento'
        Visible = False
      end
      object CB_INF_OLDLbl: TLabel
        Left = 247
        Top = 127
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tasa Anterior:'
        Visible = False
      end
      object lblPorcentaje: TLabel
        Left = 285
        Top = 119
        Width = 8
        Height = 13
        Alignment = taRightJustify
        Caption = '%'
        Visible = False
      end
      object CB_INF_INI: TZetaDBFecha
        Left = 126
        Top = 9
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '15/dic/97'
        Valor = 35779.000000000000000000
        DataField = 'CB_INF_INI'
        DataSource = DataSource
      end
      object CB_INF_ANT: TZetaDBFecha
        Left = 126
        Top = 33
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '15/dic/97'
        Valor = 35779.000000000000000000
        DataField = 'CB_INF_ANT'
        DataSource = DataSource
      end
      object CB_INFCRED: TDBEdit
        Left = 125
        Top = 56
        Width = 190
        Height = 21
        DataField = 'CB_INFCRED'
        DataSource = DataSource
        TabOrder = 2
      end
      object CB_INFTIPO: TZetaDBKeyCombo
        Left = 125
        Top = 78
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 3
        OnChange = CB_INFTIPOChange
        ListaFija = lfTipoInfonavit
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'CB_INFTIPO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object CB_INFTASA: TZetaDBNumero
        Left = 125
        Top = 100
        Width = 97
        Height = 21
        Mascara = mnTasa
        TabOrder = 4
        Text = '0.0 %'
        DataField = 'CB_INFTASA'
        DataSource = DataSource
      end
      object CB_INFDISM: TDBCheckBox
        Left = 22
        Top = 121
        Width = 116
        Height = 17
        Alignment = taLeftJustify
        BiDiMode = bdLeftToRight
        Caption = 'Disminuci'#243'n Tasa %:'
        DataField = 'CB_INFDISM'
        DataSource = DataSource
        ParentBiDiMode = False
        ReadOnly = True
        TabOrder = 5
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CB_INFACT: TDBCheckBox
        Left = 52
        Top = 137
        Width = 86
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Cr'#233'dito Activo:'
        DataField = 'CB_INFACT'
        DataSource = DataSource
        ReadOnly = True
        TabOrder = 6
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CB_INFMANT: TDBCheckBox
        Left = 256
        Top = 108
        Width = 44
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Mantenimiento:'
        DataField = 'CB_INFMANT'
        DataSource = DataSource
        TabOrder = 7
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        Visible = False
      end
      object zkcCB_INF_OLD: TZetaKeyCombo
        Left = 280
        Top = 119
        Width = 24
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 8
        Visible = False
        OnChange = zkcCB_INF_OLDChange
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
    end
    object tsFonacot: TTabSheet
      Caption = 'Fonacot'
      ImageIndex = 3
      object Label3: TLabel
        Left = 8
        Top = 19
        Width = 97
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero de Fonacot:'
      end
      object CB_FONACOT: TDBEdit
        Left = 113
        Top = 15
        Width = 151
        Height = 21
        DataField = 'CB_FONACOT'
        DataSource = DataSource
        TabOrder = 0
      end
    end
    object tsPPrimerEmpleo: TTabSheet
      Caption = 'Programa de Primer Empleo'
      ImageIndex = 4
      object CB_EMPLEO: TDBCheckBox
        Left = 8
        Top = 12
        Width = 111
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Aplica al Programa:'
        DataField = 'CB_EMPLEO'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
    object tsAsistencia: TTabSheet
      Caption = 'Asistencia'
      ImageIndex = 1
      object LTipoCreden: TLabel
        Left = 19
        Top = 15
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Letra Credencial:'
      end
      object Label1: TLabel
        Left = 23
        Top = 54
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero Tarjeta:'
      end
      object lblNumeroBiometrico: TLabel
        Left = 7
        Top = 78
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero Biom'#233'trico:'
        Visible = False
      end
      object CB_ID_BIO: TZetaDBTextBox
        Left = 103
        Top = 75
        Width = 148
        Height = 21
        AutoSize = False
        Caption = 'CB_ID_BIO'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object btnAsignarBio: TSpeedButton
        Left = 253
        Top = 73
        Width = 29
        Height = 24
        Hint = 'Asignar ID biom'#233'trico'
        Glyph.Data = {
          42020000424D4202000000000000420000002800000010000000100000000100
          1000030000000002000000000000000000000000000000000000007C0000E003
          00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C00001F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C00001F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C007C00001F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1042007C007C00001F7C007C007C007C00001F7C1F7C1F7C
          1F7C1F7C1F7C1042007C00001F7C1F7C1F7C1F7C007C007C00001F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C0000
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C
          00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          007C007C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C}
        ParentShowHint = False
        ShowHint = True
        Visible = False
        OnClick = btnAsignarBioClick
      end
      object btnBorrarBio: TSpeedButton
        Left = 284
        Top = 73
        Width = 29
        Height = 24
        Hint = 'Borrar ID biom'#233'trico'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55558888888585599958555555555550305555555550055BB0555555550FB000
          0055555550FB0BF0F05555550FBFBF0FB05555550BFBF0FB005555500FBFBFB0
          B055550E0BFBFB00B05550EEE0BFB0B0B055000EEE0BFBF0B0550000EEE00000
          B05500000E055550705550000055555505555500055555550555}
        ParentShowHint = False
        ShowHint = True
        Visible = False
        OnClick = btnBorrarBioClick
      end
      object lblCB_GP_COD: TLabel
        Left = 8
        Top = 103
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = 'Grupo Dispositivos:'
        Visible = False
      end
      object BtnEnrolar: TSpeedButton
        Left = 104
        Top = 139
        Width = 113
        Height = 29
        BiDiMode = bdLeftToRight
        Caption = 'Enrolar Huellas'
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          777777777777777770000070000077777000E00BFBFB07777777E0BFBF000777
          7700E0FBFBFBF0777700E0BFBF0000007777E0FBFBFBFBFB0799E0BF00000000
          7799000BFB077777777777700077777777007777777777777700777777777777
          7777777777777777700077777777777770007777777777777777}
        ParentBiDiMode = False
        Visible = False
        OnClick = BtnEnrolarClick
      end
      object btnBorrarHuellas: TSpeedButton
        Left = 224
        Top = 139
        Width = 113
        Height = 29
        BiDiMode = bdLeftToRight
        Caption = 'Borrar Huellas'
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          7777777777777177777777777777117777717777777711177717777777777117
          7117000000000001157707777777775117770700700707151177077777777117
          7115044444451444475174FFFFF15FFF477774F00F00F00F477774FFFFFFFFFF
          4777744444444444477774744744744747777444444444444777}
        ParentBiDiMode = False
        Visible = False
        OnClick = btnBorrarHuellasClick
      end
      object CB_CREDENC: TDBEdit
        Left = 103
        Top = 11
        Width = 30
        Height = 21
        CharCase = ecUpperCase
        DataField = 'CB_CREDENC'
        DataSource = DataSource
        TabOrder = 0
      end
      object CB_CHECA: TDBCheckBox
        Left = 29
        Top = 33
        Width = 87
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Checa Tarjeta:'
        DataField = 'CB_CHECA'
        DataSource = DataSource
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CB_ID_NUM: TDBEdit
        Left = 103
        Top = 51
        Width = 211
        Height = 21
        DataField = 'CB_ID_NUM'
        DataSource = DataSource
        TabOrder = 2
        OnExit = CB_ID_NUMExit
      end
      object CB_GP_COD: TZetaDBKeyLookup
        Left = 103
        Top = 100
        Width = 212
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataSource = DataSource
      end
      object chkTieneHuella: TCheckBox
        Left = 38
        Top = 122
        Width = 78
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Tiene huella:'
        Enabled = False
        TabOrder = 4
        Visible = False
      end
    end
    object tsEvaluacion: TTabSheet
      Caption = 'Evaluaci'#243'n'
      ImageIndex = 5
      object LFechaEva: TLabel
        Left = 25
        Top = 20
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
      end
      object LEvalua: TLabel
        Left = 7
        Top = 43
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Resultado:'
      end
      object LProxima: TLabel
        Left = 18
        Top = 68
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pr'#243'xima:'
      end
      object CB_LAST_EV: TZetaDBFecha
        Left = 64
        Top = 15
        Width = 115
        Height = 22
        Cursor = crArrow
        Opcional = True
        TabOrder = 0
        Text = '15/dic/97'
        Valor = 35779.000000000000000000
        DataField = 'CB_LAST_EV'
        DataSource = DataSource
      end
      object CB_EVALUA: TZetaDBNumero
        Left = 64
        Top = 39
        Width = 80
        Height = 21
        Mascara = mnPesosDiario
        TabOrder = 1
        Text = '0.00'
        DataField = 'CB_EVALUA'
        DataSource = DataSource
      end
      object CB_NEXT_EV: TZetaDBFecha
        Left = 64
        Top = 63
        Width = 115
        Height = 22
        Cursor = crArrow
        Opcional = True
        TabOrder = 2
        Text = '15/dic/97'
        Valor = 35779.000000000000000000
        DataField = 'CB_NEXT_EV'
        DataSource = DataSource
      end
    end
    object tsCuentas: TTabSheet
      Caption = 'Cuentas'
      ImageIndex = 2
      object lblTarjetasGasolina: TLabel
        Left = 23
        Top = 83
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tarjeta Gasolina:'
      end
      object lblTarjetasDespensa: TLabel
        Left = 16
        Top = 110
        Width = 87
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tarjeta Despensa:'
      end
      object btnTarjetaGasolina: TSpeedButton
        Left = 320
        Top = 78
        Width = 25
        Height = 25
        Hint = 
          'Verificar si el N'#250'mero de Tarjeta de Gasolina Pertenece a otro E' +
          'mpleado'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          555555555555555555555555555555555555555555FF55555555555559055555
          55555555577FF5555555555599905555555555557777F5555555555599905555
          555555557777FF5555555559999905555555555777777F555555559999990555
          5555557777777FF5555557990599905555555777757777F55555790555599055
          55557775555777FF5555555555599905555555555557777F5555555555559905
          555555555555777FF5555555555559905555555555555777FF55555555555579
          05555555555555777FF5555555555557905555555555555777FF555555555555
          5990555555555555577755555555555555555555555555555555}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnTarjetaGasolinaClick
      end
      object btnTarjetaDespensa: TSpeedButton
        Left = 320
        Top = 105
        Width = 25
        Height = 25
        Hint = 
          'Verificar si el N'#250'mero de Tarjeta de Despensa Pertenece a otro E' +
          'mpleado'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          555555555555555555555555555555555555555555FF55555555555559055555
          55555555577FF5555555555599905555555555557777F5555555555599905555
          555555557777FF5555555559999905555555555777777F555555559999990555
          5555557777777FF5555557990599905555555777757777F55555790555599055
          55557775555777FF5555555555599905555555555557777F5555555555559905
          555555555555777FF5555555555559905555555555555777FF55555555555579
          05555555555555777FF5555555555557905555555555555777FF555555555555
          5990555555555555577755555555555555555555555555555555}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnTarjetaDespensaClick
      end
      object Label2: TLabel
        Left = 48
        Top = 136
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Subcuenta:'
      end
      object lbNeto: TLabel
        Left = 8
        Top = 160
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Neto (Piramidaci'#243'n):'
      end
      object CB_CTA_GAS: TDBEdit
        Left = 106
        Top = 79
        Width = 211
        Height = 21
        DataField = 'CB_CTA_GAS'
        DataSource = DataSource
        TabOrder = 1
      end
      object CB_CTA_VAL: TDBEdit
        Left = 106
        Top = 106
        Width = 211
        Height = 21
        DataField = 'CB_CTA_VAL'
        DataSource = DataSource
        TabOrder = 2
      end
      object CB_SUB_CTA: TDBEdit
        Left = 106
        Top = 132
        Width = 211
        Height = 21
        DataField = 'CB_SUB_CTA'
        DataSource = DataSource
        TabOrder = 3
      end
      object CB_NETO: TZetaDBNumero
        Left = 106
        Top = 156
        Width = 97
        Height = 21
        Mascara = mnPesos
        TabOrder = 4
        Text = '0.00'
        DataField = 'CB_NETO'
        DataSource = DataSource
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 5
        Width = 419
        Height = 67
        TabOrder = 0
        object LBanca: TLabel
          Left = 13
          Top = 41
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = 'Banca Electr'#243'nica:'
        end
        object BtnBAN_ELE: TSpeedButton
          Left = 320
          Top = 36
          Width = 25
          Height = 25
          Hint = 
            'Verificar si el N'#250'mero de Banca Electr'#243'nica Pertenece a otro Emp' +
            'leado'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            555555555555555555555555555555555555555555FF55555555555559055555
            55555555577FF5555555555599905555555555557777F5555555555599905555
            555555557777FF5555555559999905555555555777777F555555559999990555
            5555557777777FF5555557990599905555555777757777F55555790555599055
            55557775555777FF5555555555599905555555555557777F5555555555559905
            555555555555777FF5555555555559905555555555555777FF55555555555579
            05555555555555777FF5555555555557905555555555555777FF555555555555
            5990555555555555577755555555555555555555555555555555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BtnBAN_ELEClick
        end
        object lBanco: TLabel
          Left = 69
          Top = 17
          Width = 34
          Height = 13
          Alignment = taRightJustify
          Caption = 'Banco:'
        end
        object CB_BAN_ELE: TDBEdit
          Left = 106
          Top = 37
          Width = 211
          Height = 21
          DataField = 'CB_BAN_ELE'
          DataSource = DataSource
          TabOrder = 1
        end
        object CB_BANCO: TZetaDBKeyLookup
          Left = 106
          Top = 13
          Width = 300
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_BANCO'
          DataSource = DataSource
        end
      end
    end
    object tsConfidencialidad: TTabSheet
      Caption = 'Confidencialidad'
      ImageIndex = 8
      object LblNivel0: TLabel
        Left = 22
        Top = 23
        Width = 81
        Height = 13
        Alignment = taRightJustify
        Caption = 'Confidencialidad:'
      end
      object CB_NIVEL0: TZetaDBKeyLookup
        Left = 106
        Top = 18
        Width = 238
        Height = 21
        LookupDataset = dmSistema.cdsNivel0
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 55
        DataField = 'CB_NIVEL0'
        DataSource = DataSource
      end
    end
    object tsDatosMedicos: TTabSheet
      Caption = 'Datos M'#233'dicos'
      ImageIndex = 7
      object lblTSangre: TLabel
        Left = 76
        Top = 19
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Sangre:'
      end
      object lblAlerPadec: TLabel
        Left = 36
        Top = 45
        Width = 116
        Height = 13
        Alignment = taRightJustify
        Caption = 'Al'#233'rgico / Padecimiento:'
      end
      object CB_ALERGIA: TDBEdit
        Left = 155
        Top = 41
        Width = 318
        Height = 21
        DataField = 'CB_ALERGIA'
        DataSource = DataSource
        TabOrder = 1
      end
      object CB_TSANGRE: TDBComboBox
        Left = 155
        Top = 15
        Width = 150
        Height = 21
        DataField = 'CB_TSANGRE'
        DataSource = DataSource
        ItemHeight = 13
        Items.Strings = (
          ''
          'O +'
          'O -'
          'A +'
          'A -'
          'B +'
          'B -'
          'AB +'
          'AB -')
        TabOrder = 0
      end
    end
    object tsBrigada: TTabSheet
      Caption = 'Brigadas de Protecci'#243'n Civil'
      ImageIndex = 6
      object lblTipoBrigada: TLabel
        Left = 127
        Top = 34
        Width = 24
        Height = 13
        Caption = 'Tipo:'
      end
      object lblNoPiso: TLabel
        Left = 93
        Top = 103
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'No. de Piso:'
      end
      object lblRol: TLabel
        Left = 132
        Top = 59
        Width = 19
        Height = 13
        Alignment = taRightJustify
        Caption = 'Rol:'
      end
      object lblConocimiento: TLabel
        Left = 84
        Top = 79
        Width = 67
        Height = 13
        Alignment = taRightJustify
        Caption = 'Conocimiento:'
      end
      object CB_BRG_TIP: TZetaDBKeyCombo
        Left = 155
        Top = 32
        Width = 207
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 1
        OnChange = CB_INFTIPOChange
        ListaFija = lfTipoBrigada
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'CB_BRG_TIP'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object CB_BRG_NOP: TDBEdit
        Left = 155
        Top = 101
        Width = 206
        Height = 21
        DataField = 'CB_BRG_NOP'
        DataSource = DataSource
        MaxLength = 30
        TabOrder = 6
      end
      object CB_BRG_ACT: TDBCheckBox
        Left = 28
        Top = 14
        Width = 140
        Height = 17
        Alignment = taLeftJustify
        BiDiMode = bdLeftToRight
        Caption = 'Pertenece a una brigada:'
        DataField = 'CB_BRG_ACT'
        DataSource = DataSource
        ParentBiDiMode = False
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_BRG_ACTClick
      end
      object CB_BRG_JEF: TDBCheckBox
        Left = 235
        Top = 57
        Width = 104
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'Jefe de Brigada'
        DataField = 'CB_BRG_JEF'
        DataSource = DataSource
        ParentBiDiMode = False
        TabOrder = 3
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_BRG_ACTClick
      end
      object CB_BRG_ROL: TDBCheckBox
        Left = 155
        Top = 57
        Width = 70
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'Brigadista'
        DataField = 'CB_BRG_ROL'
        DataSource = DataSource
        ParentBiDiMode = False
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_BRG_ACTClick
      end
      object CB_BRG_CON: TDBCheckBox
        Left = 155
        Top = 77
        Width = 66
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'Te'#243'rico'
        DataField = 'CB_BRG_CON'
        DataSource = DataSource
        ParentBiDiMode = False
        TabOrder = 4
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_BRG_ACTClick
      end
      object CB_BRG_PRA: TDBCheckBox
        Left = 235
        Top = 77
        Width = 102
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'Pr'#225'ctico'
        DataField = 'CB_BRG_PRA'
        DataSource = DataSource
        ParentBiDiMode = False
        TabOrder = 5
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_BRG_ACTClick
      end
    end
    object tsTimbrado: TTabSheet
      Caption = 'Timbrado de N'#243'mina'
      ImageIndex = 9
      object Label16: TLabel
        Left = 21
        Top = 21
        Width = 69
        Height = 13
        Caption = 'R'#233'gimen SAT:'
      end
      object ZetaDBKeyCombo1: TZetaDBKeyCombo
        Left = 98
        Top = 17
        Width = 300
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfTipoRegimenesSAT
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
        DataField = 'CB_REGIMEN'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 1
  end
end
