inherited EditHisPermiso_DevEx: TEditHisPermiso_DevEx
  Left = 374
  Top = 376
  Caption = 'Permisos'
  ClientHeight = 328
  ClientWidth = 409
  PixelsPerInch = 96
  TextHeight = 13
  object LIN_FEC_INI: TLabel [0]
    Left = 38
    Top = 61
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha Inicio:'
  end
  object LIN_DIAS: TLabel [1]
    Left = 73
    Top = 84
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = 'D'#237'as:'
  end
  object LIN_FEC_FIN: TLabel [2]
    Left = 56
    Top = 109
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Regresa:'
  end
  object Label13: TLabel [3]
    Left = 75
    Top = 156
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo:'
  end
  object Label11: TLabel [4]
    Left = 44
    Top = 180
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Referencia:'
  end
  object Label5: TLabel [5]
    Left = 25
    Top = 204
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observaciones:'
    FocusControl = PM_COMENTA
  end
  object UsuarioLbl: TLabel [6]
    Left = 56
    Top = 226
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modific'#243':'
  end
  object PM_CAPTURA: TZetaDBTextBox [7]
    Left = 105
    Top = 246
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'PM_CAPTURA'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'PM_CAPTURA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label2: TLabel [8]
    Left = 44
    Top = 248
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modificado:'
  end
  object Label3: TLabel [9]
    Left = 70
    Top = 133
    Width = 29
    Height = 13
    Caption = 'Clase:'
  end
  object PM_FEC_FIN: TZetaDBTextBox [10]
    Left = 105
    Top = 107
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'PM_FEC_FIN'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'PM_FEC_FIN'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object US_DESCRIP: TZetaDBTextBox [11]
    Left = 105
    Top = 224
    Width = 145
    Height = 17
    AutoSize = False
    Caption = 'US_DESCRIP'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_DESCRIP'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label1: TLabel [12]
    Left = 24
    Top = 268
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Registro Global:'
  end
  inherited PanelBotones: TPanel
    Top = 292
    Width = 409
    TabOrder = 7
    inherited OK_DevEx: TcxButton
      Left = 246
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 325
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 409
    TabOrder = 8
    inherited ValorActivo2: TPanel
      Width = 83
      inherited textoValorActivo2: TLabel
        Width = 77
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 14
  end
  object PM_COMENTA: TDBEdit [16]
    Left = 105
    Top = 200
    Width = 275
    Height = 21
    DataField = 'PM_COMENTA'
    DataSource = DataSource
    MaxLength = 40
    TabOrder = 6
  end
  object PM_TIPO: TZetaDBKeyLookup_DevEx [17]
    Left = 105
    Top = 152
    Width = 300
    Height = 21
    LookupDataset = dmTablas.cdsIncidencias
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 4
    TabStop = True
    WidthLlave = 60
    OnValidKey = PM_TIPOValidKey
    DataField = 'PM_TIPO'
    DataSource = DataSource
  end
  object PM_FEC_INI: TZetaDBFecha [18]
    Left = 105
    Top = 56
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 0
    Text = '17/dic/97'
    Valor = 35781.000000000000000000
    DataField = 'PM_FEC_INI'
    DataSource = DataSource
  end
  object PM_DIAS: TZetaDBNumero [19]
    Left = 105
    Top = 80
    Width = 65
    Height = 21
    Mascara = mnDias
    TabOrder = 1
    Text = '0'
    DataField = 'PM_DIAS'
    DataSource = DataSource
  end
  object PM_NUMERO: TDBEdit [20]
    Left = 105
    Top = 176
    Width = 130
    Height = 21
    DataField = 'PM_NUMERO'
    DataSource = DataSource
    TabOrder = 5
  end
  object PM_CLASIFI: TZetaDBKeyCombo [21]
    Left = 105
    Top = 128
    Width = 185
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 3
    OnChange = PM_CLASIFIChange
    ListaFija = lfTipoPermiso
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'PM_CLASIFI'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object PM_GLOBAL: TDBCheckBox [22]
    Left = 101
    Top = 266
    Width = 18
    Height = 17
    Alignment = taLeftJustify
    Ctl3D = False
    DataField = 'PM_GLOBAL'
    DataSource = DataSource
    ParentCtl3D = False
    ReadOnly = True
    TabOrder = 10
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object PM_FEC_REG: TZetaDBFecha [23]
    Left = 105
    Top = 104
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 2
    Text = '05/dic/12'
    Valor = 41248.000000000000000000
    DataField = 'PM_FEC_FIN'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 372
    Top = 52
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
