inherited EmpInscribirGridSelect: TEmpInscribirGridSelect
  Left = 380
  Top = 183
  Width = 500
  Height = 440
  Caption = 'Inscribir empleados a curso'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 366
    Width = 484
    TabOrder = 1
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 194
    Width = 484
    Height = 172
    TabOrder = 2
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 76
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        Title.Caption = 'Nombre'
        Width = 392
        Visible = True
      end>
  end
  inherited PanelSuperior: TPanel
    Top = 145
    Width = 484
    Height = 49
    TabOrder = 3
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 0
    Width = 484
    Height = 145
    Align = alTop
    TabOrder = 0
    object grMostrarEmpleados: TRadioGroup
      Left = 1
      Top = 1
      Width = 216
      Height = 72
      Caption = 'Mos&trar'
      Items.Strings = (
        'Empleados con el curso programado'
        'Empleados con el curso reprobado'
        'Todos')
      TabOrder = 1
    end
    object btnMostrar: TBitBtn
      Left = 304
      Top = 32
      Width = 113
      Height = 57
      Caption = '&Mostrar lista'
      TabOrder = 0
      OnClick = btnMostrarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
      Layout = blGlyphTop
    end
    object lTruncar: TCheckBox
      Left = 9
      Top = 118
      Width = 313
      Height = 17
      Caption = '&Al agregar lista limitar al cupo del grupo. '
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object btnFiltrosCondiciones: TBitBtn
      Left = 10
      Top = 80
      Width = 113
      Height = 33
      Caption = 'Filtro&s y Condiciones'
      TabOrder = 3
      OnClick = btnFiltrosCondicionesClick
      Layout = blGlyphTop
    end
  end
  inherited DataSource: TDataSource
    Left = 376
    Top = 160
  end
end
