unit FEditHisCierre_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, ZetaNumero, StdCtrls, Mask, ZetaFecha, ExtCtrls,
  Db, Buttons, ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditHisCierre_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    VA_D_GOZOLbl: TLabel;
    VA_D_PAGOLbl: TLabel;
    Label4: TLabel;
    VA_FEC_INI: TZetaDBFecha;
    VA_D_GOZO: TZetaDBNumero;
    VA_D_PAGO: TZetaDBNumero;
    Label2: TLabel;
    VA_GLOBAL: TDBCheckBox;
    VA_COMENTA: TDBMemo;
    VA_D_PRIMA: TZetaDBNumero;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Connect;override;
  public
    { Public declarations }
  end;

var
  EditHisCierre_DevEx: TEditHisCierre_DevEx;

implementation

uses dRecursos, ZetaCommonLists, ZetaCommonClasses, ZAccesosTress;

{$R *.DFM}

procedure TEditHisCierre_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H11511_Cerrar_vacaciones;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_VACA;
     FirstControl := VA_FEC_INI;
     TipoValorActivo1 := stEmpleado;
     VA_COMENTA.MaxLength := K_ANCHO_TITULO;
end;

procedure TEditHisCierre_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsHisVacacion.Conectar;
          DataSource.DataSet := cdsHisVacacion;
     end;
end;

end.
