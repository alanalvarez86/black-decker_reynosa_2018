inherited CapturaFoto: TCapturaFoto
  Left = 299
  Top = 189
  Caption = 'Capturar Imagen Desde Video'
  ClientHeight = 370
  ClientWidth = 387
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 334
    Width = 387
    inherited OK: TBitBtn
      Left = 225
      TabOrder = 2
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 304
      TabOrder = 3
    end
    object btnCapturar: TBitBtn
      Left = 86
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Congelar Imagen Del Video'
      Caption = 'Capturar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnCapturarClick
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000000000000000
        00000000000000000000000000000000000000000000808080FF000000FF0000
        00FF808080FF0000000000000000000000000000000000000000000000000000
        0000808080FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF808080FF000000000000
        0000000000FF808080FF808080FF000000FF000000FF808080FF808080FFFFFF
        00FFFFFF00FF000000FF000000FF808080FF808080FF000000FF000000000000
        0000000000FF808080FF808080FF000000FF808080FF808080FF808080FF8080
        80FF808080FFFFFF00FF000000FF808080FF808080FF000000FF000000000000
        0000000000FF808080FF808080FF000000FF808080FFFFFF00FF808080FF8080
        80FF808080FFFFFF00FF000000FF808080FF808080FF000000FF000000000000
        0000000000FF808080FF808080FF000000FF808080FFFFFFFFFFFFFF00FF8080
        80FF808080FF808080FF000000FF808080FF808080FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF808080FF808080FF8080
        80FF808080FF000000FF000000FFFFFFFFFFFFFFFFFF000000FF000000FFFFFF
        FFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFF000000FF000000FF000000FF0000
        00FF000000FF000000FFFFFFFFFFFFFFFFFFFFFFFFFF000000FF000000FFFFFF
        FFFF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFFFFF
        FFFFFFFFFFFF000000FF000000FFFFFFFFFF808080FF000000FFFFFFFFFFFFFF
        FFFF000000FF808080FF00000000000000FF000000FF00000000000000FFFFFF
        FFFF808080FF808080FF808080FFFFFFFFFF808080FF808080FF000000FF0000
        00FF808080FF0000000000000000000000000000000000000000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FF0000000000000000000000000000000000000000000000FFFFFF
        FFFF808080FF808080FF808080FFFFFFFFFF808080FF808080FF808080FFFFFF
        FFFF000000FF0000000000000000000000000000000000000000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FF0000000000000000000000000000000000000000800000FF8000
        00FF800000FF800000FF800000FF800000FF800000FF800000FF800000FF8000
        00FF800000FF0000000000000000000000000000000000000000800000FF8000
        00FF800000FF800000FF800000FF800000FF800000FF800000FF800000FF8000
        00FF800000FF0000000000000000000000000000000000000000}
    end
    object btnVideo: TBitBtn
      Left = 6
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Continuar Tomando Video'
      Caption = 'Video'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnVideoClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
        0000377777777777777703030303030303037F7F7F7F7F7F7F7F000000000000
        00007777777777777777933393303933337073F37F37F73F3377393393303393
        379037FF7F37F37FF777379793303379793037777337F3777737339933303339
        93303377F3F7F3F77F3733993930393993303377F737F7377FF7399993303399
        999037777337F377777793993330333393307377FF37F3337FF7333993303333
        993033377F37F33377F7333993303333993033377337F3337737333333303333
        33303FFFFFF7FFFFFFF700000000000000007777777777777777030303030303
        03037F7F7F7F7F7F7F7F00000000000000007777777777777777}
      NumGlyphs = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 19
    Width = 387
    Height = 87
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 60
      Top = 12
      Width = 24
      Height = 13
      Caption = 'Tipo:'
    end
    object Label2: TLabel
      Left = 10
      Top = 36
      Width = 74
      Height = 13
      Caption = 'Observaciones:'
    end
    object lblDriver: TLabel
      Left = 53
      Top = 61
      Width = 31
      Height = 13
      Caption = 'Driver:'
    end
    object IM_TIPO: TDBComboBox
      Left = 88
      Top = 8
      Width = 105
      Height = 21
      DataField = 'IM_TIPO'
      DataSource = DataSource1
      ItemHeight = 13
      Items.Strings = (
        'FOTO'
        'FIRMA'
        'HUELLA')
      TabOrder = 0
    end
    object IM_OBSERVA: TDBEdit
      Left = 88
      Top = 32
      Width = 245
      Height = 21
      DataField = 'IM_OBSERVA'
      DataSource = DataSource1
      TabOrder = 1
    end
    object cbDrivers: TComboBox
      Left = 88
      Top = 57
      Width = 245
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbDriversChange
    end
  end
  object Panel1: TPanel
    Left = 64
    Top = 114
    Width = 280
    Height = 209
    BevelInner = bvLowered
    TabOrder = 2
    object Documento: TPDBMultiImage
      Left = 7
      Top = 6
      Width = 266
      Height = 200
      ImageReadRes = lAutoMatic
      ImageWriteRes = sAutoMatic
      DataEngine = deBDE
      JPegSaveQuality = 75
      JPegSaveSmooth = 0
      ImageDither = True
      UpdateAsJPG = False
      UpdateAsBMP = False
      UpdateAsGIF = False
      UpdateAsPCX = False
      UpdateAsPNG = False
      UpdateAsTIF = True
      BorderStyle = bsNone
      Color = clBtnFace
      DataField = 'IM_BLOB'
      DataSource = DataSource1
      RichTools = False
      StretchRatio = True
      TabOrder = 0
      TextLeft = 0
      TextTop = 0
      TextRotate = 0
      TifSaveCompress = sNONE
      UseTwainWindow = False
    end
    object VideoCap1: TVideoCap
      Left = 7
      Top = 6
      Width = 266
      Height = 200
      color = clBlack
      DriverOpen = False
      DriverIndex = -1
      VideoOverlay = False
      VideoPreview = False
      PreviewScaleToWindow = True
      PreviewScaleProportional = True
      PreviewRate = 30
      MicroSecPerFrame = 66667
      FrameRate = 15
      CapAudio = False
      VideoFileName = 'Video.avi'
      SingleImageFile = 'Capture.bmp'
      CapTimeLimit = 0
      CapIndexSize = 0
      CapToFile = False
      CapAudioFormat.Channels = Stereo
      BufferFileSize = 0
    end
  end
  object pnValorActivo: TPanel
    Left = 0
    Top = 0
    Width = 387
    Height = 19
    Align = alTop
    Alignment = taLeftJustify
    BorderWidth = 8
    Caption = 'ValorActivo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  object DataSource1: TDataSource
    Left = 312
    Top = 16
  end
  object Timer: TTimer
    Enabled = False
    Interval = 1
    OnTimer = TimerTimer
    Left = 8
    Top = 136
  end
end
