unit FGridCuentasGasolina_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ExtCtrls, StdCtrls, ZetaSmartLists, ZBaseGridEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TTarjetasGasolina_DevEx = class(TBaseGridEdicion_DevEx)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEmpleado;
  protected
    procedure Connect; override;
    procedure Buscar; override;
  public
    { Public declarations }
  end;

var
  TarjetasGasolina_DevEx: TTarjetasGasolina_DevEx;

implementation

uses DCliente,
     DRecursos,
     ZetaCommonClasses,
     ZetaBuscaEmpleado_DevEx;

{$R *.DFM}

{ TBancaElectronica }

procedure TTarjetasGasolina_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_REG_TARJETAS_GASOLINA;
end;

procedure TTarjetasGasolina_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmRecursos do
     begin
          cdsTarjetasGasolina.Refrescar;
          DataSource.DataSet:= cdsTarjetasGasolina;
     end;
end;

procedure TTarjetasGasolina_DevEx.Buscar;
begin
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado;
     end;
end;

procedure TTarjetasGasolina_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( VACIO, sKey, sDescription ) then
     begin
          with dmRecursos.cdsTarjetasGasolina do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName('CB_CODIGO').AsString:= sKey;
          end;
     end;
end;

end.
