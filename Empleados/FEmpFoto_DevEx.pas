unit FEmpFoto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, ComCtrls, Db,
  ExtCtrls, ZetaDBTextBox, StdCtrls, DBCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxNavigator, cxDBNavigator, Menus, cxButtons,
  imageenview, ieview, dbimageen, imageen, IdBaseComponent, IdComponent,
  IdUDPBase, IdUDPClient, QuickRpt, QRCtrls, Qrdctrls;


type
  TImageEnDBView = class(TImageEnView);
  TEmpFoto_DevEx = class(TBaseConsulta)
    Panel2: TPanel;
    DBNavigator: TcxDBNavigator;
    Panel1: TPanel;
    Label1: TLabel;
    IM_TIPO: TZetaDBTextBox;
    IM_OBSERVA: TZetaDBTextBox;
    Label2: TLabel;
    btnCapturar: TcxButton;
    FOTO: TImageEnView;
    procedure FormCreate(Sender: TObject);
    procedure btnCapturarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
  end;


var
   EmpFoto_DevEx: TEmpFoto_DevEx;

implementation

uses dRecursos, ZetaCommonLists, ZetaCommonClasses, ZAccesosTress, ZAccesosMgr, FToolsImageEn;

{$R *.DFM}

procedure TEmpFoto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10122_Expediente_Foto_empleado;
end;

procedure TEmpFoto_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsEmpFoto.Conectar;
          DataSource.DataSet:= cdsEmpFoto;
     end;
end;

procedure TEmpFoto_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then     // Navegando
        FToolsImageEn.AsignaBlobAImagen( FOTO, dmRecursos.cdsEmpFoto, 'IM_BLOB' );
end;

procedure TEmpFoto_DevEx.Refresh;
begin
     dmRecursos.cdsEmpFoto.Refrescar;
     dmRecursos.cdsEmpFoto.First;
     FOTO.BorderStyle := bsSingle;
end;

procedure TEmpFoto_DevEx.Agregar;
begin
     dmRecursos.cdsEmpFoto.Agregar;
end;

procedure TEmpFoto_DevEx.Borrar;
begin
     dmRecursos.cdsEmpFoto.Borrar;
end;

procedure TEmpFoto_DevEx.Modificar;
begin
     dmRecursos.cdsEmpFoto.Modificar;
end;

procedure TEmpFoto_DevEx.btnCapturarClick(Sender: TObject);
begin
     inherited;
     dmRecursos.ObtenerFotografia;
end;

end.
