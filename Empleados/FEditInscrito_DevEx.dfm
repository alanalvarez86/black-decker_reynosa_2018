inherited EditInscrito_DevEx: TEditInscrito_DevEx
  Left = 310
  Top = 192
  Caption = 'Inscripci'#243'n'
  ClientHeight = 369
  PixelsPerInch = 96
  TextHeight = 13
  object US_CODIGO: TZetaDBTextBox [0]
    Left = 104
    Top = 311
    Width = 300
    Height = 17
    AutoSize = False
    Caption = 'US_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label2: TLabel [1]
    Left = 66
    Top = 60
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status:'
  end
  object Label5: TLabel [2]
    Left = 27
    Top = 161
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de Baja:'
  end
  object Label6: TLabel [3]
    Left = 34
    Top = 186
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Evaluaci'#243'n 1:'
  end
  object Label7: TLabel [4]
    Left = 34
    Top = 211
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Evaluaci'#243'n 2:'
  end
  object Label8: TLabel [5]
    Left = 34
    Top = 237
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Evaluaci'#243'n 3:'
  end
  object Label9: TLabel [6]
    Left = 18
    Top = 263
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Evaluaci'#243'n Final:'
  end
  object Label10: TLabel [7]
    Left = 38
    Top = 289
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Comentarios:'
  end
  object Label11: TLabel [8]
    Left = 56
    Top = 313
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modific'#243':'
  end
  inherited PanelBotones: TPanel
    Top = 333
    TabOrder = 10
  end
  inherited PanelIdentifica: TPanel
    TabOrder = 0
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 14
  end
  object IC_FEC_BAJ: TZetaDBFecha [12]
    Left = 104
    Top = 156
    Width = 121
    Height = 22
    Cursor = crArrow
    TabOrder = 3
    Text = '04/may/05'
    Valor = 38476.000000000000000000
    DataField = 'IC_FEC_BAJ'
    DataSource = DataSource
  end
  object IC_STATUS: TZetaDBKeyCombo [13]
    Left = 104
    Top = 56
    Width = 115
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 1
    ListaFija = lfStatusInscrito
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'IC_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object IC_EVA_1: TZetaDBNumero [14]
    Left = 104
    Top = 182
    Width = 121
    Height = 21
    Mascara = mnHoras
    TabOrder = 4
    Text = '0.00'
    DataField = 'IC_EVA_1'
    DataSource = DataSource
  end
  object IC_EVA_2: TZetaDBNumero [15]
    Left = 104
    Top = 207
    Width = 121
    Height = 21
    Mascara = mnHoras
    TabOrder = 5
    Text = '0.00'
    DataField = 'IC_EVA_2'
    DataSource = DataSource
  end
  object IC_EVA_3: TZetaDBNumero [16]
    Left = 104
    Top = 233
    Width = 121
    Height = 21
    Mascara = mnHoras
    TabOrder = 6
    Text = '0.00'
    DataField = 'IC_EVA_3'
    DataSource = DataSource
  end
  object IC_EVA_FIN: TZetaDBNumero [17]
    Left = 104
    Top = 259
    Width = 121
    Height = 21
    Mascara = mnHoras
    TabOrder = 7
    Text = '0.00'
    DataField = 'IC_EVA_FIN'
    DataSource = DataSource
  end
  object GroupBox1: TGroupBox [18]
    Left = 104
    Top = 79
    Width = 163
    Height = 73
    Caption = ' Inscripci'#243'n '
    TabOrder = 2
    object Label3: TLabel
      Left = 6
      Top = 24
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object Label4: TLabel
      Left = 13
      Top = 48
      Width = 26
      Height = 13
      Alignment = taRightJustify
      Caption = 'Hora:'
    end
    object IC_FEC_INS: TZetaDBFecha
      Left = 42
      Top = 19
      Width = 113
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '04/may/05'
      Valor = 38476.000000000000000000
      DataField = 'IC_FEC_INS'
      DataSource = DataSource
    end
    object IC_HOR_INS: TZetaDBHora
      Left = 42
      Top = 44
      Width = 38
      Height = 21
      EditMask = '99:99;0'
      TabOrder = 1
      Text = '    '
      Tope = 24
      Valor = '    '
      DataField = 'IC_HOR_INS'
      DataSource = DataSource
    end
  end
  object IC_COMENTA: TDBEdit [19]
    Left = 104
    Top = 285
    Width = 300
    Height = 21
    DataField = 'IC_COMENTA'
    DataSource = DataSource
    TabOrder = 8
  end
  inherited DataSource: TDataSource
    Left = 380
    Top = 9
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
