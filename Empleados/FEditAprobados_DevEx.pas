unit FEditAprobados_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, 
  ComCtrls, Mask, ZetaNumero, ZBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, ZetaKeyLookup_DevEx, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC;

type
  TEditAprobado_DevEx = class(TBaseEdicion_DevEx)
    PageControl: TcxPageControl;
    TabClasifi: TcxTabSheet;
    HorarioLbl: TLabel;
    PuestoLbl: TLabel;
    ClasificacionLbl: TLabel;
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    TabNiveles: TcxTabSheet;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    CB_NIVEL9: TZetaDBKeyLookup_DevEx;
    CB_NIVEL8: TZetaDBKeyLookup_DevEx;
    CB_NIVEL7: TZetaDBKeyLookup_DevEx;
    CB_NIVEL6: TZetaDBKeyLookup_DevEx;
    CB_NIVEL5: TZetaDBKeyLookup_DevEx;
    CB_NIVEL4: TZetaDBKeyLookup_DevEx;
    CB_NIVEL3: TZetaDBKeyLookup_DevEx;
    CB_NIVEL2: TZetaDBKeyLookup_DevEx;
    CB_NIVEL1: TZetaDBKeyLookup_DevEx;
    Label1: TLabel;
    KC_EVALUA: TZetaDBNumero;
    KC_HORAS: TZetaDBNumero;
    KC_HORASLbl: TLabel;
    CB_NIVEL10lbl: TLabel;
    CB_NIVEL10: TZetaDBKeyLookup_DevEx;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL11: TZetaDBKeyLookup_DevEx;
    CB_NIVEL12: TZetaDBKeyLookup_DevEx;
    CB_NIVEL12lbl: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure SetCamposNivel;
    procedure ObtieneNombreEmpleado;
    {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$ifend}
  protected
    { protected declarations }
    procedure Connect; override;
    procedure EscribirCambios;override;
    procedure DoCancelChanges;override;
    function CheckDerechos(const iDerecho: Integer): Boolean;override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  EditAprobado_DevEx: TEditAprobado_DevEx;

implementation

uses DTablas,
     DGlobal,
     DCatalogos,
     DRecursos,
     ZetaCommonClasses,
     ZetaCommonLists;

{$R *.DFM}

procedure TEditAprobado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     {$endif}
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H_APROBADOS;
     SetCamposNivel;

     CB_TURNO.LookupDataset := dmCatalogos.cdsTurnos;
     CB_PUESTO.LookupDataset := dmCatalogos.cdsPuestos;
     CB_CLASIFI.LookupDataset := dmCatalogos.cdsClasifi;
     CB_NIVEL1.LookupDataset := dmTablas.cdsNivel1;
     CB_NIVEL2.LookupDataset := dmTablas.cdsNivel2;
     CB_NIVEL3.LookupDataset := dmTablas.cdsNivel3;
     CB_NIVEL4.LookupDataset := dmTablas.cdsNivel4;
     CB_NIVEL5.LookupDataset := dmTablas.cdsNivel5;
     CB_NIVEL6.LookupDataset := dmTablas.cdsNivel6;
     CB_NIVEL7.LookupDataset := dmTablas.cdsNivel7;
     CB_NIVEL8.LookupDataset := dmTablas.cdsNivel8;
     CB_NIVEL9.LookupDataset := dmTablas.cdsNivel9;
     {$ifdef ACS}
     CB_NIVEL10.LookupDataset := dmTablas.cdsNivel10;
     CB_NIVEL11.LookupDataset := dmTablas.cdsNivel11;
     CB_NIVEL12.LookupDataset := dmTablas.cdsNivel12;
     {$endif}
end;

procedure TEditAprobado_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := TabClasifi;
     ObtieneNombreEmpleado;
     KC_EVALUA.SetFocus;
     {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     CambiosVisuales;
     {$ifend}
end;

procedure TEditAprobado_DevEx.ObtieneNombreEmpleado;
begin
     with dmRecursos.cdsAprobados do
     begin
          TipoValorActivo1 := StNinguno;
          ValorActivo1.Caption := FieldByName('CB_CODIGO').AsString + ': ' + FieldByName('PRETTYNAME').AsString;
     end;
end;

procedure TEditAprobado_DevEx.Connect;
begin
     with dmTablas do
     begin
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     with dmCatalogos do
     begin
          cdsClasifi.Conectar;
          cdsPuestos.Conectar;
          cdsTurnos.Conectar;
     end;
     DataSource.DataSet:= dmRecursos.cdsAprobados;

end;

procedure TEditAprobado_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12 );
     {$endif}
end;

function TEditAprobado_DevEx.CheckDerechos( const iDerecho: Integer): Boolean;
begin
     Result := True;
end;

procedure TEditAprobado_DevEx.DoCancelChanges;
begin
     ClientDataSet.Cancel;
end;

procedure TEditAprobado_DevEx.EscribirCambios;
begin
     ClientDataSet.Post;
end;

function TEditAprobado_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No Se Pueden Agregar Registros En Esta Pantalla';
     Result := False;
end;

function TEditAprobado_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No Se Pueden Borrar Registros En Esta Pantalla';
     Result := False;
end;

procedure TEditAprobado_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = Nil ) then
         ObtieneNombreEmpleado;
end;


{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TEditAprobado_DevEx.CambiosVisuales;
var
  I : Integer;
begin
     EditAprobado_DevEx.Width := K_WIDTH_SMALLFORM;
     for I := 0 to TabNiveles.ControlCount - 1 do
     begin
          if TabNiveles.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT - (TabNiveles.Controls[I].Width+2);
          end;
          if TabNiveles.Controls[I].ClassNameIs( 'TZetaDbKeyLookup_DevEx' ) then
          begin
               if ( TabNiveles.Controls[I].Visible ) then
               begin
                    TabNiveles.Controls[I].Width := K_WIDTH_LOOKUP;
                    TZetaDbKeyLookup_DevEx( TabNiveles.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                    TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT;
               end;
          end;
     end;
end;
{$ifend}

end.
