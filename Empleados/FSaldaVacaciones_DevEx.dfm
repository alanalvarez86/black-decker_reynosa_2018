inherited SaldaVacaciones_DevEx: TSaldaVacaciones_DevEx
  Caption = 'Saldar Vacaciones'
  ClientHeight = 238
  ClientWidth = 366
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 202
    Width = 366
    DesignSize = (
      366
      36)
    inherited OK_DevEx: TcxButton
      Left = 200
      OnClick = OKClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 280
      Cancel = True
      OnClick = CancelarClick
    end
  end
  object GroupBox1: TGroupBox [1]
    Left = 0
    Top = 0
    Width = 366
    Height = 202
    Align = alClient
    TabOrder = 1
    object GroupBox2: TGroupBox
      Left = 24
      Top = 96
      Width = 177
      Height = 97
      TabOrder = 3
      object PagadosLbl: TLabel
        Left = 44
        Top = 17
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Por Pagar:'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object GozadosLbl: TLabel
        Left = 44
        Top = 40
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Por Gozar:'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object PrimaLbl: TLabel
        Left = 9
        Top = 64
        Width = 85
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima Vacacional:'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DerechoPago: TZetaNumero
        Left = 101
        Top = 13
        Width = 50
        Height = 21
        Enabled = False
        Mascara = mnDiasFraccion
        TabOrder = 0
        Text = '0.00'
      end
      object DerechoGozo: TZetaNumero
        Left = 101
        Top = 36
        Width = 50
        Height = 21
        Enabled = False
        Mascara = mnDiasFraccion
        TabOrder = 1
        Text = '0.00'
      end
      object DerechoPrima: TZetaNumero
        Left = 101
        Top = 60
        Width = 50
        Height = 21
        Enabled = False
        Mascara = mnDiasFraccion
        TabOrder = 2
        Text = '0.00'
      end
    end
    object SaldaBaja: TcxRadioButton
      Tag = 1
      Left = 24
      Top = 45
      Width = 153
      Height = 17
      Caption = 'Saldar a la Baja'
      TabOrder = 1
      OnClick = SaldaBajaReingresoClick
      Transparent = True
    end
    object SaldaReingreso: TcxRadioButton
      Left = 24
      Top = 16
      Width = 153
      Height = 17
      Caption = 'Saldar en Cero'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = SaldaBajaReingresoClick
      Transparent = True
    end
    object gbSaldos: TGroupBox
      Left = 212
      Top = 8
      Width = 125
      Height = 81
      Caption = 'Saldo al Reingreso:'
      TabOrder = 4
      object Label5: TLabel
        Left = 21
        Top = 21
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Gozo:'
      end
      object ZSaldoGozo: TZetaTextBox
        Left = 53
        Top = 19
        Width = 60
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label10: TLabel
        Left = 21
        Top = 40
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pago:'
      end
      object ZSaldoPago: TZetaTextBox
        Left = 53
        Top = 38
        Width = 60
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object ZSaldoPrima: TZetaTextBox
        Left = 53
        Top = 57
        Width = 60
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label11: TLabel
        Left = 20
        Top = 59
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima:'
      end
    end
    object EditaSaldo: TcxRadioButton
      Tag = 2
      Left = 24
      Top = 72
      Width = 153
      Height = 17
      Caption = 'Editar Saldo al Reingreso'
      TabOrder = 2
      OnClick = SaldaBajaReingresoClick
      Transparent = True
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
