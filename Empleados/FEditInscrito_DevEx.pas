unit FEditInscrito_DevEx;

interface

uses
  Windows, Messages, SysUtils, {$ifdef 130}Variants,{$endif} Classes, Graphics, Controls, Forms,
  Dialogs, ZetaHora, StdCtrls, ZetaDBTextBox, ZetaNumero,
  ZetaKeyCombo, Mask, ZetaFecha, DB, ExtCtrls, DBCtrls,
  ZetaSmartLists, Buttons, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditInscrito_DevEx = class(TBaseEdicion_DevEx)
    IC_FEC_BAJ: TZetaDBFecha;
    IC_STATUS: TZetaDBKeyCombo;
    IC_EVA_1: TZetaDBNumero;
    IC_EVA_2: TZetaDBNumero;
    IC_EVA_3: TZetaDBNumero;
    IC_EVA_FIN: TZetaDBNumero;
    US_CODIGO: TZetaDBTextBox;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    IC_FEC_INS: TZetaDBFecha;
    IC_HOR_INS: TZetaDBHora;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    IC_COMENTA: TDBEdit;
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ObtieneNombreEmpleado;
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure EscribirCambios; override;
    procedure DoCancelChanges; override;

    function CheckDerechos(const iDerecho: Integer): Boolean;override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;

  public
    { Public declarations }
  end;

var
  EditInscrito_DevEx: TEditInscrito_DevEx;

implementation
uses DRecursos,
     ZetaCommonClasses,
     ZetaCommonLists;


{$R *.dfm}

procedure TEditInscrito_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_INSCRITOS;
     TipoValorActivo1 := stEmpleado;
     FirstControl := IC_STATUS;
end;

procedure TEditInscrito_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ObtieneNombreEmpleado;
end;

procedure TEditInscrito_DevEx.Connect;
begin
     with dmRecursos do
     begin
          DataSource.DataSet:= cdsInscritos;
     end;
     
end;

procedure TEditInscrito_DevEx.ObtieneNombreEmpleado;
begin
     with dmRecursos.cdsInscritos do
     begin
          TipoValorActivo1 := StNinguno;
          ValorActivo1.Caption := FieldByName('CB_CODIGO').AsString + ': ' + FieldByName('PRETTYNAME').AsString;
     end;
end;

function TEditInscrito_DevEx.CheckDerechos( const iDerecho: Integer): Boolean;
begin
     Result := True;
end;

procedure TEditInscrito_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     if ( Field = Nil ) then
         ObtieneNombreEmpleado;
end;


function TEditInscrito_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := FALSE;
     sMensaje := 'No Se Pueden Agregar Registros En Esta Pantalla';
end;

function TEditInscrito_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := FALSE;
     sMensaje := 'No Se Pueden Borrar Registros En Esta Pantalla';
end;

procedure TEditInscrito_DevEx.DoCancelChanges;
begin
     ClientDataSet.Cancel;
end;

procedure TEditInscrito_DevEx.EscribirCambios;
begin
     ClientDataSet.Post;
end;

end.





