inherited HisSegurosGastosMedicos: THisSegurosGastosMedicos
  Left = 114
  Top = 162
  Caption = 'Seguros de Gastos M'#233'dicos'
  ClientHeight = 287
  ClientWidth = 1271
  PixelsPerInch = 96
  TextHeight = 13
  object ZetaDBGrid1: TZetaDBGrid [0]
    Left = 0
    Top = 91
    Width = 1271
    Height = 196
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'EP_ORDEN'
        Title.Caption = 'Orden'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PM_NUMERO'
        Title.Caption = '# de P'#243'liza'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PV_REFEREN'
        Title.Caption = 'Referencia'
        Width = 61
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PM_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 187
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_CERTIFI'
        Title.Caption = 'Certificado'
        Width = 81
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_TIPO'
        Title.Caption = 'Tipo'
        Width = 77
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_DEPEND'
        Title.Caption = 'Pariente'
        Width = 203
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PA_RELACIO'
        Title.Caption = 'Parentesco'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_STATUS'
        Title.Caption = 'Status'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_FEC_INI'
        Title.Caption = 'Inicio'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_FEC_FIN'
        Title.Caption = 'Fin'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EP_CAN_FEC'
        Title.Caption = 'Cancelaci'#243'n'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_DESCRIP'
        Title.Caption = 'Modific'#243
        Width = 131
        Visible = True
      end>
  end
  inherited PanelIdentifica: TPanel
    Width = 1271
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 945
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 1271
    Height = 72
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 5
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 19
      Top = 27
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object Label3: TLabel
      Left = 11
      Top = 51
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = '# P'#243'liza:'
    end
    object lkpSGMS: TZetaKeyLookup
      Left = 56
      Top = 3
      Width = 300
      Height = 21
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
    end
    object zcbStatus: TZetaKeyCombo
      Left = 55
      Top = 25
      Width = 145
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      ListaFija = lfStatusSGM
      ListaVariable = lvPuesto
      Offset = -1
      Opcional = False
      EsconderVacios = False
    end
    object btnRefrescar: TBitBtn
      Left = 368
      Top = 3
      Width = 89
      Height = 41
      Caption = '&Refrescar'
      TabOrder = 2
      OnClick = btnRefrescarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555555555555555555555555555555555555555FF55555555555559055555
        55555555577FF5555555555599905555555555557777F5555555555599905555
        555555557777FF5555555559999905555555555777777F555555559999990555
        5555557777777FF5555557990599905555555777757777F55555790555599055
        55557775555777FF5555555555599905555555555557777F5555555555559905
        555555555555777FF5555555555559905555555555555777FF55555555555579
        05555555555555777FF5555555555557905555555555555777FF555555555555
        5990555555555555577755555555555555555555555555555555}
      NumGlyphs = 2
    end
    object txtPoliza: TZetaEdit
      Left = 55
      Top = 48
      Width = 144
      Height = 21
      TabOrder = 3
    end
  end
  inherited DataSource: TDataSource
    Left = 504
    Top = 24
  end
end
