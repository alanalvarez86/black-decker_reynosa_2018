unit FWizPlazas_DevEx;

interface

uses
  Windows, Messages, SysUtils,
  {$ifndef VER130}
  Variants,
  {$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZcxBaseWizard, StdCtrls, Mask, ZetaFecha, ZetaKeyCombo, ZetaEdit,
  ComCtrls, Buttons, ZetaWizard, ExtCtrls, DBCtrls,
  ZetaNumero, Grids, DBGrids, ZetaDBGrid, DB, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, ZetaCXWizard, cxGroupBox, cxLabel, dxGDIPlusClasses,
  dxCustomWizardControl, dxWizardControl, cxImage, ZetaKeyLookup_DevEx,
  Menus, cxButtons;

type
  TWizPlazas_DevEx = class(TcxBaseWizard)
    lblPuesto: TLabel;
    PU_CODIGO: TZetaKeyLookup_DevEx;
    Label5: TLabel;
    PL_CODIGO: TZetaEdit;
    PL_NOMBRE: TZetaEdit;
    Label6: TLabel;
    PL_REPORTA: TZetaKeyLookup_DevEx;
    Label16: TLabel;
    PL_TIREP: TZetaKeyCombo;
    Label2: TLabel;
    NumeroPlazas: TZetaNumero;
    GroupBox1: TGroupBox;
    lblVence: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    PL_FEC_FIN: TZetaFecha;
    ckVence: TCheckBox;
    PL_FEC_INI: TZetaFecha;
    PL_TIPO: TZetaKeyCombo;
    Contratacion: TdxWizardControlPage;
    PL_NOMINA: TZetaKeyCombo;
    PL_CLASIFI: TZetaKeyLookup_DevEx;
    PL_TURNO: TZetaKeyLookup_DevEx;
    PL_PATRON: TZetaKeyLookup_DevEx;
    PL_CONTRAT: TZetaKeyLookup_DevEx;
    PL_AREA: TZetaKeyLookup_DevEx;
    PL_NIVEL0: TZetaKeyLookup_DevEx;
    PL_CHECA: TCheckBox;
    Label9: TLabel;
    Label4: TLabel;
    Label26: TLabel;
    Label29: TLabel;
    Label27: TLabel;
    Arealbl: TLabel;
    lblConfidencialidad: TLabel;
    Label21: TLabel;
    Area: TdxWizardControlPage;
    PL_NIVEL12: TZetaKeyLookup_DevEx;
    PL_NIVEL11: TZetaKeyLookup_DevEx;
    PL_NIVEL10: TZetaKeyLookup_DevEx;
    PL_NIVEL9: TZetaKeyLookup_DevEx;
    PL_NIVEL8: TZetaKeyLookup_DevEx;
    PL_NIVEL7: TZetaKeyLookup_DevEx;
    PL_NIVEL6: TZetaKeyLookup_DevEx;
    PL_NIVEL5: TZetaKeyLookup_DevEx;
    PL_NIVEL4: TZetaKeyLookup_DevEx;
    PL_NIVEL3: TZetaKeyLookup_DevEx;
    PL_NIVEL2: TZetaKeyLookup_DevEx;
    PL_NIVEL1: TZetaKeyLookup_DevEx;
    PL_NIVEL12lbl: TLabel;
    PL_NIVEL11lbl: TLabel;
    PL_NIVEL10lbl: TLabel;
    PL_NIVEL9lbl: TLabel;
    PL_NIVEL8lbl: TLabel;
    PL_NIVEL7lbl: TLabel;
    PL_NIVEL6lbl: TLabel;
    PL_NIVEL5lbl: TLabel;
    PL_NIVEL4lbl: TLabel;
    PL_NIVEL3lbl: TLabel;
    PL_NIVEL2lbl: TLabel;
    PL_NIVEL1lbl: TLabel;
    Salario: TdxWizardControlPage;
    PL_ZONA_GE: TZetaKeyCombo;
    PL_TABLASS: TZetaKeyLookup_DevEx;
    PL_PER_VAR: TZetaNumero;
    PL_SALARIO: TZetaNumero;
    PL_AUTOSAL: TCheckBox;
    Label31: TLabel;
    Label28: TLabel;
    Label33: TLabel;
    LblSalario: TLabel;
    LblTabulador: TLabel;
    Generales: TdxWizardControlPage;
    PL_SUB_CTA: TEdit;
    PL_TEXTO: TEdit;
    PL_NUMERO: TZetaNumero;
    PL_INGLES: TEdit;
    Label10: TLabel;
    Label8: TLabel;
    CO_NUMEROLbl: TLabel;
    Label7: TLabel;
    Titulares: TdxWizardControlPage;
    gridEmpleados: TZetaDBGrid;
    Panel2: TPanel;
    DataSource: TDataSource;
    btnFinalizar: TcxButton;
    BBAgregar: TcxButton;
    BBBorrar: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;var CanMove: Boolean);
    procedure PU_CODIGOValidKey(Sender: TObject);
    procedure ckVenceClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure gridEmpleadosKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure WizardAfterMove(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure PL_AUTOSALClick(Sender: TObject);
    procedure PL_NIVEL12ValidLookup(Sender: TObject);
    procedure PL_NIVEL11ValidLookup(Sender: TObject);
    procedure PL_NIVEL10ValidLookup(Sender: TObject);
    procedure PL_NIVEL9ValidLookup(Sender: TObject);
    procedure PL_NIVEL8ValidLookup(Sender: TObject);
    procedure PL_NIVEL7ValidLookup(Sender: TObject);
    procedure PL_NIVEL6ValidLookup(Sender: TObject);
    procedure PL_NIVEL5ValidLookup(Sender: TObject);
    procedure PL_NIVEL4ValidLookup(Sender: TObject);
    procedure PL_NIVEL3ValidLookup(Sender: TObject);
    procedure PL_NIVEL2ValidLookup(Sender: TObject);
    procedure btnFinalizarClick(Sender: TObject);
    procedure gridEmpleadosDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean); //DevEx(by am)
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    FFiltroEmpleado : string;
    FReportaA: integer;
    FFinalizar:Boolean;
    procedure Connect;
    procedure DoConnect;
    procedure CargaDataSet;
    procedure SetCamposNivel;
    procedure HabilitaFecha;
    procedure BorraCelda;
    procedure Buscar;
    procedure SeleccionaSiguienteRenglon;
    procedure SetCycleControl;
    procedure DefaultsDePuesto;
    procedure CreaListaPlazas;
    {$ifdef ACS}procedure SetPosicionNiveles;{ACS}{$endif}
    {$ifdef ACS}
    procedure LimpiaLookUpOlfKey;{ACS}
    {$endif}
    {$if Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$endif}
  public
    { Public declarations }
    property ReportaA: integer read FReportaA write FReportaA;
  protected
    { Protected declarations }
    procedure CargaParametros;override;
    function EjecutarWizard: Boolean; override;
  end;
  TDBGridHack = class(TDBGrid); //declared
var
  WizPlazas_DevEx: TWizPlazas_DevEx;

{$ifdef ACS}
const
     K_FORMA_ACS = 460;
     K_ALT_DEF = 23;
{$endif}

implementation
uses
    dGlobal,
    ZGlobalTress,
    ZetaCommonLists,
    ZetaClientTools,
    ZetaCommonTools,
    ZetaCommonClasses,
    dCatalogos,
    dRecursos,
    ZetaBuscaEmpleado_DevEx,
    ZetaDialogo,
    dSistema,
    dTablas,
    DCliente;

{$R *.dfm}

procedure TWizPlazas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     PL_NIVEL12lbl.Top := PL_NIVEL1lbl.Top;
     PL_NIVEL11lbl.Top := PL_NIVEL12lbl.Top + K_ALT_DEF;
     PL_NIVEL10lbl.Top := PL_NIVEL11lbl.Top + K_ALT_DEF;
     PL_NIVEL9lbl.Top   := PL_NIVEL10lbl.Top + K_ALT_DEF;
     PL_NIVEL8lbl.Top  := PL_NIVEL9lbl.Top + K_ALT_DEF;
     PL_NIVEL7lbl.Top  := PL_NIVEL8lbl.Top + K_ALT_DEF;
     PL_NIVEL6lbl.Top  := PL_NIVEL7lbl.Top + K_ALT_DEF;
     PL_NIVEL5lbl.Top  := PL_NIVEL6lbl.Top + K_ALT_DEF;
     PL_NIVEL4lbl.Top  := PL_NIVEL5lbl.Top + K_ALT_DEF;
     PL_NIVEL3lbl.Top  := PL_NIVEL4lbl.Top + K_ALT_DEF;
     PL_NIVEL2lbl.Top  := PL_NIVEL3lbl.Top + K_ALT_DEF;
     PL_NIVEL1lbl.Top  := PL_NIVEL2lbl.Top + K_ALT_DEF;

     PL_NIVEL12.Top := PL_NIVEL1.Top;
     PL_NIVEL11.Top := PL_NIVEL12.Top + K_ALT_DEF;
     PL_NIVEL10.Top := PL_NIVEL11.Top + K_ALT_DEF;
     PL_NIVEL9.Top   := PL_NIVEL10.Top + K_ALT_DEF;
     PL_NIVEL8.Top  := PL_NIVEL9.Top + K_ALT_DEF;
     PL_NIVEL7.Top  := PL_NIVEL8.Top + K_ALT_DEF;
     PL_NIVEL6.Top  := PL_NIVEL7.Top + K_ALT_DEF;
     PL_NIVEL5.Top  := PL_NIVEL6.Top + K_ALT_DEF;
     PL_NIVEL4.Top  := PL_NIVEL5.Top + K_ALT_DEF;
     PL_NIVEL3.Top  := PL_NIVEL4.Top + K_ALT_DEF;
     PL_NIVEL2.Top  := PL_NIVEL3.Top + K_ALT_DEF;
     PL_NIVEL1.Top  := PL_NIVEL2.Top + K_ALT_DEF;

     PL_NIVEL12.TabOrder := 1;
     PL_NIVEL11.TabOrder := 2;
     PL_NIVEL10.TabOrder := 3;
     PL_NIVEL9.TabOrder := 4;
     PL_NIVEL8.TabOrder := 5;
     PL_NIVEL7.TabOrder := 6;
     PL_NIVEL6.TabOrder := 7;
     PL_NIVEL5.TabOrder := 8;
     PL_NIVEL4.TabOrder := 9;
     PL_NIVEL3.TabOrder := 10;
     PL_NIVEL2.TabOrder := 11;
     PL_NIVEL1.TabOrder := 12;

     Self.Height := K_FORMA_ACS;
     {
     PL_NIVEL10.Visible := True;
     PL_NIVEL11.Visible := True;
     PL_NIVEL12.Visible := True;
     PL_NIVEL10lbl.Visible := True;
     PL_NIVEL11lbl.Visible := True;
     PL_NIVEL12lbl.Visible := True;
     }
     {$endif}
     HelpContext:= H_EMP_WIZ_PLAZA;
     ckVence.Checked:= False;
     DataSource.DataSet := dmRecursos.cdsListaEmpPlazas;
     gridEmpleados.DataSource := DataSource;
     with dmCatalogos do
     begin
          PL_AREA.LookupDataSet := cdsAreas;
          PU_CODIGO.LookupDataSet := cdsPuestos;
          PL_CLASIFI.LookupDataSet := cdsClasifi;
          PL_TURNO.LookupDataSet :=  cdsTurnos;
          PL_PATRON.LookupDataSet := cdsRPatron;
          PL_CONTRAT.LookupDataSet := cdsContratos;
          PL_TABLASS.LookupDataset := cdsSSocial;
     end;
     PL_NIVEL0.LookupDataset  := dmSistema.cdsNivel0;
     with dmTablas do
     begin
          {$ifdef ACS}
          PL_NIVEL12.LookupDataset := cdsNivel12;
          PL_NIVEL11.LookupDataset := cdsNivel11;
          PL_NIVEL10.LookupDataset := cdsNivel10;
          {$endif}
          PL_NIVEL9.LookupDataset  := cdsNivel9;
          PL_NIVEL8.LookupDataset  := cdsNivel8;
          PL_NIVEL7.LookupDataset  := cdsNivel7;
          PL_NIVEL6.LookupDataset  := cdsNivel6;
          PL_NIVEL5.LookupDataset  := cdsNivel5;
          PL_NIVEL4.LookupDataset  := cdsNivel4;
          PL_NIVEL3.LookupDataset  := cdsNivel3;
          PL_NIVEL2.LookupDataset  := cdsNivel2;
          PL_NIVEL1.LookupDataset  := cdsNivel1;
     end;
     PL_REPORTA.LookUpDataSet := dmRecursos.cdsPlazasLookup;

     //DevEx(by am): Como se agregaron paginas en la implementacion determinar que la ultima pagina debe ser Ejecucion
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
     Advertencia.Caption :='Se crearan las nuevas plazas con los par�metros capturados. Presione el bot�n ''Ejecutar'' para iniciar el proceso.';


     //DevEx (by am):Colores Base para el grid de Edicion
     gridEmpleados.FixedColor := RGB(235,235,235);//Gris
     gridEmpleados.TitleFont.Color := RGB(77,59,75);//Gris-Morado tono fuerte
     gridEmpleados.Font.Color := RGB(156,129,139);//Gris-Morado
     //DevEx (by am): Asigna el evento OnMouseWheel al grid de edicion
     TDBGridHack( gridEmpleados ).OnMouseWheel:= MyMouseWheel;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TWizPlazas_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     LimpiaLookUpOlfKey;
     {$endif}
     //PageControl.ActivePage := Parametros;
     PU_CODIGO.SetFocus;
     SetCamposNivel;
     DoConnect;
     PL_FEC_INI.Valor := dmCliente.FechaDefault;
     PL_FEC_FIN.Valor := dmCliente.FechaDefault + 1;
     NumeroPlazas.Valor := 1;
     PL_TIPO.ItemIndex := Ord( jitpPlanta );
     PL_TIREP.ItemIndex := Ord( jiprLinea );
     PL_REPORTA.Valor := FReportaA;
     HabilitaFecha;
     FFiltroEmpleado:= VACIO;
//   dmRecursos.PuestoPlaza := VACIO;
//     PL_NOMINA.Valor := Ord( dmCliente.PeriodoTipo );
     PL_NOMINA.ListaFija:=lfTipoPeriodoConfidencial; //acl
     PL_NOMINA.Llave := IntToStr( Ord( dmCliente.PeriodoTipo ) );//acl   
     {$ifdef ACS}SetPosicionNiveles;{ACS}{$endif}
     FFinalizar := False;
     {$if Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12) or Defined(ANCHO_NIVELES_12)}
     CambiosVisuales;
     {$endif}
end;

//DevEx(by am): Agregado para marcar el renglon donde va el cursor de color lila
procedure TWizPlazas_DevEx.MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
     Handled := True;
     with gridEmpleados.DataSource.DataSet do
     begin
          if WheelDelta > 0 then
             Prior
          else
              Next;
     end;
end;

procedure TWizPlazas_DevEx.Connect;
begin
     with dmTablas do
     begin
          if PL_NIVEL1.Visible then cdsNivel1.Conectar;
          if PL_NIVEL2.Visible then cdsNivel2.Conectar;
          if PL_NIVEL3.Visible then cdsNivel3.Conectar;
          if PL_NIVEL4.Visible then cdsNivel4.Conectar;
          if PL_NIVEL5.Visible then cdsNivel5.Conectar;
          if PL_NIVEL6.Visible then cdsNivel6.Conectar;
          if PL_NIVEL7.Visible then cdsNivel7.Conectar;
          if PL_NIVEL8.Visible then cdsNivel8.Conectar;
          if PL_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if PL_NIVEL10.Visible then cdsNivel10.Conectar;
          if PL_NIVEL11.Visible then cdsNivel11.Conectar;
          if PL_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmRecursos do
     begin
          cdsPlazasLookup.Conectar;
     end;

     with dmSistema do
     begin
          cdsNivel0.Conectar;
     end;

     with dmCatalogos do
     begin
          cdsAreas.Conectar;
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
          cdsTurnos.Conectar;
          cdsRPatron.Conectar;
          cdsContratos.Conectar;
          cdsSSocial.Conectar;
//          cdsTPeriodos.Conectar;
     end;
end;

procedure TWizPlazas_DevEx.DoConnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
             except
                   on Error: Exception do
                      zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
             end;
          finally
                 Cursor := oCursor;
          end;
     end;

end;

procedure TWizPlazas_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, PL_NIVEL1lbl, PL_NIVEL1 );
     SetCampoNivel( 2, iNiveles, PL_NIVEL2lbl, PL_NIVEL2 );
     SetCampoNivel( 3, iNiveles, PL_NIVEL3lbl, PL_NIVEL3 );
     SetCampoNivel( 4, iNiveles, PL_NIVEL4lbl, PL_NIVEL4 );
     SetCampoNivel( 5, iNiveles, PL_NIVEL5lbl, PL_NIVEL5 );
     SetCampoNivel( 6, iNiveles, PL_NIVEL6lbl, PL_NIVEL6 );
     SetCampoNivel( 7, iNiveles, PL_NIVEL7lbl, PL_NIVEL7 );
     SetCampoNivel( 8, iNiveles, PL_NIVEL8lbl, PL_NIVEL8 );
     SetCampoNivel( 9, iNiveles, PL_NIVEL9lbl, PL_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, PL_NIVEL10lbl, PL_NIVEL10 );
     SetCampoNivel( 11, iNiveles, PL_NIVEL11lbl, PL_NIVEL11 );
     SetCampoNivel( 12, iNiveles, PL_NIVEL12lbl, PL_NIVEL12 );
     {$endif}
end;

procedure TWizPlazas_DevEx.HabilitaFecha;
const
     K_UN_DIA = 1;
begin
     PL_FEC_FIN.Enabled := ckVence.Checked;
     lblVence.Enabled := ckVence.Checked;
     if ckVence.Checked and ( PL_FEC_FIN.Valor = NullDateTime ) then
        PL_FEC_FIN.Valor := PL_FEC_INI.Valor + K_UN_DIA;
end;

procedure TWizPlazas_DevEx.WizardBeforeMove(Sender: TObject;var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if  Adelante then
          begin
               if EsPaginaActual ( Parametros ) then
               begin
                    if StrVacio( PU_CODIGO.Llave ) then
                    begin
                         CanMove := Error('El puesto no puede quedar vac�o',PU_CODIGO)
                    end
                    else
                    if CanMove and StrVacio( PL_NOMBRE.Text ) then
                    begin
                         CanMove := Error( 'La Descripci�n no puede quedar vac�a',PL_NOMBRE )
                    end
                    else
                    if ( CanMove and ( NumeroPlazas.ValorEntero <= 0 ) )then
                    begin
                         CanMove := Error( 'El n�mero de plazas no puede ser 0 (Cero)',NumeroPlazas )
                    end
                    else
                    if ( CanMove and ckVence.Checked and( PL_FEC_FIN.Valor <= PL_FEC_INI.Valor ) )then
                    begin
                         CanMove := Error( 'La fecha final de vigencia debe ser mayor a la fecha de inicio',PL_FEC_FIN )
                    end;
                    if CanMove then
                    begin
                         PL_NIVEL0.Enabled := dmSistema.HayNivel0;
                         lblConfidencialidad.Enabled := PL_NIVEL0.Enabled;
                         if FFinalizar then
                         begin
                              CreaListaPlazas;
                              iNewPage := Titulares.PageIndex;
                              //Anterior.Enabled := True;
                         end;
                         //btnFinalizar.Visible := False;
                        if PL_NIVEL0.Enabled then
                        begin
                             if StrLleno( dmCliente.Confidencialidad ) then
                             begin
                                  PL_NIVEL0.Filtro :=  Format( 'TB_CODIGO in %s', [dmCliente.ConfidencialidadListaIN] ) ;
                             end
                             else
                             begin
                                  PL_NIVEL0.Filtro := VACIO;
                             end;
                         end;
                    end;
               end;
               if EsPaginaActual ( Contratacion ) and dmCliente.UsaPlazas then
               begin
                    if StrVacio( PL_CLASIFI.Llave ) then
                    begin
                         CanMove := Error('La clasificaci�n no puede quedar vac�a',PL_CLASIFI)
                    end
                    else
                    if CanMove and StrVacio( PL_TURNO.Llave ) then
                    begin
                         CanMove := Error('El Turno no puede quedar vac�o',PL_TURNO)
                    end
                    else
                    if CanMove and StrVacio( PL_PATRON.Llave ) then
                    begin
                         CanMove := Error('El Registro patronal no puede quedar vac�o',PL_PATRON)
                    end;
               end;
               if EsPaginaActual ( Area ) and dmCliente.UsaPlazas  then
               begin
                    if PL_NIVEL1lbl.Visible and StrVacio( PL_NIVEL1.Llave ) then
                    begin
                         CanMove := Error('El nivel '+ StrLeft(PL_NIVEL1lbl.Caption,Length( PL_NIVEL1lbl.Caption )-1 ) +' no puede quedar vac�o',PL_NIVEL1)
                    end
                    else
                    if CanMove and PL_NIVEL2lbl.Visible and StrVacio( PL_NIVEL2.Llave ) then
                    begin
                         CanMove := Error('El nivel '+ StrLeft(PL_NIVEL2lbl.Caption,Length( PL_NIVEL2lbl.Caption )-1 ) +' no puede quedar vac�o',PL_NIVEL2)
                    end
                    else
                    if CanMove and PL_NIVEL3lbl.Visible and StrVacio( PL_NIVEL3.Llave ) then
                    begin
                         CanMove := Error('El nivel '+ StrLeft(PL_NIVEL3lbl.Caption,Length( PL_NIVEL3lbl.Caption )-1 ) +' no puede quedar vac�o',PL_NIVEL3)
                    end
                    else
                    if CanMove and PL_NIVEL4lbl.Visible and StrVacio( PL_NIVEL4.Llave ) then
                    begin
                         CanMove := Error('El nivel '+ StrLeft(PL_NIVEL4lbl.Caption,Length( PL_NIVEL4lbl.Caption )-1 ) +' no puede quedar vac�o',PL_NIVEL4)
                    end
                    else
                    if CanMove and PL_NIVEL5lbl.Visible and StrVacio( PL_NIVEL5.Llave ) then
                    begin
                         CanMove := Error('El nivel '+ StrLeft(PL_NIVEL5lbl.Caption,Length( PL_NIVEL5lbl.Caption )-1 ) +' no puede quedar vac�o',PL_NIVEL5)
                    end
                    else
                    if CanMove and PL_NIVEL6lbl.Visible and StrVacio( PL_NIVEL6.Llave ) then
                    begin
                         CanMove := Error('El nivel '+ StrLeft(PL_NIVEL6lbl.Caption,Length( PL_NIVEL6lbl.Caption )-1 ) +' no puede quedar vac�o',PL_NIVEL6)
                    end
                    else
                    if CanMove and PL_NIVEL7lbl.Visible and StrVacio( PL_NIVEL7.Llave ) then
                    begin
                         CanMove := Error('El nivel '+ StrLeft(PL_NIVEL7lbl.Caption,Length( PL_NIVEL7lbl.Caption )-1 ) +' no puede quedar vac�o',PL_NIVEL7)
                    end
                    else
                    if CanMove and PL_NIVEL8lbl.Visible and StrVacio( PL_NIVEL8.Llave ) then
                    begin
                         CanMove := Error('El nivel '+ StrLeft(PL_NIVEL8lbl.Caption,Length( PL_NIVEL8lbl.Caption )-1 ) +' no puede quedar vac�o',PL_NIVEL8)
                    end
                    else
                    if CanMove and PL_NIVEL9lbl.Visible and StrVacio( PL_NIVEL9.Llave ) then
                    begin
                         CanMove := Error('El nivel '+ StrLeft(PL_NIVEL9lbl.Caption,Length( PL_NIVEL9lbl.Caption )-1 ) +' no puede quedar vac�o',PL_NIVEL9)
                    end;
                    {$ifdef ACS}
                    if CanMove and PL_NIVEL10lbl.Visible and StrVacio( PL_NIVEL10.Llave ) then
                    begin
                         CanMove := Error('El nivel '+ StrLeft(PL_NIVEL10lbl.Caption,Length( PL_NIVEL10lbl.Caption )-1 ) +' no puede quedar vac�o',PL_NIVEL10)
                    end;

                    if CanMove and PL_NIVEL11lbl.Visible and StrVacio( PL_NIVEL11.Llave ) then
                    begin
                         CanMove := Error('El nivel '+ StrLeft(PL_NIVEL11lbl.Caption,Length( PL_NIVEL11lbl.Caption )-1 ) +' no puede quedar vac�o',PL_NIVEL11)
                    end;

                    if CanMove and PL_NIVEL12lbl.Visible and StrVacio( PL_NIVEL12.Llave ) then
                    begin
                         CanMove := Error('El nivel '+ StrLeft(PL_NIVEL12lbl.Caption,Length( PL_NIVEL12lbl.Caption )-1 ) +' no puede quedar vac�o',PL_NIVEL12)
                    end;
                    {$endif}
               end
               else if EspaginaActual( Generales ) then
               begin
                    CreaListaPlazas;
               end;
          {end
          else
          begin
               if ( EsPaginaActual ( Contratacion ) )then   //PENDIENTE: NO SE NECESITA
               begin
                    btnFinalizar.Visible := True;
               end; }

          end;
     end;
end;

procedure TWizPlazas_DevEx.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if Wizard.EsPaginaActual( Titulares ) then
     begin
          ActiveControl := gridEmpleados;
          if ( ActiveControl = gridEmpleados ) then
          begin
               gridEmpleados.SelectedField := gridEmpleados.Columns[ 1 ].Field;   // Posicionar en la primer columna
          end;
     end;
end;

procedure TWizPlazas_DevEx.DefaultsDePuesto;

function GetConfidencialidadDefault( sConfiden : string ) : String;
var
   lista : TStringList;
   i : integer;
begin
    Result :=VACIO;

    if strLleno( sConfiden )  then
    begin
         lista :=   TStringList.Create;
         lista.CommaText := sConfiden;
         for i:=0 to lista.Count-1 do
         begin
              if ListaIntersectaConfidencialidad( lista[i], dmCliente.Confidencialidad ) then
              begin
                 Result := lista[i];
                 break;
              end;
         end;
         FreeAndNil ( lista );
    end;
end;

begin
     with dmCatalogos.cdsPuestos do
     begin
          PL_CLASIFI.Llave := FieldByName('PU_CLASIFI').AsString;
          PL_TURNO.Llave   := FieldByName('PU_TURNO').AsString;
          PL_PATRON.Llave  := FieldByName('PU_PATRON').AsString;
          PL_NIVEL0.Llave  := GetConfidencialidadDefault( FieldByName('PU_NIVEL0').AsString );
          PL_CONTRAT.Llave := FieldByName('PU_CONTRAT').AsString;
          PL_AREA.Llave    := FieldByName('PU_AREA').AsString;

          if ( FieldByName('PU_CHECA').AsString = K_USAR_GLOBAL ) then
             PL_CHECA.Checked := Global.GetGlobalBooleano( K_GLOBAL_DEF_CHECA_TARJETA )
          else
              PL_CHECA.Checked := (FieldByName('PU_CHECA').AsString = K_GLOBAL_SI);

          PL_NIVEL1.Llave  := FieldByName('PU_NIVEL1').AsString;
          PL_NIVEL2.Llave  := FieldByName('PU_NIVEL2').AsString;
          PL_NIVEL3.Llave  := FieldByName('PU_NIVEL3').AsString;
          PL_NIVEL4.Llave  := FieldByName('PU_NIVEL4').AsString;
          PL_NIVEL5.Llave  := FieldByName('PU_NIVEL5').AsString;
          PL_NIVEL6.Llave  := FieldByName('PU_NIVEL6').AsString;
          PL_NIVEL7.Llave  := FieldByName('PU_NIVEL7').AsString;
          PL_NIVEL8.Llave  := FieldByName('PU_NIVEL8').AsString;
          PL_NIVEL9.Llave  := FieldByName('PU_NIVEL9').AsString;
          {$ifdef ACS}
          PL_NIVEL10.Llave  := FieldByName('PU_NIVEL10').AsString;
          PL_NIVEL11.Llave  := FieldByName('PU_NIVEL11').AsString;
          PL_NIVEL12.Llave  := FieldByName('PU_NIVEL12').AsString;
          {$endif}

          if ( FieldByName('PU_AUTOSAL').AsString = K_USAR_GLOBAL ) then
             PL_AUTOSAL.Checked := Global.GetGlobalBooleano( K_GLOBAL_DEF_SALARIO_TAB )
          else
              PL_AUTOSAL.Checked := (FieldByName('PU_AUTOSAL').AsString = K_GLOBAL_SI);

          PL_SALARIO.Valor := FieldByName('PU_SALARIO').AsFloat;
          PL_PER_VAR.Valor := FieldByName('PU_PER_VAR').AsFloat;
          PL_TABLASS.Llave := FieldByName('PU_TABLASS').AsString;

          if FieldByName('PU_ZONA_GE').AsString = 'A' then
             PL_ZONA_GE.ItemIndex  := 0
          else if FieldByName('PU_ZONA_GE').AsString = 'B' then
               PL_ZONA_GE.ItemIndex  := 1
          else
              PL_ZONA_GE.ItemIndex  := 2
     end;
end;

procedure TWizPlazas_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddInteger('NumeroPlazas',StrToInt( NumeroPlazas.Text ) );
          AddString( 'PU_CODIGO', PU_CODIGO.Llave );
          AddString( 'PL_CODIGO', PL_CODIGO.Text );
          AddString( 'PL_NOMBRE', PL_NOMBRE.Text );
          AddInteger( 'PL_REPORTA', StrToIntDef( PL_REPORTA.Llave, 0 ) );

          AddInteger( 'PL_TIPO', PL_TIPO.ItemIndex );
          AddInteger( 'PL_TIREP', PL_TIREP.ItemIndex );
          AddDate( 'PL_FEC_INI', PL_FEC_INI.Valor );
          if ckVence.Checked then
             AddDate( 'PL_FEC_FIN', PL_FEC_FIN.Valor )
          else
              AddDate( 'PL_FEC_FIN', NullDateTime );
          AddString( 'PL_CLASIFI', PL_CLASIFI.Llave );
          AddString( 'PL_TURNO', PL_TURNO.Llave );
          AddString( 'PL_PATRON', PL_PATRON.Llave );
          AddString( 'PL_NIVEL0', PL_NIVEL0.Llave );
          AddString( 'PL_CONTRAT', PL_CONTRAT.Llave );
          AddString( 'PL_AREA', PL_AREA.Llave );
          AddString( 'PL_CHECA', ZBoolToStr( PL_CHECA.Checked ) );
          AddString( 'PL_NOMINA', PL_NOMINA.Llave); 

          if PL_NIVEL1.Visible then AddString( 'PL_NIVEL1', PL_NIVEL1.Llave )
          else  AddString( 'PL_NIVEL1', VACIO );
          if PL_NIVEL2.Visible then AddString( 'PL_NIVEL2', PL_NIVEL2.Llave )
          else  AddString( 'PL_NIVEL2', VACIO );
          if PL_NIVEL3.Visible then AddString( 'PL_NIVEL3', PL_NIVEL3.Llave )
          else  AddString( 'PL_NIVEL3', VACIO );
          if PL_NIVEL4.Visible then AddString( 'PL_NIVEL4', PL_NIVEL4.Llave )
          else  AddString( 'PL_NIVEL4', VACIO );
          if PL_NIVEL5.Visible then AddString( 'PL_NIVEL5', PL_NIVEL5.Llave )
          else  AddString( 'PL_NIVEL5', VACIO );
          if PL_NIVEL6.Visible then AddString( 'PL_NIVEL6', PL_NIVEL6.Llave )
          else  AddString( 'PL_NIVEL6', VACIO );
          if PL_NIVEL7.Visible then AddString( 'PL_NIVEL7', PL_NIVEL7.Llave )
          else  AddString( 'PL_NIVEL7', VACIO );
          if PL_NIVEL8.Visible then AddString( 'PL_NIVEL8', PL_NIVEL8.Llave )
          else  AddString( 'PL_NIVEL8', VACIO );
          if PL_NIVEL9.Visible then AddString( 'PL_NIVEL9', PL_NIVEL9.Llave )
          else  AddString( 'PL_NIVEL9', VACIO );
          {$ifdef ACS}
          if PL_NIVEL10.Visible then AddString( 'PL_NIVEL10', PL_NIVEL10.Llave )
          else  AddString( 'PL_NIVEL10', VACIO );
          if PL_NIVEL11.Visible then AddString( 'PL_NIVEL11', PL_NIVEL11.Llave )
          else  AddString( 'PL_NIVEL11', VACIO );
          if PL_NIVEL12.Visible then AddString( 'PL_NIVEL12', PL_NIVEL12.Llave )
          else  AddString( 'PL_NIVEL12', VACIO );
          {$endif}

          AddString( 'PL_AUTOSAL', ZBoolToStr(PL_AUTOSAL.Checked ) );
          AddFloat( 'PL_SALARIO',  PL_SALARIO.Valor  );
          AddFloat( 'PL_PER_VAR',  PL_PER_VAR.Valor  );
          AddString( 'PL_TABLASS', PL_TABLASS.Llave );
          AddString( 'PL_ZONA_GE', PL_ZONA_GE.Text );

          AddString( 'PL_INGLES', PL_INGLES.Text );
          AddInteger( 'PL_NUMERO', PL_NUMERO.ValorEntero  );
          AddString( 'PL_TEXTO', PL_TEXTO.Text );
          AddString( 'PL_SUB_CTA', PL_SUB_CTA.Text );
     end;
     with Descripciones do
     begin
          AddString( 'Puesto', PL_CODIGO.Text );
          AddString( 'Descripci�n puesto', PL_NOMBRE.Text );
          AddInteger('N�mero de Plazas',StrToInt( NumeroPlazas.Text ) );

     end;
end;

procedure TWizPlazas_DevEx.PU_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     PL_NOMBRE.Text := PU_CODIGO.Descripcion;
     PL_CODIGO.Text := PU_CODIGO.Llave;
     DefaultsDePuesto;
end;

procedure TWizPlazas_DevEx.ckVenceClick(Sender: TObject);
begin
     inherited;
     HabilitaFecha;
end;

procedure TWizPlazas_DevEx.CargaDataSet;
var
   sNombre,sCodigo: string;
   oEvento:TFieldNotifyEvent;
   function LongitudEsSucifiente(sValor:string;iLongitud:Integer):Boolean;
   begin
         Result :=  Length( sValor ) < iLongitud ;
   end;

begin
     with ParameterList do
     begin
           with dmRecursos do
           begin
                cdsListaEmpPlazas.DisableControls;
                oEvento := cdsPlazas.FieldByName( 'CB_CODIGO' ).OnValidate;
                cdsPlazas.FieldByName( 'CB_CODIGO' ).OnValidate := Nil;
                try
                   cdsPlazas.EmptyDataSet;      // Elimina registro activo - Solo quedar�n los registros agregados
                   cdsListaEmpPlazas.First;
                   while not cdsListaEmpPlazas.EOF do
                   begin
                        with cdsPlazas do
                        begin
                             Append;
                             sNombre := cdsListaEmpPlazas.FieldByName( 'PL_NOMBRE' ).AsString;
                             sCodigo := ParamByName('PL_CODIGO').AsString + StrZero( IntToStr(cdsListaEmpPlazas.FieldByName('PL_ORDEN').AsInteger ),3 );

                             if ( LongitudEsSucifiente( sNombre,K_ANCHO_OBSERVACIONES ) )then
                                FieldByName( 'PL_NOMBRE' ).AsString  := sNombre;
                             //else //Truncar
                             if ( LongitudEsSucifiente( sCodigo,K_ANCHO_OBSERVACIONES ) )then
                                FieldByName( 'PL_CODIGO' ).AsString  := sCodigo;
                             //else //Truncar

                             FieldByName( 'PL_REPORTA' ).AsInteger := ParamByName('PL_REPORTA').AsInteger;
                             FieldByName( 'PU_CODIGO' ).AsString  := ParamByName('PU_CODIGO').AsString;
                             FieldByName( 'CB_CODIGO' ).AsInteger := cdsListaEmpPlazas.FieldByName( 'CB_CODIGO' ).AsInteger;

                             FieldByName( 'PL_FEC_INI' ).AsDateTime := ParamByName('PL_FEC_INI').AsDateTime;
                             FieldByName( 'PL_FEC_FIN' ).AsDateTime := ParamByName('PL_FEC_FIN').AsDateTime;

                             FieldByName( 'PL_CLASIFI' ).AsString := ParamByName('PL_CLASIFI').AsString;
                             FieldByName( 'PL_TURNO' ).AsString   := ParamByName('PL_TURNO').AsString;
                             FieldByName( 'PL_PATRON' ).AsString  := ParamByName('PL_PATRON').AsString;
                             FieldByName( 'PL_NIVEL0' ).AsString  := ParamByName('PL_NIVEL0').AsString;
                             FieldByName( 'PL_CONTRAT' ).AsString := ParamByName('PL_CONTRAT').AsString;
                             FieldByName( 'PL_AREA' ).AsString    := ParamByName('PL_AREA').AsString;
                             FieldByName( 'PL_CHECA' ).AsString   := ParamByName('PL_CHECA').AsString;
                             FieldByName( 'PL_NOMINA' ).AsInteger := ParamByName('PL_NOMINA').AsInteger;

                             FieldByName( 'PL_NIVEL1' ).AsString := ParamByName('PL_NIVEL1').AsString;
                             FieldByName( 'PL_NIVEL2' ).AsString := ParamByName('PL_NIVEL2').AsString;
                             FieldByName( 'PL_NIVEL3' ).AsString := ParamByName('PL_NIVEL3').AsString;
                             FieldByName( 'PL_NIVEL4' ).AsString := ParamByName('PL_NIVEL4').AsString;
                             FieldByName( 'PL_NIVEL5' ).AsString := ParamByName('PL_NIVEL5').AsString;
                             FieldByName( 'PL_NIVEL6' ).AsString := ParamByName('PL_NIVEL6').AsString;
                             FieldByName( 'PL_NIVEL7' ).AsString := ParamByName('PL_NIVEL7').AsString;
                             FieldByName( 'PL_NIVEL8' ).AsString := ParamByName('PL_NIVEL8').AsString;
                             FieldByName( 'PL_NIVEL9' ).AsString := ParamByName('PL_NIVEL9').AsString;
                             {$ifdef ACS}
                             FieldByName( 'PL_NIVEL10' ).AsString := ParamByName('PL_NIVEL10').AsString;
                             FieldByName( 'PL_NIVEL11' ).AsString := ParamByName('PL_NIVEL11').AsString;
                             FieldByName( 'PL_NIVEL12' ).AsString := ParamByName('PL_NIVEL12').AsString;
                             {$endif}

                             FieldByName( 'PL_AUTOSAL' ).AsString := ParamByName('PL_AUTOSAL').AsString;
                             FieldByName( 'PL_SALARIO' ).AsFloat  := ParamByName('PL_SALARIO').AsFloat;
                             FieldByName( 'PL_PER_VAR' ).AsFloat  := ParamByName('PL_PER_VAR').AsFloat;
                             FieldByName( 'PL_TABLASS' ).AsString := ParamByName('PL_TABLASS').AsString;
                             FieldByName( 'PL_ZONA_GE' ).AsString := ParamByName('PL_ZONA_GE').AsString;
                             FieldByName( 'PL_ORDEN' ).AsInteger  := cdsListaEmpPlazas.FieldByName( 'PL_ORDEN' ).AsInteger;
                             Post;
                        end;
                        cdsListaEmpPlazas.Next;
                   end;
                finally
                       cdsPlazas.FieldByName( 'CB_CODIGO' ).OnValidate := oEvento;
                       cdsListaEmpPlazas.EnableControls;
                end;
           end;
     end;
end;

// metodos del Grid ///
procedure TWizPlazas_DevEx.Buscar;
var
   sKey, sDescription: String;
begin
     with dmRecursos do
     begin
          ZetaBuscaEmpleado_DevEx.SetOnlyActivos( True );
          dmCliente.cdsEmpleadoLookUp.Close;

          try
             if (cdsListaEmpPlazas.State = dsBrowse ) then
                LlenaFiltroEmpleados(cdsListaEmpPlazas);
             if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( ConcatFiltros( K_SIN_PLAZA_ASIGNADA, FiltroEmpleadosPlaza ), sKey, sDescription ) then
             begin
                  with cdsListaEmpPlazas do
                  begin
                       if not ( State in [ dsEdit, dsInsert ] ) then
                          Edit;
                       FieldByName( 'CB_CODIGO' ).AsString := sKey;
                       Post;
                       SeleccionaSiguienteRenglon;
                       gridEmpleados.SelectedField := FieldByName( 'CB_CODIGO' );
                  end;
             end;
          finally
                 ZetaBuscaEmpleado_DevEx.SetOnlyActivos( False );
          end;
     end;
end;


procedure TWizPlazas_DevEx.BBAgregarClick(Sender: TObject);
begin
     Buscar;
end;

procedure TWizPlazas_DevEx.BBBorrarClick(Sender: TObject);
begin
     BorraCelda;
end;

procedure TWizPlazas_DevEx.SeleccionaSiguienteRenglon;
begin
     with dmRecursos.cdsListaEmpPlazas do
     begin
          Next;
          if EOF then
             SetCycleControl;
     end;
end;

procedure TWizPlazas_DevEx.SetCycleControl;
begin
     {if Ejecutar.Enabled then
        Ejecutar.SetFocus
     else
        Salir.SetFocus; } //PENDIENTE VER SI SE OCUPA
end;

procedure TWizPlazas_DevEx.BorraCelda;
var
   oCursor: TCursor;
begin
     if ZetaDialogo.zConfirm( '�Atenci�n!', '�Desea borrar al titular de la plaza?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with dmRecursos.cdsListaEmpPlazas do
             begin
                  Edit;
                  FieldByName( 'CB_CODIGO' ).AsInteger := 0;
                  FieldByName( 'PRETTYNAME' ).AsString := VACIO;
                  Post;
                  gridEmpleados.SelectedField := FieldByName( 'CB_CODIGO' );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TWizPlazas_DevEx.gridEmpleadosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     inherited;
     if ( ActiveControl = gridEmpleados ) then
     begin
          if ( ssShift in Shift ) then { SHIFT }
          begin
               case Key of
                    VK_DELETE:    { DEL = Borrar }
                    begin
                         Key := 0;
                         BorraCelda;
                    end;
               end;
          end
          else
          begin
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        70,66 : { Letra F,B = Buscar }
                        begin
                             Key := 0;
                             Buscar;
                        end;
                   end;
              end;
          end;
          if ( Key = VK_RETURN ) then
          begin
               Key := 0;
               SeleccionaSiguienteRenglon;
               gridEmpleados.SelectedField := dmRecursos.cdsListaEmpPlazas.FieldByName( 'CB_CODIGO' );
          end;
          if ( Key = VK_DOWN ) then
          begin
               Key := 0;
               SeleccionaSiguienteRenglon;
          end;
          if ( Key = VK_TAB ) then
          begin
               Key := 0;
               SeleccionaSiguienteRenglon;
          end;
          if ( Key = VK_DELETE ) and ( not gridEmpleados.EditorMode ) then     // Si est� editando la celda lo deja pasar
          begin
               Key := 0;
               BorraCelda;
          end;
     end
     else
         inherited KeyDown( Key, Shift );
end;

function TWizPlazas_DevEx.EjecutarWizard: Boolean;
begin
     Result := FALSE;
     with dmRecursos do
     begin
          try
             CargaDataSet;
             ModificarPlaza( dmCliente.FechaDefault );
             Result := (cdsPlazas.ChangeCount = 0);
          finally
                 if Result then
                 begin
                      FReportaA := PL_REPORTA.Valor;
                      FFiltroEmpleado:= VACIO;
                 end
                 else
                     cdsPlazas.CancelUpdates;
          end;
     end;
end;

procedure TWizPlazas_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     BBBorrar.Enabled := not( DataSource.DataSet.FieldByName('PRETTYNAME').AsString = VACIO );
end;


procedure TWizPlazas_DevEx.CreaListaPlazas;
var
   iUltimoOrden,iIndice:Integer;
begin
     inherited;
     with dmRecursos do
     begin
          cdsListaEmpPlazas.DisableControls;
          try
             CrearListaPlazas;
             iUltimoOrden :=  GetMaxOrdenPlazas( PU_CODIGO.Llave );
             with cdsListaEmpPlazas do
             begin

                  for iIndice := 1 to NumeroPlazas.ValorEntero do
                  begin
                       Append;
                       FieldByName( 'PL_ORDEN' ).AsInteger := iUltimoOrden + iIndice;
                       FieldByName( 'PL_NOMBRE' ).AsString := PL_NOMBRE.Text +' '+ StrZero( IntToStr( iUltimoOrden + iIndice ),3 );
                       Post;
                  end;
                  First;
             end;
          finally
                 cdsListaEmpPlazas.EnableControls;
          end;
     end;
end;

procedure TWizPlazas_DevEx.PL_AUTOSALClick(Sender: TObject);
begin
     inherited;
     PL_SALARIO.Enabled := NOT PL_AUTOSAL.Checked;
     LblSalario.Enabled := PL_SALARIO.Enabled;
end;

{ACS: Se establece la relaci�n del nivel seleccionado con el nivel que tiene relacionado
      (nivel 12 al 2, el nivel 1 no tiene relaci�n por ser el nivel inferior).}
procedure TWizPlazas_DevEx.PL_NIVEL12ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if StrLleno( dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString ) and PL_NIVEL11.Visible then
        PL_NIVEL11.Llave := dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TWizPlazas_DevEx.PL_NIVEL11ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if StrLleno( dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString ) and PL_NIVEL10.Visible then
        PL_NIVEL10.Llave := dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TWizPlazas_DevEx.PL_NIVEL10ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if StrLleno( dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString ) and PL_NIVEL9.Visible then
        PL_NIVEL9.Llave := dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TWizPlazas_DevEx.PL_NIVEL9ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if StrLleno( dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString ) and PL_NIVEL8.Visible then
        PL_NIVEL8.Llave := dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TWizPlazas_DevEx.PL_NIVEL8ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if StrLleno( dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString ) and PL_NIVEL7.Visible then
        PL_NIVEL7.Llave := dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TWizPlazas_DevEx.PL_NIVEL7ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if StrLleno( dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString ) and PL_NIVEL6.Visible then
        PL_NIVEL6.Llave := dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TWizPlazas_DevEx.PL_NIVEL6ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if StrLleno( dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString ) and PL_NIVEL5.Visible then
        PL_NIVEL5.Llave := dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TWizPlazas_DevEx.PL_NIVEL5ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if StrLleno( dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString ) and PL_NIVEL4.Visible then
        PL_NIVEL4.Llave := dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TWizPlazas_DevEx.PL_NIVEL4ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if StrLleno( dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString ) and PL_NIVEL3.Visible then
        PL_NIVEL3.Llave := dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TWizPlazas_DevEx.PL_NIVEL3ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if StrLleno( dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString ) and PL_NIVEL2.Visible then
        PL_NIVEL2.Llave := dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TWizPlazas_DevEx.PL_NIVEL2ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if StrLleno( dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString ) and PL_NIVEL1.Visible then
        PL_NIVEL1.Llave := dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

{$ifdef ACS}
{ACS: Este procedimiento re-ajustar� la posici�n de los controles de los niveles.}
procedure TWizPlazas_DevEx.SetPosicionNiveles;
const
     K_ALTURA_DEF = 24;
     K_TOP_N1 = 276;
     K_TOP_N1_LBL = 280;
     K_TOP_N2 = 252;
     K_TOP_N2_LBL = 254;
     K_TOP_N3 = 228;
     K_TOP_N3_LBL = 232;
     K_TOP_N4 = 204;
     K_TOP_N4_LBL = 208;
     K_TOP_N5 = 180;
     K_TOP_N5_LBL = 184;
     K_TOP_N6 = 156;
     K_TOP_N6_LBL = 160;
     K_TOP_N7 = 132;
     K_TOP_N7_LBL = 136;
     K_TOP_N8 = 108;
     K_TOP_N8_LBL = 112;
     K_TOP_N9 = 84;
     K_TOP_N9_LBL = 88;
     K_TOP_N10 = 60;
     K_TOP_N10_LBL = 64;
     K_TOP_N11 = 36;
     K_TOP_N11_LBL = 40;
     K_TOP_N12 = 12;
     K_TOP_N12_LBL = 16;
var
     iNivelesNoVisibles, iTotalHorizontal: Integer;
begin
     {Se obtiene la cantidad total de niveles no visibles}
     iNivelesNoVisibles := 0;
     //iTotalHorizontal := 0;
     if Not PL_NIVEL12.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL11.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL10.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL9.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL8.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL7.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL6.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL5.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL4.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL3.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL2.Visible then
        inc( iNivelesNoVisibles );

     iTotalHorizontal := ( iNivelesNoVisibles * K_ALTURA_DEF );
     {Se re-ajustan todos los controles en la posici�n horizontal}
     PL_NIVEL12.Top := K_TOP_N12 - iTotalHorizontal;
     PL_NIVEL12lbl.Top := K_TOP_N12_LBL - iTotalHorizontal;
     PL_NIVEL11.Top := K_TOP_N11 - iTotalHorizontal;
     PL_NIVEL11lbl.Top := K_TOP_N11_LBL - iTotalHorizontal;
     PL_NIVEL10.Top := K_TOP_N10 - iTotalHorizontal;
     PL_NIVEL10lbl.Top := K_TOP_N10_LBL - iTotalHorizontal;
     PL_NIVEL9.Top := K_TOP_N9 - iTotalHorizontal;
     PL_NIVEL9lbl.Top := K_TOP_N9_LBL - iTotalHorizontal;
     PL_NIVEL8.Top := K_TOP_N8 - iTotalHorizontal;
     PL_NIVEL8lbl.Top := K_TOP_N8_LBL - iTotalHorizontal;
     PL_NIVEL7.Top := K_TOP_N7 - iTotalHorizontal;
     PL_NIVEL7lbl.Top := K_TOP_N7_LBL - iTotalHorizontal;
     PL_NIVEL6.Top := K_TOP_N6 - iTotalHorizontal;
     PL_NIVEL6lbl.Top := K_TOP_N6_LBL - iTotalHorizontal;
     PL_NIVEL5.Top := K_TOP_N5 - iTotalHorizontal;
     PL_NIVEL5lbl.Top := K_TOP_N5_LBL - iTotalHorizontal;
     PL_NIVEL4.Top := K_TOP_N4 - iTotalHorizontal;
     PL_NIVEL4lbl.Top := K_TOP_N4_LBL - iTotalHorizontal;
     PL_NIVEL3.Top := K_TOP_N3 - iTotalHorizontal;
     PL_NIVEL3lbl.Top := K_TOP_N3_LBL - iTotalHorizontal;
     PL_NIVEL2.Top := K_TOP_N2 - iTotalHorizontal;
     PL_NIVEL2lbl.Top := K_TOP_N2_LBL - iTotalHorizontal;
     PL_NIVEL1.Top := K_TOP_N1 - iTotalHorizontal;
     PL_NIVEL1lbl.Top := K_TOP_N1_LBL - iTotalHorizontal;
end;
{$endif}

{$ifdef ACS}
{Este procedimiento sirve para eliminar cualquier OLDKEY de los lookups de niveles}
procedure TWizPlazas_DevEx.LimpiaLookUpOlfKey;
begin
          PL_NIVEL1.ResetMemory;
          PL_NIVEL2.ResetMemory;
          PL_NIVEL3.ResetMemory;
          PL_NIVEL4.ResetMemory;
          PL_NIVEL5.ResetMemory;
          PL_NIVEL6.ResetMemory;
          PL_NIVEL7.ResetMemory;
          PL_NIVEL8.ResetMemory;
          PL_NIVEL9.ResetMemory;
          PL_NIVEL10.ResetMemory;
          PL_NIVEL11.ResetMemory;
          PL_NIVEL12.ResetMemory;
end;
{$endif}

procedure TWizPlazas_DevEx.btnFinalizarClick(Sender: TObject);
begin
     //inherited;
     FFinalizar := True;
     Wizard.Siguiente;
     FFinalizar := False;
end;

procedure TWizPlazas_DevEx.gridEmpleadosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
   iCurrentRow, iRecNo: Integer;
begin
  inherited;
  with gridEmpleados, Canvas, Brush do
    begin
         if ( State = [] ) then
         begin
              iCurrentRow := Rect.Top div (1+Rect.Bottom-Rect.Top);
              iRecNo:= TDBGridHack( gridEmpleados ).Row;
              if iCurrentRow = iRecNo then
              begin
                   //Color:= clBtnShadow;
                   Color:=  RGB(237,230,249);//RGB(156,129,139); //morado row seleccionado
                   Font.Color := clWindowText;
              end;
         end;
         if gdSelected in State then
         begin
              //Color:= clHighlight;
              Color:= RGB(212,188,251);//RGB(136,174,213); //Azul de celda seleccionado
              Font.Color := clHighlightText; //Texto en blanco para celda seleccionada
         end;
         DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
end;



{$IF Defined(ANCHO_NIVELES_10) or Defined(ANCHO_CODIGO_12) or Defined(ANCHO_CODIGO_12)}
procedure TWizPlazas_DevEx.CambiosVisuales;
var
  I : Integer;
begin
     for I := 0 to Area.ControlCount - 1 do
     begin
          if Area.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               Area.Controls[I].Left := K_WIDTH_MEDIUM_LEFT -(Area.Controls[I].Width+2);
          end;
          if Area.Controls[I].ClassNameIs( 'TZetaKeyLookup_DevEx' ) then
          begin
               if ( Area.Controls[I].Visible ) then
               begin
                    Area.Controls[I].Width := K_WIDTH_LOOKUP;
                    TZetaDbKeyLookup_DevEx( Area.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                    Area.Controls[I].Left := K_WIDTH_MEDIUM_LEFT;
               end;
          end;
     end;
end;
{$endif}

procedure TWizPlazas_DevEx.SetEditarSoloActivos;
begin
     PU_CODIGO.EditarSoloActivos := TRUE;
     PL_CLASIFI.EditarSoloActivos := TRUE;
     PL_PATRON.EditarSoloActivos := TRUE;
     PL_CONTRAT.EditarSoloActivos := TRUE;
     PL_AREA.EditarSoloActivos := TRUE;
     PL_TABLASS.EditarSoloActivos := TRUE;
     PL_NIVEL1.EditarSoloActivos := TRUE;
     PL_NIVEL2.EditarSoloActivos := TRUE;
     PL_NIVEL3.EditarSoloActivos := TRUE;
     PL_NIVEL4.EditarSoloActivos := TRUE;
     PL_NIVEL5.EditarSoloActivos := TRUE;
     PL_NIVEL6.EditarSoloActivos := TRUE;
     PL_NIVEL7.EditarSoloActivos := TRUE;
     PL_NIVEL8.EditarSoloActivos := TRUE;
     PL_NIVEL9.EditarSoloActivos := TRUE;
     PL_NIVEL10.EditarSoloActivos := TRUE;
     PL_NIVEL11.EditarSoloActivos := TRUE;
     PL_NIVEL12.EditarSoloActivos := TRUE;
end;

end.
