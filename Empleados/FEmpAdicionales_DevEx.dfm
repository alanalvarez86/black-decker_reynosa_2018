inherited EmpAdicionales_DevEx: TEmpAdicionales_DevEx
  Left = 207
  Top = 236
  Caption = 'Datos Adicionales'
  ClientHeight = 204
  ClientWidth = 592
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 592
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 266
      inherited textoValorActivo2: TLabel
        Width = 260
      end
    end
  end
  object PageControl: TcxPageControl [1]
    Left = 0
    Top = 19
    Width = 592
    Height = 185
    Align = alClient
    TabOrder = 1
    Properties.CustomButtons.Buttons = <>
    Properties.MultiLine = True
    Properties.TabPosition = tpBottom
    ClientRectBottom = 183
    ClientRectLeft = 2
    ClientRectRight = 590
    ClientRectTop = 2
  end
  inherited DataSource: TDataSource
    Left = 280
    Top = 48
  end
end
