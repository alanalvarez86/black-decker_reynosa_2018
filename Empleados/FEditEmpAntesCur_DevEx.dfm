inherited EditEmpAntesCur_DevEx: TEditEmpAntesCur_DevEx
  Left = 823
  Top = 232
  Caption = 'Cursos Anteriores'
  ClientHeight = 197
  ClientWidth = 421
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 27
    Top = 65
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label2: TLabel [1]
    Left = 34
    Top = 114
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha:'
  end
  object Label3: TLabel [2]
    Left = 37
    Top = 137
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Lugar:'
  end
  object Label4: TLabel [3]
    Left = 37
    Top = 89
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Curso:'
  end
  inherited PanelBotones: TPanel
    Top = 161
    Width = 421
    TabOrder = 4
    inherited OK_DevEx: TcxButton
      Left = 256
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 335
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 421
    TabOrder = 5
    inherited ValorActivo2: TPanel
      Width = 95
      inherited textoValorActivo2: TLabel
        Width = 89
      end
    end
  end
  object AR_CURSO: TDBEdit [7]
    Left = 83
    Top = 85
    Width = 330
    Height = 21
    DataField = 'AR_CURSO'
    DataSource = DataSource
    TabOrder = 1
  end
  object AR_FECHA: TZetaDBFecha [8]
    Left = 83
    Top = 109
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 2
    Text = '15/dic/97'
    Valor = 35779.000000000000000000
    DataField = 'AR_FECHA'
    DataSource = DataSource
  end
  object AR_LUGAR: TDBEdit [9]
    Left = 83
    Top = 133
    Width = 330
    Height = 21
    DataField = 'AR_LUGAR'
    DataSource = DataSource
    TabOrder = 3
  end
  object AR_FOLIO: TZetaDBNumero [10]
    Left = 83
    Top = 61
    Width = 60
    Height = 21
    Mascara = mnDias
    TabOrder = 0
    Text = '0'
    DataField = 'AR_FOLIO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 20
    Top = 161
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
