inherited GridGlobalTools_DevEx: TGridGlobalTools_DevEx
  Left = 113
  Top = 178
  BorderIcons = [biMaximize]
  Caption = 'Entrega Global'
  ClientHeight = 365
  ClientWidth = 560
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 329
    Width = 560
    inherited OK_DevEx: TcxButton
      Left = 394
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 474
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 560
    inherited ValorActivo2: TPanel
      Width = 234
      inherited textoValorActivo2: TLabel
        Width = 228
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 104
    Width = 560
    Height = 225
    TabOrder = 4
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'Empleado'
        Width = 65
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre Completo'
        Width = 250
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KT_REFEREN'
        Title.Caption = 'Referencia'
        Width = 105
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KT_TALLA'
        Title.Caption = 'Talla'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KT_COMENTA'
        Title.Caption = 'Observaciones'
        Width = 350
        Visible = True
      end>
  end
  object PanelTool: TPanel [4]
    Left = 0
    Top = 50
    Width = 560
    Height = 54
    Align = alTop
    TabOrder = 2
    object FechaLbl: TLabel
      Left = 37
      Top = 30
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = '&Fecha:'
      FocusControl = KT_FEC_INI
    end
    object HerramientaLbl: TLabel
      Left = 10
      Top = 9
      Width = 60
      Height = 13
      Alignment = taRightJustify
      Caption = '&Herramienta:'
    end
    object KT_FEC_INI: TZetaFecha
      Left = 73
      Top = 27
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '19/dic/97'
      Valor = 35783.000000000000000000
    end
    object TO_CODIGO: TZetaKeyLookup_DevEx
      Left = 73
      Top = 4
      Width = 375
      Height = 21
      LookupDataset = dmCatalogos.cdsTools
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
