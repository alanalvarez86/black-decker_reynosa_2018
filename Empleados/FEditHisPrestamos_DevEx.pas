unit FEditHisPrestamos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaNumero, ZetaDBTextBox, StdCtrls, ZetaKeyLookup_DevEx,
  Mask, ZetaFecha, DBCtrls, Db,
  {$ifdef CAJAAHORRO}
  dCajaAhorro,
  {$else}
  dRecursos,
  {$endif}
  Grids, DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls, Buttons, ZetaKeyCombo,
  ZetaMessages, ZetaEdit, ZetaSmartLists, ZBaseEdicionRenglon_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
    TressMorado2013,
  dxSkinsDefaultPainters,  cxControls,
  dxSkinsdxBarPainter, ImgList, dxBarExtItems, dxBar, cxClasses,
  cxNavigator, cxDBNavigator, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxPC, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxTextEdit, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxContainer, cxMaskEdit;

type
  TEditHisPrestamos_DevEx = class(TBaseEdicionRenglon_DevEx)
    AH_TIPOLbl: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    PR_FORMULA: TDBMemo;
    PR_FECHA: TZetaDBFecha;
    GBTotales: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    PR_CARGOS: TZetaDBTextBox;
    Label5: TLabel;
    PR_ABONOS: TZetaDBTextBox;
    Label6: TLabel;
    PR_TOTAL: TZetaDBTextBox;
    LSigno1: TLabel;
    LSigno2: TLabel;
    LSigno3: TLabel;
    LSigno4: TLabel;
    SaldoLbl: TLabel;
    PR_SALDO: TZetaDBTextBox;
    PR_SALDO_I: TZetaDBNumero;
    DedLbl: TLabel;
    PR_NUMERO: TZetaDBTextBox;
    Label7: TLabel;
    PR_STATUS: TZetaDBKeyCombo;
    Label8: TLabel;
    PR_REFEREN: TZetaDBEdit;
    Label9: TLabel;
    PR_MONTO: TZetaDBNumero;
    Label10: TLabel;
    US_DESCRIP: TZetaDBTextBox;
    Label11: TLabel;
    ZFecha: TZetaDBFecha;
    Label12: TLabel;
    PR_SUB_CTA: TZetaDBEdit;
    PR_TIPO_TXT: TZetaDBTextBox;
    PR_TIPO_DESC: TZetaTextBox;
    PR_TIPO: TZetaDBKeyLookup_DevEx;
    Label18: TLabel;
    PR_OBSERVA: TZetaDBEdit;
    SBCO_FORMULA_DevEx: TcxButton;
    Intereses: TcxTabSheet;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label17: TLabel;
    PR_TASA: TZetaDBNumero;
    PR_MONTO_S: TZetaDBNumero;
    PR_MESES: TZetaDBNumero;
    PR_INTERES: TZetaDBNumero;
    PR_PAGOS: TZetaDBNumero;
    PR_PAG_PER: TZetaDBNumero;
    PR_MONTO_INT: TZetaDBNumero;
    Fonacot: TcxTabSheet;
    GroupBox2: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    F_PLAZO: TZetaDBNumero;
    F_CUOTAS: TZetaDBNumero;
    F_RETENSION: TZetaDBNumero;
    F_MONTO: TZetaDBTextBox;
    NuevoFonacot: TcxTabSheet;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    PF_YEAR: TcxGridDBColumn;
    PF_MES: TcxGridDBColumn;
    PF_PAGO: TcxGridDBColumn;
    ZetaDBGridDBTableView1: TcxGridDBTableView;
    ZetaDBGridDBTableView1EP_ORDEN: TcxGridDBColumn;
    ZetaDBGridDBTableView1EP_DESCRIP: TcxGridDBColumn;
    ZetaDBGridDBTableView1EP_PESO: TcxGridDBColumn;
    ZetaDBGridDBTableView1EL_NOMBRE: TcxGridDBColumn;
    ZetaDBGridDBTableView1EP_NUMERO: TcxGridDBColumn;
    ZetaDBGridDBTableView1EP_GET_COM: TcxGridDBColumn;
    ZetaDBGridLevel: TcxGridLevel;
    PF_RETENCIONTRESS: TcxGridDBColumn;
    PF_PLAZO: TcxGridDBColumn;
    PF_PAGADAS: TcxGridDBColumn;
    dsHistorialFonacot: TDataSource;
    Panel3: TPanel;
    Label19: TLabel;
    TotalPago: TZetaNumero;
    TotalRetencion: TZetaNumero;
    lblPR_FON_MOT: TLabel;
    PR_FON_MOT: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure SBCO_FORMULAClick(Sender: TObject);
    procedure GridRenglonesColExit(Sender: TObject);
    procedure GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure dsRenglonStateChange(Sender: TObject);
    procedure PR_TIPOValidLookup(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PR_TIPOValidKey(Sender: TObject);
    procedure DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
    procedure SetEditarSoloActivos;
    procedure dxBarButton_ImprimirBtnClick(Sender: TObject);
    procedure dxBarButton_ExportarBtnClick(Sender: TObject);
    procedure PR_STATUSChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dsHistorialFonacotDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    FPuedeRefrescar: Boolean; //Agregada para corregir Bug de navegacion en el detalle del grid Cargos/Abonos
    FTipoAntes: String;
    procedure ControlFecha;
    procedure SetError;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure FonacotTab;
    {$ifdef FAPSA}
    procedure ActivaControlesPrestamo;
    procedure LimpiaControlesPrestamo;
    {$endif}
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure KeyPress(var Key: Char); override; { TWinControl }
    procedure Agregar; override;
    {$ifdef CAJAAHORRO}
    function dmRecursos : TdmCajaAhorro;
    {$endif}
    {$ifdef FAPSA}
    procedure HabilitaControles; override;
    {$endif}
    procedure ActivarMotivoCancelacion;
  public
    { Public declarations }
    procedure DoPrintFonacot;
    procedure ImprimirFonacot;
    procedure DoExportarFonacot;
    procedure ExportarFonacot;
  end;

var
  EditHisPrestamos_DevEx: TEditHisPrestamos_DevEx;
  tPlazo : Double;
  tRetension : Double;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZImprimeGrid,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZConstruyeFormula,
     ZAccesosTress,
     dSistema,
     dTablas,
     ZAccesosMgr,
     ZGlobalTress,
     DGlobal;
{$R *.DFM}

procedure TEditHisPrestamos_DevEx.SBCO_FORMULAClick(Sender: TObject);
begin
     with dmRecursos.cdsHisPrestamos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
            FieldByName( 'PR_FORMULA').AsString := GetFormulaConst( enNomina, PR_FORMULA.Lines.Text, PR_FORMULA.SelStart, evBase );
     end;
end;

procedure TEditHisPrestamos_DevEx.Connect;
begin
     dmTablas.cdsTPresta.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsHisPrestamos.Conectar;
          Datasource.Dataset := cdsHisPrestamos;
          cdsPCarAbo.Conectar;
          dsRenglon.DataSet := cdsPCarAbo;
          {$ifndef CAJAAHORRO}
          cdsHistorialFonacot.Refrescar;
          dsHistorialFonacot.DataSet := cdsHistorialFonacot;
          {$endif}
     end;
     PR_TIPO_DESC.Caption := PR_TIPO.Descripcion;

     {$ifdef FAPSA}
      ActivaControlesPrestamo;
     {$endif}     
end;

procedure TEditHisPrestamos_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
  var i : integer;
begin
  inherited;
  i := 1;

end;

procedure TEditHisPrestamos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     GridRenglones.Options := [ dgEditing,
                                dgTitles,
                                dgIndicator,
                                dgColumnResize,
                                dgColLines,
                                dgRowLines,
                                dgTabs ];

     TipoValorActivo1 := stEmpleado;
     PR_TIPO.LookupDataset := dmTablas.cdsTPresta;

     HelpContext:= ZetaCommonClasses.H10142_Prestamos;
     IndexDerechos := ZAccesosTress.D_EMP_NOM_PRESTAMOS;

     {$ifdef CAJAAHORRO}
     PR_TIPO.Visible := False;
     PR_TIPO_TXT.Visible := True;
     PR_TIPO_DESC.Visible := True;
     TipoValorActivo2 := stTipoAhorro;
     FirstControl := PR_REFEREN;
     {$else}
     PR_TIPO.Visible := True;
     PR_TIPO_TXT.Visible := False;
     PR_TIPO_DESC.Visible := False;
     FirstControl := PR_TIPO;
     {$endif}
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;

end;


procedure TEditHisPrestamos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     tPlazo := F_PLAZO.Valor;
     tRetension := F_RETENSION.Valor;
     FTipoAntes := PR_TIPO.Llave;
     FonacotTab;

     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := False; //Muestra la caja de agrupamiento
     ZetaDBGridDBTableView.ApplyBestFit();

     {$ifndef CAJAAHORRO}
     TotalPago.Valor := dmRecursos.SumaPagosFonacot;
     TotalRetencion.Valor := dmRecursos.SumaPagosRetencion;

     ActivarMotivoCancelacion;
     {$endif}
end;

procedure TEditHisPrestamos_DevEx.GridRenglonesColExit(Sender: TObject);
begin
     inherited;
     with GridRenglones do
     begin
          if ( SelectedField <> nil ) and
             ( SelectedField.FieldName = 'CR_FECHA' ) and
             ( zFecha.Visible ) then
          begin
               zFecha.Visible:= False;
          end;
     end;
end;

procedure TEditHisPrestamos_DevEx.GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'CR_FECHA' ) then
          begin
               with zFecha do
               begin
                    Left := Rect.Left + GridRenglones.Left;
                    Top := Rect.Top + GridRenglones.Top;
                    Width := Column.Width + 2;
                    Visible := True;
               end;
          end;
     end;
end;

procedure TEditHisPrestamos_DevEx.ControlFecha;
begin
     if PageControl.ActivePage = Tabla then
     begin
          Self.ActiveControl := GridRenglones;
          GridRenglones.SelectedField := GridRenglones.Columns[ PrimerColumna + 1 ].Field;
          GridRenglones.SelectedField := GridRenglones.Columns[ PrimerColumna ].Field;
     end;
end;

procedure TEditHisPrestamos_DevEx.FonacotTab;
begin
     if PR_TIPO.Llave = Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO) then
     begin
          if ( Global.GetGlobalBooleano(K_GLOBAL_FONACOT_PRESTAMO_CONCILIA ) ) then
          begin
               PageControl.Pages[4].TabVisible := True;
               PageControl.Pages[3].TabVisible := False;  //Fonacot Antes
               PageControl.Pages[2].TabVisible := False;
               PageControl.Pages[1].TabVisible := False;
               PageControl.Pages[0].TabVisible := False;

               PageControl.ActivePageIndex := 4;

               {$ifndef CAJAAHORRO}
               TotalPago.Valor := dmRecursos.SumaPagosFonacot;
               TotalRetencion.Valor := dmRecursos.SumaPagosRetencion;
               {$endif}
               ZetaDBGridDBTableView.ApplyBestFit();
          end
          else
          begin
               if PageControl.ActivePageIndex = 2 then
                  PageControl.ActivePageIndex := 0;

               PageControl.Pages[4].TabVisible := False;
               PageControl.Pages[2].TabVisible := False;
               PageControl.Pages[3].TabVisible := True;
               PageControl.Pages[1].TabVisible := True;
               PageControl.Pages[0].TabVisible := True;
          end;
    end
    else
    begin
         if ( Global.GetGlobalBooleano(K_GLOBAL_FONACOT_PRESTAMO_CONCILIA ) ) then
         begin
              if (PageControl.ActivePageIndex = 4 ) then
                 PageControl.ActivePageIndex := 0;

              PageControl.Pages[0].TabVisible := True;
              PageControl.Pages[1].TabVisible := True;
              PageControl.Pages[2].TabVisible := True;
              PageControl.Pages[3].TabVisible := False; //Fonacot Antes
              PageControl.Pages[4].TabVisible := False;
         end
         else
         begin
               if (PageControl.ActivePageIndex = 3 ) then
                  PageControl.ActivePageIndex := 0;

               PageControl.Pages[0].TabVisible := True;
               PageControl.Pages[1].TabVisible := True;
               PageControl.Pages[2].TabVisible := True;
               PageControl.Pages[3].TabVisible := False;
               PageControl.Pages[4].TabVisible := False;
         end;
    end;
end;

procedure TEditHisPrestamos_DevEx.BBBorrarClick(Sender: TObject);
begin
     inherited;
     ControlFecha;
end;

procedure TEditHisPrestamos_DevEx.BBAgregarClick(Sender: TObject);
begin
     inherited;
     SeleccionaPrimerColumna;
end;

procedure TEditHisPrestamos_DevEx.KeyPress( var Key: Char );
begin
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( GridRenglones.SelectedField.FieldName = 'CR_FECHA' ) then
               begin
                    zFecha.SetFocus;
                    SendMessage( zFecha.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end
               else
               begin
                    if ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
                    begin
                         with GridRenglones do
                         begin
                              Key := #0;
                              SelectedIndex := PosicionaSiguienteColumna;
                              if ( Selectedindex = PrimerColumna ) then
                                 SetOk;
                         end;
                    end
                    else
                    begin
                         if ( Key = Chr( VK_ESCAPE ) ) then
                         begin
                              SeleccionaPrimerColumna;
                         end;
                    end;
               end;
          end;
     end
     else
     begin
          if ( ActiveControl = zFecha ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
          begin
               Key := #0;
               with GridRenglones do
               begin
                    SetFocus;
                    SelectedField := dmRecursos.cdsPCarAbo.FieldByName( 'CR_CARGO' );
               end;
          end;
     end;
     inherited;
end;

procedure TEditHisPrestamos_DevEx.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          GridRenglones.SelectedIndex := PosicionaSiguienteColumna;
          if ( GridRenglones.SelectedIndex = PrimerColumna ) then
             Setok;
     end;
end;

procedure TEditHisPrestamos_DevEx.PageControlChange(Sender: TObject);
begin
     with PageControl do
     begin
          if ( ActivePage = Tabla ) then
          begin
               if ( ZetaCommonTools.StrLleno( PR_TIPO.Llave ) ) then
               begin
                    SeleccionaPrimerColumna;
                    ControlFecha;
               end
               else
                   SetError;
          end;
     end;
end;

procedure TEditHisPrestamos_DevEx.PR_STATUSChange(Sender: TObject);
var lEnabled : Boolean;
begin
     {$ifndef CAJAAHORRO}
     PR_FON_MOT.Valor  := ord ( sbfNoAplica );

     lEnabled := False;

     if Global.GetGlobalBooleano (K_GLOBAL_FONACOT_PRESTAMO_CONCILIA) then
          if dmRecursos.EsPrestamoFonacot( PR_TIPO.Llave ) then
               if PR_STATUS.ItemIndex <> ord ( spActivo ) then
                  lEnabled := True;

    PR_FON_MOT.Enabled := lEnabled;
    lblPR_FON_MOT.Enabled := lEnabled;
    {$endif}

    inherited;
end;

procedure TEditHisPrestamos_DevEx.PR_TIPOValidKey(Sender: TObject);
begin
     inherited;
     {$ifdef FAPSA}
     ActivaControlesPrestamo;
     LimpiaControlesPrestamo;
     {$endif}
     if PR_TIPO.Llave <> FTipoAntes then
     begin
          FTipoAntes := PR_TIPO.Llave;
          FonacotTab;
     end;

     ActivarMotivoCancelacion;
end;

procedure TEditHisPrestamos_DevEx.PR_TIPOValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef FAPSA}
      LimpiaControlesPrestamo;
     {$endif}

     if PR_TIPO.Llave <> FTipoAntes then
     begin
          FTipoAntes := PR_TIPO.Llave;
          FonacotTab;
     end;

     ActivarMotivoCancelacion;
end;

procedure TEditHisPrestamos_DevEx.SetError;
begin
     PageControl.ActivePage := Datos;
     ZetaDialogo.ZError( 'Pr�stamo', 'No se Pueden Agregar Cargos o Abonos si el' + CR_LF +
                                     'C�digo del Pr�stamo est� Vacio ', 0 );
     FocusFirstControl;
end;

{$ifdef CAJAAHORRO}
function TEditHisPrestamos_DevEx.dmRecursos: TdmCajaAhorro;
begin
     Result := dmCajaAhorro;
end;
{$endif}


procedure TEditHisPrestamos_DevEx.Agregar;
begin
     {$ifdef CAJAAHORRO}
     ClientDataset.Agregar;
     {$else}
     inherited Agregar;
     {$endif}
end;

procedure TEditHisPrestamos_DevEx.OK_DevExClick(Sender: TObject);
var
   tMonto : Double;
begin
     if ( dmRecursos.EsPrestamoFonacot( PR_TIPO.Llave ) and ( Not ( Global.GetGlobalBooleano(K_GLOBAL_FONACOT_PRESTAMO_CONCILIA ) ) ) ) or
        ( Not dmRecursos.EsPrestamoFonacot( PR_TIPO.Llave ) ) then
     begin
          if ( not Inserting ) and ( ( tRetension <> F_RETENSION.Valor ) or ( tPlazo <> F_PLAZO.Valor ) ) then
          begin
               if ZConfirm('','�Desea modificar el monto prestado?',0, mbYes ) then
               begin
                    tMonto := F_RETENSION.Valor * F_PLAZO.Valor;
                    dmRecursos.cdsHisPrestamos.FieldByName('PR_MONTO').AsFloat  := tMonto;
                    tPlazo := F_PLAZO.Valor;
                    tRetension := F_RETENSION.Valor;
               end;
          end;
     end;
     inherited;
     ControlFecha;
end;

procedure TEditHisPrestamos_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     ControlFecha;
 	 {$ifdef FAPSA}
     	ActivaControlesPrestamo;
  	 {$endif}
end;

{***Correccion de bug (by: am): Agregado para corregir bug en VS 2013 que no actualiza el
          detalle de cargos/abonos al navegar entre los prestamos***}
procedure TEditHisPrestamos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if Field = nil then
     begin
          if dmRecursos.cdsHisPrestamos.State = dsBrowse then
          begin
               if dsRenglon.DataSet <> nil then //Para evitar que se realice antes de que el grid tenga datos
               begin
                    if FPuedeRefrescar and ( dmRecursos.cdsPCarAbo.ChangeCount = 0 ) then
                    begin
                         dmRecursos.cdsPCarAbo.Refrescar;
                         if (not (PR_TIPO.Llave = Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO)))
                            or (not ( Global.GetGlobalBooleano(K_GLOBAL_FONACOT_PRESTAMO_CONCILIA ) ))
                         then
                            PageControl.ActivePageIndex := 0;
                    end;
               end;
          end;

          {$ifndef CAJAAHORRO}
          dmRecursos.cdsHistorialFonacot.Refrescar;
          {$endif}
          if PR_TIPO.Llave <> FTipoAntes then
          begin
               FTipoAntes := PR_TIPO.Llave;
               FonacotTab;
          end;
     end;
end;

procedure TEditHisPrestamos_DevEx.dsHistorialFonacotDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     {$ifndef CAJAAHORRO}
     TotalPago.Valor := dmRecursos.SumaPagosFonacot;
     TotalRetencion.Valor := dmRecursos.SumaPagosRetencion;
     {$endif}
end;

procedure TEditHisPrestamos_DevEx.dsRenglonStateChange(Sender: TObject);
begin
     inherited;
     FPuedeRefrescar := ( dsRenglon.State = dsBrowse );
end;

procedure TEditHisPrestamos_DevEx.dxBarButton_ExportarBtnClick(Sender: TObject);
begin
     if PR_TIPO.Llave = Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO) then
        DoExportarFonacot
     else
         inherited;

end;

procedure TEditHisPrestamos_DevEx.dxBarButton_ImprimirBtnClick(Sender: TObject);
begin
     if PR_TIPO.Llave = Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO) then
        DoPrintFonacot
     else
         inherited;
end;

{$ifdef FAPSA}
procedure TEditHisPrestamos_DevEx.ActivaControlesPrestamo;
begin
    with dmRecursos do
     begin
          if (dmTablas.cdsTPresta.FieldByName( 'TB_CODIGO' ).AsString <> cdsHisPrestamos.FieldByName( 'PR_TIPO' ).AsString )then
             dmTablas.cdsTPresta.Locate( 'TB_CODIGO', cdsHisPrestamos.FieldByName( 'PR_TIPO' ).AsString, [] );

          EsPrestamoDias := (dmTablas.cdsTPresta.FieldByName( 'TB_NUMERO' ).AsInteger = 1);

          PR_MONTO.Enabled := not EsPrestamoDias;
          PR_MONTO_S.Enabled := not EsPrestamoDias;
          PR_TASA.Enabled := not EsPrestamoDias;
          PR_INTERES.Enabled := not EsPrestamoDias;
          PR_MONTO_INT.Enabled := not EsPrestamoDias;
          if EsPrestamoDias then
             Label16.Caption := 'D�as:'
          else
              Label16.Caption := 'Duraci�n:';
     end;    
end;

procedure TEditHisPrestamos_DevEx.LimpiaControlesPrestamo;
begin
    if( dmTablas.cdsTPresta.FieldByName( 'TB_NUMERO' ).AsInteger = 1 ) and
       ( dmRecursos.cdsHisPrestamos.State in [dsEdit] ) then
     begin
          with dmRecursos.cdsHisPrestamos do
          begin
               FieldByName( 'PR_MONTO' ).AsInteger := 0;
               FieldByName( 'PR_MONTO_S' ).AsInteger := 0;
               FieldByName( 'PR_TASA' ).AsInteger := 0;
               FieldByName( 'PR_INTERES' ).AsInteger := 0;
               FieldByName( 'PR_MESES' ).AsInteger := 0;
               FieldByName( 'PR_PAGOS' ).AsInteger := 0;
          end;
     end;
end;

procedure TEditHisPrestamos_DevEx.HabilitaControles;
begin
     inherited HabilitaControles;
     Self.ActivaControlesPrestamo;
end;     
{$endif}

procedure TEditHisPrestamos_DevEx.DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
begin
     inherited;
     {$ifdef FAPSA}
      ActivaControlesPrestamo;
     {$endif}
end;

procedure TEditHisPrestamos_DevEx.SetEditarSoloActivos;
begin
     PR_TIPO.EditarSoloActivos := TRUE;
end;

procedure TEditHisPrestamos_DevEx.DoPrintFonacot;
var
   sMensaje: String;
begin
     if PuedeImprimir( sMensaje ) then
        ImprimirFonacot
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

//DevEx(by am): Modificado para que pueda imprimir los grids de DevExpress
procedure TEditHisPrestamos_DevEx.ImprimirFonacot;
var
   lImprime: Boolean;
   i: integer;
   Valor : Variant;
begin
     lImprime := False;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( Components[ i ] is TcxGridDBTableView )  then
          begin
               lImprime := True;
               if ZetaDialogo.zConfirm( 'Imprimir...',
                                        '� Desea imprimir el grid de la pantalla ' + Caption + ' ?',
                                        0,
                                        mbYes ) then
               begin
                    Valor := ValoresGrid;
                    if (Components[ i ] is TDBGrid) then
                       ZImprimeGrid.ImprimirGrid( TDBGrid( Components[ i ] ), TDBGrid( Components[ i ] ).DataSource.DataSet, Caption, 'IM',
                                                                                                         Valor[0],Valor[1],Valor[2],Valor[3])
                    else
                    begin
                         ZImprimeGrid.ImprimirGrid( ( Components[ i ] as TcxGridDBTableView), (Components[ i ] as TcxGridDBTableView).DataController.DataSource.DataSet , Caption, 'IM',
                                                    Valor[0],Valor[1],Valor[2],Valor[3]);
                    end;

               end;
               Break;
          end;
     end;
     if not lImprime and ZetaDialogo.zConfirm( 'Imprimir...', '� Desea imprimir la pantalla ' + Caption + ' ?', 0, mbYes ) then
        Print;
end;


procedure TEditHisPrestamos_DevEx.ActivarMotivoCancelacion;
var lEnabled : Boolean;
begin
     inherited;
     lEnabled := False;
     if Global.GetGlobalBooleano (K_GLOBAL_FONACOT_PRESTAMO_CONCILIA) then
          if dmRecursos.EsPrestamoFonacot( PR_TIPO.Llave ) and ( PR_STATUS.ItemIndex <> ord ( spActivo ) ) then
               lEnabled := True;

     PR_FON_MOT.Enabled := lEnabled;
     lblPR_FON_MOT.Enabled := lEnabled;
end;

procedure TEditHisPrestamos_DevEx.DoExportarFonacot;
var
   sMensaje: String;
begin
     if PuedeImprimir( sMensaje ) then
        ExportarFonacot
     else
         ZetaDialogo.zInformation( Caption, 'No tiene permiso para exportar registros', 0 );
end;

 //DevEx(by am): Se modifica el metodo de exportar para que peuda utilizar el nuevo Grid.
 procedure TEditHisPrestamos_DevEx.ExportarFonacot;
 var
    lExporta: Boolean;
    i: integer;
    Valor : Variant;
begin
     lExporta:= FALSE;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( Components[ i ] is TcxGridDBTableView ) then
          begin
               lExporta:= TRUE;
               if zConfirm( 'Exportar...', '� Desea exportar a excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
               begin
                    Valor := ValoresGrid;
                    if (Components[ i ] is TDBGrid) then
                       ZImprimeGrid.ExportarGrid( TDBGrid( Components[ i ] ),
                                                                                                         TDBGrid( Components[ i ] ).DataSource.DataSet, Caption, 'IM',
                                                                                                         Valor[0],Valor[1],Valor[2],Valor[3])
                    else
                    begin

                         ZImprimeGrid.ExportarGrid(  ( Components[ i ] as TcxGridDBTableView ),
                                                     (Components[ i ] as TcxGridDBTableView).DataController.DataSource.DataSet, Caption, 'IM',
                                                     Valor[0],Valor[1],Valor[2],Valor[3]);
                    end;
               end;
               Break;
          end;
     end;
     if not lExporta then
        zInformation( 'Exportar...',Format( 'La pantalla %s no contiene ning�n grid para exportar', [Caption] ), 0);
end;


end.
