unit FGridEmpTools_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ExtCtrls, StdCtrls, Mask, ZetaFecha, ZetaMessages,
  ZetaSmartLists, ZBaseGridEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  ZetaKeyLookup_DevEx, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TGridEmpTools_DevEx = class(TBaseGridEdicion_DevEx)
    PanelEmp: TPanel;
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaKeyLookup_DevEx;
    FechaLbl: TLabel;
    KT_FEC_INI: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaTool;
    procedure BuscaTalla;
    procedure AsignaDatosTools;
    procedure ValidaDatosTools;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  protected
    procedure Connect; override;
    procedure Buscar; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;

var
  GridEmpTools_DevEx: TGridEmpTools_DevEx;

implementation

uses dRecursos, dCliente, dCatalogos, dTablas, ZetaCommonClasses, ZetaCommonTools, FAutoClasses;

{$R *.DFM}

procedure TGridEmpTools_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H11613_Entregar_herramienta;
     with dmCliente do
     begin
          CB_CODIGO.SetLlaveDescripcion( IntToStr( Empleado ), GetDatosEmpleadoActivo.Nombre );  // Evita que se Haga el DoLookup del Control
          CB_CODIGO.LookupDataset := cdsEmpleadoLookUp;
          KT_FEC_INI.Valor := FechaDefault;
     end;
end;

procedure TGridEmpTools_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     self.ActiveControl:= CB_CODIGO;
end;

procedure TGridEmpTools_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmTablas do
     begin
          cdsMotTool.Conectar;
          cdsTallas.Conectar;
     end;
     dmCatalogos.cdsTools.Conectar;
     with dmRecursos do
     begin
          cdsGridEmpTools.Refrescar;
          DataSource.DataSet:= cdsGridEmpTools;
     end;
end;

procedure TGridEmpTools_DevEx.Buscar;
begin
     inherited;
     with ZetaDBGrid.SelectedField do
     begin
          if ( FieldName = 'TO_CODIGO' ) then
             BuscaTool
          else if ( FieldName = 'KT_TALLA' ) then
             BuscaTalla;
     end;
end;

procedure TGridEmpTools_DevEx.BuscaTool;
var
   sTool, sToolDesc: String;
begin
     with dmRecursos.cdsGridEmpTools do
     begin
          sTool := FieldByName( 'TO_CODIGO' ).AsString;
          if dmCatalogos.cdsTools.Search_DevEx( '', sTool, sToolDesc ) then
          begin
               if ( sTool <> FieldByName( 'TO_CODIGO' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( 'TO_CODIGO' ).AsString := sTool;
               end;
          end;
     end;
end;

procedure TGridEmpTools_DevEx.BuscaTalla;
var
   sTalla, sTallaDesc: String;
begin
     with dmRecursos.cdsGridEmpTools do
     begin
          sTalla := FieldByName( 'KT_TALLA' ).AsString;
          if dmTablas.cdsTallas.Search_DevEx( '', sTalla, sTallaDesc ) then
          begin
               if ( sTalla <> FieldByName( 'KT_TALLA' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( 'KT_TALLA' ).AsString := sTalla;
               end;
          end;
     end;
end;

procedure TGridEmpTools_DevEx.ValidaDatosTools;
begin
     if StrVacio( CB_CODIGO.Llave ) then
     begin
          ActiveControl := CB_CODIGO;
          DataBaseError( 'No Se Ha Especificado Un Empleado' );
     end;
end;

procedure TGridEmpTools_DevEx.AsignaDatosTools;
begin
     with dmRecursos.cdsGridEmpTools do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          DisableControls;
          try
             First;
             while not EOF do
             begin
                  Edit;
                  FieldByName( 'CB_CODIGO' ).AsString := CB_CODIGO.Llave;
                  FieldByName( 'KT_FEC_INI' ).AsDateTime := KT_FEC_INI.Valor;
                  FieldByName( 'KT_ACTIVO' ).AsString := K_GLOBAL_SI;
                  FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
                  Next;
             end;
          finally
             EnableControls;
          end;
     end;
end;

procedure TGridEmpTools_DevEx.OKClick(Sender: TObject);
begin
     if dmCliente.ModuloAutorizado( okHerramientas ) then
        try
           ValidaDatosTools;
           AsignaDatosTools;

           inherited;

        except
           self.ModalResult:= mrNone;
           raise;
        end;
end;

procedure TGridEmpTools_DevEx.KeyPress(var Key: Char);
begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr(9) ) and ( Key <> #0 ) and ZetaCommonTools.EsMinuscula( Key ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'TO_CODIGO' ) or
                  ( ZetaDBGrid.SelectedField.FieldName = 'KT_TALLA' ) then
                  Key := Chr( Ord( Key ) - 32 );
          end;
     end;
end;

procedure TGridEmpTools_DevEx.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          if ( ZetaDBGrid.SelectedIndex = PrimerColumna ) and
             ( StrVacio( dmRecursos.cdsGridEmpTools.FieldByName( 'TO_CODIGO' ).AsString ) ) then
          begin
               if OK_DevEx.Enabled then
                  OK_DevEx.SetFocus
               else
                  Cancelar_DevEx.SetFocus;
          end
          else
          begin
               ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
               if ( ZetaDBGrid.SelectedIndex = PrimerColumna ) then
                  SeleccionaSiguienteRenglon;
          end;
     end;
end;

procedure TGridEmpTools_DevEx.OK_DevExClick(Sender: TObject);
begin
    if dmCliente.ModuloAutorizado( okHerramientas ) then
        try
           ValidaDatosTools;
           AsignaDatosTools;

           inherited;

        except
           self.ModalResult:= mrNone;
           raise;
        end;
end;

end.
