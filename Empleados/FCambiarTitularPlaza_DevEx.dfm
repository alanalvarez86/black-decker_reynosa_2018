inherited CambiarTitularPlaza_DevEx: TCambiarTitularPlaza_DevEx
  Top = 212
  Caption = 'Cambiar Titular de la plaza'
  ClientHeight = 204
  ClientWidth = 402
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 26
    Top = 17
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Titular nuevo:'
  end
  object lblFecha: TLabel [1]
    Left = 6
    Top = 42
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de cambio:'
  end
  object lblObservaciones: TLabel [2]
    Left = 17
    Top = 67
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observaciones:'
  end
  inherited PanelBotones: TPanel
    Top = 168
    Width = 402
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 232
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 312
      Cancel = True
    end
  end
  object Empleado: TZetaKeyLookup_DevEx [4]
    Left = 93
    Top = 13
    Width = 300
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 0
    TabStop = False
    WidthLlave = 60
    OnValidKey = EmpleadoValidKey
  end
  object Fecha: TZetaFecha [5]
    Left = 93
    Top = 38
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 1
    Text = '14/jun/06'
    Valor = 38882.000000000000000000
  end
  object Observaciones: TcxMemo [6]
    Left = 93
    Top = 63
    Properties.MaxLength = 255
    Properties.ScrollBars = ssVertical
    TabOrder = 2
    Height = 93
    Width = 300
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
