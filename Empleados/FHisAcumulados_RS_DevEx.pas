unit FHisAcumulados_RS_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, Grids, DBGrids, ExtCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions, ZetaKeyCombo;

type
  THisAcumulados_RS_DevEx = class(TBaseGridLectura_DevEx)
    CO_NUMERO: TcxGridDBColumn;
    CO_DESCRIP: TcxGridDBColumn;
    AC_ANUAL: TcxGridDBColumn;
    AC_MES_01: TcxGridDBColumn;
    AC_MES_02: TcxGridDBColumn;
    AC_MES_03: TcxGridDBColumn;
    AC_MES_04: TcxGridDBColumn;
    AC_MES_05: TcxGridDBColumn;
    AC_MES_06: TcxGridDBColumn;
    AC_MES_07: TcxGridDBColumn;
    AC_MES_08: TcxGridDBColumn;
    AC_MES_09: TcxGridDBColumn;
    AC_MES_10: TcxGridDBColumn;
    AC_MES_11: TcxGridDBColumn;
    AC_MES_12: TcxGridDBColumn;
    AC_MES_13: TcxGridDBColumn;
    pnlRazonSocial: TPanel;
    lblRazonSocial: TLabel;
    cbxRazonSocial: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbxRazonSocialChange(Sender: TObject);
    procedure LlenaRazonSocialAcumulados;
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

const
     K_NINGUNO = 0;
     K_MENSAJE = 'No es posible %s los acumulados del empleado, es necesario elegir una raz�n social para realizar ajustes por concepto y empleado';

var
  HisAcumulados_RS_DevEx: THisAcumulados_RS_DevEx;

implementation

{$R *.DFM}

uses DRecursos,
     DCatalogos,
     DCliente,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo,
     ZetaCommonTools;

procedure THisAcumulados_RS_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stSistema;
     HelpContext:= H10143_Acumulados;
end;

procedure THisAcumulados_RS_DevEx.Connect;
begin
     LlenaRazonSocialAcumulados;
     dmCatalogos.cdsConceptos.Conectar;
     with dmRecursos do
     begin
          RazonSocialAcumulados := cbxRazonSocial.Llave;
          cdsHisAcumuladosRS.Refrescar;
          DataSource.DataSet:= cdsHisAcumuladosRS;
     end;
end;

procedure THisAcumulados_RS_DevEx.Refresh;
begin
     dmRecursos.cdsHisAcumuladosRS.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisAcumulados_RS_DevEx.Agregar;
begin
     if not StrVacio( dmCliente.RazonSocialAcumulado ) then
        dmRecursos.cdsHisAcumuladosRS.Agregar
     else
         ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( ukInsert ) ), Format( K_MENSAJE, [ LowerCase( ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( ukInsert ) ) ) ] ), 0 );
end;

procedure THisAcumulados_RS_DevEx.Borrar;
var
   iAC_ID : Integer;
begin
     iAC_ID := dmRecursos.cdsHisAcumuladosRS.FieldByName( 'AC_ID' ).AsInteger;
     if iAC_ID > K_NINGUNO then
     begin
          dmRecursos.cdsHisAcumuladosRS.Borrar;
          ZetaDBGridDBTableView.ApplyBestFit();
     end
     else
         ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( ukDelete ) ), Format( K_MENSAJE, [ LowerCase( ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( ukDelete ) ) ) ] ), 0 );

end;

procedure THisAcumulados_RS_DevEx.cbxRazonSocialChange(Sender: TObject);
begin
     inherited;
     with dmRecursos do
     begin
          if cbxRazonSocial.Llave <> VACIO then
          begin
               RazonSocialAcumulados := cbxRazonSocial.Llave;
               dmCliente.RazonSocialAcumulado := RazonSocialAcumulados;
          end
          else
              RazonSocialAcumulados := dmCliente.RazonSocialAcumulado;
          cdsHisAcumuladosRS.Refrescar;
     end;
end;

procedure THisAcumulados_RS_DevEx.LlenaRazonSocialAcumulados;
const
     K_RAZON_TODOS_TEXTO  = 'Suma de las razones sociales';
begin

     with cbxRazonSocial.Lista do
     begin
          BeginUpdate;
          cbxRazonSocial.Sorted := False;
          try
             Clear;
             with dmCatalogos.cdsRSocial do
             begin
                  Conectar;
                  First;
                  Add( VACIO + ' = ' + K_RAZON_TODOS_TEXTO );
                  while ( not EOF ) do
                  begin
                       Add( FieldByName('RS_CODIGO').AsString + ' = ' + FieldByName('RS_NOMBRE').AsString );
                       Next;
                  end;
                  First;
             end;
          finally
                 EndUpdate;
          end;
          if dmCliente.RazonSocialAcumulado <> VACIO then
          begin
               cbxRazonSocial.Llave := dmCliente.RazonSocialAcumulado;
          end
          else
          begin
               cbxRazonSocial.ItemIndex := 0;
               dmCliente.RazonSocialAcumulado := cbxRazonSocial.Llave;
          end;
     end;
end;

procedure THisAcumulados_RS_DevEx.Modificar;
begin
     if not StrVacio( dmCliente.RazonSocialAcumulado ) then
        dmRecursos.cdsHisAcumuladosRS.Modificar
     else
         ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( ukModify ) ), Format( K_MENSAJE, [ LowerCase( ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( ukModify ) ) ) ] ), 0 );
end;

procedure THisAcumulados_RS_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
  inherited;

end;

end.
