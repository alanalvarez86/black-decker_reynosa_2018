inherited EditHisTools_DevEx: TEditHisTools_DevEx
  Left = 591
  Top = 234
  Caption = 'Entrega de Herramienta'
  ClientHeight = 330
  ClientWidth = 424
  PixelsPerInch = 96
  TextHeight = 13
  object LblFecha: TLabel [0]
    Left = 51
    Top = 67
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = '&Fecha:'
    FocusControl = KT_FEC_INI
  end
  object LblHerramienta: TLabel [1]
    Left = 24
    Top = 91
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = '&Herramienta:'
    FocusControl = TO_CODIGO
  end
  object LblReferencia: TLabel [2]
    Left = 29
    Top = 114
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = '&Referencia:'
  end
  object LblTalla: TLabel [3]
    Left = 58
    Top = 139
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = '&Talla:'
    FocusControl = KT_TALLA
  end
  object LblObservaciones: TLabel [4]
    Left = 10
    Top = 162
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Obser&vaciones:'
    FocusControl = KT_COMENTA
  end
  inherited PanelBotones: TPanel
    Top = 294
    Width = 424
    inherited OK_DevEx: TcxButton
      Left = 262
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 341
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 424
    inherited Splitter: TSplitter
      Left = 289
    end
    inherited ValorActivo1: TPanel
      Width = 289
      inherited textoValorActivo1: TLabel
        Width = 283
      end
    end
    inherited ValorActivo2: TPanel
      Left = 292
      Width = 132
      inherited textoValorActivo2: TLabel
        Width = 126
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 12
  end
  object KT_FEC_INI: TZetaDBFecha [8]
    Left = 88
    Top = 62
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 3
    Text = '13/jun/00'
    Valor = 36690.000000000000000000
    DataField = 'KT_FEC_INI'
    DataSource = DataSource
  end
  object TO_CODIGO: TZetaDBKeyLookup_DevEx [9]
    Left = 88
    Top = 87
    Width = 325
    Height = 21
    LookupDataset = dmCatalogos.cdsTools
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 4
    TabStop = True
    WidthLlave = 60
    DataField = 'TO_CODIGO'
    DataSource = DataSource
  end
  object KT_TALLA: TZetaDBKeyLookup_DevEx [10]
    Left = 88
    Top = 135
    Width = 325
    Height = 21
    LookupDataset = dmTablas.cdsTallas
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 5
    TabStop = True
    WidthLlave = 60
    DataField = 'KT_TALLA'
    DataSource = DataSource
  end
  object KT_COMENTA: TDBEdit [11]
    Left = 88
    Top = 159
    Width = 300
    Height = 21
    DataField = 'KT_COMENTA'
    DataSource = DataSource
    MaxLength = 40
    TabOrder = 6
  end
  object GBDevolucion: TGroupBox [12]
    Left = 16
    Top = 189
    Width = 393
    Height = 97
    Caption = ' Devoluci'#243'n '
    TabOrder = 7
    object LblFecDev: TLabel
      Left = 37
      Top = 41
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fech&a:'
      FocusControl = KT_FEC_FIN
    end
    object LblMotivo: TLabel
      Left = 35
      Top = 65
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = '&Motivo:'
      FocusControl = KT_MOT_FIN
    end
    object KT_ACTIVO: TDBCheckBox
      Left = 25
      Top = 19
      Width = 64
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Re&gres'#243':'
      DataField = 'KT_ACTIVO'
      DataSource = DataSource
      TabOrder = 0
      ValueChecked = 'N'
      ValueUnchecked = 'S'
      OnClick = KT_ACTIVOClick
    end
    object KT_FEC_FIN: TZetaDBFecha
      Left = 76
      Top = 37
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '13/jun/00'
      Valor = 36690.000000000000000000
      DataField = 'KT_FEC_FIN'
      DataSource = DataSource
    end
    object KT_MOT_FIN: TZetaDBKeyLookup_DevEx
      Left = 76
      Top = 62
      Width = 309
      Height = 21
      LookupDataset = dmTablas.cdsMotTool
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 2
      TabStop = True
      WidthLlave = 60
      DataField = 'KT_MOT_FIN'
      DataSource = DataSource
    end
  end
  object ZetaDBEdit1: TZetaDBEdit [13]
    Left = 88
    Top = 110
    Width = 129
    Height = 21
    TabOrder = 8
    Text = 'KT_REFEREN'
    DataField = 'KT_REFEREN'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
