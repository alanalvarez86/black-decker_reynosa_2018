inherited Aprobados_DevEx: TAprobados_DevEx
  Left = 308
  Top = 209
  Caption = 'Aprobados'
  ClientWidth = 617
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 617
    inherited OK_DevEx: TcxButton
      Left = 450
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 530
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 617
    inherited ValorActivo2: TPanel
      Width = 291
      inherited textoValorActivo2: TLabel
        Width = 285
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 121
    Width = 617
    Height = 266
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'N'#250'mero'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre'
        Width = 350
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KC_EVALUA'
        Title.Caption = 'Evaluaci'#243'n'
        Width = 82
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KC_HORAS'
        Title.Caption = 'Horas'
        Width = 73
        Visible = True
      end>
  end
  inherited Panel1: TPanel
    Width = 617
    Height = 71
    inherited lblFolio: TLabel
      Left = 496
    end
    inherited SE_FOLIO: TZetaDBTextBox
      Left = 523
    end
    object Label3: TLabel
      Left = 479
      Top = 29
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = 'Inscritos:'
    end
    object SE_INSCRITO: TZetaDBTextBox
      Left = 523
      Top = 27
      Width = 73
      Height = 17
      AutoSize = False
      Caption = 'SE_INSCRITO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SE_INSCRITO'
      DataSource = dsSesion
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lblAprobados: TLabel
      Left = 467
      Top = 48
      Width = 54
      Height = 13
      Alignment = taRightJustify
      Caption = 'Aprobados:'
    end
    object lblPeriodo: TLabel
      Left = 7
      Top = 48
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Periodo:'
    end
    object ztbPeriodo: TZetaTextBox
      Left = 48
      Top = 46
      Width = 391
      Height = 17
      AutoSize = False
      Caption = 'ztbPeriodo'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object ztbAprobados: TZetaTextBox
      Left = 523
      Top = 46
      Width = 73
      Height = 17
      AutoSize = False
      Caption = 'ztbAprobados'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    OnDataChange = DataSourceDataChange
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
