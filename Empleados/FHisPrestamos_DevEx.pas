unit FHisPrestamos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, Grids,
  ZetaDBGrid, DBGrids, ExtCtrls, ZetaDBTextBox, StdCtrls, ZetaKeyCombo,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, System.Actions;

type
  THisPrestamos_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    lblTipo: TLabel;
    PR_TIPO: TZetaKeyCombo;
    Panel2: TPanel;
    PR_FECHA: TcxGridDBColumn;
    PR_TIPO_GRID: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    PR_REFEREN: TcxGridDBColumn;
    PR_MONTO: TcxGridDBColumn;
    PR_SALDO: TcxGridDBColumn;
    PR_STATUS_GRID: TcxGridDBColumn;
    PR_SUB_CTA: TcxGridDBColumn;
    PR_OBSERVA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure PR_TIPOChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    //FActualizandoLista: Boolean;
    //procedure LlenaListaStatus;
    //procedure LlenaTPresta;
    //function PreparaFiltro: String;
    procedure CambiaOrdenamiento;
    //procedure ControlFiltro; //DevEx (by am): Anteriormente se aplicaba el filtro en el metodo de CuentaRegistros. Observarlo en forma clasica
    procedure ConfigAgrupamiento;
  protected
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  HisPrestamos_DevEx: THisPrestamos_DevEx;

implementation

{$R *.DFM}

uses dRecursos, dSistema, DTablas, ZetaCommonLists, ZetaCommonTools, ZetaCommonClasses;

procedure THisPrestamos_DevEx.Connect;
begin
     dmTablas.cdsTPresta.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsHisPrestamos.Conectar;
          DataSource.DataSet:= cdsHisPrestamos;
          cdsHistorialFonacot.Conectar;
     end;
     //LlenaTPresta;
     //LlenaListaStatus;

     CambiaOrdenamiento;
     //ControlFiltro;
end;

{procedure THisPrestamos_DevEx.LlenaListaStatus;
var
   iOldIndexStatus: Integer;
begin
     FActualizandoLista:= TRUE;
     iOldIndexStatus:= PR_STATUS.ItemIndex;
     try
        ZetaCommonLists.LlenaLista( lfStatusPrestamo, PR_STATUS.Items );
        PR_STATUS.Items.Insert( 0, 'Todos' );
     finally
           PR_STATUS.ItemIndex:= iMax(iOldIndexStatus,0);
           FActualizandoLista:= FALSE;
     end;
end;}

{
procedure THisPrestamos_DevEx.LlenaTPresta;
var
   sOldLlave: String;
begin
      with PR_TIPO do
      begin
           FActualizandoLista:= TRUE;
           sOldLlave:= PR_TIPO.Llave;
           with Lista do
           begin
                BeginUpdate;
                try
                   Clear;
                   with dmTablas.cdsTPresta do
                   begin
                        Conectar;
                        First;
                        Add( 'X8G7=Todos' );
                        while not EOF do
                        begin
                             Add( FieldByName('TB_CODIGO').AsString + '=' + FieldByName('TB_ELEMENT').AsString );
                             Next;
                        end;
                   end;
                finally
                       EndUpdate;
                end;
           end;
           Llave:= sOldLlave;
           ItemIndex:= iMax(ItemIndex, 0);
           FActualizandoLista:= FALSE;
      end;
end;
}

procedure THisPrestamos_DevEx.Refresh;
begin
     dmRecursos.cdsHisPrestamos.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisPrestamos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     {$ifdef CAJAAHORRO}
     TipoValorActivo2 := stTipoAhorro;
     {$endif}
     //CambiaOrdenamiento; //(@am): Se cambia por GridMode.
     HelpContext:= H10142_Prestamos;
end;

procedure THisPrestamos_DevEx.Agregar;
begin
     dmRecursos.cdsHisPrestamos.Agregar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisPrestamos_DevEx.Borrar;
begin
     dmRecursos.cdsHisPrestamos.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisPrestamos_DevEx.Modificar;
begin
     dmRecursos.cdsHisPrestamos.Modificar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisPrestamos_DevEx.CambiaOrdenamiento;
const
     K_DESCENDENTE = 1;
begin
    dmRecursos.OrdenPrestamo:= TOrdenKardex( K_DESCENDENTE );
end;

procedure THisPrestamos_DevEx.PR_TIPOChange(Sender: TObject);
begin
     inherited;
{
     if not FActualizandoLista then
        ControlFiltro;
}
end;

{
procedure THisPrestamos_DevEx.ControlFiltro;
begin
     if strLleno( pr_TIPO.Llave )  then
        dmRecursos.AplicaFiltrosPrestamo( PreparaFiltro );
end;

function THisPrestamos_DevEx.PreparaFiltro: String;
var
   sLlave: String;
begin
     Result := VACIO;
     sLlave := PR_TIPO.Llave;
     if( sLlave <> 'X8G7' )then
         Result := Format( 'PR_TIPO = %s', [EntreComillas(sLlave)] );
end;
}

//DevEx (by am): Metodo agregado para determinar por que columnas se podra agrupar
procedure THisPrestamos_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     PR_FECHA.Options.Grouping:= FALSE;
     PR_TIPO_GRID.Options.Grouping:= FALSE;
     TB_ELEMENT.Options.Grouping := TRUE; //Descripcion
     PR_REFEREN.Options.Grouping:= FALSE;
     PR_MONTO.Options.Grouping:= FALSE;
     PR_SALDO.Options.Grouping:= FALSE;
     PR_STATUS_GRID.Options.Grouping:= FALSE;
     PR_SUB_CTA.Options.Grouping:= FALSE;
     PR_OBSERVA.Options.Grouping:= FALSE;
end;

procedure THisPrestamos_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
      {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;
  ConfigAgrupamiento;

end;

end.
