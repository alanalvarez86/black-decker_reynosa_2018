inherited Enrolamiento_DevEx: TEnrolamiento_DevEx
  Left = 195
  Top = 244
  Caption = 'Enrolamiento de Huellas'
  ClientHeight = 142
  ClientWidth = 383
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 389
  ExplicitHeight = 171
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 106
    Width = 383
    ExplicitTop = 106
    ExplicitWidth = 383
    inherited OK_DevEx: TcxButton
      Left = 304
      Cancel = True
      Enabled = False
      ExplicitLeft = 304
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 384
      Cancel = False
      Visible = False
      ExplicitLeft = 384
    end
  end
  object lblMsg: TStaticText [1]
    Left = 80
    Top = 8
    Width = 291
    Height = 68
    Align = alCustom
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Inicializando dispositivo Biometrico...'
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 2
  end
  object ProgresoHuellas: TProgressBar [2]
    Left = 83
    Top = 83
    Width = 289
    Height = 17
    Anchors = [akLeft, akRight, akBottom]
    Position = 100
    TabOrder = 3
    Visible = False
  end
  object BtnEnrolar: TcxButton [3]
    Left = 8
    Top = 8
    Width = 65
    Height = 26
    Hint = 'Inicia el proceso de captura de huella digital.'
    Caption = '&Enrolar'
    Default = True
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = BtnEnrolarClick
  end
  object Recargar: TcxButton [4]
    Left = 8
    Top = 75
    Width = 65
    Height = 26
    Hint = 'Recargar'
    Caption = 'Recargar'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    TabStop = False
    OnClick = RecargarClick
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object TimerInicio: TTimer
    Interval = 500
    OnTimer = TimerInicioTimer
    Left = 344
    Top = 8
  end
  object TimerEnrollTimeout: TTimer
    Enabled = False
    Interval = 7000
    OnTimer = TimerEnrollTimeoutTimer
    Left = 272
    Top = 8
  end
end
