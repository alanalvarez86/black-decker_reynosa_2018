inherited Enrolamiento: TEnrolamiento
  Left = 195
  Top = 244
  Caption = 'Enrolamiento de Huellas'
  ClientHeight = 142
  ClientWidth = 383
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Recargar: TSpeedButton [0]
    Left = 8
    Top = 80
    Width = 65
    Height = 22
    Caption = 'Recargar'
    Enabled = False
    OnClick = RecargarClick
  end
  inherited PanelBotones: TPanel
    Top = 106
    Width = 383
    inherited Cancelar: TBitBtn [0]
      Left = 297
      Visible = False
    end
    inherited OK: TBitBtn [1]
      Left = 297
      Enabled = False
    end
  end
  object lblMsg: TStaticText
    Left = 80
    Top = 8
    Width = 291
    Height = 68
    Align = alCustom
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight, akBottom]
    AutoSize = False
    BorderStyle = sbsSunken
    Caption = 'Inicializando dispositivo Biometrico...'
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 2
  end
  object BtnEnrolar: TButton
    Left = 8
    Top = 8
    Width = 65
    Height = 33
    Hint = 'Inicia el proceso de captura de huella digital.'
    Caption = '&Enrolar'
    Default = True
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = BtnEnrolarClick
  end
  object ProgresoHuellas: TProgressBar
    Left = 83
    Top = 83
    Width = 289
    Height = 17
    Anchors = [akLeft, akRight, akBottom]
    Position = 100
    TabOrder = 3
    Visible = False
  end
  object TimerInicio: TTimer
    Interval = 500
    OnTimer = TimerInicioTimer
    Left = 344
    Top = 8
  end
  object TimerEnrollTimeout: TTimer
    Enabled = False
    Interval = 7000
    OnTimer = TimerEnrollTimeoutTimer
    Left = 320
    Top = 8
  end
end
