unit FKardexBase_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, Mask,
  ComCtrls, Db, Buttons, ExtCtrls, 
  ZetaDBTextBox, dbClient,
  ZetaFecha, ZetaCommonClasses, ZetaNumero, ZetaSmartLists,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxPC, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxDBEdit, dxSkinsDefaultPainters;

type
  TKardexBase_DevEx = class(TBaseEdicion_DevEx)
    PageControl_DevEx: TcxPageControl;
    General_DevEx: TcxTabSheet;
    Bitacora_DevEx: TcxTabSheet;
    Notas_DevEx: TcxTabSheet;
    PanelFecha: TPanel;
    Label1: TLabel;
    CB_FECHA: TZetaDBFecha;
    CB_NOTA: TcxDBMemo;
    GCapturoLbl: TLabel;
    GCapturaLbl: TLabel;
    GNivelLbl: TLabel;
    GStatusLbl: TLabel;
    CB_GLOBAL: TDBCheckBox;
    CB_FEC_CAP: TZetaDBTextBox;
    CB_NIVEL: TZetaDBTextBox;
    Label2: TLabel;
    CB_COMENTA: TDBEdit;
    US_DESCRIP: TZetaDBTextBox;
    CB_STATUS: TZetaDBTextBox;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
  protected
    procedure Connect;override;
    function CheckDerechos(const iDerecho: Integer): Boolean;override;
  public
    { Public declarations }
  end;

var
  KardexBase_DevEx: TKardexBase_DevEx;

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonTools,
     DCliente;

{$R *.DFM}

{ TKardexBase }

procedure TKardexBase_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := CB_FECHA;
end;

procedure TKardexBase_DevEx.FormShow(Sender: TObject);
begin
     PageControl_DevEx.ActivePage := General_DevEx;
     inherited;
end;

procedure TKardexBase_DevEx.Connect;
begin
{     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsEditHisKardex.Conectar;
          DataSource.DataSet := cdsEditHisKardex;
     end; }
end;

procedure TKardexBase_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     if ( TClientDataSet( DataSource.DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
        ( not ( DataSource.DataSet.State in [ dsEdit, dsInsert ] ) ) then    //y no se tienen cambios pendientes
        Close;
end;

function TKardexBase_DevEx.CheckDerechos( const iDerecho: Integer ): Boolean;
begin
     Result := Inherited CheckDerechos( iDerecho );
     if Result and NOT dmCliente.ModoSuper and
        zStrToBool( DataSource.Dataset.FieldByName( 'TB_SISTEMA' ).AsString ) then
        Result := ZAccesosMgr.CheckDerecho( D_EMP_EXP_KARDEX, K_DERECHO_SIST_KARDEX );
end;

procedure TKardexBase_DevEx.OK_DevExClick(Sender: TObject);
begin
 inherited;
     if ( TClientDataSet( DataSource.DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
        ( not ( DataSource.DataSet.State in [ dsEdit, dsInsert ] ) ) then    //y no se tienen cambios pendientes
        Close;
end;

end.

