inherited HisKardex_DevEx: THisKardex_DevEx
  Left = 194
  Top = 201
  Caption = 'Kardex'
  ClientWidth = 697
  ExplicitWidth = 697
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 697
    ExplicitWidth = 697
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 438
      ExplicitWidth = 438
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 432
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 697
    ExplicitWidth = 697
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Kind = skCount
          Position = spFooter
          Column = CB_TIPO
        end>
      object CB_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'CB_FECHA'
      end
      object CB_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'CB_TIPO'
      end
      object CB_COMENTA: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'CB_COMENTA'
      end
      object CB_SALARIO: TcxGridDBColumn
        Caption = 'Salario Diario'
        DataBinding.FieldName = 'CB_SALARIO'
      end
      object CB_MONTO: TcxGridDBColumn
        Caption = 'Monto/D'#237'as'
        DataBinding.FieldName = 'CB_MONTO'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 32
    Top = 88
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
