inherited EmpAntesCur_DevEx: TEmpAntesCur_DevEx
  Left = 741
  Top = 251
  Caption = 'Cursos Anteriores'
  ClientWidth = 586
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 586
    inherited Slider: TSplitter
      Left = 322
    end
    inherited ValorActivo1: TPanel
      Width = 306
      inherited textoValorActivo1: TLabel
        Width = 300
      end
    end
    inherited ValorActivo2: TPanel
      Left = 325
      Width = 261
      inherited textoValorActivo2: TLabel
        Width = 255
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 586
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object AR_FOLIO: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'AR_FOLIO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 75
        Width = 75
      end
      object AR_CURSO: TcxGridDBColumn
        Caption = 'Curso'
        DataBinding.FieldName = 'AR_CURSO'
        MinWidth = 75
        Width = 240
      end
      object AR_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'AR_FECHA'
        MinWidth = 75
        Width = 75
      end
      object AR_LUGAR: TcxGridDBColumn
        Caption = 'Lugar'
        DataBinding.FieldName = 'AR_LUGAR'
        MinWidth = 75
        Width = 226
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
