inherited EmpOtros: TEmpOtros
  Left = 522
  Top = 356
  Caption = 'Otros Datos del Empleado'
  ClientHeight = 513
  ClientWidth = 664
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 664
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 338
      inherited textoValorActivo2: TLabel
        Width = 332
      end
    end
  end
  object pcOtros: TPageControl [1]
    Left = 0
    Top = 19
    Width = 664
    Height = 494
    ActivePage = tsTimbrado
    Align = alClient
    MultiLine = True
    TabOrder = 1
    OnChange = pcOtrosChange
    object tsInfonavit: TTabSheet
      Caption = 'Infonavit'
      object sbInfonavit: TScrollBox
        Left = 0
        Top = 0
        Width = 656
        Height = 448
        Align = alClient
        BevelInner = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        object LblDescuento: TLabel
          Left = 226
          Top = 80
          Width = 15
          Height = 13
          Alignment = taRightJustify
          Caption = '     '
        end
        object Label3: TLabel
          Left = 22
          Top = 12
          Width = 98
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicio de Descuento:'
        end
        object CB_INF_INI: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_INF_INI'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_INF_INI'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object CB_INF_ANTLbl: TLabel
          Left = 3
          Top = 35
          Width = 117
          Height = 13
          Caption = 'Otorgamiento de Cr'#233'dito:'
        end
        object CB_INF_ANT: TZetaDBTextBox
          Left = 123
          Top = 33
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_INF_ANT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_INF_ANT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LCredito: TLabel
          Left = 59
          Top = 58
          Width = 61
          Height = 13
          Alignment = taRightJustify
          Caption = '# de Cr'#233'dito:'
        end
        object CB_INFCRED: TZetaDBTextBox
          Left = 123
          Top = 56
          Width = 175
          Height = 17
          AutoSize = False
          Caption = 'CB_INFCRED'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_INFCRED'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label1: TLabel
          Left = 41
          Top = 77
          Width = 79
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo Descuento:'
        end
        object ZDES_TP_PRE: TZetaTextBox
          Left = 122
          Top = 75
          Width = 100
          Height = 17
          AutoSize = False
          Caption = 'ZDES_TP_PRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object LDescuento: TLabel
          Left = 38
          Top = 96
          Width = 82
          Height = 13
          Alignment = taRightJustify
          Caption = 'Valor Descuento:'
        end
        object CB_INFTASA: TZetaDBTextBox
          Left = 122
          Top = 94
          Width = 100
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_INFTASA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_INFTASA'
          DataSource = DataSource
          FormatFloat = '%14.4n'
          FormatCurrency = '%m'
        end
        object Label4: TLabel
          Left = 187
          Top = 115
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa Anterior:'
          Visible = False
        end
        object CB_INF_OLD: TZetaDBTextBox
          Left = 255
          Top = 113
          Width = 39
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_INF_OLD'
          ShowAccelChar = False
          Visible = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_INF_OLD'
          DataSource = DataSource
          FormatFloat = '%14.4n'
          FormatCurrency = '%m'
        end
        object CB_INFDISM: TDBCheckBox
          Left = 21
          Top = 112
          Width = 114
          Height = 17
          Alignment = taLeftJustify
          BiDiMode = bdLeftToRight
          Caption = 'Disminuci'#243'n Tasa %:'
          DataField = 'CB_INFDISM'
          DataSource = DataSource
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_INFACT: TDBCheckBox
          Left = 50
          Top = 129
          Width = 85
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Cr'#233'dito Activo:'
          DataField = 'CB_INFACT'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_INFMANT: TDBCheckBox
          Left = 255
          Top = 129
          Width = 39
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Mantenimiento:'
          DataField = 'CB_INFMANT'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 2
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          Visible = False
        end
      end
    end
    object tsFonacot: TTabSheet
      Caption = 'Fonacot'
      ImageIndex = 3
      object sbFonacot: TScrollBox
        Left = 0
        Top = 0
        Width = 656
        Height = 448
        Align = alClient
        BevelInner = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        object CB_FONACOT: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 125
          Height = 17
          AutoSize = False
          Caption = 'CB_FONACOT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_FONACOT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label9: TLabel
          Left = 24
          Top = 12
          Width = 97
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero de Fonacot:'
        end
      end
    end
    object tsPPrimerEmpleo: TTabSheet
      Caption = 'Programa de Primer Empleo'
      ImageIndex = 4
      object sbPPrimerEmpleo: TScrollBox
        Left = 0
        Top = 0
        Width = 656
        Height = 448
        Align = alClient
        BevelInner = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        object CB_EMPLEO: TDBCheckBox
          Left = 29
          Top = 10
          Width = 107
          Height = 17
          TabStop = False
          Alignment = taLeftJustify
          Caption = 'Aplica al programa: '
          DataField = 'CB_EMPLEO'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
    end
    object tsAsistencia: TTabSheet
      Caption = 'Asistencia'
      ImageIndex = 1
      object sbAsistencia: TScrollBox
        Left = 0
        Top = 0
        Width = 656
        Height = 448
        Align = alClient
        BevelInner = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        object LTipoCreden: TLabel
          Left = 37
          Top = 12
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Letra Credencial:'
        end
        object CB_CREDENC: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 29
          Height = 17
          AutoSize = False
          Caption = 'CB_CREDENC'
          ShowAccelChar = False
          Brush.Color = clSilver
          Border = False
          DataField = 'CB_CREDENC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label2: TLabel
          Left = 47
          Top = 32
          Width = 70
          Height = 13
          Alignment = taRightJustify
          Caption = 'Checa Tarjeta:'
        end
        object Label5: TLabel
          Left = 41
          Top = 52
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero Tarjeta:'
        end
        object CB_ID_NUM: TZetaDBTextBox
          Left = 123
          Top = 50
          Width = 190
          Height = 17
          AutoSize = False
          Caption = 'CB_ID_NUM'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_ID_NUM'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object lblNumeroBiometrico: TLabel
          Left = 25
          Top = 71
          Width = 92
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero Biom'#233'trico:'
          Enabled = False
          Visible = False
        end
        object CB_ID_BIO: TZetaDBTextBox
          Left = 123
          Top = 69
          Width = 190
          Height = 17
          AutoSize = False
          Caption = 'CB_ID_BIO'
          Enabled = False
          ShowAccelChar = False
          Visible = False
          Brush.Color = clBtnFace
          Border = True
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object lblCB_GP_COD: TLabel
          Left = 26
          Top = 90
          Width = 91
          Height = 13
          Alignment = taRightJustify
          Caption = 'Grupo Dispositivos:'
          Enabled = False
          Visible = False
        end
        object CB_GP_COD: TZetaDBTextBox
          Left = 123
          Top = 88
          Width = 190
          Height = 17
          AutoSize = False
          Caption = 'CB_GP_COD'
          Enabled = False
          ShowAccelChar = False
          Visible = False
          Brush.Color = clBtnFace
          Border = True
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object CB_CHECA: TDBCheckBox
          Left = 123
          Top = 30
          Width = 14
          Height = 17
          DataField = 'CB_CHECA'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object chkTieneHuella: TDBCheckBox
          Left = 57
          Top = 107
          Width = 79
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Tiene huella:'
          ReadOnly = True
          TabOrder = 1
          ValueChecked = 'True'
          ValueUnchecked = 'False'
          Visible = False
        end
      end
    end
    object tsEvaluacion: TTabSheet
      Caption = 'Evaluacion'
      ImageIndex = 5
      object sbEvaluacion: TScrollBox
        Left = 0
        Top = 0
        Width = 656
        Height = 448
        Align = alClient
        BevelInner = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        object CB_EVALUA: TZetaDBTextBox
          Left = 123
          Top = 28
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_EVALUA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_EVALUA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LProxima: TLabel
          Left = 79
          Top = 49
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Pr'#243'xima:'
        end
        object LFechaEva: TLabel
          Left = 86
          Top = 12
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object LEvalua: TLabel
          Left = 68
          Top = 30
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Resultado:'
        end
        object CB_NEXT_EV: TZetaDBTextBox
          Left = 123
          Top = 47
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_NEXT_EV'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_NEXT_EV'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object CB_LAST_EV: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_LAST_EV'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_LAST_EV'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
    end
    object tsCuentas: TTabSheet
      Caption = 'Cuentas'
      ImageIndex = 2
      object sbCuentas: TScrollBox
        Left = 0
        Top = 0
        Width = 656
        Height = 448
        Align = alClient
        BevelInner = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        object GroupBox1: TGroupBox
          Left = 24
          Top = 13
          Width = 455
          Height = 60
          TabOrder = 0
          object LBanca: TLabel
            Left = 12
            Top = 36
            Width = 90
            Height = 13
            Alignment = taRightJustify
            Caption = 'Banca Electr'#243'nica:'
          end
          object CB_BAN_ELE: TZetaDBTextBox
            Left = 104
            Top = 34
            Width = 190
            Height = 17
            AutoSize = False
            Caption = 'CB_BAN_ELE'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'CB_BAN_ELE'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label10: TLabel
            Left = 68
            Top = 15
            Width = 34
            Height = 13
            Alignment = taRightJustify
            Caption = 'Banco:'
          end
          object CB_BANCO: TZetaDBTextBox
            Left = 104
            Top = 13
            Width = 84
            Height = 17
            AutoSize = False
            Caption = 'CB_BANCO'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'CB_BANCO'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object TB_ELEM_BAN: TZetaDBTextBox
            Left = 192
            Top = 13
            Width = 255
            Height = 17
            AutoSize = False
            Caption = 'TB_ELEM_BAN'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'TB_ELEM_BAN'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
        end
        object GroupBox2: TGroupBox
          Left = 24
          Top = 73
          Width = 455
          Height = 104
          TabOrder = 1
          object CB_NETO: TZetaDBTextBox
            Left = 105
            Top = 76
            Width = 84
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_NETO'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'CB_NETO'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label8: TLabel
            Left = 23
            Top = 14
            Width = 80
            Height = 13
            Alignment = taRightJustify
            Caption = 'Tarjeta Gasolina:'
          end
          object CB_CTA_GAS: TZetaDBTextBox
            Left = 105
            Top = 12
            Width = 190
            Height = 17
            AutoSize = False
            Caption = 'CB_CTA_GAS'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'CB_CTA_GAS'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label7: TLabel
            Left = 16
            Top = 35
            Width = 87
            Height = 13
            Alignment = taRightJustify
            Caption = 'Tarjeta Despensa:'
          end
          object CB_CTA_VAL: TZetaDBTextBox
            Left = 105
            Top = 33
            Width = 190
            Height = 17
            AutoSize = False
            Caption = 'CB_CTA_VAL'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'CB_CTA_VAL'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label6: TLabel
            Left = 48
            Top = 57
            Width = 55
            Height = 13
            Alignment = taRightJustify
            Caption = 'Subcuenta:'
          end
          object CB_SUB_CTA: TZetaDBTextBox
            Left = 105
            Top = 55
            Width = 190
            Height = 17
            AutoSize = False
            Caption = 'CB_SUB_CTA'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'CB_SUB_CTA'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object lbNeto: TLabel
            Left = 8
            Top = 78
            Width = 95
            Height = 13
            Alignment = taRightJustify
            Caption = 'Neto (Piramidaci'#243'n):'
          end
        end
      end
    end
    object tsConfidencialidad: TTabSheet
      Caption = 'Confidencialidad'
      ImageIndex = 9
      object sbConfidencialidad: TScrollBox
        Left = 0
        Top = 0
        Width = 656
        Height = 448
        Align = alClient
        BevelInner = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        object CB_NIVEL0: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 84
          Height = 17
          AutoSize = False
          Caption = 'CB_NIVEL0'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_NIVEL0'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LblNivel0: TLabel
          Left = 40
          Top = 12
          Width = 81
          Height = 13
          Alignment = taRightJustify
          Caption = 'Confidencialidad:'
        end
      end
    end
    object tsMisDatos: TTabSheet
      Caption = 'Mis Datos'
      ImageIndex = 6
      object sbMisDatos: TScrollBox
        Left = 0
        Top = 0
        Width = 656
        Height = 448
        Align = alClient
        BevelInner = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        object btnBorrarNIP: TBitBtn
          Left = 10
          Top = 10
          Width = 97
          Height = 49
          Caption = 'Borrar NIP'
          TabOrder = 0
          OnClick = btnBorrarNIPClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          Layout = blGlyphTop
          NumGlyphs = 2
        end
      end
    end
    object tsDatosMedicos: TTabSheet
      Caption = 'Datos M'#233'dicos'
      ImageIndex = 8
      object sbDatosMedicos: TScrollBox
        Left = 0
        Top = 0
        Width = 656
        Height = 448
        Align = alClient
        BevelInner = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        object lblTSangre: TLabel
          Left = 43
          Top = 12
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Sangre:'
        end
        object CB_TSANGRE: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 245
          Height = 17
          AutoSize = False
          Caption = 'CB_TSANGRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_TSANGRE'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object lblAlerPadec: TLabel
          Left = 5
          Top = 33
          Width = 116
          Height = 13
          Alignment = taRightJustify
          Caption = 'Al'#233'rgico / Padecimiento:'
        end
        object CB_ALERGIA: TZetaDBTextBox
          Left = 123
          Top = 31
          Width = 245
          Height = 17
          AutoSize = False
          Caption = 'CB_ALERGIA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_ALERGIA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
    end
    object tsBrigada: TTabSheet
      Caption = 'Brigadas de Protecci'#243'n Civil'
      ImageIndex = 7
      object sbBrigada: TScrollBox
        Left = 0
        Top = 0
        Width = 656
        Height = 448
        Align = alClient
        BevelInner = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        object lblTipoBrigada: TLabel
          Left = 100
          Top = 29
          Width = 24
          Height = 13
          Caption = 'Tipo:'
        end
        object CB_BRG_TIP: TZetaTextBox
          Left = 128
          Top = 27
          Width = 223
          Height = 17
          AutoSize = False
          Caption = 'CB_BRG_TIP'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object lblNoPisoBrigada: TLabel
          Left = 66
          Top = 85
          Width = 58
          Height = 13
          Caption = 'No. de Piso:'
        end
        object CB_BRG_NOP: TZetaDBTextBox
          Left = 128
          Top = 83
          Width = 223
          Height = 18
          AutoSize = False
          Caption = 'CB_BRG_NOP'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_BRG_NOP'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object lblRol: TLabel
          Left = 105
          Top = 49
          Width = 19
          Height = 13
          Caption = 'Rol:'
        end
        object lblConocimiento: TLabel
          Left = 57
          Top = 67
          Width = 67
          Height = 13
          Caption = 'Conocimiento:'
        end
        object CB_BRG_ACT: TDBCheckBox
          Left = 3
          Top = 10
          Width = 138
          Height = 17
          Alignment = taLeftJustify
          BiDiMode = bdLeftToRight
          Caption = 'Pertenece a una brigada:'
          DataField = 'CB_BRG_ACT'
          DataSource = DataSource
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_BRG_CON: TDBCheckBox
          Left = 128
          Top = 65
          Width = 66
          Height = 17
          BiDiMode = bdLeftToRight
          Caption = 'Te'#243'rico'
          DataField = 'CB_BRG_CON'
          DataSource = DataSource
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 3
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_BRG_PRA: TDBCheckBox
          Left = 208
          Top = 65
          Width = 138
          Height = 17
          BiDiMode = bdLeftToRight
          Caption = 'Pr'#225'ctico'
          DataField = 'CB_BRG_PRA'
          DataSource = DataSource
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 4
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_BRG_ROL: TDBCheckBox
          Left = 128
          Top = 47
          Width = 74
          Height = 17
          BiDiMode = bdLeftToRight
          Caption = 'Brigadista'
          DataField = 'CB_BRG_ROL'
          DataSource = DataSource
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_BRG_JEF: TDBCheckBox
          Left = 208
          Top = 47
          Width = 138
          Height = 17
          BiDiMode = bdLeftToRight
          Caption = 'Jefe de Brigada'
          DataField = 'CB_BRG_JEF'
          DataSource = DataSource
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 2
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
    end
    object tsTimbrado: TTabSheet
      Caption = 'Timbrado de N'#243'mina'
      ImageIndex = 10
      object sbTimbrado: TScrollBox
        Left = 0
        Top = 0
        Width = 656
        Height = 448
        Align = alClient
        BevelInner = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        object ZetaDBTextBox1: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 278
          Height = 17
          AutoSize = False
          Caption = 'ZetaDBTextBox1'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_REGIMEN'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label11: TLabel
          Left = 52
          Top = 12
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Caption = 'R'#233'gimen SAT:'
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 620
  end
end
