unit FGridIncapaci_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ExtCtrls, StdCtrls, ZetaKeyCombo, Mask, ZetaFecha, ZetaSmartLists, ZetaCommonTools, ZetaClientDataset,
  ZBaseGridEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TGridIncapaci_DevEx = class(TBaseGridEdicion_DevEx)
    ZFecha: TZetaDBFecha;
    zComboMot: TZetaDBKeyCombo;
    zComboFin: TZetaDBKeyCombo;
    ZFechaSUA: TZetaDBFecha;
    btnBuscaPeriodo: TcxButton;
    ZFechaRH: TZetaDBFecha;
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnBuscaPeriodoClick(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEmpleado;
    procedure BuscaTipo;  
    procedure BuscaPeriodo;
    procedure SetFiltroPeriodo;
  protected
    { Protected declarations }
    procedure Buscar; override;
    procedure Connect; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;

var
  GridIncapaci_DevEx: TGridIncapaci_DevEx;

implementation

uses dRecursos, dTablas, DCliente,
     ZetaCommonClasses,
     ZetaBuscaEmpleado_DevEx,
     ZetaCommonLists,
     ZetaBuscaPeriodo_DevEx, dCatalogos;

{$R *.DFM}

procedure TGridIncapaci_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H11514_Registro_de_incapacidades;
end;

procedure TGridIncapaci_DevEx.Connect;
begin
     dmTablas.cdsIncidencias.Conectar;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmRecursos do
     begin
          cdsGridIncapaci.Refrescar;
          DataSource.DataSet:= cdsGridIncapaci;
     end;
end;

procedure TGridIncapaci_DevEx.Buscar;
begin
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado
          else if ( SelectedField.FieldName = 'IN_TIPO' ) then
             BuscaTipo
          else if ( SelectedField.FieldName = 'IN_NOMNUME' ) then
             BuscaPeriodo;
{          else       // Si no se cumple ninguno de estos casos no se hace nada
 }
     end;
end;

procedure TGridIncapaci_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmRecursos.cdsGridIncapaci do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_CODIGO' ).AsString := sKey;
          end;
     end;
end;

procedure TGridIncapaci_DevEx.BuscaTipo;
var
   sKey, sDescription: String;
begin
     if dmTablas.cdsIncidencias.Search_DevEx( dmRecursos.GetFiltroIncapacidad, sKey, sDescription ) then
     begin
          with dmRecursos.cdsGridIncapaci do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'IN_TIPO' ).AsString := sKey;
          end;
     end;
end;

procedure TGridIncapaci_DevEx.BuscaPeriodo;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaPeriodo_DevEx.BuscaPeriodoDialogo(dmCatalogos.cdsPeriodo, '', sKey, sDescription ) then
     begin
          with dmRecursos.cdsGridIncapaci do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'IN_NOMNUME' ).AsString := sKey;
          end;
     end;
end;

procedure TGridIncapaci_DevEx.SetFiltroPeriodo;
var
   iYear : Integer;
   TipoPer : eTipoPeriodo;
begin
     with DataSource.DataSet do
     begin
          iYear := FieldByName( 'IN_NOMYEAR' ).AsInteger;
          dmCliente.SetEmpleadoNumero(FieldByName('CB_CODIGO').AsInteger); 
          TipoPer := eTipoPeriodo ( dmCliente.cdsEmpleado.FieldByName('CB_NOMINA').AsInteger );
     end;
     with dmCatalogos do
     begin
          with cdsPeriodo do
          begin
               if ( IsEmpty ) or
                  ( ( iYear <> FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPer ) <> FieldByName( 'PE_TIPO' ).AsInteger ) ) then
               begin
                    GetDatosPeriodo( iYear, TipoPer );
               end;
          end;
     end;
end;

procedure TGridIncapaci_DevEx.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'CB_CODIGO' )then
               begin
                    SetFiltroPeriodo;
                    with dmRecursos.cdsGridIncapaci do
                    begin
                         if State <> dsBrowse then
                            FieldByName('IN_NOMTIPO').AsInteger := dmCliente.cdsEmpleado.FieldByName('CB_NOMINA').AsInteger;
                    end;
               end
               else if ( SelectedField.FieldName = 'IN_FEC_INI' ) and ( zFecha.Visible ) then
                  zFecha.Visible:= FALSE
               else if ( SelectedField.FieldName = 'IN_MOTIVO' ) and ( zComboMot.Visible ) then
                   zComboMot.Visible := FALSE
               else if ( SelectedField.FieldName = 'IN_FIN' ) and ( zComboFin.Visible ) then
                   zComboFin.Visible := FALSE
               else if ( SelectedField.FieldName = 'IN_SUA_INI' ) and ( zFechaSUA.Visible ) then
                   zFechaSUA.Visible := FALSE
               else if ( SelectedField.FieldName = 'IN_FEC_RH' ) and ( zFechaRH.Visible ) then
                   zFechaRH.Visible := FALSE
               else if ( SelectedField.FieldName = 'IN_NOMNUME' ) and ( btnBuscaPeriodo.Visible ) then
                    btnBuscaPeriodo.Visible := FALSE;
          end;
     end;
end;

procedure TGridIncapaci_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer;
          Column: TColumn; State: TGridDrawState);

   procedure AjustaControlEnCelda( oControl: TControl );
   begin
        with oControl do
        begin
             Left := Rect.Left + ZetaDBGrid.Left;
             Top := Rect.Top + ZetaDBGrid.Top;
             Width := Column.Width + 2;
             Visible := True;
        end;
   end;

begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'IN_FEC_INI' ) then
             AjustaControlEnCelda( zFecha )
          else if ( Column.FieldName = 'IN_MOTIVO' ) then
             AjustaControlEnCelda( zComboMot )
          else if ( Column.FieldName = 'IN_FIN' ) then
             AjustaControlEnCelda( zComboFin )
          else if ( Column.FieldName = 'IN_SUA_INI' ) then
             AjustaControlEnCelda( zFechaSUA )
          else if ( Column.FieldName = 'IN_FEC_RH' ) then
             AjustaControlEnCelda( zFechaRH )
          else if ( Column.FieldName = 'IN_NOMNUME' ) then
          begin
               with btnBuscaPeriodo do
               begin
                    Left := Rect.Left + ZetaDBGrid.Left + 2;
                    Top := Rect.Top + ZetaDBGrid.Top + 2;
                    Visible := True;
               end;
          end;
     end;
end;

procedure TGridIncapaci_DevEx.KeyPress(var Key: Char);

   function EsControlGrid: Boolean;
   begin
        Result := ( self.ActiveControl = zFecha ) or ( self.ActiveControl = zComboMot ) or ( self.ActiveControl = zComboFin ) or ( self.ActiveControl = zFechaSUA ) or ( self.ActiveControl = zFechaRH );
   end;

begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'IN_FEC_INI' ) then
               begin
                    zFecha.SetFocus;
                    SendMessage( zFecha.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'IN_MOTIVO' ) then
               begin
                    if ( zComboMot.Enabled ) then
                    begin
                         zComboMot.SetFocus;
                         SendMessage( zComboMot.Handle, WM_Char, Word( Key ), 0 );
                    end;
                    Key := #0;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'IN_FIN' ) then
               begin
                    if ( zComboFin.Enabled ) then
                    begin
                         zComboFin.SetFocus;
                         SendMessage( zComboFin.Handle, WM_Char, Word( Key ), 0 );
                    end;
                    Key := #0;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'IN_NUMERO' ) then
               begin
                    if (  ZetaCommonTools.EsMinuscula( Key ) )then
                       Key := Chr( Ord( Key ) - 32 );
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'IN_SUA_INI' ) then
               begin
                    zFechaSUA.SetFocus;
                    SendMessage( zFechaSUA.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'IN_FEC_RH' ) then
               begin
                    zFechaRH.SetFocus;
                    SendMessage( zFechaRH.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end;
          end;
     end
     else if EsControlGrid and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
     begin
          Key := #0;
          with ZetaDBGrid do
          begin
               SetFocus;
               ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
          end;
     end;
end;

procedure TGridIncapaci_DevEx.btnBuscaPeriodoClick(Sender: TObject);
begin
  inherited;
  BuscaPeriodo;
end;

end.
