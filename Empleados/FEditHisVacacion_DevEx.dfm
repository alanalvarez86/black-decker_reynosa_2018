inherited EditHisVacacion_DevEx: TEditHisVacacion_DevEx
  Left = 912
  Top = 194
  Caption = 'Vacaciones'
  ClientHeight = 466
  ClientWidth = 414
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 430
    Width = 414
    TabOrder = 2
    DesignSize = (
      414
      36)
    inherited OK_DevEx: TcxButton
      Left = 242
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 323
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 414
    TabOrder = 4
    inherited ValorActivo2: TPanel
      Width = 88
      inherited textoValorActivo2: TLabel
        Width = 82
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 414
    Height = 119
    Align = alTop
    TabOrder = 0
    object LVA_VAC_DEL: TLabel
      Left = 48
      Top = 13
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha de Inicio:'
    end
    object LblGozados: TLabel
      Left = 53
      Top = 63
      Width = 71
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'as Gozados:'
    end
    object LVA_VAC_AL: TLabel
      Left = 81
      Top = 38
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Regresa:'
    end
    object VA_FEC_INI: TZetaDBFecha
      Left = 128
      Top = 11
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '16/dic/97'
      Valor = 35780.000000000000000000
      DataField = 'VA_FEC_INI'
      DataSource = DataSource
    end
    object VA_GOZO: TZetaDBNumero
      Left = 128
      Top = 61
      Width = 60
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 3
      Text = '0.00'
      DataField = 'VA_GOZO'
      DataSource = DataSource
    end
    object VA_FEC_FIN: TZetaDBFecha
      Left = 128
      Top = 36
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '16/dic/97'
      Valor = 35780.000000000000000000
      DataField = 'VA_FEC_FIN'
      DataSource = DataSource
    end
    object gbSaldos: TGroupBox
      Left = 280
      Top = 6
      Width = 125
      Height = 102
      TabOrder = 5
      object Label5: TLabel
        Left = 21
        Top = 45
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Gozo:'
      end
      object ZSaldoGozo: TZetaTextBox
        Left = 53
        Top = 43
        Width = 60
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ZSaldoGozo'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label10: TLabel
        Left = 21
        Top = 64
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pago:'
      end
      object ZSaldoPago: TZetaTextBox
        Left = 53
        Top = 62
        Width = 60
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ZSaldoPago'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object ZSaldoPrima: TZetaTextBox
        Left = 53
        Top = 81
        Width = 60
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ZSaldoPrima'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label11: TLabel
        Left = 20
        Top = 83
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima:'
      end
      object RBAniversario: TRadioButton
        Left = 3
        Top = 7
        Width = 113
        Height = 17
        Caption = 'Saldo al Aniversario'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = RBSaldosClick
      end
      object RBFecha: TRadioButton
        Tag = 1
        Left = 3
        Top = 24
        Width = 113
        Height = 17
        Caption = 'Saldo a la Fecha'
        TabOrder = 1
        OnClick = RBSaldosClick
      end
    end
    object BtnRecalculo_DevEx: TcxButton
      Left = 190
      Top = 61
      Width = 21
      Height = 21
      Hint = 'Forzar Rec'#225'lculo de D'#237'as Gozados'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      TabStop = False
      OnClick = BtnRecalculo_DevExClick
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A4050000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFD0CCD0FFFDFDFDFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCD9DDFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF948B95FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFA39BA3FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF9A919AFFFFFFFFFFB8B2B9FFBEB8BFFFFAF9FAFF948B95FFF6F5
        F6FFCCC8CCFFAFA9B0FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFFFFFFF6F5F6FFF6F5
        F6FFFFFFFFFFEFEDEFFFFFFFFFFFFAF9FAFFF2F1F2FFFFFFFFFFA8A1A9FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A91
        9AFFFFFFFFFFBEB8BFFFCAC6CBFFF8F7F8FFA199A2FFF6F5F6FFD7D4D7FFBEB8
        BFFFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF9A919AFFFFFFFFFFE9E7E9FFEFEDEFFFFFFFFFFFDBD7
        DBFFFDFDFDFFEFEDEFFFE4E1E4FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFFFFFFD3D0
        D4FFD7D4D7FFFBFBFBFFBEB8BFFFFAF9FAFFE0DDE0FFCECACEFFFFFFFFFFA8A1
        A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF9A919AFFFFFFFFFFD9D5D9FFDCD9DDFFFBFBFBFFC2BCC2FFFBFBFBFFE6E3
        E6FFD3D0D4FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFFFFFFFAF9FAFFF0EFF1FFF0EF
        F1FFF0EFF1FFF0EFF1FFF0EFF1FFFAF9FAFFFFFFFFFFA8A1A9FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFF
        FFFFA49DA5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFFFFF
        FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF9A919AFFFFFFFFFF9B939CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF9A919AFFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFFDFDFDFFDCD9DDFFC5C0
        C6FFC5C0C6FFC5C0C6FFC5C0C6FFC5C0C6FFD5D2D6FFFFFFFFFF9F97A0FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFC3BEC4FFF8F7F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF9
        FAFFCECACEFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      OptionsImage.Margin = 1
    end
    object BtnRegreso_DevEx: TcxButton
      Left = 245
      Top = 37
      Width = 21
      Height = 21
      Hint = 'Forzar Rec'#225'lculo de Fecha de Regreso'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TabStop = False
      OnClick = BtnRegreso_DevExClick
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A4050000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFD0CCD0FFFDFDFDFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCD9DDFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF948B95FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFA39BA3FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF9A919AFFFFFFFFFFB8B2B9FFBEB8BFFFFAF9FAFF948B95FFF6F5
        F6FFCCC8CCFFAFA9B0FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFFFFFFF6F5F6FFF6F5
        F6FFFFFFFFFFEFEDEFFFFFFFFFFFFAF9FAFFF2F1F2FFFFFFFFFFA8A1A9FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A91
        9AFFFFFFFFFFBEB8BFFFCAC6CBFFF8F7F8FFA199A2FFF6F5F6FFD7D4D7FFBEB8
        BFFFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF9A919AFFFFFFFFFFE9E7E9FFEFEDEFFFFFFFFFFFDBD7
        DBFFFDFDFDFFEFEDEFFFE4E1E4FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFFFFFFD3D0
        D4FFD7D4D7FFFBFBFBFFBEB8BFFFFAF9FAFFE0DDE0FFCECACEFFFFFFFFFFA8A1
        A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF9A919AFFFFFFFFFFD9D5D9FFDCD9DDFFFBFBFBFFC2BCC2FFFBFBFBFFE6E3
        E6FFD3D0D4FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFFFFFFFAF9FAFFF0EFF1FFF0EF
        F1FFF0EFF1FFF0EFF1FFF0EFF1FFFAF9FAFFFFFFFFFFA8A1A9FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFF
        FFFFA49DA5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFFFFF
        FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF9A919AFFFFFFFFFF9B939CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF9A919AFFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFFDFDFDFFDCD9DDFFC5C0
        C6FFC5C0C6FFC5C0C6FFC5C0C6FFC5C0C6FFD5D2D6FFFFFFFFFF9F97A0FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFC3BEC4FFF8F7F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF9
        FAFFCECACEFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      OptionsImage.Margin = 1
    end
  end
  object cxPageControl: TcxPageControl [4]
    Left = 0
    Top = 169
    Width = 414
    Height = 261
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = TabMovimiento_DevEx
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 259
    ClientRectLeft = 2
    ClientRectRight = 412
    ClientRectTop = 27
    object TabMovimiento_DevEx: TcxTabSheet
      Caption = 'D'#237'as a Pagar'
      ImageIndex = 0
      object LblPago: TLabel
        Left = 53
        Top = 10
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as a Pagar:'
      end
      object L_VA_P_PRIMA: TLabel
        Left = 10
        Top = 34
        Width = 109
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as prima vacacional:'
      end
      object LblPeriodo: TLabel
        Left = 27
        Top = 58
        Width = 92
        Height = 13
        Caption = 'Per'#237'odo Trabajado:'
      end
      object LblObservaciones: TLabel
        Left = 45
        Top = 80
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Observaciones:'
      end
      object VA_PAGO: TZetaDBNumero
        Left = 124
        Top = 8
        Width = 65
        Height = 21
        Mascara = mnDiasFraccion
        TabOrder = 0
        Text = '0.00'
        DataField = 'VA_PAGO'
        DataSource = DataSource
      end
      object VA_P_PRIMA: TZetaDBNumero
        Left = 124
        Top = 32
        Width = 65
        Height = 21
        Mascara = mnDiasFraccion
        TabOrder = 1
        Text = '0.00'
        DataField = 'VA_P_PRIMA'
        DataSource = DataSource
      end
      object VA_PERIODO: TDBEdit
        Tag = 99
        Left = 124
        Top = 56
        Width = 269
        Height = 21
        DataField = 'VA_PERIODO'
        DataSource = DataSource
        MaxLength = 40
        TabOrder = 2
      end
      object VA_COMENTA: TDBMemo
        Left = 124
        Top = 80
        Width = 269
        Height = 52
        DataField = 'VA_COMENTA'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 3
      end
      object GBNominaPago: TGroupBox
        Left = 8
        Top = 137
        Width = 385
        Height = 90
        Caption = 'N'#243'mina a Pagar '
        TabOrder = 4
        object Label7: TLabel
          Left = 88
          Top = 40
          Width = 24
          Height = 13
          Caption = 'Tipo:'
        end
        object Label8: TLabel
          Left = 72
          Top = 64
          Width = 40
          Height = 13
          Caption = 'N'#250'mero:'
        end
        object Label9: TLabel
          Left = 90
          Top = 16
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
        end
        object VA_NOMTIPO: TZetaDBKeyCombo
          Left = 117
          Top = 38
          Width = 190
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfTipoPeriodo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
          DataField = 'VA_NOMTIPO'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object VA_NOMNUME: TZetaDBKeyLookup_DevEx
          Left = 117
          Top = 62
          Width = 265
          Height = 21
          LookupDataset = dmCatalogos.cdsPeriodo
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
          OnValidKey = VA_NOMNUMEValidKey
          DataField = 'VA_NOMNUME'
          DataSource = DataSource
        end
        object VA_NOMYEAR: TZetaDBNumero
          Left = 117
          Top = 14
          Width = 60
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          DataField = 'VA_NOMYEAR'
          DataSource = DataSource
        end
      end
    end
    object TabGenerales_DevEx: TcxTabSheet
      Caption = 'Montos a Pagar'
      ImageIndex = 1
      object Label1: TLabel
        Left = 39
        Top = 18
        Width = 65
        Height = 13
        Caption = 'Salario Diario:'
      end
      object CB_SALARIO: TZetaDBTextBox
        Left = 108
        Top = 16
        Width = 110
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CB_SALARIO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_SALARIO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Monto: TLabel
        Left = 45
        Top = 39
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vacaciones:'
      end
      object VA_MONTO: TZetaDBTextBox
        Left = 108
        Top = 37
        Width = 110
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'VA_MONTO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_MONTO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label21: TLabel
        Left = 42
        Top = 59
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'S'#233'ptimo D'#237'a:'
      end
      object VA_SEVEN: TZetaDBTextBox
        Left = 108
        Top = 57
        Width = 110
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'VA_SEVEN'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_SEVEN'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label22: TLabel
        Left = 19
        Top = 79
        Width = 85
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima Vacacional:'
      end
      object VA_PRIMA: TZetaDBTextBox
        Left = 108
        Top = 77
        Width = 110
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'VA_PRIMA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_PRIMA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object LblOtros: TLabel
        Left = 76
        Top = 103
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Otros:'
      end
      object TotalLbl: TLabel
        Left = 49
        Top = 139
        Width = 55
        Height = 13
        Caption = 'Total Pago:'
      end
      object VA_TOTAL: TZetaDBTextBox
        Left = 108
        Top = 137
        Width = 110
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'VA_TOTAL'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_TOTAL'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object VA_TASA_PR: TZetaDBTextBox
        Left = 228
        Top = 77
        Width = 89
        Height = 17
        AutoSize = False
        Caption = 'VA_TASA_PR'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_TASA_PR'
        DataSource = DataSource
        FormatFloat = '%14.5n'
        FormatCurrency = '%m'
      end
      object Label3: TLabel
        Left = 323
        Top = 79
        Width = 8
        Height = 13
        Caption = '%'
        Visible = False
      end
      object Bevel1: TBevel
        Left = 108
        Top = 128
        Width = 110
        Height = 3
        Shape = bsBottomLine
      end
      object VA_OTROS: TZetaDBNumero
        Left = 108
        Top = 99
        Width = 110
        Height = 21
        Mascara = mnPesos
        TabOrder = 0
        Text = '0.00'
        DataField = 'VA_OTROS'
        DataSource = DataSource
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Generales'
      ImageIndex = 2
      object LTablaIMSS: TLabel
        Left = 26
        Top = 16
        Width = 94
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tabla Prestaciones:'
      end
      object ZetaDBTextBox1: TZetaDBTextBox
        Left = 124
        Top = 14
        Width = 46
        Height = 17
        AutoSize = False
        Caption = 'ZetaDBTextBox1'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_TABLASS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object GCapturoLbl: TLabel
        Left = 77
        Top = 36
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modific'#243':'
      end
      object US_DESCRIP: TZetaDBTextBox
        Left = 124
        Top = 34
        Width = 143
        Height = 17
        AutoSize = False
        Caption = 'US_DESCRIP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object GCapturaLbl: TLabel
        Left = 65
        Top = 56
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modificado:'
      end
      object VA_CAPTURA: TZetaDBTextBox
        Left = 124
        Top = 54
        Width = 80
        Height = 17
        AutoSize = False
        Caption = 'VA_CAPTURA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_CAPTURA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label4: TLabel
        Left = 45
        Top = 76
        Width = 75
        Height = 13
        Caption = 'Registro Global:'
      end
      object Label6: TLabel
        Left = 26
        Top = 96
        Width = 94
        Height = 13
        Alignment = taRightJustify
        Caption = 'A'#241'o de Antig'#252'edad:'
      end
      object ZetaDBTextBox3: TZetaDBTextBox
        Left = 124
        Top = 94
        Width = 45
        Height = 17
        AutoSize = False
        Caption = 'ZetaDBTextBox3'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VA_YEAR'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object VA_GLOBAL: TDBCheckBox
        Left = 124
        Top = 74
        Width = 17
        Height = 17
        DataField = 'VA_GLOBAL'
        DataSource = DataSource
        ReadOnly = True
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
