inherited EmpPercepcion_DevEx: TEmpPercepcion_DevEx
  Left = 360
  Top = 261
  Caption = 'Percepciones'
  ClientHeight = 351
  ClientWidth = 458
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 458
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 132
      inherited textoValorActivo2: TLabel
        Width = 126
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 458
    Height = 146
    Align = alTop
    TabOrder = 2
    object LTablaIMSS: TLabel
      Left = 251
      Top = 77
      Width = 94
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tabla Prestaciones:'
    end
    object CB_TABLASS: TZetaDBTextBox
      Left = 352
      Top = 75
      Width = 30
      Height = 17
      AutoSize = False
      Caption = 'CB_TABLASS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_TABLASS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LZonaGeo: TLabel
      Left = 262
      Top = 57
      Width = 83
      Height = 13
      Alignment = taRightJustify
      Caption = 'Zona Geogr'#225'fica:'
    end
    object CB_ZONA_GE: TZetaDBTextBox
      Left = 352
      Top = 55
      Width = 30
      Height = 17
      AutoSize = False
      Caption = 'CB_ZONA_GE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_ZONA_GE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CB_FAC_INTlbl: TLabel
      Left = 4
      Top = 38
      Width = 104
      Height = 13
      Alignment = taRightJustify
      Caption = 'Factor de Integraci'#243'n:'
    end
    object CB_FAC_INT: TZetaDBTextBox
      Left = 112
      Top = 36
      Width = 72
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CB_FAC_INT'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_FAC_INT'
      DataSource = DataSource
      FormatFloat = '%14.4n'
      FormatCurrency = '%m'
    end
    object Label2: TLabel
      Left = 41
      Top = 58
      Width = 67
      Height = 13
      Alignment = taRightJustify
      Caption = 'Pre-Integrado:'
    end
    object CB_PRE_INT: TZetaDBTextBox
      Left = 112
      Top = 56
      Width = 72
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CB_PRE_INT'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_PRE_INT'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CB_PER_VAR: TZetaDBTextBox
      Left = 112
      Top = 75
      Width = 72
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CB_PER_VAR'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_PER_VAR'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label3: TLabel
      Left = 13
      Top = 77
      Width = 95
      Height = 13
      Alignment = taRightJustify
      Caption = 'Variables Gravadas:'
    end
    object Label4: TLabel
      Left = 35
      Top = 97
      Width = 73
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fijas Gravadas:'
    end
    object CB_TOT_GRA: TZetaDBTextBox
      Left = 112
      Top = 95
      Width = 72
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CB_TOT_GRA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_TOT_GRA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Bevel1: TBevel
      Left = 4
      Top = 114
      Width = 437
      Height = 27
      Shape = bsFrame
    end
    object LSalCotiza: TLabel
      Left = 25
      Top = 122
      Width = 83
      Height = 13
      Alignment = taRightJustify
      Caption = 'Salario Integrado:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object CB_SAL_INT: TZetaDBTextBox
      Left = 112
      Top = 120
      Width = 72
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CB_SAL_INT'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_SAL_INT'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label5: TLabel
      Left = 307
      Top = 122
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cambi'#243':'
    end
    object CB_FEC_INT: TZetaDBTextBox
      Left = 352
      Top = 120
      Width = 80
      Height = 17
      AutoSize = False
      Caption = 'CB_FEC_INT'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_FEC_INT'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Bevel2: TBevel
      Left = 4
      Top = 6
      Width = 437
      Height = 27
      Shape = bsFrame
    end
    object CB_SALARIOlbl: TLabel
      Left = 43
      Top = 12
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Salario Diario:'
    end
    object CB_SALARIO: TZetaDBTextBox
      Left = 112
      Top = 10
      Width = 72
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CB_SALARIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_SALARIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label1: TLabel
      Left = 307
      Top = 12
      Width = 38
      Height = 13
      Caption = 'Cambi'#243':'
    end
    object CB_FEC_REV: TZetaDBTextBox
      Left = 352
      Top = 10
      Width = 80
      Height = 17
      AutoSize = False
      Caption = 'CB_FEC_REV'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_FEC_REV'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label6: TLabel
      Left = 241
      Top = 38
      Width = 104
      Height = 13
      Alignment = taRightJustify
      Caption = 'Salario por Tabulador:'
    end
    object CB_RANGO_SLbl: TLabel
      Left = 273
      Top = 97
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'Rango Salarial:'
    end
    object CB_RANGO_S: TZetaDBTextBox
      Left = 352
      Top = 95
      Width = 49
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CB_RANGO_S'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_RANGO_S'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LblSalSem: TLabel
      Left = 266
      Top = 97
      Width = 79
      Height = 13
      Alignment = taRightJustify
      Caption = 'Salario Semanal:'
    end
    object CB_SAL_SEM: TZetaTextBox
      Left = 352
      Top = 95
      Width = 72
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CB_SAL_SEM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object CB_CLASIFIlbl: TLabel
      Left = 46
      Top = 146
      Width = 62
      Height = 13
      Alignment = taRightJustify
      Caption = 'Clasificaci'#243'n:'
    end
    object CB_CLASIFI: TZetaDBTextBox
      Left = 112
      Top = 144
      Width = 72
      Height = 17
      AutoSize = False
      Caption = 'CB_CLASIFI'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CB_CLASIFI'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ZCLASIFICACION: TZetaTextBox
      Left = 187
      Top = 144
      Width = 245
      Height = 17
      AutoSize = False
      Caption = 'ZCLASIFICACION'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
    end
    object CB_AUTOSAL: TDBCheckBox
      Tag = 99
      Left = 352
      Top = 36
      Width = 33
      Height = 17
      DataField = 'CB_AUTOSAL'
      DataSource = DataSource
      ReadOnly = True
      TabOrder = 0
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 165
    Width = 458
    Height = 186
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = dsFijas
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object KF_FOLIO: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'KF_FOLIO'
        Width = 64
      end
      object KF_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'KF_CODIGO'
        Width = 64
      end
      object DESCRIPCION: TcxGridDBColumn
        Caption = 'Percepciones Fijas'
        DataBinding.FieldName = 'DESCRIPCION'
        Width = 64
      end
      object KF_MONTO: TcxGridDBColumn
        Caption = 'Monto'
        DataBinding.FieldName = 'KF_MONTO'
        Width = 64
      end
      object KF_GRAVADO: TcxGridDBColumn
        Caption = 'Gravado'
        DataBinding.FieldName = 'KF_GRAVADO'
        Width = 64
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 296
    Top = 5
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsFijas: TDataSource
    Left = 362
    Top = 5
  end
end
