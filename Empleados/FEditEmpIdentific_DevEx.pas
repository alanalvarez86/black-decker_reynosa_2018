unit FEditEmpIdentific_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, StdCtrls, Mask, DBCtrls, ZetaDBTextBox, Db,
  ExtCtrls, ZetaFecha, ZetaKeyCombo, ZetaNumero, ZetaKeyLookup_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditEmpIdentific_DevEx = class(TBaseEdicion_DevEx)
    LCB_APE_PAT: TLabel;
    LCB_APE_MAT: TLabel;
    LCB_NOMBRES: TLabel;
    LCB_FEC_NAC: TLabel;
    LRFC: TLabel;
    LCB_SEGSOC: TLabel;
    LCB_CURP: TLabel;
    CB_APE_PAT: TDBEdit;
    CB_APE_MAT: TDBEdit;
    CB_NOMBRES: TDBEdit;
    CB_CURP: TDBEdit;
    lblSexo: TLabel;
    CB_FEC_NAC: TZetaDBFecha;
    CB_RFC: TDBEdit;
    CB_SEGSOC: TDBEdit;
    bDigitoVerificador: TcxButton;
    Label1: TLabel;
    EmpleadoNumero: TZetaDBTextBox;
    CB_SEXO: TZetaKeyCombo;
    ZEdad: TZetaTextBox;
    bbCalculaRFC: TcxButton;
    CB_CANDIDA: TZetaDBNumero;
    LCANDIDA: TLabel;
    lblEntidad: TLabel;
    CB_ENT_NAC: TZetaDBKeyLookup_DevEx;
    bbCalculaCURP: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure bDigitoVerificadorClick(Sender: TObject);
    procedure CB_SEXOChange(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure CB_FEC_NACExit(Sender: TObject);
    procedure bbCalculaRFCClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bbCalculaCURPClick(Sender: TObject);
  private
    procedure IniciaSexo;
    procedure SetControlSeleccion( const lEnabled: Boolean );
    procedure CambiaLeyendas;
  protected
    procedure Connect; override;
    procedure ImprimirForma;override;
  public
  end;

var
  EditEmpIdentific_DevEx: TEditEmpIdentific_DevEx;

implementation

uses dRecursos, dGlobal, dTablas, ZImprimeForma, ZetaCommonLists, ZetaCommonClasses, ZAccesosTress,
     ZetaTipoEntidad, ZGlobalTress,
     FTressShell, ZetaCommonTools, DCliente, ZetaDialogo;

{$R *.DFM}

procedure TEditEmpIdentific_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_EMP_DATOS_IDENTIFICA;
     FirstControl := CB_APE_PAT;
     CB_ENT_NAC.LookupDataSet := dmTablas.cdsEstado;
     HelpContext:= H10111_Datos_identificacion_empleado;
end;

procedure TEditEmpIdentific_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetControlSeleccion( strLleno( Global.GetGlobalString( K_GLOBAL_EMPRESA_SELECCION ) ) );
     CambiaLeyendas;
end;

procedure TEditEmpIdentific_DevEx.Connect;
begin
     dmTablas.cdsEstado.Conectar;
     with dmRecursos do
     begin
          DataSource.DataSet:= cdsDatosEmpleado;
          IniciaSexo;
     end;
end;

procedure TEditEmpIdentific_DevEx.IniciaSexo;
begin
     with dmRecursos.cdsDatosEmpleado, CB_SEXO do
     begin
          if ( FieldByName( 'CB_SEXO' ).AsString = 'F' ) then
             ItemIndex:= Ord( esFemenino )
          else
             ItemIndex:= Ord( esMasculino );
     end;
end;

procedure TEditEmpIdentific_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     TressShell.RefrescaEmpleado;
     Close;
end;

procedure TEditEmpIdentific_DevEx.bDigitoVerificadorClick(Sender: TObject);
begin
     dmRecursos.ModificaDigitoVerificador( Trim( CB_SEGSOC.Text ) );
end;

procedure TEditEmpIdentific_DevEx.bbCalculaRFCClick(Sender: TObject);
begin
     dmRecursos.CalculaRFC;
end;

procedure TEditEmpIdentific_DevEx.bbCalculaCURPClick(Sender: TObject);
begin
     inherited;
     dmRecursos.CalculaCURP;
end;

procedure TEditEmpIdentific_DevEx.CB_SEXOChange(Sender: TObject);
var
   sSexo: String;
begin
     sSexo:= ObtieneElemento( lfSexo, CB_SEXO.ItemIndex );
     with dmRecursos.cdsDatosEmpleado do
          if ( sSexo <> FieldByName( 'CB_SEXO' ).AsString ) then
          begin
               if not Editing then
                  Edit;
               FieldByName( 'CB_SEXO' ).AsString:= sSexo;
          end;
end;

procedure TEditEmpIdentific_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     IniciaSexo;
end;

procedure TEditEmpIdentific_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

procedure TEditEmpIdentific_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
var
   dNacimiento : TDateTime;
   rValor: Double;
begin
     inherited;
     rValor := 0;
     with DataSource.DataSet do
     begin
          if ( State <> dsInactive ) then
          begin
               dNacimiento := FieldByName( 'CB_FEC_NAC' ).AsDateTime;
               if ( dNacimiento > 0 ) then
                  rValor := Trunc( Now - dNacimiento ) + 1;
          end;
          ZEdad.Caption := TiempoDias( rValor, etMeses );
     end;
end;

procedure TEditEmpIdentific_DevEx.CB_FEC_NACExit(Sender: TObject);
var
   rAnnos: Real;
begin
     inherited;
     if ( CB_FEC_NAC.Valor <> 0 ) then
     begin
          rAnnos := DaysToYears( Trunc( dmCliente.FechaDefault - CB_FEC_NAC.Valor ) + 1 );
          if ( rAnnos < 15 ) or ( rAnnos > 100 ) then
          begin
               ZetaDialogo.ZWarning( self.Caption, 'La Edad debe ser de 15 a 100 a�os', 0, mbOK );
          end;
     end;
end;

procedure TEditEmpIdentific_DevEx.SetControlSeleccion( const lEnabled: Boolean );
begin
     CB_CANDIDA.Visible := lEnabled;
     LCANDIDA.Visible := lEnabled;
end;

procedure TEditEmpIdentific_DevEx.CambiaLeyendas;
begin
     Global.Conectar;
     if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
     begin
          lblSexo.Caption := 'G�nero:';
          lRFC.Caption := '# de Ced. de Ident.:';
          LCB_SEGSOC.Caption := '# de Carnet de IHSS:';
          LCB_CURP.Visible := FALSE;
          CB_CURP.Visible := LCB_CURP.Visible;
          bbCalculaCURP.Visible := LCB_CURP.Visible;
          LCANDIDA.Top := 258;
          CB_CANDIDA.Top := 254;
     end
     else
     begin
          lblSexo.Caption := 'Sexo:';
          lRFC.Caption := 'R.F.C.:';
          LCB_SEGSOC.Caption := '# Seguro Social:';
          LCB_CURP.Visible := TRUE;
          CB_CURP.Visible := LCB_CURP.Visible;
          bbCalculaCURP.Visible := LCB_CURP.Visible;
          LCANDIDA.Top := 282;
          CB_CANDIDA.Top := 278;
     end;
end;

end.
