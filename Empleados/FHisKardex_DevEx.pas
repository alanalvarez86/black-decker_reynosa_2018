unit FHisKardex_DevEx;
                                                                                                                                                                                                  
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, Grids, DBGrids,
  ZetaDBGrid, ExtCtrls, StdCtrls, ZetaKeyCombo, ZetaDBTextBox,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  THisKardex_DevEx = class(TBaseGridLectura_DevEx)
    CB_FECHA: TcxGridDBColumn;
    CB_TIPO: TcxGridDBColumn;
    CB_COMENTA: TcxGridDBColumn;
    CB_SALARIO: TcxGridDBColumn;
    CB_MONTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
  private
    procedure ConfigAgrupamiento;
    procedure CambiaOrdenamiento;
  protected
    //function PuedeAgregar( var sMensaje: String ): Boolean; override; - Se Valida en dmRecursos : cdsHisKardexAlAgregar
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure ImprimirForma;override;
  public
  end;

const
     K_COLUMNA_SALARIO = 3;
    
var
  HisKardex_DevEx: THisKardex_DevEx;

implementation

{$R *.DFM}

uses dRecursos, dSistema, dTablas, ZetaDialogo, ZetaCommonLists,ZetaTipoEntidad,
     ZetaCommonTools, ZetaCommonClasses, ZImprimeForma,ZAccesosMgr, ZAccesosTress,
     DCliente, ZetaClientDataSet;

procedure THisKardex_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
      //CambiaOrdenamiento; //Se mueve a OnShow por cambio a gridMode
     HelpContext:= H10131_Kardex;
end;

procedure THisKardex_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     CB_FECHA.Options.Grouping:= FALSE;
     CB_TIPO.Options.Grouping := TRUE;
     CB_COMENTA.Options.Grouping:= FALSE;
     CB_SALARIO.Options.Grouping:= FALSE;
     CB_MONTO.Options.Grouping:= FALSE;
end;

procedure THisKardex_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     //CreaColumaAgrupadaSumatoria( CB_TIPO.Name, skCount );
     inherited;
     //DevEx (by am): Indica bajo que criterio la columna de salario sera visible
     CB_SALARIO.Visible := ZAccesosMgr.CheckDerecho( D_REPORTES_CONFIDENCIALES, K_DERECHO_CONSULTA );
     //DevEx (by am):Configuracion Especial para agrupamiento
     ConfigAgrupamiento;
     //El cambio del ordenamiento se hace aqui, para asegurar que el dataset este abierto.
     CambiaOrdenamiento;
end;

procedure THisKardex_DevEx.Connect;
begin
     with dmRecursos do
     begin
          ConectarKardexLookups;
          cdsHisKardex.Conectar;
          DataSource.DataSet:= cdsHisKardex;
          cdsEditHisKardex.Conectar;
     end;
end;

procedure THisKardex_DevEx.Refresh;
begin
     dmRecursos.cdsHisKardex.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisKardex_DevEx.ZetaDBGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  //Cuando se ordena por fecha, existe un FieldName Secundario que es CB_NIVEL
  if AColumn.Name = 'CB_FECHA' then
      ZetaDBGrid.OrdenarPor( AColumn, TZetaClientDataset(DataSource.DataSet),AColumn.Name+';'+'CB_NIVEL' )
  else
      inherited;
end;

procedure THisKardex_DevEx.Agregar;
begin
     dmRecursos.cdsHisKardex.Agregar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisKardex_DevEx.Modificar;
begin
     with dmRecursos do
     begin
          TabKardex := K_TAB_CONTRATA;
          cdsHisKardex.Modificar;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisKardex_DevEx.Borrar;
begin
     dmRecursos.cdsHisKardex.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisKardex_DevEx.ImprimirForma;
 var sTipo : string;
begin

     with dmRecursos.cdsHisKardex do
     begin
          sTipo := FieldByName('CB_TIPO').AsString;
          if (sTipo = K_T_VACA) OR
             (sTipo = K_T_PERM) OR
             (sTipo = K_T_INCA) then
             ZetaDialogo.ZInformation( Caption, 'Solamente se pueden Imprimir Formas de Movimientos de Kardex',0 )
          else ZImprimeForma.ImprimeUnaForma( enKardex, dmRecursos.cdsHisKardex );
     end;
end;

function THisKardex_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje );
     if Result and zStrToBool( dmRecursos.cdsHisKardex.FieldByName( 'TB_SISTEMA' ).AsString ) then
     begin
          Result := ZAccesosMgr.CheckDerecho( D_EMP_EXP_KARDEX, K_DERECHO_SIST_KARDEX );
          if Result then
          begin
               if( dmRecursos.cdsHisKardex.FieldByName( 'CB_TIPO' ).AsString = K_T_CAMBIO )then
               begin
                    Result := ZAccesosMgr.CheckDerecho( D_REPORTES_CONFIDENCIALES, K_DERECHO_CONSULTA );
                    if ( not Result ) then
                       sMensaje := 'No Tiene Permiso Para Borrar Registros Con Datos Confidenciales';
               end;
          end
          else
              sMensaje:= 'No Tiene Permiso Para Borrar Registros de este Tipo';
     end;
end;

procedure THisKardex_DevEx.CambiaOrdenamiento;
begin
    dmRecursos.OrdenKardex:= TOrdenKardex( Ord( eokDescendente ));
end;

function THisKardex_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     {cv: 18-dic-2001
     Esta validacion se quito para implementar que los movimientos de kardex
     se puedan consultar la forma de edicion sin modificar.
     Esta validacion se hace dentro de FKardexBase


     Result:= inherited PuedeModificar( sMensaje );
     if Result and zStrToBool( dmRecursos.cdsHisKardex.FieldByName( 'TB_SISTEMA' ).AsString ) then
     begin
          Result := ZAccesosMgr.CheckDerecho( D_EMP_EXP_KARDEX, K_DERECHO_SIST_KARDEX );
          if not Result then
             sMensaje:= 'No Tiene Permiso Para Modificar Registros de este Tipo';
     end; }

     { ma: 3-marzo-04
           Se agreg� esta validaci�n para regresar al comportamiento anterior al momento de
           consultar movimientos de sistema }
    if zStrToBool( dmRecursos.cdsHisKardex.FieldByName( 'TB_SISTEMA' ).AsString ) then
     begin
          Result := ( dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION ) or
                    ZAccesosMgr.CheckDerecho( D_EMP_EXP_KARDEX, K_DERECHO_SIST_KARDEX ) or
                    not ZAccesosMgr.CheckDerecho( D_EMP_EXP_KARDEX, K_DERECHO_BANCA );
          if Result then
          begin
               if( dmRecursos.cdsHisKardex.FieldByName( 'CB_TIPO' ).AsString = K_T_CAMBIO )then
               begin
                    Result := ZAccesosMgr.CheckDerecho( D_REPORTES_CONFIDENCIALES, K_DERECHO_CONSULTA );
                    if ( not Result ) then
                       sMensaje := 'No Tiene Permiso Para Modificar Registros Con Datos Confidenciales';
               end;
          end
          else
              sMensaje:= 'No Tiene Permisos Sobre Movimientos De Kardex Del Sistema';
          {if not Result then
             sMensaje:= 'No Tiene Permisos Sobre Movimientos De Kardex Del Sistema';}
     end
     else
         Result := True;
end;

end.
