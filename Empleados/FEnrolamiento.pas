unit FEnrolamiento;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.7                                 ::
  :: Unidad:      FEnrolamiento.pas                          ::
  :: Descripci�n: Ventana de Enrolamiento de Huellas         ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal, OleCtrls, SFEStandardLib_TLB, ExtCtrls, Buttons,
  StdCtrls, ZetaKeyCombo, ZetaCommonLists, ZetaDBTextBox, ZetaDialogo, ZetaCommonClasses, SZCodeBaseX,
  IdBaseComponent, IdCoder, IdCoder3to4, IdCoderMIME, ComCtrls;

type
  TEnrolamiento = class(TZetaDlgModal)
    lblMsg: TStaticText;
    BtnEnrolar: TButton;
    TimerInicio: TTimer;
    Recargar: TSpeedButton;
    ProgresoHuellas: TProgressBar;
    TimerEnrollTimeout: TTimer;

    function WaitForTakeOff : Integer;
    procedure BtnEnrolarClick(Sender: TObject);
    procedure PrintError(error : Integer);
    procedure PrintMensaje(sMensaje: String; lLimpiaPantalla: Boolean);
    function GetError(error : Integer):String; 
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TimerInicioTimer(Sender: TObject);
    procedure RecargarClick(Sender: TObject);
    procedure TimerEnrollTimeoutTimer(Sender: TObject);

  private
    { Private declarations }
    FEmpleado : Integer;
    FInvitador : Integer;
    SFEStandard1 : TSFEStandard;
    function LeeArchivo:Boolean;
    function AdquiereHuellas : Boolean;
    function CaptureFinger() : Integer;

    procedure enableButtons(bEnable : Boolean);
    procedure switchWorking(bWorking : Boolean);

  protected
    { Private declarations }


  public
    { Public declarations }
    property Empleado: Integer read FEmpleado write FEmpleado; // SYNERGY
    property Invitador: Integer read FInvitador write FInvitador; // BIOMETRICO

  end;

var
  Enrolamiento: TEnrolamiento;

implementation

{$R *.dfm}

uses
  DRecursos,
  DCliente,
  dSistema,
  ZetaClientDataSet,
  ConvUtils, DB;

var
  gTemplate         : array[0..1403] of Integer; //351*4 = 1404
  gImageBytes       : array[0..65535] of Byte; // 256 * 256
  addrOfTemplate    : Integer; //Direccion de memoria del template
  gHintStopPos      : Integer;
  addrOfImageBytes  : Integer; //Direccion de memoria de la imagen a mostrar
  gWorking          : Boolean; //Bandera de lector en procesos
  iDedo             : Integer;

const
  FINGER_BITMAP_WIDTH   : Integer = 256; //Ancho de la imagen de la huella
  FINGER_BITMAP_HEIGHT  : Integer = 256; //Alto de la imagen de la huella
  ColorNColor           : Integer = 3;

//--------------------------------------------------------------------------------------------------------

function TEnrolamiento.LeeArchivo : Boolean;
var
   iArchivoId, nRet : Integer;
   F : TSearchRec;

   function GetFileDateTime(FileName: string): TDateTime;
   var intFileAge: LongInt;
   begin
     intFileAge := FileAge(FileName);
     if intFileAge = -1 then
       Result := 0
     else
       Result := FileDateToDateTime(intFileAge)
   end;

begin
     with dmSistema do
     begin
          try
             if FileExists( LocalBioDbPath ) then
                DeleteFile( LocalBioDbPath );
             if not FileExists( LocalBioDbPath ) then //Si no existe lo crea vac�o.
             begin
                iArchivoId := FileCreate( LocalBioDbPath );
                FileClose( iArchivoId );
                //FechaPivote := NullDateTime;
                ReiniciaHuellas;                
             end
             else
             begin
                  FindFirst( LocalBioDbPath, faAnyFile, F );
                  //FechaPivote := FileDateToDatetime( F.Time );
                  FindClose( F );
             end;
             //Inicia con el archivo.
             {$ifdef ANTES}
             nRet := SFEStandard1.Open( LocalBioDbPath, 4, 0);
             {$else}
             nRet := SFEStandard1.Open( LocalBioDbPath, K_TIPO_BIOMETRICO, 0 );
             {$endif}
             if nRet = 0 then
             begin
                  enableButtons(True);
                  PrintMensaje(' Huellas anteriormente encontradas: ' + IntToStr(SFEStandard1.GetEnrollCount()) + ' de todos los empleados.', False);
                  SFEStandard1.DotNET();
                  Result := True;
             end
             else
             begin
                  PrintError( nRet );
                  Result := False;
             end;
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                     switchWorking(False);
                     PrintMensaje('Lecor detenido, error en inicializacion de motor de huellas. Intente de nuevo.' + Error.Message, True);
                     DeleteFile( LocalBioDbPath );
                     Result := False;
                end;
          end;
     end;
end;

procedure TEnrolamiento.enableButtons(bEnable: Boolean);
begin
     BtnEnrolar.Enabled := bEnable;
     Recargar.Enabled := bEnable;
     switchWorking( Not bEnable );
end;

function TEnrolamiento.AdquiereHuellas : Boolean;
var
   nRet, iId, iDedo, iValor: Integer;
   iContador : Extended;
   sFilename : string;
   ss           :TStringStream;
   ms           :TMemoryStream;
   oCursor: TCursor;
begin
     iId := 0;
     iDedo := 0;
     iContador := 0;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     switchWorking(true);
     try
          PrintMensaje('Inicializa dispositivo biometrico y motor de enrolamiento, espere un momento.',True);

          dmSistema.ReportaHuellaBorra;

          with dmSistema.cdsHuellaGti do
          begin
               Refrescar;
               First;
               Filter := Format( 'ID_NUMERO = %d', [ dmSistema.IdBiometrico ] );
               Filtered := True;

               try
               ms := TMemoryStream.Create;
               while not EOF do
               begin
                    //Barra de Progreso.
                    iContador := iContador + 1;
                    iValor := Round( 100 * iContador / RecordCount );
                    ProgresoHuellas.Position := iValor;
                    ProgresoHuellas.Visible := true;
                    Application.ProcessMessages();

                    try
                       iId := FieldByName( 'ID_NUMERO' ).AsInteger;
                       iDedo := FieldByName( 'HU_INDICE' ).AsInteger;

                       nRet := SFEStandard1.CheckFingerNum(iId, iDedo);
                       if nRet <> 0 then
                       begin
                            //Logica de enrolamiento
                            ss := TStringStream.Create( FieldByName( 'HU_HUELLA' ).AsString );
                            ms.Position := 0;
                            SZDecodeBase64( ss, ms );

                            sFilename := ExtractFilePath( Application.ExeName ) + IntToStr( iId ) + '_' + IntToStr( iDedo ) +  K_EXT_TEMPLATE;
                            ms.SaveToFile( sFilename );

                            nRet := SFEStandard1.LoadTemplateFromFile( sFilename, addrOfTemplate);

                            if nRet < 0 then
                                 //PrintError(nRet)
                            else
                            begin
                                 nRet := SFEStandard1.TemplateEnroll( iId , iDedo, 0, addrOfTemplate);
                                 if ( ( nRet < 0 ) and ( nRet <> -102 ) ) then
                                      //PrintError(nRet);
                            end;

                            DeleteFile( sFilename );
                            dmSistema.ReportaHuella( FieldByName( 'HU_ID' ).AsInteger );

                            Application.ProcessMessages();
                       end;
                    except
                          on Error: Exception do
                          begin
                               PrintMensaje(Format( 'ERROR [Id: %d, Indice: %d, IdHuella: %d]: ' + Error.Message, [ iId, iDedo, FieldByName( 'HU_ID' ).AsInteger ] ),False);
                          end;
                    end;
                    Next;
               end;
               finally
                      Filtered := False;
               end;
          end;
          dmSistema.ReportaHuellas;
     finally
            Screen.Cursor := oCursor;
            switchWorking(False);
     end;
     Result := True;
     PrintMensaje('Terminado, presione el bot�n de enrolar.', True);
     ProgresoHuellas.Visible := false;
     ms.Clear;
end;

procedure TEnrolamiento.BtnEnrolarClick(Sender: TObject);
var
   nRet, nCount, iId : Integer;
   sArchivoTemporal : String;
   sPrueba: TStringStream;
label stopWorking;
          function BuscaSiguienteDedo : Boolean;
          begin
               Result := True;
               while iDedo <= 10 do
                    if ( SFEStandard1.CheckFingerNum( dmSistema.IdBiometrico, iDedo ) < 0 ) then
                         break
                    else
                         iDedo := iDedo + 1;

               if ( iDedo > 10 ) then
               begin
                    PrintMensaje('Ya se enrolaron las 10 huellas del empleado.',True);
                    Result := False;
               end;                   
          end;

          function StreamToString(Stream: TStream): String;
          begin
              with TStringStream.Create('') do
                   try
                       CopyFrom(Stream, Stream.Size);
                       Result := DataString;
                   finally
                       Free;
                   end;
          end;

          procedure LoadFromFileToMem(const filename:string; memStream:TStream);
          var afileStream:TFileStream;
          begin
               afileStream:=TFileStream.Create(filename,fmOpenRead);
               try
                  memStream.CopyFrom(afileStream, afilestream.size );
               finally
                  afileStream.Free;
               end;
          end;

          function SaveTemplateToFile( sArchivo : String ) : Boolean;
          var
            nRet      : Integer;
          begin
               nRet := SFEStandard1.SaveTemplateToFile( sArchivo, addrOfTemplate );
               if nRet < 0 then
               begin
                    PrintError(nRet);
                    SaveTemplateToFile := False;
                    Exit;
               end;
               SaveTemplateToFile := True;
          end;
begin
     inherited;
     iDedo := 1;
     if ( BuscaSiguienteDedo ) then
     begin
          //Filepath
          sArchivoTemporal := ExtractFilePath( Application.ExeName ) + IntToStr( dmSistema.IdBiometrico ) + '_' + IntToStr( iDedo );
          iId := dmSistema.IdBiometrico;

          switchWorking(True);

          nRet := SFEStandard1.EnrollStart();
          if nRet < 0 then
          begin
               PrintError(nRet);
               goto stopWorking;
          end;

          for nCount := 1 to 3 do
          begin
               PrintMensaje('Coloque su dedo por ' + IntToStr(nCount) + 'a vez!',True);
               Application.ProcessMessages();
               TimerEnrollTimeout.Enabled := True;
               nRet := CaptureFinger();
               TimerEnrollTimeout.Enabled := False;

               if nRet <> 1 then
                    goto stopWorking;

               Application.ProcessMessages();
               PrintMensaje('Retire el dedo del lector.',True);

               nRet := SFEStandard1.EnrollNth(nCount);

               if nRet < 0 then
               begin
                    PrintError(nRet);
                    goto stopWorking;
               end;

               nRet := WaitForTakeOff();

               if nRet <> 1 then
                    goto stopWorking;
          end;



          nRet := SFEStandard1.EnrollEnd(iId, iDedo, 0);
          if nRet < 0 then
          begin
               PrintError(nRet);
               goto stopWorking;
          end;

          nRet := SFEStandard1.TemplateGetFromDB(iId, iDedo, addrOfTemplate);
          if nRet < 0 then
          begin
               PrintError(nRet);
               Exit;
          end;

          //Guarda Template File
          nRet := SFEStandard1.SaveTemplateToFile( sArchivoTemporal + K_EXT_TEMPLATE , addrOfTemplate);
          if nRet < 0 then
          begin
               PrintError(nRet);
               Exit;
          end;

          try
             try
                sPrueba := TStringStream.Create('');
                SZEncodeBase64( sArchivoTemporal + K_EXT_TEMPLATE, sPrueba );

                //Ya reporta
                dmSistema.InsertaHuella( Empleado, iDedo, sPrueba.DataString, Invitador );

                PrintMensaje( 'Enrolado exitoso!', true );
                iDedo := iDedo + 1;
             except
                PrintMensaje( 'Enrolado fallido!', true );
             end;
          finally
            DeleteFile( sArchivoTemporal + K_EXT_TEMPLATE );
          end;

        stopWorking:
          switchWorking(False);
     end;
end;

procedure TEnrolamiento.switchWorking(bWorking: Boolean);
begin
     BtnEnrolar.Enabled := not bWorking;
     gWorking := bWorking;
     gHintStopPos := 0;
     OK.Enabled := not bWorking;
     Cancelar.Enabled := not bWorking;
     Recargar.Enabled := not bWorking;
end;

procedure TEnrolamiento.PrintError(error: Integer);
begin
     PrintMensaje(GetError(error), True);
end;

function TEnrolamiento.WaitForTakeOff: Integer;
var
  nRet      : Integer;
  nCapArea  : Integer;
begin
     Result := 0;
     while True do
     begin
          Application.ProcessMessages();

          if Not gWorking then
          begin
               WaitForTakeOff := 0;
               break;
          end;

          nRet := SFEStandard1.Capture();

          if nRet < 0 then
          begin
               PrintError(nRet);
               WaitForTakeOff := 0;
               break;
          end;

          nCapArea := SFEStandard1.IsFinger();

          if nCapArea < 0 then
          begin
               WaitForTakeOff := 1;
               break;
          end;
     end;
end;

function TEnrolamiento.CaptureFinger: Integer;
var
  nRet        : Integer;
  nCapArea    : Integer;
  nMaxCapArea : Integer;
  nResult     : Integer;
begin
     nResult := 1;
     nMaxCapArea := 0;

     while True do
     begin
          try
             Application.ProcessMessages();
             if Not gWorking then
             begin
                  nResult := 0;
                  break;
             end;

             nRet := SFEStandard1.Capture();
             if nRet < 0 then
             begin
                  PrintError(nRet);
                  nResult := 0;
                  break;
             end;

             nCapArea := SFEStandard1.IsFinger();
             if nCapArea >= 0 then
             begin
                  if nCapArea < nMaxCapArea + 2 then break;
                  if nCapArea > nMaxCapArea then nMaxCapArea := nCapArea;
                  if nCapArea > 45 then break;
             end;
          except
                Result := nResult;
                Exit;
          end;
     end;

     if nResult = 1 then
     begin
          nRet := SFEStandard1.GetImage(addrOfImageBytes);
          if nRet < 0 then
          begin
               PrintError(nRet);
               nResult := 0;
          end
     end;
     CaptureFinger := nResult;
end;

procedure TEnrolamiento.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     switchWorking(False);
     if ( SFEStandard1 <> nil ) then
          SFEStandard1.Close();
     SFEStandard1.Free;
     inherited;
end;

procedure TEnrolamiento.TimerInicioTimer(Sender: TObject);

     function InicializaControl : Boolean;
     begin
          Result := False;
          try
             if ( SFEStandard1 = nil ) then
             begin
                  SFEStandard1 := TSFEStandard.Create(self);
                  SFEStandard1.TabStop := False;
             end;
             Result := True;
          except
                on E : Exception do
                   PrintMensaje('Driver de dispositivo biom�trico no detectado. ', False);
          end;
     end;
     
begin
     inherited;
     TimerInicio.Enabled := False;
     iDedo := 1;
     addrOfImageBytes := Integer(addr(gImageBytes));
     addrOfTemplate := Integer(addr(gTemplate));
     gHintStopPos := 0;

     if ( InicializaControl ) then
     begin
          if ( LeeArchivo ) then
               AdquiereHuellas
          else
               enableButtons(False);
     end;

     if ( Empleado = 0 ) and ( Invitador = 0 ) then
     begin
          PrintMensaje('No hay informaci�n del empleado o invitador!', True);
          BtnEnrolar.Enabled := False;
          Recargar.Enabled := False;
     end;
end;

procedure TEnrolamiento.RecargarClick(Sender: TObject);
begin
     inherited;

     if ( ZConfirm(self.Caption, '�Est� seguro de descargar nuevamente todas las huellas? ' + CR_LF +
                                 'Este proceso puede tomar unos minutos.', 0, mbNo ) ) then
     begin
          with dmSistema do
          begin
               switchWorking(False);
               SFEStandard1.Close();
               Application.ProcessMessages();
               
               //FechaPivote := NullDateTime;
               DeleteFile( LocalBioDbPath );
               ReiniciaHuellas;
          end;
          TimerInicio.Enabled := True;
     end;
end;

function TEnrolamiento.GetError(error: Integer): String;
begin
     case error of
        0: Result := 'Exito!';
        -1: Result := 'Error de imagen de huella!';
        -2: Result := 'Huella inv�lida!';
        -3: Result := 'Error de n�mero biom�trico!';
        -4: Result := 'Error de archivo de dispositivo!';
        -6: Result := 'Error de almacenamiento de dispositivo!';
        -7: Result := 'Error de sensor de dispositivo!';
        -8: Result := 'Orden de enrolamiento incorrecto!';
        -9: Result := 'No puede analizar las tres capturas, intente de nuevo!';
        -11: Result := 'La imagen no es un dedo!';
        -100: Result := 'No hay dispositivo biom�trico!';
        -101: Result := 'No puede abrir archivo de dispositivo local!';
        -102: Result := 'Ya ha enrolado esa huella!';
        -103: Result := 'Error de identificaci�n de huella!';
        -104: Result := 'Error de verificaci�n de huella!';
        -105: Result := 'No puede abrir la imagen de huella!';
        -106: Result := 'No puede crear imagen de huella!';
        -107: Result := 'No puede abrir huella!';
        -108: Result := 'No puede crear huella!';
        else Result := 'Error desconocido! (Numero =' + IntToStr(error) + ')';
    end;
end;

procedure TEnrolamiento.PrintMensaje(sMensaje: String; lLimpiaPantalla: Boolean);
begin
     if (lLimpiaPantalla) then
        lblMsg.Caption := sMensaje
     else
         lblMsg.Caption :=  lblMsg.Caption + CR_LF + sMensaje; 
end;

procedure TEnrolamiento.TimerEnrollTimeoutTimer(Sender: TObject);
begin
     inherited;
     TimerEnrollTimeout.Enabled := False;
     switchWorking(False);
     PrintMensaje('Tiempo expirado para enrolar. Presione nuevamente enrolar.', True);
     Application.ProcessMessages();
end;

end.
