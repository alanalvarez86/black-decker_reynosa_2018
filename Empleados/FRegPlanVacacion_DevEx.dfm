inherited RegPlanVacacion_DevEx: TRegPlanVacacion_DevEx
  Left = 327
  Top = 208
  Caption = 'Solicitar Vacaciones'
  ClientHeight = 325
  ClientWidth = 419
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 289
    Width = 419
    inherited OK_DevEx: TcxButton
      Left = 251
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 330
      Top = 5
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 419
    inherited ValorActivo2: TPanel
      Width = 93
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 419
    Height = 41
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 16
      Top = 12
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
      FocusControl = CB_CODIGO
    end
    object CB_CODIGO: TZetaDBKeyLookup_DevEx
      Left = 72
      Top = 8
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CB_CODIGO'
      DataSource = DataSource
    end
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 92
    Width = 417
    Height = 75
    TabOrder = 4
    object LVA_VAC_DEL: TLabel
      Left = 68
      Top = 7
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = 'Salida:'
      FocusControl = VP_FEC_INI
    end
    object LblGozados: TLabel
      Left = 31
      Top = 53
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'as gozados:'
    end
    object LVA_VAC_AL: TLabel
      Left = 57
      Top = 30
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Regresa:'
      FocusControl = VP_FEC_FIN
    end
    object VP_DIAS: TZetaDBTextBox
      Left = 104
      Top = 54
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'VP_DIAS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'VP_DIAS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object VP_FEC_INI: TZetaDBFecha
      Left = 104
      Top = 5
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '16/Dec/97'
      Valor = 35780.000000000000000000
      DataField = 'VP_FEC_INI'
      DataSource = DataSource
    end
    object VP_FEC_FIN: TZetaDBFecha
      Left = 104
      Top = 28
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '16/Dec/97'
      Valor = 35780.000000000000000000
      DataField = 'VP_FEC_FIN'
      DataSource = DataSource
    end
    object gbSaldos: TGroupBox
      Left = 240
      Top = 7
      Width = 161
      Height = 62
      Caption = ' Saldo de vacaciones'
      TabOrder = 2
      object Label5: TLabel
        Left = 6
        Top = 18
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'A'#241'os anteriores:'
      end
      object Label10: TLabel
        Left = 20
        Top = 38
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Proporcional:'
      end
      object VP_SAL_ANT: TZetaDBTextBox
        Left = 88
        Top = 16
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'VP_SAL_ANT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VP_SAL_ANT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object VP_SAL_PRO: TZetaDBTextBox
        Left = 88
        Top = 36
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'VP_SAL_PRO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'VP_SAL_PRO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
  end
  object GroupBox1: TGroupBox [5]
    Left = 0
    Top = 168
    Width = 417
    Height = 121
    Caption = 'Observaciones'
    TabOrder = 5
    object VP_SOL_COM: TcxDBMemo
      Left = 8
      Top = 16
      DataBinding.DataField = 'VP_SOL_COM'
      DataBinding.DataSource = DataSource
      Properties.ScrollBars = ssVertical
      TabOrder = 0
      Height = 89
      Width = 401
    end
  end
  inherited DataSource: TDataSource
    Left = 436
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
