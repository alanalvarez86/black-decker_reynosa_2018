inherited HisPagosIMSS_DevEx: THisPagosIMSS_DevEx
  Left = 153
  Top = 136
  Caption = 'Historial Pagos IMSS'
  ClientWidth = 613
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 613
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 287
      inherited textoValorActivo2: TLabel
        Width = 281
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 613
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object LS_PATRON: TcxGridDBColumn
        Caption = 'Patr'#243'n'
        DataBinding.FieldName = 'LS_PATRON'
      end
      object LS_YEAR: TcxGridDBColumn
        Caption = 'A'#241'o'
        DataBinding.FieldName = 'LS_YEAR'
      end
      object LS_MONTH: TcxGridDBColumn
        Caption = 'Mes'
        DataBinding.FieldName = 'LS_MONTH'
      end
      object LS_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'LS_TIPO'
      end
      object LE_DIAS_CO: TcxGridDBColumn
        Caption = 'D'#237'as'
        DataBinding.FieldName = 'LE_DIAS_CO'
      end
      object LE_TOT_IMS: TcxGridDBColumn
        Caption = 'IMSS'
        DataBinding.FieldName = 'LE_TOT_IMS'
      end
      object LE_TOT_RET: TcxGridDBColumn
        Caption = 'Retiro'
        DataBinding.FieldName = 'LE_TOT_RET'
      end
      object LE_TOT_INF: TcxGridDBColumn
        Caption = 'INFONAVIT'
        DataBinding.FieldName = 'LE_TOT_INF'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
