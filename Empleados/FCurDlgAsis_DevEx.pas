unit FCurDlgAsis_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Mask, ZetaNumero, Buttons,ZetaCommonTools,ZetaCommonClasses,
  ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons, cxControls, cxContainer,
  cxEdit, cxLabel, cxGroupBox, cxRadioGroup, cxCheckBox;

type
  TCurDlgAsis_DevEx = class(TZetaDlgModal_DevEx)
    Label6: TLabel;
    Label5: TLabel;
    cxGroupBox1: TcxGroupBox;
    chkValidarAsistencia: TcxCheckBox;
    chkValidarEvaluaciones: TcxCheckBox;
    rdAsistenciaMinima: TcxRadioGroup;
    cxLabel1: TcxLabel;
    lblEval1: TLabel;
    IC_EVA_1: TZetaNumero;
    IC_EVA_2: TZetaNumero;
    lblEval2: TLabel;
    lblEval3: TLabel;
    IC_EVA_3: TZetaNumero;
    IC_EVA_FIN: TZetaNumero;
    lblEvalFin: TLabel;
    NumSesiones: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure rdAsistenciaMinimaClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure chkValidarAsistenciaClick(Sender: TObject);
    procedure chkValidarEvaluacionesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FParametros :TZetaParams;
    procedure HabilitarControles;
  public
    { Public declarations }
    property Parametros :TZetaParams read FParametros write FParametros; 

  end;

var
  CurDlgAsis_DevEx: TCurDlgAsis_DevEx;


implementation
uses
    ZetaDialogo;
    
{$R *.dfm}

procedure TCurDlgAsis_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  Parametros:= TZetaParams.Create;
  NumSesiones.Valor := 1;
end;

procedure TCurDlgAsis_DevEx.OKClick(Sender: TObject);
var
   Sesiones: Integer;
begin
     Sesiones := NumSesiones.ValorEntero;
     if ( chkValidarAsistencia.Checked ) and ( rdAsistenciaMinima.ItemIndex = 0 ) then
        Sesiones := 0;
     if ( chkValidarAsistencia.Checked ) and ( rdAsistenciaMinima.ItemIndex = 1 ) and ( NumSesiones.ValorEntero = 0 ) then
     begin
        ZetaDialogo.zError( '� Error de N�mero de Sesiones !', 'La Asistencia M�nima Debe Ser Mayor a Cero', 0 );
        ModalResult := mrIgnore;
     end
     else
     begin
          inherited;
          with FParametros do
          begin
               AddBoolean('ValidarAsis',chkValidarAsistencia.Checked );
               AddBoolean('ValidarEval',chkValidarEvaluaciones.Checked );
               AddBoolean('AsisPerfecta',(rdAsistenciaMinima.ItemIndex = 0));
               AddInteger('AsisSesiones',Sesiones);
               AddFloat ('Eval1',IC_EVA_1.Valor );
               AddFloat('Eval2',IC_EVA_2.Valor );
               AddFloat('Eval3',IC_EVA_3.Valor );
               AddFloat('EvalFin',IC_EVA_FIN.Valor );
          end;
     end;
end;

procedure TCurDlgAsis_DevEx.rdAsistenciaMinimaClick(Sender: TObject);
begin
     inherited;
     NumSesiones.Enabled := rdAsistenciaMinima.ItemIndex = 1;
end;

procedure TCurDlgAsis_DevEx.FormCloseQuery(Sender: TObject;  var CanClose: Boolean);
begin
     inherited;
     CanClose := ModalResult <> mrIgnore;

end;

procedure  TCurDlgAsis_DevEx.HabilitarControles;
begin
     rdAsistenciaMinima.Enabled := chkValidarAsistencia.Checked;
     //NumSesiones.Enabled := rdAsistenciaMinima.Enabled; //Bug de clasica
     //gpEvaluaciones.Enabled  := chkValidarEvaluaciones.Checked;
     {lblEval1.Enabled := gpEvaluaciones.Enabled;
     lblEval2.Enabled := gpEvaluaciones.Enabled;
     lblEval3.Enabled := gpEvaluaciones.Enabled;
     lblEvalFin.Enabled := gpEvaluaciones.Enabled; }
     lblEval1.Enabled := chkValidarEvaluaciones.Checked;
     lblEval2.Enabled := chkValidarEvaluaciones.Checked;
     lblEval3.Enabled := chkValidarEvaluaciones.Checked;
     lblEvalFin.Enabled := chkValidarEvaluaciones.Checked;
     IC_EVA_1.Enabled := chkValidarEvaluaciones.Checked;
     IC_EVA_2.Enabled := chkValidarEvaluaciones.Checked;
     IC_EVA_3.Enabled := chkValidarEvaluaciones.Checked;
     IC_EVA_FIN.Enabled := chkValidarEvaluaciones.Checked;
end;

procedure TCurDlgAsis_DevEx.chkValidarAsistenciaClick(Sender: TObject);
begin
     inherited;
     HabilitarControles;
end;

procedure TCurDlgAsis_DevEx.chkValidarEvaluacionesClick(Sender: TObject);
begin
     inherited;
     HabilitarControles;
end;

procedure TCurDlgAsis_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     HabilitarControles;
end;

procedure TCurDlgAsis_DevEx.OK_DevExClick(Sender: TObject);
Var
Sesiones: Integer;
begin
Sesiones := NumSesiones.ValorEntero;
     if ( chkValidarAsistencia.Checked ) and ( rdAsistenciaMinima.ItemIndex = 0 ) then
        Sesiones := 0;
     if ( chkValidarAsistencia.Checked ) and ( rdAsistenciaMinima.ItemIndex = 1 ) and ( NumSesiones.ValorEntero = 0 ) then
     begin
        ZetaDialogo.zError( '� Error de N�mero de Sesiones !', 'La Asistencia M�nima Debe Ser Mayor a Cero', 0 );
        ModalResult := mrIgnore;
     end
     else
     begin
          inherited;
          with FParametros do
          begin
               AddBoolean('ValidarAsis',chkValidarAsistencia.Checked );
               AddBoolean('ValidarEval',chkValidarEvaluaciones.Checked );
               AddBoolean('AsisPerfecta',(rdAsistenciaMinima.ItemIndex = 0));
               AddInteger('AsisSesiones',Sesiones);
               AddFloat ('Eval1',IC_EVA_1.Valor );
               AddFloat('Eval2',IC_EVA_2.Valor );
               AddFloat('Eval3',IC_EVA_3.Valor );
               AddFloat('EvalFin',IC_EVA_FIN.Valor );
          end;
     end;
end;

end.
