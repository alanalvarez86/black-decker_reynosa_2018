unit FEditReservaAula_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons, StdCtrls,
  Mask, ZetaFecha, ZetaHora, ZetaDBTextBox, ZetaNumero,
  ZetaEdit, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxDBEdit;

type
  TEditReservaAula_DevEx = class(TBaseEdicion_DevEx)
    lblAula: TLabel;
    AL_CODIGO: TZetaDBKeyLookup_DevEx;
    lblFecIni: TLabel;
    RV_FEC_INI: TZetaDBFecha;
    lblFecFin: TLabel;
    RV_FEC_FIN: TZetaDBFecha;
    lblHoraIni: TLabel;
    lblHoraFin: TLabel;
    RV_HOR_INI: TZetaDBHora;
    RV_HOR_FIN: TZetaDBHora;
    lblResumen: TLabel;
    RV_RESUMEN: TDBEdit;
    gbDetalle: TGroupBox;
    RV_DETALLE: TcxDBMemo;
    lblReferencia: TLabel;
    gbReserva: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    RV_FEC_RES: TZetaDBTextBox;
    RV_HOR_RES: TZetaDBTextBox;
    RV_TIPO: TZetaDBTextBox;
    Label5: TLabel;
    MA_CODIGO: TZetaDBKeyLookup_DevEx;
    lblReservo: TLabel;
    US_DESCRIP: TZetaDBTextBox;
    RV_ORDEN: TZetaDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    function ObtieneMensaje( const sPalabra: String ): String;
    procedure ControlesReservacion;
  protected
    procedure Connect; override;
    function EsEditable: Boolean; virtual;
    function ValidaTipoReserva: Boolean; virtual;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
  end;

var
  EditReservaAula_DevEx: TEditReservaAula_DevEx;

implementation

uses DRecursos,
     DCatalogos,
     ZAccesosTress,
     ZetaCommonLists,
     ZetaDialogo,
     ZetaCommonClasses;

{$R *.DFM}

procedure TEditReservaAula_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          AL_CODIGO.LookupDataSet := cdsAulas;
          MA_CODIGO.LookupDataSet := cdsMaestros;
     end;
     IndexDerechos := D_CAPA_RESERVAS;
     HelpContext:= H_RESERVACION_DE_AULAS;
     FirstControl := RV_ORDEN;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditReservaAula_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsAulas.Conectar;
          cdsMaestros.Conectar;
     end;
     DataSource.DataSet:= dmRecursos.cdsReserva;
end;

function TEditReservaAula_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     if( inherited PuedeBorrar( sMensaje ) )then
     begin
          Result := ValidaTipoReserva;
          if not Result then
             sMensaje := ObtieneMensaje( 'borrar' );
     end;
end;

function TEditReservaAula_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     if( inherited PuedeModificar( sMensaje ) ) then
     begin
          Result := ValidaTipoReserva;
          if not Result then
             sMensaje := ObtieneMensaje( 'modificar' );
     end;
end;

function TEditReservaAula_DevEx.ValidaTipoReserva: Boolean;
begin
     Result := ( dmRecursos.cdsReserva.FieldByName('RV_TIPO').AsInteger = Ord( rvBloqueo ) );
end;

function TEditReservaAula_DevEx.ObtieneMensaje( const sPalabra: String ): String;
begin
     Result :=  Format( 'Solo se pueden %s reservaciones de tipo Bloqueo', [ sPalabra ] );
end;


procedure TEditReservaAula_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     ControlesReservacion;
end;

procedure TEditReservaAula_DevEx.ControlesReservacion;
var
   lAutoEdit: Boolean;
begin
     lAutoEdit := EsEditable;
     DataSource.AutoEdit := lAutoEdit;
     RV_FEC_INI.Enabled := lAutoEdit;
     RV_FEC_FIN.Enabled := lAutoEdit;
     RV_HOR_INI.Enabled := lAutoEdit;
     RV_HOR_FIN.Enabled := lAutoEdit;
     RV_ORDEN.Enabled := lAutoEdit;
     AL_CODIGO.Enabled := lAutoEdit;
     MA_CODIGO.Enabled := lAutoEdit;
     RV_RESUMEN.Enabled := lAutoEdit;
     RV_DETALLE.Enabled := lAutoEdit;
end;

function TEditReservaAula_DevEx.EsEditable: Boolean;
begin
     Result := ValidaTipoReserva;
end;

procedure TEditReservaAula_DevEx.SetEditarSoloActivos;
begin
     MA_CODIGO.EditarSoloActivos := TRUE;
end;

end.
