unit FEditHisCurso_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaNumero, StdCtrls, DBCtrls, Mask, ZetaFecha,
  ComCtrls, Db, 
  ExtCtrls, Buttons, ZetaDBTextBox, ZetaSmartLists, ZBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013,
  dxSkinsDefaultPainters, 
  cxControls, dxSkinsdxBarPainter, ZetaKeyLookup_DevEx,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, dxBarBuiltInMenu;

type
  TEditHisCurso_DevEx = class(TBaseEdicion_DevEx)
    PageControl: TcxPageControl;
    TabCurso: TcxTabSheet;
    TabClasifi: TcxTabSheet;
    TabNiveles: TcxTabSheet;
    Label1: TLabel;
    KC_FEC_TOMLbl: TLabel;
    KC_HORASLbl: TLabel;
    KC_EVALUALbl: TLabel;
    MA_CODIGOLbl: TLabel;
    KC_FEC_TOM: TZetaDBFecha;
    KC_HORAS: TZetaDBNumero;
    KC_EVALUA: TZetaDBNumero;
    MA_CODIGO: TZetaDBKeyLookup_DevEx;
    HorarioLbl: TLabel;
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    PuestoLbl: TLabel;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    ClasificacionLbl: TLabel;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    CB_NIVEL9: TZetaDBKeyLookup_DevEx;
    CB_NIVEL8: TZetaDBKeyLookup_DevEx;
    CB_NIVEL7: TZetaDBKeyLookup_DevEx;
    CB_NIVEL6: TZetaDBKeyLookup_DevEx;
    CB_NIVEL5: TZetaDBKeyLookup_DevEx;
    CB_NIVEL4: TZetaDBKeyLookup_DevEx;
    CB_NIVEL3: TZetaDBKeyLookup_DevEx;
    CB_NIVEL2: TZetaDBKeyLookup_DevEx;
    CB_NIVEL1: TZetaDBKeyLookup_DevEx;
    CU_CODIGO: TZetaDBKeyLookup_DevEx;
    KC_FEC_FINLbl: TLabel;
    KC_FEC_FIN: TZetaDBFecha;
    lblRevision: TLabel;
    KC_REVISIO: TDBEdit;
    lblSesion: TLabel;
    SE_FOLIO: TZetaDBTextBox;
    CB_NIVEL10lbl: TLabel;
    CB_NIVEL10: TZetaDBKeyLookup_DevEx;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL11: TZetaDBKeyLookup_DevEx;
    CB_NIVEL12lbl: TLabel;
    CB_NIVEL12: TZetaDBKeyLookup_DevEx;
    KC_EST: TZetaDBKeyLookup_DevEx;
    lblEstablec: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure CB_NIVEL12ValidLookup(Sender: TObject);
    procedure CB_NIVEL11ValidLookup(Sender: TObject);
    procedure CB_NIVEL10ValidLookup(Sender: TObject);
    procedure CB_NIVEL9ValidLookup(Sender: TObject);
    procedure CB_NIVEL8ValidLookup(Sender: TObject);
    procedure CB_NIVEL7ValidLookup(Sender: TObject);
    procedure CB_NIVEL6ValidLookup(Sender: TObject);
    procedure CB_NIVEL5ValidLookup(Sender: TObject);
    procedure CB_NIVEL4ValidLookup(Sender: TObject);
    procedure CB_NIVEL3ValidLookup(Sender: TObject);
    procedure CB_NIVEL2ValidLookup(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    procedure SetCamposNivel;
    procedure ControlesSesion;
    function EsEditable: Boolean;
    {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$ifend}
    {$ifdef ACS}procedure SetPosicionNiveles;{ACS}{$endif}
    {$ifdef ACS}
    procedure LimpiaLookUpOlfKey;{ACS}
    {$endif}
  protected
    procedure Connect; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    function PuedeAgregar( var sMensaje: String ):Boolean;override;
  public
    FCursoAnterior:string;
    { Public declarations }
  end;

var
  EditHisCurso_DevEx: TEditHisCurso_DevEx;

  {$ifdef ACS}
const
     K_FORMA_ACS = 452;
     K_ALT_DEF = 24;
{$endif}

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaClientTools,
     ZAccesosTress,
     DTablas,
     DCatalogos,
     {$IFDEF SUPERVISORES}
     DSuper,
     {$ELSE}
     DRecursos,
     {$ENDIF}
     DGlobal,
     ZetaClientDataSet,
     ZGlobalTress;

{$R *.DFM}

procedure TEditHisCurso_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;

     CB_NIVEL12lbl.Top := CB_NIVEL1lbl.Top;
     CB_NIVEL11lbl.Top := CB_NIVEL12lbl.Top + K_ALT_DEF;
     CB_NIVEL10lbl.Top := CB_NIVEL11lbl.Top + K_ALT_DEF;
     CB_NIVEL9lbl.Top := CB_NIVEL10lbl.Top + K_ALT_DEF;
     CB_NIVEL8lbl.Top := CB_NIVEL9lbl.Top + K_ALT_DEF;
     CB_NIVEL7lbl.Top := CB_NIVEL8lbl.Top + K_ALT_DEF;
     CB_NIVEL6lbl.Top := CB_NIVEL7lbl.Top + K_ALT_DEF;
     CB_NIVEL5lbl.Top := CB_NIVEL6lbl.Top + K_ALT_DEF;
     CB_NIVEL4lbl.Top := CB_NIVEL5lbl.Top + K_ALT_DEF;
     CB_NIVEL3lbl.Top := CB_NIVEL4lbl.Top + K_ALT_DEF;
     CB_NIVEL2lbl.Top := CB_NIVEL3lbl.Top + K_ALT_DEF;
     CB_NIVEL1lbl.Top := CB_NIVEL2lbl.Top + K_ALT_DEF;

     CB_NIVEL12.Top := CB_NIVEL1.Top;
     CB_NIVEL11.Top := CB_NIVEL12.Top + K_ALT_DEF;
     CB_NIVEL10.Top := CB_NIVEL11.Top + K_ALT_DEF;
     CB_NIVEL9.Top := CB_NIVEL10.Top + K_ALT_DEF;
     CB_NIVEL8.Top := CB_NIVEL9.Top + K_ALT_DEF;
     CB_NIVEL7.Top := CB_NIVEL8.Top + K_ALT_DEF;
     CB_NIVEL6.Top := CB_NIVEL7.Top + K_ALT_DEF;
     CB_NIVEL5.Top := CB_NIVEL6.Top + K_ALT_DEF;
     CB_NIVEL4.Top := CB_NIVEL5.Top + K_ALT_DEF;
     CB_NIVEL3.Top := CB_NIVEL4.Top + K_ALT_DEF;
     CB_NIVEL2.Top := CB_NIVEL3.Top + K_ALT_DEF;
     CB_NIVEL1.Top := CB_NIVEL2.Top + K_ALT_DEF;

     CB_NIVEL12.TabOrder := 1;
     CB_NIVEL11.TabOrder := 2;
     CB_NIVEL10.TabOrder := 3;
     CB_NIVEL9.TabOrder := 4;
     CB_NIVEL8.TabOrder := 5;
     CB_NIVEL7.TabOrder := 6;
     CB_NIVEL6.TabOrder := 7;
     CB_NIVEL5.TabOrder := 8;
     CB_NIVEL4.TabOrder := 9;
     CB_NIVEL3.TabOrder := 10;
     CB_NIVEL2.TabOrder := 11;
     CB_NIVEL1.TabOrder := 12;

     Self.Height := K_FORMA_ACS;

     {
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     }
     {$endif}
     HelpContext:= H11515_Curso_tomado;
     TipoValorActivo1 := stEmpleado;
     {$IFDEF SUPERVISORES}
     IndexDerechos := ZAccesosTress.D_SUPER_CURSOS_TOMADOS;
     {$ELSE}
     IndexDerechos := ZAccesosTress.D_EMP_EXP_CURSOS_TOMADOS;
     {$ENDIF}
     FirstControl := CU_CODIGO;
     SetCamposNivel;

     MA_CODIGO.LookupDataset := dmCatalogos.cdsMaestros;
     CU_CODIGO.LookupDataset := dmCatalogos.cdsCursos;
     CB_TURNO.LookupDataset := dmCatalogos.cdsTurnos;
     CB_PUESTO.LookupDataset := dmCatalogos.cdsPuestos;
     CB_CLASIFI.LookupDataset := dmCatalogos.cdsClasifi;
     {$ifdef ACS}
     CB_NIVEL12.LookupDataset := dmTablas.cdsNivel12;
     CB_NIVEL11.LookupDataset := dmTablas.cdsNivel11;
     CB_NIVEL10.LookupDataset := dmTablas.cdsNivel10;
     {$endif}
     CB_NIVEL9.LookupDataset := dmTablas.cdsNivel9;
     CB_NIVEL8.LookupDataset := dmTablas.cdsNivel8;
     CB_NIVEL7.LookupDataset := dmTablas.cdsNivel7;
     CB_NIVEL6.LookupDataset := dmTablas.cdsNivel6;
     CB_NIVEL5.LookupDataset := dmTablas.cdsNivel5;
     CB_NIVEL4.LookupDataset := dmTablas.cdsNivel4;
     CB_NIVEL3.LookupDataset := dmTablas.cdsNivel3;
     CB_NIVEL2.LookupDataset := dmTablas.cdsNivel2;
     CB_NIVEL1.LookupDataset := dmTablas.cdsNivel1;
     KC_EST.LookupDataset := dmCatalogos.cdsEstablecimientos; 
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditHisCurso_DevEx.FormShow(Sender: TObject);
begin
     PageControl.ActivePage := TabCurso;
     CU_CODIGO.ResetMemory;
     {$ifdef ACS}SetPosicionNiveles;{ACS}{$endif}

     inherited;
     {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     CambiosVisuales;
     {$ifend}

     {$ifdef ICUMEDICAL_CURSOS}
     {$ifdef SUPERVISORES}
     KC_REVISIO.Enabled := False;
     {$endif}
     {$endif}
end;

procedure TEditHisCurso_DevEx.Connect;
begin
     with dmTablas do
     begin
          {$ifdef ACS}
          LimpiaLookUpOlfKey;
          {$endif}
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     with dmCatalogos do
     begin
          cdsTurnos.Conectar;
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
          cdsCursos.Conectar;
          cdsMaestros.Conectar;
          cdsEstablecimientos.Conectar;
     end;
     {$IFDEF SUPERVISORES}
     with dmSuper do
     {$ELSE}
     with dmRecursos do
     {$ENDIF}
     begin
          cdsHisCursos.Conectar;
          DataSource.DataSet := cdsHisCursos;
     end;
end;

procedure TEditHisCurso_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12 );
     {$endif}
end;
procedure TEditHisCurso_DevEx.ControlesSesion;
var
   lAutoEdit: Boolean;
begin
     lAutoEdit := EsEditable;
     DataSource.AutoEdit := lAutoEdit;
     KC_FEC_TOM.Enabled := lAutoEdit;
     KC_FEC_FIN.Enabled := lAutoEdit;
     CU_CODIGO.Enabled := lAutoEdit;
     KC_REVISIO.Enabled := lAutoEdit;
     KC_HORAS.Enabled := lAutoEdit;
     KC_EVALUA.Enabled := lAutoEdit;
     MA_CODIGO.Enabled := lAutoEdit;
     CB_PUESTO.Enabled := lAutoEdit;
     CB_TURNO.Enabled := lAutoEdit;
     CB_CLASIFI.Enabled := lAutoEdit;
     CB_NIVEL1.Enabled := lAutoEdit;
     CB_NIVEL2.Enabled := lAutoEdit;
     CB_NIVEL3.Enabled := lAutoEdit;
     CB_NIVEL4.Enabled := lAutoEdit;
     CB_NIVEL5.Enabled := lAutoEdit;
     CB_NIVEL6.Enabled := lAutoEdit;
     CB_NIVEL7.Enabled := lAutoEdit;
     CB_NIVEL8.Enabled := lAutoEdit;
     CB_NIVEL9.Enabled := lAutoEdit;
     {$ifdef ACS}
     CB_NIVEL10.Enabled := lAutoEdit;
     CB_NIVEL11.Enabled := lAutoEdit;
     CB_NIVEL12.Enabled := lAutoEdit;
     {$endif}
     dxBarButton_BorrarBtn.Enabled := lAutoEdit;
     dxBarButton_ModificarBtn.Enabled := lAutoEdit;
     KC_EST.Enabled := lAutoEdit;
end;

function TEditHisCurso_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     if EsEditable then
     begin
          Result := inherited PuedeBorrar( sMensaje );
     end
     else
     begin
          sMensaje := 'Este registro pertenece a un grupo';
          Result := False;
     end;
end;

function TEditHisCurso_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     if EsEditable then
     begin
          Result := inherited PuedeModificar( sMensaje );
     end
     else
     begin
          sMensaje := 'Este registro pertenece a un grupo';
          Result := False;
     end;
end;

function TEditHisCurso_DevEx.EsEditable: Boolean;
begin
     Result := ( TZetaClientDataSet( DataSource.DataSet ).FieldByName('SE_FOLIO').AsInteger = 0 );
     //Result := (dmRecursos.cdsHisCursos.FieldByName('SE_FOLIO').AsInteger = 0);
end;

procedure TEditHisCurso_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
const
K_INACTIVO = '';
begin
     inherited;
     ControlesSesion;
     {$IFNDEF SUPERVISORES}
     if not Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS ) then
     begin
          if Field = nil then
             dmRecursos.FUltimoCurso := DataSource.DataSet.FieldByName('CU_CODIGO').AsString;
     end;
     {$ENDIF}
end;


function TEditHisCurso_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     {$IFDEF SUPERVISORES}
     Result:= FALSE;
     sMensaje:= 'No se puede agregar grupos desde esta pantalla'
     {$ELSE}
     Result:= inherited PuedeAgregar( sMensaje );
     {$ENDIF}
end;

{ACS: Se establece la relaci�n del nivel seleccionado con el nivel que tiene relacionado
      (nivel 12 al 2, el nivel 1 no tiene relaci�n por ser el nivel inferior).}
procedure TEditHisCurso_DevEx.CB_NIVEL12ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL11.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL11' ).AsString := dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditHisCurso_DevEx.CB_NIVEL11ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL10.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL10' ).AsString := dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditHisCurso_DevEx.CB_NIVEL10ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL9.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL9' ).AsString := dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditHisCurso_DevEx.CB_NIVEL9ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL8.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL8' ).AsString := dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditHisCurso_DevEx.CB_NIVEL8ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL7.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL7' ).AsString := dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditHisCurso_DevEx.CB_NIVEL7ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL6.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL6' ).AsString := dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditHisCurso_DevEx.CB_NIVEL6ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL5.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL5' ).AsString := dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditHisCurso_DevEx.CB_NIVEL5ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL4.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL4' ).AsString := dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditHisCurso_DevEx.CB_NIVEL4ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL3.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL3' ).AsString := dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditHisCurso_DevEx.CB_NIVEL3ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL2.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL2' ).AsString := dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditHisCurso_DevEx.CB_NIVEL2ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL1.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL1' ).AsString := dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

{$ifdef ACS}
{ACS: Este procedimiento re-ajustar� la posici�n de los controles de los niveles.}
procedure TEditHisCurso_DevEx.SetPosicionNiveles;
const
     K_ALTURA_DEF = 24;
     K_TOP_N1 = 272;
     K_TOP_N1_LBL = 276;
     K_TOP_N2 = 248;
     K_TOP_N2_LBL = 252;
     K_TOP_N3 = 224;
     K_TOP_N3_LBL = 228;
     K_TOP_N4 = 200;
     K_TOP_N4_LBL = 204;
     K_TOP_N5 = 176;
     K_TOP_N5_LBL = 180;
     K_TOP_N6 = 152;
     K_TOP_N6_LBL = 156;
     K_TOP_N7 = 128;
     K_TOP_N7_LBL = 132;
     K_TOP_N8 = 104;
     K_TOP_N8_LBL = 108;
     K_TOP_N9 = 80;
     K_TOP_N9_LBL = 84;
     K_TOP_N10 = 56;
     K_TOP_N10_LBL = 60;
     K_TOP_N11 = 32;
     K_TOP_N11_LBL = 36;
     K_TOP_N12 = 8;
     K_TOP_N12_LBL = 12;
var
     iNivelesNoVisibles, iTotalHorizontal: Integer;
begin
     {Se obtiene la cantidad total de niveles no visibles}
     iNivelesNoVisibles := 0;
    // iTotalHorizontal := 0;
     if Not CB_NIVEL12.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL11.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL10.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL9.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL8.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL7.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL6.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL5.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL4.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL3.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL2.Visible then
        inc( iNivelesNoVisibles );

     iTotalHorizontal := ( iNivelesNoVisibles * K_ALTURA_DEF );
     {Se re-ajustan todos los controles en la posici�n horizontal}
     CB_NIVEL12.Top := K_TOP_N12 - iTotalHorizontal;
     CB_NIVEL12lbl.Top := K_TOP_N12_LBL - iTotalHorizontal;
     CB_NIVEL11.Top := K_TOP_N11 - iTotalHorizontal;
     CB_NIVEL11lbl.Top := K_TOP_N11_LBL - iTotalHorizontal;
     CB_NIVEL10.Top := K_TOP_N10 - iTotalHorizontal;
     CB_NIVEL10lbl.Top := K_TOP_N10_LBL - iTotalHorizontal;
     CB_NIVEL9.Top := K_TOP_N9 - iTotalHorizontal;
     CB_NIVEL9lbl.Top := K_TOP_N9_LBL - iTotalHorizontal;
     CB_NIVEL8.Top := K_TOP_N8 - iTotalHorizontal;
     CB_NIVEL8lbl.Top := K_TOP_N8_LBL - iTotalHorizontal;
     CB_NIVEL7.Top := K_TOP_N7 - iTotalHorizontal;
     CB_NIVEL7lbl.Top := K_TOP_N7_LBL - iTotalHorizontal;
     CB_NIVEL6.Top := K_TOP_N6 - iTotalHorizontal;
     CB_NIVEL6lbl.Top := K_TOP_N6_LBL - iTotalHorizontal;
     CB_NIVEL5.Top := K_TOP_N5 - iTotalHorizontal;
     CB_NIVEL5lbl.Top := K_TOP_N5_LBL - iTotalHorizontal;
     CB_NIVEL4.Top := K_TOP_N4 - iTotalHorizontal;
     CB_NIVEL4lbl.Top := K_TOP_N4_LBL - iTotalHorizontal;
     CB_NIVEL3.Top := K_TOP_N3 - iTotalHorizontal;
     CB_NIVEL3lbl.Top := K_TOP_N3_LBL - iTotalHorizontal;
     CB_NIVEL2.Top := K_TOP_N2 - iTotalHorizontal;
     CB_NIVEL2lbl.Top := K_TOP_N2_LBL - iTotalHorizontal;
     CB_NIVEL1.Top := K_TOP_N1 - iTotalHorizontal;
     CB_NIVEL1lbl.Top := K_TOP_N1_LBL - iTotalHorizontal;
end;
{$endif}

{$ifdef ACS}
{Este procedimiento sirve para eliminar cualquier OLDKEY de los lookups de niveles}
procedure TEditHisCurso_DevEx.LimpiaLookUpOlfKey;
begin
          CB_NIVEL1.ResetMemory;
          CB_NIVEL2.ResetMemory;
          CB_NIVEL3.ResetMemory;
          CB_NIVEL4.ResetMemory;
          CB_NIVEL5.ResetMemory;
          CB_NIVEL6.ResetMemory;
          CB_NIVEL7.ResetMemory;
          CB_NIVEL8.ResetMemory;
          CB_NIVEL9.ResetMemory;
          CB_NIVEL10.ResetMemory;
          CB_NIVEL11.ResetMemory;
          CB_NIVEL12.ResetMemory;
end;
{$endif}


{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TEditHisCurso_DevEx.CambiosVisuales;
var
  I : Integer;
begin
     for I := 0 to TabNiveles.ControlCount - 1 do
     begin
          if TabNiveles.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT - (TabNiveles.Controls[I].Width+2);
          end;
          if TabNiveles.Controls[I].ClassNameIs( 'TZetaDbKeyLookup_DevEx' ) then
          begin
               if ( TabNiveles.Controls[I].Visible ) then
               begin
                    TabNiveles.Controls[I].Width := K_WIDTH_LOOKUP;
                    TZetaDbKeyLookup_DevEx( TabNiveles.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                    TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT;
               end;
          end;
     end;
end;
{$ifend}

procedure TEditHisCurso_DevEx.SetEditarSoloActivos;
begin
     CU_CODIGO.EditarSoloActivos := TRUE;
     MA_CODIGO.EditarSoloActivos := TRUE;
     KC_EST.EditarSoloActivos := TRUE;
     CB_PUESTO.EditarSoloActivos := TRUE;
     CB_CLASIFI.EditarSoloActivos := TRUE;
     CB_TURNO.EditarSoloActivos := TRUE;
     CB_NIVEL1.EditarSoloActivos := TRUE;
     CB_NIVEL2.EditarSoloActivos := TRUE;
     CB_NIVEL3.EditarSoloActivos := TRUE;
     CB_NIVEL4.EditarSoloActivos := TRUE;
     CB_NIVEL5.EditarSoloActivos := TRUE;
     CB_NIVEL6.EditarSoloActivos := TRUE;
     CB_NIVEL7.EditarSoloActivos := TRUE;
     CB_NIVEL8.EditarSoloActivos := TRUE;
     CB_NIVEL9.EditarSoloActivos := TRUE;
     CB_NIVEL10.EditarSoloActivos := TRUE;
     CB_NIVEL11.EditarSoloActivos := TRUE;
     CB_NIVEL12.EditarSoloActivos := TRUE;
end;


end.







