unit FKardexPuesto_DevEx;

interface                                                       

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ZetaFecha, ExtCtrls, StdCtrls, DBCtrls, ZetaDBTextBox, Mask, ComCtrls,
  Buttons, Grids, DBGrids,
  {$ifndef VER130}
  Variants,
  {$endif}
  FKardexBase_DevEx,
  ZetaNumero,
  ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxControls, cxContainer, cxEdit, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxTextEdit, cxMemo, cxDBEdit, cxPC, cxButtons, ZetaKeyLookup_DevEx;

type
  TKardexPuesto_DevEx = class(TKardexBase_DevEx)
    CB_RANGO_S: TZetaDBNumero;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    CB_RANGO_SLbl: TLabel;
    ClasificacionLbl: TLabel;
    PuestoLbl: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure CB_PUESTOExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure SetEditarSoloActivos;
  protected
    procedure Connect;override;
    procedure EscribirCambios; override;
  private
    { Private declarations }
    sPuesto: String;
    FOldPuesto: String;
    procedure SetMatsushita;
  public
    { Public declarations }
  end;

var
  KardexPuesto_DevEx: TKardexPuesto_DevEx;

implementation

uses ZAccesosTress,ZetaCommonClasses,ZetaCommonLists, ZetaCommonTools, ZetaDialogo,
     dCatalogos, dSistema, dRecursos, DCliente;

{$R *.DFM}

procedure TKardexPuesto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10156_Cambio_puesto;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX;
     with dmCatalogos do
     begin
          CB_PUESTO.LookupDataSet  := cdsPuestos;
          CB_CLASIFI.LookupDataSet := cdsClasifi;
     end;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TKardexPuesto_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetMatsushita;
     CB_PUESTO.Enabled := ( not dmCliente.UsaPlazas );
     if CB_PUESTO.Enabled then
        self.Caption := 'Cambio de Puesto'
     else
         self.Caption := 'Cambio de Clasificaci�n';
end;

procedure TKardexPuesto_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
     end;
     with dmRecursos do
     begin
          cdsEditHisKardex.Conectar;
          DataSource.DataSet := cdsEditHisKardex;
          sPuesto := cdsEditHisKardex.FieldByName( 'CB_PUESTO' ).AsString;
     end;
end;

procedure TKardexPuesto_DevEx.EscribirCambios;
begin
     with dmRecursos do
     begin
          if ( FOldPuesto = cdsEditHisKardex.FieldByName( 'CB_PUESTO' ).AsString ) or
             ( ( dmCliente.EsPuestoValido( cdsEditHisKardex.FieldByName( 'CB_PUESTO' ).AsString ) ) ) then
          begin
               if zStrToBool( cdsEditHisKardex.FieldByName( 'CB_AUTOSAL' ).AsString ) then
               begin
                    case ZSiNoCancel( 'Cambio de Clasificaci�n', 'Empleado tiene Tabulador de Salario. Desea Generar cambio de Salario ?', 0, mbYes ) of
                         mrYes :
                         begin
                              if ValidaSalario( cdsDatosEmpleado.FieldByName( 'CB_SALARIO' ).AsFloat,
                                                GetSalClasifi( CB_CLASIFI.Llave ) ) then
                              begin
                                   CambiosMultiples:= TRUE;
                                   try
                                      OtrosDatos := VarArrayOf( [ CB_FECHA.Valor, CB_COMENTA.Text, CB_NOTA.Text] );
                                      with cdsDatosEmpleado do
                                      begin
                                           Edit;
                                           FieldByName( 'CB_PUESTO' ).AsString := CB_PUESTO.Llave;
                                           FieldByName( 'CB_CLASIFI' ).AsString := CB_CLASIFI.Llave;
                                           if ( CB_RANGO_S.Visible ) and ( CB_RANGO_S.Enabled ) then
                                              FieldByName( 'CB_RANGO_S' ).AsFloat := CB_RANGO_S.Valor;
                                           Enviar;
                                      end;
                                   finally
                                      CambiosMultiples := FALSE;
                                   end;
                                   cdsEditHisKardex.MergeChangeLog;
                                   RefrescaHisKardex:= TRUE;
                                   RefrescaKardex( K_T_CAMBIO );      // Se posiciona y coloca sobre el Cambio de Salario Originado, adem�s refresca Percepciones
                                   if ( dmRecursos.cdsEditHisKardex.ChangeCount = 0 ) then    //Si se aplicaron los cambios
                                      Close;
                                   //inherited;                       // No se requiere grabar, ya se realiz� el cambio atrav�s de cdsDatosEmpleado
                              end;
                         end;
                         mrNo :
                         begin
                              cdsEditHisKardex.FieldByName( 'CB_AUTOSAL' ).AsString := K_GLOBAL_NO;
                              inherited;
                         end;
                    end;
                    // cancel equivale a no hacer el inherited.....
               end
               else
                   inherited;
          end;
     end;
end;

procedure TKardexPuesto_DevEx.CB_PUESTOExit(Sender: TObject);
var
   sClasifi : String;
begin
     if StrLleno( CB_PUESTO.Llave ) and ( sPuesto <> CB_PUESTO.Llave ) then
     begin
          sPuesto := CB_PUESTO.LLave;
          sClasifi := dmCatalogos.cdsPuestos.FieldByName('PU_CLASIFI').AsString ;
          if StrLleno( sClasifi ) then
             CB_CLASIFI.Llave := sClasifi;
     end;
end;

procedure TKardexPuesto_DevEx.SetMatsushita;
var
   lEnabled: Boolean;
begin
     lEnabled:= ( dmCliente.Matsushita ) and
                ( zStrToBool( dmRecursos.cdsEditHisKardex.FieldByName( 'CB_AUTOSAL' ).AsString ) ) and
                Inserting;
     with dmCliente do
     begin
          with CB_RANGO_SLbl do
          begin
               Visible:= lEnabled;
               Enabled:= FALSE;
          end;
          with CB_RANGO_S do
          begin
               Visible:= lEnabled;
               Enabled:= FALSE;
          end;
     end;
end;

procedure TKardexPuesto_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( dmCliente.Matsushita ) and ( Field <> nil ) and ( ( Field.FieldName = 'CB_PUESTO' ) or ( Field.FieldName = 'CB_CLASIFI' ) ) then
     begin
          CB_RANGO_S.Enabled := CB_RANGO_S.Visible;
          CB_RANGO_SLbl.Enabled := CB_RANGO_S.Enabled;
          if ( Field.FieldName = 'CB_CLASIFI' ) and ( CB_RANGO_S.Visible ) then
             ActiveControl := CB_RANGO_S;
     end;
     if( Field = nil )then
     begin
          FOldPuesto := DataSource.DataSet.FieldByName( 'CB_PUESTO' ).AsString;
     end;
end;

procedure TKardexPuesto_DevEx.SetEditarSoloActivos;
begin
     CB_PUESTO.EditarSoloActivos := TRUE;
     CB_CLASIFI.EditarSoloActivos := TRUE;
end;


end.
