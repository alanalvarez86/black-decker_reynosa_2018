unit FShowAdvVacaPendientes_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ZetaDBTextBox, ZetaCommonClasses, ZetaCommonLists,
  Mask, ZetaNumero, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013, cxButtons, dxGDIPlusClasses;

type

  TShowAdvVacaPendientes_DevEx = class(TForm)
    Mensaje: TLabel;
    Image1: TImage;
    EmpleadoLb: TLabel;
    FechaAntLb: TLabel;
    Label5: TLabel;
    TipoAutLbl: TLabel;
    FechaLbl: TLabel;
    EmpleadoLbl: TLabel;
    FechaIngLb: TLabel;
    gbSaldos: TGroupBox;
    Label1: TLabel;
    ZSaldoGozo: TZetaTextBox;
    Label10: TLabel;
    ZSaldoPago: TZetaTextBox;
    ZSaldoPrima: TZetaTextBox;
    Label11: TLabel;
    ReplaceBtn: TcxButton;
    Ok: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

    function ShowDlgVacacionesPendientes( iEmpleado: TNumEmp; FechaIng, FechaAnt: TDate; rGozo,rPago,rPrima: TPesos ): Boolean;
var
  ShowAdvVacaPendientes_DevEx: TShowAdvVacaPendientes_DevEx;

implementation

uses
    ZetaCommonTools;

{$R *.DFM}

procedure TShowAdvVacaPendientes_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext:= H10153_Reingreso;
end;

function ShowDlgVacacionesPendientes( iEmpleado: TNumEmp; FechaIng, FechaAnt: TDate; rGozo,rPago,rPrima: TPesos ): Boolean;
begin
     if ( ShowAdvVacaPendientes_DevEx = nil ) then
     begin
          ShowAdvVacaPendientes_DevEx := TShowAdvVacaPendientes_DevEx.Create( Application ); // Se destruye al Salir del Programa //
     end;

     with ShowAdvVacaPendientes_DevEx do
     begin
          EmpleadoLb.Caption:= IntToStr(iEmpleado);
          FechaIngLb.Caption:= FechaCorta( FechaIng );
          FechaAntLb.Caption:= FechaCorta( FechaAnt );

          zSaldoGozo.Caption:= FloatToStr(rGozo);
          zSaldoPago.Caption:= FloatToStr(rPago);
          zSaldoPrima.Caption:= FloatToStr(rPrima);

          ShowModal;
          Result:= ( ModalResult = mrOk );
     end;
end;

procedure TShowAdvVacaPendientes_DevEx.FormShow(Sender: TObject);
begin
     Ok.SetFocus;
end;

end.
