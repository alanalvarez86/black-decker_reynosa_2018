inherited KardexBase_DevEx: TKardexBase_DevEx
  Left = 836
  Top = 150
  Caption = 'KardexBase_DevEx'
  ClientHeight = 293
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 257
    TabOrder = 2
    inherited Cancelar_DevEx: TcxButton
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    TabOrder = 4
  end
  object PageControl_DevEx: TcxPageControl [2]
    Left = 0
    Top = 80
    Width = 474
    Height = 177
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = General_DevEx
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 175
    ClientRectLeft = 2
    ClientRectRight = 472
    ClientRectTop = 28
    object General_DevEx: TcxTabSheet
      Caption = 'General'
    end
    object Notas_DevEx: TcxTabSheet
      Caption = 'Observaciones'
      object CB_NOTA: TcxDBMemo
        Left = 0
        Top = 0
        Align = alClient
        DataBinding.DataField = 'CB_NOTA'
        DataBinding.DataSource = DataSource
        TabOrder = 0
        Height = 147
        Width = 470
      end
    end
    object Bitacora_DevEx: TcxTabSheet
      Caption = 'Bit'#225'cora'
      object GCapturoLbl: TLabel
        Left = 138
        Top = 13
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modific'#243':'
      end
      object GCapturaLbl: TLabel
        Left = 126
        Top = 34
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modificado:'
      end
      object GNivelLbl: TLabel
        Left = 154
        Top = 74
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel:'
      end
      object GStatusLbl: TLabel
        Left = 148
        Top = 93
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object CB_FEC_CAP: TZetaDBTextBox
        Left = 192
        Top = 32
        Width = 80
        Height = 17
        AutoSize = False
        Caption = 'CB_FEC_CAP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_FEC_CAP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL: TZetaDBTextBox
        Left = 192
        Top = 72
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label2: TLabel
        Left = 148
        Top = 54
        Width = 33
        Height = 13
        Caption = 'Global:'
      end
      object US_DESCRIP: TZetaDBTextBox
        Left = 192
        Top = 11
        Width = 153
        Height = 17
        AutoSize = False
        Caption = 'US_DESCRIP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_STATUS: TZetaDBTextBox
        Left = 192
        Top = 91
        Width = 153
        Height = 17
        AutoSize = False
        Caption = 'CB_STATUS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_STATUS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_GLOBAL: TDBCheckBox
        Left = 192
        Top = 52
        Width = 58
        Height = 17
        DataField = 'CB_GLOBAL'
        DataSource = DataSource
        Enabled = False
        ReadOnly = True
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
  end
  object PanelFecha: TPanel [4]
    Left = 0
    Top = 50
    Width = 474
    Height = 30
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 13
      Top = 9
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Movimiento:'
    end
    object CB_FECHA: TZetaDBFecha
      Left = 80
      Top = 4
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '10/dic/97'
      Valor = 35774.000000000000000000
      DataField = 'CB_FECHA'
      DataSource = DataSource
    end
    object CB_COMENTA: TDBEdit
      Left = 206
      Top = 5
      Width = 260
      Height = 21
      DataField = 'CB_COMENTA'
      DataSource = DataSource
      MaxLength = 50
      TabOrder = 1
    end
  end
  inherited DataSource: TDataSource
    Left = 428
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ModificarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
