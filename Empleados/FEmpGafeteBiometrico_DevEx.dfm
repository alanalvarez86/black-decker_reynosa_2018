inherited EmpGafeteBiometrico_DevEx: TEmpGafeteBiometrico_DevEx
  Left = 360
  Top = 261
  Caption = 'Gafete y biom'#233'trico'
  ClientHeight = 351
  ClientWidth = 458
  ExplicitWidth = 458
  ExplicitHeight = 351
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 458
    TabOrder = 3
    ExplicitWidth = 458
    inherited Slider: TSplitter
      Left = 323
      ExplicitLeft = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      ExplicitWidth = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 132
      ExplicitLeft = 326
      ExplicitWidth = 132
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 126
        ExplicitLeft = 46
      end
    end
  end
  object GroupBox1: TGroupBox [1]
    Left = 8
    Top = 88
    Width = 377
    Height = 70
    Caption = ' Proximidad '
    TabOrder = 1
    object CB_ID_NUM: TZetaDBTextBox
      Left = 146
      Top = 30
      Width = 190
      Height = 17
      AutoSize = False
      Caption = 'CB_ID_NUM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_ID_NUM'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lblNumeroTarjeta: TLabel
      Left = 68
      Top = 30
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero tarjeta:'
    end
  end
  object GroupBox2: TGroupBox [2]
    Left = 8
    Top = 164
    Width = 377
    Height = 124
    Caption = ' Biom'#233'trico '
    TabOrder = 2
    object CB_ID_BIO: TZetaDBTextBox
      Left = 146
      Top = 21
      Width = 190
      Height = 17
      AutoSize = False
      Caption = 'CB_ID_BIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_ID_BIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lblNumeroBiometrico: TLabel
      Left = 28
      Top = 22
      Width = 112
      Height = 13
      Alignment = taRightJustify
      Caption = 'Identificador biom'#233'trico:'
    end
    object lblCB_GP_COD: TLabel
      Left = 43
      Top = 45
      Width = 97
      Height = 13
      Alignment = taRightJustify
      Caption = 'Grupo de terminales:'
    end
    object CB_GP_COD: TZetaDBTextBox
      Left = 146
      Top = 44
      Width = 190
      Height = 17
      AutoSize = False
      Caption = 'CB_GP_COD'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_GP_COD'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lblCantidad_Huellas: TLabel
      Left = 44
      Top = 68
      Width = 96
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cantidad de huellas:'
    end
    object CANTIDAD_HUELLAS: TZetaDBTextBox
      Left = 146
      Top = 67
      Width = 190
      Height = 17
      AutoSize = False
      Caption = 'CANTIDAD_HUELLAS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object chkTieneHuella: TDBCheckBox
      Left = 32
      Top = 88
      Width = 128
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Reconocimiento facial:'
      ReadOnly = True
      TabOrder = 0
    end
  end
  object GroupBox3: TGroupBox [3]
    Left = 8
    Top = 25
    Width = 377
    Height = 57
    Caption = ' C'#243'digo de barras '
    TabOrder = 0
    object CB_CREDENC: TZetaDBTextBox
      Left = 146
      Top = 23
      Width = 29
      Height = 17
      AutoSize = False
      Caption = 'CB_CREDENC'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CB_CREDENC'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LTipoCreden: TLabel
      Left = 61
      Top = 24
      Width = 79
      Height = 13
      Alignment = taRightJustify
      Caption = 'Letra credencial:'
    end
  end
  inherited DataSource: TDataSource
    Left = 416
    Top = 309
  end
end
