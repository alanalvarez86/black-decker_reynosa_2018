unit FEditEmpAdicionales_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, StdCtrls, DBCtrls, Mask, Db, Buttons, ExtCtrls, ZetaDBTextBox,
  ZetaFecha, ZetaNumero, ZetaKeyLookup, ComCtrls, ZetaSmartLists,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons, cxScrollBar,
  dxSkinsDefaultPainters;

type
  TEditEmpAdicionales_DevEx = class(TBaseEdicion_DevEx)
    PageControl: TcxPageControl;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    function GetFirstControlAdicional( oControl: TWinControl ): TWinControl;
    //tcPrimerControl : TWinControl;
    //iLleno : Integer;
    //procedure SetCampo( sValor: String; oLabel: TLabel; oControl: TWinControl ) ;
  protected
    procedure Connect; override;
    procedure ImprimirForma;override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;

  public
  end;

{
const
     K_LIMITE = 9;
}
var
  EditEmpAdicionales_DevEx: TEditEmpAdicionales_DevEx;

implementation

{$R *.DFM}

uses dRecursos, dTablas, {dGlobal,} dSistema, ZAccesosTress,
     {ZGlobalTress,} ZImprimeForma,ZetaTipoEntidad,
     ZetaCommonClasses, ZetaCommonTools, ZetaCommonLists ;

procedure TEditEmpAdicionales_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_EMP_DATOS_ADICION;
     HelpContext:= H10116_Datos_adicionales_empleado;
     TipoValorActivo1 := stEmpleado;
     DataSource.DataSet:= dmRecursos.cdsDatosEmpleado;
     dmSistema.CamposAdic.ContruyeFormaEdicion( dmSistema.Clasificaciones, self.DataSource, self, PageControl,
                                                dmTablas.GetEntidadAdicionalConnect, TRUE );
{
     CB_G_TAB_1.LookupDataset := dmTablas.cdsExtra1;
     CB_G_TAB_2.LookupDataset := dmTablas.cdsExtra2;
     CB_G_TAB_3.LookupDataset := dmTablas.cdsExtra3;
     CB_G_TAB_4.LookupDataset := dmTablas.cdsExtra4;
}
end;

procedure TEditEmpAdicionales_DevEx.FormShow(Sender: TObject);
{
var
   sGlobal : String;
}
begin
{
     iLLeno := 0;
     with Global do
     begin
          SetCampo( GetGlobalString( K_GLOBAL_TEXTO1 ), TextAdicLbl1, CB_G_TEX_1 );
          SetCampo( GetGlobalString( K_GLOBAL_TEXTO2 ), TextAdicLbl2, CB_G_TEX_2 );
          SetCampo( GetGlobalString( K_GLOBAL_TEXTO3 ), TextAdicLbl3, CB_G_TEX_3 );
          SetCampo( GetGlobalString( K_GLOBAL_TEXTO4 ), TextAdicLbl4, CB_G_TEX_4 );
          SetCampo( GetGlobalString( K_GLOBAL_TAB1 ), TablaAdicLbl1, CB_G_TAB_1 );
          SetCampo( GetGlobalString( K_GLOBAL_TAB2 ), TablaAdicLbl2, CB_G_TAB_2 );
          SetCampo( GetGlobalString( K_GLOBAL_TAB3 ), TablaAdicLbl3, CB_G_TAB_3 );
          SetCampo( GetGlobalString( K_GLOBAL_TAB4 ), TablaAdicLbl4, CB_G_TAB_4 );
          SetCampo( GetGlobalString( K_GLOBAL_NUM1 ), NumAdicLbl1, CB_G_NUM_1 );
          SetCampo( GetGlobalString( K_GLOBAL_NUM2 ), NumAdicLbl2, CB_G_NUM_2 );
          SetCampo( GetGlobalString( K_GLOBAL_NUM3 ), NumAdicLbl3, CB_G_NUM_3 );
          SetCampo( GetGlobalString( K_GLOBAL_FECHA1 ), FechaAdicLbl1, CB_G_FEC_1 );
          SetCampo( GetGlobalString( K_GLOBAL_FECHA2 ), FechaAdicLbl2, CB_G_FEC_2 );
          SetCampo( GetGlobalString( K_GLOBAL_FECHA3 ), FechaAdicLbl3, CB_G_FEC_3 );
          // Adicionales L�gicos
          sGlobal := Global.GetGlobalString( K_GLOBAL_LOG1 );
          SetCampo( sGlobal, BoolAdicLbl1, CB_G_LOG_1 );
          CB_G_LOG_1.Caption := sGlobal;
          sGlobal := Global.GetGlobalString( K_GLOBAL_LOG2 );
          SetCampo( sGlobal, BoolAdicLbl2, CB_G_LOG_2 );
          CB_G_LOG_2.Caption := sGlobal;
          sGlobal := Global.GetGlobalString( K_GLOBAL_LOG3 );
          SetCampo( sGlobal, BoolAdicLbl3, CB_G_LOG_3 );
          CB_G_LOG_3.Caption := sGlobal;
     end;
     BoolAdicLbl1.Visible := False;
     BoolAdicLbl2.Visible := False;
     BoolAdicLbl3.Visible := False;
     TabAdicionales2.TabVisible := ( iLLeno > K_LIMITE );
     TabAdicionales2.Visible    := ( iLLeno > K_LIMITE );
     TabAdicionales1.TabVisible := ( iLLeno > K_LIMITE );
}
     PageControl.ActivePageIndex := 0; // Primer tabsheet
     FirstControl := GetFirstControlAdicional( PageControl.ActivePage );
     inherited;
     //El derecho de modificar de esta forma se controla de otra manera.
     Datasource.AutoEdit := TRUE;
end;

procedure TEditEmpAdicionales_DevEx.Connect;
begin
     with dmRecursos do
     begin
{
          if CB_G_TAB_1.Visible then cdsExtra1.Conectar;
          if CB_G_TAB_2.Visible then cdsExtra2.Conectar;
          if CB_G_TAB_3.Visible then cdsExtra3.Conectar;
          if CB_G_TAB_4.Visible then cdsExtra4.Conectar;
}
          DataSource.DataSet:= cdsDatosEmpleado;
     end;
end;

function TEditEmpAdicionales_DevEx.GetFirstControlAdicional( oControl: TWinControl ): TWinControl;
var
   i : Integer;
   oControlHijo : TWinControl;
   lOk : Boolean;
begin
     Result := nil;
     for i:= 0 to oControl.ControlCount - 1 do
     begin
          if ( oControl.Controls[i] is TWinControl ) then  // Solo controles que hereden de TWinControl
          begin
               oControlHijo := TWinControl( oControl.Controls[i] );
               with oControlHijo do
               begin
                    lOk := ( ClassType = TDBEdit ) or ( ClassType = TZetaDBNumero ) or
                           ( ClassType = TDBCheckBox ) or ( ClassType = TZetaDBFecha ) or
                           ( ClassType = TZetaDBKeyLookup );
               end;
               if lOk then
               begin
                    Result := oControlHijo;
                    Break;                  // Solo el primer control de ese tipo
               end
               else
               begin
                    if ( oControlHijo.ControlCount > 0 ) then
                       Result := GetFirstControlAdicional( oControlHijo );
               end;
          end;
     end;
end;

{
function TEditEmpAdicionales.GetFirstControlAdicional( const oTab: TTabSheet ): TWinControl;
var
   i, j : Integer;
   oControl: TWinControl;
   lOk : Boolean;
begin
     Result := nil;
     for i := 0 to ( oTab.ControlCount - 1 ) do
     begin
          if ( oTab.Controls[i].ClassType = TScrollBox ) then    // Quedan dentro de un TScrollBox
          begin
               oControl := TWinControl( oTab.Controls[i] );
               for j:= 0 to oControl.ControlCount - 1 do
               begin
                    with oControl.Controls[j] do
                    begin
                         lOk := ( ClassType = TDBEdit ) or ( ClassType = TZetaDBNumero ) or
                                ( ClassType = TDBCheckBox ) or ( ClassType = TZetaDBFecha ) or
                                ( ClassType = TZetaDBKeyLookup );
                    end;
                    if lOk then
                    begin
                         Result := TWinControl( Controls[j] );
                         Break;
                    end;
               end;
               Break;
          end;
     end;
end;
}

procedure TEditEmpAdicionales_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     Close;
end;

procedure TEditEmpAdicionales_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

{
procedure TEditEmpAdicionales.SetCampo( sValor: String; oLabel: TLabel; oControl: TWinControl ) ;
const
     //K_LAB_LEFT = 178;
     //K_CTRL_LEFT = 190;
     K_LAB_LEFT = 225;
     K_CTRL_LEFT = 230;
begin
     if StrLleno( sValor ) then
     begin
          with oLabel do
          begin
               Caption := ' ';
               Left := K_LAB_LEFT;
               Caption := Trim( sValor ) + ':';
          end;
          oControl.Left := K_CTRL_LEFT;
          if iLleno < K_LIMITE then
          begin
               PageControl.ActivePage := TabAdicionales1;
               with oControl do
               begin
                    Parent := TabAdicionales1;
                    Top  := 5 + ( iLleno * 24 );
               end;
               oLabel.Parent := TabAdicionales1;
               if iLleno = 0 then
                    tcPrimerControl := oControl;
          end
          else
          begin
               PageControl.ActivePage := TabAdicionales2;
               oControl.Parent := TabAdicionales2;
               oLabel.Parent := TabAdicionales2;
               oControl.Top   := 5 + ( ( iLleno - K_LIMITE ) * 24 );
          end;
          oLabel.Top := oControl.Top + 4;
          oLabel.Visible   := True;
          oControl.Visible := True;
          iLLeno := iLLeno + 1;
     end
     else
     begin
          oControl.Visible := False;
          oLabel.Visible := False;
     end;
end;
}

function TEditEmpAdicionales_DevEx.PuedeImprimir(var sMensaje:String):Boolean;
begin
     Result := IntToBool( PageControl.ActivePage.Tag - 1 );
     sMensaje := Format ('No tiene permisos para imprimir %s ',[ PageControl.ActivePage.Caption ]);
end;

end.







