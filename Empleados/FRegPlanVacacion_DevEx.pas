unit FRegPlanVacacion_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZetaDBTextBox, StdCtrls, DBCtrls, Mask, ZetaFecha,
  DB, ExtCtrls, ZetaSmartLists, Buttons, ZBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxDBEdit;

type
  TRegPlanVacacion_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    Panel2: TPanel;
    LVA_VAC_DEL: TLabel;
    LblGozados: TLabel;
    LVA_VAC_AL: TLabel;
    VP_FEC_INI: TZetaDBFecha;
    VP_FEC_FIN: TZetaDBFecha;
    gbSaldos: TGroupBox;
    Label5: TLabel;
    Label10: TLabel;
    GroupBox1: TGroupBox;
    VP_SOL_COM: TcxDBMemo;
    VP_DIAS: TZetaDBTextBox;
    VP_SAL_ANT: TZetaDBTextBox;
    VP_SAL_PRO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure Connect; override;
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RegPlanVacacion_DevEx: TRegPlanVacacion_DevEx;

implementation

{$R *.dfm}
uses DCliente, {$ifdef SUPERVISORES}dSuper{$else}dRecursos{$endif}, ZetaCommonClasses, ZAccesosTress, ZetaClientDataSet;


{ TEditPlanVacacion }

procedure TRegPlanVacacion_DevEx.FormCreate(Sender: TObject);
begin
     {$ifdef SUPERVISORES}
     IndexDerechos := ZAccesosTress.D_SUPER_PLANVACACION;
     HelpContext := H_PlanVacacion_Super;
     {$else}
     IndexDerechos := ZAccesosTress.D_CONS_PLANVACACION;
     HelpContext:= H_PlanVacacion;
     {$endif}
     FirstControl := CB_CODIGO;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookup;
end;

procedure TRegPlanVacacion_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     {$ifdef SUPERVISORES}
     with dmSuper do
     {$else}
     with dmRecursos do
     {$endif}
     DataSource.Dataset := cdsPlanVacacion;
end;


procedure TRegPlanVacacion_DevEx.OKClick(Sender: TObject);
begin
  inherited;
  if ( TZetaClientDataSet( DataSource.DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
        ( not ( DataSource.DataSet.State in [ dsEdit, dsInsert ] ) ) then    //y no se tienen cambios pendientes
        Close;
end;

end.
