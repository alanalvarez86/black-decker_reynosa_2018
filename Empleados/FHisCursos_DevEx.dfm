inherited HisCursos_DevEx: THisCursos_DevEx
  Left = 190
  Top = 223
  Caption = 'Historial de Cursos Tomados'
  ClientWidth = 640
  ExplicitWidth = 640
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 640
    ExplicitWidth = 640
    inherited Slider: TSplitter
      Left = 221
      ExplicitLeft = 221
    end
    inherited ValorActivo1: TPanel
      Width = 205
      ExplicitWidth = 205
      inherited textoValorActivo1: TLabel
        Width = 199
      end
    end
    inherited ValorActivo2: TPanel
      Left = 224
      Width = 416
      ExplicitLeft = 224
      ExplicitWidth = 416
      inherited textoValorActivo2: TLabel
        Width = 410
        ExplicitLeft = 330
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 640
    ExplicitWidth = 640
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object KC_FEC_TOM: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'KC_FEC_TOM'
        MinWidth = 75
      end
      object CU_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CU_CODIGO'
        MinWidth = 70
      end
      object CU_NOMBRE: TcxGridDBColumn
        Caption = 'Curso'
        DataBinding.FieldName = 'CU_NOMBRE'
        MinWidth = 75
      end
      object KC_REVISIO: TcxGridDBColumn
        Caption = 'Revisi'#243'n'
        DataBinding.FieldName = 'KC_REVISIO'
        MinWidth = 80
      end
      object KC_HORAS: TcxGridDBColumn
        Caption = 'Horas'
        DataBinding.FieldName = 'KC_HORAS'
        MinWidth = 70
      end
      object KC_EVALUA: TcxGridDBColumn
        Caption = 'Evaluaci'#243'n'
        DataBinding.FieldName = 'KC_EVALUA'
        MinWidth = 85
      end
      object SE_FOLIO: TcxGridDBColumn
        Caption = 'Grupo'
        DataBinding.FieldName = 'SE_FOLIO'
        MinWidth = 75
      end
    end
  end
  inherited DataSource: TDataSource
    Top = 208
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
