inherited VerHistorialMatrizCursos_DevEx: TVerHistorialMatrizCursos_DevEx
  Left = 553
  Top = 291
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Consulta de Historial'
  ClientHeight = 273
  ClientWidth = 541
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  ExplicitWidth = 547
  ExplicitHeight = 302
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 237
    Width = 541
    ExplicitTop = 237
    ExplicitWidth = 541
    inherited Cancelar_DevEx: TcxButton [0]
      Left = 454
      Visible = False
      ParentFont = False
      OnClick = Cancelar_DevExClick
      ExplicitLeft = 454
    end
    inherited OK_DevEx: TcxButton [1]
      Left = 460
      Cancel = True
      ExplicitLeft = 460
    end
  end
  object Panel2: TPanel [1]
    Left = 0
    Top = 20
    Width = 541
    Height = 217
    Align = alClient
    TabOrder = 1
    ExplicitTop = 33
    ExplicitHeight = 204
    object GridRenglones: TZetaDBGrid
      Left = 1
      Top = 1
      Width = 539
      Height = 215
      Align = alClient
      DataSource = dsEvaluacionMult
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'KC_FEC_TOM'
          Title.Caption = 'Fecha'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KC_REVISIO'
          Title.Caption = 'Revisi'#243'n'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KC_HORAS'
          Title.Caption = 'Horas'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KC_EVALUA'
          Title.Caption = 'Evaluaci'#243'n'
          Width = 80
          Visible = True
        end>
    end
  end
  object PanelIdentifica: TPanel [2]
    Left = 0
    Top = 0
    Width = 541
    Height = 20
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Splitter: TSplitter
      Left = 249
      Top = 0
      Height = 20
      ExplicitLeft = 323
      ExplicitHeight = 19
    end
    object ValorActivo1: TPanel
      Left = 0
      Top = 0
      Width = 249
      Height = 20
      Align = alLeft
      Alignment = taLeftJustify
      BorderWidth = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object textoValorActivo1: TLabel
        Left = 3
        Top = 3
        Width = 243
        Height = 13
        Align = alTop
        Caption = 'textoValorActivo1'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        WordWrap = True
        ExplicitWidth = 83
      end
    end
    object ValorActivo2: TPanel
      Left = 252
      Top = 0
      Width = 289
      Height = 20
      Align = alClient
      Alignment = taRightJustify
      BorderWidth = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      ExplicitLeft = 284
      ExplicitWidth = 257
      ExplicitHeight = 33
      object textoValorActivo2: TLabel
        Left = 3
        Top = 3
        Width = 283
        Height = 13
        Align = alTop
        Alignment = taRightJustify
        Caption = 'textoValorActivo2'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        WordWrap = True
        ExplicitLeft = 171
        ExplicitWidth = 83
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object dsEvaluacionMult: TDataSource
    AutoEdit = False
    Left = 352
    Top = 120
  end
  object cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 6291544
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000002F3E40006D
          8F9300202B2C0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000003E515400BD
          F8FF00B0E8EF002C3A3C000000000000000000000000000000000070939700A4
          D9DF00A4D9DF00A4D9DF00A4D9DF00A4D9DF00A4D9DF00A4D9DF006D8F9300A1
          D5DB00A4D9DF00B9F3FB0026323400000000000000000000000000BDF8FF00BD
          F8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00A7DDE3006D
          8F9300BDF8FF00BDF8FF00A4D9DF000C10100000000000000000007CA2A700BD
          F8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF0076
          9B9F00A4D9DF00BDF8FF00BDF8FF007CA2A70000000000000000002C3A3C0047
          5D6000475D6000475D6000475D6000475D6000475D6000475D6000475D600047
          5D60004D656800B9F3FB00BDF8FF00BDF8FF004D65680000000000ADE5EB00BD
          F8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00B0E8EF0066888B00BDF8FF00BDF8FF00B3EBF300202B2C00AAE1E700BD
          F8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00BDF8FF007CA2A70085ADB300BDF8FF00BDF8FF0099C9CF00232F300047
          5D6000475D6000475D6000475D6000475D6000475D6000475D6000475D600047
          5D6000475D60001B2324000F131400A7DDE300B9F3FB00567074008BB6BB00BD
          F8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00BDF8FF007CA2A70000000000263234002936380000000000B9F3FB00BD
          F8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00BDF8FF00A4D9DF00000000000000000000000000000000005F7C80008E
          BABF008EBABF008EBABF008EBABF008EBABF008EBABF008EBABF008EBABF008E
          BABF008EBABF00536D7000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end>
  end
end
