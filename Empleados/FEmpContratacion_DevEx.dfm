inherited EmpContratacion_DevEx: TEmpContratacion_DevEx
  Left = 365
  Top = 199
  Caption = 'Contrataci'#243'n'
  ClientHeight = 252
  ClientWidth = 481
  PixelsPerInch = 96
  TextHeight = 13
  object CB_FEC_INGlbl: TLabel [0]
    Left = 58
    Top = 27
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingreso:'
  end
  object CB_CONTRATlbl: TLabel [1]
    Left = 14
    Top = 84
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Contrato:'
  end
  object CB_FEC_CONlbl: TLabel [2]
    Left = 10
    Top = 65
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Inicio de Contrato:'
  end
  object CB_PUESTOlbl: TLabel [3]
    Left = 60
    Top = 120
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Puesto:'
  end
  object CB_CLASIFIlbl: TLabel [4]
    Left = 34
    Top = 158
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'Clasificaci'#243'n:'
  end
  object CB_HORARIOlbl: TLabel [5]
    Left = 65
    Top = 177
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Turno:'
  end
  object CB_FEC_ING: TZetaDBTextBox [6]
    Left = 104
    Top = 25
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_FEC_ING'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_FEC_ING'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object ZCONTRATO: TZetaTextBox [7]
    Left = 188
    Top = 82
    Width = 235
    Height = 17
    AutoSize = False
    Caption = 'ZCONTRATO'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object CB_FEC_CON: TZetaDBTextBox [8]
    Left = 104
    Top = 62
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_FEC_CON'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_FEC_CON'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object ZPUESTO: TZetaTextBox [9]
    Left = 188
    Top = 118
    Width = 235
    Height = 17
    AutoSize = False
    Caption = 'ZPUESTO'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object ZCLASIFICACION: TZetaTextBox [10]
    Left = 188
    Top = 156
    Width = 235
    Height = 17
    AutoSize = False
    Caption = 'ZCLASIFICACION'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object ZTURNO: TZetaTextBox [11]
    Left = 188
    Top = 175
    Width = 235
    Height = 17
    AutoSize = False
    Caption = 'ZTURNO'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object LVencimiento: TLabel [12]
    Left = 35
    Top = 103
    Width = 61
    Height = 13
    Caption = 'Vencimiento:'
  end
  object Label1: TLabel [13]
    Left = 7
    Top = 46
    Width = 89
    Height = 13
    Caption = 'Antig'#252'edad desde:'
  end
  object CB_FEC_ANT: TZetaDBTextBox [14]
    Left = 104
    Top = 43
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_FEC_ANT'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CB_FEC_ANT'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label2: TLabel [15]
    Left = 12
    Top = 196
    Width = 84
    Height = 13
    Caption = 'Registro Patronal:'
  end
  object ZPATRON: TZetaTextBox [16]
    Left = 188
    Top = 194
    Width = 235
    Height = 17
    AutoSize = False
    Caption = 'ZPATRON'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object ZAntiguedad: TZetaTextBox [17]
    Left = 188
    Top = 44
    Width = 151
    Height = 17
    AutoSize = False
    Caption = 'ZAntiguedad'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object CB_CONTRAT: TZetaDBTextBox [18]
    Left = 104
    Top = 81
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_CONTRAT'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_CONTRAT'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_PUESTO: TZetaDBTextBox [19]
    Left = 104
    Top = 118
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_PUESTO'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_PUESTO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_CLASIFI: TZetaDBTextBox [20]
    Left = 104
    Top = 156
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_CLASIFI'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_CLASIFI'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_TURNO: TZetaDBTextBox [21]
    Left = 104
    Top = 175
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_TURNO'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_TURNO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_PATRON: TZetaDBTextBox [22]
    Left = 104
    Top = 194
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_PATRON'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_PATRON'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label3: TLabel [23]
    Left = 67
    Top = 139
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Plaza:'
  end
  object CB_PLAZA: TZetaDBTextBox [24]
    Left = 104
    Top = 137
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_PLAZA'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_PLAZA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object ZPlaza: TZetaTextBox [25]
    Left = 188
    Top = 137
    Width = 235
    Height = 17
    AutoSize = False
    Caption = 'ZPLaza'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object Label4: TLabel [26]
    Left = 18
    Top = 215
    Width = 78
    Height = 13
    Caption = 'Tipo de N'#243'mina:'
  end
  object CB_NOMINA: TZetaDBTextBox [27]
    Left = 104
    Top = 213
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_NOMINA'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NOMINA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object ZNOMINA: TZetaTextBox [28]
    Left = 188
    Top = 213
    Width = 235
    Height = 17
    AutoSize = False
    Caption = 'ZNOMINA'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object CB_FEC_COV: TZetaDBTextBox [29]
    Left = 104
    Top = 100
    Width = 81
    Height = 17
    AutoSize = False
    Caption = 'CB_FEC_COV'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CB_FEC_COV'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object bbMostrarCalendario: TcxButton [30]
    Left = 425
    Top = 172
    Width = 22
    Height = 22
    Hint = 'Mostrar Calendario del Turno'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = bbMostrarCalendarioClick
    OptionsImage.Glyph.Data = {
      76060000424D7606000000000000360000002800000014000000140000000100
      2000000000004006000000000000000000000000000000000000D8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFEEDBC9FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFF3E6D9FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFFAF4EFFFFFFFFFFFF0DFCFFFF0DFCFFFF2E3D5FFF9F3EDFFF0DF
      CFFFF0DFCFFFF8EFE7FFF2E3D5FFF0DFCFFFF0DFCFFFFDFBF9FFFFFFFFFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFD8AA
      7FFFD8AA7FFFDDB58FFFF0DFCFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
      7FFFD8AA7FFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFFAF4EFFFFFFFFFFFE2BF9FFFE2BF9FFFE5C7ABFFF4E7DBFFE2BF
      9FFFE2BF9FFFF0DFCFFFE5C7ABFFE2BF9FFFE2BF9FFFFBF7F3FFFFFFFFFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFECD5
      BFFFECD5BFFFEEDAC7FFF8EFE7FFECD5BFFFECD5BFFFF5EADFFFEEDAC7FFECD5
      BFFFECD5BFFFFDFAF7FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFDDB58FFFF0DFCFFFD8AA
      7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFECD5
      BFFFECD5BFFFEEDAC7FFF8EFE7FFECD5BFFFECD5BFFFF5EADFFFEEDAC7FFECD5
      BFFFECD5BFFFFDFAF7FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFFAF4EFFFFFFFFFFFE2BF9FFFE2BF9FFFE5C7ABFFF4E7DBFFE2BF
      9FFFE2BF9FFFF0DFCFFFE5C7ABFFE2BF9FFFE2BF9FFFFBF7F3FFFFFFFFFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFD8AA
      7FFFD8AA7FFFDDB58FFFF0DFCFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
      7FFFD8AA7FFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFFAF4EFFFFFFFFFFFF0DFCFFFF0DFCFFFF2E3D5FFF9F3EDFFF0DF
      CFFFF0DFCFFFF8EFE7FFF2E3D5FFF0DFCFFFF0DFCFFFFDFBF9FFFFFFFFFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFCF8
      F5FFFAF4EFFFFAF4EFFFFDFAF7FFFFFFFFFFFFFFFFFFFDFBF9FFFAF4EFFFFAF4
      EFFFFCF8F5FFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFFAF4EFFFFFFFFFFFE7CAAFFFE3C2A3FFE3C2A3FFECD5BFFFFFFF
      FFFFFFFFFFFFF0DFCFFFE1BE9DFFE5C6A9FFE7CAAFFFFFFFFFFFFFFFFFFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFEFDCCBFFFFFFFFFFE7CA
      AFFFF5EADFFFF5EADFFFECD5BFFFFFFFFFFFFFFFFFFFF0DFCFFFF0DFCFFFFAF4
      EFFFE7CAAFFFFFFFFFFFF4E7DBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFDCB38DFFDAAE85FFECD5BFFFEEDBC9FFDAAF87FFDDB5
      8FFFDDB58FFFDBB189FFE9CEB5FFF3E4D7FFDAAE85FFDDB58FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
      7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
  end
  inherited PanelIdentifica: TPanel
    Width = 481
    inherited Slider: TSplitter
      Left = 402
    end
    inherited ValorActivo1: TPanel
      Width = 386
      inherited textoValorActivo1: TLabel
        Width = 380
      end
    end
    inherited ValorActivo2: TPanel
      Left = 405
      Width = 76
      inherited textoValorActivo2: TLabel
        Width = 70
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 220
    Top = 8
  end
end
