unit FProcEmpleado;
                  
interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, ToolWin, Db, ExtCtrls, ImgList,
     ZBaseConsulta,
     ZBaseConsultaBotones;

type
  TProcEmpleado = class(TBaseBotones)
    CalcularIntegrados: TToolButton;
    PromediarVariables: TToolButton;
    CambioSalario: TToolButton;
    VacacionesGlobales: TToolButton;
    CancelarVacaciones: TToolButton;
    AplicarTabulador: TToolButton;
    AplicarEventos: TToolButton;
    ImportarKardex: TToolButton;
    CancelarKardex: TToolButton;
    CursoGlobal: TToolButton;
    Transferencia: TToolButton;
    CierreGlobalVacaciones: TToolButton;
    EntregarHerramienta: TToolButton;
    RegresarHerramienta: TToolButton;
    ImportarAltas: TToolButton;
    RenumerarEmpleados: TToolButton;
    PermisosGlobales: TToolButton;
    BorrarCursoTomadoGlobal: TToolButton;
    ComidasGrupales: TToolButton;
    CorrigeFechasGlobales: TToolButton;
    ImportarAsistenciaSesiones: TToolButton;
    CancelarCierreVacaciones: TToolButton;
    CancelarPermisos: TToolButton;
    Importar_Aho_Pre: TToolButton;
    RecalcularSaldoVacaciones: TToolButton;
    Curso_Prog_Global: TToolButton;
    Cancelar_Curso_Prog_Global: TToolButton;
    FoliarCapacitacionesSTPS: TToolButton;
    RenovarSeguroGastosMedicos: TToolButton;
    BorrarSeguroGastosMedicos: TToolButton;
    ImportarSeguroGastosMedicos: TToolButton;
    ImportarOrganigrama: TToolButton;
    RevisarDatosSTPS: TToolButton;
    ReiniciarFolioCapacitacionesSTPS: TToolButton;
    ImportarImagenes: TToolButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ProcEmpleado: TProcEmpleado;

implementation

{$R *.DFM}

uses FTressShell,
     ZetaCommonClasses;

{ TProcEmpleado }

procedure TProcEmpleado.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H10016_Procesos_empleados;
end;

end.
