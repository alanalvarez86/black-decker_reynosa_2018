unit FCandidatosGridShow_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls,
  ExtCtrls, ZBaseGridShow_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid, cxButtons, cxCalendar;

type
  TCandidatosGridShow_DevEx = class(TBaseGridShow_DevEx)
    SO_FOLIO: TcxGridDBColumn;
    SO_APE_PAT: TcxGridDBColumn;
    SO_APE_MAT: TcxGridDBColumn;
    SO_NOMBRES: TcxGridDBColumn;
    SO_FEC_NAC: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CandidatosGridShow_DevEx: TCandidatosGridShow_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

procedure TCandidatosGridShow_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_Lista_de_Candidatos;
end;

procedure TCandidatosGridShow_DevEx.OKClick(Sender: TObject);
const
     K_YA_CONTRATADO = 'No Se Puede Seleccionar Este Candidato' + CR_LF +
                       'Ya Fu� Contratado Con El N�mero: %d';
var
   iEmpleado: Integer;
begin
     inherited;
     iEmpleado := Dataset.FieldByName( 'CB_CODIGO' ).AsInteger;
     if ( iEmpleado > 0 ) then
        ZetaDialogo.ZError( self.Caption, Format( K_YA_CONTRATADO, [ iEmpleado ] ), 0 )
     else
        ModalResult := mrOk;   // Cierra la forma
end;

procedure TCandidatosGridShow_DevEx.ZetaDBGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   iValor: Integer;
begin

  inherited;
   iValor := StrToInt ( AViewInfo.GridRecord.DisplayTexts[CB_CODIGO.Index]);
   if ( AViewInfo.Selected ) then
        ACanvas.Font.Color := Font.Color
   else
   begin
        if ( iValor > 0  ) then
           ACanvas.Font.Color := clRed
        else
            ACanvas.Font.Color := Font.Color;
   end;

end;

procedure TCandidatosGridShow_DevEx.ZetaDBGridDBTableViewDblClick(
  Sender: TObject);
begin
  inherited;
  OK_DevEx.SetFocus;
  OK_DevEx.Click;
end;

procedure TCandidatosGridShow_DevEx.ZetaDBGridDBTableViewKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
          if OK_DevEx.Visible then
          begin
               OK_DevEx.SetFocus;
               OK_DevEx.Click;
          end;
  end;
end;

end.
