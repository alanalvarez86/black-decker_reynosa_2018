inherited EmpAlta: TEmpAlta
  Left = 2282
  Top = 220
  Caption = 'Alta de Empleado'
  ClientHeight = 639
  ClientWidth = 621
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 603
    Width = 621
    TabOrder = 1
    inherited OK: TBitBtn
      Left = 453
    end
    inherited Cancelar: TBitBtn
      Left = 538
    end
  end
  inherited PanelSuperior: TPanel
    Width = 621
    TabOrder = 2
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 621
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 295
      inherited textoValorActivo2: TLabel
        Width = 289
      end
    end
  end
  object PageControl: TPageControl [3]
    Left = 76
    Top = 51
    Width = 545
    Height = 552
    ActivePage = TabOtros
    Align = alClient
    TabOrder = 3
    object TabIdentifica: TTabSheet
      Caption = 'Identificaci'#243'n'
      TabVisible = False
      object LCB_APE_PAT: TLabel
        Left = 26
        Top = 32
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Apellido Paterno:'
      end
      object LCB_APE_MAT: TLabel
        Left = 24
        Top = 55
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Apellido Materno:'
      end
      object LCB_NOMBRES: TLabel
        Left = 55
        Top = 77
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre(s):'
      end
      object LCB_FEC_NAC: TLabel
        Left = 17
        Top = 124
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha Nacimiento:'
      end
      object LRFC: TLabel
        Left = 73
        Top = 168
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'R.F.C.:'
      end
      object LCB_SEGSOC: TLabel
        Left = 27
        Top = 191
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = '# Seguro Social:'
      end
      object LCB_CURP: TLabel
        Left = 61
        Top = 214
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'C.U.R.P.:'
      end
      object lblEmpleado: TLabel
        Left = 66
        Top = 9
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object lblSexo: TLabel
        Left = 79
        Top = 100
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = 'Sexo:'
      end
      object bDigitoVerificador: TSpeedButton
        Left = 241
        Top = 185
        Width = 25
        Height = 25
        Hint = 'Calcula D'#237'gito Verificador'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00337000000000
          73333337777777773F333308888888880333337F3F3F3FFF7F33330808089998
          0333337F737377737F333308888888880333337F3F3F3F3F7F33330808080808
          0333337F737373737F333308888888880333337F3F3F3F3F7F33330808080808
          0333337F737373737F333308888888880333337F3F3F3F3F7F33330808080808
          0333337F737373737F333308888888880333337F3FFFFFFF7F33330800000008
          0333337F7777777F7F333308000E0E080333337F7FFFFF7F7F33330800000008
          0333337F777777737F333308888888880333337F333333337F33330888888888
          03333373FFFFFFFF733333700000000073333337777777773333}
        NumGlyphs = 2
        OnClick = bDigitoVerificadorClick
      end
      object LEdad: TLabel
        Left = 248
        Top = 124
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Edad:'
      end
      object ZEdad: TZetaTextBox
        Left = 285
        Top = 122
        Width = 145
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object lblEntidad: TLabel
        Left = 11
        Top = 146
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Entidad Nacimiento:'
      end
      object CB_APE_PAT: TDBEdit
        Left = 116
        Top = 28
        Width = 220
        Height = 21
        DataField = 'CB_APE_PAT'
        DataSource = DataSource
        TabOrder = 1
        OnChange = PersonalesChange
      end
      object CB_APE_MAT: TDBEdit
        Left = 116
        Top = 51
        Width = 220
        Height = 21
        DataField = 'CB_APE_MAT'
        DataSource = DataSource
        TabOrder = 2
        OnChange = PersonalesChange
      end
      object CB_NOMBRES: TDBEdit
        Left = 116
        Top = 73
        Width = 220
        Height = 21
        DataField = 'CB_NOMBRES'
        DataSource = DataSource
        TabOrder = 3
        OnChange = PersonalesChange
      end
      object CB_CURP: TDBEdit
        Left = 116
        Top = 210
        Width = 190
        Height = 21
        CharCase = ecUpperCase
        DataField = 'CB_CURP'
        DataSource = DataSource
        TabOrder = 9
        OnChange = CB_RFCChange
      end
      object CB_CODIGO: TZetaDBNumero
        Left = 116
        Top = 5
        Width = 80
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 0
        DataField = 'CB_CODIGO'
        DataSource = DataSource
      end
      object CB_FEC_NAC: TZetaDBFecha
        Left = 116
        Top = 119
        Width = 121
        Height = 22
        Cursor = crArrow
        Opcional = True
        TabOrder = 5
        Text = '18/dic/97'
        Valor = 35782.000000000000000000
        OnExit = CB_FEC_NACExit
        OnValidDate = PersonalesChange
        DataField = 'CB_FEC_NAC'
        DataSource = DataSource
      end
      object CB_RFC: TDBEdit
        Left = 116
        Top = 164
        Width = 121
        Height = 21
        CharCase = ecUpperCase
        DataField = 'CB_RFC'
        DataSource = DataSource
        TabOrder = 7
        OnChange = CB_RFCChange
      end
      object CB_SEGSOC: TDBEdit
        Left = 116
        Top = 187
        Width = 121
        Height = 21
        DataField = 'CB_SEGSOC'
        DataSource = DataSource
        TabOrder = 8
        OnChange = CB_RFCChange
      end
      object Panel1: TPanel
        Left = 344
        Top = 192
        Width = 100
        Height = 41
        BevelOuter = bvNone
        TabOrder = 10
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object CB_SEXO: TZetaKeyCombo
        Left = 116
        Top = 96
        Width = 121
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 0
        ParentCtl3D = False
        TabOrder = 4
        OnChange = CB_SEXOChange
        ListaFija = lfSexoDesc
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object BtnCandidato: TBitBtn
        Left = 360
        Top = 36
        Width = 95
        Height = 43
        Hint = 'Buscar Candidato por Nombre'
        Caption = 'Buscar Candidato'
        TabOrder = 11
        OnClick = BtnCandidatoClick
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
          DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
          8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
          C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
          7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
        Layout = blGlyphTop
      end
      object CB_ENT_NAC: TZetaDBKeyLookup
        Left = 116
        Top = 142
        Width = 316
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 45
        DataField = 'CB_ENT_NAC'
        DataSource = DataSource
      end
    end
    object TabPersonales: TTabSheet
      Caption = 'Personales'
      TabVisible = False
      object LLugNac: TLabel
        Left = 34
        Top = 12
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Lugar Nacimiento:'
      end
      object LNacio: TLabel
        Left = 55
        Top = 35
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nacionalidad:'
      end
      object LEdoCivil: TLabel
        Left = 62
        Top = 81
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Estado Civil:'
      end
      object LCB_LA_MAT: TLabel
        Left = 6
        Top = 104
        Width = 114
        Height = 13
        Alignment = taRightJustify
        Caption = 'Lugar y A'#241'o Matrimonio:'
      end
      object LTransporte: TLabel
        Left = 66
        Top = 178
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Transporte:'
      end
      object Label23: TLabel
        Left = 32
        Top = 129
        Width = 88
        Height = 13
        Caption = 'Nombre del Padre:'
      end
      object Label24: TLabel
        Left = 21
        Top = 153
        Width = 99
        Height = 13
        Caption = 'Nombre de la Madre:'
      end
      object lblTDiscapacidad: TLabel
        Left = 14
        Top = 218
        Width = 107
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Discapacidad:'
      end
      object CB_LUG_NAC: TDBEdit
        Left = 128
        Top = 8
        Width = 200
        Height = 21
        DataField = 'CB_LUG_NAC'
        DataSource = DataSource
        TabOrder = 0
      end
      object CB_NACION: TDBEdit
        Left = 128
        Top = 31
        Width = 100
        Height = 21
        DataField = 'CB_NACION'
        DataSource = DataSource
        TabOrder = 1
      end
      object CB_PASAPOR: TDBCheckBox
        Left = 37
        Top = 56
        Width = 104
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Tiene Pasaporte:'
        DataField = 'CB_PASAPOR'
        DataSource = DataSource
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CB_LA_MAT: TDBEdit
        Left = 128
        Top = 100
        Width = 200
        Height = 21
        DataField = 'CB_LA_MAT'
        DataSource = DataSource
        TabOrder = 4
      end
      object CB_MED_TRA: TZetaDBKeyLookup
        Left = 128
        Top = 174
        Width = 260
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 45
        DataField = 'CB_MED_TRA'
        DataSource = DataSource
      end
      object CB_EDO_CIV: TZetaDBKeyLookup
        Left = 128
        Top = 77
        Width = 260
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 30
        DataField = 'CB_EDO_CIV'
        DataSource = DataSource
      end
      object NombrePadre: TEdit
        Left = 128
        Top = 125
        Width = 200
        Height = 21
        MaxLength = 30
        TabOrder = 5
      end
      object NombreMadre: TEdit
        Left = 128
        Top = 149
        Width = 200
        Height = 21
        MaxLength = 30
        TabOrder = 6
      end
      object Panel2: TPanel
        Left = 360
        Top = 200
        Width = 100
        Height = 33
        BevelOuter = bvNone
        TabOrder = 10
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object CB_DISCAPA: TDBCheckBox
        Left = 1
        Top = 197
        Width = 140
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Sufre una Discapacidad:'
        DataField = 'CB_DISCAPA'
        DataSource = DataSource
        TabOrder = 8
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_DISCAPAClick
      end
      object CB_INDIGE: TDBCheckBox
        Left = 57
        Top = 237
        Width = 84
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Es Ind'#237'gena:   '
        DataField = 'CB_INDIGE'
        DataSource = DataSource
        TabOrder = 9
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CB_TDISCAP: TZetaDBKeyCombo
        Left = 128
        Top = 214
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 11
        ListaFija = lfDiscapacidadSTPS
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'CB_TDISCAP'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
    object TabResidencia: TTabSheet
      Caption = 'Domicilio'
      TabVisible = False
      object LDireccion: TLabel
        Left = 99
        Top = 10
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'Calle:'
      end
      object LColonia: TLabel
        Left = 87
        Top = 55
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Colonia:'
      end
      object LCiudad: TLabel
        Left = 89
        Top = 166
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ciudad:'
      end
      object LCP: TLabel
        Left = 57
        Top = 99
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo Postal:'
      end
      object LEstado: TLabel
        Left = 89
        Top = 188
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Estado:'
      end
      object LZona: TLabel
        Left = 61
        Top = 143
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = 'Zona Ciudad:'
      end
      object LViveCon: TLabel
        Left = 79
        Top = 305
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vive Con:'
      end
      object LHabita: TLabel
        Left = 85
        Top = 327
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vive En:'
      end
      object Label19: TLabel
        Left = 7
        Top = 349
        Width = 118
        Height = 13
        Alignment = taRightJustify
        Caption = 'Residencia en la Ciudad:'
      end
      object Label11: TLabel
        Left = 188
        Top = 349
        Width = 24
        Height = 13
        Caption = 'A'#241'os'
      end
      object Label21: TLabel
        Left = 276
        Top = 349
        Width = 31
        Height = 13
        Caption = 'Meses'
      end
      object LCB_CLINICA: TLabel
        Left = 89
        Top = 121
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cl'#237'nica:'
      end
      object Label1: TLabel
        Left = 79
        Top = 33
        Width = 48
        Height = 13
        Caption = '# Exterior:'
      end
      object Label2: TLabel
        Left = 218
        Top = 33
        Width = 45
        Height = 13
        Caption = '# Interior:'
      end
      object Label10: TLabel
        Left = 77
        Top = 210
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Municipio:'
      end
      object CB_ESTADO: TZetaDBKeyLookup
        Left = 133
        Top = 184
        Width = 260
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        WidthLlave = 45
        OnValidKey = CB_ESTADOValidKey
        DataField = 'CB_ESTADO'
        DataSource = DataSource
      end
      object CB_CALLE: TDBEdit
        Left = 133
        Top = 6
        Width = 205
        Height = 21
        DataField = 'CB_CALLE'
        DataSource = DataSource
        TabOrder = 0
      end
      object CB_COLONIA: TDBEdit
        Left = 133
        Top = 73
        Width = 205
        Height = 21
        DataField = 'CB_COLONIA'
        DataSource = DataSource
        TabOrder = 4
      end
      object CB_CIUDAD: TDBEdit
        Left = 133
        Top = 162
        Width = 100
        Height = 21
        DataField = 'CB_CIUDAD'
        DataSource = DataSource
        TabOrder = 8
      end
      object CB_ZONA: TDBEdit
        Left = 133
        Top = 139
        Width = 65
        Height = 21
        DataField = 'CB_ZONA'
        DataSource = DataSource
        TabOrder = 7
      end
      object CB_CODPOST: TDBEdit
        Left = 133
        Top = 95
        Width = 57
        Height = 21
        DataField = 'CB_CODPOST'
        DataSource = DataSource
        TabOrder = 5
      end
      object CB_VIVECON: TZetaDBKeyLookup
        Left = 133
        Top = 301
        Width = 260
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 12
        TabStop = True
        WidthLlave = 45
        DataField = 'CB_VIVECON'
        DataSource = DataSource
      end
      object CB_VIVEEN: TZetaDBKeyLookup
        Left = 133
        Top = 323
        Width = 260
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 13
        TabStop = True
        WidthLlave = 45
        DataField = 'CB_VIVEEN'
        DataSource = DataSource
      end
      object ZYear: TZetaNumero
        Left = 133
        Top = 345
        Width = 45
        Height = 21
        Mascara = mnDias
        TabOrder = 14
        Text = '0'
        OnExit = ZYearExit
      end
      object ZMonth: TZetaNumero
        Left = 221
        Top = 345
        Width = 45
        Height = 21
        Mascara = mnDiasFraccion
        TabOrder = 15
        Text = '0.00'
        OnExit = ZMonthExit
      end
      object Panel3: TPanel
        Left = 400
        Top = 200
        Width = 68
        Height = 33
        BevelOuter = bvNone
        TabOrder = 16
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object CB_COD_COL: TZetaDBKeyLookup
        Left = 133
        Top = 51
        Width = 310
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_COD_COL'
        DataSource = DataSource
      end
      object CB_CLINICA: TDBEdit
        Left = 133
        Top = 117
        Width = 45
        Height = 21
        DataField = 'CB_CLINICA'
        DataSource = DataSource
        MaxLength = 3
        TabOrder = 6
        OnChange = PersonalesChange
      end
      object CB_NUM_EXT: TDBEdit
        Left = 133
        Top = 29
        Width = 80
        Height = 21
        DataField = 'CB_NUM_EXT'
        DataSource = DataSource
        TabOrder = 1
      end
      object CB_NUM_INT: TDBEdit
        Left = 266
        Top = 29
        Width = 80
        Height = 21
        DataField = 'CB_NUM_INT'
        DataSource = DataSource
        TabOrder = 2
      end
      object CB_MUNICIP: TZetaDBKeyLookup
        Left = 133
        Top = 206
        Width = 260
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        WidthLlave = 45
        DataField = 'CB_MUNICIP'
        DataSource = DataSource
      end
      object GroupBox6: TGroupBox
        Left = 16
        Top = 230
        Width = 497
        Height = 65
        Caption = ' Contacto: '
        TabOrder = 11
        object LTelefono: TLabel
          Left = 64
          Top = 16
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tel'#233'fono:'
        end
        object Label12: TLabel
          Left = 20
          Top = 38
          Width = 89
          Height = 13
          Alignment = taRightJustify
          Caption = 'Correo electr'#243'nico:'
        end
        object CB_E_MAIL: TDBEdit
          Left = 117
          Top = 34
          Width = 310
          Height = 21
          Hint = 
            'Correo electr'#243'nico del empleado '#250'til para los procesos de Timbra' +
            'do de N'#243'mina ( capture en formato usuario@servidor.com ) '
          DataField = 'CB_E_MAIL'
          DataSource = DataSource
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object CB_TEL: TDBEdit
          Left = 117
          Top = 12
          Width = 100
          Height = 21
          DataField = 'CB_TEL'
          DataSource = DataSource
          TabOrder = 0
        end
      end
    end
    object TabExperiencia: TTabSheet
      Caption = 'Experiencia'
      TabVisible = False
      object Panel4: TPanel
        Left = 432
        Top = 104
        Width = 76
        Height = 33
        BevelOuter = bvNone
        TabOrder = 2
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 537
        Height = 217
        Align = alTop
        Caption = ' Experiencia Acad'#233'mica '
        TabOrder = 0
        object LEstudios: TLabel
          Left = 63
          Top = 21
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = 'Grado de Estudios:'
        end
        object Label9: TLabel
          Left = 102
          Top = 46
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Instituci'#243'n:'
        end
        object Label6: TLabel
          Left = 63
          Top = 70
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Instituci'#243'n:'
        end
        object LCarrera: TLabel
          Left = 63
          Top = 95
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = 'Carrera Terminada:'
        end
        object Label7: TLabel
          Left = 5
          Top = 118
          Width = 148
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Documento Probatorio:'
        end
        object Label8: TLabel
          Left = 19
          Top = 143
          Width = 134
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o de Emisi'#243'n Documento:'
        end
        object CB_EST_HORlbl: TLabel
          Left = 71
          Top = 183
          Width = 82
          Height = 13
          Alignment = taRightJustify
          Caption = 'Carrera y Horario:'
          Enabled = False
        end
        object CB_ESTUDIO: TZetaDBKeyLookup
          Left = 159
          Top = 17
          Width = 365
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_ESTUDIO'
          DataSource = DataSource
        end
        object CB_CARRERA: TDBEdit
          Left = 159
          Top = 91
          Width = 365
          Height = 21
          DataField = 'CB_CARRERA'
          DataSource = DataSource
          TabOrder = 3
        end
        object CB_EST_HOY: TDBCheckBox
          Left = 51
          Top = 162
          Width = 121
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Estudia Actualmente:'
          DataField = 'CB_EST_HOY'
          DataSource = DataSource
          TabOrder = 6
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          OnClick = CB_EST_HOYClick
        end
        object CB_EST_HOR: TDBEdit
          Left = 159
          Top = 179
          Width = 365
          Height = 21
          DataField = 'CB_EST_HOR'
          DataSource = DataSource
          Enabled = False
          TabOrder = 7
        end
        object CB_ESCUELA: TDBEdit
          Left = 159
          Top = 42
          Width = 365
          Height = 21
          DataField = 'CB_ESCUELA'
          DataSource = DataSource
          TabOrder = 1
        end
        object CB_TESCUEL: TZetaDBKeyCombo
          Left = 159
          Top = 66
          Width = 126
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 2
          ListaFija = lfTipoInstitucionSTPS
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'CB_TESCUEL'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object CB_TITULO: TZetaDBKeyCombo
          Left = 159
          Top = 114
          Width = 126
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 4
          ListaFija = lfTipoDocProbatorioSTPS
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'CB_TITULO'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object CB_YTITULO: TZetaDBNumero
          Left = 159
          Top = 139
          Width = 125
          Height = 21
          Mascara = mnDias
          TabOrder = 5
          Text = '0'
          DataField = 'CB_YTITULO'
          DataSource = DataSource
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 217
        Width = 537
        Height = 325
        Align = alClient
        Caption = ' Habilidades '
        TabOrder = 1
        object CB_IDIOMAlbl: TLabel
          Left = 72
          Top = 39
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = 'Idioma y Dominio:'
          Enabled = False
        end
        object LMaquinas: TLabel
          Left = 45
          Top = 63
          Width = 110
          Height = 13
          Alignment = taRightJustify
          Caption = 'M'#225'quinas que Conoce:'
        end
        object LExperiencia: TLabel
          Left = 97
          Top = 87
          Width = 58
          Height = 13
          Alignment = taRightJustify
          Caption = 'Experiencia:'
        end
        object CB_HABLA: TDBCheckBox
          Left = 64
          Top = 15
          Width = 108
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Habla Otro Idioma:'
          DataField = 'CB_HABLA'
          DataSource = DataSource
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          OnClick = CB_HABLAClick
        end
        object CB_IDIOMA: TDBEdit
          Left = 158
          Top = 35
          Width = 365
          Height = 21
          DataField = 'CB_IDIOMA'
          DataSource = DataSource
          Enabled = False
          TabOrder = 1
        end
        object CB_MAQUINA: TDBEdit
          Left = 158
          Top = 59
          Width = 365
          Height = 21
          DataField = 'CB_MAQUINA'
          DataSource = DataSource
          TabOrder = 2
        end
        object CB_EXPERIE: TDBEdit
          Left = 158
          Top = 83
          Width = 365
          Height = 21
          DataField = 'CB_EXPERIE'
          DataSource = DataSource
          TabOrder = 3
        end
      end
    end
    object TabContratacion: TTabSheet
      Caption = 'Contrataci'#243'n'
      TabVisible = False
      object Label34: TLabel
        Left = 10
        Top = 37
        Width = 89
        Height = 13
        Caption = 'Antig'#252'edad desde:'
      end
      object ZAntiguedad: TZetaTextBox
        Left = 241
        Top = 35
        Width = 153
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object CB_PUESTOlbl: TLabel
        Left = 62
        Top = 150
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object CB_TURNOlbl: TLabel
        Left = 67
        Top = 219
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object CB_PATRONlbl: TLabel
        Left = 14
        Top = 242
        Width = 84
        Height = 13
        Caption = 'Registro Patronal:'
      end
      object CB_CLASIFIlbl: TLabel
        Left = 36
        Top = 196
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
      end
      object Label18: TLabel
        Left = 61
        Top = 13
        Width = 38
        Height = 13
        Caption = 'Ingreso:'
      end
      object bbMostrarCalendario: TSpeedButton
        Left = 407
        Top = 215
        Width = 23
        Height = 22
        Hint = 'Mostrar Calendario del Turno'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
          003337777777777777F330FFFFFFFFFFF03337F3333FFF3337F330FFFF000FFF
          F03337F33377733337F330FFFFF0FFFFF03337F33337F33337F330FFFF00FFFF
          F03337F33377F33337F330FFFFF0FFFFF03337F33337333337F330FFFFFFFFFF
          F03337FFF3F3F3F3F7F33000F0F0F0F0F0333777F7F7F7F7F7F330F0F000F070
          F03337F7F777F777F7F330F0F0F0F070F03337F7F7373777F7F330F0FF0FF0F0
          F03337F733733737F7F330FFFFFFFF00003337F33333337777F330FFFFFFFF0F
          F03337FFFFFFFF7F373330999999990F033337777777777F733330FFFFFFFF00
          333337FFFFFFFF77333330000000000333333777777777733333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = bbMostrarCalendarioClick
      end
      object CB_PLAZALBL: TLabel
        Left = 69
        Top = 173
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'P&laza:'
        Enabled = False
        FocusControl = CB_PLAZA
      end
      object Label4: TLabel
        Left = 19
        Top = 266
        Width = 78
        Height = 13
        Caption = 'Tipo de N'#243'mina:'
      end
      object CB_FEC_ANT: TZetaDBFecha
        Left = 107
        Top = 32
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '16/dic/97'
        Valor = 35780.000000000000000000
        DataField = 'CB_FEC_ANT'
        DataSource = DataSource
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 56
        Width = 409
        Height = 82
        Caption = 'Contrato'
        TabOrder = 2
        object CB_FEC_CONlbl: TLabel
          Left = 62
          Top = 13
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicio:'
        end
        object CB_CONTRATlbl: TLabel
          Left = 66
          Top = 36
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
        end
        object LVencimiento: TLabel
          Left = 29
          Top = 60
          Width = 61
          Height = 13
          Caption = 'Vencimiento:'
        end
        object CB_FEC_CON: TZetaDBFecha
          Left = 98
          Top = 8
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '16/dic/97'
          Valor = 35780.000000000000000000
          DataField = 'CB_FEC_CON'
          DataSource = DataSource
        end
        object CB_CONTRAT: TZetaDBKeyLookup
          Left = 98
          Top = 32
          Width = 300
          Height = 21
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
          OnValidLookup = CB_CONTRATValidLookup
          DataField = 'CB_CONTRAT'
          DataSource = DataSource
        end
        object CB_FEC_COV: TZetaDBFecha
          Left = 98
          Top = 56
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 2
          Text = '12/may/08'
          Valor = 39580.000000000000000000
          DataField = 'CB_FEC_COV'
          DataSource = DataSource
        end
      end
      object CB_PUESTO: TZetaDBKeyLookup
        Left = 106
        Top = 146
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnExit = CB_PUESTOExit
        DataField = 'CB_PUESTO'
        DataSource = DataSource
      end
      object CB_CLASIFI: TZetaDBKeyLookup
        Left = 106
        Top = 192
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        OnExit = CB_CLASIFIExit
        DataField = 'CB_CLASIFI'
        DataSource = DataSource
      end
      object CB_TURNO: TZetaDBKeyLookup
        Left = 106
        Top = 215
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTurnos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TURNO'
        DataSource = DataSource
      end
      object CB_PATRON: TZetaDBKeyLookup
        Left = 106
        Top = 238
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_PATRON'
        DataSource = DataSource
      end
      object CB_FEC_ING: TZetaDBFecha
        Left = 107
        Top = 8
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '16/dic/97'
        Valor = 35780.000000000000000000
        DataField = 'CB_FEC_ING'
        DataSource = DataSource
      end
      object Panel5: TPanel
        Left = 432
        Top = 195
        Width = 36
        Height = 33
        BevelOuter = bvNone
        TabOrder = 9
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object CB_PLAZA: TZetaDBKeyLookup
        Left = 106
        Top = 169
        Width = 300
        Height = 21
        Enabled = False
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_PLAZA'
        DataSource = DataSource
      end
      object CB_NOMINA: TZetaDBKeyCombo
        Left = 106
        Top = 262
        Width = 190
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 8
        ListaFija = lfTipoPeriodoConfidencial
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
        DataField = 'CB_NOMINA'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object GroupBox7: TGroupBox
        Left = 8
        Top = 295
        Width = 409
        Height = 49
        Caption = ' Timbrado de n'#243'mina: '
        TabOrder = 10
        object Label16: TLabel
          Left = 21
          Top = 21
          Width = 69
          Height = 13
          Caption = 'R'#233'gimen SAT:'
        end
        object ZetaDBKeyCombo1: TZetaDBKeyCombo
          Left = 98
          Top = 17
          Width = 300
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfTipoRegimenesSAT
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
          DataField = 'CB_REGIMEN'
          DataSource = DataSource
          LlaveNumerica = True
        end
      end
    end
    object TabArea: TTabSheet
      Caption = 'Area'
      TabVisible = False
      object CB_NIVEL1lbl: TLabel
        Left = 131
        Top = 12
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object CB_NIVEL2lbl: TLabel
        Left = 131
        Top = 36
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object CB_NIVEL3lbl: TLabel
        Left = 131
        Top = 60
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object CB_NIVEL4lbl: TLabel
        Left = 131
        Top = 84
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object CB_NIVEL5lbl: TLabel
        Left = 131
        Top = 108
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object CB_NIVEL6lbl: TLabel
        Left = 131
        Top = 132
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object CB_NIVEL7lbl: TLabel
        Left = 131
        Top = 156
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object CB_NIVEL8lbl: TLabel
        Left = 131
        Top = 180
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object CB_NIVEL9lbl: TLabel
        Left = 131
        Top = 204
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object CB_NIVEL10lbl: TLabel
        Left = 125
        Top = 228
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object CB_NIVEL11lbl: TLabel
        Left = 125
        Top = 252
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object CB_NIVEL12lbl: TLabel
        Left = 125
        Top = 276
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
      object CB_NIVEL9: TZetaDBKeyLookup
        Left = 176
        Top = 200
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL9ValidLookup
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
      end
      object CB_NIVEL8: TZetaDBKeyLookup
        Left = 176
        Top = 176
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL8ValidLookup
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
      end
      object CB_NIVEL7: TZetaDBKeyLookup
        Left = 176
        Top = 152
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL7ValidLookup
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
      end
      object CB_NIVEL6: TZetaDBKeyLookup
        Left = 176
        Top = 128
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL6ValidLookup
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
      end
      object CB_NIVEL5: TZetaDBKeyLookup
        Left = 176
        Top = 104
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL5ValidLookup
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
      end
      object CB_NIVEL4: TZetaDBKeyLookup
        Left = 176
        Top = 80
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL4ValidLookup
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
      end
      object CB_NIVEL3: TZetaDBKeyLookup
        Left = 176
        Top = 56
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL3ValidLookup
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
      end
      object CB_NIVEL2: TZetaDBKeyLookup
        Left = 176
        Top = 32
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL2ValidLookup
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
      end
      object CB_NIVEL1: TZetaDBKeyLookup
        Left = 176
        Top = 8
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
      end
      object Panel6: TPanel
        Left = 16
        Top = 264
        Width = 100
        Height = 33
        BevelOuter = bvNone
        TabOrder = 12
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object CB_NIVEL10: TZetaDBKeyLookup
        Left = 176
        Top = 224
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL10ValidLookup
        DataField = 'CB_NIVEL10'
      end
      object CB_NIVEL11: TZetaDBKeyLookup
        Left = 176
        Top = 248
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL11ValidLookup
        DataField = 'CB_NIVEL11'
      end
      object CB_NIVEL12: TZetaDBKeyLookup
        Left = 176
        Top = 272
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL12ValidLookup
        DataField = 'CB_NIVEL12'
      end
    end
    object TabSalario: TTabSheet
      Caption = 'Salario'
      TabVisible = False
      object CB_SALARIOlbl: TLabel
        Left = 75
        Top = 32
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Salario Diario:'
      end
      object CB_PER_VARlbl: TLabel
        Left = 32
        Top = 56
        Width = 108
        Height = 13
        Caption = 'Promedio de Variables:'
      end
      object CB_TABLASSlbl: TLabel
        Left = 31
        Top = 105
        Width = 109
        Height = 13
        Caption = 'Tabla de Prestaciones:'
      end
      object CB_ZONA_GElbl: TLabel
        Left = 57
        Top = 80
        Width = 83
        Height = 13
        Caption = 'Zona Geogr'#225'fica:'
      end
      object CB_RANGO_SLbl: TLabel
        Left = 68
        Top = 129
        Width = 72
        Height = 13
        Caption = 'Rango Salarial:'
      end
      object CB_SALARIO: TZetaDBNumero
        Left = 148
        Top = 28
        Width = 90
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        OnExit = CB_SALARIOExit
        DataField = 'CB_SALARIO'
        DataSource = DataSource
      end
      object CB_PER_VAR: TZetaDBNumero
        Left = 148
        Top = 52
        Width = 90
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
        DataField = 'CB_PER_VAR'
        DataSource = DataSource
      end
      object CB_TABLASS: TZetaDBKeyLookup
        Left = 148
        Top = 101
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TABLASS'
        DataSource = DataSource
      end
      object CB_ZONA_GE: TDBComboBox
        Left = 148
        Top = 76
        Width = 60
        Height = 21
        Style = csDropDownList
        DataField = 'CB_ZONA_GE'
        DataSource = DataSource
        ItemHeight = 13
        Items.Strings = (
          'A'
          'B'
          'C')
        TabOrder = 3
      end
      object CB_AUTOSAL: TDBCheckBox
        Left = 35
        Top = 9
        Width = 126
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Salario por Tabulador:'
        DataField = 'CB_AUTOSAL'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_AUTOSALClick
      end
      object Panel7: TPanel
        Left = 360
        Top = 200
        Width = 100
        Height = 33
        BevelOuter = bvNone
        TabOrder = 6
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object CB_RANGO_S: TZetaDBNumero
        Left = 148
        Top = 124
        Width = 90
        Height = 21
        Mascara = mnTasa
        TabOrder = 5
        Text = '0.0 %'
        OnExit = CB_RANGO_SExit
        DataField = 'CB_RANGO_S'
        DataSource = DataSource
      end
    end
    object TabPrestacion: TTabSheet
      Caption = 'Percepciones'
      TabVisible = False
      object Label20: TLabel
        Left = 28
        Top = 14
        Width = 151
        Height = 13
        Caption = 'Percepciones Disponibles:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object ZetaSmartListsButton2: TZetaSmartListsButton
        Left = 219
        Top = 130
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080000000000000008080008080008080008080008080008080008080008080
          00808000808000808000808000000000000000FFFF0000000080800080800080
          8000808000808000808000808000808000808000808000000000000000FFFF00
          FFFF00FFFF000000008080008080008080008080008080008080008080008080
          00000000000000FFFF00FFFF00FFFF00FFFF00FFFF0000000000000000000000
          0000000000000000000000000000000000FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000008080008080
          00000000000000FFFF00FFFF00FFFF00FFFF00FFFF0000000000000000000000
          0000000000000000000000808000808000808000808000000000000000FFFF00
          FFFF00FFFF000000008080008080008080008080008080008080008080008080
          00808000808000808000808000000000000000FFFF0000000080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080000000000000008080008080008080008080008080008080008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080}
        Tipo = bsRechazar
        SmartLists = ZetaSmartLists
      end
      object ZetaSmartListsButton1: TZetaSmartListsButton
        Left = 219
        Top = 105
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000000080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          FFFF000000000000008080008080008080008080008080008080008080008080
          00808000808000808000808000000000FFFF00FFFF00FFFF0000000000000080
          8000808000808000808000000000000000000000000000000000000000000000
          FFFF00FFFF00FFFF00FFFF00FFFF00000000000000808000808000000000FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00000000000000000000000000000000000000000000000000000000
          FFFF00FFFF00FFFF00FFFF00FFFF000000000000008080008080008080008080
          00808000808000808000808000000000FFFF00FFFF00FFFF0000000000000080
          8000808000808000808000808000808000808000808000808000808000000000
          FFFF000000000000008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000000080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080008080008080
          0080800080800080800080800080800080800080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          8080008080008080008080008080008080008080008080008080}
        Tipo = bsEscoger
        SmartLists = ZetaSmartLists
      end
      object Label22: TLabel
        Left = 272
        Top = 14
        Width = 169
        Height = 13
        Caption = 'Percepciones Seleccionadas:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object ZetaSmartListsButton3: TZetaSmartListsButton
        Left = 458
        Top = 105
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000008080008080
          0080800080800080800080800000000000000000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800000000000000000000000000000FF0000000000000000000000000080
          800080800080800080800080800080800080800000000000FF0000FF0000FF00
          00FF0000FF0000FF0000FF000000008080008080008080008080008080008080
          0080800080800000000000FF0000FF0000FF0000FF0000FF0000000080800080
          800080800080800080800080800080800080800080800000000000FF0000FF00
          00FF0000FF0000FF000000008080008080008080008080008080008080008080
          0080800080800080800000000000FF0000FF0000FF0000000080800080800080
          800080800080800080800080800080800080800080800080800000000000FF00
          00FF0000FF000000008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800080800080800080800080800000000080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          0000008080008080008080008080008080008080008080008080}
        Tipo = bsSubir
        SmartLists = ZetaSmartLists
      end
      object ZetaSmartListsButton4: TZetaSmartListsButton
        Left = 458
        Top = 130
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000008080008080
          0080800080800080800080800080800000000080800080800080800080800080
          8000808000808000808000808000808000808000808000808000808000808000
          0000008080008080008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800080800080800000000000FF0000FF0000FF0000000080800080800080
          800080800080800080800080800080800080800080800080800000000000FF00
          00FF0000FF000000008080008080008080008080008080008080008080008080
          0080800080800000000000FF0000FF0000FF0000FF0000FF0000000080800080
          800080800080800080800080800080800080800080800000000000FF0000FF00
          00FF0000FF0000FF000000008080008080008080008080008080008080008080
          0080800000000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000000080
          8000808000808000808000808000808000808000000000000000000000000000
          00FF000000000000000000000000008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          00FF000000008080008080008080008080008080008080008080008080008080
          0080800080800080800080800000000000FF0000000080800080800080800080
          8000808000808000808000808000808000808000808000808000808000000000
          0000000000008080008080008080008080008080008080008080}
        Tipo = bsBajar
        SmartLists = ZetaSmartLists
      end
      object LBDisponibles: TZetaSmartListBox
        Left = 8
        Top = 42
        Width = 200
        Height = 180
        ItemHeight = 13
        TabOrder = 0
      end
      object LBSeleccion: TZetaSmartListBox
        Left = 253
        Top = 42
        Width = 200
        Height = 180
        ItemHeight = 13
        TabOrder = 1
      end
      object Panel11: TPanel
        Left = 360
        Top = 224
        Width = 100
        Height = 17
        BevelOuter = bvNone
        TabOrder = 2
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
    end
    object TabOtros: TTabSheet
      Caption = 'Otros'
      TabVisible = False
      object GBAsistencia: TGroupBox
        Left = 5
        Top = 143
        Width = 320
        Height = 125
        Caption = ' Asistencia '
        TabOrder = 3
        object LTipoCreden: TLabel
          Left = 18
          Top = 14
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Letra Credencial:'
        end
        object Label3: TLabel
          Left = 22
          Top = 54
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero Tarjeta:'
        end
        object lblNumeroBiometrico: TLabel
          Left = 7
          Top = 78
          Width = 92
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero Biom'#233'trico:'
          Enabled = False
          Visible = False
        end
        object btnAsignarBio: TSpeedButton
          Left = 252
          Top = 72
          Width = 29
          Height = 24
          Hint = 'Asignar ID biom'#233'trico'
          Glyph.Data = {
            42020000424D4202000000000000420000002800000010000000100000000100
            1000030000000002000000000000000000000000000000000000007C0000E003
            00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C00001F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C00001F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C007C00001F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1042007C007C00001F7C007C007C007C00001F7C1F7C1F7C
            1F7C1F7C1F7C1042007C00001F7C1F7C1F7C1F7C007C007C00001F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C0000
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C
            00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            007C007C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C}
          ParentShowHint = False
          ShowHint = True
          Visible = False
          OnClick = btnAsignarBioClick
        end
        object btnBorrarBio: TSpeedButton
          Left = 283
          Top = 72
          Width = 29
          Height = 24
          Hint = 'Borrar ID biom'#233'trico'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55558888888585599958555555555550305555555550055BB0555555550FB000
            0055555550FB0BF0F05555550FBFBF0FB05555550BFBF0FB005555500FBFBFB0
            B055550E0BFBFB00B05550EEE0BFB0B0B055000EEE0BFBF0B0550000EEE00000
            B05500000E055550705550000055555505555500055555550555}
          ParentShowHint = False
          ShowHint = True
          Visible = False
          OnClick = btnBorrarBioClick
        end
        object CB_ID_BIO: TZetaDBTextBox
          Left = 102
          Top = 74
          Width = 148
          Height = 21
          AutoSize = False
          Caption = 'CB_ID_BIO'
          ShowAccelChar = False
          Visible = False
          Brush.Color = clBtnFace
          Border = True
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object lblCB_GP_COD: TLabel
          Left = 8
          Top = 102
          Width = 91
          Height = 13
          Alignment = taRightJustify
          Caption = 'Grupo Dispositivos:'
          Enabled = False
          Visible = False
        end
        object CB_CHECA: TDBCheckBox
          Left = 24
          Top = 32
          Width = 91
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Checa Tarjeta:'
          DataField = 'CB_CHECA'
          DataSource = DataSource
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_CREDENC: TDBEdit
          Left = 102
          Top = 10
          Width = 30
          Height = 21
          CharCase = ecUpperCase
          DataField = 'CB_CREDENC'
          DataSource = DataSource
          TabOrder = 0
        end
        object CB_ID_NUM: TDBEdit
          Left = 102
          Top = 50
          Width = 211
          Height = 21
          DataField = 'CB_ID_NUM'
          DataSource = DataSource
          TabOrder = 2
          OnExit = CB_ID_NUMExit
        end
        object CB_GP_COD: TZetaDBKeyLookup
          Left = 102
          Top = 98
          Width = 212
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 3
          TabStop = True
          Visible = False
          WidthLlave = 60
          DataSource = DataSource
        end
      end
      object GBEvaluacion: TGroupBox
        Left = 327
        Top = 143
        Width = 181
        Height = 125
        Caption = ' Evaluaci'#243'n '
        TabOrder = 4
        object lblFechaEv: TLabel
          Left = 33
          Top = 16
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object lblResultadoEv: TLabel
          Left = 15
          Top = 36
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Resultado:'
        end
        object lblProximaEv: TLabel
          Left = 26
          Top = 58
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Pr'#243'xima:'
        end
        object CB_LAST_EV: TZetaDBFecha
          Left = 70
          Top = 11
          Width = 105
          Height = 22
          Cursor = crArrow
          Opcional = True
          TabOrder = 0
          Text = '15/dic/97'
          Valor = 35779.000000000000000000
          DataField = 'CB_LAST_EV'
          DataSource = DataSource
        end
        object CB_EVALUA: TZetaDBNumero
          Left = 70
          Top = 32
          Width = 73
          Height = 21
          Mascara = mnPesosDiario
          TabOrder = 1
          Text = '0.00'
          DataField = 'CB_EVALUA'
          DataSource = DataSource
        end
        object CB_NEXT_EV: TZetaDBFecha
          Left = 70
          Top = 53
          Width = 105
          Height = 22
          Cursor = crArrow
          Opcional = True
          TabOrder = 2
          Text = '15/dic/97'
          Valor = 35779.000000000000000000
          DataField = 'CB_NEXT_EV'
          DataSource = DataSource
        end
      end
      object GBFonacot: TGroupBox
        Left = 327
        Top = -2
        Width = 181
        Height = 67
        Caption = ' Fonacot '
        TabOrder = 1
        object Label5: TLabel
          Left = 5
          Top = 31
          Width = 10
          Height = 13
          Alignment = taRightJustify
          Caption = '#:'
        end
        object CB_FONACOT: TDBEdit
          Left = 18
          Top = 27
          Width = 158
          Height = 21
          DataField = 'CB_FONACOT'
          DataSource = DataSource
          TabOrder = 0
        end
      end
      object GBEmpleo: TGroupBox
        Left = 327
        Top = 72
        Width = 181
        Height = 71
        Caption = ' Programa de Primer Empleo '
        TabOrder = 2
        object CB_EMPLEO: TDBCheckBox
          Left = 31
          Top = 28
          Width = 111
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Aplica al programa:'
          DataField = 'CB_EMPLEO'
          DataSource = DataSource
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
      object GBInfonavit: TGroupBox
        Left = 5
        Top = -2
        Width = 320
        Height = 145
        Caption = ' Infonavit  '
        TabOrder = 0
        object CB_INFTASALbl: TLabel
          Left = 24
          Top = 109
          Width = 97
          Height = 13
          Alignment = taRightJustify
          Caption = 'Valor de Descuento:'
        end
        object CB_INFCREDLbl: TLabel
          Left = 60
          Top = 85
          Width = 61
          Height = 13
          Alignment = taRightJustify
          Caption = '# de Cr'#233'dito:'
        end
        object LblDescuento: TLabel
          Left = 210
          Top = 56
          Width = 15
          Height = 13
          Alignment = taRightJustify
          Caption = '     '
        end
        object Label13: TLabel
          Left = 35
          Top = 13
          Width = 86
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Pr'#233'stamo:'
        end
        object CB_INF_OLDLbl: TLabel
          Left = 246
          Top = 28
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa Anterior:'
          Visible = False
        end
        object CB_INF_INILbl: TLabel
          Left = 23
          Top = 37
          Width = 98
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicio de Descuento:'
        end
        object lblPorcentaje: TLabel
          Left = 376
          Top = 140
          Width = 8
          Height = 13
          Alignment = taRightJustify
          Caption = '%'
          Visible = False
        end
        object CB_INF_ANTLbl: TLabel
          Left = 4
          Top = 61
          Width = 117
          Height = 13
          Caption = 'Otorgamiento de Cr'#233'dito:'
        end
        object CB_INFMANT: TDBCheckBox
          Left = 296
          Top = 12
          Width = 11
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Mantenimiento:'
          DataField = 'CB_INFMANT'
          DataSource = DataSource
          TabOrder = 5
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          Visible = False
        end
        object CB_INFCRED: TDBEdit
          Left = 124
          Top = 81
          Width = 187
          Height = 21
          DataField = 'CB_INFCRED'
          DataSource = DataSource
          TabOrder = 3
        end
        object CB_INFTIPO: TZetaDBKeyCombo
          Left = 124
          Top = 9
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          OnChange = CB_INFTIPOChange
          ListaFija = lfTipoInfonavit
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'CB_INFTIPO'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object CB_INFTASA: TZetaDBNumero
          Left = 124
          Top = 105
          Width = 97
          Height = 21
          Enabled = False
          Mascara = mnTasa
          TabOrder = 4
          Text = '0.0 %'
          DataField = 'CB_INFTASA'
          DataSource = DataSource
        end
        object CB_INF_INI: TZetaDBFecha
          Left = 124
          Top = 33
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '15/dic/97'
          Valor = 35779.000000000000000000
          DataField = 'CB_INF_INI'
          DataSource = DataSource
        end
        object zkcCB_INF_OLD: TZetaKeyCombo
          Left = 280
          Top = 44
          Width = 23
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 6
          Visible = False
          OnChange = zkcCB_INF_OLDChange
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object CB_INFDISM: TDBCheckBox
          Left = 22
          Top = 126
          Width = 115
          Height = 17
          Alignment = taLeftJustify
          BiDiMode = bdLeftToRight
          Caption = 'Disminuci'#243'n Tasa %'
          DataField = 'CB_INFDISM'
          DataSource = DataSource
          ParentBiDiMode = False
          TabOrder = 7
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_INF_ANT: TZetaDBFecha
          Left = 124
          Top = 57
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 2
          Text = '15/dic/97'
          Valor = 35779.000000000000000000
          DataField = 'CB_INF_ANT'
          DataSource = DataSource
        end
      end
      object GroupBox4: TGroupBox
        Left = 5
        Top = 268
        Width = 503
        Height = 130
        Caption = ' Cuentas '
        TabOrder = 5
        object lblTarjDesp: TLabel
          Left = 14
          Top = 110
          Width = 87
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tarjeta Despensa:'
        end
        object btnVerTarjetaDesp: TSpeedButton
          Left = 319
          Top = 104
          Width = 25
          Height = 25
          Hint = 
            'Verificar si el N'#250'mero de Tarjeta de Despensa pertenece a otro E' +
            'mpleado'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            555555555555555555555555555555555555555555FF55555555555559055555
            55555555577FF5555555555599905555555555557777F5555555555599905555
            555555557777FF5555555559999905555555555777777F555555559999990555
            5555557777777FF5555557990599905555555777757777F55555790555599055
            55557775555777FF5555555555599905555555555557777F5555555555559905
            555555555555777FF5555555555559905555555555555777FF55555555555579
            05555555555555777FF5555555555557905555555555555777FF555555555555
            5990555555555555577755555555555555555555555555555555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btnVerTarjetaDespClick
        end
        object lblTarjGas: TLabel
          Left = 21
          Top = 81
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tarjeta Gasolina:'
        end
        object btnVerTarjetaGasolina: TSpeedButton
          Left = 319
          Top = 77
          Width = 25
          Height = 25
          Hint = 
            'Verificar si el N'#250'mero de Tarjeta de Gasolina pertenece a otro E' +
            'mpleado'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            555555555555555555555555555555555555555555FF55555555555559055555
            55555555577FF5555555555599905555555555557777F5555555555599905555
            555555557777FF5555555559999905555555555777777F555555559999990555
            5555557777777FF5555557990599905555555777757777F55555790555599055
            55557775555777FF5555555555599905555555555557777F5555555555559905
            555555555555777FF5555555555559905555555555555777FF55555555555579
            05555555555555777FF5555555555557905555555555555777FF555555555555
            5990555555555555577755555555555555555555555555555555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btnVerTarjetaGasolinaClick
        end
        object CB_CTA_VAL: TDBEdit
          Left = 106
          Top = 106
          Width = 211
          Height = 21
          DataField = 'CB_CTA_VAL'
          DataSource = DataSource
          TabOrder = 2
        end
        object CB_CTA_GAS: TDBEdit
          Left = 106
          Top = 79
          Width = 211
          Height = 21
          DataField = 'CB_CTA_GAS'
          DataSource = DataSource
          TabOrder = 1
        end
        object GroupBox5: TGroupBox
          Left = 5
          Top = 11
          Width = 357
          Height = 64
          TabOrder = 0
          object LBanca: TLabel
            Left = 6
            Top = 39
            Width = 90
            Height = 13
            Alignment = taRightJustify
            Caption = 'Banca Electr'#243'nica:'
          end
          object BtnBAN_ELE: TSpeedButton
            Left = 314
            Top = 33
            Width = 25
            Height = 25
            Hint = 
              'Verificar si el N'#250'mero de Banca Electr'#243'nica Pertenece a otro Emp' +
              'leado'
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
              555555555555555555555555555555555555555555FF55555555555559055555
              55555555577FF5555555555599905555555555557777F5555555555599905555
              555555557777FF5555555559999905555555555777777F555555559999990555
              5555557777777FF5555557990599905555555777757777F55555790555599055
              55557775555777FF5555555555599905555555555557777F5555555555559905
              555555555555777FF5555555555559905555555555555777FF55555555555579
              05555555555555777FF5555555555557905555555555555777FF555555555555
              5990555555555555577755555555555555555555555555555555}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            OnClick = BtnBAN_ELEClick
          end
          object lBanco: TLabel
            Left = 62
            Top = 14
            Width = 34
            Height = 13
            Alignment = taRightJustify
            Caption = 'Banco:'
          end
          object CB_BAN_ELE: TDBEdit
            Left = 101
            Top = 35
            Width = 212
            Height = 21
            DataField = 'CB_BAN_ELE'
            DataSource = DataSource
            TabOrder = 1
          end
          object CB_BANCO: TZetaDBKeyLookup
            Left = 101
            Top = 10
            Width = 238
            Height = 21
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 55
            DataField = 'CB_BANCO'
            DataSource = DataSource
          end
        end
      end
      object GBNomina: TGroupBox
        Left = 5
        Top = 397
        Width = 503
        Height = 59
        TabOrder = 6
        object LblNivel0: TLabel
          Left = 18
          Top = 37
          Width = 81
          Height = 13
          Alignment = taRightJustify
          Caption = 'Confidencialidad:'
        end
        object lbNeto: TLabel
          Left = 4
          Top = 13
          Width = 95
          Height = 13
          Alignment = taRightJustify
          Caption = 'Neto (Piramidaci'#243'n):'
        end
        object CB_NIVEL0: TZetaDBKeyLookup
          Left = 102
          Top = 33
          Width = 238
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 55
          DataField = 'CB_NIVEL0'
          DataSource = DataSource
        end
        object CB_NETO: TZetaDBNumero
          Left = 102
          Top = 9
          Width = 97
          Height = 21
          Mascara = mnPesos
          TabOrder = 0
          Text = '0.00'
          DataField = 'CB_NETO'
          DataSource = DataSource
        end
      end
      object gbDatosMedicos: TGroupBox
        Left = 5
        Top = 458
        Width = 503
        Height = 69
        Caption = 'Datos M'#233'dicos'
        TabOrder = 7
        object Label14: TLabel
          Left = 43
          Top = 19
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Sangre:'
        end
        object Label15: TLabel
          Left = 3
          Top = 47
          Width = 116
          Height = 13
          Alignment = taRightJustify
          Caption = 'Al'#233'rgico / Padecimiento:'
        end
        object CB_ALERGIA: TDBEdit
          Left = 123
          Top = 43
          Width = 337
          Height = 21
          DataField = 'CB_ALERGIA'
          DataSource = DataSource
          TabOrder = 1
        end
        object CB_TSANGRE: TDBComboBox
          Left = 123
          Top = 15
          Width = 193
          Height = 21
          DataField = 'CB_TSANGRE'
          DataSource = DataSource
          ItemHeight = 13
          Items.Strings = (
            ''
            'O +'
            'O -'
            'A +'
            'A -'
            'B +'
            'B -'
            'AB +'
            'AB -')
          TabOrder = 0
        end
        object Panel8: TPanel
          Left = 365
          Top = 20
          Width = 116
          Height = 13
          BevelOuter = bvNone
          TabOrder = 2
          TabStop = True
          OnEnter = SaleUltimoCampo
        end
      end
    end
    object TabVacacion: TTabSheet
      Caption = 'Vacaciones'
      TabVisible = False
      object DiasGB: TGroupBox
        Left = 120
        Top = 28
        Width = 217
        Height = 141
        Caption = 'Datos de Vacaciones'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object FechaCierreLbl: TLabel
          Left = 24
          Top = 34
          Width = 78
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha de Cierre:'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object PagadosLbl: TLabel
          Left = 52
          Top = 57
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Por Pagar:'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object GozadosLbl: TLabel
          Left = 52
          Top = 81
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Por Gozar:'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object ZCierre: TZetaTextBox
          Left = 109
          Top = 32
          Width = 80
          Height = 17
          AutoSize = False
          Enabled = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object PrimaLbl: TLabel
          Left = 17
          Top = 105
          Width = 85
          Height = 13
          Alignment = taRightJustify
          Caption = 'Prima Vacacional:'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object CB_DER_PAG: TZetaDBNumero
          Left = 109
          Top = 53
          Width = 50
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 0
          Text = '0.00'
          DataField = 'CB_DER_PAG'
          DataSource = DataSource
        end
        object CB_DER_GOZ: TZetaDBNumero
          Left = 109
          Top = 77
          Width = 50
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 1
          Text = '0.00'
          DataField = 'CB_DER_GOZ'
          DataSource = DataSource
        end
        object CB_DER_PV: TZetaDBNumero
          Left = 109
          Top = 101
          Width = 50
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 2
          Text = '0.00'
          DataField = 'CB_DER_PV'
          DataSource = DataSource
        end
      end
      object Panel12: TPanel
        Left = 360
        Top = 200
        Width = 100
        Height = 33
        BevelOuter = bvNone
        TabOrder = 1
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
    end
  end
  object ListEmp: TListBox [4]
    Left = 0
    Top = 51
    Width = 76
    Height = 552
    Align = alLeft
    ItemHeight = 13
    Items.Strings = (
      'Identificaci'#243'n'
      'Personales'
      'Domicilio'
      'Experiencia'
      'Contrataci'#243'n'
      'Area'
      'Salario'
      'Percepciones'
      'Otros'
      'Adicionales 1'
      'Adicionales 2'
      'Vacaciones')
    TabOrder = 4
    OnClick = ListEmpClick
  end
  inherited DataSource: TDataSource
    Left = 476
    Top = 2
  end
  object ZetaSmartLists: TZetaSmartLists
    BorrarAlCopiar = True
    CopiarObjetos = True
    ListaDisponibles = LBDisponibles
    ListaEscogidos = LBSeleccion
    Left = 512
    Top = 61
  end
end
