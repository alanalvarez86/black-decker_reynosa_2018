inherited EditEmpPersonal: TEditEmpPersonal
  Left = 728
  Top = 265
  Caption = 'Datos Personales'
  ClientHeight = 410
  ClientWidth = 460
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 374
    Width = 460
    inherited OK: TBitBtn
      Left = 295
    end
    inherited Cancelar: TBitBtn
      Left = 380
    end
  end
  inherited PanelSuperior: TPanel
    Width = 460
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 460
    inherited ValorActivo2: TPanel
      Width = 134
      inherited textoValorActivo2: TLabel
        Width = 128
      end
    end
  end
  object PageControl: TPageControl [3]
    Left = 0
    Top = 51
    Width = 460
    Height = 323
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 3
    object TabGenerales: TTabSheet
      Caption = 'Generales'
      object LEdoCivil: TLabel
        Left = 71
        Top = 81
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Estado Civil:'
      end
      object LCB_LA_MAT: TLabel
        Left = 15
        Top = 105
        Width = 114
        Height = 13
        Alignment = taRightJustify
        Caption = 'Lugar y A'#241'o Matrimonio:'
      end
      object LLugNac: TLabel
        Left = 43
        Top = 12
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Lugar Nacimiento:'
      end
      object LNacio: TLabel
        Left = 64
        Top = 35
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nacionalidad:'
      end
      object LResidencia: TLabel
        Left = 11
        Top = 176
        Width = 118
        Height = 13
        Alignment = taRightJustify
        Caption = 'Residencia en la Ciudad:'
      end
      object LTransporte: TLabel
        Left = 75
        Top = 201
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Transporte:'
      end
      object LHabita: TLabel
        Left = 89
        Top = 152
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vive En:'
      end
      object LViveCon: TLabel
        Left = 83
        Top = 128
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vive Con:'
      end
      object Label1: TLabel
        Left = 206
        Top = 176
        Width = 24
        Height = 13
        Caption = 'A'#241'os'
      end
      object Label2: TLabel
        Left = 308
        Top = 176
        Width = 31
        Height = 13
        Caption = 'Meses'
      end
      object lblTDiscapacidad: TLabel
        Left = 22
        Top = 243
        Width = 107
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Discapacidad:'
      end
      object CB_PASAPOR: TDBCheckBox
        Left = 46
        Top = 55
        Width = 103
        Height = 19
        Alignment = taLeftJustify
        Caption = 'Tiene Pasaporte:'
        DataField = 'CB_PASAPOR'
        DataSource = DataSource
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CB_LUG_NAC: TDBEdit
        Left = 136
        Top = 8
        Width = 310
        Height = 21
        DataField = 'CB_LUG_NAC'
        DataSource = DataSource
        TabOrder = 0
      end
      object CB_NACION: TDBEdit
        Left = 136
        Top = 31
        Width = 100
        Height = 21
        DataField = 'CB_NACION'
        DataSource = DataSource
        TabOrder = 1
      end
      object CB_EDO_CIV: TZetaDBKeyLookup
        Left = 136
        Top = 77
        Width = 310
        Height = 21
        LookupDataset = dmTablas.cdsEstadoCivil
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_EDO_CIV'
        DataSource = DataSource
      end
      object CB_LA_MAT: TDBEdit
        Left = 136
        Top = 101
        Width = 200
        Height = 21
        DataField = 'CB_LA_MAT'
        DataSource = DataSource
        TabOrder = 4
      end
      object CB_VIVECON: TZetaDBKeyLookup
        Left = 136
        Top = 124
        Width = 310
        Height = 21
        LookupDataset = dmTablas.cdsViveCon
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_VIVECON'
        DataSource = DataSource
      end
      object CB_VIVEEN: TZetaDBKeyLookup
        Left = 136
        Top = 148
        Width = 310
        Height = 21
        LookupDataset = dmTablas.cdsHabitacion
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_VIVEEN'
        DataSource = DataSource
      end
      object CB_MED_TRA: TZetaDBKeyLookup
        Left = 136
        Top = 196
        Width = 310
        Height = 22
        LookupDataset = dmTablas.cdsTransporte
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_MED_TRA'
        DataSource = DataSource
      end
      object ZYear: TZetaNumero
        Left = 136
        Top = 172
        Width = 60
        Height = 21
        Mascara = mnDias
        TabOrder = 7
        Text = '0'
        OnExit = ZYearExit
      end
      object ZMonth: TZetaNumero
        Left = 240
        Top = 172
        Width = 60
        Height = 21
        Mascara = mnDiasFraccion
        TabOrder = 8
        Text = '0.00'
        OnExit = ZMonthExit
      end
      object CB_DISCAPA: TDBCheckBox
        Left = 10
        Top = 220
        Width = 139
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Sufre una Discapacidad:'
        DataField = 'CB_DISCAPA'
        DataSource = DataSource
        TabOrder = 10
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_DISCAPAClick
      end
      object CB_INDIGE: TDBCheckBox
        Left = 66
        Top = 263
        Width = 83
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Es Ind'#237'gena:'
        DataField = 'CB_INDIGE'
        DataSource = DataSource
        TabOrder = 12
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CB_TDISCAP: TZetaDBKeyCombo
        Left = 136
        Top = 239
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 11
        ListaFija = lfDiscapacidadSTPS
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'CB_TDISCAP'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
    object TabDireccion: TTabSheet
      Caption = 'Domicilio'
      object LDireccion: TLabel
        Left = 68
        Top = 12
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'Calle:'
      end
      object LColonia: TLabel
        Left = 56
        Top = 60
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Colonia:'
      end
      object LCiudad: TLabel
        Left = 58
        Top = 182
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ciudad:'
      end
      object LCP: TLabel
        Left = 26
        Top = 109
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo Postal:'
      end
      object LEstado: TLabel
        Left = 58
        Top = 206
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Estado:'
      end
      object LZona: TLabel
        Left = 30
        Top = 157
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = 'Zona Ciudad:'
      end
      object LCB_CLINICA: TLabel
        Left = 58
        Top = 133
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cl'#237'nica:'
      end
      object Label3: TLabel
        Left = 49
        Top = 36
        Width = 48
        Height = 13
        Caption = '# Exterior:'
      end
      object Label4: TLabel
        Left = 192
        Top = 36
        Width = 45
        Height = 13
        Caption = '# Interior:'
      end
      object Label5: TLabel
        Left = 46
        Top = 230
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Municipio:'
      end
      object CB_ZONA: TDBEdit
        Left = 102
        Top = 153
        Width = 60
        Height = 21
        DataField = 'CB_ZONA'
        DataSource = DataSource
        TabOrder = 7
      end
      object CB_ESTADO: TZetaDBKeyLookup
        Left = 102
        Top = 202
        Width = 310
        Height = 21
        LookupDataset = dmTablas.cdsEstado
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        WidthLlave = 60
        OnValidKey = CB_ESTADOValidKey
        DataField = 'CB_ESTADO'
        DataSource = DataSource
      end
      object CB_CODPOST: TDBEdit
        Left = 102
        Top = 105
        Width = 60
        Height = 21
        DataField = 'CB_CODPOST'
        DataSource = DataSource
        TabOrder = 5
      end
      object CB_CIUDAD: TDBEdit
        Left = 102
        Top = 178
        Width = 150
        Height = 21
        DataField = 'CB_CIUDAD'
        DataSource = DataSource
        TabOrder = 8
      end
      object CB_COLONIA: TDBEdit
        Left = 102
        Top = 81
        Width = 310
        Height = 21
        DataField = 'CB_COLONIA'
        DataSource = DataSource
        TabOrder = 4
      end
      object CB_CALLE: TDBEdit
        Left = 102
        Top = 8
        Width = 310
        Height = 21
        DataField = 'CB_CALLE'
        DataSource = DataSource
        TabOrder = 0
      end
      object CB_COD_COL: TZetaDBKeyLookup
        Left = 102
        Top = 56
        Width = 313
        Height = 21
        LookupDataset = dmTablas.cdsColonia
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 70
        DataField = 'CB_COD_COL'
        DataSource = DataSource
      end
      object CB_CLINICA: TDBEdit
        Left = 102
        Top = 129
        Width = 45
        Height = 21
        DataField = 'CB_CLINICA'
        DataSource = DataSource
        MaxLength = 3
        TabOrder = 6
      end
      object CB_NUM_EXT: TDBEdit
        Left = 102
        Top = 32
        Width = 80
        Height = 21
        DataField = 'CB_NUM_EXT'
        DataSource = DataSource
        TabOrder = 1
      end
      object CB_NUM_INT: TDBEdit
        Left = 240
        Top = 32
        Width = 80
        Height = 21
        DataField = 'CB_NUM_INT'
        DataSource = DataSource
        TabOrder = 2
      end
      object CB_MUNICIP: TZetaDBKeyLookup
        Left = 102
        Top = 226
        Width = 310
        Height = 21
        LookupDataset = dmTablas.cdsMunicipios
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_MUNICIP'
        DataSource = DataSource
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Contacto'
      ImageIndex = 2
      object LTelefono: TLabel
        Left = 51
        Top = 6
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fono:'
      end
      object Label6: TLabel
        Left = 7
        Top = 30
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Correo electr'#243'nico:'
      end
      object CB_E_MAIL: TDBEdit
        Left = 104
        Top = 26
        Width = 331
        Height = 21
        Hint = 
          'Correo electr'#243'nico del empleado '#250'til para los procesos de Timbra' +
          'do de N'#243'mina ( capture en formato usuario@servidor.com ) '
        DataField = 'CB_E_MAIL'
        DataSource = DataSource
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object CB_TEL: TDBEdit
        Left = 104
        Top = 2
        Width = 150
        Height = 21
        DataField = 'CB_TEL'
        DataSource = DataSource
        TabOrder = 1
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 404
    Top = 1
  end
end
