inherited dmCaseta: TdmCaseta
  OldCreateOrder = True
  Height = 349
  Width = 502
  inherited cdsEmpleado: TClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'Gafete'
        ParamType = ptInputOutput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = 'FechaHora'
        ParamType = ptInput
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'Caseta'
        ParamType = ptInputOutput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = 'TipoGafete'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'Tipo'
        ParamType = ptInputOutput
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'Status'
        ParamType = ptOutput
        Value = 0
      end>
    ReadOnly = True
  end
  object cdsDatosEmpleado: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 80
    object cdsDatosEmpleadoNombre: TStringField
      FieldName = 'Nombre'
      Size = 90
    end
    object cdsDatosEmpleadoEmpresa: TStringField
      FieldName = 'Empresa'
      Size = 60
    end
    object cdsDatosEmpleadoStatus: TIntegerField
      FieldName = 'Status'
    end
    object cdsDatosEmpleadoHorario: TStringField
      FieldName = 'Horario'
      Size = 60
    end
    object cdsDatosEmpleadoTurno: TStringField
      FieldName = 'Turno'
      Size = 60
    end
    object cdsDatosEmpleadoSuper: TStringField
      FieldName = 'Super'
      Size = 60
    end
    object cdsDatosEmpleadoFoto: TBlobField
      FieldName = 'Foto'
    end
    object cdsDatosEmpleadoNombreLBL: TStringField
      FieldName = 'NombreLBL'
      Size = 30
    end
    object cdsDatosEmpleadoEmpresaLBL: TStringField
      FieldName = 'EmpresaLBL'
      Size = 30
    end
    object cdsDatosEmpleadoHorarioLBL: TStringField
      FieldName = 'HorarioLBL'
      Size = 30
    end
    object cdsDatosEmpleadoTurnoLBL: TStringField
      FieldName = 'TurnoLBL'
      Size = 30
    end
    object cdsDatosEmpleadoSuperLBL: TStringField
      FieldName = 'SuperLBL'
      Size = 30
    end
  end
  object cdsUltimas: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 144
    object cdsUltimasNombre: TStringField
      DisplayWidth = 90
      FieldName = 'Nombre'
      Size = 90
    end
    object cdsUltimasEmpresa: TStringField
      FieldName = 'Empresa'
      Size = 60
    end
    object cdsUltimasStatus: TIntegerField
      FieldName = 'Status'
    end
    object cdsUltimasHorario: TStringField
      FieldName = 'Horario'
      Size = 60
    end
    object cdsUltimasTurno: TStringField
      FieldName = 'Turno'
      Size = 60
    end
    object cdsUltimasSuper: TStringField
      FieldName = 'Super'
      Size = 60
    end
    object cdsUltimasFoto: TBlobField
      FieldName = 'Foto'
    end
    object cdsUltimasNombreLBL: TStringField
      FieldName = 'NombreLBL'
      Size = 30
    end
    object cdsUltimasEmpresaLBL: TStringField
      FieldName = 'EmpresaLBL'
      Size = 30
    end
    object cdsUltimasHorarioLBL: TStringField
      FieldName = 'HorarioLBL'
      Size = 30
    end
    object cdsUltimasTurnoLBL: TStringField
      FieldName = 'TurnoLBL'
      Size = 30
    end
    object cdsUltimasSuperLBL: TStringField
      FieldName = 'SuperLBL'
      Size = 30
    end
    object cdsUltimasStatusChecada: TStringField
      FieldName = 'StatusChecada'
      Size = 30
    end
    object cdsUltimasCodigo: TStringField
      FieldName = 'Codigo'
    end
    object cdsUltimasResultado: TIntegerField
      FieldName = 'Resultado'
    end
    object cdsUltimasMensaje: TStringField
      FieldName = 'Mensaje'
      Size = 60
    end
    object cdsUltimasHora: TStringField
      FieldName = 'Hora'
    end
  end
  object cdsInformacion: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 208
    object cdsInformacionValor: TStringField
      DisplayWidth = 130
      FieldName = 'Valor'
      Size = 130
    end
    object cdsInformacionStatus: TBooleanField
      FieldName = 'Status'
    end
  end
  object cdsInfoUltima: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 176
    Top = 144
    object StringField1: TStringField
      DisplayWidth = 130
      FieldName = 'Valor'
      Size = 130
    end
    object BooleanField1: TBooleanField
      FieldName = 'Status'
    end
    object cdsInfoUltimaCodigo: TStringField
      FieldName = 'Codigo'
    end
  end
  object cdsReiniciaHuellas: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'Computer'
        ParamType = ptInputOutput
        Size = 50
        Value = Null
      end>
    ReadOnly = True
    Left = 256
    Top = 21
  end
  object dsReiniciaHuellas: TDataSource
    DataSet = cdsReiniciaHuellas
    Left = 352
    Top = 21
  end
  object cdsHuellas: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'Huellas'
        ParamType = ptOutput
        Size = 50
        Value = Null
      end
      item
        DataType = ftString
        Name = 'Letrero'
        ParamType = ptOutput
        Size = 50
        Value = Null
      end
      item
        DataType = ftString
        Name = 'Computer'
        ParamType = ptInputOutput
        Size = 50
        Value = Null
      end
      item
        DataType = ftString
        Name = 'IdCafeteria'
        ParamType = ptInputOutput
        Size = 50
        Value = Null
      end
      item
        DataType = ftInteger
        Name = 'TipoEstacion'
        ParamType = ptInputOutput
        Value = 0
      end>
    ReadOnly = True
    Left = 257
    Top = 65
  end
  object dsHuellas: TDataSource
    DataSet = cdsHuellas
    Left = 352
    Top = 64
  end
  object cdsReportaHuellas: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'Computer'
        ParamType = ptInputOutput
        Size = 50
        Value = Null
      end
      item
        DataType = ftString
        Name = 'Lista'
        ParamType = ptInputOutput
        Size = 50
        Value = Null
      end>
    ReadOnly = True
    Left = 256
    Top = 111
  end
  object dsReportaHuellas: TDataSource
    DataSet = cdsReportaHuellas
    Left = 352
    Top = 111
  end
  object cdsListado: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftDateTime
        Name = 'FechaInicial'
        ParamType = ptInput
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'FechaFinal'
        ParamType = ptInput
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'TipoComidas'
        ParamType = ptInput
        Value = #39#39
      end
      item
        DataType = ftString
        Name = 'Reloj'
        ParamType = ptInput
        Value = #39#39
      end
      item
        DataType = ftString
        Name = 'Detalle'
        ParamType = ptInputOutput
        Value = #39#39
      end
      item
        DataType = ftString
        Name = 'Totales'
        ParamType = ptInputOutput
        Value = #39#39
      end>
    Left = 256
    Top = 160
  end
end
