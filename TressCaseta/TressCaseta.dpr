program TressCaseta;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  Forms,
  ZBaseTerminalShell in '..\Tools\ZBaseTerminalShell.pas' {BaseTerminalShell},
  FTerminalShell in 'FTerminalShell.pas' {TerminalShell},
  dCaseta in 'dCaseta.pas' {dmCaseta: TDataModule},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FUltimasChecadas in 'FUltimasChecadas.pas' {UltimasChecadas},
  GraphicField in '..\ServerCafe\Cliente\GraphicField.pas',
  GrabSplitter in '..\ServerCafe\Cliente\GrabSplitter.pas',
  CafeteraUtils in '..\ServerCafe\CafeteraUtils.pas';

{$R *.RES}
{$R WindowsXP.res}
{***DevEx (by am): Spanish.RES es el recurso con todas las traducciones de textos para los componentes de
DevExpress. El recruso es controlado por los componentes cxLocalizer que se encuentran en el Shell y en la forma
de la Matriz de habilidades. Puede haber varios cxLocalizer en el proyecto, pero debe haber un solo recurso de traduccion.
Estos recursos son generados desde un proyecto el cual se encuentra en StartTeam (Traducciones\Spanish.ini),. Cuando se
requiera agregar un texto el Spanish.ini debe ser modificado, y el Spanish.RES debe ser construido y cambiado en los folders
de los proyectos.

La siguiente directiva es la que indica que se utilizara este recurso el cual debe estar presente en el folder del proyecto.
***}

{$R ..\Traducciones\Spanish.RES}

begin
  Application.Initialize;
  Application.Title := 'Tress Caseta';
  Application.CreateForm(TTerminalShell, TerminalShell);
  Application.Run;
end.
