unit FUltimasChecadas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Mask, ZetaNumero, ZetaDBTextBox, Buttons,
  ExtCtrls, ZetaEdit, Grids, DBGrids, Db;

type
  TUltimasChecadas = class(TZetaDlgModal)
    gridChecadas: TDBGrid;
    DataSource: TDataSource;
    procedure gridChecadasDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gridChecadasDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
  private
    { Private declarations }
    procedure HabilitaControles;
  public
    { Public declarations }
  end;

var
  UltimasChecadas: TUltimasChecadas;

implementation

uses
    dCaseta,
    FTerminalShell,
    ZetaDialogo;

{$R *.DFM}

procedure TUltimasChecadas.gridChecadasDblClick(Sender: TObject);
begin
     inherited;
     if OK.Enabled then
        ModalResult := mrOK
     else
         ModalResult := mrCancel;
end;

procedure TUltimasChecadas.FormCreate(Sender: TObject);
begin
     DataSource.DataSet := dmCaseta.cdsUltimas;
end;

procedure TUltimasChecadas.FormShow(Sender: TObject);
begin
     inherited;
     Habilitacontroles;
end;

procedure TUltimasChecadas.HabilitaControles;
begin
     OK.Enabled := ( DataSource.DataSet.RecordCount <> 0 );
     if OK.Enabled then
     begin
          with Cancelar do
          begin
               Kind := bkCancel;
               ModalResult := mrCancel;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end
     else
     begin
          with Cancelar do
          begin
               Kind := bkClose;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
               if Self.Active then  // EZM: S�lo si la forma es visible
                  SetFocus;
          end;
     end;
end;

procedure TUltimasChecadas.gridChecadasDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);

   procedure PintaColumna(const sField: string);
   begin
        if ( Column.FieldName = sField ) then
        begin
             with gridChecadas do
             begin
                  with DataSource.DataSet do
                  begin
                       if ( FieldByName('StatusChecada').AsString = 'Error Grave' ) or ( FieldByName('StatusChecada').AsString = 'Error' ) then
                          Canvas.Font.Color := clRed
                       else
                           Canvas.Font.Color := clGreen;
                  end;
                  DefaultDrawDataCell( Rect, Column.Field, State );
             end;
        end;
   end;

begin
     PintaColumna( 'Codigo' );
     PintaColumna( 'Hora' );     
     PintaColumna( 'Nombre' );
     PintaColumna( 'StatusChecada' );
end;

end.
