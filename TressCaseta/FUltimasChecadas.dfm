inherited UltimasChecadas: TUltimasChecadas
  Left = 288
  Top = 210
  Caption = '�ltimas Checadas'
  ClientHeight = 271
  ClientWidth = 488
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 235
    Width = 488
    inherited OK: TBitBtn
      Left = 320
    end
    inherited Cancelar: TBitBtn
      Left = 405
    end
  end
  object gridChecadas: TDBGrid
    Left = 0
    Top = 0
    Width = 488
    Height = 235
    Align = alClient
    DataSource = DataSource
    FixedColor = clMaroon
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    Options = [dgTitles, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clYellow
    TitleFont.Height = -16
    TitleFont.Name = 'Arial Black'
    TitleFont.Style = []
    OnDrawColumnCell = gridChecadasDrawColumnCell
    OnDblClick = gridChecadasDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Codigo'
        Title.Caption = 'Gafete'
        Title.Color = clNavy
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Hora'
        Title.Color = clNavy
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nombre'
        Title.Caption = 'Empleado'
        Title.Color = clNavy
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'StatusChecada'
        Title.Caption = 'Estatus'
        Title.Color = clNavy
        Width = 155
        Visible = True
      end>
  end
  object DataSource: TDataSource
    Left = 264
  end
end
